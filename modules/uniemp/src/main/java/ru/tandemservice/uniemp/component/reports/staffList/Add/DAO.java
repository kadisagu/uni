/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffList.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.OrgUnitRankComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.organization.catalog.entity.codes.OrgUnitTypeCodes;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;
import ru.tandemservice.uniemp.entity.report.StaffListReport;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author dseleznev
 *         Created on: 05.08.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null != model.getFiltersPresetModel() && null != model.getFiltersPresetModel().getFiltersPreset()) return;

        model.setReportDate(new Date());
        model.setFormingDate(new Date());
        List<IdentifiableWrapper> activityTypesList = new ArrayList<>();
        activityTypesList.add(new IdentifiableWrapper(StaffListReportUtil.ACTIVITY_TYPE_BUDGET, "Бюджетные средства"));
        activityTypesList.add(new IdentifiableWrapper(StaffListReportUtil.ACTIVITY_TYPE_OFF_BUDGET, "Внебюджетные средства"));
        model.setActivityTypesList(activityTypesList);

        model.setOrgUnitExcludeListModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IEntity> resultList = new ArrayList<>();

                for (Object key : primaryKeys)
                {
                    OrgUnit orgUnit = get(OrgUnit.class, (Long) key);
                    if (model.getOrgUnitList() == null || model.getOrgUnitList().isEmpty())
                        resultList.add(orgUnit);
                    else
                        for (OrgUnit orgUnitParent : model.getOrgUnitList())
                            if (orgUnit.getParent().equals(orgUnitParent))
                                resultList.add(orgUnit);
                }

                return resultList;
            }

            @Override
            public ListResult findValues(String filter)
            {
                if (model.getOrgUnitList() == null || model.getOrgUnitList().isEmpty())
                    return OrgUnitManager.instance().dao().getOrgUnitList(filter, 50, true);

                List<OrgUnit> resultList = new ArrayList<>();
                for (OrgUnit orgUnit : model.getOrgUnitList())
                {
                    for (OrgUnit childOrgUnit : OrgUnitManager.instance().dao().getChildOrgUnits(orgUnit, true))
                        if (childOrgUnit.getTitle().contains(filter))
                            resultList.add(childOrgUnit);
                }

                return new ListResult<>(resultList);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnit) value).getTitleWithType();
            }
        });

        model.setOrgUnitListModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IEntity> resultList = new ArrayList<>();

                for (Object key : primaryKeys)
                {
                    IEntity entity = get(OrgUnit.class, (Long) key);
                    resultList.add(entity);
                }

                return resultList;
            }

            @Override
            public ListResult findValues(String filter)
            {
                return OrgUnitManager.instance().dao().getOrgUnitList(filter, 50, true);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnit) value).getTitleWithType();
            }
        });
        model.setEmployeeTypeListModel(new LazySimpleSelectModel<>(EmployeeType.class).setSortProperty(EmployeeType.P_CODE));
        model.setQualificationLevelsList(getList(QualificationLevel.class, QualificationLevel.P_CODE));
        model.setProfQualificationGroupsList(getList(ProfQualificationGroup.class, ProfQualificationGroup.P_CODE));

        model.setEmployeePostListModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IEntity> resultList = new ArrayList<>();
                for (Object primaryKey : primaryKeys)
                {
                    IEntity entity = get(PostBoundedWithQGandQL.class, (Long) primaryKey);
                    if (findValues("").getObjects().contains(entity))
                        resultList.add(entity);
                }

                return resultList;
            }

            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "rel", new String[]{OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L});
                builder.add(MQExpression.like("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE, CoreStringUtils.escapeLike(filter)));

                if (null != model.getOrgUnitList())
                    for (OrgUnit orgUnit : model.getOrgUnitList())
                        builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_ORG_UNIT_TYPE, orgUnit.getOrgUnitType()));

                if (null != model.getEmployeeTypeList() && !model.getEmployeeTypeList().isEmpty())
                    builder.add(MQExpression.in("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getEmployeeTypeList()));

                if (null != model.getQualificationLevel())
                    builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, model.getQualificationLevel()));

                if (null != model.getProfQualificationGroup())
                    builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, model.getProfQualificationGroup()));

                builder.setNeedDistinct(true);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((PostBoundedWithQGandQL) value).getFullTitleWithSalary();
            }
        });

        model.setFiltersPresetModel(new FiltersPresetModel("StaffListReport", new IFiltersPresetManager()
        {
            @Override
            public void initFilters(IDataSettings dataSettings)
            {
                model.setOrgUnitList(dataSettings.<List<OrgUnit>>get("orgUnit"));
                model.setOrgUnitExcludeList(dataSettings.<List<OrgUnit>>get("orgUnitExclude"));
                model.setPostList(dataSettings.<List<PostBoundedWithQGandQL>>get("post"));
                model.setEmployeeTypeList(dataSettings.<List<EmployeeType>>get("employeeType"));
                model.setQualificationLevel(dataSettings.<QualificationLevel>get("qualificationLevel"));
                model.setProfQualificationGroup(dataSettings.<ProfQualificationGroup>get("profQualificationGroup"));
                model.setActivityType(dataSettings.<IdentifiableWrapper>get("activityType"));
                model.setSelectedOrgUnitTypesList(dataSettings.<List<IEntity>>get("selectedOrgUnitTypes"));
                model.setSelectedShowChildOrgUnitsList(dataSettings.<List<IEntity>>get("selectedShowChildOrgUnitsList"));
                model.setSelectedShowChildOrgUnitsAsOwnList(dataSettings.<List<IEntity>>get("selectedShowChildOrgUnitsAsOwnList"));
                model.setShowOrgUnitWithoutStaffList(dataSettings.<Boolean>get("showOrgUnitWithoutStaffList"));

                if (null == model.getSelectedOrgUnitTypesList()) model.setSelectedOrgUnitTypesList(new ArrayList<>());
                if (null == model.getSelectedShowChildOrgUnitsList()) model.setSelectedShowChildOrgUnitsList(new ArrayList<>());
                if (null == model.getSelectedShowChildOrgUnitsAsOwnList()) model.setSelectedShowChildOrgUnitsAsOwnList(new ArrayList<>());
            }

            @Override
            public void setFilters(IDataSettings dataSettings)
            {
                dataSettings.set("post", model.getPostList());
                dataSettings.set("orgUnit", model.getOrgUnitList());
                dataSettings.set("orgUnitExclude", model.getOrgUnitExcludeList());
                dataSettings.set("activityType", model.getActivityType());
                dataSettings.set("employeeType", model.getEmployeeTypeList());
                dataSettings.set("qualificationLevel", model.getQualificationLevel());
                dataSettings.set("profQualificationGroup", model.getProfQualificationGroup());
                dataSettings.set("selectedOrgUnitTypes", model.getSelectedOrgUnitTypesList());
                dataSettings.set("selectedShowChildOrgUnitsList", model.getSelectedShowChildOrgUnitsList());
                dataSettings.set("selectedShowChildOrgUnitsAsOwnList", model.getSelectedShowChildOrgUnitsAsOwnList());
                dataSettings.set("showOrgUnitWithoutStaffList", model.getShowOrgUnitWithoutStaffList());
            }
        }, new String[]{"presetsBlock", "activityType", "orgUnit", "orgUnitExclude", "employeeType", "qualificationLevel", "profQualificationGroup", "post", "orgUnitTypesList", "showOrgUnitWithoutStaffList"}, getSession()));

        prepareOrgUnitTypesList(model);
        preparePaymentsList(model);
    }

    /**
     * Подготавливает данные для фильтра со списком подразделений ОУ
     */
    private void prepareOrgUnitTypesList(Model model)
    {
        MQBuilder builder = new MQBuilder(OrgUnitType.ENTITY_CLASS, "out");
        builder.add(MQExpression.eq("out", OrgUnitType.P_ENABLED, Boolean.TRUE));
        builder.add(MQExpression.notEq("out", OrgUnitType.P_CODE, OrgUnitTypeCodes.TOP));
        builder.addOrder("out", OrgUnitType.P_PRIORITY);
        List<OrgUnitType> typesList = builder.getResultList(getSession());

        List<OrgUnitTypeWrapper> orgUnitTypesList = new ArrayList<>();
        for (OrgUnitType out : typesList)
        {
            orgUnitTypesList.add(new OrgUnitTypeWrapper(out, true, false, false));
        }
        model.setOrgUnitTypesList(orgUnitTypesList);

        // Устанавливаем значения по умолчанию при загрузке формы
        List<IEntity> selectedTypes = new ArrayList<>();
        selectedTypes.addAll(orgUnitTypesList);

        List<IEntity> selectedShowChilds = new ArrayList<>();
        selectedShowChilds.addAll(orgUnitTypesList);

        model.setSelectedOrgUnitTypesList(selectedTypes);
        model.setSelectedShowChildOrgUnitsList(selectedShowChilds);
    }

    /**
     * Подготавливает данные для фильтра со списком выплат
     */
    private void preparePaymentsList(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListRepPaymentSettings.ENTITY_CLASS, "s");
        AbstractExpression expr1 = MQExpression.eq("s", StaffListRepPaymentSettings.P_TAKE_INTO_ACCOUNT, Boolean.TRUE);
        AbstractExpression expr2 = MQExpression.isNull("s", StaffListRepPaymentSettings.L_PAYMENT);
        builder.add(MQExpression.or(expr1, expr2));
        builder.addOrder("s", StaffListRepPaymentSettings.P_PRIORITY);
        model.setPaymentsList(builder.<StaffListRepPaymentSettings>getResultList(getSession()));
    }

    @Override
    public void prepareOrgUnitTypesDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getOrgUnitTypesDataSource(), model.getOrgUnitTypesList());
    }

    /**
     * Подготавливает полные списки требуемых данных из базы:
     * 1. Допустимые подразделения,
     * 2. Должности штатного расписания допустимых подразделений,
     * 3. Должности штатной расстановки допустимых подразделений,
     * 4. Выплаты по должностям штатной расстановки допустимых подразделений.
     */
    private void getDataFromDataBase(Model model)
    {
        List<OrgUnitType> orgUnitTypesList = new ArrayList<>();
        for (OrgUnitTypeWrapper type : model.getOrgUnitTypesList())
            if (type.isUseOrgUnit()) orgUnitTypesList.add(type.getOrgUnitType());

        List<Payment> paymentsList = new ArrayList<>();
        for (StaffListRepPaymentSettings setting : model.getPaymentsList())
            if (setting.isTakeIntoAccount()) paymentsList.add(setting.getPayment());

        List<Long> allowedOrgUnitIdsList = new ArrayList<>();
        if (null != model.getOrgUnitList())
        {
            for (OrgUnit choseOrgUnit : model.getOrgUnitList())
            {
                allowedOrgUnitIdsList.add(choseOrgUnit.getId());
                for (OrgUnit orgUnit : OrgUnitManager.instance().dao().getChildOrgUnits(choseOrgUnit, true))
                    if (!orgUnit.isArchival() || orgUnit.getArchivingDate().getTime() > model.getReportDate().getTime())
                        allowedOrgUnitIdsList.add(orgUnit.getId());
            }
        }

        if (model.getOrgUnitExcludeList() != null)
            for (OrgUnit orgUnit : model.getOrgUnitExcludeList())
                allowedOrgUnitIdsList.remove(orgUnit.getId());

        AbstractExpression expr1 = MQExpression.isNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression expr2 = MQExpression.isNotNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression expr3 = MQExpression.greatOrEq("ou", OrgUnit.P_ARCHIVING_DATE, model.getReportDate());

        MQBuilder orgUnitBuilder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        orgUnitBuilder.add(MQExpression.in("ou", OrgUnit.L_ORG_UNIT_TYPE, orgUnitTypesList));
        if (!allowedOrgUnitIdsList.isEmpty())
            orgUnitBuilder.add(MQExpression.in("ou", OrgUnit.P_ID, allowedOrgUnitIdsList));
        else if (model.getOrgUnitExcludeList() != null && !model.getOrgUnitExcludeList().isEmpty())
            orgUnitBuilder.add(MQExpression.notIn("ou", OrgUnit.P_ID, ids(model.getOrgUnitExcludeList())));
        orgUnitBuilder.add(MQExpression.or(expr1, MQExpression.and(expr2, expr3)));

        orgUnitBuilder.addOrder("ou", OrgUnit.P_TITLE);
        List<OrgUnit> orgUnitsList = orgUnitBuilder.getResultList(getSession());

        List<OrgUnit> staffOrgUnits = new ArrayList<>();
        MQBuilder staffListBuilder = new MQBuilder(StaffList.ENTITY_CLASS, "sl");
        staffListBuilder.add(MQExpression.in("sl", StaffList.staffListState().code().s(), UniempDefines.STAFF_LIST_STATUS_ACTIVE, UniempDefines.STAFF_LIST_STATUS_ARCHIVE));
        staffListBuilder.add(MQExpression.lessOrEq("sl", StaffList.P_ACCEPTION_DATE, model.getReportDate()));
        staffListBuilder.add(MQExpression.in("sl", StaffList.L_ORG_UNIT, orgUnitsList));
        staffListBuilder.addDescOrder("sl", StaffList.P_ACCEPTION_DATE);

        List<StaffList> staffListsList = new ArrayList<>();
        for (StaffList staffList : staffListBuilder.<StaffList>getResultList(getSession()))
        {
            if (!staffOrgUnits.contains(staffList.getOrgUnit()) && (null == staffList.getArchivingDate() || (staffList.getArchivingDate().getTime() >= model.getReportDate().getTime())))
            {
                staffListsList.add(staffList);
                staffOrgUnits.add(staffList.getOrgUnit());
            }
        }

        MQBuilder staffListItemsBuilder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        staffListItemsBuilder.add(MQExpression.in("sli", StaffListItem.L_STAFF_LIST, staffListsList));
        if (model.getActivityType() != null)
            staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.financingSource().code().s(), (StaffListReportUtil.ACTIVITY_TYPE_BUDGET.equals(model.getActivityType().getId()) ? UniempDefines.FINANCING_SOURCE_BUDGET : UniempDefines.FINANCING_SOURCE_OFF_BUDGET)));
        if (null != model.getPostList() && !model.getPostList().isEmpty())
        {
            staffListItemsBuilder.add(MQExpression.in("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, model.getPostList()));
        }
        else
        {
            if (null != model.getEmployeeTypeList() && !model.getEmployeeTypeList().isEmpty())
                staffListItemsBuilder.add(MQExpression.in("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getEmployeeTypeList()));

            if (null != model.getQualificationLevel())
                staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, model.getQualificationLevel()));

            if (null != model.getProfQualificationGroup())
                staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, model.getProfQualificationGroup()));
        }
        //        staffListItemsBuilder.addOrder("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_ORG_UNIT + "." + OrgUnit.P_TITLE);

        List<StaffListItem> staffListItemsList = staffListItemsBuilder.getResultList(getSession());

        MQBuilder allocItemsBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "ai");
        allocItemsBuilder.add(MQExpression.in("ai", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffListsList));
        if (null != model.getPostList() && !model.getPostList().isEmpty())
        {
            allocItemsBuilder.add(MQExpression.in("ai", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, model.getPostList()));
        }
        else
        {
            if (null != model.getEmployeeTypeList() && !model.getEmployeeTypeList().isEmpty())
                allocItemsBuilder.add(MQExpression.in("ai", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, model.getEmployeeTypeList()));

            if (null != model.getQualificationLevel())
                allocItemsBuilder.add(MQExpression.eq("ai", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, model.getQualificationLevel()));

            if (null != model.getProfQualificationGroup())
                allocItemsBuilder.add(MQExpression.eq("ai", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, model.getProfQualificationGroup()));
        }

        List<StaffListAllocationItem> allocItemsList = allocItemsBuilder.getResultList(getSession());

        MQBuilder paymentsBuilder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "pi");
        paymentsBuilder.addLeftJoin("pi", StaffListPaymentBase.L_FINANCING_SOURCE, "fs");
        paymentsBuilder.add(MQExpression.in("pi", StaffListPaymentBase.L_STAFF_LIST_ITEM, staffListItemsList));
        paymentsBuilder.add(MQExpression.in("pi", StaffListPaymentBase.L_PAYMENT, paymentsList));
        if (null != model.getActivityType())
        {
            AbstractExpression paymExpr1 = MQExpression.isNull("pi", StaffListPaymentBase.L_FINANCING_SOURCE);
            AbstractExpression paymExpr2 = MQExpression.eq("pi", StaffListPaymentBase.L_FINANCING_SOURCE + "." + ICatalogItem.CATALOG_ITEM_CODE, model.getActivityType().getId().equals(StaffListReportUtil.ACTIVITY_TYPE_BUDGET) ? UniempDefines.FINANCING_SOURCE_BUDGET : UniempDefines.FINANCING_SOURCE_OFF_BUDGET);
            paymentsBuilder.add(MQExpression.or(paymExpr1, paymExpr2));
        }
        List<StaffListPaymentBase> slPaymentsList = paymentsBuilder.getResultList(getSession());

        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", EmployeePost.L_POST_STATUS + "." + EmployeePostStatus.P_ACTIVE, Boolean.TRUE));
        builder.add(MQExpression.isNotNull("p", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_RANK));
        builder.addOrder("p", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_RANK);
        //builder.add(MQExpression.eq("p", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_RANK, "Ректор"));
        List<EmployeePost> rectors = builder.getResultList(getSession());
        if (null != TopOrgUnit.getInstance().getHead()) rectors.add(0, (EmployeePost) TopOrgUnit.getInstance().getHead());
        if (!rectors.isEmpty())
        {
            IdentityCard identityCard = rectors.get(0).getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder rectorFIO = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName)) rectorFIO.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName)) rectorFIO.append(middleName.substring(0, 1).toUpperCase()).append(".");
            rectorFIO.append(" ").append(lastName);
            model.setRector(rectorFIO.toString());
        }

        model.setAccountableOrgUnitsList(orgUnitsList);
        model.setRawStaffListItemsList(staffListItemsList);
        model.setRawStaffListAllocationItemsList(allocItemsList);
        model.setRawStaffListPaymentsList(slPaymentsList);
    }

    /**
     * Формирует печатную форму отчета
     */
    private RtfDocument getDocument(final Model model)
    {
        // Подготавливаем данные к генерации печатной формы отчета
        getDataFromDataBase(model);
        StaffListReportUtil.prepareSerializedOrgstructure(this, model, model.getAccountableOrgUnitsList());
        StaffListReportUtil.preparePaymentsHeader(model);
        StaffListReportUtil.prepareDataForPrinting(model); //TODO:
        StaffListReportUtil.renderTableData(model); //TODO:

        // получить шаблон отчета
        ITemplateDocument templateDocument = getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_PPS_STAFF_LIST);
        RtfDocument template = new RtfReader().read(templateDocument.getContent());
        RtfDocument document = template.getClone();

        TopOrgUnit academy = TopOrgUnit.getInstance();
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("academy_G", null != academy.getGenitiveCaseTitle() ? academy.getGenitiveCaseTitle() : academy.getFullTitle());
        paramModifier.put("dateStr", new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(model.getReportDate()));
        paramModifier.put("employeeType", null != model.getEmployeeTypeList() && model.getEmployeeTypeList().size() == 1 ? ": " + model.getEmployeeTypeList().get(0).getTitle() : "");
        paramModifier.put("rector", model.getRector());

        RtfTableModifier tableModifier = new RtfTableModifier();

        tableModifier.put("PH", new String[][]{});
        tableModifier.put("CN", new String[][]{model.getNumberHeaders()});
        tableModifier.put("S", model.getSummaryData());
        tableModifier.put("T", model.getTableData());

        // Устанавливаем модифаер для таблицы заголовков верхнего уровня
        tableModifier.put("PH", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                if (model.getPaymentColumnWidth().length > 0)
                {
                    RtfUtil.splitRow(table.getRowList().get(1), StaffListReportUtil.PAYMENTS_COLUMN_IDX, (newCell, index) -> {
                        String content = model.getPaymentHeaders()[index];
                        IRtfElement text = RtfBean.getElementFactory().createRtfText(content);
                        newCell.getElementList().add(text);
                    }, model.getPaymentColumnWidth());
                }
                else
                {
                    RtfUtil.splitRow(table.getRowList().get(1), StaffListReportUtil.PAYMENTS_COLUMN_IDX, (newCell, index) -> {
                        String content = "Надбавок и доплат нет";
                        IRtfElement text = RtfBean.getElementFactory().createRtfText(content);
                        newCell.getElementList().add(text);
                    }, new int[]{1});
                }
            }
        });

        // Устанавливаем модифаер для нумерованного заголовка таблицы
        tableModifier.put("CN", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                if (model.getPaymentColumnWidth().length > 0)
                    for (RtfRow row : table.getRowList())
                        RtfUtil.splitRow(row, StaffListReportUtil.PAYMENTS_COLUMN_IDX + (table.getRowList().indexOf(row) == table.getRowList().size() - 1 ? -1 : 0), null, model.getPaymentColumnWidth());
            }
        });

        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            // Форматируем строки в соответствии с установленными стилями
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int cnt = -startIndex;
                for (RtfRow row : newRowList)
                {
                    if (cnt >= 0 && null != model.getRowStyles()[cnt])
                        StaffListReportUtil.applyRtfRowStyles(row, model.getRowStyles()[cnt]);
                    cnt++;
                }
            }
        });

        paramModifier.modify(document);
        tableModifier.modify(document);
        return document;
    }

    /**
     * Генерирует и сохраняет отчет в базу данных
     */
    @Override
    public void update(Model model)
    {
        if (null == model.getSelectedOrgUnitTypesList() || model.getSelectedOrgUnitTypesList().isEmpty())
            throw new ApplicationException("Необходимо указать хотя бы один тип подразделений.");

        StaffListReport report = model.getReport();

        report.setFormingDate(new Date());
        report.setReportDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDate()));
        report.setActivityType(null != model.getActivityType() ? model.getActivityType().getTitle() : null);
        StringBuilder orgUnitBuilder = new StringBuilder();
        if (model.getOrgUnitList() != null)
        {
            for (OrgUnit orgUnit : model.getOrgUnitList())
                orgUnitBuilder.append(orgUnitBuilder.length() > 0 ? ";" : "").append(orgUnit.getFullTitle());
            if (orgUnitBuilder.length() > 3999)
                orgUnitBuilder.delete(3997, orgUnitBuilder.length()).append("..");
        }
        report.setOrgUnit(orgUnitBuilder.length() > 0 ? orgUnitBuilder.append(".").toString() : null);
        StringBuilder orgUnitExcludeBuilder = new StringBuilder();
        if (model.getOrgUnitExcludeList() != null)
        {
            for (OrgUnit exclideOrgUnit : model.getOrgUnitExcludeList())
                orgUnitExcludeBuilder.append(orgUnitExcludeBuilder.length() > 0 ? ";" : "").append(exclideOrgUnit.getFullTitle());
            if (orgUnitExcludeBuilder.length() > 3999)
                orgUnitExcludeBuilder.delete(3997, orgUnitExcludeBuilder.length()).append("..");
        }
        report.setExcludeOrgUnit(orgUnitExcludeBuilder.length() > 0 ? orgUnitExcludeBuilder.append(".").toString() : null);
        report.setQualificationLevel(null != model.getQualificationLevel() ? model.getQualificationLevel().getTitle() : null);
        report.setProfQualificationGroup(null != model.getProfQualificationGroup() ? model.getProfQualificationGroup().getTitle() : null);
        StringBuilder postBuilder = new StringBuilder();
        if (model.getPostList() != null)
        {
            for (PostBoundedWithQGandQL post : model.getPostList())
                postBuilder.append(postBuilder.length() > 0 ? ";" : "").append(post.getFullTitleWithSalary());
            if (postBuilder.length() > 3999)
                postBuilder.delete(3997, postBuilder.length()).append("..");
        }
        report.setPost(postBuilder.length() > 0 ? postBuilder.append(".").toString() : null);
        StringBuilder emplTypesBuilder = new StringBuilder();
        for (EmployeeType type : model.getEmployeeTypeList())
            emplTypesBuilder.append(emplTypesBuilder.length() > 0 ? ";" : "").append(type.getShortTitle());
        report.setEmployeePostType(emplTypesBuilder.length() > 0 ? emplTypesBuilder.append(".").toString() : "");

        if (null == model.getSelectedOrgUnitTypesList() || model.getSelectedOrgUnitTypesList().isEmpty())
            report.setOrgUnitTypes(null);
        else if (model.getOrgUnitTypesList().size() == model.getSelectedOrgUnitTypesList().size())
            report.setOrgUnitTypes("Все");
        else
        {
            StringBuilder orgUnitTypesStr = new StringBuilder();
            for (IEntity wrapper : model.getSelectedOrgUnitTypesList())
                orgUnitTypesStr.append(orgUnitTypesStr.length() > 0 ? "; " : "").append(((OrgUnitTypeWrapper) wrapper).getOrgUnitType().getTitle());
            report.setOrgUnitTypes(orgUnitTypesStr.append(".").toString());
        }

        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(RtfUtil.toByteArray(getDocument(model)));
        getSession().save(databaseFile);

        report.setContent(databaseFile);
        getSession().save(report);
    }

    /**
     * Возвращает список дочерних подразделений для указанного подразделения,
     * включая дочерние подразделения дочерних подразделений и т.д.
     * Другими словами, возвращает иерархию оргструктуры в последовательном виде
     */
    @Override
    public List<OrgUnit> getChildOrgUnits(OrgUnit orgUnit, List<OrgUnit> orgUnitsToTake, Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap, Date actualDate)
    {
        if (null != orgUnit.getParent() && !orgUnitTypesMap.get(orgUnit.getOrgUnitType().getId()).isShowChildOrgUnits())
            return new ArrayList<>();

        List<OrgUnit> result = new ArrayList<>();

        AbstractExpression expr1 = MQExpression.isNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression expr2 = MQExpression.isNotNull("ou", OrgUnit.P_ARCHIVING_DATE);
        AbstractExpression expr3 = MQExpression.greatOrEq("ou", OrgUnit.P_ARCHIVING_DATE, actualDate);

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.L_PARENT, orgUnit));
        builder.add(MQExpression.or(expr1, MQExpression.and(expr2, expr3)));
        builder.addOrder("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_PRIORITY);
        builder.addOrder("ou", OrgUnit.P_NOMINATIVE_CASE_TITLE);
        builder.addOrder("ou", OrgUnit.P_TITLE);
        List<OrgUnit> childsList = builder.getResultList(getSession());

        Collections.sort(childsList, OrgUnitRankComparator.INSTANCE);

        for (OrgUnit child : childsList)
        {
            if (orgUnitsToTake.contains(child))
            {
                result.add(child);
                if (orgUnitTypesMap.get(child.getOrgUnitType().getId()).isShowChildOrgUnits())
                    result.addAll(getChildOrgUnits(child, orgUnitsToTake, orgUnitTypesMap, actualDate));
            }
        }

        return result;
    }

    @Override
    public List<OrgUnit> getOrgUnitWithActiveStaffList()
    {
        MQBuilder builder = new MQBuilder(StaffList.ENTITY_CLASS, "b", new String[]{StaffList.orgUnit().s()});
        builder.add(MQExpression.eq("b", StaffList.staffListState().code().s(), UniempDefines.STAFF_LIST_STATUS_ACTIVE));

        return builder.getResultList(getSession());
    }
}