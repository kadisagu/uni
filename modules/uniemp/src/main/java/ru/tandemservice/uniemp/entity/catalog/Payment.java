package ru.tandemservice.uniemp.entity.catalog;

import java.util.List;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.gen.PaymentGen;

public class Payment extends PaymentGen implements ITitledWithCases
{
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_PAYMENTS_REL_LIST = "paymentsRelList";
    public static final Object TYPE_KEY = new String[]{L_TYPE, PaymentType.P_TITLE};
    public static final Object UNIT_KEY = new String[]{L_PAYMENT_UNIT, PaymentUnit.P_TITLE};
    public static final Object FINANCING_SOURCE_KEY = new String[]{L_FINANCING_SOURCE, FinancingSource.P_TITLE};

    public List<Payment> getPaymentsRelList()
    {
        return UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(this);
    }

    public Payment()
    {
        super();
    }

    public Payment(String title)
    {
        super();
        setTitle(title);
    }

    /**
     * Dummy-payment constructor
     */
    public Payment(Long id, String title)
    {
        super();
        setId(id);
        setTitle(title);
    }

    public String getFullTitle()
    {
        return getTitle() + " (" + getType().getShortTitle() + ")";
    }

    @Override
    public String getCaseTitle(GrammaCase caze)
    {
        return TitledWithCasesUtil.getCaseTitle(this, caze);
    }
}