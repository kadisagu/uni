/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.ui.Detail.EmpMedicalStuffReportDetailUI;
import ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 01.04.13
 */
public class EmpMedicalStuffReportDetailIgnoreSearchDSHandler extends DefaultSearchDataSourceHandler
{
    public EmpMedicalStuffReportDetailIgnoreSearchDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        final Collection<Long> ignoreEmployeeId = context.get(EmpMedicalStuffReportDetailUI.PROP_IGNORE_EMPLOYEE__ID_LIST);
        final Date reportDate = context.get(EmpMedicalStuffReportDetailUI.PROP_REPORT_DATE);
        final List<Long> postTypeIdList = context.get(EmpMedicalStuffReportDetailUI.PROP_POST_TYPE_ID_LIST);
        final Collection<Long> medicalEducationLevelIdList = context.get(EmpMedicalStuffReportDetailUI.PROP_MED_SPEC_ID_LIST);
        final List<Long> employeeTypeIdList = context.get(EmpMedicalStuffReportDetailUI.PROP_EMPLOYEE_TYPE_ID_LIST);
        final Long eduLevelId = context.get(EmpMedicalStuffReportDetailUI.PROP_EDU_LEVEL_ID);

        final List<EmployeeMedicalSpeciality> employeeMedicalSpecialityList = new ArrayList<>();
        BatchUtils.execute(ignoreEmployeeId, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeMedicalSpeciality.class, "m")
                    .where(in(property(EmployeeMedicalSpeciality.employee().id().fromAlias("m")), elements))
                    .where(le(property(EmployeeMedicalSpeciality.assignDate().fromAlias("m")), value(reportDate, PropertyType.DATE)))
                    .where(or(
                            isNull(property(EmployeeMedicalSpeciality.categoryAssignDate().fromAlias("m"))),
                            le(property(EmployeeMedicalSpeciality.categoryAssignDate().fromAlias("m")), value(reportDate, PropertyType.DATE))));

            if (medicalEducationLevelIdList != null && !medicalEducationLevelIdList.isEmpty())
                builder.where(in(property(EmployeeMedicalSpeciality.medicalEducationLevel().id().fromAlias("m")), medicalEducationLevelIdList));

            employeeMedicalSpecialityList.addAll(builder.createStatement(context.getSession()).<EmployeeMedicalSpeciality>list());
        });

        final List<EmployeePost> employeePostList = new ArrayList<>();
        BatchUtils.execute(CommonBaseUtil.<Long>getPropertiesList(employeeMedicalSpecialityList, EmployeeMedicalSpeciality.employee().id().s()), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep")
                .where(in(property(EmployeePost.employee().id().fromAlias("ep")), elements))
                .where(ne(property(EmployeePost.postStatus().code().fromAlias("ep")), value(EmployeePostStatusCodes.STATUS_POSSIBLE)))
                .where(le(property(EmployeePost.postDate().fromAlias("ep")), value(reportDate, PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeePost.dismissalDate().fromAlias("ep"))),
                        ge(property(EmployeePost.dismissalDate().fromAlias("ep")), value(reportDate, PropertyType.DATE))));

        if (employeeTypeIdList != null && !employeeTypeIdList.isEmpty())
            builder.where(in(property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().id().fromAlias("ep")), employeeTypeIdList));

        if (eduLevelId != null)
            builder.where(or(
                    eq(property(EmployeePost.employee().person().personEduInstitution().educationLevelStage().id().fromAlias("ep")), value(eduLevelId)),
                    eq(property(EmployeePost.employee().person().personEduInstitution().educationLevelStage().parent().id().fromAlias("ep")), value(eduLevelId))
            ));

        if (postTypeIdList != null && !postTypeIdList.isEmpty())
            builder.where(in(property(EmployeePost.postType().id().fromAlias("ep")), postTypeIdList));

        employeePostList.addAll(builder.createStatement(context.getSession()).<EmployeePost>list());
        });

        Map<Employee, List<EmployeeMedicalSpeciality>> employeeMedicalSpecialityMap = new HashMap<>();
        for (EmployeeMedicalSpeciality speciality : employeeMedicalSpecialityList)
        {
            List<EmployeeMedicalSpeciality> list = employeeMedicalSpecialityMap.get(speciality.getEmployee());
            if (list == null)
                employeeMedicalSpecialityMap.put(speciality.getEmployee(), list = new ArrayList<>());
            list.add(speciality);
        }

        List<DataWrapper> wrapperList = new ArrayList<>();
        for (EmployeePost post : employeePostList)
        {
            DataWrapper wrapper = new DataWrapper(post.getId(), post.getPerson().getFullFio());
            wrapper.setProperty("employeeCode", post.getEmployee().getEmployeeCode());
            wrapper.setProperty("medSpec", CommonBaseEntityUtil.getPropertiesList(employeeMedicalSpecialityMap.get(post.getEmployee()), EmployeeMedicalSpeciality.P_CUSTOM_TITLE));
            wrapper.setProperty("post", post.getPostRelation().getPostBoundedWithQGandQL().getFullTitleWithSalary());
            wrapper.setProperty("orgUnit", post.getOrgUnit().getTitleWithType());
            wrapper.setProperty("postType", post.getPostType().getTitle());

            wrapper.setProperty("employeeId", post.getEmployee().getId());

            wrapperList.add(wrapper);
        }

        Collections.sort(wrapperList, new EntityComparator<>(input.getEntityOrder(), new EntityOrder("employeeId")));

        return ListOutputBuilder.get(input, wrapperList).pageable(true).build();
    }
}
