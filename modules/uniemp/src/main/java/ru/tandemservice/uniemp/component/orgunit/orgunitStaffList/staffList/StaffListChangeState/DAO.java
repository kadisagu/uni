/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListChangeState;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.StaffList;

/**
 * @author dseleznev
 * Created on: 20.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStaffList(get(StaffList.class, model.getStaffListId()));

        List<StaffListState> staffListStatesList = new ArrayList<>();
        if (model.getStaffList().getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_FORMING))
        {
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACCEPTING));
            model.getStaffList().setStaffListState(staffListStatesList.get(0));
        }
        else if (model.getStaffList().getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACCEPTING))
        {
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_REJECTED));
            model.getStaffList().setStaffListState(null);
        }
        else if (model.getStaffList().getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACCEPTED))
        {
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACTIVE));
        }
        else if (model.getStaffList().getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACTIVE))
        {
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ARCHIVE));
        }
        else if (model.getStaffList().getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_REJECTED) || model.getStaffList().getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ARCHIVE))
        {
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));
            model.getStaffList().setStaffListState(staffListStatesList.get(0));
        }

        model.setStaffListStatesList(staffListStatesList);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        if (UniempDefines.STAFF_LIST_STATUS_ACTIVE.equals(model.getStaffList().getStaffListState().getCode()))
        {
            if (null == model.getStaffList().getAcceptionDate())
                model.getStaffList().setAcceptionDate(new Date());

            Criteria crit = getSession().createCriteria(StaffList.class);
            crit.add(Restrictions.eq(StaffList.L_ORG_UNIT, model.getStaffList().getOrgUnit()));
            crit.add(Restrictions.ne(StaffList.P_ID, model.getStaffList().getId()));
            crit.add(Restrictions.eq(StaffList.L_STAFF_LIST_STATE, getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACTIVE)));
            for (StaffList slItem : (List<StaffList>)crit.list())
            {
                slItem.setStaffListState(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ARCHIVE));
                slItem.setArchivingDate(model.getStaffList().getAcceptionDate());
                update(slItem);
            }
        }
        
        if (!UniempDefines.STAFF_LIST_STATUS_ARCHIVE.equals(model.getStaffList().getStaffListState().getCode()))
            model.getStaffList().setArchivingDate(null);
        
        getSession().update(model.getStaffList());
    }
}