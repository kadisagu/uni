package ru.tandemservice.uniemp.component.reports.quantityEmployeeByDepartments.quantityEmployeeByDepartmentsPub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;

import ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport;

@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    QuantityEmpByDepReport _report = new QuantityEmpByDepReport();

    public QuantityEmpByDepReport getReport()
    {
        return _report;
    }

    public void setReport(QuantityEmpByDepReport report)
    {
        _report = report;
    }
}
