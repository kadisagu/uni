/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.AdditionalDataEdit;

import org.tandemframework.core.component.Input;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uniemp.entity.employee.EmployeeCard;

/**
 * @author dseleznev
 * Created on: 31.07.2008
 */
@Input(keys = { "employeeCardId", "employeeId" }, bindings = { "employeeCard.id", "employee.id" })
public class Model
{
    private Employee _employee = new Employee();
    private EmployeeCard _employeeCard = new EmployeeCard();

    public Employee getEmployee()
    {
        return _employee;
    }

    public void setEmployee(Employee employee)
    {
        this._employee = employee;
    }

    public EmployeeCard getEmployeeCard()
    {
        return _employeeCard;
    }

    public void setEmployeeCard(EmployeeCard employeeCard)
    {
        this._employeeCard = employeeCard;
    }

}