package ru.tandemservice.uniemp.component.reports.quantityEmployeeByDepartments.quantityEmployeeByDepartmentsAdd;

import java.util.Date;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.getReport().setFormingDate(new Date());
        model.getReport().setReportDate(new Date());
        model.setOrgUnitTypeList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnitType.ENTITY_NAME, alias);
                if (filter != null)
                {
                    builder.add(MQExpression.like(alias, OrgUnitType.title().toString(), filter));
                }
                builder.addOrder(alias, OrgUnitType.title().toString());
                return builder;
            }
        });
        model.setOrgUnitList(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_NAME, alias);

                if (filter != null)
                {
                    builder.add(MQExpression.like(alias, OrgUnit.title().s(), filter));
                }
                if (model.getReport().getOrgUnitType() != null)
                {
                    builder.add(MQExpression.eq(alias,  OrgUnit.orgUnitType().s(), model.getReport().getOrgUnitType()));
                }
                builder.add(MQExpression.eq(alias, OrgUnit.P_ARCHIVAL, false));
                builder.addOrder(alias, OrgUnit.title().s());
                builder.addOrder(alias, OrgUnit.orgUnitType().priority().s());
                return builder;
            }
        });

        ((UniQueryFullCheckSelectModel)model.getOrgUnitList()).setLabelProperties(new String[] {OrgUnit.fullTitle().s()});
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        DatabaseFile databaseFile = new QuantityEmpByDepReportBuilder(model, session).getContent();
        session.save(databaseFile);

        QuantityEmpByDepReport report = model.getReport();
        report.setContent(databaseFile);
        session.save(report);
    }
}
