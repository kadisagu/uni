/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.StaffListRegistry;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniemp.entity.employee.StaffListItem;

/**
 * @author dseleznev
 * Created on: 20.09.2008
 */
public class Model
{
    private IMultiSelectModel _stateModel;
    private List<OrgUnitType> _orgUnitTypesList;
    private ISelectModel _orgUnitList;
    private List<HSelectOption> _employeeTypesList;
    private ISelectModel _postsAutocompleteModel;
    private DynamicListDataSource<StaffListItem> _dataSource;
    
    private IDataSettings _settings;

    //Getters & Setters

    public IMultiSelectModel getStateModel()
    {
        return _stateModel;
    }

    public void setStateModel(IMultiSelectModel stateModel)
    {
        _stateModel = stateModel;
    }

    public List<OrgUnitType> getOrgUnitTypesList()
    {
        return _orgUnitTypesList;
    }

    public void setOrgUnitTypesList(List<OrgUnitType> orgUnitTypesList)
    {
        this._orgUnitTypesList = orgUnitTypesList;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        this._orgUnitList = orgUnitList;
    }

    public List<HSelectOption> getEmployeeTypesList()
    {
        return _employeeTypesList;
    }

    public void setEmployeeTypesList(List<HSelectOption> employeeTypesList)
    {
        this._employeeTypesList = employeeTypesList;
    }

    public ISelectModel getPostsAutocompleteModel()
    {
        return _postsAutocompleteModel;
    }

    public void setPostsAutocompleteModel(ISelectModel postsAutocompleteModel)
    {
        this._postsAutocompleteModel = postsAutocompleteModel;
    }

    public DynamicListDataSource<StaffListItem> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StaffListItem> dataSource)
    {
        this._dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        this._settings = settings;
    }

}