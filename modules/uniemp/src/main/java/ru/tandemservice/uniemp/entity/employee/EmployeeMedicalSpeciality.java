package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniemp.entity.employee.gen.EmployeeMedicalSpecialityGen;

/**
 * Врачебная специальность сотрудника
 */
public class EmployeeMedicalSpeciality extends EmployeeMedicalSpecialityGen
{
    public static String P_CERTIFICATE_AVAILABLE = "certificateAvailable";
    public static String P_PRINTING_DISABLED = "printingDisabled";
    public static String P_CUSTOM_TITLE = "customTitle";

    public boolean isPrintingDisabled()
    {
        return (getExpertCertificate() != null ? false : true);
    }

    /**
     * @return Возвращает <tt><b>true</tt></b>, если поле <tt>_expertCertificateAvailable</tt> не <tt><b>null</tt></b> и в нем лежит <tt><b>true</tt></b>,<p>
     * иначе <tt><b>false</tt></b>
     */
    public boolean getCertificateAvailable()
    {
        return getExpertCertificateAvailable() != null && getExpertCertificateAvailable();
    }

    public String getCustomTitle()
    {
        return getMedicalEducationLevel().getTitle() +
                " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(getAssignDate()) + "); " +
                getMedicalQualificationCategory().getTitle() +
                (getCategoryAssignDate() != null ? " (" + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCategoryAssignDate()) + ")" : "");
    }
}