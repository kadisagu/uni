/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.uniemp.entity.employee.EmployeeTrainingItem;

/**
 * Create by ashaburov
 * Date 26.03.12
 */
public class EmpEmployeeTrainingItemDAO extends CommonDAO implements IEmpEmployeeTrainingItemDAO
{
    @Override
    public void saveOrUpdateItem(EmployeeTrainingItem item)
    {
        saveOrUpdate(item);
    }
}
