package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.gen.StaffListAllocationItemGen;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;

/**
 * Должность штатной расстановки
 */
public class StaffListAllocationItem extends StaffListAllocationItemGen implements ISecLocalEntityOwner
{
    //    public static final String P_STAFF_RATE_STR = "staffRateStr";
    public static final String P_CANT_BE_DELETED = "cantBeDeleted";
    //поле для враппера
    public static final String P_CANT_BE_COPIED = "cantBeCopied";
    public static final String P_INVALID = "invalid";
    public static final String P_ERROR_MESSAGE = "errMessage";
    public static final String P_SAFE_TITLE = "safeTitle";

    public static String P_STAFF_RATE = "staffRate";
    
    public static final String[] POST_TITLE_KEY = new String[] { StaffListAllocationItem.L_STAFF_LIST_ITEM, StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.P_TITLE };
    public static final String[] POST_SIMPLE_TITLE_KEY = new String[] { StaffListAllocationItem.L_STAFF_LIST_ITEM, StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.P_TITLE };
    public static final String[] POST_TYPE_KEY = new String[] { StaffListAllocationItem.L_STAFF_LIST_ITEM, StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_POST, Post.L_EMPLOYEE_TYPE, EmployeeType.P_SHORT_TITLE };
    public static final String[] QUALIFICATION_LEVEL_KEY = new String[] { StaffListAllocationItem.L_STAFF_LIST_ITEM, StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, QualificationLevel.P_SHORT_TITLE };
    public static final String[] PROF_QUALIFICATION_GROUP_KEY = new String[] { StaffListAllocationItem.L_STAFF_LIST_ITEM, StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, ProfQualificationGroup.P_SHORT_TITLE };
    public static final String[] ETKS_KEY = new String[] { StaffListAllocationItem.L_STAFF_LIST_ITEM, StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_ETKS_LEVELS, EtksLevels.P_TITLE };
    public static final String[] BASE_SALARY_KEY = new String[] { StaffListAllocationItem.L_STAFF_LIST_ITEM, StaffListItem.P_SALARY };
    public static final String[] EMPLOYEE_FIO_KEY = new String[] { StaffListAllocationItem.L_EMPLOYEE_POST, EmployeePost.L_EMPLOYEE, Employee.L_PERSON, Person.P_FIO };
    
    public boolean isCantBeDeleted()
    {
        // TODO:
        /*UniempDaoFacade.getUniempDAO().isStaffListAllocationHasStaffListItem(this) && (null != getEmployeePost() || !UniempDaoFacade.getUniempDAO().isStaffListAllocationEmpty(this) ||getStaffListItem().getStaffList().isCantBeEdited())*/
        //if (isReserve())
            return getStaffListItem().getStaffList().getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ARCHIVE) ? true : false;
        /*else
            return getStaffListItem().getStaffList().isCantBeEdited();*/
    }
    
    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return Collections.<IEntity>singletonList(this.getStaffListItem().getStaffList().getOrgUnit());
    }
    
    public String getSafeTitle()
    {
        return null != getEmployeePost() ? (getEmployeePost().getPerson().getFullFio() + " - " + getEmployeePost().getPostStatus().getTitle()) : "  новая ставка";
    }

    public void setStaffRate(double value)
    {
        setStaffRateInteger((int) (value * 10000));
    }

    public double getStaffRate()
    {
        BigDecimal x = new java.math.BigDecimal(getStaffRateInteger() * 0.0001);
        x = x.setScale(3, BigDecimal.ROUND_HALF_UP);

        return x.doubleValue();
    }

    /**
     * Бфзовый оклад
     * @return базовый оклад для должности штатной расстановки
     */
    public Double getBaseSalary()
    {
        return UniempDaoFacade.getStaffListDAO().getAllocItemBaseSalary(this);
    }

    /**
     * Должность.
     * @return должность штатной расстановки
     */
    public PostBoundedWithQGandQL getPost()
    {
        return UniempDaoFacade.getStaffListDAO().getAllocItemPost(this);
    }
}