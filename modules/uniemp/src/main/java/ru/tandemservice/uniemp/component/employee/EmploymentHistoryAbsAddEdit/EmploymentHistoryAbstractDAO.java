/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
public class EmploymentHistoryAbstractDAO<M extends EmploymentHistoryAbstractModel> extends UniempDAO<M> implements EmploymentHistoryAbstractIDAO<M>
{

    @Override
    public void prepare(M model)
    {
        if (null != model.getEmployeeId())
            model.setEmployee(get(Employee.class, model.getEmployeeId()));

        if (null != model.getEmploymentHistoryItem().getId())
            model.setEmploymentHistoryItem((EmploymentHistoryItemBase)get(model.getEmploymentHistoryItem().getId()));
        else
        {
            model.getEmploymentHistoryItem().setEmployee(model.getEmployee());
            model.getEmploymentHistoryItem().setCurrent(false);
        }

        model.setPostTypesList(getCatalogItemList(PostType.class));
    }

    @Override
    public void update(M model)
    {
        getSession().saveOrUpdate(model.getEmploymentHistoryItem());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(M model, ErrorCollector errors)
    {
        if ((model.getEmploymentHistoryItem().getExtractDate() != null || model.getEmploymentHistoryItem().getExtractNumber() != null) &&
                (model.getEmploymentHistoryItem().getExtractDate() == null || model.getEmploymentHistoryItem().getExtractNumber() == null))
            errors.add("Поля Номер приказа и Дата приказа должны быть заполнены, либо оставаться пустыми.", "extractDate", "extractNumber");

        if (null != model.getEmploymentHistoryItem().getStaffRate())
        {
            if (model.getEmploymentHistoryItem().getStaffRate() == 0)
                errors.add("Суммарная ставка не может быть нулевой", "staffRateEdt");

            if (model.getEmploymentHistoryItem().getStaffRate() > 1)
                errors.add("Суммарная ставка не может превышать единицу", "staffRateEdt");
        }

        Long assignDate = model.getEmploymentHistoryItem().getAssignDate().getTime();
        
        if(assignDate > new Date().getTime())
            errors.add("Дата назначения не может превышать текущую", "assignDate");
        
        if (null != model.getEmploymentHistoryItem().getDismissalDate())
        {
            Long dismissalDate = model.getEmploymentHistoryItem().getDismissalDate().getTime();
            if (dismissalDate > new Date().getTime())
                errors.add("Дата увольнения не может превышать текущую", "dismissalDate1", "dismissalDate2");

            if (assignDate > dismissalDate)
                errors.add("Дата назначения не может быть больше даты увольнения", "assignDate", "dismissalDate1", "dismissalDate2");
        }

        String postTypeCode = model.getEmploymentHistoryItem().getPostType().getCode();
        if (UniDefines.POST_TYPE_MAIN_JOB.equals(postTypeCode))
        {
            DQLSelectBuilder exclBuilder = new DQLSelectBuilder().fromEntity(EmploymentHistoryItemFake.class, "e").column(property(EmploymentHistoryItemFake.id().fromAlias("e")))
                    .where(eqValue(property(EmploymentHistoryItemFake.employee().fromAlias("e")), model.getEmployee()));

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmploymentHistoryItemBase.class, "b")
            .where(notIn(property(EmploymentHistoryItemBase.id().fromAlias("b")), exclBuilder.buildQuery()))
            .where(eqValue(property(EmploymentHistoryItemBase.employee().fromAlias("b")), model.getEmployee()))
            .where(eqValue(property(EmploymentHistoryItemBase.postType().code().fromAlias("b")), PostTypeCodes.MAIN_JOB));
            if (null != model.getEmploymentHistoryItem().getId())
                builder.where(ne(property(EmploymentHistoryItemBase.id().fromAlias("b")), model.getEmploymentHistoryItem().getId()));

            Date disDate = model.getEmploymentHistoryItem().getDismissalDate();
            Long dismissalDate = null != disDate ? disDate.getTime() : System.currentTimeMillis();
            for (EmploymentHistoryItemBase item : builder.createStatement(getSession()).<EmploymentHistoryItemBase>list())
            {
                if ((assignDate >= item.getAssignDate().getTime() && (null == item.getDismissalDate() || assignDate <= item.getDismissalDate().getTime()))
                        || (dismissalDate >= item.getAssignDate().getTime() && (null == item.getDismissalDate() || dismissalDate <= item.getDismissalDate().getTime()))
                        || (assignDate < item.getAssignDate().getTime() && dismissalDate > item.getAssignDate().getTime()))
                    errors.add("Указанный период пересекается с периодом, в который данный кадровый ресурс занимал другую основную должность", "assignDate", "dismissalDate1", "dismissalDate2");
            }
        }
    }

}