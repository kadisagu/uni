/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryIntAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.IOrgUnitTypeModel;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit.EmploymentHistoryAbstractModel;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemInner;

/**
 * @author dseleznev
 * Created on: 24.12.2008
 */
@Input(keys = { "employeeId", "rowId" }, bindings = { "employeeId", "employmentHistoryItem.id" })
public class Model extends EmploymentHistoryAbstractModel implements IOrgUnitTypeModel
{
    private EmploymentHistoryItemInner _employmentHistoryItem = new EmploymentHistoryItemInner();

    private List<OrgUnitType> _orgUnitTypeList;
    private ISelectModel _orgUnitListModel;
    private ISelectModel _postListModel;
    private OrgUnit _organization;
    private OrgUnitType _orgUnitType;

    private IMultiSelectModel _serviceLengthTypeModel;
    private List<ServiceLengthType> _serviceLengthTypeList;

    // Getters & Setters

    public IMultiSelectModel getServiceLengthTypeModel()
    {
        return _serviceLengthTypeModel;
    }

    public void setServiceLengthTypeModel(IMultiSelectModel serviceLengthTypeModel)
    {
        _serviceLengthTypeModel = serviceLengthTypeModel;
    }

    public List<ServiceLengthType> getServiceLengthTypeList()
    {
        return _serviceLengthTypeList;
    }

    public void setServiceLengthTypeList(List<ServiceLengthType> serviceLengthTypeList)
    {
        _serviceLengthTypeList = serviceLengthTypeList;
    }

    @Override
    public EmploymentHistoryItemInner getEmploymentHistoryItem()
    {
        return _employmentHistoryItem;
    }

    @Override
    public void setEmploymentHistoryItem(EmploymentHistoryItemBase employmentHistoryItem)
    {
        this._employmentHistoryItem = (EmploymentHistoryItemInner) employmentHistoryItem;
    }

    public List<OrgUnitType> getOrgUnitTypeList()
    {
        return _orgUnitTypeList;
    }

    public void setOrgUnitTypeList(List<OrgUnitType> orgUnitTypeList)
    {
        this._orgUnitTypeList = orgUnitTypeList;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        this._orgUnitListModel = orgUnitListModel;
    }

    public ISelectModel getPostListModel()
    {
        return _postListModel;
    }

    public void setPostListModel(ISelectModel postListModel)
    {
        this._postListModel = postListModel;
    }

    public OrgUnit getOrganization()
    {
        return _organization;
    }

    public void setOrganization(OrgUnit organization)
    {
        this._organization = organization;
    }

    @Override
    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        this._orgUnitType = orgUnitType;
    }

}