package ru.tandemservice.uniemp.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
public class MS_uniemp_2x9x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		if (!tool.columnExists("employeelabourcontract_t", "employmentdate_p")) {
			tool.createColumn("employeelabourcontract_t", new DBColumn("employmentdate_p", DBType.DATE));
		}
    }
}