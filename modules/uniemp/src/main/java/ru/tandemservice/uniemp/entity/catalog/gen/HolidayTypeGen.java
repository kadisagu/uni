package ru.tandemservice.uniemp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Вид отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class HolidayTypeGen extends EntityBase
 implements INaturalIdentifiable<HolidayTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.catalog.HolidayType";
    public static final String ENTITY_NAME = "holidayType";
    public static final int VERSION_HASH = 113662835;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_USER_CODE = "userCode";
    public static final String P_CHILD_CARE = "childCare";
    public static final String P_LONG_TIME = "longTime";
    public static final String P_COMPENSABLE = "compensable";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _userCode;     // Пользовательский код
    private boolean _childCare;     // Уход за ребенком
    private boolean _longTime;     // Длительный
    private boolean _compensable;     // Компенсируемый при увольнении
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Пользовательский код.
     */
    @Length(max=255)
    public String getUserCode()
    {
        return _userCode;
    }

    /**
     * @param userCode Пользовательский код.
     */
    public void setUserCode(String userCode)
    {
        dirty(_userCode, userCode);
        _userCode = userCode;
    }

    /**
     * @return Уход за ребенком. Свойство не может быть null.
     */
    @NotNull
    public boolean isChildCare()
    {
        return _childCare;
    }

    /**
     * @param childCare Уход за ребенком. Свойство не может быть null.
     */
    public void setChildCare(boolean childCare)
    {
        dirty(_childCare, childCare);
        _childCare = childCare;
    }

    /**
     * @return Длительный. Свойство не может быть null.
     */
    @NotNull
    public boolean isLongTime()
    {
        return _longTime;
    }

    /**
     * @param longTime Длительный. Свойство не может быть null.
     */
    public void setLongTime(boolean longTime)
    {
        dirty(_longTime, longTime);
        _longTime = longTime;
    }

    /**
     * @return Компенсируемый при увольнении. Свойство не может быть null.
     */
    @NotNull
    public boolean isCompensable()
    {
        return _compensable;
    }

    /**
     * @param compensable Компенсируемый при увольнении. Свойство не может быть null.
     */
    public void setCompensable(boolean compensable)
    {
        dirty(_compensable, compensable);
        _compensable = compensable;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof HolidayTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((HolidayType)another).getCode());
            }
            setUserCode(((HolidayType)another).getUserCode());
            setChildCare(((HolidayType)another).isChildCare());
            setLongTime(((HolidayType)another).isLongTime());
            setCompensable(((HolidayType)another).isCompensable());
            setTitle(((HolidayType)another).getTitle());
        }
    }

    public INaturalId<HolidayTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<HolidayTypeGen>
    {
        private static final String PROXY_NAME = "HolidayTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof HolidayTypeGen.NaturalId) ) return false;

            HolidayTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends HolidayTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) HolidayType.class;
        }

        public T newInstance()
        {
            return (T) new HolidayType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "userCode":
                    return obj.getUserCode();
                case "childCare":
                    return obj.isChildCare();
                case "longTime":
                    return obj.isLongTime();
                case "compensable":
                    return obj.isCompensable();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "userCode":
                    obj.setUserCode((String) value);
                    return;
                case "childCare":
                    obj.setChildCare((Boolean) value);
                    return;
                case "longTime":
                    obj.setLongTime((Boolean) value);
                    return;
                case "compensable":
                    obj.setCompensable((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "userCode":
                        return true;
                case "childCare":
                        return true;
                case "longTime":
                        return true;
                case "compensable":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "userCode":
                    return true;
                case "childCare":
                    return true;
                case "longTime":
                    return true;
                case "compensable":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "userCode":
                    return String.class;
                case "childCare":
                    return Boolean.class;
                case "longTime":
                    return Boolean.class;
                case "compensable":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<HolidayType> _dslPath = new Path<HolidayType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "HolidayType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Пользовательский код.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#getUserCode()
     */
    public static PropertyPath<String> userCode()
    {
        return _dslPath.userCode();
    }

    /**
     * @return Уход за ребенком. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#isChildCare()
     */
    public static PropertyPath<Boolean> childCare()
    {
        return _dslPath.childCare();
    }

    /**
     * @return Длительный. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#isLongTime()
     */
    public static PropertyPath<Boolean> longTime()
    {
        return _dslPath.longTime();
    }

    /**
     * @return Компенсируемый при увольнении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#isCompensable()
     */
    public static PropertyPath<Boolean> compensable()
    {
        return _dslPath.compensable();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends HolidayType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _userCode;
        private PropertyPath<Boolean> _childCare;
        private PropertyPath<Boolean> _longTime;
        private PropertyPath<Boolean> _compensable;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(HolidayTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Пользовательский код.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#getUserCode()
     */
        public PropertyPath<String> userCode()
        {
            if(_userCode == null )
                _userCode = new PropertyPath<String>(HolidayTypeGen.P_USER_CODE, this);
            return _userCode;
        }

    /**
     * @return Уход за ребенком. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#isChildCare()
     */
        public PropertyPath<Boolean> childCare()
        {
            if(_childCare == null )
                _childCare = new PropertyPath<Boolean>(HolidayTypeGen.P_CHILD_CARE, this);
            return _childCare;
        }

    /**
     * @return Длительный. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#isLongTime()
     */
        public PropertyPath<Boolean> longTime()
        {
            if(_longTime == null )
                _longTime = new PropertyPath<Boolean>(HolidayTypeGen.P_LONG_TIME, this);
            return _longTime;
        }

    /**
     * @return Компенсируемый при увольнении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#isCompensable()
     */
        public PropertyPath<Boolean> compensable()
        {
            if(_compensable == null )
                _compensable = new PropertyPath<Boolean>(HolidayTypeGen.P_COMPENSABLE, this);
            return _compensable;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.HolidayType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(HolidayTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return HolidayType.class;
        }

        public String getEntityName()
        {
            return "holidayType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
