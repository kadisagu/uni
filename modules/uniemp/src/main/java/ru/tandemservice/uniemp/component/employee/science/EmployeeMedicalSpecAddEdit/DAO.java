/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.science.EmployeeMedicalSpecAddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;
import ru.tandemservice.uniemp.entity.catalog.MedicalQualificationCategory;
import ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality;

import java.io.IOException;

/**
 * @author dseleznev
 * Created on: 21.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {

        if (null != model.getEmployeeMedicalSpecialityId())
            model.setEmployeeMedicalSpeciality(get(EmployeeMedicalSpeciality.class, model.getEmployeeMedicalSpecialityId()));
        else
        {
            model.setEmployeeMedicalSpeciality(new EmployeeMedicalSpeciality());
            model.getEmployeeMedicalSpeciality().setEmployee(get(Employee.class, model.getEmployeeId()));
        }

        model.setMedicalEducationLevelListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult<MedicalEducationLevel> findValues(String filter)
            {
                MQBuilder subBuilder = new MQBuilder(EmployeeMedicalSpeciality.ENTITY_CLASS, "ems", new String[] {EmployeeMedicalSpeciality.medicalEducationLevel().id().s()});
                subBuilder.add(MQExpression.eq("ems", EmployeeMedicalSpeciality.employee().s(), model.getEmployeeMedicalSpeciality().getEmployee()));
                if (null != model.getEmployeeMedicalSpeciality() && null != model.getEmployeeMedicalSpeciality().getMedicalEducationLevel())
                    subBuilder.add(MQExpression.notEq("ems", EmployeeMedicalSpeciality.medicalEducationLevel().s(), model.getEmployeeMedicalSpeciality().getMedicalEducationLevel()));

                MQBuilder builder = new MQBuilder(MedicalEducationLevel.ENTITY_CLASS, "mel");
                builder.add(MQExpression.notIn("mel", MedicalEducationLevel.id().s(), subBuilder));
                if(!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like("mel", MedicalEducationLevel.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("mel", MedicalEducationLevel.title().s());
                return new ListResult<>(builder.<MedicalEducationLevel>getResultList(getSession(), 0, 50), builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get((Long) primaryKey);
            }
        });

        model.setMedicalQualificationCategoryListModel(new FullCheckSelectModel(MedicalQualificationCategory.P_SHORT_TITLE)
        {
            @Override
            public ListResult<MedicalQualificationCategory> findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(MedicalQualificationCategory.ENTITY_CLASS, "mqc");
                if(!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like("mqc", MedicalQualificationCategory.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("mqc", MedicalQualificationCategory.code().s());
                return new ListResult<>(builder.<MedicalQualificationCategory>getResultList(getSession(), 0, 50), builder.getResultCount(getSession()));
            }
        });
        if(null == model.getEmployeeMedicalSpeciality().getMedicalQualificationCategory() || UniempDefines.MEDICAL_QUALIFICATION_CATEGORY_WITHOUT_CATEGORY.equals(model.getEmployeeMedicalSpeciality().getMedicalQualificationCategory().getCode()))
            model.setCategoryRelatedFieldsVisible(false);
        else
            model.setCategoryRelatedFieldsVisible(true);

        //видимость кнопки удаления сертификата. Кнопка видна, если идет редактирование врач. спец. и к ней прикреплен сертификат
        if (model.getEmployeeMedicalSpeciality() != null && model.getEmployeeMedicalSpeciality().getCertificateAvailable() &&
                model.getEmployeeMedicalSpeciality().getExpertCertificate() != null)
            model.setDeleteVisible(true);

        //видимость поля добавления файла. Поле видно, если указано, что имеется сертификат и сам сертификат не добавлен
        if (model.getEmployeeMedicalSpeciality() != null && model.getEmployeeMedicalSpeciality().getCertificateAvailable() &&
                model.getEmployeeMedicalSpeciality().getExpertCertificate() == null)
            model.setFileUploadVisible(true);
    }

    @Override
    public void update(Model model)
    {
        if (model.getCertificateFile() != null &&
                model.getEmployeeMedicalSpeciality().getCertificateAvailable())
        {
            try
            {
                byte[] content = IOUtils.toByteArray(model.getCertificateFile().getStream());

                if ((content != null) && (content.length > 0))
                {
                    if (model.getEmployeeMedicalSpeciality().getExpertCertificate() == null)
                        model.getEmployeeMedicalSpeciality().setExpertCertificate(new DatabaseFile());
                    model.getEmployeeMedicalSpeciality().getExpertCertificate().setContent(content);
                    model.getEmployeeMedicalSpeciality().setCertificateFileName(model.getCertificateFile().getFileName());
                    model.getEmployeeMedicalSpeciality().setCertificateFileType(CommonBaseUtil.getContentType(model.getCertificateFile()));
                    getSession().save(model.getEmployeeMedicalSpeciality().getExpertCertificate());
                }
            }
            catch (IOException e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        if(null == model.getEmployeeMedicalSpeciality().getMedicalQualificationCategory() || UniempDefines.MEDICAL_QUALIFICATION_CATEGORY_WITHOUT_CATEGORY.equals(model.getEmployeeMedicalSpeciality().getMedicalQualificationCategory().getCode()))
        {
            model.getEmployeeMedicalSpeciality().setCategoryAcceptionDate(null);
            model.getEmployeeMedicalSpeciality().setCategoryAssignDate(null);
        }

        if (!model.getEmployeeMedicalSpeciality().getCertificateAvailable())
            model.getEmployeeMedicalSpeciality().setExpertCertificate(null);

        getSession().saveOrUpdate(model.getEmployeeMedicalSpeciality());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getCertificateFile() != null &&
                model.getEmployeeMedicalSpeciality().getCertificateAvailable())
        {
            try
            {
                byte[] content = IOUtils.toByteArray(model.getCertificateFile().getStream());
                if ((content != null) && (content.length > 0))
                    if (model.getCertificateFile().getSize() > 512000)
                        errors.add("Размер файла должен быть не более 500 Кб.", "fileUpload");
            }
            catch (IOException e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }
}