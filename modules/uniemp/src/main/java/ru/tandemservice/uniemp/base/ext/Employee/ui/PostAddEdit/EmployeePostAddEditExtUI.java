/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.PostAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnBeforeUpdateEmployeePostExt;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnChangePostRelationEmployeePostExt;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnChangePostStateEmployeePostExt;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
public class EmployeePostAddEditExtUI extends UIAddon implements IOnChangePostRelationEmployeePostExt, IOnChangePostStateEmployeePostExt, IOnBeforeUpdateEmployeePostExt
{
    private EmployeePostAddEditLegacyModel model;

    public EmployeePostAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        prepareLegacyModel();
    }

    // Getters & Setters

    public EmployeePostAddEditLegacyModel getModel()
    {
        return model;
    }

    public Double getStaffRateValue()
    {
        return (Double) getModel().getStaffRateDataSource().getColumnValue();
    }

    public void setStaffRateValue(Double value)
    {
        getModel().getStaffRateDataSource().setColumnValue(value);
    }

    // Listeners

    public void onChangeFreelance()
    {
        final EmployeePostAddEditUI baseModel = getPresenter();

        final ICommonDAO dao = DataAccessServices.dao();

        //если Сотрудник вне штата, то список должностей заполняем в без учета ШР
        //если метка Вне штата снята, то заполняем селект должность относительно ШР сотрудник
        if (baseModel.getEmployeePost().isFreelance())
        {
            baseModel.setPostRelationListModel(new BaseSingleSelectModel()
            {
                private static final String FULL_LIST_FILTER = "getFullList";

                @Override
                public ListResult<OrgUnitTypePostRelation> findValues(String filter)
                {
                    if (baseModel.getEmployeePost().getOrgUnit() == null) return ListResult.getEmpty();
                    boolean fullList = false;
                    if (FULL_LIST_FILTER.equals(filter))
                    {
                        fullList = true;
                        filter = "";
                    }

                    MQBuilder subBuilder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep", new String[]{EmployeePost.postRelation().id().s()});
                    subBuilder.add(MQExpression.eq("ep", EmployeePost.postRelation().multiPost().s(), Boolean.FALSE));
                    subBuilder.add(MQExpression.eq("ep", EmployeePost.orgUnit().s(), baseModel.getEmployeePost().getOrgUnit()));
                    subBuilder.add(MQExpression.eq("ep", EmployeePost.postStatus().active().s(), Boolean.TRUE));
                    if (null != baseModel.getEmployeePost().getId())
                        subBuilder.add(MQExpression.notEq("ep", EmployeePost.id().s(), baseModel.getEmployeePost().getId()));

                    MQBuilder builder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "pr");
                    builder.add(MQExpression.notIn("pr", OrgUnitTypePostRelation.id().s(), subBuilder));
                    builder.add(MQExpression.eq("pr", OrgUnitTypePostRelation.orgUnitType().s(), baseModel.getEmployeePost().getOrgUnit().getOrgUnitType()));
                    if (!StringUtils.isEmpty(filter))
                        builder.add(MQExpression.like("pr", OrgUnitTypePostRelation.postBoundedWithQGandQL().title().s(), CoreStringUtils.escapeLike(filter)));
                    builder.addOrder("pr", OrgUnitTypePostRelation.postBoundedWithQGandQL().title().s());

                    if (fullList)
                        return new ListResult<>(ISharedBaseDao.instance.get().<OrgUnitTypePostRelation>getList(builder));
                    return new ListResult<>(ISharedBaseDao.instance.get().<OrgUnitTypePostRelation>getList(builder, 0, 50), ISharedBaseDao.instance.get().getCount(builder));
                }

                @Override
                public Object getValue(Object primaryKey)
                {
                    OrgUnitTypePostRelation entity = dao.get((Long) primaryKey);
                    if (findValues(FULL_LIST_FILTER).getObjects().contains(entity))
                        return entity;

                    return null;
                }

                @Override
                public String getLabelFor(Object value, int columnIndex)
                {
                    return ((OrgUnitTypePostRelation) value).getPostBoundedWithQGandQL().getFullTitleWithSalary();
                }
            });
        }
        else//если метка Вне штата снята, то заполняем селект должность относительно ШР сотрудник, если оно есть и активно
        {
            baseModel.setPostRelationListModel(new BaseSingleSelectModel()
            {
                @Override
                public ListResult findValues(String filter)
                {
                    if (baseModel.getEmployeePost().getOrgUnit() == null)
                    {
                        return ListResult.getEmpty();
                    }

                    int maxCount = UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnitCount(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation() != null ? baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL() : null, filter, null, null);
                    return new ListResult<>(UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnit(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation() != null ? baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL() : null, filter, null, null, 50), maxCount);
                }

                @Override
                public Object getValue(Object primaryKey)
                {
                    IEntity entity = DataAccessServices.dao().get(OrgUnitTypePostRelation.class, (Long) primaryKey);
                    if (null != entity && (null == baseModel.getEmployeePost().getOrgUnit() || !((OrgUnitTypePostRelation) entity).getOrgUnitType().equals(baseModel.getEmployeePost().getOrgUnit().getOrgUnitType())))
                        return null;

                    if ((null != baseModel.getEmployeePost().getPostStatus() && !baseModel.getEmployeePost().getPostStatus().isActive())
                            || UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnit(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation() != null ? baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL() : null, "", null, null, null).contains(entity))
                    {
                        return entity;
                    }

                    baseModel.getEmployeePost().setPostRelation(null);
                    return null;
                }

                @Override
                public String getLabelFor(Object value, int columnIndex)
                {
                    return ((OrgUnitTypePostRelation) value).getPostBoundedWithQGandQL().getFullTitleWithSalary();
                }
            });
        }

        onChangePostRelationExt();
    }

    public void onClickAddStaffRate()
    {
        final EmployeePostAddEditUI baseModel = getPresenter();

        EmployeePostStaffRateItem item = new EmployeePostStaffRateItem();

        short discriminator = EntityRuntime.getMeta(EmployeePostStaffRateItem.class).getEntityCode();
        long id = EntityIDGenerator.generateNewId(discriminator);

        item.setId(id);
        item.setEmployeePost(baseModel.getEmployeePost());

        //добавляем новую не заполненную ставку в список ставок
        model.getStaffRateItemList().add(item);

        prepareStaffRateDataSource();

        prepareEmpHRColumn();

//        if (model.isThereAnyActiveStaffList())
//            model.getEmployeeHRModelMap().put(item.getId(), new IndividualizedBaseMultiSelectModel(item.getId(), model.getStaffRateDataSource(), baseModel));
    }

    @SuppressWarnings("unchecked")
    public void onChangeStaffRate()
    {
        EmployeePostAddEditUI baseModel = getPresenter();

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double staffRateSumm = 0d;
        for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                if (value instanceof Long)
                    val = ((Long) value).doubleValue();
                else
                    val = (Double) value;
            }

            staffRateSumm += val;
        }

        baseModel.setStaffRate(staffRateSumm);

        prepareEmpHRColumn();

        baseModel.onChangePostRelationExt();
    }

    public void onChangeFinSrc()
    {
        //final EmployeePostAddEditUI baseModel = getPresenter();

//        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        //final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

//        Long itemId = getListenerParameterAsLong() != null ?
//                getListenerParameterAsLong() :
//                model.getStaffRateDataSource().getCurrentEntity().getId();

//        if (finSrcMap.get(itemId) != null)
//        {
//            ((BlockColumn) model.getStaffRateDataSource().getColumn("financingSourceItem")).setValueMap(new HashMap());
//            if (baseModel.getEmployeePost().getOrgUnit() != null && baseModel.getEmployeePost().getPostRelation() != null)
//                model.getFinancingSourceItemModelMap().put(itemId, new FinSrcItmSingleSelectModel(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), finSrcMap.get(itemId), model.isThereAnyActiveStaffList()));
//        }

        prepareEmpHRColumn();
    }

    public void onClickDeleteItem()
    {
        EmployeePostStaffRateItem item = getListenerParameter();

        model.getStaffRateItemList().remove(item);

//        model.getFinancingSourceItemModelMap().remove(item.getId());

        prepareStaffRateDataSource();

        if (!model.isThereAnyActiveStaffList())
            prepareEmpHRColumn();
    }

    // Custom ExtPoint Listeners

    @SuppressWarnings("unchecked")
    public void onChangePostRelationExt()
    {
        EmployeePostAddEditUI baseModel = getPresenter();

        //определяем показывать ли колонку Кадровая расстановка в сечлисте и нужно ли перерисовывать сечлист
        if (baseModel.getEmployeePost().isFreelance())
        {
            if (model.isThereAnyActiveStaffList())
            {
                model.setThereAnyActiveStaffList(false);
                //model.setStaffRateDataSource(null);
                prepareStaffRateDataSource(getPresenterConfig().getBusinessComponent());
            }
        }
        else
        {
            if (UniempDaoFacade.getStaffListDAO().getActiveStaffList(baseModel.getEmployeePost().getOrgUnit()) != null)
            {
                if (!model.isThereAnyActiveStaffList())
                {
                    model.setThereAnyActiveStaffList(true);
                    //model.setStaffRateDataSource(null);
                    prepareStaffRateDataSource(getPresenterConfig().getBusinessComponent());
                    //prepareEmployeeHR();
                }
            } else if (model.isThereAnyActiveStaffList())
            {
                model.setThereAnyActiveStaffList(false);
                //model.setStaffRateDataSource(null);
                prepareStaffRateDataSource(getPresenterConfig().getBusinessComponent());
            }
        }

        //определяем какие источники финансирования показывать
        if (baseModel.getEmployeePost().getOrgUnit() != null && baseModel.getEmployeePost().getPostRelation() != null)
            if (UniempDaoFacade.getStaffListDAO().getActiveStaffList(baseModel.getEmployeePost().getOrgUnit()) != null && !baseModel.getEmployeePost().isFreelance())
                model.setFinancingSourceModel(UniempDaoFacade.getStaffListDAO().getFinancingSourcesStaffListItems(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()));
            else
                model.setFinancingSourceModel(CatalogManager.instance().dao().getCatalogItemList(FinancingSource.class));

        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());
        final IValueMapHolder finSrcItemHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        final Map<Long, FinancingSourceItem> finSrcItemMap = (null == finSrcItemHolder ? Collections.emptyMap() : finSrcItemHolder.getValueMap());
        final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
        final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? new HashMap<>() : empHRHolder.getValueMap());

        for (IEntity entity : model.getStaffRateItemList())
        {
            finSrcMap.put(entity.getId(), null);
            finSrcItemMap.put(entity.getId(), null);
            empHRMap.put(entity.getId(), null);
        }
    }

    public void onChangePostStateExt()
    {
        prepareEmpHRColumn();
    }

//    public void onValidateExt()
//    {
//        EmployeePostAddEditUI baseModel = getPresenter();
//
//        ErrorCollector errors = ContextLocal.getErrorCollector();
//        if (model.getStaffRateItemList().isEmpty())
//            errors.add("Необходимо указать ставку сотруднику.");
//
//        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
//        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());
//        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
//        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());
//        final IValueMapHolder finSrcItemHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
//        final Map<Long, FinancingSourceItem> finSrcItemMap = (null == finSrcItemHolder ? Collections.emptyMap() : finSrcItemHolder.getValueMap());
//        final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
//        final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());
//        //если была доступна колонка Кадровой расстановки, достаем из враперов объекты Штатной расстановки
//        final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<Long, List<StaffListAllocationItem>>();
//        //Мапа, указывающая на то выбрана ли строчка <Новая ставка> в мультиселекте
//        Map<Long, Boolean> hasNewStaffRateMap = new HashMap<Long, Boolean>();
//        if (model.isThereAnyActiveStaffList())
//        {
//            for (IEntity entity : model.getStaffRateItemList())
//            {
//                List<StaffListAllocationItem> allocationItemList = new LinkedList<StaffListAllocationItem>();
//                for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
//                {
//                    if (wrapper.getId() != 0l)
//                    {
//                        StaffListAllocationItem item = UniBaseDao.instance2.get().get(StaffListAllocationItem.class, wrapper.getId());
//                        allocationItemList.add(item);
//                    }
//
//                }
//
//                allocItemsMap.put(entity.getId(), allocationItemList);
//
//                hasNewStaffRateMap.put(entity.getId(), empHRMap.get(entity.getId()).contains(new IdentifiableWrapper(0l, "")));
//            }
//        }
//
//        //если редактируем должность, то заполним лист его ставок, что бы далее проверить выбранна ли она в мультиселекте
//        if (!baseModel.isAddForm())
//            for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
//                model.getSelfAllocItemList().addAll(UniempDaoFacade.getStaffListDAO().getStaffListAllocationItemList(
//                        UniempDaoFacade.getStaffListDAO().getActiveStaffList(baseModel.getEmployeePost().getOrgUnit()),
//                        baseModel.getEmployeePost(),
//                        baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(),
//                        finSrcMap.get(item.getId()),
//                        finSrcItemMap.get(item.getId())));
//
//        for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
//        {
//            if (staffRateMap.get(item.getId()) == null)
//                errors.add("Поле «Ставка» обязательно для заполнения.", "staffRate_id_" + item.getId());
//
//            if (staffRateMap.get(item.getId()) != null)
//            {
//                Double val = 0.0d;
//                if (null != staffRateMap.get(item.getId()))
//                {
//                    Object value = staffRateMap.get(item.getId());
//                    if (value instanceof Long)
//                        val = ((Long) value).doubleValue();
//                    else
//                        val = (Double) value;
//                }
//                if (val <= 0)
//                    errors.add("Поле «Ставка» не может быть меньше или равно нуля.", "staffRate_id_" + item.getId());
//            }
//
//            if (finSrcMap.get(item.getId()) == null)
//                errors.add("Поле «Источник финансирования» обязательно для заполнения.", "finSrc_id_" + item.getId());
//
//            if (model.isThereAnyActiveStaffList() && empHRMap.get(item.getId()).size() == 0)
//                errors.add("Поле «Кадровая расстановка» обязательно для заполнения.", "employeeHR_Id_" + item.getId());
//        }
//
//        if (!errors.hasErrors())
//        {
//            prepareStaffRateList();
//
//            for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
//                for (EmployeePostStaffRateItem item2nd : model.getStaffRateItemList())
//                    if (!item.equals(item2nd))
//                        if (item.getFinancingSource().equals(item2nd.getFinancingSource()))
//                            if ((item.getFinancingSourceItem() == null && item2nd.getFinancingSourceItem() == null) ||
//                                    ((item.getFinancingSourceItem() != null && item2nd.getFinancingSourceItem() != null) && item.getFinancingSourceItem().equals(item2nd.getFinancingSourceItem())))
//                                errors.add("Набор полей «Источник финансирования», «Источник финансирования (детально)» должен быть уникальным в рамках сотрудника.",
//                                        "finSrc_id_" + item.getId(), "finSrcItm_id_" + item.getId());
//        }
//
//        if (!errors.hasErrors())
//            if (model.getStaffRateItemList().size() != 0)
//                UniempDaoFacade.getUniempDAO().validateStaffRates(errors, baseModel.getEmployee(), baseModel.getEmployeePost(), model.getStaffRateItemList(), baseModel.isAddForm(), true);
//
//        if (model.isThereAnyActiveStaffList() && !errors.hasErrors())
//            for (EmployeePostStaffRateItem staffRateItem : model.getStaffRateItemList())
//            {
//                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());
//                Double diff = 0.0d;
//                if (staffListItem != null)
//                    diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
//
//                Double sumStaffRateAllocItems = 0.0d;
//                for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
//                {
//                    sumStaffRateAllocItems += allocItem.getStaffRate();
//                    //если это ставка выбранного сотрудника, то прибавляем ее к свободным ставкам, что бы она не учитывалась в проверке
//                    if (model.getSelfAllocItemList().contains(allocItem) && allocItem.getEmployeePost().getPostStatus().isActive())
//                        diff += allocItem.getStaffRate();
//                }
//
//                BigDecimal x = new java.math.BigDecimal(diff);
//                x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
//                diff = x.doubleValue();
//
//                x = new java.math.BigDecimal(sumStaffRateAllocItems);
//                x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
//                sumStaffRateAllocItems = x.doubleValue();
//
//                if ((staffRateItem.getStaffRate() > sumStaffRateAllocItems && !hasNewStaffRateMap.get(staffRateItem.getId())) ||
//                        (staffRateItem.getStaffRate() > diff && hasNewStaffRateMap.get(staffRateItem.getId())))
//                    errors.add("Размер ставки " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() +
//                            " превышает суммарную ставку, выбранную в поле «Кадровая расстановка».",
//                            "staffRate_id_" + staffRateItem.getId());
//
//                if (staffRateItem.getStaffRate() <= sumStaffRateAllocItems && hasNewStaffRateMap.get(staffRateItem.getId()))
//                    errors.add("Необходимо точнее указать занимаемые ставки в поле «Кадровая расстановка» для "
//                            + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() + ".",
//                            "employeeHR_Id_" + staffRateItem.getId());
//            }
//    }

    public void onBeforeUpdateExt()
    {
        EmployeePostAddEditUI baseModel = getPresenter();

        if (!baseModel.isAddForm())
        {
            //если редактируем должность, то перед тем как создать новые релейшены(ставки) удаляем старые
            //необходимо для того что бы в момент сохранения EmployeePost(в методе updateExt соответствующего расширеня) не возникло ошибки уникальности объекта EmployeePostStaffRateItem
            for (EmployeePostStaffRateItem item : model.getOldStaffRateItemList())
                DataAccessServices.dao().delete(item);
        }
    }

//    @Override
//    public void onUpdateExt()
//    {
//        EmployeePostAddEditUI baseModel = getPresenter();
//
//        /*Сохраняем ссылки на сотрудника в штатной расстановке(назначаем на ставки ШР)*/
//
//        //обновляем StaffRateList(переносим значения из valueMap'ов колонок сечлиста в объекты списка _staffRateList),
//        //что бы работать сразу с объектами списка
//        prepareStaffRateList();
//
//        //если была доступна колонка Кадровой расстановки
//        if (model.isThereAnyActiveStaffList())
//        {
//            //достаем элементы штатной расстановки, которые выбраны в мультиселектах
//            final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
//            final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());
//            //Мапа, id энтини сечлиста на выбранные в мультиселекте объекты штатной расстановки
//            final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<Long, List<StaffListAllocationItem>>();
//
//            //достаем из враперов объекты Штатной расстановки
//            for (IEntity entity : model.getStaffRateItemList())
//            {
//                List<StaffListAllocationItem> allocationItemList = new LinkedList<StaffListAllocationItem>();
//                for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
//                {
//                    if (wrapper.getId() != 0l)
//                    {
//                        StaffListAllocationItem item = UniBaseDao.instance2.get().get(StaffListAllocationItem.class, wrapper.getId());
//                        allocationItemList.add(item);
//                    }
//                }
//
//                allocItemsMap.put(entity.getId(), allocationItemList);
//            }
//
//            //если редактируем должность, то перед тем как проставить новые ссылки на этого сотрудника(у объектов штатной расстановки)
//            //удаляем старые ставки этого сотрудника из активного ШР
//            if (!baseModel.isAddForm())
//            {
//                List<StaffListAllocationItem> choseAllocItem = new LinkedList<StaffListAllocationItem>();
//                for (EmployeePostStaffRateItem staffRateItem : model.getStaffRateItemList())
//                    for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
//                        choseAllocItem.add(allocItem);
//
//                for (StaffListAllocationItem allocationItem : UniempDaoFacade.getUniempDAO().getEmployeePostAllocationItemList(baseModel.getEmployeePost()))
//                {
//                    if (!choseAllocItem.contains(allocationItem))
//                    {
//                        allocationItem.setEmployeePost(null);
//                        DataAccessServices.dao().delete(allocationItem);
//                    }
//                }
//            }
//
//            for (EmployeePostStaffRateItem staffRateItem : model.getStaffRateItemList())
//            {
//                //достаем должность ШР
//                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());
//
//                //создаем новую ставку ШР
//                StaffListAllocationItem allocationItem = new StaffListAllocationItem();
//                //удаляем выбранные ставки с неакт. сотрудниками, если они выбранны
//                for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
//                {
//                    //если выбранная ставка это не ставка редактируемого сотрудника, то удаляем ее
//                    if (!model.getSelfAllocItemList().contains(allocItem))
//                        DataAccessServices.dao().delete(allocItem);
//                    else
//                        //есди это ставка редактируемого сотрудника, то обновляем ее
//                        allocationItem = allocItem;
//                }
//                //заполняем поля ставки
//                allocationItem.setStaffRate(staffRateItem.getStaffRate());
//                allocationItem.setFinancingSource(staffRateItem.getFinancingSource());
//                allocationItem.setFinancingSourceItem(staffRateItem.getFinancingSourceItem());
//                allocationItem.setStaffListItem(staffListItem);
//                allocationItem.setEmployeePost(baseModel.getEmployeePost());
//                allocationItem.setRaisingCoefficient(baseModel.getEmployeePost().getRaisingCoefficient());
//                if (allocationItem.getRaisingCoefficient() != null)
//                    allocationItem.setMonthBaseSalaryFund(allocationItem.getRaisingCoefficient().getRecommendedSalary() * allocationItem.getStaffRate());
//                else if (allocationItem.getStaffListItem() != null)
//                    allocationItem.setMonthBaseSalaryFund(allocationItem.getStaffListItem().getSalary() * allocationItem.getStaffRate());
//                DataAccessServices.dao().saveOrUpdate(allocationItem);
//
//                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());
//            }
//        }
//
//        /*Сохраняем ставки сотрудника(релейшены)*/
////        //если редактируем должность, то перед тем как создать новые релейшены(ставки) удаляем старые
////        if (baseModel.isEditForm())
////        {
//////            List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(baseModel.getEmployeePost());
////            for (EmployeePostStaffRateItem item : model.getOldStaffRateItemList())
////                delete(item);
////
//////            getSession().flush();
////        }
//
//        //создаем указанные ставки(релейшены)
//        for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
//        {
//            if (item.getEmployeePost() != null && item.getFinancingSource() != null)
//            {
//                EmployeePostStaffRateItem newItem = new EmployeePostStaffRateItem();
//                newItem.update(item);
//                newItem.setEmployeePost(((EmployeePostAddEditUI) getPresenter()).getEmployeePost());
//
//                DataAccessServices.dao().save(newItem);
//            }
//        }
//    }

    // Private

    public void prepareLegacyModel()
    {
        // controller
        model = new EmployeePostAddEditLegacyModel();
        final EmployeePostAddEditUI baseModel = getPresenter();
        baseModel.setOrgUnitUpdates(baseModel.getOrgUnitUpdates() + ", allocItemBlock");
        baseModel.setPostUpdates((baseModel.getPostUpdates().length() > 0 ? baseModel.getPostUpdates() + ", " : "") + "allocItemBlock");
        IBusinessComponent component = getPresenterConfig().getBusinessComponent();
        prepareStaffRateDataSource(component);

        // dao prepare
        baseModel.setStaffRate(0d);
        if (baseModel.getEmployeePost().getId() != null)
        {
//            if (baseModel.getEmployeePost().getOrgUnit() != null && baseModel.getEmployeePost().getPostRelation() != null)
//                if (UniempDaoFacade.getStaffListDAO().getActiveStaffList(baseModel.getEmployeePost().getOrgUnit()) != null && !baseModel.getEmployeePost().isFreelance())
//                    model.setFinancingSourceModel(UniempDaoFacade.getStaffListDAO().getFinancingSourcesStaffListItems(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()));
//                else
            model.setFinancingSourceModel(CatalogManager.instance().dao().getCatalogItemList(FinancingSource.class));

            //поднимаем ставки Сотрудника
            model.setStaffRateItemList(UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(baseModel.getEmployeePost()));

            List<EmployeePostStaffRateItem> staffRateItems = model.getStaffRateItemList();
            Double staffRateSumm = 0d;
            for (EmployeePostStaffRateItem item : staffRateItems)
            {
                staffRateSumm += item.getStaffRate();
                model.getOldStaffRateList().add(item.getStaffRate());
            }

            baseModel.setStaffRate(staffRateSumm);
        }

        if (!baseModel.getEmployeePost().isFreelance())
        {
            baseModel.setPostRelationListModel(new BaseSingleSelectModel()
            {
                @Override
                public ListResult findValues(String filter)
                {
                    if (baseModel.getEmployeePost().getOrgUnit() == null)
                    {
                        return ListResult.getEmpty();
                    }

                    int maxCount = UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnitCount(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation() != null ? baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL() : null, filter, null, null);
                    return new ListResult<>(UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnit(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation() != null ? baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL() : null, filter, null, null, 50), maxCount);
                }

                @Override
                public Object getValue(Object primaryKey)
                {
                    IEntity entity = DataAccessServices.dao().get(OrgUnitTypePostRelation.class, (Long) primaryKey);
                    if (null != entity && (null == baseModel.getEmployeePost().getOrgUnit() || !((OrgUnitTypePostRelation) entity).getOrgUnitType().equals(baseModel.getEmployeePost().getOrgUnit().getOrgUnitType())))
                        return null;

                    if ((null != baseModel.getEmployeePost().getPostStatus() && !baseModel.getEmployeePost().getPostStatus().isActive())
                            || UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnit(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation() != null ? baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL() : null, "", null, null, null).contains(entity))
                    {
                        return entity;
                    }

                    baseModel.getEmployeePost().setPostRelation(null);
                    return null;
                }

                @Override
                public String getLabelFor(Object value, int columnIndex)
                {
                    return ((OrgUnitTypePostRelation) value).getPostBoundedWithQGandQL().getFullTitleWithSalary();
                }
            });
        }

        model.setFinancingSourceItemModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                for (IEntity item : findValues("").getObjects())
                    if (item.getId().equals(primaryKey))
                        return item;

                return null;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<FinancingSourceItem> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                FinancingSource financingSource = finSrcMap.get(id);
                if (model.isThereAnyActiveStaffList())
                {
                    PostBoundedWithQGandQL post = baseModel.getEmployeePost().getPostRelation() != null ? baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL() : null;
                    return new ListResult<>(UniempDaoFacade.getStaffListDAO().getFinancingSourcesItemStaffListItems(baseModel.getEmployeePost().getOrgUnit(), post, financingSource, filter));
                }
                else
                {
                    if(null == financingSource) return ListResult.getEmpty();
                    else return new ListResult<>(UniempDaoFacade.getUniempDAO().getFinSrcItm(financingSource.getId(), filter));
                }
            }
        });

        model.setEmployeeHRModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> resultList = new ArrayList<>();

                for (IdentifiableWrapper wrapper : findValues("").getObjects())
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);

                return resultList;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
                final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                String NEW_STAFF_RATE_STR = "<Новая ставка> - ";

                if (finSrcMap.get(id) == null || baseModel.getEmployeePost().getOrgUnit() == null || baseModel.getEmployeePost().getPostRelation() == null)
                    return ListResult.getEmpty();

                List<StaffListAllocationItem> staffListAllocList = UniempDaoFacade.getStaffListDAO().getFreeStaffListAllocItems(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation(), finSrcMap.get(id), finSrcItmMap.get(id));

                Double summStaffRate = 0.0d;
                for (StaffListAllocationItem item : staffListAllocList)
                    summStaffRate += item.getStaffRate();

                Set<IdentifiableWrapper> _resultList = new LinkedHashSet<>();
                if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), finSrcMap.get(id), finSrcItmMap.get(id)))
                {
                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), finSrcMap.get(id), finSrcItmMap.get(id));
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate() - summStaffRate;
                    if (diff > 0 && NEW_STAFF_RATE_STR.contains(filter))
                        _resultList.add(new IdentifiableWrapper(0L, NEW_STAFF_RATE_STR + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                if (!baseModel.isAddForm())
                {
                    for (StaffListAllocationItem allocationItem : UniempDaoFacade.getStaffListDAO().getStaffListAllocationItemList(UniempDaoFacade.getStaffListDAO().getActiveStaffList(baseModel.getEmployeePost().getOrgUnit()), baseModel.getEmployeePost(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), finSrcMap.get(id), finSrcItmMap.get(id)))
                        _resultList.add(new IdentifiableWrapper(allocationItem.getId(), allocationItem.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allocationItem.getStaffRate()) + " - " + allocationItem.getEmployeePost().getPostType().getShortTitle() + " " + allocationItem.getEmployeePost().getPostStatus().getShortTitle()));
                }

                for (StaffListAllocationItem item : staffListAllocList)
                {
                    if (item.getEmployeePost().getPerson().getFullFio().contains(filter))
                        _resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                return new ListResult<>(_resultList);
            }
        });

        if (!baseModel.isAddForm())
        {
            //заполняем поля существующими ставками
            prepareColumns(model.getStaffRateItemList());
            model.getOldStaffRateItemList().addAll(model.getStaffRateItemList());
        }
    }

    private void prepareStaffRateDataSource(IBusinessComponent component)
    {
        // I don't understand it
        //final Model model = getModel(component);
        //component.getModel(IUniComponents.EMPLOYEE_POST_ADD_EDIT);
        //if (!model.isNeedUpdateDataSource() && model.getStaffRateDataSource() != null)
        //    return;
        //
        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> prepareStaffRateDataSource(), 5);

        dataSource.addColumn(new BlockColumn("staffRate", "Ставка"));
        dataSource.addColumn(new BlockColumn("financingSource", "Источник финансирования"));
        dataSource.addColumn(new BlockColumn("financingSourceItem", "Источник финансирования (детально)"));
        if (model.isThereAnyActiveStaffList())
            dataSource.addColumn(new BlockColumn("employeeHR", "Кадровая расстановка"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeePostAddEditExtUI:onClickDeleteItem");
        actionColumn.setDisableSecondSubmit(false);
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setStaffRateDataSource(dataSource);
    }

    private void prepareStaffRateDataSource()
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getStaffRateItemList());
    }

    @SuppressWarnings("unchecked")
    public void prepareEmpHRColumn()
    {
        final EmployeePostAddEditUI baseModel = getPresenter();

        if (baseModel.getEmployeePost().isFreelance() || baseModel.getEmployeePost().getPostStatus() == null || !baseModel.getEmployeePost().getPostStatus().isActive())
        {
            prepareStaffRateList();
            model.setThereAnyActiveStaffList(false);
            prepareStaffRateDataSource(getPresenterConfig().getBusinessComponent());
            prepareColumns(model.getStaffRateItemList());
            return;
        }

        if (baseModel.isAddForm() || model.isThereAnyActiveStaffList())
            return;

        StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(baseModel.getEmployeePost().getOrgUnit());
        if (baseModel.getEmployeePost().getPostRelation() != null &&
                activeStaffList != null)
        {
            prepareStaffRateList();
            model.setThereAnyActiveStaffList(true);
            prepareStaffRateDataSource(getPresenterConfig().getBusinessComponent());
            //prepareEmployeeHR();

            prepareColumns(model.getStaffRateItemList());
            Map<Long, List<IdentifiableWrapper>> idWrapperMap = new HashMap<>();
            for (EmployeePostStaffRateItem rateItem : model.getStaffRateItemList())
            {
                List<IdentifiableWrapper> wrapperList = new ArrayList<>();
                List<StaffListAllocationItem> allocationItemList = UniempDaoFacade.getStaffListDAO().getStaffListAllocationItemList(activeStaffList, baseModel.getEmployeePost(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), rateItem.getFinancingSource(), rateItem.getFinancingSourceItem());

                for (StaffListAllocationItem allocationItem : allocationItemList)
                    wrapperList.add(new IdentifiableWrapper(allocationItem.getId(), allocationItem.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allocationItem.getStaffRate()) + " - " + allocationItem.getEmployeePost().getPostType().getShortTitle() + " " + allocationItem.getEmployeePost().getPostStatus().getShortTitle()));

                idWrapperMap.put(rateItem.getId(), wrapperList);
            }
            if (idWrapperMap.size() > 0)
                ((BlockColumn) model.getStaffRateDataSource().getColumn(3)).setValueMap(idWrapperMap);

        }
    }

    @SuppressWarnings("unchecked")
    public void prepareStaffRateList()
    {
        //достаем введенные знаечения для Ставок и обновляем их в списке
        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

        final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

        for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                val = ((Number) value).doubleValue();
            }
            item.setStaffRate(val);
            item.setFinancingSource(finSrcMap.get(item.getId()));
            item.setFinancingSourceItem(finSrcItmMap.get(item.getId()));
        }
    }

//    private void prepareEmployeeHR()
//    {
//        final EmployeePostAddEditUI baseModel = getPresenter();
//
//        if (!model.isThereAnyActiveStaffList())
//            return;
//
//        for (IEntity entity : model.getStaffRateItemList())
//        {
//            model.getEmployeeHRModelMap().put(entity.getId(), new IndividualizedBaseMultiSelectModel(entity.getId(), model.getStaffRateDataSource(), baseModel));
//        }
//    }

    @SuppressWarnings("unchecked")
    private void prepareColumns(List<EmployeePostStaffRateItem> staffRateItems)
    {
//        for (EmployeePostStaffRateItem item : staffRateItems)
//            model.getFinancingSourceItemModelMap().put(item.getId(), new FinSrcItmSingleSelectModel(baseModel.getEmployeePost().getOrgUnit(), baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), item.getFinancingSource(), model.isThereAnyActiveStaffList()));

        Map<Long, Double> staffRateMap = new HashMap<>();
        for (EmployeePostStaffRateItem item : staffRateItems)
            staffRateMap.put(item.getId(), item.getStaffRate());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(0)).setValueMap(staffRateMap);

        Map<Long, FinancingSource> finSrcMap = new HashMap<>();
        for (EmployeePostStaffRateItem item : staffRateItems)
            finSrcMap.put(item.getId(), item.getFinancingSource());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(1)).setValueMap(finSrcMap);

        Map<Long, FinancingSourceItem> finSrcItmMap = new HashMap<>();
        for (EmployeePostStaffRateItem item : staffRateItems)
            finSrcItmMap.put(item.getId(), item.getFinancingSourceItem());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(2)).setValueMap(finSrcItmMap);
    }
}
