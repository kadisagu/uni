/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.EmployeeServiceLengthCounter;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.base.ext.Employee.logic.EmploymentCalculator;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceLengthWrapper;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author euroelessar
 * @since 07.04.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        if (null == model.getServiceLengthMap())
        {
            model.setServiceLengthTypeList(getCatalogItemList(ServiceLengthType.class));

            List<EmpHistoryToServiceLengthTypeRelation> relationList = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "r").column("r")
                    .where(eqValue(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().employee().id().fromAlias("r")), model.getEmployeeId()))
                    .order(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().title().fromAlias("r")))
                    .createStatement(getSession()).list();

            Map<EmploymentHistoryItemBase, List<ServiceLengthType>> historyItemRelationMap = new HashMap<>();
            for (EmpHistoryToServiceLengthTypeRelation relation : relationList)
            {
                List<ServiceLengthType> list = historyItemRelationMap.get(relation.getEmploymentHistoryItemBase());
                if (list == null)
                    historyItemRelationMap.put(relation.getEmploymentHistoryItemBase(), list = new ArrayList<>());
                list.add(relation.getServiceLengthType());
            }

            List<EmploymentHistoryItemBase> historyItemList = new ArrayList<>();
            for (EmpHistoryToServiceLengthTypeRelation relation : relationList)
                historyItemList.add(relation.getEmploymentHistoryItemBase());

            List<EmployeeServiceLengthWrapper> serviceLengthWrapperList = UniempUtil.getEmployeeServiceLengthWrapperList(historyItemList, historyItemRelationMap);

            Map<ServiceLengthType, List<EmployeeServiceLengthWrapper>> map = new HashMap<>();
            for (EmployeeServiceLengthWrapper serviceLengthWrapper : serviceLengthWrapperList)
            {
                if (null == model.getServiceLengthType())
                    model.setServiceLengthType(serviceLengthWrapper.getType());

                List<EmployeeServiceLengthWrapper> list = map.get(serviceLengthWrapper.getType());
                if (null == list)
                    map.put(serviceLengthWrapper.getType(), list = new ArrayList<>());
                list.add(serviceLengthWrapper);
            }

            if (null == model.getServiceLengthType())
                model.setServiceLengthType(model.getServiceLengthTypeList().get(0));
            model.setServiceLengthMap(map);
        }
        Date date = model.getDate();
        if (null == date)
            date = new Date();
        if (null == model.getServiceLengthType())
            model.setText("");
        else
        {
            List<EmployeeServiceLengthWrapper> list = model.getServiceLengthMap().get(model.getServiceLengthType());
            int totalDays = 0;
            int years = 0;
            int months = 0;
            int days = 0;
            if (null != list)
            {
                EmploymentCalculator calc = new EmploymentCalculator();
                calc.setCurrentDate(date);
                List<CoreCollectionUtils.Pair<Date, Date>> periods = new ArrayList<>();
                for (EmployeeServiceLengthWrapper serviceLengthWrapper : list)
                {
                    Date beginDate = serviceLengthWrapper.getBeginDate();
                    Date endDate = null == serviceLengthWrapper.getEndDate() ? date : serviceLengthWrapper.getEndDate();
                    if (beginDate.compareTo(date) > 0)
                        continue;
                    if (endDate.compareTo(date) > 0)
                        endDate = date;
                    periods.add(new CoreCollectionUtils.Pair<>(beginDate, endDate));
                }
                calc.calculateFullEmploymentPeriod(periods);
                totalDays = calc.getFullDaysAmount();
                years = calc.getYearsAmount();
                months = calc.getMonthsAmount();
                days = calc.getDaysAmount();
            }
            StringBuilder builder = new StringBuilder();
            builder.append("Стаж: ");
            builder.append(CommonBaseDateUtil.getDaysCountWithName(totalDays));
            if (totalDays != 0)
            {
                builder.append(" (");
                builder.append(CommonBaseDateUtil.getDatePeriodWithNames(years, months, days));
                builder.append(")");
            }
            model.setText(builder.append(".").toString());
        }
    }
}