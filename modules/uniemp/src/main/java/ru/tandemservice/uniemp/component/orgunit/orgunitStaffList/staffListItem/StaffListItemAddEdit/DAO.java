/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListItemAddEdit;

import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        if (null != model.getStaffListId())
        {
            model.setStaffList(get(StaffList.class, model.getStaffListId()));
            model.getStaffListItem().setStaffList(model.getStaffList());
            model.setOrgUnitPostsList(getOrgUnitPostRelations(model.getStaffList(), model.getStaffListItemId()));
        }
        if (null != model.getStaffListItemId())
        {
            model.setStaffListItem(get(StaffListItem.class, model.getStaffListItemId()));
            model.setStaffRate(model.getStaffListItem().getStaffRate());
        }

        model.setFinancingSourceItemModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                FinancingSourceItem item = get((Long) primaryKey);

                if (findValues("").getObjects().contains(item))
                    return item;

                return null;
            }

            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", FinancingSourceItem.L_FINANCING_SOURCE, model.getStaffListItem().getFinancingSource()));
                builder.add(MQExpression.like("b", FinancingSourceItem.P_TITLE, CoreStringUtils.escapeLike(filter)));

                return new ListResult<>(builder.<FinancingSourceItem>getResultList(getSession()));
            }
        });

        model.setFinancingSourceModel(getCatalogItemList(FinancingSource.class));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        boolean isNewPost = null == model.getStaffListItem().getId();
        if(isNewPost && null != model.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getSalary())
            model.getStaffListItem().setSalary(model.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getSalary());
        getSession().saveOrUpdate(model.getStaffListItem());
        
        if (isNewPost)
        {
            List<Payment> requiredPayments = getSession().createCriteria(Payment.class).add(Restrictions.eq(Payment.P_ACTIVE, Boolean.TRUE)).add(Restrictions.eq(Payment.P_REQUIRED, Boolean.TRUE)).list();
            for (Payment payment : requiredPayments)
            {
                StaffListPostPayment paymentItem = new StaffListPostPayment();
                paymentItem.setStaffListItem(model.getStaffListItem());
                paymentItem.setAmount(null != payment.getValue() ? payment.getValue() : 0d);
                paymentItem.setFinancingSource(payment.getFinancingSource() != null ? payment.getFinancingSource() : model.getStaffListItem().getFinancingSource());
                paymentItem.setPayment(payment);
                save(paymentItem);
            }
        }
    }
    
    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        IDataSettings settings = DataSettingsFacade.getSettings("general", UniempDefines.STAFF_RATE_STEP_SETTINGS_PREFIX);
        Double minStaffRate = null != settings.get(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME) ? (Double)settings.get(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME) : 0d;

        if (!minStaffRate.equals(0d))
        {
            Double reminder = Math.round((model.getStaffListItem().getStaffRate() % minStaffRate) * 100) / 100d;
            if (reminder != 0 && !minStaffRate.equals(reminder))
                errors.add("Количество штатных единиц должно быть кратно " + minStaffRate, "staffRate");
        }

        //поднимаем все Должности штатного расписания что бы проверить на уникальность полей Должность, Ист.фин. и Ист.фин.(дет.) в рамках этого ШР
        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", StaffListItem.L_STAFF_LIST, model.getStaffList()));
        builder.add(MQExpression.eq("b", StaffListItem.L_ORG_UNIT_POST_RELATION, model.getStaffListItem().getOrgUnitPostRelation()));
        builder.add(MQExpression.notEq("b", StaffListItem.P_ID, model.getStaffListItem().getId()));
        List<StaffListItem> staffListItems = builder.getResultList(getSession());

        StaffListItem newItem = model.getStaffListItem();
        for (StaffListItem item : staffListItems)
        {
            if (newItem.getOrgUnitPostRelation().equals(item.getOrgUnitPostRelation()))
                if (newItem.getFinancingSource().equals(item.getFinancingSource()))
                    if ((newItem.getFinancingSourceItem() == null && item.getFinancingSourceItem() == null) || (newItem.getFinancingSourceItem() != null && newItem.getFinancingSourceItem().equals(item.getFinancingSourceItem())))
                        errors.add("Должность " + newItem.getOrgUnitPostRelation().getTitle() + " c указанными источниками финансирования уже существует.", "ouPostRel", "financingSource", "financingSourceItem");
        }
    }

    @SuppressWarnings("unchecked")
    private List<OrgUnitPostRelation> getOrgUnitPostRelations(StaffList staffList, Long staffListItemId)
    {
        StringBuilder query = new StringBuilder("from " + OrgUnitPostRelation.ENTITY_CLASS + " rel");
        query.append(" where rel." + OrgUnitPostRelation.L_ORG_UNIT + "=:" + OrgUnitPostRelation.L_ORG_UNIT);

        query.append(" order by rel." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE);

        Query q = getSession().createQuery(query.toString());
        q.setParameter(OrgUnitPostRelation.L_ORG_UNIT, staffList.getOrgUnit());

        return q.list();
    }
}