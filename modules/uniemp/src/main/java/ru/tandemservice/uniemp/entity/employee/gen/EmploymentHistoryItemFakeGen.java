package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент Стажа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmploymentHistoryItemFakeGen extends EmploymentHistoryItemBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake";
    public static final String ENTITY_NAME = "employmentHistoryItemFake";
    public static final int VERSION_HASH = -435203466;
    private static IEntityMeta ENTITY_META;

    public static final String P_COMMENT = "comment";

    private String _comment;     // Примечание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Примечание.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Примечание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmploymentHistoryItemFakeGen)
        {
            setComment(((EmploymentHistoryItemFake)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmploymentHistoryItemFakeGen> extends EmploymentHistoryItemBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmploymentHistoryItemFake.class;
        }

        public T newInstance()
        {
            return (T) new EmploymentHistoryItemFake();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmploymentHistoryItemFake> _dslPath = new Path<EmploymentHistoryItemFake>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmploymentHistoryItemFake");
    }
            

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends EmploymentHistoryItemFake> extends EmploymentHistoryItemBase.Path<E>
    {
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EmploymentHistoryItemFakeGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return EmploymentHistoryItemFake.class;
        }

        public String getEntityName()
        {
            return "employmentHistoryItemFake";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
