/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistency.Add;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnitTypeRestriction;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.ScienceDegreeType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.IUniempDAO;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.util.UniempReportUtil;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 15.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReportDate(new Date());
        model.setOrgUnitTypesList(getOrgUnitTypesList());
        model.setOrgUnitsListModel(new OrgUnitMultiSelectModel(model, this));
        
        OrgUnitType facultyType = getCatalogItem(OrgUnitType.class, OrgUnitTypeCodes.FACULTY); 
        if (model.getOrgUnitTypesList().contains(facultyType))
            model.setOrgUnitType(facultyType);
    }

    private List<OrgUnitType> getOrgUnitTypesList()
    {
        MQBuilder builder = new MQBuilder(OrgUnitTypeRestriction.ENTITY_CLASS, "outr", new String[] { OrgUnitTypeRestriction.L_PARENT });
        builder.add(MQExpression.eq("outr", OrgUnitTypeRestriction.L_CHILD + "." + OrgUnitType.P_CODE, OrgUnitTypeCodes.CATHEDRA));
        builder.addOrder("outr", OrgUnitTypeRestriction.L_PARENT + "." + OrgUnitType.P_TITLE);
        return builder.getResultList(getSession());
    }
    
    @Override
    public List<OrgUnit> getOrgUnitsList(OrgUnitType orgUnitType, String filter)
    {
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.L_ORG_UNIT_TYPE, orgUnitType));
        builder.addOrder("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_TITLE);
        builder.addOrder("ou", OrgUnit.P_TITLE);
        return builder.getResultList(getSession());
    }
    
    @Override
    public Integer preparePrintReport(Model model)
    {
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("reportDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDate()));
        String orgUnit = model.isOrgUnitActive() ? model.getOrgUnit().getFullTitle() : ("по " + model.getOrgUnitType().getDativePlural());
        paramModifier.put("orgUnit", orgUnit);
        
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.getName2data().putAll(prepareTablesData(model));
        tableModifier.put("T", new RtfRowIntercepterBase(){
        // Format rows according to template
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int cnt = -startIndex;
                for(RtfRow row : newRowList)
                {
                    cnt++;
                    if(cnt % 2 == 0)
                    {
                        row.getCellList().get(0).getElementList().add(0, RtfBean.getElementFactory().createRtfControl(IRtfData.I));
                        IRtfControl contr = RtfBean.getElementFactory().createRtfControl(IRtfData.I, 0);
//                        contr.setValue(0);
                        row.getCellList().get(row.getCellList().size() - 1).getElementList().add(contr);
                    }
                    else
                    {
                        row.getFormatting().setHeight(600);
                    }
                }
            }
        });
        
        return UniempReportUtil.preparePrintingReport(getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_QUALITY_CONSISTENCY), paramModifier, tableModifier);
    }
    
    private Map<String, String[][]> prepareTablesData(Model model)
    {
        Map<String, String[][]> result = new HashMap<>();

        List<OrgUnit> orgUnitsList = new ArrayList<>();
        Map<Long, Integer> totalOrgUnitMap = new HashMap<>();
        Map<Long, Integer> totalOrgUnitSJMap = new HashMap<>();
        Map<Person, ScienceDegreeType> scienceDegreesMap = IUniempDAO.instance.get().getEmployeeDegreesMap(model.getReportDate());
        Map<Long, Integer> doctorsMap = new HashMap<>();
        Map<Long, Integer> doctorsSJMap = new HashMap<>();
        Map<Long, Integer> mastersMap = new HashMap<>();
        Map<Long, Integer> mastersSJMap = new HashMap<>();
        Map<Long, Integer> ppsMap = new HashMap<>();
        Map<Long, Integer> ppsSJMap = new HashMap<>();
        totalOrgUnitMap.put(0L, 0);
        totalOrgUnitSJMap.put(0L, 0);
        doctorsMap.put(0L, 0);
        doctorsSJMap.put(0L, 0);
        mastersMap.put(0L, 0);
        mastersSJMap.put(0L, 0);
        ppsMap.put(0L, 0);
        ppsSJMap.put(0L, 0);

        for (EmployeePost post : getEmployeePostsList(model))
        {
            Long orgUnitId = post.getOrgUnit().getId();
            if (!orgUnitsList.contains(post.getOrgUnit())) orgUnitsList.add(post.getOrgUnit());
            
            putValuesToMaps(post, orgUnitId, totalOrgUnitMap, totalOrgUnitSJMap);
            ScienceDegreeType degreeType = scienceDegreesMap.get(post.getEmployee().getPerson());
            if (null != degreeType)
            {
                if (UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR.equals(degreeType.getCode()))
                {
                    putValuesToMaps(post, orgUnitId, doctorsMap, doctorsSJMap);
                }
                else
                {
                    putValuesToMaps(post, orgUnitId, mastersMap, mastersSJMap);
                }
            }
            else
            {
                putValuesToMaps(post, orgUnitId, ppsMap, ppsSJMap);
            }
        }

        List<String[]> resultList = new ArrayList<>();
        for (OrgUnit orgUnit : orgUnitsList)
        {
            resultList.add(generateReportLine(orgUnit.getId(), orgUnit.getTitle(), totalOrgUnitMap, doctorsMap, mastersMap, ppsMap));
            resultList.add(generateReportLine(orgUnit.getId(), "в т.ч. совместители", totalOrgUnitSJMap, doctorsSJMap, mastersSJMap, ppsSJMap));
        }
        result.put("T", resultList.toArray(new String[][] {}));

        result.put("TS1", new String[][] { generateReportLine(0L, "Итого", totalOrgUnitMap, doctorsMap, mastersMap, ppsMap) });
        result.put("TS2", new String[][] { generateReportLine(0L, "в т.ч. совместители", totalOrgUnitSJMap, doctorsSJMap, mastersSJMap, ppsSJMap) });

        return result;
    }
    
    private void putValuesToMaps(EmployeePost post, Long orgUnitId, Map<Long, Integer> totMap, Map<Long, Integer> totSJMap)
    {
        Integer typedCnt = totMap.get(orgUnitId);
        if (null == typedCnt) typedCnt = 0;
        totMap.put(orgUnitId, ++typedCnt);
        totMap.put(0L, totMap.get(0L) + 1);
        if (UniDefines.POST_TYPE_SECOND_JOB.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode()))
        {
            Integer typedSJCnt = totSJMap.get(orgUnitId);
            if (null == typedSJCnt) typedSJCnt = 0;
            totSJMap.put(orgUnitId, ++typedSJCnt);
            totSJMap.put(0L, totSJMap.get(0L) + 1);
        }
    }
    
    private String[] generateReportLine(Long orgUnitId, String title, Map<Long, Integer> totalMap, Map<Long, Integer> doctorsMap, Map<Long, Integer> mastersMap, Map<Long, Integer> ppsMap)
    {
        String[] line = new String[8];

        line[0] = title;
        line[1] = doctorsMap.containsKey(orgUnitId) && doctorsMap.get(orgUnitId) > 0? doctorsMap.get(orgUnitId).toString() : "-";
        line[2] = mastersMap.containsKey(orgUnitId) && mastersMap.get(orgUnitId) > 0 ? mastersMap.get(orgUnitId).toString() : "-";
        line[3] = ppsMap.containsKey(orgUnitId) && ppsMap.get(orgUnitId) > 0 ? ppsMap.get(orgUnitId).toString() : "-";
        line[4] = totalMap.containsKey(orgUnitId) && totalMap.get(orgUnitId) > 0 ? totalMap.get(orgUnitId).toString() : "-";

        return line;
    }

    private List<EmployeePost> getEmployeePostsList(Model model)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        //builder.add(MQExpression.eq("ep", EmployeePost.L_POST_STATUS + "." + EmployeePostStatus.P_ACTIVE, Boolean.TRUE));
        builder.add(MQExpression.in("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, EmployeeManager.instance().dao().getEmployeeTypeWithAllChilds(getCatalogItem(EmployeeType.class, EmployeeTypeCodes.EDU_STAFF))));
        builder.add(MQExpression.lessOrEq("ep", EmployeePost.P_POST_DATE, model.getReportDate()));
        AbstractExpression expr1 = MQExpression.isNull("ep", EmployeePost.P_DISMISSAL_DATE);
        AbstractExpression expr2 = MQExpression.greatOrEq("ep", EmployeePost.P_DISMISSAL_DATE, model.getReportDate());
        builder.add(MQExpression.or(expr1, expr2));
        builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_CODE, OrgUnitTypeCodes.CATHEDRA));
        builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_PARENT + "." + OrgUnit.L_ORG_UNIT_TYPE, model.getOrgUnitType()));
        if (model.isOrgUnitActive())
            builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_PARENT, model.getOrgUnit()));
        builder.addOrder("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_PRIORITY);
        builder.addOrder("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.P_TITLE);
        return builder.getResultList(getSession());
    }
}