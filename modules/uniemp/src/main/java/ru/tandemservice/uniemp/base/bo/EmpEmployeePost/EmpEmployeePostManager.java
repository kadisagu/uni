/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeePost;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniemp.base.bo.EmpEmployeePost.logic.EmpEmployeePostDAO;
import ru.tandemservice.uniemp.base.bo.EmpEmployeePost.logic.IEmpEmployeePostDAO;

/**
 * Create by ashaburov
 * Date 30.05.12
 */
@Configuration
public class EmpEmployeePostManager extends BusinessObjectManager
{
    public static EmpEmployeePostManager instance()
    {
        return instance(EmpEmployeePostManager.class);
    }

    @Bean
    public IEmpEmployeePostDAO employeePostDAO()
    {
        return new EmpEmployeePostDAO();
    }
}
