/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.EmpSummaryPPSReportManager;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.11.12
 */
public class EmpSummaryPPSReportDao extends UniBaseDao implements IEmpSummaryPPSReportDao
{
    @Override
    public List<SummaryDataWrapper<EmployeeWrapper>> getSummaryPPSList(Date reportDate, List<PostType> postTypeList, List<EmployeeWrapper> employeeWrapperList)
    {
        List<SummaryDataWrapper<EmployeeWrapper>> resultWrapperList = new ArrayList<>();

        postTypeList = postTypeList != null && !postTypeList.isEmpty() ? postTypeList : getCatalogItemList(PostType.class);

        long id = 1L;
        for (final TripletKey<String, Double, Double> key : getRowList())
        {
            SummaryDataWrapper<EmployeeWrapper> row = new SummaryDataWrapper<>();
            row.setId(id++);
            row.setTitle(key.getFirst());

            for (final PostType postType : postTypeList)
            {
                row.addIncrement(postType.getCode(), input -> {
                    if (key.getSecond() <= input.getStaffRate().doubleValue() && input.getStaffRate().doubleValue() <= key.getThird())
                    {
                        List<EmployeePost> postList = input.postList(postType);
                        for (EmployeePost post : postList)
                            return !post.isHourlyPaid();
                    }
                    return false;
                });
            }
            row.addIncrement("hourlyPaid", input -> {
                if (key.getSecond() <= input.getStaffRate().doubleValue() && input.getStaffRate().doubleValue() <= key.getThird())
                    return input.hasHourlyPaid();
                return false;
            });

            for (EmployeeWrapper wrapper : employeeWrapperList)
                row.inc(wrapper);

            resultWrapperList.add(row);
        }

        return resultWrapperList;
    }

    @Override
    public List<SummaryDataWrapper<EmployeeWrapper>> getSummaryPPSWithScienceList(Date reportDate, List<PostType> postTypeList, List<EmployeeWrapper> employeeWrapperList)
    {
        List<SummaryDataWrapper<EmployeeWrapper>> resultWrapperList = new ArrayList<>();

        postTypeList = postTypeList != null && !postTypeList.isEmpty() ? postTypeList : getCatalogItemList(PostType.class);

        long id = 1L;
        for (final TripletKey<String, Double, Double> key : getRowList())
        {
            SummaryDataWrapper<EmployeeWrapper> row = new SummaryDataWrapper<>();
            row.setId(id++);
            row.setTitle(key.getFirst());

            for (final PostType postType : postTypeList)
            {
                row.addIncrement(postType.getCode(), input -> {
                    if (key.getSecond() <= input.getStaffRate().doubleValue() && input.getStaffRate().doubleValue() <= key.getThird())
                    {
                        List<EmployeePost> postList = input.postList(postType);
                        for (EmployeePost post : postList)
                            return !post.isHourlyPaid() && input.hasScience();
                    }
                    return false;
                });
            }
            row.addIncrement("hourlyPaid", input -> {
                if (key.getSecond() <= input.getStaffRate().doubleValue() && input.getStaffRate().doubleValue() <= key.getThird())
                    return input.hasHourlyPaid() && input.hasScience();
                return false;
            });

            for (EmployeeWrapper wrapper : employeeWrapperList)
                row.inc(wrapper);

            resultWrapperList.add(row);
        }

        return resultWrapperList;
    }

    @Override
    public List<SummaryDataWrapper<EmployeeWrapper>> getSummaryPPSWithHighScienceList(Date reportDate, List<PostType> postTypeList, List<EmployeeWrapper> employeeWrapperList)
    {
        List<SummaryDataWrapper<EmployeeWrapper>> resultWrapperList = new ArrayList<>();

        postTypeList = postTypeList != null && !postTypeList.isEmpty() ? postTypeList : getCatalogItemList(PostType.class);

        long id = 1L;
        for (final TripletKey<String, Double, Double> key : getRowList())
        {
            SummaryDataWrapper<EmployeeWrapper> row = new SummaryDataWrapper<>();
            row.setId(id++);
            row.setTitle(key.getFirst());

            for (final PostType postType : postTypeList)
            {
                row.addIncrement(postType.getCode(), input -> {
                    if (key.getSecond() <= input.getStaffRate().doubleValue() && input.getStaffRate().doubleValue() <= key.getThird())
                    {
                        List<EmployeePost> postList = input.postList(postType);
                        for (EmployeePost post : postList)
                            return !post.isHourlyPaid() && input.hasHighScience();

                    }
                    return false;
                });
            }
            row.addIncrement("hourlyPaid", input -> {
                if (key.getSecond() <= input.getStaffRate().doubleValue() && input.getStaffRate().doubleValue() <= key.getThird())
                    return input.hasHourlyPaid() && input.hasHighScience();
                return false;
            });

            for (EmployeeWrapper wrapper : employeeWrapperList)
                row.inc(wrapper);

            resultWrapperList.add(row);
        }

        return resultWrapperList;
    }

    public List<EmployeeWrapper> getEmployeeWrapperList(Date reportDate)
    {
        final Map<Long, EmployeeWrapper> resultEmployeeWrappers = new HashMap<>();
        final Map<Long, List<ScienceDegree>> personDegreeMap = new HashMap<>();
        final Map<Long, List<ScienceStatus>> personStatusMap = new HashMap<>();
        final Map<Long, BigDecimal> postStaffRateMap = new HashMap<>();

        final List<EmployeePost> employeePostList = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column(property("ep"))
                .where(eq(property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().code().fromAlias("ep")), value(EmployeeTypeCodes.EDU_STAFF)))
                .where(ne(property(EmployeePost.postStatus().code().fromAlias("ep")), value(EmployeePostStatusCodes.STATUS_POSSIBLE)))
                .where(le(property(EmployeePost.postDate().fromAlias("ep")), value(reportDate, PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeePost.dismissalDate().fromAlias("ep"))),
                        ge(property(EmployeePost.dismissalDate().fromAlias("ep")), value(reportDate, PropertyType.DATE))
                ))
                .createStatement(getSession()).list();

        BatchUtils.execute(UniBaseUtils.getPropertiesSet(employeePostList, EmployeePost.employee().person().id().s()), 128, elements -> {
            List<PersonAcademicDegree> list = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "d").column(property("d"))
                    .where(in(property(PersonAcademicDegree.person().id().fromAlias("d")), elements))
                    .createStatement(getSession()).list();

            for (PersonAcademicDegree academicDegree : list)
            {
                List<ScienceDegree> degreeList = personDegreeMap.get(academicDegree.getPerson().getId());
                if (degreeList == null)
                    personDegreeMap.put(academicDegree.getPerson().getId(), degreeList = new ArrayList<>());
                degreeList.add(academicDegree.getAcademicDegree());
            }
        });

        BatchUtils.execute(UniBaseUtils.getPropertiesSet(employeePostList, EmployeePost.employee().person().id().s()), 128, elements -> {
            List<PersonAcademicStatus> list = new DQLSelectBuilder().fromEntity(PersonAcademicStatus.class, "s").column(property("s"))
                    .where(in(property(PersonAcademicStatus.person().id().fromAlias("s")), elements))
                    .createStatement(getSession()).list();

            for (PersonAcademicStatus academicDegree : list)
            {
                List<ScienceStatus> statusList = personStatusMap.get(academicDegree.getPerson().getId());
                if (statusList == null)
                    personStatusMap.put(academicDegree.getPerson().getId(), statusList = new ArrayList<>());
                statusList.add(academicDegree.getAcademicStatus());
            }
        });

        BatchUtils.execute(ids(employeePostList), 128, elements -> {
            List<EmployeePostStaffRateItem> list = new DQLSelectBuilder().fromEntity(EmployeePostStaffRateItem.class, "r").column("r")
                    .where(in(property(EmployeePostStaffRateItem.employeePost().id().fromAlias("r")), elements))
                    .createStatement(getSession()).list();

            for (EmployeePostStaffRateItem item : list)
            {
                BigDecimal rate = postStaffRateMap.get(item.getEmployeePost().getId());
                if (rate == null)
                    postStaffRateMap.put(item.getEmployeePost().getId(), new BigDecimal(item.getStaffRate()).setScale(2, RoundingMode.HALF_UP));
                else
                    postStaffRateMap.put(item.getEmployeePost().getId(), new BigDecimal(rate.doubleValue() + item.getStaffRate()).setScale(2, RoundingMode.HALF_UP));
            }
        });

        for (EmployeePost post : employeePostList)
        {
            EmployeeWrapper wrapper = resultEmployeeWrappers.get(post.getEmployee().getId());
            if (wrapper == null)
                resultEmployeeWrappers.put(post.getEmployee().getId(), wrapper = new EmployeeWrapper(post.getEmployee().getId()));
            wrapper.getPostList().add(post);
            wrapper.getScienceDegreeList().addAll(personDegreeMap.containsKey(post.getPerson().getId()) ? personDegreeMap.get(post.getPerson().getId()) : Collections.<ScienceDegree>emptyList());
            wrapper.getScienceStatusList().addAll(personStatusMap.containsKey(post.getPerson().getId()) ? personStatusMap.get(post.getPerson().getId()) : Collections.<ScienceStatus>emptyList());
            wrapper.setStaffRate(new BigDecimal(postStaffRateMap.containsKey(post.getId()) ? postStaffRateMap.get(post.getId()).doubleValue() : 0d).setScale(2, RoundingMode.HALF_UP));
        }

        return new ArrayList<>(resultEmployeeWrappers.values());
    }

    protected List<TripletKey<String, Double, Double>> getRowList()
    {
        List<TripletKey<String, Double, Double>> resultList = new ArrayList<>();
        resultList.add(new TripletKey<>("0 - 0,25", 0d, 0.25d));
        resultList.add(new TripletKey<>("0,26 - 0,5", 0.26d, 0.5d));
        resultList.add(new TripletKey<>("0,51 - 0,75", 0.51d, 0.75d));
        resultList.add(new TripletKey<>("0,76 - 1", 0.76d, 1d));
        resultList.add(new TripletKey<>("Всего", 0d, 1d));

        return resultList;
    }

    @Override
    public RtfDocument getReportDocument(Date reportDate, List<PostType> postTypeList)
    {
        List<EmployeeWrapper> employeeWrapperList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getEmployeeWrapperList(reportDate);

        List<SummaryDataWrapper<EmployeeWrapper>> summaryPPSList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSList(reportDate, postTypeList, employeeWrapperList);
        List<SummaryDataWrapper<EmployeeWrapper>> summaryPPSWithScienceList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSWithScienceList(reportDate, postTypeList, employeeWrapperList);
        List<SummaryDataWrapper<EmployeeWrapper>> summaryPPSWithHighScienceList = EmpSummaryPPSReportManager.instance().empSummaryPPSReportDao().getSummaryPPSWithHighScienceList(reportDate, postTypeList, employeeWrapperList);

        byte[] content = DataAccessServices.dao().get(EmployeeTemplateDocument.class, EmployeeTemplateDocument.code(), "empSummaryPPSReport").getContent();
        RtfDocument document = new RtfReader().read(content);

        final List<PostType> postTypes = postTypeList != null && !postTypeList.isEmpty() ? postTypeList : getCatalogItemList(PostType.class);
        int columnNumber = 2 + postTypes.size();

        List<String[]> summaryPPSLineList = new ArrayList<>();
        List<String[]> summaryPPSWithScienceLineList = new ArrayList<>();
        List<String[]> summaryPPSWithHighScienceLineList = new ArrayList<>();

        for (SummaryDataWrapper wrapper : summaryPPSList)
        {
            int index = 0;

            String[] line = new String[columnNumber];

            line[index++] = wrapper.getTitle();
            for (PostType postType : postTypes)
                line[index++] = wrapper.getProperty(postType.getCode()).toString();
            line[index] = wrapper.getProperty("hourlyPaid").toString();

            summaryPPSLineList.add(line);
        }

        for (SummaryDataWrapper wrapper : summaryPPSWithScienceList)
        {
            int index = 0;

            String[] line = new String[columnNumber];

            line[index++] = wrapper.getTitle();
            for (PostType postType : postTypes)
                line[index++] = wrapper.getProperty(postType.getCode()).toString();
            line[index] = wrapper.getProperty("hourlyPaid").toString();

            summaryPPSWithScienceLineList.add(line);
        }

        for (SummaryDataWrapper wrapper : summaryPPSWithHighScienceList)
        {
            int index = 0;

            String[] line = new String[columnNumber];

            line[index++] = wrapper.getTitle();
            for (PostType postType : postTypes)
                line[index++] = wrapper.getProperty(postType.getCode()).toString();
            line[index] = wrapper.getProperty("hourlyPaid").toString();

            summaryPPSWithHighScienceLineList.add(line);
        }

        RtfRowIntercepterBase rowIntercepter = new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                // на число колонок равно кол-ву выбранных Типов назначения
                int[] parts = new int[postTypes.size()];
                Arrays.fill(parts, 1);

                // разбиваем колонки
                RtfRow row = table.getRowList().get(currentRowIndex);
                RtfUtil.splitRow(row, 1, null, parts);
            }
        };

        RtfRowIntercepterBase rowHeadIntercepter = new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                // на число колонок равно кол-ву выбранных Типов назначения
                int[] parts = new int[postTypes.size()];
                Arrays.fill(parts, 1);
                final Iterator<PostType> iterator = postTypes.iterator();

                // интерцептор для именования колонок
                IRtfRowSplitInterceptor headerInterceptor = (newCell, index) -> {
                    PostType next = iterator.next();
                    IRtfElement text = RtfBean.getElementFactory().createRtfText(next.getShortTitle());
                    newCell.getElementList().add(text);
                };

                // разбиваем колонки
                RtfRow row = table.getRowList().get(currentRowIndex);
                RtfUtil.splitRow(row, 1, headerInterceptor, parts);
            }
        };

        new RtfTableModifier()
                .put("T1", summaryPPSLineList.toArray(new String[summaryPPSLineList.size()][columnNumber]))
                .put("T2", summaryPPSWithScienceLineList.toArray(new String[summaryPPSWithScienceLineList.size()][columnNumber]))
                .put("T3", summaryPPSWithHighScienceLineList.toArray(new String[summaryPPSWithHighScienceLineList.size()][columnNumber]))
                .put("T1", rowIntercepter)
                .put("T2", rowIntercepter)
                .put("T3", rowIntercepter)
                .put("postTypeColumns", new String[][]{new String[]{""}})
                .put("postTypeColumns", rowHeadIntercepter)
                .modify(document);

        new RtfInjectModifier()
                .put("reportDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(reportDate))
                .modify(document);

        return document;
    }
}
