/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeHolidayAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import ru.tandemservice.uni.dao.IEmployeeDAO;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author dseleznev
 * Created on: 22.12.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errCollector = component.getUserContext().getErrorCollector();
        getDao().validate(getModel(component), errCollector);

        if (!errCollector.hasErrors())
        {
            getDao().update(getModel(component));
            deactivate(component);
        }
    }

    public void onChangeHolidayType(IBusinessComponent component)
    {
        Model model = getModel(component);
        if(model.getEmployeeHoliday().getHolidayType() == null || !model.getEmployeeHoliday().getHolidayType().isChildCare())
        {
            model.getEmployeeHoliday().setChildNumber(null);
        }

        if (model.getEmployeeHoliday().getHolidayType() != null && model.getEmployeeHoliday().getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL))
            model.setPeriodRequired(true);
        else
            model.setPeriodRequired(false);
    }

    public void onDateChange(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getEmployeeHoliday().getStartDate() != null && model.getEmployeeHoliday().getFinishDate() != null &&
                model.getEmployeeHoliday().getDuration() == 0 && model.getEmployeeHoliday().getHolidayType() != null)
        {
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            Integer holidayDuration;
            if (UniempUtil.getAnnualHolidayTypes().contains(model.getEmployeeHoliday().getHolidayType().getCode()) && model.getEmployeeHoliday().getHolidayType().isCompensable())
                holidayDuration = EmployeeManager.instance().dao().getEmployeeHolidayDuration(workWeekDuration, model.getEmployeeHoliday().getStartDate(), model.getEmployeeHoliday().getFinishDate());
            else
                holidayDuration = (int) CommonBaseDateUtil.getBetweenPeriod(model.getEmployeeHoliday().getStartDate(), model.getEmployeeHoliday().getFinishDate(), Calendar.DAY_OF_YEAR) + 1;

            if (holidayDuration != null)
                model.getEmployeeHoliday().setDuration(holidayDuration);
            else
                model.getEmployeeHoliday().setDuration(0);
        }
    }

    public void onDurationChange(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getEmployeeHoliday().getStartDate() != null && (Integer) model.getEmployeeHoliday().getDuration() != null &&
                model.getEmployeeHoliday().getFinishDate() == null && model.getEmployeeHoliday().getHolidayType() != null)
        {
            GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            int counter = 0;
            int duration = model.getEmployeeHoliday().getDuration();

            date.setTime(model.getEmployeeHoliday().getStartDate());

            if (duration > 0)
            {
                do
                {
                    if (UniempUtil.getAnnualHolidayTypes().contains(model.getEmployeeHoliday().getHolidayType().getCode()) && model.getEmployeeHoliday().getHolidayType().isCompensable())
                    {
                        if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                            counter++;
                    }
                    else
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);

                model.getEmployeeHoliday().setFinishDate(date.getTime());
            }


        }
    }
}