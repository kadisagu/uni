/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemEdit;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

/**
 * @author dseleznev
 * Created on: 21.01.2011
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setVacationScheduleItem(get(VacationScheduleItem.class, model.getVacationScheduleItemId()));
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errs = UserContext.getInstance().getErrorCollector();
        validateVacationScheduleItemHasIntersections(model.getVacationScheduleItem(), errs, "Указанный планируемый отпуск", "planDate", "factDate", "daysAmount");

        if (!errs.hasErrors())
            update(model.getVacationScheduleItem());
    }
}