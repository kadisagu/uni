/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemSplit;

import java.util.Date;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

/**
 * @author dseleznev
 * Created on: 27.01.2011
 */
@Input(@Bind(key = "vacationScheduleItemId", binding = "vacationScheduleItemId"))
public class Model
{
    private Long _vacationScheduleItemId;
    private VacationScheduleItem _vacationScheduleItem;

    private Date _firstPartPlanDate;
    private int _firstPartDaysAmount;

    private Date _secondPartPlanDate;
    private int _secondPartDaysAmount;

    public Long getVacationScheduleItemId()
    {
        return _vacationScheduleItemId;
    }

    public void setVacationScheduleItemId(Long vacationScheduleItemId)
    {
        this._vacationScheduleItemId = vacationScheduleItemId;
    }

    public VacationScheduleItem getVacationScheduleItem()
    {
        return _vacationScheduleItem;
    }

    public void setVacationScheduleItem(VacationScheduleItem vacationScheduleItem)
    {
        this._vacationScheduleItem = vacationScheduleItem;
    }

    public Date getFirstPartPlanDate()
    {
        return _firstPartPlanDate;
    }

    public void setFirstPartPlanDate(Date firstPartPlanDate)
    {
        this._firstPartPlanDate = firstPartPlanDate;
    }

    public int getFirstPartDaysAmount()
    {
        return _firstPartDaysAmount;
    }

    public void setFirstPartDaysAmount(int firstPartDaysAmount)
    {
        this._firstPartDaysAmount = firstPartDaysAmount;
    }

    public Date getSecondPartPlanDate()
    {
        return _secondPartPlanDate;
    }

    public void setSecondPartPlanDate(Date secondPartPlanDate)
    {
        this._secondPartPlanDate = secondPartPlanDate;
    }

    public int getSecondPartDaysAmount()
    {
        return _secondPartDaysAmount;
    }

    public void setSecondPartDaysAmount(int secondPartDaysAmount)
    {
        this._secondPartDaysAmount = secondPartDaysAmount;
    }
}