/**
 * $Id$
 */
package ru.tandemservice.uniemp.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author dseleznev
 * Created on: 04.08.2008
 */
public class UniempDaoFacade
{
    private static IStaffListDAO _staffListDAO;
    private static IStaffListReportsDAO _staffListReportsDAO;

    public static IUniempDAO<Object> getUniempDAO()
    {
        return IUniempDAO.instance.get();
    }

    @SuppressWarnings("unchecked")
    public static IStaffListDAO getStaffListDAO()
    {
        if (_staffListDAO == null)
            _staffListDAO = (IStaffListDAO)ApplicationRuntime.getBean(IStaffListDAO.STAFF_LIST_DAO_BEAN_NAME);
        return _staffListDAO;
    }

    public static IStaffListReportsDAO getStaffListReportsDAO()
    {
        if (_staffListReportsDAO == null)
            _staffListReportsDAO = (IStaffListReportsDAO)ApplicationRuntime.getBean(IStaffListReportsDAO.STAFF_LIST_REPORTS_DAO_BEAN_NAME);
        return _staffListReportsDAO;
    }
}