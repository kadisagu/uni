/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.employee.EmployeeSickListAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author E. Grigoriev
 * @since 08.07.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        getDao().prepare(getModel(context));
        super.onRefreshComponent(context);
    }
    
    public void onClickApply(IBusinessComponent context)
    {
        ErrorCollector errors = context.getUserContext().getErrorCollector();
        Model model = getModel(context);
        getDao().validate(model, errors);
        if (errors.hasErrors()) return;
        
        getDao().save(getModel(context).getEmployeeSickList());
        deactivate(context);
    }
    
}
