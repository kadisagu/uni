package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь Истории должностей и Вида стажа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmpHistoryToServiceLengthTypeRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation";
    public static final String ENTITY_NAME = "empHistoryToServiceLengthTypeRelation";
    public static final int VERSION_HASH = -172236907;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYMENT_HISTORY_ITEM_BASE = "employmentHistoryItemBase";
    public static final String L_SERVICE_LENGTH_TYPE = "serviceLengthType";

    private EmploymentHistoryItemBase _employmentHistoryItemBase;     // Элемент истории должностей сотрудника (базовый)
    private ServiceLengthType _serviceLengthType;     // Виды стажа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент истории должностей сотрудника (базовый). Свойство не может быть null.
     */
    @NotNull
    public EmploymentHistoryItemBase getEmploymentHistoryItemBase()
    {
        return _employmentHistoryItemBase;
    }

    /**
     * @param employmentHistoryItemBase Элемент истории должностей сотрудника (базовый). Свойство не может быть null.
     */
    public void setEmploymentHistoryItemBase(EmploymentHistoryItemBase employmentHistoryItemBase)
    {
        dirty(_employmentHistoryItemBase, employmentHistoryItemBase);
        _employmentHistoryItemBase = employmentHistoryItemBase;
    }

    /**
     * @return Виды стажа. Свойство не может быть null.
     */
    @NotNull
    public ServiceLengthType getServiceLengthType()
    {
        return _serviceLengthType;
    }

    /**
     * @param serviceLengthType Виды стажа. Свойство не может быть null.
     */
    public void setServiceLengthType(ServiceLengthType serviceLengthType)
    {
        dirty(_serviceLengthType, serviceLengthType);
        _serviceLengthType = serviceLengthType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmpHistoryToServiceLengthTypeRelationGen)
        {
            setEmploymentHistoryItemBase(((EmpHistoryToServiceLengthTypeRelation)another).getEmploymentHistoryItemBase());
            setServiceLengthType(((EmpHistoryToServiceLengthTypeRelation)another).getServiceLengthType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmpHistoryToServiceLengthTypeRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmpHistoryToServiceLengthTypeRelation.class;
        }

        public T newInstance()
        {
            return (T) new EmpHistoryToServiceLengthTypeRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employmentHistoryItemBase":
                    return obj.getEmploymentHistoryItemBase();
                case "serviceLengthType":
                    return obj.getServiceLengthType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employmentHistoryItemBase":
                    obj.setEmploymentHistoryItemBase((EmploymentHistoryItemBase) value);
                    return;
                case "serviceLengthType":
                    obj.setServiceLengthType((ServiceLengthType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employmentHistoryItemBase":
                        return true;
                case "serviceLengthType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employmentHistoryItemBase":
                    return true;
                case "serviceLengthType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employmentHistoryItemBase":
                    return EmploymentHistoryItemBase.class;
                case "serviceLengthType":
                    return ServiceLengthType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmpHistoryToServiceLengthTypeRelation> _dslPath = new Path<EmpHistoryToServiceLengthTypeRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmpHistoryToServiceLengthTypeRelation");
    }
            

    /**
     * @return Элемент истории должностей сотрудника (базовый). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation#getEmploymentHistoryItemBase()
     */
    public static EmploymentHistoryItemBase.Path<EmploymentHistoryItemBase> employmentHistoryItemBase()
    {
        return _dslPath.employmentHistoryItemBase();
    }

    /**
     * @return Виды стажа. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation#getServiceLengthType()
     */
    public static ServiceLengthType.Path<ServiceLengthType> serviceLengthType()
    {
        return _dslPath.serviceLengthType();
    }

    public static class Path<E extends EmpHistoryToServiceLengthTypeRelation> extends EntityPath<E>
    {
        private EmploymentHistoryItemBase.Path<EmploymentHistoryItemBase> _employmentHistoryItemBase;
        private ServiceLengthType.Path<ServiceLengthType> _serviceLengthType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент истории должностей сотрудника (базовый). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation#getEmploymentHistoryItemBase()
     */
        public EmploymentHistoryItemBase.Path<EmploymentHistoryItemBase> employmentHistoryItemBase()
        {
            if(_employmentHistoryItemBase == null )
                _employmentHistoryItemBase = new EmploymentHistoryItemBase.Path<EmploymentHistoryItemBase>(L_EMPLOYMENT_HISTORY_ITEM_BASE, this);
            return _employmentHistoryItemBase;
        }

    /**
     * @return Виды стажа. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation#getServiceLengthType()
     */
        public ServiceLengthType.Path<ServiceLengthType> serviceLengthType()
        {
            if(_serviceLengthType == null )
                _serviceLengthType = new ServiceLengthType.Path<ServiceLengthType>(L_SERVICE_LENGTH_TYPE, this);
            return _serviceLengthType;
        }

        public Class getEntityClass()
        {
            return EmpHistoryToServiceLengthTypeRelation.class;
        }

        public String getEntityName()
        {
            return "empHistoryToServiceLengthTypeRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
