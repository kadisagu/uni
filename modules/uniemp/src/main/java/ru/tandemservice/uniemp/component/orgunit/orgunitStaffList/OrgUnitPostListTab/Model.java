/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitPostListTab;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;

/**
 * @author dseleznev
 * Created on: 16.09.2008
 */
@State( { @Bind(key = "orgUnitId", binding = "orgUnitId") })
@Output(keys = { "orgUnitId", "orgUnitPostRelationId" }, bindings = { "orgUnitId", "orgUnitPostRelationId" })
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private Long _orgUnitPostRelationId;
    private DynamicListDataSource<OrgUnitPostRelation> _orgUnitPostDataSource;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public DynamicListDataSource<OrgUnitPostRelation> getOrgUnitPostDataSource()
    {
        return _orgUnitPostDataSource;
    }

    public void setOrgUnitPostDataSource(DynamicListDataSource<OrgUnitPostRelation> orgUnitPostDataSource)
    {
        this._orgUnitPostDataSource = orgUnitPostDataSource;
    }

    public Long getOrgUnitPostRelationId()
    {
        return _orgUnitPostRelationId;
    }

    public void setOrgUnitPostRelationId(Long orgUnitPostRelationId)
    {
        this._orgUnitPostRelationId = orgUnitPostRelationId;
    }

    public String getAddPostKey()
    {
        return "orgUnit_addPost_" + getOrgUnit().getOrgUnitType().getCode();
    }
}