package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;
import ru.tandemservice.uniemp.entity.catalog.MedicalQualificationCategory;
import ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Врачебная специальность сотрудника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeMedicalSpecialityGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality";
    public static final String ENTITY_NAME = "employeeMedicalSpeciality";
    public static final int VERSION_HASH = -894928854;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE = "employee";
    public static final String L_MEDICAL_EDUCATION_LEVEL = "medicalEducationLevel";
    public static final String L_MEDICAL_QUALIFICATION_CATEGORY = "medicalQualificationCategory";
    public static final String P_ASSIGN_DATE = "assignDate";
    public static final String P_ACCEPTION_DATE = "acceptionDate";
    public static final String P_CATEGORY_ASSIGN_DATE = "categoryAssignDate";
    public static final String P_CATEGORY_ACCEPTION_DATE = "categoryAcceptionDate";
    public static final String P_EXPERT_CERTIFICATE_AVAILABLE = "expertCertificateAvailable";
    public static final String L_EXPERT_CERTIFICATE = "expertCertificate";
    public static final String P_CERTIFICATE_FILE_NAME = "certificateFileName";
    public static final String P_CERTIFICATE_FILE_TYPE = "certificateFileType";

    private Employee _employee;     // Кадровый ресурс
    private MedicalEducationLevel _medicalEducationLevel;     // Врачебные специальности
    private MedicalQualificationCategory _medicalQualificationCategory;     // Квалификационные категории врачей
    private Date _assignDate;     // Дата присуждения
    private Date _acceptionDate;     // Дата подтверждения
    private Date _categoryAssignDate;     // Дата присуждения категории
    private Date _categoryAcceptionDate;     // Дата подтверждения категории
    private Boolean _expertCertificateAvailable;     // Имеется Сертификат специалиста
    private DatabaseFile _expertCertificate;     // Сертификат специалиста
    private String _certificateFileName;     // Имя файла Сертификат специалиста
    private String _certificateFileType;     // Тип файла Сертификат специалиста

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     */
    @NotNull
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Кадровый ресурс. Свойство не может быть null.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    /**
     * @return Врачебные специальности. Свойство не может быть null.
     */
    @NotNull
    public MedicalEducationLevel getMedicalEducationLevel()
    {
        return _medicalEducationLevel;
    }

    /**
     * @param medicalEducationLevel Врачебные специальности. Свойство не может быть null.
     */
    public void setMedicalEducationLevel(MedicalEducationLevel medicalEducationLevel)
    {
        dirty(_medicalEducationLevel, medicalEducationLevel);
        _medicalEducationLevel = medicalEducationLevel;
    }

    /**
     * @return Квалификационные категории врачей. Свойство не может быть null.
     */
    @NotNull
    public MedicalQualificationCategory getMedicalQualificationCategory()
    {
        return _medicalQualificationCategory;
    }

    /**
     * @param medicalQualificationCategory Квалификационные категории врачей. Свойство не может быть null.
     */
    public void setMedicalQualificationCategory(MedicalQualificationCategory medicalQualificationCategory)
    {
        dirty(_medicalQualificationCategory, medicalQualificationCategory);
        _medicalQualificationCategory = medicalQualificationCategory;
    }

    /**
     * @return Дата присуждения. Свойство не может быть null.
     */
    @NotNull
    public Date getAssignDate()
    {
        return _assignDate;
    }

    /**
     * @param assignDate Дата присуждения. Свойство не может быть null.
     */
    public void setAssignDate(Date assignDate)
    {
        dirty(_assignDate, assignDate);
        _assignDate = assignDate;
    }

    /**
     * @return Дата подтверждения.
     */
    public Date getAcceptionDate()
    {
        return _acceptionDate;
    }

    /**
     * @param acceptionDate Дата подтверждения.
     */
    public void setAcceptionDate(Date acceptionDate)
    {
        dirty(_acceptionDate, acceptionDate);
        _acceptionDate = acceptionDate;
    }

    /**
     * @return Дата присуждения категории.
     */
    public Date getCategoryAssignDate()
    {
        return _categoryAssignDate;
    }

    /**
     * @param categoryAssignDate Дата присуждения категории.
     */
    public void setCategoryAssignDate(Date categoryAssignDate)
    {
        dirty(_categoryAssignDate, categoryAssignDate);
        _categoryAssignDate = categoryAssignDate;
    }

    /**
     * @return Дата подтверждения категории.
     */
    public Date getCategoryAcceptionDate()
    {
        return _categoryAcceptionDate;
    }

    /**
     * @param categoryAcceptionDate Дата подтверждения категории.
     */
    public void setCategoryAcceptionDate(Date categoryAcceptionDate)
    {
        dirty(_categoryAcceptionDate, categoryAcceptionDate);
        _categoryAcceptionDate = categoryAcceptionDate;
    }

    /**
     * @return Имеется Сертификат специалиста.
     */
    public Boolean getExpertCertificateAvailable()
    {
        return _expertCertificateAvailable;
    }

    /**
     * @param expertCertificateAvailable Имеется Сертификат специалиста.
     */
    public void setExpertCertificateAvailable(Boolean expertCertificateAvailable)
    {
        dirty(_expertCertificateAvailable, expertCertificateAvailable);
        _expertCertificateAvailable = expertCertificateAvailable;
    }

    /**
     * @return Сертификат специалиста.
     */
    public DatabaseFile getExpertCertificate()
    {
        return _expertCertificate;
    }

    /**
     * @param expertCertificate Сертификат специалиста.
     */
    public void setExpertCertificate(DatabaseFile expertCertificate)
    {
        dirty(_expertCertificate, expertCertificate);
        _expertCertificate = expertCertificate;
    }

    /**
     * @return Имя файла Сертификат специалиста.
     */
    @Length(max=255)
    public String getCertificateFileName()
    {
        return _certificateFileName;
    }

    /**
     * @param certificateFileName Имя файла Сертификат специалиста.
     */
    public void setCertificateFileName(String certificateFileName)
    {
        dirty(_certificateFileName, certificateFileName);
        _certificateFileName = certificateFileName;
    }

    /**
     * @return Тип файла Сертификат специалиста.
     */
    @Length(max=255)
    public String getCertificateFileType()
    {
        return _certificateFileType;
    }

    /**
     * @param certificateFileType Тип файла Сертификат специалиста.
     */
    public void setCertificateFileType(String certificateFileType)
    {
        dirty(_certificateFileType, certificateFileType);
        _certificateFileType = certificateFileType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeMedicalSpecialityGen)
        {
            setEmployee(((EmployeeMedicalSpeciality)another).getEmployee());
            setMedicalEducationLevel(((EmployeeMedicalSpeciality)another).getMedicalEducationLevel());
            setMedicalQualificationCategory(((EmployeeMedicalSpeciality)another).getMedicalQualificationCategory());
            setAssignDate(((EmployeeMedicalSpeciality)another).getAssignDate());
            setAcceptionDate(((EmployeeMedicalSpeciality)another).getAcceptionDate());
            setCategoryAssignDate(((EmployeeMedicalSpeciality)another).getCategoryAssignDate());
            setCategoryAcceptionDate(((EmployeeMedicalSpeciality)another).getCategoryAcceptionDate());
            setExpertCertificateAvailable(((EmployeeMedicalSpeciality)another).getExpertCertificateAvailable());
            setExpertCertificate(((EmployeeMedicalSpeciality)another).getExpertCertificate());
            setCertificateFileName(((EmployeeMedicalSpeciality)another).getCertificateFileName());
            setCertificateFileType(((EmployeeMedicalSpeciality)another).getCertificateFileType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeMedicalSpecialityGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeMedicalSpeciality.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeMedicalSpeciality();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employee":
                    return obj.getEmployee();
                case "medicalEducationLevel":
                    return obj.getMedicalEducationLevel();
                case "medicalQualificationCategory":
                    return obj.getMedicalQualificationCategory();
                case "assignDate":
                    return obj.getAssignDate();
                case "acceptionDate":
                    return obj.getAcceptionDate();
                case "categoryAssignDate":
                    return obj.getCategoryAssignDate();
                case "categoryAcceptionDate":
                    return obj.getCategoryAcceptionDate();
                case "expertCertificateAvailable":
                    return obj.getExpertCertificateAvailable();
                case "expertCertificate":
                    return obj.getExpertCertificate();
                case "certificateFileName":
                    return obj.getCertificateFileName();
                case "certificateFileType":
                    return obj.getCertificateFileType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
                case "medicalEducationLevel":
                    obj.setMedicalEducationLevel((MedicalEducationLevel) value);
                    return;
                case "medicalQualificationCategory":
                    obj.setMedicalQualificationCategory((MedicalQualificationCategory) value);
                    return;
                case "assignDate":
                    obj.setAssignDate((Date) value);
                    return;
                case "acceptionDate":
                    obj.setAcceptionDate((Date) value);
                    return;
                case "categoryAssignDate":
                    obj.setCategoryAssignDate((Date) value);
                    return;
                case "categoryAcceptionDate":
                    obj.setCategoryAcceptionDate((Date) value);
                    return;
                case "expertCertificateAvailable":
                    obj.setExpertCertificateAvailable((Boolean) value);
                    return;
                case "expertCertificate":
                    obj.setExpertCertificate((DatabaseFile) value);
                    return;
                case "certificateFileName":
                    obj.setCertificateFileName((String) value);
                    return;
                case "certificateFileType":
                    obj.setCertificateFileType((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employee":
                        return true;
                case "medicalEducationLevel":
                        return true;
                case "medicalQualificationCategory":
                        return true;
                case "assignDate":
                        return true;
                case "acceptionDate":
                        return true;
                case "categoryAssignDate":
                        return true;
                case "categoryAcceptionDate":
                        return true;
                case "expertCertificateAvailable":
                        return true;
                case "expertCertificate":
                        return true;
                case "certificateFileName":
                        return true;
                case "certificateFileType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employee":
                    return true;
                case "medicalEducationLevel":
                    return true;
                case "medicalQualificationCategory":
                    return true;
                case "assignDate":
                    return true;
                case "acceptionDate":
                    return true;
                case "categoryAssignDate":
                    return true;
                case "categoryAcceptionDate":
                    return true;
                case "expertCertificateAvailable":
                    return true;
                case "expertCertificate":
                    return true;
                case "certificateFileName":
                    return true;
                case "certificateFileType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employee":
                    return Employee.class;
                case "medicalEducationLevel":
                    return MedicalEducationLevel.class;
                case "medicalQualificationCategory":
                    return MedicalQualificationCategory.class;
                case "assignDate":
                    return Date.class;
                case "acceptionDate":
                    return Date.class;
                case "categoryAssignDate":
                    return Date.class;
                case "categoryAcceptionDate":
                    return Date.class;
                case "expertCertificateAvailable":
                    return Boolean.class;
                case "expertCertificate":
                    return DatabaseFile.class;
                case "certificateFileName":
                    return String.class;
                case "certificateFileType":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeMedicalSpeciality> _dslPath = new Path<EmployeeMedicalSpeciality>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeMedicalSpeciality");
    }
            

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    /**
     * @return Врачебные специальности. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getMedicalEducationLevel()
     */
    public static MedicalEducationLevel.Path<MedicalEducationLevel> medicalEducationLevel()
    {
        return _dslPath.medicalEducationLevel();
    }

    /**
     * @return Квалификационные категории врачей. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getMedicalQualificationCategory()
     */
    public static MedicalQualificationCategory.Path<MedicalQualificationCategory> medicalQualificationCategory()
    {
        return _dslPath.medicalQualificationCategory();
    }

    /**
     * @return Дата присуждения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getAssignDate()
     */
    public static PropertyPath<Date> assignDate()
    {
        return _dslPath.assignDate();
    }

    /**
     * @return Дата подтверждения.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getAcceptionDate()
     */
    public static PropertyPath<Date> acceptionDate()
    {
        return _dslPath.acceptionDate();
    }

    /**
     * @return Дата присуждения категории.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getCategoryAssignDate()
     */
    public static PropertyPath<Date> categoryAssignDate()
    {
        return _dslPath.categoryAssignDate();
    }

    /**
     * @return Дата подтверждения категории.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getCategoryAcceptionDate()
     */
    public static PropertyPath<Date> categoryAcceptionDate()
    {
        return _dslPath.categoryAcceptionDate();
    }

    /**
     * @return Имеется Сертификат специалиста.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getExpertCertificateAvailable()
     */
    public static PropertyPath<Boolean> expertCertificateAvailable()
    {
        return _dslPath.expertCertificateAvailable();
    }

    /**
     * @return Сертификат специалиста.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getExpertCertificate()
     */
    public static DatabaseFile.Path<DatabaseFile> expertCertificate()
    {
        return _dslPath.expertCertificate();
    }

    /**
     * @return Имя файла Сертификат специалиста.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getCertificateFileName()
     */
    public static PropertyPath<String> certificateFileName()
    {
        return _dslPath.certificateFileName();
    }

    /**
     * @return Тип файла Сертификат специалиста.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getCertificateFileType()
     */
    public static PropertyPath<String> certificateFileType()
    {
        return _dslPath.certificateFileType();
    }

    public static class Path<E extends EmployeeMedicalSpeciality> extends EntityPath<E>
    {
        private Employee.Path<Employee> _employee;
        private MedicalEducationLevel.Path<MedicalEducationLevel> _medicalEducationLevel;
        private MedicalQualificationCategory.Path<MedicalQualificationCategory> _medicalQualificationCategory;
        private PropertyPath<Date> _assignDate;
        private PropertyPath<Date> _acceptionDate;
        private PropertyPath<Date> _categoryAssignDate;
        private PropertyPath<Date> _categoryAcceptionDate;
        private PropertyPath<Boolean> _expertCertificateAvailable;
        private DatabaseFile.Path<DatabaseFile> _expertCertificate;
        private PropertyPath<String> _certificateFileName;
        private PropertyPath<String> _certificateFileType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

    /**
     * @return Врачебные специальности. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getMedicalEducationLevel()
     */
        public MedicalEducationLevel.Path<MedicalEducationLevel> medicalEducationLevel()
        {
            if(_medicalEducationLevel == null )
                _medicalEducationLevel = new MedicalEducationLevel.Path<MedicalEducationLevel>(L_MEDICAL_EDUCATION_LEVEL, this);
            return _medicalEducationLevel;
        }

    /**
     * @return Квалификационные категории врачей. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getMedicalQualificationCategory()
     */
        public MedicalQualificationCategory.Path<MedicalQualificationCategory> medicalQualificationCategory()
        {
            if(_medicalQualificationCategory == null )
                _medicalQualificationCategory = new MedicalQualificationCategory.Path<MedicalQualificationCategory>(L_MEDICAL_QUALIFICATION_CATEGORY, this);
            return _medicalQualificationCategory;
        }

    /**
     * @return Дата присуждения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getAssignDate()
     */
        public PropertyPath<Date> assignDate()
        {
            if(_assignDate == null )
                _assignDate = new PropertyPath<Date>(EmployeeMedicalSpecialityGen.P_ASSIGN_DATE, this);
            return _assignDate;
        }

    /**
     * @return Дата подтверждения.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getAcceptionDate()
     */
        public PropertyPath<Date> acceptionDate()
        {
            if(_acceptionDate == null )
                _acceptionDate = new PropertyPath<Date>(EmployeeMedicalSpecialityGen.P_ACCEPTION_DATE, this);
            return _acceptionDate;
        }

    /**
     * @return Дата присуждения категории.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getCategoryAssignDate()
     */
        public PropertyPath<Date> categoryAssignDate()
        {
            if(_categoryAssignDate == null )
                _categoryAssignDate = new PropertyPath<Date>(EmployeeMedicalSpecialityGen.P_CATEGORY_ASSIGN_DATE, this);
            return _categoryAssignDate;
        }

    /**
     * @return Дата подтверждения категории.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getCategoryAcceptionDate()
     */
        public PropertyPath<Date> categoryAcceptionDate()
        {
            if(_categoryAcceptionDate == null )
                _categoryAcceptionDate = new PropertyPath<Date>(EmployeeMedicalSpecialityGen.P_CATEGORY_ACCEPTION_DATE, this);
            return _categoryAcceptionDate;
        }

    /**
     * @return Имеется Сертификат специалиста.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getExpertCertificateAvailable()
     */
        public PropertyPath<Boolean> expertCertificateAvailable()
        {
            if(_expertCertificateAvailable == null )
                _expertCertificateAvailable = new PropertyPath<Boolean>(EmployeeMedicalSpecialityGen.P_EXPERT_CERTIFICATE_AVAILABLE, this);
            return _expertCertificateAvailable;
        }

    /**
     * @return Сертификат специалиста.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getExpertCertificate()
     */
        public DatabaseFile.Path<DatabaseFile> expertCertificate()
        {
            if(_expertCertificate == null )
                _expertCertificate = new DatabaseFile.Path<DatabaseFile>(L_EXPERT_CERTIFICATE, this);
            return _expertCertificate;
        }

    /**
     * @return Имя файла Сертификат специалиста.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getCertificateFileName()
     */
        public PropertyPath<String> certificateFileName()
        {
            if(_certificateFileName == null )
                _certificateFileName = new PropertyPath<String>(EmployeeMedicalSpecialityGen.P_CERTIFICATE_FILE_NAME, this);
            return _certificateFileName;
        }

    /**
     * @return Тип файла Сертификат специалиста.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality#getCertificateFileType()
     */
        public PropertyPath<String> certificateFileType()
        {
            if(_certificateFileType == null )
                _certificateFileType = new PropertyPath<String>(EmployeeMedicalSpecialityGen.P_CERTIFICATE_FILE_TYPE, this);
            return _certificateFileType;
        }

        public Class getEntityClass()
        {
            return EmployeeMedicalSpeciality.class;
        }

        public String getEntityName()
        {
            return "employeeMedicalSpeciality";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
