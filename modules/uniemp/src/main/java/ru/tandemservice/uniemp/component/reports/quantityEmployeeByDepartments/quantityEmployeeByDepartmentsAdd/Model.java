package ru.tandemservice.uniemp.component.reports.quantityEmployeeByDepartments.quantityEmployeeByDepartmentsAdd;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport;

public class Model
{
    private QuantityEmpByDepReport _report = new QuantityEmpByDepReport();

    private ISelectModel _orgUnitList;
    private ISelectModel _orgUnitTypeList;

    public QuantityEmpByDepReport getReport()
    {
        return _report;
    }

    public void setReport(QuantityEmpByDepReport report)
    {
        _report = report;
    }

    public ISelectModel getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(ISelectModel orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public ISelectModel getOrgUnitTypeList()
    {
        return _orgUnitTypeList;
    }

    public void setOrgUnitTypeList(ISelectModel orgUnitTypeList)
    {
        _orgUnitTypeList = orgUnitTypeList;
    }
}
