/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryArchAddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit.EmploymentHistoryAbstractDAO;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Create by ashaburov
 * Date 17.08.11
 */
public class DAO extends EmploymentHistoryAbstractDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        // если редактирование
        if (model.getEmploymentHistoryItem().getId() != null)
        {
            List<ServiceLengthType> serviceLengthTypeList = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")))
                    .where(eq(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("b")), value(model.getEmploymentHistoryItem())))
                    .createStatement(getSession()).list();

            model.setServiceLengthTypeList(serviceLengthTypeList);
        }
        else
        {
            model.setServiceLengthTypeList(Arrays.asList(getCatalogItem(ServiceLengthType.class, UniempDefines.SERVICE_LENGTH_TYPE_COMMON), getCatalogItem(ServiceLengthType.class, UniempDefines.SERVICE_LENGTH_TYPE_COMPANY)));
        }

        model.setServiceLengthTypeModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ServiceLengthType.class, "b").column("b")
                        .where(likeUpper(property(ServiceLengthType.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(ServiceLengthType.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(ServiceLengthType.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder);
            }
        });
    }

    @Override
    public void update(Model model)
    {
        if (model.getEmploymentHistoryItem().getId() != null)
        {
            new DQLDeleteBuilder(EmpHistoryToServiceLengthTypeRelation.class)
                    .where(eq(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase()), value(model.getEmploymentHistoryItem())))
                    .createStatement(getSession()).execute();
        }

        super.update(model);

        if (model.getServiceLengthTypeList() != null && !model.getServiceLengthTypeList().isEmpty())
        {
            for (ServiceLengthType type : model.getServiceLengthTypeList())
            {
                EmpHistoryToServiceLengthTypeRelation relation = new EmpHistoryToServiceLengthTypeRelation();
                relation.setEmploymentHistoryItemBase(model.getEmploymentHistoryItem());
                relation.setServiceLengthType(type);

                save(relation);
            }

            model.getEmploymentHistoryItem().setEmptyServiceLengthType(false);
            saveOrUpdate(model.getEmploymentHistoryItem());
        }
        else
        {
            model.getEmploymentHistoryItem().setEmptyServiceLengthType(true);
            saveOrUpdate(model.getEmploymentHistoryItem());
        }
    }
}
