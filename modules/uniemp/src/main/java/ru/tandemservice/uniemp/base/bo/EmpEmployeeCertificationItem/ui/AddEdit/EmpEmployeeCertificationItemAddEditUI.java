/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.EmpEmployeeCertificationItemManager;
import ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem;

/**
 * Create by ashaburov
 * Date 22.03.12
 */
@Input({
    @Bind(key = EmpEmployeeCertificationItemAddEditUI.EMPLOYEE_POST_ID, binding = EmpEmployeeCertificationItemAddEditUI.EMPLOYEE_POST_ID),
    @Bind(key = EmpEmployeeCertificationItemAddEditUI.ITEM_ID, binding = EmpEmployeeCertificationItemAddEditUI.ITEM_ID)
})
public class EmpEmployeeCertificationItemAddEditUI extends UIPresenter
{
    public static final String EMPLOYEE_POST_ID = "employeePostId";
    public static final String ITEM_ID = "itemId";

    private boolean _addForm;

    private Long _employeePostId;
    private Long _itemId;

    private EmployeePost _employeePost;
    private EmployeeCertificationItem _item;

    //Presenter methods

    @Override
    public void onComponentRefresh()
    {
        //заполняем поле Сотрудника
        setEmployeePost(DataAccessServices.dao().<EmployeePost>getNotNull(EmployeePost.class, getEmployeePostId()));

        //заполняем поле Факта аттестации
        if (getItemId() == null)
            setItem(new EmployeeCertificationItem());
        else
            setItem(DataAccessServices.dao().<EmployeeCertificationItem>getNotNull(EmployeeCertificationItem.class, getItemId()));

        if (getItemId() == null)
            setAddForm(true);
        else
            setAddForm(false);
    }


    //Listeners

    public void onClickApply()
    {
        getItem().setEmployeePost(getEmployeePost());

        EmpEmployeeCertificationItemManager.instance().dao().saveOrUpdateItem(getItem());
        deactivate();
    }

    //Getters & Setters

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public Long getItemId()
    {
        return _itemId;
    }

    public void setItemId(Long itemId)
    {
        _itemId = itemId;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public EmployeeCertificationItem getItem()
    {
        return _item;
    }

    public void setItem(EmployeeCertificationItem item)
    {
        _item = item;
    }
}
