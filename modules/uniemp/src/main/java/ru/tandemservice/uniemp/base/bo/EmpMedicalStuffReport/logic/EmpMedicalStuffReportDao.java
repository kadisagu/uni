/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.logic;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;
import ru.tandemservice.uniemp.entity.catalog.MedicalEducationLevel;
import ru.tandemservice.uniemp.entity.catalog.codes.MedicalQualificationCategoryCodes;
import ru.tandemservice.uniemp.entity.employee.EmployeeMedicalSpeciality;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
public class EmpMedicalStuffReportDao extends UniBaseDao implements IEmpMedicalStuffReportDao
{
    @Override
    public List<SummaryDataWrapper<EmployeeMedicalSpeciality>> getReportWrapperList(Date reportDate,
                                                                                    List<PostType> postTypeList,
                                                                                    List<MedicalEducationLevel> medicalEducationLevelList,
                                                                                    List<EmployeeType> employeeTypeList,
                                                                                    List<EduLevel> eduLevelList)
    {
        // поднимем данные из базы, с учетом фильтров, если они указаны
        DQLSelectBuilder exists = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column(property(EmployeePost.id().fromAlias("ep")))
                .where(eq(property(EmployeePost.employee().id().fromAlias("ep")), property(EmployeeMedicalSpeciality.employee().id().fromAlias("m"))))
                .where(ne(property(EmployeePost.postStatus().code().fromAlias("ep")), value(EmployeePostStatusCodes.STATUS_POSSIBLE)))
                .where(le(property(EmployeePost.postDate().fromAlias("ep")), value(reportDate, PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeePost.dismissalDate().fromAlias("ep"))),
                        ge(property(EmployeePost.dismissalDate().fromAlias("ep")), value(reportDate, PropertyType.DATE))));

        if (employeeTypeList != null && !employeeTypeList.isEmpty())
            exists.where(in(property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().fromAlias("ep")), employeeTypeList));


        // присоединяем новые документы об образовании и фильтруем по ним
        if (eduLevelList != null && !eduLevelList.isEmpty()) {
            exists
                    .joinEntity("ep", DQLJoinType.inner, PersonEduDocument.class, "ed", eq(property(EmployeePost.person().fromAlias("ep")), property(PersonEduDocument.person().fromAlias("ed"))))
                    .where(in(property(PersonEduDocument.eduLevel().fromAlias("ed")), eduLevelList));
        }

        if (postTypeList != null && !postTypeList.isEmpty())
            exists.where(in(property(EmployeePost.postType().code().fromAlias("ep")), CommonBaseEntityUtil.getPropertiesList(postTypeList, PostType.code())));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeMedicalSpeciality.class, "m")
                .where(exists(exists.buildQuery()))
                .where(le(property(EmployeeMedicalSpeciality.assignDate().fromAlias("m")), value(reportDate, PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeeMedicalSpeciality.categoryAssignDate().fromAlias("m"))),
                        le(property(EmployeeMedicalSpeciality.categoryAssignDate().fromAlias("m")), value(reportDate, PropertyType.DATE))));

        if (medicalEducationLevelList != null && !medicalEducationLevelList.isEmpty())
            builder.where(in(property(EmployeeMedicalSpeciality.medicalEducationLevel().id().fromAlias("m")), UniBaseDao.ids(medicalEducationLevelList)));

        List<EmployeeMedicalSpeciality> medicalSpecialityList = builder.createStatement(getSession()).list();

        List<Long> skipEmployeeIdList = getIgnoreEmployeeIdList(medicalSpecialityList);
        Map<Employee, EmployeeMedicalSpeciality> employeeMedicalSpecialityMap = new HashMap<>();
        for (EmployeeMedicalSpeciality medicalSpeciality : medicalSpecialityList)
        {
            if (skipEmployeeIdList.contains(medicalSpeciality.getEmployee().getId()))
                continue;

            EmployeeMedicalSpeciality speciality = employeeMedicalSpecialityMap.get(medicalSpeciality.getEmployee());
            if (speciality == null)
            {
                employeeMedicalSpecialityMap.put(medicalSpeciality.getEmployee(), medicalSpeciality);
            }
            else
            {
                int compare = medicalSpeciality.getMedicalQualificationCategory().getCode().compareTo(speciality.getMedicalQualificationCategory().getCode());
                if (compare > 0)
                    employeeMedicalSpecialityMap.put(medicalSpeciality.getEmployee(), medicalSpeciality);
            }
        }

        Collection<EmployeeMedicalSpeciality> medicalSpecialityCollection = employeeMedicalSpecialityMap.values();

        // строим список строк(враперов) отчета
        List<SummaryDataWrapper<EmployeeMedicalSpeciality>> wrapperList = new ArrayList<>();
        DQLSelectBuilder medicalBuilder = new DQLSelectBuilder().fromEntity(MedicalEducationLevel.class, "b");
        if (medicalEducationLevelList != null && !medicalEducationLevelList.isEmpty())
            medicalBuilder.where(in(property(MedicalEducationLevel.id().fromAlias("b")), UniBaseDao.ids(medicalEducationLevelList)));
        medicalBuilder.order(property(MedicalEducationLevel.title().fromAlias("b")));

        // подготавливаем строки отчета по элементам справочника Врачебные специальности
        long id = 1;
        for (final MedicalEducationLevel medicalEducationLevel : medicalBuilder.createStatement(getSession()).<MedicalEducationLevel>list())
        {
            // создаем врапер по элементу справочника и прописываем логику колонки
            SummaryDataWrapper<EmployeeMedicalSpeciality> wrapper = new SummaryDataWrapper<>();
            wrapper.setId(id++);
            wrapper.setTitle(medicalEducationLevel.getTitle());
            wrapper.addIncrement("total", input -> medicalEducationLevel.getCode().equals(input.getMedicalEducationLevel().getCode()));
            wrapper.addIncrement("high", input -> {
                boolean eqCode = medicalEducationLevel.getCode().equals(input.getMedicalEducationLevel().getCode());
                boolean high = input.getMedicalQualificationCategory().getCode().equals(MedicalQualificationCategoryCodes.HIGHEST_CATEGORY);
                return eqCode && high;
            });
            wrapper.addIncrement("first", input -> {
                boolean eqCode = medicalEducationLevel.getCode().equals(input.getMedicalEducationLevel().getCode());
                boolean first = input.getMedicalQualificationCategory().getCode().equals(MedicalQualificationCategoryCodes.FIRST_CATEGORY);
                return eqCode && first;
            });
            wrapper.addIncrement("second", input -> {
                boolean eqCode = medicalEducationLevel.getCode().equals(input.getMedicalEducationLevel().getCode());
                boolean second = input.getMedicalQualificationCategory().getCode().equals(MedicalQualificationCategoryCodes.SECOND_CATEGORY);
                return eqCode && second;
            });
            wrapper.addIncrement("certificate", input -> {
                boolean eqCode = medicalEducationLevel.getCode().equals(input.getMedicalEducationLevel().getCode());
                return eqCode && input.getCertificateAvailable();
            });

            // прогоняем все врач. спец. сотрудников по враперу для заполнения значений в его колонках
            for (EmployeeMedicalSpeciality speciality : medicalSpecialityCollection)
                wrapper.inc(speciality);

            wrapperList.add(wrapper);
        }

        // Итоговая строка
        SummaryDataWrapper<EmployeeMedicalSpeciality> totalWrapper = new SummaryDataWrapper<>();
        totalWrapper.setId(id++);
        totalWrapper.setTitle("Итого");
        totalWrapper.addIncrement("total", input -> true);
        totalWrapper.addIncrement("high", input -> input.getMedicalQualificationCategory().getCode().equals(MedicalQualificationCategoryCodes.HIGHEST_CATEGORY));
        totalWrapper.addIncrement("first", input -> input.getMedicalQualificationCategory().getCode().equals(MedicalQualificationCategoryCodes.FIRST_CATEGORY));
        totalWrapper.addIncrement("second", input -> input.getMedicalQualificationCategory().getCode().equals(MedicalQualificationCategoryCodes.SECOND_CATEGORY));
        totalWrapper.addIncrement("certificate", EmployeeMedicalSpeciality::getCertificateAvailable);

        for (EmployeeMedicalSpeciality speciality : medicalSpecialityCollection)
            totalWrapper.inc(speciality);
        wrapperList.add(totalWrapper);

        // Итоговая строка для женщин
        SummaryDataWrapper<EmployeeMedicalSpeciality> totalWomenWrapper = new SummaryDataWrapper<>();
        totalWomenWrapper.setId(id);
        totalWomenWrapper.setTitle("Из них женщин");
        totalWomenWrapper.addIncrement("total", input -> !input.getEmployee().getPerson().isMale());
        totalWomenWrapper.addIncrement("high", input -> {
            boolean high = input.getMedicalQualificationCategory().getCode().equals(MedicalQualificationCategoryCodes.HIGHEST_CATEGORY);
            return high && !input.getEmployee().getPerson().isMale();
        });
        totalWomenWrapper.addIncrement("first", input -> {
            boolean first = input.getMedicalQualificationCategory().getCode().equals(MedicalQualificationCategoryCodes.FIRST_CATEGORY);
            return first && !input.getEmployee().getPerson().isMale();
        });
        totalWomenWrapper.addIncrement("second", input -> {
            boolean second = input.getMedicalQualificationCategory().getCode().equals(MedicalQualificationCategoryCodes.SECOND_CATEGORY);
            return second && !input.getEmployee().getPerson().isMale();
        });
        totalWomenWrapper.addIncrement("certificate", input -> input.getCertificateAvailable() && !input.getEmployee().getPerson().isMale());

        for (EmployeeMedicalSpeciality speciality : medicalSpecialityCollection)
            totalWomenWrapper.inc(speciality);
        wrapperList.add(totalWomenWrapper);

        return wrapperList;
    }

    @Override
    public List<Long> getIgnoreEmployeeList(Date reportDate,
                                            List<PostType> postTypeList,
                                            List<MedicalEducationLevel> medicalEducationLevelList,
                                            List<EmployeeType> employeeTypeList,
                                            List<EduLevel> eduLevelList)
    {
        DQLSelectBuilder empPostExists = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column(property(EmployeePost.id().fromAlias("ep")))
                .where(eq(property(EmployeePost.employee().id().fromAlias("ep")), property(EmployeeMedicalSpeciality.employee().id().fromAlias("m"))))
                .where(ne(property(EmployeePost.postStatus().code().fromAlias("ep")), value(EmployeePostStatusCodes.STATUS_POSSIBLE)))
                .where(le(property(EmployeePost.postDate().fromAlias("ep")), value(reportDate, PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeePost.dismissalDate().fromAlias("ep"))),
                        ge(property(EmployeePost.dismissalDate().fromAlias("ep")), value(reportDate, PropertyType.DATE))));

        if (postTypeList != null && !postTypeList.isEmpty())
            empPostExists.where(in(property(EmployeePost.postType().code().fromAlias("ep")), CommonBaseEntityUtil.getPropertiesList(postTypeList, PostType.code())));

        if (employeeTypeList != null && !employeeTypeList.isEmpty())
            empPostExists.where(in(property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().fromAlias("ep")), employeeTypeList));


        // присоединяем новые документы об образовании и фильтруем по ним
        if (eduLevelList != null && !eduLevelList.isEmpty()) {
            empPostExists
                    .joinEntity("ep", DQLJoinType.inner, PersonEduDocument.class, "ed", eq(property(EmployeePost.person().fromAlias("ep")), property(PersonEduDocument.person().fromAlias("ed"))))
                    .where(in(property(PersonEduDocument.eduLevel().fromAlias("ed")), eduLevelList));
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeMedicalSpeciality.class, "m").column(property("m"))
                .where(exists(empPostExists.buildQuery()))
                .where(le(property(EmployeeMedicalSpeciality.assignDate().fromAlias("m")), value(reportDate, PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeeMedicalSpeciality.categoryAssignDate().fromAlias("m"))),
                        le(property(EmployeeMedicalSpeciality.categoryAssignDate().fromAlias("m")), value(reportDate, PropertyType.DATE))));

        if (medicalEducationLevelList != null && !medicalEducationLevelList.isEmpty())
            builder.where(in(property(EmployeeMedicalSpeciality.medicalEducationLevel().id().fromAlias("m")), UniBaseDao.ids(medicalEducationLevelList)));

        return getIgnoreEmployeeIdList(builder.createStatement(getSession()).<EmployeeMedicalSpeciality>list());
    }

    protected List<Long> getIgnoreEmployeeIdList(List<EmployeeMedicalSpeciality> specialityList)
    {
        List<Long> ignoreEmployeeIdList = new ArrayList<>();
        Map<Employee, List<Integer>> employeeCategoryCodesMap = new HashMap<>();
        for (EmployeeMedicalSpeciality medicalSpeciality : specialityList)
        {
            List<Integer> codeList = employeeCategoryCodesMap.get(medicalSpeciality.getEmployee());
            if (codeList == null)
                employeeCategoryCodesMap.put(medicalSpeciality.getEmployee(), codeList = new ArrayList<>());
            codeList.add(Integer.valueOf(medicalSpeciality.getMedicalQualificationCategory().getCode()));
        }

        for (Map.Entry<Employee, List<Integer>> entry : employeeCategoryCodesMap.entrySet())
        {
            if (entry.getValue().size() > 1)
            {
                Integer maxCategoryCode = null;
                for (Integer code : entry.getValue())
                {
                    if (maxCategoryCode == null)
                        maxCategoryCode = code;

                    if (code > maxCategoryCode)
                        maxCategoryCode = code;
                }

                entry.getValue().remove(maxCategoryCode);
                for (Integer code : entry.getValue())
                {
                    if (maxCategoryCode.equals(code))
                    {
                        ignoreEmployeeIdList.add(entry.getKey().getId());
                        break;
                    }
                }
            }
        }

        return ignoreEmployeeIdList;
    }
}
