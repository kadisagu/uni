/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.financingSourceItem.FinancingSourceItemPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;

/**
 * @author AutoGenerator
 * Created on 15.12.2010 
 */
public class Controller extends DefaultCatalogPubController<FinancingSourceItem, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<FinancingSourceItem> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Источник финансирования", FinancingSourceItem.financingSource().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", FinancingSourceItem.orgUnit().fullTitle().s()).setClickable(false));
    }
}