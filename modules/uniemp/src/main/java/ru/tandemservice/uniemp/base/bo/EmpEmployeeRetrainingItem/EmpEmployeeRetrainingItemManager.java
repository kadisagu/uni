/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.logic.EmpEmployeeRetrainingItemDAO;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.logic.IEmpEmployeeRetrainingItemDAO;

/**
 * Create by ashaburov
 * Date 28.03.12
 */
@Configuration
public class EmpEmployeeRetrainingItemManager extends BusinessObjectManager
{
    public static EmpEmployeeRetrainingItemManager instance()
    {
        return instance(EmpEmployeeRetrainingItemManager.class);
    }

    @Bean
    public IEmpEmployeeRetrainingItemDAO dao()
    {
        return new EmpEmployeeRetrainingItemDAO();
    }
}
