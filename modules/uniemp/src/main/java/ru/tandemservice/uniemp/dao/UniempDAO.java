/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import org.tandemframework.shared.employeebase.base.entity.*;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.IHolidayDuration;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceDegreeType;
import org.tandemframework.shared.person.catalog.entity.ScienceStatusType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceLengthWrapper;
import ru.tandemservice.uniemp.entity.catalog.*;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPost;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPostType;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.math.BigDecimal;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author E. Grigoriev
 * @since 08.07.2008
 */
public class UniempDAO<Model> extends UniDao<Model> implements IUniempDAO<Model>
{
    @Override
    public List<StaffListAllocationItem> getEmployeePostAllocationItemList(EmployeePost employeePost)
    {
        if (employeePost.getId() == null)
            return Collections.emptyList();

        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), UniempDaoFacade.getStaffListDAO().getActiveStaffList(employeePost.getOrgUnit())));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().s(), employeePost));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.combination().s(), false));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.combinationPost().s(), null));

        return builder.getResultList(getSession());
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<Long, List<EmployeePostStaffRateItem>> getEmployeePostStaffRateItemMap()
    {
        Map<Long, List<EmployeePostStaffRateItem>> resultMap = new HashMap<>();

        for (EmployeePostStaffRateItem rel : (List<EmployeePostStaffRateItem>)getSession().createCriteria(EmployeePostStaffRateItem.class).list())
        {
            if (resultMap.containsKey(rel.getEmployeePost().getId()))
                resultMap.get(rel.getEmployeePost().getId()).add(rel);
            else
            {
                List<EmployeePostStaffRateItem> items = new ArrayList<>();
                items.add(rel);
                resultMap.put(rel.getEmployeePost().getId(), items);
            }
        }

        return resultMap;
    }

    @Override
    public List<EmployeePostStaffRateItem> getEmployeePostStaffRateItemList(EmployeePost employeePost)
    {
        if (employeePost.getId() == null)
            return Collections.emptyList();

        MQBuilder builder = new MQBuilder(EmployeePostStaffRateItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", EmployeePostStaffRateItem.employeePost().s(), employeePost));

        return builder.getResultList(getSession());
    }

    public static EmployeeCard getEmployeeCard(Employee employee, Session session)
    {
        Criteria criteria = session.createCriteria(EmployeeCard.class);
        criteria.add(Restrictions.eq(EmployeeCard.L_EMPLOYEE, employee));
        return (EmployeeCard) criteria.uniqueResult();
    }

    @Override
    public EmployeeCard updateEmployeeForEmployeeCard(Employee employee)
    {
        Criteria criteria = getSession().createCriteria(EmployeeCard.class);
        criteria.add(Restrictions.eq(EmployeeCard.L_EMPLOYEE, employee));
        EmployeeCard employeeCard = (EmployeeCard) criteria.uniqueResult();

        if (employeeCard == null)
        {
            employeeCard = new EmployeeCard();
            employeeCard.setEmployee(employee);
            employeeCard.setInvention(false);
            employeeCard.setScientificWork(false);
            save(employeeCard);
        }

        return employeeCard;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void deleteOrgUnitPostRelation(OrgUnitPostRelation rel)
    {
        Session session = getSession();

        for (OrgUnitPostToScStatusRelation item : (List<OrgUnitPostToScStatusRelation>) session.createCriteria(OrgUnitPostToScStatusRelation.class).add(Restrictions.eq(OrgUnitPostToScStatusRelation.L_ORG_UNIT_POST_RELATION, rel)).list())
            delete(item);

        for (OrgUnitPostToScDegreeRelation item : (List<OrgUnitPostToScDegreeRelation>) session.createCriteria(OrgUnitPostToScDegreeRelation.class).add(Restrictions.eq(OrgUnitPostToScDegreeRelation.L_ORG_UNIT_POST_RELATION, rel)).list())
            delete(item);

        for (OrgUnitPostToTypeRelation item : (List<OrgUnitPostToTypeRelation>) session.createCriteria(OrgUnitPostToTypeRelation.class).add(Restrictions.eq(OrgUnitPostToTypeRelation.L_ORG_UNIT_POST_RELATION, rel)).list())
            delete(item);

        delete(rel);
    }

    @Override
    public <T> List<T> getOrgUnitPostRelations(OrgUnitPostRelation parentRel, Class<T> relTypeClass, String catalogProperty)
    {
        MQBuilder builder = new MQBuilder(relTypeClass.getName(), "rel");
        builder.add(MQExpression.eq("rel", OrgUnitPostToTypeRelation.L_ORG_UNIT_POST_RELATION, parentRel));
        builder.addOrder("rel", catalogProperty + "." + ICatalogItem.CATALOG_ITEM_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public Double getStaffRateForOrgUnitPost(OrgUnitTypePostRelation orgUnitTypePostRelation, OrgUnit orgUnit, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        MQBuilder mqBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), UniempDaoFacade.getStaffListDAO().getActiveStaffList(orgUnit)));
        mqBuilder.add(MQExpression.notEq("b", StaffListAllocationItem.employeePost().s(), null));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().postRelation().postBoundedWithQGandQL().s(), orgUnitTypePostRelation.getPostBoundedWithQGandQL()));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().orgUnit().s(), orgUnit));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().postStatus().active().s(), true));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.financingSource().s(), financingSource));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.financingSourceItem().s(), financingSourceItem));

        List<StaffListAllocationItem> allocationItemList = mqBuilder.getResultList(getSession());

        Double result = 0.0d;
        for (StaffListAllocationItem item : allocationItemList)
            result += item.getStaffRate();

        return result;
    }

    @Override
    public Double getStaffRateForOrgUnitPost(OrgUnitTypePostRelation orgUnitTypePostRelation, OrgUnit orgUnit, FinancingSource financingSource)
    {
        MQBuilder mqBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), UniempDaoFacade.getStaffListDAO().getActiveStaffList(orgUnit)));
        mqBuilder.add(MQExpression.notEq("b", StaffListAllocationItem.employeePost().s(), null));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().postRelation().s(), orgUnitTypePostRelation));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().orgUnit().s(), orgUnit));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().postStatus().active().s(), true));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.financingSource().s(), financingSource));

        List<StaffListAllocationItem> allocationItemList = mqBuilder.getResultList(getSession());

        Double result = 0.0d;
        for (StaffListAllocationItem item : allocationItemList)
            result += item.getStaffRate();

        return result;
    }

    @Override
    public Double getStaffRateForOrgUnitPost(StaffList staffList, OrgUnitPostRelation orgUnitPostRelation, OrgUnit orgUnit, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        MQBuilder mqBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), staffList));

//        AbstractExpression notNullEmployee = MQExpression.isNotNull("b", StaffListAllocationItem.employeePost().s());
//        AbstractExpression activeEmployee = MQExpression.eq("b", StaffListAllocationItem.employeePost().postStatus().active().s(), Boolean.TRUE);
//        AbstractExpression employeeExpression = MQExpression.and(notNullEmployee, activeEmployee);
//
//        AbstractExpression nullEmployee = MQExpression.isNull("b", StaffListAllocationItem.employeePost().s());
//        AbstractExpression reserveAllocItem = MQExpression.eq("b", StaffListAllocationItem.reserve().s(), Boolean.TRUE);
//        AbstractExpression reserveExpression = MQExpression.and(nullEmployee, reserveAllocItem);
//
//        mqBuilder.add(MQExpression.or(employeeExpression, reserveAllocItem));

        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().orgUnitPostRelation().s(), orgUnitPostRelation));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnit().s(), orgUnit));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.financingSource().s(), financingSource));
        mqBuilder.add(MQExpression.eq("b", StaffListAllocationItem.financingSourceItem().s(), financingSourceItem));
        AbstractExpression notNull = MQExpression.isNotNull("b", StaffListAllocationItem.employeePost().s());
        AbstractExpression active = MQExpression.eq("b", StaffListAllocationItem.employeePost().postStatus().active().s(), true);
        AbstractExpression reserve = MQExpression.eq("b", StaffListAllocationItem.reserve().s(), true);
        mqBuilder.add(MQExpression.or(reserve, MQExpression.and(notNull, active)));

        List<StaffListAllocationItem> allocationItemList = mqBuilder.getResultList(getSession());

        Double result = 0.0d;
        for (StaffListAllocationItem item : allocationItemList)
            result += item.getStaffRate();

        return result;
    }

    @Override
    public void validateStaffRates(ErrorCollector errCollector, Employee employee, EmployeePost employeePost, List<EmployeePostStaffRateItem> staffRateItemList, boolean isNew, boolean hasFiledsToCollectErrors)
    {
        if(!employeePost.getPostStatus().isActive()) return;

        Double summStaffRate = 0d;
        List<String> fields = new ArrayList<>();//список id полей Ставка для коллектора ошибок
        for (EmployeePostStaffRateItem item : staffRateItemList)
        {
            summStaffRate += item.getStaffRate();

            fields.add("staffRate_id_" + item.getId());
        }

        IDataSettings settings = DataSettingsFacade.getSettings("general", UniempDefines.STAFF_RATE_STEP_SETTINGS_PREFIX);
        Double minStaffRate = null != settings.get(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME) ? (Double)settings.get(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME) : 0d;

        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", EmployeePost.L_EMPLOYEE, employee));
        if (!isNew) builder.add(MQExpression.notEq("p", EmployeePost.P_ID, employeePost.getId()));
        builder.add(MQExpression.eq("p", EmployeePost.L_POST_TYPE, getCatalogItem(PostType.class, UniDefines.POST_TYPE_MAIN_JOB)));
        builder.add(MQExpression.eq("p", EmployeePost.L_POST_STATUS + "." + EmployeePostStatus.P_ACTIVE, Boolean.TRUE));

        List<EmployeePost> list = builder.getResultList(getSession());
        if (list.size() > 0 && UniDefines.POST_TYPE_MAIN_JOB.equals(employeePost.getPostType().getCode()))
            addError(errCollector, "Кадровый ресурс может иметь только одну основную активную должность.", hasFiledsToCollectErrors ? new String[]{"postType"}:null);
        else if (summStaffRate > 0.5
                && (UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(employeePost.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(employeePost.getPostType().getCode())))
            addError(errCollector, "Суммарная доля ставки по совместительству не может быть больше 0,5.", hasFiledsToCollectErrors ? fields.toArray(new String[fields.size()]) :null);

        if (summStaffRate.equals(0d))
            addError(errCollector, "Суммарная доля ставки не может быть нулевой.", hasFiledsToCollectErrors ? fields.toArray(new String[fields.size()]) : null);
        else
        {
            if (summStaffRate > 1)
                addError(errCollector, "Суммарная доля ставки не должна превышать 1.", hasFiledsToCollectErrors ? fields.toArray(new String[fields.size()]) : null);

            for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
            {
                Double reminder = Math.round((staffRateItem.getStaffRate() % minStaffRate) * 100) / 100d;
                if (reminder != 0 && !minStaffRate.equals(reminder))
                    addError(errCollector, "Доля ставки должна быть кратна " + minStaffRate + ".", hasFiledsToCollectErrors ? "staffRate_id_" + staffRateItem.getId() : null);
            }
        }
    }

    private void addError(ErrorCollector errCollector, String errMessage, String ... fieldIds)
    {
        if(null != errCollector)
            if(fieldIds == null)
                errCollector.add(errMessage);
            else
                errCollector.add(errMessage, fieldIds);
        else
            throw new ApplicationException(errMessage);
    }

    protected OrderDirection getOrderDirection(DynamicListDataSource<?> dataSource)
    {
        return (dataSource.getEntityOrder() != null) ? dataSource.getEntityOrder().getDirection() : OrderDirection.desc;
    }

    @Override
    public EmployeeLabourContract getEmployeePostLabourContract(EmployeePost employeePost)
    {
        Criteria crit = getSession().createCriteria(EmployeeLabourContract.class).add(Restrictions.eq(EmployeeLabourContract.L_EMPLOYEE_POST, employeePost));
        if (crit.list().size() == 1)
            return (EmployeeLabourContract)crit.uniqueResult();
        return null;
    }

    @Override
    public List<ContractCollateralAgreement> getContractCollateralAgreementsList(EmployeeLabourContract contract)
    {
        return getList(ContractCollateralAgreement.class, ContractCollateralAgreement.L_CONTRACT, contract);
    }

    @Override
    public EmployeeType getVPOReportLineEmployeeType(EmployeeVPO1ReportLines repLine)
    {
        MQBuilder builder = new MQBuilder(EmployeeVPO1RepLineToPostType.ENTITY_CLASS, "rel", new String[]{EmployeeVPO1RepLineToPostType.L_EMPLOYEE_TYPE});
        builder.add(MQExpression.eq("rel", EmployeeVPO1RepLineToPostType.L_EMPLOYEE_V_P_O1_REPORT_LINES, repLine));
        List<EmployeeType> result = builder.getResultList(getSession());
        if(!result.isEmpty()) return result.get(0);
        return null;
    }

    @Override
    public List<PostBoundedWithQGandQL> getVPOReportLinePosts(EmployeeVPO1ReportLines repLine)
    {
        MQBuilder builder = new MQBuilder(EmployeeVPO1RepLineToPost.ENTITY_CLASS, "rel", new String[] {EmployeeVPO1RepLineToPost.L_POST_BOUNDED_WITH_Q_GAND_Q_L});
        builder.add(MQExpression.eq("rel", EmployeeVPO1RepLineToPost.L_EMPLOYEE_V_P_O1_REPORT_LINES, repLine));
        builder.addOrder("rel", EmployeeVPO1RepLineToPost.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public double getPaymentBaseValueForPost(Payment payment, PostBoundedWithQGandQL post)
    {
        if(null == payment) return 0d;
        if (null == post) return null != payment.getValue() ? payment.getValue() : 0d;

        MQBuilder builder = new MQBuilder(PaymentToPostBaseValue.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", PaymentToPostBaseValue.L_PAYMENT, payment));
        builder.add(MQExpression.eq("rel", PaymentToPostBaseValue.L_POST_BOUNDED_WITH_Q_GAND_Q_L, post));
        if(builder.getResultCount(getSession()) == 0) return null != payment.getValue() ? payment.getValue() : 0d;
        else return ((PaymentToPostBaseValue)builder.uniqueResult(getSession())).getValue();
    }

    @Override
    public PersonAcademicDegree getEmployeeUpperAcademicDegree(Employee employee)
    {
        MQBuilder builder = new MQBuilder(PersonAcademicDegree.ENTITY_CLASS, "ead");
        builder.add(MQExpression.eq("ead", PersonAcademicDegree.person(), employee.getPerson()));
        builder.addLeftJoin("ead", PersonAcademicDegree.academicDegree().type().s(), "sdt");
        builder.addOrder("sdt", ScienceDegreeType.level().s(), OrderDirection.desc);
        if (builder.getResultCount(getSession()) > 0) return builder.<PersonAcademicDegree>getResultList(getSession()).get(0);
        else return null;
    }

    @Override
    public PersonAcademicStatus getEmployeeUpperAcademicStatus(Employee employee)
    {
        MQBuilder builder = new MQBuilder(PersonAcademicStatus.ENTITY_CLASS, "aes");
        builder.add(MQExpression.eq("aes", PersonAcademicStatus.person(), employee.getPerson()));
        builder.addLeftJoin("aes", PersonAcademicStatus.academicStatus().type().s(), "ast");
        builder.addOrder("ast", ScienceStatusType.level().s(), OrderDirection.desc);
        if (builder.getResultCount(getSession()) > 0) return builder.<PersonAcademicStatus>getResultList(getSession()).get(0);
        else return null;
    }

    @Override
    public List<PersonAcademicDegree> getEmployeeAcademicDegreeList(Employee employee)
    {
        MQBuilder builder = new MQBuilder(PersonAcademicDegree.ENTITY_CLASS, "ead");
        builder.add(MQExpression.eq("ead", PersonAcademicDegree.person(), employee.getPerson()));
        builder.addLeftJoin("ead", PersonAcademicDegree.academicDegree().type().s(), "sdt");
        builder.addOrder("sdt", ScienceDegreeType.level().s(), OrderDirection.desc);
        if (builder.getResultCount(getSession()) > 0) return builder.getResultList(getSession());
        else return null;
    }

    @Override
    public void deleteVacationSchedule(VacationSchedule vacationSchedule)
    {
        if (vacationSchedule.getState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACCEPTED))
            throw new ApplicationException("Нельзя удалить график отпусков в состоянии согласовано.");

        MQBuilder builder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi");
        builder.add(MQExpression.eq("vsi", VacationScheduleItem.vacationSchedule().s(), vacationSchedule));

        for (VacationScheduleItem item : builder.<VacationScheduleItem> getResultList(getSession()))
        {
            delete(item);
        }
        delete(vacationSchedule);
    }

    @Override
    public Map<Person, ScienceDegreeType> getEmployeeDegreesMap(Date reportDate)
    {
        Map<Person, ScienceDegreeType> resultMap = new HashMap<>();
        MQBuilder builder = new MQBuilder(PersonAcademicDegree.ENTITY_CLASS, "aed");
        builder.addLeftJoinFetch("aed", PersonAcademicStatus.person().s(), "p");
        if (null != reportDate)
            builder.add(MQExpression.lessOrEq("aed", PersonAcademicDegree.P_DATE, reportDate));
        List<PersonAcademicDegree> degreesList = builder.getResultList(getSession());

        for(PersonAcademicDegree degree : degreesList)
        {
            ScienceDegreeType type = resultMap.get(degree.getPerson());
            if(null == type || degree.getAcademicDegree().getType().getLevel() > type.getLevel())
                type = degree.getAcademicDegree().getType();
            resultMap.put(degree.getPerson(), type);
        }

        return resultMap;
    }

    @Override
    public Map<Person, ScienceStatusType> getEmployeeStatusesMap(Date reportDate)
    {
        Map<Person, ScienceStatusType> resultMap = new HashMap<>();
        MQBuilder builder = new MQBuilder(PersonAcademicStatus.ENTITY_CLASS, "aes");
        builder.addLeftJoinFetch("aes", PersonAcademicStatus.person().s(), "p");
        if (null != reportDate)
            builder.add(MQExpression.lessOrEq("aes", PersonAcademicStatus.P_DATE, reportDate));
        List<PersonAcademicStatus> statusesList = builder.getResultList(getSession());

        for (PersonAcademicStatus status : statusesList)
        {
            ScienceStatusType type = resultMap.get(status.getPerson());
            if ((null == type) || (status.getAcademicStatus().getType() != null && status.getAcademicStatus().getType().getLevel() > type.getLevel()))
            {
                type = status.getAcademicStatus().getType();
            }
            resultMap.put(status.getPerson(), type);
        }

        return resultMap;
    }

    @Override
    public Map<Person, List<ScienceDegreeType>> getEmployeeDegreesFullMap(Date reportDate)
    {
        Map<Person, List<ScienceDegreeType>> resultMap = new HashMap<>();
        MQBuilder builder = new MQBuilder(PersonAcademicDegree.ENTITY_CLASS, "aed");
        if (null != reportDate)
            builder.add(MQExpression.lessOrEq("aed", PersonAcademicDegree.P_DATE, reportDate));
        List<PersonAcademicDegree> degreesList = builder.getResultList(getSession());

        for(PersonAcademicDegree degree : degreesList)
        {
            ScienceDegreeType type = degree.getAcademicDegree().getType();
            List<ScienceDegreeType> fullType = resultMap.get(degree.getPerson());
            if(null == fullType) fullType = new ArrayList<>();
            List<ScienceDegreeType> resultFullType = new ArrayList<>();
            resultFullType.addAll(fullType);

            if(!fullType.contains(type)) resultFullType.add(type);
            else for(ScienceDegreeType item : fullType)
                if(type.getLevel() > item.getLevel())
                    resultFullType.add(resultFullType.indexOf(item), type);
            resultMap.put(degree.getPerson(), resultFullType);
        }

        return resultMap;
    }

    @Override
    public Map<Person, List<ScienceStatusType>> getEmployeeStatusesFullMap(Date reportDate)
    {
        Map<Person, List<ScienceStatusType>> resultMap = new HashMap<>();
        MQBuilder builder = new MQBuilder(PersonAcademicStatus.ENTITY_CLASS, "aes");
        if (null != reportDate)
            builder.add(MQExpression.lessOrEq("aes", PersonAcademicStatus.P_DATE, reportDate));
        List<PersonAcademicStatus> statusesList = builder.getResultList(getSession());

        for(PersonAcademicStatus status : statusesList)
        {
            ScienceStatusType type = status.getAcademicStatus().getType();

            if (null != type)
            {
                List<ScienceStatusType> fullType = resultMap.get(status.getPerson());
                if (null == fullType) fullType = new ArrayList<>();
                List<ScienceStatusType> resultFullType = new ArrayList<>();
                resultFullType.addAll(fullType);

                if (!fullType.contains(type)) resultFullType.add(type);
                else for (ScienceStatusType item : fullType)
                    if (type.getLevel() > item.getLevel())
                        resultFullType.add(resultFullType.indexOf(item), type);
                resultMap.put(status.getPerson(), resultFullType);
            }
        }

        return resultMap;
    }

    @Override
    public void validateVacationScheduleItemHasIntersections(VacationScheduleItem vacationScheduleItem, ErrorCollector errorCollector, String errorObject, String planDateFieldName, String factDateFieldName, String daysAmountFieldName)
    {
        MQBuilder builder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi");
        builder.add(MQExpression.eq("vsi", VacationScheduleItem.employeePost().s(), vacationScheduleItem.getEmployeePost()));
        if (null != vacationScheduleItem.getId())
            builder.add(MQExpression.notEq("vsi", VacationScheduleItem.id().s(), vacationScheduleItem.getId()));
        List<VacationScheduleItem> vsItemsToAnalyzeList = builder.getResultList(getSession());

        for (VacationScheduleItem vsi : vsItemsToAnalyzeList)
        {
            Date d1Start = null != vsi.getFactDate() ? vsi.getFactDate() : vsi.getPlanDate();
            Date d2Start = null != vacationScheduleItem.getFactDate() ? vacationScheduleItem.getFactDate() : vacationScheduleItem.getPlanDate();
            if (d1Start == null || d2Start == null) continue;

            Date d1Finish = CoreDateUtils.add(d1Start, Calendar.DATE, vsi.getDaysAmount());
            Date d2Finish = CoreDateUtils.add(d2Start, Calendar.DATE, vacationScheduleItem.getDaysAmount());

            Long t1Start = d1Start.getTime();
            Long t2Start = d2Start.getTime();
            Long t1Finish = d1Finish.getTime();
            Long t2Finish = d2Finish.getTime();

            if ((t1Start >= t2Start && t1Start <= t2Finish) || (t1Finish >= t2Start && t1Finish <= t2Finish)
                    || (t2Start >= t1Start && t2Start <= t1Finish) || (t2Finish >= t1Start && t2Finish <= t1Finish))
            {
                StringBuilder errorBuilder = new StringBuilder(errorObject);
                errorBuilder.append(" пересекается с планируемым отпуском из графика отпусков ");
                if (null != vsi.getVacationSchedule().getNumber())
                    errorBuilder.append(" № ").append(vsi.getVacationSchedule().getNumber());
                errorBuilder.append(" на ").append(vsi.getVacationSchedule().getYear()).append(" год от ");
                errorBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(vsi.getVacationSchedule().getCreateDate()));
                errorBuilder.append(" - ").append(vsi.getVacationSchedule().getOrgUnit().getFullTitle());
                errorBuilder.append(".");

                errorCollector.add(errorBuilder.toString(), daysAmountFieldName, null != vacationScheduleItem.getFactDate() ? factDateFieldName : planDateFieldName);
            }
        }
    }

    @Override
    public List<Payment> getPaymentOnFOTToPaymentsList(final Payment payment)
    {
        final MQBuilder builder = new MQBuilder(PaymentOnFOTToPaymentsRelation.ENTITY_CLASS, "rel", new String[] {IEntityRelation.L_SECOND});
        builder.add(MQExpression.eq("rel", IEntityRelation.L_FIRST, payment));
        builder.addOrder("rel", IEntityRelation.L_SECOND + "." + Payment.title().s());
        return builder.getResultList(getSession());
    }

    @Override
    public List<FinancingSourceItem> getFinSrcItm(Long finSrcId, String filter)
    {
        MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", FinancingSourceItem.financingSource().id().s(), finSrcId));
        builder.add(MQExpression.like("b", FinancingSourceItem.P_TITLE, CoreStringUtils.escapeLike(filter)));
        return builder.getResultList(getSession());
    }

    @Override
    public boolean isIndustrialCalendarHolidayDay(EmployeeWorkWeekDuration weekDuration, Date date)
    {
        GregorianCalendar dayDate = (GregorianCalendar)GregorianCalendar.getInstance();

        dayDate.setTime(date);

        MQBuilder calendarBuilder = new MQBuilder(IndustrialCalendar.ENTITY_CLASS, "ic");
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.P_YEAR, dayDate.get(Calendar.YEAR)));
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.L_WEEK_DURATION, weekDuration));
        IndustrialCalendar calendar = (IndustrialCalendar)calendarBuilder.uniqueResult(getSession());
        if (null == calendar)
            return false;

        MQBuilder builder = new MQBuilder(IndustrialCalendarHoliday.ENTITY_CLASS, "ich");
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_CALENDAR, calendar));
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_HOLIDAY + "." + Holiday.P_PREHOLIDAY, Boolean.FALSE));
        List<IHolidayDuration> holidays = builder.getResultList(getSession());

        builder = new MQBuilder(IndustrialCalendarHolidayDuration.ENTITY_CLASS, "ichd");
        builder.add(MQExpression.in("ichd", IndustrialCalendarHolidayDuration.L_HOLIDAY, holidays));
        builder.add(MQExpression.greatOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        builder.add(MQExpression.lessOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        holidays.addAll(builder.<IHolidayDuration>getResultList(getSession()));

        for(IHolidayDuration holiday : holidays)
        {
            if (holiday.getMonth() != (dayDate.get(Calendar.MONTH) + 1))
                continue;

            int begin = holiday.getBeginDay();
            int end = null == holiday.getEndDay() ? begin : holiday.getEndDay();

            if (holiday.getMonth() == dayDate.get(Calendar.MONTH) + 1 &&
                    dayDate.get(Calendar.DAY_OF_MONTH) <= end &&
                    dayDate.get(Calendar.DAY_OF_MONTH) >= begin)
                return true;
        }
        return false;
    }

    @Override
    public List<EmployeePayment> getEmployeePostPaymentList(EmployeePost employeePost)
    {
        MQBuilder builder = new MQBuilder(EmployeePayment.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", EmployeePayment.employeePost().s(), employeePost));

        return builder.getResultList(getSession());
    }

    @Override
    public EmployeePost getMainJob(Employee employee)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.add(MQExpression.eq("ep", EmployeePost.employee().s(), employee));
        builder.add(MQExpression.eq("ep", EmployeePost.postStatus().active().s(), Boolean.TRUE));
        builder.add(MQExpression.eq("ep", EmployeePost.mainJob().s(), Boolean.TRUE));
        if (builder.getResultCount(getSession()) == 1)
            return builder.<EmployeePost> getResultList(getSession()).get(0);
        else
            return null;
    }

    @Override
    public List<EmployeePost> getEmployeeSecondJobList(Employee employee, String postType)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", EmployeePost.employee().s(), employee));
        builder.add(MQExpression.eq("s", EmployeePost.postType().code().s(), postType));
        builder.add(MQExpression.eq("s", EmployeePost.postStatus().active().s(), Boolean.TRUE));
        builder.addOrder("s", EmployeePost.postRelation().postBoundedWithQGandQL().title().s());
        return builder.getResultList(getSession());
    }

    @Override
    public List<CombinationPostStaffRateItem> getCombinationPostStaffRateItemList(CombinationPost post)
    {
        MQBuilder builder = new MQBuilder(CombinationPostStaffRateItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", CombinationPostStaffRateItem.combinationPost().s(), post));

        return builder.getResultList(getSession());
    }

    @Override
    public List<OrgUnitTypePostRelation> getPostRelationList(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, String filter, Integer takeTopElementsCount)
    {
        MQBuilder subBuilder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep", new String[]{EmployeePost.L_POST_RELATION + ".id"});
        subBuilder.add(MQExpression.eq("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.P_MULTI_POST, Boolean.FALSE));
        subBuilder.add(MQExpression.eq("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_ORG_UNIT_TYPE, orgUnit.getOrgUnitType()));
        subBuilder.add(MQExpression.eq("ep", EmployeePost.L_POST_STATUS + "." + EmployeePostStatus.P_ACTIVE, Boolean.TRUE));
        subBuilder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT, orgUnit));
        if (null != postBoundedWithQGandQL)
            subBuilder.add(MQExpression.notEq("ep", EmployeePost.postRelation().postBoundedWithQGandQL().s(), postBoundedWithQGandQL));

        MQBuilder builder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "rel");
        builder.add(MQExpression.notIn("rel", OrgUnitTypePostRelation.P_ID, subBuilder));
        builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_ORG_UNIT_TYPE, orgUnit.getOrgUnitType()));
        builder.add(MQExpression.like("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE, CoreStringUtils.escapeLike(filter)));
        builder.addOrder("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE);
        if (null != takeTopElementsCount) return builder.getResultList(getSession(), 0, takeTopElementsCount);
        return builder.getResultList(getSession());
    }

    @Override
    public void validateStaffListRate(ErrorCollector errCollector, EmployeePost employeePost, List<EmployeePostStaffRateItem> staffRateItemList, boolean hasFiledsToCollectErrors)

    {
        for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
        {
            StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getActiveStaffListItemList(employeePost.getPostRelation(), employeePost.getOrgUnit(), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());
            //если у сотрудника меняется ставка, то его предыдущюю ставку нужно вычесть из занятых, что бы корректно посчитать превышение ставки
            //для этого берем его ставку в ШР на той же самой должности ШР и источниками финансирования
            //сотрудник должен быть в активном состоянии, т.к. если сотрудник был в неактивном состоянии, то его ставка уже исключена из занятых для данной должности
            //исключаем ставки по совмещению
            double allocationItemStaffRate = 0d;
            if (employeePost.getId() != null)
            {
                MQBuilder allocBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
                allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().s(), staffListItem));
                allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().s(), employeePost));
                allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().postStatus().active().s(), true));
                allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.combination().s(), false));
                allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.combinationPost().s(), null));
                allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.financingSource().s(), staffRateItem.getFinancingSource()));
                allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.financingSourceItem().s(), staffRateItem.getFinancingSourceItem()));

                for (StaffListAllocationItem item : allocBuilder.<StaffListAllocationItem>getResultList(getSession()))
                    allocationItemStaffRate += item.getStaffRate();

                BigDecimal x = new java.math.BigDecimal(allocationItemStaffRate);
                x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
                allocationItemStaffRate = x.doubleValue();
            }

            if (null != staffListItem)
            {
                Double occStaffRate = staffListItem.getOccStaffRate() + staffRateItem.getStaffRate() - allocationItemStaffRate;
                BigDecimal x = new java.math.BigDecimal(occStaffRate);
                x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
                occStaffRate = x.doubleValue();
                String finSrcTitle = staffRateItem.getFinancingSource().getTitle();
                String finSrcItmTitle = staffRateItem.getFinancingSourceItem() != null ? " (" + staffRateItem.getFinancingSourceItem().getTitle() + ")" : "";
                if (occStaffRate > staffListItem.getStaffRate())
                    addError(errCollector, "Невозможно назначить сотрудника на должность, так как превышено количество штатных единиц на подразделении " + '"' + employeePost.getOrgUnit().getTitleWithType() + '"' + " по должности " + '"' + employeePost.getPostRelation().getPostBoundedWithQGandQL().getFullTitle() + '"' + " - " + finSrcTitle + finSrcItmTitle + ".", hasFiledsToCollectErrors ? "staffRate_id_" + staffRateItem.getId() : null);
            }
        }
    }

    @Override
    public List<ServiceLengthType> getPostServiceLengthTypeList(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        return new DQLSelectBuilder().fromEntity(PostToServiceLengthTypeRelation.class, "b").column(property(PostToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")))
                .where(eq(property(PostToServiceLengthTypeRelation.postBoundedWithQGandQL().fromAlias("b")), value(postBoundedWithQGandQL)))
                .order(property(PostToServiceLengthTypeRelation.serviceLengthType().title().fromAlias("b")))
                .createStatement(getSession()).list();
    }

    @Override
    public List<ServiceLengthType> getEmploymentHistoryServiceLengthTypeList(EmploymentHistoryItemBase employmentHistoryItemBase)
    {
        return new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")))
                .where(eq(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("b")), value(employmentHistoryItemBase)))
                .order(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().title().fromAlias("b")))
                .createStatement(getSession()).list();
    }

    @Override
    public List<EmployeeServiceLengthWrapper> getEmployeeServiceLengthWrapperList(Employee employee, Long entityId)
    {
        DQLSelectBuilder historyBuilder = new DQLSelectBuilder().fromEntity(EmploymentHistoryItemBase.class, "e").column("e")
                .where(eqValue(property(EmploymentHistoryItemBase.employee().fromAlias("e")), employee));

        if (entityId != null)
            historyBuilder.where(ne(property(EmploymentHistoryItemBase.id().fromAlias("e")), entityId));

        List<EmploymentHistoryItemBase> historyItemBaseList = historyBuilder.createStatement(getSession()).list();

        List<EmpHistoryToServiceLengthTypeRelation> typeRelationList = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column("b")
                .where(in(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("b")), historyItemBaseList))
                .order(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().title().fromAlias("b")))
                .createStatement(getSession()).list();

        Map<EmploymentHistoryItemBase, List<ServiceLengthType>> historyItemRelationMap = new HashMap<>();

        for (EmpHistoryToServiceLengthTypeRelation relation : typeRelationList)
        {
            List<ServiceLengthType> list = historyItemRelationMap.get(relation.getEmploymentHistoryItemBase());
            if (list == null)
                historyItemRelationMap.put(relation.getEmploymentHistoryItemBase(), list = new ArrayList<>());
            list.add(relation.getServiceLengthType());
        }

        return UniempUtil.getEmployeeServiceLengthWrapperList(historyItemBaseList, historyItemRelationMap);
    }
}