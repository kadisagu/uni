/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp;

/**
 * @author E. Grigoriev
 * @since 04.07.2008
 */
public interface UniempDefines
{
    String STAFF_RATE_STEP_SETTINGS_PREFIX = "StaffRateStep_";
    String STAFF_RATE_STEP_POST_SETING_NAME = "post";

    String CATALOG_SCIENCE_BRANCH_CODE = "ScienceBranch";

    String CATALOG_SICK_LIST_BASICS_CODE = "SickListBasics";

    String CATALOG_STAFF_LIST_STATES = "StaffListState";
    String STAFF_LIST_STATUS_FORMING = "1";
    String STAFF_LIST_STATUS_ACCEPTING = "2";
    String STAFF_LIST_STATUS_ACCEPTED = "3";
    String STAFF_LIST_STATUS_REJECTED = "4";
    String STAFF_LIST_STATUS_ACTIVE = "5";
    String STAFF_LIST_STATUS_ARCHIVE = "6";

    String CATALOG_LABOUR_CONTRACT_TYPE = "LabourContractType";

    String CATALOG_PAYMENT_TYPE = "PaymentType";
    String PAYMENT_TYPE_COMPENSATION = "1";                   // Компенсационные выплаты
    String PAYMENT_TYPE_STLIMULATION = "2";                   // Стимулирующие выплаты
    String PAYMENT_TYPE_COMPENSATION_ADD_PAYMENT = "3";       // доплаты компенсационного характера
    String PAYMENT_TYPE_COMPENSATION_INCREMENT = "4";         // надбавки компенсационного характера
    String PAYMENT_TYPE_STLIMULATION_ADD_PAYMENT = "5";       // доплаты стимулирующего характера
    String PAYMENT_TYPE_STLIMULATION_INCREMENT = "6";         // адбавки стимулирующего характера
    String PAYMENT_TYPE_STLIMULATION_BONUS = "7";             // премии
    String PAYMENT_TYPE_STLIMULATION_PERFORMANCE_FEE = "8";   // поощрительные выплаты

    String CATALOG_PAYMENT_UNIT = "PaymentUnit";
    String PAYMENT_UNIT_RUBLES = "1";
    String PAYMENT_UNIT_BASE_PERCENT = "2";
    String PAYMENT_UNIT_COEFFICIENT = "3";
    String PAYMENT_UNIT_FULL_PERCENT = "4";

    String CATALOG_FINANCING_SOURCE = "FinancingSource";
    String FINANCING_SOURCE_BUDGET = "1";
    String FINANCING_SOURCE_OFF_BUDGET = "2";

    String TEMPLATE_EMPLOYEE_SAMPLE = "employeeSample";
    String TEMPLATE_PROF_LECT_CONSISTENCY = "profLectStaffConsistency";
    String TEMPLATE_QUALITY_CONSISTENCY = "qualityConsistency";
    String TEMPLATE_QUALITY_CONSISTENCY_FULL = "qualityConsistencyFull";
    String TEMPLATE_QUALITY_QUANTITY_CONSISTENCY_FULL = "qualityQuantityConsistencyFull";
    String TEMPLATE_PPS_ALLOCATION = "ppsAllocation";
    String TEMPLATE_EMPLOYEE_MOVE_PPS = "employeeMovePPS";
    String TEMPLATE_QUALITY_CONSISTENCY_FOR_LIST = "qualityConsistencyForList";
    String TEMPLATE_QUANTITY_EXTRACT_FORM_P4 = "quantityExtractFormP4";

    String TEMPLATE_PPS_STAFF_LIST = "ppsStaffList";
    String TEMPLATE_PPS_STAFF_LIST_ALLOCATION = "ppsStaffListAlloc";
    String TEMPLATE_VPO1 = "employeeVPO1Report";

    String TEMPLATE_QUANTITY_EMP_DEP = "quantityEmpByDep";

    String TEMPLATE_VACATION_SCHEDULE = "vacationSchedule";
    String TEMPLATE_VACATION_CERTIFICATE = "vacationCertificate";

    String CATALOG_HOLIDAY_TYPE = "HolidayType";
    String HOLIDAY_TYPE_ANNUAL = "1"; // Ежегодный отпуск
    String HOLIDAY_TYPE_WITHOUT_SALARY = "2"; //Отпуск без сохранения заработной платы
    String HOLIDAY_TYPE_PREGNANCY_AND_CHILD_BORN = "3"; //Отпуск по беременности и родам
    String HOLIDAY_TYPE_WOMAN_ADOPTED_A_CHILD = "4"; //Отпуск женщинам, усыновившим новорожденных детей непосредственно из родильного дома
    String HOLIDAY_TYPE_HARMFUL_CONDITIONS = "5"; //Дополнительный отпуск рабочим и служащим, занятым на работах с вредными условиями труда
    String HOLIDAY_TYPE_SOME_AREAS_OF_PUBLIC_CULTURE = "6"; //Дополнительный отпуск рабочим и служащим, занятым в отдельных отраслях народного хозяйства и имеющим продолжительный стаж работы на одном предприятии, в организации
    String HOLIDAY_TYPE_NON_NORMED_WORK_DAY = "7"; //Дополнительный отпуск работникам с ненормированным рабочим днем
    String HOLIDAY_TYPE_EDGE_NORTH = "8"; //Дополнительный отпуск рабочим и служащим, работающим в районах Крайнего Севера и приравненных местностях
    String HOLIDAY_TYPE_WORKING_AT_POLLUTED_REGION = "9"; //Дополнительный отпуск работающим на территориях в районах загрязнения от аварий на ЧАЭС
    String HOLIDAY_TYPE_CHERNOBYL_LIQUIDATION = "10"; //Дополнительный отпуск участникам ликвидации аварий на ЧАЭС и других радиационных аварий
    String HOLIDAY_TYPE_EVENING_EXAMS = "11"; //Отпуск для сдачи экзаменов в вечерних (сменных) общеобразовательных школах
    String HOLIDAY_TYPE_EVENING_STUDY = "12"; //Отпуск в связи с обучением в вечерних профессионально-технических училищах
    String HOLIDAY_TYPE_ENTRANCE_POST_GRADUATE_EXAMS = "13"; //Дополнительный отпуск для сдачи вступительных экзаменов в аспирантуру
    String HOLIDAY_TYPE_POST_GRADUATE = "14"; //Дополнительный ежегодный отпуск аспирантам
    String HOLIDAY_TYPE_ENTRANCE_EXAMS_WITHOUT_SALARY = "15"; //Отпуск без сохранения заработной платы для сдачи вступительных экзаменов в высшие и средние учреждения профессионального образования
    String HOLIDAY_TYPE_EVENING_AND_CORRESPONDENCE_STUDY = "16"; //Отпуск  в связи с обучением в вечерних и заочных высших и средних учреждениях профессионального образования
    String HOLIDAY_TYPE_GRADUATE_WORK_PREPARING = "17"; //Отпуск  для ознакомления с работой по избранной специальности и подготовки материалов к дипломному проекту
    String HOLIDAY_TYPE_CREATIVE = "19"; //Творческий отпуск
    String HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_1_5 = "20"; //Частично оплачиваемый отпуск женщинам, имеющим детей в возрасте до 1,5 лет
    String HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_3 = "21"; //Дополнительный отпуск без сохранения заработной платы матерям, имеющим детей в возрасте до 3 лет
    String HOLIDAY_TYPE_STATE_EMPLOYEE = "22"; //Дополнительный отпуск государственного служащего за стаж работы

    //владелец настроек модуля uniemp
    String UNIEMP_MODULE_OWNER = "uniempModuleOwner";

    //имя настройки в которой хранится признак используемости отчета
    String IS_REPORT_USED = "isReportUsed";

    String CATALOG_SERVICE_LENGTH_TYPE = "serviceLengthType";
    String SERVICE_LENGTH_TYPE_COMMON = "1";
    String SERVICE_LENGTH_TYPE_BRANCH = "2";
    String SERVICE_LENGTH_TYPE_COMPANY = "3";
    String SERVICE_LENGTH_TYPE_TUTOR = "4";
    String SERVICE_LENGTH_TYPE_EDUCATOR = "5";

    String MEDICAL_QUALIFICATION_CATEGORY_WITHOUT_CATEGORY = "0";

    String CATALOG_EMPLOYEE_EXTRACT_GROUP = "EmployeeExtractGroup";//Группы приказов по движению кадров
    String EMPLOYEE_EXTRACT_GROUP_HOLYDAY = "1";                   //Отпуск
    String EMPLOYEE_EXTRACT_GROUP_EMPLOYEE_ADD = "2";              //Назначение на должность, смена должности
    String EMPLOYEE_EXTRACT_GROUP_CHANGE_NAME = "3";               //Смена фамилии, имени, отчеств
    String EMPLOYEE_EXTRACT_GROUP_DISMISS = "4";                   //Увольнение
    String EMPLOYEE_EXTRACT_GROUP_PAYMENT = "5";                   //Установление доплат
    String EMPLOYEE_EXTRACT_GROUP_EMPLOYEE_ENCOURAGEMENT = "6";    //Поощрение работника
    String EMPLOYEE_EXTRACT_GROUP_COMBINATION = "7";               //Совмещение/ увеличение объема работ
    String EMPLOYEE_EXTRACT_GROUP_REPEAL = "8";                    //Отмена приказа
    String EMPLOYEE_EXTRACT_GROUP_ACTING = "9";                    //Исполнение обязанностей

}