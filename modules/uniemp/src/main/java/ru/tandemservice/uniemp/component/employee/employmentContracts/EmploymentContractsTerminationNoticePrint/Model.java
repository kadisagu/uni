/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsTerminationNoticePrint;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * Create by ashaburov
 * Date 19.08.11
 */
@Input(
        {
         @Bind(key="massPrint", binding = "massPrint"),
         @Bind(key = "employeeLabourContractList", binding = "employeeLabourContractList"),
         @Bind(key = "numberMap", binding = "numberMap"),
         @Bind(key = "dateMap", binding = "dateMap")
        }
)
public class Model
{
    private Boolean _massPrint;                                      //Параметр определеяет выбрана массовая печать или одиночная
    private List<EmployeeLabourContract> _employeeLabourContractList;//Список трудовых договоров (массовая печать)
    private Map<Long, String> _numberMap;                                //Мапа номеров уведомлений
    private Map<Long, Date> _dateMap;                                    //Мапа дат форм. уведомлений

    //Getters & Setters

    public Map<Long, String> getNumberMap()
    {
        return _numberMap;
    }

    public void setNumberMap(Map<Long, String> numberMap)
    {
        _numberMap = numberMap;
    }

    public Map<Long, Date> getDateMap()
    {
        return _dateMap;
    }

    public void setDateMap(Map<Long, Date> dateMap)
    {
        _dateMap = dateMap;
    }

    public Boolean getMassPrint()
    {
        return _massPrint;
    }

    public void setMassPrint(Boolean massPrint)
    {
        _massPrint = massPrint;
    }

    public List<EmployeeLabourContract> getEmployeeLabourContractList()
    {
        return _employeeLabourContractList;
    }

    public void setEmployeeLabourContractList(List<EmployeeLabourContract> employeeLabourContractList)
    {
        _employeeLabourContractList = employeeLabourContractList;
    }
}
