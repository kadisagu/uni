/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployee.ui.LabourContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexander Shaburov
 * @since 22.10.12
 */
@Configuration
public class EmpEmployeeLabourContract extends BusinessComponentManager
{
    // action buttons list
    public static final String ACTION_BUTTON_LIST = "employeeLabourContractActionButtonList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }

    @Bean
    public ButtonListExtPoint buttonListExtPoint()
    {
        return buttonListExtPointBuilder(ACTION_BUTTON_LIST)
                .addButton(submitButton("editContract", "onClickEditContract").visible("ui:hasLabourContract").permissionKey("editLabourContract_employeePost"))
                .addButton(submitButton("replaceContractFile", "onClickAddContractFile").visible("mvel:presenter.hasLabourContract && presenter.hasLabourContractFile").permissionKey("editLabourContract_employeePost"))
                .addButton(submitButton("printContractFile", "onPrintContractFile").visible("mvel:presenter.hasLabourContract && presenter.hasLabourContractFile").permissionKey("getLabourContract_employeePost"))
                .addButton(submitButton("deleteContractFile", "onClickDeleteContractFile").visible("mvel:presenter.hasLabourContract && presenter.hasLabourContractFile").permissionKey("deleteLabourContract_employeePost"))
                .addButton(submitButton("addContractFile", "onClickAddContractFile").visible("mvel:presenter.hasLabourContract && !presenter.hasLabourContractFile").permissionKey("editLabourContract_employeePost"))
                .addButton(submitButton("addContractAgreement", "onClickAddContractAgreement").visible("ui:hasLabourContract").permissionKey("addContractAgreement_employeePost"))
                .addButton(submitButton("addContract", "onClickAddContract").visible("mvel:!presenter.hasLabourContract").permissionKey("addLabourContract_employeePost"))
                .create();
    }
}
