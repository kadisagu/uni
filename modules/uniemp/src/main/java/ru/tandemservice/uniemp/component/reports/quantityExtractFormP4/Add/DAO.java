/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.quantityExtractFormP4.Add;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeWeekWorkLoadCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeWorkWeekDurationCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.util.UniempReportUtil;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 20.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final long DAY_DURATION = 86400000L;

    private static final List<String> HOLIDAY_TYPE_CODES_LIST = new ArrayList<>();
    static
    {
        HOLIDAY_TYPE_CODES_LIST.add("3");
        HOLIDAY_TYPE_CODES_LIST.add("4");
        HOLIDAY_TYPE_CODES_LIST.add("11");
        HOLIDAY_TYPE_CODES_LIST.add("12");
        HOLIDAY_TYPE_CODES_LIST.add("15");
        HOLIDAY_TYPE_CODES_LIST.add("20");
        HOLIDAY_TYPE_CODES_LIST.add("21");
    }

    @Override
    public Integer preparePrintReport(Model model)
    {
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("reportDate", model.getMonth().getTitle().toLowerCase() + " " + model.getYear().getTitle());
        if (null != model.getExecutorId())
        {
            EmployeePost executor = get(EmployeePost.class, model.getExecutorId());
            paramModifier.put("executiveEmployeeFIO", executor.getPerson().getFio());
        }
        else
        {
            paramModifier.put("executiveEmployeeFIO", "Администратор");
        }

        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(Calendar.YEAR, model.getYear().getId().intValue());
        cal.set(Calendar.MONTH, model.getMonth().getId().intValue() - 1);
        Date monthStartDate = cal.getTime();
        cal.add(Calendar.MONTH, 1);
        cal.add(Calendar.MILLISECOND, -1);
        Date monthEndDate = cal.getTime();

        cal.clear();
        cal.set(Calendar.YEAR, model.getYear().getId().intValue());
        Date yearStartDate = cal.getTime();
        cal.add(Calendar.YEAR, 1);
        cal.add(Calendar.MILLISECOND, -1);
        Date yearEndDate = cal.getTime();

        List<EmployeePost> postsList = getEmployeePostsList(monthStartDate, monthEndDate);
        Map<Long, List<EmployeeHoliday>> holidaysMap = getEmployeeHolidaysMap(monthEndDate);
        Map<Long, List<EmployeeHoliday>> holidaysWOSalaryMap = getEmployeeHolidaysWOSalaryMap(monthEndDate);

        Map<Long, List<EmployeePostStaffRateItem>> staffRatesMap = getEmployeePostsStaffRatesMap(monthStartDate, monthEndDate);

        double empl40x5MainCnt = 0;
        double empl36x5MainCnt = 0;
        double empl40x6MainCnt = 0;
        double empl36x6MainCnt = 0;
        double empl40x5SecondCnt = 0;
        double empl36x5SecondCnt = 0;
        double empl40x6SecondCnt = 0;
        double empl36x6SecondCnt = 0;

        int emplHolidayCnt = 0;
        double emplHolidayDurations = 0;

        int acceptedEmployeeCnt = 0;
        int dismissedEmployeeCnt = 0;
        int remainingEmployeeCnt = 0;

        for(EmployeePost post : postsList)
        {
            List<EmployeePostStaffRateItem> staffRateItems = staffRatesMap.get(post.getId());

            double staffRate = 0.0d;
            if(!(null == staffRateItems))
            {
                for (EmployeePostStaffRateItem item : staffRateItems)
                    staffRate += item.getStaffRate();
            }
            int monthEmploymentDays = Long.valueOf(((null != post.getDismissalDate() ? Math.min(post.getDismissalDate().getTime(), monthEndDate.getTime()) : monthEndDate.getTime()) - Math.max(post.getPostDate().getTime(), monthStartDate.getTime())) / DAY_DURATION).intValue();

            List<EmployeeHoliday> holidays = holidaysMap.get(post.getId());
            if(null != holidays)
            {
                for(EmployeeHoliday holiday : holidays)
                {
                    if(null != holiday.getStartDate() && holiday.getDuration() > 0)
                    {
                        cal.setTime(holiday.getStartDate());
                        cal.add(Calendar.DAY_OF_YEAR, holiday.getDuration());
                        Date holidayEndDate = cal.getTime();
                        if((holiday.getStartDate().getTime() >= monthStartDate.getTime() && holiday.getStartDate().getTime() < monthEndDate.getTime())
                                || (holidayEndDate.getTime() >= monthStartDate.getTime() && holidayEndDate.getTime() < monthEndDate.getTime())
                                || (holiday.getStartDate().getTime() < monthStartDate.getTime() && holidayEndDate.getTime() >= monthEndDate.getTime()))
                        {
                            long excludePeriodStart = Math.max(holiday.getStartDate().getTime(), monthStartDate.getTime());
                            long excludePeriodEnd = Math.min(holidayEndDate.getTime(), monthEndDate.getTime());
                            monthEmploymentDays -= (excludePeriodEnd - excludePeriodStart) / DAY_DURATION;
                        }
                    }
                }
            }

            List<EmployeeHoliday> holidaysWOSalary = holidaysWOSalaryMap.get(post.getId());
            if(null != holidaysWOSalary)
            {
                emplHolidayCnt++;
                for(EmployeeHoliday holiday : holidaysWOSalary)
                {
                    if(null != holiday.getStartDate() && holiday.getDuration() > 0)
                    {
                        cal.setTime(holiday.getStartDate());
                        cal.add(Calendar.DAY_OF_YEAR, holiday.getDuration());
                        Date holidayEndDate = cal.getTime();
                        if((holiday.getStartDate().getTime() >= monthStartDate.getTime() && holiday.getStartDate().getTime() < monthEndDate.getTime())
                                || (holidayEndDate.getTime() >= monthStartDate.getTime() && holidayEndDate.getTime() < monthEndDate.getTime())
                                || (holiday.getStartDate().getTime() < monthStartDate.getTime() && holidayEndDate.getTime() >= monthEndDate.getTime()))
                        {
                            long excludePeriodStart = Math.max(holiday.getStartDate().getTime(), monthStartDate.getTime());
                            long excludePeriodEnd = Math.min(holidayEndDate.getTime(), monthEndDate.getTime());
                            emplHolidayDurations += /*staffRate **/ (excludePeriodEnd - excludePeriodStart) / DAY_DURATION;
                        }
                    }
                }
            }

            if (EmployeeWorkWeekDurationCodes.WEEK_5.equals(post.getWorkWeekDuration().getCode()))
            {
                if (EmployeeWeekWorkLoadCodes.TITLE_40.equals(post.getWeekWorkLoad().getCode()))
                {
                    if (!UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode()))
                        empl40x5MainCnt += staffRate * monthEmploymentDays;
                    else
                        empl40x5SecondCnt += staffRate * monthEmploymentDays;

                }
                else if (post.getWeekWorkLoad().getHoursAmount() == 36d)
                {
                    if (!UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode()))
                        empl36x5MainCnt += staffRate * monthEmploymentDays;
                    else
                        empl36x5SecondCnt += staffRate * monthEmploymentDays;
                }
            }
            else if (EmployeeWorkWeekDurationCodes.WEEK_6.equals(post.getWorkWeekDuration().getCode()))
            {
                if (EmployeeWeekWorkLoadCodes.TITLE_40.equals(post.getWeekWorkLoad().getCode()))
                {
                    if (!UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode()))
                        empl40x6MainCnt += staffRate * monthEmploymentDays;
                    else
                        empl40x6SecondCnt += staffRate * monthEmploymentDays;
                }
                else if (post.getWeekWorkLoad().getHoursAmount() == 36d)
                {
                    if (!UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode()))
                        empl36x6MainCnt += staffRate * monthEmploymentDays;
                    else
                        empl36x6SecondCnt += staffRate * monthEmploymentDays;
                }
            }

            if(post.getPostDate().getTime() >= yearStartDate.getTime() && post.getPostDate().getTime() < yearEndDate.getTime())
                acceptedEmployeeCnt++;
            if(null != post.getDismissalDate() && post.getDismissalDate().getTime() >= yearStartDate.getTime() && post.getDismissalDate().getTime() < yearEndDate.getTime())
                dismissedEmployeeCnt++;

            if(post.getPostDate().getTime() < yearEndDate.getTime() && (null == post.getDismissalDate() || post.getDismissalDate().getTime() > yearEndDate.getTime()))
                remainingEmployeeCnt++;

        }

        int monthDays = ((Long)((monthEndDate.getTime() - monthStartDate.getTime()) / DAY_DURATION)).intValue();

        double empl40x5MainVal = Math.ceil(empl40x5MainCnt / monthDays);
        double empl36x5MainVal = Math.ceil(empl36x5MainCnt / monthDays);
        double empl40x6MainVal = Math.ceil(empl40x6MainCnt / monthDays);
        double empl36x6MainVal = Math.ceil(empl36x6MainCnt / monthDays);
        double empl40x5SecondVal = Math.ceil(empl40x5SecondCnt / monthDays);
        double empl36x5SecondVal = Math.ceil(empl36x5SecondCnt / monthDays);
        double empl40x6SecondVal = Math.ceil(empl40x6SecondCnt / monthDays);
        double empl36x6SecondVal = Math.ceil(empl36x6SecondCnt / monthDays);

        DoubleFormatter fmt = DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS;
        paramModifier.put("reportN1", fmt.format(empl40x5MainVal));
        paramModifier.put("reportN2", fmt.format(empl36x5MainVal));
        paramModifier.put("reportN3", fmt.format(empl40x6MainVal));
        paramModifier.put("reportN4", fmt.format(empl36x6MainVal));
        paramModifier.put("reportN5", fmt.format(Math.round(empl40x5MainVal + empl36x5MainVal + empl40x6MainVal + empl36x6MainVal)));
        paramModifier.put("reportN6", fmt.format(empl40x5SecondVal));
        paramModifier.put("reportN7", fmt.format(empl36x5SecondVal));
        paramModifier.put("reportN8", fmt.format(empl40x6SecondVal));
        paramModifier.put("reportN9", fmt.format(empl36x6SecondVal));
        paramModifier.put("reportN10", fmt.format(Math.round(empl40x5SecondVal + empl36x5SecondVal + empl40x6SecondVal + empl36x6SecondVal)));

        paramModifier.put("reportN11", String.valueOf(emplHolidayCnt));
        paramModifier.put("reportN12", fmt.format(Math.ceil(emplHolidayDurations)));

        paramModifier.put("reportN13", String.valueOf(acceptedEmployeeCnt));
        paramModifier.put("reportN14", String.valueOf(dismissedEmployeeCnt));
        paramModifier.put("reportN15", String.valueOf(remainingEmployeeCnt));

        double sumStaffRate = 0;
        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_STAFF_LIST_STATE, getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACTIVE)));
        List<StaffListItem> staffListItemsList = builder.getResultList(getSession());
        for(StaffListItem item : staffListItemsList)
        {
//            sumStaffRate += item.getRemainingBudgetStaffRate() + item.getRemainingOffBudgetStaffRate();
            sumStaffRate += item.getStaffRate() - item.getOccStaffRate();
        }
        paramModifier.put("reportN16", fmt.format(Math.ceil(sumStaffRate)));

        return UniempReportUtil.preparePrintingReport(getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_QUANTITY_EXTRACT_FORM_P4), paramModifier, null);
    }

    /**
     * Возвращает список сотрудников
     */
    private List<EmployeePost> getEmployeePostsList(Date monthStartDate, Date monthEndDate)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.add(MQExpression.isNotNull("ep", EmployeePost.weekWorkLoad().s()));
        builder.add(MQExpression.isNotNull("ep", EmployeePost.workWeekDuration().s()));
        builder.add(MQExpression.lessOrEq("ep", EmployeePost.P_POST_DATE, monthEndDate));
        AbstractExpression expr1 = MQExpression.isNull("ep", EmployeePost.P_DISMISSAL_DATE);
        AbstractExpression expr2 = MQExpression.great("ep", EmployeePost.P_DISMISSAL_DATE, monthStartDate);
        builder.add(MQExpression.or(expr1, expr2));
        builder.add(MQExpression.eq("ep", EmployeePost.postStatus().active(), true));
        return builder.getResultList(getSession());
    }

    /**
     * Возвращает MAP ставок сотрудников
     */
    private Map<Long, List<EmployeePostStaffRateItem>> getEmployeePostsStaffRatesMap(Date monthStartDate, Date monthEndDate)
    {
        MQBuilder builder = new MQBuilder(EmployeePostStaffRateItem.ENTITY_CLASS, "rel");
        builder.add(MQExpression.less("rel", EmployeePostStaffRateItem.L_EMPLOYEE_POST + "." + EmployeePost.P_POST_DATE, monthEndDate));
        AbstractExpression expr1 = MQExpression.isNull("rel", EmployeePostStaffRateItem.L_EMPLOYEE_POST + "." + EmployeePost.P_DISMISSAL_DATE);
        AbstractExpression expr2 = MQExpression.great("rel", EmployeePostStaffRateItem.L_EMPLOYEE_POST + "." + EmployeePost.P_DISMISSAL_DATE, monthStartDate);
        builder.add(MQExpression.or(expr1, expr2));
        List<EmployeePostStaffRateItem> relsList = builder.getResultList(getSession());

        Map<Long, List<EmployeePostStaffRateItem>> result = new HashMap<>();
        for (EmployeePostStaffRateItem rel : relsList)
        {
            if (result.containsKey(rel.getEmployeePost().getId()))
                result.get(rel.getEmployeePost().getId()).add(rel);
            else
            {
                List<EmployeePostStaffRateItem> items = new ArrayList<>();
                items.add(rel);
                result.put(rel.getEmployeePost().getId(), items);
            }
        }

        return result;
    }
    
    /**
     * Возвращает MAP списков отпусков сотрудников
     */
    private Map<Long, List<EmployeeHoliday>> getEmployeeHolidaysMap(Date monthEndDate)
    {
        MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "h");
        builder.add(MQExpression.less("h", EmployeeHoliday.P_START_DATE, monthEndDate));
        builder.add(MQExpression.notIn("h", EmployeeHoliday.L_HOLIDAY_TYPE + "." + HolidayType.P_CODE, HOLIDAY_TYPE_CODES_LIST));
        List<EmployeeHoliday> holidaysList = builder.getResultList(getSession());

        Map<Long, List<EmployeeHoliday>> result = new HashMap<>();
        for (EmployeeHoliday holiday : holidaysList)
        {
            Long key = holiday.getEmployeePost().getId();
            List<EmployeeHoliday> value = result.get(key);
            if (null == value)
                result.put(key, value = new ArrayList<>());
            value.add(holiday);
        }

        return result;
    }
    
    /**
     * Возвращает MAP списков отпусков сотрудников
     */
    private Map<Long, List<EmployeeHoliday>> getEmployeeHolidaysWOSalaryMap(Date monthEndDate)
    {
        MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "h");
        builder.add(MQExpression.less("h", EmployeeHoliday.P_START_DATE, monthEndDate));
        builder.add(MQExpression.eq("h", EmployeeHoliday.L_HOLIDAY_TYPE + "." + HolidayType.P_CODE, "2"));
        List<EmployeeHoliday> holidaysList = builder.getResultList(getSession());

        Map<Long, List<EmployeeHoliday>> result = new HashMap<>();
        for (EmployeeHoliday holiday : holidaysList)
        {
            Long key = holiday.getEmployeePost().getId();
            List<EmployeeHoliday> value = result.get(key);
            if (null == value)
                result.put(key, value = new ArrayList<>());
            value.add(holiday);
        }

        return result;
    }
}