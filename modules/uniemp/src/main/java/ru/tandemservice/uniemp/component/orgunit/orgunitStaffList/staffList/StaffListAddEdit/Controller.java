/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author dseleznev
 *         Created on: 19.09.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        boolean newObject = null == getModel(component).getStaffList().getId();
        getDao().update(getModel(component));
        if (newObject)
        {
            deactivate(component, 2);
            activateInRoot(component, new PublisherActivator(getModel(component).getStaffList()));
        }
        else
        {
            deactivate(component);
        }
    }
}