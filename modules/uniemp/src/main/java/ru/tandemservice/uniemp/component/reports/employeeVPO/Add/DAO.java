/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.employeeVPO.Add;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPost;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1RepLineToPostType;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1Report;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 * Created on: 21.01.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final String LEVEL = "level";

    @Override
    public void prepare(Model model)
    {
        model.setFormingDate(new Date());
        model.setReportDate(new Date());

        GregorianCalendar ageDate = new GregorianCalendar();
        ageDate.add(Calendar.YEAR, 1);
        model.setAgeDate(CoreDateUtils.getYearFirstTimeMoment(ageDate.get(Calendar.YEAR)));
    }

    /**
     * Формирует печатную форму отчета
     */
    private RtfDocument getDocument(final Model model)
    {
        // получить шаблон отчета
        ITemplateDocument templateDocument = getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_VPO1);
        RtfDocument template = new RtfReader().read(templateDocument.getContent());
        RtfDocument document = template.getClone();

        // заполняем строчные метки
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        SimpleDateFormat df = new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU);
        paramModifier.put("reportDate", df.format(model.getReportDate()));
        paramModifier.put("ageCalculationDate", df.format(model.getAgeDate()));
        Integer foreignEmpQuantity = new DQLSelectBuilder().fromEntity(EmployeePost.class, "b").column(property(EmployeePost.employee().id().fromAlias("b")))
                .where(eqValue(property(EmployeePost.postStatus().active().fromAlias("b")), true))
                .where(ne(property(EmployeePost.person().identityCard().citizenship().code().fromAlias("b")), IKladrDefines.RUSSIA_COUNTRY_CODE))
                .where(le(property(EmployeePost.postDate().fromAlias("b")), value(model.getReportDate(), PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeePost.dismissalDate().fromAlias("b"))),
                        ge(property(EmployeePost.dismissalDate().fromAlias("b")), value(model.getReportDate(), PropertyType.DATE))))
                .predicate(DQLPredicateType.distinct)
                .createStatement(getSession()).list().size();
        paramModifier.put("foreignEmpQuantity", foreignEmpQuantity.toString());

        // заполняем табличные метки
        RtfTableModifier tableModifier = new RtfTableModifier();
        injectTables(tableModifier, model);

        paramModifier.modify(document);
        tableModifier.modify(document);

        RtfUtil.optimizeFontAndColorTable(document);
        return document;
    }

    /**
     * Подготавливает и заполняет табличные метки.
     */
    private void injectTables(final RtfTableModifier tableModifier, final Model model)
    {
        // подготавливаем общие данные отчета
        pepareReportData(model);
        
        // Распределение численности основного персонала по уровню образования (без совмест.)
        prepareEduMainData(model);
        injectEduMainTable(tableModifier, model);

        // Распределение численности основного персонала по уровню образования
        prepareEduCombData(model);
        injectEduCombTable(tableModifier, model);

        // Распределение персонала по стажу работы
        prepareServiceLengthData(model);
        injectServiceLengthTable(tableModifier, model);

        // Распределение персонала по стажу работы
        prepareSexData(model);
        injectSexTable(tableModifier, model);
    }

    /**
     * Поднимаются из базы и обрабатываются общие для отчета данные.
     */
    @SuppressWarnings("deprecation")
    protected void pepareReportData(final Model model)
    {
        List<EmployeePost> employeePostList = new DQLSelectBuilder().fromEntity(EmployeePost.class, "b").column("b")
                .where(eqValue(property(EmployeePost.postType().code().fromAlias("b")), PostTypeCodes.MAIN_JOB))
                .where(eqValue(property(EmployeePost.postStatus().active().fromAlias("b")), true))
                .where(le(property(EmployeePost.postDate().fromAlias("b")), value(model.getReportDate(), PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeePost.dismissalDate().fromAlias("b"))),
                        ge(property(EmployeePost.dismissalDate().fromAlias("b")), value(model.getReportDate(), PropertyType.DATE))))
                .createStatement(getSession()).list();

        model.setEmployeePostList(employeePostList);

        List<EmployeePost> employeePostCombList = new DQLSelectBuilder().fromEntity(EmployeePost.class, "b").column("b")
                .where(eqValue(property(EmployeePost.postType().code().fromAlias("b")), PostTypeCodes.SECOND_JOB_OUTER))
                .where(eqValue(property(EmployeePost.postStatus().active().fromAlias("b")), true))
                .where(eqValue(property(EmployeePost.hourlyPaid().fromAlias("b")), false))
                .where(le(property(EmployeePost.postDate().fromAlias("b")), value(model.getReportDate(), PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeePost.dismissalDate().fromAlias("b"))),
                        ge(property(EmployeePost.dismissalDate().fromAlias("b")), value(model.getReportDate(), PropertyType.DATE))))
                .where(notExists(new DQLSelectBuilder().fromEntity(EmployeePost.class, "a").column(property(EmployeePost.id().fromAlias("a")))
                        .where(eq(property(EmployeePost.employee().id().fromAlias("a")), property(EmployeePost.employee().id().fromAlias("b"))))
                        .where(eq(property(EmployeePost.postType().code().fromAlias("a")), value(PostTypeCodes.MAIN_JOB)))
                        .where(le(property(EmployeePost.postDate().fromAlias("a")), value(model.getReportDate(), PropertyType.DATE)))
                        .where(or(
                                isNull(property(EmployeePost.dismissalDate().fromAlias("a"))),
                                ge(property(EmployeePost.dismissalDate().fromAlias("a")), value(model.getReportDate(), PropertyType.DATE))))
                        .buildQuery()))
                .createStatement(getSession()).list();

        model.setEmployeePostCombList(employeePostCombList);

        List<EmployeePost> postList = new ArrayList<>();
        postList.addAll(employeePostList);
        postList.addAll(employeePostCombList);

        model.setPostList(postList);

        final List<EmployeePostStaffRateItem> employeePostStaffRateItemList = new ArrayList<>();
        BatchUtils.execute(postList, DQL.MAX_VALUES_ROW_NUMBER, elements -> employeePostStaffRateItemList.addAll(
                new DQLSelectBuilder().fromEntity(EmployeePostStaffRateItem.class, "b").column("b")
                .where(in(property(EmployeePostStaffRateItem.employeePost().fromAlias("b")), elements))
                .createStatement(getSession()).<EmployeePostStaffRateItem>list()
        ));

        final List<PersonEduInstitution> personEduInstitutionList = new ArrayList<>();
        final List<PersonEduDocument> personEduDocumentList = new ArrayList<>();
        final List<PersonAcademicStatus> personAcademicStatusList = new ArrayList<>();
        final List<PersonAcademicDegree> personAcademicDegreeList = new ArrayList<>();

        BatchUtils.execute(CommonBaseEntityUtil.getPropertiesSet(postList, EmployeePost.person()), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            personEduInstitutionList.addAll(
                    new DQLSelectBuilder().fromEntity(PersonEduInstitution.class, "b").column(property("b"))
                            .where(in(property(PersonEduInstitution.person().fromAlias("b")), elements))
                            .where(in(property(PersonEduInstitution.educationLevelStage().code().fromAlias("b")), EducationLevelStageCodes.HIGH_PROF, EducationLevelStageCodes.NEPOLNOE_VYSSHEE, EducationLevelStageCodes.BAKALAVRIAT, EducationLevelStageCodes.PODGOTOVKA_DIPLOMIROVANNYH_SPETSIALISTOV, EducationLevelStageCodes.MAGISTRATURA))
                            .createStatement(getSession()).<PersonEduInstitution>list()
            );
            personEduDocumentList.addAll(
                    new DQLSelectBuilder().fromEntity(PersonEduDocument.class, "b").column(property("b"))
                            .where(in(property(PersonEduDocument.person().fromAlias("b")), elements))
                            .where(in(property(PersonEduDocument.eduLevel().code().fromAlias("b")), EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT, EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA, EduLevelCodes.VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII))
                            .createStatement(getSession()).<PersonEduDocument>list()
            );
            personAcademicStatusList.addAll(
                    new DQLSelectBuilder().fromEntity(PersonAcademicStatus.class, "b").column(property("b"))
                            .where(in(property(PersonAcademicStatus.person().fromAlias("b")), elements))
                            .createStatement(getSession()).<PersonAcademicStatus>list()
            );
            personAcademicDegreeList.addAll(
                    new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "b").column(property("b"))
                            .where(in(property(PersonAcademicDegree.person().fromAlias("b")), elements))
                            .createStatement(getSession()).<PersonAcademicDegree>list()
            );
        });

        final CoreCollectionUtils.Pair<Date, Date> eduYearPair = getEduYearDates(model.getReportDate());

        final List<EmployeeRetrainingItem> employeeRetrainingItemList = new ArrayList<>();
        final List<EmployeeTrainingItem> employeeTrainingItemList = new ArrayList<>();
        BatchUtils.execute(CommonBaseEntityUtil.getPropertiesSet(postList, EmployeePost.employee()), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            employeeRetrainingItemList.addAll(
                    new DQLSelectBuilder().fromEntity(EmployeeRetrainingItem.class, "b").column(property("b"))
                            .where(in(property(EmployeeRetrainingItem.employee().fromAlias("b")), elements))
                            .where(betweenDays(EmployeeRetrainingItem.finishDate().fromAlias("b"), eduYearPair.getX(), eduYearPair.getY()))
                            .createStatement(getSession()).<EmployeeRetrainingItem>list()
            );

            employeeTrainingItemList.addAll(
                    new DQLSelectBuilder().fromEntity(EmployeeTrainingItem.class, "b").column(property("b"))
                            .where(in(property(EmployeeTrainingItem.employee().fromAlias("b")), elements))
                            .where(betweenDays(EmployeeTrainingItem.finishDate().fromAlias("b"), eduYearPair.getX(), eduYearPair.getY()))
                            .createStatement(getSession()).<EmployeeTrainingItem>list()
            );
        });

        Map<EmployeePost, Double> employeePostStaffRateMap = new HashMap<>();
        for (EmployeePostStaffRateItem item : employeePostStaffRateItemList)
        {
            Double rate = employeePostStaffRateMap.get(item.getEmployeePost());
            if (rate == null)
                rate = 0d;
            rate += item.getStaffRate();
            employeePostStaffRateMap.put(item.getEmployeePost(), rate);
        }

        model.setEmployeePostStaffRateMap(employeePostStaffRateMap);

        List<Person> highEducationPersonList = new ArrayList<>();
        for (PersonEduInstitution institution : personEduInstitutionList) {
            highEducationPersonList.add(institution.getPerson());
        }
        for (PersonEduDocument document : personEduDocumentList) {
            highEducationPersonList.add(document.getPerson());
        }


        model.setHighEducationPersonList(highEducationPersonList);

        Map<Person, List<String>> personAcademicStatusCodeMap = new HashMap<>();
        for (PersonAcademicStatus academicStatus : personAcademicStatusList)
        {
            if (academicStatus.getAcademicStatus() == null || academicStatus.getAcademicStatus().getType() == null)
                continue;

            List<String> list = personAcademicStatusCodeMap.get(academicStatus.getPerson());
            if (list == null)
                personAcademicStatusCodeMap.put(academicStatus.getPerson(), list = new ArrayList<>());
            list.add(academicStatus.getAcademicStatus().getType().getCode());
        }

        model.setPersonAcademicStatusCodeMap(personAcademicStatusCodeMap);

        Map<Person, List<String>> personAcademicDegreeCodeMap = new HashMap<>();
        for (PersonAcademicDegree academicDegree : personAcademicDegreeList)
        {
            if (academicDegree.getAcademicDegree() == null || academicDegree.getAcademicDegree().getType() == null)
                continue;

            List<String> list = personAcademicDegreeCodeMap.get(academicDegree.getPerson());
            if (list == null)
                personAcademicDegreeCodeMap.put(academicDegree.getPerson(), list = new ArrayList<>());
            list.add(academicDegree.getAcademicDegree().getType().getCode());
        }

        model.setPersonAcademicDegreeCodeMap(personAcademicDegreeCodeMap);

        List<Person> retrainingPersonList = new ArrayList<>();
        for (EmployeeRetrainingItem item : employeeRetrainingItemList)
        {
            retrainingPersonList.add(item.getEmployee().getPerson());
        }

        for (EmployeeTrainingItem item : employeeTrainingItemList)
        {
            retrainingPersonList.add(item.getEmployee().getPerson());
        }

        model.setRetrainingPersonList(retrainingPersonList);

        List<EmployeeVPO1ReportLines> reportLinesList = getCatalogItemList(EmployeeVPO1ReportLines.class);
        Collections.sort(reportLinesList, CommonCatalogUtil.createCodeComparator(EmployeeVPO1ReportLines.P_LINE_CODE));

        // вычесляем какие Должности ассоциированы со стркой отчета, на основании Настройки
        List<EmployeeVPO1RepLineToPost> lineToPostList = new DQLSelectBuilder().fromEntity(EmployeeVPO1RepLineToPost.class, "b").column("b").createStatement(getSession()).list();
        List<EmployeeVPO1RepLineToPostType> lineToPostTypeList = new DQLSelectBuilder().fromEntity(EmployeeVPO1RepLineToPostType.class, "b").column("b").createStatement(getSession()).list();

        // строка отчета -> список id должностей
        Map<EmployeeVPO1ReportLines, List<Long>> linePostMap = new HashMap<>();
        // строка отчета -> список типов должностей
        Map<EmployeeVPO1ReportLines, List<EmployeeType>> linePostTypeMap = new HashMap<>();

        // формируем первую мапу, все что указано у дочерних строк должно быть у родительских
        for (EmployeeVPO1RepLineToPost relation : lineToPostList)
        {
            EmployeeVPO1ReportLines parent = relation.getEmployeeVPO1ReportLines();
            while (parent != null)
            {
                List<Long> list = linePostMap.get(parent);
                if (list == null)
                    linePostMap.put(parent, list = new ArrayList<>());
                list.add(relation.getPostBoundedWithQGandQL().getId());

                parent = parent.getParent();
            }
        }

        // формируем второую мапу, все что указано у дочерних строк должно быть у родительских
        for (EmployeeVPO1RepLineToPostType relation : lineToPostTypeList)
        {
            EmployeeVPO1ReportLines parent = relation.getEmployeeVPO1ReportLines();
            while (parent != null)
            {
                List<EmployeeType> list = linePostTypeMap.get(parent);
                if (list == null)
                    linePostTypeMap.put(parent, list = new ArrayList<>());
                list.add(relation.getEmployeeType());

                parent = parent.getParent();
            }
        }

        // если для строки(и ее дочерних строк) не указаны должности, то если указаны Типы должности указываем все должности данного типа со строкой
        // иначе ни одна должность не указана для строки, в строке всегда будут нули
        for (EmployeeVPO1ReportLines reportLine : reportLinesList)
        {
            List<Long> list = linePostMap.get(reportLine);
            if (list == null)
            {
                linePostMap.put(reportLine, list = new ArrayList<>());

                if (linePostTypeMap.containsKey(reportLine))
                {
                    list.addAll(new DQLSelectBuilder().fromEntity(PostBoundedWithQGandQL.class, "b").column(property(PostBoundedWithQGandQL.id().fromAlias("b")))
                            .where(in(property(PostBoundedWithQGandQL.post().employeeType().code().fromAlias("b")),
                                      CommonBaseEntityUtil.getPropertiesSet(linePostTypeMap.get(reportLine), EmployeeType.code())))
                            .createStatement(getSession()).<Long>list());
                }
            }

        }

        // код строки -> список id должностей
        Map<String, List<Long>> reportLineCodePostIdMap = new HashMap<>();
        for (Map.Entry<EmployeeVPO1ReportLines, List<Long>> entry : linePostMap.entrySet())
            reportLineCodePostIdMap.put(entry.getKey().getLineCode(), entry.getValue());

        model.setReportLineList(reportLinesList);
        model.setReportLineCodePostIdMap(reportLineCodePostIdMap);
    }

    /**
     * Подготавливаются строки таблицы Распределение численности основного персонала по уровню образования (без совмест.)
     */
    protected void prepareEduMainData(final Model model)
    {
        List<ReportEducationLineWrapper> reportEducationLineWrapperList = new ArrayList<>();
        int number = 1;
        EmployeeVPO1ReportLines preReportLine = null;
        for (EmployeeVPO1ReportLines reportLine : model.getReportLineList())
        {
            int level = 1;
            EmployeeVPO1ReportLines parent = reportLine.getParent();
            while (parent != null)
            {
                level++;
                parent = parent.getParent();
            }

            String title = preReportLine != null && preReportLine.getPrefix() != null ? preReportLine.getPrefix() + "par" + reportLine.getTitle() : reportLine.getTitle();
            reportEducationLineWrapperList.add(new ReportEducationLineWrapper(level, title, reportLine.getLineCode(), number++));

            preReportLine = reportLine;
        }
        reportEducationLineWrapperList.add(new ReportEducationLineWrapper(1, "Кроме того:parЧисленность профессорско-преподавательского состава, работающих по договорам гражданско-правового характера", String.valueOf(20), number));

        for (ReportEducationLineWrapper wrapper : reportEducationLineWrapperList)
        {
            Set<Employee> employeeSet = new HashSet<>();
            for (EmployeePost post : model.getEmployeePostList())
            {
                if (wrapper.getReportLineCode().equals("20"))
                {
                    if (model.getReportLineCodePostIdMap().get("07").contains(post.getPostRelation().getPostBoundedWithQGandQL().getId()) &&
                            post.isHourlyPaid())
                        employeeSet.add(post.getEmployee());
                }
                else
                {
                    if (model.getReportLineCodePostIdMap().get(wrapper.getReportLineCode()).contains(post.getPostRelation().getPostBoundedWithQGandQL().getId()) &&
                            !post.isHourlyPaid())
                        employeeSet.add(post.getEmployee());
                }
            }

            wrapper.setTotal(employeeSet.size());

            Set<Person> gr3HighEduPersonSet = new HashSet<>();
            for (Employee employee : employeeSet)
            {

                if (model.getHighEducationPersonList().contains(employee.getPerson()))
                    gr3HighEduPersonSet.add(employee.getPerson());
            }

            wrapper.setGr3HighEdu(gr3HighEduPersonSet.size());

            int dok = 0;
            for (Person person : gr3HighEduPersonSet)
            {
                if (model.getPersonAcademicDegreeCodeMap().containsKey(person))
                {
                    if (model.getPersonAcademicDegreeCodeMap().get(person).contains(UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR))
                        dok++;
                }
            }

            wrapper.setDoctor(dok);

            int cand = 0;
            for (Person person : gr3HighEduPersonSet)
            {
                if (model.getPersonAcademicDegreeCodeMap().containsKey(person))
                {
                    if (model.getPersonAcademicDegreeCodeMap().get(person).contains(UniDefines.SCIENCE_DEGREE_TYPE_MASTER) && !model.getPersonAcademicDegreeCodeMap().get(person).contains(UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR))
                        cand++;
                }
            }

            wrapper.setCandidate(cand);

            int prof = 0;
            for (Person person : gr3HighEduPersonSet)
            {
                if (model.getPersonAcademicStatusCodeMap().containsKey(person))
                {
                    if (model.getPersonAcademicStatusCodeMap().get(person).contains(UniDefines.SCIENCE_STATUS_TYPE_PROFESSOR))
                        prof++;
                }
            }

            wrapper.setProfessor(prof);

            int doc = 0;
            for (Person person : gr3HighEduPersonSet)
            {
                if (model.getPersonAcademicStatusCodeMap().containsKey(person))
                {
                    if (model.getPersonAcademicStatusCodeMap().get(person).contains(UniDefines.SCIENCE_STATUS_TYPE_DOCENT) && !model.getPersonAcademicStatusCodeMap().get(person).contains(UniDefines.SCIENCE_STATUS_TYPE_PROFESSOR))
                        doc++;
                }
            }

            wrapper.setDocent(doc);

            int rate025 = 0;
            int rate050 = 0;
            int rate075 = 0;
            for (EmployeePost post : model.getEmployeePostList())
            {
                if (!gr3HighEduPersonSet.contains(post.getPerson()) || !model.getEmployeePostStaffRateMap().containsKey(post))
                    continue;

                Double staffRate = model.getEmployeePostStaffRateMap().get(post);

                if (staffRate.equals(0.25d))
                    rate025++;
                else if (staffRate.equals(0.5d))
                    rate050++;
                else if (staffRate.equals(0.75d))
                    rate075++;
            }

            wrapper.setStaffRate025(rate025);
            wrapper.setStaffRate050(rate050);
            wrapper.setStaffRate075(rate075);

            int women = 0;
            for (Employee employee : employeeSet)
            {
                if (!employee.getPerson().isMale())
                    women++;
            }

            wrapper.setWomen(women);

            int profQuali = 0;
            for (Employee employee : employeeSet)
            {
                if (model.getRetrainingPersonList().contains(employee.getPerson()))
                    profQuali++;
            }

            wrapper.setProfQuali(profQuali);
        }

        model.setReportEducationLineWrappers(reportEducationLineWrapperList);
    }

    /**
     * Подготавливаются строки для таблицы Распределение численности основного персонала по уровню образования
     */
    protected void prepareEduCombData(final Model model)
    {
        List<String> codeList = Arrays.asList("01", "07", "08", "09", "10", "11", "12", "13", "14", "15", "18");
        List<ReportEducationLineWrapper> reportEducationLineWrapperList = new ArrayList<>();
        int number = 1;
        EmployeeVPO1ReportLines preReportLine = null;
        for (EmployeeVPO1ReportLines reportLine : model.getReportLineList())
        {
            if (!codeList.contains(reportLine.getLineCode()))
                continue;

            int level = 1;
            EmployeeVPO1ReportLines parent = reportLine.getParent();
            while (parent != null)
            {
                level++;
                parent = parent.getParent();
            }

            String title = preReportLine != null && preReportLine.getPrefix() != null ? preReportLine.getPrefix() + "par" + reportLine.getTitle() : reportLine.getTitle();
            reportEducationLineWrapperList.add(new ReportEducationLineWrapper(level, title, reportLine.getLineCode(), number++));

            preReportLine = reportLine;
        }

        for (ReportEducationLineWrapper wrapper : reportEducationLineWrapperList)
        {
            Set<Employee> employeeSet = new HashSet<>();
            for (EmployeePost post : model.getEmployeePostCombList())
            {
                if (model.getReportLineCodePostIdMap().get(wrapper.getReportLineCode()).contains(post.getPostRelation().getPostBoundedWithQGandQL().getId()))
                    employeeSet.add(post.getEmployee());
            }

            wrapper.setTotal(employeeSet.size());

            Set<Person> gr3HighEduPersonSet = new HashSet<>();
            for (Employee employee : employeeSet)
            {

                if (model.getHighEducationPersonList().contains(employee.getPerson()))
                    gr3HighEduPersonSet.add(employee.getPerson());
            }

            wrapper.setGr3HighEdu(gr3HighEduPersonSet.size());

            int dok = 0;
            for (Person person : gr3HighEduPersonSet)
            {
                if (model.getPersonAcademicDegreeCodeMap().containsKey(person))
                {
                    if (model.getPersonAcademicDegreeCodeMap().get(person).contains(UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR))
                        dok++;
                }
            }

            wrapper.setDoctor(dok);

            int cand = 0;
            for (Person person : gr3HighEduPersonSet)
            {
                if (model.getPersonAcademicDegreeCodeMap().containsKey(person))
                {
                    if (model.getPersonAcademicDegreeCodeMap().get(person).contains(UniDefines.SCIENCE_DEGREE_TYPE_MASTER) && !model.getPersonAcademicDegreeCodeMap().get(person).contains(UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR))
                        cand++;
                }
            }

            wrapper.setCandidate(cand);

            int prof = 0;
            for (Person person : gr3HighEduPersonSet)
            {
                if (model.getPersonAcademicStatusCodeMap().containsKey(person))
                {
                    if (model.getPersonAcademicStatusCodeMap().get(person).contains(UniDefines.SCIENCE_STATUS_TYPE_PROFESSOR))
                        prof++;
                }
            }

            wrapper.setProfessor(prof);

            int doc = 0;
            for (Person person : gr3HighEduPersonSet)
            {
                if (model.getPersonAcademicStatusCodeMap().containsKey(person))
                {
                    if (model.getPersonAcademicStatusCodeMap().get(person).contains(UniDefines.SCIENCE_STATUS_TYPE_DOCENT) && !model.getPersonAcademicStatusCodeMap().get(person).contains(UniDefines.SCIENCE_STATUS_TYPE_PROFESSOR))
                        doc++;
                }
            }

            wrapper.setDocent(doc);

            int rate025 = 0;
            int rate050 = 0;
            int rate075 = 0;
            for (EmployeePost post : model.getEmployeePostCombList())
            {
                if (!gr3HighEduPersonSet.contains(post.getPerson()) || !model.getEmployeePostStaffRateMap().containsKey(post))
                    continue;

                Double staffRate = model.getEmployeePostStaffRateMap().get(post);

                if (staffRate.equals(0.25d))
                    rate025++;
                else if (staffRate.equals(0.5d))
                    rate050++;
                else if (staffRate.equals(0.75d))
                    rate075++;
            }

            wrapper.setStaffRate025(rate025);
            wrapper.setStaffRate050(rate050);
            wrapper.setStaffRate075(rate075);

            int women = 0;
            for (Employee employee : employeeSet)
            {
                if (!employee.getPerson().isMale())
                    women++;
            }

            wrapper.setWomen(women);

            int profQuali = 0;
            for (Employee employee : employeeSet)
            {
                if (model.getRetrainingPersonList().contains(employee.getPerson()))
                    profQuali++;
            }

            wrapper.setProfQuali(profQuali);
        }

        model.setReportEducationCombinationLineWrappers(reportEducationLineWrapperList);
    }

    /**
     * Подготавливаются строки для таблицы Распределение персонала по стажу работы
     */
    protected void prepareServiceLengthData(final Model model)
    {
        final List<EmpHistoryToServiceLengthTypeRelation> typeRelationList = new ArrayList<>();
        BatchUtils.execute(CommonBaseEntityUtil.getPropertiesSet(model.getPostList(), EmployeePost.employee().s()), DQL.MAX_VALUES_ROW_NUMBER, elements -> typeRelationList.addAll(
                new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column("b")
                        .where(in(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().employee().fromAlias("b")), elements))
                        .where(in(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().code().fromAlias("b")), Arrays.asList(UniempDefines.SERVICE_LENGTH_TYPE_COMMON, UniempDefines.SERVICE_LENGTH_TYPE_EDUCATOR)))
                        .createStatement(getSession()).<EmpHistoryToServiceLengthTypeRelation>list()
        ));

        Map<Employee, List<EmploymentHistoryItemBase>> employeeCommonHistoryItemMap = new HashMap<>();
        Map<Employee, List<EmploymentHistoryItemBase>> employeeEducatorHistoryItemMap = new HashMap<>();
        for (EmpHistoryToServiceLengthTypeRelation relation : typeRelationList)
        {
            if (relation.getServiceLengthType().getCode().equals(UniempDefines.SERVICE_LENGTH_TYPE_COMMON))
            {
                List<EmploymentHistoryItemBase> list = employeeCommonHistoryItemMap.get(relation.getEmploymentHistoryItemBase().getEmployee());
                if (list == null)
                    employeeCommonHistoryItemMap.put(relation.getEmploymentHistoryItemBase().getEmployee(), list = new ArrayList<>());
                list.add(relation.getEmploymentHistoryItemBase());
            }
            else
            {
                List<EmploymentHistoryItemBase> list = employeeEducatorHistoryItemMap.get(relation.getEmploymentHistoryItemBase().getEmployee());
                if (list == null)
                    employeeEducatorHistoryItemMap.put(relation.getEmploymentHistoryItemBase().getEmployee(), list = new ArrayList<>());
                list.add(relation.getEmploymentHistoryItemBase());
            }
        }

        Map<Employee, Integer> employeeCommonDurationMap = new HashMap<>();
        Map<Employee, Integer> employeeEducatorDurationMap = new HashMap<>();
        for (Map.Entry<Employee, List<EmploymentHistoryItemBase>> entry : employeeCommonHistoryItemMap.entrySet())
        {
            List<CoreCollectionUtils.Pair<Date, Date>> pairList = new ArrayList<>();
            for (EmploymentHistoryItemBase item : entry.getValue())
                pairList.add(new CoreCollectionUtils.Pair<>(item.getAssignDate(), item.getDismissalDate()));

            employeeCommonDurationMap.put(entry.getKey(), UniempUtil.getServiceLengthTypeDuration(model.getReportDate(), pairList).getThird());
        }
        for (Map.Entry<Employee, List<EmploymentHistoryItemBase>> entry : employeeEducatorHistoryItemMap.entrySet())
        {
            List<CoreCollectionUtils.Pair<Date, Date>> pairList = new ArrayList<>();
            for (EmploymentHistoryItemBase item : entry.getValue())
                pairList.add(new CoreCollectionUtils.Pair<>(item.getAssignDate(), item.getDismissalDate()));

            employeeEducatorDurationMap.put(entry.getKey(), UniempUtil.getServiceLengthTypeDuration(model.getReportDate(), pairList).getThird());
        }


        {
            List<String> codeList = Arrays.asList("02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14");
            List<ReportSrviceLengthLineWrapper> reportServiceLengthLineWrappers = new ArrayList<>();
            int number = 1;
            EmployeeVPO1ReportLines preReportLine = null;
            for (EmployeeVPO1ReportLines reportLine : model.getReportLineList())
            {
                if (!codeList.contains(reportLine.getLineCode()))
                    continue;

                int level = 1;
                EmployeeVPO1ReportLines parent = reportLine.getParent();
                while (parent != null)
                {
                    level++;
                    parent = parent.getParent();
                }

                String title = preReportLine != null && preReportLine.getPrefix() != null ? preReportLine.getPrefix() + "par" + reportLine.getTitle() : reportLine.getTitle();
                reportServiceLengthLineWrappers.add(new ReportSrviceLengthLineWrapper(level, title, reportLine.getLineCode(), number++));

                preReportLine = reportLine;
            }
            reportServiceLengthLineWrappers.add(new ReportSrviceLengthLineWrapper(1, "Кроме того:parПрофессорско-преподавательский состав, работающий на условиях штатного совместительства (внешние совместители)", "07", number));

            for (ReportSrviceLengthLineWrapper wrapper : reportServiceLengthLineWrappers)
            {
                Set<Employee> employeeSet = new HashSet<>();
                if (reportServiceLengthLineWrappers.indexOf(wrapper) == reportServiceLengthLineWrappers.size() - 1)
                {
                    for (EmployeePost post : model.getEmployeePostCombList())
                    {
                        if (model.getReportLineCodePostIdMap().get(wrapper.getReportLineCode()).contains(post.getPostRelation().getPostBoundedWithQGandQL().getId()))
                            employeeSet.add(post.getEmployee());
                    }
                }
                else
                {
                    for (EmployeePost post : model.getEmployeePostList())
                    {
                        if (!post.isHourlyPaid())
                            if (model.getReportLineCodePostIdMap().get(wrapper.getReportLineCode()).contains(post.getPostRelation().getPostBoundedWithQGandQL().getId()))
                                employeeSet.add(post.getEmployee());
                    }
                }

                wrapper.setCommon(employeeSet.size());

                int common3 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeCommonDurationMap.containsKey(employee))
                        if (employeeCommonDurationMap.get(employee) < 3)
                            common3++;
                }

                wrapper.setCommon3(common3);

                int common3_5 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeCommonDurationMap.containsKey(employee))
                        if (employeeCommonDurationMap.get(employee) >= 3 && employeeCommonDurationMap.get(employee) < 5)
                            common3_5++;
                }

                wrapper.setCommon3_5(common3_5);

                int common5_10 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeCommonDurationMap.containsKey(employee))
                        if (employeeCommonDurationMap.get(employee) >= 5 && employeeCommonDurationMap.get(employee) < 10)
                            common5_10++;
                }

                wrapper.setCommon5_10(common5_10);

                int common10_15 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeCommonDurationMap.containsKey(employee))
                        if (employeeCommonDurationMap.get(employee) >= 10 && employeeCommonDurationMap.get(employee) < 15)
                            common10_15++;
                }

                wrapper.setCommon10_15(common10_15);

                int common15_20 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeCommonDurationMap.containsKey(employee))
                        if (employeeCommonDurationMap.get(employee) >= 15 && employeeCommonDurationMap.get(employee) < 20)
                            common15_20++;
                }

                wrapper.setCommon15_20(common15_20);

                int common20 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeCommonDurationMap.containsKey(employee))
                        if (employeeCommonDurationMap.get(employee) >= 20)
                            common20++;
                }

                wrapper.setCommon20(common20);

                int educator = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeEducatorDurationMap.get(employee) != null)
                        educator++;
                }

                wrapper.setEducator(educator);

                int educator3 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeEducatorDurationMap.containsKey(employee))
                        if (employeeEducatorDurationMap.get(employee) < 3)
                            educator3++;
                }

                wrapper.setEducator3(educator3);

                int educator3_5 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeEducatorDurationMap.containsKey(employee))
                        if (employeeEducatorDurationMap.get(employee) >= 3 && employeeEducatorDurationMap.get(employee) < 5)
                            educator3_5++;
                }

                wrapper.setEducator3_5(educator3_5);

                int educator5_10 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeEducatorDurationMap.containsKey(employee))
                        if (employeeEducatorDurationMap.get(employee) >= 5 && employeeEducatorDurationMap.get(employee) < 10)
                            educator5_10++;
                }

                wrapper.setEducator5_10(educator5_10);

                int educator10_15 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeEducatorDurationMap.containsKey(employee))
                        if (employeeEducatorDurationMap.get(employee) >= 10 && employeeEducatorDurationMap.get(employee) < 15)
                            educator10_15++;
                }

                wrapper.setEducator10_15(educator10_15);

                int educator15_20 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeEducatorDurationMap.containsKey(employee))
                        if (employeeEducatorDurationMap.get(employee) >= 15 && employeeEducatorDurationMap.get(employee) < 20)
                            educator15_20++;
                }

                wrapper.setEducator15_20(educator15_20);

                int educator20 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeEducatorDurationMap.containsKey(employee))
                        if (employeeEducatorDurationMap.get(employee) >= 20)
                            educator20++;
                }

                wrapper.setEducator20(educator20);
            }

            model.setReportSrviceLengthLineWrappers(reportServiceLengthLineWrappers);
        }
    }

    /**
     * Заполняются строки для таблицы Распределение персонала по стажу работы
     */
    protected void prepareSexData(final Model model)
    {
        Map<Employee, Integer> employeeAgeMap = new HashMap<>();
        for (EmployeePost post : model.getPostList())
        {
            if (employeeAgeMap.containsKey(post.getEmployee()))
                continue;

            Date birthDate = post.getPerson().getIdentityCard().getBirthDate();
            employeeAgeMap.put(post.getEmployee(), birthDate != null ? CoreDateUtils.getFullYearsAge(birthDate, model.getAgeDate()) : 0);
        }

        {
            List<String> codeList = Arrays.asList("02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14");
            List<ReportSexLineWrapper> reportSexLineWrappers = new ArrayList<>();
            int number = 1;
            EmployeeVPO1ReportLines preReportLine = null;
            for (EmployeeVPO1ReportLines reportLine : model.getReportLineList())
            {
                if (!codeList.contains(reportLine.getLineCode()))
                    continue;

                int level = 1;
                EmployeeVPO1ReportLines parent = reportLine.getParent();
                while (parent != null)
                {
                    level++;
                    parent = parent.getParent();
                }

                String title = preReportLine != null && preReportLine.getPrefix() != null ? preReportLine.getPrefix() + "par" + reportLine.getTitle() : reportLine.getTitle();
                reportSexLineWrappers.add(new ReportSexLineWrapper(level, title, reportLine.getLineCode(), number++));

                preReportLine = reportLine;
            }
            reportSexLineWrappers.add(new ReportSexLineWrapper(1, "Кроме того:parПрофессорско-преподавательский состав, работающий на условиях штатного совместительства (внешние совместители)", "07", number));

            for (ReportSexLineWrapper wrapper : reportSexLineWrappers)
            {
                Set<Employee> employeeSet = new HashSet<>();
                if (reportSexLineWrappers.indexOf(wrapper) == reportSexLineWrappers.size() - 1)
                {
                    for (EmployeePost post : model.getEmployeePostCombList())
                    {
                        if (model.getReportLineCodePostIdMap().get(wrapper.getReportLineCode()).contains(post.getPostRelation().getPostBoundedWithQGandQL().getId()))
                            employeeSet.add(post.getEmployee());
                    }
                }
                else
                {
                    for (EmployeePost post : model.getEmployeePostList())
                    {
                        if (!post.isHourlyPaid())
                            if (model.getReportLineCodePostIdMap().get(wrapper.getReportLineCode()).contains(post.getPostRelation().getPostBoundedWithQGandQL().getId()))
                                employeeSet.add(post.getEmployee());
                    }
                }

                wrapper.setTotal(employeeSet.size());

                int total25 = 0;
                int women25 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) < 25)
                        {
                            total25++;
                            if (!employee.getPerson().isMale())
                                women25++;
                        }
                }

                wrapper.setTotal25(total25);
                wrapper.setWomen25(women25);

                int total25_29 = 0;
                int women25_29 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) >= 25 && employeeAgeMap.get(employee) <= 29)
                        {
                            total25_29++;
                            if (!employee.getPerson().isMale())
                                women25_29++;
                        }
                }

                wrapper.setTotal25_29(total25_29);
                wrapper.setWomen25_29(women25_29);

                int total30_34 = 0;
                int women30_34 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) >= 30 && employeeAgeMap.get(employee) <= 34)
                        {
                            total30_34++;
                            if (!employee.getPerson().isMale())
                                women30_34++;
                        }
                }

                wrapper.setTotal30_34(total30_34);
                wrapper.setWomen30_34(women30_34);

                int total35_39 = 0;
                int women35_39 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) >= 35 && employeeAgeMap.get(employee) <= 39)
                        {
                            total35_39++;
                            if (!employee.getPerson().isMale())
                                women35_39++;
                        }
                }

                wrapper.setTotal35_39(total35_39);
                wrapper.setWomen35_39(women35_39);

                int total40_44 = 0;
                int women40_44 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) >= 40 && employeeAgeMap.get(employee) <= 44)
                        {
                            total40_44++;
                            if (!employee.getPerson().isMale())
                                women40_44++;
                        }
                }

                wrapper.setTotal40_44(total40_44);
                wrapper.setWomen40_44(women40_44);

                int total45_49 = 0;
                int women45_49 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) >= 45 && employeeAgeMap.get(employee) <= 49)
                        {
                            total45_49++;
                            if (!employee.getPerson().isMale())
                                women45_49++;
                        }
                }

                wrapper.setTotal45_49(total45_49);
                wrapper.setWomen45_49(women45_49);

                int total50_54 = 0;
                int women50_54 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) >= 50 && employeeAgeMap.get(employee) <= 54)
                        {
                            total50_54++;
                            if (!employee.getPerson().isMale())
                                women50_54++;
                        }
                }

                wrapper.setTotal50_54(total50_54);
                wrapper.setWomen50_54(women50_54);

                int total55_59 = 0;
                int women55_59 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) >= 55 && employeeAgeMap.get(employee) <= 59)
                        {
                            total55_59++;
                            if (!employee.getPerson().isMale())
                                women55_59++;
                        }
                }

                wrapper.setTotal55_59(total55_59);
                wrapper.setWomen55_59(women55_59);

                int total60_64 = 0;
                int women60_64 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) >= 60 && employeeAgeMap.get(employee) <= 64)
                        {
                            total60_64++;
                            if (!employee.getPerson().isMale())
                                women60_64++;
                        }
                }

                wrapper.setTotal60_64(total60_64);
                wrapper.setWomen60_64(women60_64);

                int total65 = 0;
                int women65 = 0;
                for (Employee employee : employeeSet)
                {
                    if (employeeAgeMap.containsKey(employee))
                        if (employeeAgeMap.get(employee) >= 65)
                        {
                            total65++;
                            if (!employee.getPerson().isMale())
                                women65++;
                        }
                }

                wrapper.setTotal65(total65);
                wrapper.setWomen65(women65);
            }

            model.setReportSexLineWrappers(reportSexLineWrappers);
        }
    }

    /**
     * Заполняется метка для таблицы Распределение численности основного персонала по уровню образования (без совмест.)
     */
    protected void injectEduMainTable(final RtfTableModifier tableModifier, final Model model)
    {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMinimumIntegerDigits(2);

        List<String[]> lineList = new ArrayList<>();

        for (ReportEducationLineWrapper wrapper : model.getReportEducationLineWrappers())
        {
            String[] line = new String[13];

            line[0] = (wrapper.getLevel() - 1) + LEVEL + wrapper.getReportLineName();
            line[1] = numberFormat.format(wrapper.getNumberLine());
            line[2] = String.valueOf(wrapper.getTotal());
            line[3] = String.valueOf(wrapper.getGr3HighEdu());
            line[4] = String.valueOf(wrapper.getDoctor());
            line[5] = String.valueOf(wrapper.getCandidate());
            line[6] = String.valueOf(wrapper.getProfessor());
            line[7] = String.valueOf(wrapper.getDocent());
            line[8] = String.valueOf(wrapper.getStaffRate025());
            line[9] = String.valueOf(wrapper.getStaffRate050());
            line[10] = String.valueOf(wrapper.getStaffRate075());
            line[11] = String.valueOf(wrapper.getWomen());
            line[12] = String.valueOf(wrapper.getProfQuali());

            lineList.add(line);
        }

        tableModifier.put("T1", lineList.toArray(new String[lineList.size()][13]));
        tableModifier.put("T1", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (RtfRow row : newRowList)
                {
                    RtfCell cell = row.getCellList().get(0);
                    Integer indent = parseLevels(cell, null);

                    if (indent != null)
                    {
                        for (IRtfElement element : new ArrayList<>(cell.getElementList()))
                        {
                            if (element instanceof IRtfControl)
                            {
                                if (((IRtfControl) element).getIndex() == IRtfData.LI)
                                {
                                    int i = cell.getElementList().indexOf(element);
                                    IRtfControl newControl = RtfBean.getElementFactory().createRtfControl(IRtfData.LI, indent * 200);
                                    cell.getElementList().add(i, newControl);
                                    cell.getElementList().remove(element);
                                }
                            }
                        }
                    }
                }
            }
                        @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (colIndex == 0)
                {
                    if (value.contains("par"))
                    {
                        String[] pars = value.split("par");
                        RtfString rtfString = new RtfString();

                        rtfString.append(pars[0]).append(IRtfData.LINE).append(pars[1]);

                        return rtfString.toList();
                    }
                }

                return null;
            }
        });
    }

    /**
     * Заполняется метка для таблицы Распределение численности основного персонала по уровню образования
     */
    protected void injectEduCombTable(final RtfTableModifier tableModifier, final Model model)
    {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMinimumIntegerDigits(2);

        List<String[]> lineList = new ArrayList<>();

        for (ReportEducationLineWrapper wrapper : model.getReportEducationCombinationLineWrappers())
        {
            String[] line = new String[13];

            line[0] = (wrapper.getLevel() - 1) + LEVEL + wrapper.getReportLineName();
            line[1] = numberFormat.format(wrapper.getNumberLine());
            line[2] = String.valueOf(wrapper.getTotal());
            line[3] = String.valueOf(wrapper.getGr3HighEdu());
            line[4] = String.valueOf(wrapper.getDoctor());
            line[5] = String.valueOf(wrapper.getCandidate());
            line[6] = String.valueOf(wrapper.getProfessor());
            line[7] = String.valueOf(wrapper.getDocent());
            line[8] = String.valueOf(wrapper.getStaffRate025());
            line[9] = String.valueOf(wrapper.getStaffRate050());
            line[10] = String.valueOf(wrapper.getStaffRate075());
            line[11] = String.valueOf(wrapper.getWomen());
            line[12] = String.valueOf(wrapper.getProfQuali());

            lineList.add(line);
        }

        tableModifier.put("T2", lineList.toArray(new String[lineList.size()][13]));
        tableModifier.put("T2", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (RtfRow row : newRowList)
                {
                    RtfCell cell = row.getCellList().get(0);
                    Integer indent = parseLevels(cell, null);

                    if (indent != null)
                    {
                        for (IRtfElement element : new ArrayList<>(cell.getElementList()))
                        {
                            if (element instanceof IRtfControl)
                            {
                                if (((IRtfControl) element).getIndex() == IRtfData.LI)
                                {
                                    int i = cell.getElementList().indexOf(element);
                                    IRtfControl newControl = RtfBean.getElementFactory().createRtfControl(IRtfData.LI, indent * 200);
                                    cell.getElementList().add(i, newControl);
                                    cell.getElementList().remove(element);
                                }
                            }
                        }
                    }
                }
            }
                        @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (colIndex == 0)
                {
                    if (value.contains("par"))
                    {
                        String[] pars = value.split("par");
                        RtfString rtfString = new RtfString();

                        rtfString.append(pars[0]).append(IRtfData.LINE).append(pars[1]);

                        return rtfString.toList();
                    }
                }

                return null;
            }
        });
    }

    /**
     * Заполняется метка для таблицы Распределение персонала по стажу работы
     */
    protected void injectServiceLengthTable(final RtfTableModifier tableModifier, final Model model)
    {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMinimumIntegerDigits(2);

         List<String[]> lineList = new ArrayList<>();

        for (ReportSrviceLengthLineWrapper wrapper : model.getReportSrviceLengthLineWrappers())
        {
            String[] line = new String[16];
            line[0] = (wrapper.getLevel() - 1) + LEVEL + wrapper.getReportLineName();
            line[1] = numberFormat.format(wrapper.getNumberLine());
            line[2] = String.valueOf(wrapper.getCommon());
            line[3] = String.valueOf(wrapper.getCommon3());
            line[4] = String.valueOf(wrapper.getCommon3_5());
            line[5] = String.valueOf(wrapper.getCommon5_10());
            line[6] = String.valueOf(wrapper.getCommon10_15());
            line[7] = String.valueOf(wrapper.getCommon15_20());
            line[8] = String.valueOf(wrapper.getCommon20());
            line[9] = String.valueOf(wrapper.getEducator());
            line[10] = String.valueOf(wrapper.getEducator3());
            line[11] = String.valueOf(wrapper.getEducator3_5());
            line[12] = String.valueOf(wrapper.getEducator5_10());
            line[13] = String.valueOf(wrapper.getEducator10_15());
            line[14] = String.valueOf(wrapper.getEducator15_20());
            line[15] = String.valueOf(wrapper.getEducator20());

            lineList.add(line);
        }

        tableModifier.put("T3", lineList.toArray(new String[lineList.size()][15]));
        tableModifier.put("T3", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (RtfRow row : newRowList)
                {
                    RtfCell cell = row.getCellList().get(0);
                    Integer indent = parseLevels(cell, null);

                    if (indent != null)
                    {
                        for (IRtfElement element : new ArrayList<>(cell.getElementList()))
                        {
                            if (element instanceof IRtfControl)
                            {
                                if (((IRtfControl) element).getIndex() == IRtfData.LI)
                                {
                                    int i = cell.getElementList().indexOf(element);
                                    IRtfControl newControl = RtfBean.getElementFactory().createRtfControl(IRtfData.LI, indent * 200);
                                    cell.getElementList().add(i, newControl);
                                    cell.getElementList().remove(element);
                                }
                            }
                        }
                    }
                }
            }
                        @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (colIndex == 0)
                {
                    if (value.contains("par"))
                    {
                        String[] pars = value.split("par");
                        RtfString rtfString = new RtfString();

                        rtfString.append(pars[0]).append(IRtfData.LINE).append(pars[1]);

                        return rtfString.toList();
                    }
                }

                return null;
            }
        });
    }

    /**
     * Заполняется метка для таблицы Распределение персонала по стажу работы
     */
    protected void injectSexTable(final RtfTableModifier tableModifier, final Model model)
    {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMinimumIntegerDigits(2);

        List<String[]> lineList = new ArrayList<>();

        for (ReportSexLineWrapper wrapper : model.getReportSexLineWrappers())
        {
            String[] line = new String[23];

            line[0] = (wrapper.getLevel() - 1) + LEVEL + wrapper.getReportLineName();
            line[1] = numberFormat.format(wrapper.getNumberLine());
            line[2] = String.valueOf(wrapper.getTotal());
            line[3] = String.valueOf(wrapper.getTotal25());
            line[4] = String.valueOf(wrapper.getWomen25());
            line[5] = String.valueOf(wrapper.getTotal25_29());
            line[6] = String.valueOf(wrapper.getWomen25_29());
            line[7] = String.valueOf(wrapper.getTotal30_34());
            line[8] = String.valueOf(wrapper.getWomen30_34());
            line[9] = String.valueOf(wrapper.getTotal35_39());
            line[10] = String.valueOf(wrapper.getWomen35_39());
            line[11] = String.valueOf(wrapper.getTotal40_44());
            line[12] = String.valueOf(wrapper.getWomen40_44());
            line[13] = String.valueOf(wrapper.getTotal45_49());
            line[14] = String.valueOf(wrapper.getWomen45_49());
            line[15] = String.valueOf(wrapper.getTotal50_54());
            line[16] = String.valueOf(wrapper.getWomen50_54());
            line[17] = String.valueOf(wrapper.getTotal55_59());
            line[18] = String.valueOf(wrapper.getWomen55_59());
            line[19] = String.valueOf(wrapper.getTotal60_64());
            line[20] = String.valueOf(wrapper.getWomen60_64());
            line[21] = String.valueOf(wrapper.getTotal65());
            line[22] = String.valueOf(wrapper.getWomen65());

            lineList.add(line);
        }

        tableModifier.put("T4", lineList.toArray(new String[lineList.size()][23]));
        tableModifier.put("T4", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (RtfRow row : newRowList)
                {
                    RtfCell cell = row.getCellList().get(0);
                    Integer indent = parseLevels(cell, null);

                    if (indent != null)
                    {
                        for (IRtfElement element : new ArrayList<>(cell.getElementList()))
                        {
                            if (element instanceof IRtfControl)
                            {
                                if (((IRtfControl) element).getIndex() == IRtfData.LI)
                                {
                                    int i = cell.getElementList().indexOf(element);
                                    IRtfControl newControl = RtfBean.getElementFactory().createRtfControl(IRtfData.LI, indent * 200);
                                    cell.getElementList().add(i, newControl);
                                    cell.getElementList().remove(element);
                                }
                            }
                        }
                    }
                }
            }
                        @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (colIndex == 0)
                {
                    if (value.contains("par"))
                    {
                        String[] pars = value.split("par");
                        RtfString rtfString = new RtfString();

                        rtfString.append(pars[0]).append(IRtfData.LINE).append(pars[1]);

                        return rtfString.toList();
                    }
                }

                return null;
            }
        });
    }

    private Integer parseLevels(RtfCell cell, Integer indent)
    {
        for (IRtfElement element : cell.getElementList())
        {
            if (element instanceof IRtfText)
                indent = parseLevel((IRtfText) element, indent);
            else if (element instanceof IRtfGroup)
            {
                IRtfGroup group = (IRtfGroup) element;
                for (IRtfElement grouElement : group.getElementList())
                {
                    if (grouElement instanceof IRtfText)
                    {
                        indent = parseLevel((IRtfText) grouElement, indent);
                    }
                }
            }
        }
        return indent;
    }

    private Integer parseLevel(IRtfText text, Integer indent)
    {
        String string = text.getString();
        if (string.contains(LEVEL))
        {
            String[] levels = string.split(LEVEL);
            indent = Integer.valueOf(levels[0]);
            text.setString(levels[1]);
        }
        return indent;
    }

    /**
     * Генерирует и сохраняет отчет в базу данных
     */
    @Override
    public void update(Model model)
    {
        EmployeeVPO1Report report = model.getReport();
        report.setFormingDate(model.getFormingDate());
        report.setReportDate(model.getReportDate());
        report.setAgeDate(model.getAgeDate());

        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(RtfUtil.toByteArray(getDocument(model)));
        getSession().save(databaseFile);

        report.setContent(databaseFile);
        getSession().save(report);
    }

    /**
     * Вычисляет дату Предыдущего учебного года относительно Даты отчета.
     * @param reportDate дата отчета
     * @return <code>Pair<дата начала учебного года, дата окончания учебного года></code>
     */
    protected CoreCollectionUtils.Pair<Date, Date> getEduYearDates(Date reportDate)
    {
        Calendar currentEnd = new GregorianCalendar();
        currentEnd.set(Calendar.MONTH, 8);
        currentEnd.set(Calendar.DATE, 31);
        currentEnd.set(Calendar.HOUR_OF_DAY, 23);
        currentEnd.set(Calendar.MINUTE, 59);
        currentEnd.set(Calendar.SECOND, 59);
        currentEnd.set(Calendar.MILLISECOND, 999);

        if (reportDate.getTime() > currentEnd.getTime().getTime())
        {
            Calendar begin = new GregorianCalendar();
            begin.add(Calendar.YEAR, -1);
            begin.set(Calendar.MONTH, 9);
            begin.set(Calendar.DATE, 1);

            Calendar end = new GregorianCalendar();
            end.set(Calendar.MONTH, 8);
            end.set(Calendar.DATE, 31);

            return new CoreCollectionUtils.Pair<>(begin.getTime(), end.getTime());
        }
        else
        {
            Calendar begin = new GregorianCalendar();
            begin.add(Calendar.YEAR, -2);
            begin.set(Calendar.MONTH, 9);
            begin.set(Calendar.DATE, 1);

            Calendar end = new GregorianCalendar();
            end.add(Calendar.YEAR, -1);
            end.set(Calendar.MONTH, 8);
            end.set(Calendar.DATE, 31);

            return new CoreCollectionUtils.Pair<>(begin.getTime(), end.getTime());
        }
    }
}