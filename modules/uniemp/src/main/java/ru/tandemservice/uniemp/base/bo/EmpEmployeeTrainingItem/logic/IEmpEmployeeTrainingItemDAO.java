/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniemp.entity.employee.EmployeeTrainingItem;

/**
 * Create by ashaburov
 * Date 26.03.12
 */
public interface IEmpEmployeeTrainingItemDAO extends INeedPersistenceSupport
{
    void saveOrUpdateItem(EmployeeTrainingItem item);
}
