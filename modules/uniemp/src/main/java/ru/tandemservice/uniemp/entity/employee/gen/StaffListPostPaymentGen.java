package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выплата должности штатного расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffListPostPaymentGen extends StaffListPaymentBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.StaffListPostPayment";
    public static final String ENTITY_NAME = "staffListPostPayment";
    public static final int VERSION_HASH = -816546946;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StaffListPostPaymentGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffListPostPaymentGen> extends StaffListPaymentBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffListPostPayment.class;
        }

        public T newInstance()
        {
            return (T) new StaffListPostPayment();
        }
    }
    private static final Path<StaffListPostPayment> _dslPath = new Path<StaffListPostPayment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffListPostPayment");
    }
            

    public static class Path<E extends StaffListPostPayment> extends StaffListPaymentBase.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return StaffListPostPayment.class;
        }

        public String getEntityName()
        {
            return "staffListPostPayment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
