/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.logic;

import jxl.write.WritableCellFormat;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 25.01.13
 */
public class EmpServiceLengthReportModel
{
    public static final String ARIAL_8_EMPTY_BORDER = "arial8Empty";
    public static final String ARIAL_8_MID_RIGHT_BORDER = "arial8MidRightBorder";
    public static final String ARIAL_8_MID_BOT_BORDER = "arial8MidBotBorder";
    public static final String ARIAL_8_BOT_BORDER = "arial8BotBorder";
    public static final String ARIAL_8_RIGHT_BOT_BORDER = "arial8RightBotBorder";
    public static final String ARIAL_8_RIGHT_BORDER = "arial8RightBorder";
    public static final String ARIAL_8_LEFT_BORDER = "arial8LeftBorder";
    public static final String ARIAL_8_BOLD_LEFT_BORDER_GRAY = "arial8BoldLeftBorderGray";
    public static final String ARIAL_8_BOLD_RIGHT_BORDER_GRAY = "arial8BoldRightBorderGray";
    public static final String ARIAL_8_BOLD_TOP_BOT_BORDER_GRAY = "arial8BoldTopBotBorderGray";
    public static final String ARIAL_10 = "arial10";
    public static final String ARIAL_10_BOLD = "arial10Bold";

    // filters field
    private Date _formingDate = new Date();
    private List<OrgUnit> _orgUnitList;
    private boolean _orgUnitChild;
    private List<EmployeeType> _empTypeList;
    private List<PostBoundedWithQGandQL> _postList;
    private List<EmployeePostStatus> _empPostStatusList;
    private Long _ageFrom;
    private Long _ageTo;
    private Sex _sex;
    private Date _servLenghDate = new Date();
    private List<ServiceLengthType> _servLenghTypeList;

    // print field
    private Integer _row;
    private List<EmpServiceLengthReportRowWrapper> _rowWrapperList;
    private List<CoreCollectionUtils.Pair<String, Integer>> _columnList;
    private Map<String, WritableCellFormat> _fontMap;

    // Getters & Setters

    public Map<String, WritableCellFormat> getFontMap()
    {
        return _fontMap;
    }

    public void setFontMap(Map<String, WritableCellFormat> fontMap)
    {
        _fontMap = fontMap;
    }

    public Integer getRow()
    {
        return _row;
    }

    public void setRow(Integer row)
    {
        _row = row;
    }

    public List<CoreCollectionUtils.Pair<String, Integer>> getColumnList()
    {
        return _columnList;
    }

    public void setColumnList(List<CoreCollectionUtils.Pair<String, Integer>> columnList)
    {
        _columnList = columnList;
    }

    public List<EmpServiceLengthReportRowWrapper> getRowWrapperList()
    {
        return _rowWrapperList;
    }

    public void setRowWrapperList(List<EmpServiceLengthReportRowWrapper> rowWrapperList)
    {
        _rowWrapperList = rowWrapperList;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public List<OrgUnit> getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public boolean isOrgUnitChild()
    {
        return _orgUnitChild;
    }

    public void setOrgUnitChild(boolean orgUnitChild)
    {
        _orgUnitChild = orgUnitChild;
    }

    public List<EmployeeType> getEmpTypeList()
    {
        return _empTypeList;
    }

    public void setEmpTypeList(List<EmployeeType> empTypeList)
    {
        _empTypeList = empTypeList;
    }

    public List<PostBoundedWithQGandQL> getPostList()
    {
        return _postList;
    }

    public void setPostList(List<PostBoundedWithQGandQL> postList)
    {
        _postList = postList;
    }

    public List<EmployeePostStatus> getEmpPostStatusList()
    {
        return _empPostStatusList;
    }

    public void setEmpPostStatusList(List<EmployeePostStatus> empPostStatusList)
    {
        _empPostStatusList = empPostStatusList;
    }

    public Long getAgeFrom()
    {
        return _ageFrom;
    }

    public void setAgeFrom(Long ageFrom)
    {
        _ageFrom = ageFrom;
    }

    public Long getAgeTo()
    {
        return _ageTo;
    }

    public void setAgeTo(Long ageTo)
    {
        _ageTo = ageTo;
    }

    public Sex getSex()
    {
        return _sex;
    }

    public void setSex(Sex sex)
    {
        _sex = sex;
    }

    public Date getServLenghDate()
    {
        return _servLenghDate;
    }

    public void setServLenghDate(Date servLenghDate)
    {
        _servLenghDate = servLenghDate;
    }

    public List<ServiceLengthType> getServLenghTypeList()
    {
        return _servLenghTypeList;
    }

    public void setServLenghTypeList(List<ServiceLengthType> servLenghTypeList)
    {
        _servLenghTypeList = servLenghTypeList;
    }
}
