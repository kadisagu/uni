/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.View;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEdit;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.View.EmployeeView;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.View.EmployeeViewUI;
import org.tandemframework.shared.person.base.bo.Person.ui.AcademicDegreeAddEdit.PersonAcademicDegreeAddEdit;
import org.tandemframework.shared.person.base.bo.Person.ui.AcademicStatusAddEdit.PersonAcademicStatusAddEdit;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.ui.AddEdit.EmpEmployeeRetrainingItemAddEdit;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.ui.AddEdit.EmpEmployeeRetrainingItemAddEditUI;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.ui.AddEdit.EmpEmployeeTrainingItemAddEdit;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.ui.AddEdit.EmpEmployeeTrainingItemAddEditUI;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceLengthWrapper;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceWrapper;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.PeriodWrapper;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Весь код получен из legacy компонента
 *
 * @author Vasily Zhukov
 * @since 30.03.2012
 */
@SuppressWarnings("unchecked")
public class EmployeeViewExtUI extends UIAddon
{
    private EmployeeViewLegacyModel model;

    public EmployeeViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        prepareLegacyModel();
    }

    // Getters

    public EmployeeViewLegacyModel getModel()
    {
        return model;
    }

    // Listeners

    public void onClickAddEmployeeEncouragement()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYEE_ENCOURAGEMENT, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", null)
                .activate();
    }

    public void onClickAddEmployeePost()
    {
        getActivationBuilder().asDesktopRoot(EmployeePostAddEdit.class)
                .parameter(EmployeePostAddEditUI.EMPLOYEE_POST_ID, null)
                .parameter(EmployeePostAddEditUI.EMPLOYEE_ID, model.getEmployee().getId())
                .activate();
    }

    public void onClickAddMedicalSpec()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_MEDICAL_SPEC, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("employeeMedicalSpecialityId", null)
                .activate();
    }

    public void onClickAddEmployeeDegree()
    {
        getActivationBuilder().asRegion(PersonAcademicDegreeAddEdit.class, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("personAcademicDegreeId", null)
                .parameter("personId", model.getEmployee().getPerson().getId())
                .activate();
    }

    public void onClickAddEmployeeStatus()
    {
        getActivationBuilder().asRegion(PersonAcademicStatusAddEdit.class, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("personAcademicStatusId", null)
                .parameter("personId", model.getEmployee().getPerson().getId())
                .activate();
    }

    public void onClickEditAdditionalData()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADDITIONAL_DATA_EDIT, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("employeeCardId", model.getEmployeeCard() == null ? null : model.getEmployeeCard().getId())
                .activate();
    }

    public void onClickAddServiceLength()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_SERVICE_LENGTH_ADD_EDIT, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", null)
                .activate();
    }

    public void onClickCountEmployeeServiceLength()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_SERVICE_LENGTH_COUNTER, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .activate();
    }

    public void onClickAddSickList()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_SICK_LIST, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", null)
                .activate();
    }

    public void onClickAddTrainingItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeTrainingItemAddEdit.class, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter(EmpEmployeeTrainingItemAddEditUI.EMPLOYEE_ID, model.getEmployee().getId())
                .parameter(EmpEmployeeTrainingItemAddEditUI.ITEM_ID, null)
                .activate();
    }

    public void onClickAddRetrainingItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeRetrainingItemAddEdit.class, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter(EmpEmployeeRetrainingItemAddEditUI.EMPLOYEE_ID, model.getEmployee().getId())
                .parameter(EmpEmployeeRetrainingItemAddEditUI.ITEM_ID, null)
                .activate();
    }

    public void onClickAddExternalEmployment()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYMENT_HISTORY_ITEM_EXT, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", null)
                .activate();
    }

    public void onClickAddInternalEmployment()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYMENT_HISTORY_ITEM_INT, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", null)
                .activate();
    }

    public void onClickAddArchiveEmployment()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYMENT_HISTORY_ITEM_ARCH, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", null)
                .activate();
    }

    // Filter Listeners

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EmployeeView.EMPLOYEE_POST_DS))
        {
            dataSource.put("actual", model.isActual());
        }
    }

    public void onClickSearchServiceLength()
    {
        model.getEmployeeServiceLengthDataSource().showFirstPage();
        model.getEmployeeServiceLengthDataSource().refresh();
    }

    public void onClickClearServiceLength()
    {
        IDataSettings settings = getPresenter().getSettings().getSettings();
        settings.removeAll("employeeServiceLength");
        onClickSearchServiceLength();
    }

    public void onClickSearchEmployment()
    {
        model.getEmploymentHistoryDataSource().showFirstPage();
        model.getEmploymentHistoryDataSource().refresh();
    }

    public void onClickClearEmployment()
    {
        model.setPostFilter(null);
        model.setAssignDateFilter(null);
        model.setDismissalDateFilter(null);
        onClickSearchEmployment();
    }

    public void onClickSearchEmployeePostStatus()
    {
        getPresenter().getConfig().<BaseSearchListDataSource>getDataSource(EmployeeView.EMPLOYEE_POST_DS).getLegacyDataSource().refresh();
    }

    // DataSource Listeners

    public void onDeleteRowFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        getPresenterConfig().getBusinessComponent().refresh();
    }

    public void onClickEditMedicalSpec()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_MEDICAL_SPEC, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("employeeMedicalSpecialityId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditScDegree()
    {
        getActivationBuilder().asRegion(PersonAcademicDegreeAddEdit.class, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("personAcademicDegreeId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditScStatus()
    {
        getActivationBuilder().asRegion(PersonAcademicStatusAddEdit.class, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("personAcademicStatusId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditSickList()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_SICK_LIST, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", getListenerParameterAsLong())
                .activate();
    }

    public void onEditEmploymentHistoryItem()
    {
        Long id = getListenerParameterAsLong();
        EmploymentHistoryItemBase item = DataAccessServices.dao().getNotNull(id);

        if (item instanceof EmploymentHistoryItemInner)
            getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYMENT_HISTORY_ITEM_INT, EmployeeView.EMPLOYEE_TAB_PANEL)
                    .parameter("employeeId", model.getEmployee().getId())
                    .parameter("rowId", id)
                    .activate();
        else if (item instanceof EmploymentHistoryItemExt)
            getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYMENT_HISTORY_ITEM_EXT, EmployeeView.EMPLOYEE_TAB_PANEL)
                    .parameter("employeeId", model.getEmployee().getId())
                    .parameter("rowId", id)
                    .activate();
        else
            getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYMENT_HISTORY_ITEM_ARCH, EmployeeView.EMPLOYEE_TAB_PANEL)
                    .parameter("employeeId", model.getEmployee().getId())
                    .parameter("rowId", id)
                    .activate();
    }

    public void onClickEditEmployeeEncouragement()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_ADD_EMPLOYEE_ENCOURAGEMENT, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickEditEmployeeServiceLength()
    {
        getActivationBuilder().asRegion(IUniempComponents.EMPLOYEE_SERVICE_LENGTH_ADD_EDIT, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter("employeeId", model.getEmployee().getId())
                .parameter("rowId", getListenerParameterAsLong())
                .activate();
    }

    public void onClickPrintEduDocumentTraining()
    {
        EmployeeTrainingItem item = DataAccessServices.dao().getNotNull(EmployeeTrainingItem.class, getListenerParameterAsLong());

        byte[] content = item.getEduDocumentFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл документа пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(item.getEduDocumentFileName()).contentType(item.getEduDocumentFileType()).document(content), false);
    }

    public void onClickEditTrainingItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeTrainingItemAddEdit.class, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter(EmpEmployeeTrainingItemAddEditUI.EMPLOYEE_ID, model.getEmployee().getId())
                .parameter(EmpEmployeeTrainingItemAddEditUI.ITEM_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onClickPrintEduDocumentRetraining()
    {
        EmployeeRetrainingItem item = DataAccessServices.dao().getNotNull(EmployeeRetrainingItem.class, getListenerParameterAsLong());

        byte[] content = item.getEduDocumentFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл документа пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(item.getEduDocumentFileName()).contentType(item.getEduDocumentFileType()).document(content), false);
    }

    public void onClickEditRetrainingItem()
    {
        getActivationBuilder().asRegion(EmpEmployeeRetrainingItemAddEdit.class, EmployeeView.EMPLOYEE_TAB_PANEL)
                .parameter(EmpEmployeeRetrainingItemAddEditUI.EMPLOYEE_ID, model.getEmployee().getId())
                .parameter(EmpEmployeeRetrainingItemAddEditUI.ITEM_ID, getListenerParameterAsLong())
                .activate();
    }

    // Private

    private void prepareLegacyModel()
    {
        // controller
        model = new EmployeeViewLegacyModel();
        model.setBaseModel(getPresenter());

        // dao prepare
        EmployeeCard employeeCard = UniempDAO.getEmployeeCard(model.getEmployee(), getSession());
        if (employeeCard != null)
            model.setEmployeeCard(employeeCard);
        model.setEmployeeCardTypePPS(true);
        model.setServiceLengthTypeList(CatalogManager.instance().dao().getCatalogItemList(ServiceLengthType.class));
        model.setActual(true);
        MQBuilder builder = new MQBuilder(EmploymentHistoryItemBase.ENTITY_CLASS, "emnt");
        builder.add(MQExpression.eq("emnt", EmploymentHistoryItemBase.L_EMPLOYEE, model.getEmployee()));
        for (EmploymentHistoryItemBase item : builder.<EmploymentHistoryItemBase>getResultList(getSession()))
            model.getEmploymentDuration().addDatePeriod(item.getAssignDate(), item.getDismissalDate());
        model.getEmploymentDuration().calculateEmploymentPeriod();

        // controller
        IBusinessComponent component = getPresenterConfig().getBusinessComponent();
        prepareMedicalSpecDataSource(component);
        prepareScienceDegreeDataSource(component);
        prepareScienceStatusDataSource(component);
        prepareSickListDataSource(component);
        prepareEmployementHistoryDataSource(component);
        prepareEncouragementsDataSource(component);
        prepareServiceLengthDataSource(component);
        prepareServiceLengthTotalDataSource(component);
        prepareTrainingDataSource(component);
        prepareRetrainingDataSource(component);
    }

    private void prepareMedicalSpecDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeMedicalSpeciality> dataSource = new DynamicListDataSource<>(component, context -> {
            MQBuilder builder = new MQBuilder(EmployeeMedicalSpeciality.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", EmployeeMedicalSpeciality.L_EMPLOYEE, model.getEmployee()));
            new OrderDescriptionRegistry("e").applyOrder(builder, model.getMedicalSpecDataSource().getEntityOrder());
            UniBaseUtils.createPage(model.getMedicalSpecDataSource(), builder, getSession());
        }, 5);

        dataSource.addColumn(new SimpleColumn("Врачебная специальность", EmployeeMedicalSpeciality.medicalEducationLevel().title().s()));
        dataSource.addColumn(new SimpleColumn("Дата присуждения", EmployeeMedicalSpeciality.assignDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата подтверждения", EmployeeMedicalSpeciality.acceptionDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Квалификационная категория", EmployeeMedicalSpeciality.medicalQualificationCategory().shortTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата присуждения категории", EmployeeMedicalSpeciality.categoryAssignDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата подтверждения категории", EmployeeMedicalSpeciality.categoryAcceptionDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Имеется сертификат специалиста", EmployeeMedicalSpeciality.P_CERTIFICATE_AVAILABLE));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("uniempEmployeeViewExtUI:onClickPrintMedicalSpec", "Печатать файл сертификата").setDisabledProperty(EmployeeMedicalSpeciality.P_PRINTING_DISABLED).setPermissionKey("printMedicalSpecialityCertificate__employeePost"));

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeeViewExtUI:onClickEditMedicalSpec").setPermissionKey("editMedicalSpec_employee"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeeViewExtUI:onDeleteRowFromList", "Удалить врачебную специальность?").setPermissionKey("deleteMedicalSpec_employee"));
        }

        model.setMedicalSpecDataSource(dataSource);
    }

    public void onClickPrintMedicalSpec() {
        EmployeeMedicalSpeciality speciality = IUniBaseDao.instance.get().get(EmployeeMedicalSpeciality.class, getListenerParameterAsLong());
        byte[] content = speciality.getExpertCertificate().getContent();
        if (content == null)
            throw new ApplicationException("Файл сертификата пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .contentType(speciality.getCertificateFileType())
            .fileName(speciality.getCertificateFileName())
            .document(content), true);
    }

    private void prepareScienceDegreeDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<PersonAcademicDegree> dataSource = new DynamicListDataSource<>(component, context -> {
            MQBuilder builder = new MQBuilder(PersonAcademicDegree.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", PersonAcademicDegree.person(), model.getEmployee().getPerson()));
            new OrderDescriptionRegistry("e").applyOrder(builder, model.getScienceDegreeDataSource().getEntityOrder());
            UniBaseUtils.createPage(model.getScienceDegreeDataSource(), builder, getSession());
        }, 5);

//        dataSource.addColumn(new SimpleColumn("Ученая степень", PersonAcademicDegree.academicDegree().title().s()));
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Ученая степень", PersonAcademicDegree.academicDegree().title().s());
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                ParametersMap map = new ParametersMap();
                map.add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
                map.add(ISecureRoleContext.SECURE_ROLE_CONTEXT, ((EmployeeViewUI) getPresenter()).getSecureRoleContext());
                map.add("accessible", model.isAccessible());
                return map;
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Отрасль науки", PersonAcademicDegree.scienceBranch().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата присуждения", PersonAcademicDegree.date().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("uniempEmployeeViewExtUI:onClickPrintPersonAcademicDegreeDocument", "Печатать файл диплома").setDisabledProperty(PersonAcademicDegree.P_PRINTING_DISABLED).setPermissionKey("printDegree_employee"));

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeeViewExtUI:onClickEditScDegree").setPermissionKey("editDegree_employee"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeeViewExtUI:onDeleteRowFromList", "Удалить ученую степень?").setPermissionKey("deleteDegree_employee"));
        }

        model.setScienceDegreeDataSource(dataSource);
    }

    public void onClickPrintPersonAcademicDegreeDocument() {
        PersonAcademicDegree academicStatus = IUniBaseDao.instance.get().get(PersonAcademicDegree.class, getListenerParameterAsLong());
        byte[] content = academicStatus.getDiplomFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл диплома пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .contentType(academicStatus.getDiplomFileType())
            .fileName(academicStatus.getDiplomFileName())
            .document(content), true);
    }

    private void prepareScienceStatusDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<PersonAcademicStatus> dataSource = new DynamicListDataSource<>(component, context -> {
            MQBuilder builder = new MQBuilder(PersonAcademicStatus.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", PersonAcademicStatus.person(), model.getEmployee().getPerson()));
            new OrderDescriptionRegistry("e").applyOrder(builder, model.getScienceStatusDataSource().getEntityOrder());
            UniBaseUtils.createPage(model.getScienceStatusDataSource(), builder, getSession());
        }, 5);

//        dataSource.addColumn(new SimpleColumn("Ученое звание", PersonAcademicStatus.academicStatus().title().s()));
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Ученое звание", PersonAcademicStatus.academicStatus().title().s());
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                ParametersMap map = new ParametersMap();
                map.add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
                map.add(ISecureRoleContext.SECURE_ROLE_CONTEXT, ((EmployeeViewUI) getPresenter()).getSecureRoleContext());
                map.add("accessible", model.isAccessible());
                return map;
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Дата присуждения", PersonAcademicStatus.date().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("uniempEmployeeViewExtUI:onClickPrintPersonAcademicStatusDocument", "Печатать файл аттестата").setDisabledProperty(PersonAcademicStatus.P_PRINTING_DISABLED).setPermissionKey("printStatus_employee"));

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeeViewExtUI:onClickEditScStatus").setPermissionKey("editStatus_employee"));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeeViewExtUI:onDeleteRowFromList", "Удалить ученое звание?").setPermissionKey("deleteStatus_employee"));
        }

        model.setScienceStatusDataSource(dataSource);
    }

    public void onClickPrintPersonAcademicStatusDocument() {
        PersonAcademicStatus academicStatus = IUniBaseDao.instance.get().get(PersonAcademicStatus.class, getListenerParameterAsLong());
        byte[] content = academicStatus.getCertificateFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл аттестата пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
            .contentType(academicStatus.getCertificateFileType())
            .fileName(academicStatus.getCertificateFileName())
            .document(content), true);
    }


    private void prepareSickListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeSickList> dataSource = new DynamicListDataSource<>(component, context -> {
            OrderDirection orderDirection = model.getEmployeeSickListDataSource().getEntityOrder() != null ? model.getEmployeeSickListDataSource().getEntityOrder().getDirection() : OrderDirection.desc;

            MQBuilder builder = new MQBuilder(EmployeeSickList.ENTITY_CLASS, "esl");
            builder.add(MQExpression.eq("esl", EmployeeSickList.L_EMPLOYEE, model.getEmployee()));
            builder.addOrder("esl", EmployeeSickList.P_CLOSE_DATE, orderDirection);
            UniBaseUtils.createPage(model.getEmployeeSickListDataSource(), builder.<EmployeeSickList>getResultList(getSession()));
        }, 10);

        dataSource.addColumn(new SimpleColumn("Номер", EmployeeSickList.number().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата открытия", EmployeeSickList.openDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата закрытия", EmployeeSickList.closeDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Мед. учреждение", EmployeeSickList.medicalFoundation().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Основание", EmployeeSickList.basic().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Номер листа продления", EmployeeSickList.prolongNumber().s()).setClickable(false).setOrderable(false));

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("", ActionColumn.EDIT, "uniempEmployeeViewExtUI:onClickEditSickList").setPermissionKey("editSickList_employee"));
            dataSource.addColumn(new ActionColumn("", ActionColumn.DELETE, "uniempEmployeeViewExtUI:onDeleteRowFromList", "Удалить лист нетрудоспособности?").setPermissionKey("deleteSickList_employee"));
        }

        //dataSource.changeOrder(1);//TODO: hack
        model.setEmployeeSickListDataSource(dataSource);
    }

    private void prepareEmployementHistoryDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmploymentHistoryItemBase> dataSource = new DynamicListDataSource<>(component, context -> {
            MQBuilder excludeBuilder = new MQBuilder(EmploymentHistoryItemFake.ENTITY_CLASS, "ex", new String[]{EmploymentHistoryItemFake.P_ID})
                    .add(MQExpression.eq("ex", EmploymentHistoryItemFake.L_EMPLOYEE, model.getEmployee()));

            MQBuilder builder = new MQBuilder(EmploymentHistoryItemBase.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", EmploymentHistoryItemBase.L_EMPLOYEE, model.getEmployee()));
            builder.add(MQExpression.notIn("e", EmploymentHistoryItemBase.P_ID, excludeBuilder));
            if (null != model.getAssignDateFilter() && null != model.getDismissalDateFilter())
            {
                AbstractExpression expr1 = MQExpression.between("e", EmploymentHistoryItemBase.P_ASSIGN_DATE, model.getAssignDateFilter(), model.getDismissalDateFilter());
                AbstractExpression expr2 = MQExpression.between("e", EmploymentHistoryItemBase.P_DISMISSAL_DATE, model.getAssignDateFilter(), model.getDismissalDateFilter());
                builder.add(MQExpression.or(expr1, expr2));
            } else if (null != model.getAssignDateFilter())
            {
                AbstractExpression expr1 = MQExpression.between("e", EmploymentHistoryItemBase.P_ASSIGN_DATE, model.getAssignDateFilter(), new Date());
                AbstractExpression expr2 = MQExpression.between("e", EmploymentHistoryItemBase.P_DISMISSAL_DATE, model.getAssignDateFilter(), new Date());
                builder.add(MQExpression.or(expr1, expr2));
            } else if (null != model.getDismissalDateFilter())
            {
                builder.add(MQExpression.lessOrEq("e", EmploymentHistoryItemBase.P_ASSIGN_DATE, model.getDismissalDateFilter()));
            }

            new OrderDescriptionRegistry("e").applyOrder(builder, model.getEmploymentHistoryDataSource().getEntityOrder());

            boolean starts = null != model.getPostFilter() && model.getPostFilter().startsWith("!");
            String prepFilter = null != model.getPostFilter() ? model.getPostFilter().replace('*', ' ').replaceAll(" ", "").replaceAll("!", "").toUpperCase() : null;
            List<EmploymentHistoryItemBase> itemsList = new ArrayList<>();
            for (EmploymentHistoryItemBase item : builder.<EmploymentHistoryItemBase>getResultList(getSession()))
                if (null == prepFilter || (starts ? item.getPostTitle().toUpperCase().startsWith(prepFilter) : item.getPostTitle().toUpperCase().contains(prepFilter)))
                    itemsList.add(item);

            UniBaseUtils.createPage(model.getEmploymentHistoryDataSource(), itemsList);

            DQLSelectBuilder relationBuilder = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column("b")
                    .where(in(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("b")), itemsList))
                    .order(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().title().fromAlias("b")));

            Map<EmploymentHistoryItemBase, List<EmpHistoryToServiceLengthTypeRelation>> relationMap = new HashMap<>();
            for (EmpHistoryToServiceLengthTypeRelation relation : relationBuilder.createStatement(getSession()).<EmpHistoryToServiceLengthTypeRelation>list())
            {
                List<EmpHistoryToServiceLengthTypeRelation> list = relationMap.get(relation.getEmploymentHistoryItemBase());
                if (list == null)
                    relationMap.put(relation.getEmploymentHistoryItemBase(), list = new ArrayList<>());
                list.add(relation);
            }

            for (ViewWrapper<EmploymentHistoryItemBase> wrapper : ViewWrapper.<EmploymentHistoryItemBase>getPatchedList(model.getEmploymentHistoryDataSource()))
            {
                wrapper.setViewProperty("serviceLengthTypeList", relationMap.get(wrapper.getEntity()));
            }
        }, 15);

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Основание", EmploymentHistoryItemBase.P_BASICS);
        linkColumn.setOrderable(false);
        linkColumn.setDisableHandler(entity -> {
            EmploymentHistoryItemBase holiday = ((ViewWrapper<EmploymentHistoryItemBase>) entity).getEntity();
            if (holiday.getExtract() == null || holiday.getExtractNumber() == null || holiday.getExtractDate() == null)
                return true;

            String orderNumber = (String) holiday.getExtract().getProperty("paragraph.order.number");
            Date orderDate = (Date) holiday.getExtract().getProperty("paragraph.order.commitDate");

            return orderNumber == null || orderDate == null || !orderNumber.equals(holiday.getExtractNumber()) || !(orderDate.getTime() == holiday.getExtractDate().getTime());

        });
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                EmploymentHistoryItemBase employmentHistoryItemBase = ((ViewWrapper<EmploymentHistoryItemBase>) entity).getEntity();
                if (employmentHistoryItemBase.getExtract().getProperty("type.parent.code").equals("3"))
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, employmentHistoryItemBase.getExtract().getId());
                else
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, employmentHistoryItemBase.getExtract().getProperty("paragraph.order.id"));
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                EmploymentHistoryItemBase employmentHistoryItemBase = ((ViewWrapper<EmploymentHistoryItemBase>) entity).getEntity();
                if (employmentHistoryItemBase.getExtract().getProperty("type.parent.code").equals("3"))
                    return "ru.tandemservice.moveemployee.component.singleemplextract.e" +
                            employmentHistoryItemBase.getExtract().getProperty("type.index") +
                            ".Pub";
                else
                    return null;
            }
        });

        dataSource.addColumn(new BooleanColumn("Текущая", EmploymentHistoryItemBase.P_CURRENT));
        dataSource.addColumn(new SimpleColumn("Должность", EmploymentHistoryItemBase.P_POST_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", EmploymentHistoryItemBase.P_ORG_UNIT_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Организация", EmploymentHistoryItemBase.P_ORGANIZATION_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата назначения", EmploymentHistoryItemBase.P_ASSIGN_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата окончания", EmploymentHistoryItemBase.P_DISMISSAL_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Суммарная ставка", EmploymentHistoryItemBase.P_STAFF_RATE, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип назнач. на долж.", EmploymentHistoryItemBase.postType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Учитывать при расчете стажа", "serviceLengthTypeList", new CollectionFormatter(source -> ((EmpHistoryToServiceLengthTypeRelation) source).getServiceLengthType().getShortTitle())));
        dataSource.addColumn(new SimpleColumn("Всего дней", EmploymentHistoryItemBase.P_EMPLOYMENT_DURATION_FULL_DAYS_AMOUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Лет", EmploymentHistoryItemBase.P_EMPLOYMENT_DURATION_YEARS_AMOUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Месяцев", EmploymentHistoryItemBase.P_EMPLOYMENT_DURATION_MONTHS_AMOUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дней", EmploymentHistoryItemBase.P_EMPLOYMENT_DURATION_DAYS_AMOUNT).setClickable(false).setOrderable(false));

        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать запись", ActionColumn.EDIT, "uniempEmployeeViewExtUI:onEditEmploymentHistoryItem").setPermissionKey("editEmploymentHistoryItem_employee"));
            dataSource.addColumn(new ActionColumn("Удалить запись", ActionColumn.DELETE, "uniempEmployeeViewExtUI:onDeleteRowFromList", "Удалить запись {0} из истории должностей?", EmploymentHistoryItemBase.P_FULL_TITLE).setPermissionKey("deleteEmploymentHistoryItem_employee"));
        }

        dataSource.changeOrder(3);//TODO: hack
        model.setEmploymentHistoryDataSource(dataSource);
    }

    private void prepareEncouragementsDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeEncouragement> dataSource = new DynamicListDataSource<>(component, component1 -> {
            MQBuilder builder = new MQBuilder(EmployeeEncouragement.ENTITY_CLASS, "e");
            builder.add(MQExpression.eq("e", EmployeeEncouragement.L_EMPLOYEE, model.getEmployee()));
            new OrderDescriptionRegistry("e").applyOrder(builder, model.getEmployeeEncouragementsListDataSource().getEntityOrder());
            UniBaseUtils.createPage(model.getEmployeeEncouragementsListDataSource(), builder, getSession());
        }, 15);

        dataSource.addColumn(new SimpleColumn("Название", EmployeeEncouragement.P_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип поощрения/награды", EmployeeEncouragement.KEY_ENCOURAGEMENT_TYPE_TITLE).setClickable(false));

        HeadColumn documentColumn = (HeadColumn) new HeadColumn("documents", "Документ").setHeaderAlign("center");
        dataSource.addColumn(documentColumn);
        documentColumn.addColumn(new SimpleColumn("Наименование", EmployeeEncouragement.document().s()).setClickable(false));
        documentColumn.addColumn(new SimpleColumn("Номер", EmployeeEncouragement.documentNumber().s()).setClickable(false));
        documentColumn.addColumn(new SimpleColumn("Дата присуждения", EmployeeEncouragement.P_ASSIGN_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));


        if (model.isAccessible())
        {
            dataSource.addColumn(new ActionColumn("Редактировать поощрение / награду", ActionColumn.EDIT, "uniempEmployeeViewExtUI:onClickEditEmployeeEncouragement").setPermissionKey("editEmployeeEncouragement_employee"));
            dataSource.addColumn(new ActionColumn("Удалить поощрение / награду", ActionColumn.DELETE, "uniempEmployeeViewExtUI:onDeleteRowFromList", "Удалить поощрение / награду «{0}»?", new Object[]{EmployeeEncouragement.P_TITLE}).setPermissionKey("deleteEmployeeEncouragement_employee"));
        }

        dataSource.changeOrder(2);
        dataSource.changeOrder(2);
        model.setEmployeeEncouragementsListDataSource(dataSource);
    }

    private void prepareServiceLengthDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeServiceLengthWrapper> dataSource = new DynamicListDataSource<>(component,
                component1 -> {
                        IDataSettings settings = component.getSettings();
                        ServiceLengthType type = settings.get("employeeServiceLengthType");
                        Date beginDate = settings.get("employeeServiceLengthBegin");
                        Date endDate = settings.get("employeeServiceLengthEnd");

                        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmploymentHistoryItemBase.class, "e").column("e")
                                .where(eqValue(property(EmploymentHistoryItemBase.employee().fromAlias("e")), model.getEmployee()));

                        if (null != type)
                            builder.where(exists(new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "r").column("r.id")
                                    .where(eqValue(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("r")), type))
                                    .where(eq(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("r")), property("e")))
                                    .buildQuery()
                            ));
                        if (null == beginDate && null != endDate)
                        {
                            builder.where(or(
                                    le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(endDate)),
                                    le(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(endDate))));
                        }
                        else if (null == endDate && null != beginDate)
                        {
                            builder.where(or(
                                    isNull(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e"))),
                                    and(
                                            isNotNull(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e"))),
                                            or(
                                                    ge(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(beginDate)),
                                                    ge(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(beginDate))))));
                        }
                        else if (null != beginDate)
                        {
                            builder.where(or(
                                    and(
                                            isNull(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e"))),
                                            le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(beginDate)),
                                            le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(endDate))),
                                    and(
                                            isNotNull(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e"))),
                                            or(
                                                    le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(endDate)),
                                                    le(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(endDate))),
                                            or(
                                                    ge(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(beginDate)),
                                                    ge(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(beginDate)))
                                    )));
                        }
                        if (null != beginDate)
                        {
                            builder.where(or(
                                    ge(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(beginDate)),
                                    ge(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(beginDate))));
                        }
                        if (null != endDate)
                        {
                            builder.where(or(
                                    le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(endDate)),
                                    le(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(endDate))));
                        }

                        List<EmploymentHistoryItemBase> historyItemBaseList = builder.createStatement(getSession()).list();

                        DQLSelectBuilder relationBuilder = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column("b")
                                .where(in(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("b")), historyItemBaseList))
                                .order(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().title().fromAlias("b")));
                        if (type != null)
                            relationBuilder.where(eqValue(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")), type));

                        List<EmpHistoryToServiceLengthTypeRelation> relationList = relationBuilder.createStatement(getSession()).list();

                        Map<EmploymentHistoryItemBase, List<ServiceLengthType>> historyItemRelationMap = new HashMap<>();

                        for (EmpHistoryToServiceLengthTypeRelation relation : relationList)
                        {
                            List<ServiceLengthType> list = historyItemRelationMap.get(relation.getEmploymentHistoryItemBase());
                            if (list == null)
                                historyItemRelationMap.put(relation.getEmploymentHistoryItemBase(), list = new ArrayList<>());
                            list.add(relation.getServiceLengthType());
                        }

                        List<EmployeeServiceLengthWrapper> serviceLengthWrapperList = UniempUtil.getEmployeeServiceLengthWrapperList(historyItemBaseList, historyItemRelationMap);

                        Collections.sort(serviceLengthWrapperList, (o1, o2) -> {
                            boolean asc = model.getEmployeeServiceLengthDataSource().getEntityOrder().getDirection().equals(OrderDirection.asc);
                            String keyString = model.getEmployeeServiceLengthDataSource().getEntityOrder().getKeyString();

                            switch (keyString)
                            {
                                case "type.title":
                                    return asc ? o1.getType().getTitle().compareTo(o2.getType().getTitle()) : -1 * o1.getType().getTitle().compareTo(o2.getType().getTitle());
                                case "beginDate":
                                    return asc ? o1.getBeginDate().compareTo(o2.getBeginDate()) : -1 * o1.getBeginDate().compareTo(o2.getBeginDate());
                                case "endDate":
                                    return o1.getEndDate() == null && o2.getEndDate() == null ? 0 :
                                            (o1.getEndDate() != null && o2.getEndDate() != null ? (asc ? o1.getEndDate().compareTo(o2.getEndDate()) : -1 * o1.getEndDate().compareTo(o2.getEndDate())) :
                                                    (o1.getEndDate() == null ? (asc ? 1 : -1) : (asc ? -1 : 1)));
                            }

                            return 0;
                        });

                        UniBaseUtils.createPage(model.getEmployeeServiceLengthDataSource(), serviceLengthWrapperList);
                });

        IStyleResolver styleResolver = rowEntity -> {
            boolean fake = ((DataWrapper)rowEntity).getWrapped() instanceof EmploymentHistoryItemFake;
            return !fake ? "color:#696969;" : "";
        };

        dataSource.addColumn(new SimpleColumn("Тип стажа", "type.title").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата начала", "beginDate", DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата окончания", "endDate", DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Примечание", "comment").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать стаж", ActionColumn.EDIT, "uniempEmployeeViewExtUI:onClickEditEmployeeServiceLength").setPermissionKey("editEmployeeServiceLength_employee").setDisabledProperty("notEditable"));
        dataSource.addColumn(new ActionColumn("Удалить стаж", ActionColumn.DELETE, "uniempEmployeeViewExtUI:onClickDeleteEmployeeServiceLength", "Удалить «{0}»?", "titleWithPeriod").setPermissionKey("deleteEmployeeServiceLength_employee").setDisabledProperty("notEditable"));

        for (AbstractColumn column : dataSource.getColumns())
            column.setStyleResolver(styleResolver);

        model.setEmployeeServiceLengthDataSource(dataSource);
    }

    private void prepareServiceLengthTotalDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<DataWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ServiceLengthType srvType = component1.getSettings().get("employeeServiceLengthType");
            List<EmployeeServiceLengthWrapper> entityList;// подготавливаеи список врапперов из списка Периоды стажей, на основе него будем строить список
            {
                IDataSettings settings = component1.getSettings();
                ServiceLengthType type = settings.get("employeeServiceLengthType");
                Date beginDate = settings.get("employeeServiceLengthBegin");
                Date endDate = settings.get("employeeServiceLengthEnd");

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmploymentHistoryItemBase.class, "e").column("e")
                        .where(eqValue(property(EmploymentHistoryItemBase.employee().fromAlias("e")), model.getEmployee()));

                if (null != type)
                    builder.where(exists(new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "r").column("r.id")
                            .where(eqValue(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("r")), type))
                            .where(eq(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("r")), property("e")))
                            .buildQuery()
                    ));
                if (null == beginDate && null != endDate)
                {
                    builder.where(or(
                            le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(endDate)),
                            le(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(endDate))));
                }
                else if (null == endDate && null != beginDate)
                {
                    builder.where(or(
                            isNull(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e"))),
                            and(
                                    isNotNull(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e"))),
                                    or(
                                            ge(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(beginDate)),
                                            ge(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(beginDate))))));
                }
                else if (null != beginDate)
                {
                    builder.where(or(
                            and(
                                    isNull(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e"))),
                                    le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(beginDate)),
                                    le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(endDate))),
                            and(
                                    isNotNull(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e"))),
                                    or(
                                            le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(endDate)),
                                            le(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(endDate))),
                                    or(
                                            ge(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(beginDate)),
                                            ge(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(beginDate)))
                            )));
                }
                if (null != beginDate)
                {
                    builder.where(or(
                            ge(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(beginDate)),
                            ge(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(beginDate))));
                }
                if (null != endDate)
                {
                    builder.where(or(
                            le(property(EmploymentHistoryItemBase.assignDate().fromAlias("e")), valueDate(endDate)),
                            le(property(EmploymentHistoryItemBase.dismissalDate().fromAlias("e")), valueDate(endDate))));
                }

                List<EmploymentHistoryItemBase> historyItemBaseList = builder.createStatement(getSession()).list();

                DQLSelectBuilder relationBuilder = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column("b")
                        .where(in(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("b")), historyItemBaseList))
                        .order(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().title().fromAlias("b")));
                if (type != null)
                    relationBuilder.where(eqValue(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")), type));

                List<EmpHistoryToServiceLengthTypeRelation> relationList = relationBuilder.createStatement(getSession()).list();

                Map<EmploymentHistoryItemBase, List<ServiceLengthType>> historyItemRelationMap = new HashMap<>();

                for (EmpHistoryToServiceLengthTypeRelation relation : relationList)
                {
                    List<ServiceLengthType> list = historyItemRelationMap.get(relation.getEmploymentHistoryItemBase());
                    if (list == null)
                        historyItemRelationMap.put(relation.getEmploymentHistoryItemBase(), list = new ArrayList<>());
                    list.add(relation.getServiceLengthType());
                }

                entityList = UniempUtil.getEmployeeServiceLengthWrapperList(historyItemBaseList, historyItemRelationMap);
            }

            DQLSelectBuilder relationBuilder = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column("b")
                    .where(in(property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().id().fromAlias("b")), CommonBaseUtil.getPropertiesList(entityList, EmployeeServiceLengthWrapper.ID)))
                    .order(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().title().fromAlias("b")));
            if (srvType != null)
                relationBuilder.where(eqValue(property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().fromAlias("b")), srvType));

            Map<EmploymentHistoryItemBase, List<ServiceLengthType>> historyItemRelationMap = new HashMap<>();

            for (EmpHistoryToServiceLengthTypeRelation relation : relationBuilder.createStatement(getSession()).<EmpHistoryToServiceLengthTypeRelation>list())
            {
                List<ServiceLengthType> list = historyItemRelationMap.get(relation.getEmploymentHistoryItemBase());
                if (list == null)
                    historyItemRelationMap.put(relation.getEmploymentHistoryItemBase(), list = new ArrayList<>());
                list.add(relation.getServiceLengthType());
            }

            Map<ServiceLengthType, EmployeeServiceWrapper> serviceLengthWrapperMap = new TreeMap<>((o1, o2) -> {
                return o1.getTitle().compareTo(o2.getTitle());
            });
            for (EmployeeServiceLengthWrapper wrapper : entityList)
            {
                ServiceLengthType serviceLengthType = wrapper.getType();
                EmploymentHistoryItemBase item = wrapper.getWrapped();

                EmployeeServiceWrapper employeeServiceWrapper = serviceLengthWrapperMap.get(serviceLengthType);
                if (employeeServiceWrapper == null)
                    serviceLengthWrapperMap.put(serviceLengthType, employeeServiceWrapper = new EmployeeServiceWrapper(serviceLengthType));

                employeeServiceWrapper.getPeriodList().add(new PeriodWrapper(item.getAssignDate(), item.getDismissalDate()));
            }

            List<DataWrapper> resultWrapperList = new ArrayList<>();
            int id = 0;
            for (EmployeeServiceWrapper wrapper : serviceLengthWrapperMap.values())
            {
                wrapper.combinationPeriods();

                List<DataWrapper> innerWrapperList = new ArrayList<>();
                List<CoreCollectionUtils.Pair<Date, Date>> periodList = new ArrayList<>();
                for (PeriodWrapper periodWrapper : wrapper.getPeriodList())
                {
                    DataWrapper dataWrapper = new DataWrapper(id++, "");
                    dataWrapper.setProperty("type", wrapper.getServiceLengthType().getTitle());
                    dataWrapper.setProperty("begin", DateFormatter.DEFAULT_DATE_FORMATTER.format(periodWrapper.getBeginDate()));
                    dataWrapper.setProperty("end", DateFormatter.DEFAULT_DATE_FORMATTER.format(periodWrapper.getEndDate()));

                    periodList.add(new CoreCollectionUtils.Pair<>(periodWrapper.getBeginDate(), periodWrapper.getEndDate()));
                    innerWrapperList.add(dataWrapper);
                }

                TripletKey<Integer,Integer,Integer> serviceLengthTypeDuration = UniempUtil.getServiceLengthTypeDuration(new Date(), periodList);
                for (DataWrapper dataWrapper : innerWrapperList)
                {
                    dataWrapper.setProperty("days", serviceLengthTypeDuration.getFirst());
                    dataWrapper.setProperty("months", serviceLengthTypeDuration.getSecond());
                    dataWrapper.setProperty("years", serviceLengthTypeDuration.getThird());
                }

                resultWrapperList.addAll(innerWrapperList);
            }

            model.getEmployeeServiceLengthTotalDataSource().setCountRow(resultWrapperList.size());
            UniBaseUtils.createPage(model.getEmployeeServiceLengthTotalDataSource(), resultWrapperList);
        });

        IMergeRowIdResolver mergeRowIdResolver = entity -> String.valueOf(entity.getProperty("type"));
        IStyleResolver styleResolver = rowEntity -> "font-weight: bold;";

        dataSource.addColumn(new SimpleColumn("Тип стажа", "type").setMergeRowIdResolver(mergeRowIdResolver).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата начала", "begin").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата окончания", "end").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Лет", "years").setMergeRowIdResolver(mergeRowIdResolver).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Месяцев", "months").setMergeRowIdResolver(mergeRowIdResolver).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дней", "days").setMergeRowIdResolver(mergeRowIdResolver).setClickable(false).setOrderable(false));

        dataSource.getColumn("type").setStyleResolver(styleResolver);
        dataSource.getColumn("years").setStyleResolver(styleResolver);
        dataSource.getColumn("months").setStyleResolver(styleResolver);
        dataSource.getColumn("days").setStyleResolver(styleResolver);

        model.setEmployeeServiceLengthTotalDataSource(dataSource);
    }

    private void prepareTrainingDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeTrainingItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeTrainingItem.class, "b").column("b");
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeTrainingItem.employee().fromAlias("b")), model.getEmployee()));

            //применяем фильтры
            IUISettings settings = getPresenter().getSettings();
            if (settings.get("startTrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.startDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("startTrainingDateFrom"))));
            if (settings.get("startTrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeTrainingItem.startDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("startTrainingDateTo"))));
            if (settings.get("finishTrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.finishDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("finishTrainingDateFrom"))));
            if (settings.get("finishTrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeTrainingItem.finishDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("finishTrainingDateTo"))));
//        if (settings.get("eduDocumentTrainingNumberFiltr") != null)
//            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EmployeeTrainingItem.eduDocumentNumber().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike((String) settings.get("eduDocumentTrainingNumberFiltr")))));
            if (settings.get("eduDocumentTrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeTrainingItem.eduDocumentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("eduDocumentTrainingDateFrom"))));
            if (settings.get("eduDocumentTrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeTrainingItem.eduDocumentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("eduDocumentTrainingDateTo"))));

            new DQLOrderDescriptionRegistry(EmployeeTrainingItem.class, "b").applyOrder(builder, model.getTrainingDataSource().getEntityOrder());

            UniBaseUtils.createPage(model.getTrainingDataSource(), builder, getSession());
        });

        HeadColumn headDateColumn = new HeadColumn("headDateColumn", "Период обучения");
        headDateColumn.setHeaderAlign("center");
        headDateColumn.addColumn(new SimpleColumn("Дата начала", EmployeeTrainingItem.startDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        headDateColumn.addColumn(new SimpleColumn("Дата окончания", EmployeeTrainingItem.finishDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headDateColumn);
        dataSource.addColumn(new SimpleColumn("Вид повышения квалификации", EmployeeTrainingItem.P_TRAINING_TITLE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Образовательное учреждение", EmployeeTrainingItem.P_EDU_INSTITUTION_AND_ADDRESS).setOrderable(false));
        HeadColumn headDocumentColumn = new HeadColumn("headDocumentColumn", "Документ");
        headDocumentColumn.setHeaderAlign("center");
        headDocumentColumn.addColumn(new SimpleColumn("Тип", EmployeeTrainingItem.eduDocumentType().title()));
        headDocumentColumn.addColumn(new SimpleColumn("Номер", EmployeeTrainingItem.P_SERIA_AND_NUMBER).setOrderable(false));
        headDocumentColumn.addColumn(new SimpleColumn("Дата", EmployeeTrainingItem.eduDocumentDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headDocumentColumn);
        dataSource.addColumn(new SimpleColumn("Основание", EmployeeTrainingItem.P_BASICS).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Файл копии документа о повышении квалификации", "printer", "uniempEmployeeViewExtUI:onClickPrintEduDocumentTraining")
                                     .setDisableHandler(entity -> ((EmployeeTrainingItem) entity).getEduDocumentFile() == null)
                                     .setPermissionKey("printEmployeeTrainingItem_employee"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeeViewExtUI:onClickEditTrainingItem").setPermissionKey("editEmployeeTrainingItem_employee"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeeViewExtUI:onDeleteRowFromList", "Удалить данные о повышении квалификации с {0} по {1}?", EmployeeTrainingItem.startDate(), EmployeeTrainingItem.finishDate()).setPermissionKey("deleteEmployeeTrainingItem_employee"));

        model.setTrainingDataSource(dataSource);
    }

    private void prepareRetrainingDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<EmployeeRetrainingItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeRetrainingItem.class, "b").column("b");
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeRetrainingItem.employee().fromAlias("b")), model.getEmployee()));

            //применяем фильтры
            IUISettings settings = getPresenter().getSettings();
            if (settings.get("startRetrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.startDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("startRetrainingDateFrom"))));
            if (settings.get("startRetrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeRetrainingItem.startDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("startRetrainingDateTo"))));
            if (settings.get("finishRetrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.finishDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("finishRetrainingDateFrom"))));
            if (settings.get("finishRetrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeRetrainingItem.finishDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("finishRetrainingDateTo"))));
//        if (settings.get("eduDocumentRetrainingNumberFiltr") != null)
//            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EmployeeRetrainingItem.eduDocumentNumber().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike((String) settings.get("eduDocumentRetrainingNumberFiltr")))));
            if (settings.get("eduDocumentRetrainingDateFrom") != null)
                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.eduDocumentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("eduDocumentRetrainingDateFrom"))));
            if (settings.get("eduDocumentRetrainingDateTo") != null)
                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeRetrainingItem.eduDocumentDate().fromAlias("b")), DQLExpressions.commonValue(settings.get("eduDocumentRetrainingDateTo"))));

            new DQLOrderDescriptionRegistry(EmployeeRetrainingItem.class, "b").applyOrder(builder, model.getRetrainingDataSource().getEntityOrder());

            UniBaseUtils.createPage(model.getRetrainingDataSource(), builder, getSession());
        });

        HeadColumn headDateColumn = new HeadColumn("headDateColumn", "Период переподготовки");
        headDateColumn.setHeaderAlign("center");
        headDateColumn.addColumn(new SimpleColumn("Дата начала", EmployeeRetrainingItem.startDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        headDateColumn.addColumn(new SimpleColumn("Дата окончания", EmployeeRetrainingItem.finishDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headDateColumn);
        dataSource.addColumn(new SimpleColumn("Специальность (Направление)", EmployeeRetrainingItem.P_SPECIALITY_TITLE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Образовательное учреждение", EmployeeRetrainingItem.P_EDU_INSTITUTION_AND_ADDRESS).setOrderable(false));
        HeadColumn headDocumentColumn = new HeadColumn("headDocumentColumn", "Документ");
        headDocumentColumn.setHeaderAlign("center");
        headDocumentColumn.addColumn(new SimpleColumn("Тип", EmployeeRetrainingItem.eduDocumentType().title()));
        headDocumentColumn.addColumn(new SimpleColumn("Номер", EmployeeRetrainingItem.P_SERIA_AND_NUMBER).setOrderable(false));
        headDocumentColumn.addColumn(new SimpleColumn("Дата", EmployeeRetrainingItem.eduDocumentDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(headDocumentColumn);
        dataSource.addColumn(new SimpleColumn("Основание", EmployeeRetrainingItem.P_BASICS).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Файл копии документа о профессиональной переподготовке", "printer", "uniempEmployeeViewExtUI:onClickPrintEduDocumentRetraining")
                                     .setDisableHandler(entity -> ((EmployeeRetrainingItem) entity).getEduDocumentFile() == null)
                                     .setPermissionKey("printEmployeeRetrainingItem_employee"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "uniempEmployeeViewExtUI:onClickEditRetrainingItem").setPermissionKey("editEmployeeRetrainingItem_employee"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "uniempEmployeeViewExtUI:onDeleteRowFromList", "Удалить данные о профессиональной переподготовке с {0} по {1}?", EmployeeRetrainingItem.startDate(), EmployeeRetrainingItem.finishDate()).setPermissionKey("deleteEmployeeRetrainingItem_employee"));

        model.setRetrainingDataSource(dataSource);
    }

    public void onClickDeleteEmployeeServiceLength()
    {
        IEntity iEntity = DataAccessServices.dao().get(getListenerParameterAsLong());
        if (iEntity instanceof EmploymentHistoryItemFake)
            DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
