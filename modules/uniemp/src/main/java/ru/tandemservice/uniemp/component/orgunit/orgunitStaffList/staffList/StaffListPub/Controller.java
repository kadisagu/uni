/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

/**
 * @author dseleznev
 *         Created on: 19.09.2008
 */
@SuppressWarnings({ "unchecked", "deprecation" })
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    private static final IStyleResolver RIGHT_ALIGNED_COLUMN_STYLE_RESOLVER = rowEntity -> "text-align: right;";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        prepareListDataSource(component);
        preparePaymentsDataSource(component);
        preparePaymentsTotalDataSource(component);
        prepareAllocationListDataSource(component);
        prepareAllocationTotalListDataSource(component);
        prepareEmployeeHRListDataSource(component);
    }

    private SimpleColumn getRightAlignedSimpleColumn(String caption, Object key, IFormatter formatter)
    {
        SimpleColumn column = new SimpleColumn(caption, key, formatter);
        column.setStyleResolver(RIGHT_ALIGNED_COLUMN_STYLE_RESOLVER);
        column.setClickable(false).setOrderable(false);
        return column;
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<StaffListItem> dataSource = new DynamicListDataSource<>(component, this);
        dataSource.addColumn(new SimpleColumn("Название должности", StaffListItem.POST_SIMPLE_TITLE_KEY));
        dataSource.addColumn(new SimpleColumn("Тип должн.", StaffListItem.POST_TYPE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ПКГ", StaffListItem.PROF_QUALIFICATION_GROUP_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("КУ", StaffListItem.QUALIFICATION_LEVEL_KEY).setClickable(false));
        dataSource.addColumn(getRightAlignedSimpleColumn("Базовый оклад", StaffListItem.P_SALARY, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED));
        dataSource.addColumn(new SimpleColumn("Разряд ЕТКС", StaffListItem.ETKS_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Количество штатных единиц", StaffListItem.P_STAFF_RATE, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Из них занято", StaffListItem.P_OCC_STAFF_RATE, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", new String[] { StaffListItem.L_FINANCING_SOURCE, FinancingSource.P_TITLE}).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", new String[] { StaffListItem.L_FINANCING_SOURCE_ITEM, FinancingSourceItem.P_TITLE}).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setDisabledProperty(StaffListItem.L_STAFF_LIST + "." + StaffList.P_CANT_BE_EDITED).setPermissionKey("editStaffListItem_StaffListTab"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить должность «{0}» из версии штатного расписания?", new Object[]{new String[]{StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.P_TITLE_WITH_SALARY}}).setDisabledProperty(StaffListItem.L_STAFF_LIST + "." + StaffList.P_CANT_BE_EDITED).setPermissionKey("deleteStaffListItem_StaffListTab"));
        model.setDataSource(dataSource);
    }

    private void preparePaymentsDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        IStyleResolver styleResolver = rowEntity -> {
            PaymentWrapper wrapper = (PaymentWrapper) rowEntity;

            if (wrapper.isBold())
                return "font-weight:bold;";
            else
                return null;
        };

        IMergeRowIdResolver mergeRowResolver = entity -> {
            PaymentWrapper wrapper = (PaymentWrapper) entity;

            return wrapper.getStaffListItem() != null ? wrapper.getStaffListItem().getId().toString() : wrapper.getId().toString();
        };

        if (model.getPaymentsDataSource() != null)
        {
            HeadColumn paymentsColumn = (HeadColumn)model.getPaymentsDataSource().getColumn("payments");

            if (null != paymentsColumn)
            {
                paymentsColumn.getColumns().clear();
                for (Payment payment : model.getPaymentsList())
                {
                    SimpleColumn codeColumn = new SimpleColumn(payment.getShortTitle(), payment.getCode());
                    codeColumn.setClickable(false);
                    codeColumn.setOrderable(false);
                    codeColumn.setStyleResolver(styleResolver);
                    paymentsColumn.addColumn(codeColumn);
                }
            }

            return;
        }

        DynamicListDataSource<PaymentWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareStaffListPaymentsDataSource(model);
        });

        PublisherLinkColumn postSimpleColumn = new PublisherLinkColumn("Название должности", PaymentWrapper.P_POST_TITLE);
        postSimpleColumn.setResolver(new SimplePublisherLinkResolver("staffListItem.id"));
        postSimpleColumn.setStyleResolver(styleResolver);
        postSimpleColumn.setMergeRowIdResolver(mergeRowResolver);
        dataSource.addColumn(postSimpleColumn);
        dataSource.addColumn(new SimpleColumn("Тип должн.", PaymentWrapper.P_POST_TYPE).setClickable(false).setMergeRowIdResolver(mergeRowResolver));
        dataSource.addColumn(new SimpleColumn("ПКГ", PaymentWrapper.P_PROF_QUALIFICATION_GROUP).setMergeRowIdResolver(mergeRowResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("КУ", PaymentWrapper.P_QUALIFICATION_LEVEL).setMergeRowIdResolver(mergeRowResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Базовый оклад", PaymentWrapper.P_SALARY).setMergeRowIdResolver(mergeRowResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Разряд ЕТКС", PaymentWrapper.P_ETKS).setMergeRowIdResolver(mergeRowResolver).setClickable(false).setOrderable(false));
        AbstractColumn staffRateColumn = new SimpleColumn("Кол-во штатных единиц", PaymentWrapper.P_STAFF_RATE).setClickable(false).setOrderable(false);
        staffRateColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(staffRateColumn);
        AbstractColumn financingSourceColumn = new SimpleColumn("Источник финансирования", PaymentWrapper.P_FINANCING_SOURCE).setClickable(false);
        financingSourceColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(financingSourceColumn);
        AbstractColumn financingSourceItemColumn = new SimpleColumn("Источник финансирования (детально)", PaymentWrapper.P_FINANCING_SOURCE_ITEM).setClickable(false).setOrderable(false);
        financingSourceItemColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(financingSourceItemColumn);

        HeadColumn paymentsColumn = (HeadColumn) new HeadColumn("payments", "Выплаты").setHeaderAlign("center");
        dataSource.addColumn(paymentsColumn);

        for (Payment payment : model.getPaymentsList())
        {
            SimpleColumn codeColumn = new SimpleColumn(payment.getShortTitle(), payment.getCode());
            codeColumn.setClickable(false);
            codeColumn.setOrderable(false);
            codeColumn.setStyleResolver(styleResolver);
            paymentsColumn.addColumn(codeColumn);
        }

        SimpleColumn monthSalaryColumn = new SimpleColumn("Месячный фонд оплаты труда по тарифу (руб)", PaymentWrapper.P_MONTH_BASE_SALARY_FUND);
        monthSalaryColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(monthSalaryColumn.setClickable(false));
        SimpleColumn targetSalaryColumn = new SimpleColumn("Месячный фонд оплаты труда с надбавками", PaymentWrapper.P_TARGET_SALARY_FUND);
        targetSalaryColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(targetSalaryColumn.setClickable(false));

        model.setPaymentsDataSource(dataSource);
    }

    public void preparePaymentsTotalDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        IStyleResolver styleResolver = rowEntity -> {
            IdentifiableWrapper wrapper = (IdentifiableWrapper) ((ViewWrapper) rowEntity).getEntity();

            if (wrapper.getTitle().equals("bold"))
                return "font-weight:bold;";
            else
                return null;
        };

        if (model.getPaymentsTotalDataSource() != null)
        {
            HeadColumn paymentsColumn = (HeadColumn)model.getPaymentsTotalDataSource().getColumn("payments");

            if (null != paymentsColumn)
            {
                paymentsColumn.getColumns().clear();
                for (Payment payment : model.getPaymentsList())
                {
                    SimpleColumn codeColumn = getRightAlignedSimpleColumn(payment.getShortTitle(), payment.getCode(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED);
                    codeColumn.setStyleResolver(styleResolver);
                    paymentsColumn.addColumn(codeColumn);
                }
            }

            return;
        }

        DynamicListDataSource<IdentifiableWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareStaffListTotalPaymentsDataSource(model);
        });

        AbstractColumn financingSourceColumn = new SimpleColumn("Источник финансирования", "FINANCING_SOURCE").setClickable(false).setOrderable(false);
        financingSourceColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(financingSourceColumn);
        AbstractColumn financingSourceItemColumn = new SimpleColumn("Источник финансирования (детально)", "FINANCING_SOURCE_ITEM").setClickable(false).setOrderable(false);
        financingSourceItemColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(financingSourceItemColumn);

        HeadColumn paymentsColumn = (HeadColumn) new HeadColumn("payments", "Выплаты").setHeaderAlign("center");
        dataSource.addColumn(paymentsColumn);

        for (Payment payment : model.getPaymentsList())
        {
            SimpleColumn codeColumn = getRightAlignedSimpleColumn(payment.getShortTitle(), payment.getCode(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED);
            codeColumn.setStyleResolver(styleResolver);
            paymentsColumn.addColumn(codeColumn);
        }

        SimpleColumn monthSalaryColumn = getRightAlignedSimpleColumn("Месячный фонд оплаты труда по тарифу (руб)", StaffListItem.P_MONTH_BASE_SALARY_FUND, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED);
        monthSalaryColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(monthSalaryColumn);
        SimpleColumn targetSalaryColumn = getRightAlignedSimpleColumn("Месячный фонд оплаты труда с надбавками", StaffListItem.P_TARGET_SALARY_FUND, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED);
        targetSalaryColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(targetSalaryColumn);

        model.setPaymentsTotalDataSource(dataSource);
    }

    private void prepareAllocationListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        IMergeRowIdResolver mergeRowResolver = entity -> {
            AllocationWrapper wrapper = (AllocationWrapper) entity;

            return wrapper.getAllocationItem() != null ? wrapper.getAllocationItem().getId().toString() : wrapper.getId().toString();
        };

        IStyleResolver styleResolver = rowEntity -> {
            AllocationWrapper wrapper = (AllocationWrapper) rowEntity;

            if (wrapper.isBold())
                return "font-weight:bold;";
            else
                return null;
        };

        IStyleResolver staffRateStyleResolver = rowEntity -> {
            AllocationWrapper wrapper = (AllocationWrapper) rowEntity;
            String styleString = RIGHT_ALIGNED_COLUMN_STYLE_RESOLVER.getStyle(rowEntity);
            if (wrapper.isStaffRateInvalid() || wrapper.isPostStaffRateInvalid())
            {
                styleString += " background-color: #ffe8f3; cursor:point; cursor:hand;";
            }
            if (wrapper.isBold())
            {
                styleString += " font-weight:bold;";
            }
            return styleString;
        };

        IStyleResolver postStyleResolver = rowEntity -> {
            String styleString = "text-align:left;";
            AllocationWrapper wrapper = (AllocationWrapper) rowEntity;
            StaffListAllocationItem allocationItem = wrapper.getAllocationItem();
            if (allocationItem == null)
                return null;

            OrgUnit firstOrgUnit = allocationItem.getStaffListItem().getStaffList().getOrgUnit();
            OrgUnit secondOrgUnit = allocationItem.getEmployeePost() != null ? allocationItem.getEmployeePost().getOrgUnit() : null;
            if (allocationItem.getEmployeePost() != null && !firstOrgUnit.equals(secondOrgUnit))
                styleString += " background-color: #ffe8f3; cursor:point; cursor:hand;";

            OrgUnitTypePostRelation firstPost = allocationItem.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation();
            OrgUnitTypePostRelation secondPost = allocationItem.getEmployeePost() != null ? allocationItem.getEmployeePost().getPostRelation() : null;
            if (allocationItem.getEmployeePost() != null && !firstPost.equals(secondPost))
                styleString += " background-color: #ffe8f3; cursor:point; cursor:hand;";

            if (wrapper.isPostStaffRateInvalid())
                styleString += " background-color: #ffe8f3; cursor:point; cursor:hand;";

            if (allocationItem.isReserve())
                styleString += " font-weight:bold;";

            return styleString;
        };

        IListenerParametersResolver parametersResolver = (entity, valueEntity) -> {
            AllocationWrapper wrapper = (AllocationWrapper) entity;
            return wrapper.getAllocationItem();
        };

        if (model.getAllocationDataSource() != null)
        {

            HeadColumn paymentsColumn = (HeadColumn)model.getAllocationDataSource().getColumn("allocPayments");

            if (null != paymentsColumn)
            {
                paymentsColumn.getColumns().clear();
                for (Payment payment : model.getPaymentsList())
                {
                    SimpleColumn paymentColumn = new SimpleColumn(payment.getShortTitle(), payment.getCode());
                    paymentColumn.setOrderable(false);
                    paymentColumn.setClickable(false);
                    paymentColumn.setStyleResolver(styleResolver);
                    paymentsColumn.addColumn(paymentColumn);
                }
            }

            return;
        }

        DynamicListDataSource<AllocationWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareStaffListAllocationsDataSource(model);
        });


        PublisherLinkColumn postColumn = new PublisherLinkColumn("Должность", AllocationWrapper.P_POST_TITLE);
        postColumn.setResolver(new SimplePublisherLinkResolver("allocationItem.id"));
        postColumn.setStyleResolver(styleResolver);
        postColumn.setMergeRowIdResolver(mergeRowResolver);
        dataSource.addColumn(postColumn);
        dataSource.addColumn(new SimpleColumn("Тип должн.", AllocationWrapper.P_POST_TYPE).setMergeRowIdResolver(mergeRowResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ПКГ", AllocationWrapper.P_PROF_QUALIFICATION_GROUP).setMergeRowIdResolver(mergeRowResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("КУ", AllocationWrapper.P_QUALIFICATION_LEVEL).setMergeRowIdResolver(mergeRowResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Базовый оклад", AllocationWrapper.P_SALARY).setMergeRowIdResolver(mergeRowResolver).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Р", AllocationWrapper.P_ETKS).setMergeRowIdResolver(mergeRowResolver).setClickable(false).setOrderable(false));
        BlockColumn staffRateColumn = new BlockColumn("staffRate", "Ставка");
        staffRateColumn.setStyleResolver(staffRateStyleResolver);
        dataSource.addColumn(staffRateColumn);
        AbstractColumn financingSourceColumn = new SimpleColumn("Источник финансирования", AllocationWrapper.P_FINANCING_SOURCE).setClickable(false);
        financingSourceColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(financingSourceColumn);
        AbstractColumn financingSourceItemColumn = new SimpleColumn("Источник финансирования (детально)", AllocationWrapper.P_FINANCING_SOURCE_ITEM).setClickable(false).setOrderable(false);
        financingSourceItemColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(financingSourceItemColumn);

        HeadColumn paymentsColumn = (HeadColumn) new HeadColumn("allocPayments", "Выплаты").setHeaderAlign("center");
        dataSource.addColumn(paymentsColumn);

        for (Payment payment : model.getPaymentsList())
        {
            SimpleColumn paymentColumn = new SimpleColumn(payment.getShortTitle(), payment.getCode());
            paymentColumn.setOrderable(false);
            paymentColumn.setClickable(false);
            paymentColumn.setStyleResolver(styleResolver);
            paymentsColumn.addColumn(paymentColumn);
        }

        SimpleColumn monthColumn = new SimpleColumn("Месячный фонд оплаты труда по тарифу (руб)", AllocationWrapper.P_MONTH_BASE_SALARY_FUND);
        monthColumn.setClickable(false);
        monthColumn.setOrderable(false);
        monthColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(monthColumn);
        SimpleColumn targetColumn = new SimpleColumn("Месячный фонд оплаты труда с надбавками", AllocationWrapper.P_TARGET_SALARY_FUND);
        targetColumn.setOrderable(false);
        targetColumn.setClickable(false);
        targetColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(targetColumn);
        BlockColumn employeePostColumn = new BlockColumn("employeePost", "ФИО сотрудника");
        employeePostColumn.setMergeRowIdResolver(mergeRowResolver);
        employeePostColumn.setStyleResolver(postStyleResolver);
        dataSource.addColumn(employeePostColumn);
        ActionColumn cloneColumn = new ActionColumn("Дублировать", "clone", "onClickCopyAllocationItem");
        cloneColumn.setParametersResolver(parametersResolver);
        dataSource.addColumn(cloneColumn.setMergeRowIdResolver(mergeRowResolver).setDisabledProperty(AllocationWrapper.P_CANT_COPY).setPermissionKey("copyStaffListAllocationItem_StaffListAllocationTab"));
        ActionColumn editColumn = new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditAllocationItem");
        editColumn.setParametersResolver(parametersResolver);
        dataSource.addColumn(editColumn.setMergeRowIdResolver(mergeRowResolver).setPermissionKey("editStaffListAllocationItem_StaffListAllocationTab").setDisabledProperty(AllocationWrapper.P_CANT_EDIT));
        ActionColumn delColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteAllocItem", "Удалить элемент штатной расстановки «{0}» из версии штатного расписания?", "SAFE_TITLE");
        delColumn.setParametersResolver(parametersResolver);
        dataSource.addColumn(delColumn.setMergeRowIdResolver(mergeRowResolver).setDisabledProperty(AllocationWrapper.P_CANT_DELETE).setPermissionKey("deleteStaffListAllocationItem_StaffListAllocationTab"));
        model.setAllocationDataSource(dataSource);

    }

    private void prepareAllocationTotalListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        IStyleResolver styleResolver = rowEntity -> {
            IdentifiableWrapper wrapper = (IdentifiableWrapper) ((ViewWrapper) rowEntity).getEntity();

            if (wrapper.getTitle().equals("bold"))
                return "font-weight:bold;";
            else
                return null;
        };

        if (model.getAllocationTotalDataSource() != null)
        {

            HeadColumn paymentsColumn = (HeadColumn)model.getAllocationTotalDataSource().getColumn("allocPayments");

            if (null != paymentsColumn)
            {
                paymentsColumn.getColumns().clear();
                for (Payment payment : model.getPaymentsList())
                {
                    SimpleColumn paymentColumn = getRightAlignedSimpleColumn(payment.getShortTitle(), payment.getCode(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED);
                    paymentColumn.setStyleResolver(styleResolver);
                    paymentsColumn.addColumn(paymentColumn);
                }
            }

            return;
        }

        // TODO: wrapper
        DynamicListDataSource<IdentifiableWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareStaffListAllocationsTotalDataSource(model);
        });

        AbstractColumn financingSourceColumn = new SimpleColumn("Источник финансирования", "L_FINANCING_SOURCE").setClickable(false).setOrderable(false);
        financingSourceColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(financingSourceColumn);
        AbstractColumn financingSourceItemColumn = new SimpleColumn("Источник финансирования (детально)", "L_FINANCING_SOURCE_ITEM").setClickable(false).setOrderable(false);
        financingSourceItemColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(financingSourceItemColumn);

        HeadColumn paymentsColumn = (HeadColumn) new HeadColumn("allocPayments", "Выплаты").setHeaderAlign("center");
        dataSource.addColumn(paymentsColumn);

        for (Payment payment : model.getPaymentsList())
        {
            SimpleColumn paymentColumn = getRightAlignedSimpleColumn(payment.getShortTitle(), payment.getCode(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED);
            paymentColumn.setStyleResolver(styleResolver);
            paymentsColumn.addColumn(paymentColumn);
        }

        SimpleColumn monthColumn = getRightAlignedSimpleColumn("Месячный фонд оплаты труда по тарифу (руб)", StaffListAllocationItem.P_MONTH_BASE_SALARY_FUND, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED);
        monthColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(monthColumn);
        SimpleColumn targetColumn = getRightAlignedSimpleColumn("Месячный фонд оплаты труда с надбавками", StaffListAllocationItem.P_TARGET_SALARY, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED);
        targetColumn.setStyleResolver(styleResolver);
        dataSource.addColumn(targetColumn);

        model.setAllocationTotalDataSource(dataSource);
    }

    private void prepareEmployeeHRListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getEmployeeHRDataSource() != null)
        {
            HeadColumn paymentsColumn = (HeadColumn)model.getAllocationDataSource().getColumn("employeePayments");

            if (null != paymentsColumn)
            {
                paymentsColumn.getColumns().clear();
                for (Payment payment : model.getCompensPaymentsList())
                {
                    paymentsColumn.addColumn(getRightAlignedSimpleColumn(payment.getShortTitle(), payment.getCode(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED));
                }
            }

            return;
        }

        IMergeRowIdResolver mergeRowIdResolver = entity -> {
            EmployeeHRLineWrapper wrapper = (EmployeeHRLineWrapper) entity;

            if (wrapper.getEmployeePost() != null)
                return wrapper.getEmployeePost().getId().toString() + wrapper.getStaffListItem().getId().toString();
            else if (wrapper.isReserve())
                return wrapper.getStaffListItem().getId().toString() + wrapper.getStaffRate().toString();
            else
                return wrapper.getStaffListItem().getId().toString();
        };

        // TODO: wrapper
        DynamicListDataSource<EmployeeHRLineWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareStaffListEmployeeHRDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Должность", EmployeeHRLineWrapper.P_POST_TITLE).setMergeRowIdResolver(mergeRowIdResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип должн.", EmployeeHRLineWrapper.P_POST_TYPE).setMergeRowIdResolver(mergeRowIdResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ПКГ", EmployeeHRLineWrapper.P_PROF_QUALIFICATION_GROUP_SHORT_TITLE).setMergeRowIdResolver(mergeRowIdResolver).setClickable(false));
        dataSource.addColumn(new SimpleColumn("КУ", EmployeeHRLineWrapper.P_QUALIFICATION_LEVEL_SHORT_TITLE).setMergeRowIdResolver(mergeRowIdResolver).setClickable(false));
        dataSource.addColumn(getRightAlignedSimpleColumn("Базовый оклад", EmployeeHRLineWrapper.P_BASE_SALARY, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setMergeRowIdResolver(mergeRowIdResolver));
        dataSource.addColumn(new SimpleColumn("Р", EmployeeHRLineWrapper.P_ETKS_LEVEL_TITLE).setMergeRowIdResolver(mergeRowIdResolver).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeeHRLineWrapper.P_STAFF_RATE_STR).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeeHRLineWrapper.P_FINANCING_SOURCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeeHRLineWrapper.P_FINANCING_SOURCE_ITEM).setClickable(false).setOrderable(false));

        HeadColumn paymentsColumn = (HeadColumn) new HeadColumn("employeePayments", "Выплаты").setHeaderAlign("center");
        dataSource.addColumn(paymentsColumn);

        for (Payment payment : model.getCompensPaymentsList())
        {
            paymentsColumn.addColumn(getRightAlignedSimpleColumn(payment.getShortTitle(), payment.getCode(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED));
        }

        dataSource.addColumn(getRightAlignedSimpleColumn("Месячный фонд оплаты труда по тарифу (руб)", EmployeeHRLineWrapper.P_MONTH_BASE_SALARY_FUND, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED));
        dataSource.addColumn(getRightAlignedSimpleColumn("Месячный фонд оплаты труда с надбавками", EmployeeHRLineWrapper.P_TARGET_SALARY, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED));

        PublisherLinkColumn postColumn = new PublisherLinkColumn("ФИО сотрудника", EmployeeHRLineWrapper.P_EMPLOYEE_FIO);
        postColumn.setMergeRowIdResolver(mergeRowIdResolver);
        postColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                EmployeePost post = ((EmployeeHRLineWrapper) entity).getEmployeePost();
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, null != post ? post.getId() : null).add("selectedTab", "employeePostTab").add("selectedDataTab", ((EmployeeHRLineWrapper) entity).isCombination() ? "Combination" : null);
            }
        });
        postColumn.setStyleResolver(rowEntity -> ((EmployeeHRLineWrapper) rowEntity).isReserve() ? "font-weight:bold;" : null);
        dataSource.addColumn(postColumn);

        //dataSource.addColumn(new ActionColumn("Дублировать", "clone", "onClickCopyAllocationItem").setDisabledProperty(StaffListAllocationItem.P_CANT_BE_COPIED).setPermissionKey("copyStaffListAllocationItem_StaffListAllocationTab"));
        model.setEmployeeHRDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickChangeState(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_CHANGE_STATE));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createChildRegion("dataScope", new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ADDEDIT,
                                                                      new ParametersMap().add("copyStaffList", Boolean.FALSE)));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().delete(getModel(component).getStaffList());
        deactivate(component);
    }

    public void onClickAddItem(IBusinessComponent component)
    {
        component.createChildRegion("dataScope", new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ITEM_ADDEDIT, new ParametersMap().add("staffListItemId", null)));
    }

    public void onClickEditItem(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ITEM_ADDEDIT, new ParametersMap().add("staffListItemId", component.getListenerParameter())));
    }

    public void onClickDeleteAllocItem(IBusinessComponent component)
    {
        StaffListAllocationItem item = component.getListenerParameter();
        getDao().delete(item);
        Model model = getModel(component);
        getDao().prepareStaffListPaymentsDataSource(model);
        getDao().prepareStaffListAllocationsDataSource(model);
        getDao().prepareStaffListTotalPaymentsDataSource(model);
        getDao().prepareStaffListAllocationsTotalDataSource(model);
    }

    public void onClickDeleteItem(IBusinessComponent component)
    {
        getDao().delete(component.<Long>getListenerParameter());
        Model model = getModel(component);
        getDao().prepareStaffListPaymentsDataSource(model);
        getDao().prepareStaffListAllocationsDataSource(model);
        getDao().prepareStaffListTotalPaymentsDataSource(model);
        getDao().prepareStaffListAllocationsTotalDataSource(model);
    }

    public void onClickAddAllocationItem(IBusinessComponent component)
    {
        getModel(component).setStaffListAllocationItemId(null);
        component.createChildRegion("allocationScope", new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ALLOC_ITEM_ADDEDIT));
    }

    public void onClickAddReserveAllocationItem(IBusinessComponent component)
    {
        getModel(component).setStaffListAllocationItemId(null);
        component.createChildRegion("allocationScope", new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ALLOC_ITEM_ADDEDIT).setParameters(
                new ParametersMap().add("reserve", true)
        ));
    }

    public void onClickEditAllocationItem(IBusinessComponent component)
    {
        StaffListAllocationItem item = component.getListenerParameter();
        getModel(component).setStaffListAllocationItemId(item.getId());
        activateInRoot(component, new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ALLOC_ITEM_ADDEDIT));
    }

    public void onClickCopyAllocationItem(IBusinessComponent component)
    {
        StaffListAllocationItem item = component.getListenerParameter();
        getDao().updateCopyStaffListAllocationItem(item.getId());
    }

    public void onClickFillAllocationAutomatically(IBusinessComponent component)
    {
        //TODO:
        getDao().updateFillStaffListAllocationAutomatically(getModel(component));
        onRefreshComponent(component);
    }

    public void onClickCopyStaffList(IBusinessComponent component)
    {
        component.createChildRegion("dataScope", new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ADDEDIT,
                                                                      new ParametersMap().add("copyStaffList", Boolean.TRUE)));
    }

    public void onClickSearch(IBusinessComponent context)
    {
        getModel(context).getAllocationDataSource().showFirstPage();
        getModel(context).getAllocationDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent context)
    {
        getModel(context).setEmployeeTypeFilter(null);
        getModel(context).setPostFilter(null);
        getModel(context).setLastNameFilter(null);
        onClickSearch(context);
    }

    public void onClickHRSearch(IBusinessComponent context)
    {
        getModel(context).getEmployeeHRDataSource().showFirstPage();
        getModel(context).getEmployeeHRDataSource().refresh();
    }

    public void onClickHRClear(IBusinessComponent context)
    {
        getModel(context).setHrEmployeeTypeFilter(null);
        getModel(context).setHrPostFilter(null);
        getModel(context).setHrLastNameFilter(null);
        onClickHRSearch(context);
    }

    public void onChangeColumn(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (component.getListenerParameter().equals("payments"))
            model.getPaymentsTotalDataSource().setColumnSettings(model.getPaymentsDataSource().getColumnSettings());
        else
            model.getPaymentsDataSource().setColumnSettings(model.getPaymentsTotalDataSource().getColumnSettings());
    }
}