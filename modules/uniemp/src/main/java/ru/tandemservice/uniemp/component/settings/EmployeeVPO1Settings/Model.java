/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.EmployeeVPO1Settings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;

/**
 * @author dseleznev
 * Created on: 21.01.2010
 */
public class Model
{
    private DynamicListDataSource<EmployeeVPO1ReportLines> _dataSource;

    public DynamicListDataSource<EmployeeVPO1ReportLines> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EmployeeVPO1ReportLines> dataSource)
    {
        this._dataSource = dataSource;
    }
}