/**
 *$Id:$
 */
package ru.tandemservice.uniemp.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniemp.entity.employee.PostToServiceLengthTypeRelation;

/**
 * @author Alexander Shaburov
 * @since 17.09.12
 */
public class Controller extends org.tandemframework.shared.employeebase.component.catalog.postBoundedWithQGandQL.PostBoundedWithQGandQLPub.Controller
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<PostBoundedWithQGandQL> dataSource)
    {
        super.addColumnsBeforeEditColumn(context, dataSource);

        dataSource.addColumn(new SimpleColumn("Учитывать при расчете стажа", "serviceLengthTypeList", new CollectionFormatter(source -> {
            return ((PostToServiceLengthTypeRelation)source).getServiceLengthType().getShortTitle();
        })));
    }
}
