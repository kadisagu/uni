package ru.tandemservice.uniemp.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.RosobrReports;
import ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь отчета Рособразования с выплатой сотрудникам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RosobrReportToPaymentRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel";
    public static final String ENTITY_NAME = "rosobrReportToPaymentRel";
    public static final int VERSION_HASH = -2095577339;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIRST = "first";
    public static final String L_SECOND = "second";
    public static final String P_INCLUDE_PAYMENT = "includePayment";
    public static final String P_SEPARATE_COLUMN = "separateColumn";

    private RosobrReports _first;     // Отчеты по формам Рособразования
    private Payment _second;     // Выплата
    private boolean _includePayment;     // Включить в отчет
    private boolean _separateColumn;     // Выводить отдельным столбцом

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отчеты по формам Рособразования. Свойство не может быть null.
     */
    @NotNull
    public RosobrReports getFirst()
    {
        return _first;
    }

    /**
     * @param first Отчеты по формам Рособразования. Свойство не может быть null.
     */
    public void setFirst(RosobrReports first)
    {
        dirty(_first, first);
        _first = first;
    }

    /**
     * @return Выплата. Свойство не может быть null.
     */
    @NotNull
    public Payment getSecond()
    {
        return _second;
    }

    /**
     * @param second Выплата. Свойство не может быть null.
     */
    public void setSecond(Payment second)
    {
        dirty(_second, second);
        _second = second;
    }

    /**
     * @return Включить в отчет. Свойство не может быть null.
     */
    @NotNull
    public boolean isIncludePayment()
    {
        return _includePayment;
    }

    /**
     * @param includePayment Включить в отчет. Свойство не может быть null.
     */
    public void setIncludePayment(boolean includePayment)
    {
        dirty(_includePayment, includePayment);
        _includePayment = includePayment;
    }

    /**
     * @return Выводить отдельным столбцом. Свойство не может быть null.
     */
    @NotNull
    public boolean isSeparateColumn()
    {
        return _separateColumn;
    }

    /**
     * @param separateColumn Выводить отдельным столбцом. Свойство не может быть null.
     */
    public void setSeparateColumn(boolean separateColumn)
    {
        dirty(_separateColumn, separateColumn);
        _separateColumn = separateColumn;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RosobrReportToPaymentRelGen)
        {
            setFirst(((RosobrReportToPaymentRel)another).getFirst());
            setSecond(((RosobrReportToPaymentRel)another).getSecond());
            setIncludePayment(((RosobrReportToPaymentRel)another).isIncludePayment());
            setSeparateColumn(((RosobrReportToPaymentRel)another).isSeparateColumn());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RosobrReportToPaymentRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RosobrReportToPaymentRel.class;
        }

        public T newInstance()
        {
            return (T) new RosobrReportToPaymentRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "first":
                    return obj.getFirst();
                case "second":
                    return obj.getSecond();
                case "includePayment":
                    return obj.isIncludePayment();
                case "separateColumn":
                    return obj.isSeparateColumn();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "first":
                    obj.setFirst((RosobrReports) value);
                    return;
                case "second":
                    obj.setSecond((Payment) value);
                    return;
                case "includePayment":
                    obj.setIncludePayment((Boolean) value);
                    return;
                case "separateColumn":
                    obj.setSeparateColumn((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "first":
                        return true;
                case "second":
                        return true;
                case "includePayment":
                        return true;
                case "separateColumn":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "first":
                    return true;
                case "second":
                    return true;
                case "includePayment":
                    return true;
                case "separateColumn":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "first":
                    return RosobrReports.class;
                case "second":
                    return Payment.class;
                case "includePayment":
                    return Boolean.class;
                case "separateColumn":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RosobrReportToPaymentRel> _dslPath = new Path<RosobrReportToPaymentRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RosobrReportToPaymentRel");
    }
            

    /**
     * @return Отчеты по формам Рособразования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel#getFirst()
     */
    public static RosobrReports.Path<RosobrReports> first()
    {
        return _dslPath.first();
    }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel#getSecond()
     */
    public static Payment.Path<Payment> second()
    {
        return _dslPath.second();
    }

    /**
     * @return Включить в отчет. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel#isIncludePayment()
     */
    public static PropertyPath<Boolean> includePayment()
    {
        return _dslPath.includePayment();
    }

    /**
     * @return Выводить отдельным столбцом. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel#isSeparateColumn()
     */
    public static PropertyPath<Boolean> separateColumn()
    {
        return _dslPath.separateColumn();
    }

    public static class Path<E extends RosobrReportToPaymentRel> extends EntityPath<E>
    {
        private RosobrReports.Path<RosobrReports> _first;
        private Payment.Path<Payment> _second;
        private PropertyPath<Boolean> _includePayment;
        private PropertyPath<Boolean> _separateColumn;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отчеты по формам Рособразования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel#getFirst()
     */
        public RosobrReports.Path<RosobrReports> first()
        {
            if(_first == null )
                _first = new RosobrReports.Path<RosobrReports>(L_FIRST, this);
            return _first;
        }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel#getSecond()
     */
        public Payment.Path<Payment> second()
        {
            if(_second == null )
                _second = new Payment.Path<Payment>(L_SECOND, this);
            return _second;
        }

    /**
     * @return Включить в отчет. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel#isIncludePayment()
     */
        public PropertyPath<Boolean> includePayment()
        {
            if(_includePayment == null )
                _includePayment = new PropertyPath<Boolean>(RosobrReportToPaymentRelGen.P_INCLUDE_PAYMENT, this);
            return _includePayment;
        }

    /**
     * @return Выводить отдельным столбцом. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel#isSeparateColumn()
     */
        public PropertyPath<Boolean> separateColumn()
        {
            if(_separateColumn == null )
                _separateColumn = new PropertyPath<Boolean>(RosobrReportToPaymentRelGen.P_SEPARATE_COLUMN, this);
            return _separateColumn;
        }

        public Class getEntityClass()
        {
            return RosobrReportToPaymentRel.class;
        }

        public String getEntityName()
        {
            return "rosobrReportToPaymentRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
