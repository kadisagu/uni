/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffListAllocation.Add;

import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;

/**
 * @author dseleznev
 * Created on: 15.11.2010
 */
public class EmployeePostFakeWrapper
{
    private Long _id;
    private String _lastName;
    private String _firstName;
    private String _middleName;
    private Long _employeePostId;
    private FinancingSource _financingSource;
    private FinancingSourceItem _financingSourceItem;
    private PostType _postType;
    private OrgUnit _orgUnit;
    private OrgUnitTypePostRelation _postRelation;
    private double _budgetStaffRate;
    private double _offBudgetStaffRate;
    private double _salary = 0d;

    public EmployeePostFakeWrapper()
    {
    }

    public EmployeePostFakeWrapper(Object[] fieldsArray)
    {
        _id = (Long)fieldsArray[0];
        _lastName = (String)fieldsArray[1];
        _firstName = (String)fieldsArray[2];
        _middleName = (String)fieldsArray[3];
        _employeePostId = (Long)fieldsArray[4];
        _postType = (PostType)fieldsArray[5];
        _orgUnit = (OrgUnit)fieldsArray[6];
        _postRelation = (OrgUnitTypePostRelation)fieldsArray[7];
        _budgetStaffRate = (Double)fieldsArray[8];
        _offBudgetStaffRate = (Double)fieldsArray[9];
        _salary = (Double) fieldsArray[10];
        _financingSource = (FinancingSource) fieldsArray[11];
        _financingSourceItem = (FinancingSourceItem) fieldsArray[12];
    }

    public EmployeePostFakeWrapper(Long id, String lastName, String firstName, String middleName, Long employeePostId, PostType postType, OrgUnit orgUnit, OrgUnitTypePostRelation postRelation, double budgetStaffRate, double offBudgetStaffRate, double salary, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        _id = id;
        _lastName = lastName;
        _firstName = firstName;
        _middleName = middleName;
        _employeePostId = employeePostId;
        _postType = postType;
        _orgUnit = orgUnit;
        _postRelation = postRelation;
        _budgetStaffRate = budgetStaffRate;
        _offBudgetStaffRate = offBudgetStaffRate;
        _salary = salary;
        _financingSource = financingSource;
        _financingSourceItem = financingSourceItem;
    }

    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    public void setFinancingSource(FinancingSource financingSource)
    {
        _financingSource = financingSource;
    }

    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        _financingSourceItem = financingSourceItem;
    }

    public String getFullFio()
    {
        return getLastName() + " " + getFirstName() + (null != getMiddleName() ? (" " + getMiddleName()) : "");
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        this._id = id;
    }

    public String getLastName()
    {
        return _lastName;
    }

    public void setLastName(String lastName)
    {
        this._lastName = lastName;
    }

    public String getFirstName()
    {
        return _firstName;
    }

    public void setFirstName(String firstName)
    {
        this._firstName = firstName;
    }

    public String getMiddleName()
    {
        return _middleName;
    }

    public void setMiddleName(String middleName)
    {
        this._middleName = middleName;
    }

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        this._employeePostId = employeePostId;
    }

    public PostType getPostType()
    {
        return _postType;
    }

    public void setPostType(PostType postType)
    {
        this._postType = postType;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public OrgUnitTypePostRelation getPostRelation()
    {
        return _postRelation;
    }

    public void setPostRelation(OrgUnitTypePostRelation postRelation)
    {
        this._postRelation = postRelation;
    }

    public double getBudgetStaffRate()
    {
        return _budgetStaffRate;
    }

    public void setBudgetStaffRate(double budgetStaffRate)
    {
        this._budgetStaffRate = budgetStaffRate;
    }

    public double getOffBudgetStaffRate()
    {
        return _offBudgetStaffRate;
    }

    public void setOffBudgetStaffRate(double offBudgetStaffRate)
    {
        this._offBudgetStaffRate = offBudgetStaffRate;
    }

    public double getSalary()
    {
        return _salary;
    }

    public void setSalary(double salary)
    {
        this._salary = salary;
    }
}