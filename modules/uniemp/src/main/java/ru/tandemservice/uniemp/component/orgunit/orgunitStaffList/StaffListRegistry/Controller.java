/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.StaffListRegistry;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 20.09.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getModel(component).setSettings(UniBaseUtils.getDataSettings(component, "StaffListRegistry"));
        getDao().prepare(getModel(component));
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<StaffListItem> dataSource = new DynamicListDataSource<>(component, this, 25);
        CheckboxColumn checkboxColumn = new CheckboxColumn();
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Подразделение", StaffListItem.ORG_UNIT_TITLE_KEY);
        linkColumn.setOrderable(false).setMergeRows(true);
        linkColumn.setMergeRowIdResolver(entity -> ((StaffListItem)entity).getStaffList().getId().toString());
        linkColumn.setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Map<String, Object> params = new HashMap<>();
                params.put(PublisherActivator.PUBLISHER_ID_KEY, ((StaffListItem)entity).getStaffList().getId());
                //params.put("selectedPage", "abstractOrgUnit_OrgUnitPostList");
                return params;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return null;
            }
        });
        checkboxColumn.setMergeRowIdResolver(linkColumn.getMergeRowIdResolver()).setOrderable(false);

        dataSource.addColumn(checkboxColumn.setMergeRows(true));
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Номер версии", StaffListItem.staffList().number().s()).setMergeRowIdResolver(linkColumn.getMergeRowIdResolver()).setOrderable(false).setClickable(false).setMergeRows(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", StaffListItem.staffList().creationDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setMergeRowIdResolver(linkColumn.getMergeRowIdResolver()).setOrderable(false).setClickable(false).setMergeRows(true));
        dataSource.addColumn(new SimpleColumn("Дата согласования", StaffListItem.staffList().acceptionDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setMergeRowIdResolver(linkColumn.getMergeRowIdResolver()).setOrderable(false).setClickable(false).setMergeRows(true));
        dataSource.addColumn(new SimpleColumn("Дата отправки в архив", StaffListItem.staffList().archivingDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setMergeRowIdResolver(linkColumn.getMergeRowIdResolver()).setOrderable(false).setClickable(false).setMergeRows(true));
        dataSource.addColumn(new SimpleColumn("Состояние", StaffListItem.staffList().staffListState().title().s()).setMergeRowIdResolver(linkColumn.getMergeRowIdResolver()).setOrderable(false).setClickable(false).setMergeRows(true));
        dataSource.addColumn(new SimpleColumn("Должность", StaffListItem.POST_TITLE_WITH_SALARY_KEY).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во штатных единиц", StaffListItem.P_STAFF_RATE, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Из них занято", StaffListItem.P_OCC_STAFF_RATE, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", new String[] {StaffListItem.L_FINANCING_SOURCE, FinancingSource.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", new String[] {StaffListItem.L_FINANCING_SOURCE_ITEM, FinancingSourceItem.P_TITLE}).setClickable(false).setOrderable(false));
        model.setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickChangeState(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

        Collection<IEntity> selectedObjects = ((CheckboxColumn) model.getDataSource().getColumn(0)).getSelectedObjects();

        if (selectedObjects.isEmpty())
            errorCollector.add("Необходимо выбрать хотя бы одну версию ШР в списке.");

        if (errorCollector.hasErrors())
            return;

        StaffListItem staffListItem = (StaffListItem) selectedObjects.toArray()[0];

        for (IEntity listItem : selectedObjects)
            if (!staffListItem.getStaffList().getStaffListState().getCode().equals(((StaffListItem) listItem).getStaffList().getStaffListState().getCode()))
                errorCollector.add("Необходимо выбрать версии ШР подразделений в одном состоянии.");

        if (errorCollector.hasErrors())
            return;

        List<Long> staffListIdList = new ArrayList<>();
        for (IEntity item :  selectedObjects)
            staffListIdList.add(((StaffListItem)item).getStaffList().getId());

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IUniempComponents.STAFF_LIST_REGISTRY_CHANGE_STATE,
                new ParametersMap().add("staffListIdList", staffListIdList)));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component); 
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

}