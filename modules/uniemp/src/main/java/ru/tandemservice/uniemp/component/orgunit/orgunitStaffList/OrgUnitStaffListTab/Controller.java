/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitStaffListTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.StaffList;

/**
 * @author dseleznev
 * Created on: 16.09.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareStaffListDataSource(component);
    }

    private void prepareStaffListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getStaffListDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<StaffList> staffDataSource = new DynamicListDataSource<>(component, this);
        staffDataSource.addColumn(new SimpleColumn("Дата создания", StaffList.P_CREATION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false));
        staffDataSource.addColumn(new SimpleColumn("Номер версии", StaffList.P_NUMBER).setOrderable(false).setClickable(false));
        staffDataSource.addColumn(new SimpleColumn("Дата согласования", StaffList.P_ACCEPTION_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        staffDataSource.addColumn(new SimpleColumn("Состояние", StaffList.L_STAFF_LIST_STATE + "." + StaffListState.P_TITLE).setOrderable(false).setClickable(false));

        if (!model.getOrgUnit().isArchival())
        {
            staffDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditStaffList").setDisabledProperty(StaffList.P_CANT_BE_EDITED).setPermissionKey("orgUnit_editStaffList_" + getModel(component).getOrgUnit().getOrgUnitType().getCode()));
            staffDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteStaffList", "Удалить штатное расписание № {0} от {1}?", new Object[] { StaffList.P_NUMBER, StaffList.P_CREATION_DATE_STR }).setDisabledProperty(StaffList.P_CANT_BE_DELETED).setPermissionKey("orgUnit_deleteStaffList_" + getModel(component).getOrgUnit().getOrgUnitType().getCode()));
        }

        getModel(component).setStaffListDataSource(staffDataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickAddStaffList(IBusinessComponent component)
    {
        getModel(component).setStaffListId(null);
        component.createChildRegion("staffListScope", new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ADDEDIT));
    }

    public void onClickEditStaffList(IBusinessComponent component)
    {
        getModel(component).setStaffListId((Long)component.getListenerParameter());
        component.createChildRegion("staffListScope", new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ADDEDIT));
    }

    public void onClickDeleteStaffList(IBusinessComponent component)
    {
        getDao().delete((Long)component.getListenerParameter());
    }
}
