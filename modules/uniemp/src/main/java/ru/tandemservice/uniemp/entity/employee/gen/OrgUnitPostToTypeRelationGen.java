package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostToTypeRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь должности на подразделении с типами назначения на должность
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitPostToTypeRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.OrgUnitPostToTypeRelation";
    public static final String ENTITY_NAME = "orgUnitPostToTypeRelation";
    public static final int VERSION_HASH = 943223157;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT_POST_RELATION = "orgUnitPostRelation";
    public static final String L_POST_TYPE = "postType";

    private OrgUnitPostRelation _orgUnitPostRelation;     // Должность на подразделении
    private PostType _postType;     // Тип назначения на должность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     */
    @NotNull
    public OrgUnitPostRelation getOrgUnitPostRelation()
    {
        return _orgUnitPostRelation;
    }

    /**
     * @param orgUnitPostRelation Должность на подразделении. Свойство не может быть null.
     */
    public void setOrgUnitPostRelation(OrgUnitPostRelation orgUnitPostRelation)
    {
        dirty(_orgUnitPostRelation, orgUnitPostRelation);
        _orgUnitPostRelation = orgUnitPostRelation;
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     */
    @NotNull
    public PostType getPostType()
    {
        return _postType;
    }

    /**
     * @param postType Тип назначения на должность. Свойство не может быть null.
     */
    public void setPostType(PostType postType)
    {
        dirty(_postType, postType);
        _postType = postType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitPostToTypeRelationGen)
        {
            setOrgUnitPostRelation(((OrgUnitPostToTypeRelation)another).getOrgUnitPostRelation());
            setPostType(((OrgUnitPostToTypeRelation)another).getPostType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitPostToTypeRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitPostToTypeRelation.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitPostToTypeRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnitPostRelation":
                    return obj.getOrgUnitPostRelation();
                case "postType":
                    return obj.getPostType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnitPostRelation":
                    obj.setOrgUnitPostRelation((OrgUnitPostRelation) value);
                    return;
                case "postType":
                    obj.setPostType((PostType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnitPostRelation":
                        return true;
                case "postType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnitPostRelation":
                    return true;
                case "postType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnitPostRelation":
                    return OrgUnitPostRelation.class;
                case "postType":
                    return PostType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitPostToTypeRelation> _dslPath = new Path<OrgUnitPostToTypeRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitPostToTypeRelation");
    }
            

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToTypeRelation#getOrgUnitPostRelation()
     */
    public static OrgUnitPostRelation.Path<OrgUnitPostRelation> orgUnitPostRelation()
    {
        return _dslPath.orgUnitPostRelation();
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToTypeRelation#getPostType()
     */
    public static PostType.Path<PostType> postType()
    {
        return _dslPath.postType();
    }

    public static class Path<E extends OrgUnitPostToTypeRelation> extends EntityPath<E>
    {
        private OrgUnitPostRelation.Path<OrgUnitPostRelation> _orgUnitPostRelation;
        private PostType.Path<PostType> _postType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToTypeRelation#getOrgUnitPostRelation()
     */
        public OrgUnitPostRelation.Path<OrgUnitPostRelation> orgUnitPostRelation()
        {
            if(_orgUnitPostRelation == null )
                _orgUnitPostRelation = new OrgUnitPostRelation.Path<OrgUnitPostRelation>(L_ORG_UNIT_POST_RELATION, this);
            return _orgUnitPostRelation;
        }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostToTypeRelation#getPostType()
     */
        public PostType.Path<PostType> postType()
        {
            if(_postType == null )
                _postType = new PostType.Path<PostType>(L_POST_TYPE, this);
            return _postType;
        }

        public Class getEntityClass()
        {
            return OrgUnitPostToTypeRelation.class;
        }

        public String getEntityName()
        {
            return "orgUnitPostToTypeRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
