/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.benefitData.BenefitDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.paymentData.PaymentDataBlock;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
@Configuration
public class EmpReportPersonPaymentsDataTab extends BusinessComponentManager
{
    // tab block lists
    public static final String EMPLOYEE_PAYMENTS_DATA_BLOCK_LIST = "employeePaymentsDataBlockList";

    // block names
    public static final String PAYMENT_DATA = "paymentData";
    public static final String BENEFIT_DATA = "benefitData";

    @Bean
    public BlockListExtPoint employeePaymentsDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(EMPLOYEE_PAYMENTS_DATA_BLOCK_LIST)
                .addBlock(htmlBlock(PAYMENT_DATA, "block/paymentData/PaymentData"))
                .addBlock(htmlBlock(BENEFIT_DATA, "block/benefitData/BenefitData"))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler paymentTypeComboDSHandler()
    {
        return PaymentDataBlock.createPaymentTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler paymentComboDSHandler()
    {
        return PaymentDataBlock.createPaymentDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler encouragementTypeComboDSHandler()
    {
        return BenefitDataBlock.createEncouragementTypeDS(getName());
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PaymentDataBlock.PAYMENT_TYPE_DS, paymentTypeComboDSHandler()))
                .addDataSource(selectDS(PaymentDataBlock.PAYMENT_DS, paymentComboDSHandler()))
                .addDataSource(selectDS(BenefitDataBlock.ENCOURAGEMENT_TYPE_DS, encouragementTypeComboDSHandler()))
                .create();
    }
}
