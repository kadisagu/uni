/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeCombinationPostAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.codes.CombinationPostTypeCodes;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;

import java.util.*;

/**
 * Create by ashaburov
 * Date 08.12.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
        prepareStaffRateDataSource(component);

        if (model.getEntityId() != null)
        {
            hasActiveStaffList(component);
            fillStaffRateValueMaps(model);
        }
    }

    public void onClickFreelance(IBusinessComponent component)
    {
        hasActiveStaffList(component);
    }

    public void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        component.getModel(IUniComponents.EMPLOYEE_POST_ADD_EDIT);
        if (!model.isNeedUpdateDataSource() && model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<CombinationPostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> getDao().prepareStaffRateDataSource(model), 5);

        dataSource.addColumn(new BlockColumn("staffRate", "Ставка"));
        dataSource.addColumn(new BlockColumn("financingSource", "Источник финансирования"));
        dataSource.addColumn(new BlockColumn("financingSourceItem", "Источник финансирования (детально)"));
        if (model.isThereAnyActiveStaffList())
            dataSource.addColumn(new BlockColumn("employeeHR", "Кадровая расстановка"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem");
        actionColumn.setDisableSecondSubmit(false);
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setStaffRateDataSource(dataSource);
    }

    public void onClickAddStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        CombinationPostStaffRateItem item = new CombinationPostStaffRateItem();

        short discriminator = EntityRuntime.getMeta(CombinationPostStaffRateItem.class).getEntityCode();
        long id = EntityIDGenerator.generateNewId(discriminator);

        item.setId(id);
        item.setCombinationPost(model.getEntity());

        model.getCombinationPostStaffRateItemList().add(item);

        model.getStaffRateDataSource().refresh();
    }

    @SuppressWarnings("unchecked")
    public void onClickDeleteItem(IBusinessComponent component)
    {
        Model model = getModel(component);

        CombinationPostStaffRateItem item = component.getListenerParameter();

        model.getCombinationPostStaffRateItemList().remove(item);

        model.getStaffRateDataSource().refresh();

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double staffRateSumm = 0d;
        for (CombinationPostStaffRateItem staffRateItem : model.getCombinationPostStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(staffRateItem.getId()))
            {
                Object value = staffRateMap.get(staffRateItem.getId());
                val = ((Number) value).doubleValue();
            }

            staffRateSumm += val;
        }

        if (model.getEntity().getSalaryRaisingCoefficient() != null)
            model.getEntity().setSalary(model.getEntity().getSalaryRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (model.getEntity().getPostBoundedWithQGandQL().getSalary() != null)
            model.getEntity().setSalary(model.getEntity().getPostBoundedWithQGandQL().getSalary() * staffRateSumm);
    }

    @SuppressWarnings("unchecked")
    public void onChangeStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double staffRateSumm = 0d;
        for (CombinationPostStaffRateItem item : model.getCombinationPostStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                val = ((Number) value).doubleValue();
            }

            staffRateSumm += val;
        }

        if (model.getEntity().getSalaryRaisingCoefficient() != null)
            model.getEntity().setSalary(model.getEntity().getSalaryRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (model.getEntity().getPostBoundedWithQGandQL().getSalary() != null)
            model.getEntity().setSalary(model.getEntity().getPostBoundedWithQGandQL().getSalary() * staffRateSumm);
    }

    public void onChangeFinSrc(IBusinessComponent component)
    {

    }

    @SuppressWarnings("unchecked")
    public void onChangePostRaisingCoefficient(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double staffRateSumm = 0d;
        for (CombinationPostStaffRateItem item : model.getCombinationPostStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                val = ((Number) value).doubleValue();
            }

            staffRateSumm += val;
        }

        if (model.getEntity().getSalaryRaisingCoefficient() != null)
            model.getEntity().setSalary(model.getEntity().getSalaryRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (model.getEntity().getPostBoundedWithQGandQL() != null && model.getEntity().getPostBoundedWithQGandQL().getSalary() != null)
            model.getEntity().setSalary(model.getEntity().getPostBoundedWithQGandQL().getSalary() * staffRateSumm);
    }

    @SuppressWarnings("unchecked")
    public void prepareEmpHRColumn(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getEntityId() == null || !model.isThereAnyActiveStaffList())
            return;

        StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEntity().getOrgUnit());
        if (model.getEntity().getPostBoundedWithQGandQL() != null &&
                activeStaffList != null)
        {
//            actualizeStaffRateItemList(model);
            prepareStaffRateDataSource(component);

            fillStaffRateValueMaps(model);
            Map<Long, List<IdentifiableWrapper>> idWrapperMap = new HashMap<>();
            for (CombinationPostStaffRateItem rateItem : model.getCombinationPostStaffRateItemList())
            {
                List<IdentifiableWrapper> wrapperList = new ArrayList<>();
                List<StaffListAllocationItem> allocationItemList = UniempDaoFacade.getStaffListDAO().getStaffListAllocationItemList(activeStaffList, model.getEntity(), model.getEntity().getPostBoundedWithQGandQL(), rateItem.getFinancingSource(), rateItem.getFinancingSourceItem());

                for (StaffListAllocationItem allocationItem : allocationItemList)
                    if (allocationItem.getCombinationPost() != null && allocationItem.getCombinationPost().equals(model.getEntity()))
                        wrapperList.add(new IdentifiableWrapper(allocationItem.getId(), allocationItem.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allocationItem.getStaffRate()) + " - " + allocationItem.getCombinationPost().getCombinationPostType().getShortTitle() +  " " + allocationItem.getEmployeePost().getPostStatus().getShortTitle()));

                idWrapperMap.put(rateItem.getId(), wrapperList);
            }
            if (!idWrapperMap.isEmpty())
                ((BlockColumn) model.getStaffRateDataSource().getColumn(3)).setValueMap(idWrapperMap);

        }
    }

    public void hasActiveStaffList(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getEntity().getOrgUnit() != null &&
                UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEntity().getOrgUnit()) != null && !model.getEntity().isFreelance())
        {
            if (!model.isThereAnyActiveStaffList())
            {
                model.setThereAnyActiveStaffList(true);
                model.setStaffRateDataSource(null);
                prepareStaffRateDataSource(component);
                prepareEmpHRColumn(component);
            }
        }
        else
            if (model.isThereAnyActiveStaffList())
            {
                model.setThereAnyActiveStaffList(false);
                model.setStaffRateDataSource(null);
                prepareStaffRateDataSource(component);
            }
    }

    public void onChangeOrgUnit(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.getEntity().setPostBoundedWithQGandQL(null);
        if (!model.getCombinationPostStaffRateItemList().isEmpty())
        {
            model.setCombinationPostStaffRateItemList(new ArrayList<>());
            model.getStaffRateDataSource().refresh();
        }

        hasActiveStaffList(component);

        InfoCollector infoCollector = ContextLocal.getInfoCollector();
        getDao().validateCombinationPeriods(model, infoCollector);
    }

    public void onChangePost(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.getEntity().setSalaryRaisingCoefficient(null);

        if (!model.getCombinationPostStaffRateItemList().isEmpty())
        {
            model.setCombinationPostStaffRateItemList(new ArrayList<>());
            model.getStaffRateDataSource().refresh();
        }

        if (model.getEntity().getPostBoundedWithQGandQL().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
            model.getEntity().setCombinationPostType(getDao().getCatalogItem(CombinationPostType.class, CombinationPostTypeCodes.INC_WORK));
        else if (!model.getEntity().getPostBoundedWithQGandQL().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
            model.getEntity().setCombinationPostType(getDao().getCatalogItem(CombinationPostType.class, CombinationPostTypeCodes.COMBINATION));

        InfoCollector infoCollector = ContextLocal.getInfoCollector();
        getDao().validateCombinationPeriods(model, infoCollector);
    }

    public void onChangeDate(IBusinessComponent component)
    {
        Model model = getModel(component);
        InfoCollector infoCollector = ContextLocal.getInfoCollector();
        getDao().validateCombinationPeriods(model, infoCollector);
    }

    @SuppressWarnings("unchecked")
    public void actualizeStaffRateItemList(Model model)
    {
        //достаем введенные знаечения для Ставок и обновляем их в списке
        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

        final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

        for (CombinationPostStaffRateItem item : model.getCombinationPostStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                val = ((Number) value).doubleValue();
            }
            item.setStaffRate(val);
            item.setFinancingSource(finSrcMap.get(item.getId()));
            item.setFinancingSourceItem(finSrcItmMap.get(item.getId()));
        }
    }

    @SuppressWarnings("unchecked")
    public void fillStaffRateValueMaps(Model model)
    {
        Map<Long, Double> staffRateMap = new HashMap<>();
        for (CombinationPostStaffRateItem item : model.getCombinationPostStaffRateItemList())
            staffRateMap.put(item.getId(), item.getStaffRate());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(0)).setValueMap(staffRateMap);

        Map<Long, FinancingSource> finSrcMap = new HashMap<>();
        for (CombinationPostStaffRateItem item : model.getCombinationPostStaffRateItemList())
            finSrcMap.put(item.getId(), item.getFinancingSource());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(1)).setValueMap(finSrcMap);

        Map<Long, FinancingSourceItem> finSrcItmMap = new HashMap<>();
        for (CombinationPostStaffRateItem item : model.getCombinationPostStaffRateItemList())
            finSrcItmMap.put(item.getId(), item.getFinancingSourceItem());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(2)).setValueMap(finSrcItmMap);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        actualizeStaffRateItemList(model);

        getDao().validate(model, errorCollector);

        if (errorCollector.hasErrors())
        {
            return;
        }


        getDao().update(model);

        deactivate(component);
    }
}
