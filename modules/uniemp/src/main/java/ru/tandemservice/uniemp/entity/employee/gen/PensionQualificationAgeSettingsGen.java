package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка Пенсионный возраст
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PensionQualificationAgeSettingsGen extends EntityBase
 implements INaturalIdentifiable<PensionQualificationAgeSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings";
    public static final String ENTITY_NAME = "pensionQualificationAgeSettings";
    public static final int VERSION_HASH = -836787531;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_SEX = "sex";
    public static final String P_DATE = "date";
    public static final String P_AGE = "age";
    public static final String P_DOCUMENT_TITLE = "documentTitle";
    public static final String P_DOCUMENT_NUMBER = "documentNumber";
    public static final String P_DOCUMENT_DATE = "documentDate";

    private String _code;     // Код
    private Sex _sex;     // Пол
    private Date _date;     // Дата установления
    private int _age;     // Возраст (лет)
    private String _documentTitle;     // Название документа
    private String _documentNumber;     // Номер документа
    private Date _documentDate;     // Дата документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Пол. Свойство не может быть null.
     */
    @NotNull
    public Sex getSex()
    {
        return _sex;
    }

    /**
     * @param sex Пол. Свойство не может быть null.
     */
    public void setSex(Sex sex)
    {
        dirty(_sex, sex);
        _sex = sex;
    }

    /**
     * @return Дата установления. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата установления. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Возраст (лет). Свойство не может быть null.
     */
    @NotNull
    public int getAge()
    {
        return _age;
    }

    /**
     * @param age Возраст (лет). Свойство не может быть null.
     */
    public void setAge(int age)
    {
        dirty(_age, age);
        _age = age;
    }

    /**
     * @return Название документа.
     */
    @Length(max=255)
    public String getDocumentTitle()
    {
        return _documentTitle;
    }

    /**
     * @param documentTitle Название документа.
     */
    public void setDocumentTitle(String documentTitle)
    {
        dirty(_documentTitle, documentTitle);
        _documentTitle = documentTitle;
    }

    /**
     * @return Номер документа.
     */
    @Length(max=255)
    public String getDocumentNumber()
    {
        return _documentNumber;
    }

    /**
     * @param documentNumber Номер документа.
     */
    public void setDocumentNumber(String documentNumber)
    {
        dirty(_documentNumber, documentNumber);
        _documentNumber = documentNumber;
    }

    /**
     * @return Дата документа.
     */
    public Date getDocumentDate()
    {
        return _documentDate;
    }

    /**
     * @param documentDate Дата документа.
     */
    public void setDocumentDate(Date documentDate)
    {
        dirty(_documentDate, documentDate);
        _documentDate = documentDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PensionQualificationAgeSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((PensionQualificationAgeSettings)another).getCode());
            }
            setSex(((PensionQualificationAgeSettings)another).getSex());
            setDate(((PensionQualificationAgeSettings)another).getDate());
            setAge(((PensionQualificationAgeSettings)another).getAge());
            setDocumentTitle(((PensionQualificationAgeSettings)another).getDocumentTitle());
            setDocumentNumber(((PensionQualificationAgeSettings)another).getDocumentNumber());
            setDocumentDate(((PensionQualificationAgeSettings)another).getDocumentDate());
        }
    }

    public INaturalId<PensionQualificationAgeSettingsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<PensionQualificationAgeSettingsGen>
    {
        private static final String PROXY_NAME = "PensionQualificationAgeSettingsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PensionQualificationAgeSettingsGen.NaturalId) ) return false;

            PensionQualificationAgeSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PensionQualificationAgeSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PensionQualificationAgeSettings.class;
        }

        public T newInstance()
        {
            return (T) new PensionQualificationAgeSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "sex":
                    return obj.getSex();
                case "date":
                    return obj.getDate();
                case "age":
                    return obj.getAge();
                case "documentTitle":
                    return obj.getDocumentTitle();
                case "documentNumber":
                    return obj.getDocumentNumber();
                case "documentDate":
                    return obj.getDocumentDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "sex":
                    obj.setSex((Sex) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "age":
                    obj.setAge((Integer) value);
                    return;
                case "documentTitle":
                    obj.setDocumentTitle((String) value);
                    return;
                case "documentNumber":
                    obj.setDocumentNumber((String) value);
                    return;
                case "documentDate":
                    obj.setDocumentDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "sex":
                        return true;
                case "date":
                        return true;
                case "age":
                        return true;
                case "documentTitle":
                        return true;
                case "documentNumber":
                        return true;
                case "documentDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "sex":
                    return true;
                case "date":
                    return true;
                case "age":
                    return true;
                case "documentTitle":
                    return true;
                case "documentNumber":
                    return true;
                case "documentDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "sex":
                    return Sex.class;
                case "date":
                    return Date.class;
                case "age":
                    return Integer.class;
                case "documentTitle":
                    return String.class;
                case "documentNumber":
                    return String.class;
                case "documentDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PensionQualificationAgeSettings> _dslPath = new Path<PensionQualificationAgeSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PensionQualificationAgeSettings");
    }
            

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getSex()
     */
    public static Sex.Path<Sex> sex()
    {
        return _dslPath.sex();
    }

    /**
     * @return Дата установления. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Возраст (лет). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getAge()
     */
    public static PropertyPath<Integer> age()
    {
        return _dslPath.age();
    }

    /**
     * @return Название документа.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getDocumentTitle()
     */
    public static PropertyPath<String> documentTitle()
    {
        return _dslPath.documentTitle();
    }

    /**
     * @return Номер документа.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getDocumentNumber()
     */
    public static PropertyPath<String> documentNumber()
    {
        return _dslPath.documentNumber();
    }

    /**
     * @return Дата документа.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getDocumentDate()
     */
    public static PropertyPath<Date> documentDate()
    {
        return _dslPath.documentDate();
    }

    public static class Path<E extends PensionQualificationAgeSettings> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private Sex.Path<Sex> _sex;
        private PropertyPath<Date> _date;
        private PropertyPath<Integer> _age;
        private PropertyPath<String> _documentTitle;
        private PropertyPath<String> _documentNumber;
        private PropertyPath<Date> _documentDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(PensionQualificationAgeSettingsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getSex()
     */
        public Sex.Path<Sex> sex()
        {
            if(_sex == null )
                _sex = new Sex.Path<Sex>(L_SEX, this);
            return _sex;
        }

    /**
     * @return Дата установления. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(PensionQualificationAgeSettingsGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Возраст (лет). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getAge()
     */
        public PropertyPath<Integer> age()
        {
            if(_age == null )
                _age = new PropertyPath<Integer>(PensionQualificationAgeSettingsGen.P_AGE, this);
            return _age;
        }

    /**
     * @return Название документа.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getDocumentTitle()
     */
        public PropertyPath<String> documentTitle()
        {
            if(_documentTitle == null )
                _documentTitle = new PropertyPath<String>(PensionQualificationAgeSettingsGen.P_DOCUMENT_TITLE, this);
            return _documentTitle;
        }

    /**
     * @return Номер документа.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getDocumentNumber()
     */
        public PropertyPath<String> documentNumber()
        {
            if(_documentNumber == null )
                _documentNumber = new PropertyPath<String>(PensionQualificationAgeSettingsGen.P_DOCUMENT_NUMBER, this);
            return _documentNumber;
        }

    /**
     * @return Дата документа.
     * @see ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings#getDocumentDate()
     */
        public PropertyPath<Date> documentDate()
        {
            if(_documentDate == null )
                _documentDate = new PropertyPath<Date>(PensionQualificationAgeSettingsGen.P_DOCUMENT_DATE, this);
            return _documentDate;
        }

        public Class getEntityClass()
        {
            return PensionQualificationAgeSettings.class;
        }

        public String getEntityName()
        {
            return "pensionQualificationAgeSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
