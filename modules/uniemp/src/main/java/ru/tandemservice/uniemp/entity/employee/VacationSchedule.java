package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.gen.VacationScheduleGen;
import ru.tandemservice.unimv.IAbstractDocument;

/**
 * График отпусков
 */
public class VacationSchedule extends VacationScheduleGen implements IAbstractDocument
{
    public static final String EDIT_DISABLED = "editDisabled";
    public static final String DELETE_DISABLED = "deleteDisabled";

    public boolean isEditDisabled()
    {
        return getOrgUnit().isArchival() || !UniempDefines.STAFF_LIST_STATUS_FORMING.equals(getState().getCode());
    }

    public boolean isDeleteDisabled()
    {
        return getOrgUnit().isArchival();
    }

    @Override
    public String getTitle()
    {
        if (getOrgUnit() == null) {
            return this.getClass().getSimpleName();
        }
        StringBuilder result = new StringBuilder("График отпусков");
        if (null != getNumber()) result.append(" № ").append(getNumber());
        result.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate()));
        result.append(" - ").append(getOrgUnit().getFullTitle());
        return result.toString();
    }

    @Override
    public ICatalogItem getType()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @EntityDSLSupport(parts = {VacationScheduleGen.P_YEAR})
    @Override
    public String getSimpleTitleWithYear()
    {
        return "График отпусков на " + getYear() + " год";
    }

    @EntityDSLSupport(parts = {VacationScheduleGen.P_YEAR, VacationScheduleGen.P_CREATE_DATE})
    @Override
    public String getTitleWithYearAndCreateDate()
    {
        return getSimpleTitleWithYear() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate());
    }

    @Override
    public void setState(ICatalogItem state)
    {
        super.setState((StaffListState)state);
    }
}