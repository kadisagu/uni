/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListAddEdit;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

import java.util.Date;

/**
 * @author dseleznev
 *         Created on: 19.09.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        if (null != model.getOrgUnitId())
            model.setOrgUnit((OrgUnit)get(model.getOrgUnitId()));
        
        if (null != model.getCopyStaffList() && model.getCopyStaffList())
        {
            model.setBaseStaffList(get(StaffList.class, model.getStaffListId()));
            model.getStaffList().setNumber(generateNewItemNumber(model.getBaseStaffList().getOrgUnit()));
            model.getStaffList().setCreationDate(new Date());
            model.getStaffList().setOrgUnit(model.getBaseStaffList().getOrgUnit());
            model.getStaffList().setStaffListState(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));
            
            model.setAddNewRequiredPaymentsVisible(isStaffListDoesntIncludeAllRequiredPayments(model));
            model.setRemoveNonActivePaymentsVisible(isStaffListContainsNonActivePayments(model));
            if(model.getAddNewRequiredPaymentsVisible()) model.setAddNewRequiredPayments(true);
            else model.setAddNewRequiredPayments(true);
            model.setRemoveNonActivePayments(false);
        }
        else if (null == model.getStaffListId())
        {
            model.getStaffList().setNumber(generateNewItemNumber(model.getOrgUnit()));
            model.getStaffList().setCreationDate(new Date());
            model.getStaffList().setOrgUnit(model.getOrgUnit());
            model.getStaffList().setStaffListState(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));
        }
        else
        {
            model.setStaffList(get(StaffList.class, model.getStaffListId()));
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getStaffList());
        if (null != model.getCopyStaffList() && model.getCopyStaffList())
            StaffListPaymentsUtil.copyStaffList(model.getStaffList(), model.getBaseStaffList(), model.getAddNewRequiredPayments(), model.getRemoveNonActivePayments(), getSession());
    }

    private int generateNewItemNumber(OrgUnit orgUnit)
    {
        Number maxNumber = (Number) getSession().createCriteria(StaffList.class).add(Restrictions.eq(StaffList.L_ORG_UNIT, orgUnit)).setProjection(Projections.max(StaffList.P_NUMBER)).uniqueResult();
        return null != maxNumber ? maxNumber.intValue() + 1 : 1;
    }
    
    private boolean isStaffListDoesntIncludeAllRequiredPayments(Model model)
    {
        MQBuilder paymentsBuilder = new MQBuilder(Payment.ENTITY_CLASS, "p");
        paymentsBuilder.add(MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE));
        paymentsBuilder.add(MQExpression.eq("p", Payment.P_REQUIRED, Boolean.TRUE));
        
        MQBuilder rightStaffListBuilder = new MQBuilder(StaffListPostPayment.ENTITY_CLASS, "pp", new String[] {StaffListPostPayment.L_STAFF_LIST_ITEM + "." + StaffListItem.P_ID});
        rightStaffListBuilder.add(MQExpression.eq("pp", StaffListPostPayment.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, model.getBaseStaffList()));
        rightStaffListBuilder.add(MQExpression.in("pp", StaffListPostPayment.L_PAYMENT, paymentsBuilder));
        
        MQBuilder resultBuilder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        resultBuilder.add(MQExpression.notIn("sli", StaffListItem.P_ID, rightStaffListBuilder));

        return resultBuilder.getResultCount(getSession()) > 0;
    }
    
    private boolean isStaffListContainsNonActivePayments(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", StaffListPaymentBase.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, model.getBaseStaffList()));
        builder.add(MQExpression.eq("p", StaffListPaymentBase.L_PAYMENT + "." + Payment.P_ACTIVE, Boolean.FALSE));
        return builder.getResultCount(getSession()) > 0;
    }
}