/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.paymentType.PaymentTypeAddEdit;

import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;

/**
 * @author AutoGenerator
 * Created on 09.12.2008
 */
public class DAO extends DefaultCatalogAddEditDAO<PaymentType, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setPossibleParentsList(HierarchyUtil.listHierarchyNodesWithParents(getList(PaymentType.class), true));
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
    }
}