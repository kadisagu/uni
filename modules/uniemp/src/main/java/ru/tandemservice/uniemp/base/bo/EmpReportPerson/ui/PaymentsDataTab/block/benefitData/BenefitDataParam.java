/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.benefitData;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement;

import java.util.Date;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class BenefitDataParam implements IReportDQLModifier
{
    private IReportParam<List<EncouragementType>> _emplEncrgmntType = new ReportParam<>();
    private IReportParam<Date> _emplEncrgmntBeginFrom = new ReportParam<>();
    private IReportParam<Date> _emplEncrgmntEndTo = new ReportParam<>();

    private String alias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = dql.innerJoinEntity(Employee.class, Employee.person());

        // соединяем поощерение
        return dql.innerJoinEntity(employeeAlias, EmployeeEncouragement.class, EmployeeEncouragement.employee());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_emplEncrgmntType.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "benefitData.emplEncrgmntType", CommonBaseStringUtil.<EncouragementType>join(_emplEncrgmntType.getData(), EncouragementType.P_TITLE, ", "));
            
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(EmployeeEncouragement.type().fromAlias(alias(dql))), _emplEncrgmntType.getData()));
        }

        if (_emplEncrgmntBeginFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "benefitData.emplEncrgmntBeginFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_emplEncrgmntBeginFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeEncouragement.assignDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_emplEncrgmntBeginFrom.getData()))));
        }

        if (_emplEncrgmntEndTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "benefitData.emplEncrgmntEndTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_emplEncrgmntEndTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeEncouragement.assignDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_emplEncrgmntEndTo.getData(), 1))));
        }
    }

    // Getters

    public IReportParam<List<EncouragementType>> getEmplEncrgmntType()
    {
        return _emplEncrgmntType;
    }

    public IReportParam<Date> getEmplEncrgmntBeginFrom()
    {
        return _emplEncrgmntBeginFrom;
    }

    public IReportParam<Date> getEmplEncrgmntEndTo()
    {
        return _emplEncrgmntEndTo;
    }
}
