/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeHolidayAddEdit;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendar;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendarHoliday;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendarHolidayDuration;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.Holiday;
import org.tandemframework.shared.organization.base.util.IHolidayDuration;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 22.12.2008
 */
public class DAO extends UniempDAO<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        if (null != model.getEmployeePostId())
            model.setEmployeePost(get(EmployeePost.class, model.getEmployeePostId()));

        if (null != model.getEmployeeHoliday().getId())
        {
            model.setEmployeeHoliday(get(EmployeeHoliday.class, model.getEmployeeHoliday().getId()));
            if (model.getEmployeeHoliday().getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL))
                model.setPeriodRequired(true);
            else
                model.setPeriodRequired(false);
        }
        else
            model.getEmployeeHoliday().setEmployeePost(model.getEmployeePost());

        model.setHolidayTypesList(getCatalogItemList(HolidayType.class));
    }

    @Override
    public void update(Model model)
    {
//        String orderNumber = (String) model.getEmployeeHoliday().getHolidayExtract().getProperty("paragraph.order.number");
//        Date orderDate = (Date) model.getEmployeeHoliday().getHolidayExtract().getProperty("paragraph.order.commitDate");
//
//        if (!orderNumber.equals(model.getEmployeeHoliday().getHolidayExtractNumber()) || !orderDate.equals(model.getEmployeeHoliday().getHolidayExtractDate()))
//            model.getEmployeeHoliday().setHolidayExtract(null);

        getSession().saveOrUpdate(model.getEmployeeHoliday());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        EmployeeHoliday holiday = model.getEmployeeHoliday();

        if ((holiday.getHolidayExtractDate() == null || holiday.getHolidayExtractNumber() == null) && (holiday.getHolidayExtractDate() != null || holiday.getHolidayExtractNumber() != null))
            errors.add("Дата и Номер приказа должны быть заполнены, либо оставаться пустыми.", "holidayExtractDate", "holidayExtractNumber");

        if(holiday.getBeginDate() != null && holiday.getEndDate() != null && holiday.getBeginDate().getTime() > holiday.getEndDate().getTime())
            errors.add("Дата начала периода должна быть меньше даты его окончания.", "beginDate", "endDate", "beginDateRequired", "endDateRequired");

        if(holiday.getStartDate().getTime() > holiday.getFinishDate().getTime())
            errors.add("Дата начала отпуска должна быть меньше даты его окончания.", "startDate", "finishDate");

        //проверяем, что отпуск не пересекается с существующими отпусками
        MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
        empHolidayBuilder.add(MQExpression.eq("eh", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
        if (model.getEmployeeHoliday().getId() != null)
            empHolidayBuilder.add(MQExpression.notEq("eh", EmployeeHoliday.P_ID, model.getEmployeeHoliday().getId()));
        for (EmployeeHoliday employeeHoliday : empHolidayBuilder.<EmployeeHoliday>getResultList(getSession()))
        {
            Date beginDate = holiday.getStartDate();
            Date endDate = holiday.getFinishDate();

            if (CommonBaseDateUtil.isBetween(beginDate, employeeHoliday.getStartDate(), employeeHoliday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, employeeHoliday.getStartDate(), employeeHoliday.getFinishDate()))
                errors.add("Данный отпуск пересекается с отпуском " + employeeHoliday.getHolidayType().getTitle() +
                        " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeHoliday.getStartDate()) +
                        " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeHoliday.getFinishDate()) + ".", "startDate", "finishDate");
            else {
                if (CommonBaseDateUtil.isBetween(employeeHoliday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(employeeHoliday.getFinishDate(), beginDate, endDate))
                    errors.add("Данный отпуск пересекается с отпуском " + employeeHoliday.getHolidayType().getTitle() +
                            " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeHoliday.getStartDate()) +
                            " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeHoliday.getFinishDate()) + ".", "startDate", "finishDate");
            }
        }

        Criteria crit = getSession().createCriteria(EmployeeHoliday.class);
        crit.add(Restrictions.eq(EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
        if(null != holiday.getId())
            crit.add(Restrictions.ne(EmployeeHoliday.P_ID, model.getEmployeeHoliday().getId()));
        crit.add(Restrictions.ilike(EmployeeHoliday.P_TITLE, model.getEmployeeHoliday().getTitle(), MatchMode.EXACT));
        crit.setProjection(Projections.rowCount());
    }
    //является ли date праздником в производственном календаре
    @Override
    public boolean isIndustrialCalendarHoliday(EmployeeWorkWeekDuration weekDuration, Date date)
    {
        GregorianCalendar dayDate = (GregorianCalendar)GregorianCalendar.getInstance();

        dayDate.setTime(date);

        MQBuilder calendarBuilder = new MQBuilder(IndustrialCalendar.ENTITY_CLASS, "ic");
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.P_YEAR, dayDate.get(Calendar.YEAR)));
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.L_WEEK_DURATION, weekDuration));
        IndustrialCalendar calendar = (IndustrialCalendar)calendarBuilder.uniqueResult(getSession());
        if (null == calendar)
            return false;

        MQBuilder builder = new MQBuilder(IndustrialCalendarHoliday.ENTITY_CLASS, "ich");
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_CALENDAR, calendar));
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_HOLIDAY + "." + Holiday.P_PREHOLIDAY, Boolean.FALSE));
        List<IHolidayDuration> holidays = builder.getResultList(getSession());

        builder = new MQBuilder(IndustrialCalendarHolidayDuration.ENTITY_CLASS, "ichd");
        builder.add(MQExpression.in("ichd", IndustrialCalendarHolidayDuration.L_HOLIDAY, holidays));
        builder.add(MQExpression.greatOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        builder.add(MQExpression.lessOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        holidays.addAll(builder.<IHolidayDuration>getResultList(getSession()));

        for(IHolidayDuration holiday : holidays)
        {
            if (holiday.getMonth() != (dayDate.get(Calendar.MONTH) + 1))
                continue;

            int begin = holiday.getBeginDay();
            int end = null == holiday.getEndDay() ? begin : holiday.getEndDay();

            if (holiday.getMonth() == dayDate.get(Calendar.MONTH) + 1 &&
                    dayDate.get(Calendar.DAY_OF_MONTH) <= end &&
                    dayDate.get(Calendar.DAY_OF_MONTH) >= begin)
                return true;
        }
        return false;
    }

}