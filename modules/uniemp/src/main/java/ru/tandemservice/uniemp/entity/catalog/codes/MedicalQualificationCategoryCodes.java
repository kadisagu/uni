package ru.tandemservice.uniemp.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Квалификационные категории врачей"
 * Имя сущности : medicalQualificationCategory
 * Файл data.xml : uniemp.catalogs.data.xml
 */
public interface MedicalQualificationCategoryCodes
{
    /** Константа кода (code) элемента : Без категории (title) */
    String NO_CATEGORY = "0";
    /** Константа кода (code) элемента : Первая категория (title) */
    String FIRST_CATEGORY = "1";
    /** Константа кода (code) элемента : Вторая категория (title) */
    String SECOND_CATEGORY = "2";
    /** Константа кода (code) элемента : Третья категория (title) */
    String THIRD_CATEGORY = "3";
    /** Константа кода (code) элемента : Высшая категория (title) */
    String HIGHEST_CATEGORY = "4";

    Set<String> CODES = ImmutableSet.of(NO_CATEGORY, FIRST_CATEGORY, SECOND_CATEGORY, THIRD_CATEGORY, HIGHEST_CATEGORY);
}
