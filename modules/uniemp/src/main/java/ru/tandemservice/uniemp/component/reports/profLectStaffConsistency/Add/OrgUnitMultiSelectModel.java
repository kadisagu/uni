/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.profLectStaffConsistency.Add;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;

import java.util.List;
import java.util.Set;

/**
 * @author dseleznev
 * Created on: 13.04.2009
 */
public class OrgUnitMultiSelectModel extends BaseMultiSelectModel
{
    private Model _model;
    private IDAO _dao;

    OrgUnitMultiSelectModel(Model model, IDAO dao)
    {
        super(IEntity.P_ID, new String[] { OrgUnit.P_FULL_TITLE });
        _model = model;
        _dao = dao;
    }

    @Override
    public ListResult findValues(String filter)
    {
        if (null == _model.getOrgUnitTypesList() || _model.getOrgUnitTypesList().isEmpty())
            return ListResult.getEmpty();
        return new ListResult<>(_dao.getOrgUnitsList(_model.getOrgUnitType(), filter));
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        return _dao.getValues(OrgUnit.class, primaryKeys);
    }
}