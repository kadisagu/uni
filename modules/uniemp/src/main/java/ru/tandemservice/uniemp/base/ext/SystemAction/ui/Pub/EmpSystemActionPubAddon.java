/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.hibsupport.log.InsertEntityEvent;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uniemp.base.bo.EmpSystemAction.EmpSystemActionManager;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class EmpSystemActionPubAddon extends UIAddon
{
    public EmpSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickCreateEmployeeCards() throws Exception
    {
        EmpSystemActionManager.instance().dao().updateEmployeeCards();
    }

    public void onClickRecalculateStaffListPayments()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateStaffListPayments();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Произведен перерасчет выплат для должностей штатной расстановки.", TopOrgUnit.getInstance()));
    }

    public void onClickMoveActiveStaffListVersionsToFormation()
    {
        EmpSystemActionManager.instance().dao().updateActiveStaffLists();
    }

    public void onClickSynchronizeEmploymentHistory()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateSynchronizeEmploymentHistory();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Произведена синхронизация истории должностей.", TopOrgUnit.getInstance()));
    }

    @Deprecated
    public void onClickSynchronizeEmploymentHistoryStaffRates()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateSynchronizeEmploymentHistoryStaffRates();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Произведена синхронизация долей ставок в истории должностей.", TopOrgUnit.getInstance()));
    }

    public void onClickSyncStaffListCreateAndAcceptDates()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateSyncStaffListCreateAndAcceptDates();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Произведена синхронизация дат создания и согласования для версий ШР", TopOrgUnit.getInstance()));
    }

    public void onClickInitStaffListArchivingDates()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateInitStaffListArchivingDates();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Произведена коррекция дат архивации для архивных версий ШР", TopOrgUnit.getInstance()));
    }

    public void onClickReallocateStaffListAllocItems()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateReallocateStaffListAllocItems();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Произведена переразбивка ставок в штатной расстановке с привязкой сотрудников для активных версий ШР", TopOrgUnit.getInstance()));
    }

    public void onClickCorrectEmptySalaryAllocItems()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateCorrectEmptySalaryAllocItems();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Произведена корректировка базовых окладов и перерасчет выплат для должностей штатной расстановки.", TopOrgUnit.getInstance()));
    }

    public void onClickCorrectEmployeeHolidayFinishDate()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateCorrectEmployeeHolidayFinishDate();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Произведена корректировка дат окончания отпусков", TopOrgUnit.getInstance()));
    }

    public void onClickFillStaffListsAutomatically()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateFillStaffListsAutomatically();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Сформированы штатные расписания ОУ по списку сотрудников", TopOrgUnit.getInstance()));
    }

    public void onClickGetPostCatalogs()
    {
        EmpSystemActionManager.instance().dao().getPostCatalogs();
    }

    public void onClickActualizeOrgUnitTypePostRelation()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateOrgUnitTypePostRelation();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Актуализированы связи типов подразделений и должностей", TopOrgUnit.getInstance()));
    }

    public void onClickBindSecondJobEmployeePost()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateSecondJobEmployeePost();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Привязать должности по совмещению к основным должностям сотрудников", TopOrgUnit.getInstance()));
    }

    public void onClickSetEmployeeContractEndDate()
    {
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            EmpSystemActionManager.instance().dao().updateEmployeeContractEndDate();
        }
        finally
        {
            eventLock.release();
        }
        CoreServices.eventService().fireEvent(new InsertEntityEvent("Установить даты окончания трудовых договоров сотрудников в соответствии с датами увольнения", TopOrgUnit.getInstance()));
    }
}
