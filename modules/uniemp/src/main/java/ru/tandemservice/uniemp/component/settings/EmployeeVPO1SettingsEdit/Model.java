/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.EmployeeVPO1SettingsEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;

/**
 * @author dseleznev
 * Created on: 21.01.2010
 */
@Input(keys = {"employeeVPO1ReportLinesId", "employeeVPO1ReportLinesId"})
public class Model
{
    private Long _employeeVPO1ReportLinesId;
    private EmployeeVPO1ReportLines _reportLines;

    private List<HSelectOption> _employeeTypesList;
    private EmployeeType _employeeTypeFilter;
    
    private IMultiSelectModel _postsListModel;
    private List<PostBoundedWithQGandQL> _selectedPostsList;

    public Long getEmployeeVPO1ReportLinesId()
    {
        return _employeeVPO1ReportLinesId;
    }

    public void setEmployeeVPO1ReportLinesId(Long employeeVPO1ReportLinesId)
    {
        this._employeeVPO1ReportLinesId = employeeVPO1ReportLinesId;
    }

    public EmployeeVPO1ReportLines getReportLines()
    {
        return _reportLines;
    }

    public void setReportLines(EmployeeVPO1ReportLines reportLines)
    {
        this._reportLines = reportLines;
    }

    public List<HSelectOption> getEmployeeTypesList()
    {
        return _employeeTypesList;
    }

    public void setEmployeeTypesList(List<HSelectOption> employeeTypesList)
    {
        this._employeeTypesList = employeeTypesList;
    }

    public EmployeeType getEmployeeTypeFilter()
    {
        return _employeeTypeFilter;
    }

    public void setEmployeeTypeFilter(EmployeeType employeeTypeFilter)
    {
        this._employeeTypeFilter = employeeTypeFilter;
    }

    public IMultiSelectModel getPostsListModel()
    {
        return _postsListModel;
    }

    public void setPostsListModel(IMultiSelectModel postsListModel)
    {
        this._postsListModel = postsListModel;
    }

    public List<PostBoundedWithQGandQL> getSelectedPostsList()
    {
        return _selectedPostsList;
    }

    public void setSelectedPostsList(List<PostBoundedWithQGandQL> selectedPostsList)
    {
        this._selectedPostsList = selectedPostsList;
    }
}