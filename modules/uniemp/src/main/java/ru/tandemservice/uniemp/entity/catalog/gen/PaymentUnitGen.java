package ru.tandemservice.uniemp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniemp.entity.catalog.PaymentUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Формат ввода выплаты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PaymentUnitGen extends EntityBase
 implements INaturalIdentifiable<PaymentUnitGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.catalog.PaymentUnit";
    public static final String ENTITY_NAME = "paymentUnit";
    public static final int VERSION_HASH = 332770410;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_NOMINATIVE_CASE_TITLE = "nominativeCaseTitle";
    public static final String P_GENITIVE_CASE_TITLE = "genitiveCaseTitle";
    public static final String P_DATIVE_CASE_TITLE = "dativeCaseTitle";
    public static final String P_ACCUSATIVE_CASE_TITLE = "accusativeCaseTitle";
    public static final String P_INSTRUMENTAL_CASE_TITLE = "instrumentalCaseTitle";
    public static final String P_PREPOSITIONAL_CASE_TITLE = "prepositionalCaseTitle";
    public static final String P_PLURAL_NOMINATIVE_CASE_TITLE = "pluralNominativeCaseTitle";
    public static final String P_PLURAL_GENITIVE_CASE_TITLE = "pluralGenitiveCaseTitle";
    public static final String P_PLURAL_DATIVE_CASE_TITLE = "pluralDativeCaseTitle";
    public static final String P_PLURAL_ACCUSATIVE_CASE_TITLE = "pluralAccusativeCaseTitle";
    public static final String P_PLURAL_INSTRUMENTAL_CASE_TITLE = "pluralInstrumentalCaseTitle";
    public static final String P_PLURAL_PREPOSITIONAL_CASE_TITLE = "pluralPrepositionalCaseTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _nominativeCaseTitle;     // Именительный падеж
    private String _genitiveCaseTitle;     // Родительный падеж
    private String _dativeCaseTitle;     // Дательный падеж
    private String _accusativeCaseTitle;     // Винительный падеж
    private String _instrumentalCaseTitle;     // Творительный падеж
    private String _prepositionalCaseTitle;     // Предложный падеж
    private String _pluralNominativeCaseTitle;     // Именительный падеж мн. ч.
    private String _pluralGenitiveCaseTitle;     // Родительный падеж мн. ч.
    private String _pluralDativeCaseTitle;     // Дательный падеж мн. ч.
    private String _pluralAccusativeCaseTitle;     // Винительный падеж мн. ч.
    private String _pluralInstrumentalCaseTitle;     // Творительный падеж мн. ч.
    private String _pluralPrepositionalCaseTitle;     // Предложный падеж мн. ч.
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Именительный падеж.
     */
    @Length(max=255)
    public String getNominativeCaseTitle()
    {
        return _nominativeCaseTitle;
    }

    /**
     * @param nominativeCaseTitle Именительный падеж.
     */
    public void setNominativeCaseTitle(String nominativeCaseTitle)
    {
        dirty(_nominativeCaseTitle, nominativeCaseTitle);
        _nominativeCaseTitle = nominativeCaseTitle;
    }

    /**
     * @return Родительный падеж.
     */
    @Length(max=255)
    public String getGenitiveCaseTitle()
    {
        return _genitiveCaseTitle;
    }

    /**
     * @param genitiveCaseTitle Родительный падеж.
     */
    public void setGenitiveCaseTitle(String genitiveCaseTitle)
    {
        dirty(_genitiveCaseTitle, genitiveCaseTitle);
        _genitiveCaseTitle = genitiveCaseTitle;
    }

    /**
     * @return Дательный падеж.
     */
    @Length(max=255)
    public String getDativeCaseTitle()
    {
        return _dativeCaseTitle;
    }

    /**
     * @param dativeCaseTitle Дательный падеж.
     */
    public void setDativeCaseTitle(String dativeCaseTitle)
    {
        dirty(_dativeCaseTitle, dativeCaseTitle);
        _dativeCaseTitle = dativeCaseTitle;
    }

    /**
     * @return Винительный падеж.
     */
    @Length(max=255)
    public String getAccusativeCaseTitle()
    {
        return _accusativeCaseTitle;
    }

    /**
     * @param accusativeCaseTitle Винительный падеж.
     */
    public void setAccusativeCaseTitle(String accusativeCaseTitle)
    {
        dirty(_accusativeCaseTitle, accusativeCaseTitle);
        _accusativeCaseTitle = accusativeCaseTitle;
    }

    /**
     * @return Творительный падеж.
     */
    @Length(max=255)
    public String getInstrumentalCaseTitle()
    {
        return _instrumentalCaseTitle;
    }

    /**
     * @param instrumentalCaseTitle Творительный падеж.
     */
    public void setInstrumentalCaseTitle(String instrumentalCaseTitle)
    {
        dirty(_instrumentalCaseTitle, instrumentalCaseTitle);
        _instrumentalCaseTitle = instrumentalCaseTitle;
    }

    /**
     * @return Предложный падеж.
     */
    @Length(max=255)
    public String getPrepositionalCaseTitle()
    {
        return _prepositionalCaseTitle;
    }

    /**
     * @param prepositionalCaseTitle Предложный падеж.
     */
    public void setPrepositionalCaseTitle(String prepositionalCaseTitle)
    {
        dirty(_prepositionalCaseTitle, prepositionalCaseTitle);
        _prepositionalCaseTitle = prepositionalCaseTitle;
    }

    /**
     * @return Именительный падеж мн. ч..
     */
    @Length(max=255)
    public String getPluralNominativeCaseTitle()
    {
        return _pluralNominativeCaseTitle;
    }

    /**
     * @param pluralNominativeCaseTitle Именительный падеж мн. ч..
     */
    public void setPluralNominativeCaseTitle(String pluralNominativeCaseTitle)
    {
        dirty(_pluralNominativeCaseTitle, pluralNominativeCaseTitle);
        _pluralNominativeCaseTitle = pluralNominativeCaseTitle;
    }

    /**
     * @return Родительный падеж мн. ч..
     */
    @Length(max=255)
    public String getPluralGenitiveCaseTitle()
    {
        return _pluralGenitiveCaseTitle;
    }

    /**
     * @param pluralGenitiveCaseTitle Родительный падеж мн. ч..
     */
    public void setPluralGenitiveCaseTitle(String pluralGenitiveCaseTitle)
    {
        dirty(_pluralGenitiveCaseTitle, pluralGenitiveCaseTitle);
        _pluralGenitiveCaseTitle = pluralGenitiveCaseTitle;
    }

    /**
     * @return Дательный падеж мн. ч..
     */
    @Length(max=255)
    public String getPluralDativeCaseTitle()
    {
        return _pluralDativeCaseTitle;
    }

    /**
     * @param pluralDativeCaseTitle Дательный падеж мн. ч..
     */
    public void setPluralDativeCaseTitle(String pluralDativeCaseTitle)
    {
        dirty(_pluralDativeCaseTitle, pluralDativeCaseTitle);
        _pluralDativeCaseTitle = pluralDativeCaseTitle;
    }

    /**
     * @return Винительный падеж мн. ч..
     */
    @Length(max=255)
    public String getPluralAccusativeCaseTitle()
    {
        return _pluralAccusativeCaseTitle;
    }

    /**
     * @param pluralAccusativeCaseTitle Винительный падеж мн. ч..
     */
    public void setPluralAccusativeCaseTitle(String pluralAccusativeCaseTitle)
    {
        dirty(_pluralAccusativeCaseTitle, pluralAccusativeCaseTitle);
        _pluralAccusativeCaseTitle = pluralAccusativeCaseTitle;
    }

    /**
     * @return Творительный падеж мн. ч..
     */
    @Length(max=255)
    public String getPluralInstrumentalCaseTitle()
    {
        return _pluralInstrumentalCaseTitle;
    }

    /**
     * @param pluralInstrumentalCaseTitle Творительный падеж мн. ч..
     */
    public void setPluralInstrumentalCaseTitle(String pluralInstrumentalCaseTitle)
    {
        dirty(_pluralInstrumentalCaseTitle, pluralInstrumentalCaseTitle);
        _pluralInstrumentalCaseTitle = pluralInstrumentalCaseTitle;
    }

    /**
     * @return Предложный падеж мн. ч..
     */
    @Length(max=255)
    public String getPluralPrepositionalCaseTitle()
    {
        return _pluralPrepositionalCaseTitle;
    }

    /**
     * @param pluralPrepositionalCaseTitle Предложный падеж мн. ч..
     */
    public void setPluralPrepositionalCaseTitle(String pluralPrepositionalCaseTitle)
    {
        dirty(_pluralPrepositionalCaseTitle, pluralPrepositionalCaseTitle);
        _pluralPrepositionalCaseTitle = pluralPrepositionalCaseTitle;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PaymentUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((PaymentUnit)another).getCode());
            }
            setShortTitle(((PaymentUnit)another).getShortTitle());
            setNominativeCaseTitle(((PaymentUnit)another).getNominativeCaseTitle());
            setGenitiveCaseTitle(((PaymentUnit)another).getGenitiveCaseTitle());
            setDativeCaseTitle(((PaymentUnit)another).getDativeCaseTitle());
            setAccusativeCaseTitle(((PaymentUnit)another).getAccusativeCaseTitle());
            setInstrumentalCaseTitle(((PaymentUnit)another).getInstrumentalCaseTitle());
            setPrepositionalCaseTitle(((PaymentUnit)another).getPrepositionalCaseTitle());
            setPluralNominativeCaseTitle(((PaymentUnit)another).getPluralNominativeCaseTitle());
            setPluralGenitiveCaseTitle(((PaymentUnit)another).getPluralGenitiveCaseTitle());
            setPluralDativeCaseTitle(((PaymentUnit)another).getPluralDativeCaseTitle());
            setPluralAccusativeCaseTitle(((PaymentUnit)another).getPluralAccusativeCaseTitle());
            setPluralInstrumentalCaseTitle(((PaymentUnit)another).getPluralInstrumentalCaseTitle());
            setPluralPrepositionalCaseTitle(((PaymentUnit)another).getPluralPrepositionalCaseTitle());
            setTitle(((PaymentUnit)another).getTitle());
        }
    }

    public INaturalId<PaymentUnitGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<PaymentUnitGen>
    {
        private static final String PROXY_NAME = "PaymentUnitNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PaymentUnitGen.NaturalId) ) return false;

            PaymentUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PaymentUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PaymentUnit.class;
        }

        public T newInstance()
        {
            return (T) new PaymentUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "nominativeCaseTitle":
                    return obj.getNominativeCaseTitle();
                case "genitiveCaseTitle":
                    return obj.getGenitiveCaseTitle();
                case "dativeCaseTitle":
                    return obj.getDativeCaseTitle();
                case "accusativeCaseTitle":
                    return obj.getAccusativeCaseTitle();
                case "instrumentalCaseTitle":
                    return obj.getInstrumentalCaseTitle();
                case "prepositionalCaseTitle":
                    return obj.getPrepositionalCaseTitle();
                case "pluralNominativeCaseTitle":
                    return obj.getPluralNominativeCaseTitle();
                case "pluralGenitiveCaseTitle":
                    return obj.getPluralGenitiveCaseTitle();
                case "pluralDativeCaseTitle":
                    return obj.getPluralDativeCaseTitle();
                case "pluralAccusativeCaseTitle":
                    return obj.getPluralAccusativeCaseTitle();
                case "pluralInstrumentalCaseTitle":
                    return obj.getPluralInstrumentalCaseTitle();
                case "pluralPrepositionalCaseTitle":
                    return obj.getPluralPrepositionalCaseTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "nominativeCaseTitle":
                    obj.setNominativeCaseTitle((String) value);
                    return;
                case "genitiveCaseTitle":
                    obj.setGenitiveCaseTitle((String) value);
                    return;
                case "dativeCaseTitle":
                    obj.setDativeCaseTitle((String) value);
                    return;
                case "accusativeCaseTitle":
                    obj.setAccusativeCaseTitle((String) value);
                    return;
                case "instrumentalCaseTitle":
                    obj.setInstrumentalCaseTitle((String) value);
                    return;
                case "prepositionalCaseTitle":
                    obj.setPrepositionalCaseTitle((String) value);
                    return;
                case "pluralNominativeCaseTitle":
                    obj.setPluralNominativeCaseTitle((String) value);
                    return;
                case "pluralGenitiveCaseTitle":
                    obj.setPluralGenitiveCaseTitle((String) value);
                    return;
                case "pluralDativeCaseTitle":
                    obj.setPluralDativeCaseTitle((String) value);
                    return;
                case "pluralAccusativeCaseTitle":
                    obj.setPluralAccusativeCaseTitle((String) value);
                    return;
                case "pluralInstrumentalCaseTitle":
                    obj.setPluralInstrumentalCaseTitle((String) value);
                    return;
                case "pluralPrepositionalCaseTitle":
                    obj.setPluralPrepositionalCaseTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "nominativeCaseTitle":
                        return true;
                case "genitiveCaseTitle":
                        return true;
                case "dativeCaseTitle":
                        return true;
                case "accusativeCaseTitle":
                        return true;
                case "instrumentalCaseTitle":
                        return true;
                case "prepositionalCaseTitle":
                        return true;
                case "pluralNominativeCaseTitle":
                        return true;
                case "pluralGenitiveCaseTitle":
                        return true;
                case "pluralDativeCaseTitle":
                        return true;
                case "pluralAccusativeCaseTitle":
                        return true;
                case "pluralInstrumentalCaseTitle":
                        return true;
                case "pluralPrepositionalCaseTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "nominativeCaseTitle":
                    return true;
                case "genitiveCaseTitle":
                    return true;
                case "dativeCaseTitle":
                    return true;
                case "accusativeCaseTitle":
                    return true;
                case "instrumentalCaseTitle":
                    return true;
                case "prepositionalCaseTitle":
                    return true;
                case "pluralNominativeCaseTitle":
                    return true;
                case "pluralGenitiveCaseTitle":
                    return true;
                case "pluralDativeCaseTitle":
                    return true;
                case "pluralAccusativeCaseTitle":
                    return true;
                case "pluralInstrumentalCaseTitle":
                    return true;
                case "pluralPrepositionalCaseTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "nominativeCaseTitle":
                    return String.class;
                case "genitiveCaseTitle":
                    return String.class;
                case "dativeCaseTitle":
                    return String.class;
                case "accusativeCaseTitle":
                    return String.class;
                case "instrumentalCaseTitle":
                    return String.class;
                case "prepositionalCaseTitle":
                    return String.class;
                case "pluralNominativeCaseTitle":
                    return String.class;
                case "pluralGenitiveCaseTitle":
                    return String.class;
                case "pluralDativeCaseTitle":
                    return String.class;
                case "pluralAccusativeCaseTitle":
                    return String.class;
                case "pluralInstrumentalCaseTitle":
                    return String.class;
                case "pluralPrepositionalCaseTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PaymentUnit> _dslPath = new Path<PaymentUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PaymentUnit");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Именительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getNominativeCaseTitle()
     */
    public static PropertyPath<String> nominativeCaseTitle()
    {
        return _dslPath.nominativeCaseTitle();
    }

    /**
     * @return Родительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getGenitiveCaseTitle()
     */
    public static PropertyPath<String> genitiveCaseTitle()
    {
        return _dslPath.genitiveCaseTitle();
    }

    /**
     * @return Дательный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getDativeCaseTitle()
     */
    public static PropertyPath<String> dativeCaseTitle()
    {
        return _dslPath.dativeCaseTitle();
    }

    /**
     * @return Винительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getAccusativeCaseTitle()
     */
    public static PropertyPath<String> accusativeCaseTitle()
    {
        return _dslPath.accusativeCaseTitle();
    }

    /**
     * @return Творительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getInstrumentalCaseTitle()
     */
    public static PropertyPath<String> instrumentalCaseTitle()
    {
        return _dslPath.instrumentalCaseTitle();
    }

    /**
     * @return Предложный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPrepositionalCaseTitle()
     */
    public static PropertyPath<String> prepositionalCaseTitle()
    {
        return _dslPath.prepositionalCaseTitle();
    }

    /**
     * @return Именительный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralNominativeCaseTitle()
     */
    public static PropertyPath<String> pluralNominativeCaseTitle()
    {
        return _dslPath.pluralNominativeCaseTitle();
    }

    /**
     * @return Родительный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralGenitiveCaseTitle()
     */
    public static PropertyPath<String> pluralGenitiveCaseTitle()
    {
        return _dslPath.pluralGenitiveCaseTitle();
    }

    /**
     * @return Дательный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralDativeCaseTitle()
     */
    public static PropertyPath<String> pluralDativeCaseTitle()
    {
        return _dslPath.pluralDativeCaseTitle();
    }

    /**
     * @return Винительный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralAccusativeCaseTitle()
     */
    public static PropertyPath<String> pluralAccusativeCaseTitle()
    {
        return _dslPath.pluralAccusativeCaseTitle();
    }

    /**
     * @return Творительный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralInstrumentalCaseTitle()
     */
    public static PropertyPath<String> pluralInstrumentalCaseTitle()
    {
        return _dslPath.pluralInstrumentalCaseTitle();
    }

    /**
     * @return Предложный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralPrepositionalCaseTitle()
     */
    public static PropertyPath<String> pluralPrepositionalCaseTitle()
    {
        return _dslPath.pluralPrepositionalCaseTitle();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends PaymentUnit> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _nominativeCaseTitle;
        private PropertyPath<String> _genitiveCaseTitle;
        private PropertyPath<String> _dativeCaseTitle;
        private PropertyPath<String> _accusativeCaseTitle;
        private PropertyPath<String> _instrumentalCaseTitle;
        private PropertyPath<String> _prepositionalCaseTitle;
        private PropertyPath<String> _pluralNominativeCaseTitle;
        private PropertyPath<String> _pluralGenitiveCaseTitle;
        private PropertyPath<String> _pluralDativeCaseTitle;
        private PropertyPath<String> _pluralAccusativeCaseTitle;
        private PropertyPath<String> _pluralInstrumentalCaseTitle;
        private PropertyPath<String> _pluralPrepositionalCaseTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(PaymentUnitGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(PaymentUnitGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Именительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getNominativeCaseTitle()
     */
        public PropertyPath<String> nominativeCaseTitle()
        {
            if(_nominativeCaseTitle == null )
                _nominativeCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_NOMINATIVE_CASE_TITLE, this);
            return _nominativeCaseTitle;
        }

    /**
     * @return Родительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getGenitiveCaseTitle()
     */
        public PropertyPath<String> genitiveCaseTitle()
        {
            if(_genitiveCaseTitle == null )
                _genitiveCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_GENITIVE_CASE_TITLE, this);
            return _genitiveCaseTitle;
        }

    /**
     * @return Дательный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getDativeCaseTitle()
     */
        public PropertyPath<String> dativeCaseTitle()
        {
            if(_dativeCaseTitle == null )
                _dativeCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_DATIVE_CASE_TITLE, this);
            return _dativeCaseTitle;
        }

    /**
     * @return Винительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getAccusativeCaseTitle()
     */
        public PropertyPath<String> accusativeCaseTitle()
        {
            if(_accusativeCaseTitle == null )
                _accusativeCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_ACCUSATIVE_CASE_TITLE, this);
            return _accusativeCaseTitle;
        }

    /**
     * @return Творительный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getInstrumentalCaseTitle()
     */
        public PropertyPath<String> instrumentalCaseTitle()
        {
            if(_instrumentalCaseTitle == null )
                _instrumentalCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_INSTRUMENTAL_CASE_TITLE, this);
            return _instrumentalCaseTitle;
        }

    /**
     * @return Предложный падеж.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPrepositionalCaseTitle()
     */
        public PropertyPath<String> prepositionalCaseTitle()
        {
            if(_prepositionalCaseTitle == null )
                _prepositionalCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_PREPOSITIONAL_CASE_TITLE, this);
            return _prepositionalCaseTitle;
        }

    /**
     * @return Именительный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralNominativeCaseTitle()
     */
        public PropertyPath<String> pluralNominativeCaseTitle()
        {
            if(_pluralNominativeCaseTitle == null )
                _pluralNominativeCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_PLURAL_NOMINATIVE_CASE_TITLE, this);
            return _pluralNominativeCaseTitle;
        }

    /**
     * @return Родительный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralGenitiveCaseTitle()
     */
        public PropertyPath<String> pluralGenitiveCaseTitle()
        {
            if(_pluralGenitiveCaseTitle == null )
                _pluralGenitiveCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_PLURAL_GENITIVE_CASE_TITLE, this);
            return _pluralGenitiveCaseTitle;
        }

    /**
     * @return Дательный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralDativeCaseTitle()
     */
        public PropertyPath<String> pluralDativeCaseTitle()
        {
            if(_pluralDativeCaseTitle == null )
                _pluralDativeCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_PLURAL_DATIVE_CASE_TITLE, this);
            return _pluralDativeCaseTitle;
        }

    /**
     * @return Винительный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralAccusativeCaseTitle()
     */
        public PropertyPath<String> pluralAccusativeCaseTitle()
        {
            if(_pluralAccusativeCaseTitle == null )
                _pluralAccusativeCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_PLURAL_ACCUSATIVE_CASE_TITLE, this);
            return _pluralAccusativeCaseTitle;
        }

    /**
     * @return Творительный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralInstrumentalCaseTitle()
     */
        public PropertyPath<String> pluralInstrumentalCaseTitle()
        {
            if(_pluralInstrumentalCaseTitle == null )
                _pluralInstrumentalCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_PLURAL_INSTRUMENTAL_CASE_TITLE, this);
            return _pluralInstrumentalCaseTitle;
        }

    /**
     * @return Предложный падеж мн. ч..
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getPluralPrepositionalCaseTitle()
     */
        public PropertyPath<String> pluralPrepositionalCaseTitle()
        {
            if(_pluralPrepositionalCaseTitle == null )
                _pluralPrepositionalCaseTitle = new PropertyPath<String>(PaymentUnitGen.P_PLURAL_PREPOSITIONAL_CASE_TITLE, this);
            return _pluralPrepositionalCaseTitle;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.PaymentUnit#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(PaymentUnitGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return PaymentUnit.class;
        }

        public String getEntityName()
        {
            return "paymentUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
