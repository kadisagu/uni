/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListPub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import java.util.HashMap;
import java.util.Map;

/**
 * Create by ashaburov
 * Date 05.05.12
 * <p/>
 * Врапер для элементов в списке Выплаты в Штатном расписании.<p/>
 * Врапер нужен для вывода в списке строк разных типов:
 * <ol>
 * <li>строки должности</li>
 * <li>строки выплат должности по источникам финансирования</li>
 * <li>итоговой строки по источникам финансирования</li>
 * <li>итоговой строки</li>
 * </ol>
 */
public class PaymentWrapper extends DataWrapper
{
    //Constructor

    public PaymentWrapper(Long id, String title)
    {
        super(id, title);
    }

    public PaymentWrapper(Long id, String title, String postTitle, StaffListItem item, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        super(id, title);
        setStaffListItem(item);
        setFinancingSource(financingSource);
        setFinancingSourceItem(financingSourceItem);
    }


    //Property

    public static String P_POST_TITLE = "title";
    public static String P_POST_TYPE = "postType";
    public static String P_PROF_QUALIFICATION_GROUP = "profQualiGroup";
    public static String P_QUALIFICATION_LEVEL = "qualiLevel";
    public static String P_SALARY = "salary";
    public static String P_ETKS = "etks";
    public static String P_STAFF_RATE = "staffRate";
    public static String P_FINANCING_SOURCE = "financingSourceTitle";
    public static String P_FINANCING_SOURCE_ITEM = "financingSourceItemTitle";
    public static String P_MONTH_BASE_SALARY_FUND = "monthBaseSalaryFund";
    public static String P_TARGET_SALARY_FUND = "targetSalaryFund";


    //Fields

    private Map<String, Double> _codeValueMap = new HashMap<>();//код выплаты на сумму выплаты
    private StaffListItem _staffListItem;//должность штатного расписания
    private FinancingSource _financingSource;//источник финансирования
    private FinancingSourceItem _financingSourceItem;//источник финансирования (детально)
    private boolean _finSrcTotal;//врапер Итоговой строки по источникам финансирования
    private boolean _total;//врапер Итоговыой строки
    private boolean _finSrc;//врапер строки выплат должности
    private Double _totalStaffRate = 0d;//итоговое значение ставок для итоговых строк
    private Double _totalMonthBaseSalary = 0d;//итоговое значение месячного фонда оплаты труда для итоговых строк
    private Double _totalTargetSalary = 0d;//итоговое значение месячного фонда оплаты труда с надбавками для итоговых строк
    private boolean _bold;//строчка должна быть жырной


    //Methods

    @Override
    public Object getProperty(Object propertyPath)
    {
        if (_codeValueMap.containsKey(propertyPath))
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(_codeValueMap.get(propertyPath));

        return super.getProperty(propertyPath);
    }

    /**
     * Связывает указаный код выплаты с её суммой.<p/>
     * Если для кода уже сохранено значение, то оно суммируются с указанным.
     * @param paymentCode код выплаты
     * @param value сумма выплаты
     */
    public void putPaymentValue(String paymentCode, Double value)
    {
        if (paymentCode == null)
            throw new NullPointerException("'paymentCode' must not be null");

        _codeValueMap.put(paymentCode, _codeValueMap.get(paymentCode) != null ? _codeValueMap.get(paymentCode) + value : value);
    }

    /**
     * @param financingSource источник финансирования
     * @param financingSourceItem источник финансирования (детально)
     * @return <tt>true</tt>, если указанные истчники финансирования совпадают с источниками финансирования врапера, иначе <tt>false</tt>
     */
    public boolean equalsFinSrs(FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> key = new CoreCollectionUtils.Pair<>(getFinancingSource(), getFinancingSourceItem());
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> egKey = new CoreCollectionUtils.Pair<>(financingSource, financingSourceItem);

        return key.equals(egKey);
    }


    //Property Getters
    //методы возвращают различные значения в зависимости от того в качестве чего используется врапер:
    //1. стрка должности,
    //2. строка для выплат должности,
    //3. итоговая строка по источникам финансирования,
    //4. итоговая строка

    public String getPostType()
    {
        if (!_total && !_finSrcTotal)
            return getStaffListItem() != null ? getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getPost().getEmployeeType().getShortTitle() : "";
        else
            return  "";
    }

    public String getProfQualiGroup()
    {
        if (!_total && !_finSrcTotal)
            return getStaffListItem() != null ? getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getProfQualificationGroup().getShortTitle() : "";
        else
            return "";
    }

    public String getQualiLevel()
    {
        if (!_total && !_finSrcTotal)
            return getStaffListItem() != null ? getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getQualificationLevel().getShortTitle() : "";
        else
            return "";
    }

    public String getSalary()
    {
        if (!_total && !_finSrcTotal)
            return getStaffListItem() != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(getStaffListItem().getSalary()) : "";
        else
            return "";
    }

    public String getEtks()
    {
        if (!_total && !_finSrcTotal)
            return getStaffListItem() != null ? getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getEtksLevels() != null ? getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getEtksLevels().getTitle() : "" : "";
        else
            return "";
    }

    public String getStaffRate()
    {
        if (_finSrc)
            return "";
        else if (!_total && !_finSrcTotal)
            return getStaffListItem() != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(getStaffListItem().getStaffRate()) : "";
        else
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(_totalStaffRate);
    }

    public String getFinancingSourceTitle()
    {
        if (!_total)
            return getFinancingSource().getTitle();
        else
            return "";
    }

    public String getFinancingSourceItemTitle()
    {
        if (!_total)
            return getFinancingSourceItem() != null ? getFinancingSourceItem().getTitle() : "";
        else
            return "";
    }

    public String getMonthBaseSalaryFund()
    {
        if (_finSrc)
            return "";
        else if (!_total && !_finSrcTotal)
        {
            Double salary = getStaffListItem() != null ? getStaffListItem().getMonthBaseSalaryFund() : 0d;
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(salary);
        }
        else
        {
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(_totalMonthBaseSalary);
        }
    }

    public String getTargetSalaryFund()
    {
        if (_finSrc)
        {
            Double salary = 0d;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                salary += entry.getValue();

            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(salary);
        }
        if (!_total && !_finSrcTotal)
        {
            Double salary = getStaffListItem() != null ? getStaffListItem().getMonthBaseSalaryFund() : 0d;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                salary += entry.getValue();

            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(salary);
        }
        else
        {
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(_totalTargetSalary);
        }
    }

    public Double getMonthBaseSalary()
    {
        if (_finSrc)
            return 0d;
        else if (!_total && !_finSrcTotal)
        {
            return getStaffListItem() != null ? getStaffListItem().getMonthBaseSalaryFund() : 0d;
        }
        else
        {
            return _totalMonthBaseSalary;
        }
    }

    public Double getTargetSalary()
    {
        if (_finSrc)
        {
            Double salary = 0d;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                salary += entry.getValue();

            return salary;
        }
        if (!_total && !_finSrcTotal)
        {
            Double salary = getStaffListItem() != null ? getStaffListItem().getMonthBaseSalaryFund() : 0d;
            for (Map.Entry<String, Double> entry : _codeValueMap.entrySet())
                salary += entry.getValue();

            return salary;
        } else
        {
            return _totalTargetSalary;
        }
    }


    //Getters & Setters

    public Double getTotalStaffRate()
    {
        return _totalStaffRate;
    }

    public void setTotalStaffRate(Double totalStaffRate)
    {
        _totalStaffRate = totalStaffRate;
    }

    public Double getTotalMonthBaseSalary()
    {
        return _totalMonthBaseSalary;
    }

    public void setTotalMonthBaseSalary(Double totalMonthBaseSalary)
    {
        _totalMonthBaseSalary = totalMonthBaseSalary;
    }

    public Double getTotalTargetSalary()
    {
        return _totalTargetSalary;
    }

    public void setTotalTargetSalary(Double totalTargetSalary)
    {
        _totalTargetSalary = totalTargetSalary;
    }

    public Map<String, Double> getCodeValueMap()
    {
        return _codeValueMap;
    }

    public void setCodeValueMap(Map<String, Double> codeValueMap)
    {
        _codeValueMap = codeValueMap;
    }

    public StaffListItem getStaffListItem()
    {
        return _staffListItem;
    }

    public void setStaffListItem(StaffListItem staffListItem)
    {
        _staffListItem = staffListItem;
    }

    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    public void setFinancingSource(FinancingSource financingSource)
    {
        _financingSource = financingSource;
    }

    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        _financingSourceItem = financingSourceItem;
    }

    public boolean isFinSrcTotal()
    {
        return _finSrcTotal;
    }

    public void setFinSrcTotal(boolean finSrcTotal)
    {
        _finSrcTotal = finSrcTotal;
    }

    public boolean isTotal()
    {
        return _total;
    }

    public void setTotal(boolean total)
    {
        _total = total;
    }

    public boolean isBold()
    {
        return _bold;
    }

    public void setBold(boolean bold)
    {
        _bold = bold;
    }

    public boolean isFinSrc()
    {
        return _finSrc;
    }

    public void setFinSrc(boolean finSrc)
    {
        _finSrc = finSrc;
    }
}
