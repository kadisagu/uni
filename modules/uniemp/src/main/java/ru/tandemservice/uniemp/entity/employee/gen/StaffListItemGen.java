package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Должность штатного расписания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffListItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.StaffListItem";
    public static final String ENTITY_NAME = "staffListItem";
    public static final int VERSION_HASH = 1620831399;
    private static IEntityMeta ENTITY_META;

    public static final String L_STAFF_LIST = "staffList";
    public static final String L_ORG_UNIT_POST_RELATION = "orgUnitPostRelation";
    public static final String P_STAFF_RATE_INTEGER = "staffRateInteger";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";
    public static final String P_SALARY = "salary";

    private StaffList _staffList;     // Штатное расписание
    private OrgUnitPostRelation _orgUnitPostRelation;     // Должность на подразделении
    private int _staffRateInteger;     // Количество штатных единиц (целое)
    private FinancingSource _financingSource;     // Источник финансирования
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (детально)
    private double _salary;     // Должностной оклад

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Штатное расписание. Свойство не может быть null.
     */
    @NotNull
    public StaffList getStaffList()
    {
        return _staffList;
    }

    /**
     * @param staffList Штатное расписание. Свойство не может быть null.
     */
    public void setStaffList(StaffList staffList)
    {
        dirty(_staffList, staffList);
        _staffList = staffList;
    }

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     */
    @NotNull
    public OrgUnitPostRelation getOrgUnitPostRelation()
    {
        return _orgUnitPostRelation;
    }

    /**
     * @param orgUnitPostRelation Должность на подразделении. Свойство не может быть null.
     */
    public void setOrgUnitPostRelation(OrgUnitPostRelation orgUnitPostRelation)
    {
        dirty(_orgUnitPostRelation, orgUnitPostRelation);
        _orgUnitPostRelation = orgUnitPostRelation;
    }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     */
    @NotNull
    public int getStaffRateInteger()
    {
        return _staffRateInteger;
    }

    /**
     * @param staffRateInteger Количество штатных единиц (целое). Свойство не может быть null.
     */
    public void setStaffRateInteger(int staffRateInteger)
    {
        dirty(_staffRateInteger, staffRateInteger);
        _staffRateInteger = staffRateInteger;
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     */
    @NotNull
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования. Свойство не может быть null.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    /**
     * @return Должностной оклад. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Должностной оклад. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StaffListItemGen)
        {
            setStaffList(((StaffListItem)another).getStaffList());
            setOrgUnitPostRelation(((StaffListItem)another).getOrgUnitPostRelation());
            setStaffRateInteger(((StaffListItem)another).getStaffRateInteger());
            setFinancingSource(((StaffListItem)another).getFinancingSource());
            setFinancingSourceItem(((StaffListItem)another).getFinancingSourceItem());
            setSalary(((StaffListItem)another).getSalary());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffListItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffListItem.class;
        }

        public T newInstance()
        {
            return (T) new StaffListItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "staffList":
                    return obj.getStaffList();
                case "orgUnitPostRelation":
                    return obj.getOrgUnitPostRelation();
                case "staffRateInteger":
                    return obj.getStaffRateInteger();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
                case "salary":
                    return obj.getSalary();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "staffList":
                    obj.setStaffList((StaffList) value);
                    return;
                case "orgUnitPostRelation":
                    obj.setOrgUnitPostRelation((OrgUnitPostRelation) value);
                    return;
                case "staffRateInteger":
                    obj.setStaffRateInteger((Integer) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "staffList":
                        return true;
                case "orgUnitPostRelation":
                        return true;
                case "staffRateInteger":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
                case "salary":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "staffList":
                    return true;
                case "orgUnitPostRelation":
                    return true;
                case "staffRateInteger":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
                case "salary":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "staffList":
                    return StaffList.class;
                case "orgUnitPostRelation":
                    return OrgUnitPostRelation.class;
                case "staffRateInteger":
                    return Integer.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
                case "salary":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffListItem> _dslPath = new Path<StaffListItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffListItem");
    }
            

    /**
     * @return Штатное расписание. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getStaffList()
     */
    public static StaffList.Path<StaffList> staffList()
    {
        return _dslPath.staffList();
    }

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getOrgUnitPostRelation()
     */
    public static OrgUnitPostRelation.Path<OrgUnitPostRelation> orgUnitPostRelation()
    {
        return _dslPath.orgUnitPostRelation();
    }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getStaffRateInteger()
     */
    public static PropertyPath<Integer> staffRateInteger()
    {
        return _dslPath.staffRateInteger();
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (детально).
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    /**
     * @return Должностной оклад. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    public static class Path<E extends StaffListItem> extends EntityPath<E>
    {
        private StaffList.Path<StaffList> _staffList;
        private OrgUnitPostRelation.Path<OrgUnitPostRelation> _orgUnitPostRelation;
        private PropertyPath<Integer> _staffRateInteger;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;
        private PropertyPath<Double> _salary;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Штатное расписание. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getStaffList()
     */
        public StaffList.Path<StaffList> staffList()
        {
            if(_staffList == null )
                _staffList = new StaffList.Path<StaffList>(L_STAFF_LIST, this);
            return _staffList;
        }

    /**
     * @return Должность на подразделении. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getOrgUnitPostRelation()
     */
        public OrgUnitPostRelation.Path<OrgUnitPostRelation> orgUnitPostRelation()
        {
            if(_orgUnitPostRelation == null )
                _orgUnitPostRelation = new OrgUnitPostRelation.Path<OrgUnitPostRelation>(L_ORG_UNIT_POST_RELATION, this);
            return _orgUnitPostRelation;
        }

    /**
     * @return Количество штатных единиц (целое). Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getStaffRateInteger()
     */
        public PropertyPath<Integer> staffRateInteger()
        {
            if(_staffRateInteger == null )
                _staffRateInteger = new PropertyPath<Integer>(StaffListItemGen.P_STAFF_RATE_INTEGER, this);
            return _staffRateInteger;
        }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (детально).
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

    /**
     * @return Должностной оклад. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffListItem#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(StaffListItemGen.P_SALARY, this);
            return _salary;
        }

        public Class getEntityClass()
        {
            return StaffListItem.class;
        }

        public String getEntityName()
        {
            return "staffListItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
