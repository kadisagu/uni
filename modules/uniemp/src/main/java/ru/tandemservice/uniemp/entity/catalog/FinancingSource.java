package ru.tandemservice.uniemp.entity.catalog;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.uniemp.entity.catalog.gen.FinancingSourceGen;

public class FinancingSource extends FinancingSourceGen implements ITitledWithCases
{
    @Override
    public String getCaseTitle(GrammaCase caze)
    {
        return TitledWithCasesUtil.getCaseTitle(this, caze);
    }
}
