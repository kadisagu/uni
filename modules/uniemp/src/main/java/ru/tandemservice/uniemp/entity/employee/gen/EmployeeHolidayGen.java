package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.gen.IAbstractDocumentGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отпуск сотрудника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeHolidayGen extends EntityBase
 implements IChoseRowTransferAnnualHolidayExtract{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeeHoliday";
    public static final String ENTITY_NAME = "employeeHoliday";
    public static final int VERSION_HASH = -1980027617;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_TITLE = "title";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_HOLIDAY_TYPE = "holidayType";
    public static final String P_DURATION = "duration";
    public static final String P_START_DATE = "startDate";
    public static final String P_FINISH_DATE = "finishDate";
    public static final String P_CHILD_NUMBER = "childNumber";
    public static final String L_HOLIDAY_EXTRACT = "holidayExtract";
    public static final String P_HOLIDAY_EXTRACT_NUMBER = "holidayExtractNumber";
    public static final String P_HOLIDAY_EXTRACT_DATE = "holidayExtractDate";

    private EmployeePost _employeePost;     // Сотрудник
    private String _title;     // Название
    private Date _beginDate;     // Дата начала периода
    private Date _endDate;     // Дата окончания периода
    private HolidayType _holidayType;     // Вид отпуска
    private int _duration;     // Длительность отпуска
    private Date _startDate;     // Дата начала отпуска
    private Date _finishDate;     // Дата окончания отпуска
    private Integer _childNumber;     // Номер ребенка
    private IAbstractDocument _holidayExtract;     // Выписка приказа об отпуске
    private String _holidayExtractNumber;     // Номер приказа об отпуске
    private Date _holidayExtractDate;     // Дата приказа об отпуске

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Дата начала периода.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала периода.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания периода.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания периода.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Вид отпуска. Свойство не может быть null.
     */
    @NotNull
    public HolidayType getHolidayType()
    {
        return _holidayType;
    }

    /**
     * @param holidayType Вид отпуска. Свойство не может быть null.
     */
    public void setHolidayType(HolidayType holidayType)
    {
        dirty(_holidayType, holidayType);
        _holidayType = holidayType;
    }

    /**
     * @return Длительность отпуска. Свойство не может быть null.
     */
    @NotNull
    public int getDuration()
    {
        return _duration;
    }

    /**
     * @param duration Длительность отпуска. Свойство не может быть null.
     */
    public void setDuration(int duration)
    {
        dirty(_duration, duration);
        _duration = duration;
    }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала отпуска. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getFinishDate()
    {
        return _finishDate;
    }

    /**
     * @param finishDate Дата окончания отпуска. Свойство не может быть null.
     */
    public void setFinishDate(Date finishDate)
    {
        dirty(_finishDate, finishDate);
        _finishDate = finishDate;
    }

    /**
     * @return Номер ребенка.
     */
    public Integer getChildNumber()
    {
        return _childNumber;
    }

    /**
     * @param childNumber Номер ребенка.
     */
    public void setChildNumber(Integer childNumber)
    {
        dirty(_childNumber, childNumber);
        _childNumber = childNumber;
    }

    /**
     * @return Выписка приказа об отпуске.
     */
    public IAbstractDocument getHolidayExtract()
    {
        return _holidayExtract;
    }

    /**
     * @param holidayExtract Выписка приказа об отпуске.
     */
    public void setHolidayExtract(IAbstractDocument holidayExtract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && holidayExtract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractDocument.class);
            IEntityMeta actual =  holidayExtract instanceof IEntity ? EntityRuntime.getMeta((IEntity) holidayExtract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_holidayExtract, holidayExtract);
        _holidayExtract = holidayExtract;
    }

    /**
     * @return Номер приказа об отпуске.
     */
    @Length(max=255)
    public String getHolidayExtractNumber()
    {
        return _holidayExtractNumber;
    }

    /**
     * @param holidayExtractNumber Номер приказа об отпуске.
     */
    public void setHolidayExtractNumber(String holidayExtractNumber)
    {
        dirty(_holidayExtractNumber, holidayExtractNumber);
        _holidayExtractNumber = holidayExtractNumber;
    }

    /**
     * @return Дата приказа об отпуске.
     */
    public Date getHolidayExtractDate()
    {
        return _holidayExtractDate;
    }

    /**
     * @param holidayExtractDate Дата приказа об отпуске.
     */
    public void setHolidayExtractDate(Date holidayExtractDate)
    {
        dirty(_holidayExtractDate, holidayExtractDate);
        _holidayExtractDate = holidayExtractDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeHolidayGen)
        {
            setEmployeePost(((EmployeeHoliday)another).getEmployeePost());
            setTitle(((EmployeeHoliday)another).getTitle());
            setBeginDate(((EmployeeHoliday)another).getBeginDate());
            setEndDate(((EmployeeHoliday)another).getEndDate());
            setHolidayType(((EmployeeHoliday)another).getHolidayType());
            setDuration(((EmployeeHoliday)another).getDuration());
            setStartDate(((EmployeeHoliday)another).getStartDate());
            setFinishDate(((EmployeeHoliday)another).getFinishDate());
            setChildNumber(((EmployeeHoliday)another).getChildNumber());
            setHolidayExtract(((EmployeeHoliday)another).getHolidayExtract());
            setHolidayExtractNumber(((EmployeeHoliday)another).getHolidayExtractNumber());
            setHolidayExtractDate(((EmployeeHoliday)another).getHolidayExtractDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeHolidayGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeHoliday.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeHoliday();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "title":
                    return obj.getTitle();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "holidayType":
                    return obj.getHolidayType();
                case "duration":
                    return obj.getDuration();
                case "startDate":
                    return obj.getStartDate();
                case "finishDate":
                    return obj.getFinishDate();
                case "childNumber":
                    return obj.getChildNumber();
                case "holidayExtract":
                    return obj.getHolidayExtract();
                case "holidayExtractNumber":
                    return obj.getHolidayExtractNumber();
                case "holidayExtractDate":
                    return obj.getHolidayExtractDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "holidayType":
                    obj.setHolidayType((HolidayType) value);
                    return;
                case "duration":
                    obj.setDuration((Integer) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "finishDate":
                    obj.setFinishDate((Date) value);
                    return;
                case "childNumber":
                    obj.setChildNumber((Integer) value);
                    return;
                case "holidayExtract":
                    obj.setHolidayExtract((IAbstractDocument) value);
                    return;
                case "holidayExtractNumber":
                    obj.setHolidayExtractNumber((String) value);
                    return;
                case "holidayExtractDate":
                    obj.setHolidayExtractDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "title":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "holidayType":
                        return true;
                case "duration":
                        return true;
                case "startDate":
                        return true;
                case "finishDate":
                        return true;
                case "childNumber":
                        return true;
                case "holidayExtract":
                        return true;
                case "holidayExtractNumber":
                        return true;
                case "holidayExtractDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "title":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "holidayType":
                    return true;
                case "duration":
                    return true;
                case "startDate":
                    return true;
                case "finishDate":
                    return true;
                case "childNumber":
                    return true;
                case "holidayExtract":
                    return true;
                case "holidayExtractNumber":
                    return true;
                case "holidayExtractDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "title":
                    return String.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "holidayType":
                    return HolidayType.class;
                case "duration":
                    return Integer.class;
                case "startDate":
                    return Date.class;
                case "finishDate":
                    return Date.class;
                case "childNumber":
                    return Integer.class;
                case "holidayExtract":
                    return IAbstractDocument.class;
                case "holidayExtractNumber":
                    return String.class;
                case "holidayExtractDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeHoliday> _dslPath = new Path<EmployeeHoliday>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeHoliday");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Вид отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getHolidayType()
     */
    public static HolidayType.Path<HolidayType> holidayType()
    {
        return _dslPath.holidayType();
    }

    /**
     * @return Длительность отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getDuration()
     */
    public static PropertyPath<Integer> duration()
    {
        return _dslPath.duration();
    }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getFinishDate()
     */
    public static PropertyPath<Date> finishDate()
    {
        return _dslPath.finishDate();
    }

    /**
     * @return Номер ребенка.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getChildNumber()
     */
    public static PropertyPath<Integer> childNumber()
    {
        return _dslPath.childNumber();
    }

    /**
     * @return Выписка приказа об отпуске.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getHolidayExtract()
     */
    public static IAbstractDocumentGen.Path<IAbstractDocument> holidayExtract()
    {
        return _dslPath.holidayExtract();
    }

    /**
     * @return Номер приказа об отпуске.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getHolidayExtractNumber()
     */
    public static PropertyPath<String> holidayExtractNumber()
    {
        return _dslPath.holidayExtractNumber();
    }

    /**
     * @return Дата приказа об отпуске.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getHolidayExtractDate()
     */
    public static PropertyPath<Date> holidayExtractDate()
    {
        return _dslPath.holidayExtractDate();
    }

    public static class Path<E extends EmployeeHoliday> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<String> _title;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private HolidayType.Path<HolidayType> _holidayType;
        private PropertyPath<Integer> _duration;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _finishDate;
        private PropertyPath<Integer> _childNumber;
        private IAbstractDocumentGen.Path<IAbstractDocument> _holidayExtract;
        private PropertyPath<String> _holidayExtractNumber;
        private PropertyPath<Date> _holidayExtractDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EmployeeHolidayGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeHolidayGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeHolidayGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Вид отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getHolidayType()
     */
        public HolidayType.Path<HolidayType> holidayType()
        {
            if(_holidayType == null )
                _holidayType = new HolidayType.Path<HolidayType>(L_HOLIDAY_TYPE, this);
            return _holidayType;
        }

    /**
     * @return Длительность отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getDuration()
     */
        public PropertyPath<Integer> duration()
        {
            if(_duration == null )
                _duration = new PropertyPath<Integer>(EmployeeHolidayGen.P_DURATION, this);
            return _duration;
        }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(EmployeeHolidayGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getFinishDate()
     */
        public PropertyPath<Date> finishDate()
        {
            if(_finishDate == null )
                _finishDate = new PropertyPath<Date>(EmployeeHolidayGen.P_FINISH_DATE, this);
            return _finishDate;
        }

    /**
     * @return Номер ребенка.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getChildNumber()
     */
        public PropertyPath<Integer> childNumber()
        {
            if(_childNumber == null )
                _childNumber = new PropertyPath<Integer>(EmployeeHolidayGen.P_CHILD_NUMBER, this);
            return _childNumber;
        }

    /**
     * @return Выписка приказа об отпуске.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getHolidayExtract()
     */
        public IAbstractDocumentGen.Path<IAbstractDocument> holidayExtract()
        {
            if(_holidayExtract == null )
                _holidayExtract = new IAbstractDocumentGen.Path<IAbstractDocument>(L_HOLIDAY_EXTRACT, this);
            return _holidayExtract;
        }

    /**
     * @return Номер приказа об отпуске.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getHolidayExtractNumber()
     */
        public PropertyPath<String> holidayExtractNumber()
        {
            if(_holidayExtractNumber == null )
                _holidayExtractNumber = new PropertyPath<String>(EmployeeHolidayGen.P_HOLIDAY_EXTRACT_NUMBER, this);
            return _holidayExtractNumber;
        }

    /**
     * @return Дата приказа об отпуске.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeHoliday#getHolidayExtractDate()
     */
        public PropertyPath<Date> holidayExtractDate()
        {
            if(_holidayExtractDate == null )
                _holidayExtractDate = new PropertyPath<Date>(EmployeeHolidayGen.P_HOLIDAY_EXTRACT_DATE, this);
            return _holidayExtractDate;
        }

        public Class getEntityClass()
        {
            return EmployeeHoliday.class;
        }

        public String getEntityName()
        {
            return "employeeHoliday";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
