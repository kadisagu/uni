/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistencyForList.Add;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceStatusType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.util.UniempReportUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 * Created on: 20.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    protected static final Comparator<PostBoundedWithQGandQL> PPS_POST_COMPARATOR = (o1, o2) -> {
        if (null != o1.getPost().getRank() && null != o2.getPost().getRank() && !o1.getPost().getRank().equals(o2.getPost().getRank()))
            return o1.getPost().getRank().compareTo(o2.getPost().getRank());
        else if (null != o1.getPost().getRank() && null == o2.getPost().getRank())
            return -1;
        else if (null != o2.getPost().getRank() && null == o1.getPost().getRank())
            return 1;
        else if (!o2.getQualificationLevel().equals(o1.getQualificationLevel()))
            return o2.getQualificationLevel().getShortTitle().compareTo(o1.getQualificationLevel().getShortTitle());
        else
            return o1.getTitle().compareTo(o2.getTitle());
    };

    protected static final Comparator<LineWrapper> LINE_COMPARATOR = (o1, o2) -> {
        int degreeLevel1 = o1.getScienceDegreeList() == null || o1.getScienceDegreeList().isEmpty() ? 0 : o1.getScienceDegreeList().iterator().next().getType().getLevel();
        int degreeLevel2 = o2.getScienceDegreeList() == null || o2.getScienceDegreeList().isEmpty() ? 0 : o2.getScienceDegreeList().iterator().next().getType().getLevel();

        int result = degreeLevel1 == degreeLevel2 ? 0 : (degreeLevel1 > degreeLevel2 ? -1 : 1);
        if (result == 0)
        {
            int statusLevel1 = o1.getScienceStatus() == null ? 0 : o1.getScienceStatus().getType().getLevel();
            int statusLevel2 = o2.getScienceStatus() == null ? 0 : o2.getScienceStatus().getType().getLevel();

            result = statusLevel1 == statusLevel2 ? 0 : (statusLevel1 > statusLevel2 ? -1 : 1);
        }
        if (result == 0)
        {
            result = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getFio(), o2.getFio());
        }

        return result;
    };

    @Override
    public Integer preparePrintReport(Model model)
    {
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("reportDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDate()));

        RtfTableModifier tableModifier = new RtfTableModifier();

        injectTables(tableModifier, model, model.getReportDate());

        return UniempReportUtil.preparePrintingReport(getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_QUALITY_CONSISTENCY_FOR_LIST), paramModifier, tableModifier);
    }

    protected void injectTables(RtfTableModifier tableModifier, Model model, Date reportDate)
    {
        prepareReportData(model, reportDate);

        prepareAndInjectTable(tableModifier, model);
    }

    /**
     * Поднимаем из базы и подготавливаем данные для отчета.
     */
    protected void prepareReportData(Model model, Date reportDate)
    {
        List<EmployeePost> employeePostList = new DQLSelectBuilder().fromEntity(EmployeePost.class, "b").column("b")
                .where(eqValue(property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().code().fromAlias("b")), EmployeeTypeCodes.EDU_STAFF))
                .where(ne(property(EmployeePost.postStatus().code().fromAlias("b")), EmployeePostStatusCodes.STATUS_POSSIBLE))
                .where(le(property(EmployeePost.postDate().fromAlias("b")), value(model.getReportDate(), PropertyType.DATE)))
                .where(or(
                        isNull(property(EmployeePost.dismissalDate().fromAlias("b"))),
                        ge(property(EmployeePost.dismissalDate().fromAlias("b")), value(model.getReportDate(), PropertyType.DATE))))
                .createStatement(getSession()).list();

        List<PostBoundedWithQGandQL> ppsPostList = new DQLSelectBuilder().fromEntity(PostBoundedWithQGandQL.class, "b").column("b")
                .where(eqValue(property(PostBoundedWithQGandQL.post().employeeType().code().fromAlias("b")), EmployeeTypeCodes.EDU_STAFF))
                .createStatement(getSession()).list();

        Collections.sort(ppsPostList, PPS_POST_COMPARATOR);

        final List<PersonAcademicStatus> personAcademicStatusList = new ArrayList<>();
        final List<PersonAcademicDegree> personAcademicDegreeList = new ArrayList<>();
        BatchUtils.execute(CommonBaseEntityUtil.getPropertiesSet(employeePostList, EmployeePost.person()), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            personAcademicStatusList.addAll(
                    new DQLSelectBuilder().fromEntity(PersonAcademicStatus.class, "b").column("b")
                            .where(in(property(PersonAcademicStatus.person().fromAlias("b")), elements))
                            .createStatement(getSession()).<PersonAcademicStatus>list()
            );
            personAcademicDegreeList.addAll(
                    new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "b").column("b")
                            .where(in(property(PersonAcademicDegree.person().fromAlias("b")), elements))
                            .createStatement(getSession()).<PersonAcademicDegree>list()
            );
        });

        Map<Person, List<ScienceStatus>> personAcademicStatusCodeMap = new HashMap<>();
        for (PersonAcademicStatus academicStatus : personAcademicStatusList)
        {
            List<ScienceStatus> list = personAcademicStatusCodeMap.get(academicStatus.getPerson());
            if (list == null)
                personAcademicStatusCodeMap.put(academicStatus.getPerson(), list = new ArrayList<>());
            list.add(academicStatus.getAcademicStatus());
        }

        Map<Person, List<ScienceDegree>> personAcademicDegreeCodeMap = new HashMap<>();
        for (PersonAcademicDegree academicDegree : personAcademicDegreeList)
        {
            List<ScienceDegree> list = personAcademicDegreeCodeMap.get(academicDegree.getPerson());
            if (list == null)
                personAcademicDegreeCodeMap.put(academicDegree.getPerson(), list = new ArrayList<>());
            list.add(academicDegree.getAcademicDegree());
        }

        model.setPpsPostList(ppsPostList);
        model.setEmployeePostList(employeePostList);
        model.setPersonAcademicStatusCodeMap(personAcademicStatusCodeMap);
        model.setPersonAcademicDegreeCodeMap(personAcademicDegreeCodeMap);
    }

    /**
     * Подготавливает данные и заполняет табличную метку для таблицы.
     * @param tableModifier табличный модифайер
     * @param model модель
     */
    protected void prepareAndInjectTable(RtfTableModifier tableModifier, Model model)
    {
        Map<PostBoundedWithQGandQL, List<String>> alreadyAddPostMap = new HashMap<>();
        Map<PostBoundedWithQGandQL, List<LineWrapper>> postLinesMap = new HashMap<>();
        for (EmployeePost post : model.getEmployeePostList())
        {
            List<String> stringList = alreadyAddPostMap.get(post.getPostRelation().getPostBoundedWithQGandQL());
            if (stringList == null)
                alreadyAddPostMap.put(post.getPostRelation().getPostBoundedWithQGandQL(), stringList = new ArrayList<>());

            if (stringList.contains(post.getEmployee().getId().toString() + post.getPostRelation().getPostBoundedWithQGandQL().getId().toString()))
                continue;

            List<LineWrapper> lineWrapperList = postLinesMap.get(post.getPostRelation().getPostBoundedWithQGandQL());
            if (lineWrapperList == null)
                postLinesMap.put(post.getPostRelation().getPostBoundedWithQGandQL(), lineWrapperList = new ArrayList<>());
            lineWrapperList.add(new LineWrapper(post.getPostRelation().getPostBoundedWithQGandQL(), post.getPerson().getFullFio(), getAcademicDegreeSet(model.getPersonAcademicDegreeCodeMap().get(post.getPerson())), getAcademicStatus(model.getPersonAcademicStatusCodeMap().get(post.getPerson()))));

            stringList.add(post.getEmployee().getId().toString() + post.getPostRelation().getPostBoundedWithQGandQL().getId().toString());
        }

        for (List<LineWrapper> list : postLinesMap.values())
            Collections.sort(list, LINE_COMPARATOR);

        List<String[]> lineList = new ArrayList<>();

        final List<Integer> mergeIndexList = new ArrayList<>();
        for (Map.Entry<PostBoundedWithQGandQL, List<LineWrapper>> entry : postLinesMap.entrySet())
        {
            if (!entry.getValue().isEmpty())
            {
                mergeIndexList.add(lineList.size());
                lineList.add(new String[]{"Список сотрудников на должности " + entry.getKey().getTitle()});
            }

            int number = 1;
            for (LineWrapper wrapper : entry.getValue())
            {
                String[] line = new String[4];

                line[0] = String.valueOf(number++);
                line[1] = wrapper.getFio();
                line[2] = wrapper.getScienceDegreeList() != null && !wrapper.getScienceDegreeList().isEmpty() ? UniStringUtils.join(wrapper.getScienceDegreeList(), ScienceDegree.shortTitle().s(), ", ")  : "";
                line[3] = wrapper.getScienceStatus() != null ? wrapper.getScienceStatus().getTitle().toLowerCase() : "";

                lineList.add(line);
            }
        }

        tableModifier.put("T1", lineList.toArray(new String[lineList.size()][4]));
        tableModifier.put("T1", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (mergeIndexList.contains(rowIndex))
                {
                    RtfString rtfString = new RtfString();
                    rtfString.append(IRtfData.QC).boldBegin().append(value).boldEnd();

                    return rtfString.toList();
                }

                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int col1 = 0;
                int col2 = 3;
                for (Integer rowIndex : mergeIndexList)
                {
                    int index = rowIndex + startIndex;
                    List<RtfCell> cells = newRowList.get(index).getCellList();
                    cells.get(col1).setMergeType(MergeType.HORIZONTAL_MERGED_FIRST);
                    for (int i = col1 + 1; i <= col2; i++)
                        cells.get(i).setMergeType(MergeType.HORIZONTAL_MERGED_NEXT);
                }
            }
        });
    }

    protected Set<ScienceDegree> getAcademicDegreeSet(List<ScienceDegree> scienceDegreeList)
    {
        Set<ScienceDegree> resultList = new HashSet<>();

        if (scienceDegreeList != null && !scienceDegreeList.isEmpty())
        {
            if (scienceDegreeList.size() == 1)
                return new HashSet<>(scienceDegreeList);

            resultList.addAll(scienceDegreeList);

            for (int i = 0; i < resultList.size(); i++)
            {
                ScienceDegree scienceDegree1 = scienceDegreeList.get(i);

                for (int j = i + 1; j < resultList.size(); j++)
                {
                    ScienceDegree scienceDegree2 = scienceDegreeList.get(j);

                    if (scienceDegree1.getType().getLevel() > scienceDegree2.getType().getLevel())
                        resultList.remove(scienceDegree2);
                    else if (scienceDegree1.getType().getLevel() < scienceDegree2.getType().getLevel())
                        resultList.remove(scienceDegree1);
                }
            }
        }

        return resultList;
    }

    protected ScienceStatus getAcademicStatus(List<ScienceStatus> scienceStatusList)
    {
        ScienceStatus result = null;

        if (scienceStatusList != null)
            for (ScienceStatus scienceStatus : scienceStatusList)
            {
                ScienceStatusType type = scienceStatus.getType();

                if (result == null && null != type)
                    result = scienceStatus;

                if (result != null && null != type && type.getLevel() > result.getType().getLevel())
                    result = scienceStatus;
            }

        return result;
    }
}