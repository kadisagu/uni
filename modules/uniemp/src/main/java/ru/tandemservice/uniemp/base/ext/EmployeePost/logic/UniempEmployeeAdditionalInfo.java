/*$Id$*/
package ru.tandemservice.uniemp.base.ext.EmployeePost.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.DefaultEmployeeAdditionalInfo;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 04.08.2014
 */
public class UniempEmployeeAdditionalInfo extends DefaultEmployeeAdditionalInfo
{
	@Override
	public Integer getRate(@NotNull final Long employeePostId)
	{
		return DataAccessServices.dao().getCalculatedValue(session -> {
            Number rate = new DQLSelectBuilder().fromEntity(EmployeePostStaffRateItem.class, "r")
                    .column(DQLFunctions.sum(property("r", EmployeePostStaffRateItem.P_STAFF_RATE_INTEGER)))
                    .where(eq(property("r", EmployeePostStaffRateItem.L_EMPLOYEE_POST), value(employeePostId)))
                    .createStatement(session).uniqueResult();
            if(rate == null)
                return 0;
            return rate.intValue();
        });
	}

    @Override
    public Map<Long, Long> getRateMap(Collection<Long> employeePostIds)
    {
        return DataAccessServices.dao().getCalculatedValue(
                session -> new DQLSelectBuilder().fromEntity(EmployeePostStaffRateItem.class, "r")
                        .column(property("ep.id"))
                        .column(DQLFunctions.sum(property("r", EmployeePostStaffRateItem.staffRateInteger())))
                        .joinPath(DQLJoinType.inner, EmployeePostStaffRateItem.employeePost().fromAlias("r"), "ep")
                        .where(in(property("ep.id"), employeePostIds))
                        .where(eq(property("ep", EmployeePost.employee().archival()), value(Boolean.FALSE)))
                        .where(eq(property("ep", EmployeePost.postStatus().actual()), value(Boolean.TRUE)))
                        .group(property("ep.id"))
                        .createStatement(session)
                        .<Object[]>list()
                        .stream()
                        .collect(Collectors.toMap(item -> (Long) item[0], item -> (Long) item[1]))
        );
    }
}
