/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListFakePaymentAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniemp.entity.catalog.Payment;

/**
 * @author dseleznev
 * Created on: 05.05.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        Model model = getModel(component);
        getDao().validate(model, errors);
        if (errors.hasErrors()) return;
        
        getDao().update(getModel(component));
        deactivate(component);
    }

    public void onChangePayment(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (null != model.getStaffListFakePayment().getPayment())
        {
            Payment payment = model.getStaffListFakePayment().getPayment();
            model.getStaffListFakePayment().setAmount(null != payment.getValue() ? payment.getValue() : 0d);
            model.getStaffListFakePayment().setFinancingSource(model.getStaffListFakePayment().getPayment().getFinancingSource());
        }
        else
        {
            model.getStaffListFakePayment().setAmount(0);
            model.getStaffListFakePayment().setFinancingSource(null);
        }
    }
}