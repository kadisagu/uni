/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeePost.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

import java.util.Date;

/**
 * Create by ashaburov
 * Date 30.05.12
 */
public interface IEmpEmployeePostDAO extends INeedPersistenceSupport
{
    /**
     * Подготавливает <code>RtfInjectModifier</code> для печати Личной карточки сотрудника.
     * @param employeePost сотрудник
     * @param printDate дата печати
     * @return <code>RtfInjectModifier</code>
     */
    RtfInjectModifier preparePersonCardModifier(EmployeePost employeePost, Date printDate);

    /**
     * Подготавливает <code>RtfTableModifier</code> для печати Личной карточки сотрудника.
     * @param employeePost сотрудник
     * @param printDate дата печати
     * @return <code>RtfTableModifier</code>
     */
    RtfTableModifier preparePersonCardTableModifier(EmployeePost employeePost, Date printDate);
}
