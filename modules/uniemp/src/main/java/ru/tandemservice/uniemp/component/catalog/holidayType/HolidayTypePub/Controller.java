/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.holidayType.HolidayTypePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;

/**
 * @author AutoGenerator
 * Created on 11.02.2011 
 */
public class Controller extends DefaultCatalogPubController<HolidayType, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<HolidayType> dataSource)
    {
        dataSource.addColumn(new BooleanColumn("Уход за ребенком", HolidayType.childCare().s()));
        dataSource.addColumn(new BooleanColumn("Длительный", HolidayType.longTime().s()));
        dataSource.addColumn(new BooleanColumn("Компенсируемый при увольнении", HolidayType.compensable().s()));
    }
}