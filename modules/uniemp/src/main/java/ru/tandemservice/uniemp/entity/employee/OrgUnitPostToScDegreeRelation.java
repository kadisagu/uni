package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import ru.tandemservice.uniemp.entity.employee.gen.OrgUnitPostToScDegreeRelationGen;

public class OrgUnitPostToScDegreeRelation extends OrgUnitPostToScDegreeRelationGen
{

    public OrgUnitPostToScDegreeRelation()
    {
    }

    public OrgUnitPostToScDegreeRelation(OrgUnitPostRelation rel, ScienceDegree academicDegree)
    {
        setOrgUnitPostRelation(rel);
        setAcademicDegree(academicDegree);
    }

}