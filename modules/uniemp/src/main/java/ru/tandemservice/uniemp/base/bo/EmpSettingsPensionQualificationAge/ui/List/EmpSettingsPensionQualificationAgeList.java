/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.EmpSettingsPensionQualificationAgeManager;
import ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings;

/**
 * @author Alexander Shaburov
 * @since 11.09.12
 */
@Configuration
public class EmpSettingsPensionQualificationAgeList extends BusinessComponentManager
{
    // dateSource
    public static String SEARCH_LIST_DS = "searchListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SEARCH_LIST_DS, columnListExtPoint(), EmpSettingsPensionQualificationAgeManager.instance().pensionQualificationAgeSearchListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnListExtPoint()
    {
        return columnListExtPointBuilder(SEARCH_LIST_DS)
                .addColumn(textColumn("sex", PensionQualificationAgeSettings.sex().title().s()))
                .addColumn(textColumn("age", PensionQualificationAgeSettings.age().s()))
                .addColumn(dateColumn("date", PensionQualificationAgeSettings.date().s()))
                .addColumn(headerColumn("document")
                        .addSubColumn(textColumn("documentTitle", PensionQualificationAgeSettings.documentTitle().s()).create())
                        .addSubColumn(dateColumn("documentDate", PensionQualificationAgeSettings.documentDate().s()).create())
                        .addSubColumn(textColumn("documentNumber", PensionQualificationAgeSettings.documentNumber().s()).create()))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert(FormattedMessage.with().template("searchListDS.delete.alert").create()).disabled("disabled"))
                .create();
    }
}
