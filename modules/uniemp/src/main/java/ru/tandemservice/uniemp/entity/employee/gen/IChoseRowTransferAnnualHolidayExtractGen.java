package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract;

/**
 * Интерфейс объектов выбранных в полях выписки О переносе ежегодного отпуска и самой выписки
 *
 * Общий интерфейс объектов поля выписки О переносе ежегодного отпуска.
 * Интерфейс используется для возможности хранении в поле выписки разных объектов.
 */
@SuppressWarnings({"all", "unchecked", "unused"})
public abstract class IChoseRowTransferAnnualHolidayExtractGen extends InterfaceStubBase
 implements IChoseRowTransferAnnualHolidayExtract{
    public static final int VERSION_HASH = -1198952449;



    private static final Path<IChoseRowTransferAnnualHolidayExtract> _dslPath = new Path<IChoseRowTransferAnnualHolidayExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract");
    }
            

    public static class Path<E extends IChoseRowTransferAnnualHolidayExtract> extends EntityPath<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return IChoseRowTransferAnnualHolidayExtract.class;
        }

        public String getEntityName()
        {
            return "ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
