/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationSchedulePub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.employeebase.base.ui.formatters.EmployeeCodeFormatter;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 10.01.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);

        model.setSettings(UniBaseUtils.getDataSettings(component, "VacationSchedulePub.filter"));
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;
        DynamicListDataSource<VacationScheduleItem> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new CheckboxColumn("selected", "Выбор", false));
        dataSource.addColumn(new SimpleColumn("Подразделение", VacationScheduleItem.employeePost().orgUnit().fullTitle().s()));
        dataSource.addColumn(new SimpleColumn("Должность", VacationScheduleItem.employeePost().postRelation().postBoundedWithQGandQL().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ФИО сотрудника", VacationScheduleItem.employeePost().employee().person().fullFio().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Табельный номер", VacationScheduleItem.employeePost().employee().employeeCode().s(), new EmployeeCodeFormatter()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во дней", VacationScheduleItem.daysAmount().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("План. дата начала", VacationScheduleItem.planDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Факт. дата начала", VacationScheduleItem.factDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Основание переноса отпуска", VacationScheduleItem.postponeBasic().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата переноса отпуска", VacationScheduleItem.postponeDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Комментарий", VacationScheduleItem.comment().s()).setClickable(false));
        dataSource.addColumn(new ActionColumn("Дробить отпуск", "clone", "onClickSplitVacationScheduleItem").setPermissionKey(model.getSecModel().getPermission("splitVacationScheduleItem")).setDisabledProperty(VacationScheduleItem.P_EDIT_DISABLED));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickVacationScheduleItemEdit").setPermissionKey(model.getSecModel().getPermission("editVacationScheduleItem")));
        dataSource.addColumn(new ActionColumn("Исключить", ActionColumn.DELETE, "onClickVacationScheduleItemDelete", "Исключить запись о планируемом отпуске сотрудника «{0}» из графика отпусков?", VacationScheduleItem.employeePost().employee().person().fullFio()).setPermissionKey(model.getSecModel().getPermission("deleteVacationScheduleItem")).setDisabledProperty(VacationScheduleItem.P_EDIT_DISABLED));
        model.setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");

        getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    public void onClickReject(IBusinessComponent component)
    {
        getDao().doReject(getModel(component));
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        UniempDaoFacade.getUniempDAO().deleteVacationSchedule(getModel(component).getVacationSchedule());
        deactivate(component);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IUniempComponents.VACATION_SCHEDULE_ADD_EDIT, new ParametersMap().add("vacationScheduleId", getModel(component).getVacationSchedule().getId())));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniempComponents.VACATION_SCHEDULE_PRINT, new ParametersMap().add("vacationScheduleId", getModel(component).getVacationSchedule().getId())));
    }

    public void onClickAddVacationScheduleItems(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IUniempComponents.VACATION_SCHEDULE_ITEMS_ADD, new ParametersMap().add("vacationScheduleId", getModel(component).getVacationSchedule().getId())));
    }

    public void onClickEditVacationScheduleItems(IBusinessComponent component)
    {
        List<Long> idsList = new ArrayList<>();
        for (IEntity entity : ((CheckboxColumn)getModel(component).getDataSource().getColumn("selected")).getSelectedObjects())
        {
            idsList.add(entity.getId());
        }
        if(idsList.isEmpty()) throw new ApplicationException("Необходимо выбрать хотя бы одного сотрудника.");
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IUniempComponents.VACATION_SCHEDULE_ITEMS_EDIT, new ParametersMap().add("vacationScheduleItemIdsList", idsList)));
    }

    public void onClickSplitVacationScheduleItem(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IUniempComponents.VACATION_SCHEDULE_ITEM_SPLIT, new ParametersMap().add("vacationScheduleItemId", component.getListenerParameter())));
    }

    public void onClickVacationScheduleItemEdit(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(IUniempComponents.VACATION_SCHEDULE_ITEM_EDIT, new ParametersMap().add("vacationScheduleItemId", component.getListenerParameter())));
    }

    public void onClickVacationScheduleItemDelete(IBusinessComponent component)
    {
        getDao().delete((Long)component.getListenerParameter());
    }

    public void onClickSearch(IBusinessComponent component)
    {
        DataSettingsFacade.saveSettings(getModel(component).getSettings());
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }
}