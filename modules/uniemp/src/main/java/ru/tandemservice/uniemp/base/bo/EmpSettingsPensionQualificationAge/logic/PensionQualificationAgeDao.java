/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.logic;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 11.09.12
 */
public class PensionQualificationAgeDao extends CommonDAO implements IPensionQualificationAgeDao
{
    @Override
    public ErrorCollector validate(PensionQualificationAgeSettings entity)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PensionQualificationAgeSettings.class, "b").column("b")
                .where(eqValue(property(PensionQualificationAgeSettings.sex().code().fromAlias("b")), entity.getSex().getCode()))
                .where(eqValue(property(PensionQualificationAgeSettings.date().fromAlias("b")), entity.getDate()));

        if (entity.getId() != null)
            builder.where(ne(property(PensionQualificationAgeSettings.id().fromAlias("b")), entity.getId()));

        PensionQualificationAgeSettings settings = builder.createStatement(getSession()).uniqueResult();

        if (settings != null)
            errorCollector.add("Для пола «" + entity.getSex().getTitle() + "» уже указан пенсионный возраст " + settings.getAge() + " лет, установленный с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entity.getDate()) + ".", "sex", "date");

        return errorCollector;
    }

    @Override
    public void saveOrUpdate(PensionQualificationAgeSettings entity)
    {
        if (entity.getId() == null)
        {
            List<String> codeList = new DQLSelectBuilder().fromEntity(PensionQualificationAgeSettings.class, "b").column(property(PensionQualificationAgeSettings.code().fromAlias("b")))
                    .order(property(PensionQualificationAgeSettings.code().fromAlias("b")), OrderDirection.asc)
                    .createStatement(getSession()).list();

            Integer newCode = 1;
            while (codeList.contains(newCode.toString()))
            {
                newCode++;
            }

            entity.setCode(newCode.toString());
        }

        getSession().saveOrUpdate(entity);
    }
}
