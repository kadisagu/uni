package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniemp.entity.employee.gen.*;

/**
 * Факт Повышения квалификации
 */
public class EmployeeTrainingItem extends EmployeeTrainingItemGen
{
    public static String P_TRAINING_TITLE = "trainingTitle";
    public static String P_EDU_INSTITUTION_AND_ADDRESS = "eduInstitutionAndAddress";
    public static String P_BASICS = "basics";
    public static String P_SERIA_AND_NUMBER = "seriaAndNumber";

    public String getTrainingTitle()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(getTitle());
        if (getAmountHours() != null)
            builder.append("; ").append(getAmountHours()).append(" ч.");

        return builder.toString();
    }

    public String getEduInstitutionAndAddress()
    {
        StringBuilder builder = new StringBuilder();
        if (getEduInstitution() != null)
            builder.append(getEduInstitution().getTitle()).append(", ");
        builder.append(getAddressItem().getTitleWithType());

        return builder.toString();
    }

    public String getBasics()
    {
        if (getOrderDate() != null || getOrderNumber() != null)
        {
            StringBuilder builder = new StringBuilder("Приказ");
            if (getOrderNumber() != null)
                builder.append(" №").append(getOrderNumber());
            if (getOrderDate() != null)
                builder.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getOrderDate()));

            return builder.toString();
        }
        else
            return "";
    }

    public String getSeriaAndNumber()
    {
        StringBuilder builder = new StringBuilder();
        if (getEduDocumentSeria() != null)
            builder.append(getEduDocumentSeria()).append(" ");
        builder.append(getEduDocumentNumber());

        return builder.toString();
    }
}