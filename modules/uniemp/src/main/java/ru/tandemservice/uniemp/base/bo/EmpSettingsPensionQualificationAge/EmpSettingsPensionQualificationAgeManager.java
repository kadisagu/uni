/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.runtime.EntityDataRuntime;
import org.tandemframework.core.tool.synchronization.SynchronizeMeta;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.logic.IPensionQualificationAgeDao;
import ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.logic.PensionQualificationAgeDao;
import ru.tandemservice.uniemp.entity.employee.PensionQualificationAgeSettings;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 11.09.12
 */
@Configuration
public class EmpSettingsPensionQualificationAgeManager extends BusinessObjectManager
{
    public static EmpSettingsPensionQualificationAgeManager instance()
    {
        return instance(EmpSettingsPensionQualificationAgeManager.class);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> pensionQualificationAgeSearchListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName(), PensionQualificationAgeSettings.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DSOutput output = super.execute(input, context);

                List<DataWrapper> wrapperList = DataWrapper.wrap(output);
                for (DataWrapper wrapper : wrapperList)
                {
                    PensionQualificationAgeSettings wrapped = wrapper.<PensionQualificationAgeSettings>getWrapped();
                    wrapper.setProperty("disabled", EntityDataRuntime.getEntityPolicy(wrapped.getEntityMeta().getName()).getItemPolicy(wrapped.getCode()).getSynchronize().equals(SynchronizeMeta.user));
                }

                return ListOutputBuilder.get(input, wrapperList).pageable(true).build();
            }
        };
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> sexDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), Sex.class);
    }

    @Bean
    public IPensionQualificationAgeDao pensionQualificationAgeDao()
    {
        return new PensionQualificationAgeDao();
    }
}
