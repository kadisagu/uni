/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.reports.staffList.List;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.report.StaffListReport;

import java.util.Calendar;
import java.util.Date;

/**
 * @author dseleznev
 * Created on: 13.08.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("report");

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StaffListReport.ENTITY_CLASS, "report");
        Date formingDate = (Date)model.getSettings().get("formingDate");
        if (formingDate != null)
        {
            builder.add(MQExpression.between("report", StaffListReport.P_FORMING_DATE, formingDate, CoreDateUtils.add(formingDate, Calendar.DATE, 1)));
        }

        DynamicListDataSource<StaffListReport> dataSource = model.getDataSource();
        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}