/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.EmpSettingsPensionQualificationAgeManager;

/**
 * @author Alexander Shaburov
 * @since 11.09.12
 */
@Configuration
public class EmpSettingsPensionQualificationAgeAddEdit extends BusinessComponentManager
{
    private static String SEX_DS = "sexDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SEX_DS, EmpSettingsPensionQualificationAgeManager.instance().sexDSHandler()))
                .create();
    }
}
