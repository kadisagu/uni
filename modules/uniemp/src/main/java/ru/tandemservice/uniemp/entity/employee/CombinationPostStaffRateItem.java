package ru.tandemservice.uniemp.entity.employee;

import ru.tandemservice.uniemp.entity.employee.gen.CombinationPostStaffRateItemGen;

import java.math.BigDecimal;

/**
 * Ставка должности по совмещению
 */
public class CombinationPostStaffRateItem extends CombinationPostStaffRateItemGen
{
    public void setStaffRate(double value)
    {
        setStaffRateInteger((int) (value * 10000));
    }

    public double getStaffRate()
    {
        BigDecimal x = new java.math.BigDecimal(getStaffRateInteger() * 0.0001);
        x = x.setScale(3, BigDecimal.ROUND_HALF_UP);

        return x.doubleValue();
    }
}