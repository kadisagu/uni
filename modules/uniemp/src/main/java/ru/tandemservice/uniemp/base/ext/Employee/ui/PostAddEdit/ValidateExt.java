/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnValidateEmployeePostExt;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;

import java.util.*;

/**
 * Create by ashaburov
 * Date 28.04.12
 */
public class ValidateExt implements IOnValidateEmployeePostExt
{
    @SuppressWarnings("unchecked")
    @Override
    public void onValidateExt(IUIAddon addon)
    {
        EmployeePostAddEditExtUI presenerExt = (EmployeePostAddEditExtUI) addon;

        EmployeePostAddEditUI baseModel = presenerExt.getPresenter();
        EmployeePostAddEditLegacyModel model = presenerExt.getModel();

        ErrorCollector errors = ContextLocal.getErrorCollector();
        if (model.getStaffRateItemList().isEmpty())
            errors.add("Необходимо указать ставку сотруднику.");

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());
        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());
        final IValueMapHolder finSrcItemHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        final Map<Long, FinancingSourceItem> finSrcItemMap = (null == finSrcItemHolder ? Collections.emptyMap() : finSrcItemHolder.getValueMap());
        final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
        final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());
        //если была доступна колонка Кадровой расстановки, достаем из враперов объекты Штатной расстановки
        final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
        //Мапа, указывающая на то выбрана ли строчка <Новая ставка> в мультиселекте
        Map<Long, Boolean> hasNewStaffRateMap = new HashMap<>();
        if (model.isThereAnyActiveStaffList())
        {
            for (IEntity entity : model.getStaffRateItemList())
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                        allocationItemList.add(item);
                    }

                }

                allocItemsMap.put(entity.getId(), allocationItemList);

                hasNewStaffRateMap.put(entity.getId(), empHRMap.get(entity.getId()).contains(new IdentifiableWrapper(0L, "")));
            }
        }

        //если редактируем должность, то заполним лист его ставок, что бы далее проверить выбранна ли она в мультиселекте
        if (!baseModel.isAddForm())
            for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
                model.getSelfAllocItemList().addAll(UniempDaoFacade.getStaffListDAO().getStaffListAllocationItemList(
                        UniempDaoFacade.getStaffListDAO().getActiveStaffList(baseModel.getEmployeePost().getOrgUnit()),
                        baseModel.getEmployeePost(),
                        baseModel.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(),
                        finSrcMap.get(item.getId()),
                        finSrcItemMap.get(item.getId())));

        for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
        {
            if (staffRateMap.get(item.getId()) == null)
                errors.add("Поле «Ставка» обязательно для заполнения.", "staffRate_id_" + item.getId());

            if (staffRateMap.get(item.getId()) != null)
            {
                Double val = 0.0d;
                if (null != staffRateMap.get(item.getId()))
                {
                    Object value = staffRateMap.get(item.getId());
                    val = ((Number) value).doubleValue();
                }
                if (val <= 0)
                    errors.add("Поле «Ставка» не может быть меньше или равно нуля.", "staffRate_id_" + item.getId());
            }

            if (finSrcMap.get(item.getId()) == null)
                errors.add("Поле «Источник финансирования» обязательно для заполнения.", "finSrc_id_" + item.getId());

            if (model.isThereAnyActiveStaffList() && empHRMap.get(item.getId()).size() == 0)
                errors.add("Поле «Кадровая расстановка» обязательно для заполнения.", "employeeHR_Id_" + item.getId());
        }

        if (!errors.hasErrors())
        {
            presenerExt.prepareStaffRateList();

            for (EmployeePostStaffRateItem item : model.getStaffRateItemList())
                for (EmployeePostStaffRateItem item2nd : model.getStaffRateItemList())
                    if (!item.equals(item2nd))
                        if (item.getFinancingSource().equals(item2nd.getFinancingSource()))
                            if ((item.getFinancingSourceItem() == null && item2nd.getFinancingSourceItem() == null) ||
                                    ((item.getFinancingSourceItem() != null && item2nd.getFinancingSourceItem() != null) && item.getFinancingSourceItem().equals(item2nd.getFinancingSourceItem())))
                                errors.add("Набор полей «Источник финансирования», «Источник финансирования (детально)» должен быть уникальным в рамках сотрудника.",
                                        "finSrc_id_" + item.getId(), "finSrcItm_id_" + item.getId());
        }

        if (!errors.hasErrors())
            if (model.getStaffRateItemList().size() != 0)
                UniempDaoFacade.getUniempDAO().validateStaffRates(errors, baseModel.getEmployee() != null ? baseModel.getEmployee() : baseModel.getEmployeePost().getEmployee(), baseModel.getEmployeePost(), model.getStaffRateItemList(), baseModel.isAddForm(), true);

        if (model.isThereAnyActiveStaffList() && !errors.hasErrors())
            UniempDaoFacade.getUniempDAO().validateStaffListRate(errors, baseModel.getEmployeePost(), model.getStaffRateItemList(), true);

        if (model.isThereAnyActiveStaffList() && !errors.hasErrors())
            for (EmployeePostStaffRateItem staffRateItem : model.getStaffRateItemList())
            {
                Double sumStaffRateAllocItems = 0.0d;
                for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
                {
                    sumStaffRateAllocItems += allocItem.getStaffRate();
                }

                if (staffRateItem.getStaffRate() <= sumStaffRateAllocItems && hasNewStaffRateMap.get(staffRateItem.getId()))
                    errors.add("Необходимо точнее указать занимаемые ставки в поле «Кадровая расстановка» для "
                            + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() + ".",
                            "employeeHR_Id_" + staffRateItem.getId());
            }
    }
}
