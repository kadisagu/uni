/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.quantityExtractFormP4.Add;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;

/**
 * @author dseleznev
 * Created on: 20.04.2009
 */
@SuppressWarnings("unchecked")
public class Model
{
    private Long _executorId;

    private IdentifiableWrapper _month;
    private IdentifiableWrapper _year;

    private List<IdentifiableWrapper> _monthsList;
    private List<IdentifiableWrapper> _yearsList;

    public Long getExecutorId()
    {
        return _executorId;
    }

    public void setExecutorId(Long executorId)
    {
        this._executorId = executorId;
    }

    public IdentifiableWrapper getMonth()
    {
        return _month;
    }

    public void setMonth(IdentifiableWrapper month)
    {
        this._month = month;
    }

    public IdentifiableWrapper getYear()
    {
        return _year;
    }

    public void setYear(IdentifiableWrapper year)
    {
        this._year = year;
    }

    public List<IdentifiableWrapper> getMonthsList()
    {
        return _monthsList;
    }

    public void setMonthsList(List<IdentifiableWrapper> monthsList)
    {
        this._monthsList = monthsList;
    }

    public List<IdentifiableWrapper> getYearsList()
    {
        return _yearsList;
    }

    public void setYearsList(List<IdentifiableWrapper> yearsList)
    {
        this._yearsList = yearsList;
    }
}