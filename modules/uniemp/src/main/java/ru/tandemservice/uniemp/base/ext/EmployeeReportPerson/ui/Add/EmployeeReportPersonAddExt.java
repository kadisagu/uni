/* $Id$ */
package ru.tandemservice.uniemp.base.ext.EmployeeReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAdd;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.EmpReportPersonAdditionalDataTab;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.CombinationPostTab.EmpReportPersonCombinationPostTab;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PPSData.EmpReportPersonPPSData;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.EmpReportPersonPaymentsDataTab;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PrintBlock.EmpReportPersonPrintBlock;

/**
 * @author azhebko
 * @since 26.03.2014
 */
@Configuration
public class EmployeeReportPersonAddExt extends BusinessComponentExtensionManager
{
    public static final String EMPLOYEE_PPS_DATA = "employeePPSData";
    public static final String EMPLOYEE_PRINT_DATA = "employeePrintData";
    public static final String EMPLOYEE_ADDITIONAL_DATA_TAB = "employeeAdditionalDataTab";
    public static final String EMPLOYEE_PAYMENTS_DATA_TAB = "employeePaymentsDataTab";
    public static final String EMPLOYEE_COMBINATION_POST_TAB = "combinationPostTab";

    @Autowired
    private EmployeeReportPersonAdd _reportPersonAdd;

    @Bean
    public BlockListExtension employeeCommonBlockListExtension()
    {
        return blockListExtensionBuilder(_reportPersonAdd.employeeCommonBlockListExtPoint())
                .addBlock(componentBlock(EMPLOYEE_PPS_DATA, EmpReportPersonPPSData.class))
                .create();
    }

    @Bean
    public BlockListExtension employeeScheetBlockListExtension()
    {
        return blockListExtensionBuilder(_reportPersonAdd.employeeScheetBlockListExtPoint())
                .addBlock(componentBlock(EMPLOYEE_PRINT_DATA, EmpReportPersonPrintBlock.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_reportPersonAdd.employeeTabPanelExtPoint())
                .addTab(componentTab(EMPLOYEE_ADDITIONAL_DATA_TAB, EmpReportPersonAdditionalDataTab.class))
                .addTab(componentTab(EMPLOYEE_PAYMENTS_DATA_TAB, EmpReportPersonPaymentsDataTab.class))
                .addTab(componentTab(EMPLOYEE_COMBINATION_POST_TAB, EmpReportPersonCombinationPostTab.class))
                .addAllAfter(EmployeeReportPersonAdd.EMPLOYEE_COMMON_TAB)
                .create();
    }
}