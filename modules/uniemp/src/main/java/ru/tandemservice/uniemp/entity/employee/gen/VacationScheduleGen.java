package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * График отпусков
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class VacationScheduleGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.VacationSchedule";
    public static final String ENTITY_NAME = "vacationSchedule";
    public static final int VERSION_HASH = -424808571;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_YEAR = "year";
    public static final String P_ACCEPT_DATE = "acceptDate";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_LAB_UNION_OPINION_NUMBER = "labUnionOpinionNumber";
    public static final String P_LAB_UNION_OPINION_DATE = "labUnionOpinionDate";
    public static final String P_COMMENT = "comment";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_STATE = "state";
    public static final String P_SIMPLE_TITLE_WITH_YEAR = "simpleTitleWithYear";
    public static final String P_TITLE_WITH_YEAR_AND_CREATE_DATE = "titleWithYearAndCreateDate";

    private String _number;     // Номер
    private int _year;     // Год
    private Date _acceptDate;     // Дата согласования
    private Date _createDate;     // Дата формирования
    private String _labUnionOpinionNumber;     // Номер решения профсоюза
    private Date _labUnionOpinionDate;     // Дата решения профсоюза
    private String _comment;     // Комментарий
    private OrgUnit _orgUnit;     // Подразделение
    private StaffListState _state;     // Состояние штатного расписания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Год. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Дата согласования.
     */
    public Date getAcceptDate()
    {
        return _acceptDate;
    }

    /**
     * @param acceptDate Дата согласования.
     */
    public void setAcceptDate(Date acceptDate)
    {
        dirty(_acceptDate, acceptDate);
        _acceptDate = acceptDate;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Номер решения профсоюза.
     */
    @Length(max=255)
    public String getLabUnionOpinionNumber()
    {
        return _labUnionOpinionNumber;
    }

    /**
     * @param labUnionOpinionNumber Номер решения профсоюза.
     */
    public void setLabUnionOpinionNumber(String labUnionOpinionNumber)
    {
        dirty(_labUnionOpinionNumber, labUnionOpinionNumber);
        _labUnionOpinionNumber = labUnionOpinionNumber;
    }

    /**
     * @return Дата решения профсоюза.
     */
    public Date getLabUnionOpinionDate()
    {
        return _labUnionOpinionDate;
    }

    /**
     * @param labUnionOpinionDate Дата решения профсоюза.
     */
    public void setLabUnionOpinionDate(Date labUnionOpinionDate)
    {
        dirty(_labUnionOpinionDate, labUnionOpinionDate);
        _labUnionOpinionDate = labUnionOpinionDate;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Состояние штатного расписания. Свойство не может быть null.
     */
    @NotNull
    public StaffListState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние штатного расписания. Свойство не может быть null.
     */
    public void setState(StaffListState state)
    {
        dirty(_state, state);
        _state = state;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof VacationScheduleGen)
        {
            setNumber(((VacationSchedule)another).getNumber());
            setYear(((VacationSchedule)another).getYear());
            setAcceptDate(((VacationSchedule)another).getAcceptDate());
            setCreateDate(((VacationSchedule)another).getCreateDate());
            setLabUnionOpinionNumber(((VacationSchedule)another).getLabUnionOpinionNumber());
            setLabUnionOpinionDate(((VacationSchedule)another).getLabUnionOpinionDate());
            setComment(((VacationSchedule)another).getComment());
            setOrgUnit(((VacationSchedule)another).getOrgUnit());
            setState(((VacationSchedule)another).getState());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends VacationScheduleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) VacationSchedule.class;
        }

        public T newInstance()
        {
            return (T) new VacationSchedule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "year":
                    return obj.getYear();
                case "acceptDate":
                    return obj.getAcceptDate();
                case "createDate":
                    return obj.getCreateDate();
                case "labUnionOpinionNumber":
                    return obj.getLabUnionOpinionNumber();
                case "labUnionOpinionDate":
                    return obj.getLabUnionOpinionDate();
                case "comment":
                    return obj.getComment();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "state":
                    return obj.getState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "acceptDate":
                    obj.setAcceptDate((Date) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "labUnionOpinionNumber":
                    obj.setLabUnionOpinionNumber((String) value);
                    return;
                case "labUnionOpinionDate":
                    obj.setLabUnionOpinionDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "state":
                    obj.setState((StaffListState) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "year":
                        return true;
                case "acceptDate":
                        return true;
                case "createDate":
                        return true;
                case "labUnionOpinionNumber":
                        return true;
                case "labUnionOpinionDate":
                        return true;
                case "comment":
                        return true;
                case "orgUnit":
                        return true;
                case "state":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "year":
                    return true;
                case "acceptDate":
                    return true;
                case "createDate":
                    return true;
                case "labUnionOpinionNumber":
                    return true;
                case "labUnionOpinionDate":
                    return true;
                case "comment":
                    return true;
                case "orgUnit":
                    return true;
                case "state":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "year":
                    return Integer.class;
                case "acceptDate":
                    return Date.class;
                case "createDate":
                    return Date.class;
                case "labUnionOpinionNumber":
                    return String.class;
                case "labUnionOpinionDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "state":
                    return StaffListState.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<VacationSchedule> _dslPath = new Path<VacationSchedule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "VacationSchedule");
    }
            

    /**
     * @return Номер.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getAcceptDate()
     */
    public static PropertyPath<Date> acceptDate()
    {
        return _dslPath.acceptDate();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Номер решения профсоюза.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getLabUnionOpinionNumber()
     */
    public static PropertyPath<String> labUnionOpinionNumber()
    {
        return _dslPath.labUnionOpinionNumber();
    }

    /**
     * @return Дата решения профсоюза.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getLabUnionOpinionDate()
     */
    public static PropertyPath<Date> labUnionOpinionDate()
    {
        return _dslPath.labUnionOpinionDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Состояние штатного расписания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getState()
     */
    public static StaffListState.Path<StaffListState> state()
    {
        return _dslPath.state();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getSimpleTitleWithYear()
     */
    public static SupportedPropertyPath<String> simpleTitleWithYear()
    {
        return _dslPath.simpleTitleWithYear();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getTitleWithYearAndCreateDate()
     */
    public static SupportedPropertyPath<String> titleWithYearAndCreateDate()
    {
        return _dslPath.titleWithYearAndCreateDate();
    }

    public static class Path<E extends VacationSchedule> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<Integer> _year;
        private PropertyPath<Date> _acceptDate;
        private PropertyPath<Date> _createDate;
        private PropertyPath<String> _labUnionOpinionNumber;
        private PropertyPath<Date> _labUnionOpinionDate;
        private PropertyPath<String> _comment;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private StaffListState.Path<StaffListState> _state;
        private SupportedPropertyPath<String> _simpleTitleWithYear;
        private SupportedPropertyPath<String> _titleWithYearAndCreateDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(VacationScheduleGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(VacationScheduleGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getAcceptDate()
     */
        public PropertyPath<Date> acceptDate()
        {
            if(_acceptDate == null )
                _acceptDate = new PropertyPath<Date>(VacationScheduleGen.P_ACCEPT_DATE, this);
            return _acceptDate;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(VacationScheduleGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Номер решения профсоюза.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getLabUnionOpinionNumber()
     */
        public PropertyPath<String> labUnionOpinionNumber()
        {
            if(_labUnionOpinionNumber == null )
                _labUnionOpinionNumber = new PropertyPath<String>(VacationScheduleGen.P_LAB_UNION_OPINION_NUMBER, this);
            return _labUnionOpinionNumber;
        }

    /**
     * @return Дата решения профсоюза.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getLabUnionOpinionDate()
     */
        public PropertyPath<Date> labUnionOpinionDate()
        {
            if(_labUnionOpinionDate == null )
                _labUnionOpinionDate = new PropertyPath<Date>(VacationScheduleGen.P_LAB_UNION_OPINION_DATE, this);
            return _labUnionOpinionDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(VacationScheduleGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Состояние штатного расписания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getState()
     */
        public StaffListState.Path<StaffListState> state()
        {
            if(_state == null )
                _state = new StaffListState.Path<StaffListState>(L_STATE, this);
            return _state;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getSimpleTitleWithYear()
     */
        public SupportedPropertyPath<String> simpleTitleWithYear()
        {
            if(_simpleTitleWithYear == null )
                _simpleTitleWithYear = new SupportedPropertyPath<String>(VacationScheduleGen.P_SIMPLE_TITLE_WITH_YEAR, this);
            return _simpleTitleWithYear;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniemp.entity.employee.VacationSchedule#getTitleWithYearAndCreateDate()
     */
        public SupportedPropertyPath<String> titleWithYearAndCreateDate()
        {
            if(_titleWithYearAndCreateDate == null )
                _titleWithYearAndCreateDate = new SupportedPropertyPath<String>(VacationScheduleGen.P_TITLE_WITH_YEAR_AND_CREATE_DATE, this);
            return _titleWithYearAndCreateDate;
        }

        public Class getEntityClass()
        {
            return VacationSchedule.class;
        }

        public String getEntityName()
        {
            return "vacationSchedule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getSimpleTitleWithYear();

    public abstract String getTitleWithYearAndCreateDate();
}
