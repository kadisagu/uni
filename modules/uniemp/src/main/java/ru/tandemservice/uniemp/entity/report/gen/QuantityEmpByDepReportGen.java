package ru.tandemservice.uniemp.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Численность персонала по подразделениям»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class QuantityEmpByDepReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport";
    public static final String ENTITY_NAME = "quantityEmpByDepReport";
    public static final int VERSION_HASH = -1485813704;
    private static IEntityMeta ENTITY_META;

    public static final String P_REPORT_DATE = "reportDate";
    public static final String L_ORG_UNIT_TYPE = "orgUnitType";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_VIEW_CHILD_ORG_UNIT = "viewChildOrgUnit";

    private Date _reportDate;     // Дата отчета
    private OrgUnitType _orgUnitType;     // Тип подразделения
    private OrgUnit _orgUnit;     // Подразделение
    private Boolean _viewChildOrgUnit = true;     // Выводить дочерние подразделения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчета.
     */
    public Date getReportDate()
    {
        return _reportDate;
    }

    /**
     * @param reportDate Дата отчета.
     */
    public void setReportDate(Date reportDate)
    {
        dirty(_reportDate, reportDate);
        _reportDate = reportDate;
    }

    /**
     * @return Тип подразделения.
     */
    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    /**
     * @param orgUnitType Тип подразделения.
     */
    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        dirty(_orgUnitType, orgUnitType);
        _orgUnitType = orgUnitType;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Выводить дочерние подразделения.
     */
    public Boolean getViewChildOrgUnit()
    {
        return _viewChildOrgUnit;
    }

    /**
     * @param viewChildOrgUnit Выводить дочерние подразделения.
     */
    public void setViewChildOrgUnit(Boolean viewChildOrgUnit)
    {
        dirty(_viewChildOrgUnit, viewChildOrgUnit);
        _viewChildOrgUnit = viewChildOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof QuantityEmpByDepReportGen)
        {
            setReportDate(((QuantityEmpByDepReport)another).getReportDate());
            setOrgUnitType(((QuantityEmpByDepReport)another).getOrgUnitType());
            setOrgUnit(((QuantityEmpByDepReport)another).getOrgUnit());
            setViewChildOrgUnit(((QuantityEmpByDepReport)another).getViewChildOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends QuantityEmpByDepReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) QuantityEmpByDepReport.class;
        }

        public T newInstance()
        {
            return (T) new QuantityEmpByDepReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return obj.getReportDate();
                case "orgUnitType":
                    return obj.getOrgUnitType();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "viewChildOrgUnit":
                    return obj.getViewChildOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reportDate":
                    obj.setReportDate((Date) value);
                    return;
                case "orgUnitType":
                    obj.setOrgUnitType((OrgUnitType) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "viewChildOrgUnit":
                    obj.setViewChildOrgUnit((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                        return true;
                case "orgUnitType":
                        return true;
                case "orgUnit":
                        return true;
                case "viewChildOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return true;
                case "orgUnitType":
                    return true;
                case "orgUnit":
                    return true;
                case "viewChildOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return Date.class;
                case "orgUnitType":
                    return OrgUnitType.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "viewChildOrgUnit":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<QuantityEmpByDepReport> _dslPath = new Path<QuantityEmpByDepReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "QuantityEmpByDepReport");
    }
            

    /**
     * @return Дата отчета.
     * @see ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport#getReportDate()
     */
    public static PropertyPath<Date> reportDate()
    {
        return _dslPath.reportDate();
    }

    /**
     * @return Тип подразделения.
     * @see ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport#getOrgUnitType()
     */
    public static OrgUnitType.Path<OrgUnitType> orgUnitType()
    {
        return _dslPath.orgUnitType();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Выводить дочерние подразделения.
     * @see ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport#getViewChildOrgUnit()
     */
    public static PropertyPath<Boolean> viewChildOrgUnit()
    {
        return _dslPath.viewChildOrgUnit();
    }

    public static class Path<E extends QuantityEmpByDepReport> extends StorableReport.Path<E>
    {
        private PropertyPath<Date> _reportDate;
        private OrgUnitType.Path<OrgUnitType> _orgUnitType;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Boolean> _viewChildOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчета.
     * @see ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport#getReportDate()
     */
        public PropertyPath<Date> reportDate()
        {
            if(_reportDate == null )
                _reportDate = new PropertyPath<Date>(QuantityEmpByDepReportGen.P_REPORT_DATE, this);
            return _reportDate;
        }

    /**
     * @return Тип подразделения.
     * @see ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport#getOrgUnitType()
     */
        public OrgUnitType.Path<OrgUnitType> orgUnitType()
        {
            if(_orgUnitType == null )
                _orgUnitType = new OrgUnitType.Path<OrgUnitType>(L_ORG_UNIT_TYPE, this);
            return _orgUnitType;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Выводить дочерние подразделения.
     * @see ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport#getViewChildOrgUnit()
     */
        public PropertyPath<Boolean> viewChildOrgUnit()
        {
            if(_viewChildOrgUnit == null )
                _viewChildOrgUnit = new PropertyPath<Boolean>(QuantityEmpByDepReportGen.P_VIEW_CHILD_ORG_UNIT, this);
            return _viewChildOrgUnit;
        }

        public Class getEntityClass()
        {
            return QuantityEmpByDepReport.class;
        }

        public String getEntityName()
        {
            return "quantityEmpByDepReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
