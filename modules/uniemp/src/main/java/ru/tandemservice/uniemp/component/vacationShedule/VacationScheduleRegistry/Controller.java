/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleRegistry;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;

/**
 * @author dseleznev
 * Created on: 27.01.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "vacationScheduleRegistry"));

        getDao().prepare(model);
        prepareStaffListDataSource(component);
    }

    private void prepareStaffListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<VacationSchedule> dataSource = new DynamicListDataSource<>(component, this);

        dataSource.addColumn(new SimpleColumn("Год", VacationSchedule.year().s()));
        dataSource.addColumn(new SimpleColumn("Номер", VacationSchedule.number().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата формирования", VacationSchedule.createDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата согласования", VacationSchedule.acceptDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", VacationSchedule.orgUnit().fullTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", VacationSchedule.state().title().s()).setClickable(false));

        IndicatorColumn column = new IndicatorColumn("print", "Печать", null, "onClickPrintVacationSchedule", (String) null);
        column.setOrderable(false);
        column.setImageHeader(false);
        column.defaultIndicator(new IndicatorColumn.Item("printer", "Печать"));
        column.setPermissionKey("printVacationSchedule");
        column.setDisableSecondSubmit(false);
        dataSource.addColumn(column);

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditVacationSchedule").setDisabledProperty(VacationSchedule.EDIT_DISABLED).setPermissionKey("editVacationSchedule"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteVacationSchedule", "Удалить график отпусков № {0} от {1} ({2})?", new Object[] { VacationSchedule.number().s(), VacationSchedule.createDate().s(), VacationSchedule.orgUnit().fullTitle().s() }).setDisabledProperty(VacationSchedule.DELETE_DISABLED).setPermissionKey("deleteVacationSchedule"));

        model.setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickAddVacationSchedule(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.VACATION_SCHEDULE_ADD_EDIT));
    }

    public void onClickPrintVacationSchedule(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUniempComponents.VACATION_SCHEDULE_PRINT, new ParametersMap().add("vacationScheduleId", component.getListenerParameter())));
    }

    public void onClickEditVacationSchedule(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.VACATION_SCHEDULE_ADD_EDIT, new ParametersMap()
                .add("vacationScheduleId", component.getListenerParameter())
        ));
    }

    public void onClickDeleteVacationSchedule(IBusinessComponent component)
    {
        UniempDaoFacade.getUniempDAO().deleteVacationSchedule(getDao().getNotNull(VacationSchedule.class, (Long)component.getListenerParameter()));
    }
}