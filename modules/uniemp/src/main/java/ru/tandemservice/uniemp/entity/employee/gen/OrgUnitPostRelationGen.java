package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Должность на подразделении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitPostRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation";
    public static final String ENTITY_NAME = "orgUnitPostRelation";
    public static final int VERSION_HASH = 2102859235;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_ORG_UNIT_TYPE_POST_RELATION = "orgUnitTypePostRelation";
    public static final String P_HEADER_POST = "headerPost";
    public static final String P_MIN_AGE = "minAge";
    public static final String P_MAX_AGE = "maxAge";
    public static final String P_ACADEMIC_STATUS_REQUIRED = "academicStatusRequired";
    public static final String P_ACADEMIC_DEGREE_REQUIRED = "academicDegreeRequired";
    public static final String P_AGE_REQUIRED = "ageRequired";
    public static final String P_POST_TYPE_REQUIRED = "postTypeRequired";

    private OrgUnit _orgUnit;     // Подразделение
    private OrgUnitTypePostRelation _orgUnitTypePostRelation;     // Должность в типе орг. юнита
    private boolean _headerPost;     // Руководящая
    private Integer _minAge;     // Минимальный возраст
    private Integer _maxAge;     // Максимальный возраст
    private boolean _academicStatusRequired;     // Ученое звание значимо
    private boolean _academicDegreeRequired;     // Ученая степень значима
    private boolean _ageRequired;     // Возраст значим
    private boolean _postTypeRequired;     // Тип назначения на должность значим

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность в типе орг. юнита. Свойство не может быть null.
     */
    @NotNull
    public OrgUnitTypePostRelation getOrgUnitTypePostRelation()
    {
        return _orgUnitTypePostRelation;
    }

    /**
     * @param orgUnitTypePostRelation Должность в типе орг. юнита. Свойство не может быть null.
     */
    public void setOrgUnitTypePostRelation(OrgUnitTypePostRelation orgUnitTypePostRelation)
    {
        dirty(_orgUnitTypePostRelation, orgUnitTypePostRelation);
        _orgUnitTypePostRelation = orgUnitTypePostRelation;
    }

    /**
     * @return Руководящая. Свойство не может быть null.
     */
    @NotNull
    public boolean isHeaderPost()
    {
        return _headerPost;
    }

    /**
     * @param headerPost Руководящая. Свойство не может быть null.
     */
    public void setHeaderPost(boolean headerPost)
    {
        dirty(_headerPost, headerPost);
        _headerPost = headerPost;
    }

    /**
     * @return Минимальный возраст.
     */
    public Integer getMinAge()
    {
        return _minAge;
    }

    /**
     * @param minAge Минимальный возраст.
     */
    public void setMinAge(Integer minAge)
    {
        dirty(_minAge, minAge);
        _minAge = minAge;
    }

    /**
     * @return Максимальный возраст.
     */
    public Integer getMaxAge()
    {
        return _maxAge;
    }

    /**
     * @param maxAge Максимальный возраст.
     */
    public void setMaxAge(Integer maxAge)
    {
        dirty(_maxAge, maxAge);
        _maxAge = maxAge;
    }

    /**
     * @return Ученое звание значимо. Свойство не может быть null.
     */
    @NotNull
    public boolean isAcademicStatusRequired()
    {
        return _academicStatusRequired;
    }

    /**
     * @param academicStatusRequired Ученое звание значимо. Свойство не может быть null.
     */
    public void setAcademicStatusRequired(boolean academicStatusRequired)
    {
        dirty(_academicStatusRequired, academicStatusRequired);
        _academicStatusRequired = academicStatusRequired;
    }

    /**
     * @return Ученая степень значима. Свойство не может быть null.
     */
    @NotNull
    public boolean isAcademicDegreeRequired()
    {
        return _academicDegreeRequired;
    }

    /**
     * @param academicDegreeRequired Ученая степень значима. Свойство не может быть null.
     */
    public void setAcademicDegreeRequired(boolean academicDegreeRequired)
    {
        dirty(_academicDegreeRequired, academicDegreeRequired);
        _academicDegreeRequired = academicDegreeRequired;
    }

    /**
     * @return Возраст значим. Свойство не может быть null.
     */
    @NotNull
    public boolean isAgeRequired()
    {
        return _ageRequired;
    }

    /**
     * @param ageRequired Возраст значим. Свойство не может быть null.
     */
    public void setAgeRequired(boolean ageRequired)
    {
        dirty(_ageRequired, ageRequired);
        _ageRequired = ageRequired;
    }

    /**
     * @return Тип назначения на должность значим. Свойство не может быть null.
     */
    @NotNull
    public boolean isPostTypeRequired()
    {
        return _postTypeRequired;
    }

    /**
     * @param postTypeRequired Тип назначения на должность значим. Свойство не может быть null.
     */
    public void setPostTypeRequired(boolean postTypeRequired)
    {
        dirty(_postTypeRequired, postTypeRequired);
        _postTypeRequired = postTypeRequired;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitPostRelationGen)
        {
            setOrgUnit(((OrgUnitPostRelation)another).getOrgUnit());
            setOrgUnitTypePostRelation(((OrgUnitPostRelation)another).getOrgUnitTypePostRelation());
            setHeaderPost(((OrgUnitPostRelation)another).isHeaderPost());
            setMinAge(((OrgUnitPostRelation)another).getMinAge());
            setMaxAge(((OrgUnitPostRelation)another).getMaxAge());
            setAcademicStatusRequired(((OrgUnitPostRelation)another).isAcademicStatusRequired());
            setAcademicDegreeRequired(((OrgUnitPostRelation)another).isAcademicDegreeRequired());
            setAgeRequired(((OrgUnitPostRelation)another).isAgeRequired());
            setPostTypeRequired(((OrgUnitPostRelation)another).isPostTypeRequired());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitPostRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitPostRelation.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitPostRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "orgUnitTypePostRelation":
                    return obj.getOrgUnitTypePostRelation();
                case "headerPost":
                    return obj.isHeaderPost();
                case "minAge":
                    return obj.getMinAge();
                case "maxAge":
                    return obj.getMaxAge();
                case "academicStatusRequired":
                    return obj.isAcademicStatusRequired();
                case "academicDegreeRequired":
                    return obj.isAcademicDegreeRequired();
                case "ageRequired":
                    return obj.isAgeRequired();
                case "postTypeRequired":
                    return obj.isPostTypeRequired();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "orgUnitTypePostRelation":
                    obj.setOrgUnitTypePostRelation((OrgUnitTypePostRelation) value);
                    return;
                case "headerPost":
                    obj.setHeaderPost((Boolean) value);
                    return;
                case "minAge":
                    obj.setMinAge((Integer) value);
                    return;
                case "maxAge":
                    obj.setMaxAge((Integer) value);
                    return;
                case "academicStatusRequired":
                    obj.setAcademicStatusRequired((Boolean) value);
                    return;
                case "academicDegreeRequired":
                    obj.setAcademicDegreeRequired((Boolean) value);
                    return;
                case "ageRequired":
                    obj.setAgeRequired((Boolean) value);
                    return;
                case "postTypeRequired":
                    obj.setPostTypeRequired((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "orgUnitTypePostRelation":
                        return true;
                case "headerPost":
                        return true;
                case "minAge":
                        return true;
                case "maxAge":
                        return true;
                case "academicStatusRequired":
                        return true;
                case "academicDegreeRequired":
                        return true;
                case "ageRequired":
                        return true;
                case "postTypeRequired":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "orgUnitTypePostRelation":
                    return true;
                case "headerPost":
                    return true;
                case "minAge":
                    return true;
                case "maxAge":
                    return true;
                case "academicStatusRequired":
                    return true;
                case "academicDegreeRequired":
                    return true;
                case "ageRequired":
                    return true;
                case "postTypeRequired":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "orgUnitTypePostRelation":
                    return OrgUnitTypePostRelation.class;
                case "headerPost":
                    return Boolean.class;
                case "minAge":
                    return Integer.class;
                case "maxAge":
                    return Integer.class;
                case "academicStatusRequired":
                    return Boolean.class;
                case "academicDegreeRequired":
                    return Boolean.class;
                case "ageRequired":
                    return Boolean.class;
                case "postTypeRequired":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitPostRelation> _dslPath = new Path<OrgUnitPostRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitPostRelation");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность в типе орг. юнита. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#getOrgUnitTypePostRelation()
     */
    public static OrgUnitTypePostRelation.Path<OrgUnitTypePostRelation> orgUnitTypePostRelation()
    {
        return _dslPath.orgUnitTypePostRelation();
    }

    /**
     * @return Руководящая. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isHeaderPost()
     */
    public static PropertyPath<Boolean> headerPost()
    {
        return _dslPath.headerPost();
    }

    /**
     * @return Минимальный возраст.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#getMinAge()
     */
    public static PropertyPath<Integer> minAge()
    {
        return _dslPath.minAge();
    }

    /**
     * @return Максимальный возраст.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#getMaxAge()
     */
    public static PropertyPath<Integer> maxAge()
    {
        return _dslPath.maxAge();
    }

    /**
     * @return Ученое звание значимо. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isAcademicStatusRequired()
     */
    public static PropertyPath<Boolean> academicStatusRequired()
    {
        return _dslPath.academicStatusRequired();
    }

    /**
     * @return Ученая степень значима. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isAcademicDegreeRequired()
     */
    public static PropertyPath<Boolean> academicDegreeRequired()
    {
        return _dslPath.academicDegreeRequired();
    }

    /**
     * @return Возраст значим. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isAgeRequired()
     */
    public static PropertyPath<Boolean> ageRequired()
    {
        return _dslPath.ageRequired();
    }

    /**
     * @return Тип назначения на должность значим. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isPostTypeRequired()
     */
    public static PropertyPath<Boolean> postTypeRequired()
    {
        return _dslPath.postTypeRequired();
    }

    public static class Path<E extends OrgUnitPostRelation> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private OrgUnitTypePostRelation.Path<OrgUnitTypePostRelation> _orgUnitTypePostRelation;
        private PropertyPath<Boolean> _headerPost;
        private PropertyPath<Integer> _minAge;
        private PropertyPath<Integer> _maxAge;
        private PropertyPath<Boolean> _academicStatusRequired;
        private PropertyPath<Boolean> _academicDegreeRequired;
        private PropertyPath<Boolean> _ageRequired;
        private PropertyPath<Boolean> _postTypeRequired;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность в типе орг. юнита. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#getOrgUnitTypePostRelation()
     */
        public OrgUnitTypePostRelation.Path<OrgUnitTypePostRelation> orgUnitTypePostRelation()
        {
            if(_orgUnitTypePostRelation == null )
                _orgUnitTypePostRelation = new OrgUnitTypePostRelation.Path<OrgUnitTypePostRelation>(L_ORG_UNIT_TYPE_POST_RELATION, this);
            return _orgUnitTypePostRelation;
        }

    /**
     * @return Руководящая. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isHeaderPost()
     */
        public PropertyPath<Boolean> headerPost()
        {
            if(_headerPost == null )
                _headerPost = new PropertyPath<Boolean>(OrgUnitPostRelationGen.P_HEADER_POST, this);
            return _headerPost;
        }

    /**
     * @return Минимальный возраст.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#getMinAge()
     */
        public PropertyPath<Integer> minAge()
        {
            if(_minAge == null )
                _minAge = new PropertyPath<Integer>(OrgUnitPostRelationGen.P_MIN_AGE, this);
            return _minAge;
        }

    /**
     * @return Максимальный возраст.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#getMaxAge()
     */
        public PropertyPath<Integer> maxAge()
        {
            if(_maxAge == null )
                _maxAge = new PropertyPath<Integer>(OrgUnitPostRelationGen.P_MAX_AGE, this);
            return _maxAge;
        }

    /**
     * @return Ученое звание значимо. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isAcademicStatusRequired()
     */
        public PropertyPath<Boolean> academicStatusRequired()
        {
            if(_academicStatusRequired == null )
                _academicStatusRequired = new PropertyPath<Boolean>(OrgUnitPostRelationGen.P_ACADEMIC_STATUS_REQUIRED, this);
            return _academicStatusRequired;
        }

    /**
     * @return Ученая степень значима. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isAcademicDegreeRequired()
     */
        public PropertyPath<Boolean> academicDegreeRequired()
        {
            if(_academicDegreeRequired == null )
                _academicDegreeRequired = new PropertyPath<Boolean>(OrgUnitPostRelationGen.P_ACADEMIC_DEGREE_REQUIRED, this);
            return _academicDegreeRequired;
        }

    /**
     * @return Возраст значим. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isAgeRequired()
     */
        public PropertyPath<Boolean> ageRequired()
        {
            if(_ageRequired == null )
                _ageRequired = new PropertyPath<Boolean>(OrgUnitPostRelationGen.P_AGE_REQUIRED, this);
            return _ageRequired;
        }

    /**
     * @return Тип назначения на должность значим. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation#isPostTypeRequired()
     */
        public PropertyPath<Boolean> postTypeRequired()
        {
            if(_postTypeRequired == null )
                _postTypeRequired = new PropertyPath<Boolean>(OrgUnitPostRelationGen.P_POST_TYPE_REQUIRED, this);
            return _postTypeRequired;
        }

        public Class getEntityClass()
        {
            return OrgUnitPostRelation.class;
        }

        public String getEntityName()
        {
            return "orgUnitPostRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
