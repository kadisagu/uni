package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.gen.IAbstractDocumentGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент истории должностей сотрудника (базовый)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmploymentHistoryItemBaseGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase";
    public static final String ENTITY_NAME = "employmentHistoryItemBase";
    public static final int VERSION_HASH = 827582437;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE = "employee";
    public static final String P_ASSIGN_DATE = "assignDate";
    public static final String P_DISMISSAL_DATE = "dismissalDate";
    public static final String P_STAFF_RATE = "staffRate";
    public static final String L_POST_TYPE = "postType";
    public static final String P_CURRENT = "current";
    public static final String L_EXTRACT = "extract";
    public static final String P_EXTRACT_NUMBER = "extractNumber";
    public static final String P_EXTRACT_DATE = "extractDate";
    public static final String P_EMPTY_SERVICE_LENGTH_TYPE = "emptyServiceLengthType";

    private Employee _employee;     // Кадровый ресурс
    private Date _assignDate;     // Дата назначения
    private Date _dismissalDate;     // Дата увольнения
    private Double _staffRate;     // Ставка (суммарная)
    private PostType _postType;     // Тип назначения на должность
    private Boolean _current;     // Текущая
    private IAbstractDocument _extract;     // Выписка приказа
    private String _extractNumber;     // Номер приказа
    private Date _extractDate;     // Дата приказа
    private boolean _emptyServiceLengthType;     // Не учитывать при расчете стажа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     */
    @NotNull
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Кадровый ресурс. Свойство не может быть null.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    /**
     * @return Дата назначения. Свойство не может быть null.
     */
    @NotNull
    public Date getAssignDate()
    {
        return _assignDate;
    }

    /**
     * @param assignDate Дата назначения. Свойство не может быть null.
     */
    public void setAssignDate(Date assignDate)
    {
        dirty(_assignDate, assignDate);
        _assignDate = assignDate;
    }

    /**
     * @return Дата увольнения.
     */
    public Date getDismissalDate()
    {
        return _dismissalDate;
    }

    /**
     * @param dismissalDate Дата увольнения.
     */
    public void setDismissalDate(Date dismissalDate)
    {
        dirty(_dismissalDate, dismissalDate);
        _dismissalDate = dismissalDate;
    }

    /**
     * @return Ставка (суммарная).
     */
    public Double getStaffRate()
    {
        return _staffRate;
    }

    /**
     * @param staffRate Ставка (суммарная).
     */
    public void setStaffRate(Double staffRate)
    {
        dirty(_staffRate, staffRate);
        _staffRate = staffRate;
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     */
    @NotNull
    public PostType getPostType()
    {
        return _postType;
    }

    /**
     * @param postType Тип назначения на должность. Свойство не может быть null.
     */
    public void setPostType(PostType postType)
    {
        dirty(_postType, postType);
        _postType = postType;
    }

    /**
     * @return Текущая.
     */
    public Boolean getCurrent()
    {
        return _current;
    }

    /**
     * @param current Текущая.
     */
    public void setCurrent(Boolean current)
    {
        dirty(_current, current);
        _current = current;
    }

    /**
     * @return Выписка приказа.
     */
    public IAbstractDocument getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка приказа.
     */
    public void setExtract(IAbstractDocument extract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && extract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractDocument.class);
            IEntityMeta actual =  extract instanceof IEntity ? EntityRuntime.getMeta((IEntity) extract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Номер приказа.
     */
    @Length(max=255)
    public String getExtractNumber()
    {
        return _extractNumber;
    }

    /**
     * @param extractNumber Номер приказа.
     */
    public void setExtractNumber(String extractNumber)
    {
        dirty(_extractNumber, extractNumber);
        _extractNumber = extractNumber;
    }

    /**
     * @return Дата приказа.
     */
    public Date getExtractDate()
    {
        return _extractDate;
    }

    /**
     * @param extractDate Дата приказа.
     */
    public void setExtractDate(Date extractDate)
    {
        dirty(_extractDate, extractDate);
        _extractDate = extractDate;
    }

    /**
     * @return Не учитывать при расчете стажа. Свойство не может быть null.
     */
    @NotNull
    public boolean isEmptyServiceLengthType()
    {
        return _emptyServiceLengthType;
    }

    /**
     * @param emptyServiceLengthType Не учитывать при расчете стажа. Свойство не может быть null.
     */
    public void setEmptyServiceLengthType(boolean emptyServiceLengthType)
    {
        dirty(_emptyServiceLengthType, emptyServiceLengthType);
        _emptyServiceLengthType = emptyServiceLengthType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmploymentHistoryItemBaseGen)
        {
            setEmployee(((EmploymentHistoryItemBase)another).getEmployee());
            setAssignDate(((EmploymentHistoryItemBase)another).getAssignDate());
            setDismissalDate(((EmploymentHistoryItemBase)another).getDismissalDate());
            setStaffRate(((EmploymentHistoryItemBase)another).getStaffRate());
            setPostType(((EmploymentHistoryItemBase)another).getPostType());
            setCurrent(((EmploymentHistoryItemBase)another).getCurrent());
            setExtract(((EmploymentHistoryItemBase)another).getExtract());
            setExtractNumber(((EmploymentHistoryItemBase)another).getExtractNumber());
            setExtractDate(((EmploymentHistoryItemBase)another).getExtractDate());
            setEmptyServiceLengthType(((EmploymentHistoryItemBase)another).isEmptyServiceLengthType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmploymentHistoryItemBaseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmploymentHistoryItemBase.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EmploymentHistoryItemBase is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employee":
                    return obj.getEmployee();
                case "assignDate":
                    return obj.getAssignDate();
                case "dismissalDate":
                    return obj.getDismissalDate();
                case "staffRate":
                    return obj.getStaffRate();
                case "postType":
                    return obj.getPostType();
                case "current":
                    return obj.getCurrent();
                case "extract":
                    return obj.getExtract();
                case "extractNumber":
                    return obj.getExtractNumber();
                case "extractDate":
                    return obj.getExtractDate();
                case "emptyServiceLengthType":
                    return obj.isEmptyServiceLengthType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
                case "assignDate":
                    obj.setAssignDate((Date) value);
                    return;
                case "dismissalDate":
                    obj.setDismissalDate((Date) value);
                    return;
                case "staffRate":
                    obj.setStaffRate((Double) value);
                    return;
                case "postType":
                    obj.setPostType((PostType) value);
                    return;
                case "current":
                    obj.setCurrent((Boolean) value);
                    return;
                case "extract":
                    obj.setExtract((IAbstractDocument) value);
                    return;
                case "extractNumber":
                    obj.setExtractNumber((String) value);
                    return;
                case "extractDate":
                    obj.setExtractDate((Date) value);
                    return;
                case "emptyServiceLengthType":
                    obj.setEmptyServiceLengthType((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employee":
                        return true;
                case "assignDate":
                        return true;
                case "dismissalDate":
                        return true;
                case "staffRate":
                        return true;
                case "postType":
                        return true;
                case "current":
                        return true;
                case "extract":
                        return true;
                case "extractNumber":
                        return true;
                case "extractDate":
                        return true;
                case "emptyServiceLengthType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employee":
                    return true;
                case "assignDate":
                    return true;
                case "dismissalDate":
                    return true;
                case "staffRate":
                    return true;
                case "postType":
                    return true;
                case "current":
                    return true;
                case "extract":
                    return true;
                case "extractNumber":
                    return true;
                case "extractDate":
                    return true;
                case "emptyServiceLengthType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employee":
                    return Employee.class;
                case "assignDate":
                    return Date.class;
                case "dismissalDate":
                    return Date.class;
                case "staffRate":
                    return Double.class;
                case "postType":
                    return PostType.class;
                case "current":
                    return Boolean.class;
                case "extract":
                    return IAbstractDocument.class;
                case "extractNumber":
                    return String.class;
                case "extractDate":
                    return Date.class;
                case "emptyServiceLengthType":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmploymentHistoryItemBase> _dslPath = new Path<EmploymentHistoryItemBase>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmploymentHistoryItemBase");
    }
            

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    /**
     * @return Дата назначения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getAssignDate()
     */
    public static PropertyPath<Date> assignDate()
    {
        return _dslPath.assignDate();
    }

    /**
     * @return Дата увольнения.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getDismissalDate()
     */
    public static PropertyPath<Date> dismissalDate()
    {
        return _dslPath.dismissalDate();
    }

    /**
     * @return Ставка (суммарная).
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getStaffRate()
     */
    public static PropertyPath<Double> staffRate()
    {
        return _dslPath.staffRate();
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getPostType()
     */
    public static PostType.Path<PostType> postType()
    {
        return _dslPath.postType();
    }

    /**
     * @return Текущая.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getCurrent()
     */
    public static PropertyPath<Boolean> current()
    {
        return _dslPath.current();
    }

    /**
     * @return Выписка приказа.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getExtract()
     */
    public static IAbstractDocumentGen.Path<IAbstractDocument> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getExtractNumber()
     */
    public static PropertyPath<String> extractNumber()
    {
        return _dslPath.extractNumber();
    }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getExtractDate()
     */
    public static PropertyPath<Date> extractDate()
    {
        return _dslPath.extractDate();
    }

    /**
     * @return Не учитывать при расчете стажа. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#isEmptyServiceLengthType()
     */
    public static PropertyPath<Boolean> emptyServiceLengthType()
    {
        return _dslPath.emptyServiceLengthType();
    }

    public static class Path<E extends EmploymentHistoryItemBase> extends EntityPath<E>
    {
        private Employee.Path<Employee> _employee;
        private PropertyPath<Date> _assignDate;
        private PropertyPath<Date> _dismissalDate;
        private PropertyPath<Double> _staffRate;
        private PostType.Path<PostType> _postType;
        private PropertyPath<Boolean> _current;
        private IAbstractDocumentGen.Path<IAbstractDocument> _extract;
        private PropertyPath<String> _extractNumber;
        private PropertyPath<Date> _extractDate;
        private PropertyPath<Boolean> _emptyServiceLengthType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

    /**
     * @return Дата назначения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getAssignDate()
     */
        public PropertyPath<Date> assignDate()
        {
            if(_assignDate == null )
                _assignDate = new PropertyPath<Date>(EmploymentHistoryItemBaseGen.P_ASSIGN_DATE, this);
            return _assignDate;
        }

    /**
     * @return Дата увольнения.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getDismissalDate()
     */
        public PropertyPath<Date> dismissalDate()
        {
            if(_dismissalDate == null )
                _dismissalDate = new PropertyPath<Date>(EmploymentHistoryItemBaseGen.P_DISMISSAL_DATE, this);
            return _dismissalDate;
        }

    /**
     * @return Ставка (суммарная).
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getStaffRate()
     */
        public PropertyPath<Double> staffRate()
        {
            if(_staffRate == null )
                _staffRate = new PropertyPath<Double>(EmploymentHistoryItemBaseGen.P_STAFF_RATE, this);
            return _staffRate;
        }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getPostType()
     */
        public PostType.Path<PostType> postType()
        {
            if(_postType == null )
                _postType = new PostType.Path<PostType>(L_POST_TYPE, this);
            return _postType;
        }

    /**
     * @return Текущая.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getCurrent()
     */
        public PropertyPath<Boolean> current()
        {
            if(_current == null )
                _current = new PropertyPath<Boolean>(EmploymentHistoryItemBaseGen.P_CURRENT, this);
            return _current;
        }

    /**
     * @return Выписка приказа.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getExtract()
     */
        public IAbstractDocumentGen.Path<IAbstractDocument> extract()
        {
            if(_extract == null )
                _extract = new IAbstractDocumentGen.Path<IAbstractDocument>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getExtractNumber()
     */
        public PropertyPath<String> extractNumber()
        {
            if(_extractNumber == null )
                _extractNumber = new PropertyPath<String>(EmploymentHistoryItemBaseGen.P_EXTRACT_NUMBER, this);
            return _extractNumber;
        }

    /**
     * @return Дата приказа.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#getExtractDate()
     */
        public PropertyPath<Date> extractDate()
        {
            if(_extractDate == null )
                _extractDate = new PropertyPath<Date>(EmploymentHistoryItemBaseGen.P_EXTRACT_DATE, this);
            return _extractDate;
        }

    /**
     * @return Не учитывать при расчете стажа. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase#isEmptyServiceLengthType()
     */
        public PropertyPath<Boolean> emptyServiceLengthType()
        {
            if(_emptyServiceLengthType == null )
                _emptyServiceLengthType = new PropertyPath<Boolean>(EmploymentHistoryItemBaseGen.P_EMPTY_SERVICE_LENGTH_TYPE, this);
            return _emptyServiceLengthType;
        }

        public Class getEntityClass()
        {
            return EmploymentHistoryItemBase.class;
        }

        public String getEntityName()
        {
            return "employmentHistoryItemBase";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
