/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;
import ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic.EmployeeWrapper;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 20.11.12
 */
@Configuration
public class EmpSummaryPPSReportAdd extends BusinessComponentManager
{
    //ds
    public static final String POST_TYPE_DS = "postTypeDS";

    public static final String PPS_SEARCH_DS = "ppsSearchDS";
    public static final String PPS_WITH_SCIENCE_SEARCH_DS = "ppsWithScienceSearchDS";
    public static final String PPS_WITH_HIGH_SCIENCE_SEARCH_DS = "ppsWithHighScienceSearchDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(POST_TYPE_DS, postTypeDSHandler()))
                .addDataSource(searchListDS(PPS_SEARCH_DS, ppsSearchDSColumns(), ppsSearchDSHandler()))
                .addDataSource(searchListDS(PPS_WITH_SCIENCE_SEARCH_DS, ppsWithScienceSearchDSColumns(), ppsWithScienceSearchDSHandler()))
                .addDataSource(searchListDS(PPS_WITH_HIGH_SCIENCE_SEARCH_DS, ppsWithHighScienceSearchDSColumns(), ppsWithHighScienceSearchDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> postTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PostType.class)
                .order(PostType.title())
                .filter(PostType.title());
    }

    @Bean
    public ColumnListExtPoint ppsSearchDSColumns()
    {
        IStyleResolver styleResolver = rowEntity -> ((SummaryDataWrapper)rowEntity).getTitle().equals("Всего") ? "font-weight:bold;" : "";
        return columnListExtPointBuilder(PPS_SEARCH_DS)
                .addColumn(textColumn("title", "title").styleResolver(styleResolver))
                .addColumn(actionColumn("6", "6", "onClickPPS6").visible("mvel:presenter.isColumnVisible('6')").styleResolver(styleResolver))
                .addColumn(actionColumn("4", "4", "onClickPPS4").visible("mvel:presenter.isColumnVisible('4')").styleResolver(styleResolver))
                .addColumn(actionColumn("5", "5", "onClickPPS5").visible("mvel:presenter.isColumnVisible('5')").styleResolver(styleResolver))
                .addColumn(actionColumn("3", "3", "onClickPPS3").visible("mvel:presenter.isColumnVisible('3')").styleResolver(styleResolver))
                .addColumn(actionColumn("1", "1", "onClickPPS1").visible("mvel:presenter.isColumnVisible('1')").styleResolver(styleResolver))
                .addColumn(actionColumn("7", "7", "onClickPPS7").visible("mvel:presenter.isColumnVisible('7')").styleResolver(styleResolver))
                .addColumn(actionColumn("2", "2", "onClickPPS2").visible("mvel:presenter.isColumnVisible('2')").styleResolver(styleResolver))
                .addColumn(actionColumn("8", "8", "onClickPPS8").visible("mvel:presenter.isColumnVisible('8')").styleResolver(styleResolver))
                .addColumn(actionColumn("hourlyPaid", "hourlyPaid", "onClickPPSHourlyPaid").styleResolver(styleResolver))
                .create();
    }

    @Bean
    public ColumnListExtPoint ppsWithScienceSearchDSColumns()
    {
        IStyleResolver styleResolver = rowEntity -> ((SummaryDataWrapper)rowEntity).getTitle().equals("Всего") ? "font-weight:bold;" : "";
        return columnListExtPointBuilder(PPS_WITH_SCIENCE_SEARCH_DS)
                .addColumn(textColumn("title", "title").styleResolver(styleResolver))
                .addColumn(actionColumn("6", "6", "onClickPPSWithScience6").visible("mvel:presenter.isColumnVisible('6')").styleResolver(styleResolver))
                .addColumn(actionColumn("4", "4", "onClickPPSWithScience4").visible("mvel:presenter.isColumnVisible('4')").styleResolver(styleResolver))
                .addColumn(actionColumn("5", "5", "onClickPPSWithScience5").visible("mvel:presenter.isColumnVisible('5')").styleResolver(styleResolver))
                .addColumn(actionColumn("3", "3", "onClickPPSWithScience3").visible("mvel:presenter.isColumnVisible('3')").styleResolver(styleResolver))
                .addColumn(actionColumn("1", "1", "onClickPPSWithScience1").visible("mvel:presenter.isColumnVisible('1')").styleResolver(styleResolver))
                .addColumn(actionColumn("7", "7", "onClickPPSWithScience7").visible("mvel:presenter.isColumnVisible('7')").styleResolver(styleResolver))
                .addColumn(actionColumn("2", "2", "onClickPPSWithScience2").visible("mvel:presenter.isColumnVisible('2')").styleResolver(styleResolver))
                .addColumn(actionColumn("8", "8", "onClickPPSWithScience8").visible("mvel:presenter.isColumnVisible('8')").styleResolver(styleResolver))
                .addColumn(actionColumn("hourlyPaid", "hourlyPaid", "onClickPPSWithScienceHourlyPaid").styleResolver(styleResolver))
                .create();
    }

    @Bean
    public ColumnListExtPoint ppsWithHighScienceSearchDSColumns()
    {
        IStyleResolver styleResolver = rowEntity -> ((SummaryDataWrapper)rowEntity).getTitle().equals("Всего") ? "font-weight:bold;" : "";
        return columnListExtPointBuilder(PPS_WITH_HIGH_SCIENCE_SEARCH_DS)
                .addColumn(textColumn("title", "title").styleResolver(styleResolver))
                .addColumn(actionColumn("6", "6", "onClickPPSWithHighScience6").visible("mvel:presenter.isColumnVisible('6')").styleResolver(styleResolver))
                .addColumn(actionColumn("4", "4", "onClickPPSWithHighScience4").visible("mvel:presenter.isColumnVisible('4')").styleResolver(styleResolver))
                .addColumn(actionColumn("5", "5", "onClickPPSWithHighScience5").visible("mvel:presenter.isColumnVisible('5')").styleResolver(styleResolver))
                .addColumn(actionColumn("3", "3", "onClickPPSWithHighScience3").visible("mvel:presenter.isColumnVisible('3')").styleResolver(styleResolver))
                .addColumn(actionColumn("1", "1", "onClickPPSWithHighScience1").visible("mvel:presenter.isColumnVisible('1')").styleResolver(styleResolver))
                .addColumn(actionColumn("7", "7", "onClickPPSWithHighScience7").visible("mvel:presenter.isColumnVisible('7')").styleResolver(styleResolver))
                .addColumn(actionColumn("2", "2", "onClickPPSWithHighScience2").visible("mvel:presenter.isColumnVisible('2')").styleResolver(styleResolver))
                .addColumn(actionColumn("8", "8", "onClickPPSWithHighScience8").visible("mvel:presenter.isColumnVisible('8')").styleResolver(styleResolver))
                .addColumn(actionColumn("hourlyPaid", "hourlyPaid", "onClickPPSWithHighScienceHourlyPaid").styleResolver(styleResolver))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ppsSearchDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<SummaryDataWrapper<EmployeeWrapper>> list = context.get(EmpSummaryPPSReportAddUI.PROP_PPS_LIST);

                return ListOutputBuilder.get(input, list).pageable(false).build();
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ppsWithScienceSearchDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<SummaryDataWrapper<EmployeeWrapper>> list = context.get(EmpSummaryPPSReportAddUI.PROP_PPS_WITH_SCIENCE_LIST);

                return ListOutputBuilder.get(input, list).pageable(false).build();
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> ppsWithHighScienceSearchDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<SummaryDataWrapper<EmployeeWrapper>> list = context.get(EmpSummaryPPSReportAddUI.PROP_PPS_WITH_HIGH_SCIENCE_LIST);

                return ListOutputBuilder.get(input, list).pageable(false).build();
            }
        };
    }
}
