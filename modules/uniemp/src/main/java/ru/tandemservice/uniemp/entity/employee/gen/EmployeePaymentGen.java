package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выплаты сотруднику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeePaymentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeePayment";
    public static final String ENTITY_NAME = "employeePayment";
    public static final int VERSION_HASH = -1232258849;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_PAYMENT = "payment";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";
    public static final String P_ASSIGN_DATE = "assignDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_AMOUNT = "amount";
    public static final String P_PAYMENT_VALUE = "paymentValue";

    private EmployeePost _employeePost;     // Сотрудник
    private Payment _payment;     // Выплата
    private FinancingSource _financingSource;     // Источник финансирования (настройка названий)
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (настройка названий, детально)
    private Date _assignDate;     // Дата назначения
    private Date _beginDate;     // Назначена с даты
    private Date _endDate;     // Назначена по дату
    private Double _amount;     // Сумма, руб
    private Double _paymentValue;     // Размер выплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Выплата. Свойство не может быть null.
     */
    @NotNull
    public Payment getPayment()
    {
        return _payment;
    }

    /**
     * @param payment Выплата. Свойство не может быть null.
     */
    public void setPayment(Payment payment)
    {
        dirty(_payment, payment);
        _payment = payment;
    }

    /**
     * @return Источник финансирования (настройка названий).
     */
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования (настройка названий).
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (настройка названий, детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    /**
     * @return Дата назначения. Свойство не может быть null.
     */
    @NotNull
    public Date getAssignDate()
    {
        return _assignDate;
    }

    /**
     * @param assignDate Дата назначения. Свойство не может быть null.
     */
    public void setAssignDate(Date assignDate)
    {
        dirty(_assignDate, assignDate);
        _assignDate = assignDate;
    }

    /**
     * @return Назначена с даты. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Назначена с даты. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Назначена по дату.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Назначена по дату.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Сумма, руб.
     */
    public Double getAmount()
    {
        return _amount;
    }

    /**
     * @param amount Сумма, руб.
     */
    public void setAmount(Double amount)
    {
        dirty(_amount, amount);
        _amount = amount;
    }

    /**
     * @return Размер выплаты.
     */
    public Double getPaymentValue()
    {
        return _paymentValue;
    }

    /**
     * @param paymentValue Размер выплаты.
     */
    public void setPaymentValue(Double paymentValue)
    {
        dirty(_paymentValue, paymentValue);
        _paymentValue = paymentValue;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeePaymentGen)
        {
            setEmployeePost(((EmployeePayment)another).getEmployeePost());
            setPayment(((EmployeePayment)another).getPayment());
            setFinancingSource(((EmployeePayment)another).getFinancingSource());
            setFinancingSourceItem(((EmployeePayment)another).getFinancingSourceItem());
            setAssignDate(((EmployeePayment)another).getAssignDate());
            setBeginDate(((EmployeePayment)another).getBeginDate());
            setEndDate(((EmployeePayment)another).getEndDate());
            setAmount(((EmployeePayment)another).getAmount());
            setPaymentValue(((EmployeePayment)another).getPaymentValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeePaymentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeePayment.class;
        }

        public T newInstance()
        {
            return (T) new EmployeePayment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "payment":
                    return obj.getPayment();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
                case "assignDate":
                    return obj.getAssignDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "amount":
                    return obj.getAmount();
                case "paymentValue":
                    return obj.getPaymentValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "payment":
                    obj.setPayment((Payment) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
                case "assignDate":
                    obj.setAssignDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "amount":
                    obj.setAmount((Double) value);
                    return;
                case "paymentValue":
                    obj.setPaymentValue((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "payment":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
                case "assignDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "amount":
                        return true;
                case "paymentValue":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "payment":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
                case "assignDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "amount":
                    return true;
                case "paymentValue":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "payment":
                    return Payment.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
                case "assignDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "amount":
                    return Double.class;
                case "paymentValue":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeePayment> _dslPath = new Path<EmployeePayment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeePayment");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getPayment()
     */
    public static Payment.Path<Payment> payment()
    {
        return _dslPath.payment();
    }

    /**
     * @return Источник финансирования (настройка названий).
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    /**
     * @return Дата назначения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getAssignDate()
     */
    public static PropertyPath<Date> assignDate()
    {
        return _dslPath.assignDate();
    }

    /**
     * @return Назначена с даты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Назначена по дату.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Сумма, руб.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getAmount()
     */
    public static PropertyPath<Double> amount()
    {
        return _dslPath.amount();
    }

    /**
     * @return Размер выплаты.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getPaymentValue()
     */
    public static PropertyPath<Double> paymentValue()
    {
        return _dslPath.paymentValue();
    }

    public static class Path<E extends EmployeePayment> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private Payment.Path<Payment> _payment;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;
        private PropertyPath<Date> _assignDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Double> _amount;
        private PropertyPath<Double> _paymentValue;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getPayment()
     */
        public Payment.Path<Payment> payment()
        {
            if(_payment == null )
                _payment = new Payment.Path<Payment>(L_PAYMENT, this);
            return _payment;
        }

    /**
     * @return Источник финансирования (настройка названий).
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

    /**
     * @return Дата назначения. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getAssignDate()
     */
        public PropertyPath<Date> assignDate()
        {
            if(_assignDate == null )
                _assignDate = new PropertyPath<Date>(EmployeePaymentGen.P_ASSIGN_DATE, this);
            return _assignDate;
        }

    /**
     * @return Назначена с даты. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeePaymentGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Назначена по дату.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeePaymentGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Сумма, руб.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getAmount()
     */
        public PropertyPath<Double> amount()
        {
            if(_amount == null )
                _amount = new PropertyPath<Double>(EmployeePaymentGen.P_AMOUNT, this);
            return _amount;
        }

    /**
     * @return Размер выплаты.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeePayment#getPaymentValue()
     */
        public PropertyPath<Double> paymentValue()
        {
            if(_paymentValue == null )
                _paymentValue = new PropertyPath<Double>(EmployeePaymentGen.P_PAYMENT_VALUE, this);
            return _paymentValue;
        }

        public Class getEntityClass()
        {
            return EmployeePayment.class;
        }

        public String getEntityName()
        {
            return "employeePayment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
