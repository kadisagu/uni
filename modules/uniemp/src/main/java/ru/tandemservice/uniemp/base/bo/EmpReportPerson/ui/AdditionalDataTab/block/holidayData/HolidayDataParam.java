/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.holidayData;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class HolidayDataParam implements IReportDQLModifier
{
    private IReportParam<Date> _holidayBeginFrom = new ReportParam<>();
    private IReportParam<Date> _holidayBeginTo = new ReportParam<>();
    private IReportParam<Date> _holidayEndFrom = new ReportParam<>();
    private IReportParam<Date> _holidayEndTo = new ReportParam<>();
    private IReportParam<List<HolidayType>> _holidayType = new ReportParam<>();

    private String alias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = dql.innerJoinEntity(Employee.class, Employee.person());

        // соединяем должность
        String employeePostAlias = dql.innerJoinEntity(employeeAlias, EmployeePost.class, EmployeePost.employee());

        // соединяем отпуск
        return dql.innerJoinEntity(employeePostAlias, EmployeeHoliday.class, EmployeeHoliday.employeePost());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_holidayBeginFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "holidayData.holidayBeginFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_holidayBeginFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeHoliday.startDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_holidayBeginFrom.getData()))));
        }

        if (_holidayBeginTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "holidayData.holidayBeginTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_holidayBeginTo.getData()));

            Date nextDay = CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(_holidayBeginTo.getData(), Calendar.DAY_OF_YEAR, 1));
            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeHoliday.startDate().fromAlias(alias(dql))), DQLExpressions.valueDate(nextDay)));
        }

        if (_holidayEndFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "holidayData.holidayEndFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_holidayEndFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeHoliday.finishDate().fromAlias(alias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_holidayEndFrom.getData()))));
        }

        if (_holidayEndTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "holidayData.holidayEndTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_holidayEndTo.getData()));

            Date nextDay = CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(_holidayEndTo.getData(), Calendar.DAY_OF_YEAR, 1));
            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeHoliday.finishDate().fromAlias(alias(dql))), DQLExpressions.valueDate(nextDay)));
        }

        if (_holidayType.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "holidayData.holidayType", CommonBaseStringUtil.join(_holidayType.getData(), HolidayType.P_TITLE, ", "));

            dql.builder.where(DQLExpressions.in(DQLExpressions.property(EmployeeHoliday.holidayType().fromAlias(alias(dql))), _holidayType.getData()));
        }
    }

    // Getters

    public IReportParam<Date> getHolidayBeginFrom()
    {
        return _holidayBeginFrom;
    }

    public IReportParam<Date> getHolidayBeginTo()
    {
        return _holidayBeginTo;
    }

    public IReportParam<Date> getHolidayEndFrom()
    {
        return _holidayEndFrom;
    }

    public IReportParam<Date> getHolidayEndTo()
    {
        return _holidayEndTo;
    }

    public IReportParam<List<HolidayType>> getHolidayType()
    {
        return _holidayType;
    }
}
