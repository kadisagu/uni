package ru.tandemservice.uniemp.entity.report;

import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.report.gen.RosobrReportStimPaymentPriorityGen;

/**
 * Приоритет следования выплат стимулирующего характера в отчете
 */
public class RosobrReportStimPaymentPriority extends RosobrReportStimPaymentPriorityGen
{
    public RosobrReportStimPaymentPriority()
    {
        super();
    }

    public RosobrReportStimPaymentPriority(Payment payment)
    {
        setPayment(payment);
    }
}