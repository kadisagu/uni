/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.EmployeeSickListAddEdit;

import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.entity.catalog.SickListBasics;
import ru.tandemservice.uniemp.entity.employee.EmployeeSickList;

/**
 * @author E. Grigoriev
 * @since 08.07.2008
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (null != model.getEmployeeSickList().getId())
            model.setEmployeeSickList(get(EmployeeSickList.class, model.getEmployeeSickList().getId()));
        model.getEmployeeSickList().setEmployee((Employee)get(model.getEmployeeId()));
        model.setSickListBasics(getCatalogItemList(SickListBasics.class));
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if(model.getEmployeeSickList().getOpenDate().getTime() > model.getEmployeeSickList().getCloseDate().getTime())
            errors.add("Дата открытия листа нетрудоспособности не может быть больше даты его закрытия", "openDate", "closeDate");
    }

}