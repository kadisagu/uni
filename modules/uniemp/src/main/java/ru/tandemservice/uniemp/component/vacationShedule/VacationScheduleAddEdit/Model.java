/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleAddEdit;

import org.tandemframework.core.component.Input;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;

/**
 * @author dseleznev
 * Created on: 10.01.2011
 */
@Input(keys = { "vacationScheduleId", "orgUnitId" }, bindings = { "vacationScheduleId", "orgUnitId" })
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private boolean _addForm;

    private Long _vacationScheduleId;
    private VacationSchedule _vacationSchedule;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        this._addForm = addForm;
    }

    public Long getVacationScheduleId()
    {
        return _vacationScheduleId;
    }

    public void setVacationScheduleId(Long vacationScheduleId)
    {
        this._vacationScheduleId = vacationScheduleId;
    }

    public VacationSchedule getVacationSchedule()
    {
        return _vacationSchedule;
    }

    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        this._vacationSchedule = vacationSchedule;
    }
}