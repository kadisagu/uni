/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.service;

import org.tandemframework.core.exception.ApplicationException;

import ru.tandemservice.uni.services.UniService;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;

/**
 * Стандартный сервис отрицательного завершения процедуры согласования приказа
 *
 * @author iolshvang
 * @since 07.04.2011
 */
public class RejectVisaVacationScheduleService extends UniService implements ITouchVisaTaskHandler
{
    private IAbstractDocument _vacationSchedule;

    @Override
    public void init(IAbstractDocument document)
    {
        _vacationSchedule = (IAbstractDocument)document;
    }

    @Override
    protected void doValidate()
    {
        if (UniempDefines.STAFF_LIST_STATUS_FORMING.equals(_vacationSchedule.getState().getCode()))
            throw new ApplicationException("График уже в состоянии «" + _vacationSchedule.getState().getTitle() + "».");
    }

    @Override
    protected void doExecute() throws Exception
    {
        _vacationSchedule.setState(getCoreDao().getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));
        ((VacationSchedule)_vacationSchedule).setAcceptDate(null);
        getCoreDao().update(_vacationSchedule);
    }
}