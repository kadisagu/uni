/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffList.StaffListPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
@State({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "publisherId"),
    @Bind(key = "staffListSelectedTab", binding = "staffListSelectedTab")
})
@Output(keys = { "staffListId", "staffListAllocationItemId" }, bindings = { "staffList.id", "staffListAllocationItemId" })
public class Model
{
    private Long _publisherId;
    private StaffList _staffList;
    private String _staffListSelectedTab;
    private Long _staffListAllocationItemId;
    private DynamicListDataSource<StaffListItem> _dataSource;
    private DynamicListDataSource<PaymentWrapper> _paymentsDataSource;
    private DynamicListDataSource<IdentifiableWrapper> _paymentsTotalDataSource;
    private DynamicListDataSource<AllocationWrapper> _allocationDataSource;
    private DynamicListDataSource<IdentifiableWrapper> _allocationTotalDataSource;
    private DynamicListDataSource<EmployeeHRLineWrapper> _employeeHRDataSource;

    Map<Long, StaffListAllocationItem> _idAllocItem = new HashMap<>();

    private ISelectModel _postsList;
    private ISelectModel _hrPostsList;
    private OrgUnitPostRelation _postFilter;
    private OrgUnitPostRelation _hrPostFilter;
    private List<HSelectOption> _employeeTypesList;
    private EmployeeType _employeeTypeFilter;
    private EmployeeType _hrEmployeeTypeFilter;
    private String _lastNameFilter;
    private String _hrLastNameFilter;

    private boolean _staffListEmpty;

    List<Payment> _paymentsList;
    List<Payment> _allocPaymentsList;
    List<Payment> _compensPaymentsList;

    public Map<Long, StaffListAllocationItem> getIdAllocItem()
    {
        return _idAllocItem;
    }

    public void setIdAllocItem(Map<Long, StaffListAllocationItem> idAllocItem)
    {
        _idAllocItem = idAllocItem;
    }

    public DynamicListDataSource<IdentifiableWrapper> getAllocationTotalDataSource()
    {
        return _allocationTotalDataSource;
    }

    public void setAllocationTotalDataSource(DynamicListDataSource<IdentifiableWrapper> allocationTotalDataSource)
    {
        _allocationTotalDataSource = allocationTotalDataSource;
    }

    public DynamicListDataSource<IdentifiableWrapper> getPaymentsTotalDataSource()
    {
        return _paymentsTotalDataSource;
    }

    public void setPaymentsTotalDataSource(DynamicListDataSource<IdentifiableWrapper> paymentsTotalDataSource)
    {
        _paymentsTotalDataSource = paymentsTotalDataSource;
    }

    public Long getPublisherId()
    {
        return _publisherId;
    }

    public void setPublisherId(Long publisherId)
    {
        this._publisherId = publisherId;
    }

    public StaffList getStaffList()
    {
        return _staffList;
    }

    public void setStaffList(StaffList staffList)
    {
        this._staffList = staffList;
    }

    public DynamicListDataSource<StaffListItem> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StaffListItem> dataSource)
    {
        this._dataSource = dataSource;
    }

    public DynamicListDataSource<PaymentWrapper> getPaymentsDataSource()
    {
        return _paymentsDataSource;
    }

    public void setPaymentsDataSource(DynamicListDataSource<PaymentWrapper> paymentsDataSource)
    {
        _paymentsDataSource = paymentsDataSource;
    }

    public DynamicListDataSource<AllocationWrapper> getAllocationDataSource()
    {
        return _allocationDataSource;
    }

    public void setAllocationDataSource(DynamicListDataSource<AllocationWrapper> allocationDataSource)
    {
        _allocationDataSource = allocationDataSource;
    }

    public DynamicListDataSource<EmployeeHRLineWrapper> getEmployeeHRDataSource()
    {
        return _employeeHRDataSource;
    }

    public void setEmployeeHRDataSource(DynamicListDataSource<EmployeeHRLineWrapper> employeeHRDataSource)
    {
        this._employeeHRDataSource = employeeHRDataSource;
    }

    public String getEditKey()
    {
        return "editStaffList";
    }

    public String getDelKey()
    {
        return "deleteStaffList";
    }

    public String getChangeStateKey()
    {
        return "changeStateStaffList";
    }

    public String getAddItemKey()
    {
        return "addStaffListItem_StaffListTab";
    }

    public String getAddAllocationItemKey()
    {
        return "addStaffListAllocationItem_StaffListAllocationTab";
    }

    public String getAddReserveAllocationItemKey()
    {
        return "addStaffListReserveAllocationItem_StaffListAllocationTab";
    }

    public Boolean getDisabledAddReserveAllocationItem()
    {
        return getStaffList().getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ARCHIVE);
    }

    public String getFillAllocAutKey()
    {
        return "fillStaffListAllocationAutomatically";
    }

    public String getCopyKey()
    {
        return "copyStaffList";
    }

    public String getStaffListSelectedTab()
    {
        return _staffListSelectedTab;
    }

    public void setStaffListSelectedTab(String staffListSelectedTab)
    {
        this._staffListSelectedTab = staffListSelectedTab;
    }

    public Long getStaffListAllocationItemId()
    {
        return _staffListAllocationItemId;
    }

    public void setStaffListAllocationItemId(Long staffListAllocationItemId)
    {
        this._staffListAllocationItemId = staffListAllocationItemId;
    }

    public List<Payment> getPaymentsList()
    {
        return _paymentsList;
    }

    public void setPaymentsList(List<Payment> paymentsList)
    {
        this._paymentsList = paymentsList;
    }

    public ISelectModel getPostsList()
    {
        return _postsList;
    }

    public void setPostsList(ISelectModel postsList)
    {
        this._postsList = postsList;
    }

    public ISelectModel getHrPostsList()
    {
        return _hrPostsList;
    }

    public void setHrPostsList(ISelectModel hrPostsList)
    {
        this._hrPostsList = hrPostsList;
    }

    public OrgUnitPostRelation getPostFilter()
    {
        return _postFilter;
    }

    public void setPostFilter(OrgUnitPostRelation postFilter)
    {
        this._postFilter = postFilter;
    }

    public OrgUnitPostRelation getHrPostFilter()
    {
        return _hrPostFilter;
    }

    public void setHrPostFilter(OrgUnitPostRelation hrPostFilter)
    {
        this._hrPostFilter = hrPostFilter;
    }

    public boolean isStaffListEmpty()
    {
        return _staffListEmpty;
    }

    public void setStaffListEmpty(boolean staffListEmpty)
    {
        this._staffListEmpty = staffListEmpty;
    }

    public List<Payment> getAllocPaymentsList()
    {
        return _allocPaymentsList;
    }

    public void setAllocPaymentsList(List<Payment> allocPaymentsList)
    {
        this._allocPaymentsList = allocPaymentsList;
    }

    public List<Payment> getCompensPaymentsList()
    {
        return _compensPaymentsList;
    }

    public void setCompensPaymentsList(List<Payment> compensPaymentsList)
    {
        this._compensPaymentsList = compensPaymentsList;
    }

    public List<HSelectOption> getEmployeeTypesList()
    {
        return _employeeTypesList;
    }

    public void setEmployeeTypesList(List<HSelectOption> employeeTypesList)
    {
        _employeeTypesList = employeeTypesList;
    }

    public void setEmployeeTypeFilter(EmployeeType _employeeTypeFilter)
    {
        this._employeeTypeFilter = _employeeTypeFilter;
    }

    public EmployeeType getEmployeeTypeFilter()
    {
        return _employeeTypeFilter;
    }
    
    public EmployeeType getHrEmployeeTypeFilter()
    {
        return _hrEmployeeTypeFilter;
    }

    public void setHrEmployeeTypeFilter(EmployeeType hrEmployeeTypeFilter)
    {
        this._hrEmployeeTypeFilter = hrEmployeeTypeFilter;
    }

    public String getLastNameFilter()
    {
        return _lastNameFilter;
    }

    public void setLastNameFilter(String lastNameFilter)
    {
        this._lastNameFilter = lastNameFilter;
    }

    public String getHrLastNameFilter()
    {
        return _hrLastNameFilter;
    }

    public void setHrLastNameFilter(String hrLastNameFilter)
    {
        this._hrLastNameFilter = hrLastNameFilter;
    }

    public boolean isArchivedVersion()
    {
        return UniempDefines.STAFF_LIST_STATUS_ARCHIVE.equals(getStaffList().getStaffListState().getCode());
    }
}