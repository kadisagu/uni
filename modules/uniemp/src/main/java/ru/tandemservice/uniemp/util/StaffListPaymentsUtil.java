/**
 * $Id$
 */
package ru.tandemservice.uniemp.util;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author dseleznev
 * Created on: 09.04.2009
 */
public class StaffListPaymentsUtil
{
    /**
     * Обновляет значения для всех выплат с учетом изменений в других выплатах должности штатной расстановки 
     * и базовой части оклада.
     * <br/>
     * @param allocationItem - должность штатной расстановки
     * @param session - hibernate сессия
     */
    public static void recalculateAllPaymentsValue(StaffListAllocationItem allocationItem, Session session)
    {
        // Вычисляем оклад сотрудника по базовому для должности штатного расписания уможенному на суммарную ставку
        Double monthBaseSalaryFund = allocationItem.getMonthBaseSalaryFund();
        
        // Вычисляем сумму всех выплат должности штатной расстановки
        Double sumPayments = UniempDaoFacade.getStaffListDAO().getSumOfPayments(allocationItem, true);
        
        // Вычисляем месячный фонд оплаты труда для должности штатной расстановки, с учетом должностного оклада и всех выплат 
        Double targetSalary = Math.round((monthBaseSalaryFund + sumPayments) * 100d) / 100d;

        // Обновляем значение месячного фонда оплаты труда у должности штатной расстановки
        allocationItem.setTargetSalary(targetSalary);
        session.update(allocationItem);
    }
    
    /**
     * Обновляет значения для всех выплат с учетом изменений в других выплатах должности штатного расписания 
     * и базовой части оклада. <br/>
     * Пересчитывает так же выплаты для всех созданных на должность должностей штатной расстановки.
     * <br/>
     * @param session - hibernate сессия
     */
    public static void recalculateAllPaymentsValueForStaffListItem(StaffListItem staffListItem, Session session)
    {
        List<StaffListAllocationItem> allocations = UniempDaoFacade.getStaffListDAO().getStaffListItemAllocations(staffListItem);
        
        for(StaffListAllocationItem item : allocations)
        {
            recalculateAllPaymentsValue(item, session);
        }
    }
    
    /**
     * Возвращает Map занятости штатных единиц по каждой должности
     * @param staffList - версия штатного расписания
     * @param session - hibernate сессия
     * @return Map&lt;Long, Double&gt; - ключ - id должности штатного расписания,
     * значение - ставка
     */
    public static Map<Long, Double> getOccupatedStaffListItemsMap(StaffList staffList, Session session)
    {
        MQBuilder allocationItems = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "sla");
        allocationItems.add(MQExpression.eq("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffList));
        List<StaffListAllocationItem> allocationItemsList = allocationItems.getResultList(session);

        Map<Long, Double> sumStaffRatesMap = new HashMap<>();
        for (StaffListAllocationItem item : allocationItemsList)
        {
            if (item.getEmployeePost() != null && item.getEmployeePost().getPostStatus().isActive())
            {
                BigDecimal staffRate = sumStaffRatesMap.get(item.getStaffListItem().getId()) != null ? new BigDecimal(sumStaffRatesMap.get(item.getStaffListItem().getId())) : null;
                if (staffRate == null)
                    staffRate = new BigDecimal(item.getStaffRate());
                else
                    staffRate = staffRate.add(new BigDecimal(item.getStaffRate()));

                sumStaffRatesMap.put(item.getStaffListItem().getId(), staffRate.setScale(2, RoundingMode.HALF_UP).doubleValue());
            }
        }
        return sumStaffRatesMap;
    }
    
    /**
     * Проверяет, укладываются ли введенные ставки (б/в) в суммарные ставки по штатному расписанию
     * @param session - hibernate сессия
     */
    public static boolean isStaffListAllocationItemValid(StaffListAllocationItem allocationItem, Session session)
    {
        Double staffRate = getOccupatedStaffListItemsMap(allocationItem.getStaffListItem().getStaffList(), session).get(allocationItem.getStaffListItem().getId());

        if(null == staffRate) staffRate = 0d;

        if (null == allocationItem.getId())
        {
            staffRate +=allocationItem.getStaffRate();
        }

        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, allocationItem.getStaffListItem().getStaffList()));
        builder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION, allocationItem.getStaffListItem().getOrgUnitPostRelation()));
        builder.add(MQExpression.eq("sli", StaffListItem.L_FINANCING_SOURCE, allocationItem.getStaffListItem().getFinancingSource()));
        builder.add(MQExpression.eq("sli", StaffListItem.L_FINANCING_SOURCE_ITEM, allocationItem.getStaffListItem().getFinancingSourceItem()));
        StaffListItem item = (StaffListItem)builder.uniqueResult(session);

        if(null == item)
            return false;

        return new BigDecimal(staffRate).setScale(2, RoundingMode.HALF_UP).doubleValue() <= new BigDecimal(item.getStaffRate()).setScale(2, RoundingMode.HALF_UP).doubleValue();

    }
    
    /**
     * Возвращает список id должностей подразделения, по которым превышено количество штатных единиц
     * @param staffList - версия штатного расписания
     * @param session - hibernate сессия
     * @return List&lt;Long&gt; - список id должностей штатного расписания, по которым превышено количество штатных единиц
     */
    public static List<Long> getInvalidPostIds(StaffList staffList, Session session)
    {
        MQBuilder sumStaffRates = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        sumStaffRates.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, staffList));
        List<StaffListItem> staffListItemList = sumStaffRates.getResultList(session);

        List<Long> excludedPostIds = new ArrayList<>();

        Map<Long, Double> occupatedStaffRatesMap = getOccupatedStaffListItemsMap(staffList, session);
        List<Long> itemsToRemove = new ArrayList<>();

        for (StaffListItem item : staffListItemList)
        {
            Double occRate = occupatedStaffRatesMap.get(item.getId()) != null ? new BigDecimal(occupatedStaffRatesMap.get(item.getId())).setScale(2, RoundingMode.HALF_UP).doubleValue() : null;
            if (null != occRate)
            {
                double staffRate = new BigDecimal(item.getStaffRate()).setScale(2, RoundingMode.HALF_UP).doubleValue();
                if (occRate > staffRate)
                    excludedPostIds.add(item.getId());
            }
            itemsToRemove.add(item.getId());
        }
        for(Map.Entry<Long, Double> entry : occupatedStaffRatesMap.entrySet())
            if(!itemsToRemove.contains(entry.getKey()))
                excludedPostIds.add(entry.getKey());
            
        return excludedPostIds;
    }
    
    /**
     * Возвращает список id должностей подразделения, которые нельзя копировать
     * @param staffList - версия штатного расписания
     * @param session - hibernate сессия
     * @return List&lt;Long&gt; - список id должностей штатного расписания, по которым потенциально будет превышено количество штатных единиц
     */
    public static List<Long> getUncopyableStaffListAllocationItemIds(StaffList staffList, Session session)
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "sla");
        builder.add(MQExpression.eq("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffList));
        List<StaffListAllocationItem> staffListAllocationItemList = builder.getResultList(session);

        List<Long> uncopyablePostIds = new ArrayList<>();

        Map<Long, Double> occupatedStaffRatesMap = getOccupatedStaffListItemsMap(staffList, session);

        for (StaffListAllocationItem item : staffListAllocationItemList)
        {
            Long id = item.getStaffListItem().getId();

            if (null == id)
            {
                uncopyablePostIds.add(item.getId());
                continue;
            }

            Double occRate = occupatedStaffRatesMap.get(id);

            if (null == occRate)
            {
                occRate = 0d;
                occupatedStaffRatesMap.put(id, occRate);
            }

            if (occRate + item.getStaffRate() > item.getStaffListItem().getStaffRate())
                uncopyablePostIds.add(item.getId());
        }
        return uncopyablePostIds;
    }

    /**
     * Возвращает true если версия штатного расписания не содержит элементов
     */
    public static boolean isStaffListEmpty(StaffList staffList, Session session)
    {
        MQBuilder staffListItemBuilder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        staffListItemBuilder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, staffList));
        if (staffListItemBuilder.getResultCount(session) > 0) return false;

        MQBuilder staffListAllocItemBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "sla");
        staffListAllocItemBuilder.add(MQExpression.eq("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffList));
        return staffListAllocItemBuilder.getResultCount(session) <= 0;

    }

    /**
     * Производит автозаполнение версии штатного расписания исходя из уже имеющихся на подразделении
     * сотрудников, их ставок и обязательных выплат.
     * @param staffList - версия штатного расписания
     * @param session - hibernate сессия
     */
    @SuppressWarnings("unchecked")
    public static void fillStaffListAutomatically(StaffList staffList, Session session)
    {
        if (!isStaffListEmpty(staffList, session))
            throw new ApplicationException("Автозаполнение штатной расстановки не может быть выполнено, поскольку в ней уже имеются должности.");

        MQBuilder builder = new MQBuilder(EmployeePostStaffRateItem.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", EmployeePostStaffRateItem.employeePost().orgUnit().s(), staffList.getOrgUnit()));

        //список ставок сотрудников данного подразделения
        List<EmployeePostStaffRateItem> employeePostsList = builder.<EmployeePostStaffRateItem> getResultList(session);
        // Key - id должности на типе подразделения, value - должность подразделения
        Map<OrgUnitTypePostRelation, OrgUnitPostRelation> orgUnitPostsMap = new HashMap<>();
        //список существующих должностей подразделения
        List<OrgUnitPostRelation> orgUnitPostsList = session.createCriteria(OrgUnitPostRelation.class).add(Restrictions.eq(OrgUnitPostRelation.L_ORG_UNIT, staffList.getOrgUnit())).list();
        //список обязательных выплат, которые необходимо добавить в StaffListItem'ы
        List<Payment> paymentsList = session.createCriteria(Payment.class).add(Restrictions.eq(Payment.P_ACTIVE, Boolean.TRUE)).add(Restrictions.eq(Payment.P_REQUIRED, Boolean.TRUE)).list();
        //список объектов настройки Базовые значения выплат для должностей
        MQBuilder paymentBaseValueBuilder = new MQBuilder(PaymentToPostBaseValue.ENTITY_CLASS, "pbv");
        paymentBaseValueBuilder.add(MQExpression.in("pbv", PaymentToPostBaseValue.L_PAYMENT, paymentsList));
        List<PaymentToPostBaseValue> paymentBaseValueList = paymentBaseValueBuilder.getResultList(session);
        //мапа для того что бы узнать значение выплаты из настройки
        Map<CoreCollectionUtils.Pair<Payment, PostBoundedWithQGandQL>, Double> paymentBaseValueMap = new HashMap<>();
        //мапа по которой будем создавать StaffListItem'ы
        Map<TripletKey<OrgUnitPostRelation, FinancingSource, FinancingSourceItem> , Double> postMap = new HashMap<>();
        //мапа для поднятия Должности ШР(StaffListItem) по ставке сотрудника
        Map<TripletKey<OrgUnitTypePostRelation, FinancingSource, FinancingSourceItem>, StaffListItem> staffListItemMap = new HashMap<>();

        for (PaymentToPostBaseValue paymentBaseValue : paymentBaseValueList)
            paymentBaseValueMap.put(new CoreCollectionUtils.Pair<>(paymentBaseValue.getPayment(), paymentBaseValue.getPostBoundedWithQGandQL()), paymentBaseValue.getValue());

        for (OrgUnitPostRelation item : orgUnitPostsList)
            orgUnitPostsMap.put(item.getOrgUnitTypePostRelation(), item);

        for (EmployeePostStaffRateItem staffRateItem : employeePostsList)
        {
            //создаем Должности, на которые наздачены сотрудники, но на подразделении их нет
            OrgUnitTypePostRelation postRelation = staffRateItem.getEmployeePost().getPostRelation();
            if (!orgUnitPostsMap.keySet().contains(postRelation))
            {
                OrgUnitPostRelation newOrgUnitPostRelation = new OrgUnitPostRelation();
                newOrgUnitPostRelation.setOrgUnit(staffList.getOrgUnit());
                newOrgUnitPostRelation.setOrgUnitTypePostRelation(postRelation);
                session.save(newOrgUnitPostRelation);
                orgUnitPostsMap.put(postRelation, newOrgUnitPostRelation);
            }

            //заполняем postMap, что бы вычислить суммарную ставку сотрудников на одной должности
            if (staffRateItem.getEmployeePost().getPostStatus().isActive())
            {
                TripletKey<OrgUnitPostRelation, FinancingSource, FinancingSourceItem> key = new TripletKey<>(orgUnitPostsMap.get(postRelation), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());
                Double staffRate = postMap.get(key);
                if (staffRate == null)
                    staffRate = 0d;
                staffRate += staffRateItem.getStaffRate();
                postMap.put(key, staffRate);
            }
        }

        for (Map.Entry<TripletKey<OrgUnitPostRelation, FinancingSource, FinancingSourceItem> , Double> entry : postMap.entrySet())
        {
            //создаем Должности ШР
            StaffListItem newStaffListItem = new StaffListItem();
            newStaffListItem.setStaffList(staffList);
            newStaffListItem.setOrgUnitPostRelation(entry.getKey().getFirst());
            newStaffListItem.setSalary(entry.getKey().getFirst().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getSalary() != null ? entry.getKey().getFirst().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getSalary() : 0d);
            newStaffListItem.setStaffRate(entry.getValue());
            newStaffListItem.setFinancingSource(entry.getKey().getSecond());
            newStaffListItem.setFinancingSourceItem(entry.getKey().getThird());
            session.save(newStaffListItem);

            //заполняем мапу, что бы потом достать соответствующие StaffListItem'ы
            staffListItemMap.put(new TripletKey<>(entry.getKey().getFirst().getOrgUnitTypePostRelation(), entry.getKey().getSecond(), entry.getKey().getThird()), newStaffListItem);

            //пробегаем по обязательным выплатам и создаем выплаты ШР для только что созданных должностей ШР
            for (Payment payment : paymentsList)
            {
                StaffListPostPayment newPayment = new StaffListPostPayment();
                //учитываем настройку Базовые значения выплат для должностей
                Double amount = paymentBaseValueMap.get(new CoreCollectionUtils.Pair<>(payment, entry.getKey().getFirst().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL()));
                newPayment.setAmount(amount != null ? amount : (payment.getValue() != null ? payment.getValue() : 0d));
                newPayment.setPayment(payment);
                newPayment.setFinancingSource(entry.getKey().getSecond());
                newPayment.setFinancingSourceItem(entry.getKey().getThird());
                newPayment.setStaffListItem(newStaffListItem);
                session.save(newPayment);
            }
        }

        //создаем ставки ШР(StaffListAllocationItem)
        for (EmployeePostStaffRateItem staffRateItem : employeePostsList)
        {
            if (staffRateItem.getEmployeePost().getPostStatus().isActive())
            {
                StaffListAllocationItem newAllocationItem = new StaffListAllocationItem();
                TripletKey<OrgUnitTypePostRelation, FinancingSource, FinancingSourceItem> key = new TripletKey<>(staffRateItem.getEmployeePost().getPostRelation(), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());
                newAllocationItem.setStaffListItem(staffListItemMap.get(key));
                newAllocationItem.setEmployeePost(staffRateItem.getEmployeePost());
                newAllocationItem.setFinancingSource(staffRateItem.getFinancingSource());
                newAllocationItem.setFinancingSourceItem(staffRateItem.getFinancingSourceItem());
                newAllocationItem.setRaisingCoefficient(staffRateItem.getEmployeePost().getRaisingCoefficient());
                if (newAllocationItem.getRaisingCoefficient() != null)
                    newAllocationItem.setMonthBaseSalaryFund(newAllocationItem.getRaisingCoefficient().getRecommendedSalary() * staffRateItem.getStaffRate());
                else
                    newAllocationItem.setMonthBaseSalaryFund(newAllocationItem.getStaffListItem().getSalary() * staffRateItem.getStaffRate());
                newAllocationItem.setStaffRate(staffRateItem.getStaffRate());

                session.save(newAllocationItem);
                StaffListPaymentsUtil.recalculateAllPaymentsValue(newAllocationItem, session);
            }
        }
    }

    /**
     * Производит копирование содержимого версии штатного расписания сотрудников, их ставок и обязательных выплат
     * @param staffList - новая версия штатного расписания
     * @param staffListToCopy - версия штатного расписания, из которой нужно скопировать содержимое
     * @param session - hibernate сессия
     * @deprecated use UniempDaoFacade.getStaffListDAO().copyStaffList()
     */
    @Deprecated
    public static void copyStaffList(StaffList staffList, StaffList staffListToCopy, boolean addNewRequiredPayments, boolean removeNonActivePayments, Session session)
    {
        UniempDaoFacade.getStaffListDAO().copyStaffList(staffList, staffListToCopy, addNewRequiredPayments, removeNonActivePayments);
    }
    
    /**
     * Копирует должность штатного распределения
     * @param session - hibernate сессия
     * @deprecated use UniempDaoFacade.getStaffListDAO().copyStaffListAllocationItem()
     */
    @Deprecated
    public static void copyStaffListAllocationItem(Long staffListAllocationItemId, Session session)
    {
        UniempDaoFacade.getStaffListDAO().copyStaffListAllocationItem(staffListAllocationItemId);
    }

    /**
     * Вычесляет id сотрудников, у которых доли ставки меньше чем суммарная ставка соответствующих должностей ШР или доли ставки их совмещения.<p/>
     * Т.е. ставка по ист. фин. в ШР превышает ссумарную ставку у сотрудника по должности на ист. фин., либо совмещения сотрудника на ист. фин.
     * @param allocationItemList список должностей штатной расстановки
     * @param session hibernate сессия
     * @return список id сотрудников с неккоректыми ставками в ШР
     */
    public static List<Long> getEmployeePostStaffRateInvalidIdList(final List<StaffListAllocationItem> allocationItemList, Session session)
    {
        Map<TripletKey<EmployeePost, FinancingSource, FinancingSourceItem>, Double> allocationItemMap = new HashMap<>();
        Map<TripletKey<CombinationPost, FinancingSource, FinancingSourceItem>, Double> combinationAllocationItemMap = new HashMap<>();
        Map<TripletKey<EmployeePost, FinancingSource, FinancingSourceItem>, Double> staffRateMap = new HashMap<>();
        Map<TripletKey<CombinationPost, FinancingSource, FinancingSourceItem>, Double> combinationStaffRateMap = new HashMap<>();
        List<Long> resultList = new ArrayList<>();

        List<Long> employeePostId = new ArrayList<>();
        for (StaffListAllocationItem item : allocationItemList)
            if (item.getEmployeePost() != null)
                employeePostId.add(item.getEmployeePost().getId());
        if (employeePostId.isEmpty())
            return resultList;

        List<EmployeePostStaffRateItem> staffRateList = new DQLSelectBuilder().fromEntity(EmployeePostStaffRateItem.class, "sr").column("sr")
                .where(in(property(EmployeePostStaffRateItem.employeePost().id().fromAlias("sr")), employeePostId))
                .createStatement(session).list();

        List<CombinationPostStaffRateItem> combinationStaffRateList = new DQLSelectBuilder().fromEntity(CombinationPostStaffRateItem.class, "sr").column("sr")
                .where(in(property(CombinationPostStaffRateItem.combinationPost().employeePost().id().fromAlias("sr")), employeePostId))
                .createStatement(session).list();

        for (CombinationPostStaffRateItem item : combinationStaffRateList)
        {
            TripletKey<CombinationPost, FinancingSource, FinancingSourceItem> key = new TripletKey<>(item.getCombinationPost(), item.getFinancingSource(), item.getFinancingSourceItem());
            Double staffRate = combinationStaffRateMap.get(key);
            if (staffRate == null)
                staffRate = 0d;
            staffRate += item.getStaffRate();
            combinationStaffRateMap.put(key, staffRate);
        }

        for (EmployeePostStaffRateItem item : staffRateList)
        {
            TripletKey<EmployeePost, FinancingSource, FinancingSourceItem> key = new TripletKey<>(item.getEmployeePost(), item.getFinancingSource(), item.getFinancingSourceItem());
            Double staffRate = staffRateMap.get(key);
            if (staffRate == null)
                staffRate = 0d;
            staffRate += item.getStaffRate();
            staffRateMap.put(key, staffRate);
        }

        for (StaffListAllocationItem item : allocationItemList)
        {
            if (item.getEmployeePost() == null)
                continue;

            if (item.isCombination() && item.getCombinationPost() != null)
            {
                TripletKey<CombinationPost, FinancingSource, FinancingSourceItem> key = new TripletKey<>(item.getCombinationPost(), item.getFinancingSource(), item.getFinancingSourceItem());

                Double staffRate = combinationAllocationItemMap.get(key);
                if (staffRate == null)
                    staffRate = 0d;
                staffRate += item.getStaffRate();
                combinationAllocationItemMap.put(key, staffRate);
            }
            else
            {
                TripletKey<EmployeePost, FinancingSource, FinancingSourceItem> key = new TripletKey<>(item.getEmployeePost(), item.getFinancingSource(), item.getFinancingSourceItem());

                Double staffRate = allocationItemMap.get(key);
                if (staffRate == null)
                    staffRate = 0d;
                staffRate += item.getStaffRate();
                allocationItemMap.put(key, staffRate);
            }
        }

        for (Map.Entry<TripletKey<EmployeePost, FinancingSource, FinancingSourceItem>, Double> entry : allocationItemMap.entrySet())
        {
            Double postStaffRate = staffRateMap.containsKey(entry.getKey()) ? new BigDecimal(staffRateMap.get(entry.getKey())).setScale(2, RoundingMode.HALF_UP).doubleValue() : null;
            Double allocStaffRate = new BigDecimal(entry.getValue()).setScale(2, RoundingMode.HALF_UP).doubleValue();

            if ((postStaffRate == null)|| postStaffRate.compareTo(allocStaffRate) < 0)
                resultList.add(entry.getKey().getFirst().getId());
        }

        for (Map.Entry<TripletKey<CombinationPost, FinancingSource, FinancingSourceItem>, Double> entry : combinationAllocationItemMap.entrySet())
        {
            Double combinationStaffRate = combinationStaffRateMap.containsKey(entry.getKey()) ? new BigDecimal(combinationStaffRateMap.get(entry.getKey())).setScale(2, RoundingMode.HALF_UP).doubleValue() : null;
            Double allocStaffRate = new BigDecimal(entry.getValue()).setScale(2, RoundingMode.HALF_UP).doubleValue();

            if ((combinationStaffRate == null)|| combinationStaffRate.compareTo(allocStaffRate) < 0)
                resultList.add(entry.getKey().getFirst().getId());
        }

        return resultList;
    }
    
}