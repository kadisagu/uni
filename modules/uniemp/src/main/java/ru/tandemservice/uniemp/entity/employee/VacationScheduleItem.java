package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.employee.gen.VacationScheduleItemGen;

/**
 * Строка графика отпусков
 */
public class VacationScheduleItem extends VacationScheduleItemGen
{
    public static final String P_EDIT_DISABLED = "editDisabled";
    public static final String P_COORDINATED_VACATION_SCHEDULE_EDIT_DISABLED = "coordinatedVacationEditDisabled";

    public boolean isEditDisabled()
    {
        return null != getVacationSchedule() && null != getVacationSchedule().getState() && !UniempDefines.STAFF_LIST_STATUS_FORMING.equals(getVacationSchedule().getState().getCode());
    }

    @EntityDSLSupport(parts = {VacationScheduleItem.P_PLAN_DATE, VacationScheduleItem.P_DAYS_AMOUNT})
    @Override
    public String getPlanedVacationTitle()
    {
        if (getPlanDate() != null)
            return "Планируемый отпуск " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getPlanDate()) + " на " + CommonBaseDateUtil.getDaysCountWithName(getDaysAmount());

        return "Планируемый отпуск на " + CommonBaseDateUtil.getDaysCountWithName(getDaysAmount());
    }
}