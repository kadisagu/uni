/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.EmployeeVPO1Settings;

import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 21.01.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(EmployeeVPO1ReportLines.ENTITY_CLASS, "rl");
        builder.addOrder("rl", EmployeeVPO1ReportLines.P_LINE_CODE);

        List<EmployeeVPO1ReportLines> resultList = builder.getResultList(getSession());
        model.getDataSource().setCountRow(resultList.size());

        UniBaseUtils.createPage(model.getDataSource(), resultList);
    }
}