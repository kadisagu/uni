/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.AdditionalAgreementFileAdd;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;

/**
 * @author dseleznev
 * Created on: 12.11.2008
 */
@Input(keys = "rowId", bindings = "agreementId")
public class Model
{
    private Long _agreementId;
    private ContractCollateralAgreement _agreement;
    private IUploadFile _file;

    public Long getAgreementId()
    {
        return _agreementId;
    }

    public void setAgreementId(Long agreementId)
    {
        this._agreementId = agreementId;
    }

    public ContractCollateralAgreement getAgreement()
    {
        return _agreement;
    }

    public void setAgreement(ContractCollateralAgreement agreement)
    {
        this._agreement = agreement;
    }

    public IUploadFile getFile()
    {
        return _file;
    }

    public void setFile(IUploadFile file)
    {
        this._file = file;
    }
    
    public boolean isAddForm()
    {
        return null == getAgreement().getAgreementFile();
    }

}