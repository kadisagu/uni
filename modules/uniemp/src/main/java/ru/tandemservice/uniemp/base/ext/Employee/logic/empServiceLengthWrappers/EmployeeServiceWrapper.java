/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers;

import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;

import java.util.ArrayList;
import java.util.List;

/**
 * Враппер строки Списка Итоговых данных о Стаже Кадрового ресурса.
 * Хранит Типа стажа и Периоды, в которые КР работал по указанному типу стажа.
 *
 * @author Alexander Shaburov
 * @since 21.09.12
 */
public class EmployeeServiceWrapper
{
    // fields

    private ServiceLengthType _serviceLengthType;
    private List<PeriodWrapper> _periodList = new ArrayList<>();

    // constructor

    /**
     * @param serviceLengthType Вид стажа, not null
     * @throws NullPointerException if serviceLengthType is null
     */
    public EmployeeServiceWrapper(ServiceLengthType serviceLengthType)
    {
        if (serviceLengthType == null)
            throw new NullPointerException();

        _serviceLengthType = serviceLengthType;
    }

    // logic

    /**
     * Сливает пересекающиеся периоды в один.<p/>
     * <b>Note:</b></p/>
     * Если периоды идут друг за другом, то считается, что они пересекаются.<p/>
     * дата начала - минимальная из дат начал двух слитых(пересекающихся) периодов<p/>
     * дата окончания - максимальная из дат окончаний двух слитых(пересекающихся) периодов, null считается больше not null
     */
    public void combinationPeriods()
    {
        if (_periodList.size() < 2)
            return;

        boolean stop = false;

        for (int i = 0; i < _periodList.size() - 1; i++)
        {
            if (stop)
                break;

            PeriodWrapper wrapper = _periodList.get(i);

            for (int j = i + 1; j < _periodList.size(); j++)
            {
                if (stop)
                    break;

                PeriodWrapper innerWrapper = _periodList.get(j);

                if (wrapper.intersectTo(innerWrapper))
                {
                    stop = true;

                    _periodList.add(i, new PeriodWrapper(PeriodWrapper.min(wrapper.getBeginDate(), innerWrapper.getBeginDate()), PeriodWrapper.max(wrapper.getEndDate(), innerWrapper.getEndDate())));

                    _periodList.remove(wrapper);
                    _periodList.remove(innerWrapper);
                }
            }
        }

        if (stop)
            combinationPeriods();
    }

    // Getters & Setters

    public ServiceLengthType getServiceLengthType()
    {
        return _serviceLengthType;
    }

    public List<PeriodWrapper> getPeriodList()
    {
        return _periodList;
    }
}
