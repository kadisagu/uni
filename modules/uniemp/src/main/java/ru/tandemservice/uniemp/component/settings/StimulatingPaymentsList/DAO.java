/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.StimulatingPaymentsList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.catalog.RosobrReports;
import ru.tandemservice.uniemp.entity.report.RosobrReportStimPaymentPriority;
import ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel;

/**
 * @author dseleznev
 * Created on: 01.02.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        preparePaymentsList(model);
        model.setRosobrReports(getList(RosobrReports.class, RosobrReports.P_NUMBER));
        prepareMapsAndLists(model);
    }
    
    private void preparePaymentsList(Model model)
    {
        Map<Payment, RosobrReportStimPaymentPriority> priorMap = new HashMap<>();
        MQBuilder relsBuilder = new MQBuilder(RosobrReportStimPaymentPriority.ENTITY_CLASS, "rel");
        relsBuilder.addOrder("rel", RosobrReportStimPaymentPriority.P_PRIORITY);
        
        List<Payment> paymentsList = new ArrayList<>();
        for(RosobrReportStimPaymentPriority priority : relsBuilder.<RosobrReportStimPaymentPriority>getResultList(getSession()))
        {
            paymentsList.add(priority.getPayment());
            priorMap.put(priority.getPayment(), priority);
        }
        model.setPrioritiesMap(priorMap);
        
        MQBuilder builder = new MQBuilder(Payment.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE));
        builder.addOrder("p", Payment.P_TITLE);
        
        for (Payment payment : builder.<Payment> getResultList(getSession()))
        {
            if (!paymentsList.contains(payment))
            {
                PaymentType rootParentType = payment.getType();
                while (null != rootParentType.getParent()) rootParentType = rootParentType.getParent();
                if (UniempDefines.PAYMENT_TYPE_STLIMULATION.equals(rootParentType.getCode()))
                    paymentsList.add(payment);
            }
        }
        model.setCompensationPayments(paymentsList);
    }

    @SuppressWarnings("unchecked")
    private void prepareMapsAndLists(Model model)
    {
        Map<RosobrReports, List<IEntity>> selectedIncludeRepPaymentsMap = new HashMap<>();
        Map<RosobrReports, List<IEntity>> selectedSeparateRepPaymentsMap = new HashMap<>();
        Map<Payment, Map<RosobrReports, RosobrReportToPaymentRel>> settingsMap = new HashMap<>();
        
        for(RosobrReportToPaymentRel rel : (List<RosobrReportToPaymentRel>)getSession().createCriteria(RosobrReportToPaymentRel.class).list())
        {
            List<IEntity> listInclude = selectedIncludeRepPaymentsMap.get(rel.getFirst());
            if(null == listInclude) listInclude = new ArrayList<>();
            if(rel.isIncludePayment()) listInclude.add(rel.getSecond());
            selectedIncludeRepPaymentsMap.put(rel.getFirst(), listInclude);
            
            List<IEntity> listSeparate = selectedSeparateRepPaymentsMap.get(rel.getFirst());
            if(null == listSeparate) listSeparate = new ArrayList<>();
            if(rel.isSeparateColumn()) listSeparate.add(rel.getSecond());
            selectedSeparateRepPaymentsMap.put(rel.getFirst(), listSeparate);
            
            Map<RosobrReports, RosobrReportToPaymentRel> map = settingsMap.get(rel.getSecond());
            if(null == map) map = new  HashMap<>();
            map.put(rel.getFirst(), rel);
            settingsMap.put(rel.getSecond(), map);
        }
        
        model.setSettingsMap(settingsMap);
        model.setSelectedIncludeRepPaymentsMap(selectedIncludeRepPaymentsMap);
        model.setSelectedSeparateRepPaymentsMap(selectedSeparateRepPaymentsMap);
        
        for(RosobrReports rep : model.getRosobrReports())
        {
            if(!selectedIncludeRepPaymentsMap.containsKey(rep)) selectedIncludeRepPaymentsMap.put(rep, new ArrayList<IEntity>());
            if(!selectedSeparateRepPaymentsMap.containsKey(rep)) selectedSeparateRepPaymentsMap.put(rep, new ArrayList<IEntity>());
        }
        
        List<IEntity> selectedScDegreePaymentsList = new ArrayList<>();
        
        for(RosobrReportStimPaymentPriority priority : (List<RosobrReportStimPaymentPriority>)getSession().createCriteria(RosobrReportStimPaymentPriority.class).list())
        {
            if(priority.isScienceDegreePayment()) selectedScDegreePaymentsList.add(priority.getPayment());
        }
        
        model.setSelectedScDegreePaymentsList(selectedScDegreePaymentsList);
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getDataSource(), model.getCompensationPayments());
        ((CheckboxColumn)model.getDataSource().getColumn("scDegreeColumn")).setSelectedObjects(model.getSelectedScDegreePaymentsList());
    }

    @Override
    public void update(Model model)
    {
        updateEnsureWrappersWasCreated(model);

        for(RosobrReports report : model.getRosobrReports())
        {
            Collection<IEntity> selectedIncludePayments = (Collection<IEntity>)((CheckboxColumn)model.getDataSource().getColumn("include" + report.getCode())).getSelectedObjects();
            Collection<IEntity> selectedSeparatePayments = (Collection<IEntity>)((CheckboxColumn)model.getDataSource().getColumn("separateColumn" + report.getCode())).getSelectedObjects();
            
            for(Payment payment : model.getCompensationPayments())
            {
                if(null == model.getSettingsMap().get(payment)) model.getSettingsMap().put(payment, new HashMap<RosobrReports, RosobrReportToPaymentRel>());
                RosobrReportToPaymentRel rel = model.getSettingsMap().get(payment).get(report);

                if(null == rel)
                {
                    rel = new RosobrReportToPaymentRel();
                    rel.setFirst(report);
                    rel.setSecond(payment);
                }
                rel.setIncludePayment(selectedIncludePayments.contains(payment));
                rel.setSeparateColumn(selectedSeparatePayments.contains(payment));
                model.getSettingsMap().get(payment).put(report, rel);
                getSession().saveOrUpdate(rel);
            }
        }

        Collection<IEntity> selectedScDegreePayments = (Collection<IEntity>)((CheckboxColumn)model.getDataSource().getColumn("scDegreeColumn")).getSelectedObjects();

        for (Payment payment : model.getDataSource().getEntityList())
        {
            RosobrReportStimPaymentPriority priority = model.getPrioritiesMap().get(payment);
            if (null != priority)
            {
                priority.setScienceDegreePayment(selectedScDegreePayments.contains(priority.getPayment()));
                getSession().saveOrUpdate(priority);
            }
        }

        prepareMapsAndLists(model);
    }
    
    @Override
    public void updatePriority(Model model, Long paymentId, boolean up)
    {
        updateEnsureWrappersWasCreated(model);
        
        Payment payment = get(Payment.class, paymentId);
        int idx = model.getCompensationPayments().indexOf(payment);
        
        if((idx == 0 && up) || (idx == model.getCompensationPayments().size() - 1 && !up)) return;
        
        Payment paymntToMove = model.getCompensationPayments().get(idx + (up ? -1 : 1));
        Collections.swap(model.getCompensationPayments(), idx, idx + (up ? -1 : 1));
        
        RosobrReportStimPaymentPriority priority = model.getPrioritiesMap().get(payment);
        RosobrReportStimPaymentPriority priorityToMove = model.getPrioritiesMap().get(paymntToMove);
        
        int prior1 = priority.getPriority() + (up ? -1 : 1);
        int prior2 = priorityToMove.getPriority() + (up ? 1 : -1);
        
        priority.setPriority(-1);
        priorityToMove.setPriority(-2);
        
        update(priority);
        update(priorityToMove);
        
        getSession().flush();
        
        priority.setPriority(prior1);
        priorityToMove.setPriority(prior2);
        
        update(priority);
        update(priorityToMove);
    }
    
    private void updateEnsureWrappersWasCreated(Model model)
    {
        List<RosobrReportStimPaymentPriority> priorityList = new ArrayList<>();
        
        for(Payment payment : model.getCompensationPayments())
        {
            if(!model.getPrioritiesMap().containsKey(payment))
            {
                RosobrReportStimPaymentPriority priority = new RosobrReportStimPaymentPriority(payment);
                model.getPrioritiesMap().put(payment, priority);
                priorityList.add(priority);
            }
            else
                priorityList.add(model.getPrioritiesMap().get(payment));
        }
        
        int i = 0;
        for(RosobrReportStimPaymentPriority priority : priorityList)
        {
            priority.setPriority(i++);
            getSession().saveOrUpdate(priority);
        }
    }
}