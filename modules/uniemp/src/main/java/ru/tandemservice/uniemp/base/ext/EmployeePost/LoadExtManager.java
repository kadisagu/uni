/*$Id$*/
package ru.tandemservice.uniemp.base.ext.EmployeePost;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.IEmployeeAdditionalInfo;
import ru.tandemservice.uniemp.base.ext.EmployeePost.logic.UniempEmployeeAdditionalInfo;

/**
 * @author DMITRY KNYAZEV
 * @since 31.07.2014
 */
@Configuration
public class LoadExtManager extends BusinessObjectExtensionManager
{
	@Bean
	@BeanOverride
	public IEmployeeAdditionalInfo employeeInfo()
	{
		return new UniempEmployeeAdditionalInfo();
	}
}
