/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.logic;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.tandemframework.core.CoreCollectionUtils;

/**
 * @author dseleznev
 * Created on: 11.01.2009
 */
public class EmploymentCalculator
{

    private static final double MILIS_IN_DAY = 86400000d;

    private Calendar _calendar = Calendar.getInstance();

    private Date _currentDate = new Date();

    private static Comparator<CoreCollectionUtils.Pair<Date, Date>> PERIODS_COMPARATOR = new Comparator<CoreCollectionUtils.Pair<Date, Date>>()
    {
        @Override
        public int compare(CoreCollectionUtils.Pair<Date, Date> o1, CoreCollectionUtils.Pair<Date, Date> o2)
        {
            return o1.getX().compareTo(o2.getX()) == 0 ? o1.getY().compareTo(o2.getY()) : o1.getX().compareTo(o2.getX());
        }
    };

    private Integer _fullDaysAmount = 0;

    private Integer _yearsAmount = 0;

    private Integer _monthsAmount = 0;

    private Integer _daysAmount = 0;

    /**
     * Calculates amount of years, months and days amounts for given dates period.
     * @param startDate - begin of calculating period or employee assigning date to the post.
     * @param endDate - end of calculating period or employee dismissal date.
     */
    public void calculateEmploymentPeriod(Date startDate, Date endDate)
    {
        int daysCnt = 0;
        int monthsCnt = 0;
        int yearsCnt = 0;

        _calendar.setTime(null != endDate ? endDate : _currentDate);
        _calendar.add(Calendar.DAY_OF_YEAR, 1); //Last day at work should be taken into account too
        setCalendarToDayBegin();
        long secondDate = _calendar.getTimeInMillis();
        _calendar.setTime(startDate);
        setCalendarToDayBegin();
        long firstDate = _calendar.getTimeInMillis();

        _calendar.add(Calendar.MONTH, 1);
        long nextMonthBegin = _calendar.getTimeInMillis();

        _calendar.add(Calendar.MONTH, -1);
        _calendar.add(Calendar.YEAR, 1);
        long nextYearBegin = _calendar.getTimeInMillis();

        // Iterate over the given dates period day by day, in order to take into account leap years
        while (firstDate < secondDate)
        {
            daysCnt++;
            _calendar.setTimeInMillis(firstDate);
            _calendar.add(Calendar.DAY_OF_YEAR, 1);
            firstDate = _calendar.getTimeInMillis();

            // If whole month passed since assign date, then increment month counter
            if (firstDate >= nextMonthBegin)
            {
                monthsCnt++;
                _calendar.add(Calendar.MONTH, 1);
                nextMonthBegin = _calendar.getTimeInMillis();
                _calendar.add(Calendar.MONTH, -1);
            }

            // If whole year passed since assign date, then increment year counter  
            if (firstDate >= nextYearBegin)
            {
                yearsCnt++;
                _calendar.add(Calendar.YEAR, 1);
                nextYearBegin = _calendar.getTimeInMillis();
            }
        }

        _calendar.setTimeInMillis(nextMonthBegin);
        _calendar.add(Calendar.MONTH, -1);

        _yearsAmount = yearsCnt;
        _monthsAmount = (int)(monthsCnt % 12);
        _daysAmount = (int)Math.round((secondDate - _calendar.getTimeInMillis()) / MILIS_IN_DAY);
        _fullDaysAmount = daysCnt;
    }

    /**
     * Transforms any date periods set into non-crossing date periods ordered by start time.<br/>
     * If any of periods intersects, then resulting period will be combined. 
     * @param periods - list of periods, presented as date pairs. Periods can intersect with the others. 
     * @return Processed date periods list ordered by start time.
     */
    private List<CoreCollectionUtils.Pair<Date, Date>> processPeriods(List<CoreCollectionUtils.Pair<Date, Date>> periods)
    {
        if (periods.isEmpty()) return new ArrayList<>();

        for(CoreCollectionUtils.Pair<Date, Date> period : periods)
        {
            if(null == period.getY() || period.getY().compareTo(_currentDate) > 0)
            {
                _calendar.setTime(_currentDate);
                setCalendarToDayBegin();
                period.setY(_calendar.getTime());
            }
        }

        Collections.sort(periods, PERIODS_COMPARATOR);
        List<CoreCollectionUtils.Pair<Date, Date>> resultPeriods = new ArrayList<>();

        while (!periods.isEmpty())
        {
            long startDate = periods.get(0).getX().getTime();
            long endDate = periods.get(0).getY().getTime();

            // Just the list of periods, which already taken into account and should be removed
            List<CoreCollectionUtils.Pair<Date, Date>> periodsToDel = new ArrayList<>();
            periodsToDel.add(periods.get(0));

            for (CoreCollectionUtils.Pair<Date, Date> period : periods)
            {
                if (!periodsToDel.contains(period))
                {
                    // If end period is covered by the generated period and it's start is less than generated start, then we should widen generated period start
                    if (period.getX().getTime() >= startDate && period.getX().getTime() <= endDate && period.getY().getTime() > endDate)
                    {
                        endDate = period.getY().getTime();
                        periodsToDel.add(period);
                    }
                    // If start of period is covered by the generated period and it's end is exceed the end of generated period, then we should increase it
                    else if (period.getY().getTime() >= startDate && period.getY().getTime() <= endDate && period.getX().getTime() < startDate)
                    {
                        startDate = period.getX().getTime();
                        periodsToDel.add(period);
                    }
                    // If current period is completely covered by the generated period, we can just omit it without loss
                    else if (period.getX().getTime() >= startDate && period.getY().getTime() <= endDate)
                        periodsToDel.add(period);
                }
            }

            // This periods already are taken into account, so they should be removed
            periods.removeAll(periodsToDel);
            resultPeriods.add(new CoreCollectionUtils.Pair<>(new Date(startDate), new Date(endDate)));
        }

        return resultPeriods;
    }

    /**
     * Calculates total deployment duration for given date periods set.
     * @param periods - list of periods, presented as date pairs. Periods can intersect with the others.
     * @return nothing. Any calculated parameter can be accessed by it's getter. 
     * <br/><br/>
     * They are:<br/> 
     * fullDaysAmount - total days amount for employment period.<br/>
     * yearsAmount - total years amount for employment period.<br/>
     * monthsAmount - rest month amount for employment period.<br/>
     * daysAmount - rest days amount for employment period.
     */
    public void calculateFullEmploymentPeriod(List<CoreCollectionUtils.Pair<Date, Date>> periods)
    {
        // Transforming periods before calculating
        periods = processPeriods(periods);

        int fullDaysAmount = 0;
        int yearsAmount = 0;
        int monthsAmount = 0;
        int daysAmount = 0;

        // Iterate over the list of employment periods
        for (CoreCollectionUtils.Pair<Date, Date> period : periods)
        {
            // Calculating of single period days, years, months amounts
            calculateEmploymentPeriod(period.getX(), period.getY());
            // and add them to total amounts variables 
            fullDaysAmount += getFullDaysAmount();
            yearsAmount += getYearsAmount();
            monthsAmount += getMonthsAmount();
            daysAmount += getDaysAmount();
        }

        // Correct total days and months amounts according to 12 months year and 30 days per month system 
        monthsAmount += daysAmount / 30;
        daysAmount = daysAmount % 30;
        yearsAmount += monthsAmount / 12;
        monthsAmount = monthsAmount % 12;

        // Assign total amounts to the accessible external variables
        _fullDaysAmount = fullDaysAmount;
        _yearsAmount = yearsAmount;
        _monthsAmount = monthsAmount;
        _daysAmount = daysAmount;
    }

    private void setCalendarToDayBegin()
    {
        _calendar.set(Calendar.HOUR_OF_DAY, 0);
        _calendar.set(Calendar.MINUTE, 0);
        _calendar.set(Calendar.SECOND, 0);
        _calendar.set(Calendar.MILLISECOND, 0);
    }

    public Integer getFullDaysAmount()
    {
        return _fullDaysAmount;
    }

    public Integer getYearsAmount()
    {
        return _yearsAmount;
    }

    public Integer getMonthsAmount()
    {
        return _monthsAmount;
    }

    public Integer getDaysAmount()
    {
        return _daysAmount;
    }

    public Date getCurrentDate()
    {
        return _currentDate;
    }

    public void setCurrentDate(Date currentDate)
    {
        _currentDate = currentDate;
    }
}
