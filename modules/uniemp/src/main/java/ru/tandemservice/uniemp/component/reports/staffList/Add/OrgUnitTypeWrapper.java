/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffList.Add;

import java.io.IOException;
import java.io.Serializable;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;

import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * @author dseleznev
 * Created on: 06.08.2009
 */
public class OrgUnitTypeWrapper extends IdentifiableWrapper<OrgUnitType> implements IEntity, Serializable
{
    private static final long serialVersionUID = 5638933254247804299L;
    
    public static final String P_TITLE = "title";
    public static final String P_USE_ORG_UNIT = "useOrgUnit";
    public static final String P_SHOW_CHILD_ORG_UNITS = "showChildOrgUnits";
    public static final String P_CALCULATE_CHILD_ORG_UNIT_POSTS_AS_OWN = "showChildOrgUnitPostsAsOwn";

    private boolean _useOrgUnit;
    private OrgUnitType _orgUnitType;
    private boolean _showChildOrgUnits;
    private boolean _showChildOrgUnitPostsAsOwn;
    
    public OrgUnitTypeWrapper(OrgUnitType orgUnitType, boolean useOrgUnit, boolean showChildOrgUnits, boolean showChildOrgUnitPostsAsOwn)
    {
        super(orgUnitType.getId(), orgUnitType.getTitle());
        setUseOrgUnit(useOrgUnit);
        setOrgUnitType(orgUnitType);
        setShowChildOrgUnits(showChildOrgUnits);
        setShowChildOrgUnitPostsAsOwn(showChildOrgUnitPostsAsOwn);
    }

    private void writeObject(java.io.ObjectOutputStream out) throws IOException
    {
        out.writeBoolean(_useOrgUnit);
        out.writeBoolean(_showChildOrgUnits);
        out.writeBoolean(_showChildOrgUnitPostsAsOwn);
        out.writeLong(_orgUnitType.getId());
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException
    {
        _useOrgUnit = in.readBoolean();
        _showChildOrgUnits = in.readBoolean();
        _showChildOrgUnitPostsAsOwn = in.readBoolean();
        _orgUnitType = UniDaoFacade.getCoreDao().get(OrgUnitType.class, in.readLong());
    }

    public boolean isUseOrgUnit()
    {
        return _useOrgUnit;
    }

    public void setUseOrgUnit(boolean useOrgUnit)
    {
        this._useOrgUnit = useOrgUnit;
    }

    public OrgUnitType getOrgUnitType()
    {
        return _orgUnitType;
    }

    public void setOrgUnitType(OrgUnitType orgUnitType)
    {
        this._orgUnitType = orgUnitType;
    }

    public boolean isShowChildOrgUnits()
    {
        return _showChildOrgUnits;
    }

    public void setShowChildOrgUnits(boolean showChildOrgUnits)
    {
        this._showChildOrgUnits = showChildOrgUnits;
    }

    public boolean isShowChildOrgUnitPostsAsOwn()
    {
        return _showChildOrgUnitPostsAsOwn;
    }

    public void setShowChildOrgUnitPostsAsOwn(boolean showChildOrgUnitPostsAsOwn)
    {
        this._showChildOrgUnitPostsAsOwn = showChildOrgUnitPostsAsOwn;
    }
}