package ru.tandemservice.uniemp.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * 
 * @author nkokorina
 * Created on: 03.02.2010
 */

public class UniEmployeeOrgUnitPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniemp");
        config.setName("uni-emp-sec-config");
        config.setTitle("");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            PermissionGroupMeta permissionGroupEmployeeTab = PermissionMetaUtil.createPermissionGroup(config, code + "EmployeeTabAllPermissionGroup", "Вкладка «Кадры»");

            PermissionGroupMeta permissionGroupPost = PermissionMetaUtil.createPermissionGroup(permissionGroupEmployeeTab, "orgUnit_viewPostList_" + code, "Вкладка «Должности»");
            PermissionMetaUtil.createPermission(permissionGroupPost, "orgUnit_viewPostList_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupPost, "orgUnit_viewPost_" + code, "Просмотр должностей на подразделении");
            PermissionMetaUtil.createPermission(permissionGroupPost, "orgUnit_addPost_" + code, "Добавление должности на подразделении");
            PermissionMetaUtil.createPermission(permissionGroupPost, "orgUnit_editPost_" + code, "Редактирование должности на подразделении");
            PermissionMetaUtil.createPermission(permissionGroupPost, "orgUnit_deletePost_" + code, "Удаление должностей на подразделении");

            PermissionGroupMeta permissionGroupStaff = PermissionMetaUtil.createPermissionGroup(permissionGroupEmployeeTab, "orgUnit_viewStaffList_" + code, "Вкладка «Штатное расписание»");
            PermissionMetaUtil.createPermission(permissionGroupStaff, "orgUnit_viewStaffList_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupStaff, "orgUnit_addStaffList_" + code, "Добавление версии штатного расписания");
            PermissionMetaUtil.createPermission(permissionGroupStaff, "orgUnit_editStaffList_" + code, "Редактирование версии штатного расписания");
            PermissionMetaUtil.createPermission(permissionGroupStaff, "orgUnit_deleteStaffList_" + code, "Удаление версий штатного расписания");

            PermissionGroupMeta permissionGroupVacationSchedule = PermissionMetaUtil.createPermissionGroup(permissionGroupEmployeeTab, "orgUnit_viewVacationSchedulesList_" + code, "Вкладка «Графики отпусков»");
            PermissionMetaUtil.createPermission(permissionGroupVacationSchedule, "orgUnit_viewVacationSchedulesList_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupVacationSchedule, "orgUnit_addVacationSchedule_" + code, "Добавление графика отпусков");
            PermissionMetaUtil.createPermission(permissionGroupVacationSchedule, "orgUnit_printVacationSchedule_" + code, "Печать графика отпусков");
            PermissionMetaUtil.createPermission(permissionGroupVacationSchedule, "orgUnit_editVacationSchedule_" + code, "Редактирование графика отпусков");
            PermissionMetaUtil.createPermission(permissionGroupVacationSchedule, "orgUnit_deleteVacationSchedule_" + code, "Удаление графиков отпусков");

            securityConfigMetaMap.put(config.getName(), config);
        }
    }
}