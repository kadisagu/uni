/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.employeeMovePPS.Add;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.ScienceDegreeType;
import org.tandemframework.shared.person.catalog.entity.ScienceStatusType;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.IUniempDAO;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.util.UniempReportUtil;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 17.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final int DOCTOR_PROFESSOR = 1;
    private static final int DOCTOR_DOCENT = 2;
    private static final int DOCTOR_WO_SC_STATUS = 3;
    private static final int PROFESSOR_WO_DEGREE = 4;
    private static final int MASTER_PROFESSOR = 5;
    private static final int MASTER_DOCENT = 6;
    private static final int MASTER_WO_SC_STATUS = 7;
    private static final int DOCENT_WO_DEGREE = 8;
    private static final int SIMPLE_PPS = 9;
    private static final int TOTAL_MAIN_JOB = 10;
    private static final int TOTAL = 11;
    private static final int TOTAL_WOMAN = 12;

    private static final String[] TITLES = new String[] { "Дн Пр", "Дн Дц", "Дн без уч.зв.", "Кн Пр", "Пр без уч.ст.", "Кн Дц", "Кн без уч.зв.", "Дц без уч.ст.", "Спец.без уч.ст.,уч.зв.", "ВСЕГО, без совм.:", "ВСЕГО, с совм.:", "В т.ч. женщин" };

    private static final Map<CoreCollectionUtils.Pair<String, String>, Integer> PPS_TYPES_MAP = new HashMap<>();
    static
    {
        PPS_TYPES_MAP.put(new CoreCollectionUtils.Pair<>(UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR, UniDefines.SCIENCE_STATUS_TYPE_PROFESSOR), DOCTOR_PROFESSOR);
        PPS_TYPES_MAP.put(new CoreCollectionUtils.Pair<>(UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR, UniDefines.SCIENCE_STATUS_TYPE_DOCENT), DOCTOR_DOCENT);
        PPS_TYPES_MAP.put(new CoreCollectionUtils.Pair<>(UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR, "-"), DOCTOR_WO_SC_STATUS);
        PPS_TYPES_MAP.put(new CoreCollectionUtils.Pair<>("-", UniDefines.SCIENCE_STATUS_TYPE_PROFESSOR), PROFESSOR_WO_DEGREE);
        PPS_TYPES_MAP.put(new CoreCollectionUtils.Pair<>(UniDefines.SCIENCE_DEGREE_TYPE_MASTER, UniDefines.SCIENCE_STATUS_TYPE_PROFESSOR), MASTER_PROFESSOR);
        PPS_TYPES_MAP.put(new CoreCollectionUtils.Pair<>(UniDefines.SCIENCE_DEGREE_TYPE_MASTER, UniDefines.SCIENCE_STATUS_TYPE_DOCENT), MASTER_DOCENT);
        PPS_TYPES_MAP.put(new CoreCollectionUtils.Pair<>(UniDefines.SCIENCE_DEGREE_TYPE_MASTER, "-"), MASTER_WO_SC_STATUS);
        PPS_TYPES_MAP.put(new CoreCollectionUtils.Pair<>("-", UniDefines.SCIENCE_STATUS_TYPE_DOCENT), DOCENT_WO_DEGREE);
        PPS_TYPES_MAP.put(new CoreCollectionUtils.Pair<>("-", "-"), SIMPLE_PPS);
    }

    @Override
    public Integer preparePrintReport(Model model)
    {
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("academyTitle", TopOrgUnit.getInstance().getTitle());
        paramModifier.put("repDtFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDateFrom()));
        paramModifier.put("repDtTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDateTo()));

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.getName2data().putAll(prepareTablesData(model.getReportDateFrom(), model.getReportDateTo(), paramModifier));
        return UniempReportUtil.preparePrintingReport(getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_EMPLOYEE_MOVE_PPS), paramModifier, tableModifier);
    }

    private Map<String, String[][]> prepareTablesData(Date reportDateFrom, Date reportDateTo, RtfInjectModifier paramModifier)
    {
        Map<String, String[][]> result = new HashMap<>();

        List<String[]> lines = new ArrayList<>();
        Map<Integer, Integer[]> statMap = new HashMap<>();

        List<EmployeePost> postsList = getEmployeePostsList();
        Map<Person, ScienceDegreeType> scienceDegreesMap = IUniempDAO.instance.get().getEmployeeDegreesMap(reportDateTo);
        Map<Person, ScienceStatusType> scienceStatusesMap = IUniempDAO.instance.get().getEmployeeStatusesMap(reportDateTo);
        statMap.put(TOTAL_MAIN_JOB, new Integer[] { 0, 0, 0, null, 0, 0, 0 });
        statMap.put(TOTAL_WOMAN, new Integer[] { 0, 0, 0, null, 0, 0, 0 });
        statMap.put(TOTAL, new Integer[] { 0, 0, 0, null, 0, 0, 0 });

        CoreCollectionUtils.Pair<Integer, Integer> holidays = getHolidaysCount(postsList, reportDateFrom, reportDateTo);
        paramModifier.put("stillInHoliday", holidays.getX().toString());
        paramModifier.put("returnedFromHoliday", holidays.getY().toString());

        for (EmployeePost post : postsList)
        {
            if(null == post.getPostDate()) continue;

            Person person = post.getEmployee().getPerson();
            ScienceDegreeType scienceDegreeType = scienceDegreesMap.get(person);
            String scDegreeCode = null != scienceDegreeType ? scienceDegreeType.getCode() : "-";
            ScienceStatusType scienceStatusType = scienceStatusesMap.get(person);
            String scStatusCode = null != scienceStatusType ? scienceStatusType.getCode() : "-";
            CoreCollectionUtils.Pair<String, String> pair = new CoreCollectionUtils.Pair<>(scDegreeCode, scStatusCode);
            if (PPS_TYPES_MAP.containsKey(pair))
            {
                Integer rowIndex = PPS_TYPES_MAP.get(pair);
                Integer[] statLine = statMap.get(rowIndex);
                if (null == statLine)
                {
                    statLine = new Integer[] { 0, 0, 0, null, 0, 0, 0 };
                }

                Integer[] totalMainJob = statMap.get(TOTAL_MAIN_JOB);
                Integer[] totalWoman = statMap.get(TOTAL_WOMAN);
                Integer[] total = statMap.get(TOTAL);

                if ((post.getPostDate().getTime() < reportDateFrom.getTime()) && ((null == post.getDismissalDate()) || (post.getDismissalDate().getTime() > reportDateFrom.getTime())))
                {
                    putValuesIntoAllTheCounters(post, 0, statLine, totalMainJob, totalWoman, total);
                }

                if ((post.getPostDate().getTime() >= reportDateFrom.getTime()) && (post.getPostDate().getTime() < reportDateTo.getTime()))
                {
                    putValuesIntoAllTheCounters(post, 1, statLine, totalMainJob, totalWoman, total);
                    if (UniDefines.POST_TYPE_SECOND_JOB.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode()))
                    {
                        putValuesIntoAllTheCounters(post, 2, statLine, totalMainJob, totalWoman, total);
                    }
                }
                if ((null != post.getDismissalDate()) && (post.getDismissalDate().getTime() > reportDateFrom.getTime()) && (post.getDismissalDate().getTime() < reportDateTo.getTime()))
                {
                    putValuesIntoAllTheCounters(post, 4, statLine, totalMainJob, totalWoman, total);
                    if (UniDefines.POST_TYPE_SECOND_JOB.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode()))
                    {
                        putValuesIntoAllTheCounters(post, 5, statLine, totalMainJob, totalWoman, total);
                    }
                }

                if ((post.getPostDate().getTime() < reportDateTo.getTime()) && ((null == post.getDismissalDate()) || (post.getDismissalDate().getTime() > reportDateTo.getTime())))
                {
                    putValuesIntoAllTheCounters(post, 6, statLine, totalMainJob, totalWoman, total);
                }

                statMap.put(rowIndex, statLine);
            }
        }

        for (int i = DOCTOR_PROFESSOR; i <= TOTAL_WOMAN; i++)
        {
            String[] line = new String[8];
            line[0] = TITLES[i - 1];

            Integer[] valLine = statMap.get(i);
            if (null == valLine)
            {
                valLine = new Integer[] { 0, 0, 0, null, 0, 0, 0 };
            }

            for (int j = 0; j < valLine.length; j++)
            {
                line[j + 1] = null != valLine[j] ? (valLine[j] != 0 ? String.valueOf(valLine[j]) : "") : "";
            }
            lines.add(line);
        }

        result.put("T", lines.toArray(new String[][] {}));

        return result;
    }

    private void putValuesIntoAllTheCounters(EmployeePost post, int index, Integer[] curLine, Integer[] totalMainJobLine, Integer[] totalWomanLine, Integer[] totalLine)
    {
        curLine[index]++;
        totalLine[index]++;
        if (!(UniDefines.POST_TYPE_SECOND_JOB.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(post.getPostType().getCode()) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(post.getPostType().getCode())))
        {
            totalMainJobLine[index]++;
        }
        if (SexCodes.FEMALE.equals(post.getPerson().getIdentityCard().getSex().getCode()))
        {
            totalWomanLine[index]++;
        }
    }

    /**
     * Возвращает пару значений
     * "в отпуске по уходу за ребенком", "Приступили к работе после отпуска по уходу за ребенком"
     */
    private CoreCollectionUtils.Pair<Integer, Integer> getHolidaysCount(List<EmployeePost> postsList, Date reportDateFrom, Date reportDateTo)
    {
        MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "h");
        //builder.add(MQExpression.in("h", EmployeeHoliday.L_EMPLOYEE_POST, postsList));
        AbstractExpression expr1 = MQExpression.eq("h", EmployeeHoliday.holidayType().code().s(), UniempDefines.HOLIDAY_TYPE_WOMAN_ADOPTED_A_CHILD);
        AbstractExpression expr2 = MQExpression.eq("h", EmployeeHoliday.holidayType().code().s(), UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_1_5);
        builder.add(MQExpression.or(expr1, expr2));
        List<EmployeeHoliday> holidaysList = builder.getResultList(getSession());

        Integer stillInHoliday = 0;
        Integer outFromHoliday = 0;
        Calendar cal = Calendar.getInstance();
        for (EmployeeHoliday holiday : holidaysList)
        {
            if ((null != holiday.getStartDate()) && (holiday.getDuration() > 0) && postsList.contains(holiday.getEmployeePost()))
            {
                Date startDate = holiday.getStartDate();
                cal.setTime(startDate);
                cal.add(Calendar.DAY_OF_YEAR, holiday.getDuration());
                Date endDate = cal.getTime();

                if ((endDate.getTime() >= reportDateFrom.getTime()) && (startDate.getTime() <= reportDateTo.getTime()))
                {
                    if ((endDate.getTime() >= reportDateFrom.getTime()) && (endDate.getTime() <= reportDateTo.getTime()))
                    {
                        outFromHoliday++;
                    }
                    else
                    {
                        stillInHoliday++;
                    }
                }

                /*
                if (((startDate.getTime() >= reportDateFrom.getTime()) && (startDate.getTime() <= reportDateTo.getTime())) || ((endDate.getTime() >= reportDateFrom.getTime()) && (endDate.getTime() <= reportDateTo.getTime())))
                {
                    if (endDate.getTime() >= reportDateTo.getTime())
                    {
                        stillInHoliday++;
                    }
                    else
                    {
                        outFromHoliday++;
                    }
                }*/
            }
        }
        return new CoreCollectionUtils.Pair<>(stillInHoliday, outFromHoliday);
    }

    /**
     * Возвращает список сотрудников ППС
     */
    private List<EmployeePost> getEmployeePostsList()
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.add(MQExpression.in("ep", EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().s(), EmployeeManager.instance().dao().getEmployeeTypeWithAllChilds(getCatalogItem(EmployeeType.class, EmployeeTypeCodes.EDU_STAFF))));
        builder.addJoinFetch("ep", EmployeePost.employee(), "e");
        builder.addJoinFetch("e", Employee.person(), "p");
        return builder.getResultList(getSession());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getReportDateFrom().after(model.getReportDateTo()))
        {
            errors.add("Дата начала должна быть меньше даты окончания.", "reportDateFrom", "reportDateTo");
        }
    }
}