/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.logic.EmpEmployeeTrainingItemDAO;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.logic.IEmpEmployeeTrainingItemDAO;

/**
 * Create by ashaburov
 * Date 26.03.12
 */
@Configuration
public class EmpEmployeeTrainingItemManager extends BusinessObjectManager
{
    public static EmpEmployeeTrainingItemManager instance()
    {
        return instance(EmpEmployeeTrainingItemManager.class);
    }

    @Bean
    public IEmpEmployeeTrainingItemDAO dao()
    {
        return new EmpEmployeeTrainingItemDAO();
    }
}
