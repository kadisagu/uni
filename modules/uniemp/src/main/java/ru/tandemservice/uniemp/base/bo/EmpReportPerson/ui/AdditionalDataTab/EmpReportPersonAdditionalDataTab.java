/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.contractData.ContractDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.holidayData.HolidayDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.serviceLengthData.ServiceLengthDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.sickData.SickDataBlock;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
@Configuration
public class EmpReportPersonAdditionalDataTab extends BusinessComponentManager
{
    // tab block lists
    public static final String EMPLOYEE_ADDITIONAL_DATA_BLOCK_LIST = "employeeAdditionalDataBlockList";

    // block names
    public static final String SICK_DATA = "sickData";
    public static final String CONTRACT_DATA = "contractData";
    public static final String HOLIDAY_DATA = "holidayData";
    public static final String ATTESTATION_DATA = "attestationData";
    public static final String TRAINING_DATA = "trainingData";
    public static final String RETRAINING_DATA = "retrainingData";
    public static final String SERVICE_LENGTH_DATA = "serviceLengthData";

    @Bean
    public BlockListExtPoint employeeAdditionalDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(EMPLOYEE_ADDITIONAL_DATA_BLOCK_LIST)
                .addBlock(htmlBlock(SICK_DATA, "block/sickData/SickData"))
                .addBlock(htmlBlock(CONTRACT_DATA, "block/contractData/ContractData"))
                .addBlock(htmlBlock(HOLIDAY_DATA, "block/holidayData/HolidayData"))
                .addBlock(htmlBlock(ATTESTATION_DATA, "block/attestationData/AttestationData"))
                .addBlock(htmlBlock(TRAINING_DATA, "block/trainingData/TrainingData"))
                .addBlock(htmlBlock(RETRAINING_DATA, "block/retrainingData/RetrainingData"))
                .addBlock(htmlBlock(SERVICE_LENGTH_DATA, "block/serviceLengthData/ServiceLengthData"))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler sickBasicComboDSHandler()
    {
        return SickDataBlock.createSickBasicDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler labContrTypeComboDSHandler()
    {
        return ContractDataBlock.createLabContrTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler holidayTypeComboDSHandler()
    {
        return HolidayDataBlock.createHolidayTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler serviceLengthTypeDSHandler()
    {
        return  ServiceLengthDataBlock.createServiceLengthTypeDS(getName());
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SickDataBlock.SICK_BASIC_DS, sickBasicComboDSHandler()))
                .addDataSource(selectDS(ContractDataBlock.LAB_CONTR_TYPE_DS, labContrTypeComboDSHandler()))
                .addDataSource(selectDS(HolidayDataBlock.HOLIDAY_TYPE_DS, holidayTypeComboDSHandler()))
                .addDataSource(selectDS(ServiceLengthDataBlock.SERVICE_LENGTH_TYPE_DS, serviceLengthTypeDSHandler()))
                .create();
    }
}
