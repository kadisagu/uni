/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.AdditionalAgreementAddEdit;

import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;

/**
 * @author E. Grigoriev
 * @since 11.07.2008
 */
@Input(keys = { "labourContractId", "rowId" }, bindings = { "labourContractId", "rowId" })
public class Model
{
    private Long _rowId;
    private Long _labourContractId;

    private ContractCollateralAgreement _agreement = new ContractCollateralAgreement();

    public Long getLabourContractId()
    {
        return _labourContractId;
    }

    public void setLabourContractId(Long labourContractId)
    {
        _labourContractId = labourContractId;
    }

    public ContractCollateralAgreement getAgreement()
    {
        return _agreement;
    }

    public void setAgreement(ContractCollateralAgreement agreement)
    {
        _agreement = agreement;
    }

    public Long getRowId()
    {
        return _rowId;
    }

    public void setRowId(Long rowId)
    {
        _rowId = rowId;
    }
}