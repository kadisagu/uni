/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemsAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.HolidayDuration;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniemp.dao.UniempDAO;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 18.01.2011
 */
public class DAO extends UniempDAO<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("ep");

    @Override
    public void prepare(final Model model)
    {
        model.setVacationSchedule(getNotNull(VacationSchedule.class, model.getVacationScheduleId()));

        model.setPostStatusesListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EmployeePostStatus.ENTITY_CLASS, "eps");

                if (null != model.getAcitvePostStatusFilter())
                {
                    Long id = model.getAcitvePostStatusFilter().getId();
                    if (Model.EMPLOYEE_STATUS_ACTIVE.equals(id))
                    {
                        builder.add(MQExpression.eq("eps", EmployeePostStatus.active().s(), Boolean.TRUE));
                    }
                    else if (Model.EMPLOYEE_STATUS_NON_ACTIVE.equals(id))
                    {
                        builder.add(MQExpression.eq("eps", EmployeePostStatus.active().s(), Boolean.FALSE));
                    }
                }
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get(EmployeePostStatus.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((EmployeePostStatus)value).getTitle();
            }
        });

        model.setPostTypesListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(PostType.ENTITY_CLASS, "pt");
                builder.addOrder("pt", PostType.code().s());
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setPostsListModel(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, alias);
                if (!StringUtils.isEmpty(filter))
                {
                    builder.add(MQExpression.like(alias, PostBoundedWithQGandQL.title().s(), CoreStringUtils.escapeLike(filter)));
                }
                builder.addOrder(alias, PostBoundedWithQGandQL.title().s());
                return builder;
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder subBuilder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi", new String[] { VacationScheduleItem.employeePost().id().s() });
        subBuilder.add(MQExpression.eq("vsi", VacationScheduleItem.vacationSchedule().s(), model.getVacationSchedule()));

        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.add(MQExpression.eq("ep", EmployeePost.orgUnit().s(), model.getVacationSchedule().getOrgUnit()));
        builder.add(MQExpression.notIn("ep", EmployeePost.id().s(), subBuilder));

        if (null != model.getLastNameFilter())
            builder.add(MQExpression.like("ep", EmployeePost.employee().person().identityCard().lastName().s(), CoreStringUtils.escapeLike(model.getLastNameFilter())));

        if (null != model.getPostStatusFilter())
            builder.add(MQExpression.eq("ep", EmployeePost.postStatus().s(), model.getPostStatusFilter()));
        else if (null != model.getAcitvePostStatusFilter())
            builder.add(MQExpression.eq("ep", EmployeePost.postStatus().active().s(), Model.EMPLOYEE_STATUS_ACTIVE.equals(model.getAcitvePostStatusFilter().getId()) ? Boolean.TRUE : Boolean.FALSE));

        if (null != model.getPostTypeFilter())
            builder.add(MQExpression.eq("ep", EmployeePost.postType().s(), model.getPostTypeFilter()));

        if (null != model.getPostFilter())
            builder.add(MQExpression.eq("ep", EmployeePost.postRelation().postBoundedWithQGandQL().s(), model.getPostFilter()));

        if (null != model.getHolidayDurationFilter())
            builder.add(MQExpression.eq("ep", EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().holidayDuration().daysAmount().s(), model.getHolidayDurationFilter()));

        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void validate(final Model model, ErrorCollector errors)
    {
        List<Long> idsList = new ArrayList<>();
        for (IEntity entity : ((CheckboxColumn)model.getDataSource().getColumn("selected")).getSelectedObjects())
        {
            idsList.add(entity.getId());
        }

        final List<String> alreadyAddedEmployeePosts = new ArrayList<>();
        BatchUtils.execute(idsList, 100, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(final Collection<Long> params)
            {
                MQBuilder builder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi", new String[] { VacationScheduleItem.employeePost().employee().person().identityCard().fullFio().s() });
                builder.add(MQExpression.eq("vsi", VacationScheduleItem.vacationSchedule().s(), model.getVacationSchedule()));
                builder.add(MQExpression.in("vsi", VacationScheduleItem.employeePost().id().s(), params));
                builder.addOrder("vsi", VacationScheduleItem.employeePost().employee().person().identityCard().fullFio().s());
                alreadyAddedEmployeePosts.addAll(builder.<String> getResultList(getSession()));
            }
        });

        if (!alreadyAddedEmployeePosts.isEmpty())
        {
            StringBuilder err = new StringBuilder("Следующие сотрудники уже добавлены в указанный график отпусков на ");
            err.append(model.getVacationSchedule().getYear()).append(" год:");

            for (String employee : alreadyAddedEmployeePosts)
            {
                err.append(" ").append(employee).append(",");
            }

            errors.add(err.toString());
        }
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errs = UserContext.getInstance().getErrorCollector();
        List<VacationScheduleItem> preparedToSaveItems = new ArrayList<>();

        for (IEntity entity : ((CheckboxColumn)model.getDataSource().getColumn("selected")).getSelectedObjects())
        {
            EmployeePost employeePost = (EmployeePost)entity;
            HolidayDuration holidayDuration = employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getEmployeeType().getHolidayDuration();

            VacationScheduleItem item = new VacationScheduleItem();
            if (null != holidayDuration) item.setDaysAmount(holidayDuration.getDaysAmount());
            item.setVacationSchedule(model.getVacationSchedule());
            item.setPlanDate(model.getPlanDate());
            item.setEmployeePost(employeePost);

            UniempDaoFacade.getUniempDAO().validateVacationScheduleItemHasIntersections(item, errs, "Указанный планируемый отпуск у сотрудника (" + item.getEmployeePost().getEmployeePostStr() + ")", "planDate", "", "");
            preparedToSaveItems.add(item);
        }

        if (errs.hasErrors()) return;

        for (VacationScheduleItem item : preparedToSaveItems)
            save(item);
    }
}