/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniemp.entity.catalog.ResolutionCertificationCommission;

/**
 * Create by ashaburov
 * Date 22.03.12
 */
@Configuration
public class EmpEmployeeCertificationItemAddEdit extends BusinessComponentManager
{
    public static final String EMP_RESOLUTION_DS = "resolutionDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EMP_RESOLUTION_DS, resolutionDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler resolutionDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), ResolutionCertificationCommission.class);
    }
}
