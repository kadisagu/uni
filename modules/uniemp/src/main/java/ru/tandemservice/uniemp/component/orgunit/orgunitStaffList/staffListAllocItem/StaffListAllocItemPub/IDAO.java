/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListAllocItemPub;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;

/**
 * @author dseleznev
 * Created on: 08.04.2009
 */
public interface IDAO extends IUniDao<Model>
{
    void deleteStaffListPaymentItem(Long id, StaffListAllocationItem allocationItem);
}