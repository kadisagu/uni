/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeePost.ui.PrintPersonCard;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.base.bo.EmpEmployeePost.EmpEmployeePostManager;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;

import java.util.Date;

/**
 * Create by ashaburov
 * Date 30.05.12
 */
@Input({
        @Bind(key = EmpEmployeePostPrintPersonCardUI.EMPLOYEE_POST_ID, binding = EmpEmployeePostPrintPersonCardUI.EMPLOYEE_POST_ID)
})
public class EmpEmployeePostPrintPersonCardUI extends UIPresenter
{
    public static final String EMPLOYEE_POST_ID = "employeePostId";

    // fields
    private Long _employeePostId;
    private EmployeePost _employeePost;
    private Date _printDate;


    //Presenter methods

    @Override
    public void onComponentRefresh()
    {
        _employeePost = DataAccessServices.dao().getNotNull(EmployeePost.class, _employeePostId);
        _printDate = new Date();
    }

    // Utils

    private RtfDocument initRtfDocument()
    {
        RtfDocument document = new RtfReader().read(DataAccessServices.dao().getByCode(EmployeeTemplateDocument.class, "employeePostPersonCard").getContent());
        RtfInjectModifier modifier = EmpEmployeePostManager.instance().employeePostDAO().preparePersonCardModifier(_employeePost, _printDate);
        RtfTableModifier tableModifier = EmpEmployeePostManager.instance().employeePostDAO().preparePersonCardTableModifier(_employeePost, _printDate);

        modifier.modify(document);
        tableModifier.modify(document);

        return document;
    }


    // Listeners

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("EmployeePostPersonCard.rtf").document(initRtfDocument()), true);

        deactivate();
    }


    // Getters & Setters

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public Date getPrintDate()
    {
        return _printDate;
    }

    public void setPrintDate(Date printDate)
    {
        _printDate = printDate;
    }
}
