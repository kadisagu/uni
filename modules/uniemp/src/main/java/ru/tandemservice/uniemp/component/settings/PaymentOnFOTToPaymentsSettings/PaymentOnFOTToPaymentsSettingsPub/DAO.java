/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.settings.PaymentOnFOTToPaymentsSettings.PaymentOnFOTToPaymentsSettingsPub;

import java.util.*;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListDAO;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.PaymentOnFOTToPaymentsRelation;

/**
 * Create by: ashaburov
 * Date: 26.04.11
 */
public class DAO extends AbstractRelationListDAO<Model> implements IDAO
{
    @Override
    protected String getFirstObjectEntityName()
    {
        return Payment.ENTITY_CLASS;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        Map<Long, List<Payment>> relsMap = new HashMap<>();

        MQBuilder subBuilder = new MQBuilder(PaymentOnFOTToPaymentsRelation.ENTITY_CLASS, "b");

        MQBuilder builder = new MQBuilder(getFirstObjectEntityName(), "o");
        builder.add(MQExpression.eq("o", Payment.paymentUnit().code().s(), UniempDefines.PAYMENT_UNIT_FULL_PERCENT));
        new OrderDescriptionRegistry("o").applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), (List) builder.getResultList(getSession()));

        for (Payment payment : builder.<Payment>getResultList(getSession()))
        {
            List<Payment> paymetens = new ArrayList<>();
            for (PaymentOnFOTToPaymentsRelation relation : subBuilder.<PaymentOnFOTToPaymentsRelation>getResultList(getSession()))
            {
                if (relation.getFirst().equals(payment))
                    paymetens.add(relation.getSecond());
            }
            relsMap.put(payment.getId(), paymetens);
        }

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            if (relsMap.containsKey(wrapper.getEntity().getId()))
                wrapper.setViewProperty(Model.P_PAYMENTS_LIST, relsMap.get(wrapper.getEntity().getId()));
            else
                wrapper.setViewProperty(Model.P_PAYMENTS_LIST, null);
        }
    }

    @Override
    public void updateRelations(Payment payment)
    {
        MQBuilder builder = new MQBuilder(PaymentOnFOTToPaymentsRelation.ENTITY_CLASS, "r");
        builder.add(MQExpression.eq("r", PaymentOnFOTToPaymentsRelation.first().code().s(), payment.getCode()));
        List<PaymentOnFOTToPaymentsRelation> relationList = builder.getResultList(getSession());

        for (PaymentOnFOTToPaymentsRelation relation : relationList)
            delete(relation);
    }

}
