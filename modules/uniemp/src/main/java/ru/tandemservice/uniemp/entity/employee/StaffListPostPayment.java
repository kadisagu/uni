package ru.tandemservice.uniemp.entity.employee;

import ru.tandemservice.uniemp.entity.employee.gen.StaffListPostPaymentGen;

/**
 * Выплаты должности штатного расписания
 */
public class StaffListPostPayment extends StaffListPostPaymentGen
{
    private StaffListAllocationItem _allocationItem;

    @Override
    protected double getStaffRate()
    {
        if(null != getTargetAllocItem())
            return getTargetAllocItem().getStaffRate();
        else
            return getStaffListItem().getStaffRate();
    }

    @Override
    public StaffListAllocationItem getTargetAllocItem()
    {
        return _allocationItem;
    }

    @Override
    public void setTargetAllocItem(StaffListAllocationItem allocItem)
    {
        this._allocationItem = allocItem;
    }
    
    @Override
    public boolean isEditingDisabled()
    {
        return super.isEditingDisabled() || null != getTargetAllocItem();
    }

    @Override
    public boolean isDeleteDisabled()
    {
        return super.isDeleteDisabled() || null != getTargetAllocItem();
    }
}