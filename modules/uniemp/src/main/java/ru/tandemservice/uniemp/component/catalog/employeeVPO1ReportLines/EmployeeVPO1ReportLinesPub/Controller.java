/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.catalog.employeeVPO1ReportLines.EmployeeVPO1ReportLinesPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;

/**
 * @author AutoGenerator
 * Created on 21.01.2010
 */
public class Controller extends DefaultCatalogPubController<EmployeeVPO1ReportLines, Model, IDAO>
{
    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent context, DynamicListDataSource<EmployeeVPO1ReportLines> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Код строки", EmployeeVPO1ReportLines.P_LINE_CODE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Печатный постфикс", EmployeeVPO1ReportLines.P_PREFIX).setClickable(false));
    }
}