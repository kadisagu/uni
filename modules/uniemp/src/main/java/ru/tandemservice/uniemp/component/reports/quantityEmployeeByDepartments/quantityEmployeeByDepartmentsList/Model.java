package ru.tandemservice.uniemp.component.reports.quantityEmployeeByDepartments.quantityEmployeeByDepartmentsList;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Model
{
    private IDataSettings _settings;
    private DynamicListDataSource<IEntity> _dataSource;

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<IEntity> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<IEntity> dataSource)
    {
        _dataSource = dataSource;
    }

}
