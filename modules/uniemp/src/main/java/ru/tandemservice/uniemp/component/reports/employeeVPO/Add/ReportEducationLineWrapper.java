/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.employeeVPO.Add;

/**
 * @author dseleznev
 * Created on: 22.01.2010
 */
public class ReportEducationLineWrapper
{
    public ReportEducationLineWrapper(int level, String reportLineName, String reportLineCode, int numberLine)
    {
        _level = level;
        _reportLineName = reportLineName;
        _reportLineCode = reportLineCode;
        _numberLine = numberLine;
    }

    private int _level;
    private String _reportLineName;
    private String _reportLineCode;
    private int _numberLine;
    private int _total;
    private int _gr3HighEdu;
    private int _doctor;
    private int _candidate;
    private int _professor;
    private int _docent;
    private int _staffRate025;
    private int _staffRate050;
    private int _staffRate075;
    private int _women;
    private int _profQuali;

    // Getters & Setters

    public int getLevel()
    {
        return _level;
    }

    public void setLevel(int level)
    {
        _level = level;
    }

    public String getReportLineName()
    {
        return _reportLineName;
    }

    public void setReportLineName(String reportLineName)
    {
        _reportLineName = reportLineName;
    }

    public String getReportLineCode()
    {
        return _reportLineCode;
    }

    public void setReportLineCode(String reportLineCode)
    {
        _reportLineCode = reportLineCode;
    }

    public int getNumberLine()
    {
        return _numberLine;
    }

    public void setNumberLine(int numberLine)
    {
        _numberLine = numberLine;
    }

    public int getTotal()
    {
        return _total;
    }

    public void setTotal(int total)
    {
        _total = total;
    }

    public int getGr3HighEdu()
    {
        return _gr3HighEdu;
    }

    public void setGr3HighEdu(int gr3HighEdu)
    {
        _gr3HighEdu = gr3HighEdu;
    }

    public int getDoctor()
    {
        return _doctor;
    }

    public void setDoctor(int doctor)
    {
        _doctor = doctor;
    }

    public int getCandidate()
    {
        return _candidate;
    }

    public void setCandidate(int candidate)
    {
        _candidate = candidate;
    }

    public int getProfessor()
    {
        return _professor;
    }

    public void setProfessor(int professor)
    {
        _professor = professor;
    }

    public int getDocent()
    {
        return _docent;
    }

    public void setDocent(int docent)
    {
        _docent = docent;
    }

    public int getStaffRate025()
    {
        return _staffRate025;
    }

    public void setStaffRate025(int staffRate025)
    {
        _staffRate025 = staffRate025;
    }

    public int getStaffRate050()
    {
        return _staffRate050;
    }

    public void setStaffRate050(int staffRate050)
    {
        _staffRate050 = staffRate050;
    }

    public int getStaffRate075()
    {
        return _staffRate075;
    }

    public void setStaffRate075(int staffRate075)
    {
        _staffRate075 = staffRate075;
    }

    public int getWomen()
    {
        return _women;
    }

    public void setWomen(int women)
    {
        _women = women;
    }

    public int getProfQuali()
    {
        return _profQuali;
    }

    public void setProfQuali(int profQuali)
    {
        _profQuali = profQuali;
    }
}