/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.serviceLengthData;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 19.06.12
 * Time: 19:30
 * To change this template use File | Settings | File Templates.
 */
public class ServiceLengthDataBlock
{
    // names
    public static final String SERVICE_LENGTH_TYPE_DS = "serviceLengthTypeDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, ServiceLengthDataParam param)
    {

    }

    public static IDefaultComboDataSourceHandler createServiceLengthTypeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, ServiceLengthType.class);
    }
}
