package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uniemp.base.ext.Employee.logic.EmploymentCalculator;
import ru.tandemservice.uniemp.entity.employee.gen.EmploymentHistoryItemBaseGen;

public abstract class EmploymentHistoryItemBase extends EmploymentHistoryItemBaseGen
{
    public static final String P_ORGANIZATION_TITLE = "organizationTitle";

    public static final String P_ORG_UNIT_TITLE = "orgUnitTitle";

    public static final String P_POST_TITLE = "postTitle";

    public static final String P_FULL_TITLE = "fullTitle";

    public static final String P_EMPLOYMENT_DURATION_FULL_DAYS_AMOUNT = "employmentDurationFullDaysAmount";

    public static final String P_EMPLOYMENT_DURATION_YEARS_AMOUNT = "employmentDurationYearsAmount";

    public static final String P_EMPLOYMENT_DURATION_MONTHS_AMOUNT = "employmentDurationMonthsAmount";

    public static final String P_EMPLOYMENT_DURATION_DAYS_AMOUNT = "employmentDurationDaysAmount";

    public static final String P_BASICS = "basics";

    private Integer _employmentDurationFullDaysAmount;

    private Integer _employmentDurationYearsAmount;

    private Integer _employmentDurationMonthsAmount;

    private Integer _employmentDurationDaysAmount;

    public abstract String getOrganizationTitle();

    public abstract String getOrgUnitTitle();

    public abstract String getPostTitle();

    public String getFullTitle()
    {
        return getPostTitle() + " (" + getOrgUnitTitle() + ", " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getAssignDate()) + "-" + (null != getDismissalDate() ? DateFormatter.DEFAULT_DATE_FORMATTER.format(getDismissalDate()) : "настоящее время") + ")";
    }

    private void calculateEmploymentDuration()
    {
        if (null == _employmentDurationFullDaysAmount)
        {
            EmploymentCalculator calc = new EmploymentCalculator();
            calc.calculateEmploymentPeriod(getAssignDate(), getDismissalDate());
            _employmentDurationFullDaysAmount = calc.getFullDaysAmount();
            _employmentDurationYearsAmount = calc.getYearsAmount();
            _employmentDurationMonthsAmount = calc.getMonthsAmount();
            _employmentDurationDaysAmount = calc.getDaysAmount();
        }
    }

    public Integer getEmploymentDurationFullDaysAmount()
    {
        calculateEmploymentDuration();
        return _employmentDurationFullDaysAmount;
    }

    public Integer getEmploymentDurationYearsAmount()
    {
        calculateEmploymentDuration();
        return _employmentDurationYearsAmount;
    }

    public Integer getEmploymentDurationMonthsAmount()
    {
        calculateEmploymentDuration();
        return _employmentDurationMonthsAmount;
    }

    public Integer getEmploymentDurationDaysAmount()
    {
        calculateEmploymentDuration();
        return _employmentDurationDaysAmount;
    }

    public String getBasics()
    {
        StringBuilder resultBuilder = new StringBuilder();
        if (getExtractDate() != null && getExtractNumber() != null)
        {
            resultBuilder.append("Приказ №");
            resultBuilder.append(getExtractNumber());
            resultBuilder.append(" от ");
            resultBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getExtractDate()));
        }
        return resultBuilder.toString();
    }
}