package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Штатное расписание
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffListGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.StaffList";
    public static final String ENTITY_NAME = "staffList";
    public static final int VERSION_HASH = -1804881991;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_ACCEPTION_DATE = "acceptionDate";
    public static final String P_ARCHIVING_DATE = "archivingDate";
    public static final String P_NUMBER = "number";
    public static final String L_STAFF_LIST_STATE = "staffListState";

    private OrgUnit _orgUnit;     // Подразделение
    private Date _creationDate;     // Дата создания
    private Date _acceptionDate;     // Дата согласования
    private Date _archivingDate;     // Дата архивации
    private int _number;     // Номер
    private StaffListState _staffListState;     // Состояние штатного расписания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата создания. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Дата согласования.
     */
    public Date getAcceptionDate()
    {
        return _acceptionDate;
    }

    /**
     * @param acceptionDate Дата согласования.
     */
    public void setAcceptionDate(Date acceptionDate)
    {
        dirty(_acceptionDate, acceptionDate);
        _acceptionDate = acceptionDate;
    }

    /**
     * @return Дата архивации.
     */
    public Date getArchivingDate()
    {
        return _archivingDate;
    }

    /**
     * @param archivingDate Дата архивации.
     */
    public void setArchivingDate(Date archivingDate)
    {
        dirty(_archivingDate, archivingDate);
        _archivingDate = archivingDate;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Состояние штатного расписания. Свойство не может быть null.
     */
    @NotNull
    public StaffListState getStaffListState()
    {
        return _staffListState;
    }

    /**
     * @param staffListState Состояние штатного расписания. Свойство не может быть null.
     */
    public void setStaffListState(StaffListState staffListState)
    {
        dirty(_staffListState, staffListState);
        _staffListState = staffListState;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StaffListGen)
        {
            setOrgUnit(((StaffList)another).getOrgUnit());
            setCreationDate(((StaffList)another).getCreationDate());
            setAcceptionDate(((StaffList)another).getAcceptionDate());
            setArchivingDate(((StaffList)another).getArchivingDate());
            setNumber(((StaffList)another).getNumber());
            setStaffListState(((StaffList)another).getStaffListState());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffListGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffList.class;
        }

        public T newInstance()
        {
            return (T) new StaffList();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "creationDate":
                    return obj.getCreationDate();
                case "acceptionDate":
                    return obj.getAcceptionDate();
                case "archivingDate":
                    return obj.getArchivingDate();
                case "number":
                    return obj.getNumber();
                case "staffListState":
                    return obj.getStaffListState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "acceptionDate":
                    obj.setAcceptionDate((Date) value);
                    return;
                case "archivingDate":
                    obj.setArchivingDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "staffListState":
                    obj.setStaffListState((StaffListState) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "creationDate":
                        return true;
                case "acceptionDate":
                        return true;
                case "archivingDate":
                        return true;
                case "number":
                        return true;
                case "staffListState":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "creationDate":
                    return true;
                case "acceptionDate":
                    return true;
                case "archivingDate":
                    return true;
                case "number":
                    return true;
                case "staffListState":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "creationDate":
                    return Date.class;
                case "acceptionDate":
                    return Date.class;
                case "archivingDate":
                    return Date.class;
                case "number":
                    return Integer.class;
                case "staffListState":
                    return StaffListState.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffList> _dslPath = new Path<StaffList>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffList");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getAcceptionDate()
     */
    public static PropertyPath<Date> acceptionDate()
    {
        return _dslPath.acceptionDate();
    }

    /**
     * @return Дата архивации.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getArchivingDate()
     */
    public static PropertyPath<Date> archivingDate()
    {
        return _dslPath.archivingDate();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Состояние штатного расписания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getStaffListState()
     */
    public static StaffListState.Path<StaffListState> staffListState()
    {
        return _dslPath.staffListState();
    }

    public static class Path<E extends StaffList> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<Date> _acceptionDate;
        private PropertyPath<Date> _archivingDate;
        private PropertyPath<Integer> _number;
        private StaffListState.Path<StaffListState> _staffListState;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(StaffListGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Дата согласования.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getAcceptionDate()
     */
        public PropertyPath<Date> acceptionDate()
        {
            if(_acceptionDate == null )
                _acceptionDate = new PropertyPath<Date>(StaffListGen.P_ACCEPTION_DATE, this);
            return _acceptionDate;
        }

    /**
     * @return Дата архивации.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getArchivingDate()
     */
        public PropertyPath<Date> archivingDate()
        {
            if(_archivingDate == null )
                _archivingDate = new PropertyPath<Date>(StaffListGen.P_ARCHIVING_DATE, this);
            return _archivingDate;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(StaffListGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Состояние штатного расписания. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.StaffList#getStaffListState()
     */
        public StaffListState.Path<StaffListState> staffListState()
        {
            if(_staffListState == null )
                _staffListState = new StaffListState.Path<StaffListState>(L_STAFF_LIST_STATE, this);
            return _staffListState;
        }

        public Class getEntityClass()
        {
            return StaffList.class;
        }

        public String getEntityName()
        {
            return "staffList";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
