/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.ui.AddEdit;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationalInstitutionTypeKind;
import ru.tandemservice.uniemp.base.bo.EmpEmployeeTrainingItem.EmpEmployeeTrainingItemManager;
import ru.tandemservice.uniemp.entity.employee.EmployeeTrainingItem;

import java.io.IOException;

/**
 * Create by ashaburov
 * Date 26.03.12
 */
@Input({
    @Bind(key = EmpEmployeeTrainingItemAddEditUI.EMPLOYEE_ID, binding = EmpEmployeeTrainingItemAddEditUI.EMPLOYEE_ID),
    @Bind(key = EmpEmployeeTrainingItemAddEditUI.ITEM_ID, binding = EmpEmployeeTrainingItemAddEditUI.ITEM_ID),
    @Bind(key = EmpEmployeeTrainingItemAddEditUI.CREATE_EDU_INSTITUTION_ID, binding = EmpEmployeeTrainingItemAddEditUI.CREATE_EDU_INSTITUTION_ID)

})
public class EmpEmployeeTrainingItemAddEditUI extends UIPresenter
{
    //input fields name
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String ITEM_ID = "itemId";
    public static final String CREATE_EDU_INSTITUTION_ID = "createdEduInstitutionId";

    //ds param names
    public static final String SELECTED_EDU_INSTITUTION_TYPE = "eduInstitutionParent";
    public static final String SELECTED_COUNTRY = "country";
    public static final String SELECTED_ADDRESS_ITEM = "addressItem";
    public static final String SELECTED_EDU_INSTITUTION_KIND = "eduInstitutionKind";

    private boolean _addForm;
    private boolean _notRefreshComponent;

    private boolean _fileUploadBlockVisible;
    private IUploadFile _eduDocumentFile;
    private boolean _delEduDocumentFile;

    private Long _employeeId;
    private Long _itemId;
    private Long _createdEduInstitutionId;

    private Employee _employee;
    private EmployeeTrainingItem _item;

    private AddressCountry _country;
    private EducationalInstitutionTypeKind _eduInstitutionParent;

    //Presenter methods

    @Override
    public void onComponentRefresh()
    {
        //если заполненно поле id Образовательного учреждения, значит была открыта форма добавления нового эолемента справочника и id добавленного элемента вернулся
        //нужно просетить этот новый элемент в соответствующее поле и выйти из метода, что бы не затереть значения других полей
        if (getCreatedEduInstitutionId() != null)
        {
            getItem().setEduInstitution(DataAccessServices.dao().getNotNull(EduInstitution.class, getCreatedEduInstitutionId()));
            return;
        }
        //если id элемента не пришел, но стоит метка isNotRefreshComponent, значит бала открыта форма добавления нового элемента, но нажата Отмена
        //нужно выйти из метода, что бы не обнулить другие поля и убрать метку isNotRefreshComponent
        else if (isNotRefreshComponent())
        {
            setNotRefreshComponent(false);
            return;
        }

        //заполняем КР
        setEmployee(DataAccessServices.dao().getNotNull(Employee.class, getEmployeeId()));

        //заполняем элемент данных ПК
        if (getItemId() == null)
            setItem(new EmployeeTrainingItem());
        else
        {
            setItem(DataAccessServices.dao().getNotNull(EmployeeTrainingItem.class, getItemId()));
            setCountry(getItem().getAddressItem().getCountry());
            setEduInstitutionParent(getItem().getEduInstitutionKind().getParent());
        }

        //выставляем признак добавлене\редактирование
        if (getItemId() == null)
            setAddForm(true);
        else
            setAddForm(false);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
		switch (dataSource.getName())
		{
			case EmpEmployeeTrainingItemAddEdit.EDU_INSTITUTION_KIND_DS:
				dataSource.put(SELECTED_EDU_INSTITUTION_TYPE, getEduInstitutionParent());
				break;
			case EmpEmployeeTrainingItemAddEdit.SETTLEMENT_DS:
				dataSource.put(AddressItem.PARAM_COUNTRY, getCountry());
				break;
			case EmpEmployeeTrainingItemAddEdit.EDU_INSTITUTION_DS:
				dataSource.put(SELECTED_COUNTRY, getCountry());
				dataSource.put(SELECTED_ADDRESS_ITEM, getItem().getAddressItem());
				dataSource.put(SELECTED_EDU_INSTITUTION_TYPE, getEduInstitutionParent());
				dataSource.put(SELECTED_EDU_INSTITUTION_KIND, getItem().getEduInstitutionKind());
				break;
		}
    }

    protected ErrorCollector validate()
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if (getItem().getStartDate().getTime() > getItem().getFinishDate().getTime())
            errorCollector.add("Дата начала должна быть не больше даты окончания.", "startDate", "finishDate");

        return errorCollector;
    }

    //Listeners

    public void onClickAddEduInstitution()
    {
        setNotRefreshComponent(true);

        _uiActivation.asRegionDialog(CommonManager.catalogComponentProvider().getAddEditComponent(EduInstitution.class))
                .parameter(DefaultCatalogAddEditModel.CATALOG_CODE, EduInstitution.ENTITY_NAME)
                .parameter(org.tandemframework.shared.person.component.catalog.eduInstitution.EduInstitutionAddEdit.Model.EXISTANCE_CHECKING_TYPE_PARAM_NAME, org.tandemframework.shared.person.component.catalog.eduInstitution.EduInstitutionAddEdit.Model.RETURN_EXISTING)
                .parameter("personRoleName", Employee.class.getName())
                .parameter("countryId", getCountry() != null ? getCountry().getId() : null)
                .parameter("addressItemId", getItem().getAddressItem() != null ? getItem().getAddressItem().getId() : null)
                .parameter("eduInstitutionTypeId", getItem().getEduInstitutionKind() != null ? getItem().getEduInstitutionKind().getId() : null)
                .activate();
    }

    public void onClickAddDocument()
    {
        setFileUploadBlockVisible(true);
    }

    public void onClickDelDocument()
    {
        setFileUploadBlockVisible(false);

        setDelEduDocumentFile(true);
    }

    public void onClickApply()
    {
        //деаем проверки, если они есть то выходим
        if (validate().hasErrors())
            return;

        if (getItem().getEmployee() == null)
            getItem().setEmployee(getEmployee());

        //если стоит метка на удаление файла, то удаляем его и зануляем в соответствующем поле объекта
        if (isDelEduDocumentFile())
        {
            DatabaseFile eduDocumentFile = getItem().getEduDocumentFile();
            getItem().setEduDocumentFile(null);
            DataAccessServices.dao().delete(eduDocumentFile);
        }

        //если указан файл и отображалась форма добавления файла, то создаем DatabaseFile и сохраняем ссылку на него в соответствующем поле объекта
        if (getEduDocumentFile() != null && isFileUploadBlockVisible())
        {
            try
            {
                byte[] content = IOUtils.toByteArray(getEduDocumentFile().getStream());

                if ((content != null) && (content.length > 0))
                {
                    if (getItem().getEduDocumentFile() == null)
                        getItem().setEduDocumentFile(new DatabaseFile());
                    getItem().getEduDocumentFile().setContent(content);
                    getItem().setEduDocumentFileName(getEduDocumentFile().getFileName());
                    getItem().setEduDocumentFileType(CommonBaseUtil.getContentType(getEduDocumentFile()));
                    DataAccessServices.dao().save(getItem().getEduDocumentFile());
                }
            }
            catch (IOException e)
            {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        EmpEmployeeTrainingItemManager.instance().dao().saveOrUpdateItem(getItem());

        deactivate();
    }

    //Getters & Setters

    public boolean isNotRefreshComponent()
    {
        return _notRefreshComponent;
    }

    public void setNotRefreshComponent(boolean notRefreshComponent)
    {
        _notRefreshComponent = notRefreshComponent;
    }

    public Long getCreatedEduInstitutionId()
    {
        return _createdEduInstitutionId;
    }

    public void setCreatedEduInstitutionId(Long createdEduInstitutionId)
    {
        _createdEduInstitutionId = createdEduInstitutionId;
    }

    public boolean isDelEduDocumentFile()
    {
        return _delEduDocumentFile;
    }

    public void setDelEduDocumentFile(boolean delEduDocumentFile)
    {
        _delEduDocumentFile = delEduDocumentFile;
    }

    public IUploadFile getEduDocumentFile()
    {
        return _eduDocumentFile;
    }

    public void setEduDocumentFile(IUploadFile eduDocumentFile)
    {
        _eduDocumentFile = eduDocumentFile;
    }

    public EducationalInstitutionTypeKind getEduInstitutionParent()
    {
        return _eduInstitutionParent;
    }

    public void setEduInstitutionParent(EducationalInstitutionTypeKind eduInstitutionParent)
    {
        _eduInstitutionParent = eduInstitutionParent;
    }

    public boolean isFileUploadBlockVisible()
    {
        return _fileUploadBlockVisible;
    }

    public void setFileUploadBlockVisible(boolean fileUploadBlockVisible)
    {
        _fileUploadBlockVisible = fileUploadBlockVisible;
    }

    public AddressCountry getCountry()
    {
        return _country;
    }

    public void setCountry(AddressCountry country)
    {
        _country = country;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public Long getEmployeeId()
    {
        return _employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        _employeeId = employeeId;
    }

    public Long getItemId()
    {
        return _itemId;
    }

    public void setItemId(Long itemId)
    {
        _itemId = itemId;
    }

    public Employee getEmployee()
    {
        return _employee;
    }

    public void setEmployee(Employee employee)
    {
        _employee = employee;
    }

    public EmployeeTrainingItem getItem()
    {
        return _item;
    }

    public void setItem(EmployeeTrainingItem item)
    {
        _item = item;
    }
}
