package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент истории должностей сотрудника (вне ОУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmploymentHistoryItemExtGen extends EmploymentHistoryItemBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt";
    public static final String ENTITY_NAME = "employmentHistoryItemExt";
    public static final int VERSION_HASH = -72385343;
    private static IEntityMeta ENTITY_META;

    public static final String P_ORGANIZATION_TITLE = "organizationTitle";
    public static final String P_ORG_UNIT_TITLE = "orgUnitTitle";
    public static final String P_POST_TITLE = "postTitle";

    private String _organizationTitle;     // Организация
    private String _orgUnitTitle;     // Подразделение
    private String _postTitle;     // Должность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Организация. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOrganizationTitle()
    {
        return _organizationTitle;
    }

    /**
     * @param organizationTitle Организация. Свойство не может быть null.
     */
    public void setOrganizationTitle(String organizationTitle)
    {
        dirty(_organizationTitle, organizationTitle);
        _organizationTitle = organizationTitle;
    }

    /**
     * @return Подразделение.
     */
    @Length(max=255)
    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    /**
     * @param orgUnitTitle Подразделение.
     */
    public void setOrgUnitTitle(String orgUnitTitle)
    {
        dirty(_orgUnitTitle, orgUnitTitle);
        _orgUnitTitle = orgUnitTitle;
    }

    /**
     * @return Должность. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPostTitle()
    {
        return _postTitle;
    }

    /**
     * @param postTitle Должность. Свойство не может быть null.
     */
    public void setPostTitle(String postTitle)
    {
        dirty(_postTitle, postTitle);
        _postTitle = postTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmploymentHistoryItemExtGen)
        {
            setOrganizationTitle(((EmploymentHistoryItemExt)another).getOrganizationTitle());
            setOrgUnitTitle(((EmploymentHistoryItemExt)another).getOrgUnitTitle());
            setPostTitle(((EmploymentHistoryItemExt)another).getPostTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmploymentHistoryItemExtGen> extends EmploymentHistoryItemBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmploymentHistoryItemExt.class;
        }

        public T newInstance()
        {
            return (T) new EmploymentHistoryItemExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "organizationTitle":
                    return obj.getOrganizationTitle();
                case "orgUnitTitle":
                    return obj.getOrgUnitTitle();
                case "postTitle":
                    return obj.getPostTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "organizationTitle":
                    obj.setOrganizationTitle((String) value);
                    return;
                case "orgUnitTitle":
                    obj.setOrgUnitTitle((String) value);
                    return;
                case "postTitle":
                    obj.setPostTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "organizationTitle":
                        return true;
                case "orgUnitTitle":
                        return true;
                case "postTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "organizationTitle":
                    return true;
                case "orgUnitTitle":
                    return true;
                case "postTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "organizationTitle":
                    return String.class;
                case "orgUnitTitle":
                    return String.class;
                case "postTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmploymentHistoryItemExt> _dslPath = new Path<EmploymentHistoryItemExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmploymentHistoryItemExt");
    }
            

    /**
     * @return Организация. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt#getOrganizationTitle()
     */
    public static PropertyPath<String> organizationTitle()
    {
        return _dslPath.organizationTitle();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt#getOrgUnitTitle()
     */
    public static PropertyPath<String> orgUnitTitle()
    {
        return _dslPath.orgUnitTitle();
    }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt#getPostTitle()
     */
    public static PropertyPath<String> postTitle()
    {
        return _dslPath.postTitle();
    }

    public static class Path<E extends EmploymentHistoryItemExt> extends EmploymentHistoryItemBase.Path<E>
    {
        private PropertyPath<String> _organizationTitle;
        private PropertyPath<String> _orgUnitTitle;
        private PropertyPath<String> _postTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Организация. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt#getOrganizationTitle()
     */
        public PropertyPath<String> organizationTitle()
        {
            if(_organizationTitle == null )
                _organizationTitle = new PropertyPath<String>(EmploymentHistoryItemExtGen.P_ORGANIZATION_TITLE, this);
            return _organizationTitle;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt#getOrgUnitTitle()
     */
        public PropertyPath<String> orgUnitTitle()
        {
            if(_orgUnitTitle == null )
                _orgUnitTitle = new PropertyPath<String>(EmploymentHistoryItemExtGen.P_ORG_UNIT_TITLE, this);
            return _orgUnitTitle;
        }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt#getPostTitle()
     */
        public PropertyPath<String> postTitle()
        {
            if(_postTitle == null )
                _postTitle = new PropertyPath<String>(EmploymentHistoryItemExtGen.P_POST_TITLE, this);
            return _postTitle;
        }

        public Class getEntityClass()
        {
            return EmploymentHistoryItemExt.class;
        }

        public String getEntityName()
        {
            return "employmentHistoryItemExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
