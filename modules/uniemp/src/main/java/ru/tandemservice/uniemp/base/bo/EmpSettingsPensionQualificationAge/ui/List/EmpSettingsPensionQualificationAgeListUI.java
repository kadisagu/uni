/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniemp.base.bo.EmpSettingsPensionQualificationAge.ui.AddEdit.EmpSettingsPensionQualificationAgeAddEdit;

/**
 * @author Alexander Shaburov
 * @since 11.09.12
 */
public class EmpSettingsPensionQualificationAgeListUI extends UIPresenter
{
    public void onClickAddElement()
    {
        _uiActivation.asRegion(EmpSettingsPensionQualificationAgeAddEdit.class).activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegion(EmpSettingsPensionQualificationAgeAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
