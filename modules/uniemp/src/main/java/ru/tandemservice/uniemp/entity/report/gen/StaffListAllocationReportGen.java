package ru.tandemservice.uniemp.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniemp.entity.report.StaffListAllocationReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Штатная расстановка»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffListAllocationReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.report.StaffListAllocationReport";
    public static final String ENTITY_NAME = "staffListAllocationReport";
    public static final int VERSION_HASH = 1669657332;
    private static IEntityMeta ENTITY_META;

    public static final String P_REPORT_DATE = "reportDate";
    public static final String P_ACTIVITY_TYPE = "activityType";
    public static final String P_ORG_UNIT_TYPES = "orgUnitTypes";
    public static final String P_ORG_UNIT = "orgUnit";
    public static final String P_EMPLOYEE_POST_TYPE = "employeePostType";
    public static final String P_QUALIFICATION_LEVEL = "qualificationLevel";
    public static final String P_PROF_QUALIFICATION_GROUP = "profQualificationGroup";
    public static final String P_POST = "post";

    private String _reportDate;     // Дата отчета
    private String _activityType;     // Вид деятельности
    private String _orgUnitTypes;     // Типы подразделений
    private String _orgUnit;     // Подразделение
    private String _employeePostType;     // Тип должности
    private String _qualificationLevel;     // Квалификационный уровень
    private String _profQualificationGroup;     // Профессионально-квалификационная группа
    private String _post;     // Должность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата отчета.
     */
    @Length(max=255)
    public String getReportDate()
    {
        return _reportDate;
    }

    /**
     * @param reportDate Дата отчета.
     */
    public void setReportDate(String reportDate)
    {
        dirty(_reportDate, reportDate);
        _reportDate = reportDate;
    }

    /**
     * @return Вид деятельности.
     */
    @Length(max=255)
    public String getActivityType()
    {
        return _activityType;
    }

    /**
     * @param activityType Вид деятельности.
     */
    public void setActivityType(String activityType)
    {
        dirty(_activityType, activityType);
        _activityType = activityType;
    }

    /**
     * @return Типы подразделений.
     */
    @Length(max=255)
    public String getOrgUnitTypes()
    {
        return _orgUnitTypes;
    }

    /**
     * @param orgUnitTypes Типы подразделений.
     */
    public void setOrgUnitTypes(String orgUnitTypes)
    {
        dirty(_orgUnitTypes, orgUnitTypes);
        _orgUnitTypes = orgUnitTypes;
    }

    /**
     * @return Подразделение.
     */
    @Length(max=255)
    public String getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(String orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Тип должности.
     */
    @Length(max=255)
    public String getEmployeePostType()
    {
        return _employeePostType;
    }

    /**
     * @param employeePostType Тип должности.
     */
    public void setEmployeePostType(String employeePostType)
    {
        dirty(_employeePostType, employeePostType);
        _employeePostType = employeePostType;
    }

    /**
     * @return Квалификационный уровень.
     */
    @Length(max=255)
    public String getQualificationLevel()
    {
        return _qualificationLevel;
    }

    /**
     * @param qualificationLevel Квалификационный уровень.
     */
    public void setQualificationLevel(String qualificationLevel)
    {
        dirty(_qualificationLevel, qualificationLevel);
        _qualificationLevel = qualificationLevel;
    }

    /**
     * @return Профессионально-квалификационная группа.
     */
    @Length(max=255)
    public String getProfQualificationGroup()
    {
        return _profQualificationGroup;
    }

    /**
     * @param profQualificationGroup Профессионально-квалификационная группа.
     */
    public void setProfQualificationGroup(String profQualificationGroup)
    {
        dirty(_profQualificationGroup, profQualificationGroup);
        _profQualificationGroup = profQualificationGroup;
    }

    /**
     * @return Должность.
     */
    @Length(max=255)
    public String getPost()
    {
        return _post;
    }

    /**
     * @param post Должность.
     */
    public void setPost(String post)
    {
        dirty(_post, post);
        _post = post;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StaffListAllocationReportGen)
        {
            setReportDate(((StaffListAllocationReport)another).getReportDate());
            setActivityType(((StaffListAllocationReport)another).getActivityType());
            setOrgUnitTypes(((StaffListAllocationReport)another).getOrgUnitTypes());
            setOrgUnit(((StaffListAllocationReport)another).getOrgUnit());
            setEmployeePostType(((StaffListAllocationReport)another).getEmployeePostType());
            setQualificationLevel(((StaffListAllocationReport)another).getQualificationLevel());
            setProfQualificationGroup(((StaffListAllocationReport)another).getProfQualificationGroup());
            setPost(((StaffListAllocationReport)another).getPost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffListAllocationReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffListAllocationReport.class;
        }

        public T newInstance()
        {
            return (T) new StaffListAllocationReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return obj.getReportDate();
                case "activityType":
                    return obj.getActivityType();
                case "orgUnitTypes":
                    return obj.getOrgUnitTypes();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "employeePostType":
                    return obj.getEmployeePostType();
                case "qualificationLevel":
                    return obj.getQualificationLevel();
                case "profQualificationGroup":
                    return obj.getProfQualificationGroup();
                case "post":
                    return obj.getPost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reportDate":
                    obj.setReportDate((String) value);
                    return;
                case "activityType":
                    obj.setActivityType((String) value);
                    return;
                case "orgUnitTypes":
                    obj.setOrgUnitTypes((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((String) value);
                    return;
                case "employeePostType":
                    obj.setEmployeePostType((String) value);
                    return;
                case "qualificationLevel":
                    obj.setQualificationLevel((String) value);
                    return;
                case "profQualificationGroup":
                    obj.setProfQualificationGroup((String) value);
                    return;
                case "post":
                    obj.setPost((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                        return true;
                case "activityType":
                        return true;
                case "orgUnitTypes":
                        return true;
                case "orgUnit":
                        return true;
                case "employeePostType":
                        return true;
                case "qualificationLevel":
                        return true;
                case "profQualificationGroup":
                        return true;
                case "post":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return true;
                case "activityType":
                    return true;
                case "orgUnitTypes":
                    return true;
                case "orgUnit":
                    return true;
                case "employeePostType":
                    return true;
                case "qualificationLevel":
                    return true;
                case "profQualificationGroup":
                    return true;
                case "post":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reportDate":
                    return String.class;
                case "activityType":
                    return String.class;
                case "orgUnitTypes":
                    return String.class;
                case "orgUnit":
                    return String.class;
                case "employeePostType":
                    return String.class;
                case "qualificationLevel":
                    return String.class;
                case "profQualificationGroup":
                    return String.class;
                case "post":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffListAllocationReport> _dslPath = new Path<StaffListAllocationReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffListAllocationReport");
    }
            

    /**
     * @return Дата отчета.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getReportDate()
     */
    public static PropertyPath<String> reportDate()
    {
        return _dslPath.reportDate();
    }

    /**
     * @return Вид деятельности.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getActivityType()
     */
    public static PropertyPath<String> activityType()
    {
        return _dslPath.activityType();
    }

    /**
     * @return Типы подразделений.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getOrgUnitTypes()
     */
    public static PropertyPath<String> orgUnitTypes()
    {
        return _dslPath.orgUnitTypes();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getOrgUnit()
     */
    public static PropertyPath<String> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Тип должности.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getEmployeePostType()
     */
    public static PropertyPath<String> employeePostType()
    {
        return _dslPath.employeePostType();
    }

    /**
     * @return Квалификационный уровень.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getQualificationLevel()
     */
    public static PropertyPath<String> qualificationLevel()
    {
        return _dslPath.qualificationLevel();
    }

    /**
     * @return Профессионально-квалификационная группа.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getProfQualificationGroup()
     */
    public static PropertyPath<String> profQualificationGroup()
    {
        return _dslPath.profQualificationGroup();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getPost()
     */
    public static PropertyPath<String> post()
    {
        return _dslPath.post();
    }

    public static class Path<E extends StaffListAllocationReport> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _reportDate;
        private PropertyPath<String> _activityType;
        private PropertyPath<String> _orgUnitTypes;
        private PropertyPath<String> _orgUnit;
        private PropertyPath<String> _employeePostType;
        private PropertyPath<String> _qualificationLevel;
        private PropertyPath<String> _profQualificationGroup;
        private PropertyPath<String> _post;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата отчета.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getReportDate()
     */
        public PropertyPath<String> reportDate()
        {
            if(_reportDate == null )
                _reportDate = new PropertyPath<String>(StaffListAllocationReportGen.P_REPORT_DATE, this);
            return _reportDate;
        }

    /**
     * @return Вид деятельности.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getActivityType()
     */
        public PropertyPath<String> activityType()
        {
            if(_activityType == null )
                _activityType = new PropertyPath<String>(StaffListAllocationReportGen.P_ACTIVITY_TYPE, this);
            return _activityType;
        }

    /**
     * @return Типы подразделений.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getOrgUnitTypes()
     */
        public PropertyPath<String> orgUnitTypes()
        {
            if(_orgUnitTypes == null )
                _orgUnitTypes = new PropertyPath<String>(StaffListAllocationReportGen.P_ORG_UNIT_TYPES, this);
            return _orgUnitTypes;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getOrgUnit()
     */
        public PropertyPath<String> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new PropertyPath<String>(StaffListAllocationReportGen.P_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Тип должности.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getEmployeePostType()
     */
        public PropertyPath<String> employeePostType()
        {
            if(_employeePostType == null )
                _employeePostType = new PropertyPath<String>(StaffListAllocationReportGen.P_EMPLOYEE_POST_TYPE, this);
            return _employeePostType;
        }

    /**
     * @return Квалификационный уровень.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getQualificationLevel()
     */
        public PropertyPath<String> qualificationLevel()
        {
            if(_qualificationLevel == null )
                _qualificationLevel = new PropertyPath<String>(StaffListAllocationReportGen.P_QUALIFICATION_LEVEL, this);
            return _qualificationLevel;
        }

    /**
     * @return Профессионально-квалификационная группа.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getProfQualificationGroup()
     */
        public PropertyPath<String> profQualificationGroup()
        {
            if(_profQualificationGroup == null )
                _profQualificationGroup = new PropertyPath<String>(StaffListAllocationReportGen.P_PROF_QUALIFICATION_GROUP, this);
            return _profQualificationGroup;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniemp.entity.report.StaffListAllocationReport#getPost()
     */
        public PropertyPath<String> post()
        {
            if(_post == null )
                _post = new PropertyPath<String>(StaffListAllocationReportGen.P_POST, this);
            return _post;
        }

        public Class getEntityClass()
        {
            return StaffListAllocationReport.class;
        }

        public String getEntityName()
        {
            return "staffListAllocationReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
