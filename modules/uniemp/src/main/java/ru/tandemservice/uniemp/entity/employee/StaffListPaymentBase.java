package ru.tandemservice.uniemp.entity.employee;

import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.catalog.PaymentUnit;
import ru.tandemservice.uniemp.entity.employee.gen.StaffListPaymentBaseGen;

/**
 * Базовая выплата штатного расписания
 */
public abstract class StaffListPaymentBase extends StaffListPaymentBaseGen
{
    public static final Object PAYMENT_UNIT_TYPE_KEY = new String[] { L_PAYMENT, Payment.L_PAYMENT_UNIT, PaymentUnit.P_TITLE };
    public static final Object PAYMENT_SOURCE_KEY = new String[] { L_FINANCING_SOURCE, FinancingSource.P_TITLE };
    public static final Object PAYMENT_TYPE_KEY = new String[] { L_PAYMENT, Payment.L_TYPE, PaymentType.P_TITLE };
    public static final Object PAYMENT_KEY = new String[] { L_PAYMENT, Payment.P_TITLE };

    public static final String P_TOTAL_VALUE = "totalValue";
    public static final String P_DELETE_DISABLED = "deleteDisabled";
    public static final String P_EDITING_DISABLED = "editingDisabled";

    protected abstract double getStaffRate();
    public abstract StaffListAllocationItem getTargetAllocItem();
    public abstract void setTargetAllocItem(StaffListAllocationItem allocItem);
    
    private boolean _considerAsAnAllocContext = false;
    
    public boolean isEditingDisabled()
    {
        return getStaffListItem().getStaffList().isCantBeEdited(); // TODO:
    }
    
    public boolean isDeleteDisabled()
    {
        return getStaffListItem().getStaffList().isCantBeEdited();
    }

    public Double getTotalValue()
    {
        Double specificPaymentSumInRubbles = getSpecificPaymentSumInRubbles();
        return null == specificPaymentSumInRubbles ? null : specificPaymentSumInRubbles;
    }

    protected Double getSpecificPaymentSumInRubbles()
    {
        if(null == getPayment()) return null;
        
        if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(getPayment().getPaymentUnit().getCode()) || getAmount() == 0)
        {
            return Math.round(getAmount()* (getPayment().isDoesntDependOnStaffRate() ? 1 : getStaffRate()) * 1000d) / 1000d;
        }
        else if (UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(getPayment().getPaymentUnit().getCode()))
        {
            return Math.round(getBaseSalaryRated() * getAmount() * 1000) / 1000d;
        }
        else if (UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(getPayment().getPaymentUnit().getCode()))
        {
            return Math.round(getBaseSalaryRated() * getAmount() * 10) / 1000d;
        }
        else if (UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(getPayment().getPaymentUnit().getCode()))
        {
            return UniempDaoFacade.getStaffListDAO().getMonthSalaryFundPaymentValue(this);
        }
        else
            throw new RuntimeException("Unsupported PaymentUnit code");
    }
    
    public double getBaseSalaryRated()
    {
        if(null != getTargetAllocItem())
            return getTargetAllocItem().getBaseSalary() * (getPayment().isDoesntDependOnStaffRate() ? 1 : getStaffRate());
        else
            return getStaffListItem().getSalary() * (getPayment().isDoesntDependOnStaffRate() ? 1 : getStaffRate());
    }
    
    public double getFullSalaryRated()
    {
        if(null != getTargetAllocItem()) return getTargetAllocItem().getMonthBaseSalaryFund();
        else return getBaseSalaryRated();
    }

    public boolean isConsiderAsAnAllocContext()
    {
        return _considerAsAnAllocContext;
    }
    public void setConsiderAsAnAllocContext(boolean considerAsAnAllocContext)
    {
        this._considerAsAnAllocContext = considerAsAnAllocContext;
    }
}