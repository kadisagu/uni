/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.StimulatingPaymentsList;

import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.RosobrReports;
import ru.tandemservice.uniemp.entity.report.RosobrReportStimPaymentPriority;
import ru.tandemservice.uniemp.entity.report.RosobrReportToPaymentRel;

/**
 * @author dseleznev
 * Created on: 01.02.2010
 */
 public class Model
{
    private DynamicListDataSource<Payment> _dataSource;
    
    private List<Payment> _compensationPayments; 
    
    private List<RosobrReports> _rosobrReports;
    
    Map<RosobrReports, List<IEntity>> _selectedIncludeRepPaymentsMap;
    Map<RosobrReports, List<IEntity>> _selectedSeparateRepPaymentsMap;
    List<IEntity> _selectedScDegreePaymentsList;
    
    Map<Payment, Map<RosobrReports, RosobrReportToPaymentRel>> _settingsMap;
    Map<Payment, RosobrReportStimPaymentPriority> _prioritiesMap;

    public DynamicListDataSource<Payment> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Payment> dataSource)
    {
        _dataSource = dataSource;
    }

    public List<Payment> getCompensationPayments()
    {
        return _compensationPayments;
    }

    public void setCompensationPayments(List<Payment> compensationPayments)
    {
        this._compensationPayments = compensationPayments;
    }

    public List<RosobrReports> getRosobrReports()
    {
        return _rosobrReports;
    }

    public void setRosobrReports(List<RosobrReports> rosobrReports)
    {
        this._rosobrReports = rosobrReports;
    }

    public Map<RosobrReports, List<IEntity>> getSelectedIncludeRepPaymentsMap()
    {
        return _selectedIncludeRepPaymentsMap;
    }

    public void setSelectedIncludeRepPaymentsMap(Map<RosobrReports, List<IEntity>> selectedIncludeRepPaymentsMap)
    {
        this._selectedIncludeRepPaymentsMap = selectedIncludeRepPaymentsMap;
    }

    public Map<RosobrReports, List<IEntity>> getSelectedSeparateRepPaymentsMap()
    {
        return _selectedSeparateRepPaymentsMap;
    }

    public void setSelectedSeparateRepPaymentsMap(Map<RosobrReports, List<IEntity>> selectedSeparateRepPaymentsMap)
    {
        this._selectedSeparateRepPaymentsMap = selectedSeparateRepPaymentsMap;
    }

    public List<IEntity> getSelectedScDegreePaymentsList()
    {
        return _selectedScDegreePaymentsList;
    }

    public void setSelectedScDegreePaymentsList(List<IEntity> selectedScDegreePaymentsList)
    {
        this._selectedScDegreePaymentsList = selectedScDegreePaymentsList;
    }

    public Map<Payment, Map<RosobrReports, RosobrReportToPaymentRel>> getSettingsMap()
    {
        return _settingsMap;
    }

    public void setSettingsMap(Map<Payment, Map<RosobrReports, RosobrReportToPaymentRel>> settingsMap)
    {
        this._settingsMap = settingsMap;
    }

    public Map<Payment, RosobrReportStimPaymentPriority> getPrioritiesMap()
    {
        return _prioritiesMap;
    }

    public void setPrioritiesMap(Map<Payment, RosobrReportStimPaymentPriority> prioritiesMap)
    {
        this._prioritiesMap = prioritiesMap;
    }
}