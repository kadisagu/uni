/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.qualityConsistencyForList.Add;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 20.04.2009
 */
public class Model
{
    // addform data
    private Date _reportDate;

    // Report print data
    private List<PostBoundedWithQGandQL> _ppsPostList;
    private List<EmployeePost> _employeePostList;
    private Map<Person, List<ScienceStatus>> _personAcademicStatusCodeMap;
    private Map<Person, List<ScienceDegree>> _personAcademicDegreeCodeMap;

    // Getters & Setters

    public List<PostBoundedWithQGandQL> getPpsPostList()
    {
        return _ppsPostList;
    }

    public void setPpsPostList(List<PostBoundedWithQGandQL> ppsPostList)
    {
        _ppsPostList = ppsPostList;
    }

    public Map<Person, List<ScienceStatus>> getPersonAcademicStatusCodeMap()
    {
        return _personAcademicStatusCodeMap;
    }

    public void setPersonAcademicStatusCodeMap(Map<Person, List<ScienceStatus>> personAcademicStatusCodeMap)
    {
        _personAcademicStatusCodeMap = personAcademicStatusCodeMap;
    }

    public Map<Person, List<ScienceDegree>> getPersonAcademicDegreeCodeMap()
    {
        return _personAcademicDegreeCodeMap;
    }

    public void setPersonAcademicDegreeCodeMap(Map<Person, List<ScienceDegree>> personAcademicDegreeCodeMap)
    {
        _personAcademicDegreeCodeMap = personAcademicDegreeCodeMap;
    }

    public List<EmployeePost> getEmployeePostList()
    {
        return _employeePostList;
    }

    public void setEmployeePostList(List<EmployeePost> employeePostList)
    {
        _employeePostList = employeePostList;
    }

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        this._reportDate = reportDate;
    }
}