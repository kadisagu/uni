/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeePaymentAddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
@Input(keys = { "employeePostId", "rowId" }, bindings = { "employeePostId", "employeePayment.id" })
public class Model
{
    private Long _employeePostId;
    private EmployeePost _employeePost;
    private EmployeePayment _employeePayment = new EmployeePayment();
    private List<HSelectOption> _paymentTypesList;
    private ISelectModel _paymentsModel;
    private PaymentType _paymentType;
    private ISelectModel _financingSourceItemsListModel;
    
    private List<FinancingSource> _financingSourcesList;

    private boolean _paymentValueDisabled = false;

    private boolean _showRecalculateButton = true;

    //Getters & Setters

    public boolean isShowRecalculateButton()
    {
        return _showRecalculateButton;
    }

    public void setShowRecalculateButton(boolean showRecalculateButton)
    {
        _showRecalculateButton = showRecalculateButton;
    }

    public boolean isPaymentValueDisabled()
    {
        return _paymentValueDisabled;
    }

    public void setPaymentValueDisabled(boolean paymentValueDisabled)
    {
        _paymentValueDisabled = paymentValueDisabled;
    }

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        this._employeePostId = employeePostId;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        this._employeePost = employeePost;
    }

    public EmployeePayment getEmployeePayment()
    {
        return _employeePayment;
    }

    public void setEmployeePayment(EmployeePayment employeePayment)
    {
        this._employeePayment = employeePayment;
    }

    public List<HSelectOption> getPaymentTypesList()
    {
        return _paymentTypesList;
    }

    public void setPaymentTypesList(List<HSelectOption> paymentTypesList)
    {
        this._paymentTypesList = paymentTypesList;
    }

    public ISelectModel getPaymentsModel()
    {
        return _paymentsModel;
    }

    public void setPaymentsModel(ISelectModel paymentsModel)
    {
        this._paymentsModel = paymentsModel;
    }

    public PaymentType getPaymentType()
    {
        return _paymentType;
    }

    public void setPaymentType(PaymentType paymentType)
    {
        this._paymentType = paymentType;
    }

    public List<FinancingSource> getFinancingSourcesList()
    {
        return _financingSourcesList;
    }

    public void setFinancingSourcesList(List<FinancingSource> financingSourcesList)
    {
        this._financingSourcesList = financingSourcesList;
    }

    public ISelectModel getFinancingSourceItemsListModel()
    {
        return _financingSourceItemsListModel;
    }

    public void setFinancingSourceItemsListModel(ISelectModel financingSourceItemsListModel)
    {
        this._financingSourceItemsListModel = financingSourceItemsListModel;
    }
}