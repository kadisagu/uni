package ru.tandemservice.uniemp.component.reports.quantityEmployeeByDepartments.quantityEmployeeByDepartmentsAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfBox;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport;

import java.util.*;

@SuppressWarnings("deprecation")
public class QuantityEmpByDepReportBuilder
{
    private Model _model;
    private Session _session;

// TODO: temporal fix    
    public static final int NOMINATIVE_CASE = 0;
    public static final int GENITIVE_CASE = 1;
    public static final int DATIVE_CASE = 2;
    public static final int ACCUSATIVE_CASE = 3;
    public static final int INSTRUMENTAL_CASE = 4;
    public static final int PREPOSITIONAL_CASE = 5;

    @SuppressWarnings("unchecked")
    static final Comparator orgUniComparator = new Comparator<OrgUnit>()
    {
        @Override
        public int compare(OrgUnit o1, OrgUnit o2)
        {
            int result = o1.getOrgUnitType().getPriority() - o2.getOrgUnitType().getPriority();
            return (result != 0) ? result : o1.getTitle().compareTo(o2.getTitle());
        }
    };

    public QuantityEmpByDepReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("unchecked")
    private byte[] buildReport()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_QUANTITY_EMP_DEP);
        RtfDocument document = new RtfReader().read(templateDocument.getContent());

        // типы должностей сотрудников
        MQBuilder EmpTypeBuilder = new MQBuilder(EmployeeType.ENTITY_NAME, "et");
        EmpTypeBuilder.addOrder("et", EmployeeType.code().toString());
        final List<EmployeeType> employeeTypesList = EmpTypeBuilder.getResultList(_session);

        final int empTypeSize = employeeTypesList.size();

        // список элементов заголовка
        final List<String> headerStr = new ArrayList<>(employeeTypesList.size() + 1);
        for (EmployeeType ep : employeeTypesList)
        {
            headerStr.add(ep.getShortTitle());
        }
        headerStr.add("Всего");

        // все подразделения
        MQBuilder allOrgUnitBuilder = new MQBuilder(OrgUnit.ENTITY_NAME, "ou");
        allOrgUnitBuilder.add(MQExpression.eq("ou", OrgUnit.archival().toString(), Boolean.FALSE));
        List<OrgUnit> allOrgUnits = allOrgUnitBuilder.getResultList(_session);

        // подразделения выбранные для отчета
        List<OrgUnit> orgUnitList = getOrgUnitList(allOrgUnits);
        List<HSelectOption> orgUnits = listHierarchyNodes(orgUnitList, orgUniComparator, true);
        //List<HSelectOption> orgUnits = HierarchyUtil.listHierarchyNodesWithParents(orgUnitList, orgUniComparator, true);

        // сотрудники на должностях
        MQBuilder EmployeePostBuilder = new MQBuilder(EmployeePost.ENTITY_NAME, "ep", new String[] { EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().id().s(), EmployeePost.orgUnit().id().s() });
        // DEV-4660
        EmployeePostBuilder.add(MQExpression.lessOrEq("ep", EmployeePost.postDate().s(), _model.getReport().getReportDate()));
        EmployeePostBuilder.add(MQExpression.or(MQExpression.greatOrEq("ep", EmployeePost.dismissalDate().s(), _model.getReport().getReportDate()), MQExpression.isNull("ep", EmployeePost.dismissalDate().s())));
        if (null != _model.getReport().getOrgUnit())
        {
            if(_model.getReport().getViewChildOrgUnit())
            {
                EmployeePostBuilder.add(MQExpression.in("ep", EmployeePost.orgUnit().s(), orgUnitList));
            }
            else
            {
                EmployeePostBuilder.add(MQExpression.eq("ep", EmployeePost.orgUnit().s(), _model.getReport().getOrgUnit()));
            }
        }
        else if (null != _model.getReport().getOrgUnitType() && !_model.getReport().getViewChildOrgUnit())
        {
            EmployeePostBuilder.add(MQExpression.eq("ep", EmployeePost.orgUnit().orgUnitType().s(), _model.getReport().getOrgUnitType()));
        }

        List<Object[]> empPostList = EmployeePostBuilder.getResultList(_session);

        // idOrgUnit -> row
        Map<Long, int[]> idOrgUnit2postType = new HashMap<>(orgUnitList.size());
        for (OrgUnit ou : orgUnitList)
        {
            int sum = 0;
            int[] row = new int[empTypeSize + 1];
            for (int k = 0; k < empTypeSize; k++)
            {
                sum += row[k] = getCount(ou.getId(), employeeTypesList.get(k).getId(), empPostList);
            }
            row[empTypeSize] = sum;
            idOrgUnit2postType.put(ou.getId(), row);
        }

        // суммы сотрудников для родительских подразделений из дочерних
        Map<Long, int[]> idOrgUnit2sums = collectRows(idOrgUnit2postType, orgUnitList);

        // берем идентификаторы подразделений после которых должно быть итого родительского подраздления
        Set<Long> idsBefore = new HashSet<>();
        for (OrgUnit ou : orgUnitList)
        {
            List<OrgUnit> t = new ArrayList<>();
            for (OrgUnit out : orgUnitList)
            {
                if (null != out.getParent())
                {
                    if (out.getParent().equals(ou))
                    {
                        t.add(out);
                    }
                }
            }
            if (!t.isEmpty())
            {
                Collections.sort(t, orgUniComparator);
                idsBefore.add(t.get(t.size() - 1).getId());
            }
        }

        // формируем данные для отчета
        final List<Integer> orgUnitsLevels = new ArrayList<>();
        final List<Integer> orgUnitsItalic = new ArrayList<>();
        List<String[]> data = new ArrayList<>();
        int rowDataSize = empTypeSize + 2;

        for (HSelectOption option : orgUnits)
        {
            OrgUnit ou = (OrgUnit)option.getObject();
            data.add(getRowData(getOrgUnitPrintingTitle(ou, NOMINATIVE_CASE, false, false), rowDataSize, idOrgUnit2postType.get(ou.getId())));
            orgUnitsLevels.add(option.getLevel());
            orgUnitsItalic.add(0);

            while (idsBefore.contains(ou.getId()))
            {
                OrgUnit ouParent = ou.getParent();
                String title = "Итого по " + getOrgUnitPrintingTitle(ouParent, DATIVE_CASE, true, true);
                orgUnitsLevels.add(option.getLevel() - 1);
                orgUnitsItalic.add(1);
                data.add(getRowData(title, rowDataSize, idOrgUnit2sums.get(ouParent.getId())));
                ou = ouParent;
            }
        }

        if (data.size() > 0)
        {
            data.add(getRowData("ИТОГО:", rowDataSize, sumRows(idOrgUnit2postType, rowDataSize - 1)));
            orgUnitsLevels.add(0);
            orgUnitsItalic.add(0);
        }

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", data.toArray(new String[][] {}));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                int[] parts = new int[empTypeSize + 1];
                Arrays.fill(parts, 1);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 1, (newCell, index) -> {
                    final IRtfElement rtfText = RtfBean.getElementFactory().createRtfText(headerStr.get(index));
                    newCell.getElementList().add(rtfText);
                    UniRtfUtil.setCellBold(newCell);
                }, parts);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 1, null, parts);
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int i = startIndex; i < newRowList.size(); i++)
                {
                    int level = orgUnitsLevels.get(i - startIndex);
                    int italic = orgUnitsItalic.get(i - startIndex);
                    if (level > 0)
                    {
                        RtfBox oldCellMargin = newRowList.get(i).getCellList().get(0).getCellMargin();
                        RtfBox newCellMargin = RtfBox.getInstance(
                                oldCellMargin != null ? oldCellMargin.getTop() : 0,
                                oldCellMargin != null ? oldCellMargin.getBottom() : 0,
                                120 + 120 * (level - 1),
                                oldCellMargin != null ? oldCellMargin.getRight() : 0);
                        newRowList.get(i).getCellList().get(0).setCellMargin(newCellMargin);
//                                getCellMargin().setLeft(120 + 120 * (level - 1));
                    }
                    if (italic == 1)
                    {
                        for (RtfCell cell2 : newRowList.get(i).getCellList())
                        {
                            UniRtfUtil.setCellItalic(cell2);
                        }
                    }
                    if ((i == newRowList.size() - 1) && (newRowList.size() > 1))
                    {
                        UniRtfUtil.setRowBold(newRowList.get(i));
                    }
                }
            }
        });

        tableModifier.modify(document);
        return RtfUtil.toByteArray(document);
    }

    // ряды с суммами по подразделениям
    private Map<Long, int[]> collectRows(Map<Long, int[]> idOrgUnit2postType, List<OrgUnit> orgUnitsList)
    {
        Map<Long, int[]> idOrgUnit2sums = new HashMap<>();
        if (orgUnitsList.isEmpty())
            return idOrgUnit2sums;

        int size = idOrgUnit2postType.get(orgUnitsList.get(0).getId()).length;
        for (OrgUnit ou : orgUnitsList)
        {
            if (!idOrgUnit2sums.containsKey(ou.getId()))
            {
                int[] row = idOrgUnit2postType.containsKey(ou.getId()) ? idOrgUnit2postType.get(ou.getId()) : new int[size];
                idOrgUnit2sums.put(ou.getId(), row.clone());
            }

            int[] ouRow = idOrgUnit2postType.containsKey(ou.getId()) ? idOrgUnit2postType.get(ou.getId()) : new int[size];
            while (ou.getParent() != null)
            {
                int[] parentRowSum = idOrgUnit2sums.containsKey(ou.getParent().getId()) ? idOrgUnit2sums.remove(ou.getParent().getId()) : new int[size];
                for (int i = 0; i < size; i++)
                {
                    parentRowSum[i] += ouRow[i];
                }
                idOrgUnit2sums.put(ou.getParent().getId(), parentRowSum);
                ou = ou.getParent();
            }
        }
        return idOrgUnit2sums;
    }

    // итогоговый ряд таблицы
    private int[] sumRows(Map<Long, int[]> idOrgUnit2postType, int size)
    {
        int[] rowSum = new int[size];
        if (!idOrgUnit2postType.isEmpty())
        {
            for (int[] row : idOrgUnit2postType.values())
            {
                for (int i = 0; i < row.length; i++)
                {
                    rowSum[i] += row[i];
                }
            }
        }
        return rowSum;
    }

    // возвращает число сотдуников с данным типом должности для данного типа подразделения
    private int getCount(Long orgUnitId, Long empTypeId, List<Object[]> empPostList)
    {
        int result = 0;
        for (Object[] anEmpPostList : empPostList)
        {
            if (orgUnitId.equals(anEmpPostList[1]) && empTypeId.equals(anEmpPostList[0]))
            {
                result++;
            }
        }
        return result;
    }

    // строка таблицы отчета
    public String[] getRowData(String title, int size, int[] data)
    {
        List<String> row = new ArrayList<>(size);
        row.add(title);

        if (null != data)
        {
            for (int element : data)
            {
                row.add(String.valueOf(element));
            }
        }

        return row.toArray(new String[row.size()]);
    }

    private List<OrgUnit> getOrgUnitList(List<OrgUnit> allOrgUnits)
    {
        // параметры постороения отчета
        QuantityEmpByDepReport report = _model.getReport();
        OrgUnitType orgUnitType = report.getOrgUnitType();
        OrgUnit orgUnit = report.getOrgUnit();
        Boolean viewChildOrgUnit = report.getViewChildOrgUnit();

        List<OrgUnit> orgUnits;
        Set<OrgUnit> orgUnitsWithChild = new HashSet<>();

        MQBuilder ouBuilder = new MQBuilder(OrgUnit.ENTITY_NAME, "ou");
        ouBuilder.add(MQExpression.eq("ou", OrgUnit.archival().s(), Boolean.FALSE));
        if (orgUnitType != null)
        {
            ouBuilder.add(MQExpression.eq("ou", OrgUnit.orgUnitType().s(), orgUnitType));
        }
        if (orgUnit != null)
        {
            ouBuilder.add(MQExpression.eq("ou", OrgUnit.id().s(), orgUnit.getId()));
        }
        orgUnits = ouBuilder.getResultList(_session);

        if (!(orgUnit == null && orgUnitType == null) && viewChildOrgUnit)
        {
            for (OrgUnit ou : orgUnits)
            {
                orgUnitsWithChild.addAll(OrgUnitManager.instance().dao().getChildOrgUnits(ou, true, false));
            }
            if (!orgUnitsWithChild.isEmpty())
            {
                orgUnits.addAll(orgUnitsWithChild);
            }
        }
        return orgUnits;
    }

    @SuppressWarnings("unchecked")
    public static <T extends IHierarchyItem> List<HSelectOption> listHierarchyNodes(final List<T> nodes, Comparator comparator, boolean allSelectable)
    {
        Collections.sort(nodes, comparator);

        //создаем плоское иерархичное дерево
        PlaneTree planeTree = new PlaneTree(nodes.toArray(new IHierarchyItem[nodes.size()]));
        IHierarchyItem[] hierarchyItems = planeTree.getFlatTreeObjects();
        int[] levels = planeTree.getLevels();

        //создаем опции для селекта
        List<HSelectOption> options = new ArrayList<>();
        for (int i = 0; i < hierarchyItems.length; i++)
        {
            options.add(new HSelectOption(hierarchyItems[i], levels[i], allSelectable || nodes.contains(hierarchyItems[i])));
        }

        return options;
    }
    
    public static String getOrgUnitPrintingTitle(OrgUnit orgUnit, int caseNumber, boolean uncapitalize, boolean quoted)
    {
        if(null == orgUnit) return "";
        
        String orgUnitTitle;
        String divisionTitle = null;
        switch (caseNumber)
        {
            case NOMINATIVE_CASE: 
                orgUnitTitle = orgUnit.getNominativeCaseTitle();
                divisionTitle = "";
                break;
            case GENITIVE_CASE: 
                orgUnitTitle = orgUnit.getGenitiveCaseTitle();
                divisionTitle = "подразделения";
                break;
            case DATIVE_CASE: 
                orgUnitTitle =  orgUnit.getDativeCaseTitle();
                divisionTitle = "подразделению";
                break;
            case ACCUSATIVE_CASE: 
                orgUnitTitle =  orgUnit.getAccusativeCaseTitle();
                divisionTitle = "подразделение";
                break;
            case INSTRUMENTAL_CASE: 
                orgUnitTitle =  orgUnit.getFullTitle();
                divisionTitle = "подразделением";
                break;
            case PREPOSITIONAL_CASE: 
                orgUnitTitle =  orgUnit.getPrepositionalCaseTitle();
                divisionTitle = "подразделении";
                break;
            default: orgUnitTitle = orgUnit.getFullTitle();
        }
        return null != orgUnitTitle ? (uncapitalize ? StringUtils.uncapitalize(orgUnitTitle) : orgUnitTitle)
                : (divisionTitle + " " + (quoted ? "«" : "") + orgUnit.getTypeTitle() + (quoted ? "»" : ""));
    }
}