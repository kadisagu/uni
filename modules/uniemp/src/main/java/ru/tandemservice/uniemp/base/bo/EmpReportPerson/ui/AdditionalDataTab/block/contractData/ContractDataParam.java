/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.contractData;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

import java.util.Date;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class ContractDataParam implements IReportDQLModifier
{
    private IReportParam<String> _labContrNumber = new ReportParam<>();
    private IReportParam<Date> _labContrBeginFrom = new ReportParam<>();
    private IReportParam<Date> _labContrBeginTo = new ReportParam<>();
    private IReportParam<Date> _labContrEndFrom = new ReportParam<>();
    private IReportParam<Date> _labContrEndTo = new ReportParam<>();
    private IReportParam<List<LabourContractType>> _labContrType = new ReportParam<>();
    private IReportParam<String> _collatAgreemntNumber = new ReportParam<>();
    private IReportParam<Date> _collatAgreemntFrom = new ReportParam<>();
    private IReportParam<Date> _collatAgreemntTo = new ReportParam<>();

    private String contractAlias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = dql.innerJoinEntity(Employee.class, Employee.person());

        // соединяем должность
        String employeePostAlias = dql.innerJoinEntity(employeeAlias, EmployeePost.class, EmployeePost.employee());

        // соединяем трудовой договор
        return dql.innerJoinEntity(employeePostAlias, EmployeeLabourContract.class, EmployeeLabourContract.employeePost());
    }

    private String agreementAlias(ReportDQL dql)
    {
        // соединяем доп. соглашение
        return dql.innerJoinEntity(contractAlias(dql), ContractCollateralAgreement.class, ContractCollateralAgreement.contract());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_labContrNumber.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "contractData.labContrNumber", _labContrNumber.getData());

            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(EmployeeLabourContract.number().fromAlias(contractAlias(dql))), DQLExpressions.value(_labContrNumber.getData())));
        }

        if (_labContrBeginFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "contractData.labContrBeginFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_labContrBeginFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeLabourContract.beginDate().fromAlias(contractAlias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_labContrBeginFrom.getData()))));
        }

        if (_labContrBeginTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "contractData.labContrBeginTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_labContrBeginTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeLabourContract.beginDate().fromAlias(contractAlias(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_labContrBeginTo.getData(), 1))));
        }

        if (_labContrEndFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "contractData.labContrEndFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_labContrEndFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeLabourContract.endDate().fromAlias(contractAlias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_labContrEndFrom.getData()))));
        }

        if (_labContrEndTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "contractData.labContrEndTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_labContrEndTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(EmployeeLabourContract.endDate().fromAlias(contractAlias(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_labContrEndTo.getData(), 1))));
        }

        if (_labContrType.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "contractData.labContrType", CommonBaseStringUtil.join(_labContrType.getData(), LabourContractType.P_TITLE, ","));

            dql.builder.where(DQLExpressions.in(DQLExpressions.property(EmployeeLabourContract.type().fromAlias(contractAlias(dql))), _labContrType.getData()));
        }

        if (_collatAgreemntNumber.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "contractData.collatAgreemntNumber", _collatAgreemntNumber.getData());

            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(ContractCollateralAgreement.number().fromAlias(agreementAlias(dql))), DQLExpressions.value(_collatAgreemntNumber.getData())));
        }

        if (_collatAgreemntFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "contractData.collatAgreemntFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_collatAgreemntFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(ContractCollateralAgreement.date().fromAlias(agreementAlias(dql))), DQLExpressions.valueDate(CoreDateUtils.getDayFirstTimeMoment(_collatAgreemntFrom.getData()))));
        }

        if (_collatAgreemntTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "contractData.collatAgreemntTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_collatAgreemntTo.getData()));

            dql.builder.where(DQLExpressions.lt(DQLExpressions.property(ContractCollateralAgreement.date().fromAlias(agreementAlias(dql))), DQLExpressions.valueDate(CoreDateUtils.getNextDayFirstTimeMoment(_collatAgreemntTo.getData(), 1))));
        }
    }

    // Getters

    public IReportParam<String> getLabContrNumber()
    {
        return _labContrNumber;
    }

    public IReportParam<Date> getLabContrBeginFrom()
    {
        return _labContrBeginFrom;
    }

    public IReportParam<Date> getLabContrBeginTo()
    {
        return _labContrBeginTo;
    }

    public IReportParam<Date> getLabContrEndFrom()
    {
        return _labContrEndFrom;
    }

    public IReportParam<Date> getLabContrEndTo()
    {
        return _labContrEndTo;
    }

    public IReportParam<List<LabourContractType>> getLabContrType()
    {
        return _labContrType;
    }

    public IReportParam<String> getCollatAgreemntNumber()
    {
        return _collatAgreemntNumber;
    }

    public IReportParam<Date> getCollatAgreemntFrom()
    {
        return _collatAgreemntFrom;
    }

    public IReportParam<Date> getCollatAgreemntTo()
    {
        return _collatAgreemntTo;
    }
}
