package ru.tandemservice.uniemp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строки отчета «ВПО-1 - Сведения о персонале учреждения»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeVPO1ReportLinesGen extends EntityBase
 implements INaturalIdentifiable<EmployeeVPO1ReportLinesGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines";
    public static final String ENTITY_NAME = "employeeVPO1ReportLines";
    public static final int VERSION_HASH = 1225164871;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_PARENT = "parent";
    public static final String P_LINE_CODE = "lineCode";
    public static final String P_PREFIX = "prefix";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private EmployeeVPO1ReportLines _parent;     // Строки отчета «ВПО-1 - Сведения о персонале учреждения»
    private String _lineCode;     // Код строки
    private String _prefix;     // Печатный префикс
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Строки отчета «ВПО-1 - Сведения о персонале учреждения».
     */
    public EmployeeVPO1ReportLines getParent()
    {
        return _parent;
    }

    /**
     * @param parent Строки отчета «ВПО-1 - Сведения о персонале учреждения».
     */
    public void setParent(EmployeeVPO1ReportLines parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Код строки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLineCode()
    {
        return _lineCode;
    }

    /**
     * @param lineCode Код строки. Свойство не может быть null.
     */
    public void setLineCode(String lineCode)
    {
        dirty(_lineCode, lineCode);
        _lineCode = lineCode;
    }

    /**
     * @return Печатный префикс.
     */
    @Length(max=255)
    public String getPrefix()
    {
        return _prefix;
    }

    /**
     * @param prefix Печатный префикс.
     */
    public void setPrefix(String prefix)
    {
        dirty(_prefix, prefix);
        _prefix = prefix;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeVPO1ReportLinesGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EmployeeVPO1ReportLines)another).getCode());
            }
            setParent(((EmployeeVPO1ReportLines)another).getParent());
            setLineCode(((EmployeeVPO1ReportLines)another).getLineCode());
            setPrefix(((EmployeeVPO1ReportLines)another).getPrefix());
            setTitle(((EmployeeVPO1ReportLines)another).getTitle());
        }
    }

    public INaturalId<EmployeeVPO1ReportLinesGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EmployeeVPO1ReportLinesGen>
    {
        private static final String PROXY_NAME = "EmployeeVPO1ReportLinesNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EmployeeVPO1ReportLinesGen.NaturalId) ) return false;

            EmployeeVPO1ReportLinesGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeVPO1ReportLinesGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeVPO1ReportLines.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeVPO1ReportLines();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "parent":
                    return obj.getParent();
                case "lineCode":
                    return obj.getLineCode();
                case "prefix":
                    return obj.getPrefix();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "parent":
                    obj.setParent((EmployeeVPO1ReportLines) value);
                    return;
                case "lineCode":
                    obj.setLineCode((String) value);
                    return;
                case "prefix":
                    obj.setPrefix((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "parent":
                        return true;
                case "lineCode":
                        return true;
                case "prefix":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "parent":
                    return true;
                case "lineCode":
                    return true;
                case "prefix":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "parent":
                    return EmployeeVPO1ReportLines.class;
                case "lineCode":
                    return String.class;
                case "prefix":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeVPO1ReportLines> _dslPath = new Path<EmployeeVPO1ReportLines>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeVPO1ReportLines");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Строки отчета «ВПО-1 - Сведения о персонале учреждения».
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getParent()
     */
    public static EmployeeVPO1ReportLines.Path<EmployeeVPO1ReportLines> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Код строки. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getLineCode()
     */
    public static PropertyPath<String> lineCode()
    {
        return _dslPath.lineCode();
    }

    /**
     * @return Печатный префикс.
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getPrefix()
     */
    public static PropertyPath<String> prefix()
    {
        return _dslPath.prefix();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EmployeeVPO1ReportLines> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EmployeeVPO1ReportLines.Path<EmployeeVPO1ReportLines> _parent;
        private PropertyPath<String> _lineCode;
        private PropertyPath<String> _prefix;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EmployeeVPO1ReportLinesGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Строки отчета «ВПО-1 - Сведения о персонале учреждения».
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getParent()
     */
        public EmployeeVPO1ReportLines.Path<EmployeeVPO1ReportLines> parent()
        {
            if(_parent == null )
                _parent = new EmployeeVPO1ReportLines.Path<EmployeeVPO1ReportLines>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Код строки. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getLineCode()
     */
        public PropertyPath<String> lineCode()
        {
            if(_lineCode == null )
                _lineCode = new PropertyPath<String>(EmployeeVPO1ReportLinesGen.P_LINE_CODE, this);
            return _lineCode;
        }

    /**
     * @return Печатный префикс.
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getPrefix()
     */
        public PropertyPath<String> prefix()
        {
            if(_prefix == null )
                _prefix = new PropertyPath<String>(EmployeeVPO1ReportLinesGen.P_PREFIX, this);
            return _prefix;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniemp.entity.catalog.EmployeeVPO1ReportLines#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EmployeeVPO1ReportLinesGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EmployeeVPO1ReportLines.class;
        }

        public String getEntityName()
        {
            return "employeeVPO1ReportLines";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
