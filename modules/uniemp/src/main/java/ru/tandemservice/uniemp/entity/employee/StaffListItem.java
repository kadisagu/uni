package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.sec.ISecLocalEntityOwner;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.gen.StaffListItemGen;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;

public class StaffListItem extends StaffListItemGen implements ISecLocalEntityOwner
{
    public static final String[] POST_TITLE_KEY = new String[] { StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.P_TITLE };
    public static final String[] POST_TITLE_WITH_SALARY_KEY = new String[] { StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.P_TITLE_WITH_SALARY };
    public static final String[] ORG_UNIT_TITLE_KEY = new String[] { StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT, OrgUnit.P_FULL_TITLE };
    public static final String[] POST_SIMPLE_TITLE_KEY = new String[] { StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.P_TITLE };
    public static final String[] POST_TYPE_KEY = new String[] { StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_POST, Post.L_EMPLOYEE_TYPE, EmployeeType.P_SHORT_TITLE };
    public static final String[] QUALIFICATION_LEVEL_KEY = new String[] { StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, QualificationLevel.P_SHORT_TITLE };
    public static final String[] PROF_QUALIFICATION_GROUP_KEY = new String[] { StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, ProfQualificationGroup.P_SHORT_TITLE };
    public static final String[] ETKS_KEY = new String[] { StaffListItem.L_ORG_UNIT_POST_RELATION, OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.L_ETKS_LEVELS, EtksLevels.P_TITLE };


//    public static final String P_STAFF_RATE_SUMM = "staffRateSumm";
    public static final String P_OCC_STAFF_RATE = "occStaffRate";

    public static final String P_REMAINING_STAFF_RATE = "remainingStaffRate";
    public static final String P_REMAINING_STAFF_RATE_STR = "remainingStaffRateStr";

    
    public static final String P_MONTH_BASE_SALARY_FUND = "monthBaseSalaryFund";
    public static final String P_TARGET_SALARY_FUND = "targetSalaryFund";
    
    public static final String P_CANT_BE_DELETED = "cantBeDeleted";

    public static String P_STAFF_RATE = "staffRate";

    public void setStaffRate(double value)
    {
        setStaffRateInteger((int) (value * 10000));
    }

    public double getStaffRate()
    {
        BigDecimal x = new java.math.BigDecimal(getStaffRateInteger() * 0.0001);
        x = x.setScale(3, BigDecimal.ROUND_HALF_UP);

        return x.doubleValue();
    }

    // кол-во занятых штатных единиц
    public Double getOccStaffRate()
    {
        return UniempDaoFacade.getUniempDAO().getStaffRateForOrgUnitPost(getStaffList(), getOrgUnitPostRelation(), getOrgUnitPostRelation().getOrgUnit(), getFinancingSource(), getFinancingSourceItem());
    }

    public Double getRemainingStaffRate()
    {
        return Math.round((getStaffRate() - getOccStaffRate()) * 100) / 100.0;
    }

    public String getRemainingStaffRateStr()
    {
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getRemainingStaffRate());
    }
    
    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return Collections.<IEntity>singletonList(this.getStaffList().getOrgUnit());
    }
    
    public Double getMonthBaseSalaryFund()
    {
        return getSalary() * getStaffRate();
    }
    
    public Double getTargetSalaryFund()
    {
        return getMonthBaseSalaryFund() + UniempDaoFacade.getStaffListDAO().getSumOfPayments(this, true);
    }
    
    public boolean isCantBeDeleted()
    {
        return false; //TODO:
    }
}