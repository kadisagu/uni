/* $Id$ */
package ru.tandemservice.uniemp.entity.catalog;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

/**
 * @author esych
 * Created on: 17.01.2011
 */
public interface ITitledWithCases extends ITitled
{
    String P_NOMINATIVE_CASE_TITLE = "nominativeCaseTitle";
    String P_GENITIVE_CASE_TITLE = "genitiveCaseTitle";
    String P_DATIVE_CASE_TITLE = "dativeCaseTitle";
    String P_ACCUSATIVE_CASE_TITLE = "accusativeCaseTitle";
    String P_INSTRUMENTAL_CASE_TITLE = "instrumentalCaseTitle";
    String P_PREPOSITIONAL_CASE_TITLE = "prepositionalCaseTitle";

    String getNominativeCaseTitle();
    void setNominativeCaseTitle(String nominativeCaseTitle);

    String getGenitiveCaseTitle();
    void setGenitiveCaseTitle(String genitiveCaseTitle);

    String getDativeCaseTitle();
    void setDativeCaseTitle(String dativeCaseTitle);

    String getAccusativeCaseTitle();
    void setAccusativeCaseTitle(String accusativeCaseTitle);

    String getInstrumentalCaseTitle();
    void setInstrumentalCaseTitle(String instrumentalCaseTitle);

    String getPrepositionalCaseTitle();
    void setPrepositionalCaseTitle(String prepositionalCaseTitle);

    String getCaseTitle(GrammaCase caze);
}
