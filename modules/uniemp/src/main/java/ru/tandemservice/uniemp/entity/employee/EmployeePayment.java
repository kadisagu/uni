package ru.tandemservice.uniemp.entity.employee;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.gen.EmployeePaymentGen;

public class EmployeePayment extends EmployeePaymentGen
{
    public static final String[] KEY_PAYMENT_TITLE = new String[] { L_PAYMENT, Payment.P_TITLE };
    public static final String[] KEY_PAYMENT_TYPE_TITLE = new String[] { L_PAYMENT, Payment.L_TYPE, PaymentType.P_TITLE };
    public static final String[] KEY_FINANCING_SOURCE_TITLE = new String[] { L_FINANCING_SOURCE, FinancingSource.P_TITLE };
    //public static final String[] KEY_FINANCING_ORG_UNIT_TITLE = new String[] { L_FINANCING_ORG_UNIT, OrgUnit.P_FULL_TITLE };

    public static final String P_START_DATE_STR = "startDateStr";

    public String getStartDateStr()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getAssignDate());
    }
    /**
    * выводится в формате «<Название типа выплаты> с <Назначена с даты в формате хх.хх.хххх> [по <Назначена по дату в формате хх.хх.хххх> (выводится только если задана)]»
    */
    public String getTitleDate()
    {
        return getPayment().getFullTitle() + " с " + getStartDateStr() +
                (getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate()) : "");
    }

    /**
     * Формат вывода: «<Название выплаты> в размере <размер выплаты> <формат ввода_РП> c <Назначена с даты>[ по <Назначена по дату>]»
     */
    public String getTitle()
    {
        String resultString;

        resultString = (getPayment().getNominativeCaseTitle() != null ? getPayment().getNominativeCaseTitle() : getPayment().getFullTitle()) +
                " в размере " + (getPayment().getValue() != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(getPayment().getValue()) : "") + " "
                + (getPayment().getPaymentUnit().getGenitiveCaseTitle() != null ? getPayment().getPaymentUnit().getGenitiveCaseTitle(): "")
                + " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getBeginDate());
        resultString += getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getEndDate()) : "";

        return resultString;
    }
}