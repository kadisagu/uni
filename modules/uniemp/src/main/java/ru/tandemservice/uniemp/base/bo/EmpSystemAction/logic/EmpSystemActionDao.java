/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSystemAction.logic;

import org.apache.commons.io.FilenameUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.FourKey;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.*;
import ru.tandemservice.uniemp.entity.catalog.codes.CombinationPostTypeCodes;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class EmpSystemActionDao extends UniBaseDao implements IEmpSystemActionDao
{
    @Override
    @SuppressWarnings("unchecked")
    public void updateEmployeeCards()
    {
        Map<Long, EmployeeCard> emplCardsMap = new HashMap<>();
        for (EmployeeCard card : (List<EmployeeCard>)getSession().createCriteria(EmployeeCard.class).list())
            emplCardsMap.put(card.getEmployee().getId(), card);

        for (Employee employee : (List<Employee>)getSession().createCriteria(Employee.class).list())
            if (!emplCardsMap.containsKey(employee.getId()))
                save(new EmployeeCard(employee));

        List<Long> emplPostList = new ArrayList<>();
        for (EmployeePostStaffRateItem staffRate : (List<EmployeePostStaffRateItem>)getSession().createCriteria(EmployeePostStaffRateItem.class).list())
            if (!emplPostList.contains(staffRate.getEmployeePost().getId()))
                emplPostList.add(staffRate.getEmployeePost().getId());

        List<FinancingSource> financingSourceList = getCatalogItemList(FinancingSource.class);
        for (EmployeePost post : (List<EmployeePost>)getSession().createCriteria(EmployeePost.class).list())
            if (!emplPostList.contains(post.getId()))
            {
                EmployeePostStaffRateItem budgetItem = new EmployeePostStaffRateItem(post, financingSourceList.get(0));
                //EmployeePostStaffRateItem offBudgetItem = new EmployeePostStaffRateItem(post, financingSourceList.get(1));
                save(budgetItem);
                //save(offBudgetItem);
            }
    }

    @Override
    public void updateStaffListPayments()
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "ai");
        for(StaffListAllocationItem item : builder.<StaffListAllocationItem>getResultList(getSession()))
            StaffListPaymentsUtil.recalculateAllPaymentsValue(item, getSession());
    }

    @Override
    public void updateActiveStaffLists()
    {
        StaffListState state = getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING);

        MQBuilder builder = new MQBuilder(StaffList.ENTITY_CLASS, "sl");
        builder.add(MQExpression.eq("sl", StaffList.L_STAFF_LIST_STATE + "." + StaffListState.P_CODE, UniempDefines.STAFF_LIST_STATUS_ACTIVE));
        List<StaffList> staffListsList = builder.getResultList(getSession());

        for(StaffList staffList : staffListsList)
        {
            //staffList.setAcceptionDate(null);
            staffList.setStaffListState(state);
            update(staffList);
        }
    }

    @Override
    public void updateSynchronizeEmploymentHistory()
    {
        Map<Employee, List<EmploymentHistoryItemInner>> historyMap = new HashMap<>();

        for(EmploymentHistoryItemInner item : getList(EmploymentHistoryItemInner.class))
        {
            List<EmploymentHistoryItemInner> list = historyMap.get(item.getEmployee());
            if(null == list) list = new ArrayList<>();
            list.add(item);
            historyMap.put(item.getEmployee(), list);
        }

        Map<Long, List<EmployeePostStaffRateItem>> staffRatesMap = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemMap();

        int cnt = 0;
        for(EmployeePost post : getList(EmployeePost.class))
        {
            List<EmployeePostStaffRateItem> staffRateItems = staffRatesMap.get(post.getId());
            EmploymentHistoryItemInner hist = new EmploymentHistoryItemInner();
            if(historyMap.containsKey(post.getEmployee()))
            {
                for(EmploymentHistoryItemInner item : historyMap.get(post.getEmployee()))
                {
                    if(item.getOrgUnit().equals(post.getOrgUnit())
                            && item.getPostBoundedWithQGandQL().getPost().equals(post.getPostRelation().getPostBoundedWithQGandQL().getPost())
                            && item.getPostType().equals(post.getPostType())
                            && item.getAssignDate().equals(post.getPostDate()))
                    {
                        double staffRateSumm = 0.0d;
                        if (staffRateItems != null && staffRateItems.size() > 0)
                        {
                            for (EmployeePostStaffRateItem rateItem : staffRateItems)
                                staffRateSumm += rateItem.getStaffRate();

                            if (staffRateItems.size() != 0 && item.getStaffRate() != null && item.getStaffRate() == staffRateSumm)
                                hist = null;
                        }
                    }
                }
            }

            if(null != hist)
            {
                cnt++;
                hist.setEmployee(post.getEmployee());
                hist.setOrgUnit(post.getOrgUnit());
                hist.setPostBoundedWithQGandQL(post.getPostRelation().getPostBoundedWithQGandQL());
                hist.setPostType(post.getPostType());
                hist.setAssignDate(post.getPostDate());

                if(staffRateItems != null && staffRateItems.size() > 0)
                {
                    Double summStaffRate = 0.0d;
                    for (EmployeePostStaffRateItem staffRateItem : staffRateItems)
                        summStaffRate += staffRateItem.getStaffRate();

                    hist.setStaffRate(summStaffRate);
                }

                if(null == post.getDismissalDate()) hist.setCurrent(true);
                else hist.setDismissalDate(post.getDismissalDate());

                getSession().save(hist);
            }
        }

        System.out.println("There are " + cnt + " employment items were created");
    }

    @Override
    @Deprecated
    public void updateSynchronizeEmploymentHistoryStaffRates()
    {
        Map<Employee, List<EmploymentHistoryItemInner>> historyMap = new HashMap<>();

        List<EmploymentHistoryItemInner> allTheHistoryItems = getList(EmploymentHistoryItemInner.class);
        for (EmploymentHistoryItemInner item : allTheHistoryItems)
        {
            List<EmploymentHistoryItemInner> list = historyMap.get(item.getEmployee());
            if (null == list) list = new ArrayList<>();
            list.add(item);
            historyMap.put(item.getEmployee(), list);
        }

        Map<Long, List<EmployeePostStaffRateItem>> staffRatesMap = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemMap();

        Set<EmploymentHistoryItemInner> processedHistoryItems = new HashSet<>();

        for (EmployeePost post : getList(EmployeePost.class))
        {
            List<EmployeePostStaffRateItem> staffRateItems = staffRatesMap.get(post.getId());
            EmploymentHistoryItemInner hist = new EmploymentHistoryItemInner();
            if (historyMap.containsKey(post.getEmployee()))
            {
                for (EmploymentHistoryItemInner item : historyMap.get(post.getEmployee()))
                {
                    if (item.getOrgUnit().equals(post.getOrgUnit())
                            && item.getPostType().equals(post.getPostType())
                            && item.getAssignDate().equals(post.getPostDate())
                            && item.getPostBoundedWithQGandQL().getPost().equals(post.getPostRelation().getPostBoundedWithQGandQL().getPost())
                            && ((null == item.getDismissalDate() && null == post.getDismissalDate())
                                    || (item.getDismissalDate() != null && post.getDismissalDate() != null && item.getDismissalDate().equals(post.getDismissalDate()))))
                    {
                        double staffRateSumm = 0.0d;
                        if (staffRateItems != null && staffRateItems.size() > 0)
                        {
                            for (EmployeePostStaffRateItem rateItem : staffRateItems)
                                staffRateSumm += rateItem.getStaffRate();

                            if (staffRateItems.size() != 0 && hist.getStaffRate() == staffRateSumm)
                                hist = null;
                        }
                    }
                }
            }

            // FIXME WTF?
            if (null != hist.getId())
            {
                if (null == post.getDismissalDate()) hist.setCurrent(true);
                else hist.setDismissalDate(post.getDismissalDate());
                processedHistoryItems.add(hist);

                if(staffRateItems != null && staffRateItems.size() > 0)
                {
                    Double summStaffRate = 0.0d;
                    for (EmployeePostStaffRateItem staffRateItem : staffRateItems)
                        summStaffRate += staffRateItem.getStaffRate();

                    hist.setStaffRate(summStaffRate);
                }

                getSession().update(hist);
            }
        }

        allTheHistoryItems.removeAll(processedHistoryItems);
        SimpleDateFormat df = new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU);
        for (EmploymentHistoryItemInner item : allTheHistoryItems)
            System.out.println(item.getEmployee().getPerson().getFullFio() + "\t-\t" + item.getPostTitle() + " : " + item.getOrgUnitTitle() + " с " +
                                       df.format(item.getAssignDate()) + "");
    }

    @Override
    public void updateSyncStaffListCreateAndAcceptDates()
    {
        MQBuilder builder = new MQBuilder(StaffList.ENTITY_CLASS, "sl");
        builder.add(MQExpression.isNotNull("sl", StaffList.P_ACCEPTION_DATE));
        List<StaffList> staffListsList = builder.getResultList(getSession());
        for(StaffList item : staffListsList) item.setAcceptionDate(item.getCreationDate());
    }

    @Override
    public void updateInitStaffListArchivingDates()
    {
        MQBuilder builder = new MQBuilder(StaffList.ENTITY_CLASS, "sl");
        builder.addJoinFetch("sl", StaffList.L_STAFF_LIST_STATE, "sls");
        builder.add(MQExpression.isNull("sl", StaffList.P_ARCHIVING_DATE));
        builder.add(MQExpression.isNotNull("sl", StaffList.P_ACCEPTION_DATE));
        builder.addOrder("sl", StaffList.L_ORG_UNIT + "." + OrgUnit.P_TITLE);
        builder.addOrder("sl", StaffList.P_ACCEPTION_DATE);
        List<StaffList> staffListsList = builder.getResultList(getSession());

        List<StaffList> archivedStaffLists = new ArrayList<>();
        Map<OrgUnit, List<StaffList>> staffListsMap = new HashMap<>();
        for(StaffList staffList : staffListsList)
        {
            OrgUnit orgUnit = staffList.getOrgUnit();
            List<StaffList> staffLists = staffListsMap.get(orgUnit);
            if(null == staffLists) staffLists = new ArrayList<>();
            staffLists.add(staffList);
            staffListsMap.put(orgUnit, staffLists);

            if(UniempDefines.STAFF_LIST_STATUS_ARCHIVE.equals(staffList.getStaffListState().getCode()))
                archivedStaffLists.add(staffList);
        }

        for(StaffList archivedStaffList : archivedStaffLists)
        {
            List<StaffList> orgUnitStaffLists = staffListsMap.get(archivedStaffList.getOrgUnit());
            int archivedStaffListIdx = orgUnitStaffLists.indexOf(archivedStaffList);
            StaffList nextStaffList = archivedStaffListIdx + 1 < orgUnitStaffLists.size() ? orgUnitStaffLists.get(archivedStaffListIdx + 1) : null;
            if (null != nextStaffList)
            {
                archivedStaffList.setArchivingDate(nextStaffList.getAcceptionDate());
                getSession().update(archivedStaffList);
            }
        }

        MQBuilder archStaffListBuilder = new MQBuilder(StaffList.ENTITY_CLASS, "sl");
        archStaffListBuilder.add(MQExpression.eq("sl", StaffList.L_STAFF_LIST_STATE + "." + StaffListState.P_CODE, UniempDefines.STAFF_LIST_STATUS_ARCHIVE));
        archStaffListBuilder.add(MQExpression.eq("sl", StaffList.L_ORG_UNIT + "." + OrgUnit.P_ARCHIVAL, Boolean.TRUE));

        for(StaffList archivalStaffList : archStaffListBuilder.<StaffList>getResultList(getSession()))
        {
            if(null != archivalStaffList.getOrgUnit().getArchivingDate())
            {
                archivalStaffList.setArchivingDate(archivalStaffList.getOrgUnit().getArchivingDate());
                getSession().update(archivalStaffList);
            }
        }
    }

    @Override
    public void updateReallocateStaffListAllocItems()
    {
        MQBuilder slBuilder = new MQBuilder(StaffList.ENTITY_CLASS, "sl", new String[]{StaffList.L_ORG_UNIT});
        slBuilder.add(MQExpression.eq("sl", StaffList.L_STAFF_LIST_STATE + "." + StaffListState.P_CODE, UniempDefines.STAFF_LIST_STATUS_ACTIVE));
        slBuilder.add(MQExpression.eq("sl", StaffList.L_ORG_UNIT + "." + OrgUnit.P_ARCHIVAL, Boolean.FALSE));

        List<OrgUnit> orgUnitsWithActiveStaffLists = slBuilder.getResultList(getSession());

        MQBuilder sliBuilder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        sliBuilder.addJoinFetch("sli", StaffListItem.L_ORG_UNIT_POST_RELATION, "oupr");
        sliBuilder.addJoinFetch("oupr", OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, "outpr");
        sliBuilder.addJoinFetch("oupr", OrgUnitPostRelation.L_ORG_UNIT, "ou");
        sliBuilder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_STAFF_LIST_STATE + "." + StaffListState.P_CODE, UniempDefines.STAFF_LIST_STATUS_ACTIVE));
        sliBuilder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
        List<StaffListItem> staffListItemsList = sliBuilder.getResultList(getSession());

        MQBuilder slaBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "sla");
        slaBuilder.addJoinFetch("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM, "sli");
        slaBuilder.addJoinFetch("sli", StaffListItem.L_ORG_UNIT_POST_RELATION, "oupr");
        slaBuilder.addJoinFetch("oupr", OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, "outpr");
        slaBuilder.addJoinFetch("oupr", OrgUnitPostRelation.L_ORG_UNIT, "ou");
        slaBuilder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST + "." + StaffList.L_STAFF_LIST_STATE + "." + StaffListState.P_CODE, UniempDefines.STAFF_LIST_STATUS_ACTIVE));
        sliBuilder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
        slaBuilder.addDescOrder("sla", StaffListAllocationItem.P_STAFF_RATE_INTEGER);
        List<StaffListAllocationItem> staffListAllocItemsList = slaBuilder.getResultList(getSession());

        MQBuilder epsrBuilder = new MQBuilder(EmployeePostStaffRateItem.ENTITY_CLASS, "rel");
        epsrBuilder.addJoinFetch("rel", EmployeePostStaffRateItem.L_EMPLOYEE_POST, "ep");
        epsrBuilder.addJoinFetch("ep", EmployeePost.L_POST_RELATION, "outpr");
        epsrBuilder.addJoinFetch("ep", EmployeePost.L_ORG_UNIT, "ou");
        epsrBuilder.add(MQExpression.eq("ep", EmployeePost.L_POST_STATUS + "." + EmployeePostStatus.P_ACTIVE, Boolean.TRUE));
        sliBuilder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
        epsrBuilder.addDescOrder("rel", EmployeePostStaffRateItem.P_STAFF_RATE_INTEGER);
        List<EmployeePostStaffRateItem> employeePostStaffRateItemList = epsrBuilder.getResultList(getSession());


        Map<FourKey<OrgUnitTypePostRelation, OrgUnit, FinancingSource, FinancingSourceItem>, StaffListItem> staffListItemsMap = new HashMap<>();
        Map<PairKey<OrgUnitTypePostRelation, OrgUnit>, List<StaffListAllocationItem>> staffListAllocItemsMap = new HashMap<>();
        Map<PairKey<OrgUnitTypePostRelation, OrgUnit>, List<EmployeePostStaffRateItem>> staffRateReslsMap = new HashMap<>();
        Map<EmployeePost, StaffListAllocationItem> emplPostAllocItemsMap = new HashMap<>();


        for(StaffListItem item : staffListItemsList)
        {
            staffListItemsMap.put(new FourKey<>(item.getOrgUnitPostRelation().getOrgUnitTypePostRelation(), item.getStaffList().getOrgUnit(), item.getFinancingSource(), item.getFinancingSourceItem()), item);
        }

        for(StaffListAllocationItem item : staffListAllocItemsList)
        {
            if(null != item.getEmployeePost()) emplPostAllocItemsMap.put(item.getEmployeePost(), item);

            PairKey<OrgUnitTypePostRelation, OrgUnit> key = new PairKey<>(item.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation(), item.getStaffListItem().getStaffList().getOrgUnit());
            List<StaffListAllocationItem> postAllocItemsList = staffListAllocItemsMap.get(key);
            if(null == postAllocItemsList) postAllocItemsList = new ArrayList<>();
            postAllocItemsList.add(item);
            staffListAllocItemsMap.put(key, postAllocItemsList);
        }

        for(EmployeePostStaffRateItem item : employeePostStaffRateItemList)
        {
            PairKey<OrgUnitTypePostRelation, OrgUnit> key = new PairKey<>(item.getEmployeePost().getPostRelation(), item.getEmployeePost().getOrgUnit());
            List<EmployeePostStaffRateItem> postRelsList = staffRateReslsMap.get(key);
            if(null == postRelsList) postRelsList = new ArrayList<>();
            postRelsList.add(item);
            staffRateReslsMap.put(key, postRelsList);
        }

        List<EmployeePostStaffRateItem> resolvedRels = new ArrayList<>();

        for(EmployeePostStaffRateItem rel : employeePostStaffRateItemList)
        {
            EmployeePost post = rel.getEmployeePost();

            if(!orgUnitsWithActiveStaffLists.contains(post.getOrgUnit())) continue;

            PairKey<OrgUnitTypePostRelation, OrgUnit> key = new PairKey<>(post.getPostRelation(), post.getOrgUnit());
            List<StaffListAllocationItem> allocItemsList = staffListAllocItemsMap.get(key);

            StaffListAllocationItem emplAllocItem = emplPostAllocItemsMap.get(rel.getEmployeePost());
            if(null != emplAllocItem)
            {
                if(!isTwoDoublesAreEqual(emplAllocItem.getStaffRate(), rel.getStaffRate()) ||
                        !emplAllocItem.getFinancingSource().equals(rel.getFinancingSource()) ||
                        (emplAllocItem.getFinancingSourceItem() != null && !emplAllocItem.getFinancingSourceItem().equals(rel.getFinancingSourceItem())) ||
                        (rel.getFinancingSourceItem() != null && !rel.getFinancingSourceItem().equals(emplAllocItem.getFinancingSourceItem())))
                    emplAllocItem.setEmployeePost(null);
                else
                    resolvedRels.add(rel);
            }

            if(null != allocItemsList && (null == emplAllocItem || null == emplAllocItem.getEmployeePost()))
            {
                for(StaffListAllocationItem item : allocItemsList)
                {
                    if(null == item.getEmployeePost() && isTwoDoublesAreEqual(item.getStaffRate(), rel.getStaffRate()) &&
                            item.getFinancingSource().equals(rel.getFinancingSource()) &&
                            ((item.getFinancingSourceItem() == null && rel.getFinancingSource() == null) ||
                            (item.getFinancingSourceItem() != null && item.getFinancingSourceItem().equals(rel.getFinancingSourceItem()))))
                    {
                        item.setEmployeePost(post);
                        resolvedRels.add(rel);
                        break;
                    }
                }
            }
        }


        Map<StaffListItem, Double> occupationMap = new HashMap<>();

        for(StaffListItem staffListItem : staffListItemsMap.values())
        {
            PairKey<OrgUnitTypePostRelation, OrgUnit> key = new PairKey<>(staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation(), staffListItem.getStaffList().getOrgUnit());
            List<StaffListAllocationItem> staffListAllocItemToIterate = new ArrayList<>();
            List<StaffListAllocationItem> staffListAllocItems = staffListAllocItemsMap.get(key);

            if(null != staffListAllocItems)
            {
                staffListAllocItemToIterate.addAll(staffListAllocItems);

                for(StaffListAllocationItem allocItem : staffListAllocItemToIterate)
                {
                    if(null == allocItem.getEmployeePost())
                    {
                        staffListAllocItems.remove(allocItem);
                        getSession().delete(allocItem);
                    }
                    else if (allocItem.getEmployeePost().getPostStatus().isActive())
                    {
                        Double occRate = occupationMap.get(staffListItem);
                        if(null == occRate) occRate = 0d;
                        occRate += allocItem.getStaffRate();
                        occupationMap.put(staffListItem, occRate);
                    }
                }
            }
        }


        List<StaffListAllocationItem> newStaffListAllocItems = new ArrayList<>();
        List<EmployeePostStaffRateItem> offendedEmployeePosts = new ArrayList<>();

        for(EmployeePostStaffRateItem rel : employeePostStaffRateItemList)
        {
            EmployeePost post = rel.getEmployeePost();

            if (!resolvedRels.contains(rel) && orgUnitsWithActiveStaffLists.contains(post.getOrgUnit()))
            {
                FourKey<OrgUnitTypePostRelation, OrgUnit, FinancingSource, FinancingSourceItem> key = new FourKey<>(post.getPostRelation(), post.getOrgUnit(), rel.getFinancingSource(), rel.getFinancingSourceItem());

                StaffListItem staffListItem = staffListItemsMap.get(key);
                if(null != staffListItem)
                {
                    Double occRate = occupationMap.get(staffListItem);
                    if(null == occRate) occRate = 0d;
                    if(staffListItem.getStaffRate() - occRate - rel.getStaffRate() >= 0)
                    {
                        StaffListAllocationItem item = new StaffListAllocationItem();
                        item.setStaffListItem(staffListItem);
                        item.setStaffRate(rel.getStaffRate());
                        item.setFinancingSource(rel.getFinancingSource());
                        item.setFinancingSourceItem(rel.getFinancingSourceItem());
                        item.setEmployeePost(post);
                        item.setRaisingCoefficient(post.getRaisingCoefficient());
                        if (item.getRaisingCoefficient() != null)
                            item.setMonthBaseSalaryFund(item.getRaisingCoefficient().getRecommendedSalary() * item.getStaffRate());
                        else if (item.getStaffListItem() != null)
                            item.setMonthBaseSalaryFund(item.getStaffListItem().getSalary() * item.getStaffRate());
                        newStaffListAllocItems.add(item);

                        occRate += rel.getStaffRate();
                    }
                    else
                    {
                        offendedEmployeePosts.add(rel);
                    }
                    occupationMap.put(staffListItem, occRate);
                }
            }
        }

        for(List<StaffListAllocationItem> items : staffListAllocItemsMap.values())
        {
            for(StaffListAllocationItem item : items) getSession().update(item);
        }

        for(StaffListAllocationItem item : newStaffListAllocItems)
        {
            getSession().save(item);
            StaffListPaymentsUtil.recalculateAllPaymentsValue(item, getSession());
        }

        System.out.println("\"Обиженные сотрудники\":");
        for(EmployeePostStaffRateItem rel : offendedEmployeePosts)
        {
            System.out.println(rel.getEmployeePost().getExtendedPostTitle() + " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(rel.getStaffRate()) + ")");
        }
    }

    private boolean isTwoDoublesAreEqual(double number1, double number2)
    {
        return Math.abs(number1 - number2) < 0.0005;
    }

    @Override
    public void updateCorrectEmptySalaryAllocItems()
    {
        List<StaffListAllocationItem> staffListAllocItemsToRecalculate = new ArrayList<>();

        MQBuilder staffAllocItemsBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "sla");
        staffAllocItemsBuilder.addJoinFetch("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM, "sli");
        List<StaffListAllocationItem> staffListAllocItemsList = staffAllocItemsBuilder.getResultList(getSession());

        for(StaffListAllocationItem item : staffListAllocItemsList)
        {
            if(0 == item.getMonthBaseSalaryFund() && 0 != item.getStaffListItem().getSalary())
            {
                staffListAllocItemsToRecalculate.add(item);
                item.setMonthBaseSalaryFund((item.getStaffRate()) * item.getStaffListItem().getSalary());
                getSession().update(item);
            }
        }

        for(StaffListAllocationItem item : staffListAllocItemsToRecalculate)
            StaffListPaymentsUtil.recalculateAllPaymentsValue(item, getSession());
   }



    //Deprecated
    /*
    @Override
    public void updateSynchronizeEmployeeStages()
    {
        ServiceLengthType holeExp = getCatalogItem(ServiceLengthType.class,UniempDefines.SERVICE_LENGTH_TYPE_COMMON);
        ServiceLengthType companyExp = getCatalogItem(ServiceLengthType.class,UniempDefines.SERVICE_LENGTH_TYPE_COMPANY);
        ServiceLengthType sienceExp = getCatalogItem(ServiceLengthType.class,UniempDefines.SERVICE_LENGTH_TYPE_TUTOR);

        MQBuilder empServLenBuilder = new MQBuilder(EmployeeServiceLength.ENTITY_CLASS, "esl");
        List<EmployeeServiceLength> employeeServiceLengthListBase = empServLenBuilder.getResultList(getSession());
    //общий стаж, стаж на предприятии,научно-педагогический обрабатывается из EmploeePost
        MQBuilder empPostBuilder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        empPostBuilder.addJoinFetch("ep",EmployeePost.L_EMPLOYEE,"e");
        empPostBuilder.addJoinFetch("ep",EmployeePost.L_POST_RELATION,"mpr");
        empPostBuilder.addJoinFetch("mpr",OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L,"mpbw");
        empPostBuilder.addJoinFetch("mpbw", PostBoundedWithQGandQL.L_POST,"p");
        empPostBuilder.addJoinFetch("p", Post.L_EMPLOYEE_TYPE,"et");
        List<EmployeePost> employeePostList = empPostBuilder.getResultList(getSession());
        //
        //общий стаж, стаж на предприятии,научно-педагогический обрабатывается из employmentHistoryItemInner
        MQBuilder empHistItemIn = new MQBuilder(EmploymentHistoryItemInner.ENTITY_CLASS, "ehii");
        List<EmploymentHistoryItemInner> employmentHistoryItemInnerList = empHistItemIn.getResultList(getSession());
        //общий стаж обрабатывается из employmentHistoryItemExt
        MQBuilder empHistItemExt = new MQBuilder(EmploymentHistoryItemExt.ENTITY_CLASS, "ehie");
        List<EmploymentHistoryItemExt> employmentHistoryItemExtList = empHistItemExt.getResultList(getSession());

        List<EmployeeServiceLength> employeeServiceLengthListCurr = new ArrayList<EmployeeServiceLength>();

        ServiceLengthType serviceLengthType = holeExp;
        int i;
        EmployeeServiceLength temp;
        Date startDate, endDate;
        for(i=0;i<=2;i++)
        {
            if(i==0)
                serviceLengthType=holeExp;  //общий
            if(i==1)
                serviceLengthType=companyExp;//на предприятии
            if(i==2)
                serviceLengthType=sienceExp; //научно-педагогический

            for(EmployeePost itemEP : employeePostList)
            {
                if((i==2)&&!(itemEP.getPostRelation().getPostBoundedWithQGandQL().getPost().getEmployeeType().equals(getCatalogItem(EmployeeType.class,UniDefines.EMPLOYEE_TYPE_PPS_CODE))))
                continue;
                startDate = itemEP.getPostDate();
                endDate = itemEP.getDismissalDate();
                if(null!=endDate)
                {
                    if(endDate.before(startDate))
                        continue;
                }
                temp = new EmployeeServiceLength();
                temp.setBeginDate(startDate);
                if(endDate!=null)
                    temp.setEndDate(endDate);
                temp.setEmployee(itemEP.getEmployee());
                temp.setType(serviceLengthType);
                employeeServiceLengthListCurr.add(temp);
            }

            for(EmploymentHistoryItemInner itemEP : employmentHistoryItemInnerList)
            {
                if((i==2)&&!(itemEP.getPostBoundedWithQGandQL().getPost().getEmployeeType().equals(getCatalogItem(EmployeeType.class,UniDefines.EMPLOYEE_TYPE_PPS_CODE))))
                continue;
                startDate = itemEP.getAssignDate();
                endDate = itemEP.getDismissalDate();
                if(null!=endDate)
                {
                    if(endDate.before(startDate))
                        continue;
                }
                temp = new EmployeeServiceLength();
                temp.setBeginDate(startDate);
                if(endDate!=null)
                    temp.setEndDate(endDate);
                temp.setEmployee(itemEP.getEmployee());
                temp.setType(serviceLengthType);
                employeeServiceLengthListCurr.add(temp);
            }
        }

        serviceLengthType = holeExp;
        for(EmploymentHistoryItemExt itemEP : employmentHistoryItemExtList)
            {
                startDate = itemEP.getAssignDate();
                endDate = itemEP.getDismissalDate();
                if(null!=endDate)
                {
                    if(endDate.before(startDate))
                        continue;
                }
                temp = new EmployeeServiceLength();
                temp.setBeginDate(startDate);
                if(endDate!=null)
                    temp.setEndDate(endDate);
                temp.setEmployee(itemEP.getEmployee());
                temp.setType(serviceLengthType);
                employeeServiceLengthListCurr.add(temp);
            }

        boolean sameExist;
        Date startDateEP, endDateEP, startDateESL, endDateESL;

        for(EmployeeServiceLength itemEP : employeeServiceLengthListCurr)
        {
            startDateEP = itemEP.getBeginDate();
            endDateEP = itemEP.getEndDate();
            if(null!=endDateEP)
            {
                if(endDateEP.before(startDateEP))
                    continue;
            }

            sameExist=false;
            for(EmployeeServiceLength itemESL : employeeServiceLengthListBase)
            {
                startDateESL = itemESL.getBeginDate();
                endDateESL = itemESL.getEndDate();
                if(null!=endDateESL)
                {
                    if(endDateESL.before(startDateESL))
                        continue;
                }

                if(itemEP.getEmployee().equals(itemESL.getEmployee())&&(itemESL.getType().equals(itemEP.getType())))
                {
                    if((null==endDateEP)&&(null==endDateESL))
                    {
                        if(startDateEP.compareTo(startDateESL)==0)
                        {
                            sameExist = true;
                            break;
                        }
                        else if(startDateEP.before(startDateESL))
                        {
                            itemESL.setBeginDate(startDateEP);
                            sameExist = true;
                        }
                        else
                            sameExist = true;
                    }
                    else if((null!=endDateEP)&&(null!=endDateESL))
                    {
                        if((startDateEP.compareTo(startDateESL)==0)&&(endDateEP.compareTo(endDateESL)==0))
                        {
                            sameExist=true;
                        }
                        else if(startDateEP.before(startDateESL)&&(compareDate(endDateEP,startDateESL)<-3))
                        {
                            sameExist = false;
                        }
                        else if(startDateEP.before(startDateESL)&&(compareDate(endDateEP,startDateESL)>-3))
                        {
                            itemESL.setBeginDate(startDateEP);
                            if(endDateEP.after(endDateESL))
                                    itemESL.setEndDate(endDateEP);
                            sameExist=true;

                        }
                        else if((startDateEP.compareTo(startDateESL)>-1)&&endDateEP.before(endDateESL))
                        {
                            sameExist=true;
                        }
                        else if((startDateEP.compareTo(startDateESL)>-1)&&(endDateEP.compareTo(endDateESL)>-1))
                        {
                            itemESL.setEndDate(endDateEP);
                            sameExist=true;
                        }
                }

            }
        }

            for(EmployeeServiceLength item : employeeServiceLengthListBase)
                {
                    if(sameExist)
                        break;
                    if((null!=item.getEndDate())&&(null!=itemEP.getEndDate()))
                    {
                        if(item.getEmployee().equals(itemEP.getEmployee())&&(item.getBeginDate().getTime()==itemEP.getBeginDate().getTime())&&(item.getEndDate().getTime()==itemEP.getEndDate().getTime())&&item.getType().equals(itemEP.getType()))
                        {
                            sameExist = true;
                        }
                    }
                    if ((null==item.getEndDate())&&(null==itemEP.getEndDate()))
                    {
                        if(item.getEmployee().equals(itemEP.getEmployee())&&(item.getBeginDate().getTime()==itemEP.getBeginDate().getTime())&&item.getType().equals(itemEP.getType()))
                        {
                            sameExist = true;
                        }
                    }
                }

            if(!sameExist)
            {
                employeeServiceLengthListBase.add(itemEP);
            }

        }


        ArrayList<EmployeeServiceLength> resList = new ArrayList<EmployeeServiceLength>();
        for(EmployeeServiceLength itemEP : employeeServiceLengthListBase)
        {
            startDateEP = itemEP.getBeginDate();
            endDateEP = itemEP.getEndDate();
            if(null!=endDateEP)
            {
                if(endDateEP.before(startDateEP))
                    continue;
            }

            sameExist=false;
            for(EmployeeServiceLength itemESL : resList)
            {
                startDateESL = itemESL.getBeginDate();
                endDateESL = itemESL.getEndDate();
                if(null!=endDateESL)
                {
                    if(endDateESL.before(startDateESL))
                        continue;
                }

                if(itemEP.getEmployee().equals(itemESL.getEmployee())&&(itemESL.getType().equals(itemEP.getType())))
                {

                    if((null!=endDateEP)&&(null==endDateESL))
                    {
                        if (startDateEP.before(startDateESL)&&(compareDate(endDateEP,startDateESL)<-3))
                        {
                            sameExist =false;
                        }
                        else if (startDateEP.before(startDateESL)&&compareDate(endDateEP,startDateESL)>=-3)
                        {
                            itemESL.setBeginDate(startDateEP);
                            sameExist = true;
                        }
                        else
                            sameExist = true;
                    }
                    else if((null==endDateEP)&&(null!=endDateESL))
                    {
                        if(startDateEP.before(startDateESL))
                        {
                            itemESL.setBeginDate(startDateEP);
                            itemESL.setEndDate(null);
                            sameExist=true;
                        }
                        else if((startDateEP.compareTo(startDateESL)>-1)&&compareDate(startDateEP,endDateESL)<=3)
                        {
                            itemESL.setEndDate(null);
                            sameExist = true;
                        }
                        else if(compareDate(startDateEP,endDateESL)>3)
                            sameExist = false;
                    }


                }

            }


            for(EmployeeServiceLength item : resList)
                {
                    if(sameExist)
                        break;
                    if((null!=item.getEndDate())&&(null!=itemEP.getEndDate()))
                    {
                        if(item.getEmployee().equals(itemEP.getEmployee())&&(item.getBeginDate().getTime()==itemEP.getBeginDate().getTime())&&(item.getEndDate().getTime()==itemEP.getEndDate().getTime())&&item.getType().equals(itemEP.getType()))
                        {
                            sameExist = true;
                        }
                    }
                    if ((null==item.getEndDate())&&(null==itemEP.getEndDate()))
                    {
                        if(item.getEmployee().equals(itemEP.getEmployee())&&(item.getBeginDate().getTime()==itemEP.getBeginDate().getTime())&&item.getType().equals(itemEP.getType()))
                        {
                            sameExist = true;
                        }
                    }
                }

            if(!sameExist)
            {
                resList.add(itemEP);
            }

        }



        for(EmployeeServiceLength item : resList )
            {
                getSession().saveOrUpdate(item);
            }

    }
    */

    @Override
    public void updateCorrectEmployeeHolidayFinishDate()
    {
        MQBuilder employeeHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
        List<EmployeeHoliday> employeeHolidayList = employeeHolidayBuilder.getResultList(getSession());
        GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
        EmployeeWorkWeekDuration workWeekDuration;

        for (EmployeeHoliday employeeHoliday : employeeHolidayList )
        {
            workWeekDuration = employeeHoliday.getEmployeePost().getWorkWeekDuration();
            date.setTime(employeeHoliday.getStartDate());
            int counter = 0;
            int duration = employeeHoliday.getDuration();

            if (duration > 0)
            {
                do
                {
                    if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);

                employeeHoliday.setFinishDate(date.getTime());
            }
        }
    }

    @Override
    public void updateFillStaffListsAutomatically()
    {
        MQBuilder exlBuilder = new MQBuilder(StaffList.ENTITY_CLASS, "exlB", new String[] {StaffList.orgUnit().id().s()});
        exlBuilder.setNeedDistinct(true);

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "b");
        builder.add(MQExpression.notIn("b", OrgUnit.P_ID, exlBuilder));

        List<OrgUnit> orgUnitList = builder.getResultList(getSession());

        StaffListState formativeState = getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING);

        for (OrgUnit orgUnit : orgUnitList)
        {
            StaffList newStaffList = new StaffList();
            newStaffList.setNumber(1);
            newStaffList.setCreationDate(new Date());
            newStaffList.setOrgUnit(orgUnit);
            newStaffList.setStaffListState(formativeState);

            save(newStaffList);

            StaffListPaymentsUtil.fillStaffListAutomatically(newStaffList, getSession());
        }
    }

//    private boolean isIndustrialCalendarHoliday(EmployeeWorkWeekDuration weekDuration, Date date)
//    {
//        GregorianCalendar dayDate = (GregorianCalendar)GregorianCalendar.getInstance();
//
//        dayDate.setTime(date);
//
//        MQBuilder calendarBuilder = new MQBuilder(IndustrialCalendar.ENTITY_CLASS, "ic");
//        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.P_YEAR, dayDate.get(Calendar.YEAR)));
//        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.L_WEEK_DURATION, weekDuration));
//        IndustrialCalendar calendar = (IndustrialCalendar)calendarBuilder.uniqueResult(getSession());
//        if (null == calendar)
//            return false;
//
//        MQBuilder builder = new MQBuilder(IndustrialCalendarHoliday.ENTITY_CLASS, "ich");
//        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_CALENDAR, calendar));
//        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_HOLIDAY + "." + Holiday.P_PREHOLIDAY, Boolean.FALSE));
//        List<IHolidayDuration> holidays = builder.getResultList(getSession());
//
//        builder = new MQBuilder(IndustrialCalendarHolidayDuration.ENTITY_CLASS, "ichd");
//        builder.add(MQExpression.in("ichd", IndustrialCalendarHolidayDuration.L_HOLIDAY, holidays));
//        builder.add(MQExpression.greatOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
//                dayDate.get(Calendar.MONTH) + 1));
//        builder.add(MQExpression.lessOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
//                dayDate.get(Calendar.MONTH) + 1));
//        holidays.addAll(builder.<IHolidayDuration>getResultList(getSession()));
//
//        for(IHolidayDuration holiday : holidays)
//        {
//            if (holiday.getMonth() != (dayDate.get(Calendar.MONTH) + 1))
//                continue;
//
//            int begin = holiday.getBeginDay();
//            int end = null == holiday.getEndDay() ? begin : holiday.getEndDay();
//
//            if (holiday.getMonth() == dayDate.get(Calendar.MONTH) + 1 &&
//                    dayDate.get(Calendar.DAY_OF_MONTH) <= end &&
//                    dayDate.get(Calendar.DAY_OF_MONTH) >= begin)
//                return true;
//        }
//        return false;
//    }
//
//    private long compareDate(Date firstDate, Date secondDate)
//    {
//        return (firstDate.getTime() - secondDate.getTime()) / (1000 * 60 * 60 * 24);
//    }
//
    @Override
    public void getPostCatalogs()
    {
        MQBuilder holidayDurationBuilder = new MQBuilder(HolidayDuration.ENTITY_CLASS, "hd");
        holidayDurationBuilder.addOrder("hd", HolidayDuration.code());
        List<HolidayDuration> holidayDurationList = holidayDurationBuilder.getResultList(getSession());

        MQBuilder employeeTypeBuilder = new MQBuilder(EmployeeType.ENTITY_CLASS, "et");
        employeeTypeBuilder.addOrder("et", EmployeeType.code());
        List<EmployeeType> employeeTypeList = employeeTypeBuilder.getResultList(getSession());

        MQBuilder postBuilder = new MQBuilder(Post.ENTITY_CLASS, "p");
        postBuilder.addOrder("p", Post.title());
        List<Post> postList = postBuilder.getResultList(getSession());

        MQBuilder profQualificationGroupBuilder = new MQBuilder(ProfQualificationGroup.ENTITY_CLASS, "pqg");
        profQualificationGroupBuilder.addOrder("pqg", ProfQualificationGroup.title());
        List<ProfQualificationGroup> profQualificationGroupList = profQualificationGroupBuilder.getResultList(getSession());

        MQBuilder qualifacationLevelBuilder = new MQBuilder(QualificationLevel.ENTITY_CLASS, "ql");
        qualifacationLevelBuilder.addOrder("ql", QualificationLevel.title());
        List<QualificationLevel> qualificationLevelList = qualifacationLevelBuilder.getResultList(getSession());

        MQBuilder postBoundedBuilder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, "pb");
        postBoundedBuilder.addOrder("pb", PostBoundedWithQGandQL.title());
        List<PostBoundedWithQGandQL> postBoundedWithQGandQLList = postBoundedBuilder.getResultList(getSession());

        MQBuilder salaryRaisingCoefficientBuilder = new MQBuilder(SalaryRaisingCoefficient.ENTITY_CLASS, "slrc");
        salaryRaisingCoefficientBuilder.addOrder("slrc", SalaryRaisingCoefficient.post().title());
        salaryRaisingCoefficientBuilder.addOrder("slrc", SalaryRaisingCoefficient.post().id());
        salaryRaisingCoefficientBuilder.addOrder("slrc", SalaryRaisingCoefficient.raisingCoefficient());
        salaryRaisingCoefficientBuilder.getResultList(getSession());

        MQBuilder medicalEducationLevelBuilder = new MQBuilder(MedicalEducationLevel.ENTITY_CLASS, "mel");
        medicalEducationLevelBuilder.addOrder("mel", MedicalEducationLevel.title());
        medicalEducationLevelBuilder.getResultList(getSession());

        System.out.println("\n\n--- HOLIDAY DURATION ---");
        for(HolidayDuration holiudayDuration : holidayDurationList)
        {
            System.out.println(holiudayDuration.getCode() + "\t|\t" + holiudayDuration.getTitle() + "\t|\t" + holiudayDuration.getDaysAmount());
        }

        System.out.println("\n\n--- EMPLOYEE TYPE ---");
        for(EmployeeType employeeType : employeeTypeList)
        {
            System.out.println(employeeType.getCode() + "\t|\t" + employeeType.getTitle() + "\t|\t" + employeeType.getShortTitle() + "\t|\t" + getPrintableSafeValue(employeeType, EmployeeType.holidayDuration().title()) + "\t|\t" + (null != employeeType.getParent() ? (employeeType.getParent().getCode() + " - " + employeeType.getParent().getTitle()) : ""));
        }

        System.out.println("\n\n--- POST ---");
        for(Post post : postList)
        {
            System.out.println(post.getCode() + "\t|\t" + post.getTitle() + "\t|\t" + post.getEmployeeType().getTitle() + "\t|\t" + getPrintableSafeValue(post, Post.rank()) + "\t|\t" + getPrintableSafeValue(post, Post.nominativeCaseTitle()) + "\t|\t" + getPrintableSafeValue(post, Post.genitiveCaseTitle()) + "\t|\t" + getPrintableSafeValue(post, Post.dativeCaseTitle()) + "\t|\t" + getPrintableSafeValue(post, Post.accusativeCaseTitle()) + "\t|\t" + getPrintableSafeValue(post, Post.instrumentalCaseTitle()) + "\t|\t" + getPrintableSafeValue(post, Post.prepositionalCaseTitle()));
        }

        System.out.println("\n\n--- PROF-QUALIFICATION GROUP ---");
        for(ProfQualificationGroup group : profQualificationGroupList)
        {
            System.out.println(group.getCode() + "\t|\t" + group.getTitle() + "\t|\t" + group.getShortTitle() + "\t|\t" + getPrintableSafeValue(group, ProfQualificationGroup.minSalary()) + "\t|\t" + getPrintableSafeValue(group, ProfQualificationGroup.orderNumber()) + "\t|\t" + getPrintableSafeValue(group, ProfQualificationGroup.orderDate()));
        }

        System.out.println("\n\n--- QUALIFICATION LEVEL ---");
        for(QualificationLevel level : qualificationLevelList)
        {
            System.out.println(level.getCode() + "\t|\t" + level.getTitle() + "\t|\t" + level.getShortTitle());
        }

        System.out.println("\n\n--- POST BOUNDED WITH PQG AND QL ---");
        for(PostBoundedWithQGandQL postBounded : postBoundedWithQGandQLList)
        {
            System.out.println(postBounded.getCode() + "\t|\t" + postBounded.getTitle() + "\t|\t" + postBounded.getPost().getTitle() + "\t|\t" + postBounded.getProfQualificationGroup().getTitle() + "\t|\t" + postBounded.getQualificationLevel().getTitle() + "\t|\t" + getPrintableSafeValue(postBounded, PostBoundedWithQGandQL.etksLevels().title()) + "\t|\t" + getPrintableSafeValue(postBounded, PostBoundedWithQGandQL.salary()) + "\t|\t" + getPrintableSafeValue(postBounded, PostBoundedWithQGandQL.order()) + "\t|\t" + getPrintableSafeValue(postBounded, PostBoundedWithQGandQL.scienceDegreeType().title()) + "\t|\t" + getPrintableSafeValue(postBounded, PostBoundedWithQGandQL.scienceStatusType().title()));
        }
    }

    private String getPrintableSafeValue(IEntity entity, PropertyPath path)
    {
        Object obj = entity.getProperty(path);
        if(null == obj) return "";
        else
        {
            if(obj instanceof Double) return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(obj);
            else if(obj instanceof Date) return DateFormatter.DEFAULT_DATE_FORMATTER.format((Date)obj);
            else return obj.toString();
        }
    }

    @Override
    public void updateOrgUnitTypePostRelation()
    {
        MQBuilder postBuilder = new MQBuilder(EmployeePost.ENTITY_CLASS, "b");
        List<EmployeePost> employeePostList = new ArrayList<>(postBuilder.<EmployeePost>getResultList(getSession()));

        MQBuilder orgUnitBuilder = new MQBuilder(OrgUnitPostRelation.ENTITY_CLASS, "b");
        List<OrgUnitPostRelation> relationList = new ArrayList<>(orgUnitBuilder.<OrgUnitPostRelation>getResultList(getSession()));

        MQBuilder orgUnitTypePostRelationBuilder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "b");
        List<OrgUnitTypePostRelation> postRelationList = new ArrayList<>(orgUnitTypePostRelationBuilder.<OrgUnitTypePostRelation>getResultList(getSession()));

        Map<OrgUnitType, Map<PostBoundedWithQGandQL, OrgUnitTypePostRelation>> orgUnitTypePostRelationMap = new HashMap<>();

        for (OrgUnitTypePostRelation relation : postRelationList)
        {
            Map<PostBoundedWithQGandQL, OrgUnitTypePostRelation> postOrgUnitTypeMap = orgUnitTypePostRelationMap.get(relation.getOrgUnitType());
            if (postOrgUnitTypeMap == null)
                postOrgUnitTypeMap = new HashMap<>();

            postOrgUnitTypeMap.put(relation.getPostBoundedWithQGandQL(), relation);
            orgUnitTypePostRelationMap.put(relation.getOrgUnitType(), postOrgUnitTypeMap);
        }

        for (EmployeePost post : employeePostList)
        {
            OrgUnitType orgUnitType = post.getOrgUnit().getOrgUnitType();
            OrgUnitType postOrgUnitType = post.getPostRelation().getOrgUnitType();
            if (!orgUnitType.equals(postOrgUnitType))
            {
                Map<PostBoundedWithQGandQL, OrgUnitTypePostRelation> postOrgUnitTypeMap = orgUnitTypePostRelationMap.get(orgUnitType);
                if (postOrgUnitTypeMap == null)
                    postOrgUnitTypeMap = new HashMap<>();

                OrgUnitTypePostRelation postRelation = postOrgUnitTypeMap.get(post.getPostRelation().getPostBoundedWithQGandQL());
                if (postRelation == null)
                {
                    postRelation = new OrgUnitTypePostRelation();
                    postRelation.setOrgUnitType(orgUnitType);
                    postRelation.setPostBoundedWithQGandQL(post.getPostRelation().getPostBoundedWithQGandQL());
                    postRelation.setMultiPost(post.getPostRelation().isMultiPost());
                    postRelation.setHeaderPost(post.getPostRelation().isHeaderPost());

                    save(postRelation);

                    postOrgUnitTypeMap.put(post.getPostRelation().getPostBoundedWithQGandQL(), postRelation);
                    orgUnitTypePostRelationMap.put(orgUnitType, postOrgUnitTypeMap);
                }

                post.setPostRelation(postRelation);

                update(post);
            }

        }

        for (OrgUnitPostRelation relation : relationList)
        {
            OrgUnitType orgUnitType = relation.getOrgUnit().getOrgUnitType();
            OrgUnitType postOrgUnitType = relation.getOrgUnitTypePostRelation().getOrgUnitType();
            if (!orgUnitType.equals(postOrgUnitType))
            {
                Map<PostBoundedWithQGandQL, OrgUnitTypePostRelation> postOrgUnitTypeMap = orgUnitTypePostRelationMap.get(orgUnitType);
                if (postOrgUnitTypeMap == null)
                    postOrgUnitTypeMap = new HashMap<>();

                OrgUnitTypePostRelation postRelation = postOrgUnitTypeMap.get(relation.getOrgUnitTypePostRelation().getPostBoundedWithQGandQL());
                if (postRelation == null)
                {
                    postRelation = new OrgUnitTypePostRelation();
                    postRelation.setOrgUnitType(orgUnitType);
                    postRelation.setPostBoundedWithQGandQL(relation.getOrgUnitTypePostRelation().getPostBoundedWithQGandQL());
                    postRelation.setMultiPost(relation.getOrgUnitTypePostRelation().isMultiPost());
                    postRelation.setHeaderPost(relation.getOrgUnitTypePostRelation().isHeaderPost());

                    save(postRelation);

                    postOrgUnitTypeMap.put(relation.getOrgUnitTypePostRelation().getPostBoundedWithQGandQL(), postRelation);
                    orgUnitTypePostRelationMap.put(orgUnitType, postOrgUnitTypeMap);
                }

                relation.setOrgUnitTypePostRelation(postRelation);

                update(relation);
            }
        }
    }

    @Override
    public void updateSecondJobEmployeePost()
    {
        //1. ПОДГОТАВЛИВАЕМ ДАННЫЕ

        //поднимаем Сотрудников с Типом назночения По совмещению
        DQLSelectBuilder secondJobBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "sj").column("sj");
        secondJobBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePost.postType().code().fromAlias("sj")), PostTypeCodes.SECOND_JOB));
        secondJobBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePost.postStatus().active().fromAlias("sj")), true));

        List<EmployeePost> secondJobEmployeePostList = secondJobBuilder.createStatement(getSession()).list();

        //поднимаем Должности Штатной расстановки для Сотруудников По совмещению
        DQLSelectBuilder staffListAllocBuilder = new DQLSelectBuilder().fromEntity(StaffListAllocationItem.class, "sl").column("sl");
        staffListAllocBuilder.where(DQLExpressions.in(DQLExpressions.property(StaffListAllocationItem.employeePost().fromAlias("sl")), secondJobEmployeePostList));

        //создаем мапу: Сотрудник По совмещению на список его Должностей Штатной расстановки
        Map<EmployeePost, List<StaffListAllocationItem>> postStaffListAllocMap = new LinkedHashMap<>();
        for (StaffListAllocationItem allocItem : staffListAllocBuilder.createStatement(getSession()).<StaffListAllocationItem>list())
        {
            EmployeePost employeePost = allocItem.getEmployeePost();
            List<StaffListAllocationItem> allocItemList = postStaffListAllocMap.get(employeePost);
            if (allocItemList == null)
                allocItemList = new ArrayList<>();
            allocItemList.add(allocItem);
            postStaffListAllocMap.put(employeePost, allocItemList);
        }

        //достаем ставки Сотрудников По совмещению
        DQLSelectBuilder staffRateBuilder = new DQLSelectBuilder().fromEntity(EmployeePostStaffRateItem.class, "sr").column("sr");
        staffRateBuilder.where(DQLExpressions.in(DQLExpressions.property(EmployeePostStaffRateItem.employeePost().fromAlias("sr")), secondJobEmployeePostList));

        //создаем мапу: Сотрудник По совмещению на список его Долей ставок
        Map<EmployeePost, List<EmployeePostStaffRateItem>> postStaffRateMap = new LinkedHashMap<>();
        for (EmployeePostStaffRateItem item : staffRateBuilder.createStatement(getSession()).<EmployeePostStaffRateItem>list())
        {
            EmployeePost employeePost = item.getEmployeePost();
            List<EmployeePostStaffRateItem> rateList = postStaffRateMap.get(employeePost);
            if (rateList == null)
                rateList = new ArrayList<>();
            rateList.add(item);
            postStaffRateMap.put(employeePost, rateList);
        }

        //создаем сет с идентификаторами Кадровых ресурсов Сотрудников с Типом назначения По совмещению
        Set<Long> employeeIdList = new LinkedHashSet<>();
        for (EmployeePost employeePost : secondJobEmployeePostList)
            employeeIdList.add(employeePost.getEmployee().getId());

        //поднимаем всех Сотрудников "рядом с которыми" есть Сотрудник с Типом назначения По совмещению
        DQLSelectBuilder employeePostBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep").column("ep");
        employeePostBuilder.where(DQLExpressions.in(DQLExpressions.property(EmployeePost.employee().id().fromAlias("ep")), employeeIdList));
        employeePostBuilder.where(DQLExpressions.notIn(DQLExpressions.property(EmployeePost.id().fromAlias("ep")), ids(secondJobEmployeePostList)));

        List<EmployeePost> employeePostList = employeePostBuilder.createStatement(getSession()).list();

        //создаем мапу: Кадровый ресурс на список соответствующих Сотрудников
        Map<Employee, List<EmployeePost>> employeePostMap = new LinkedHashMap<>();
        for (EmployeePost employeePost : employeePostList)
        {
            Employee employee = employeePost.getEmployee();
            List<EmployeePost> postList = employeePostMap.get(employee);
            if (postList == null)
                postList = new ArrayList<>();
            postList.add(employeePost);
            employeePostMap.put(employee, postList);
        }

        //список Сотружников, которых не получилось привязать. в конце нужно вынести их в файл
        List<EmployeePost> notBindSecondPost = new ArrayList<>();

        EmployeePostStatus postStatusPossible = getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_POSSIBLE);

        //2. ПРОИЗВОДИМ ДЕЙСТВИЯ

        //бежим по Сотрудникам По совмещению
        for (EmployeePost secondPost : secondJobEmployeePostList)
        {
            //выбираем среди Сотрудников "рядом" с Сотрудником По совмещению основного Сотружника к которому будем привязывать Совмещение
            List<EmployeePost> postList = employeePostMap.get(secondPost.getEmployee());
            EmployeePost mainPost = null;
            if (postList != null)
            {
                boolean alreadyHasSecondPost = false;//метка, что бы узнать есть ли у Сотрудника несколько активных должностей по совмещению
                for (EmployeePost post : postList)
                {
                    if (post.getPostType().getCode().equals(PostTypeCodes.MAIN_JOB) && post.getPostStatus().isActive())
                    {
                        mainPost = post;
                        break;
                    }
                    else if (post.getPostType().getCode().equals(PostTypeCodes.SECOND_JOB_OUTER) && post.getPostStatus().isActive())
                    {
                        //если alreadyHasSecondPost true значит у Сотрудника уже есть активная должность по совмещению -
                        //в данной ситуации мы не знаем к какому из них нам привязывать должность, => ставим mainPost в null, таким образом помечая Сотрудника как проблемного
                        if (alreadyHasSecondPost)
                        {
                            mainPost = null;
                            break;
                        }

                        mainPost = post;
                        alreadyHasSecondPost = true;
                    }
                }
            }

            if (mainPost == null)//если основного Сотрудника не оказалось, то сохраняем Сотрудника По совмещению в список сотружников, которых не получилось привязать
            {
                notBindSecondPost.add(secondPost);
            }
            else//если Сотрудник нашелся
            {
                //создаем для него Должность по совмещению на основе Сотрудника По совмещению
                CombinationPost newCombinationPost = new CombinationPost();
                newCombinationPost.setEmployeePost(mainPost);
                newCombinationPost.setPostBoundedWithQGandQL(secondPost.getPostRelation().getPostBoundedWithQGandQL());
                newCombinationPost.setOrgUnit(secondPost.getOrgUnit());
                newCombinationPost.setCombinationPostType(getCatalogItem(CombinationPostType.class, secondPost.getPostRelation().getPostBoundedWithQGandQL().equals(mainPost.getPostRelation().getPostBoundedWithQGandQL()) ? CombinationPostTypeCodes.INC_WORK : CombinationPostTypeCodes.COMBINATION));
                newCombinationPost.setMissingEmployeePost(secondPost.getInsteadOfEmployeePost());
                newCombinationPost.setSalary(secondPost.getSalary());
                newCombinationPost.setBeginDate(secondPost.getPostDate());
                newCombinationPost.setEndDate(secondPost.getDismissalDate());
                newCombinationPost.setEtksLevels(secondPost.getEtksLevels());
                newCombinationPost.setSalaryRaisingCoefficient(secondPost.getRaisingCoefficient());

                save(newCombinationPost);

                //создаем Ставки для Должности по совмещению на основе Ставок Сотрудника По совмещению
                if (postStaffRateMap.get(secondPost) != null)
                    for (EmployeePostStaffRateItem item : postStaffRateMap.get(secondPost))
                    {
                        CombinationPostStaffRateItem newItem = new CombinationPostStaffRateItem();
                        newItem.setCombinationPost(newCombinationPost);
                        newItem.setFinancingSource(item.getFinancingSource());
                        newItem.setFinancingSourceItem(item.getFinancingSourceItem());
                        newItem.setStaffRate(item.getStaffRate());

                        save(newItem);
                    }

                //обновляем Должности Штатной расстановки на которые был назначен Сотрудник По совмещению
                //ссылку на Сотрудника (которая раньше была на Сотрудника По совмещению) заменяем на ссылку нашего Основного Сотрудника (к которому привязываем совмещение), ставим метку того, что Должность Штатной расстановки занята Должностю по совмещению и простовляем ссылку на Должность по совмещению
                List<StaffListAllocationItem> allocItemList = postStaffListAllocMap.get(secondPost);
                if (allocItemList != null)
                    for (StaffListAllocationItem allocItem : allocItemList)
                    {
                        allocItem.setEmployeePost(mainPost);
                        allocItem.setCombination(true);
                        allocItem.setCombinationPost(newCombinationPost);

                        update(allocItem);
                    }

                //выставляем состояние Возможный у Сотрудника По совмещению, которого успешно перенесли
                secondPost.setPostStatus(postStatusPossible);
                update(secondPost);
            }
        }

        //если Сотрудников, которые вызвали проблемы нет, то выходим
        if (notBindSecondPost.isEmpty())
            return;

        //иначе создаем текстовый файл, где выводим данные по проблемным Сотрудникам
        try
        {
            final String name = "not_bind_secondJob_employeePost_" + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date());

            File file;
            int i = 0;
            do
            {
                String pathname = Paths.get(System.getProperty("catalina.base"), "logs", name + "_" + i++ + ".txt").toString();
                file = new File(pathname);
            }
            while (file.exists());

            OutputStream outputStream = new FileOutputStream(file);
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, Charset.defaultCharset())))
            {
                writer.write(DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()));
                writer.newLine();
                writer.newLine();
                writer.write("Файл содержит данные по Сотрудникам с типом назначения \"По совмещению\", которые не были перенесены на вкалдку \"Совмещение\".");
                writer.newLine();

                for (EmployeePost employeePost : notBindSecondPost)
                {
                    writer.newLine();
                    writer.newLine();
                    StringBuilder lineBuilder = new StringBuilder();
                    lineBuilder.append(employeePost.getEmployee().getEmployeeCode()).append(" ");
                    lineBuilder.append(employeePost.getEmployee().getPerson().getFullFio()).append(" - ");
                    lineBuilder.append(employeePost.getPostRelation().getPostBoundedWithQGandQL().getFullTitle()).append(" - ");
                    lineBuilder.append(employeePost.getOrgUnit().getTitleWithType()).append(" - ");
                    lineBuilder.append(employeePost.getPostStatus().getTitle());
                    writer.write(lineBuilder.toString());
                }
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException("Caught exception while processing file in SystemAction uniemp_bindSecondJobEmployeePost: ", e);
        }
    }

    @Override
    public void updateEmployeeContractEndDate()
    {
        //поднимаем трудовые договоры активных сотрудников
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeLabourContract.class, "b").column("b");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeLabourContract.employeePost().postStatus().active().fromAlias("b")), true));

        List<EmployeeLabourContract> contractList = builder.createStatement(getSession()).list();

        for (EmployeeLabourContract contract : contractList)
        {
            //для ТД простовляем дату окончания, если она не указана
            //дату берем с Сотрудника - дату увольнения, если она указана
            if (contract.getEndDate() == null && contract.getEmployeePost().getDismissalDate() != null)
            {
                contract.setEndDate(contract.getEmployeePost().getDismissalDate());
                update(contract);
            }
        }
    }
}
