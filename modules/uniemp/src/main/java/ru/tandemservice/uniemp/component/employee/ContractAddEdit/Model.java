/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.employee.ContractAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * @author E. Grigoriev
 * @since 09.07.2008
 */
@Input(keys = { "labourContractId", "employeePostId" }, bindings = { "labourContract.id", "employeePostId" })
public class Model
{
    private EmployeeLabourContract _labourContract = new EmployeeLabourContract();
    private List<LabourContractType> _typeList;
    private Long _employeePostId;

    public EmployeeLabourContract getLabourContract()
    {
        return _labourContract;
    }

    public void setLabourContract(EmployeeLabourContract labourContract)
    {
        _labourContract = labourContract;
    }

    public List<LabourContractType> getTypeList()
    {
        return _typeList;
    }

    public void setTypeList(List<LabourContractType> typeList)
    {
        _typeList = typeList;
    }

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }
}