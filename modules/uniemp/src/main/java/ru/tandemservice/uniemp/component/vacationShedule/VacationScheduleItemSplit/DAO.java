/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemSplit;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

import java.util.Calendar;
import java.util.Date;

/**
 * @author dseleznev
 * Created on: 27.01.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setVacationScheduleItem(get(VacationScheduleItem.class, model.getVacationScheduleItemId()));
        model.setFirstPartDaysAmount(model.getVacationScheduleItem().getDaysAmount());
        model.setFirstPartPlanDate(model.getVacationScheduleItem().getPlanDate());
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errs = UserContext.getInstance().getErrorCollector();

        model.getVacationScheduleItem().setDaysAmount(model.getFirstPartDaysAmount());
        model.getVacationScheduleItem().setPlanDate(model.getFirstPartPlanDate());

        VacationScheduleItem secondItem = new VacationScheduleItem();
        secondItem.update(model.getVacationScheduleItem());
        secondItem.setDaysAmount(model.getSecondPartDaysAmount());
        secondItem.setPlanDate(model.getSecondPartPlanDate());

        validate(model.getVacationScheduleItem(), secondItem, errs);
        UniempDaoFacade.getUniempDAO().validateVacationScheduleItemHasIntersections(model.getVacationScheduleItem(), errs, "Указанный планируемый отпуск", "planDate1", "", "daysAmount1");
        UniempDaoFacade.getUniempDAO().validateVacationScheduleItemHasIntersections(secondItem, errs, "Указанный планируемый отпуск", "planDate2", "", "daysAmount2");

        if (errs.hasErrors()) return;

        update(model.getVacationScheduleItem());
        save(secondItem);
    }

    private void validate(VacationScheduleItem item1, VacationScheduleItem item2, ErrorCollector errors)
    {
        Date d1Start = null != item1.getFactDate() ? item1.getFactDate() : item1.getPlanDate();
        Date d2Start = null != item2.getFactDate() ? item2.getFactDate() : item2.getPlanDate();
        if (d1Start == null || d2Start == null) return;

        Date d1Finish = CoreDateUtils.add(d1Start, Calendar.DATE, item1.getDaysAmount());
        Date d2Finish = CoreDateUtils.add(d2Start, Calendar.DATE, item2.getDaysAmount());

        Long t1Start = d1Start.getTime();
        Long t2Start = d2Start.getTime();
        Long t1Finish = d1Finish.getTime();
        Long t2Finish = d2Finish.getTime();

        if ((t1Start >= t2Start && t1Start <= t2Finish) || (t1Finish >= t2Start && t1Finish <= t2Finish))
        {
            errors.add("Периоды планируемых отпусков не должны пересекаться.", "planDate1", "daysAmount1", "planDate2", "daysAmount2");
        }
    }
}