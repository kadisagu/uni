/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.shared.person.catalog.entity.codes.ScienceDegreeTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.ScienceStatusTypeCodes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Враппер для строк отчета.
 * Агрегирует сотрудников с ставками в указанны интервалах, максимальную ставку и их уч. степени\звания.
 *
 * @author Alexander Shaburov
 * @since 20.11.12
 */
public class EmployeeWrapper extends DataWrapper
{
    public EmployeeWrapper(long id)
    {
        super(id, "");
    }

    private List<EmployeePost> _postList = new ArrayList<>();
    private BigDecimal _staffRate = new BigDecimal(0d);
    private List<ScienceDegree> _scienceDegreeList = new ArrayList<>();
    private List<ScienceStatus> _scienceStatusList = new ArrayList<>();

    /**
     * @return Список сотрудников из строки с указанным типом назначения на должность.
     */
    public List<EmployeePost> postList(PostType postType)
    {
        List<EmployeePost> resultList = new ArrayList<>();
        for (EmployeePost post : _postList)
            if (post.getPostType().getCode().endsWith(postType.getCode()))
                resultList.add(post);

        return resultList;
    }

    /**
     * @return true, если в списке сотрудников строки есть с почасовой оплатой, иначе false
     */
    public boolean hasHourlyPaid()
    {
        for (EmployeePost post : _postList)
            if (post.isHourlyPaid())
                return true;
        return false;
    }

    /**
     * @return true, если из списка сотрудников строки есть с учёной степенью и/или званием
     */
    public boolean hasScience()
    {
        return !_scienceDegreeList.isEmpty() || !_scienceStatusList.isEmpty();
    }

    /**
     * @return true, если из списка сотрудников строки есть с учёной степенью доктора наук и/или званием профессора
     */
    public boolean hasHighScience()
    {
        for (ScienceDegree degree : _scienceDegreeList)
            if (degree.getType() != null && degree.getType().getCode().equals(ScienceDegreeTypeCodes.DOCTOR))
                return true;

        for (ScienceStatus status :  _scienceStatusList)
            if (status.getType() != null && status.getType().getCode().equals(ScienceStatusTypeCodes.PROFESSOR))
                return true;

        return false;
    }

    /**
     * Сохраняет указанную ставку, как максимальную в строке, если она больше чем сохранена в строке.
     */
    public void setStaffRate(BigDecimal staffRate)
    {
        if (staffRate.doubleValue() > _staffRate.doubleValue())
            _staffRate = staffRate;
    }

    // Getters & Setters

    public List<ScienceDegree> getScienceDegreeList()
    {
        return _scienceDegreeList;
    }

    public List<ScienceStatus> getScienceStatusList()
    {
        return _scienceStatusList;
    }

    public List<EmployeePost> getPostList()
    {
        return _postList;
    }

    public BigDecimal getStaffRate()
    {
        return _staffRate;
    }
}
