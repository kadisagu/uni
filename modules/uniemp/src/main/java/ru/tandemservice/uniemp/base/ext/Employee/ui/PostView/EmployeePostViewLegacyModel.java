/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.PostView;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.employee.*;

/**
 * @author Vasily Zhukov
 * @since 30.03.2012
 */
class EmployeePostViewLegacyModel
{
    public static String COMBINATION_POST_STAFF_RATE = "combinationPostStaffRate";
    public static String COMBINATION_POST_ORDER = "combinationPostOrder";
    public static String COMBINATION_POST_AGREEMENT = "combinationPostAgreement";

    private ISelectModel _orgUnitModelFilter;
    private ISelectModel _postTypeModelFilter;
    private ISelectModel _postModelFilter;
    private DynamicListDataSource<EmployeeActingItem> _employeeActingListDataSource;
    private DynamicListDataSource<CombinationPost> _combinationPostDataSource;

    private EmployeePostViewUI _baseModel;
    private EmployeeCard _employeeCard = new EmployeeCard();
    private EmployeeLabourContract _labourContract;

    private boolean _employeeCardTypePPS;

    private Long _rowId;

    private DynamicListDataSource<PersonAcademicDegree> _scienceDegreeDataSource;
    private DynamicListDataSource<PersonAcademicStatus> _scienceStatusDataSource;
    private DynamicListDataSource<EmployeeMedicalSpeciality> _medicalSpecDataSource;
    private DynamicListDataSource<EmployeeSickList> _employeeSickListDataSource;
    private DynamicListDataSource<EmployeeHoliday> _employeeHolidaysListDataSource;
    private DynamicListDataSource<VacationScheduleItem> _employeeVacationPlannedListDataSource;
    private DynamicListDataSource<EmployeePayment> _employeePaymentsListDataSource;
    private boolean _addingVacationPlannedVisible;
    private DynamicListDataSource<ContractCollateralAgreement> _contractAgreementDataSource;
    private DynamicListDataSource<EmployeeCertificationItem> _certificationDataSource;
    private DynamicListDataSource<EmployeeTrainingItem> _trainingDataSource;
    private DynamicListDataSource<EmployeeRetrainingItem> _retrainingDataSource;

    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;

    private boolean _medicalSpecListVisible = true;

    public boolean isAddingVacationPlannedVisible()
    {
        return _addingVacationPlannedVisible;
    }

    public void setAddingVacationPlannedVisible(boolean addingVacationPlannedVisible)
    {
        _addingVacationPlannedVisible = addingVacationPlannedVisible;
    }

    public IEntityHandler getDeleteDisabledEntityHandler()
    {
        return entity -> {
            VacationScheduleItem vsi = (VacationScheduleItem) entity;
            return !vsi.getVacationSchedule().getState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_FORMING);
        };
    }

    public IEntityHandler getEditDisabledEntityHandler()
    {
        return entity -> {
            VacationScheduleItem vsi = (VacationScheduleItem) entity;
            if (!vsi.getVacationSchedule().getState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_FORMING))
                return !CoreServices.securityService().check(vsi.getEmployeePost(), ContextLocal.getUserContext().getPrincipalContext(), "editEmployeeVacationScheduleItem_employeePost");
            else return false;
        };
    }

    public DynamicListDataSource<EmployeeTrainingItem> getTrainingDataSource()
    {
        return _trainingDataSource;
    }

    public void setTrainingDataSource(DynamicListDataSource<EmployeeTrainingItem> trainingDataSource)
    {
        _trainingDataSource = trainingDataSource;
    }

    public DynamicListDataSource<EmployeeRetrainingItem> getRetrainingDataSource()
    {
        return _retrainingDataSource;
    }

    public void setRetrainingDataSource(DynamicListDataSource<EmployeeRetrainingItem> retrainingDataSource)
    {
        _retrainingDataSource = retrainingDataSource;
    }

    public DynamicListDataSource<EmployeeCertificationItem> getCertificationDataSource()
    {
        return _certificationDataSource;
    }

    public void setCertificationDataSource(DynamicListDataSource<EmployeeCertificationItem> certificationDataSource)
    {
        _certificationDataSource = certificationDataSource;
    }

    public ISelectModel getPostTypeModelFilter()
    {
        return _postTypeModelFilter;
    }

    public void setPostTypeModelFilter(ISelectModel postTypeModelFilter)
    {
        _postTypeModelFilter = postTypeModelFilter;
    }

    public DynamicListDataSource<CombinationPost> getCombinationPostDataSource()
    {
        return _combinationPostDataSource;
    }

    public void setCombinationPostDataSource(DynamicListDataSource<CombinationPost> combinationPostDataSource)
    {
        _combinationPostDataSource = combinationPostDataSource;
    }

    public ISelectModel getOrgUnitModelFilter()
    {
        return _orgUnitModelFilter;
    }

    public void setOrgUnitModelFilter(ISelectModel orgUnitModelFilter)
    {
        _orgUnitModelFilter = orgUnitModelFilter;
    }

    public ISelectModel getPostModelFilter()
    {
        return _postModelFilter;
    }

    public void setPostModelFilter(ISelectModel postModelFilter)
    {
        _postModelFilter = postModelFilter;
    }

    public DynamicListDataSource<EmployeeActingItem> getEmployeeActingListDataSource()
    {
        return _employeeActingListDataSource;
    }

    public void setEmployeeActingListDataSource(DynamicListDataSource<EmployeeActingItem> employeeActingListDataSource)
    {
        _employeeActingListDataSource = employeeActingListDataSource;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public DynamicListDataSource<VacationScheduleItem> getEmployeeVacationPlannedListDataSource()
    {
        return _employeeVacationPlannedListDataSource;
    }

    public void setEmployeeVacationPlannedListDataSource(DynamicListDataSource<VacationScheduleItem> employeeVacationPlannedListDataSource)
    {
        _employeeVacationPlannedListDataSource = employeeVacationPlannedListDataSource;
    }

    public boolean isAccessible()
    {
        return getBaseModel().isAccessible();
    }

    public Employee getEmployee()
    {
        return getBaseModel().getEmployeePost().getEmployee();
    }

    public EmployeePost getEmployeePost()
    {
        return getBaseModel().getEmployeePost();
    }

    public EmployeeCard getEmployeeCard()
    {
        return _employeeCard;
    }

    public void setEmployeeCard(EmployeeCard employeeCard)
    {
        _employeeCard = employeeCard;
    }

    public boolean isEmployeeCardTypePPS()
    {
        return _employeeCardTypePPS;
    }

    public EmployeeLabourContract getLabourContract()
    {
        return _labourContract;
    }

    public void setLabourContract(EmployeeLabourContract labourContract)
    {
        _labourContract = labourContract;
    }

    public void setEmployeeCardTypePPS(boolean employeeCardTypePPS)
    {
        _employeeCardTypePPS = employeeCardTypePPS;
    }

    public Long getRowId()
    {
        return _rowId;
    }

    public void setRowId(Long rowId)
    {
        _rowId = rowId;
    }

    public DynamicListDataSource<PersonAcademicDegree> getScienceDegreeDataSource()
    {
        return _scienceDegreeDataSource;
    }

    public void setScienceDegreeDataSource(DynamicListDataSource<PersonAcademicDegree> scienceDegreeDataSource)
    {
        _scienceDegreeDataSource = scienceDegreeDataSource;
    }

    public DynamicListDataSource<PersonAcademicStatus> getScienceStatusDataSource()
    {
        return _scienceStatusDataSource;
    }

    public void setScienceStatusDataSource(DynamicListDataSource<PersonAcademicStatus> scienceStatusDataSource)
    {
        _scienceStatusDataSource = scienceStatusDataSource;
    }

    public DynamicListDataSource<EmployeeMedicalSpeciality> getMedicalSpecDataSource()
    {
        return _medicalSpecDataSource;
    }

    public void setMedicalSpecDataSource(DynamicListDataSource<EmployeeMedicalSpeciality> medicalSpecDataSource)
    {
        this._medicalSpecDataSource = medicalSpecDataSource;
    }

    public DynamicListDataSource<EmployeeSickList> getEmployeeSickListDataSource()
    {
        return _employeeSickListDataSource;
    }

    public void setEmployeeSickListDataSource(DynamicListDataSource<EmployeeSickList> employeeSickListDataSource)
    {
        _employeeSickListDataSource = employeeSickListDataSource;
    }

    public DynamicListDataSource<EmployeeHoliday> getEmployeeHolidaysListDataSource()
    {
        return _employeeHolidaysListDataSource;
    }

    public void setEmployeeHolidaysListDataSource(DynamicListDataSource<EmployeeHoliday> employeeHolidaysListDataSource)
    {
        this._employeeHolidaysListDataSource = employeeHolidaysListDataSource;
    }

    public DynamicListDataSource<EmployeePayment> getEmployeePaymentsListDataSource()
    {
        return _employeePaymentsListDataSource;
    }

    public void setEmployeePaymentsListDataSource(DynamicListDataSource<EmployeePayment> employeePaymentsListDataSource)
    {
        this._employeePaymentsListDataSource = employeePaymentsListDataSource;
    }

    public DynamicListDataSource<ContractCollateralAgreement> getContractAgreementDataSource()
    {
        return _contractAgreementDataSource;
    }

    public void setContractAgreementDataSource(DynamicListDataSource<ContractCollateralAgreement> contractAgreementDataSource)
    {
        _contractAgreementDataSource = contractAgreementDataSource;
    }

    public EmployeePostViewUI getBaseModel()
    {
        return _baseModel;
    }

    public void setBaseModel(EmployeePostViewUI baseModel)
    {
        _baseModel = baseModel;
    }

    public boolean isMedicalSpecListVisible()
    {
        return _medicalSpecListVisible;
    }

    public void setMedicalSpecListVisible(boolean medicalSpecListVisible)
    {
        this._medicalSpecListVisible = medicalSpecListVisible;
    }
}