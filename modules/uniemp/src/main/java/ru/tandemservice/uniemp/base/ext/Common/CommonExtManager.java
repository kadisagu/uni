/* $Id:$ */
package ru.tandemservice.uniemp.base.ext.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rsizonenko
 * @since 10.03.2015
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
                .add("uniemp", () -> {

                    IUniBaseDao dao = IUniBaseDao.instance.get();
                    List<String> result = new ArrayList<>();
                    String alias = "a";

                    return result;
                })
                .create();

    }
}