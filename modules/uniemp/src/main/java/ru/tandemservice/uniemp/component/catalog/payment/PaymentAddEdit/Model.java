/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.catalog.payment.PaymentAddEdit;

import java.util.List;

import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentUnit;

/**
 * @author AutoGenerator
 * Created on 08.12.2008
 */
public class Model extends DefaultCatalogAddEditModel<Payment>
{
    private List<HSelectOption> _paymentTypesList;
    private List<PaymentUnit> _paymentUnitsList;
    
    private List<FinancingSource> _financingSourcesList;

    public List<HSelectOption> getPaymentTypesList()
    {
        return _paymentTypesList;
    }

    public void setPaymentTypesList(List<HSelectOption> paymentTypesList)
    {
        this._paymentTypesList = paymentTypesList;
    }

    public List<PaymentUnit> getPaymentUnitsList()
    {
        return _paymentUnitsList;
    }

    public void setPaymentUnitsList(List<PaymentUnit> paymentUnitsList)
    {
        this._paymentUnitsList = paymentUnitsList;
    }

    public List<FinancingSource> getFinancingSourcesList()
    {
        return _financingSourcesList;
    }

    public void setFinancingSourcesList(List<FinancingSource> financingSourcesList)
    {
        this._financingSourcesList = financingSourcesList;
    }
}