/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsTerminationNoticePrintForm;

import java.util.Date;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * Create by ashaburov
 * Date 29.07.11
 */
@Input({
    @Bind(key = "contractId", binding = "contractId"),
    @Bind(key = "contractIdList", binding = "contractIdList"),
    @Bind(key = "massPrint", binding = "massPrint")
    })
public class Model
{
    /*Поля для одиночной печати*/
    private String _numberSingleNotice;                    //Номер уведомления (одиночная печать)
    private Date _dateSingleNotice = new Date();           //Дата уведомления (одиночная печать)
    private EmployeeLabourContract _employeeLabourContract;//Трудовой договор (одиночная печать)

    /*Поля для массовой печати*/
    private List<EmployeeLabourContract> _employeeLabourContractList;             //Список трудовых договоров (массовая печать)
    private DynamicListDataSource<EmployeeLabourContract> _contractListDataSourse;//Датасорс сечлиста (массовая печать)

    /*Поля заполняющиеся с предыдущего БК*/
    private Long _contractId;          //Id контракта, получаем с предыдущего БК в случае если печатается одиночное уведомление
    private List<Long> _contractIdList;//Список id контрактов, получаем с предыдущего БК в случае если уведомления печатаются массово

    private Boolean _massPrint;        //Параметр определеяет выбрана массовая печать или одиночная

    //Getters & Setters

    public String getNumberId()
    {
        return "number_id_" + getContractListDataSourse().getCurrentEntity().getId();
    }

    public String getDateId()
    {
        return "date_id_" + getContractListDataSourse().getCurrentEntity().getId();
    }

    public String getStickerTitle()
    {
        return getMassPrint() ? "Параметры печати уведомлений о расторжении трудового договора" : "Параметры печати уведомления о расторжении трудового договора сотрудника";
    }

    public String getPage()
    {
        return Model.class.getPackage().getName() + (getMassPrint() ? ".MassPrint" : ".SinglePrint");
    }

    public DynamicListDataSource<EmployeeLabourContract> getContractListDataSourse()
    {
        return _contractListDataSourse;
    }

    public void setContractListDataSourse(DynamicListDataSource<EmployeeLabourContract> contractListDataSourse)
    {
        _contractListDataSourse = contractListDataSourse;
    }

    public List<EmployeeLabourContract> getEmployeeLabourContractList()
    {
        return _employeeLabourContractList;
    }

    public void setEmployeeLabourContractList(List<EmployeeLabourContract> employeeLabourContractList)
    {
        _employeeLabourContractList = employeeLabourContractList;
    }

    public Long getContractId()
    {
        return _contractId;
    }

    public void setContractId(Long contractId)
    {
        _contractId = contractId;
    }

    public List<Long> getContractIdList()
    {
        return _contractIdList;
    }

    public void setContractIdList(List<Long> contractIdList)
    {
        _contractIdList = contractIdList;
    }

    public Boolean getMassPrint()
    {
        return _massPrint;
    }

    public void setMassPrint(Boolean massPrint)
    {
        _massPrint = massPrint;
    }

    public String getNumberSingleNotice()
    {
        return _numberSingleNotice;
    }

    public void setNumberSingleNotice(String numberSingleNotice)
    {
        _numberSingleNotice = numberSingleNotice;
    }

    public Date getDateSingleNotice()
    {
        return _dateSingleNotice;
    }

    public void setDateSingleNotice(Date dateSingleNotice)
    {
        _dateSingleNotice = dateSingleNotice;
    }

    public EmployeeLabourContract getEmployeeLabourContract()
    {
        return _employeeLabourContract;
    }

    public void setEmployeeLabourContract(EmployeeLabourContract employeeLabourContract)
    {
        _employeeLabourContract = employeeLabourContract;
    }
}
