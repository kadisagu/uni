/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffListAllocation.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniemp.component.reports.staffList.Add.OrgUnitTypeWrapper;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 10.11.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareOrgUnitTypesListDataSource(component);
    }

    /**
     * Обработчик события выбора одного из сохраненных шаблонов
     */
    public void onChangePreset(IBusinessComponent component)
    {
        if (null != getModel(component).getFiltersPresetModel().getFiltersPreset())
        {
            getModel(component).getFiltersPresetModel().onChangePreset();
            refreshDataSourceSettings(component);
        }
    }

    /**
     * Обработчик клика по кнопке "Сохранить" в блоке шаблонов
     */
    public void onClickSaveFiltersPreset(IBusinessComponent component)
    {
        refreshSettings(component);
        getModel(component).getFiltersPresetModel().updatePreset();
    }

    /**
     * Обработчик клика по кнопке "Удалить" в блоке шаблонов
     */
    public void onClickDeleteFiltersPreset(IBusinessComponent component)
    {
        getModel(component).getFiltersPresetModel().deletePreset();
    }

    /**
     * Обновляет значения фильтров при выборе одного из сохраненных шаблонов.
     */
    public void refreshDataSourceSettings(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getFiltersPresetModel().isNewPresetSelected())
            return;

        ((CheckboxColumn)model.getOrgUnitTypesDataSource().getColumn("useOrgUnit")).setSelectedObjects(model.getSelectedOrgUnitTypesList());
        ((CheckboxColumn)model.getOrgUnitTypesDataSource().getColumn("showChildOrgUnits")).setSelectedObjects(model.getSelectedShowChildOrgUnitsList());
        ((CheckboxColumn)model.getOrgUnitTypesDataSource().getColumn("showChildOrgUnitPostsAsOwn")).setSelectedObjects(model.getSelectedShowChildOrgUnitsAsOwnList());
    }

    /**
     * Актуализирует значения настроек в локальных списках модели 
     * перед началом построения отчета
     */
    @SuppressWarnings("unchecked")
    public void refreshSettings(IBusinessComponent component)
    {
        Model model = getModel(component);
        List<OrgUnitTypeWrapper> selectedOrgUnitTypesList = new ArrayList<>((Collection) ((CheckboxColumn) model.getOrgUnitTypesDataSource().getColumn("useOrgUnit")).getSelectedObjects());
        List<OrgUnitTypeWrapper> selectedShowChildOrgUnitsList = new ArrayList<>((Collection) ((CheckboxColumn) model.getOrgUnitTypesDataSource().getColumn("showChildOrgUnits")).getSelectedObjects());
        List<OrgUnitTypeWrapper> selectedShowChildOrgUnitsAsOwnList = new ArrayList<>((Collection) ((CheckboxColumn) model.getOrgUnitTypesDataSource().getColumn("showChildOrgUnitPostsAsOwn")).getSelectedObjects());

        Map<Long, OrgUnitTypeWrapper> orgUnitTypesMap = new HashMap<>();

        for (OrgUnitTypeWrapper wrapper : model.getOrgUnitTypesList())
        {
            wrapper.setUseOrgUnit(selectedOrgUnitTypesList.contains(wrapper));
            wrapper.setShowChildOrgUnits(selectedShowChildOrgUnitsList.contains(wrapper));
            wrapper.setShowChildOrgUnitPostsAsOwn(selectedShowChildOrgUnitsAsOwnList.contains(wrapper));
            orgUnitTypesMap.put(wrapper.getId(), wrapper);
        }

        model.setOrgUnitTypesMap(orgUnitTypesMap);
        List<IEntity> selectedOrgUnitTypes = new ArrayList<>();
        List<IEntity> selectedShowChildOrgUnits = new ArrayList<>();
        List<IEntity> selectedShowChildOrgUnitsAsOwn = new ArrayList<>();
        selectedOrgUnitTypes.addAll(selectedOrgUnitTypesList);
        selectedShowChildOrgUnits.addAll(selectedShowChildOrgUnitsList);
        selectedShowChildOrgUnitsAsOwn.addAll(selectedShowChildOrgUnitsAsOwnList);

        model.setSelectedOrgUnitTypesList(selectedOrgUnitTypes);
        model.setSelectedShowChildOrgUnitsList(selectedShowChildOrgUnits);
        model.setSelectedShowChildOrgUnitsAsOwnList(selectedShowChildOrgUnitsAsOwn);
    }

    public void onClickApply(IBusinessComponent component)
    {
        refreshSettings(component);

        getDao().update(getModel(component));
        deactivate(component);
        activateInRoot(component, new PublisherActivator(getModel(component).getReport()));
    }

    /**
     * Создает список настроек видимости типов подразделений в отчете.
     * Выставляет значения по умолчанию для колонок с чекбоксами.
     */
    private void prepareOrgUnitTypesListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getOrgUnitTypesDataSource() != null) return;

        DynamicListDataSource<OrgUnitTypeWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareOrgUnitTypesDataSource(model);
        }, model.getOrgUnitTypesList().size());

        CheckboxColumn useColumn = new CheckboxColumn("useOrgUnit", "Учитывать в отчете", true);
        if (model.getSelectedOrgUnitTypesList() != null)
            useColumn.setSelectedObjects(model.getSelectedOrgUnitTypesList());
        dataSource.addColumn(useColumn);

        dataSource.addColumn(new SimpleColumn("Наименование типа подразделения", OrgUnitTypeWrapper.P_TITLE).setClickable(false));

        CheckboxColumn showColumn = new CheckboxColumn("showChildOrgUnits", "Обособ- ленное подразд-е", false);
        if (model.getSelectedShowChildOrgUnitsList() != null)
            showColumn.setSelectedObjects(model.getSelectedShowChildOrgUnitsList());
        dataSource.addColumn(showColumn);

        CheckboxColumn showAsOwnColumn = new CheckboxColumn("showChildOrgUnitPostsAsOwn", "Выводить должности дочерних подразд-й как свои", false);
        dataSource.addColumn(showAsOwnColumn);

        model.setOrgUnitTypesDataSource(dataSource);
    }
}