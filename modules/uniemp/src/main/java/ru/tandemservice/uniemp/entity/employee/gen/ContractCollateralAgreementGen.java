package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дополнительное соглашение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ContractCollateralAgreementGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement";
    public static final String ENTITY_NAME = "contractCollateralAgreement";
    public static final int VERSION_HASH = -708369984;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTRACT = "contract";
    public static final String P_NUMBER = "number";
    public static final String P_DATE = "date";
    public static final String P_DESCRIPTION = "description";
    public static final String L_AGREEMENT_FILE = "agreementFile";
    public static final String P_AGREEMENT_FILE_NAME = "agreementFileName";
    public static final String P_AGREEMENT_FILE_TYPE = "agreementFileType";

    private EmployeeLabourContract _contract;     // Трудовой договор
    private String _number;     // Номер
    private Date _date;     // Дата открытия
    private String _description;     // Описание
    private DatabaseFile _agreementFile;     // Файл соглашения
    private String _agreementFileName;     // Название файла
    private String _agreementFileType;     // Тип файла

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     */
    @NotNull
    public EmployeeLabourContract getContract()
    {
        return _contract;
    }

    /**
     * @param contract Трудовой договор. Свойство не может быть null.
     */
    public void setContract(EmployeeLabourContract contract)
    {
        dirty(_contract, contract);
        _contract = contract;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата открытия. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата открытия. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Описание. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание. Свойство не может быть null.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Файл соглашения.
     */
    public DatabaseFile getAgreementFile()
    {
        return _agreementFile;
    }

    /**
     * @param agreementFile Файл соглашения.
     */
    public void setAgreementFile(DatabaseFile agreementFile)
    {
        dirty(_agreementFile, agreementFile);
        _agreementFile = agreementFile;
    }

    /**
     * @return Название файла.
     */
    @Length(max=255)
    public String getAgreementFileName()
    {
        return _agreementFileName;
    }

    /**
     * @param agreementFileName Название файла.
     */
    public void setAgreementFileName(String agreementFileName)
    {
        dirty(_agreementFileName, agreementFileName);
        _agreementFileName = agreementFileName;
    }

    /**
     * @return Тип файла.
     */
    @Length(max=255)
    public String getAgreementFileType()
    {
        return _agreementFileType;
    }

    /**
     * @param agreementFileType Тип файла.
     */
    public void setAgreementFileType(String agreementFileType)
    {
        dirty(_agreementFileType, agreementFileType);
        _agreementFileType = agreementFileType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ContractCollateralAgreementGen)
        {
            setContract(((ContractCollateralAgreement)another).getContract());
            setNumber(((ContractCollateralAgreement)another).getNumber());
            setDate(((ContractCollateralAgreement)another).getDate());
            setDescription(((ContractCollateralAgreement)another).getDescription());
            setAgreementFile(((ContractCollateralAgreement)another).getAgreementFile());
            setAgreementFileName(((ContractCollateralAgreement)another).getAgreementFileName());
            setAgreementFileType(((ContractCollateralAgreement)another).getAgreementFileType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ContractCollateralAgreementGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ContractCollateralAgreement.class;
        }

        public T newInstance()
        {
            return (T) new ContractCollateralAgreement();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "contract":
                    return obj.getContract();
                case "number":
                    return obj.getNumber();
                case "date":
                    return obj.getDate();
                case "description":
                    return obj.getDescription();
                case "agreementFile":
                    return obj.getAgreementFile();
                case "agreementFileName":
                    return obj.getAgreementFileName();
                case "agreementFileType":
                    return obj.getAgreementFileType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "contract":
                    obj.setContract((EmployeeLabourContract) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "agreementFile":
                    obj.setAgreementFile((DatabaseFile) value);
                    return;
                case "agreementFileName":
                    obj.setAgreementFileName((String) value);
                    return;
                case "agreementFileType":
                    obj.setAgreementFileType((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "contract":
                        return true;
                case "number":
                        return true;
                case "date":
                        return true;
                case "description":
                        return true;
                case "agreementFile":
                        return true;
                case "agreementFileName":
                        return true;
                case "agreementFileType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "contract":
                    return true;
                case "number":
                    return true;
                case "date":
                    return true;
                case "description":
                    return true;
                case "agreementFile":
                    return true;
                case "agreementFileName":
                    return true;
                case "agreementFileType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "contract":
                    return EmployeeLabourContract.class;
                case "number":
                    return String.class;
                case "date":
                    return Date.class;
                case "description":
                    return String.class;
                case "agreementFile":
                    return DatabaseFile.class;
                case "agreementFileName":
                    return String.class;
                case "agreementFileType":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ContractCollateralAgreement> _dslPath = new Path<ContractCollateralAgreement>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ContractCollateralAgreement");
    }
            

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getContract()
     */
    public static EmployeeLabourContract.Path<EmployeeLabourContract> contract()
    {
        return _dslPath.contract();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата открытия. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Файл соглашения.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getAgreementFile()
     */
    public static DatabaseFile.Path<DatabaseFile> agreementFile()
    {
        return _dslPath.agreementFile();
    }

    /**
     * @return Название файла.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getAgreementFileName()
     */
    public static PropertyPath<String> agreementFileName()
    {
        return _dslPath.agreementFileName();
    }

    /**
     * @return Тип файла.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getAgreementFileType()
     */
    public static PropertyPath<String> agreementFileType()
    {
        return _dslPath.agreementFileType();
    }

    public static class Path<E extends ContractCollateralAgreement> extends EntityPath<E>
    {
        private EmployeeLabourContract.Path<EmployeeLabourContract> _contract;
        private PropertyPath<String> _number;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _description;
        private DatabaseFile.Path<DatabaseFile> _agreementFile;
        private PropertyPath<String> _agreementFileName;
        private PropertyPath<String> _agreementFileType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getContract()
     */
        public EmployeeLabourContract.Path<EmployeeLabourContract> contract()
        {
            if(_contract == null )
                _contract = new EmployeeLabourContract.Path<EmployeeLabourContract>(L_CONTRACT, this);
            return _contract;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(ContractCollateralAgreementGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата открытия. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(ContractCollateralAgreementGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(ContractCollateralAgreementGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Файл соглашения.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getAgreementFile()
     */
        public DatabaseFile.Path<DatabaseFile> agreementFile()
        {
            if(_agreementFile == null )
                _agreementFile = new DatabaseFile.Path<DatabaseFile>(L_AGREEMENT_FILE, this);
            return _agreementFile;
        }

    /**
     * @return Название файла.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getAgreementFileName()
     */
        public PropertyPath<String> agreementFileName()
        {
            if(_agreementFileName == null )
                _agreementFileName = new PropertyPath<String>(ContractCollateralAgreementGen.P_AGREEMENT_FILE_NAME, this);
            return _agreementFileName;
        }

    /**
     * @return Тип файла.
     * @see ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement#getAgreementFileType()
     */
        public PropertyPath<String> agreementFileType()
        {
            if(_agreementFileType == null )
                _agreementFileType = new PropertyPath<String>(ContractCollateralAgreementGen.P_AGREEMENT_FILE_TYPE, this);
            return _agreementFileType;
        }

        public Class getEntityClass()
        {
            return ContractCollateralAgreement.class;
        }

        public String getEntityName()
        {
            return "contractCollateralAgreement";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
