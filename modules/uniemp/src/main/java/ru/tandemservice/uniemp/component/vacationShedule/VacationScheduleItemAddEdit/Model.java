/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemAddEdit;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

/**
 * @author dseleznev
 * Created on: 21.01.2011
 */
@Input({@Bind(key = "vacationScheduleItemId", binding = "vacationScheduleItemId"),
        @Bind(key = "editForm", binding = "editForm"),
        @Bind(key = "employeePostId", binding = "employeePostId")})
public class Model
{
    private Long _vacationScheduleItemId;
    private VacationScheduleItem _vacationScheduleItem;
    private boolean _editForm;
    private List<VacationSchedule> _vacationScheduleList = new ArrayList<>();
    private Long _employeePostId;
    private boolean _disabledField;

    public boolean isDisabledField()
    {
        return _disabledField;
    }

    public void setDisabledField(boolean disabledField)
    {
        _disabledField = disabledField;
    }

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        this._employeePostId = employeePostId;
    }

    public List<VacationSchedule> getVacationScheduleList()
    {
        return _vacationScheduleList;
    }

    public void setVacationScheduleList(List<VacationSchedule> vacationScheduleList)
    {
        _vacationScheduleList = vacationScheduleList;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public Long getVacationScheduleItemId()
    {
        return _vacationScheduleItemId;
    }

    public void setVacationScheduleItemId(Long vacationScheduleItemId)
    {
        this._vacationScheduleItemId = vacationScheduleItemId;
    }

    public VacationScheduleItem getVacationScheduleItem()
    {
        return _vacationScheduleItem;
    }

    public void setVacationScheduleItem(VacationScheduleItem vacationScheduleItem)
    {
        this._vacationScheduleItem = vacationScheduleItem;
    }
}