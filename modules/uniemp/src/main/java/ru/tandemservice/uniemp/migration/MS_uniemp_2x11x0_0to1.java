package ru.tandemservice.uniemp.migration;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.List;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.processor;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniemp_2x11x0_0to1 extends IndependentMigrationScript
{

    protected final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.11.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность medicalEducationLevel

		// удалено свойство educationLevel
		{
            List<Object[]> invalidRecordsIds = tool.executeQuery(
                    processor(Long.class),
                    "select medicaleducationlevel_t.id FROM medicaleducationlevel_t " +
                            "INNER JOIN educationLevels_t " +
                            "  ON medicaleducationlevel_t.educationlevel_id = educationLevels_t.id " +
                            "WHERE medicaleducationlevel_t.educationlevel_id IS NOT NULL " +
                            "  AND educationlevels_t.eduprogramsubject_id IS NULL;"
            );

            if (CollectionUtils.isNotEmpty(invalidRecordsIds))
            {
                StringBuilder errorMessageBuilder = new StringBuilder();
                for (final Object[] row : invalidRecordsIds) {
                    errorMessageBuilder.append("Не удалось установить направление подготовки для врачебной специализации с id = ")
                            .append(String.valueOf((Long)row[0])).append("\n");
                }
                logger.error(errorMessageBuilder.toString());
                throw new IllegalStateException(errorMessageBuilder.toString());
            }

            List<Object[]> rows = tool.executeQuery(
                    processor(Long.class, Long.class),
                    "SELECT medicaleducationlevel_t.id, educationLevels_t.EDUPROGRAMSUBJECT_ID " +
                            "FROM medicaleducationlevel_t " +
                            "INNER JOIN educationLevels_t " +
                                "ON medicaleducationlevel_t.educationlevel_id = educationLevels_t.id " +
                            "WHERE medicaleducationlevel_t.educationlevel_id IS NOT NULL"
            );

			// удалить колонку
			tool.dropColumn("medicaleducationlevel_t", "educationlevel_id");

            // создано свойство programSubject
			tool.createColumn("medicaleducationlevel_t", new DBColumn("programsubject_id", DBType.LONG));

            MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater(
                    "UPDATE medicaleducationlevel_t " +
                            "SET programsubject_id = ? " +
                            "WHERE id = ?",
                    DBType.LONG, DBType.LONG);

            for (final Object[] row : rows)
            {
                Long medEduLevelId = (Long) row[0];
                Long programSubjectId = (Long) row[1];
                updater.addBatch(programSubjectId, medEduLevelId);
            }
            updater.executeUpdate(tool);

		}


    }
}