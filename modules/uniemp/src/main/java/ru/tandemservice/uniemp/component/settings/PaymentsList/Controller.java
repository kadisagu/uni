/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.settings.PaymentsList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.catalog.Payment;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 29.09.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        prepareDataSource(context);
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    private void prepareDataSource(IBusinessComponent context)
    {
        if (getModel(context).getDataSource() != null) return;
        DynamicListDataSource<Payment> dataSource = new DynamicListDataSource<>(context, this);
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Название", Payment.P_TITLE);
        linkColumn.setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Map<String, Object> params = new HashMap<>();
                params.put("paymentId", entity.getId());
                return params;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return IUniempComponents.PAYMENT_BASE_VALUES_PUB;
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Тип выплаты", Payment.TYPE_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Базовое значение", Payment.P_VALUE,  DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формат ввода выплаты", Payment.UNIT_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", Payment.FINANCING_SOURCE_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Используется", Payment.P_ACTIVE));
        dataSource.addColumn(new BooleanColumn("Обязательная", Payment.P_REQUIRED));
        dataSource.addColumn(new BooleanColumn("Не зависит от ставки", Payment.P_DOESNT_DEPEND_ON_STAFF_RATE));
        dataSource.addColumn(new BooleanColumn("Разовая", Payment.P_ONE_TIME_PAYMENT));
        getModel(context).setDataSource(dataSource);
    }
}