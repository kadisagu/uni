package ru.tandemservice.uniemp.dao;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.*;

import java.util.List;

public interface IStaffListDAO extends IUniDao<Object>
{
    String STAFF_LIST_DAO_BEAN_NAME = "staffListDao";

    StaffList getActiveStaffList(OrgUnit orgUnit);

    StaffListItem getActiveStaffListItemList(OrgUnitTypePostRelation orgUnitTypePostRelation, OrgUnit orgUnit, FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    boolean getUsingInAnyStaffList(OrgUnitPostRelation rel);

    List<StaffListItem> getStaffListItem(OrgUnit orgUnit, PostBoundedWithQGandQL postBnd);

    List<StaffListItem> getStaffListItemsList(StaffList staffList);

    List<StaffListAllocationItem> getStaffListAllocationItemsList(StaffList staffList);

    List<StaffListPostPayment> getStaffListPostPaymentsList(StaffList staffList);

    List<StaffListPostPayment> getStaffListPostPaymentsList(OrgUnit orgUnit, PostBoundedWithQGandQL post);

    List<StaffListPaymentBase> getStaffListAllocPaymentsList(StaffList staffList);

    Double getSumOfPayments(StaffListAllocationItem allocItem, boolean includeMonthSalaryFundPayments);

    Double getMonthSalaryFundPaymentValue(StaffListPaymentBase payment);

    Double getSumOfPayments(StaffListItem staffListItem, boolean includeMonthSalaryFundPayments);

    List<StaffListAllocationItem> getStaffListItemAllocations(StaffListItem staffListItem);

    int getFreePostsListForOrgUnitCount(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, String filter, QualificationLevel qLevel, ProfQualificationGroup pqGroup);

    List<OrgUnitTypePostRelation> getFreePostsListForOrgUnit(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, String filter, QualificationLevel qLevel, ProfQualificationGroup pqGroup);

    List<OrgUnitTypePostRelation> getFreePostsListForOrgUnit(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, String filter, QualificationLevel qLevel, ProfQualificationGroup pqGroup, Integer takeTopElementsCount);

    List<StaffListAllocationItem> getStaffListAllocationItemsList(StaffList staffList, PostBoundedWithQGandQL postBnd);

    /**
     * @return Возвращает Должность штатного рассписания из активного ШР указанного падразделения и ист.фин.
     */
    StaffListItem getStaffListItem(OrgUnit orgUnit, PostBoundedWithQGandQL postBnd, FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    /**
     * Возвращает список Источников финансирования, которые используются в Штатном расписании для должности.
     * @return Список возможных ист.фин. для ставок.
     */
    List<FinancingSource> getFinancingSourcesStaffListItems(OrgUnit orgUnit, PostBoundedWithQGandQL  postRelation);

    /**
     * Возвращает список Источников финансирования (детально), которые используются в Штатном расписании для должности.<p>
     * Если для подразделения нет активного ШР, то достаются соовтетствующие ист.фин.(дет.) из справочника.
     * @param filter строка, содержание которой проверяется в названии ист.фин.(дет.)
     * @return Список возможных ист.фин. (дет.).
     */
    List<FinancingSourceItem> getFinancingSourcesItemStaffListItems(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, FinancingSource financingSource, String filter);

    /**
     * Возвращает список ставок, на которых назначены неактивные сотрудники.
     * @return Список объектов штатной растановки(ставки) которые можно занять.
     */
    List<StaffListAllocationItem> getFreeStaffListAllocItems(OrgUnit orgUnit, OrgUnitTypePostRelation postRelation, FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    /**
     * Возвращает ставки с не указанными сотрудниками.
     * @return Список объектов штатной растановки(ставки) которые можно занять.
     */
    List<StaffListAllocationItem> getEmptyStaffListAllocItems(OrgUnit orgUnit, OrgUnitTypePostRelation postRelation, FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    /**
     * Проверяет, что для должности есть свободная ставка(без назначенного сотрудника) или возможность создать новую ставку.
     * @return Возможно ли создать новую ставку.
     */
    boolean isPossibleAddNewStaffRate(OrgUnit orgUnit, PostBoundedWithQGandQL postBounded, FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    /**
     * Возвращает элементы Штатной расстановки определенного ист.фин. и должности, ссылающиеся на указанного сотрудника.
     * @return Список ставок сотрудника по определенному ист.фин.
     */
    List<StaffListAllocationItem> getStaffListAllocationItemList(StaffList staffList, EmployeePost employeePost, PostBoundedWithQGandQL postBoundedWithQGandQL, FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    /**
     * Возвращает элементы Штатной расстановки определенного ист.фин. и должности, ссылающиеся на указанную должность по совмещению.
     * @return Список ставок совмещения по определенному ист.фин.
     */
    List<StaffListAllocationItem> getStaffListAllocationItemList(StaffList staffList, CombinationPost combinationPost, PostBoundedWithQGandQL postBoundedWithQGandQL, FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    /**
     * Возвращает список объектов Штатной расстановки у котрыех указан сотрудник.
     * @return Список ставок Штатной расстановки, которые потенциально можно занять.
     */
    List<StaffListAllocationItem> getOccupiedStaffListAllocationItem(OrgUnit orgUnit, PostBoundedWithQGandQL postRelation,FinancingSource financingSource, FinancingSourceItem financingSourceItem);

    /**
     * Возвращает список подразделений с активным ШР.
     * @return Возвращает список подразделений.
     */
    List<OrgUnit> getOrgUnitWithActiveStaffList();

    /**
     * Возвращает список выплат должностей, совпадающих указанным источникам финансирования.
     * @return Возвращает список выплат должностей Штатной расстановки.
     */
    List<StaffListPostPayment> getStaffListPostPaymentsList(OrgUnit orgUnit, PostBoundedWithQGandQL post, List<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>> finSrcPairList);

    Double getAllocItemBaseSalary(StaffListAllocationItem item);

    PostBoundedWithQGandQL getAllocItemPost(StaffListAllocationItem item);

    /**
     * Производит копирование содержимого версии штатного расписания сотрудников, их ставок и обязательных выплат
     * @param staffList - новая версия штатного расписания
     * @param staffListToCopy - версия штатного расписания, из которой нужно скопировать содержимое
     */
    void copyStaffList(StaffList staffList, StaffList staffListToCopy, boolean addNewRequiredPayments, boolean removeNonActivePayments);

    /**
     * Копирует должность штатного распределения
     * @param staffListAllocationItemId - должность штатного распределения, которую нужно скопировать
     */
    void copyStaffListAllocationItem(Long staffListAllocationItemId);
}