/**
 *$Id$
 */
package ru.tandemservice.uniemp.component.employee.EmployeeActingItemAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uniemp.entity.employee.EmployeeActingItem;

/**
 * Create by ashaburov
 * Date 03.11.11
 */
@Input({
        @Bind(key = "id", binding = "entityId"),
        @Bind(key = "employeePostId", binding = "employeePostId")
})
public class Model
{
    private Long _entityId;
    private EmployeeActingItem _entity;
    private Long _employeePostId;
    private EmployeePost _employeePost;

    private ISingleSelectModel _orgUnitModel;
    private ISingleSelectModel _postModel;
    private ISingleSelectModel _employeePostModel;

    //Getters & Setters

    public Long getEmployeePostId()
    {
        return _employeePostId;
    }

    public void setEmployeePostId(Long employeePostId)
    {
        _employeePostId = employeePostId;
    }

    public Long getEntityId()
    {
        return _entityId;
    }

    public void setEntityId(Long entityId)
    {
        _entityId = entityId;
    }

    public EmployeeActingItem getEntity()
    {
        return _entity;
    }

    public void setEntity(EmployeeActingItem entity)
    {
        _entity = entity;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public ISingleSelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISingleSelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public ISingleSelectModel getPostModel()
    {
        return _postModel;
    }

    public void setPostModel(ISingleSelectModel postModel)
    {
        _postModel = postModel;
    }

    public ISingleSelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISingleSelectModel employeePostModel)
    {
        _employeePostModel = employeePostModel;
    }
}
