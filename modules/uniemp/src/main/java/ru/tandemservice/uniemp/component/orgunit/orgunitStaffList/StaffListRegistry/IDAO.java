/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.StaffListRegistry;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 20.09.2008
 */
public interface IDAO extends IUniDao<Model>
{
}