/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.StaffListRegistryChangeState;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author ashaburov
 * Created on: 08.08.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

        getDao().validate(model, errorCollector);

        if (errorCollector.hasErrors())
            return;

        getDao().update(model);

        deactivate(component);
    }
}