/**
 *$Id$
 */
package ru.tandemservice.uniemp.util;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.base.ext.Employee.logic.EmploymentCalculator;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.EmployeeServiceLengthWrapper;
import ru.tandemservice.uniemp.base.ext.Employee.logic.empServiceLengthWrappers.PeriodWrapper;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake;

import java.util.*;

/**
 * Create by ashaburov
 * Date 13.10.11
 */
public class UniempUtil
{
    /**
     * Возвращает сумму выплат включенных в выплату на МФОТ с надбавками + (Базовый Оклад * ставку).
     * @param paymentList список выплат включенных в выплату на МФОТ с надбавками
     * @return Сумму в рублях.
     */
    public static double getPaymentOnFOTAmount(EmployeePost employeePost, List<Payment> paymentList)
    {
        Double resultAmount = 0d;
        double baseSalary = employeePost.getSalary() != null ? employeePost.getSalary() : 0d;
        Map<Payment, Double> employeePaymentMap = new HashMap<>();
        for (EmployeePayment employeePayment : UniempDaoFacade.getUniempDAO().getEmployeePostPaymentList(employeePost))
            employeePaymentMap.put(employeePayment.getPayment(), employeePayment.getPaymentValue());

        for (Payment payment : paymentList)
        {
            if (!employeePaymentMap.containsKey(payment))
                continue;

            double paymentValue = employeePaymentMap.get(payment) != null ? employeePaymentMap.get(payment) : 0d;

            switch (payment.getPaymentUnit().getCode())
            {
                case UniempDefines.PAYMENT_UNIT_RUBLES:
                    resultAmount += UniempDaoFacade.getUniempDAO().getPaymentBaseValueForPost(payment, employeePost.getPostRelation().getPostBoundedWithQGandQL());
                    break;
                case UniempDefines.PAYMENT_UNIT_COEFFICIENT:
                    resultAmount += paymentValue * baseSalary;
                    break;
                case UniempDefines.PAYMENT_UNIT_BASE_PERCENT:
                    resultAmount += (paymentValue * baseSalary) / 100;
                    break;
                case UniempDefines.PAYMENT_UNIT_FULL_PERCENT:
                    List<Payment> includePaymentList = UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(payment);
                    resultAmount += paymentValue * getPaymentOnFOTAmount(employeePost, includePaymentList) / 100;
                    break;
            }
        }

        return resultAmount + baseSalary;
    }

    /**
     * Подгатавливает и возвращает список типов Ежегодных оплачиваемых отпусков.
     * @return Список типов отпусков.
     */
    public static List<String> getAnnualHolidayTypes()
    {
        List<String> annualHolidayTypeList = new ArrayList<>();
        annualHolidayTypeList.add(UniempDefines.HOLIDAY_TYPE_ANNUAL);
        annualHolidayTypeList.add(UniempDefines.HOLIDAY_TYPE_HARMFUL_CONDITIONS);
        annualHolidayTypeList.add(UniempDefines.HOLIDAY_TYPE_SOME_AREAS_OF_PUBLIC_CULTURE);
        annualHolidayTypeList.add(UniempDefines.HOLIDAY_TYPE_NON_NORMED_WORK_DAY);
        annualHolidayTypeList.add(UniempDefines.HOLIDAY_TYPE_EDGE_NORTH);
        annualHolidayTypeList.add(UniempDefines.HOLIDAY_TYPE_WORKING_AT_POLLUTED_REGION);
        annualHolidayTypeList.add(UniempDefines.HOLIDAY_TYPE_CHERNOBYL_LIQUIDATION);

        return annualHolidayTypeList;
    }

    /**
     * Вычисляет Дату окончания по Дате начала и Длительности, с учетом производственного календаря.<p/>
     * Если duration отрицательное, то Дата окончания вычисляется "в другую сторну", т.е. она будет раньше Даты начала.
     */
    public static Date getEndDate(EmployeeWorkWeekDuration workWeekDuration, int duration, Date beginDate)
    {
        GregorianCalendar date = (GregorianCalendar) GregorianCalendar.getInstance();
        int counter = 0;

        date.setTime(beginDate);

        if (duration > 0)
        {
            do
            {
                if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                    counter++;

                if (counter != duration)
                    date.add(Calendar.DATE, 1);
            }
            while (counter < duration);


            return date.getTime();
        }
        else if (duration < 0)
        {
            do
            {
                if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                    counter--;

                if (counter != duration)
                    date.add(Calendar.DATE, -1);
            }
            while (counter > duration);

            date.add(Calendar.DATE, -1);
            return date.getTime();
        }

        return beginDate;
    }

    /**
     * Вычесляет суммарный стаж по списку периодов стажа определенного типа.
     * @param date дата на которую вычесляется
     * @param periodList список периодов стажа одного типа. <b>Note:</b> Периоды могут пересикаться
     * @return <code>TripletKey(days, months, years)</code>
     */
    public static TripletKey<Integer, Integer, Integer> getServiceLengthTypeDuration(Date date, Collection<CoreCollectionUtils.Pair<Date, Date>> periodList)
    {
        if (null == date)
            date = new Date();

        int years = 0;
        int months = 0;
        int days = 0;
        if (null != periodList)
        {
            EmploymentCalculator calc = new EmploymentCalculator();
            calc.setCurrentDate(date);
            List<CoreCollectionUtils.Pair<Date, Date>> periods = new ArrayList<>();
            for (CoreCollectionUtils.Pair<Date, Date> period : periodList)
            {
                Date beginDate = period.getX();
                Date endDate = null == period.getY() ? date : period.getY();
                if (beginDate.compareTo(date) > 0)
                    continue;
                if (endDate.compareTo(date) > 0)
                    endDate = date;
                periods.add(new CoreCollectionUtils.Pair<>(beginDate, endDate));
            }
            calc.calculateFullEmploymentPeriod(periods);
            years = calc.getYearsAmount();
            months = calc.getMonthsAmount();
            days = calc.getDaysAmount();
        }

        return new TripletKey<>(days, months, years);
    }

    /**
     * Вычесляет суммарный стаж по списку периодов стажа определенного типа.
     * @param date дата на которую вычесляется
     * @param periodWrapperList список периодов стажа одного типа. <b>Note:</b> Периоды могут пересикаться
     * @return <code>TripletKey(days, months, years)</code>
     */
    public static TripletKey<Integer, Integer, Integer> getServiceLengthTypeDuration(Date date, List<PeriodWrapper> periodWrapperList)
    {
        if (null == date)
            date = new Date();

        final Collection<CoreCollectionUtils.Pair<Date, Date>> periodList = new ArrayList<>();
        for (PeriodWrapper periodWrapper : periodWrapperList)
        {
            periodList.add(new CoreCollectionUtils.Pair<>(periodWrapper.getBeginDate(), periodWrapper.getEndDate()));
        }

        int years;
        int months;
        int days;
        EmploymentCalculator calc = new EmploymentCalculator();
        calc.setCurrentDate(date);
        List<CoreCollectionUtils.Pair<Date, Date>> periods = new ArrayList<>();
        for (CoreCollectionUtils.Pair<Date, Date> period : periodList)
        {
            Date beginDate = period.getX();
            Date endDate = null == period.getY() ? date : period.getY();
            if (beginDate.compareTo(date) > 0)
                continue;
            if (endDate.compareTo(date) > 0)
                endDate = date;
            periods.add(new CoreCollectionUtils.Pair<>(beginDate, endDate));
        }
        calc.calculateFullEmploymentPeriod(periods);
        years = calc.getYearsAmount();
        months = calc.getMonthsAmount();
        days = calc.getDaysAmount();

        return new TripletKey<>(days, months, years);
    }

    /**
     * Вычесляет враперы Стажей Кадрового ресурса.<p/>
     * Вычесления происходят на основе фейковых и реальных объектов Истории должностей.
     */
    public static List<EmployeeServiceLengthWrapper> getEmployeeServiceLengthWrapperList(List<EmploymentHistoryItemBase> historyItemList, Map<EmploymentHistoryItemBase, List<ServiceLengthType>> historyItemRelationMap)
    {
        // если для КР нет Элементов истории должностей, то и синхрнизировать не с чем
        if ((historyItemList == null || historyItemList.isEmpty()))
            return new ArrayList<>();

        // создаем Враперы Стажа по Истории должностей
        List<EmployeeServiceLengthWrapper> resultWrapperList = new ArrayList<>();
        for (EmploymentHistoryItemBase historyItem : historyItemList)
        {
            if (historyItemRelationMap.containsKey(historyItem))
                for (ServiceLengthType type : historyItemRelationMap.get(historyItem))
                {
                    EmployeeServiceLengthWrapper resultWrapper = new EmployeeServiceLengthWrapper(historyItem);
                    resultWrapper.setBeginDate(historyItem.getAssignDate());
                    resultWrapper.setEndDate(historyItem.getDismissalDate());
                    resultWrapper.setType(type);
                    resultWrapper.setComment(historyItem instanceof EmploymentHistoryItemFake ? ((EmploymentHistoryItemFake) historyItem).getComment() : null);

                    resultWrapperList.add(resultWrapper);
                }
        }

        return resultWrapperList;
    }
}
