/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.ContractFileAdd;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * @author dseleznev
 * Created on: 12.11.2008
 */
@Input(keys = "labourContractId", bindings = "labourContractId")
public class Model
{
    private Long _labourContractId;
    private EmployeeLabourContract _labourContract;
    private IUploadFile _file;

    public Long getLabourContractId()
    {
        return _labourContractId;
    }

    public void setLabourContractId(Long labourContractId)
    {
        this._labourContractId = labourContractId;
    }

    public EmployeeLabourContract getLabourContract()
    {
        return _labourContract;
    }

    public void setLabourContract(EmployeeLabourContract labourContract)
    {
        this._labourContract = labourContract;
    }

    public IUploadFile getFile()
    {
        return _file;
    }

    public void setFile(IUploadFile file)
    {
        this._file = file;
    }
    
    public boolean isAddForm()
    {
        return null == getLabourContract().getContractFile();
    } 

}