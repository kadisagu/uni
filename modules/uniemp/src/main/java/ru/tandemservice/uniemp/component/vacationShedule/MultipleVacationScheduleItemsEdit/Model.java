/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.MultipleVacationScheduleItemsEdit;

import java.util.Date;
import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

/**
 * @author dseleznev
 * Created on: 21.01.2011
 */
@Input(@Bind(key = "vacationScheduleItemIdsList", binding = "vacationScheduleItemIdsList"))
public class Model
{
    private List<Long> _vacationScheduleItemIdsList;
    private List<VacationScheduleItem> _vacationScheduleItemsList;
    private Date _vacationPlanDate;

    public List<Long> getVacationScheduleItemIdsList()
    {
        return _vacationScheduleItemIdsList;
    }

    public void setVacationScheduleItemIdsList(List<Long> vacationScheduleItemIdsList)
    {
        this._vacationScheduleItemIdsList = vacationScheduleItemIdsList;
    }

    public List<VacationScheduleItem> getVacationScheduleItemsList()
    {
        return _vacationScheduleItemsList;
    }

    public void setVacationScheduleItemsList(List<VacationScheduleItem> vacationScheduleItemsList)
    {
        this._vacationScheduleItemsList = vacationScheduleItemsList;
    }

    public Date getVacationPlanDate()
    {
        return _vacationPlanDate;
    }

    public void setVacationPlanDate(Date vacationPlanDate)
    {
        this._vacationPlanDate = vacationPlanDate;
    }
}