/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.employeeMovePPS.Add;

import java.util.Date;

/**
 * @author dseleznev
 * Created on: 17.04.2009
 */
public class Model
{
    private Date _reportDateFrom;
    private Date _reportDateTo;

    public Date getReportDateFrom()
    {
        return _reportDateFrom;
    }

    public void setReportDateFrom(Date reportDateFrom)
    {
        this._reportDateFrom = reportDateFrom;
    }

    public Date getReportDateTo()
    {
        return _reportDateTo;
    }

    public void setReportDateTo(Date reportDateTo)
    {
        this._reportDateTo = reportDateTo;
    }

}