package ru.tandemservice.uniemp.dao;

import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.ProfQualificationGroup;
import org.tandemframework.shared.employeebase.catalog.entity.QualificationLevel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.*;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;

import java.util.*;

public class StaffListDAO extends UniDao<Object> implements IStaffListDAO
{
//    public static final Long NEW_ITEM_ID = 0l;
//    public static Comparator<StaffListAllocationItem> SAFE_TITLE_COMPARATOR = new Comparator<StaffListAllocationItem>()
//    {
//        @Override
//        public int compare(final StaffListAllocationItem item1, final StaffListAllocationItem item2)
//        {
//            if (null == item1.getEmployeePost()) return -1;
//            else if (null == item2.getEmployeePost()) return 1;
//            return item1.getSafeTitle().compareTo(item2.getSafeTitle());
//        }
//    };

    public class SafeTitleComparator implements Comparator<StaffListAllocationItem>
    {
        private Map<StaffListAllocationItem, Integer> _corrPointsMap;

        public SafeTitleComparator(Map<StaffListAllocationItem, Integer> corrPointsMap)
        {
            _corrPointsMap = corrPointsMap;
        }

        @Override
        public int compare(StaffListAllocationItem item1, StaffListAllocationItem item2)
        {
            if (null != _corrPointsMap)
            {
                Integer item1Points = null != _corrPointsMap.get(item1) ? _corrPointsMap.get(item1) : 0;
                Integer item2Points = null != _corrPointsMap.get(item2) ? _corrPointsMap.get(item2) : 0;
                return item2Points.compareTo(item1Points);
            }
            else
            {
                if (null == item1.getEmployeePost()) return -1;
                else if (null == item2.getEmployeePost()) return 1;
                return item1.getSafeTitle().compareTo(item2.getSafeTitle());
            }
        }
    }

    @Override
    public StaffList getActiveStaffList(OrgUnit orgUnit)
    {
        MQBuilder builder = new MQBuilder(StaffList.ENTITY_CLASS, "sl");
        builder.add(MQExpression.eq("sl", StaffList.L_ORG_UNIT, orgUnit));
        builder.add(MQExpression.eq("sl", StaffList.L_STAFF_LIST_STATE + "." + StaffListState.P_CODE, UniempDefines.STAFF_LIST_STATUS_ACTIVE));
        if (builder.getResultCount(getSession()) > 1) throw new ApplicationException("У подразделения " + orgUnit.getFullTitle() + " не должно быть двух и более активных версий штатного расписания.");
        else if (builder.getResultCount(getSession()) != 1) return null;
        return (StaffList) builder.uniqueResult(getSession());
    }

    @Override
    public StaffListItem getActiveStaffListItemList(OrgUnitTypePostRelation orgUnitTypePostRelation, OrgUnit orgUnit, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, getActiveStaffList(orgUnit)));
        builder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION, orgUnitTypePostRelation));
        builder.add(MQExpression.eq("sli", StaffListItem.L_FINANCING_SOURCE, financingSource));
        builder.add(MQExpression.eq("sli", StaffListItem.L_FINANCING_SOURCE_ITEM, financingSourceItem));

        if (builder.getResultCount(getSession()) != 1)
            return null;

        return (StaffListItem) builder.uniqueResult(getSession());
    }

    @Override
    public boolean getUsingInAnyStaffList(OrgUnitPostRelation rel)
    {
        return getSession().createCriteria(StaffListItem.class).add(Restrictions.eq(StaffListItem.L_ORG_UNIT_POST_RELATION, rel)).list().size() > 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<StaffListItem> getStaffListItemsList(StaffList staffList)
    {
        return getSession().createCriteria(StaffListItem.class).add(Restrictions.eq(StaffListItem.L_STAFF_LIST, staffList)).list();
    }

    @Override
    public List<StaffListAllocationItem> getStaffListAllocationItemsList(StaffList staffList)
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "sla");
        builder.add(MQExpression.eq("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffList));
        return builder.getResultList(getSession());
    }

    @Override
    public List<StaffListAllocationItem> getStaffListAllocationItemList(StaffList staffList, EmployeePost employeePost, PostBoundedWithQGandQL postBoundedWithQGandQL, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), staffList));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), postBoundedWithQGandQL));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.combination().s(), false));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().s(), employeePost));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.financingSource().s(), financingSource));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.financingSourceItem().s(), financingSourceItem));

        return builder.getResultList(getSession());
    }

    @Override
    public List<StaffListAllocationItem> getStaffListAllocationItemList(StaffList staffList, CombinationPost combinationPost, PostBoundedWithQGandQL postBoundedWithQGandQL, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), staffList));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), postBoundedWithQGandQL));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.combination().s(), true));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.combinationPost().s(), combinationPost));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().s(), combinationPost.getEmployeePost()));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.financingSource().s(), financingSource));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.financingSourceItem().s(), financingSourceItem));

        return builder.getResultList(getSession());
    }

    @Override
    public List<StaffListPostPayment> getStaffListPostPaymentsList(StaffList staffList)
    {
        MQBuilder builder = new MQBuilder(StaffListPostPayment.ENTITY_CLASS, "slpp");
        builder.add(MQExpression.eq("slpp", StaffListPostPayment.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffList));
        return builder.getResultList(getSession());
    }

    @Override
    public List<StaffListPostPayment> getStaffListPostPaymentsList(OrgUnit orgUnit, PostBoundedWithQGandQL post)
    {
        if (null == orgUnit || null == post) return null;

        StaffList activeStaffList = getActiveStaffList(orgUnit);
        if (null == activeStaffList) return null;

        List<StaffListItem> staffListItems = getStaffListItem(activeStaffList, post);
        if (null == staffListItems) return null;

        List<AbstractExpression> expressionList = new ArrayList<>();
        MQBuilder builder = new MQBuilder(StaffListPostPayment.ENTITY_CLASS, "slpp");

        for (StaffListItem item : staffListItems)
        {
            AbstractExpression expression = MQExpression.eq("slpp", StaffListPostPayment.L_STAFF_LIST_ITEM, item);
            expressionList.add(expression);
        }
        builder.add(MQExpression.or(expressionList.toArray(new AbstractExpression[expressionList.size()])));
        builder.addOrder("slpp", StaffListPostPayment.L_PAYMENT + "." + Payment.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public List<StaffListPaymentBase> getStaffListAllocPaymentsList(StaffList staffList)
    {
        MQBuilder builder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "slpp");
        builder.add(MQExpression.eq("slpp", StaffListPaymentBase.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffList));
        return builder.getResultList(getSession());
    }

    @Override
    public Double getSumOfPayments(StaffListAllocationItem allocItem, boolean includeMonthSalaryFundPayments)
    {
        MQBuilder fakeBuilder = new MQBuilder(StaffListFakePayment.ENTITY_CLASS, "slfp", new String[]{StaffListFakePayment.P_ID});
        fakeBuilder.add(MQExpression.eq("slfp", StaffListFakePayment.L_STAFF_LIST_ITEM, allocItem.getStaffListItem()));
        AbstractExpression expr1 = MQExpression.notEq("slfp", StaffListFakePayment.L_STAFF_LIST_ALLOCATION_ITEM, allocItem);
        AbstractExpression expr2 = MQExpression.isNull("slfp", StaffListFakePayment.L_STAFF_LIST_ALLOCATION_ITEM);
        fakeBuilder.add(MQExpression.or(expr1, expr2));

        MQBuilder builder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "pi");
        builder.add(MQExpression.notIn("pi", StaffListPaymentBase.P_ID, fakeBuilder));
        builder.add(MQExpression.eq("pi", StaffListPaymentBase.L_STAFF_LIST_ITEM, allocItem.getStaffListItem()));
        builder.addOrder("pi", StaffListPaymentBase.L_PAYMENT + "." + Payment.P_TITLE);

        List<StaffListPaymentBase> paymentsList = builder.getResultList(getSession());

        Double paymentsSum = 0d;
        for (StaffListPaymentBase payment : paymentsList)
        {
            payment.setTargetAllocItem(allocItem);

            if (UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(payment.getPayment().getPaymentUnit().getCode()))
            {
                if (includeMonthSalaryFundPayments) paymentsSum += payment.getTotalValue();
            }
            else
            {
                paymentsSum += payment.getTotalValue();
            }
        }

        return paymentsSum;
    }

    @Override
    public Double getMonthSalaryFundPaymentValue(StaffListPaymentBase payment)
    {
        MQBuilder builder;
        if (null == payment.getTargetAllocItem())
        {
            builder = new MQBuilder(StaffListPostPayment.ENTITY_CLASS, "pi");
            builder.add(MQExpression.eq("pi", StaffListPostPayment.L_STAFF_LIST_ITEM, payment.getStaffListItem()));
        }
        else
        {
            MQBuilder subBuilder = new MQBuilder(StaffListFakePayment.ENTITY_CLASS, "ap", new String[]{StaffListFakePayment.P_ID});
            subBuilder.add(MQExpression.eq("ap", StaffListFakePayment.L_STAFF_LIST_ITEM, payment.getStaffListItem()));
            AbstractExpression nullAllocItem = MQExpression.isNull("ap", StaffListFakePayment.L_STAFF_LIST_ALLOCATION_ITEM);
            AbstractExpression notEqAllocItem = MQExpression.notEq("ap", StaffListFakePayment.L_STAFF_LIST_ALLOCATION_ITEM, payment.getTargetAllocItem());
            subBuilder.add(MQExpression.or(nullAllocItem, notEqAllocItem));

            builder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "pi");
            builder.add(MQExpression.notIn("pi", StaffListPaymentBase.P_ID, subBuilder));
            builder.add(MQExpression.eq("pi", StaffListPaymentBase.staffListItem().financingSource().s(), payment.getTargetAllocItem().getFinancingSource()));
            builder.add(MQExpression.eq("pi", StaffListPaymentBase.staffListItem().financingSourceItem().s(), payment.getTargetAllocItem().getFinancingSourceItem()));
            //в расчете используем только выплаты с аналогичным ИФ(если выплата на Бюджете, то и в сумме выплат учитываем только выплаты из Бюджета)
            builder.add(MQExpression.eq("pi", StaffListPaymentBase.financingSource(), payment.getFinancingSource()));
            if (payment.getFinancingSourceItem() != null)
                builder.add(MQExpression.eq("pi", StaffListPaymentBase.financingSourceItem(), payment.getFinancingSourceItem()));
            else
                builder.add(MQExpression.isNull("pi", StaffListPaymentBase.financingSourceItem()));
            builder.add(MQExpression.eq("pi", StaffListPaymentBase.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, payment.getStaffListItem().getStaffList()));
            builder.add(MQExpression.eq("pi", StaffListPaymentBase.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION, payment.getStaffListItem().getOrgUnitPostRelation()));
            builder.add(MQExpression.eq("pi", StaffListPaymentBase.staffListItem().s(), payment.getStaffListItem()));
        }

        if (null != payment.getId()) builder.add(MQExpression.notEq("pi", StaffListPaymentBase.P_ID, payment.getId()));

        Double paymentsSum = 0d;
        //поднимаем выплаты на которые расчитывается текущая выплата(по настройке "Порядок начисления выплат на ФОТ с надбавками")
        List<Payment> dependPaymentList = UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(payment.getPayment());
        if (dependPaymentList.isEmpty())//если не заполнена настройка, то расчитываем исходя из текущей суммы выплат
        {
            builder.add(MQExpression.notEq("pi", StaffListPaymentBase.L_PAYMENT + "." + Payment.L_PAYMENT_UNIT + "." + PaymentUnit.P_CODE, UniempDefines.PAYMENT_UNIT_FULL_PERCENT));
            for (StaffListPaymentBase paymentItem : builder.<StaffListPaymentBase>getResultList(getSession()))
            {
                if (!isTakePayment(payment, paymentItem))
                    continue;

                paymentItem.setTargetAllocItem(payment.getTargetAllocItem());
                paymentsSum += paymentItem.getTotalValue();
            }
        }
        else//если настройка для выплаты заполнена
        {
            //бежим по текущим выплатам и берем те, на которые расчитывается выплата исходя из настройки
            for (StaffListPaymentBase paymentItem : builder.<StaffListPaymentBase>getResultList(getSession()))
            {
                if (!dependPaymentList.contains(paymentItem.getPayment()))
                    continue;

                if (!isTakePayment(payment, paymentItem))
                    continue;

                double amount;
                paymentItem.setTargetAllocItem(payment.getTargetAllocItem());
                amount = paymentItem.getTotalValue();

                paymentsSum += amount;
            }
        }

        double fullSalaryRated = 0d;
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> innerKey = new CoreCollectionUtils.Pair<>(payment.getStaffListItem().getFinancingSource(), payment.getStaffListItem().getFinancingSourceItem());
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> key = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());
        if (innerKey.equals(key)) fullSalaryRated = payment.getFullSalaryRated();
        return Math.round((paymentsSum + fullSalaryRated) * payment.getAmount() * 10d) / 1000d;
    }

    private boolean isTakePayment(StaffListPaymentBase payment, StaffListPaymentBase possiblePayment)
    {
        if (possiblePayment.getPayment().isOneTimePayment())
            return false;

        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> innerKey = new CoreCollectionUtils.Pair<>(possiblePayment.getFinancingSource(), possiblePayment.getFinancingSourceItem());
        CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> key = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());

        return innerKey.equals(key);

    }

    @Override
    public Double getSumOfPayments(StaffListItem staffListItem, boolean includeMonthSalaryFundPayments)
    {
        MQBuilder builder = new MQBuilder(StaffListPostPayment.ENTITY_CLASS, "pi");
        builder.add(MQExpression.eq("pi", StaffListPostPayment.L_STAFF_LIST_ITEM, staffListItem));

        Double paymentsSum = 0d;
        for (StaffListPaymentBase payment : builder.<StaffListPaymentBase>getResultList(getSession()))
        {
            if (UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(payment.getPayment().getPaymentUnit().getCode()))
            {
                if (includeMonthSalaryFundPayments) paymentsSum += payment.getTotalValue();
            }
            else
            {
                paymentsSum += payment.getTotalValue();
            }
        }

        return paymentsSum;
    }

    @Override
    public List<StaffListAllocationItem> getStaffListItemAllocations(StaffListItem staffListItem)
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "sla");
        builder.add(MQExpression.eq("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM, staffListItem));
        return builder.getResultList(getSession());
    }

    @Override
    public int getFreePostsListForOrgUnitCount(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, String filter, QualificationLevel qLevel, ProfQualificationGroup pqGroup)
    {
        return getFreePostsListForOrgUnit(orgUnit, postBoundedWithQGandQL, filter, qLevel, pqGroup).size();
    }

    @Override
    public List<OrgUnitTypePostRelation> getFreePostsListForOrgUnit(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, String filter, QualificationLevel qLevel, ProfQualificationGroup pqGroup)
    {
        return getFreePostsListForOrgUnit(orgUnit, postBoundedWithQGandQL, filter, qLevel, pqGroup, null);
    }

    @Override
    public List<OrgUnitTypePostRelation> getFreePostsListForOrgUnit(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, String filter, QualificationLevel qLevel, ProfQualificationGroup pqGroup, Integer takeTopElementsCount)
    {
        StaffList staffList = getActiveStaffList(orgUnit);
        if (null == staffList)
        {
            MQBuilder subBuilder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep", new String[]{EmployeePost.L_POST_RELATION + ".id"});
            subBuilder.add(MQExpression.eq("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.P_MULTI_POST, Boolean.FALSE));
            subBuilder.add(MQExpression.eq("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_ORG_UNIT_TYPE, orgUnit.getOrgUnitType()));
            subBuilder.add(MQExpression.eq("ep", EmployeePost.L_POST_STATUS + "." + EmployeePostStatus.P_ACTIVE, Boolean.TRUE));
            subBuilder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT, orgUnit));
            if (null != postBoundedWithQGandQL)
                subBuilder.add(MQExpression.notEq("ep", EmployeePost.postRelation().postBoundedWithQGandQL().s(), postBoundedWithQGandQL));

            MQBuilder builder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "rel");
            builder.add(MQExpression.notIn("rel", OrgUnitTypePostRelation.P_ID, subBuilder));
            builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_ORG_UNIT_TYPE, orgUnit.getOrgUnitType()));
            if (null != qLevel) builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, qLevel));
            if (null != pqGroup) builder.add(MQExpression.eq("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, pqGroup));
            builder.add(MQExpression.like("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE, CoreStringUtils.escapeLike(filter)));
            builder.addOrder("rel", OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE);
            if (null != takeTopElementsCount) return builder.getResultList(getSession(), 0, takeTopElementsCount);
            return builder.getResultList(getSession());
        }
        else
        {
            MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
            builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, staffList));
            if (null != qLevel)
                builder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_QUALIFICATION_LEVEL, qLevel));
            if (null != pqGroup)
                builder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_PROF_QUALIFICATION_GROUP, pqGroup));
            builder.add(MQExpression.like("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE, CoreStringUtils.escapeLike(filter)));
            builder.addOrder("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE);

            List<OrgUnitTypePostRelation> resultList = new ArrayList<>();
            for (StaffListItem item : builder.<StaffListItem>getResultList(getSession()))
            {
                if (!resultList.contains(item.getOrgUnitPostRelation().getOrgUnitTypePostRelation()))
                    resultList.add(item.getOrgUnitPostRelation().getOrgUnitTypePostRelation());
            }

            if (null != takeTopElementsCount) return resultList.subList(0, Math.min(resultList.size(), takeTopElementsCount));
            return resultList;
        }
    }

    public List<StaffListAllocationItem> getStaffListAllocationItemsList(StaffList staffList, PostBoundedWithQGandQL postBnd)
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "sla");
        builder.add(MQExpression.eq("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffList));
        builder.add(MQExpression.eq("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, postBnd));
        return builder.getResultList(getSession());
    }

    public List<StaffListItem> getStaffListItem(StaffList staffList, PostBoundedWithQGandQL postBnd)
    {
        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, staffList));
        builder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, postBnd));
        if (builder.getResultCount(getSession()) == 0) return null;
        else return builder.<StaffListItem>getResultList(getSession());

    }

    @Override
    public StaffListItem getStaffListItem(OrgUnit orgUnit, PostBoundedWithQGandQL postBnd, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, getActiveStaffList(orgUnit)));
        builder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, postBnd));
        builder.add(MQExpression.eq("sli", StaffListItem.L_FINANCING_SOURCE, financingSource));
        builder.add(MQExpression.eq("sli", StaffListItem.L_FINANCING_SOURCE_ITEM, financingSourceItem));
        if (builder.getResultCount(getSession()) == 0) return null;
        else if (builder.getResultCount(getSession()) > 1)
            throw new ApplicationException("Пара Источников финансирования должна быть уникальный для должности ШР.");
        return (StaffListItem) builder.uniqueResult(getSession());

    }

    @Override
    public List<StaffListItem> getStaffListItem(OrgUnit orgUnit, PostBoundedWithQGandQL postBnd)
    {
        StaffList activeStaffList = getActiveStaffList(orgUnit);
        if (null == activeStaffList) return null;

        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        builder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, activeStaffList));
        builder.add(MQExpression.eq("sli", StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, postBnd));
        if (builder.getResultCount(getSession()) == 0) return null;
        else return builder.getResultList(getSession());
    }

    @Override
    public List<FinancingSource> getFinancingSourcesStaffListItems(OrgUnit orgUnit, PostBoundedWithQGandQL postRelation)
    {
        StaffList staffList = getActiveStaffList(orgUnit);

        List<StaffListItem> staffListItemList = getStaffListItemsList(staffList);

        List<FinancingSource> resultList = new ArrayList<>();
        for (StaffListItem item : staffListItemList)
        {
            if (item.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().equals(postRelation))
                if (!resultList.contains(item.getFinancingSource()))
                    resultList.add(item.getFinancingSource());
        }

        if (!resultList.isEmpty())
            return resultList;
        else
            return getCatalogItemList(FinancingSource.class);
    }

    @Override
    public List<FinancingSourceItem> getFinancingSourcesItemStaffListItems(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, FinancingSource financingSource, String filter)
    {
        if (financingSource == null)
            return new ArrayList<>();

        StaffList staffList = getActiveStaffList(orgUnit);

        MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "b", new String[]{StaffListItem.L_FINANCING_SOURCE_ITEM});

        builder.add(MQExpression.eq("b", StaffListItem.staffList().s(), staffList));
        builder.add(MQExpression.eq("b", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), postBoundedWithQGandQL));
        builder.add(MQExpression.eq("b", StaffListItem.financingSource().s(), financingSource));
        builder.add(MQExpression.isNotNull("b", StaffListItem.financingSourceItem().s()));
        builder.add(MQExpression.like("b", StaffListItem.financingSourceItem().title().s(), CoreStringUtils.escapeLike(filter)));
        builder.setNeedDistinct(true);

        return builder.getResultList(getSession());
    }

    @Override
    public List<StaffListAllocationItem> getFreeStaffListAllocItems(OrgUnit orgUnit, OrgUnitTypePostRelation postRelation, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        StaffList staffList = getActiveStaffList(orgUnit);

        List<StaffListAllocationItem> allocationItemList = getStaffListAllocationItemsList(staffList, postRelation.getPostBoundedWithQGandQL());

        if (allocationItemList.size() > 0)
            if (allocationItemList.get(0).getStaffListItem().getStaffRate() - allocationItemList.get(0).getStaffListItem().getOccStaffRate() <= 0)
                return new ArrayList<>();

        List<StaffListAllocationItem> resultList = new ArrayList<>();
        for (StaffListAllocationItem item : allocationItemList)
        {
            if (item.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().equals(postRelation))
                if (financingSource.equals(item.getFinancingSource()))
                    if ((financingSourceItem == null && item.getFinancingSourceItem() == null) || (financingSourceItem != null && financingSourceItem.equals(item.getFinancingSourceItem())))
                        if ((item.getEmployeePost() != null && !item.getEmployeePost().getPostStatus().isActive()))
                            resultList.add(item);
        }

        return resultList;
    }

    @Override
    public List<StaffListAllocationItem> getEmptyStaffListAllocItems(OrgUnit orgUnit, OrgUnitTypePostRelation postRelation, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        StaffList staffList = getActiveStaffList(orgUnit);

        List<StaffListAllocationItem> allocationItemList = getStaffListAllocationItemsList(staffList, postRelation.getPostBoundedWithQGandQL());

        List<StaffListAllocationItem> resultList = new ArrayList<>();
        for (StaffListAllocationItem item : allocationItemList)
        {
            if (item.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().equals(postRelation))
                if (financingSource.equals(item.getFinancingSource()))
                    if ((financingSourceItem == null && item.getFinancingSourceItem() == null) || (financingSourceItem != null && financingSourceItem.equals(item.getFinancingSourceItem())))
                        if (item.getEmployeePost() == null)
                            resultList.add(item);
        }

        return resultList;
    }

    @Override
    public boolean isPossibleAddNewStaffRate(OrgUnit orgUnit, PostBoundedWithQGandQL postBounded, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        StaffList staffList = getActiveStaffList(orgUnit);

        List<StaffListItem> staffListItemList = getStaffListItemsList(staffList);

        double diff = 0.0d;

        for (StaffListItem item : staffListItemList)
        {
            if (item.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().equals(postBounded))
                if (item.getFinancingSource().equals(financingSource))
                    if ((financingSourceItem == null && item.getFinancingSourceItem() == null) || (financingSourceItem != null && financingSourceItem.equals(item.getFinancingSourceItem())))
                        diff = item.getStaffRate() - item.getOccStaffRate();
        }

        List<StaffListAllocationItem> allocationItemList = getStaffListAllocationItemsList(staffList, postBounded);

        double summ = 0.0d;

        for (StaffListAllocationItem item : allocationItemList)
        {
            if (item.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().equals(postBounded))
                if (financingSource.equals(item.getFinancingSource()))
                    if ((financingSourceItem == null && item.getFinancingSourceItem() == null) || (financingSourceItem != null && financingSourceItem.equals(item.getFinancingSourceItem())))
                        if (item.getEmployeePost() != null && !item.getEmployeePost().getPostStatus().isActive())
                            summ += item.getStaffRate();
        }

        return diff - summ > 0;
    }

    @Override
    public List<StaffListAllocationItem> getOccupiedStaffListAllocationItem(OrgUnit orgUnit, PostBoundedWithQGandQL postRelation, FinancingSource financingSource, FinancingSourceItem financingSourceItem)
    {
        MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), getActiveStaffList(orgUnit)));
        builder.add(MQExpression.isNotNull("b", StaffListAllocationItem.employeePost().s()));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), postRelation));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.financingSource().s(), financingSource));
        builder.add(MQExpression.eq("b", StaffListAllocationItem.financingSourceItem().s(), financingSourceItem));

        return builder.getResultList(getSession());
    }

    @Override
    public List<OrgUnit> getOrgUnitWithActiveStaffList()
    {
        MQBuilder builder = new MQBuilder(StaffList.ENTITY_CLASS, "b", new String[]{StaffList.orgUnit().s()});
        builder.add(MQExpression.eq("b", StaffList.staffListState().code().s(), UniempDefines.STAFF_LIST_STATUS_ACTIVE));

        return builder.getResultList(getSession());
    }

    @Override
    public List<StaffListPostPayment> getStaffListPostPaymentsList(OrgUnit orgUnit, PostBoundedWithQGandQL post, List<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>> finSrcPairList)
    {
        if (null == orgUnit || null == post) return null;

        StaffList activeStaffList = getActiveStaffList(orgUnit);
        if (null == activeStaffList) return null;

        List<StaffListItem> staffListItems = getStaffListItem(activeStaffList, post);
        if (null == staffListItems) return null;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListPostPayment.class, "slpp").column("slpp");
        builder.where(DQLExpressions.in(DQLExpressions.property(StaffListPostPayment.staffListItem().fromAlias("slpp")), staffListItems));
        builder.order(DQLExpressions.property(StaffListPostPayment.payment().title().fromAlias("slpp")));

        List<StaffListPostPayment> paymentList = builder.createStatement(getSession()).list();

        if (finSrcPairList == null)
            return paymentList;

        List<StaffListPostPayment> resultList = new ArrayList<>();
        for (CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> entry : finSrcPairList)
            for (StaffListPostPayment payment : paymentList)
                if (payment.getStaffListItem().getFinancingSource().equals(entry.getX()))
                    if ((payment.getStaffListItem().getFinancingSourceItem() == null && entry.getY() == null) || (payment.getStaffListItem().getFinancingSourceItem() != null && payment.getStaffListItem().getFinancingSourceItem().equals(entry.getY())))
                        resultList.add(payment);

        return resultList;
    }

    @Override
    public Double getAllocItemBaseSalary(StaffListAllocationItem item)
    {
        return item.getStaffListItem().getSalary();
    }

    @Override
    public PostBoundedWithQGandQL getAllocItemPost(StaffListAllocationItem item)
    {
        return item.getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL();
    }

    @Override
    public void copyStaffList(StaffList staffList, StaffList staffListToCopy, boolean addNewRequiredPayments, boolean removeNonActivePayments)
    {
        /*MQBuilder allRequiredActivePaymentsBuilder = new MQBuilder(Payment.ENTITY_CLASS, "p");
        allRequiredActivePaymentsBuilder.add(MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE));
        allRequiredActivePaymentsBuilder.add(MQExpression.eq("p", Payment.P_REQUIRED, Boolean.TRUE));

        MQBuilder rightStaffListItemBuilder = new MQBuilder(StaffListPostPayment.ENTITY_CLASS, "pp", new String[] {StaffListPostPayment.L_STAFF_LIST_ITEM + "." + StaffListItem.P_ID});
        rightStaffListItemBuilder.add(MQExpression.eq("pp", StaffListPostPayment.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, model.getStaffList()));
        rightStaffListItemBuilder.add(MQExpression.in("pp", StaffListPostPayment.L_PAYMENT, allRequiredActivePaymentsBuilder));



        MQBuilder slRequiredActivePayments = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "pb", new String[] {StaffListPaymentBase.L_PAYMENT + ".id"});
        slRequiredActivePayments.add(MQExpression.eq("pb", StaffListPaymentBase.L_PAYMENT + "." + Payment.P_ACTIVE, Boolean.TRUE));
        slRequiredActivePayments.add(MQExpression.eq("pb", StaffListPaymentBase.L_PAYMENT + "." + Payment.P_REQUIRED, Boolean.TRUE));
        slRequiredActivePayments.setNeedDistinct(true);
        */
        MQBuilder allRequiredActivePaymentsBuilder = new MQBuilder(Payment.ENTITY_CLASS, "p");
        allRequiredActivePaymentsBuilder.add(MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE));
        allRequiredActivePaymentsBuilder.add(MQExpression.eq("p", Payment.P_REQUIRED, Boolean.TRUE));
        //allRequiredActivePaymentsBuilder.add(MQExpression.notIn("p", Payment.P_ID, collection));
        List<Payment> allRequiredActivePaymentsList = allRequiredActivePaymentsBuilder.getResultList(getSession());

        // Получаем полный список выплат в версии штатного расписания
        MQBuilder paymentsBuilder = new MQBuilder(StaffListPaymentBase.ENTITY_CLASS, "p");
        paymentsBuilder.add(MQExpression.eq("p", StaffListPaymentBase.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffListToCopy));
        if (removeNonActivePayments)
            paymentsBuilder.add(MQExpression.eq("p", StaffListPaymentBase.L_PAYMENT + "." + Payment.P_ACTIVE, Boolean.TRUE));

        // Разбиваем эти выплаты по мапам: одна для выплат должности штатного расписания,
        // вторая - и для должности штатной расстановки и для должности штатного расписания.
        Map<Long, List<StaffListPostPayment>> postPaymentsMap = new HashMap<>();
        Map<Long, List<StaffListFakePayment>> fakePaymentsMap = new HashMap<>();
        for (StaffListPaymentBase payment : paymentsBuilder.<StaffListPaymentBase>getResultList(getSession()))
        {
            if (payment instanceof StaffListFakePayment)
            {
                Long id = payment.getStaffListItem().getId();
                List<StaffListFakePayment> list = fakePaymentsMap.get(id);
                if (null == list) list = new ArrayList<>();
                list.add((StaffListFakePayment) payment);
                fakePaymentsMap.put(id, list);
            }
            else
            {
                Long id = payment.getStaffListItem().getId();
                List<StaffListPostPayment> list = postPaymentsMap.get(id);
                if (null == list) list = new ArrayList<>();
                list.add((StaffListPostPayment) payment);
                postPaymentsMap.put(id, list);
            }
        }

        // Получаем полный список должностей версии штатного расписания
        MQBuilder staffListItemsBuilder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli");
        staffListItemsBuilder.add(MQExpression.eq("sli", StaffListItem.L_STAFF_LIST, staffListToCopy));
        List<StaffListItem> staffListItemsList = staffListItemsBuilder.getResultList(getSession());

        Map<Long, StaffListFakePayment> id2FakePaymentMap = new HashMap<>();

        // ключ - Id исходной должности ШР, значение - Id создаваемой должности ШР
        Map<Long, StaffListItem> staffListItemsMap = new HashMap<>();

        // Копируем все должности версии штатного расписания с выплатами
        for (StaffListItem item : staffListItemsList)
        {
            // Копируем все должности версии штатного расписания
            StaffListItem staffListItem = new StaffListItem();
            staffListItem.update(item);
            staffListItem.setStaffList(staffList);
            if (null != staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getSalary())
                staffListItem.setSalary(staffListItem.getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().getSalary());
            getSession().save(staffListItem);

            // Заполняем мап соответствия старых должностей новым
            staffListItemsMap.put(item.getId(), staffListItem);

            // Список Id всех выплат скопированных у должности старого штатного расписания
            List<Long> addedPaymentIds = new ArrayList<>();

            // Копируем все выплаты должностей штатного расписания
            if (postPaymentsMap.containsKey(item.getId()))
            {
                for (StaffListPostPayment payment : postPaymentsMap.get(item.getId()))
                {
                    StaffListPostPayment postPayment = new StaffListPostPayment();
                    postPayment.update(payment);
                    postPayment.setStaffListItem(staffListItem);
                    getSession().save(postPayment);
                    addedPaymentIds.add(payment.getPayment().getId());
                }
            }

            if (fakePaymentsMap.containsKey(item.getId()))
            {
                for (StaffListFakePayment payment : fakePaymentsMap.get(item.getId()))
                {
                    StaffListFakePayment fakePayment = new StaffListFakePayment();
                    fakePayment.update(payment);
                    fakePayment.setStaffListItem(staffListItem);
                    getSession().save(fakePayment);
                    id2FakePaymentMap.put(payment.getId(), fakePayment);
                }
            }

            if (addNewRequiredPayments)
            {
                for (Payment payment : allRequiredActivePaymentsList)
                {
                    if (!addedPaymentIds.contains(payment.getId()))
                    {
                        StaffListPostPayment postPayment = new StaffListPostPayment();
                        postPayment.setPayment(payment);
                        postPayment.setStaffListItem(staffListItem);
                        postPayment.setAmount(null != payment.getValue() ? payment.getValue() : 0d);
                        postPayment.setFinancingSource(payment.getFinancingSource());
                        getSession().save(postPayment);
                    }
                }
            }

            StaffListPaymentsUtil.recalculateAllPaymentsValueForStaffListItem(staffListItem, getSession());
        }

        // Получаем полный список должностей штатной расстановки версии штатного расписания
        MQBuilder staffListAllocationItemsBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "sla");
        staffListAllocationItemsBuilder.add(MQExpression.eq("sla", StaffListAllocationItem.L_STAFF_LIST_ITEM + "." + StaffListItem.L_STAFF_LIST, staffListToCopy));
        List<StaffListAllocationItem> staffListAllocationItemsList = staffListAllocationItemsBuilder.getResultList(getSession());

        // Копируем все должности штатной расстановки с выплатами
        // выносим действие в метод для возможности переопределения в проектом слое
        copyToCreateStaffListAllocationItemList(staffListAllocationItemsList, staffListItemsMap, fakePaymentsMap, id2FakePaymentMap);
    }

    protected void copyToCreateStaffListAllocationItemList(List<StaffListAllocationItem> staffListAllocationItemsList, Map<Long, StaffListItem> staffListItemsMap, Map<Long, List<StaffListFakePayment>> fakePaymentsMap, Map<Long, StaffListFakePayment> id2FakePaymentMap)
    {
        for (StaffListAllocationItem item : staffListAllocationItemsList)
        {
            // Копируем все должности штатной расстановки
            StaffListAllocationItem staffListAllocationItem = new StaffListAllocationItem();
            staffListAllocationItem.update(item);
            staffListAllocationItem.setStaffListItem(staffListItemsMap.get(item.getStaffListItem().getId()));
            if (null != staffListAllocationItem.getRaisingCoefficient())
                staffListAllocationItem.setMonthBaseSalaryFund(Math.round(staffListAllocationItem.getRaisingCoefficient().getRecommendedSalary() * staffListAllocationItem.getStaffRate() * 100) / 100d);
            else if (null != staffListAllocationItem.getStaffListItem())
                staffListAllocationItem.setMonthBaseSalaryFund(Math.round(staffListAllocationItem.getStaffListItem().getSalary() * staffListAllocationItem.getStaffRate() * 100) / 100d);
            else
                staffListAllocationItem.setMonthBaseSalaryFund(0d);

            getSession().save(staffListAllocationItem);


            //Простовляем ссылки на allocItem у fakePayment
            if (fakePaymentsMap.containsKey(item.getStaffListItem().getId()))
            {
                for (StaffListFakePayment payment : fakePaymentsMap.get(item.getStaffListItem().getId()))
                {
                    if (payment.getStaffListAllocationItem() == item)
                    {
                        StaffListFakePayment fakePayment = id2FakePaymentMap.get(payment.getId());
                        fakePayment.setStaffListAllocationItem(staffListAllocationItem);
                        getSession().saveOrUpdate(fakePayment);
                    }
                }
            }

            StaffListPaymentsUtil.recalculateAllPaymentsValue(staffListAllocationItem, getSession());
        }
    }

    @Override
    public void copyStaffListAllocationItem(Long staffListAllocationItemId)
    {
        StaffListAllocationItem staffListAllocationItem = get(StaffListAllocationItem.class, staffListAllocationItemId);

        //TODO: validation
        MQBuilder builder = new MQBuilder(StaffListFakePayment.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", StaffListFakePayment.L_STAFF_LIST_ALLOCATION_ITEM, staffListAllocationItem));
        List<StaffListFakePayment> paymentsList = builder.getResultList(getSession());

        // выносим создание в метод для возможности переопределения в проектом слое
        StaffListAllocationItem allocationItem = createStaffListAllocationItem(staffListAllocationItem);

        for (StaffListFakePayment paymentItem : paymentsList)
        {
            StaffListFakePayment payment = new StaffListFakePayment();
            payment.update(paymentItem);
            payment.setTargetAllocItem(allocationItem);
            payment.setStaffListAllocationItem(allocationItem);
            getSession().save(payment);
        }

        StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());
    }

    protected StaffListAllocationItem createStaffListAllocationItem(StaffListAllocationItem item)
    {
        StaffListAllocationItem allocationItem = new StaffListAllocationItem();
        allocationItem.update(item);
        allocationItem.setEmployeePost(null);
        getSession().save(allocationItem);

        return allocationItem;
    }
}