package ru.tandemservice.uniemp.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uniemp.entity.catalog.gen.PaymentTypeGen;

public class PaymentType extends PaymentTypeGen implements IHierarchyItem
{
    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }
}