/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryExtAddEdit;

import ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit.EmploymentHistoryAbstractIDAO;

/**
 * @author dseleznev
 * Created on: 23.12.2008
 */
public interface IDAO extends EmploymentHistoryAbstractIDAO<Model>
{
}