/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.staffList.Add;

import org.tandemframework.core.settings.IDataSettings;

/**
 * @author dseleznev
 * Created on: 10.08.2009
 */
public interface IFiltersPresetManager
{
    void initFilters(IDataSettings dataSettings);

    void setFilters(IDataSettings dataSettings);
}