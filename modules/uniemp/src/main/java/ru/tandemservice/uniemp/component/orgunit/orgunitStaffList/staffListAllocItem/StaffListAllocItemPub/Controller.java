/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListAllocItemPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPaymentBase;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

/**
 * @author dseleznev
 * Created on: 08.04.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareListDataSource(component);
    }
    
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        IMergeRowIdResolver mergeRowResolver = entity -> {
            StaffListPostPayment payment = (StaffListPostPayment) entity;

            return payment.getPayment().getTitle();
        };
        
        DynamicListDataSource<StaffListPaymentBase> dataSource = new DynamicListDataSource<>(component, this, 10);
        dataSource.addColumn(new SimpleColumn("Выплата", StaffListPaymentBase.PAYMENT_KEY).setMergeRowIdResolver(mergeRowResolver).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип выплаты", StaffListPaymentBase.PAYMENT_TYPE_KEY).setMergeRowIdResolver(mergeRowResolver).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Значение выплаты", StaffListPaymentBase.P_AMOUNT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Формат ввода выплаты", StaffListPaymentBase.PAYMENT_UNIT_TYPE_KEY).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сумма выплаты", StaffListPaymentBase.P_TOTAL_VALUE, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", StaffListPaymentBase.L_FINANCING_SOURCE + "." + FinancingSource.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", StaffListPaymentBase.L_FINANCING_SOURCE_ITEM + "." + FinancingSourceItem.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditPayment").setDisabledProperty(StaffListPaymentBase.P_EDITING_DISABLED).setPermissionKey("editStaffListAllocPayment"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeletePayment", "Удалить выплату «{0}» из должности «{1}» штатного расписания?", new Object[] { new String[] { StaffListPaymentBase.L_PAYMENT + "." + Payment.P_TITLE }, new String[] { StaffListPaymentBase.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.P_TITLE_WITH_SALARY } }).setDisabledProperty(StaffListPaymentBase.P_DELETE_DISABLED).setPermissionKey("deleteStaffListAllocPayment"));
        model.setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }
    
    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ALLOC_ITEM_ADDEDIT));
    }
    
    public void onClickDelete(IBusinessComponent component)
    {
        getDao().delete(getModel(component).getStaffListAllocationItem());
        deactivate(component);
    }
    
    public void onClickAddPayment(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_PAYMENT_ITEM_ADDEDIT));
    }

    public void onClickAddFakePayment(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_FAKE_PAYMENT_ITEM_ADDEDIT));
    }

    public void onClickEditPayment(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_PAYMENT_ITEM_ADDEDIT, new ParametersMap().add("staffListAllocPaymentId", component.getListenerParameter())));
    }
    
    public void onClickDeletePayment(IBusinessComponent component)
    {
//        StaffListPaymentBase payment = getDao().get((Long)component.getListenerParameter());
//        if(payment instanceof StaffListFakePayment)
//        {
//            ((StaffListFakePayment)payment).setStaffListAllocationItem(null);
//            getDao().update(payment);
//        }
//        else
            getDao().deleteStaffListPaymentItem((Long)component.getListenerParameter(), getModel(component).getStaffListAllocationItem());
    }
}