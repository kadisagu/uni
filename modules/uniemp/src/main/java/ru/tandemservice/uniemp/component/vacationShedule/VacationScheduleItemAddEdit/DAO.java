/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationScheduleItemAddEdit;


import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

/**
 * @author dseleznev
 * Created on: 21.01.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EmployeePost ep = get(EmployeePost.class, model.getEmployeePostId());
        if (model.isEditForm())
            model.setVacationScheduleItem(get(VacationScheduleItem.class, model.getVacationScheduleItemId()));
        else
        {
            model.setVacationScheduleItem(new VacationScheduleItem());
            model.getVacationScheduleItem().setEmployeePost(ep);
        }

        MQBuilder ssBuilder = new MQBuilder(StaffListState.ENTITY_CLASS, "ss");
        ssBuilder.add(MQExpression.eq("ss", StaffListState.code().s(), UniempDefines.STAFF_LIST_STATUS_FORMING));
        StaffListState formingStatus = (StaffListState)ssBuilder.uniqueResult(getSession());
        MQBuilder vsBuilder = new MQBuilder(VacationSchedule.ENTITY_CLASS, "vs");
        vsBuilder.add(MQExpression.eq("vs", VacationSchedule.orgUnit().s(), ep.getOrgUnit()));
        vsBuilder.add(MQExpression.eq("vs", VacationSchedule.state().s(), formingStatus));
        vsBuilder.addOrder("vs", VacationSchedule.P_YEAR);
        model.setVacationScheduleList(vsBuilder.<VacationSchedule>getResultList(getSession()));

        model.setDisabledField(!CoreServices.securityService().check(model.getVacationScheduleItem().getEmployeePost(), ContextLocal.getUserContext().getPrincipalContext(), "editEmployeeVacationScheduleItem_employeePost"));
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errs = UserContext.getInstance().getErrorCollector();
        UniempDaoFacade.getUniempDAO().validateVacationScheduleItemHasIntersections(model.getVacationScheduleItem(), errs, "Указанный планируемый отпуск", "planDate", "factDate", "daysAmount");

        if (!errs.hasErrors())
            getSession().saveOrUpdate(model.getVacationScheduleItem());
    }
}