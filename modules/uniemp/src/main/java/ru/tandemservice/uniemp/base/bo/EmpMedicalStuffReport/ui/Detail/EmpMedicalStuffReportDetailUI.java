/**
 *$Id:$
 */
package ru.tandemservice.uniemp.base.bo.EmpMedicalStuffReport.ui.Detail;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.source.AbstractListDataSource;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 30.10.12
 */
@Input({
        @Bind(key = "medSpecIdList", binding = "medSpecIdList"),
        @Bind(key = "ignoreEmployeeIdList", binding = "ignoreEmployeeIdList"),
        @Bind(key = "reportDate", binding = "reportDate", required = true),
        @Bind(key = "postTypeIdList", binding = "postTypeIdList"),
        @Bind(key = "medicalSpecialityIdList", binding = "medicalSpecialityIdList"),
        @Bind(key = "employeeTypeIdList", binding = "employeeTypeIdList"),
        @Bind(key = "eduLevelId", binding = "eduLevelId"),
        @Bind(key = "columnTitle", binding = "columnTitle"),
        @Bind(key = "rowTitle", binding = "rowTitle")
})
public class EmpMedicalStuffReportDetailUI extends UIPresenter
{
    public static final String PROP_IGNORE_EMPLOYEE__ID_LIST = "ignoreEmployeeIdList";
    public static final String PROP_EMPLOYEE_MED_SPEC_ID_LIST = "employeeMedicalSpecialityIdList";
    public static final String PROP_MED_SPEC_ID_LIST = "medicalSpecialityIdList";
    public static final String PROP_REPORT_DATE = "reportDate";
    public static final String PROP_POST_TYPE_ID_LIST = "postTypeIdList";
    public static final String PROP_EMPLOYEE_TYPE_ID_LIST = "employeeTypeIdList";
    public static final String PROP_EDU_LEVEL_ID = "eduLevelId";

    private Collection<Long> _medSpecIdList;
    private List<Long> _ignoreEmployeeIdList;
    private List<Long> _medicalSpecialityIdList;
    private List<Long> _employeeTypeIdList;
    private Long _eduLevelId;
    private Date _reportDate;
    private List<Long> _postTypeIdList;
    private String _columnTitle;
    private String _rowTitle;

    @Override
    public void onComponentActivate()
    {
        AbstractListDataSource<IEntity> ds = getConfig().<BaseSearchListDataSource>getDataSource(EmpMedicalStuffReportDetail.EMPLOYEE_POST_SEARCH_DS).getLegacyDataSource();
        ds.setOrder(ds.getColumn("title"), OrderDirection.asc);
    }

    public String getSticker()
    {
        if (getIgnoreEmployeePostSearchVisible())
            return "Сотрудники, имеющие несколько врач. специальностей с равными категориями";
        return getConfig().getProperty("ui.sticker", _columnTitle, _rowTitle);
    }

    public boolean getEmployeePostSearchVisible()
    {
        return _medSpecIdList != null && _ignoreEmployeeIdList == null;
    }

    public boolean getIgnoreEmployeePostSearchVisible()
    {
        return _ignoreEmployeeIdList != null && _medSpecIdList == null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PROP_EMPLOYEE_MED_SPEC_ID_LIST, _medSpecIdList);
        dataSource.put(PROP_REPORT_DATE, _reportDate);
        dataSource.put(PROP_POST_TYPE_ID_LIST, _postTypeIdList);
        dataSource.put(PROP_EMPLOYEE_TYPE_ID_LIST, _employeeTypeIdList);
        dataSource.put(PROP_EDU_LEVEL_ID, _eduLevelId);

        dataSource.put(PROP_IGNORE_EMPLOYEE__ID_LIST, _ignoreEmployeeIdList);
        dataSource.put(PROP_MED_SPEC_ID_LIST, _medicalSpecialityIdList);
    }

    // Getters & Setters

    public Collection<Long> getMedSpecIdList()
    {
        return _medSpecIdList;
    }

    public void setMedSpecIdList(Collection<Long> medSpecIdList)
    {
        _medSpecIdList = medSpecIdList;
    }

    public List<Long> getIgnoreEmployeeIdList()
    {
        return _ignoreEmployeeIdList;
    }

    public void setIgnoreEmployeeIdList(List<Long> ignoreEmployeeIdList)
    {
        _ignoreEmployeeIdList = ignoreEmployeeIdList;
    }

    public List<Long> getMedicalSpecialityIdList()
    {
        return _medicalSpecialityIdList;
    }

    public void setMedicalSpecialityIdList(List<Long> medicalSpecialityIdList)
    {
        _medicalSpecialityIdList = medicalSpecialityIdList;
    }

    public List<Long> getEmployeeTypeIdList()
    {
        return _employeeTypeIdList;
    }

    public void setEmployeeTypeIdList(List<Long> employeeTypeIdList)
    {
        _employeeTypeIdList = employeeTypeIdList;
    }

    public Long getEduLevelId()
    {
        return _eduLevelId;
    }

    public void setEduLevelId(Long eduLevelId)
    {
        _eduLevelId = eduLevelId;
    }

    public Date getReportDate()
    {
        return _reportDate;
    }

    public void setReportDate(Date reportDate)
    {
        _reportDate = reportDate;
    }

    public List<Long> getPostTypeIdList()
    {
        return _postTypeIdList;
    }

    public void setPostTypeIdList(List<Long> postTypeIdList)
    {
        _postTypeIdList = postTypeIdList;
    }

    public String getColumnTitle()
    {
        return _columnTitle;
    }

    public void setColumnTitle(String columnTitle)
    {
        _columnTitle = columnTitle;
    }

    public String getRowTitle()
    {
        return _rowTitle;
    }

    public void setRowTitle(String rowTitle)
    {
        _rowTitle = rowTitle;
    }
}
