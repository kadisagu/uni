/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.retrainingData;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem;

import java.util.Date;

/**
 * @author Vasily Zhukov
 * @since 10.04.2012
 */
public class RetrainingDataParam implements IReportDQLModifier
{
    private IReportParam<Date> _startDateFrom = new ReportParam<>();
    private IReportParam<Date> _startDateTo = new ReportParam<>();
    private IReportParam<Date> _finishDateFrom = new ReportParam<>();
    private IReportParam<Date> _finishDateTo = new ReportParam<>();
    private IReportParam<Date> _noRetrainingFrom = new ReportParam<>();
    private IReportParam<Date> _noRetrainingTo = new ReportParam<>();

    private String employeeAlias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        return dql.innerJoinEntity(Employee.class, Employee.person());
    }

    private String retrainingAlias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = employeeAlias(dql);

        // соединяем переподготовку
        return dql.innerJoinEntity(employeeAlias, EmployeeRetrainingItem.class, EmployeeRetrainingItem.employee());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_startDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "retrainingData.startDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_startDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.startDate().fromAlias(retrainingAlias(dql))), DQLExpressions.valueDate(_startDateFrom.getData())));
        }

        if (_startDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "retrainingData.startDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_startDateTo.getData()));

            dql.builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeRetrainingItem.startDate().fromAlias(retrainingAlias(dql))), DQLExpressions.valueDate(_startDateTo.getData())));
        }

        if (_finishDateFrom.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "retrainingData.finishDateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_finishDateFrom.getData()));

            dql.builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.finishDate().fromAlias(retrainingAlias(dql))), DQLExpressions.valueDate(_finishDateFrom.getData())));
        }

        if (_finishDateTo.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "retrainingData.finishDateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_finishDateTo.getData()));

            dql.builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeRetrainingItem.finishDate().fromAlias(retrainingAlias(dql))), DQLExpressions.valueDate(_finishDateTo.getData())));
        }

        if (_noRetrainingFrom.isActive() || _noRetrainingTo.isActive())
        {
            String alias = dql.nextAlias();

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeRetrainingItem.class, alias)
                    .where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.employee().fromAlias(alias)), DQLExpressions.property(employeeAlias(dql))));

            if (_noRetrainingFrom.isActive())
            {
                printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "retrainingData.noRetrainingFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(_noRetrainingFrom.getData()));

                builder.where(DQLExpressions.ge(DQLExpressions.property(EmployeeRetrainingItem.finishDate().fromAlias(alias)), DQLExpressions.valueDate(_noRetrainingFrom.getData())));
            }

            if (_noRetrainingTo.isActive())
            {
                printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "retrainingData.noRetrainingTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(_noRetrainingTo.getData()));

                builder.where(DQLExpressions.le(DQLExpressions.property(EmployeeRetrainingItem.startDate().fromAlias(alias)), DQLExpressions.valueDate(_noRetrainingTo.getData())));
            }

            dql.builder.where(DQLExpressions.notExists(builder.buildQuery()));
        }
    }

    // Getters

    public IReportParam<Date> getStartDateFrom()
    {
        return _startDateFrom;
    }

    public IReportParam<Date> getStartDateTo()
    {
        return _startDateTo;
    }

    public IReportParam<Date> getFinishDateFrom()
    {
        return _finishDateFrom;
    }

    public IReportParam<Date> getFinishDateTo()
    {
        return _finishDateTo;
    }

    public IReportParam<Date> getNoRetrainingFrom()
    {
        return _noRetrainingFrom;
    }

    public IReportParam<Date> getNoRetrainingTo()
    {
        return _noRetrainingTo;
    }
}
