package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.catalog.ResolutionCertificationCommission;
import ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Факт аттестации сотрудника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeCertificationItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem";
    public static final String ENTITY_NAME = "employeeCertificationItem";
    public static final int VERSION_HASH = -804741842;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_CERTIFICATION_DATE = "certificationDate";
    public static final String L_RESOLUTION = "resolution";
    public static final String P_COMMISSION_COMMENTS = "commissionComments";
    public static final String P_DOCUMENT_NUMBER = "documentNumber";
    public static final String P_DOCUMENT_DATE = "documentDate";
    public static final String P_BASIC_ORDER_NUMBER = "basicOrderNumber";
    public static final String P_BASIC_ORDER_DATE = "basicOrderDate";

    private EmployeePost _employeePost;     // Сотрудник
    private Date _certificationDate;     // Дата аттестации
    private ResolutionCertificationCommission _resolution;     // Решения аттестационной комиссии
    private String _commissionComments;     // Рекомендация комиссии
    private String _documentNumber;     // Номер протокола заседания аттестационной комиссии
    private Date _documentDate;     // Дата протокола заседания аттестационной комиссии
    private String _basicOrderNumber;     // Номер 
    private Date _basicOrderDate;     // Дата 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Дата аттестации. Свойство не может быть null.
     */
    @NotNull
    public Date getCertificationDate()
    {
        return _certificationDate;
    }

    /**
     * @param certificationDate Дата аттестации. Свойство не может быть null.
     */
    public void setCertificationDate(Date certificationDate)
    {
        dirty(_certificationDate, certificationDate);
        _certificationDate = certificationDate;
    }

    /**
     * @return Решения аттестационной комиссии. Свойство не может быть null.
     */
    @NotNull
    public ResolutionCertificationCommission getResolution()
    {
        return _resolution;
    }

    /**
     * @param resolution Решения аттестационной комиссии. Свойство не может быть null.
     */
    public void setResolution(ResolutionCertificationCommission resolution)
    {
        dirty(_resolution, resolution);
        _resolution = resolution;
    }

    /**
     * @return Рекомендация комиссии.
     */
    @Length(max=255)
    public String getCommissionComments()
    {
        return _commissionComments;
    }

    /**
     * @param commissionComments Рекомендация комиссии.
     */
    public void setCommissionComments(String commissionComments)
    {
        dirty(_commissionComments, commissionComments);
        _commissionComments = commissionComments;
    }

    /**
     * @return Номер протокола заседания аттестационной комиссии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDocumentNumber()
    {
        return _documentNumber;
    }

    /**
     * @param documentNumber Номер протокола заседания аттестационной комиссии. Свойство не может быть null.
     */
    public void setDocumentNumber(String documentNumber)
    {
        dirty(_documentNumber, documentNumber);
        _documentNumber = documentNumber;
    }

    /**
     * @return Дата протокола заседания аттестационной комиссии. Свойство не может быть null.
     */
    @NotNull
    public Date getDocumentDate()
    {
        return _documentDate;
    }

    /**
     * @param documentDate Дата протокола заседания аттестационной комиссии. Свойство не может быть null.
     */
    public void setDocumentDate(Date documentDate)
    {
        dirty(_documentDate, documentDate);
        _documentDate = documentDate;
    }

    /**
     * @return Номер .
     */
    @Length(max=255)
    public String getBasicOrderNumber()
    {
        return _basicOrderNumber;
    }

    /**
     * @param basicOrderNumber Номер .
     */
    public void setBasicOrderNumber(String basicOrderNumber)
    {
        dirty(_basicOrderNumber, basicOrderNumber);
        _basicOrderNumber = basicOrderNumber;
    }

    /**
     * @return Дата .
     */
    public Date getBasicOrderDate()
    {
        return _basicOrderDate;
    }

    /**
     * @param basicOrderDate Дата .
     */
    public void setBasicOrderDate(Date basicOrderDate)
    {
        dirty(_basicOrderDate, basicOrderDate);
        _basicOrderDate = basicOrderDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeCertificationItemGen)
        {
            setEmployeePost(((EmployeeCertificationItem)another).getEmployeePost());
            setCertificationDate(((EmployeeCertificationItem)another).getCertificationDate());
            setResolution(((EmployeeCertificationItem)another).getResolution());
            setCommissionComments(((EmployeeCertificationItem)another).getCommissionComments());
            setDocumentNumber(((EmployeeCertificationItem)another).getDocumentNumber());
            setDocumentDate(((EmployeeCertificationItem)another).getDocumentDate());
            setBasicOrderNumber(((EmployeeCertificationItem)another).getBasicOrderNumber());
            setBasicOrderDate(((EmployeeCertificationItem)another).getBasicOrderDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeCertificationItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeCertificationItem.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeCertificationItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "certificationDate":
                    return obj.getCertificationDate();
                case "resolution":
                    return obj.getResolution();
                case "commissionComments":
                    return obj.getCommissionComments();
                case "documentNumber":
                    return obj.getDocumentNumber();
                case "documentDate":
                    return obj.getDocumentDate();
                case "basicOrderNumber":
                    return obj.getBasicOrderNumber();
                case "basicOrderDate":
                    return obj.getBasicOrderDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "certificationDate":
                    obj.setCertificationDate((Date) value);
                    return;
                case "resolution":
                    obj.setResolution((ResolutionCertificationCommission) value);
                    return;
                case "commissionComments":
                    obj.setCommissionComments((String) value);
                    return;
                case "documentNumber":
                    obj.setDocumentNumber((String) value);
                    return;
                case "documentDate":
                    obj.setDocumentDate((Date) value);
                    return;
                case "basicOrderNumber":
                    obj.setBasicOrderNumber((String) value);
                    return;
                case "basicOrderDate":
                    obj.setBasicOrderDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "certificationDate":
                        return true;
                case "resolution":
                        return true;
                case "commissionComments":
                        return true;
                case "documentNumber":
                        return true;
                case "documentDate":
                        return true;
                case "basicOrderNumber":
                        return true;
                case "basicOrderDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "certificationDate":
                    return true;
                case "resolution":
                    return true;
                case "commissionComments":
                    return true;
                case "documentNumber":
                    return true;
                case "documentDate":
                    return true;
                case "basicOrderNumber":
                    return true;
                case "basicOrderDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "certificationDate":
                    return Date.class;
                case "resolution":
                    return ResolutionCertificationCommission.class;
                case "commissionComments":
                    return String.class;
                case "documentNumber":
                    return String.class;
                case "documentDate":
                    return Date.class;
                case "basicOrderNumber":
                    return String.class;
                case "basicOrderDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeCertificationItem> _dslPath = new Path<EmployeeCertificationItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeCertificationItem");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Дата аттестации. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getCertificationDate()
     */
    public static PropertyPath<Date> certificationDate()
    {
        return _dslPath.certificationDate();
    }

    /**
     * @return Решения аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getResolution()
     */
    public static ResolutionCertificationCommission.Path<ResolutionCertificationCommission> resolution()
    {
        return _dslPath.resolution();
    }

    /**
     * @return Рекомендация комиссии.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getCommissionComments()
     */
    public static PropertyPath<String> commissionComments()
    {
        return _dslPath.commissionComments();
    }

    /**
     * @return Номер протокола заседания аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getDocumentNumber()
     */
    public static PropertyPath<String> documentNumber()
    {
        return _dslPath.documentNumber();
    }

    /**
     * @return Дата протокола заседания аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getDocumentDate()
     */
    public static PropertyPath<Date> documentDate()
    {
        return _dslPath.documentDate();
    }

    /**
     * @return Номер .
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getBasicOrderNumber()
     */
    public static PropertyPath<String> basicOrderNumber()
    {
        return _dslPath.basicOrderNumber();
    }

    /**
     * @return Дата .
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getBasicOrderDate()
     */
    public static PropertyPath<Date> basicOrderDate()
    {
        return _dslPath.basicOrderDate();
    }

    public static class Path<E extends EmployeeCertificationItem> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<Date> _certificationDate;
        private ResolutionCertificationCommission.Path<ResolutionCertificationCommission> _resolution;
        private PropertyPath<String> _commissionComments;
        private PropertyPath<String> _documentNumber;
        private PropertyPath<Date> _documentDate;
        private PropertyPath<String> _basicOrderNumber;
        private PropertyPath<Date> _basicOrderDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Дата аттестации. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getCertificationDate()
     */
        public PropertyPath<Date> certificationDate()
        {
            if(_certificationDate == null )
                _certificationDate = new PropertyPath<Date>(EmployeeCertificationItemGen.P_CERTIFICATION_DATE, this);
            return _certificationDate;
        }

    /**
     * @return Решения аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getResolution()
     */
        public ResolutionCertificationCommission.Path<ResolutionCertificationCommission> resolution()
        {
            if(_resolution == null )
                _resolution = new ResolutionCertificationCommission.Path<ResolutionCertificationCommission>(L_RESOLUTION, this);
            return _resolution;
        }

    /**
     * @return Рекомендация комиссии.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getCommissionComments()
     */
        public PropertyPath<String> commissionComments()
        {
            if(_commissionComments == null )
                _commissionComments = new PropertyPath<String>(EmployeeCertificationItemGen.P_COMMISSION_COMMENTS, this);
            return _commissionComments;
        }

    /**
     * @return Номер протокола заседания аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getDocumentNumber()
     */
        public PropertyPath<String> documentNumber()
        {
            if(_documentNumber == null )
                _documentNumber = new PropertyPath<String>(EmployeeCertificationItemGen.P_DOCUMENT_NUMBER, this);
            return _documentNumber;
        }

    /**
     * @return Дата протокола заседания аттестационной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getDocumentDate()
     */
        public PropertyPath<Date> documentDate()
        {
            if(_documentDate == null )
                _documentDate = new PropertyPath<Date>(EmployeeCertificationItemGen.P_DOCUMENT_DATE, this);
            return _documentDate;
        }

    /**
     * @return Номер .
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getBasicOrderNumber()
     */
        public PropertyPath<String> basicOrderNumber()
        {
            if(_basicOrderNumber == null )
                _basicOrderNumber = new PropertyPath<String>(EmployeeCertificationItemGen.P_BASIC_ORDER_NUMBER, this);
            return _basicOrderNumber;
        }

    /**
     * @return Дата .
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem#getBasicOrderDate()
     */
        public PropertyPath<Date> basicOrderDate()
        {
            if(_basicOrderDate == null )
                _basicOrderDate = new PropertyPath<Date>(EmployeeCertificationItemGen.P_BASIC_ORDER_DATE, this);
            return _basicOrderDate;
        }

        public Class getEntityClass()
        {
            return EmployeeCertificationItem.class;
        }

        public String getEntityName()
        {
            return "employeeCertificationItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
