/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.StaffListPaymentsList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;

/**
 * @author dseleznev
 * Created on: 16.03.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOtherPayment(new Payment(0L, "Другие надбавки"));

        preparePaymentsList(model);
        prepareSelectedColumnLists(model);
    }

    private void preparePaymentsList(Model model)
    {
        Map<Payment, StaffListRepPaymentSettings> settingsMap = new HashMap<>();
        MQBuilder relsBuilder = new MQBuilder(StaffListRepPaymentSettings.ENTITY_CLASS, "rel");
        relsBuilder.addOrder("rel", StaffListRepPaymentSettings.P_PRIORITY);

        List<Payment> paymentsList = new ArrayList<>();
        for (StaffListRepPaymentSettings setting : relsBuilder.<StaffListRepPaymentSettings> getResultList(getSession()))
        {
            paymentsList.add(null != setting.getPayment() ? setting.getPayment() : model.getOtherPayment());
            settingsMap.put(null != setting.getPayment() ? setting.getPayment() : model.getOtherPayment(), setting);
        }
        model.setSettingsMap(settingsMap);

        MQBuilder builder = new MQBuilder(Payment.ENTITY_CLASS, "p");
        //builder.add(MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE));
        builder.addOrder("p", Payment.P_TITLE);

        for (Payment payment : builder.<Payment> getResultList(getSession()))
        {
            if (!paymentsList.contains(payment)) paymentsList.add(payment);
        }

        if (!paymentsList.contains(model.getOtherPayment())) paymentsList.add(model.getOtherPayment());

        model.setPayments(paymentsList);
    }

    @SuppressWarnings("unchecked")
    private void prepareSelectedColumnLists(Model model)
    {
        List<IEntity> selectedTakeIntoAccountPayments = new ArrayList<>();
        List<IEntity> selectedIndividualColumnPayments = new ArrayList<>();

        for (StaffListRepPaymentSettings setting : (List<StaffListRepPaymentSettings>)getSession().createCriteria(StaffListRepPaymentSettings.class).list())
        {
            if (setting.isTakeIntoAccount()) selectedTakeIntoAccountPayments.add(setting.getPayment());
            if (setting.isIndividualColumn()) selectedIndividualColumnPayments.add(setting.getPayment());
        }

        model.setSelectedTakeIntoAccountPayments(selectedTakeIntoAccountPayments);
        model.setSelectedIndividualColumnPayments(selectedIndividualColumnPayments);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getDataSource(), model.getPayments());

        for (ViewWrapper<IEntity> wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            Payment payment = (Payment) wrapper.getEntity();
            if(!model.getOtherPayment().equals(payment)) wrapper.setViewProperty(Model.P_PAYMENT_SETTINGS_DISABLED, false);
            else wrapper.setViewProperty(Model.P_PAYMENT_SETTINGS_DISABLED, true);
        }
        ((CheckboxColumn)model.getDataSource().getColumn("takeIntoAccountPaymentColumn")).setSelectedObjects(model.getSelectedTakeIntoAccountPayments());
        ((CheckboxColumn)model.getDataSource().getColumn("individualColumnsColumn")).setSelectedObjects(model.getSelectedIndividualColumnPayments());
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        updateEnsureWrappersWasCreated(model);

        Collection<IEntity> selectedTakeIntoAccountPayments = (Collection<IEntity>)((CheckboxColumn)model.getDataSource().getColumn("takeIntoAccountPaymentColumn")).getSelectedObjects();
        Collection<IEntity> selectedIndividualColumnPayments = (Collection<IEntity>)((CheckboxColumn)model.getDataSource().getColumn("individualColumnsColumn")).getSelectedObjects();

        for (Object paymentWrapper : model.getDataSource().getEntityList())
        {
            Payment payment = ((ViewWrapper<Payment>)paymentWrapper).getEntity();
            StaffListRepPaymentSettings setting = model.getSettingsMap().get(payment);
            if (null != setting)
            {
                setting.setTakeIntoAccount(selectedTakeIntoAccountPayments.contains(setting.getPayment()));
                setting.setIndividualColumn(selectedIndividualColumnPayments.contains(setting.getPayment()));
                getSession().saveOrUpdate(setting);
            }
        }

        prepareSelectedColumnLists(model);
    }

    @Override
    public void updatePriority(Model model, Long paymentId, boolean up)
    {
        updateEnsureWrappersWasCreated(model);

        Payment payment = !paymentId.equals(0L) ? get(Payment.class, paymentId) : null;
        Payment virtual = payment != null ? payment : model.getOtherPayment();
        int idx = model.getPayments().indexOf(virtual);

        if ((idx == 0 && up) || (idx == model.getPayments().size() - 1 && !up)) return;

        Payment paymntToMove = model.getPayments().get(idx + (up ? -1 : 1));
        Collections.swap(model.getPayments(), idx, idx + (up ? -1 : 1));

        StaffListRepPaymentSettings setting = model.getSettingsMap().get(virtual);
        StaffListRepPaymentSettings settingToMove = model.getSettingsMap().get(paymntToMove);

        int priority1 = setting.getPriority() + (up ? -1 : 1);
        int priority2 = settingToMove.getPriority() + (up ? 1 : -1);

        setting.setPriority(-1);
        settingToMove.setPriority(-2);

        update(setting);
        update(settingToMove);

        getSession().flush();

        setting.setPriority(priority1);
        settingToMove.setPriority(priority2);

        update(setting);
        update(settingToMove);
    }

    private void updateEnsureWrappersWasCreated(Model model)
    {
        List<StaffListRepPaymentSettings> settingsList = new ArrayList<>();

        for (Payment payment : model.getPayments())
        {
            if (!model.getSettingsMap().containsKey(payment))
            {
                StaffListRepPaymentSettings setting = new StaffListRepPaymentSettings(!payment.getId().equals(0L) ? payment : null);
                model.getSettingsMap().put(payment, setting);
                settingsList.add(setting);
            }
            else
                settingsList.add(model.getSettingsMap().get(payment));
        }

        int i = 0;
        for (StaffListRepPaymentSettings setting : settingsList)
        {
            setting.setPriority(i++);
            getSession().saveOrUpdate(setting);
        }
    }
}