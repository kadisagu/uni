package ru.tandemservice.uniemp.entity.report;

import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.report.gen.StaffListRepPaymentSettingsGen;

/**
 * Настройка перечня и порядка выплат в штатном расписании
 */
public class StaffListRepPaymentSettings extends StaffListRepPaymentSettingsGen
{
    public StaffListRepPaymentSettings()
    {
        super();
    }

    public StaffListRepPaymentSettings(Payment payment)
    {
        super();
        setPayment(payment);
    }
}