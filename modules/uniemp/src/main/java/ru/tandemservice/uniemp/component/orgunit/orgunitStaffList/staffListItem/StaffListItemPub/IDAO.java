/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListItemPub;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
public interface IDAO extends IUniDao<Model>
{
    void deleteStaffListPostPayment(StaffListItem staffListItem, Long paymentId);
}