/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.orgUnitPost.OrgUnitPostAddEdit;

import java.util.List;

import org.tandemframework.common.catalog.entity.ICatalogItem;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;

/**
 * @author dseleznev
 * Created on: 16.09.2008
 */
public interface IDAO extends IUniDao<Model>
{
    List<ICatalogItem> getCatalogItemsList(String filter, Class<? extends ICatalogItem> clazz);

    void update(OrgUnitPostRelation rel, Model model);
}