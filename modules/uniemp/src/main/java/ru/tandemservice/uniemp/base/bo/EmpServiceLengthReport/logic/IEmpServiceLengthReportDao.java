/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.logic;

import jxl.write.WriteException;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import java.io.IOException;

/**
 * @author Alexander Shaburov
 * @since 25.01.13
 */
public interface IEmpServiceLengthReportDao extends INeedPersistenceSupport
{
    /**
     * Подготавливает модель формы добавления отчета.
     * Создает, заполняет значения по умолчанию.
     * @return подготовленную модель
     */
    <T extends EmpServiceLengthReportModel> T prepareModel();

    /**
     * Создает отчет.
     * @param model модель
     * @return Отчет "Сводка по направлениям и профилям"
     * @throws IOException
     * @throws WriteException
     */
    <T extends EmpServiceLengthReportModel> byte[] createReport(T model) throws IOException, WriteException;
}
