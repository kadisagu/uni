/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniemp.base.ext.Employee.ui.OrgUnitTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.OrgUnitTab.EmployeeOrgUnitTab;

/**
 * @author Vasily Zhukov
 * @since 04.04.2012
 */
@Configuration
public class EmployeeOrgUnitTabExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EmployeeOrgUnitTab _employeeOrgUnitTab;
    
    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        /**
         <component-tab name="employeePostList" label="Должности"
         component-name="ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitPostListTab" 
         permission-key="ognl:'orgUnit_viewPostList_' + model.orgUnit.orgUnitType.code"        	 	
         />
         <component-tab name="employeeStaffList" label="Штатное расписание"
         component-name="ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitStaffListTab" 
         permission-key="ognl:'orgUnit_viewStaffList_' + model.orgUnit.orgUnitType.code"         	
         />
         <component-tab name="vacaionSchedulesList" label="Графики отпусков"
         component-name="ru.tandemservice.uniemp.component.vacationShedule.OrgUnitVacationSchedulesList" 
         permission-key="ognl:'orgUnit_viewVacationSchedulesList_' + model.orgUnit.orgUnitType.code"         	
         />
         */
        return tabPanelExtensionBuilder(_employeeOrgUnitTab.employeeTabPanelExtPoint())
                .addTab(componentTab("employeePostList", "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitPostListTab").permissionKey("ui:secModel.orgUnit_viewPostList").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .addTab(componentTab("employeeStaffList", "ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.OrgUnitStaffListTab").permissionKey("ui:secModel.orgUnit_viewStaffList").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .addTab(componentTab("vacaionSchedulesList", "ru.tandemservice.uniemp.component.vacationShedule.OrgUnitVacationSchedulesList").permissionKey("ui:secModel.orgUnit_viewVacationSchedulesList").parameters("mvel:['orgUnitId':presenter.orgUnit.id]"))
                .create();
    }
}
