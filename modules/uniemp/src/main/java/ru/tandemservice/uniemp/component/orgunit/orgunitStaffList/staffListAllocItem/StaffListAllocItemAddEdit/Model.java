/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListAllocItemAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 08.04.2009
 */
@Input(keys = {"reserve", "staffListId", "staffListAllocationItemId"}, bindings = {"reserve", "staffListId", "staffListAllocationItemId"})
public class Model
{
    private Boolean _reserve;

    private ISingleSelectModel _financingSourceItemList;
    private List<FinancingSource> _financingSourcesList;

    private Long _staffListId;
    private StaffList _staffList;
    
    private Long _staffListAllocationItemId;
    private StaffListAllocationItem _staffListAllocationItem = new StaffListAllocationItem();
    
    private ISelectModel _staffListItemsListModel;
    private ISelectModel _employeePostsListModel;
    private DataWrapper _employeePost;
    
    private ISelectModel _raisingCoefficientListModel;

    private String _staffRateStr;
    
    private boolean _cantBeEdited = false;

    //Getters & Setters

    public DataWrapper getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(DataWrapper employeePost)
    {
        _employeePost = employeePost;
    }

    public String getStaffRateStr()
    {
        return _staffRateStr;
    }

    public void setStaffRateStr(String staffRateStr)
    {
        _staffRateStr = staffRateStr;
    }

    public Boolean getReserve()
    {
        return _reserve;
    }

    public void setReserve(Boolean reserve)
    {
        _reserve = reserve;
    }

    public List<FinancingSource> getFinancingSourcesList()
    {
        return _financingSourcesList;
    }

    public void setFinancingSourcesList(List<FinancingSource> financingSourcesList)
    {
        _financingSourcesList = financingSourcesList;
    }

    public ISingleSelectModel getFinancingSourceItemList()
    {
        return _financingSourceItemList;
    }

    public void setFinancingSourceItemList(ISingleSelectModel financingSourceItemList)
    {
        _financingSourceItemList = financingSourceItemList;
    }

    public Long getStaffListId()
    {
        return _staffListId;
    }

    public void setStaffListId(Long staffListId)
    {
        this._staffListId = staffListId;
    }

    public StaffList getStaffList()
    {
        return _staffList;
    }

    public void setStaffList(StaffList staffList)
    {
        this._staffList = staffList;
    }

    public Long getStaffListAllocationItemId()
    {
        return _staffListAllocationItemId;
    }

    public void setStaffListAllocationItemId(Long staffListAllocationItemId)
    {
        this._staffListAllocationItemId = staffListAllocationItemId;
    }

    public StaffListAllocationItem getStaffListAllocationItem()
    {
        return _staffListAllocationItem;
    }

    public void setStaffListAllocationItem(StaffListAllocationItem staffListAllocationItem)
    {
        this._staffListAllocationItem = staffListAllocationItem;
    }

    public ISelectModel getStaffListItemsListModel()
    {
        return _staffListItemsListModel;
    }

    public void setStaffListItemsListModel(ISelectModel staffListItemsListModel)
    {
        this._staffListItemsListModel = staffListItemsListModel;
    }

    public ISelectModel getEmployeePostsListModel()
    {
        return _employeePostsListModel;
    }

    public void setEmployeePostsListModel(ISelectModel employeePostsListModel)
    {
        this._employeePostsListModel = employeePostsListModel;
    }

    public ISelectModel getRaisingCoefficientListModel()
    {
        return _raisingCoefficientListModel;
    }

    public void setRaisingCoefficientListModel(ISelectModel raisingCoefficientListModel)
    {
        this._raisingCoefficientListModel = raisingCoefficientListModel;
    }

    public boolean isCantBeEdited()
    {
        return _cantBeEdited;
    }

    public void setCantBeEdited(boolean cantBeEdited)
    {
        this._cantBeEdited = cantBeEdited;
    }
}