/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.orgUnitPost.OrgUnitPostAddEdit;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.uniemp.entity.employee.OrgUnitPostRelation;

/**
 * @author dseleznev
 * Created on: 16.09.2008
 */
@Input(keys = { "orgUnitId", "orgUnitPostRelationId" }, bindings = { "orgUnitId", "orgUnitPostRelationId" })
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    private Long _orgUnitPostRelationId;
    private OrgUnitPostRelation _orgUnitPostRelation = new OrgUnitPostRelation();

    private ISelectModel _postRelationListModel;
    private List<ScienceStatus> _selectedScientificStatusesList = new ArrayList<>();
    private List<ScienceDegree> _selectedScientificDegreesList = new ArrayList<>();
    private List<PostType> _selectedPostTypesLevels = new ArrayList<>();

    private IMultiSelectModel _scientificStatusesModel;
    private IMultiSelectModel _scientificDegreesModel;
    private IMultiSelectModel _postTypesModel;

    private String _postTitleProperty;

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public Long getOrgUnitPostRelationId()
    {
        return _orgUnitPostRelationId;
    }

    public void setOrgUnitPostRelationId(Long orgUnitPostRelationId)
    {
        this._orgUnitPostRelationId = orgUnitPostRelationId;
    }

    public OrgUnitPostRelation getOrgUnitPostRelation()
    {
        return _orgUnitPostRelation;
    }

    public void setOrgUnitPostRelation(OrgUnitPostRelation orgUnitPostRelation)
    {
        this._orgUnitPostRelation = orgUnitPostRelation;
    }

    public ISelectModel getPostRelationListModel() {
		return _postRelationListModel;
	}

	public void setPostRelationListModel(ISelectModel postRelationListModel) {
		this._postRelationListModel = postRelationListModel;
	}

    public List<ScienceStatus> getSelectedScientificStatusesList()
    {
        return _selectedScientificStatusesList;
    }

    public void setSelectedScientificStatusesList(List<ScienceStatus> selectedScientificStatusesList)
    {
        this._selectedScientificStatusesList = selectedScientificStatusesList;
    }

    public List<ScienceDegree> getSelectedScientificDegreesList()
    {
        return _selectedScientificDegreesList;
    }

    public void setSelectedScientificDegreesList(List<ScienceDegree> selectedScientificDegreesList)
    {
        this._selectedScientificDegreesList = selectedScientificDegreesList;
    }

    public List<PostType> getSelectedPostTypesLevels()
    {
        return _selectedPostTypesLevels;
    }

    public void setSelectedPostTypesLevels(List<PostType> selectedPostTypesLevels)
    {
        this._selectedPostTypesLevels = selectedPostTypesLevels;
    }

    public IMultiSelectModel getScientificStatusesModel()
    {
        return _scientificStatusesModel;
    }

    public void setScientificStatusesModel(IMultiSelectModel scientificStatusesModel)
    {
        this._scientificStatusesModel = scientificStatusesModel;
    }

    public IMultiSelectModel getScientificDegreesModel()
    {
        return _scientificDegreesModel;
    }

    public void setScientificDegreesModel(IMultiSelectModel scientificDegreesModel)
    {
        this._scientificDegreesModel = scientificDegreesModel;
    }

    public IMultiSelectModel getPostTypesModel()
    {
        return _postTypesModel;
    }

    public void setPostTypesModel(IMultiSelectModel postTypesModel)
    {
        this._postTypesModel = postTypesModel;
    }

    public String getPostTitleProperty()
    {
        return _postTitleProperty;
    }

    public void setPostTitleProperty(String postTitleProperty)
    {
        this._postTitleProperty = postTitleProperty;
    }
}