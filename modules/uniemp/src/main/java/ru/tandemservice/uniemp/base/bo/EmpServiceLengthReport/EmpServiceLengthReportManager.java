/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.logic.EmpServiceLengthReportDao;
import ru.tandemservice.uniemp.base.bo.EmpServiceLengthReport.logic.IEmpServiceLengthReportDao;

/**
 * @author Alexander Shaburov
 * @since 25.01.13
 */
@Configuration
public class EmpServiceLengthReportManager extends BusinessObjectManager
{
    public static EmpServiceLengthReportManager instance()
    {
        return instance(EmpServiceLengthReportManager.class);
    }

    @Bean
    public IEmpServiceLengthReportDao dao()
    {
        return new EmpServiceLengthReportDao();
    }

}
