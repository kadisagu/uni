/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpSummaryPPSReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.uni.report.summaryReports.SummaryDataWrapper;

import java.util.Date;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 20.11.12
 */
public interface IEmpSummaryPPSReportDao extends INeedPersistenceSupport
{
    /**
     * Список врапперов для списка Численность профессорско-преподавательского состава.
     */
    List<SummaryDataWrapper<EmployeeWrapper>> getSummaryPPSList(Date reportDate, List<PostType> postTypeList, List<EmployeeWrapper> employeeWrapperList);

    /**
     * Список врапперов для списка Численность профессорско-преподавательского состава с учёной степенью и/или званием.
     */
    List<SummaryDataWrapper<EmployeeWrapper>> getSummaryPPSWithScienceList(Date reportDate, List<PostType> postTypeList, List<EmployeeWrapper> employeeWrapperList);

    /**
     * Список врапперов для списка Численность профессорско-преподавательского состава с учёной степенью доктора наук и/или званием профессора.
     */
    List<SummaryDataWrapper<EmployeeWrapper>> getSummaryPPSWithHighScienceList(Date reportDate, List<PostType> postTypeList, List<EmployeeWrapper> employeeWrapperList);

    /**
     * Подготавливает по даным из базы список врапперов КР-ов, содержащих все необходимые данные для отчета.
     * @param reportDate дата отчета
     * @return список враперов КР-ов
     */
    List<EmployeeWrapper> getEmployeeWrapperList(Date reportDate);

    /**
     * Подготавливает печатную форму отчета.
     * @return печатный документ отчета
     */
    RtfDocument getReportDocument(Date reportDate, List<PostType> postTypeList);
}
