/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeCertificationItem.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniemp.entity.employee.EmployeeCertificationItem;

/**
 * Create by ashaburov
 * Date 22.03.12
 */
public interface IEmpEmployeeCertificationItemDAO extends INeedPersistenceSupport
{
    void saveOrUpdateItem(EmployeeCertificationItem item);
}
