/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.AdditionalDataEdit;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.employee.EmployeeCard;

/**
 * @author dseleznev
 * Created on: 31.07.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        if(null != model.getEmployeeCard().getId())
            model.setEmployeeCard(get(EmployeeCard.class, model.getEmployeeCard().getId()));
        
        if(null == model.getEmployeeCard().getEmployee())
        {
            model.setEmployee(get(Employee.class, model.getEmployee().getId()));
            model.getEmployeeCard().setEmployee(model.getEmployee());
        }
    }

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getEmployeeCard());
    }
    
}