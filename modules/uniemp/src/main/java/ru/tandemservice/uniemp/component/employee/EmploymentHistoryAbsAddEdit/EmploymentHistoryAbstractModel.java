/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit;

import java.util.List;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;

/**
 * @author dseleznev
 * Created on: 24.12.2008
 */
public abstract class EmploymentHistoryAbstractModel
{
    private Long _employeeId;
    private Employee _employee;
    private List<PostType> _postTypesList;
    
    public abstract EmploymentHistoryItemBase getEmploymentHistoryItem();
    public abstract void setEmploymentHistoryItem(EmploymentHistoryItemBase item);

    public Long getEmployeeId()
    {
        return _employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        this._employeeId = employeeId;
    }

    public Employee getEmployee()
    {
        return _employee;
    }

    public void setEmployee(Employee employee)
    {
        this._employee = employee;
    }

    public List<PostType> getPostTypesList()
    {
        return _postTypesList;
    }

    public void setPostTypesList(List<PostType> postTypesList)
    {
        this._postTypesList = postTypesList;
    }

}