package ru.tandemservice.uniemp.entity.employee;

import java.math.BigDecimal;

import ru.tandemservice.uniemp.entity.employee.gen.StaffListFakePaymentGen;

/**
 * Фиксированная выплата должности штатного расписания
 */
public class StaffListFakePayment extends StaffListFakePaymentGen
{
    public static final String P_TITLE = "title";
    public static String P_STAFF_RATE = "staffRate";
    
    public String getTitle()
    {
        return getPayment().getTitle();
    }

    @Override
    public boolean isEditingDisabled()
    {
        return isConsiderAsAnAllocContext() || super.isEditingDisabled();
    }

    @Override
    public boolean isDeleteDisabled()
    {
        return !isConsiderAsAnAllocContext() && super.isDeleteDisabled();
    }

    public void setStaffRate(double value)
    {
        setStaffRateInteger((int) (value * 10000));
    }

    @Override
    public double getStaffRate()
    {
        BigDecimal x = new java.math.BigDecimal(getStaffRateInteger() * 0.0001);
        x = x.setScale(3, BigDecimal.ROUND_HALF_UP);

        return x.doubleValue();
    }
}