/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.StaffListPaymentsList;

import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.report.StaffListRepPaymentSettings;

/**
 * @author dseleznev
 * Created on: 16.03.2010
 */
@SuppressWarnings("unchecked")
public class Model
{
    public static String P_PAYMENT_SETTINGS_DISABLED = "paymentSettingsDisabled";
    
    private DynamicListDataSource _dataSource;

    private Payment _otherPayment;
    private List<Payment> _payments;

    private List<IEntity> _selectedTakeIntoAccountPayments;
    private List<IEntity> _selectedIndividualColumnPayments;

    private Map<Payment, StaffListRepPaymentSettings> _settingsMap;

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public Payment getOtherPayment()
    {
        return _otherPayment;
    }

    public void setOtherPayment(Payment otherPayment)
    {
        this._otherPayment = otherPayment;
    }

    public List<Payment> getPayments()
    {
        return _payments;
    }

    public void setPayments(List<Payment> payments)
    {
        this._payments = payments;
    }

    public List<IEntity> getSelectedTakeIntoAccountPayments()
    {
        return _selectedTakeIntoAccountPayments;
    }

    public void setSelectedTakeIntoAccountPayments(List<IEntity> selectedTakeIntoAccountPayments)
    {
        this._selectedTakeIntoAccountPayments = selectedTakeIntoAccountPayments;
    }

    public List<IEntity> getSelectedIndividualColumnPayments()
    {
        return _selectedIndividualColumnPayments;
    }

    public void setSelectedIndividualColumnPayments(List<IEntity> selectedIndividualColumnPayments)
    {
        this._selectedIndividualColumnPayments = selectedIndividualColumnPayments;
    }

    public Map<Payment, StaffListRepPaymentSettings> getSettingsMap()
    {
        return _settingsMap;
    }

    public void setSettingsMap(Map<Payment, StaffListRepPaymentSettings> settingsMap)
    {
        this._settingsMap = settingsMap;
    }
}