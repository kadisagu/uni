/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeePost.logic;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressInter;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.base.ext.Employee.logic.EmploymentDuration;
import ru.tandemservice.uniemp.entity.employee.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Create by ashaburov
 * Date 30.05.12
 */
public class EmpEmployeePostDAO extends CommonDAO implements IEmpEmployeePostDAO
{
    @SuppressWarnings("deprecation")
    @Override
    public RtfInjectModifier preparePersonCardModifier(EmployeePost employeePost, Date printDate)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();
        DateFormatter dateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;
        String emptyString = "";

        // общее
        TopOrgUnit topOrgUnit = TopOrgUnit.getInstance(true);

        modifier.put("commitDate", dateFormatter.format(printDate));
        modifier.put("organizationTitle", topOrgUnit.getFullTitle());
        modifier.put("OKPO", topOrgUnit.getOkpo());
        modifier.put("employeeNumber", employeePost.getEmployee().getEmployeeCode());
        modifier.put("INN", employeePost.getEmployee().getPerson().getInnNumber());
        modifier.put("policyNumber", employeePost.getEmployee().getPerson().getSnilsNumber());
        if (employeePost.getEmployee().getRecordNumber() != null)
            modifier.put("listNumber", employeePost.getEmployee().getRecordNumber().toString());
        else
            modifier.put("listNumber", emptyString);
        modifier.put("postType", employeePost.getPostType().getTitle());
        modifier.put("Sex", employeePost.getEmployee().getPerson().getIdentityCard().getSex().getTitle());

        // трудовой договор
        DQLSelectBuilder lcBuilder = new DQLSelectBuilder().fromEntity(EmployeeLabourContract.class, "b").column("b");
        lcBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeLabourContract.employeePost().fromAlias("b")), employeePost));
        EmployeeLabourContract labourContract = lcBuilder.createStatement(getSession()).uniqueResult();

        if (labourContract != null)
        {
            modifier.put("lcNumber", labourContract.getNumber());
            modifier.put("lcDate", dateFormatter.format(labourContract.getDate()));
            modifier.put("kindOfWork", labourContract.getEndDate() != null ? "Временно" : "Постоянно");
        }
        else
        {
            modifier.put("lcNumber", emptyString);
            modifier.put("lcDate", emptyString);
            modifier.put("kindOfWork", emptyString);
        }

        // Общие сведения сотрудника
        modifier.put("lastName", employeePost.getEmployee().getPerson().getIdentityCard().getLastName());
        modifier.put("firstName", employeePost.getEmployee().getPerson().getIdentityCard().getFirstName());
        modifier.put("middleName", employeePost.getEmployee().getPerson().getIdentityCard().getMiddleName());
        modifier.put("birthDate", dateFormatter.format(employeePost.getEmployee().getPerson().getIdentityCard().getBirthDate()));
        modifier.put("birthPlace", employeePost.getEmployee().getPerson().getIdentityCard().getBirthPlace());
        modifier.put("nationality", employeePost.getEmployee().getPerson().getIdentityCard().getCitizenship().getTitle());

        // иностранные языки
        DQLSelectBuilder mainLanguageBuilder = new DQLSelectBuilder().fromEntity(PersonForeignLanguage.class, "b").column("b");
        mainLanguageBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonForeignLanguage.person().fromAlias("b")), employeePost.getPerson()));
        mainLanguageBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonForeignLanguage.main().fromAlias("b")), true));

        PersonForeignLanguage mainLanguage = mainLanguageBuilder.createStatement(getSession()).uniqueResult();

        DQLSelectBuilder otherLanguageBuilder = new DQLSelectBuilder().fromEntity(PersonForeignLanguage.class, "b").column("b");
        otherLanguageBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonForeignLanguage.person().fromAlias("b")), employeePost.getPerson()));
        otherLanguageBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonForeignLanguage.main().fromAlias("b")), false));
        otherLanguageBuilder.order(DQLExpressions.property(PersonForeignLanguage.language().title().fromAlias("b")));

        List<PersonForeignLanguage> otherLanguageList = otherLanguageBuilder.createStatement(getSession()).list();

        if (mainLanguage != null)
        {
            modifier.put("language", mainLanguage.getLanguage().getTitle());
            if (mainLanguage.getSkill() != null)
                modifier.put("languageLevel", mainLanguage.getSkill().getTitle());
            else
                modifier.put("languageLevel", emptyString);
        }
        else if (!otherLanguageList.isEmpty())
        {
            modifier.put("language", otherLanguageList.get(0).getLanguage().getTitle());
            if (otherLanguageList.get(0).getSkill() != null)
                modifier.put("languageLevel", otherLanguageList.get(0).getSkill().getTitle());
            else
                modifier.put("languageLevel", emptyString);
        }
        else
        {
            modifier.put("language", emptyString);
            modifier.put("languageLevel", emptyString);
        }

        if (!otherLanguageList.isEmpty())
        {
            StringBuilder otherLangBuilder = new StringBuilder();

            for (PersonForeignLanguage language : otherLanguageList)
            {
                if (otherLanguageList.indexOf(language) == 0 && mainLanguage == null)
                    continue;

                otherLangBuilder.append(language.getLanguage().getTitle()).append(" ");
                if (language.getSkill() != null)
                    otherLangBuilder.append(language.getSkill().getTitle()).append(" ");

                if (!(otherLanguageList.indexOf(language) == otherLanguageList.size() - 1))
                    otherLangBuilder.append(", ");
            }

            modifier.put("otherLanguages", otherLangBuilder.toString());
        }
        else
            modifier.put("otherLanguages", emptyString);

        modifier.put("educationLevel", emptyString);
        modifier.put("mainAcademy", emptyString);
        modifier.put("mainDiploma", emptyString);
        modifier.put("mainDiplomaSeria", emptyString);
        modifier.put("mainDiplomaNumber", emptyString);
        modifier.put("mainYear", emptyString);
        modifier.put("mainQualification", emptyString);
        modifier.put("mainEducationType", emptyString);

        // основное образование
        if (PersonEduDocumentManager.isShowLegacyEduDocuments() && employeePost.getPerson().getPersonEduInstitution() != null)
        {
            PersonEduInstitution personEduInstitution = employeePost.getPerson().getPersonEduInstitution();

            modifier.put("educationLevel", personEduInstitution.getEducationLevel().getTitle());
            modifier.put("mainAcademy", personEduInstitution.getEduInstitutionTitle());
            modifier.put("mainDiploma", personEduInstitution.getDocumentType().getTitle());
            modifier.put("mainDiplomaSeria", personEduInstitution.getSeria());
            modifier.put("mainDiplomaNumber", personEduInstitution.getNumber());
            modifier.put("mainYear", String.valueOf(personEduInstitution.getYearEnd()));
            if (personEduInstitution.getDiplomaQualification() != null)
                modifier.put("mainQualification", personEduInstitution.getDiplomaQualification().getTitle());
            else
                modifier.put("mainQualification", emptyString);
            if (personEduInstitution.getEmployeeSpeciality() != null)
                modifier.put("mainEducationType", personEduInstitution.getEmployeeSpeciality().getTitle());
            else
                modifier.put("mainEducationType", emptyString);
        }
        if (PersonEduDocumentManager.isShowNewEduDocuments()) {
            List<PersonEduDocument> documents = getList(PersonEduDocument.class, PersonEduDocument.person(), employeePost.getPerson());
            Collections.sort(documents, (o1, o2) -> {
                if (o1.getEduLevel() == null) return 1;
                if (o2.getEduLevel() == null) return -1;
                return o2.getEduLevel().getCode().compareTo(o1.getEduLevel().getCode());
            });
            if (!documents.isEmpty() && documents.get(0).getEduLevel() != null) {
                PersonEduDocument document = documents.get(0);
                modifier.put("educationLevel", document.getEduLevel().getTitle());
                modifier.put("mainAcademy", document.getEduOrganization());
                modifier.put("mainDiploma", document.getDocumentKindTitle());
                modifier.put("mainDiplomaSeria", document.getSeria());
                modifier.put("mainDiplomaNumber", document.getNumber());
                modifier.put("mainYear", String.valueOf(document.getYearEnd()));
                if (document.getQualification() != null) modifier.put("mainQualification", document.getQualification());
                if (document.getEduProgramSubject() != null) modifier.put("mainEducationType", document.getEduProgramSubject());
            }
        }

        // дополнительное образование
        Object[] excludeEduLevelStageCodes = new String[]{"100", "101", "102", "103", "104", "105", "106", "107", "107", "108", "109", "110"};
        DQLSelectBuilder addEduInstBuilder = new DQLSelectBuilder().fromEntity(PersonEduInstitution.class, "b").column("b");
        addEduInstBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonEduInstitution.person().fromAlias("b")), employeePost.getPerson()));
        addEduInstBuilder.where(DQLExpressions.notIn(DQLExpressions.property(PersonEduInstitution.educationLevelStage().code().fromAlias("b")), excludeEduLevelStageCodes));
        if (employeePost.getPerson().getPersonEduInstitution() != null)
            addEduInstBuilder.where(DQLExpressions.ne(DQLExpressions.property(PersonEduInstitution.id().fromAlias("b")), DQLExpressions.value(employeePost.getPerson().getPersonEduInstitution().getId())));
        addEduInstBuilder.order(DQLExpressions.property(PersonEduInstitution.yearEnd().fromAlias("b")), OrderDirection.desc);

        List<PersonEduInstitution> addEduInstitutionList = addEduInstBuilder.createStatement(getSession()).list();

        PersonEduInstitution addEduInstitution = null;
        if (addEduInstitutionList.size() == 1)
        {
            addEduInstitution = addEduInstitutionList.get(0);
        }
        else if (addEduInstitutionList.size() > 1)
        {
            for (PersonEduInstitution eduInstitution : addEduInstitutionList)
                if (eduInstitution.getEducationLevelStage().getCode().equals(UniDefines.EDUCATION_LEVEL_STAGE_HIGH_PROFILE))
                {
                    addEduInstitution = eduInstitution;
                    break;
                }
        }

        if (addEduInstitution != null)
        {
            modifier.put("academy", addEduInstitution.getEduInstitutionTitle());
            modifier.put("diploma", addEduInstitution.getDocumentType().getTitle());
            modifier.put("diplomaSeria", addEduInstitution.getSeria());
            modifier.put("diplomaNumber", addEduInstitution.getNumber());
            modifier.put("year", String.valueOf(addEduInstitution.getYearEnd()));
            if (addEduInstitution.getDiplomaQualification() != null)
                modifier.put("qualification", addEduInstitution.getDiplomaQualification().getTitle());
            else
                modifier.put("qualification", emptyString);
            if (addEduInstitution.getEmployeeSpeciality() != null)
                modifier.put("educationType", addEduInstitution.getEmployeeSpeciality().getTitle());
            else
                modifier.put("educationType", emptyString);
        }
        else
        {
            modifier.put("academy", emptyString);
            modifier.put("diploma", emptyString);
            modifier.put("diplomaSeria", emptyString);
            modifier.put("diplomaNumber", emptyString);
            modifier.put("year", emptyString);
            modifier.put("qualification", emptyString);
            modifier.put("educationType", emptyString);
        }

        // послевузовское образование
        Object[] postEduCodes = new String[]{"101", "102", "103", "104", "105", "106", "107", "108", "109", "110"};
        DQLSelectBuilder postEduInstBuilder = new DQLSelectBuilder().fromEntity(PersonEduInstitution.class, "b").column("b");
        postEduInstBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonEduInstitution.person().fromAlias("b")), employeePost.getPerson()));
        postEduInstBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonEduInstitution.educationLevelStage().code().fromAlias("b")), postEduCodes));
        postEduInstBuilder.order(DQLExpressions.property(PersonEduInstitution.yearEnd().fromAlias("b")), OrderDirection.desc);

        List<PersonEduInstitution> postEduInstitutionList = postEduInstBuilder.createStatement(getSession()).list();
        if (!postEduInstitutionList.isEmpty())
        {
            PersonEduInstitution eduInstitution = postEduInstitutionList.get(0);

            modifier.put("postEducationLevel", eduInstitution.getEducationLevelStage().getTitle());
            if (eduInstitution.getEduInstitution() != null)
                modifier.put("postAcademy", eduInstitution.getEduInstitution().getTitle());
            else
                modifier.put("postAcademy", emptyString);
            modifier.put("postDiploma", eduInstitution.getDocumentType().getTitle());
            modifier.put("postDiplomaNumber", eduInstitution.getNumber());
            modifier.put("postDiplomaDate", dateFormatter.format(eduInstitution.getIssuanceDate()));
            modifier.put("postYear", String.valueOf(eduInstitution.getYearEnd()));
            if (eduInstitution.getEmployeeSpeciality() != null)
                modifier.put("postEducationType", eduInstitution.getEmployeeSpeciality().getTitle());
            else
                modifier.put("postEducationType", emptyString);
        }
        else
        {
            modifier.put("postEducationLevel", emptyString);
            modifier.put("postAcademy", emptyString);
            modifier.put("postDiploma", emptyString);
            modifier.put("postDiplomaNumber", emptyString);
            modifier.put("postDiplomaDate", emptyString);
            modifier.put("postYear", emptyString);
            modifier.put("postEducationType", emptyString);
        }

        if (employeePost.getEmployee().getProfession() != null)
            modifier.put("profession", employeePost.getEmployee().getProfession().getTitle());
        else
            modifier.put("profession", emptyString);

        // составления документа
        modifier.put("commitDay", String.valueOf(CoreDateUtils.getDayOfMonth(printDate)));
        modifier.put("commitMonthStr", RussianDateFormatUtils.getMonthName(printDate, false));
        modifier.put("commitYr", String.valueOf(CoreDateUtils.getYear(printDate)).substring(2, 4));

        // История должностей сотрудника. Общий стаж
        DQLSelectBuilder empHistBuilder = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "b").column(DQLExpressions.property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("b")));
        empHistBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().employee().fromAlias("b")), employeePost.getEmployee()));
        empHistBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmpHistoryToServiceLengthTypeRelation.serviceLengthType().code().fromAlias("b")), UniempDefines.SERVICE_LENGTH_TYPE_COMMON));

        List<EmploymentHistoryItemBase> empHistoryList = empHistBuilder.createStatement(getSession()).list();

        EmploymentDuration employmentDuration = new EmploymentDuration();
        for (EmploymentHistoryItemBase item : empHistoryList)
            employmentDuration.addDatePeriod(item.getAssignDate(), item.getDismissalDate());

        employmentDuration.calculateEmploymentPeriod();

        modifier.put("lenghOfServiceDays", employmentDuration.getEmploymentDurationDaysAmountStr());
        modifier.put("lenghOfServiceMonth", employmentDuration.getEmploymentDurationMonthsAmountStr());
        modifier.put("lenghOfServiceYear", employmentDuration.getEmploymentDurationYearsAmountStr());

        // семейное положение
        if (employeePost.getPerson().getFamilyStatus() != null)
            modifier.put("maritalStatus", employeePost.getPerson().getFamilyStatus().getTitle());
        else
            modifier.put("maritalStatus", emptyString);

        // Данные удостоверения личности
        modifier.put("passSeria", employeePost.getPerson().getIdentityCard().getSeria());
        modifier.put("passNumber", employeePost.getPerson().getIdentityCard().getNumber());
        if (employeePost.getPerson().getIdentityCard().getIssuanceDate() != null)
        {
            modifier.put("issueDay", String.valueOf(CoreDateUtils.getDayOfMonth(employeePost.getPerson().getIdentityCard().getIssuanceDate())));
            modifier.put("issueMonthStr", RussianDateFormatUtils.getMonthName(employeePost.getPerson().getIdentityCard().getIssuanceDate(), false));
            modifier.put("issueYear", String.valueOf(CoreDateUtils.getYear(printDate)));
        }
        else
        {
            modifier.put("issueDay", emptyString);
            modifier.put("issueMonthStr", emptyString);
            modifier.put("issueYear", emptyString);
        }
        modifier.put("issuancePlace", employeePost.getPerson().getIdentityCard().getIssuancePlace());
        if (employeePost.getPerson().getIdentityCard().getAddress() != null)
        {
            if(employeePost.getPerson().getIdentityCard().getAddress() instanceof AddressDetailed)
            {
                AddressDetailed addressDetailed = (AddressDetailed) employeePost.getPerson().getIdentityCard().getAddress();
                modifier.put("passportIndex", addressDetailed instanceof AddressRu ? ((AddressRu) addressDetailed).getInheritedPostCode() : ((AddressInter) addressDetailed).getPostCode());
            }
            modifier.put("passportAddress", employeePost.getPerson().getIdentityCard().getAddress().getTitle());
        }
        else
        {
            modifier.put("passportIndex", emptyString);
            modifier.put("passportAddress", emptyString);
        }
        if (employeePost.getPerson().getIdentityCard().getRegistrationPeriodFrom() != null)
        {
            modifier.put("regDay", String.valueOf(CoreDateUtils.getDayOfMonth(employeePost.getPerson().getIdentityCard().getRegistrationPeriodFrom())));
            modifier.put("regMonthStr", RussianDateFormatUtils.getMonthName(employeePost.getPerson().getIdentityCard().getRegistrationPeriodFrom(), false));
            modifier.put("regYear", String.valueOf(CoreDateUtils.getYear(employeePost.getPerson().getIdentityCard().getRegistrationPeriodFrom())));
        }
        else
        {
            modifier.put("regDay", emptyString);
            modifier.put("regMonthStr", emptyString);
            modifier.put("regYear", emptyString);
        }

        // Контакты
        if (employeePost.getPerson().getAddress() != null)
        {
            if(employeePost.getPerson().getAddress() instanceof AddressDetailed)
            {
                AddressDetailed addressDetailed = (AddressDetailed) employeePost.getPerson().getAddress();
                modifier.put("passportIndex", addressDetailed instanceof AddressRu ? ((AddressRu) addressDetailed).getInheritedPostCode() : ((AddressInter) addressDetailed).getPostCode());
            }
            modifier.put("factAddress", employeePost.getPerson().getAddress().getTitle());
        }
        else
        {
            modifier.put("factIndex", emptyString);
            modifier.put("factAddress", emptyString);
        }
        if (employeePost.getPerson().getAddress() != null)
            modifier.put("phoneNumber", employeePost.getPerson().getContactData().getMainPhones());
        else
            modifier.put("phoneNumber", "");

        // Сведения о воинском учете
        DQLSelectBuilder militaryStatusBuilder = new DQLSelectBuilder().fromEntity(PersonMilitaryStatus.class, "b").column("b");
        militaryStatusBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonMilitaryStatus.person().fromAlias("b")), employeePost.getPerson()));

        PersonMilitaryStatus militaryStatus = militaryStatusBuilder.createStatement(getSession()).uniqueResult();

        if (militaryStatus != null)
        {
            if (militaryStatus.getMilitaryRegCategory() != null)
                modifier.put("reserveCategory", militaryStatus.getMilitaryRegCategory().getTitle());
            else
                modifier.put("reserveCategory", emptyString);
            if (militaryStatus.getMilitaryRank() != null)
                modifier.put("rank", militaryStatus.getMilitaryRank().getTitle());
            else
                modifier.put("rank", emptyString);
            modifier.put("militarySpeciality", militaryStatus.getVUSNumber());
            if (militaryStatus.getMilitaryAbilityStatus() != null)
                modifier.put("militaryServiceAbilityCategory", militaryStatus.getMilitaryAbilityStatus().getTitle());
            else
                modifier.put("militaryServiceAbilityCategory", emptyString);
            if (militaryStatus.getMilitaryOffice() != null)
                modifier.put("militaryRegistrationOffice", militaryStatus.getMilitaryOffice().getTitle());
            else
                modifier.put("militaryRegistrationOffice", emptyString);
            if (militaryStatus.getMilitarySpecialRegistration() != null)
            {
                if (militaryStatus.getMilitarySpecialRegistration().getCode().equals("4"))
                    modifier.put("militaryRegister", "Нет");
                else
                    modifier.put("militaryRegister", "Да");

                List<String> militarySpecialCodeList = new ArrayList<>();
                militarySpecialCodeList.add("1");
                militarySpecialCodeList.add("2");

                if (militarySpecialCodeList.contains(militaryStatus.getMilitarySpecialRegistration().getCode()))
                    modifier.put("commonRegister", militaryStatus.getMilitarySpecialRegistration().getTitle());
                else
                    modifier.put("commonRegister", emptyString);

                militarySpecialCodeList.clear();
                militarySpecialCodeList.add("3");

                if (militarySpecialCodeList.contains(militaryStatus.getMilitarySpecialRegistration().getCode()))
                    modifier.put("specialRegister", militaryStatus.getMilitarySpecialRegistration().getTitle());
                else
                    modifier.put("specialRegister", emptyString);
            }
            else
            {
                modifier.put("militaryRegister", "Нет");
                modifier.put("commonRegister", emptyString);
                modifier.put("specialRegister", emptyString);
            }
            modifier.put("strikeOffRegister", emptyString);

        }
        else
        {
            modifier.put("reserveCategory", emptyString);
            modifier.put("rank", emptyString);
            modifier.put("militarySpeciality", emptyString);
            modifier.put("militaryServiceAbilityCategory", emptyString);
            modifier.put("militaryRegistrationOffice", emptyString);
            modifier.put("militaryRegister", "Нет");
            modifier.put("commonRegister", emptyString);
            modifier.put("specialRegister", emptyString);
            modifier.put("strikeOffRegister", emptyString);
        }

        // увольнение
        if (employeePost.getDismissalDate() != null)
        {
            modifier.put("dismissDay", String.valueOf(CoreDateUtils.getDayOfMonth(employeePost.getDismissalDate())));
            modifier.put("dismissMonthStr", RussianDateFormatUtils.getMonthName(employeePost.getDismissalDate(), false));
            modifier.put("dismissYr", String.valueOf(CoreDateUtils.getYear(employeePost.getDismissalDate())).substring(2, 4));
        }
        else
        {
            modifier.put("dismissDay", emptyString);
            modifier.put("dismissMonthStr", emptyString);
            modifier.put("dismissYr", emptyString);
        }

        return modifier;
    }

    @Override
    public RtfTableModifier preparePersonCardTableModifier(EmployeePost employeePost, Date printDate)
    {
        DateFormatter dateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;
        String emptyString = "";

        RtfTableModifier tableModifier = new RtfTableModifier();

        // Ближайшие родственники
        DQLSelectBuilder nextOfKinBuilder = new DQLSelectBuilder().fromEntity(PersonNextOfKin.class, "b").column("b");
        nextOfKinBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonNextOfKin.person().fromAlias("b")), employeePost.getPerson()));

        List<PersonNextOfKin> nextOfKinList = nextOfKinBuilder.createStatement(getSession()).list();

        List<String[]> personNextOfKinList = new ArrayList<>();

        for (PersonNextOfKin person : nextOfKinList)
        {
            String[] line = new String[3];

            line[0] = person.getRelationDegree().getTitle();
            line[1] = person.getFullFio();
            line[2] = dateFormatter.format(person.getBirthDate());

            personNextOfKinList.add(line);
        }

        while (personNextOfKinList.size() < 3)
        {
            String[] line = new String[]{"", "", ""};
            personNextOfKinList.add(line);
        }

        tableModifier.put("T1", personNextOfKinList.toArray(new String[personNextOfKinList.size()][3]));

        // Данные истории должностей сотрудника
        DQLSelectBuilder empHistBuilder = new DQLSelectBuilder().fromEntity(EmploymentHistoryItemBase.class, "b").column("b");
        empHistBuilder.where(DQLExpressions.not(DQLExpressions.instanceOf("b", EmploymentHistoryItemFake.class)));
        empHistBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmploymentHistoryItemBase.employee().fromAlias("b")), employeePost.getEmployee()));
        empHistBuilder.order(DQLExpressions.property(EmploymentHistoryItemBase.assignDate().fromAlias("b")), OrderDirection.desc);

        List<EmploymentHistoryItemBase> empHistoryList = empHistBuilder.createStatement(getSession()).list();

        List<String[]> empHistoryLineList = new ArrayList<>();

        for (EmploymentHistoryItemBase item : empHistoryList)
        {
            String[] line = new String[6];

            line[0] = dateFormatter.format(item.getAssignDate());
            line[1] = item.getOrgUnitTitle();
            line[2] = item.getPostTitle();
            line[3] = emptyString;
            line[4] = item.getBasics();
            line[5] = emptyString;

            empHistoryLineList.add(line);
        }

        while (empHistoryLineList.size() < 3)
        {
            String[] line = new String[]{"", "", ""};
            empHistoryLineList.add(line);
        }

        tableModifier.put("T2", empHistoryLineList.toArray(new String[empHistoryLineList.size()][6]));

        // Аттестация
        DQLSelectBuilder certBuilder = new DQLSelectBuilder().fromEntity(EmployeeCertificationItem.class, "b").column("b");
        certBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeCertificationItem.employeePost().fromAlias("b")), employeePost));
        certBuilder.order(DQLExpressions.property(EmployeeCertificationItem.certificationDate().fromAlias("b")), OrderDirection.desc);

        List<EmployeeCertificationItem> certificationItemList = certBuilder.createStatement(getSession()).list();

        List<String[]> certificationItemLineList = new ArrayList<>();

        for (EmployeeCertificationItem item : certificationItemList)
        {
            String[] line = new String[5];

            line[0] = dateFormatter.format(item.getCertificationDate());
            line[1] = item.getResolutionCommission();
            line[2] = item.getDocumentNumber();
            line[3] = dateFormatter.format(item.getDocumentDate());
            line[4] = item.getBasic();

            certificationItemLineList.add(line);
        }

        while (certificationItemLineList.size() < 3)
        {
            String[] line = new String[]{"", "", ""};
            certificationItemLineList.add(line);
        }

        tableModifier.put("T3", certificationItemLineList.toArray(new String[certificationItemLineList.size()][5]));

        // Повышение квалификации
        DQLSelectBuilder trainingBuilder = new DQLSelectBuilder().fromEntity(EmployeeTrainingItem.class, "b").column("b");
        trainingBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeTrainingItem.employee().fromAlias("b")), employeePost.getEmployee()));
        trainingBuilder.order(DQLExpressions.property(EmployeeTrainingItem.finishDate().fromAlias("b")), OrderDirection.desc);

        List<EmployeeTrainingItem> trainingItemList = trainingBuilder.createStatement(getSession()).list();

        List<String[]> trainingItemLineList = new ArrayList<>();

        for (EmployeeTrainingItem item : trainingItemList)
        {
            String[] line = new String[8];

            line[0] = dateFormatter.format(item.getStartDate());
            line[1] = dateFormatter.format(item.getFinishDate());
            line[2] = item.getTrainingTitle();
            line[3] = item.getEduInstitutionAndAddress();
            line[4] = item.getEduDocumentType().getTitle();
            line[5] = item.getEduDocumentSeria() + " " + item.getEduDocumentNumber();
            line[6] = dateFormatter.format(item.getEduDocumentDate());
            line[7] = item.getBasics();

            trainingItemLineList.add(line);
        }

        while (trainingItemLineList.size() < 3)
        {
            String[] line = new String[]{"", "", ""};
            trainingItemLineList.add(line);
        }

        tableModifier.put("T4", trainingItemLineList.toArray(new String[trainingItemLineList.size()][8]));

        // Профессиональная переподготовка
        DQLSelectBuilder retrainingBuilder = new DQLSelectBuilder().fromEntity(EmployeeRetrainingItem.class, "b").column("b");
        retrainingBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeRetrainingItem.employee().fromAlias("b")), employeePost.getEmployee()));
        retrainingBuilder.order(DQLExpressions.property(EmployeeRetrainingItem.finishDate().fromAlias("b")), OrderDirection.desc);

        List<EmployeeRetrainingItem> retrainingItemList = retrainingBuilder.createStatement(getSession()).list();

        List<String[]> retrainingItemLineList = new ArrayList<>();

        for (EmployeeRetrainingItem item : retrainingItemList)
        {
            String[] line = new String[7];

            line[0] = dateFormatter.format(item.getStartDate());
            line[1] = dateFormatter.format(item.getFinishDate());
            line[2] = item.getSpecialityTitle();
            line[3] = item.getEduDocumentType().getTitle();
            line[4] = item.getEduDocumentSeria() + " " + item.getEduDocumentNumber();
            line[5] = dateFormatter.format(item.getEduDocumentDate());
            line[6] = item.getBasics();

            retrainingItemLineList.add(line);
        }

        while (retrainingItemLineList.size() < 3)
        {
            String[] line = new String[]{"", "", ""};
            retrainingItemLineList.add(line);
        }

        tableModifier.put("T5", retrainingItemLineList.toArray(new String[retrainingItemLineList.size()][7]));

        // Награды и поощрения сотрудников
        DQLSelectBuilder encouragementBuilder = new DQLSelectBuilder().fromEntity(EmployeeEncouragement.class, "b").column("b");
        encouragementBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeEncouragement.employee().fromAlias("b")), employeePost.getEmployee()));
        encouragementBuilder.order(DQLExpressions.property(EmployeeEncouragement.assignDate().fromAlias("b")), OrderDirection.desc);

        List<EmployeeEncouragement> encouragementList = encouragementBuilder.createStatement(getSession()).list();

        List<String[]> encouragementLineList = new ArrayList<>();

        for (EmployeeEncouragement item : encouragementList)
        {
            String[] line = new String[4];

            line[0] = item.getTitle();
            line[1] = item.getDocument();
            line[2] = item.getDocumentNumber();
            line[3] = dateFormatter.format(item.getAssignDate());

            encouragementLineList.add(line);
        }

        while (encouragementLineList.size() < 3)
        {
            String[] line = new String[]{"", "", ""};
            encouragementLineList.add(line);
        }

        tableModifier.put("T6", encouragementLineList.toArray(new String[encouragementLineList.size()][4]));

        // отпуска сотрудника
        DQLSelectBuilder holidayBuilder = new DQLSelectBuilder().fromEntity(EmployeeHoliday.class, "b").column("b");
        holidayBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeHoliday.employeePost().fromAlias("b")), employeePost));
        holidayBuilder.order(DQLExpressions.property(EmployeeHoliday.beginDate().fromAlias("b")), OrderDirection.desc);

        List<EmployeeHoliday> holidayList = holidayBuilder.createStatement(getSession()).list();

        List<String[]> holidayLineList = new ArrayList<>();

        for (EmployeeHoliday item : holidayList)
        {
            String[] line = new String[7];

            line[0] = item.getHolidayType().getTitle();
            line[1] = dateFormatter.format(item.getBeginDate());
            line[2] = dateFormatter.format(item.getEndDate());
            line[3] = String.valueOf(item.getDuration());
            line[4] = dateFormatter.format(item.getStartDate());
            line[5] = dateFormatter.format(item.getFinishDate());
            line[6] = item.getOrderNumberDate();

            holidayLineList.add(line);
        }

        while (holidayLineList.size() < 3)
        {
            String[] line = new String[]{"", "", ""};
            holidayLineList.add(line);
        }

        tableModifier.put("T7", holidayLineList.toArray(new String[holidayLineList.size()][7]));

        // льготы сортрудника
        DQLSelectBuilder benefitBuilder = new DQLSelectBuilder().fromEntity(PersonBenefit.class, "b").column("b");
        benefitBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonBenefit.person().fromAlias("b")), employeePost.getPerson()));
        benefitBuilder.order(DQLExpressions.property(PersonBenefit.date().fromAlias("b")), OrderDirection.desc);

        List<PersonBenefit> benefitList = benefitBuilder.createStatement(getSession()).list();

        List<String[]> benefitLineList = new ArrayList<>();

        for (PersonBenefit item : benefitList)
        {
            String[] line = new String[4];

            line[0] = item.getTitle();
            line[1] = emptyString;
            line[2] = dateFormatter.format(item.getDate());
            line[3] = item.getBasic();

            benefitLineList.add(line);
        }

        while (benefitLineList.size() < 3)
        {
            String[] line = new String[]{"", "", ""};
            benefitLineList.add(line);
        }

        tableModifier.put("T8", benefitLineList.toArray(new String[benefitLineList.size()][4]));

        return tableModifier;
    }
}
