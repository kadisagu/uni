/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.AdditionalDataTab.block.serviceLengthData;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uniemp.entity.catalog.ServiceLengthType;
import ru.tandemservice.uniemp.entity.employee.EmpHistoryToServiceLengthTypeRelation;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.*;


/**
 * @author azhebko
 * Created on 19.06.12
 */
public class ServiceLengthDataParam implements IReportDQLModifier
{
    private IReportParam<Long> _yearsNumberFrom = new ReportParam<>();
    private IReportParam<Long> _yearsNumberTo = new ReportParam<>();
    private IReportParam<ServiceLengthType> _serviceLengthType = new ReportParam<>();

    private boolean _disableServiceLength = false;

    private String employeeAlias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        return dql.innerJoinEntity(Employee.class, Employee.person());
    }

    private String serviceLengthAlias(ReportDQL dql)
    {
        // соединяем кадрового ресурса
        String employeeAlias = employeeAlias(dql);

        // соединяем стаж
        return dql.innerJoinEntity(employeeAlias, EmploymentHistoryItemBase.class, EmploymentHistoryItemBase.employee());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_serviceLengthType.isActive())
        {
            printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "serviceLengthData.serviceLengthType", _serviceLengthType.getData().getTitle());

            DQLSelectBuilder relationBuilder = new DQLSelectBuilder().fromEntity(EmpHistoryToServiceLengthTypeRelation.class, "rel")
                    .column(property("eh", "id"))
                    .joinPath(DQLJoinType.inner, EmpHistoryToServiceLengthTypeRelation.employmentHistoryItemBase().fromAlias("rel"), "eh")
                    .where(eq(property("eh", EmploymentHistoryItemBase.emptyServiceLengthType()), value(Boolean.FALSE)))
                    .where(eq(property("rel", EmpHistoryToServiceLengthTypeRelation.serviceLengthType()), value(_serviceLengthType.getData())));

            DQLSelectBuilder ehiBuilder = new DQLSelectBuilder()
                    .fromEntity(EmploymentHistoryItemBase.class, "empHistItm").where(in(property("empHistItm", "id"), relationBuilder.buildQuery()));

            if (_yearsNumberFrom.isActive() || _yearsNumberTo.isActive())
            {
                Date now = new Date();

                final String begin = EmploymentHistoryItemBase.P_ASSIGN_DATE;
                final String end = EmploymentHistoryItemBase.P_DISMISSAL_DATE;
                final String employee = EmploymentHistoryItemBase.employee().id().s();
                
                // отбираем общие min и max значения для всех периодов, группируя по сотруднику
                DQLSelectBuilder totalMinMax = new DQLSelectBuilder()
                        .fromEntity(EmploymentHistoryItemBase.class, "eh")
                        .column(property("eh", employee), "ehEmpId")
                        .column(min(property("eh", begin)), "min")
                         // если значение конца периода пустое, либо большее текущей даты, то задаем ему текущую дату
                        .column(max(caseExpr(ge(coalesce(property("eh", end), valueDate(now)), valueDate(now)), valueDate(now), property("eh", end))), "max").where(in(property("eh", "id"), relationBuilder.buildQuery()))
                        .where(le(property("eh", begin), valueDate(now)))
                        .group(property("eh", employee));

                // задаем условие для промежутка являющегося пустым: t3.end > t1.end & t2.begin > t3.begin
                // для дат окончания, которые не заданы явно, либо выходят за пределы текущей даты, мы обрезаем значение по текущую дату
                DQLSelectBuilder gapIntersect = new DQLSelectBuilder()
                        .fromEntity(EmploymentHistoryItemBase.class, "t3")
                        .column(value(1))
                        .where(gt(caseExpr(or(gt(property("t3", end), valueDate(now)), isNull(property("t3", end))), valueDate(now), property("t3", end)),
                                  caseExpr(or(gt(property("t1", end), valueDate(now)), isNull(property("t1", end))), valueDate(now), property("t1", end))))
                        .where(gt(property("t2", begin), property("t3", begin))).where(eq(property("t3", employee), property("t2", employee)))
                        .where(eq(property("t3", employee), property("t1", employee))).where(in(property("t3", "id"), relationBuilder.buildQuery()));

                // находим пустые промежутки
                DQLSelectBuilder searchGap = new DQLSelectBuilder()
                        .distinct() // distinct, чтобы одинковые пробелы не суммировать
                        .column(property("t1", employee), "gapEmpId")
                        // если есть выход за текущую дату, обрезаем значение до текущей
                        .column(caseExpr(gt(property("t1", end), valueDate(now)), valueDate(now), property("t1", end)), "gapStart")
                        .column(caseExpr(gt(property("t2", begin), valueDate(now)), valueDate(now), property("t2", begin)), "gapEnd")
                        .fromEntity(EmploymentHistoryItemBase.class, "t1")
                        .fromEntity(EmploymentHistoryItemBase.class, "t2")
                        // исключаем сравнение периодов самих с собой
                        .where(ne(property("t1", "id"), property("t2", "id")))
                        // дата начала второго периода позже даты конца первого (т.е. есть явный пустой промежуток, где разница между датами больше 1 дня - исключаем попадание промежутков "на стыке")
                        .where(and(gt(property("t2", begin), property("t1", end)), gt(diffdays(property("t2", begin), property("t1", end)), value(1))))
                        // исключаем из выборки пересекающиеся между собой периоды
                        .where(not(and(lt(property("t1", begin), property("t2", end)), gt(property("t1", end), property("t2", begin)))))
                        // промежуток должен начинаться раньше текущей даты
                        .where(le(property("t1", end), valueDate(now)))
                        .where(eq(property("t1", employee), property("t2", employee))).where(in(property("t1", "id"), relationBuilder.buildQuery())).where(in(property("t2", "id"), relationBuilder.buildQuery()))

                        // задаем условие для промежутка являющегося пустым: t3.end > t1.end & t2.begin > t3.begin
                        // для дат окончания, которые не заданы явно, либо выходят за пределы текущей даты, мы обрезаем значение по текущую дату
                        .where(notExists(gapIntersect.buildQuery()));

                // возвращаем общую сумму дней, всех пустых промежутков, группируя по сотруднику
                // вычисляем разницу между началом второго периода и концом первого.
                // промежуток между датами "01.01.2015 - 03.01.2015" по факту составляет 1 день, а разность дат даст нам 2 дня, потому из разности дат вычитаем еще 1 день
                DQLSelectBuilder sumSearchGap = new DQLSelectBuilder()
                        .fromDataSource(searchGap.buildQuery(), "gap")
                        .column(property("gap.gapEmpId"), "gapEmpId")
                        .column(sum(minus(diffdays(property("gap.gapEnd"), property("gap.gapStart")), value(1))), "gapSum")
                        .group(property("gap.gapEmpId"));

                // высчитываем разницу в днях, между периодами, исключая пустые промежутки
                DQLSelectBuilder diffdays = new DQLSelectBuilder()
                        .fromDataSource(totalMinMax.buildQuery(), "eh")
                        .joinDataSource("eh", DQLJoinType.left, sumSearchGap.buildQuery(), "gap", eq(property("eh.ehEmpId"), property("gap.gapEmpId")))
                        .column(property("eh.ehEmpId"), "ehEmpId")
                        .column(div(
                                minus(
                                        // в период должны быть включены также max и min значения, поэтому +1 день к их разности
                                        plus(diffdays(property("eh.max"), property("eh.min")), value(1)),
                                        coalesce(property("gap.gapSum"), value(0))
                                ), value(365)
                        ), "diff");

                DQLSelectBuilder builder = new DQLSelectBuilder().column(value(1))
                        .fromDataSource(diffdays.buildQuery(), "eh");

                if (_yearsNumberFrom.isActive())
                {
                    printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "serviceLengthData.yearsNumberFrom", _yearsNumberFrom.getData().toString());

                    long from = _yearsNumberFrom.getData();
                    builder.where(ge(property("eh.diff"), value(from)));
                }

                if (_yearsNumberTo.isActive())
                {
                    printInfo.addPrintParam(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET, "serviceLengthData.yearsNumberTo", _yearsNumberTo.getData().toString());

                    long to = _yearsNumberTo.getData();
                    builder.where(le(property("eh.diff"), value(to)));
                }

                ehiBuilder.where(exists(
                        builder.where(eq(property("empHistItm", employee), property("eh.ehEmpId"))).buildQuery()
                ));
            }

            dql.builder.where(exists(
                    ehiBuilder.column(value(1))
                            .where(eq(property("empHistItm", EmploymentHistoryItemBase.employee().id()), property(Employee.id().fromAlias(employeeAlias(dql)))))
                            .buildQuery()
            ));
        }
    }

    // Getters

    public IReportParam<Long> getYearsNumberFrom()
    {
        return _yearsNumberFrom;
    }

    public IReportParam<Long> getYearsNumberTo()
    {
        return _yearsNumberTo;
    }

    public IReportParam<ServiceLengthType> getServiceLengthType()
    {
        return _serviceLengthType;
    }

    public boolean isDisableServiceLength()
    {
        return _disableServiceLength;
    }

    public void setDisableServiceLength(boolean disableServiceLength)
    {
        _disableServiceLength = disableServiceLength;
    }


    // Listeners

    public void changeYearsNumber()
    {
        if (_yearsNumberFrom.isActive() || _yearsNumberTo.isActive())
            _serviceLengthType.setActive(true);
        setDisableServiceLength(_yearsNumberFrom.isActive() || _yearsNumberTo.isActive());
    }
}
