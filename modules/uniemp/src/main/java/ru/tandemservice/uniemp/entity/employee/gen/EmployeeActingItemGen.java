package ru.tandemservice.uniemp.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.employee.EmployeeActingItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Факт исполнения обязанностей
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeActingItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniemp.entity.employee.EmployeeActingItem";
    public static final String ENTITY_NAME = "employeeActingItem";
    public static final int VERSION_HASH = 330969911;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_MISSING_EMPLOYEE_POST = "missingEmployeePost";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";

    private EmployeePost _employeePost;     // Сотрудник
    private EmployeePost _missingEmployeePost;     // Отсутствующий сотрудник
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Отсутствующий сотрудник.
     */
    public EmployeePost getMissingEmployeePost()
    {
        return _missingEmployeePost;
    }

    /**
     * @param missingEmployeePost Отсутствующий сотрудник.
     */
    public void setMissingEmployeePost(EmployeePost missingEmployeePost)
    {
        dirty(_missingEmployeePost, missingEmployeePost);
        _missingEmployeePost = missingEmployeePost;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeActingItemGen)
        {
            setEmployeePost(((EmployeeActingItem)another).getEmployeePost());
            setMissingEmployeePost(((EmployeeActingItem)another).getMissingEmployeePost());
            setBeginDate(((EmployeeActingItem)another).getBeginDate());
            setEndDate(((EmployeeActingItem)another).getEndDate());
            setOrgUnit(((EmployeeActingItem)another).getOrgUnit());
            setPostBoundedWithQGandQL(((EmployeeActingItem)another).getPostBoundedWithQGandQL());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeActingItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeActingItem.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeActingItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "missingEmployeePost":
                    return obj.getMissingEmployeePost();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "missingEmployeePost":
                    obj.setMissingEmployeePost((EmployeePost) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "missingEmployeePost":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "missingEmployeePost":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "missingEmployeePost":
                    return EmployeePost.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeActingItem> _dslPath = new Path<EmployeeActingItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeActingItem");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Отсутствующий сотрудник.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getMissingEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> missingEmployeePost()
    {
        return _dslPath.missingEmployeePost();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    public static class Path<E extends EmployeeActingItem> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private EmployeePost.Path<EmployeePost> _missingEmployeePost;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Отсутствующий сотрудник.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getMissingEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> missingEmployeePost()
        {
            if(_missingEmployeePost == null )
                _missingEmployeePost = new EmployeePost.Path<EmployeePost>(L_MISSING_EMPLOYEE_POST, this);
            return _missingEmployeePost;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeActingItemGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeActingItemGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniemp.entity.employee.EmployeeActingItem#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

        public Class getEntityClass()
        {
            return EmployeeActingItem.class;
        }

        public String getEntityName()
        {
            return "employeeActingItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
