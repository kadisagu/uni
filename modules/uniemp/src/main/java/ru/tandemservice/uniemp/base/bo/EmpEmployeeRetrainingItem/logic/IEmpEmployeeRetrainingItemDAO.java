/**
 *$Id$
 */
package ru.tandemservice.uniemp.base.bo.EmpEmployeeRetrainingItem.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniemp.entity.employee.EmployeeRetrainingItem;

/**
 * Create by ashaburov
 * Date 28.03.12
 */
public interface IEmpEmployeeRetrainingItemDAO extends INeedPersistenceSupport
{
    void saveOrUpdateItem(EmployeeRetrainingItem item);
}
