/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.StaffListRegistryChangeState;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.info.ErrorCollector;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.StaffListState;
import ru.tandemservice.uniemp.entity.employee.StaffList;

/**
 * @author ashaburov
 * Created on: 08.08.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        //заполняем список версий ШР с которыми будем работать по их идентификаторам
        for (Long id : model.getStaffListIdList())
                model.getStaffListList().add(get(StaffList.class, id));

        //заполняем исходное состояние элементов, берем из перого элемента в пришедшем массиве
        //массив не пустой, т.к. это проверяется перед созданием данной формы
        model.setStaffListState(model.getStaffListList().get(0).getStaffListState());

        List<StaffListState> staffListStatesList = new ArrayList<>();
        if (model.getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_FORMING))
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACCEPTING));
        else if (model.getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACCEPTING))
        {
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_REJECTED));
        }
        else if (model.getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACCEPTED))
        {
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACTIVE));
        }
        else if (model.getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ACTIVE))
        {
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ARCHIVE));
        }
        else if (model.getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_REJECTED) || model.getStaffListState().getCode().equals(UniempDefines.STAFF_LIST_STATUS_ARCHIVE))
            staffListStatesList.add(getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_FORMING));

        model.setStaffListStatesList(staffListStatesList);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        StaffListState activeStateCatalogItem = getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ACTIVE);
        StaffListState archiveStateCatalogItem = getCatalogItem(StaffListState.class, UniempDefines.STAFF_LIST_STATUS_ARCHIVE);

        //если выбранно активное состояние, то все активные ШР на этом подразделении переводим в архивное состояние, простовляя дату архивации,
        //т.к. на подразделении может быть только одно активное ШР
        if (UniempDefines.STAFF_LIST_STATUS_ACTIVE.equals(model.getChoseStaffListState().getCode()))
        {
            for (StaffList staffList : model.getStaffListList())
            {
                staffList.setAcceptionDate(model.getAcceptionDate());
                staffList.setArchivingDate(null);
                staffList.setStaffListState(activeStateCatalogItem);

                Criteria crit = getSession().createCriteria(StaffList.class);
                crit.add(Restrictions.eq(StaffList.L_ORG_UNIT, staffList.getOrgUnit()));
                crit.add(Restrictions.ne(StaffList.P_ID, staffList.getId()));
                crit.add(Restrictions.eq(StaffList.L_STAFF_LIST_STATE, activeStateCatalogItem));
                for (StaffList slItem : (List<StaffList>)crit.list())
                {
                    slItem.setStaffListState(archiveStateCatalogItem);
                    slItem.setArchivingDate(staffList.getAcceptionDate());
                    getSession().update(slItem);
                }

                getSession().update(staffList);
            }
        }
        else if (UniempDefines.STAFF_LIST_STATUS_ARCHIVE.equals(model.getChoseStaffListState().getCode()))
            for (StaffList staffList : model.getStaffListList())
            {
                staffList.setArchivingDate(model.getArchivingDate());
                staffList.setStaffListState(archiveStateCatalogItem);
                getSession().update(staffList);
            }
        //во всех остальных случая удаляем дату архивации, на случай если ШР переводится из архивного состояния
        else
            for (StaffList staffList : model.getStaffListList())
            {
                staffList.setArchivingDate(null);
                staffList.setStaffListState(model.getChoseStaffListState());
                getSession().update(staffList);
            }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.isSettingToActive())
            for (StaffList firstStaffList : model.getStaffListList())
                for (StaffList secondStaffList : model.getStaffListList())
                    if (!firstStaffList.equals(secondStaffList) && firstStaffList.getOrgUnit().equals(secondStaffList.getOrgUnit()))
                        errors.add("На подразделении может быть только одна активная версия ШР.");
    }
}