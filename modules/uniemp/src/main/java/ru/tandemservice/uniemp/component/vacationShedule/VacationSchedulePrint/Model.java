/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniemp.component.vacationShedule.VacationSchedulePrint;

import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.employee.VacationSchedule;

/**
 * @author dseleznev
 * Created on: 26.01.2011
 */
@Input(keys = "vacationScheduleId", bindings = "vacationScheduleId")
public class Model
{
    private Long _vacationScheduleId;
    private VacationSchedule _vacationSchedule;

    public Long getVacationScheduleId()
    {
        return _vacationScheduleId;
    }

    public void setVacationScheduleId(Long vacationScheduleId)
    {
        this._vacationScheduleId = vacationScheduleId;
    }

    public VacationSchedule getVacationSchedule()
    {
        return _vacationSchedule;
    }

    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        this._vacationSchedule = vacationSchedule;
    }
}