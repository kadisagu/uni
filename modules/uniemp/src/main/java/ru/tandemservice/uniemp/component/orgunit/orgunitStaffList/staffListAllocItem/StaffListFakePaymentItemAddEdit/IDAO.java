/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListAllocItem.StaffListFakePaymentItemAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 11.05.2010
 */
public interface IDAO extends IUniDao<Model>
{
}