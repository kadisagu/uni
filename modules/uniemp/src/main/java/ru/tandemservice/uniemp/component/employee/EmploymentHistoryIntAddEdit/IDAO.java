/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.employee.EmploymentHistoryIntAddEdit;

import ru.tandemservice.uniemp.component.employee.EmploymentHistoryAbsAddEdit.EmploymentHistoryAbstractIDAO;

/**
 * @author dseleznev
 * Created on: 24.12.2008
 */
public interface IDAO extends EmploymentHistoryAbstractIDAO<Model>
{
}