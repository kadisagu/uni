/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniemp.component.settings.PaymentBaseValuesPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.employee.PaymentToPostBaseValue;

/**
 * @author dseleznev
 * Created on: 29.09.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        preparePostsDataSourse(component);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickAddPaymentToPostBaseValueRelation(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.PAYMENT_BASE_VALUE_ADD_EDIT, new ParametersMap().add("paymentToPostBaseValueRelId", null).add("paymentId", getModel(component).getPayment().getId())));
    }

    public void onClickEditPaymentToPostBaseValueRelation(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.PAYMENT_BASE_VALUE_ADD_EDIT, new ParametersMap().add("paymentToPostBaseValueRelId", component.getListenerParameter()).add("paymentId", getModel(component).getPayment().getId())));
    }

    public void onClickDeletePaymentToPostBaseValueRelation(IBusinessComponent component)
    {
        getDao().delete((Long)component.getListenerParameter());
        getModel(component).getDataSource().refresh();
    }

    public void preparePostsDataSourse(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<PaymentToPostBaseValue> dataSource = new DynamicListDataSource<>(component, this);
        dataSource.addColumn(new SimpleColumn("Название должности", PaymentToPostBaseValue.POST_TITLE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип должн.", PaymentToPostBaseValue.POST_TYPE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ПКГ", PaymentToPostBaseValue.PROF_QUALIFICATION_GROUP_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("КУ", PaymentToPostBaseValue.QUALIFICATION_LEVEL_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Базовый оклад", PaymentToPostBaseValue.BASE_SALARY_KEY,  DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Р", PaymentToPostBaseValue.ETKS_LEVEL_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Базовое значение выплаты", PaymentToPostBaseValue.P_VALUE,  DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditPaymentToPostBaseValueRelation"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeletePaymentToPostBaseValueRelation", "Удалить базовое значение выплаты для должности «{0}»?", PaymentToPostBaseValue.postBoundedWithQGandQL().title().s()));
        model.setDataSource(dataSource);
    }
}
