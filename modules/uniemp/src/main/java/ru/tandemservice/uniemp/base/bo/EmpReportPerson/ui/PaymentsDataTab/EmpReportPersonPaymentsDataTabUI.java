/* $Id$ */
package ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.employeebase.base.bo.EmployeeReportPerson.ui.Add.EmployeeReportPersonAddUI;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.benefitData.BenefitDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.benefitData.BenefitDataParam;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.paymentData.PaymentDataBlock;
import ru.tandemservice.uniemp.base.bo.EmpReportPerson.ui.PaymentsDataTab.block.paymentData.PaymentDataParam;

/**
 * @author Vasily Zhukov
 * @since 05.08.2011
 */
public class EmpReportPersonPaymentsDataTabUI extends UIPresenter implements IReportDQLModifierOwner
{
    private PaymentDataParam _paymentData = new PaymentDataParam();
    private BenefitDataParam _benefitData = new BenefitDataParam();

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        PaymentDataBlock.onBeforeDataSourceFetch(dataSource, _paymentData);
        BenefitDataBlock.onBeforeDataSourceFetch(dataSource, _benefitData);
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
        
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (printInfo.isExists(EmployeeReportPersonAddUI.EMPLOYEE_SCHEET))
        {
            // фильтры модифицируют запросы
            _paymentData.modify(dql, printInfo);
            _benefitData.modify(dql, printInfo);

            // печатные блоки модифицируют запросы и создают печатные колонки
        }
    }
    
    // Getters

    public PaymentDataParam getPaymentData()
    {
        return _paymentData;
    }

    public BenefitDataParam getBenefitData()
    {
        return _benefitData;
    }
}
