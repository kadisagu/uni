/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.orgunit.orgunitStaffList.staffListItem.StaffListItemPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uniemp.IUniempComponents;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.*;

/**
 * @author dseleznev
 * Created on: 19.09.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareListDataSource(component);
    }
    
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        IMergeRowIdResolver mergeRowResolver = entity -> ((StaffListPostPayment) entity).getPayment().getTitle();

        DynamicListDataSource<StaffListPostPayment> dataSource = new DynamicListDataSource<>(component, this, 10);
        dataSource.addColumn(new SimpleColumn("Выплата", StaffListPostPayment.PAYMENT_KEY).setMergeRowIdResolver(mergeRowResolver).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип выплаты", StaffListPostPayment.PAYMENT_TYPE_KEY).setMergeRowIdResolver(mergeRowResolver).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Значение выплаты", StaffListPostPayment.P_AMOUNT, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формат ввода выплаты", StaffListPostPayment.PAYMENT_UNIT_TYPE_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Сумма выплаты", StaffListPostPayment.P_TOTAL_VALUE, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", StaffListPostPayment.L_FINANCING_SOURCE + "." + FinancingSource.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", StaffListPostPayment.L_FINANCING_SOURCE_ITEM + "." + FinancingSourceItem.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditPayment").setDisabledProperty(StaffListPostPayment.P_EDITING_DISABLED).setPermissionKey("editStaffListPostPayment"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeletePayment", "Удалить выплату «{0}» из должности «{1}» штатного расписания?", new Object[] { new String[] { StaffListPostPayment.L_PAYMENT + "." + Payment.P_TITLE }, new String[] { StaffListPostPayment.L_STAFF_LIST_ITEM + "." + StaffListItem.L_ORG_UNIT_POST_RELATION + "." + OrgUnitPostRelation.P_TITLE_WITH_SALARY } }).setDisabledProperty(StaffListPostPayment.P_DELETE_DISABLED).setPermissionKey("deleteStaffListPostPayment"));
        model.setDataSource(dataSource);
    }
    
    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_ITEM_ADDEDIT));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().delete(getModel(component).getStaffListItem());
        deactivate(component);
    }

    public void onClickAddPayment(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_POST_PAYMENT_ADDEDIT, new ParametersMap().add("staffListItemId", getModel(component).getStaffListItem().getId())));
    }

    public void onClickAddFakePayment(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_FAKE_PAYMENT_ADDEDIT, new ParametersMap().add("staffListItemId", getModel(component).getStaffListItem().getId())));
    }

    public void onClickEditPayment(IBusinessComponent component)
    {
        StaffListPaymentBase payment = getDao().get((Long)component.getListenerParameter());
        if(null != payment && payment instanceof StaffListFakePayment)
            component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_FAKE_PAYMENT_ADDEDIT, new ParametersMap().add("staffListFakePaymentId", component.getListenerParameter())));
        else
            component.createDefaultChildRegion(new ComponentActivator(IUniempComponents.ORG_UNIT_STAFF_LIST_POST_PAYMENT_ADDEDIT, new ParametersMap().add("staffListPostPaymentId", component.getListenerParameter())));
    }
    
    public void onClickDeletePayment(IBusinessComponent component)
    {
        getDao().deleteStaffListPostPayment(getModel(component).getStaffListItem(), (Long)component.getListenerParameter());
    }
}