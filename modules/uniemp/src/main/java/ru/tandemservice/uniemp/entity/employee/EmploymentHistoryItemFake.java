package ru.tandemservice.uniemp.entity.employee;

import ru.tandemservice.uniemp.entity.employee.gen.*;

/**
 * Элемент Стажа 
 */
public class EmploymentHistoryItemFake extends EmploymentHistoryItemFakeGen
{
    @Override
    public String getOrganizationTitle()
    {
        throw new IllegalStateException("don't invoked this method");
    }

    @Override
    public String getOrgUnitTitle()
    {
        throw new IllegalStateException("don't invoked this method");
    }

    @Override
    public String getPostTitle()
    {
        throw new IllegalStateException("don't invoked this method");
    }
}