/**
 * $Id$
 */
package ru.tandemservice.uniemp.component.reports.profLectStaffConsistency.Add;

import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.ScienceDegreeType;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.IUniempDAO;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniemp.util.UniempReportUtil;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 13.04.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReportDate(new Date());
        model.setOrgUnitTypesList(getCatalogItemList(OrgUnitType.class));
        model.setOrgUnitsListModel(new OrgUnitMultiSelectModel(model, this));
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<IEntity> getValues(Class clazz, Set idsList)
    {
        return (List<IEntity>) getSession().createCriteria(clazz).add(Restrictions.in(IEntity.P_ID, idsList)).list();
    }
    
    @Override
    public List<OrgUnit> getOrgUnitsList(OrgUnitType orgUnitType, String filter)
    {
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        builder.add(MQExpression.eq("ou", OrgUnit.L_ORG_UNIT_TYPE, orgUnitType));
        builder.addOrder("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_TITLE);
        builder.addOrder("ou", OrgUnit.P_TITLE);
        return builder.getResultList(getSession());
    }
    
    @Override
    public Integer preparePrintReport(Model model)
    {
        RtfInjectModifier paramModifier = new RtfInjectModifier();
        paramModifier.put("reportDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getReportDate()));
        String orgUnitType = model.isOrgUnitTypeActive() ? model.getOrgUnitType().getDativePlural() : "ОУ";
        paramModifier.put("orgUnitType", orgUnitType);
        
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.getName2data().putAll(prepareTablesData(model));
        
        return UniempReportUtil.preparePrintingReport(getCatalogItem(EmployeeTemplateDocument.class, UniempDefines.TEMPLATE_PROF_LECT_CONSISTENCY), paramModifier, tableModifier);
    }
    
    private Map<String, String[][]> prepareTablesData(Model model)
    {
        Map<String, String[][]> result = new HashMap<>();

        List<OrgUnit> orgUnitsList = getOrgUnitsList(model);
        Map<Long, Integer> totalOrgUnitMap = new HashMap<>();
        Map<Person, ScienceDegreeType> scienceDegreesMap = IUniempDAO.instance.get().getEmployeeDegreesMap(null);
        Map<Long, Integer> doctorsMap = new HashMap<>();
        Map<Long, Integer> mastersMap = new HashMap<>();
        totalOrgUnitMap.put(0L, 0);
        doctorsMap.put(0L, 0);
        mastersMap.put(0L, 0);

        for (EmployeePost post : getEmployeePostsList(model))
        {
            Long orgUnitId = post.getOrgUnit().getId();
            Integer postsCnt = totalOrgUnitMap.get(orgUnitId);
            if (null == postsCnt) postsCnt = 0;
            totalOrgUnitMap.put(orgUnitId, ++postsCnt);
            totalOrgUnitMap.put(0L, totalOrgUnitMap.get(0L) + 1);

            ScienceDegreeType degreeType = scienceDegreesMap.get(post.getEmployee().getPerson());
            if (null != degreeType)
            {
                if (UniDefines.SCIENCE_DEGREE_TYPE_DOCTOR.equals(degreeType.getCode()))
                {
                    Integer doctorsCnt = doctorsMap.get(orgUnitId);
                    if (null == doctorsCnt) doctorsCnt = 0;
                    doctorsMap.put(orgUnitId, ++doctorsCnt);
                    doctorsMap.put(0L, doctorsMap.get(0L) + 1);
                }
                else if (UniDefines.SCIENCE_DEGREE_TYPE_MASTER.equals(degreeType.getCode()))
                {
                    Integer mastersCnt = mastersMap.get(orgUnitId);
                    if (null == mastersCnt) mastersCnt = 0;
                    mastersMap.put(orgUnitId, ++mastersCnt);
                    mastersMap.put(0L, mastersMap.get(0L) + 1);
                }
            }
        }

        DoubleFormatter formatter = DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;
        List<String[]> resultList = new ArrayList<>();
        for (OrgUnit orgUnit : orgUnitsList)
        {
            String[] line = new String[8];
            Long orgUnitId = orgUnit.getId();
            line[0] = orgUnit.getFullTitle();
            
            if (!totalOrgUnitMap.containsKey(orgUnitId))
                line = new String[] { orgUnit.getFullTitle(), "0", "0", "0", "0", "0", "0", "0" };
            else
            {

                Integer total = totalOrgUnitMap.get(orgUnitId);
                line[1] = total.toString();

                Integer doctorsCnt = doctorsMap.containsKey(orgUnitId) ? doctorsMap.get(orgUnitId) : 0;
                line[2] = doctorsCnt.toString();
                line[3] = total != 0 ? formatter.format(100 * doctorsCnt / Double.valueOf(total)) : "0";

                Integer mastersCnt = mastersMap.containsKey(orgUnitId) ? mastersMap.get(orgUnitId) : 0;
                line[4] = mastersCnt.toString();
                line[5] = total != 0 ? formatter.format(100 * mastersCnt / Double.valueOf(total)) : "0";

                Integer ppsCnt = doctorsCnt + mastersCnt;
                line[6] = ppsCnt.toString();
                line[7] = total != 0 ? formatter.format(100 * ppsCnt / Double.valueOf(total)) : "0";
            }

            resultList.add(line);
        }
        result.put("T", resultList.toArray(new String[][] {}));

        Integer total = totalOrgUnitMap.get(0L);
        Integer doctorsCnt = doctorsMap.get(0L);
        String doctorPercent = total != 0 ? formatter.format(100 * doctorsCnt / Double.valueOf(total)) : "0";
        Integer mastersCnt = mastersMap.get(0L);
        String masterPercent = total != 0 ? formatter.format(100 * mastersCnt / Double.valueOf(total)) : "0";
        Integer ppsCnt = doctorsCnt + mastersCnt;
        String ppsPercent = total != 0 ? formatter.format(100 * ppsCnt / Double.valueOf(total)) : "0";
        result.put("TS", new String[][] { { "Итого", total.toString(), doctorsCnt.toString(), doctorPercent, mastersCnt.toString(), masterPercent, ppsCnt.toString(), ppsPercent } });

        return result;
    }
    
    private List<OrgUnit> getOrgUnitsList(Model model)
    {
        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");
        if(model.isOrgUnitTypeActive())
            builder.add(MQExpression.eq("ou", OrgUnit.L_ORG_UNIT_TYPE, model.getOrgUnitType()));
        
        if(model.isOrgUnitActive())
        {
            List<Long> orgUnitIdsList = new ArrayList<>();
            for(OrgUnit orgUnit : model.getOrgUnitsList()) orgUnitIdsList.add(orgUnit.getId());
            builder.add(MQExpression.in("ou", OrgUnit.P_ID, orgUnitIdsList)); 
        }
        
        builder.addOrder("ou", OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_PRIORITY);
        builder.addOrder("ou", OrgUnit.P_TITLE);
        
        return builder.getResultList(getSession());
    }
    
    private List<EmployeePost> getEmployeePostsList(Model model)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.add(MQExpression.in("ep", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.L_EMPLOYEE_TYPE, EmployeeManager.instance().dao().getEmployeeTypeWithAllChilds(getCatalogItem(EmployeeType.class, EmployeeTypeCodes.EDU_STAFF))));
        builder.add(MQExpression.lessOrEq("ep", EmployeePost.P_POST_DATE, model.getReportDate()));
        AbstractExpression expr1 = MQExpression.isNull("ep", EmployeePost.P_DISMISSAL_DATE);
        AbstractExpression expr2 = MQExpression.greatOrEq("ep", EmployeePost.P_DISMISSAL_DATE, model.getReportDate());
        builder.add(MQExpression.or(expr1, expr2));
        
        if(model.isOrgUnitTypeActive())
            builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE, model.getOrgUnitType()));
        if(model.isOrgUnitActive())
            builder.add(MQExpression.in("ep", EmployeePost.L_ORG_UNIT, model.getOrgUnitsList()));

        builder.addOrder("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_PRIORITY);
        builder.addOrder("ep", EmployeePost.L_ORG_UNIT + "." + OrgUnit.P_TITLE);
        
        return builder.getResultList(getSession());
    }
}