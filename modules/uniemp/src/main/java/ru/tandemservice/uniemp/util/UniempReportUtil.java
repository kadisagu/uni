/**
 * $Id$
 */
package ru.tandemservice.uniemp.util;

import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;

/**
 * @author dseleznev
 * Created on: 13.04.2009
 */
public class UniempReportUtil
{
    public static Integer preparePrintingReport(EmployeeTemplateDocument template, RtfInjectModifier paramModifier, RtfTableModifier tableModifier)
    {
        if(null == template)
            throw new RuntimeException("There are no any templates specified");
            
        final RtfDocument document = new RtfReader().read(template.getContent());

        if(null != paramModifier)
        {
            paramModifier.modify(document);
        }

        if(null != tableModifier)
        {
            tableModifier.modify(document);
        }

        IDocumentRenderer renderer = new CommonBaseRenderer().rtf().fileName(template.getPath().substring(template.getPath().lastIndexOf("/") + 1)).document(document);
        
        // регистрируем его в универсальном компоненте рендера
        return PrintReportTemporaryStorage.registerTemporaryPrintForm(RtfUtil.toByteArray(document), renderer.getFilename());
    }

}