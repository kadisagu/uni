package unipractice.scripts.practiceOrders

import com.google.common.collect.ImmutableMap
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.unipractice.entity.PrPracticeAssignment

/**
 * @author Andrey Avetisov
 * @since 07.10.2014
 */
return new PracticeAssignmentPrint(
        session: session,
        template: template,
        prPracticeAssignment: session.get(PrPracticeAssignment.class, object)
).print()

class PracticeAssignmentPrint
{
    Session session
    byte[] template
    PrPracticeAssignment prPracticeAssignment

    static final PRACTICE_NAME_MAP = ImmutableMap.of(
            EppRegistryStructureCodes.REGISTRY_PRACTICE_PRE_DIPLOMA, "преддипломной",
            EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION, "производственной",
            EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING, "учебной"
    )

    def print()
    {
        final im = new RtfInjectModifier()
        final document = new RtfReader().read(template)

        final student = prPracticeAssignment.practice.student
        final topOrgUnit = TopOrgUnit.getInstance()

        def settlement = topOrgUnit.legalAddress?.settlement?.titleWithType ?: topOrgUnit.address?.settlement?.titleWithType
        def practicePlace = prPracticeAssignment.practiceBase.title + (settlement != null ? ', ' + settlement : '')

        im.put("highSchoolTitle", topOrgUnit.title)
        im.put("studentFio", student.person.identityCard.fullFio)
        im.put("course", student.course.title)
        im.put("group", student.group.title)
        im.put("compensationType", student.compensationType.shortTitle)
        im.put("targetAdmissionOrgUnit", student.targetAdmissionOrgUnit?.title)
        im.put("practiceKind_G", PRACTICE_NAME_MAP.get(prPracticeAssignment.practice.registryElementPart.registryElement.parent.code))
        im.put("practicePlace", practicePlace)
        im.put("beginDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(prPracticeAssignment.dateStart))
        im.put("endDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(prPracticeAssignment.dateEnd))

        im.modify(document)
        return [document: document, fileName: 'PracticeAssignment.rtf']
    }
}