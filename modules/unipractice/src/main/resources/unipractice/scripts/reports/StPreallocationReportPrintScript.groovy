package unipractice.scripts.reports

import org.hibernate.Session
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes
import ru.tandemservice.unipractice.entity.PrPracticeAssignment
import ru.tandemservice.unipractice.entity.StPracticePreallocation

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 10.10.2014
 */
return new StPreallocationReportPrint(
        session: session,
        template: template,
        practicePreallocation: practicePreallocation,
        orgUnitId: orgUnitId

).print()

class StPreallocationReportPrint
{
    Session session
    byte[] template
    StPracticePreallocation practicePreallocation
    Long orgUnitId
    OrgUnit orgUnit

    def print()
    {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()

        if (orgUnitId!=null)
        {
            orgUnit = DataAccessServices.dao().get(OrgUnit.class, orgUnitId)
        }
        List<String[]> practiceAssignmentData = new ArrayList<>();

        def date = RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(new Date())
        def course = practicePreallocation.course
        def programSubject = practicePreallocation.educationLevelsHighSchool
        def practiceKind_A = ""
        def beginDate = RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(practicePreallocation.practiceBeginDate)
        def endDate = RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(practicePreallocation.practiceEndDate)
        def T = ""
        EppRegistryStructure registryStructure = DataAccessServices.dao().get(EppRegistryStructure.class, EppRegistryStructure.P_TITLE, practicePreallocation.practiceKind)
        switch (registryStructure.code)
        {
            case EppRegistryStructureCodes.REGISTRY_PRACTICE_PRE_DIPLOMA: practiceKind_A = "преддипломную "
                break;
            case EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION: practiceKind_A = "производственную "
                break;
            case EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING: practiceKind_A = "учебную "
                break;
        }

        fillTable(practiceAssignmentData)

        im.put("date", date);
        im.put("course", course);
        im.put("programSubject", programSubject);
        im.put("practiceKind_A", practiceKind_A);
        im.put("beginDate", beginDate);
        im.put("endDate", endDate);
        tm.put("T", practiceAssignmentData as String[][])

        RtfDocument document = new RtfReader().read(template);
        im.modify(document)
        tm.modify(document)
        return [document: document, fileName: 'PracticeAssignment.rtf']
    }

    def fillTable(List<String[]> practiceAssignmentData)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "p")
        .where(eqValue(property("p", PrPracticeAssignment.practice().year().educationYear().title()), practicePreallocation.educationYear))
        .where(eqValue(property("p", PrPracticeAssignment.practice().registryElementPart().registryElement().parent().title()),practicePreallocation.practiceKind))
        .where(eqValue(property("p", PrPracticeAssignment.dateStart()),practicePreallocation.practiceBeginDate))
        .where(eqValue(property("p", PrPracticeAssignment.dateEnd()),practicePreallocation.practiceEndDate))
        .where(eqValue(property("p", PrPracticeAssignment.practice().student().educationOrgUnit().educationLevelHighSchool().fullTitle()), practicePreallocation.educationLevelsHighSchool))

        List<String> courseList = Arrays.asList(practicePreallocation.course.split(";"))
        dql.where(DQLExpressions.in(property("p", PrPracticeAssignment.practice().student().course().title()), courseList))
        if(practicePreallocation.group!=null)
        {
            List<String> groupList = Arrays.asList(practicePreallocation.group.split(";"))
            dql.where(DQLExpressions.in(property("p", PrPracticeAssignment.practice().student().group().title()), groupList))
        }
        if (orgUnit!=null)
        {
            dql.where(eqValue(property("p", PrPracticeAssignment.practice().student().educationOrgUnit().educationLevelHighSchool().orgUnit()), orgUnit))
        }

        List<PrPracticeAssignment> practiceAssignmentList = DataAccessServices.dao().getList(dql);

        int number = 0;
        for(PrPracticeAssignment practiceAssignment: practiceAssignmentList)
        {
            number++;
            def student = practiceAssignment.practice.student
            def address = null==student.person.addressRegistration?"":student.person.addressRegistration.titleWithFlat
            def companyName = practiceAssignment.practice.student.targetAdmissionOrgUnit!=null?practiceAssignment.practice.student.targetAdmissionOrgUnit.title:"";
            def previousPracticePlace = ""
            def practiceBase = practiceAssignment.practiceBase.title
            PrPracticeAssignment previousPracitce = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "p")
                    .column("p").top(1)
                    .where(eqValue(property("p", PrPracticeAssignment.practice().student()), student))
                    .where(lt(property("p", PrPracticeAssignment.dateStart()),valueDate(practiceAssignment.dateStart)))
            .order(property("p", PrPracticeAssignment.dateStart()), OrderDirection.desc).createStatement(session).uniqueResult()

            if(previousPracitce!=null)
            {
                previousPracticePlace = previousPracitce.practiceBase.title
            }

            practiceAssignmentData.add([number, student.fullFio, address, student.compensationType.shortTitle, companyName, "",  previousPracticePlace, practiceBase] as String[])
        }
    }

}