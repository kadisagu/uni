package ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.util;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ChangePreDipAddEdit.PracticeParagraphChangePreDipAddEdit;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ChangeProizvAddEdit.PracticeParagraphChangeProizvAddEdit;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.PreDipAddEdit.PracticeParagraphPreDipAddEdit;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ProizvAddEdit.PracticeParagraphProizvAddEdit;

import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes.*;

public class ParagraphUtils {

    public static Class<? extends BusinessComponentManager> getParagraphEditComponentByOrderType(String code) {
        Class<? extends BusinessComponentManager> editClass;
        switch (code) {
            case PREDDIPLOM: editClass = PracticeParagraphPreDipAddEdit.class; break;
            case PROIZVODSTV: editClass = PracticeParagraphProizvAddEdit.class; break;
            case PREDDIPLOM_CHANGE: editClass = PracticeParagraphChangePreDipAddEdit.class; break;
            case PROIZVODSTV_CHANGE: editClass = PracticeParagraphChangeProizvAddEdit.class; break;
            default: throw new ApplicationException("Неизвестный тип приказа: " + code);
        }
        return editClass;
    }

}
