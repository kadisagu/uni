package ru.tandemservice.unipractice.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатные формы приказов о направлении на практику"
 * Имя сущности : prPracticeOrderPrintForm
 * Файл data.xml : unipractice.catalog.data.xml
 */
public interface PrPracticeOrderPrintFormCodes
{
    /** Константа кода (code) элемента : Приказ о направлении студентов на преддипломную практику (title) */
    String PRE_GRADUATION_PRACTICE_ORDER = "preGraduationPracticeOrder";
    /** Константа кода (code) элемента : Параграф приказа о направлении студентов на преддипломную практику (title) */
    String PARAGRAPH_PRE_GRADUATION_PRACTICE_ORDER = "paragraphPreGraduationPracticeOrder";
    /** Константа кода (code) элемента : Подпараграф приказа о направлении студентов на преддипломную практику (title) */
    String SUB_PARAGRAPH_PRE_GRADUATION_PRACTICE_ORDER = "subParagraphPreGraduationPracticeOrder";
    /** Константа кода (code) элемента : Приказ о направлении студентов на производственную практику (title) */
    String INDUSTRIAL_PRACTICE_ORDER = "industrialPracticeOrder";
    /** Константа кода (code) элемента : Параграф приказа о направлении студентов на производственную практику (title) */
    String PARAGRAPH_INDUSTRIAL_PRACTICE_ORDER = "paragraphIndustrialPracticeOrder";
    /** Константа кода (code) элемента : Подпараграф приказа о направлении студентов на производственную практику (title) */
    String SUB_PARAGRAPH_INDUSTRIAL_PRACTICE_ORDER = "subParagraphIndustrialPracticeOrder";
    /** Константа кода (code) элемента : Направление на производственную практику (title) */
    String PRACTICE_ASSIGNMENT = "practiceAssignment";
    /** Константа кода (code) элемента : Предварительное распределение студентов (title) */
    String STUDENT_PREALLOCATION = "stPreallocationReport";
    /** Константа кода (code) элемента : Приказ об изменении приказа о направлении студентов на преддипломную практику (title) */
    String EDIT_PRE_GRADUATION_PRACTICE_ORDER = "editPreGraduationPracticeOrder";
    /** Константа кода (code) элемента : Параграф приказа об изменении приказа о направлении студентов на преддипломную практику (title) */
    String PARAGRAPH_EDIT_PRE_GRADUATION_PRACTICE_ORDER = "paragraphEditPreGraduationPracticeOrder";
    /** Константа кода (code) элемента : Подпараграф приказа об изменении приказа о направлении студентов на преддипломную практику (title) */
    String SUB_PARAGRAPH_EDIT_PRE_GRADUATION_PRACTICE_ORDER = "subParagraphEditPreGraduationPracticeOrder";
    /** Константа кода (code) элемента : Приказ об изменении приказа о направлении студентов на производственную практику (title) */
    String EDIT_INDUSTRIAL_PRACTICE_ORDER = "editIndustrialPracticeOrder";
    /** Константа кода (code) элемента : Параграф приказа об изменении приказа о направлении студентов на производственную практику (title) */
    String PARAGRAPH_EDIT_INDUSTRIAL_PRACTICE_ORDER = "paragraphEditIndustrialPracticeOrder";
    /** Константа кода (code) элемента : Подпараграф приказа об изменении приказа о направлении студентов на производственную практику (title) */
    String SUB_PARAGRAPH_EDIT_INDUSTRIAL_PRACTICE_ORDER = "subParagraphEditIndustrialPracticeOrder";

    Set<String> CODES = ImmutableSet.of(PRE_GRADUATION_PRACTICE_ORDER, PARAGRAPH_PRE_GRADUATION_PRACTICE_ORDER, SUB_PARAGRAPH_PRE_GRADUATION_PRACTICE_ORDER, INDUSTRIAL_PRACTICE_ORDER, PARAGRAPH_INDUSTRIAL_PRACTICE_ORDER, SUB_PARAGRAPH_INDUSTRIAL_PRACTICE_ORDER, PRACTICE_ASSIGNMENT, STUDENT_PREALLOCATION, EDIT_PRE_GRADUATION_PRACTICE_ORDER, PARAGRAPH_EDIT_PRE_GRADUATION_PRACTICE_ORDER, SUB_PARAGRAPH_EDIT_PRE_GRADUATION_PRACTICE_ORDER, EDIT_INDUSTRIAL_PRACTICE_ORDER, PARAGRAPH_EDIT_INDUSTRIAL_PRACTICE_ORDER, SUB_PARAGRAPH_EDIT_INDUSTRIAL_PRACTICE_ORDER);
}
