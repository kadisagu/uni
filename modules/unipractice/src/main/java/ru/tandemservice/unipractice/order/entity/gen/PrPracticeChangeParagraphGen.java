package ru.tandemservice.unipractice.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unipractice.order.entity.PrAbstractParagraph;
import ru.tandemservice.unipractice.order.entity.PrPracticeChangeParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа об изменении приказа на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeChangeParagraphGen extends PrAbstractParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.order.entity.PrPracticeChangeParagraph";
    public static final String ENTITY_NAME = "prPracticeChangeParagraph";
    public static final int VERSION_HASH = -889989109;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PrPracticeChangeParagraphGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeChangeParagraphGen> extends PrAbstractParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeChangeParagraph.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeChangeParagraph();
        }
    }
    private static final Path<PrPracticeChangeParagraph> _dslPath = new Path<PrPracticeChangeParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeChangeParagraph");
    }
            

    public static class Path<E extends PrPracticeChangeParagraph> extends PrAbstractParagraph.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return PrPracticeChangeParagraph.class;
        }

        public String getEntityName()
        {
            return "prPracticeChangeParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
