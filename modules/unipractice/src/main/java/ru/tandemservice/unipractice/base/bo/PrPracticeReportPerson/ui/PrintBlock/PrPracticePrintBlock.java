/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.PrintBlock;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.springframework.util.ClassUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeResult;
import ru.tandemservice.unipractice.entity.catalog.PrEmploymentForm;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 10/28/14
 */
public class PrPracticePrintBlock implements IReportPrintBlock
{
    public static final String PREFETCH_STUDENT_PRACTICE_EMPLOYMENT_FORM_MAP = "studentPracticeEmploymentFormMap";

    private IReportPrintCheckbox _block = new ReportPrintCheckbox();
    private IReportPrintCheckbox _practiceCompleted = new ReportPrintCheckbox();
    private IReportPrintCheckbox _practiceArrivedPlace = new ReportPrintCheckbox();
    private IReportPrintCheckbox _practiceDistributed = new ReportPrintCheckbox();
    private IReportPrintCheckbox _practiceDelay = new ReportPrintCheckbox();
    private IReportPrintCheckbox _practiceDelayMoreTenDays = new ReportPrintCheckbox();
    private IReportPrintCheckbox _practiceWithPayment = new ReportPrintCheckbox();
    private IReportPrintCheckbox _practiceEmploymentForm = new ReportPrintCheckbox();
    private IReportPrintCheckbox _practiceDaysSustained = new ReportPrintCheckbox();


    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 8; i++)
            ids.add("chPractice" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }


    @Override
    public void modify(ReportDQL dql, final IReportPrintInfo printInfo)
    {
        String studentAlias = dql.innerJoinEntity(Student.class, Student.person());

        if(_practiceCompleted.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prc")
                    .where(eq(property(studentAlias), property("prc", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prc", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(eq(property("prc", PrPracticeResult.completed()),
                            value(true)));

            DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprc")
                    .where(eq(property(studentAlias), property("fprc", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("fprc", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(eq(property("fprc", PrPracticeResult.completed()),
                            value(false)));

            final int i = dql.addLastAggregateColumn(
                    new DQLCaseExpressionBuilder()
                            .when(exists(query.buildQuery()), value(true))
                            .otherwise(
                                    new DQLCaseExpressionBuilder()
                                            .when(and(
                                                    exists(falseQuery.buildQuery()),
                                                    notExists(query.buildQuery())
                                            ), value(false))
                                            .otherwise(nul())
                                            .build()
                            )
                            .build());

            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("practiceCompleted", i, null, new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source == null ? null : (source ? "да" : "нет");
                }
            }));


        }


        if(_practiceArrivedPlace.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prap")
                    .where(eq(property(studentAlias), property("prap", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prap", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)));


            DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "fpap")
                    .where(eq(property(studentAlias), property("fpap", PrPracticeAssignment.practice().student())))
                    .where(eq(property("fpap", PrPracticeAssignment.practice().year().educationYear().current()), value(true)));

            final int i = dql.addLastAggregateColumn(
                    new DQLCaseExpressionBuilder()
                            .when(exists(query.buildQuery()), value(true))
                            .otherwise(
                                    new DQLCaseExpressionBuilder()
                                            .when(and(
                                                    exists(falseQuery.buildQuery()),
                                                    notExists(query.buildQuery())
                                            ), value(false))
                                            .otherwise(nul())
                                            .build()
                            )
                            .build());

            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("practiceArrivedPlace", i, null, new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source == null ? null : (source ? "да" : "нет");
                }
            }));
        }
        if(_practiceDistributed.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "pad")
                    .where(eq(property(studentAlias), property("pad", PrPracticeAssignment.practice().student())))
                    .where(eq(property("pad", PrPracticeAssignment.practice().year().educationYear().current()), value(true)))
                    .where(in(property("pad", PrPracticeAssignment.state().code()),
                            Lists.newArrayList(PrPracticeAssignmentStateCodes.ACCEPTED,
                                    PrPracticeAssignmentStateCodes.FINISHED,
                                    PrPracticeAssignmentStateCodes.FOR_APPROVAL)));

            DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "fpad")
                    .where(eq(property(studentAlias), property("fpad", PrPracticeAssignment.practice().student())))
                    .where(eq(property("fpad", PrPracticeAssignment.practice().year().educationYear().current()), value(true)))
                    .where(in(property("fpad", PrPracticeAssignment.state().code()),
                            Lists.newArrayList(PrPracticeAssignmentStateCodes.FORMING,
                                    PrPracticeAssignmentStateCodes.ACCEPTABLE)));

            final int i = dql.addLastAggregateColumn(
                    new DQLCaseExpressionBuilder()
                            .when(exists(query.buildQuery()), value(true))
                            .otherwise(
                                    new DQLCaseExpressionBuilder()
                                            .when(and(
                                                    exists(falseQuery.buildQuery()),
                                                    notExists(query.buildQuery())
                                            ), value(false))
                                            .otherwise(nul())
                                            .build()
                            )
                            .build());

            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("practiceDistributed", i, null, new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source == null ? null : (source ? "да" : "нет");
                }
            }));
        }
        if(_practiceDelay.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prd")
                    .where(eq(property(studentAlias), property("prd", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prd", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(gt(property("prd", PrPracticeResult.dateStart()), property("prd", PrPracticeResult.practiceAssignment().dateStart())));

            DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprd")
                    .where(eq(property(studentAlias), property("fprd", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("fprd", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(le(property("fprd", PrPracticeResult.dateStart()), property("fprd", PrPracticeResult.practiceAssignment().dateStart())));

            final int i = dql.addLastAggregateColumn(
                    new DQLCaseExpressionBuilder()
                            .when(exists(query.buildQuery()), value(true))
                            .otherwise(
                                    new DQLCaseExpressionBuilder()
                                            .when(and(
                                                    exists(falseQuery.buildQuery()),
                                                    notExists(query.buildQuery())
                                            ), value(false))
                                            .otherwise(nul())
                                            .build()
                            )
                            .build());

            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("practiceDelay", i, null, new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source == null ? null : (source ? "да" : "нет");
                }
            }));
        }
        if(_practiceDelayMoreTenDays.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prdt")
                    .where(eq(property(studentAlias), property("prdt", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prdt", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(gt(property("prdt", PrPracticeResult.dateStart()), property("prdt", PrPracticeResult.practiceAssignment().dateStart())))
                    .where(gt(DQLFunctions.diffdays(property("prdt", PrPracticeResult.dateStart()), property("prdt", PrPracticeResult.practiceAssignment().dateStart())), value(10L)));

            DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprdt")
                    .where(eq(property(studentAlias), property("fprdt", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("fprdt", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)));

            final int i = dql.addLastAggregateColumn(
                    new DQLCaseExpressionBuilder()
                            .when(exists(query.buildQuery()), value(true))
                            .otherwise(
                                    new DQLCaseExpressionBuilder()
                                            .when(and(
                                                    exists(falseQuery.buildQuery()),
                                                    notExists(query.buildQuery())
                                            ), value(false))
                                            .otherwise(nul())
                                            .build()
                            )
                            .build());

            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("practiceDelayMoreTenDays", i, null, new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source == null ? null : (source ? "да" : "нет");
                }
            }));
        }
        if(_practiceWithPayment.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prp")
                    .where(eq(property(studentAlias), property("prp", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prp", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(eq(property("prp", PrPracticeResult.practicePayed()),
                            value(true)));

            DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprp")
                        .where(eq(property(studentAlias), property("fprp", PrPracticeResult.practiceAssignment().practice().student())))
                        .where(eq(property("fprp", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                        .where(eq(property("fprp", PrPracticeResult.practicePayed()),
                                value(false)));

            final int i = dql.addLastAggregateColumn(
                    new DQLCaseExpressionBuilder()
                            .when(exists(query.buildQuery()), value(true))
                            .otherwise(
                                    new DQLCaseExpressionBuilder()
                                            .when(and(
                                                    exists(falseQuery.buildQuery()),
                                                    notExists(query.buildQuery())
                                            ), value(false))
                                            .otherwise(nul())
                                            .build()
                            )
                            .build());

            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("practiceWithPayment", i, null, new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source == null ? null : (source ? "да" : "нет");
                }
            }));
        }
        if(_practiceEmploymentForm.isActive())
        {
            final int studentIndex = dql.addListAggregateColumn(studentAlias);

            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintListColumn("practiceEmploymentForm", studentIndex, null, studentObj -> {
                if (studentObj == null)
                    return null;

                Collection<PrEmploymentForm> employmentForms = getEmploymentForm(printInfo, (Student) studentObj);
                if (null == employmentForms)
                    return null;

                return CommonBaseStringUtil.join(employmentForms, PrEmploymentForm.shortTitle().s(), ", ");
            })
            {
                @Override
                public void prefetch(List<Object[]> rows)
                {
                    prefetchEmploymentForm(printInfo, rows, studentIndex);
                }
            });
        }
        if(_practiceDaysSustained.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prds")
                    .where(eq(property(studentAlias), property("prds", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prds", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(eq(
                            DQLFunctions.round(div(DQLFunctions.diffdays(property("prds", PrPracticeResult.practiceAssignment().dateEnd()), property("prds", PrPracticeResult.practiceAssignment().dateStart())), value(7d)), value(0)),
                            DQLFunctions.round(div(DQLFunctions.diffdays(property("prds", PrPracticeResult.dateEnd()), property("prds", PrPracticeResult.dateStart())), value(7d)), value(0))));

            DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprds")
                    .where(eq(property(studentAlias), property("fprds", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("fprds", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)));

            final int i = dql.addLastAggregateColumn(
                    new DQLCaseExpressionBuilder()
                            .when(exists(query.buildQuery()), value(true))
                            .otherwise(
                                    new DQLCaseExpressionBuilder()
                                            .when(and(
                                                    exists(falseQuery.buildQuery()),
                                                    notExists(query.buildQuery())
                                            ), value(false))
                                            .otherwise(nul())
                                            .build()
                            )
                            .build());
            printInfo.addPrintColumn(StudentReportPersonAddUI.STUDENT_SCHEET, new ReportPrintPathColumn("practiceDaysSustained", i, null, new IFormatter<Boolean>()
            {
                @Override
                public String format(Boolean source)
                {
                    return source == null ? null : (source ? "да" : "нет");
                }
            }));
        }
    }

    // Getters


    public IReportPrintCheckbox getBlock()
    {
        return _block;
    }

    public IReportPrintCheckbox getPracticeCompleted()
    {
        return _practiceCompleted;
    }

    public IReportPrintCheckbox getPracticeArrivedPlace()
    {
        return _practiceArrivedPlace;
    }

    public IReportPrintCheckbox getPracticeDistributed()
    {
        return _practiceDistributed;
    }

    public IReportPrintCheckbox getPracticeDelay()
    {
        return _practiceDelay;
    }

    public IReportPrintCheckbox getPracticeDelayMoreTenDays()
    {
        return _practiceDelayMoreTenDays;
    }

    public IReportPrintCheckbox getPracticeWithPayment()
    {
        return _practiceWithPayment;
    }

    public IReportPrintCheckbox getPracticeEmploymentForm()
    {
        return _practiceEmploymentForm;
    }

    public IReportPrintCheckbox getPracticeDaysSustained()
    {
        return _practiceDaysSustained;
    }


    private Collection<PrEmploymentForm> getEmploymentForm(IReportPrintInfo printInfo, Student student)
    {
        if (null == student)
            return Collections.emptyList();

        Map<Long, List<PrEmploymentForm>> map = printInfo.getSharedObject(PREFETCH_STUDENT_PRACTICE_EMPLOYMENT_FORM_MAP);

        return map.get(student.getId());
    }

    private void prefetchEmploymentForm(IReportPrintInfo printInfo, List<Object[]> rows, int studentIndex)
    {
        if (printInfo.getSharedObject(PREFETCH_STUDENT_PRACTICE_EMPLOYMENT_FORM_MAP) != null)
            return;

        final Map<Long, List<PrEmploymentForm>> result = new HashMap<>();
        printInfo.putSharedObject(PREFETCH_STUDENT_PRACTICE_EMPLOYMENT_FORM_MAP, result);

        final List<Long> studentIds = getStudentIds(rows, studentIndex);
        if (!studentIds.isEmpty())
        {
            final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "pr")
                    .column(property(PrPracticeResult.practiceAssignment().practice().student().id().fromAlias("pr")))
                    .column(property(PrPracticeResult.employmentForm().fromAlias("pr")))
                    .where(isNotNull(property(PrPracticeResult.employmentForm().fromAlias("pr"))))
                    .where(in(property(PrPracticeResult.practiceAssignment().practice().student().id().fromAlias("pr")), parameter("ids", PropertyType.LONG)))
                    .order(property(PrPracticeResult.practiceAssignment().practice().student().id().fromAlias("pr")))
                    .order(property(PrPracticeResult.employmentForm().title().fromAlias("pr")));

            DataAccessServices.dao().getCalculatedValue(session -> {
                IDQLStatement stmt = dql.createStatement(session);
                for (List<Long> ids : Iterables.partition(studentIds, DQL.MAX_VALUES_ROW_NUMBER))
                {
                    stmt.setParameterList("ids", ids);
                    for (Object[] row : stmt.<Object[]>list())
                    {
                        final Long studentId = (Long) row[0];
                        final PrEmploymentForm employmentForm = (PrEmploymentForm) row[1];

                        SafeMap.safeGet(result, studentId, ArrayList.class)
                                .add(employmentForm);
                    }
                }
                return null;
            });
        }
    }

    private static List<Long> getStudentIds(List<Object[]> rows, int studentListIndex)
    {
        return getEntityIds(rows, studentListIndex, 1, Student.class);
    }

    private static List<Long> getEntityIds(List<Object[]> rows, int entityListIndex, int maplevel, Class<?> check) {
        List<Long> ids = new ArrayList<>();
        for (Object[] row : rows) {
            _fillIdsFromMap(ids, row[entityListIndex], maplevel, check);
        }
        return ids;
    }

    private static void _fillIdsFromMap(Collection<Long> ids, Object o, int levels, Class<?> check)
    {
        // пустые значения пропускаем
        if (null == o) { return; }

        // если мы должны взять значение (ключ)
        if (0 == levels) {

            // предварительная обработка для Map.Entry
            if (o instanceof Map.Entry) {
                o = ((Map.Entry)o).getKey(); // значение берется из ключа
                if (null == o) { return; } // что, впринципе, бред... но чего только не случается
            }

            if (null != check) {
                if (!check.isInstance(o)) {
                    throw new ClassCastException(String.valueOf(ClassUtils.getUserClass(o)) + " is not instance of " + check);
                }
            }

            // обработка ключей
            if (o instanceof Long) { ids.add((Long)o); }
            else if (o instanceof IEntity) { ids.add(((IEntity) o).getId()); }
            else { throw new IllegalStateException(String.valueOf(ClassUtils.getUserClass(o))); }

            // значение получили - сохранили его в ids, модем выходить
            return;
        }

        // в противном случае - мы долдны получить список для дальнейшей обработки

        // предварительная обработка для Map.Entry
        if (o instanceof Map.Entry) {
            o = ((Map.Entry)o).getValue(); // коллекция берется из значения
            if (null == o) { return; } // что, впринципе, бред... но чего только не случается
        }

        // обработка значений
        if (o instanceof Collection) {
            for (Object item : (Collection)o) {
                if (null == item) { continue; }
                _fillIdsFromMap(ids, item, levels-1, check);
            }
        } else if (o instanceof Map) {
            for (Object entry : ((Map)o).entrySet()) {
                if (null == entry) { continue; }
                _fillIdsFromMap(ids, entry, levels-1, check);
            }
        } else {
            throw new IllegalStateException(String.valueOf(ClassUtils.getUserClass(o)));
        }
    }
}