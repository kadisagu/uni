package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.StudentList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.PrPracticeAssignmentManager;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddEdit.PrPracticeAssignmentAddEdit;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddRegPracticeBase.PrPracticeAssignmentAddRegPracticeBase;

import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.*;

@State({
               @Bind(key = "publisherId", binding = "studentId")
       })
public class PrPracticeAssignmentStudentListUI extends UIPresenter
{

    private Long _studentId;
    private Student student;

    @Override
    public void onComponentRefresh()
    {
        student = DataAccessServices.dao().get(Student.class, _studentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (PRACTICE_ASSIGNMENT_STUDENT_DS.equals(dataSource.getName()))
        {
            dataSource.put(STUDENT_ID, _studentId);
        }
    }

    public void onClickAddPracticeAssignment()
    {
        _uiActivation.asCurrent(PrPracticeAssignmentAddEdit.class)
                .parameter(STUDENT_ID, _studentId)
                .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asCurrent(PrPracticeAssignmentAddEdit.class)
                .parameter(PR_PRACTICE_ASSIGNMENT_ID, getListenerParameterAsLong())
                .parameter(STUDENT_ID, _studentId)
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    // Listeners

    public void onClickAddRequestForRegistrationPracticeBase()
    {
        _uiActivation.asRegionDialog(PrPracticeAssignmentAddRegPracticeBase.class)
                .activate();
    }

    public void onClickPrintEntityFromList()
    {
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().printPracticeAssignment(getListenerParameterAsLong());
    }

    //Getters & setters
    public Long getStudentId()
    {
        return this._studentId;
    }

    public void setStudentId(Long studentId)
    {
        this._studentId = studentId;
    }

    public Student getStudent()
    {
        return student;
    }

    public void setStudent(Student student)
    {
        this.student = student;
    }

}