/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.DataTab.block.practiceData;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeResult;
import ru.tandemservice.unipractice.entity.catalog.PrEmploymentForm;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 10/28/14
 */
public class PracticeDataParam implements IReportDQLModifier
{
    private IReportParam<DataWrapper> _practiceCompleted = new ReportParam<>();
    private IReportParam<DataWrapper> _practiceArrivedPlace = new ReportParam<>();
    private IReportParam<DataWrapper> _practiceDistributed = new ReportParam<>();
    private IReportParam<DataWrapper> _practiceDelay = new ReportParam<>();
    private IReportParam<DataWrapper> _practiceDelayMoreTenDays = new ReportParam<>();
    private IReportParam<DataWrapper> _practiceWithPayment = new ReportParam<>();
    private IReportParam<List<PrEmploymentForm>> _practiceEmploymentForm = new ReportParam<>();  //     PrEmploymentForm
    private IReportParam<DataWrapper> _practiceDaysSustained = new ReportParam<>();

    private String alias(ReportDQL dql)
    {
        // соединяем студента
        String studentAlias = dql.innerJoinEntity(Student.class, Student.person());

        return studentAlias;
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (_practiceCompleted.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prc")
                    .where(eq(property(alias(dql)), property("prc", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prc", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(eq(property("prc", PrPracticeResult.completed()),
                            value(true)));

            if(TwinComboDataSourceHandler.getSelectedValueNotNull(_practiceCompleted.getData()))
            {
                dql.builder.where(exists(query.buildQuery()));
            }
            else
            {
                DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprc")
                        .where(eq(property(alias(dql)), property("fprc", PrPracticeResult.practiceAssignment().practice().student())))
                        .where(eq(property("fprc", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                        .where(eq(property("fprc", PrPracticeResult.completed()),
                                value(false)));
                dql.builder.where(exists(falseQuery.buildQuery()));
                dql.builder.where(notExists(query.buildQuery()));
            }
        }

        if (_practiceArrivedPlace.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prap")
                    .where(eq(property(alias(dql)), property("prap", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prap", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)));

            if(TwinComboDataSourceHandler.getSelectedValueNotNull(_practiceArrivedPlace.getData()))
            {
                dql.builder.where(exists(query.buildQuery()));
            }
            else
            {
                DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "fpap")
                        .where(eq(property(alias(dql)), property("fpap", PrPracticeAssignment.practice().student())))
                        .where(eq(property("fpap", PrPracticeAssignment.practice().year().educationYear().current()), value(true)));
                dql.builder.where(exists(falseQuery.buildQuery()));
                dql.builder.where(notExists(query.buildQuery()));
            }

        }

        if (_practiceDistributed.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "pad")
                    .where(eq(property(alias(dql)), property("pad", PrPracticeAssignment.practice().student())))
                    .where(eq(property("pad", PrPracticeAssignment.practice().year().educationYear().current()), value(true)))
                    .where(in(property("pad", PrPracticeAssignment.state().code()),
                            Lists.newArrayList(PrPracticeAssignmentStateCodes.ACCEPTED,
                                    PrPracticeAssignmentStateCodes.FINISHED,
                                    PrPracticeAssignmentStateCodes.FOR_APPROVAL)));

            if(TwinComboDataSourceHandler.getSelectedValueNotNull(_practiceDistributed.getData()))
            {
                dql.builder.where(exists(query.buildQuery()));
            }
            else
            {
                DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "fpad")
                        .where(eq(property(alias(dql)), property("fpad", PrPracticeAssignment.practice().student())))
                        .where(eq(property("fpad", PrPracticeAssignment.practice().year().educationYear().current()), value(true)))
                        .where(in(property("fpad", PrPracticeAssignment.state().code()),
                                Lists.newArrayList(PrPracticeAssignmentStateCodes.FORMING,
                                        PrPracticeAssignmentStateCodes.ACCEPTABLE)));
                dql.builder.where(exists(falseQuery.buildQuery()));
                dql.builder.where(notExists(query.buildQuery()));

            }

        }

        if (_practiceDelay.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prd")
                    .where(eq(property(alias(dql)), property("prd", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prd", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(gt(property("prd", PrPracticeResult.dateStart()), property("prd", PrPracticeResult.practiceAssignment().dateStart())));

            if(TwinComboDataSourceHandler.getSelectedValueNotNull(_practiceDelay.getData()))
            {
                dql.builder.where(exists(query.buildQuery()));
            }
            else
            {
                DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprd")
                        .where(eq(property(alias(dql)), property("fprd", PrPracticeResult.practiceAssignment().practice().student())))
                        .where(eq(property("fprd", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                        .where(le(property("fprd", PrPracticeResult.dateStart()), property("fprd", PrPracticeResult.practiceAssignment().dateStart())));

                dql.builder.where(exists(falseQuery.buildQuery()));
                dql.builder.where(notExists(query.buildQuery()));
            }

        }

        if (_practiceDelayMoreTenDays.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prdt")
                    .where(eq(property(alias(dql)), property("prdt", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prdt", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(gt(property("prdt", PrPracticeResult.dateStart()), property("prdt", PrPracticeResult.practiceAssignment().dateStart())))
                    .where(gt(DQLFunctions.diffdays(property("prdt", PrPracticeResult.dateStart()), property("prdt", PrPracticeResult.practiceAssignment().dateStart())), value(10L)));

            if(TwinComboDataSourceHandler.getSelectedValueNotNull(_practiceDelayMoreTenDays.getData()))
            {
                dql.builder.where(exists(query.buildQuery()));
            }
            else
            {
                DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprdt")
                        .where(eq(property(alias(dql)), property("fprdt", PrPracticeResult.practiceAssignment().practice().student())))
                        .where(eq(property("fprdt", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)));
                dql.builder.where(exists(falseQuery.buildQuery()));
                dql.builder.where(notExists(query.buildQuery()));
            }

        }

        if (_practiceWithPayment.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prp")
                    .where(eq(property(alias(dql)), property("prp", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prp", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(eq(property("prp", PrPracticeResult.practicePayed()),
                            value(true)));

            if(TwinComboDataSourceHandler.getSelectedValueNotNull(_practiceWithPayment.getData()))
            {
                dql.builder.where(exists(query.buildQuery()));
            }
            else
            {
                DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprp")
                        .where(eq(property(alias(dql)), property("fprp", PrPracticeResult.practiceAssignment().practice().student())))
                        .where(eq(property("fprp", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                        .where(eq(property("fprp", PrPracticeResult.practicePayed()),
                                value(false)));
                dql.builder.where(exists(falseQuery.buildQuery()));
                dql.builder.where(notExists(query.buildQuery()));
            }

        }

        if (_practiceEmploymentForm.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "pref")
                    .where(eq(property(alias(dql)), property("pref", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("pref", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(in(property("pref", PrPracticeResult.employmentForm()),
                            _practiceEmploymentForm.getData()));

            dql.builder.where(exists(query.buildQuery()));
        }


        if (_practiceDaysSustained.isActive())
        {
            DQLSelectBuilder query = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "prds")
                    .where(eq(property(alias(dql)), property("prds", PrPracticeResult.practiceAssignment().practice().student())))
                    .where(eq(property("prds", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)))
                    .where(eq(
                            DQLFunctions.diffdays(property("prds", PrPracticeResult.practiceAssignment().dateEnd()), property("prds", PrPracticeResult.practiceAssignment().dateStart())),
                            DQLFunctions.diffdays(property("prds", PrPracticeResult.dateEnd()), property("prds", PrPracticeResult.dateStart()))));

            if(TwinComboDataSourceHandler.getSelectedValueNotNull(_practiceDaysSustained.getData()))
            {
                dql.builder.where(exists(query.buildQuery()));
            }
            else
            {
                DQLSelectBuilder falseQuery = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, "fprds")
                        .where(eq(property(alias(dql)), property("fprds", PrPracticeResult.practiceAssignment().practice().student())))
                        .where(eq(property("fprds", PrPracticeResult.practiceAssignment().practice().year().educationYear().current()), value(true)));
                dql.builder.where(exists(falseQuery.buildQuery()));
                dql.builder.where(notExists(query.buildQuery()));
            }
        }
    }

    // Getters
    public IReportParam<DataWrapper> getPracticeCompleted()
    {
        return _practiceCompleted;
    }

    public IReportParam<DataWrapper> getPracticeArrivedPlace()
    {
        return _practiceArrivedPlace;
    }

    public IReportParam<DataWrapper> getPracticeDistributed()
    {
        return _practiceDistributed;
    }

    public IReportParam<DataWrapper> getPracticeDelay()
    {
        return _practiceDelay;
    }

    public IReportParam<DataWrapper> getPracticeDelayMoreTenDays()
    {
        return _practiceDelayMoreTenDays;
    }

    public IReportParam<DataWrapper> getPracticeWithPayment()
    {
        return _practiceWithPayment;
    }

    public IReportParam<List<PrEmploymentForm>> getPracticeEmploymentForm()         //     PrEmploymentForm
    {
        return _practiceEmploymentForm;
    }

    public IReportParam<DataWrapper> getPracticeDaysSustained()
    {
        return _practiceDaysSustained;
    }
}

