package ru.tandemservice.unipractice.order.entity;

import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.order.entity.gen.PrPracticeChangeExtractGen;

/**
 * Выписка об изменении приказа на практику
 */
public class PrPracticeChangeExtract extends PrPracticeChangeExtractGen
{
    @Override
    public PrPracticeAssignment getPracticeAssignment()
    {
        return getEntity().getPracticeAssignment();
    }

    @Override
    public void setEntity(Object o)
    {
        setEntity((PrAbstractExtract)o);
    }

    @Override
    public boolean isCommitted()
    {
        return UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(getState().getCode());
    }

    @Override
    public void setCommitted(boolean b)
    {
    }
}