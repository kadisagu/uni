package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.StudentList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentDSHandler;

import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.*;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentDSHandler.*;

@Configuration
public class PrPracticeAssignmentStudentList extends BusinessComponentManager {

    @Bean
    public ColumnListExtPoint practiceAssignmentStudentDS() {
        EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> practicePath = PrPracticeAssignment.practice();

        IColumnListExtPointBuilder builder = columnListExtPointBuilder(PRACTICE_ASSIGNMENT_STUDENT_DS)
                .addColumn(textColumn(EDUCATION_YEAR_C, practicePath.year().educationYear().title()).order())
                .addColumn(textColumn(GRID_TERM_C, practicePath.term().title()).order())
                .addColumn(publisherColumn(PRACTICE_TITLE_C, practicePath.registryElementPart().registryElement().parent().title()).order())
                .addColumn(textColumn(PRACTICE_BASE_C_EXT, PRACTICE_BASE_C_EXT).order())
                .addColumn(textColumn(DATES_C_EXT, DATES_C_EXT).order())
                .addColumn(textColumn(ASSIGNMENT_STATE_C, PrPracticeAssignment.state().title()))
                .addColumn(
                        actionColumn("print",
                                     CommonDefines.ICON_PRINT,
                                     "onClickPrintEntityFromList"
                        ).disabled(ASSIGNMENT_PRINT_DISABLED).permissionKey("printPrPracticeAssignmentFromList"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME,
                        CommonDefines.ICON_EDIT,
                        EDIT_LISTENER).disabled(ASSIGNMENT_EDIT_DELETE_DISABLED).permissionKey("editStudentPrPracticeAssignmentFromList"))
                .addColumn(
                        actionColumn(
                                DELETE_COLUMN_NAME,
                                CommonDefines.ICON_DELETE,
                                DELETE_LISTENER,
                                new FormattedMessage(
                                        "practiceAssignmentStudentDS.delete.alert",
                                        practicePath.term().title().s(),
                                        practicePath.year().educationYear().title().s())
                        ).disabled(ASSIGNMENT_EDIT_DELETE_DISABLED).permissionKey("deleteStudentPrPracticeAssignmentFromList")
                );
        return builder.create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> practiceAssignmentStudentDSHandler() {
        return new PracticeAssignmentDSHandler(getName());
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(
                        searchListDS(PRACTICE_ASSIGNMENT_STUDENT_DS, practiceAssignmentStudentDS(), practiceAssignmentStudentDSHandler())
                )
                .create();
    }

}
