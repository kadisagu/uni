package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic;

import org.apache.tapestry.request.IUploadFile;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeBaseRequest;

import java.util.List;
import java.util.NavigableMap;

public interface IPracticeAssignmentDao extends INeedPersistenceSupport, ICommonDAO {

    //Filter names
    public static final String F_STUDENT_ID = "studentId";

    public static final String F_STUDENT_STATUS_OPTION = "studentStatusOption";
    public static final String F_EDUCATION_YEAR = "educationYear";
    public static final String F_PERSON_LASTNAME = "personLastName";
    public static final String F_PERSON_FIRSTNAME = "personFirstName";
    public static final String F_PERSON_MIDDLENAME = "personMiddleName";
    public static final String F_BOOK_NUMBER = "bookNumber";
    public static final String F_PRACTICE_KIND = "practiceKind";
    public static final String F_STUDENT_STATE = "studentState";
    public static final String F_COMPENSATION_TYPE = "compensationTypeFilter";
    public static final String F_TARGET_ADMISSION = "targetAdmission";
    public static final String F_COURSE = "courseFilter";
    public static final String F_FORMATIVE_ORG_UNIT = "formativeOrgUnitFilter";
    public static final String F_PRODUCING_ORG_UNIT = "producingOrgUnitFilter";
    public static final String F_PRACTICE_BASE = "practiceBase";
    public static final String F_REFERRAL_STATE = "referralStateFilter";
    public static final String F_DEVELOP_FORM = "developFormFilter";
    public static final String F_GROUP = "groupFilter";
    public static final String F_EDU_LEVEL_HIGH_SCHOOL = "educationLevelHighSchoolFilter";
    //!Filter names

    void updatePracticeAssignmentState(Long prPracticeAssignmentId, String newStateCode);

    /**
     * Метод добавляет скан-копию гарантийного письма к переданному направлению на практику
     *
     * @param practiceAssignment  направление на практику, к которому добавляется скан-копия гарантийного письма
     * @param scanCopyUploadFile  загруженная скан-копия гарантийного письма
     */
    void addGuaranteeLetterToPracticeAssignment(PrPracticeAssignment practiceAssignment, IUploadFile scanCopyUploadFile);

    /**
     * Метод удаляет скан-копию гарантийного письма из переданного направления на практику
     *
     * @param practiceAssignment  направление на практику, к которому добавляется скан-копия гарантийного письма
     */
    void deleteGuaranteeLetterFromPracticeAssignment(PrPracticeAssignment practiceAssignment);

    /**
     * Метод сохраняет новую заявку на регистрацию базы практики
     *
     * @param prPracticeBaseRequest  сохраняемая заявка
     */
    void addRequestForRegPracticeBase(PrPracticeBaseRequest prPracticeBaseRequest);

    /**
     * Получает похожие внешние организации по названию с похожестью не меньше minSimilarity
     *
     * @param title строка поиска
     * @return похожесть -> список организаций
     */
    NavigableMap<Double, List<DataWrapper>> getSimilarExternalOrgUnit(String title);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    void printPracticeAssignment(Long practiceAssignmentId);
}
