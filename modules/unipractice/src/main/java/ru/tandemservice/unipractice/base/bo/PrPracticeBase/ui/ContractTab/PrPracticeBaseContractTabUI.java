/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.ContractTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplate;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContract.logic.PrPracticeHoldingContractDSHandler;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;

/**
 * @author azhebko
 * @since 07.11.2014
 */
@State(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "practiceBase.id", required = true))
public class PrPracticeBaseContractTabUI extends UIPresenter
{
    private PrPracticeBaseExt _practiceBase = new PrPracticeBaseExt();

    @Override
    public void onComponentRefresh()
    {
        _practiceBase = IUniBaseDao.instance.get().getNotNull(PrPracticeBaseExt.class, _practiceBase.getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PrPracticeHoldingContractDSHandler.BIND_CONTRACT_NUMBER, this.getSettings().get(PrPracticeHoldingContractDSHandler.BIND_CONTRACT_NUMBER));
        dataSource.put(PrPracticeHoldingContractDSHandler.BIND_PRACTICE_BASE, getPracticeBase().getId());
    }

    @Override public String getSettingsKey() { return super.getSettingsKey() + "." + getPracticeBase().getId(); }

    public void onClickAddContract()
    {
        this.getActivationBuilder().asRegionDialog(CtrContractVersionAddByTemplate.class).parameter(IUIPresenter.PUBLISHER_ID, getPracticeBase().getId()).activate();
    }

    public void onClickSearch()
    {
        this.saveSettings();
    }

    public void onClickClear()
    {
        this.clearSettings();
        onClickSearch();
    }

    public PrPracticeBaseExt getPracticeBase() { return _practiceBase; }
}