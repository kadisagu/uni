package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.ContractTab.PrPracticeBaseContractTab;

@Configuration
public class PrPracticeBasePub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .create();
    }

    @Bean
    public TabPanelExtPoint tabPanelExtPoint()
    {
        return tabPanelExtPointBuilder("practiceBasePubTabPanel")
                .addTab(htmlTab("practiceBaseInfoTab", "PracticeBaseInfoTab"))
                .addTab(componentTab("practiceBaseContractTab", PrPracticeBaseContractTab.class)
                    .visible("ui:practiceBaseContractTabVisible")
                    .permissionKey("viewContractTab_practiceBase"))
                .create();
    }
}
