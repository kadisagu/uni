package ru.tandemservice.unipractice.component.student.StudentPracticeTab;

import ru.tandemservice.uni.component.student.StudentPub.IStudentModel;

public class Model {

    private IStudentModel _studentModel;
    private String _selectedTabId;

    // Getters & Setters
    public IStudentModel getStudentModel() {
        return _studentModel;
    }

    public void setStudentModel(IStudentModel studentModel) {
        _studentModel = studentModel;
    }

    public String get_selectedTabId() {
        return _selectedTabId;
    }

    public void set_selectedTabId(String _selectedTabId) {
        this._selectedTabId = _selectedTabId;
    }
}