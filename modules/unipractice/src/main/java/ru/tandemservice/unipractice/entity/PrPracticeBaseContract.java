package ru.tandemservice.unipractice.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContextObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.unipractice.entity.gen.PrPracticeBaseContractGen;

import java.util.Collection;
import java.util.Collections;

/**
 * Связь базы прохождения практики с договором об организации и проведении практики студентов
 *
 * Связывает договор об организации и проведении практики студентов и базы проведения практики
 */
public class PrPracticeBaseContract extends PrPracticeBaseContractGen implements ICtrContextObject
{
    public PrPracticeBaseContract()
    {
    }

    public PrPracticeBaseContract(CtrContractObject contractObject, PrPracticeBaseExt practiceBase)
    {
        this.setContractObject(contractObject);
        this.setPracticeBase(practiceBase);
    }

    @Override public Collection<IEntity> getSecLocalEntities() { return Collections.emptyList(); }
}