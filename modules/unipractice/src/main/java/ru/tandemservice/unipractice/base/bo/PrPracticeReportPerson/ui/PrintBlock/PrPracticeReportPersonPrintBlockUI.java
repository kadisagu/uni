/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.PrintBlock;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI;

/**
 * @author nvankov
 * @since 10/28/14
 */
public class PrPracticeReportPersonPrintBlockUI extends UIPresenter  implements IReportDQLModifierOwner
{
    private PrPracticePrintBlock _practicePrintBlock = new PrPracticePrintBlock();

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (printInfo.isExists(StudentReportPersonAddUI.STUDENT_SCHEET))
        {
            // фильтры модифицируют запросы

            // печатные блоки модифицируют запросы и создают печатные колонки
            _practicePrintBlock.modify(dql, printInfo);
        }
    }

    // Getters

    public PrPracticePrintBlock getPracticePrintBlock()
    {
        return _practicePrintBlock;
    }
}
