package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic;

public class PracticeAssignmentConstants
{
    public static final String PR_PRACTICE_ASSIGNMENT_ID = "prPracticeAssignmentId";
    public static final String STUDENT_ID = "studentId";
    public static final String ORG_UNIT_ID = "orgUnitId";

    public static final String PRACTICE_DS = "practiceDS";
    public static final String PRACTICE_BASE_DS = "practiceBaseDS";
    public static final String TUTOR_DS = "tutorDS";
    public static final String PRACTICE_ASSIGNMENT_DS = "practiceAssignmentDS";
    public static final String PRACTICE_ASSIGNMENT_STUDENT_DS = "practiceAssignmentStudentDS";
    public static final String F_EDUCATION_YEAR_DS = "educationYearDS";
    public static final String F_STUDENT_STATUS_OPTION_DS = "studentStatusOptionDS";
    public static final String F_PRACTICE_KINDS_DS = "practiceKindsDS";
    public static final String F_STUDENT_STATE_DS = "studentStateDS";
    public static final String F_COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String F_PRACTICE_BASE_DS = "practiceBaseDS";
    public static final String F_FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String F_PRODUCING_ORG_UNIT_DS = "producingOrgUnitDS";
    public static final String F_REFERRAL_STATE_DS = "referralStateDS";
    public static final String F_EDU_LEVEL_HIGH_SCHOOL_DS = "educationLevelHighSchoolDS";

    public static final String PRACTICE = "practice";

    public static final String ASSIGNMENT_EDIT_DELETE_DISABLED = "assignmentEditDeleteDisabled";
    public static final String ASSIGNMENT_PRINT_DISABLED = "assignmentPrintDisabled";

}
