/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.logic;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;

import java.util.Date;

/**
 * @author azhebko
 * @since 07.11.2014
 */
public interface IPrPracticeHoldingContractTemplateDao extends ICtrContractTemplateDAO
{
    /** Создает договор, версию и шаблон догоовра об организации и проведении практики студентов. */
    public void doSavePracticeHoldingContract(CtrContractVersion contractVersion, PrPracticeBaseExt practiceBase, CtrContractVersionCreateData versionCreateData, Date docStartDate);
}