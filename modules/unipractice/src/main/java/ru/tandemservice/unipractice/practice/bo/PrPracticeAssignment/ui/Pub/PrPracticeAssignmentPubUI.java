package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeResult;
import ru.tandemservice.unipractice.entity.gen.PrPracticeResultGen;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.PrPracticeAssignmentManager;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddEdit.PrPracticeAssignmentAddEdit;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddGuarantee.PrPracticeAssignmentAddGuarantee;

import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes.*;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.PR_PRACTICE_ASSIGNMENT_ID;

@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "id", required = true),
})
public class PrPracticeAssignmentPubUI extends UIPresenter {

    private Long id;
    private PrPracticeAssignment prPracticeAssignment;

    @Override
    public void onComponentRefresh() {
        prPracticeAssignment = DataAccessServices.dao().getNotNull(PrPracticeAssignment.class, id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PrPracticeAssignment getPrPracticeAssignment() {
        return prPracticeAssignment;
    }

    public void setPrPracticeAssignment(PrPracticeAssignment prPracticeAssignment) {
        this.prPracticeAssignment = prPracticeAssignment;
    }

    public void onEditPrPracticeAssignment(){
        _uiActivation.asRegion(PrPracticeAssignmentAddEdit.class).parameter(PracticeAssignmentConstants.PR_PRACTICE_ASSIGNMENT_ID, id).activate();
    }

    public String getGuaranteeLetterTitle()
    {
        String letterTitle = "";
        letterTitle += getPrPracticeAssignment().getGuaranteeNumber()!=null?"№"+getPrPracticeAssignment().getGuaranteeNumber():"";
        letterTitle += getPrPracticeAssignment().getGuaranteeDate()!=null?" от "+ RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(getPrPracticeAssignment().getGuaranteeDate()):"";
        return letterTitle;
    }

    @SuppressWarnings("unused")
    public String getDeletePracticeAssignmentAlert() {
        return String.format(_uiConfig.getProperty("ui.delete.alert"));
    }

    public void onDeletePrPracticeAssignment() {
        DataAccessServices.dao().delete(id);
        deactivate();
    }

    @SuppressWarnings("unused")
    public boolean isForming() {
        String code = prPracticeAssignment.getState().getCode();
        return code.equals(FORMING);
    }

    @SuppressWarnings("unused")
    public boolean isAcceptable() {
        String code = prPracticeAssignment.getState().getCode();
        return code.equals(ACCEPTABLE);
    }

    public boolean isAccepted() {
        String code = prPracticeAssignment.getState().getCode();
        return code.equals(ACCEPTED);
    }

    public boolean isFinished() {
        String code = prPracticeAssignment.getState().getCode();
        return code.equals(FINISHED);
    }

    public boolean isGuaranteeLetterExist() {
        return getPrPracticeAssignment().getGuaranteeCopy()!=null;
    }

    @SuppressWarnings("unused")
    public boolean isAcceptedOrFinished() {
        return isAccepted() || isFinished();
    }

    public void approve() {
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().updatePracticeAssignmentState(prPracticeAssignment.getId(), ACCEPTABLE);
        onComponentRefresh();
    }

    public void accept() {
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().updatePracticeAssignmentState(prPracticeAssignment.getId(), ACCEPTED);
        onComponentRefresh();
    }

    public void submitForResultChecking() {
        PrPracticeResult result = DataAccessServices.dao().getByNaturalId(new PrPracticeResultGen.NaturalId(prPracticeAssignment));
        if(result == null)
            throw new ApplicationException("Нельзя отправить направление на проверку результатов, так как результаты прохождения практики отсутствуют.");
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().updatePracticeAssignmentState(prPracticeAssignment.getId(), FOR_APPROVAL);
        onComponentRefresh();
    }

    public void approveResultChecking() {
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().updatePracticeAssignmentState(prPracticeAssignment.getId(), FINISHED);
        onComponentRefresh();
    }

    public void rejectResultChecking() {
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().updatePracticeAssignmentState(prPracticeAssignment.getId(), ACCEPTED);
        onComponentRefresh();
    }

    public void rejectToEditResult() {
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().updatePracticeAssignmentState(prPracticeAssignment.getId(), ACCEPTED);
        onComponentRefresh();
    }

    public boolean isSubmitForResultCheckingButtonVisible()
    {
        return ACCEPTED.equals(prPracticeAssignment.getState().getCode());
    }

    public boolean isApproveResultCheckingButtonVisible()
    {
        return FOR_APPROVAL.equals(prPracticeAssignment.getState().getCode());
    }

    public boolean isRejectResultCheckingButtonVisible()
    {
        return FOR_APPROVAL.equals(prPracticeAssignment.getState().getCode());
    }

    public boolean isRejectToEditResultButtonVisible()
    {
        return FINISHED.equals(prPracticeAssignment.getState().getCode());
    }

    public String getTargetAdmissionDetails()
    {
        if(prPracticeAssignment.getPractice().getStudent().isTargetAdmission() && prPracticeAssignment.getPractice().getStudent().getTargetAdmissionOrgUnit() != null)
        {
            ExternalOrgUnit orgUnit = prPracticeAssignment.getPractice().getStudent().getTargetAdmissionOrgUnit();

            return orgUnit.getTitleWithLegalForm() +
                    (orgUnit.getLegalAddress() != null && orgUnit.getLegalAddress().getSettlement() != null
                    ? ", " + orgUnit.getLegalAddress().getSettlement().getTitle()
                    : (orgUnit.getFactAddress() != null && orgUnit.getFactAddress().getSettlement() != null
                    ? ", " + orgUnit.getFactAddress().getSettlement().getTitle()
                    : ""));
        }
        else
            return "";
    }

    public void rejectToForming() {
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().updatePracticeAssignmentState(prPracticeAssignment.getId(), FORMING);
        onComponentRefresh();
    }

    public void onClickAddGuarantee()
    {
        _uiActivation.asRegion(PrPracticeAssignmentAddGuarantee.class)
                .parameter(PR_PRACTICE_ASSIGNMENT_ID, prPracticeAssignment.getId()).activate();
    }

    public void onClickDownLoadGuarantee()
    {
        DatabaseFile scanCopy = getPrPracticeAssignment().getGuaranteeCopy();
        if (scanCopy == null)
            throw new ApplicationException("Нет загруженной копии.");
        if (scanCopy.getContent() == null)
            throw new ApplicationException("Файл скан-копии пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(scanCopy), false);
    }

    public void onClickDeleteGuarantee()
    {
        DatabaseFile scanCopy = getPrPracticeAssignment().getGuaranteeCopy();
        if (scanCopy == null)
            throw new ApplicationException("Нет загруженной копии.");
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().deleteGuaranteeLetterFromPracticeAssignment(getPrPracticeAssignment());
    }

    public void onClickPrint()
    {
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().printPracticeAssignment(getId());
    }

}
