/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unipractice.entity.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 10/22/14
 */
public class PrPracticeBaseRequestDSHandler extends DefaultSearchDataSourceHandler
{
    public PrPracticeBaseRequestDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long prPracticeBaseKindId = context.get("prPracticeBaseKindId");


        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PrPracticeBaseRequest.class, "r")
                .joinEntity("r", DQLJoinType.left, PrPracticeBaseRequestInt.class, "ri", eq(property("ri"), property("r")))
                .joinEntity("r", DQLJoinType.left, PrPracticeBaseRequestExt.class, "re", eq(property("re"), property("r")))
                .fetchPath(DQLJoinType.left, PrPracticeBaseRequestInt.orgUnit().fromAlias("ri"), "ou")
                .fetchPath(DQLJoinType.left, PrPracticeBaseRequestInt.kind().fromAlias("ri"), "intkind")
                .fetchPath(DQLJoinType.left, PrPracticeBaseRequestExt.orgUnit().fromAlias("re"), "extou")
                .fetchPath(DQLJoinType.left, PrPracticeBaseRequestExt.kind().fromAlias("re"), "extkind")
                .column("ri")
                .column("re")
                .column(
                        new DQLCaseExpressionBuilder()
                                .when(exists(new DQLSelectBuilder().fromEntity(PrPracticeBase.class, "pb")
                                        .joinEntity("pb", DQLJoinType.left, PrPracticeBaseInt.class, "pbi", eq(property("pbi"), property("pb")))
                                        .joinEntity("pb", DQLJoinType.left, PrPracticeBaseExt.class, "pbe", eq(property("pbe"), property("pb")))
                                        .fetchPath(DQLJoinType.left, PrPracticeBaseRequestInt.orgUnit().fromAlias("pbi"), "pbou")
                                        .fetchPath(DQLJoinType.left, PrPracticeBaseRequestExt.orgUnit().fromAlias("pbe"), "pbextou")
                                        .top(1)
                                        .where(or(
                                                eq(property("pbou"), property("ou")),
                                                eq(property("pbextou"), property("extou"))))
                                        .buildQuery()), value(Boolean.TRUE))
                                .when(value(Boolean.FALSE), value(Boolean.FALSE))
                                .otherwise(value(Boolean.FALSE))
                                .build());
        if(prPracticeBaseKindId != null)
            builder.where(eq(property("r", PrPracticeBase.kind().id()), value(prPracticeBaseKindId)));

        builder.order(property("r", PrPracticeBaseRequest.kind().title()));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build().transform((Object[] item) -> {
            PrPracticeBaseRequestInt requestInt = (PrPracticeBaseRequestInt) item[0];
            PrPracticeBaseRequestExt requestExt = (PrPracticeBaseRequestExt) item[1];
            boolean baseExist = (boolean) item[2];
            return requestInt != null
                    ? new PrPracticeBaseRequestWrapper(requestInt, baseExist)
                    : new PrPracticeBaseRequestWrapper(requestExt, baseExist);
        });
    }
}
