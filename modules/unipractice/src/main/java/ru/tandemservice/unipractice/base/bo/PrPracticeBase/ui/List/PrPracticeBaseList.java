package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.PrPracticeBaseManager;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseDSHandler;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;

import static ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseConstants.*;

@Configuration
public class PrPracticeBaseList extends BusinessComponentManager {
    @Bean
    public ColumnListExtPoint prPracticeBaseDS() {
        return columnListExtPointBuilder(PR_PRACTICE_BASE_DS)
                .addColumn(publisherColumn(COLUMN_TITLE, PrPracticeBase.title()).order().required(true))
                .addColumn(textColumn(COLUMN_KIND, PrPracticeBase.kind().title()).order())
                .addColumn(toggleColumn(COLUMN_ARCHIVAL, PrPracticeBase.P_ACTIVE)
                        .toggleOffListener("onClickSendToArchive").toggleOffListenerAlert(alert(PR_PRACTICE_BASE_DS + ".sendToArchive.alert")).toggleOffLabel("В архиве")
                        .toggleOnListener("onClickSendFromArchive").toggleOnListenerAlert(alert(PR_PRACTICE_BASE_DS + ".makeActive.alert", COLUMN_TITLE)).toggleOnLabel("Активна")
                        .permissionKey("archivePrPracticeBase"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                        FormattedMessage.with().template(PR_PRACTICE_BASE_DS + ".delete.alert")
                                .parameter("title")
                                .create()).permissionKey("deletePrPracticeBase"))
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PR_PRACTICE_BASE_DS, prPracticeBaseDS(), prPracticeBaseDSHandler()))
                .addDataSource(selectDS(PR_PRACTICE_BASE_KIND_DS, PrPracticeBaseManager.instance().prPracticeBaseKindDSHandler()).addColumn(PrPracticeBaseKind.P_TITLE))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> prPracticeBaseDSHandler() {
        return new PrPracticeBaseDSHandler(getName());
    }

}
