package ru.tandemservice.unipractice.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unipractice.order.entity.PrAbstractExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка об изменении приказа на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeChangeExtractGen extends PrAbstractExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract";
    public static final String ENTITY_NAME = "prPracticeChangeExtract";
    public static final int VERSION_HASH = 722322472;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTITY = "entity";

    private PrAbstractExtract _entity;     // Абстрактная выписка на практику

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абстрактная выписка на практику. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PrAbstractExtract getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Абстрактная выписка на практику. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntity(PrAbstractExtract entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PrPracticeChangeExtractGen)
        {
            setEntity(((PrPracticeChangeExtract)another).getEntity());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeChangeExtractGen> extends PrAbstractExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeChangeExtract.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeChangeExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return obj.getEntity();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entity":
                    obj.setEntity((PrAbstractExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return PrAbstractExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeChangeExtract> _dslPath = new Path<PrPracticeChangeExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeChangeExtract");
    }
            

    /**
     * @return Абстрактная выписка на практику. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract#getEntity()
     */
    public static PrAbstractExtract.Path<PrAbstractExtract> entity()
    {
        return _dslPath.entity();
    }

    public static class Path<E extends PrPracticeChangeExtract> extends PrAbstractExtract.Path<E>
    {
        private PrAbstractExtract.Path<PrAbstractExtract> _entity;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абстрактная выписка на практику. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract#getEntity()
     */
        public PrAbstractExtract.Path<PrAbstractExtract> entity()
        {
            if(_entity == null )
                _entity = new PrAbstractExtract.Path<PrAbstractExtract>(L_ENTITY, this);
            return _entity;
        }

        public Class getEntityClass()
        {
            return PrPracticeChangeExtract.class;
        }

        public String getEntityName()
        {
            return "prPracticeChangeExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
