/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.logic.IPrPracticeHoldingContractTemplateDao;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.logic.PrPracticeHoldingContractTemplateDao;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.ui.Edit.PrPracticeHoldingContractTemplateEdit;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.ui.Pub.PrPracticeHoldingContractTemplatePub;
import ru.tandemservice.unipractice.entity.PrPracticeHoldingContractTemplateData;

/**
 * @author azhebko
 * @since 07.11.2014
 */
@Configuration
public class PrPracticeHoldingContractTemplateManager extends BusinessObjectManager implements ICtrContractTemplateManager
{
    public static PrPracticeHoldingContractTemplateManager instance() { return instance(PrPracticeHoldingContractTemplateManager.class); }

    @Override public Class<? extends CtrContractVersionTemplateData> getDataEntityClass() { return PrPracticeHoldingContractTemplateData.class; }

    @Override public Class<? extends BusinessComponentManager> getDataDisplayComponent() { return PrPracticeHoldingContractTemplatePub.class; }

    @Override public Class<? extends BusinessComponentManager> getDataEditComponent() { return PrPracticeHoldingContractTemplateEdit.class; }

    @Override @Bean public IPrPracticeHoldingContractTemplateDao dao() { return new PrPracticeHoldingContractTemplateDao(); }

    @Override public boolean isAllowEditInWizard() { return false; }
}