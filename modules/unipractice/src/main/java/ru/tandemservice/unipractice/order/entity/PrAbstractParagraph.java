package ru.tandemservice.unipractice.order.entity;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unipractice.order.entity.gen.PrAbstractParagraphGen;

import java.util.List;

/**
 * Абстрактный параграф на практику
 */
public abstract class PrAbstractParagraph extends PrAbstractParagraphGen implements IAbstractParagraph
{
    @Override
    @SuppressWarnings({"unchecked"})
    public List<? extends IAbstractExtract> getExtractList()
    {
        return UniDaoFacade.getCoreDao().getList(PrAbstractExtract.class, IAbstractExtract.L_PARAGRAPH, this, IAbstractExtract.P_NUMBER);
    }

    @Override
    public int getExtractCount()
    {
        return UniDaoFacade.getCoreDao().getCount(PrAbstractExtract.class, IAbstractExtract.L_PARAGRAPH, this);
    }

    @Override
    public void setOrder(IAbstractOrder order)
    {
        setOrder((PrPracticeOrder) order);
    }

}