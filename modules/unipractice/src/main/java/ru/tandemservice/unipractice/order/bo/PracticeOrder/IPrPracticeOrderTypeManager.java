/* $Id:$ */
package ru.tandemservice.unipractice.order.bo.PracticeOrder;

import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author oleyba
 * @since 8/22/14
 */
public interface IPrPracticeOrderTypeManager
{

    public Class<? extends BusinessComponentManager> getParagraphListBC();
}
