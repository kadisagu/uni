package ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.wrapper;

import org.tandemframework.caf.logic.wrapper.DataWrapper;

public class PrPracticeParagraphWrapper extends DataWrapper {

    public static final String TITLE_FULL_C = "titleFull";
    public static final String PARAGRAPH_NAME_C = "name";
    public static final String CHANGEABLE_ORDER_NUMBER_C = "changeableOrderNumber";
    public static final String PRACTICE_BEGIN_DATE_C = "beginDate";
    public static final String PRACTICE_END_DATE_C = "endDate";
    public static final String AMOUNT_OF_STUDENTS_C = "amount";
    public static final String FORMATIVE_ORG_UNIT_LIST = "formativeOrgUnitList";
    public static final String RESPONSIBLE_ORG_UNIT_LIST = "responsibleOrgUnitList";
    public static final String TARGET_ORDER_ID = "targetOrderId";


}
