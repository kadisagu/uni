package ru.tandemservice.unipractice.order.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unipractice.order.entity.gen.PrPracticeOrderGen;

import java.util.List;

/**
 * Приказ на практику
 */
public class PrPracticeOrder extends PrPracticeOrderGen implements IAbstractOrder
{
    @Override
    public void setState(ICatalogItem state)
    {
        super.setState((OrderStates) state);
    }

    @Override
    public String getTitle()
    {
        return "Приказ о направлении на практику" +
                (getNumber() == null ? "" : " №" + getNumber()) +
                (getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
    }

    @Override
    public int getParagraphCount()
    {
        return UniDaoFacade.getCoreDao().getCount(PrAbstractParagraph.class, IAbstractParagraph.L_ORDER, this);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<? extends IAbstractParagraph> getParagraphList()
    {
        return UniDaoFacade.getCoreDao().getList(PrAbstractParagraph.class, IAbstractParagraph.L_ORDER, this, IAbstractParagraph.P_NUMBER);
    }
}