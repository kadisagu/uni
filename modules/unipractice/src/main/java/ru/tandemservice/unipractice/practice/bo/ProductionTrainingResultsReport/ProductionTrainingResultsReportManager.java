/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.logic.IProductionTrainingResultsReportDao;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.logic.ProductionTrainingResultsReportDao;

/**
 * @author rsizonenko
 * @since 22.10.2014
 */
@Configuration
public class ProductionTrainingResultsReportManager  extends BusinessObjectManager {
    public static ProductionTrainingResultsReportManager instance() { return instance(ProductionTrainingResultsReportManager.class); }

    @Bean
    public IProductionTrainingResultsReportDao productionTrainingResultsReportDao() {
        return new ProductionTrainingResultsReportDao();
    }

    public String getPermissionKey()
    {
        return "productionTrainingResultsReportPermissionKey";
    }

}
