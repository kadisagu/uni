package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.PrPracticeAssignmentManager;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddEdit.PrPracticeAssignmentAddEdit;

import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.IPracticeAssignmentDao.*;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.PRACTICE_ASSIGNMENT_DS;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.PR_PRACTICE_ASSIGNMENT_ID;

public class PrPracticeAssignmentListUI extends UIPresenter {

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (PRACTICE_ASSIGNMENT_DS.equals(dataSource.getName())) {
            dataSource.put(F_STUDENT_STATUS_OPTION, _uiSettings.get(F_STUDENT_STATUS_OPTION));
            dataSource.put(F_EDUCATION_YEAR, _uiSettings.get(F_EDUCATION_YEAR));
            dataSource.put(F_PERSON_LASTNAME, _uiSettings.get(F_PERSON_LASTNAME));
            dataSource.put(F_PERSON_FIRSTNAME, _uiSettings.get(F_PERSON_FIRSTNAME));
            dataSource.put(F_PERSON_MIDDLENAME, _uiSettings.get(F_PERSON_MIDDLENAME));
            dataSource.put(F_BOOK_NUMBER, _uiSettings.get(F_BOOK_NUMBER));
            dataSource.put(F_PRACTICE_KIND, _uiSettings.get(F_PRACTICE_KIND));
            dataSource.put(F_STUDENT_STATE, _uiSettings.get(F_STUDENT_STATE));
            dataSource.put(F_COMPENSATION_TYPE, _uiSettings.get(F_COMPENSATION_TYPE));
            dataSource.put(F_TARGET_ADMISSION, _uiSettings.get(F_TARGET_ADMISSION));
            dataSource.put(F_PRACTICE_BASE, _uiSettings.get(F_PRACTICE_BASE));
            dataSource.put(F_GROUP, _uiSettings.get(F_GROUP));
            dataSource.put(F_COURSE, _uiSettings.get(F_COURSE));
            dataSource.put(F_DEVELOP_FORM, _uiSettings.get(F_DEVELOP_FORM));
            dataSource.put(F_FORMATIVE_ORG_UNIT, _uiSettings.get(F_FORMATIVE_ORG_UNIT));
            dataSource.put(F_PRODUCING_ORG_UNIT, _uiSettings.get(F_PRODUCING_ORG_UNIT));
            dataSource.put(F_REFERRAL_STATE, _uiSettings.get(F_REFERRAL_STATE));
            dataSource.put(F_EDU_LEVEL_HIGH_SCHOOL, _uiSettings.get(F_EDU_LEVEL_HIGH_SCHOOL));
        }
    }

    public void onEditEntityFromList(){
        _uiActivation.asCurrent(PrPracticeAssignmentAddEdit.class)
                .parameter(PR_PRACTICE_ASSIGNMENT_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickPrintEntityFromList()
    {
        PrPracticeAssignmentManager.instance().practiceAssignmentDao().printPracticeAssignment(getListenerParameterAsLong());
    }

    public void onDeleteEntityFromList() {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
