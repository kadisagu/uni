/* $Id$ */
package ru.tandemservice.unipractice.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.util.CtrContractVersionUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unipractice.entity.PrPracticeBaseContract;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;

import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 16.10.2014
 */
public class UniPracticeOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unipractice");
        config.setName("unipractice-sec-config");
        config.setTitle("");


        final ModuleLocalGroupMeta mlgPractice = PermissionMetaUtil.createModuleLocalGroup(config, "unipracticeLocal", "Модуль «Практики студентов»");
        final ClassGroupMeta lcgListDocument = PermissionMetaUtil.createClassGroup(mlgPractice, "stPracticePreallocationLocalClass", "Объект «Предварительное распределение студентов»", StPracticePreallocation.class.getName());
        PermissionMetaUtil.createGroupRelation(config, "prPracticePreallocationReportPG", lcgListDocument.getName());

        PermissionGroupMeta pg = CtrContractVersionUtil.registerPermissionGroup(config, PrPracticeBaseContract.class, null);
        PermissionMetaUtil.createGroupRelation(config, pg.getName(), "prPracticeBaseContractCG");

        // для каждого типа подразделения добавляем права:
        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            // Отчеты
            PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            PermissionGroupMeta practiceReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "unipracticeReport", "Отчеты модуля «Практики студентов»");
            PermissionMetaUtil.createPermission(practiceReports, "orgUnit_viewStPracticePreallocationReportPermissionKey_" + code, "Просмотр и печать отчета «Предварительное распределение студентов»");
            PermissionMetaUtil.createPermission(practiceReports, "orgUnit_addStPracticePreallocationReportPermissionKey_" + code, "Добавление отчета «Предварительное распределение студентов»");
            PermissionMetaUtil.createPermission(practiceReports, "orgUnit_deleteStPracticePreallocationReportPermissionKey_" + code, "Удаление отчета «Предварительное распределение студентов»");


        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
