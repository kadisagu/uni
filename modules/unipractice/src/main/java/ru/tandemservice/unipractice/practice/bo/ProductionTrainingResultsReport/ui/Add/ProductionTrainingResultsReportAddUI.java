/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ProductionTrainingResultsReportManager;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.List.ProductionTrainingResultsReportList;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.Pub.ProductionTrainingResultsReportPub;

/**
 * @author rsizonenko
 * @since 22.10.2014
 */
@State({
        @Bind(key = ProductionTrainingResultsReportList.EDUCATION_YEAR, binding = "educationYear.id", required = true)
})
public class ProductionTrainingResultsReportAddUI extends UIPresenter {

    private EducationYear educationYear = new EducationYear();
    private boolean enableDetachedExtOrgUnit;
    private ExternalOrgUnit externalOrgUnit;


    @Override
    public void onComponentRefresh()
    {
        educationYear = DataAccessServices.dao().get(educationYear.getId());
    }



    public void onClickApply()
    {
        Long reportId = ProductionTrainingResultsReportManager.instance().productionTrainingResultsReportDao().createReport(this);
        _uiActivation.
             asDesktopRoot(ProductionTrainingResultsReportPub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
        deactivate();
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public ExternalOrgUnit getExternalOrgUnit() {
        return externalOrgUnit;
    }

    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit) {
        this.externalOrgUnit = externalOrgUnit;
    }

    public boolean isEnableDetachedExtOrgUnit() {
        return enableDetachedExtOrgUnit;
    }

    public void setEnableDetachedExtOrgUnit(boolean enableDetachedExtOrgUnit) {
        this.enableDetachedExtOrgUnit = enableDetachedExtOrgUnit;
    }
}