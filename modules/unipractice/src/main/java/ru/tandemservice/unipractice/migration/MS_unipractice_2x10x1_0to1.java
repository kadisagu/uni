package ru.tandemservice.unipractice.migration;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unipractice_2x10x1_0to1 extends IndependentMigrationScript
{
    // CtrContractKindCodes
    /** Константа кода (code) элемента : Организации и проведении практики студентов (title) */
    private String CONTRACT_PRACTICE = "contract.practice";

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        // Получаем уже существующие элементы ctrcontractkind_t
        Map<String, Long> contractKindMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontractkind_t");

        fillCatalogs(contractKindMap, tool);

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractVersion

        // заполняем printTemplate и kind

        SQLSelectQuery selectContractVersionQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null or printtemplate_id is null");

        List<Object[]> contractVersionList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        for (Object[] contractVersion : contractVersionList)
        {
            setCtrPrintTemplate((Long) contractVersion[0], contractKindMap, tool);
        }

        // Проверяем, что нет версий без вида, если нет, то делаем колонку обязательной
        SQLSelectQuery selectCtrVerQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null");

        List<Object[]> ctrVerList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        if (ctrVerList.isEmpty())
        {
            tool.setColumnNullable("ctr_contractver_t", "kind_id", false);
        }
    }

    private void setCtrPrintTemplate(Long contractVersionId, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        SQLUpdateQuery updateQuery = new SQLUpdateQuery("ctr_contractver_t")
                .set("kind_id", "?")
                .set("printtemplate_id", "?")
                .where("id=?");

        /** Константа кода (code) элемента : Договор об организации и проведении практики студентов (title) */
        String DOGOVOR_OB_ORGANIZATSII_I_PROVEDENII_PRAKTIKI_STUDENTOV = "practice.01";

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "pr_practice_holding_ctmpldt_t", tool) && CtrMigrationUtil.checkContractType(contractVersionId, DOGOVOR_OB_ORGANIZATSII_I_PROVEDENII_PRAKTIKI_STUDENTOV, tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    contractKindMap.get(CONTRACT_PRACTICE),
                    null,
                    contractVersionId);
        }
    }

    private void fillCatalogs(Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractKind

        // гарантировать наличие кода сущности
        short ctrContractKindCode = tool.entityCodes().ensure("ctrContractKind");

        ////////////////////////////////////////////////////////////////////////////////
        // заполняем справочники ctrContractKind ctrcontractkind_t

        SQLInsertQuery insertCtrContractKindQuery = new SQLInsertQuery("ctrcontractkind_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("title_p", "?")
                .set("shorttitle_p", "?")
                .set("contract_p", "?");

        // Договор об организации и проведении практики студентов
        // code - "contract.practice", title - "Организация и проведение практики студентов", shortTitle - "Практика студентов", contract - "true"
        if(!contractKindMap.containsKey(CONTRACT_PRACTICE))
        {
            Long contractPracticeId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(CONTRACT_PRACTICE, contractPracticeId);
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(insertCtrContractKindQuery), contractPracticeId, ctrContractKindCode, "contract.practice", "Организация и проведение практики студентов", "Практика студентов", true);
        }
    }
}
