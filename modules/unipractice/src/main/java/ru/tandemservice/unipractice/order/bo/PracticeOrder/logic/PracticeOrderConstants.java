package ru.tandemservice.unipractice.order.bo.PracticeOrder.logic;

public class PracticeOrderConstants {
    public static final String VIEW_PROPERTY_PARAGRAPH_COUNT = "paragraphCount";
    public static final String VIEW_PROPERTY_PRINTING_DISABLED = "printingDisabled";
    public static final String VIEW_PROPERTY_EDIT_DELETE_ENABLED = "editDeleteEnabled";

    public static final String PRACTICE_ORDER_ID = "practiceOrderId";
    public static final String PRACTICE_ORDER_DS = "practiceOrderDS";

    public static final String ORDER_TYPE_DS = "orderTypeDS";
    public static final String ORDER_STATE_DS = "orderStateDS";
    public static final String EDUCATION_YEAR_DS = "educationYearDS";

    public static final String CREATE_DATE_C = "createDate";
    public static final String COMMIT_DATE_C = "commitDate";
    public static final String NUMBER_C = "number";
    public static final String TYPE_C = "type";
    public static final String COMMIT_DATE_SYSTEM_C = "commitDateSystem";
    public static final String EDUCATION_YEAR_C = "educationYear";
    public static final String PARAGRAPH_COUNT_C = "paragraphCount";
    public static final String STATE_C = "state";
}
