/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unipractice.entity.PrPracticeHoldingContractTemplateData;

/**
 * @author azhebko
 * @since 07.11.2014
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "versionTemplateData.id", required = true)})
public class PrPracticeHoldingContractTemplatePubUI extends UIPresenter
{
    private PrPracticeHoldingContractTemplateData _versionTemplateData = new PrPracticeHoldingContractTemplateData();
    public PrPracticeHoldingContractTemplateData getVersionTemplateData() { return _versionTemplateData; }

    @Override
    public void onComponentRefresh()
    {
        _versionTemplateData = IUniBaseDao.instance.get().getNotNull(PrPracticeHoldingContractTemplateData.class, _versionTemplateData.getId());
    }
}