/* $Id$ */
package ru.tandemservice.unipractice.base.ext.GlobalReport.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.ui.List.GlobalReportList;

/**
 * @author Andrey Avetisov
 * @since 09.10.2014
 */
@Configuration
public class GlobalReportListExt extends BusinessComponentExtensionManager
{
    @Autowired
    private GlobalReportList _globalReportList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_globalReportList.presenterExtPoint())
                .addAddon(uiAddon(UniPracticeGlobalReportListAddon.NAME, UniPracticeGlobalReportListAddon.class))
                .create();
    }
}