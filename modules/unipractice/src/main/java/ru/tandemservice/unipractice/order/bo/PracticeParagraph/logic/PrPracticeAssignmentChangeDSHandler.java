package ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;

import java.util.Collections;
import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

public class PrPracticeAssignmentChangeDSHandler extends DefaultSearchDataSourceHandler
{

    private static final String ASSIGNMENT = "base";
    private static final String EXTRACT = "ext";
    private static final String CHANGE_EXTRACT = "ce";

    public PrPracticeAssignmentChangeDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        Long orderToChangeId = context.get(ORDER_TO_CHANGE_F);
        Long respOrgUnit = context.get(RESPONSIBLE_ORG_UNIT_F);
        Date startDate = context.get(START_DATE);
        Date endDate = context.get(END_DATE);
        if (orderToChangeId == null || respOrgUnit == null || startDate == null || endDate == null)
            return ListOutputBuilder.get(input, Collections.emptyList()).pageable(true).build();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PrPracticeExtract.class, EXTRACT)
                .joinPath(DQLJoinType.inner, PrPracticeExtract.entity().fromAlias(EXTRACT), ASSIGNMENT)
                .column(EXTRACT)
                .where(eq(property(PrPracticeExtract.paragraph().order().fromAlias(EXTRACT)), value(orderToChangeId)))
                .where(eq(property(PrPracticeAssignment.practice().registryElementPart().registryElement().owner().id().fromAlias(ASSIGNMENT)),
                        value(respOrgUnit)))
                .where(inDay(PrPracticeAssignment.dateStart().fromAlias(ASSIGNMENT), startDate))
                .where(inDay(PrPracticeAssignment.dateEnd().fromAlias(ASSIGNMENT), endDate))
                ;

        // фильтрация ассайментов на которые еще не создано выписок на практику в других параграфах.
        DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(PrPracticeChangeExtract.class, "ppce")
                .where(eq(property("ppce", PrPracticeChangeExtract.entity().id()), property(EXTRACT, PrPracticeExtract.id())));

        // фильтрация по paragraphId
        Long paragraphId = context.get(PARAGRAPH_ID);
        if (paragraphId != null) {
            sub.where(ne(property("ppce", PrPracticeChangeExtract.paragraph().id()), value(paragraphId)));
        }
        builder.where(notExists(sub.buildQuery()));

        // фильтрация по базе практики
        IDQLExpression practiceBaseCondition = ne(
                property(PrPracticeExtract.practiceBase().fromAlias(EXTRACT)),
                property(PrPracticeAssignment.practiceBase().fromAlias(ASSIGNMENT)));

        IDQLExpression finalCondition = and(practiceBaseCondition);
        builder.where(finalCondition);

        // Order
        builder.order(property(PrPracticeAssignment.practice().student().person().identityCard().fullFio().fromAlias(ASSIGNMENT)));
        builder.order(property(PrPracticeAssignment.practice().student().id().fromAlias(ASSIGNMENT)));
        // !Order

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }

}
