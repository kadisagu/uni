package ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderType;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;

import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderConstants.EDUCATION_YEAR_DS;
import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderConstants.ORDER_TYPE_DS;

@Configuration
public class PracticeOrderAddEdit extends BusinessComponentManager {

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORDER_TYPE_DS, PracticeOrderManager.instance().orderTypeDSHandler()).addColumn(PrPracticeOrderType.P_TITLE))
                .addDataSource(selectDS(EDUCATION_YEAR_DS, PracticeOrderManager.instance().educationYearDSHandler()).addColumn(EducationYear.P_TITLE))
                .create();
    }
}
