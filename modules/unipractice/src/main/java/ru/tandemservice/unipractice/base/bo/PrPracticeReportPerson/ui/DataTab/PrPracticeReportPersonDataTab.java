/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.DataTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.DataTab.block.practiceData.PracticeDataBlock;

/**
 * @author nvankov
 * @since 10/28/14
 */
@Configuration
public class PrPracticeReportPersonDataTab extends BusinessComponentManager
{
    // tab block lists
    public static final String PR_PRACTICE_DATA_BLOCK_LIST = "prPracticeDataBlockList";

    // block names
    public static final String PRACTICE_DATA = "practiceData";


    @Bean
    public BlockListExtPoint prPracticeDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(PR_PRACTICE_DATA_BLOCK_LIST)
                .addBlock(htmlBlock(PRACTICE_DATA, "block/practiceData/PracticeData"))
                .create();
    }
    @Bean
    public IDefaultComboDataSourceHandler practiceEmploymentFormDSHandler()
    {
        return PracticeDataBlock.createPracticeEmploymentFormDS(getName());
    }


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(PracticeDataBlock.PRACTICE_EMPLOYMENT_FORM_DS, practiceEmploymentFormDSHandler()))
                .create();
    }

}



    