package ru.tandemservice.unipractice.order.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.order.entity.gen.PrPracticeExtractGen;

/**
 * Выписка о направлении на практику
 */
public class PrPracticeExtract extends PrPracticeExtractGen
{
    @Override
    public ICatalogItem getType()
    {
        return getParagraph().getOrder().getType();
    }

    @Override
    public void setState(ICatalogItem state)
    {
        if (state instanceof ExtractStates || state == null) {
            setState((ExtractStates) state);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String getTitle()
    {
        if (getParagraph() == null) {
            return this.getClass().getSimpleName();
        }
        return "Выписка " + getNumber() + "/" + getParagraph().getNumber() + " | " + getParagraph().getOrder().getTitle() + " | " + getEntity().getPractice().getStudent().getPerson().getFullFio();
    }

    @Override
    public void setEntity(Object o)
    {
        setEntity((PrPracticeAssignment)o);// TODO:!!!
    }

    @Override
    public boolean isCommitted()
    {
        return UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(getState().getCode());
    }

    @Override
    public void setCommitted(boolean b)
    {
    }

    @Override
    public PrPracticeAssignment getPracticeAssignment()
    {
        return getEntity();
    }
}