package ru.tandemservice.unipractice.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import ru.tandemservice.unipractice.entity.gen.PrPracticeAssignmentGen;

import java.util.Collection;
import java.util.Collections;

/**
 * Направление на практику
 */
public class PrPracticeAssignment extends PrPracticeAssignmentGen implements ISecLocalEntityOwner {
    @Override
    public Collection<IEntity> getSecLocalEntities() {
        return getPractice() != null ? getPractice().getStudent().getSecLocalEntities() : Collections.<IEntity>emptyList();
    }
}