package ru.tandemservice.unipractice.practice.bo.PracticeResult.ui.ScanCopyAdd;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unipractice.entity.PrPracticeResult;
import ru.tandemservice.unipractice.practice.bo.PracticeResult.PracticeResultManager;

import static ru.tandemservice.unipractice.practice.bo.PracticeResult.logic.PracticeResultConstants.PRACTICE_RESULT_ID;

@Input({
    @Bind(key = PRACTICE_RESULT_ID, binding = PRACTICE_RESULT_ID, required = true)
})
public class PracticeResultScanCopyAddUI extends UIPresenter {

    private Long practiceResultId;
    private PrPracticeResult practiceResult;
    private IUploadFile scanCopyUploadFile;

    @Override
    public void onComponentRefresh() {
        practiceResult = DataAccessServices.dao().get(PrPracticeResult.class, practiceResultId);
    }

    public void onClickApply(){
        if(scanCopyUploadFile != null){
            PracticeResultManager.instance().practiceResultDao().saveScanCopy(practiceResult, scanCopyUploadFile);
        }
        deactivate();
    }

    public Long getPracticeResultId() {
        return practiceResultId;
    }

    public void setPracticeResultId(Long practiceResultId) {
        this.practiceResultId = practiceResultId;
    }

    public PrPracticeResult getPracticeResult() {
        return practiceResult;
    }

    public void setPracticeResult(PrPracticeResult practiceResult) {
        this.practiceResult = practiceResult;
    }

    public IUploadFile getScanCopyUploadFile() {
        return scanCopyUploadFile;
    }

    public void setScanCopyUploadFile(IUploadFile scanCopyUploadFile) {
        this.scanCopyUploadFile = scanCopyUploadFile;
    }
}
