package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.List;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.*;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.gen.EducationLevelsHighSchoolGen;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeAssignmentState;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.PrPracticeAssignmentManager;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentDSHandler;

import java.util.ArrayList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.*;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentDSHandler.*;

@Configuration
public class PrPracticeAssignmentList extends BusinessComponentManager {

    private static final Long STUDENT_STATUS_ACTIVE = 0L;//опция "активные студенты"
    private static final Long STUDENT_STATUS_NON_ACTIVE = 1L;//опция "неактивные студенты"

    @Bean
    public ColumnListExtPoint practiceAssignmentDS() {
        StudentGen.Path<Student> studentPath = PrPracticeAssignment.practice().student();
        EducationLevelsHighSchoolGen.Path<EducationLevelsHighSchool> highSchool = PrPracticeAssignment.practice().student()
                .educationOrgUnit().educationLevelHighSchool();

        IColumnListExtPointBuilder builder = columnListExtPointBuilder(PRACTICE_ASSIGNMENT_DS)
                .addColumn(indicatorColumn(STUDENT_C).defaultIndicatorItem(new IndicatorColumn.Item("student")))
                .addColumn(publisherColumn(FIO_C, studentPath.person().identityCard().fullFio()).order().required(true))
                .addColumn(textColumn(FORMATIVE_ORG_UNIT_C, studentPath.educationOrgUnit().formativeOrgUnit().fullTitle()).order())
                .addColumn(textColumn(GROUP_C, studentPath.group().title()).order())
                .addColumn(textColumn(COMPENSATION_TYPE_C, studentPath.compensationType().shortTitle()).order())
                .addColumn(textColumn("targetAdmission", PrPracticeAssignment.practice().student().s()).formatter(source -> {
                    final Student student = (Student) source;

                    if (student.isTargetAdmission() && student.getTargetAdmissionOrgUnit() != null) {

                        return student.getTargetAdmissionOrgUnit().getTitleWithLegalForm() +
                                (student.getTargetAdmissionOrgUnit().getLegalAddress() != null && student.getTargetAdmissionOrgUnit().getLegalAddress().getSettlement() != null
                                        ? ", " + student.getTargetAdmissionOrgUnit().getLegalAddress().getSettlement().getTitle()
                                        : (student.getTargetAdmissionOrgUnit().getFactAddress() != null && student.getTargetAdmissionOrgUnit().getFactAddress().getSettlement() != null
                                        ? ", " + student.getTargetAdmissionOrgUnit().getFactAddress().getSettlement().getTitle()
                                        : ""));
                    }

                    return YesNoFormatter.STRICT.format(student.isTargetAdmission());
                }))
                .addColumn(textColumn(PRACTICE_TYPE, PrPracticeAssignment.practice().registryElementPart().registryElement().parent().title()).order())
                .addColumn(textColumn(SPECIALITY_C, highSchool.educationLevel().programSubjectWithCodeIndexAndGenTitle()).order())
                .addColumn(textColumn(SPECIALIZATION_C, highSchool.educationLevel().programSpecializationTitle()))
                .addColumn(textColumn(DEVELOP_FORM_C, studentPath.educationOrgUnit().developForm().title()).order())
                .addColumn(textColumn(ASSIGNMENT_STATE_C, PrPracticeAssignment.state().title()).order())
                .addColumn(textColumn(PRACTICE_BASE_C_TITLE, PrPracticeAssignment.practiceBase().title()).order())
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrintEntityFromList").disabled(ASSIGNMENT_PRINT_DISABLED).permissionKey("printPrPracticeAssignmentFromList"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled(ASSIGNMENT_EDIT_DELETE_DISABLED).permissionKey("editPrPracticeAssignmentFromList"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                                        new FormattedMessage("practiceAssignmentDS.delete.alert", studentPath.person().identityCard().fullFio().s()))
                                   .disabled(ASSIGNMENT_EDIT_DELETE_DISABLED).permissionKey("deletePrPracticeAssignmentFromList")
                );
        return builder.create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler practiceAssignmentDSHandler() {
        return new PracticeAssignmentDSHandler(getName());
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRACTICE_ASSIGNMENT_DS, practiceAssignmentDS(), practiceAssignmentDSHandler()))
                .addDataSource(this.selectDS(F_STUDENT_STATUS_OPTION_DS, studentStatusOptionDSHandler()))
                .addDataSource(this.selectDS(F_EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(this.selectDS(F_PRACTICE_KINDS_DS, practiceKindsDSHandler()))
                .addDataSource(this.selectDS(F_STUDENT_STATE_DS, studentStateDSHandler()))
                .addDataSource(this.selectDS(F_COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.shortTitle().s()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(this.selectDS(F_PRACTICE_BASE_DS, PrPracticeAssignmentManager.instance().practiceBaseDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(this.selectDS(F_FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
                .addDataSource(this.selectDS(F_PRODUCING_ORG_UNIT_DS, producingOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
                .addDataSource(this.selectDS(F_REFERRAL_STATE_DS, practiceAssignmentStateDSHandler()))
                .addDataSource(this.selectDS(F_EDU_LEVEL_HIGH_SCHOOL_DS, educationLevelHighSchoolDSHandler()).addColumn(EducationLevelsHighSchool.fullTitle().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler studentStatusOptionDSHandler() {
        ArrayList<IdentifiableWrapper> students = Lists.newArrayList(
                new IdentifiableWrapper(STUDENT_STATUS_ACTIVE, "активные студенты"),
                new IdentifiableWrapper(STUDENT_STATUS_NON_ACTIVE, "неактивные студенты")
        );
        return new SimpleTitledComboDataSourceHandler(getName()).addAll(students);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationYear.class)
                .filter(EducationYear.title())
                .order(EducationYear.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceKindsDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), EppRegistryStructure.class, EppRegistryStructure.title()) {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
                super.prepareConditions(ep);

                ep.dqlBuilder.where(eq(
                        property(EppRegistryStructure.parent().code().fromAlias("e")),
                        value(EppRegistryStructureCodes.REGISTRY_PRACTICE)));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler studentStateDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), StudentStatus.class, StudentStatus.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), CompensationType.class, CompensationType.shortTitle());
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), OrgUnit.class, OrgUnit.fullTitle()) {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
                super.prepareConditions(ep);

                ep.dqlBuilder.where(exists(EducationOrgUnit.class, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, property("e")));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler producingOrgUnitDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), OrgUnit.class, OrgUnit.fullTitle()) {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
                super.prepareConditions(ep);
                ep.dqlBuilder.where(exists(EducationOrgUnit.class, EducationOrgUnit.educationLevelHighSchool().orgUnit().s(), property("e")));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceAssignmentStateDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), PrPracticeAssignmentState.class, PrPracticeAssignmentState.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler educationLevelHighSchoolDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), EducationLevelsHighSchool.class, EducationLevelsHighSchool.fullTitle());
    }

}
