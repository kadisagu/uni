package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.PreDipAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.PracticeParagraphManager;

import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Configuration
public class PracticeParagraphPreDipAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRACTICE_ASSIGNMENT_DS, practiceAssignmentColumnListDS(), PracticeParagraphManager.instance().practiceAssignmentDSHandler()))
                .addDataSource(selectDS(RESPONSIBLE_ORG_UNIT_DS, PracticeParagraphManager.instance().practiceParagraphResponsibleOrgUnitDSHandler()))
                .addDataSource(selectDS(START_DATE_DS, PracticeParagraphManager.instance().practiceParagraphStartDateDSHandler()))
                .addDataSource(selectDS(END_DATE_DS, PracticeParagraphManager.instance().practiceParagraphEndDateDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint practiceAssignmentColumnListDS()
    {
        StudentGen.Path<Student> studentPath = PrPracticeAssignment.practice().student();
        return columnListExtPointBuilder(PRACTICE_ASSIGNMENT_DS)
                .addColumn(checkboxColumn("checkbox"))
                .addColumn(textColumn(FIO_C, studentPath.person().identityCard().fullFio()))
                .addColumn(textColumn(GROUP_C, studentPath.group().title()))
                .addColumn(textColumn(FORMATIVE_ORG_UNIT_C, studentPath.educationOrgUnit().formativeOrgUnit().fullTitle()))
                .addColumn(textColumn(RESPONSIBLE_ORG_UNIT_C, PrPracticeAssignment.practice().registryElementPart().registryElement().owner().title()))
                .addColumn(textColumn(PRACTICE_BASE_C, PrPracticeAssignment.practiceBase().title()))
                .create();
    }
}