package ru.tandemservice.unipractice.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид баз практик"
 * Имя сущности : prPracticeBaseKind
 * Файл data.xml : unipractice.catalog.data.xml
 */
public interface PrPracticeBaseKindCodes
{
    /** Константа кода (code) элемента : Образовательная организация (title) */
    String INTERNAL = "1";

    Set<String> CODES = ImmutableSet.of(INTERNAL);
}
