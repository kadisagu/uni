package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ChangePub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.block.IBlockListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.gen.PrPracticeAssignmentGen;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.ParagraphChangeExtractsDSHandler;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.AbstractPub.AbstractPracticeParagraphPub;
import ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;

import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Configuration
public class PracticeParagraphChangePub extends AbstractPracticeParagraphPub {

    private static final String PRACTICE_ORDER_PARAGRAPH_BLOCK_LIST = "practiceOrderParagraphChangeBlockList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        IPresenterExtPointBuilder builder = presenterExtPointBuilder()
                .addDataSource(searchListDS(PRACTICE_EXTRACT_LIST_DS, practiceExtractListDS(), paragraphExtractsDSHandler()));
        return presenterExtPoint(builder);
    }

    @Bean
    public ColumnListExtPoint practiceExtractListDS() {
        StudentGen.Path<Student> studentPath = PrPracticeExtract.entity().practice().student();
        PrPracticeAssignmentGen.Path<PrPracticeAssignment> assignmentPath = PrPracticeExtract.entity();
        return columnListExtPointBuilder(PRACTICE_EXTRACT_LIST_DS)
                .addColumn(publisherColumn(FIO_C, PrPracticeChangeExtract.L_ENTITY + "." + studentPath.person().identityCard().fullFio()).businessComponent(IUniComponents.STUDENT_PUB)
                        .parameters("ui:publisherStudentTabParametersMap"))
                .addColumn(textColumn(GROUP_C, PrPracticeChangeExtract.L_ENTITY + "." + studentPath.group().title()))
                .addColumn(textColumn(FORMATIVE_ORG_UNIT_C, PrPracticeChangeExtract.L_ENTITY + "." + studentPath.educationOrgUnit().formativeOrgUnit().fullTitle()))
                .addColumn(textColumn(RESPONSIBLE_ORG_UNIT_C, PrPracticeChangeExtract.L_ENTITY + "." + assignmentPath.practice().registryElementPart().registryElement().owner().fullTitle()))
                .addColumn(textColumn(PRACTICE_BASE_C, PrPracticeExtract.entity().practiceBase().title()))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                        new FormattedMessage(PRACTICE_EXTRACT_LIST_DS + ".delete.alert", PrPracticeChangeExtract.L_ENTITY + "." + studentPath.fullFio())).disabled("mvel:!presenter.editable")
                        .permissionKey("ui:secModel.deleteExtract"))
                .create();
    }
    @Bean
    public BlockListExtPoint practiceParagraphChangeBlockListExtPoint(){
        IBlockListExtPointBuilder builder = blockListExtPointBuilder(PRACTICE_ORDER_PARAGRAPH_BLOCK_LIST);
        return practiceParagraphChangeBlockListExtPoint(builder);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> paragraphExtractsDSHandler() {
        return new ParagraphChangeExtractsDSHandler(getName());
    }

}
