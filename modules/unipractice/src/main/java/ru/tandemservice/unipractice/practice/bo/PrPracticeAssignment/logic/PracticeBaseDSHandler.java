package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unipractice.entity.PrPracticeBase;

import static org.tandemframework.core.CoreStringUtils.escapeLike;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class PracticeBaseDSHandler extends EntityComboDataSourceHandler
{
    public PracticeBaseDSHandler(String name) {
        super(name, PrPracticeBase.class);
        customize((alias, dql, context, filter) -> {
            PrPracticeBase.addJoinsForUseTitle(dql, alias, "extOrgUnit", "extOuParent", "legalForm", "intOrgUnit");

            if (StringUtils.isNotEmpty(filter)) {
                dql.where(likeUpper(titleExpression(), value(escapeLike(filter, true))));
            }

            dql.where(eq(property(PrPracticeBase.archival().fromAlias(ALIAS)), value(false)));

            dql.order(titleExpression());

            return dql;
        });
    }

    private IDQLExpression titleExpression()
    {
        return PrPracticeBase.createTitleDQLExpression("extOrgUnit", "extOuParent", "legalForm", "intOrgUnit");
    }
}