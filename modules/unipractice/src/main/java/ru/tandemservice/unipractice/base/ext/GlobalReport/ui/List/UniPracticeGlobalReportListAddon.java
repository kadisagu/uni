/* $Id$ */
package ru.tandemservice.unipractice.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

/**
 * @author Andrey Avetisov
 * @since 09.10.2014
 */
public class UniPracticeGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "uniPracticeGlobalReportListAddon";

    public UniPracticeGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}