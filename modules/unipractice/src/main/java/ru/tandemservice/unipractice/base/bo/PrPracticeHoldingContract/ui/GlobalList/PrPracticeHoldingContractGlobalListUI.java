/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContract.ui.GlobalList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContract.logic.PrPracticeHoldingContractDSHandler;

/**
 * @author azhebko
 * @since 06.11.2014
 */
public class PrPracticeHoldingContractGlobalListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(this.getSettings().getAsMap(true, PrPracticeHoldingContractDSHandler.BIND_CONTRACT_NUMBER, PrPracticeHoldingContractDSHandler.BIND_PRACTICE_BASE));
    }

    public void onClickSearch()
    {
        this.saveSettings();
    }

    public void onClickClear()
    {
        this.clearSettings();
        onClickSearch();
    }
}