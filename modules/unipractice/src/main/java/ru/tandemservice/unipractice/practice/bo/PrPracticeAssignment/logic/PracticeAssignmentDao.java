package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic;

import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.hibernate.Session;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ExternalOrgUnitManager;
import ru.tandemservice.unipractice.entity.*;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeAssignmentState;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderPrintForm;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderPrintFormCodes;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes.*;

public class PracticeAssignmentDao extends CommonDAO implements IPracticeAssignmentDao {

    @Override
    public void updatePracticeAssignmentState(Long prPracticeAssignmentId, String newState) {
        PrPracticeAssignment practiceAssignment = getNotNull(prPracticeAssignmentId);
        String oldState = practiceAssignment.getState().getCode();

        if (oldState.equals(FORMING) && newState.equals(ACCEPTABLE) ||
                oldState.equals(ACCEPTABLE) && newState.equals(FORMING) ||
                oldState.equals(ACCEPTABLE) && newState.equals(ACCEPTED) ||
                oldState.equals(ACCEPTED) && newState.equals(FOR_APPROVAL) ||
                oldState.equals(ACCEPTED) && newState.equals(FORMING) ||
                oldState.equals(FOR_APPROVAL) && newState.equals(FINISHED) ||
                oldState.equals(FOR_APPROVAL) && newState.equals(ACCEPTED) ||
                oldState.equals(FINISHED) && newState.equals(ACCEPTED) ||
                oldState.equals(FINISHED) && newState.equals(FORMING)) {
        } else {
            throw new IllegalStateException("Wrong PrPracticeAssignmentStateCode: old - " + oldState + ", new - " + newState);
        }

        PrPracticeAssignmentState state = getByCode(PrPracticeAssignmentState.class, newState);
        practiceAssignment.setState(state);
        saveOrUpdate(practiceAssignment);
    }
    @Override
    public void addGuaranteeLetterToPracticeAssignment(PrPracticeAssignment practiceAssignment, IUploadFile scanCopyUploadFile)
    {
        DatabaseFile databaseFile = practiceAssignment.getGuaranteeCopy();
        if(null==databaseFile)
        {
            databaseFile = new DatabaseFile();
            practiceAssignment.setGuaranteeCopy(databaseFile);
        }
        try
        {
            databaseFile.setContent(IOUtils.toByteArray(scanCopyUploadFile.getStream()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        databaseFile.setContentType(CommonBaseUtil.getContentType(scanCopyUploadFile));
        databaseFile.setFilename(scanCopyUploadFile.getFileName());
        saveOrUpdate(databaseFile);
        saveOrUpdate(practiceAssignment);
    }

    @Override
    public void deleteGuaranteeLetterFromPracticeAssignment(PrPracticeAssignment practiceAssignment)
    {
        DatabaseFile databaseFile = practiceAssignment.getGuaranteeCopy();
        practiceAssignment.setGuaranteeCopy(null);
        practiceAssignment.setGuaranteeNumber(null);
        practiceAssignment.setGuaranteeDate(null);
        saveOrUpdate(practiceAssignment);
        delete(databaseFile);
    }

    @Override
    public void addRequestForRegPracticeBase(PrPracticeBaseRequest prPracticeBaseRequest)
    {
        validateRequest(prPracticeBaseRequest);

        if(prPracticeBaseRequest instanceof PrPracticeBaseRequestExt)
        {
            if(((PrPracticeBaseRequestExt)prPracticeBaseRequest).getGuaranteeLetterCopy() !=null)
            {
                save(((PrPracticeBaseRequestExt)prPracticeBaseRequest).getGuaranteeLetterCopy());
                getSession().flush();
            }
        }

        save(prPracticeBaseRequest);
    }

    private void validateRequest(PrPracticeBaseRequest prPracticeBaseRequest)
    {
        if(prPracticeBaseRequest instanceof PrPracticeBaseRequestInt)
        {
            if(ISharedBaseDao.instance.get().existsEntity(PrPracticeBaseRequestInt.class, PrPracticeBaseRequestInt.orgUnit().s(), ((PrPracticeBaseRequestInt) prPracticeBaseRequest).getOrgUnit()))
                throw new ApplicationException("Для этого подразделения уже создана заявка на регистрацию базы прохождения практики.");
        }
        else if(prPracticeBaseRequest instanceof PrPracticeBaseRequestExt)
        {
            if(((PrPracticeBaseRequestExt) prPracticeBaseRequest).getOrgUnit() != null && ISharedBaseDao.instance.get().existsEntity(PrPracticeBaseRequestExt.class, PrPracticeBaseRequestExt.orgUnit().s(), ((PrPracticeBaseRequestExt) prPracticeBaseRequest).getOrgUnit()))
                throw new ApplicationException("Для этого подразделения уже создана заявка на регистрацию базы прохождения практики.");
            else if(((PrPracticeBaseRequestExt) prPracticeBaseRequest).getOrgUnit() == null && ISharedBaseDao.instance.get().existsEntity(
                    new DQLSelectBuilder().fromEntity(PrPracticeBaseRequestExt.class, "ext")
                            .where(eq(property("ext", PrPracticeBaseRequestExt.orgUnitTitle()), value(((PrPracticeBaseRequestExt) prPracticeBaseRequest).getOrgUnitTitle())))
                            .where(eq(property("ext", PrPracticeBaseRequestExt.orgUnitShortTitle()), value(((PrPracticeBaseRequestExt) prPracticeBaseRequest).getOrgUnitShortTitle())))
                            .where(eq(property("ext", PrPracticeBaseRequestExt.orgUnitLegalForm()), value(((PrPracticeBaseRequestExt) prPracticeBaseRequest).getOrgUnitLegalForm())))
                            .buildQuery()
            ))
            {
                throw new ApplicationException("Для этого подразделения уже создана заявка на регистрацию базы прохождения практики.");
            }
        }
    }

    @Override
    public NavigableMap<Double, List<DataWrapper>> getSimilarExternalOrgUnit(String title)
    {
        NavigableMap<Double, List<DataWrapper>> similarMap = ExternalOrgUnitManager.instance().dao().getSimilarExternalOrgUnit(title);

        List<Long> ouIds = Lists.newArrayList();
        for(List<DataWrapper> wrappers : similarMap.values())
        {
            for(DataWrapper wrapper : wrappers)
                ouIds.add(wrapper.getId());
        }

        final List<Long> removeIds = Lists.newArrayList();

        final Session session = getSession();

        BatchUtils.execute(ouIds, 100, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                removeIds.addAll(new DQLSelectBuilder().fromEntity(PrPracticeBaseExt.class, "ext")
                        .column(property("ext", PrPracticeBaseExt.orgUnit().id()))
                        .where(in(property("ext", PrPracticeBaseExt.orgUnit().id()), elements))
                        .createStatement(session).<Long>list());
            }
        });

        if(removeIds.isEmpty())
            return similarMap;
        else
        {
            TreeMap<Double, List<DataWrapper>> resultSimilarMap = new TreeMap<>();

            for (NavigableMap.Entry<Double, List<DataWrapper>> entry : similarMap.entrySet())
            {
                for (DataWrapper wrapper : entry.getValue())
                {
                    if (!removeIds.contains(wrapper.getId()))
                    {
                        List<DataWrapper> records = resultSimilarMap.get(entry.getKey());
                        if (records == null)
                            resultSimilarMap.put(entry.getKey(), records = Lists.newArrayList());
                        records.add(wrapper);
                    }
                }
            }

            return resultSimilarMap.descendingMap();
        }
    }

    @Override
    public void printPracticeAssignment(Long practiceAssignmentId)
    {
        final IScriptItem printScript = getNotNull(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintForm.code(), PrPracticeOrderPrintFormCodes.PRACTICE_ASSIGNMENT);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(printScript, practiceAssignmentId);
    }
}
