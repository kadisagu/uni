/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.StudentPreallocationManager;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic.StPreallocationListDSHandelr;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.ui.AddEdit.StudentPreallocationAddEdit;

/**
 * @author Andrey Avetisov
 * @since 09.10.2014
 */
@State({
               @Bind(key = "orgUnitId", binding = "orgUnitId")
       })
public class StudentPreallocationListUI extends UIPresenter
{
    public static final String ORG_UNIT_ID = "orgUnitId";
    private Long _orgUnitId;

    public void onClickAddReport()
    {
        _uiActivation.asCurrent(StudentPreallocationAddEdit.class)
                .parameter("orgUnitId", getOrgUnitId())
                .activate();
    }

    public void onClickPrint()
    {
        StPracticePreallocation practicePreallocation = DataAccessServices.dao().get(getListenerParameterAsLong());

        if (practicePreallocation.getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(practicePreallocation.getContent()),
                false);

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (StudentPreallocationList.ST_PREALLOCATION_LIST_DS.equals(dataSource.getName()))
        {
            dataSource.put(StPreallocationListDSHandelr.ORG_UNIT_ID, getOrgUnitId());
        }
    }

    // Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public String getViewKey()
    {
        String permissionKey = ((StudentPreallocationManager) getConfig().getBusinessObjectManager()).getPermissionKey();
        return getOrgUnitId() == null ? permissionKey : new OrgUnitSecModel(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId)).getPermission("orgUnit_" + permissionKey);
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitId != null ? DataAccessServices.dao().get(OrgUnit.class, _orgUnitId) : super.getSecuredObject();
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return new CommonPostfixPermissionModel(_orgUnitId != null ? OrgUnitSecModel.getPostfix(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId)) : null);
    }

    public String getViewPermissionKey(){ return getSec().getPermission(_orgUnitId != null ? "orgUnit_viewStPracticePreallocationReportPermissionKey" : "stPracticePreallocationReportPermissionKey"); }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission(_orgUnitId != null ? "orgUnit_addStPracticePreallocationReportPermissionKey" : "stPracticePreallocationReportPermissionKey"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission(_orgUnitId != null ? "orgUnit_deleteStPracticePreallocationReportPermissionKey" : "stPracticePreallocationReportPermissionKey"); }


    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
