/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.logic;

import com.google.common.collect.Maps;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractKindSelectWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrVersionTemplatePromiceRestrictions;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.ui.Add.PrPracticeHoldingContractTemplateAdd;
import ru.tandemservice.unipractice.catalog.entity.codes.CtrContractKindCodes;
import ru.tandemservice.unipractice.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unipractice.entity.PrPracticeBaseContract;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;
import ru.tandemservice.unipractice.entity.PrPracticeHoldingContractTemplateData;

import java.util.*;

/**
 * @author azhebko
 * @since 07.11.2014
 */
public class PrPracticeHoldingContractTemplateDao extends UniBaseDao implements IPrPracticeHoldingContractTemplateDao
{
    @Override
    public void doSavePracticeHoldingContract(CtrContractVersion contractVersion, PrPracticeBaseExt practiceBase, CtrContractVersionCreateData versionCreateData, Date docStartDate)
    {
        CtrContractObject contract = new CtrContractObject();
        contract.setType(this.getCatalogItem(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_OB_ORGANIZATSII_I_PROVEDENII_PRAKTIKI_STUDENTOV));
        contract.setNumber(contractVersion.getNumber());
        contract.setOrgUnit(TopOrgUnit.getInstance());

        this.save(contract);

        contractVersion.setContract(contract);
        contractVersion.setCreationDate(new Date());
        contractVersion.setCreator(UserContext.getInstance().getPrincipalContext());
        contractVersion.setKind(versionCreateData.getContractKind());
        contractVersion.setPrintTemplate(versionCreateData.getPrintTemplate());
        contractVersion.setDocStartDate(docStartDate);

        this.save(contractVersion);

        PrPracticeHoldingContractTemplateData versionTemplateData = new PrPracticeHoldingContractTemplateData();
        versionTemplateData.setOwner(contractVersion);

        this.save(versionTemplateData);

        this.save(new PrPracticeBaseContract(contract, practiceBase));
    }

    @Override
    public Map<CtrContractType, List<CtrContractKindSelectWrapper>> getVersionAddComponent(final Long contextEntityId)
    {
        if (PrPracticeBaseExt.class.isAssignableFrom(EntityRuntime.getMeta(contextEntityId).getEntityClass()))
        {
            CtrContractType contractType = this.getCatalogItem(CtrContractType.class, CtrContractTypeCodes.DOGOVOR_OB_ORGANIZATSII_I_PROVEDENII_PRAKTIKI_STUDENTOV);
            Map<CtrContractType, List<CtrContractKindSelectWrapper>> map = Maps.newHashMap();
            SafeMap.safeGet(map, contractType, ArrayList.class).add(new CtrContractKindSelectWrapper(this.getCatalogItem(CtrContractKind.class, CtrContractKindCodes.CONTRACT_PRACTICE), PrPracticeHoldingContractTemplateAdd.class));
            return map;
        }

        return Collections.emptyMap();
    }

    @Override
    public ICtrVersionTemplatePromiceRestrictions getPromiceRestrictions(Class<? extends CtrContractPromice> promiceClass, CtrContractVersionTemplateData templateData)
    {
        return ICtrVersionTemplatePromiceRestrictions.DENY_ALL;
    }

    @Override public IDocumentRenderer print(CtrContractVersionTemplateData templateData) { return null; }

    @Override public boolean isAllowCreateNextVersionByTemplate(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }

    @Override public boolean isAllowCreateNextVersionByCopy(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }

    @Override public boolean isAllowDeleteTemplate(CtrContractVersionTemplateData ctrContractVersionTemplateData) { return false; }
}