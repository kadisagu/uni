package ru.tandemservice.unipractice.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipractice_2x6x8_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность prPracticeBaseRequestExt

		// создано свойство orgUnitTitle
        if(!tool.columnExists("pr_practice_base_request_ext_t", "orgunittitle_p"))
		{
			// создать колонку
			tool.createColumn("pr_practice_base_request_ext_t", new DBColumn("orgunittitle_p", DBType.createVarchar(255)));

		}

		// создано свойство orgUnitShortTitle
        if(!tool.columnExists("pr_practice_base_request_ext_t", "orgunitshorttitle_p"))
		{
			// создать колонку
			tool.createColumn("pr_practice_base_request_ext_t", new DBColumn("orgunitshorttitle_p", DBType.createVarchar(255)));

		}

		// создано свойство orgUnitLegalForm
        if(!tool.columnExists("pr_practice_base_request_ext_t", "orgunitlegalform_id"))
		{
			// создать колонку
			tool.createColumn("pr_practice_base_request_ext_t", new DBColumn("orgunitlegalform_id", DBType.LONG));

		}

		//  свойство orgUnit стало необязательным
        if(!tool.isColumnNullable("pr_practice_base_request_ext_t", "orgunit_id"))
		{
			// сделать колонку NULL
			tool.setColumnNullable("pr_practice_base_request_ext_t", "orgunit_id", true);
		}


    }
}