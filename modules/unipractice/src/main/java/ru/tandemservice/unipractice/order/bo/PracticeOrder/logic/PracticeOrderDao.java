package ru.tandemservice.unipractice.order.bo.PracticeOrder.logic;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderPrintForm;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderPrintFormCodes;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;
import ru.tandemservice.unipractice.order.entity.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.max;

public class PracticeOrderDao extends UniBaseDao implements IPracticeOrderDao {

    private static final String ALIAS = "e";

    private static final Comparator<PrPracticeAssignment> ASSIGNMENT_COMPARATOR = CommonCollator.comparing(e -> e.getPractice().getStudent().getFullFio(), true);
    private static final Comparator<PrPracticeExtract> EXTRACT_COMPARATOR = CommonCollator.comparing(e -> e.getEntity().getPractice().getStudent().getFullFio(), true);

    @Override
    public boolean isOrderNumberValid(PrPracticeOrder practiceOrder) {
        final int year = CoreDateUtils.getYear(practiceOrder.getCreateDate());
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PrPracticeOrder.class, ALIAS).column(ALIAS + ".id");
        builder.where(eq(property(PrPracticeOrder.number().fromAlias(ALIAS)), value(practiceOrder.getNumber())));
        builder.where(ge(property(PrPracticeOrder.commitDate().fromAlias(ALIAS)), valueDate(CoreDateUtils.getYearFirstTimeMoment(year))));
        builder.where(lt(property(PrPracticeOrder.commitDate().fromAlias(ALIAS)), valueDate(CoreDateUtils.getYearFirstTimeMoment(year + 1))));
        if (practiceOrder.getId() != null)
            builder.where(ne(property(ALIAS), value(practiceOrder)));
        return !existsEntity(builder.buildQuery());
    }

    @Override
    @Transactional
    public void deleteOrder(Long orderId) {
        new DQLDeleteBuilder(PrPracticeExtract.class)
                .where(eq(property(PrPracticeExtract.paragraph().order().id()), value(orderId)))
                .createStatement(getSession()).execute();
        new DQLDeleteBuilder(PrPracticeParagraph.class)
                .where(eq(property(PrPracticeParagraph.order().id()), value(orderId)))
                .createStatement(getSession()).execute();
        delete(orderId);
    }

    @Override
    @Transactional
    public void deleteParagraph(Long paragraphId) {
        deleteExtractsByParagraphId(paragraphId);
        delete(paragraphId);
    }

    private int deleteExtractsByParagraphId(Long paragraphId) {
        return new DQLDeleteBuilder(PrAbstractExtract.class)
                .where(eq(property(PrAbstractExtract.paragraph().id()), value(paragraphId)))
                .createStatement(getSession()).execute();
    }

    @Override
    @Transactional
    public PrPracticeParagraph createParagraph(Long orderId, List<PrPracticeAssignment> assignments) {
        PrPracticeParagraph prPracticeParagraph = new PrPracticeParagraph();
        prPracticeParagraph.setOrder(getNotNull(PrPracticeOrder.class, orderId));
        prPracticeParagraph.setNumber(getLastParagraphNumber(orderId) + 1);
        saveOrUpdate(prPracticeParagraph);
        createPracticeExtracts(prPracticeParagraph.getId(), assignments);
        return prPracticeParagraph;
    }

    @Override
    @Transactional
    public void editParagraph(Long paragraphId, List<PrPracticeAssignment> assignments) {
        deleteExtractsByParagraphId(paragraphId);
        createPracticeExtracts(paragraphId, assignments);
    }

    private int getLastParagraphNumber(Long orderId) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PrAbstractParagraph.class, "par")
                .column(max(property(PrAbstractParagraph.number().fromAlias("par"))));
        builder.where(eq(
                property(PrAbstractParagraph.order().id().fromAlias("par")),
                value(orderId)));
        Integer integer = builder.createStatement(getSession()).<Integer>uniqueResult();
        return integer == null ? 0 : integer;
    }

    private void createPracticeExtracts(Long paragraphId, List<PrPracticeAssignment> assignmentList) {
        PrPracticeParagraph prPracticeParagraph = getNotNull(PrPracticeParagraph.class, paragraphId);
        ExtractStates formingState = getByCode(ExtractStates.class, ExtractStatesCodes.FORMING);

        Collections.sort(assignmentList, ASSIGNMENT_COMPARATOR);

        int number = 1;
        for (PrPracticeAssignment assignment : assignmentList) {
            PrPracticeExtract practiceExtract = new PrPracticeExtract();
            practiceExtract.setNumber(number++);
            practiceExtract.setParagraph(prPracticeParagraph);
            practiceExtract.setState(formingState);
            practiceExtract.setEntity(assignment);
            practiceExtract.setPracticeBase(assignment.getPracticeBase());
            practiceExtract.setCreateDate(new Date());
            saveOrUpdate(practiceExtract);
        }
    }

    private void createPracticeChangeExtracts(Long paragraphId, List<PrPracticeExtract> extractList) {
        PrPracticeChangeParagraph paragraph = getNotNull(PrPracticeChangeParagraph.class, paragraphId);
        ExtractStates formingState = getByCode(ExtractStates.class, ExtractStatesCodes.FORMING);

        Collections.sort(extractList, EXTRACT_COMPARATOR);

        int number = 1;
        for (PrPracticeExtract extract : extractList) {
            PrPracticeChangeExtract changeExtract = new PrPracticeChangeExtract();

            changeExtract.setNumber(number++);
            changeExtract.setParagraph(paragraph);
            changeExtract.setState(formingState);
            changeExtract.setEntity(extract);
            changeExtract.setPracticeBase(extract.getEntity().getPracticeBase());
            changeExtract.setCreateDate(new Date());
            saveOrUpdate(changeExtract);
        }
    }

    @Override
    public boolean isOrderInFormingState(Long orderId) {
        return getNotNull(PrPracticeOrder.class, orderId).getState().getCode().equals(OrderStatesCodes.FORMING);
    }

    @Override
    public Map<String, String> getExtractsDuplicates(Long paragraphId, Collection<Long> assignmentIds) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PrPracticeExtract.class, "extract");
        builder.where(
                in(property(PrPracticeExtract.entity().id().fromAlias("extract")), assignmentIds)
        );
        if (paragraphId != null) {
            builder.where(ne(property(PrPracticeExtract.paragraph().id().fromAlias("extract")), value(paragraphId)));
        }
        List<PrPracticeExtract> extracts = getList(builder);

        Map<String, String> result = Maps.newHashMap();
        for (PrPracticeExtract extract : extracts) {
            result.put(
                    extract.getEntity().getPractice().getStudent().getFullFio(),
                    extract.getParagraph().getOrder().getNumber()
            );
        }
        return result;
    }

    @Override
    public Map<String, String> getChangeExtractsDuplicates(Long changeParagraphId, Collection<Long> extractIds) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PrPracticeChangeExtract.class, "ce")
                .column(property("ce"));
        builder.joinEntity("ce", DQLJoinType.inner, PrPracticeExtract.class, "e",
                eq(property(PrPracticeChangeExtract.entity().fromAlias("ce")), property("e")));
        builder.where(
                in(property(PrPracticeExtract.id().fromAlias("e")), extractIds)
        );
        if (changeParagraphId != null) {
            builder.where(ne(property(PrPracticeChangeExtract.paragraph().id().fromAlias("ce")), value(changeParagraphId)));
        }
        List<PrPracticeChangeExtract> extracts = getList(builder);

        Map<String, String> result = Maps.newHashMap();
        for (PrPracticeChangeExtract changeExtract : extracts) {
            PrPracticeExtract extract = (PrPracticeExtract)changeExtract.getEntity();
            result.put(
                    extract.getEntity().getPractice().getStudent().getFullFio(),
                    changeExtract.getParagraph().getOrder().getNumber()
            );
        }
        return result;
    }

    @Override
    public List<PrPracticeAssignment> getAssignmentsByParagraphId(Long paragraphId) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PrPracticeExtract.class, ALIAS)
                .column(property(ALIAS, PrPracticeExtract.entity()))
                .where(eq(property(ALIAS, PrPracticeExtract.paragraph()), value(paragraphId)));
        return builder.createStatement(getSession()).list();
    }

    @Override
    public List<PrPracticeExtract> getExtractsByChangeParagraphId(Long paragraphId) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PrPracticeChangeExtract.class, "ce")
                .column(property("e"));
        builder.joinEntity("ce", DQLJoinType.inner, PrPracticeExtract.class, "e",
                eq(property(PrPracticeChangeExtract.entity().fromAlias("ce")), property("e")));
        builder.where(eq(property("ce", PrPracticeChangeExtract.paragraph()), value(paragraphId)));
        return builder.createStatement(getSession()).list();
    }

    @Override
    public void doSendToCoordination(List<Long> selectedIds, IPersistentPersonable initiator)
    {
        List<PrPracticeOrder> orders = getList(PrPracticeOrder.class, selectedIds);

        for (PrPracticeOrder order : orders)
        {
            VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
            if (visaTask != null)
                throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");
            if (StringUtils.isEmpty(order.getNumber()))
                throw new ApplicationException("Нельзя отправить приказ на согласование без номера.");
            if (order.getCommitDate() == null)
                throw new ApplicationException("Нельзя отправить приказ на согласование без даты приказа.");
            if (order.getParagraphCount() == 0)
                throw new ApplicationException("Нельзя отправить приказ на согласование без параграфов.");
            if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE))
                throw new ApplicationException(orders.size() > 1 ? "Не все выбранные приказы можно отправить на согласование." : "Приказ нельзя отправить на согласование, так как он не в состоянии «Формируется».");
        }
        for (PrPracticeOrder order : orders)
        {
            //2. надо сохранить печатную форму приказа
            PracticeOrderManager.instance().practiceOrderDao().saveEnrollmentOrderText(order);

            //3. отправляем на согласование
            ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
            sendToCoordinationService.init(order, initiator);
            sendToCoordinationService.execute();
        }
    }

    @Override
    public void doCommit(List<Long> selectedIds)
    {
        synchronized (this)
        {
            if (NamedSyncInTransactionCheckLocker.hasLock("enrollmentOrderCommitSync"))
                throw new ApplicationException("Нельзя провести приказ параллельно с проведением другого приказа о зачислении. Повторите попытку проведения через несколько минут.");
            NamedSyncInTransactionCheckLocker.register(getSession(), "enrollmentOrderCommitSync");
        }

        List<PrPracticeOrder> orders = getList(PrPracticeOrder.class, selectedIds);
        for (PrPracticeOrder order : orders)
        {
            if (order.getNumber() == null)
                throw new ApplicationException("Нельзя проводить приказ без номера.");
            if (order.getCommitDate() == null)
                throw new ApplicationException("Нельзя проводить приказ без даты приказа.");
            if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED))
                throw new ApplicationException(orders.size() > 1 ? "Не все выбранные приказы можно провести." : "Приказ нельзя провести, так как он не в состоянии «Согласован».");
        }

        OrderStates stateOrderFinished = UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED);
        ExtractStates stateExtractFinished = UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED);
        for (PrPracticeOrder order : orders)
        {
            //обновляем печатную форму
            PracticeOrderManager.instance().practiceOrderDao().saveEnrollmentOrderText(order);
            MoveDaoFacade.getMoveDao().doCommitOrder(order, stateOrderFinished, stateExtractFinished, null);
        }
    }

    @Override
    public void doSendToFormative(Long orderId)
    {
        PrPracticeOrder order = getNotNull(PrPracticeOrder.class, orderId);

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на формирование, так как он на согласовании.");

        //2. надо сменить состояние на формируется
        order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        update(order);

        ExtractStates stateInOrder = getCatalogItem(ExtractStates.class, ExtractStatesCodes.IN_ORDER);
        for (PrPracticeExtract extract : getList(PrPracticeExtract.class, PrPracticeExtract.paragraph().order().s(), order)) {
            extract.setState(stateInOrder);
            update(extract);
        }
    }

    @Override
    public void doReject(PrPracticeOrder order)
    {
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        } else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(order, UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(order);
            touchService.execute();
        }
    }

    @Override
    public void doRollback(PrPracticeOrder order)
    {
        MoveDaoFacade.getMoveDao().doRollbackOrder(order, UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED), null);
    }

    @Override
    public void saveEnrollmentOrderText(PrPracticeOrder order)
    {
        PrPracticeOrderPrintForm template;
        switch (order.getType().getCode())
        {
            case PrPracticeOrderTypeCodes.PREDDIPLOM:
                template = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.PRE_GRADUATION_PRACTICE_ORDER);
                break;
            case PrPracticeOrderTypeCodes.PROIZVODSTV:
                template = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.INDUSTRIAL_PRACTICE_ORDER);
                break;
            case PrPracticeOrderTypeCodes.PREDDIPLOM_CHANGE:
                template = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.EDIT_PRE_GRADUATION_PRACTICE_ORDER);
                break;
            case PrPracticeOrderTypeCodes.PROIZVODSTV_CHANGE:
                template = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.EDIT_INDUSTRIAL_PRACTICE_ORDER);
                break;
            default: throw new IllegalStateException("Unknown order type");
        }

        Map<String, Object> result = CommonManager.instance().scriptDao().getScriptResult(template, "prPracticeOrder", order);

        // ничего не напечаталось
        if (result == null)
            throw new ApplicationException("Скрипт печати приказа о направлении на практику не создал печатную форму.");

        Object document = result.get(IScriptExecutor.DOCUMENT);
        if (!(document instanceof byte[]))
            throw new ApplicationException("Скрипт печати приказа о направлении на практику не создал печатную форму.");

        Object fileNameObject = result.get(IScriptExecutor.FILE_NAME);
        String fileName = fileNameObject == null ? "prPracticeOrder.rtf" : fileNameObject.toString();
        if (!fileName.endsWith(".rtf"))
            throw new ApplicationException("Скрипт печати приказа о направлении на практику создал неверное имя файла. Имя файла должно иметь расширение rtf.");

        DatabaseFile printForm = order.getPrintForm();
        if (null == printForm) printForm = new DatabaseFile();
        printForm.setContent((byte[]) document);
        printForm.setFilename(fileName);
        printForm.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        getSession().saveOrUpdate(printForm);

        order.setPrintForm(printForm);
        update(order);
    }


    @Override
    public void doExecuteAcceptVisaOrderService(IAbstractOrder order)
    {
        order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED));
        update(order);

        ExtractStates stateAccepted = getCatalogItem(ExtractStates.class, ExtractStatesCodes.ACCEPTED);
        for (PrPracticeExtract extract : getList(PrPracticeExtract.class, PrPracticeExtract.paragraph().order().s(), order)) {
            extract.setState(stateAccepted);
            update(extract);
        }
    }

    @Override
    public void doExecuteRejectVisaOrderService(IAbstractOrder order)
    {
        order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_REJECTED));
        update(order);

        ExtractStates stateInOrder = getCatalogItem(ExtractStates.class, ExtractStatesCodes.IN_ORDER);
        for (PrPracticeExtract extract : getList(PrPracticeExtract.class, PrPracticeExtract.paragraph().order().s(), order)) {
            extract.setState(stateInOrder);
            update(extract);
        }
    }

    @Override
    @Transactional
    public void editChangeParagraph(Long paragraphId, List<PrPracticeExtract> extractList) {
        deleteExtractsByParagraphId(paragraphId);
        createPracticeChangeExtracts(paragraphId, extractList);
    }

    @Override
    @Transactional
    public PrPracticeChangeParagraph createChangeParagraph(Long orderId, List<PrPracticeExtract> extractList) {
        PrPracticeChangeParagraph paragraph = new PrPracticeChangeParagraph();
        paragraph.setOrder(getNotNull(PrPracticeOrder.class, orderId));
        paragraph.setNumber(getLastParagraphNumber(orderId) + 1);
        saveOrUpdate(paragraph);
        createPracticeChangeExtracts(paragraph.getId(), extractList);
        return paragraph;
    }


    @Override
    public void doCommit(PrPracticeExtract extract)
    {
    }

    @Override
    public void doRollback(PrPracticeExtract extract)
    {
    }

    @Override
    public void doCommit(PrPracticeChangeExtract extract)
    {

    }

    @Override
    public void doRollback(PrPracticeChangeExtract extract)
    {

    }


    @Override
    public void getDownloadPrintForm(Long practiceOrderId)
    {
        PrPracticeOrder practiceOrder = getNotNull(PrPracticeOrder.class, practiceOrderId);

        String stateCode = practiceOrder.getState().getCode();

        // если приказ на согласовании, согласован или проведен, то печатная форма должна быть сохранена
        if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode))
        {
            // приказ проведен => печатная форма сохранена
            if (practiceOrder.getPrintForm() == null)
                throw new IllegalStateException("Print form not saved for PrPracticeOrder: id=" + practiceOrder + ", stateCode = " + stateCode);

            // загружаем сохраненную печатную форму
            if (practiceOrder.getPrintForm().getContent() == null)
                throw new ApplicationException("Файл печатной формы пуст.");
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(practiceOrder.getPrintForm()), false);

        } else {

            PrPracticeOrderPrintForm printForm;
            switch (practiceOrder.getType().getCode())
            {
                case PrPracticeOrderTypeCodes.PREDDIPLOM:
                    printForm = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.PRE_GRADUATION_PRACTICE_ORDER);
                    break;
                case PrPracticeOrderTypeCodes.PROIZVODSTV:
                    printForm = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.INDUSTRIAL_PRACTICE_ORDER);
                    break;
                case PrPracticeOrderTypeCodes.PREDDIPLOM_CHANGE:
                    printForm = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.EDIT_PRE_GRADUATION_PRACTICE_ORDER);
                    break;
                case PrPracticeOrderTypeCodes.PROIZVODSTV_CHANGE:
                    printForm = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.EDIT_INDUSTRIAL_PRACTICE_ORDER);
                    break;
                default: throw new IllegalStateException("Unknown order type");
            }

            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(printForm, "prPracticeOrder", practiceOrder);
        }
    }
}
