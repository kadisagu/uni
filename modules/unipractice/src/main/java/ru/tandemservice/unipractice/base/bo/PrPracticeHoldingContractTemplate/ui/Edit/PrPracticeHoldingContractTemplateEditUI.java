/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.ui.Edit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unipractice.entity.PrPracticeHoldingContractTemplateData;

/**
 * @author azhebko
 * @since 07.11.2014
 */
@Input(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = "templateData.id", required = true))
public class PrPracticeHoldingContractTemplateEditUI extends UIPresenter
{
    private PrPracticeHoldingContractTemplateData _templateData = new PrPracticeHoldingContractTemplateData();
    public PrPracticeHoldingContractTemplateData getTemplateData() { return _templateData; }
    public CtrContractVersion getContractVersion() { return getTemplateData().getOwner(); }

    @Override
    public void onComponentRefresh()
    {
        _templateData = IUniBaseDao.instance.get().getNotNull(PrPracticeHoldingContractTemplateData.class, _templateData.getId());
    }

    public void onClickApply()
    {
        if (getContractVersion().getDurationEndDate() != null && getContractVersion().getDurationEndDate().before(getContractVersion().getDurationBeginDate()))
        {
            UserContext.getInstance().getErrorCollector().add("Дата начала действия договора должна быть меньше даты окончания.", "durationBeginDate", "durationEndDate");
            return;
        }

        IUniBaseDao.instance.get().update(getTemplateData());
        deactivate();
    }
}