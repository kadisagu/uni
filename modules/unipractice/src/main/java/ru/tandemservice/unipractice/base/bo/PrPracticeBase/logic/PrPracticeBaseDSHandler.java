package ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseConstants.PR_PRACTICE_BASE_KIND_FILTER;
import static ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseConstants.PR_PRACTICE_BASE_STATUS_FILTER;

public class PrPracticeBaseDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {
    private static final String BASE = "base";

    public PrPracticeBaseDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        PrPracticeBaseKind kind = context.get(PR_PRACTICE_BASE_KIND_FILTER);
        Boolean archive = context.get(PR_PRACTICE_BASE_STATUS_FILTER);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PrPracticeBase.class, BASE).column(BASE);

        FilterUtils.applySelectFilter(dql, BASE, PrPracticeBase.kind(), kind);
        FilterUtils.applySelectFilter(dql, BASE, PrPracticeBase.archival(), archive);

//        // Order
        if (!PrPracticeBase.P_TITLE.equals(input.getEntityOrder().getKeyString())) {
            dql.order(property(BASE, input.getEntityOrder().getKeyString()), input.getEntityOrder().getDirection());
        }
        // теперь по злому названию
        PrPracticeBase.addJoinsForUseTitle(dql, BASE, "extOrgUnit", "extOuParent", "legalForm", "intOrgUnit");
        dql.order(PrPracticeBase.createTitleDQLExpression("extOrgUnit", "extOuParent", "legalForm", "intOrgUnit"), input.getEntityOrder().getDirection());

        dql.order(property(BASE, "id"));
//        // !Order

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();
    }
}