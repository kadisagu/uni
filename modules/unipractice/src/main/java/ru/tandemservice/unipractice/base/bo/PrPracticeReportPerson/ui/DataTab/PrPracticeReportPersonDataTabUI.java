/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.DataTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI;
import ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.DataTab.block.practiceData.PracticeDataBlock;
import ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.DataTab.block.practiceData.PracticeDataParam;

/**
 * @author nvankov
 * @since 10/28/14
 */
public class PrPracticeReportPersonDataTabUI extends UIPresenter implements IReportDQLModifierOwner
{
    private PracticeDataParam _practiceData = new PracticeDataParam();

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        PracticeDataBlock.onBeforeDataSourceFetch(dataSource, _practiceData);
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {
        
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (printInfo.isExists(StudentReportPersonAddUI.STUDENT_SCHEET))
        {
            // фильтры модифицируют запросы
            _practiceData.modify(dql, printInfo);

            // печатные блоки модифицируют запросы и создают печатные колонки
        }
    }

    // Getters
    public PracticeDataParam getPracticeData()
    {
        return _practiceData;
    }
}
