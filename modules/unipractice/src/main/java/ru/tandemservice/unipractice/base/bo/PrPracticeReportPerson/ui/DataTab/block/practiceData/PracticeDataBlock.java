/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.DataTab.block.practiceData;

import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unipractice.entity.catalog.PrEmploymentForm;

/**
 * @author nvankov
 * @since 10/28/14
 */
public class PracticeDataBlock
{
    // names
    public static final String PRACTICE_EMPLOYMENT_FORM_DS = "practiceEmploymentFormDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, PracticeDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createPracticeEmploymentFormDS(String name)
    {
        return new EntityComboDataSourceHandler(name, PrEmploymentForm.class)
                .filter(PrEmploymentForm.title())
                .order(PrEmploymentForm.title());
    }
}
