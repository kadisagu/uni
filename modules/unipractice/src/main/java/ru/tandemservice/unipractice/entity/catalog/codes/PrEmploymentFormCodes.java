package ru.tandemservice.unipractice.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Форма трудоустройства на время прохождения практики"
 * Имя сущности : prEmploymentForm
 * Файл data.xml : unipractice.catalog.data.xml
 */
public interface PrEmploymentFormCodes
{
    /** Константа кода (code) элемента : Был принят в штат предприятия (title) */
    String STAFF_MEMBER = "staff_member";
    /** Константа кода (code) элемента : Стажер, дублер (title) */
    String TRAINEE = "trainee";

    Set<String> CODES = ImmutableSet.of(STAFF_MEMBER, TRAINEE);
}
