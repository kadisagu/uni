package ru.tandemservice.unipractice.entity;

import ru.tandemservice.unipractice.entity.gen.PrPracticeBaseIntGen;

/**
 * База прохождения практики (подразделение)
 */
public class PrPracticeBaseInt extends PrPracticeBaseIntGen
{
    @Override
    public String getTitle()
    {
        if (getOrgUnit() == null) {
            return this.getClass().getSimpleName();
        }
        return this.getOrgUnit().getFullTitle();
    }

    public String getOrgUnitSettlementTitle()
    {
        return getOrgUnit().getLegalAddress() != null
                ? (getOrgUnit().getLegalAddress().getSettlement() != null ? getOrgUnit().getLegalAddress().getSettlement().getTitle() : null)
                : (getOrgUnit().getAddress() != null
                ? (getOrgUnit().getAddress().getSettlement() != null ? getOrgUnit().getAddress().getSettlement().getTitle() : null)
                : null);
    }

}