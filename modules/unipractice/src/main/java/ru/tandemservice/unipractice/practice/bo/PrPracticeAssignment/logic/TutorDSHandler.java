package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.ORG_UNIT_ID;

public class TutorDSHandler extends DefaultComboDataSourceHandler {

    private static final String ALIAS = "e";

    public TutorDSHandler(String name) {
        super(name, PpsEntry.class, PpsEntry.person().identityCard().fullFio());
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        super.prepareConditions(ep);

        Long orgUnitId = ep.context.get(ORG_UNIT_ID);
        ep.dqlBuilder.where(eq(property(PpsEntry.orgUnit().id().fromAlias(ALIAS)), value(orgUnitId)));
        ep.dqlBuilder.where(isNull(property(ALIAS, PpsEntry.removalDate())));
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order(property(ALIAS, PpsEntry.person().identityCard().lastName()));
        ep.dqlBuilder.order(property(ALIAS, PpsEntry.person().identityCard().firstName()));
        ep.dqlBuilder.order(property(ALIAS, PpsEntry.person().identityCard().middleName()));
    }
}