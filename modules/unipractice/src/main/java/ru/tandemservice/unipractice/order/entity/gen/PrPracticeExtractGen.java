package ru.tandemservice.unipractice.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.order.entity.PrAbstractExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка о направлении на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeExtractGen extends PrAbstractExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.order.entity.PrPracticeExtract";
    public static final String ENTITY_NAME = "prPracticeExtract";
    public static final int VERSION_HASH = 515753626;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTITY = "entity";

    private PrPracticeAssignment _entity;     // Направление на практику

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление на практику. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PrPracticeAssignment getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Направление на практику. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntity(PrPracticeAssignment entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PrPracticeExtractGen)
        {
            setEntity(((PrPracticeExtract)another).getEntity());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeExtractGen> extends PrAbstractExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeExtract.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return obj.getEntity();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "entity":
                    obj.setEntity((PrPracticeAssignment) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "entity":
                    return PrPracticeAssignment.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeExtract> _dslPath = new Path<PrPracticeExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeExtract");
    }
            

    /**
     * @return Направление на практику. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeExtract#getEntity()
     */
    public static PrPracticeAssignment.Path<PrPracticeAssignment> entity()
    {
        return _dslPath.entity();
    }

    public static class Path<E extends PrPracticeExtract> extends PrAbstractExtract.Path<E>
    {
        private PrPracticeAssignment.Path<PrPracticeAssignment> _entity;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление на практику. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeExtract#getEntity()
     */
        public PrPracticeAssignment.Path<PrPracticeAssignment> entity()
        {
            if(_entity == null )
                _entity = new PrPracticeAssignment.Path<PrPracticeAssignment>(L_ENTITY, this);
            return _entity;
        }

        public Class getEntityClass()
        {
            return PrPracticeExtract.class;
        }

        public String getEntityName()
        {
            return "prPracticeExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
