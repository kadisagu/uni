/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddRegPracticeBase;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.logic.SimilarExternalOrgUnitDSHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt;
import ru.tandemservice.unipractice.entity.PrPracticeBaseRequestInt;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.PrPracticeAssignmentManager;

import java.io.IOException;
import java.util.*;


/**
 * @author Andrey Avetisov
 * @since 15.10.2014
 */

public class PrPracticeAssignmentAddRegPracticeBaseUI extends UIPresenter
{

    private PrPracticeBaseKind _prPracticeBaseKind;
    private OrgUnit _orgUnit;
    private ExternalOrgUnit _externalOrgUnit;
    private String _orgUnitTitle;
    private String _orgUnitShortTitle;
    private LegalForm _orgUnitLegalForm;

    public static final String EXIST_ORGUNIT_SOURCE = "Выбрать организацию из имеющихся";
    public static final String NEW_ORGUNIT_SOURCE = "Зарегистрировать новую внешнюю организацию";
    private String[] _sourceList = new String[]{EXIST_ORGUNIT_SOURCE, NEW_ORGUNIT_SOURCE};
    private String _currentSourceValue;
    private String _guaranteeLetterNumber;
    private Date _guaranteeDate;
    private IUploadFile _scanCopyUploadFile;

    private NavigableMap<Double, List<DataWrapper>> _similarMap = new TreeMap<Double, List<DataWrapper>>();
    private boolean _searchSimilarMode = false;

    @Override
    public void onComponentRefresh()
    {
        setPrPracticeBaseKind(new PrPracticeBaseKind());
        setCurrentSourceValue(EXIST_ORGUNIT_SOURCE);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (PrPracticeAssignmentAddRegPracticeBase.SIMILAR_EXTERNAL_ORG_UNIT_DS.equals(dataSource.getName()))
            dataSource.put(SimilarExternalOrgUnitDSHandler.SIMILAR_EXTERNAL_ORG_UNIT_MAP, _similarMap);
    }

    public PrPracticeBaseKind getPrPracticeBaseKind()
    {
        return _prPracticeBaseKind;
    }

    public void setPrPracticeBaseKind(PrPracticeBaseKind prPracticeBaseKind)
    {
        _prPracticeBaseKind = prPracticeBaseKind;
    }

    public IUploadFile getScanCopyUploadFile()
    {
        return _scanCopyUploadFile;
    }

    public void setScanCopyUploadFile(IUploadFile scanCopyUploadFile)
    {
        _scanCopyUploadFile = scanCopyUploadFile;
    }

    public boolean isInnerPracticeBase()
    {
            return getPrPracticeBaseKind().isInternal();
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        _externalOrgUnit = externalOrgUnit;
    }

    public String[] getSourceList()
    {
        return _sourceList;
    }

    public void setSourceList(String[] sourceList)
    {
        _sourceList = sourceList;
    }

    public String getCurrentSourceValue()
    {
        return _currentSourceValue;
    }

    public boolean isExistOrgUnit()
    {
        return getCurrentSourceValue().equals(EXIST_ORGUNIT_SOURCE);
    }

    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    public void setOrgUnitTitle(String orgUnitTitle)
    {
        _orgUnitTitle = orgUnitTitle;
    }

    public String getOrgUnitShortTitle()
    {
        return _orgUnitShortTitle;
    }

    public void setOrgUnitShortTitle(String orgUnitShortTitle)
    {
        _orgUnitShortTitle = orgUnitShortTitle;
    }

    public LegalForm getOrgUnitLegalForm()
    {
        return _orgUnitLegalForm;
    }

    public void setOrgUnitLegalForm(LegalForm orgUnitLegalForm)
    {
        _orgUnitLegalForm = orgUnitLegalForm;
    }

    public String getGuaranteeLetterNumber()
    {
        return _guaranteeLetterNumber;
    }

    public void setGuaranteeLetterNumber(String guaranteeLetterNumber)
    {
        _guaranteeLetterNumber = guaranteeLetterNumber;
    }

    public Date getGuaranteeDate()
    {
        return _guaranteeDate;
    }

    public void setGuaranteeDate(Date guaranteeDate)
    {
        _guaranteeDate = guaranteeDate;
    }

    public void setCurrentSourceValue(String currentSourceValue)
    {
        _currentSourceValue = currentSourceValue;
    }

    public NavigableMap<Double, List<DataWrapper>> getSimilarMap()
    {
        return _similarMap;
    }

    public void setSimilarMap(NavigableMap<Double, List<DataWrapper>> similarMap)
    {
        _similarMap = similarMap;
    }

    public boolean isSearchSimilarMode()
    {
        return _searchSimilarMode;
    }

    public void setSearchSimilarMode(boolean searchSimilarMode)
    {
        _searchSimilarMode = searchSimilarMode;
    }

    public boolean isShowSaveCancelButtons()
    {
        return (getPrPracticeBaseKind() != null && getPrPracticeBaseKind().isInternal()) || !_searchSimilarMode;
    }

    public void onClickApply()
    {
        if(isInnerPracticeBase())
        {
            PrPracticeBaseRequestInt prPracticeBaseRequest = new PrPracticeBaseRequestInt();
            prPracticeBaseRequest.setKind(getPrPracticeBaseKind());
            prPracticeBaseRequest.setOrgUnit(getOrgUnit());
            PrPracticeAssignmentManager.instance().practiceAssignmentDao().addRequestForRegPracticeBase(prPracticeBaseRequest);
        }
        else
        {
            PrPracticeBaseRequestExt prPracticeBaseRequest = new PrPracticeBaseRequestExt();
            prPracticeBaseRequest.setKind(getPrPracticeBaseKind());
            prPracticeBaseRequest.setGuaranteeLetter(getGuaranteeLetterNumber());
            prPracticeBaseRequest.setGuaranteeLetterDate(getGuaranteeDate());
            if(getScanCopyUploadFile()!=null)
            {
                try
                {
                    prPracticeBaseRequest.setGuaranteeLetterCopy(new DatabaseFile());
                    prPracticeBaseRequest.getGuaranteeLetterCopy().setContent(IOUtils.toByteArray(getScanCopyUploadFile().getStream()));
                    prPracticeBaseRequest.getGuaranteeLetterCopy().setContentType(CommonBaseUtil.getContentType(getScanCopyUploadFile()));
                    prPracticeBaseRequest.getGuaranteeLetterCopy().setFilename(getScanCopyUploadFile().getFileName());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            if(getCurrentSourceValue().equals(EXIST_ORGUNIT_SOURCE))
            {
                prPracticeBaseRequest.setOrgUnit(getExternalOrgUnit());
                prPracticeBaseRequest.setOrgUnitTitle(null);
                prPracticeBaseRequest.setOrgUnitShortTitle(null);
                prPracticeBaseRequest.setOrgUnitLegalForm(null);
            }
            else
            {
                prPracticeBaseRequest.setOrgUnit(null);
                prPracticeBaseRequest.setOrgUnitTitle(getOrgUnitTitle());
                prPracticeBaseRequest.setOrgUnitShortTitle(getOrgUnitShortTitle());
                prPracticeBaseRequest.setOrgUnitLegalForm(getOrgUnitLegalForm());
            }
            PrPracticeAssignmentManager.instance().practiceAssignmentDao().addRequestForRegPracticeBase(prPracticeBaseRequest);
        }
    deactivate();
    }

    public void onClickSearchSimilar()
    {
        if(StringUtils.isEmpty(getOrgUnitTitle())) return;
        _similarMap = PrPracticeAssignmentManager.instance().practiceAssignmentDao().getSimilarExternalOrgUnit(getOrgUnitTitle());
        if(!_similarMap.isEmpty())
            _searchSimilarMode = true;
        else
            _searchSimilarMode = false;
    }

    public void onClickSimilarSelect()
    {
        Collection<IEntity> selected = _uiConfig.<BaseSearchListDataSource>getDataSource(PrPracticeAssignmentAddRegPracticeBase.SIMILAR_EXTERNAL_ORG_UNIT_DS).getOptionColumnSelectedObjects("select");
        if(selected == null || selected.isEmpty())
            throw new ApplicationException("Выберите организацию.");
        setCurrentSourceValue(EXIST_ORGUNIT_SOURCE);
        setExternalOrgUnit(((DataWrapper) selected.iterator().next()).<ExternalOrgUnit>getWrapped());
        _similarMap = new TreeMap<>();
        _searchSimilarMode = false;
    }

    public void onClickSimilarCancel()
    {
        _similarMap = new TreeMap<>();
        _searchSimilarMode = false;
    }

    public void onChangeSourceOption()
    {
        onClickSimilarCancel();
    }
}
