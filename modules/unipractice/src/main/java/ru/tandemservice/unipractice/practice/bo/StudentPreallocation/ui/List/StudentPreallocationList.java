/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic.StPreallocationListDSHandelr;

/**
 * @author Andrey Avetisov
 * @since 09.10.2014
 */
@Configuration
public class StudentPreallocationList extends BusinessComponentManager
{
    public static final String ST_PREALLOCATION_LIST_DS = "stPreallocationListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(ST_PREALLOCATION_LIST_DS, stPreallocationDSColumns(), preallocationListDSHandelr()))
                .create();
    }

    @Bean
    public ColumnListExtPoint stPreallocationDSColumns()
    {
        return columnListExtPointBuilder(ST_PREALLOCATION_LIST_DS)
                .addColumn(indicatorColumn("report").defaultIndicatorItem(new IndicatorColumn.Item("report")))
                .addColumn(publisherColumn("formDate", StPracticePreallocation.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("eduYear", StPracticePreallocation.educationYear()))
                .addColumn(textColumn("practiceKind", StPracticePreallocation.practiceKind()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete").permissionKey("ui:deleteStorableReportPermissionKey")
                        .alert(FormattedMessage.with().template("stPreallocationListDS.delete.alert").parameter(StPracticePreallocation.formingDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).create()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> preallocationListDSHandelr()
    {
        return new StPreallocationListDSHandelr(getName(), true);
    }
}
