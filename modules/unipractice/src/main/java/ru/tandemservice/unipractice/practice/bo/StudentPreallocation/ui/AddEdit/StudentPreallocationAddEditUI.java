/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation.ui.AddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderPrintForm;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderPrintFormCodes;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.StudentPreallocationManager;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic.StPreallocationEndDateDSHandler;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic.StPreallocationStartDateDSHandler;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 09.10.2014
 */
@Input({
               @Bind(key = "orgUnitId", binding = "orgUnitId")
       })
public class StudentPreallocationAddEditUI extends UIPresenter
{
    public static final String ORGUNIT_ID = "orgUnitId";
    private Long _orgUnitId;
    private StPracticePreallocation _practicePreallocation;
    private boolean _requiredField;
    private boolean _showGroup;
    private EppRegistryStructure _registryStructure;
    private EducationYear _educationYear;
    private EducationLevelsHighSchool _educationLevelsHighSchool;
    private List<Course> _courseList;
    private List<Group> _groupList;
    private DataWrapper _beginPracticeDate;
    private DataWrapper _endPracticeDate;


    @Override
    public void onComponentRefresh()
    {
        setPracticePreallocation(new StPracticePreallocation());
        _educationYear = DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), true);
        setRequiredField(true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if(dataSource.getName().equals(StudentPreallocationAddEdit.PRACTICE_BEGIN_DATE_DS) || dataSource.getName().equals(StudentPreallocationAddEdit.PRACTICE_END_DATE_DS))
        {
            if (getRegistryStructure()!=null)
            {
                dataSource.put(StudentPreallocationAddEdit.PRACTICE_KIND, getRegistryStructure().getTitle());
            }
            if(getEducationYear()!=null)
            {
                dataSource.put(StudentPreallocationAddEdit.EDU_YEAR, getEducationYear().getTitle());
            }
            dataSource.put(StudentPreallocationAddEdit.EDUCATION_LEVEL_HIGHSCHOOL, getEducationLevelsHighSchool());
        }
        if (dataSource.getName().equals(StudentPreallocationAddEdit.PRACTICE_END_DATE_DS) && getBeginPracticeDate()!=null)
        {
            dataSource.put(StPreallocationEndDateDSHandler.START_ASSIGNMENT_DATE, getBeginPracticeDate().get(StPreallocationStartDateDSHandler.PRACTICE_ASSIGNMENT_START_DATE));
        }
        if (dataSource.getName().equals(StudentPreallocationAddEdit.GROUP_DS))
        {
            dataSource.put(StudentPreallocationAddEdit.EDUCATION_LEVEL_HIGHSCHOOL, getEducationLevelsHighSchool());
        }
    }

    // Getters & Setters

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public StPracticePreallocation getPracticePreallocation()
    {
        return _practicePreallocation;
    }

    public void setPracticePreallocation(StPracticePreallocation practicePreallocation)
    {
        _practicePreallocation = practicePreallocation;
    }

    public boolean isRequiredField()
    {
        return _requiredField;
    }

    public void setRequiredField(boolean requiredField)
    {
        _requiredField = requiredField;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public EppRegistryStructure getRegistryStructure()
    {
        return _registryStructure;
    }

    public void setRegistryStructure(EppRegistryStructure registryStructure)
    {
        _registryStructure = registryStructure;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public boolean isShowGroup()
    {
        return _showGroup;
    }

    public void setShowGroup(boolean showGroup)
    {
        _showGroup = showGroup;
    }

    public DataWrapper getBeginPracticeDate()
    {
        return _beginPracticeDate;
    }

    public void setBeginPracticeDate(DataWrapper beginPracticeDate)
    {
        _beginPracticeDate = beginPracticeDate;
    }

    public DataWrapper getEndPracticeDate()
    {
        return _endPracticeDate;
    }

    public void setEndPracticeDate(DataWrapper endPracticeDate)
    {
        _endPracticeDate = endPracticeDate;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return new CommonPostfixPermissionModel(_orgUnitId != null ? OrgUnitSecModel.getPostfix(DataAccessServices.dao().get(OrgUnit.class, _orgUnitId)) : null);
    }

    public String getViewKey()
    {
        return getSec().getPermission(_orgUnitId != null ? "orgUnit_addStPracticePreallocationReportPermissionKey" : "stPracticePreallocationReportPermissionKey");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return _orgUnitId != null ? DataAccessServices.dao().get(OrgUnit.class, _orgUnitId) : super.getSecuredObject();
    }

    public void onClickApply()
    {

        getPracticePreallocation().setFormingDate(new Date());
        getPracticePreallocation().setPracticeBeginDate((Date)getBeginPracticeDate().get(StPreallocationStartDateDSHandler.PRACTICE_ASSIGNMENT_START_DATE));
        getPracticePreallocation().setPracticeEndDate((Date)getEndPracticeDate().get(StPreallocationEndDateDSHandler.PRACTICE_ASSIGNMENT_END_DATE));
        getPracticePreallocation().setEducationYear(getEducationYear().getTitle());
        getPracticePreallocation().setPracticeKind(getRegistryStructure().getTitle());
        getPracticePreallocation().setEducationLevelsHighSchool(getEducationLevelsHighSchool().getFullTitle());

        String courseTitles = "";
        for (Course course : getCourseList())
        {

            courseTitles+= courseTitles.length()>0?";"+course.getTitle():course.getTitle();
        }

        getPracticePreallocation().setCourse(courseTitles);
        if(getOrgUnitId()!=null)
        {
            getPracticePreallocation().setOrgUnit(DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId()));
        }
        if(isShowGroup())
        {
            String groupTitles = "";
            for (Group group : getGroupList())
            {

                groupTitles+= groupTitles.length()>0?";"+group.getTitle():group.getTitle();
            }

            getPracticePreallocation().setGroup(groupTitles);
        }
        else
        {
            getPracticePreallocation().setGroup(null);
        }
        PrPracticeOrderPrintForm scriptForPrint= DataAccessServices.dao().get(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintForm.P_CODE,
                                                                              PrPracticeOrderPrintFormCodes.STUDENT_PREALLOCATION);

        Map<String, Object> titleTemplate = CommonManager.instance().scriptDao().getScriptResult(scriptForPrint,
                                                                                                 "practicePreallocation", getPracticePreallocation(),
                                                                                                 "orgUnitId", getOrgUnitId());
        RtfDocument doc = (RtfDocument) titleTemplate.get("document");
        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(doc.toByteArray());
        databaseFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        databaseFile.setFilename("Предварительное распределение.rtf");
        getPracticePreallocation().setContent(databaseFile);
        StudentPreallocationManager.instance().studentPreallocationDao().savePracticePreallocation(getPracticePreallocation());

        deactivate();

    }
}
