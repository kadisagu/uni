package ru.tandemservice.unipractice.order.bo.PracticeOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.config.itemList.IItemListExtPointBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderType;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.IOrderPrinter;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.IPracticeOrderDao;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.OrderPrinter;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderDao;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.List.PracticeParagraphList;

@Configuration
public class PracticeOrderManager extends BusinessObjectManager {

    public static PracticeOrderManager instance() {
        return instance(PracticeOrderManager.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orderTypeDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), PrPracticeOrderType.class, PrPracticeOrderType.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationYearDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), EducationYear.class, EducationYear.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orderStateDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), OrderStates.class, OrderStates.title());
    }

    @Bean
    public IPracticeOrderDao practiceOrderDao() {
        return new PracticeOrderDao();
    }

    @Bean
    public ItemListExtPoint<Class<? extends BusinessComponentManager>> orderTypeExtPoint()
    {
        IItemListExtPointBuilder<Class <? extends BusinessComponentManager>> extPointBuilder = itemListOfBusinessComponentManagers();

        return extPointBuilder
            .add(PrPracticeOrderTypeCodes.PREDDIPLOM, PracticeParagraphList.class)
            .create();
    }

    @Bean
    public IOrderPrinter printer() {
        return new OrderPrinter();
    }


}
