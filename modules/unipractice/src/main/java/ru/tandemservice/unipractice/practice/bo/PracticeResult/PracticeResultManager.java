package ru.tandemservice.unipractice.practice.bo.PracticeResult;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unipractice.practice.bo.PracticeResult.logic.IPracticeResultDao;
import ru.tandemservice.unipractice.practice.bo.PracticeResult.logic.PracticeResultDao;

@Configuration
public class PracticeResultManager extends BusinessObjectManager {

    public static PracticeResultManager instance() {
        return instance(PracticeResultManager.class);
    }

    @Bean
    public IPracticeResultDao practiceResultDao() {
        return new PracticeResultDao();
    }

}
