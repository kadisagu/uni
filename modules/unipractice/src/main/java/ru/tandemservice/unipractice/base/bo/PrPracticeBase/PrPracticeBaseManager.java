package ru.tandemservice.unipractice.base.bo.PrPracticeBase;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.IPrPracticeBaseDao;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseDao;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;
import ru.tandemservice.unipractice.entity.PrPracticeBaseInt;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseConstants.PR_PRACTICE_BASE_KIND_INTERNAL;

@Configuration
public class PrPracticeBaseManager extends BusinessObjectManager {

    public static PrPracticeBaseManager instance() {
        return instance(PrPracticeBaseManager.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler externalOrganizationDSHandler() {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), ExternalOrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(notExists(PrPracticeBaseExt.class, PrPracticeBaseExt.L_ORG_UNIT, property(alias)));
            }
        };
        handler.where(ExternalOrgUnit.active(), true);
        handler.filter(ExternalOrgUnit.title());
        handler.order(ExternalOrgUnit.title());
        return handler;
    }

    @Bean
    public IDefaultComboDataSourceHandler internalOrganizationDSHandler() {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(notExists(PrPracticeBaseInt.class, PrPracticeBaseInt.L_ORG_UNIT, property(alias)));
            }
        };
        handler.where(OrgUnit.archival(), false);
        handler.filter(OrgUnit.fullTitle());
        handler.order(OrgUnit.fullTitle());
        return handler;
    }

    @Bean
    public IDefaultComboDataSourceHandler prPracticeBaseKindDSHandler() {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), PrPracticeBaseKind.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                Boolean onlyInternalKinds = context.get(PR_PRACTICE_BASE_KIND_INTERNAL);
                if (onlyInternalKinds != null) {
                    dql.where(eq(DQLExpressions.property(PrPracticeBaseKind.internal().fromAlias(alias)), value(false)));
                }
            }
        };
        handler.order(PrPracticeBaseKind.title());
        return handler;
    }

    @Bean
    public IPrPracticeBaseDao prPracticeBaseDao() {
        return new PrPracticeBaseDao();
    }

}
