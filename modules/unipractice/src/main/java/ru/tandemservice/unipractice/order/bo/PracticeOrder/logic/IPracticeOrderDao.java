package ru.tandemservice.unipractice.order.bo.PracticeOrder.logic;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.order.entity.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface IPracticeOrderDao {

    /**
     * checks if current practiceOrder has valid number
     * @param practiceOrder (order to check)
     * @return true if order number is valid (unique in current year)
     */
    boolean isOrderNumberValid(PrPracticeOrder practiceOrder);

    /**
     * deletes order with paragraphs and extracts
     * @param prPracticeOrderId order id
     */
    void deleteOrder(Long prPracticeOrderId);

    /**
     * creates paragraph for order
     * @param orderId order id
     * @param assignments assignments to add for paragraph
     * @return order paragraph
     */
    PrPracticeParagraph createParagraph(Long orderId, List<PrPracticeAssignment> assignments);

    /**
     * edits paragraph
     * @param paragraphId paragraph id to edit
     * @param assignments new assignments for this paragraph
     */
    void editParagraph(Long paragraphId, List<PrPracticeAssignment> assignments);

    /**
     * edits paragraph for change order type
     * @param paragraphId paragraph id to edit
     * @param extracts new extracts for this paragraph
     */
    void editChangeParagraph(Long paragraphId, List<PrPracticeExtract> extracts);

    /**
     * creates paragraph for change order type
     * @param orderId order id
     * @param extracts extracts to add for paragraph
     * @return order paragraph
     */
    PrPracticeChangeParagraph createChangeParagraph(Long orderId, List<PrPracticeExtract> extracts);

    /**
     * checks if order is in forming state
     * @param orderId order id to check
     * @return true if order is in forming state, otherwise false
     */
    boolean isOrderInFormingState(Long orderId);

    /**
     * checks if any of assignments already is in another paragraph
     * @param paragraphId paragraph id
     * @param assignmentIds assignment ids
     * @return map of order numbers in which paragraph with assignments already exists
     */
    Map<String,String> getExtractsDuplicates(Long paragraphId, Collection<Long> assignmentIds);

    /**
     * deletes paragraph with extracts
     * @param paragraphId paragraph id to delete
     */
    void deleteParagraph(Long paragraphId);

    Map<String, String> getChangeExtractsDuplicates(Long changeParagraphId, Collection<Long> extractIds);

    /**
     * returns every assignment for extract for paragraph
     * @param paragraphId paragraph id
     * @return returns list of assignments
     */
    List<PrPracticeAssignment> getAssignmentsByParagraphId(Long paragraphId);

    List<PrPracticeExtract> getExtractsByChangeParagraphId(Long paragraphId);

    void doSendToCoordination(List<Long> selectedIds, IPersistentPersonable initiator);

    void doCommit(List<Long> selectedIds);

    void doSendToFormative(Long orderId);

    void doReject(PrPracticeOrder order);

    void doRollback(PrPracticeOrder order);

    void saveEnrollmentOrderText(PrPracticeOrder order);

    void doExecuteAcceptVisaOrderService(IAbstractOrder order);

    void doExecuteRejectVisaOrderService(IAbstractOrder order);

    void doCommit(PrPracticeExtract extract);

    void doRollback(PrPracticeExtract extract);

    void doCommit(PrPracticeChangeExtract extract);

    void doRollback(PrPracticeChangeExtract extract);

    void getDownloadPrintForm(Long practiceOrderId);
}
