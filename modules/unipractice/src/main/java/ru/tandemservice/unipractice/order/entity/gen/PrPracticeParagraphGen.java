package ru.tandemservice.unipractice.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unipractice.order.entity.PrAbstractParagraph;
import ru.tandemservice.unipractice.order.entity.PrPracticeParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа о направлении на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeParagraphGen extends PrAbstractParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.order.entity.PrPracticeParagraph";
    public static final String ENTITY_NAME = "prPracticeParagraph";
    public static final int VERSION_HASH = 972336995;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PrPracticeParagraphGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeParagraphGen> extends PrAbstractParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeParagraph.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeParagraph();
        }
    }
    private static final Path<PrPracticeParagraph> _dslPath = new Path<PrPracticeParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeParagraph");
    }
            

    public static class Path<E extends PrPracticeParagraph> extends PrAbstractParagraph.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return PrPracticeParagraph.class;
        }

        public String getEntityName()
        {
            return "prPracticeParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
