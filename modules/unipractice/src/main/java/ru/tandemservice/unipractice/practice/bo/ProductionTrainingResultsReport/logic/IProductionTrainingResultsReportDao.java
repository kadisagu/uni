/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.Add.ProductionTrainingResultsReportAddUI;

/**
 * @author rsizonenko
 * @since 22.10.2014
 */
public interface IProductionTrainingResultsReportDao extends INeedPersistenceSupport {

    public long createReport(ProductionTrainingResultsReportAddUI model);
}
