package ru.tandemservice.unipractice.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeResult;
import ru.tandemservice.unipractice.entity.catalog.PrEmploymentForm;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Результат прохождения практики
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeResultGen extends EntityBase
 implements INaturalIdentifiable<PrPracticeResultGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.PrPracticeResult";
    public static final String ENTITY_NAME = "prPracticeResult";
    public static final int VERSION_HASH = 967706718;
    private static IEntityMeta ENTITY_META;

    public static final String L_PRACTICE_ASSIGNMENT = "practiceAssignment";
    public static final String P_DATE_START = "dateStart";
    public static final String P_DATE_END = "dateEnd";
    public static final String L_EMPLOYMENT_FORM = "employmentForm";
    public static final String P_PRACTICE_PAYED = "practicePayed";
    public static final String P_COMPLETED = "completed";
    public static final String P_ACCEPTANCE_DATE = "acceptanceDate";
    public static final String L_TUTOR_MARK = "tutorMark";
    public static final String P_EVALUATION_TEXT = "evaluationText";
    public static final String L_SCAN_COPY = "scanCopy";

    private PrPracticeAssignment _practiceAssignment;     // Направление
    private Date _dateStart;     // Фактическая дата начала практики
    private Date _dateEnd;     // Фактическая дата окончания практики
    private PrEmploymentForm _employmentForm;     // Форма трудоустройства на время прохождения практики
    private boolean _practicePayed;     // Проходил практику с оплатой
    private Boolean _completed;     // Практика пройдена
    private Date _acceptanceDate;     // Дата приема результатов практики
    private SessionMarkGradeValueCatalogItem _tutorMark;     // Оценка руководителя
    private String _evaluationText;     // Заключение кафедры
    private DatabaseFile _scanCopy;     // Копия направления с результатами

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PrPracticeAssignment getPracticeAssignment()
    {
        return _practiceAssignment;
    }

    /**
     * @param practiceAssignment Направление. Свойство не может быть null и должно быть уникальным.
     */
    public void setPracticeAssignment(PrPracticeAssignment practiceAssignment)
    {
        dirty(_practiceAssignment, practiceAssignment);
        _practiceAssignment = practiceAssignment;
    }

    /**
     * @return Фактическая дата начала практики. Свойство не может быть null.
     */
    @NotNull
    public Date getDateStart()
    {
        return _dateStart;
    }

    /**
     * @param dateStart Фактическая дата начала практики. Свойство не может быть null.
     */
    public void setDateStart(Date dateStart)
    {
        dirty(_dateStart, dateStart);
        _dateStart = dateStart;
    }

    /**
     * @return Фактическая дата окончания практики. Свойство не может быть null.
     */
    @NotNull
    public Date getDateEnd()
    {
        return _dateEnd;
    }

    /**
     * @param dateEnd Фактическая дата окончания практики. Свойство не может быть null.
     */
    public void setDateEnd(Date dateEnd)
    {
        dirty(_dateEnd, dateEnd);
        _dateEnd = dateEnd;
    }

    /**
     * @return Форма трудоустройства на время прохождения практики.
     */
    public PrEmploymentForm getEmploymentForm()
    {
        return _employmentForm;
    }

    /**
     * @param employmentForm Форма трудоустройства на время прохождения практики.
     */
    public void setEmploymentForm(PrEmploymentForm employmentForm)
    {
        dirty(_employmentForm, employmentForm);
        _employmentForm = employmentForm;
    }

    /**
     * @return Проходил практику с оплатой. Свойство не может быть null.
     */
    @NotNull
    public boolean isPracticePayed()
    {
        return _practicePayed;
    }

    /**
     * @param practicePayed Проходил практику с оплатой. Свойство не может быть null.
     */
    public void setPracticePayed(boolean practicePayed)
    {
        dirty(_practicePayed, practicePayed);
        _practicePayed = practicePayed;
    }

    /**
     * @return Практика пройдена.
     */
    public Boolean getCompleted()
    {
        return _completed;
    }

    /**
     * @param completed Практика пройдена.
     */
    public void setCompleted(Boolean completed)
    {
        dirty(_completed, completed);
        _completed = completed;
    }

    /**
     * @return Дата приема результатов практики.
     */
    public Date getAcceptanceDate()
    {
        return _acceptanceDate;
    }

    /**
     * @param acceptanceDate Дата приема результатов практики.
     */
    public void setAcceptanceDate(Date acceptanceDate)
    {
        dirty(_acceptanceDate, acceptanceDate);
        _acceptanceDate = acceptanceDate;
    }

    /**
     * @return Оценка руководителя.
     */
    public SessionMarkGradeValueCatalogItem getTutorMark()
    {
        return _tutorMark;
    }

    /**
     * @param tutorMark Оценка руководителя.
     */
    public void setTutorMark(SessionMarkGradeValueCatalogItem tutorMark)
    {
        dirty(_tutorMark, tutorMark);
        _tutorMark = tutorMark;
    }

    /**
     * @return Заключение кафедры.
     */
    public String getEvaluationText()
    {
        return _evaluationText;
    }

    /**
     * @param evaluationText Заключение кафедры.
     */
    public void setEvaluationText(String evaluationText)
    {
        dirty(_evaluationText, evaluationText);
        _evaluationText = evaluationText;
    }

    /**
     * @return Копия направления с результатами. Свойство должно быть уникальным.
     */
    public DatabaseFile getScanCopy()
    {
        return _scanCopy;
    }

    /**
     * @param scanCopy Копия направления с результатами. Свойство должно быть уникальным.
     */
    public void setScanCopy(DatabaseFile scanCopy)
    {
        dirty(_scanCopy, scanCopy);
        _scanCopy = scanCopy;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PrPracticeResultGen)
        {
            if (withNaturalIdProperties)
            {
                setPracticeAssignment(((PrPracticeResult)another).getPracticeAssignment());
            }
            setDateStart(((PrPracticeResult)another).getDateStart());
            setDateEnd(((PrPracticeResult)another).getDateEnd());
            setEmploymentForm(((PrPracticeResult)another).getEmploymentForm());
            setPracticePayed(((PrPracticeResult)another).isPracticePayed());
            setCompleted(((PrPracticeResult)another).getCompleted());
            setAcceptanceDate(((PrPracticeResult)another).getAcceptanceDate());
            setTutorMark(((PrPracticeResult)another).getTutorMark());
            setEvaluationText(((PrPracticeResult)another).getEvaluationText());
            setScanCopy(((PrPracticeResult)another).getScanCopy());
        }
    }

    public INaturalId<PrPracticeResultGen> getNaturalId()
    {
        return new NaturalId(getPracticeAssignment());
    }

    public static class NaturalId extends NaturalIdBase<PrPracticeResultGen>
    {
        private static final String PROXY_NAME = "PrPracticeResultNaturalProxy";

        private Long _practiceAssignment;

        public NaturalId()
        {}

        public NaturalId(PrPracticeAssignment practiceAssignment)
        {
            _practiceAssignment = ((IEntity) practiceAssignment).getId();
        }

        public Long getPracticeAssignment()
        {
            return _practiceAssignment;
        }

        public void setPracticeAssignment(Long practiceAssignment)
        {
            _practiceAssignment = practiceAssignment;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PrPracticeResultGen.NaturalId) ) return false;

            PrPracticeResultGen.NaturalId that = (NaturalId) o;

            if( !equals(getPracticeAssignment(), that.getPracticeAssignment()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPracticeAssignment());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPracticeAssignment());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeResultGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeResult.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeResult();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "practiceAssignment":
                    return obj.getPracticeAssignment();
                case "dateStart":
                    return obj.getDateStart();
                case "dateEnd":
                    return obj.getDateEnd();
                case "employmentForm":
                    return obj.getEmploymentForm();
                case "practicePayed":
                    return obj.isPracticePayed();
                case "completed":
                    return obj.getCompleted();
                case "acceptanceDate":
                    return obj.getAcceptanceDate();
                case "tutorMark":
                    return obj.getTutorMark();
                case "evaluationText":
                    return obj.getEvaluationText();
                case "scanCopy":
                    return obj.getScanCopy();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "practiceAssignment":
                    obj.setPracticeAssignment((PrPracticeAssignment) value);
                    return;
                case "dateStart":
                    obj.setDateStart((Date) value);
                    return;
                case "dateEnd":
                    obj.setDateEnd((Date) value);
                    return;
                case "employmentForm":
                    obj.setEmploymentForm((PrEmploymentForm) value);
                    return;
                case "practicePayed":
                    obj.setPracticePayed((Boolean) value);
                    return;
                case "completed":
                    obj.setCompleted((Boolean) value);
                    return;
                case "acceptanceDate":
                    obj.setAcceptanceDate((Date) value);
                    return;
                case "tutorMark":
                    obj.setTutorMark((SessionMarkGradeValueCatalogItem) value);
                    return;
                case "evaluationText":
                    obj.setEvaluationText((String) value);
                    return;
                case "scanCopy":
                    obj.setScanCopy((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "practiceAssignment":
                        return true;
                case "dateStart":
                        return true;
                case "dateEnd":
                        return true;
                case "employmentForm":
                        return true;
                case "practicePayed":
                        return true;
                case "completed":
                        return true;
                case "acceptanceDate":
                        return true;
                case "tutorMark":
                        return true;
                case "evaluationText":
                        return true;
                case "scanCopy":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "practiceAssignment":
                    return true;
                case "dateStart":
                    return true;
                case "dateEnd":
                    return true;
                case "employmentForm":
                    return true;
                case "practicePayed":
                    return true;
                case "completed":
                    return true;
                case "acceptanceDate":
                    return true;
                case "tutorMark":
                    return true;
                case "evaluationText":
                    return true;
                case "scanCopy":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "practiceAssignment":
                    return PrPracticeAssignment.class;
                case "dateStart":
                    return Date.class;
                case "dateEnd":
                    return Date.class;
                case "employmentForm":
                    return PrEmploymentForm.class;
                case "practicePayed":
                    return Boolean.class;
                case "completed":
                    return Boolean.class;
                case "acceptanceDate":
                    return Date.class;
                case "tutorMark":
                    return SessionMarkGradeValueCatalogItem.class;
                case "evaluationText":
                    return String.class;
                case "scanCopy":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeResult> _dslPath = new Path<PrPracticeResult>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeResult");
    }
            

    /**
     * @return Направление. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getPracticeAssignment()
     */
    public static PrPracticeAssignment.Path<PrPracticeAssignment> practiceAssignment()
    {
        return _dslPath.practiceAssignment();
    }

    /**
     * @return Фактическая дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getDateStart()
     */
    public static PropertyPath<Date> dateStart()
    {
        return _dslPath.dateStart();
    }

    /**
     * @return Фактическая дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getDateEnd()
     */
    public static PropertyPath<Date> dateEnd()
    {
        return _dslPath.dateEnd();
    }

    /**
     * @return Форма трудоустройства на время прохождения практики.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getEmploymentForm()
     */
    public static PrEmploymentForm.Path<PrEmploymentForm> employmentForm()
    {
        return _dslPath.employmentForm();
    }

    /**
     * @return Проходил практику с оплатой. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#isPracticePayed()
     */
    public static PropertyPath<Boolean> practicePayed()
    {
        return _dslPath.practicePayed();
    }

    /**
     * @return Практика пройдена.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getCompleted()
     */
    public static PropertyPath<Boolean> completed()
    {
        return _dslPath.completed();
    }

    /**
     * @return Дата приема результатов практики.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getAcceptanceDate()
     */
    public static PropertyPath<Date> acceptanceDate()
    {
        return _dslPath.acceptanceDate();
    }

    /**
     * @return Оценка руководителя.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getTutorMark()
     */
    public static SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> tutorMark()
    {
        return _dslPath.tutorMark();
    }

    /**
     * @return Заключение кафедры.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getEvaluationText()
     */
    public static PropertyPath<String> evaluationText()
    {
        return _dslPath.evaluationText();
    }

    /**
     * @return Копия направления с результатами. Свойство должно быть уникальным.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getScanCopy()
     */
    public static DatabaseFile.Path<DatabaseFile> scanCopy()
    {
        return _dslPath.scanCopy();
    }

    public static class Path<E extends PrPracticeResult> extends EntityPath<E>
    {
        private PrPracticeAssignment.Path<PrPracticeAssignment> _practiceAssignment;
        private PropertyPath<Date> _dateStart;
        private PropertyPath<Date> _dateEnd;
        private PrEmploymentForm.Path<PrEmploymentForm> _employmentForm;
        private PropertyPath<Boolean> _practicePayed;
        private PropertyPath<Boolean> _completed;
        private PropertyPath<Date> _acceptanceDate;
        private SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> _tutorMark;
        private PropertyPath<String> _evaluationText;
        private DatabaseFile.Path<DatabaseFile> _scanCopy;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getPracticeAssignment()
     */
        public PrPracticeAssignment.Path<PrPracticeAssignment> practiceAssignment()
        {
            if(_practiceAssignment == null )
                _practiceAssignment = new PrPracticeAssignment.Path<PrPracticeAssignment>(L_PRACTICE_ASSIGNMENT, this);
            return _practiceAssignment;
        }

    /**
     * @return Фактическая дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getDateStart()
     */
        public PropertyPath<Date> dateStart()
        {
            if(_dateStart == null )
                _dateStart = new PropertyPath<Date>(PrPracticeResultGen.P_DATE_START, this);
            return _dateStart;
        }

    /**
     * @return Фактическая дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getDateEnd()
     */
        public PropertyPath<Date> dateEnd()
        {
            if(_dateEnd == null )
                _dateEnd = new PropertyPath<Date>(PrPracticeResultGen.P_DATE_END, this);
            return _dateEnd;
        }

    /**
     * @return Форма трудоустройства на время прохождения практики.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getEmploymentForm()
     */
        public PrEmploymentForm.Path<PrEmploymentForm> employmentForm()
        {
            if(_employmentForm == null )
                _employmentForm = new PrEmploymentForm.Path<PrEmploymentForm>(L_EMPLOYMENT_FORM, this);
            return _employmentForm;
        }

    /**
     * @return Проходил практику с оплатой. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#isPracticePayed()
     */
        public PropertyPath<Boolean> practicePayed()
        {
            if(_practicePayed == null )
                _practicePayed = new PropertyPath<Boolean>(PrPracticeResultGen.P_PRACTICE_PAYED, this);
            return _practicePayed;
        }

    /**
     * @return Практика пройдена.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getCompleted()
     */
        public PropertyPath<Boolean> completed()
        {
            if(_completed == null )
                _completed = new PropertyPath<Boolean>(PrPracticeResultGen.P_COMPLETED, this);
            return _completed;
        }

    /**
     * @return Дата приема результатов практики.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getAcceptanceDate()
     */
        public PropertyPath<Date> acceptanceDate()
        {
            if(_acceptanceDate == null )
                _acceptanceDate = new PropertyPath<Date>(PrPracticeResultGen.P_ACCEPTANCE_DATE, this);
            return _acceptanceDate;
        }

    /**
     * @return Оценка руководителя.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getTutorMark()
     */
        public SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> tutorMark()
        {
            if(_tutorMark == null )
                _tutorMark = new SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem>(L_TUTOR_MARK, this);
            return _tutorMark;
        }

    /**
     * @return Заключение кафедры.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getEvaluationText()
     */
        public PropertyPath<String> evaluationText()
        {
            if(_evaluationText == null )
                _evaluationText = new PropertyPath<String>(PrPracticeResultGen.P_EVALUATION_TEXT, this);
            return _evaluationText;
        }

    /**
     * @return Копия направления с результатами. Свойство должно быть уникальным.
     * @see ru.tandemservice.unipractice.entity.PrPracticeResult#getScanCopy()
     */
        public DatabaseFile.Path<DatabaseFile> scanCopy()
        {
            if(_scanCopy == null )
                _scanCopy = new DatabaseFile.Path<DatabaseFile>(L_SCAN_COPY, this);
            return _scanCopy;
        }

        public Class getEntityClass()
        {
            return PrPracticeResult.class;
        }

        public String getEntityName()
        {
            return "prPracticeResult";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
