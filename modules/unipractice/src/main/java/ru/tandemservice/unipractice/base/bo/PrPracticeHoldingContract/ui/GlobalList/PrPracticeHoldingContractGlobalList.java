/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContract.ui.GlobalList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubCtr.CtrContractVersionPubCtr;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.Pub.PrPracticeBasePub;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContract.logic.PrPracticeHoldingContractDSHandler;
import ru.tandemservice.unipractice.entity.PrPracticeBaseContract;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;

/**
 * @author azhebko
 * @since 06.11.2014
 */
@Configuration
public class PrPracticeHoldingContractGlobalList extends BusinessComponentManager
{
    public static final String DS_PRACTICE_HOLDING_CONTRACT = "practiceHoldingContractDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(selectDS("practiceBaseDS", practiceBaseDSHandler()).addColumn(PrPracticeBaseExt.orgUnit().title().s()))
            .addDataSource(searchListDS(DS_PRACTICE_HOLDING_CONTRACT, prContractDSColumns(), prContractDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint prContractDSColumns()
    {
        IPublisherLinkResolver linkResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                DataWrapper row = (DataWrapper) entity;
                return row.<PrPracticeBaseContract>get(PrPracticeHoldingContractDSHandler.VP_PRACTICE_BASE_CONTRACT).getId();
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return CtrContractVersionPubCtr.class.getSimpleName();
            }
        };

        return columnListExtPointBuilder(DS_PRACTICE_HOLDING_CONTRACT)
            .addColumn(publisherColumn("contractNumber", CtrContractVersion.contract().number())
                    .publisherLinkResolver(linkResolver)
                    .width("120px"))

            .addColumn(publisherColumn("practiceBase", "title")
                .entityListProperty(PrPracticeHoldingContractDSHandler.VP_PRACTICE_BASE)
                .businessComponent(PrPracticeBasePub.class))
            .addColumn(textColumn("duration", CtrContractVersion.durationAsString()).width("200px"))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler prContractDSHandler()
    {
        return new PrPracticeHoldingContractDSHandler(this.getName())
            .order(CtrContractVersion.contract().number())
            .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceBaseDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), PrPracticeBaseExt.class)
            .filter(PrPracticeBaseExt.orgUnit().title())
            .order(PrPracticeBaseExt.orgUnit().title());
    }
}