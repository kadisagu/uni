package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;
import ru.tandemservice.unipractice.entity.PrPracticeBaseInt;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeBaseKindCodes;

import static ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseConstants.*;

@Input({
        @Bind(key = PR_PRACTICE_BASE_ID, binding = PR_PRACTICE_BASE_ID)
})
public class PrPracticeBaseAddEditUI extends UIPresenter {
    private PrPracticeBase prPracticeBase;

    private Long prPracticeBaseId;

    private OrgUnit internalOrganization;

    private ExternalOrgUnit externalOrganization;

    @Override
    public void onComponentRefresh() {
        if ( prPracticeBaseId != null ) {
            prPracticeBase = DataAccessServices.dao().getNotNull(PrPracticeBase.class, prPracticeBaseId);
        } else {
            prPracticeBase = new PrPracticeBaseInt();
            prPracticeBase.setKind(DataAccessServices.dao().getByCode(PrPracticeBaseKind.class, PrPracticeBaseKindCodes.INTERNAL));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (PR_PRACTICE_BASE_KIND_DS.equals(dataSource.getName())) {
            if (!isNewEntity())
                dataSource.put(PR_PRACTICE_BASE_KIND_INTERNAL, true);
        }
    }

    public boolean isNewEntity() {
        return prPracticeBaseId == null;
    }

    public void onClickApply() {
        if (isInternal()) {
            PrPracticeBaseInt intBase = (PrPracticeBaseInt) prPracticeBase;
            intBase.setOrgUnit(internalOrganization);
            DataAccessServices.dao().saveOrUpdate(intBase);
        } else {
            if (isNewEntity()) {
                PrPracticeBaseExt extBase = new PrPracticeBaseExt();
                extBase.setKind(prPracticeBase.getKind());
                extBase.setOrgUnit(externalOrganization);
                DataAccessServices.dao().saveOrUpdate(extBase);
            } else {
                DataAccessServices.dao().saveOrUpdate(prPracticeBase);
            }

        }
        deactivate();
    }

    public boolean isInternal() {
        return prPracticeBase.getKind().isInternal();
    }

    @SuppressWarnings("unused")
    public boolean isExternal() {
        return !isInternal();
    }

    //Getters and setters
    public PrPracticeBase getPrPracticeBase() {
        return prPracticeBase;
    }

    public void setPrPracticeBase(PrPracticeBase prPracticeBase) {
        this.prPracticeBase = prPracticeBase;
    }

    public Long getPrPracticeBaseId() {
        return prPracticeBaseId;
    }

    public void setPrPracticeBaseId(Long prPracticeBaseId) {
        this.prPracticeBaseId = prPracticeBaseId;
    }

    public OrgUnit getInternalOrganization() {
        return internalOrganization;
    }

    public void setInternalOrganization(OrgUnit internalOrganization) {
        this.internalOrganization = internalOrganization;
    }

    public ExternalOrgUnit getExternalOrganization() {
        return externalOrganization;
    }

    public void setExternalOrganization(ExternalOrgUnit externalOrganization) {
        this.externalOrganization = externalOrganization;
    }
    //!Getters and setters

}
