/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ProductionTrainingResultsReportManager;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.Add.ProductionTrainingResultsReportAdd;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.StudentPreallocationManager;

/**
 * @author rsizonenko
 * @since 22.10.2014
 */
public class ProductionTrainingResultsReportListUI extends UIPresenter {

    private EducationYear educationYear;

    @Override
    public void onComponentRefresh()
    {
        if (educationYear == null)
            educationYear = DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), true);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(ProductionTrainingResultsReportList.EDUCATION_YEAR, educationYear);
    }

    public void onClickAddReport()
    {
        _uiActivation.asRegion(ProductionTrainingResultsReportAdd.class).parameter(ProductionTrainingResultsReportList.EDUCATION_YEAR, educationYear.getId()).activate();
    }


    public void onClickPrint()
    {
        ProductionTrainingResultsReport report = DataAccessServices.dao().get(getListenerParameterAsLong());

        if (report.getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(report.getContent()),
                false
        );
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}