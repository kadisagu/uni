package ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;

import java.util.Collections;
import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

public class PrPracticeAssignmentDSHandler extends DefaultSearchDataSourceHandler
{

    private static final String ASSIGNMENT = "base";
    private static final String EXTRACT = "ext";

    public PrPracticeAssignmentDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        // Обязательные фильтры на форме добавления/редактирования
        Long orgUnitId = context.get(RESPONSIBLE_ORG_UNIT_F);
        Date startDate = context.get(START_DATE);
        Date endDate = context.get(END_DATE);

        if (orgUnitId == null || startDate == null || endDate == null)
            return ListOutputBuilder.get(input, Collections.emptyList()).pageable(true).build();

        Long paragraphId = context.get(PARAGRAPH_ID);
        String practiceType = context.get(PRACTICE_CODE);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, ASSIGNMENT)
                .column(ASSIGNMENT)
                .where(eq(
                        property(PrPracticeAssignment.state().code().fromAlias(ASSIGNMENT)),
                        value(PrPracticeAssignmentStateCodes.ACCEPTED)
                ))
                .where(eq(
                                property(PrPracticeAssignment.practice().registryElementPart().registryElement().owner().id().fromAlias(ASSIGNMENT)),
                                value(orgUnitId))
                )
                .where(inDay(PrPracticeAssignment.dateStart().fromAlias(ASSIGNMENT), startDate))
                .where(inDay(PrPracticeAssignment.dateEnd().fromAlias(ASSIGNMENT), endDate));

        // Фильтрация с учётом типа направления на практику
        if(!StringUtils.isEmpty(practiceType))
        {
            builder.where(eq(
                    property(PrPracticeAssignment.practice().registryElementPart().registryElement().parent().code().fromAlias(ASSIGNMENT)),
                    value(practiceType)
            ));
        }

        Long formativeOrgUnitId = context.get(FORMATIVE_ORG_UNIT_F);
        if(formativeOrgUnitId != null)
            builder.where(eq(property(ASSIGNMENT, PrPracticeAssignment.practice().student().educationOrgUnit().formativeOrgUnit().id()), value(formativeOrgUnitId)));

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(PrPracticeExtract.class, "ppe")
                .where(eq(property("ppe", PrPracticeExtract.entity()), property(ASSIGNMENT)));

        if (paragraphId != null)
        {
            subBuilder.where(ne(property("ppe", PrPracticeExtract.paragraph().id()), value(paragraphId)));
        }

        builder.where(notExists(subBuilder.buildQuery()));

        // Order
        builder.order(property(PrPracticeAssignment.practice().student().person().identityCard().fullFio().fromAlias(ASSIGNMENT)));
        builder.order(property(PrPracticeAssignment.practice().student().id().fromAlias(ASSIGNMENT)));
        // !Order

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }

}
