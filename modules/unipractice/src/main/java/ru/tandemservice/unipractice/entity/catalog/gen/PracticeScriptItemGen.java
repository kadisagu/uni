package ru.tandemservice.unipractice.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import ru.tandemservice.unipractice.entity.catalog.PracticeScriptItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатные формы модуля «Практики студентов»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PracticeScriptItemGen extends ScriptItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.catalog.PracticeScriptItem";
    public static final String ENTITY_NAME = "practiceScriptItem";
    public static final int VERSION_HASH = -657137851;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PracticeScriptItemGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PracticeScriptItemGen> extends ScriptItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PracticeScriptItem.class;
        }

        public T newInstance()
        {
            return (T) new PracticeScriptItem();
        }
    }
    private static final Path<PracticeScriptItem> _dslPath = new Path<PracticeScriptItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PracticeScriptItem");
    }
            

    public static class Path<E extends PracticeScriptItem> extends ScriptItem.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return PracticeScriptItem.class;
        }

        public String getEntityName()
        {
            return "practiceScriptItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
