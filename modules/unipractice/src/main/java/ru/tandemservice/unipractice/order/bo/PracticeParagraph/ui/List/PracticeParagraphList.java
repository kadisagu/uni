package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.Pub.PracticeOrderPub;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphDSHandler;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ChangePub.PracticeParagraphChangePub;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.Pub.PracticeParagraphPub;
import ru.tandemservice.unipractice.order.entity.PrAbstractParagraph;

import java.util.HashMap;

import static org.tandemframework.caf.ui.IUIPresenter.PUBLISHER_ID;
import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes.*;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.wrapper.PrPracticeParagraphWrapper.*;

@Configuration
public class PracticeParagraphList extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(PARAGRAPH_LIST_DS, prParagraphListDS(), practiceParagraphDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint prParagraphListDS()
    {
        return columnListExtPointBuilder(PARAGRAPH_LIST_DS)
                .addColumn(publisherColumn(PARAGRAPH_NAME_C, TITLE_FULL_C)
                        .publisherLinkResolver(new IPublisherLinkResolver() {
                                @Override
                                public Object getParameters(IEntity iEntity) {
                                    final IAbstractParagraph current = ((DataWrapper) iEntity).getWrapped();
                                    HashMap<String, Long> parameters = new HashMap<>();
                                    parameters.put(ORDER_ID, current.getOrder().getId());
                                    parameters.put(PARAGRAPH_ID, current.getId());
                                    return parameters;
                                }

                                @Override
                                public String getComponentName(IEntity iEntity) {
                                    PrAbstractParagraph paragraph = ((DataWrapper)iEntity).getWrapped();
                                    switch (paragraph.getOrder().getType().getCode()){
                                        case PREDDIPLOM:
                                        case PROIZVODSTV:
                                            return PracticeParagraphPub.class.getSimpleName();
                                        case PREDDIPLOM_CHANGE:
                                        case PROIZVODSTV_CHANGE:
                                            return PracticeParagraphChangePub.class.getSimpleName();
                                        default: return null;
                                    }
                                }
                        })
                )
                .addColumn(publisherColumn(CHANGEABLE_ORDER_NUMBER_C, CHANGEABLE_ORDER_NUMBER_C).visible("ui:desiredOrderType")
                        .publisherLinkResolver(new IPublisherLinkResolver() {
                               @Override
                               public Object getParameters(IEntity iEntity) {
                                   HashMap<String, Long> parameters = new HashMap<>();
                                   DataWrapper dataWrapper = (DataWrapper) iEntity;
                                   Long orderId = (Long) dataWrapper.getProperty(TARGET_ORDER_ID);
                                   parameters.put(PUBLISHER_ID, orderId);
                                   return parameters;
                               }

                               @Override
                               public String getComponentName(IEntity iEntity) {
                                   return PracticeOrderPub.class.getSimpleName();
                               }
                        })
                )
                .addColumn(publisherColumn(FORMATIVE_ORG_UNIT_C, OrgUnit.title())
                        .entityListProperty(FORMATIVE_ORG_UNIT_LIST)
                        .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(publisherColumn(RESPONSIBLE_ORG_UNIT_C, OrgUnit.title())
                        .entityListProperty(RESPONSIBLE_ORG_UNIT_LIST)
                        .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(textColumn(PRACTICE_BEGIN_DATE_C, PRACTICE_BEGIN_DATE_C).formatter(NewLineFormatter.NOBR_IN_LINES))
                .addColumn(textColumn(PRACTICE_END_DATE_C, PRACTICE_END_DATE_C).formatter(NewLineFormatter.NOBR_IN_LINES))
                .addColumn(textColumn(AMOUNT_OF_STUDENTS_C, AMOUNT_OF_STUDENTS_C))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER)
                        .permissionKey("ui:secModel.editParagraph")
                        .disabled(EDIT_DELETE_PARAGRAPH_DISABLED))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                        alert(PARAGRAPH_LIST_DS + ".delete.alert"))
                        .permissionKey("ui:secModel.deleteParagraph")
                        .disabled(EDIT_DELETE_PARAGRAPH_DISABLED))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> practiceParagraphDSHandler()
    {
        return new PracticeParagraphDSHandler(getName());
    }
}