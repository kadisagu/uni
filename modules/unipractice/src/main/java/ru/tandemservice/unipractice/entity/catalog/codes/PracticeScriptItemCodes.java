package ru.tandemservice.unipractice.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатные формы модуля «Практики студентов»"
 * Имя сущности : practiceScriptItem
 * Файл data.xml : unipractice.catalog.data.xml
 */
public interface PracticeScriptItemCodes
{
    /** Константа кода (code) элемента : Итоги производственного обучения (title) */
    String PRODUCTION_TRAINING_RESULTS_REPORT = "productionTrainingResultsReport";
    /** Константа кода (code) элемента : Итоги производственного обучения без обособления организации (title) */
    String PRODUCTION_TRAINING_RESULTS_REPORT_WITHOUT_DETACHED_ORG_UNIT = "productionTrainingResultsReportWithoutDetachedOrgUnit";

    Set<String> CODES = ImmutableSet.of(PRODUCTION_TRAINING_RESULTS_REPORT, PRODUCTION_TRAINING_RESULTS_REPORT_WITHOUT_DETACHED_ORG_UNIT);
}
