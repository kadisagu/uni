package ru.tandemservice.unipractice.component.student.StudentPracticeTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uni.component.student.StudentPub.IStudentModel;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setStudentModel((IStudentModel) component.getModel(component.getName()));
        getDao().prepare(model);
    }

}