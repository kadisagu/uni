package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.*;

@Configuration
public class PrPracticeAssignmentManager extends BusinessObjectManager {

    public static PrPracticeAssignmentManager instance() {
        return instance(PrPracticeAssignmentManager.class);
    }

    @Bean
    public IPracticeAssignmentDao practiceAssignmentDao() {
        return new PracticeAssignmentDao();
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceDSHandler() {
        return new PracticeDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceBaseDSHandler() {
        return new PracticeBaseDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler tutorDSHandler() {
        return new TutorDSHandler(getName());
    }

}
