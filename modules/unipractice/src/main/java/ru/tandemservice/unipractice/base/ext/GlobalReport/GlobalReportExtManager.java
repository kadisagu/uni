/* $Id$ */
package ru.tandemservice.unipractice.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.List.ProductionTrainingResultsReportList;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.ui.List.StudentPreallocationList;

/**
 * @author Andrey Avetisov
 * @since 15.10.2014
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;


    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("practiceList", new GlobalReportDefinition("practice", "practiceList", StudentPreallocationList.class.getSimpleName()))
                .add("productionTrainingList", new GlobalReportDefinition("practice", "productionTrainingList", ProductionTrainingResultsReportList.class.getSimpleName()))
                .create();
    }
}
