package ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;

import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

public class ParagraphDatesUtils {

    public static Set<DatePair> getDatePairs(Collection<PrPracticeExtract> extracts){
        final TreeSet<DatePair> datePairs = new TreeSet<>();
        for (PrPracticeExtract e : extracts){
            datePairs.add(
                new DatePair(e.getEntity().getDateStart(), e.getEntity().getDateEnd())
            );
        }
        return datePairs;
    }

    public static String[] getSortedFormattedDates(Collection<PrPracticeExtract> extracts){
        Set<DatePair> datePairs = getDatePairs(extracts);
        StringBuilder startDatessb = new StringBuilder();
        StringBuilder endDatessb = new StringBuilder();
        int i = 0;
        for(DatePair pair : datePairs){
            startDatessb.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(pair.startDate));
            endDatessb.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(pair.endDate));
            if(i != datePairs.size() - 1){
                startDatessb.append(", ");
                endDatessb.append(", ");
            }
            i++;
        }
        return new String[]{startDatessb.toString(),endDatessb.toString()};
    }

    public static class DatePair implements Comparable<DatePair> {
        Date startDate;
        Date endDate;

        DatePair(Date startDate, Date endDate){
            this.startDate = startDate;
            this.endDate = endDate;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof DatePair))
                return false;
            DatePair that = (DatePair) o;
            return startDate.equals(that.startDate) && endDate.equals(that.endDate);
        }

        @Override
        public int compareTo(DatePair o) {
            int cmpStartDate = startDate.compareTo(o.startDate);
            return cmpStartDate != 0 ? cmpStartDate : endDate.compareTo(o.endDate);
        }

        @Override
        public int hashCode() {
            int result = startDate != null ? startDate.hashCode() : 0;
            result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
            return result;
        }
    }

}
