package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.IStudentListModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeAssignmentState;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;

import java.util.List;

import static org.tandemframework.core.view.formatter.DateFormatter.DEFAULT_DATE_FORMATTER;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil.applySelectFilter;
import static org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil.applySimpleLikeFilter;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.IPracticeAssignmentDao.*;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.ASSIGNMENT_EDIT_DELETE_DISABLED;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.ASSIGNMENT_PRINT_DISABLED;

public class PracticeAssignmentDSHandler extends DefaultSearchDataSourceHandler
{
    //practiceAssignmentDS columns
    public static final String STUDENT_C = "student";
    public static final String FIO_C = "fio";
    public static final String FORMATIVE_ORG_UNIT_C = "formativeOrgUnit";
    public static final String GROUP_C = "group";
    public static final String COMPENSATION_TYPE_C = "compensationType";
    public static final String PRACTICE_TYPE = "practiceType";
    public static final String DEVELOP_FORM_C = "developForm";
    public static final String SPECIALITY_C = "specialityColumn";
    public static final String SPECIALIZATION_C = "specializationColumn";
    public static final String ASSIGNMENT_STATE_C = "assignmentState";
    public static final String PRACTICE_BASE_C_TITLE = "practiceBase";

    //practiceAssignmentStudentDS columns
    public static final String EDUCATION_YEAR_C = "educationYear";
    public static final String GRID_TERM_C = "gridTerm";
    public static final String PRACTICE_TITLE_C = "practiceTitle";
    public static final String PRACTICE_BASE_C_EXT = "practiceBaseExt";
    public static final String DATES_C_EXT = "datesExt";

    public PracticeAssignmentDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long studentId = context.get(F_STUDENT_ID);
        IdentifiableWrapper statusOption = context.get(F_STUDENT_STATUS_OPTION);
        EducationYear eduYear = context.get(F_EDUCATION_YEAR);
        String lastName = context.get(F_PERSON_LASTNAME);
        String firstName = context.get(F_PERSON_FIRSTNAME);
        String middleName = context.get(F_PERSON_MIDDLENAME);
        String bookNumber = context.get(F_BOOK_NUMBER);
        EppRegistryStructure practiceKind = context.get(F_PRACTICE_KIND);
        StudentStatus studentStatus = context.get(F_STUDENT_STATE);
        CompensationType compensationType = context.get(F_COMPENSATION_TYPE);
        DataWrapper taWrapper = context.get(F_TARGET_ADMISSION);
        List<DevelopForm> developFormList = context.get(F_DEVELOP_FORM);
        List<Course> courseList = context.get(F_COURSE);
        List<OrgUnit> formativeOrgUnitList = context.get(F_FORMATIVE_ORG_UNIT);
        List<OrgUnit> producingOrgUnitList = context.get(F_PRODUCING_ORG_UNIT);
        PrPracticeBase practiceBase = context.get(F_PRACTICE_BASE);
        PrPracticeAssignmentState assignmentState = context.get(F_REFERRAL_STATE);
        String acadGroupTitle = context.get(F_GROUP);
        List<EducationLevelsHighSchool> directionList = context.get(F_EDU_LEVEL_HIGH_SCHOOL);

        final String MAIN_ALIAS = "assignment";
        final String PRACTICE_ALIAS = "practice";
        final String STUDENT_ALIAS = "stud";

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, MAIN_ALIAS);
        dql.joinPath(DQLJoinType.inner, PrPracticeAssignment.practice().fromAlias(MAIN_ALIAS), PRACTICE_ALIAS);
        dql.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias(PRACTICE_ALIAS), STUDENT_ALIAS);
        dql.column(MAIN_ALIAS);

        //0.Фильтрация по студенту
        applySelectFilter(dql, STUDENT_ALIAS, Student.id(), studentId);
        //1.Статус студента
        applySelectFilter(dql, STUDENT_ALIAS, Student.status().active(), statusOption != null ? IStudentListModel.STUDENT_STATUS_ACTIVE.equals(statusOption.getId()) : null);
        //2.Учебный год
        applySelectFilter(dql, PRACTICE_ALIAS, EppStudentWorkPlanElement.year().educationYear(), eduYear);
        //3.Фамилия
        applySimpleLikeFilter(dql, STUDENT_ALIAS, Student.person().identityCard().lastName(), lastName);
        //4.Имя
        applySimpleLikeFilter(dql, STUDENT_ALIAS, Student.person().identityCard().firstName(), firstName);
        //5.Отчество
        applySimpleLikeFilter(dql, STUDENT_ALIAS, Student.person().identityCard().middleName(), middleName);
        //6.Номер зачетной книжки
        applySimpleLikeFilter(dql, STUDENT_ALIAS, Student.bookNumber(), bookNumber);
        //7.Вид практики (учебная, производственная, педагогическая, научно-исследовательская, преддипломная) - в фильтре выводятся все элементы справочника «Структура реестра мероприятий», подчиненные элементу «Практика»;
        applySelectFilter(dql, PRACTICE_ALIAS, EppStudentWorkPlanElement.registryElementPart().registryElement().parent(), practiceKind);
        //8.Состояние студента
        applySelectFilter(dql, STUDENT_ALIAS, Student.status(), studentStatus);
        //9.Вид возмещения затрат
        applySelectFilter(dql, STUDENT_ALIAS, Student.compensationType(), compensationType);
        //10.Целевой приём
        applySelectFilter(dql, STUDENT_ALIAS, Student.targetAdmission(), TwinComboDataSourceHandler.getSelectedValue(taWrapper));
        //11.Форма освоения
        applySelectFilter(dql, STUDENT_ALIAS, Student.educationOrgUnit().developForm(), developFormList);
        //12.Курс
        applySelectFilter(dql, STUDENT_ALIAS, Student.course(), courseList);
        //13.Формирующее подразделение
        applySelectFilter(dql, STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit(), formativeOrgUnitList);
        //14.Выпускающее подразделение
        applySelectFilter(dql, STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().orgUnit(), producingOrgUnitList);
        //15.База практики
        applySelectFilter(dql, MAIN_ALIAS, PrPracticeAssignment.practiceBase(), practiceBase);
        //16.Состояние направления
        applySelectFilter(dql, MAIN_ALIAS, PrPracticeAssignment.state(), assignmentState);
        //17.Академ. группа
        applySimpleLikeFilter(dql, STUDENT_ALIAS, Student.group().title(), acadGroupTitle);
        //18.Направление подготовки (специальность)
        applySelectFilter(dql, STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool(), directionList);

        String columnName = input.getEntityOrder().getColumnName();
        OrderDirection dir = input.getEntityOrder().getDirection();

        // Order
        if (PRACTICE_BASE_C_EXT.equalsIgnoreCase(columnName)) {
            dql.order(property(PrPracticeAssignment.practiceBase().kind().title().fromAlias(MAIN_ALIAS)), dir);
        } else if (DATES_C_EXT.equalsIgnoreCase(columnName)) {
            dql.order(property(PrPracticeAssignment.dateStart().fromAlias(MAIN_ALIAS)), dir);
            dql.order(property(PrPracticeAssignment.dateEnd().fromAlias(MAIN_ALIAS)), dir);
        } else if (SPECIALITY_C.equalsIgnoreCase(columnName) || SPECIALIZATION_C.equalsIgnoreCase(columnName)) {
            dql.order(property(PRACTICE_ALIAS, EppStudentWorkPlanElement.student().educationOrgUnit().educationLevelHighSchool().title()), dir);
        } else if (FIO_C.equalsIgnoreCase(columnName)) {
            dql.order(property(STUDENT_ALIAS, Student.person().identityCard().lastName()), dir);
            dql.order(property(STUDENT_ALIAS, Student.person().identityCard().firstName()), dir);
            dql.order(property(STUDENT_ALIAS, Student.person().identityCard().middleName()), dir);
        }
        else if (PRACTICE_BASE_C_TITLE.equalsIgnoreCase(columnName)) {
            dql.joinPath(DQLJoinType.inner, PrPracticeAssignment.practiceBase().fromAlias(MAIN_ALIAS), "base");
            PrPracticeBase.addJoinsForUseTitle(dql, "base", "extOrgUnit", "extOuParent", "legalForm", "intOrgUnit");
            dql.order(PrPracticeBase.createTitleDQLExpression("extOrgUnit", "extOuParent", "legalForm", "intOrgUnit"), dir);
        }
        else {
            dql.order(property(MAIN_ALIAS, input.getEntityOrder().getKeyString()), dir);
        }

        return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build().transform((PrPracticeAssignment assignment) -> {
            DataWrapper wrapper = new DataWrapper(assignment);

            PrPracticeAssignment wrapped = wrapper.getWrapped();
            PrPracticeBase base = wrapped.getPracticeBase();
            if (base != null)
            {
                wrapper.setProperty(PRACTICE_BASE_C_EXT, String.format("%s | %s", base.getTitle(), base.getKind().getTitle()));
            }

            String dStart = DEFAULT_DATE_FORMATTER.format(wrapped.getDateStart());
            String dEnd = DEFAULT_DATE_FORMATTER.format(wrapped.getDateEnd());
            wrapper.setProperty(DATES_C_EXT, String.format("%s \u2014 %s", dStart, dEnd));
            wrapper.setProperty(ASSIGNMENT_EDIT_DELETE_DISABLED, !wrapped.getState().getCode().equals(PrPracticeAssignmentStateCodes.FORMING));
            wrapper.setProperty(ASSIGNMENT_PRINT_DISABLED, false);

            return wrapper;
        });
    }

}