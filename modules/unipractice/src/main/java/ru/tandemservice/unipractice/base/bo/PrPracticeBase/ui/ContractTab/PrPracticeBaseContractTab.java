/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.ContractTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.PubCtr.CtrContractVersionPubCtr;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContract.logic.PrPracticeHoldingContractDSHandler;
import ru.tandemservice.unipractice.entity.PrPracticeBaseContract;

/**
 * @author azhebko
 * @since 07.11.2014
 */
@Configuration
public class PrPracticeBaseContractTab extends BusinessComponentManager
{
    public static final String DS_PRACTICE_HOLDING_CONTRACT = "practiceHoldingContractDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(searchListDS(DS_PRACTICE_HOLDING_CONTRACT, practiceHoldingContractDSColumns(), practiceHoldingContractDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtPoint practiceHoldingContractDSColumns()
    {
        IPublisherLinkResolver linkResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                DataWrapper row = (DataWrapper) entity;
                return row.<PrPracticeBaseContract>get(PrPracticeHoldingContractDSHandler.VP_PRACTICE_BASE_CONTRACT).getId();
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return CtrContractVersionPubCtr.class.getSimpleName();
            }
        };

        return columnListExtPointBuilder(DS_PRACTICE_HOLDING_CONTRACT)
            .addColumn(publisherColumn("contractNumber", CtrContractVersion.contract().number())
                .publisherLinkResolver(linkResolver)
                .width("120px"))

            .addColumn(textColumn("duration", CtrContractVersion.durationAsString()))
            .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler practiceHoldingContractDSHandler()
    {
        return new PrPracticeHoldingContractDSHandler(this.getName())
            .order(CtrContractVersion.contract().number())
            .pageable(true);
    }
}