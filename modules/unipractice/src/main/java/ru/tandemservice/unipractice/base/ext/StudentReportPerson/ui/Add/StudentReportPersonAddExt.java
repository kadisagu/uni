/* $Id$ */
package ru.tandemservice.unipractice.base.ext.StudentReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAdd;
import ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.DataTab.PrPracticeReportPersonDataTab;
import ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson.ui.PrintBlock.PrPracticeReportPersonPrintBlock;

/**
 * @author nvankov
 * @since 10/28/14
 */

@Configuration
public class StudentReportPersonAddExt extends BusinessComponentExtensionManager
{
//    public static final String STUDENT_PRACTICE_DATA = "studentPracticeData";
    public static final String STUDENT_PRACTICE_PRINT_DATA = "studentPracticePrintData";
    public static final String STUDENT_PRACTICE_DATA_TAB = "studentPracticeDataTab";


    @Autowired
    private StudentReportPersonAdd _reportPersonAdd;

    @Bean
    public BlockListExtension studentScheetBlockListExtension()
    {
        return blockListExtensionBuilder(_reportPersonAdd.studentScheetBlockListExtPoint())
                .addBlock(componentBlock(STUDENT_PRACTICE_PRINT_DATA, PrPracticeReportPersonPrintBlock.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_reportPersonAdd.studentTabPanelExtPoint())
                .addTab(componentTab(STUDENT_PRACTICE_DATA_TAB, PrPracticeReportPersonDataTab.class))
                .addAllAfter(StudentReportPersonAdd.STUDENT_COMMON_TAB)
                .create();
    }

}
