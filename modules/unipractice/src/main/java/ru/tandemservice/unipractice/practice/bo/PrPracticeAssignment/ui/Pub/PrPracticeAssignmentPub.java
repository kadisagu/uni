package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;

@Configuration
public class PrPracticeAssignmentPub extends BusinessComponentManager {

    private static final String PR_PRACTICE_ASSIGNMENT_BLOCK_LIST = "prPracticeAssignmentBlockList";
    private static final String PR_PRACTICE_ASSIGNMENT_BLOCK = "prPracticeAssignmentBlock";
    private static final String STUDENT_INFO_BLOCK = "studentInfoBlock";

    @Bean
    public BlockListExtPoint prPracticeAssignmentBlockListExtPoint(){
        return blockListExtPointBuilder(PR_PRACTICE_ASSIGNMENT_BLOCK_LIST)
                .addBlock(htmlBlock(PR_PRACTICE_ASSIGNMENT_BLOCK, "PrPracticeAssignmentBlock").create())
                .addBlock(htmlBlock(STUDENT_INFO_BLOCK, "StudentInfoBlock").create())
                .create();
    }

}
