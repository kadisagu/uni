package ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic;

public class PrPracticeBaseConstants {

    public static final String PR_PRACTICE_BASE_ID = "prPracticeBaseId";

    public static final String EXTERNAL_ORGANIZATION_DS = "externalOrganizationDS";
    public static final String INTERNAL_ORGANIZATION_DS = "internalOrganizationDS";
    public static final String PR_PRACTICE_BASE_KIND_DS = "prPracticeBaseKindDS";
    public static final String PR_PRACTICE_BASE_DS = "prPracticeBaseDS";

    //PrPracticeBaseDSHandler
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_KIND = "kind";
    public static final String COLUMN_ARCHIVAL = "archival";
    //!PrPracticeBaseDSHandler

    public static final String PR_PRACTICE_BASE_KIND_FILTER = "prPracticeBaseKindFilter";
    public static final String PR_PRACTICE_BASE_STATUS_FILTER = "prPracticeBaseStatusFilter";
    public static final String PR_PRACTICE_BASE_KIND_INTERNAL = "prPracticeBaseKindInternalFilter";
}
