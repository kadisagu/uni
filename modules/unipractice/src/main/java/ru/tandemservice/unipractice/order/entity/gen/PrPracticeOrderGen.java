package ru.tandemservice.unipractice.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderType;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказ на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeOrderGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.order.entity.PrPracticeOrder";
    public static final String ENTITY_NAME = "prPracticeOrder";
    public static final int VERSION_HASH = 1421613242;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_CREATE_DATE = "createDate";
    public static final String L_STATE = "state";
    public static final String L_TYPE = "type";
    public static final String P_COMMIT_DATE = "commitDate";
    public static final String P_ACTION_DATE = "actionDate";
    public static final String P_COMMIT_DATE_SYSTEM = "commitDateSystem";
    public static final String P_TEXT_PARAGRAPH = "textParagraph";
    public static final String P_EXECUTOR = "executor";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_PRINT_FORM = "printForm";

    private String _number;     // Номер
    private Date _createDate;     // Дата формирования
    private OrderStates _state;     // Состояние приказа
    private PrPracticeOrderType _type;     // Тип приказа
    private Date _commitDate;     // Дата подписания
    private Date _actionDate;     // Дата вступления в силу
    private Date _commitDateSystem;     // Дата проведения
    private String _textParagraph;     // Текстовый параграф
    private String _executor;     // Исполнитель
    private EducationYear _educationYear;     // Учебный год
    private DatabaseFile _printForm;     // Сохраненная печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     */
    @NotNull
    public OrderStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние приказа. Свойство не может быть null.
     */
    public void setState(OrderStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    public PrPracticeOrderType getType()
    {
        return _type;
    }

    /**
     * @param type Тип приказа. Свойство не может быть null.
     */
    public void setType(PrPracticeOrderType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Дата подписания.
     */
    public Date getCommitDate()
    {
        return _commitDate;
    }

    /**
     * @param commitDate Дата подписания.
     */
    public void setCommitDate(Date commitDate)
    {
        dirty(_commitDate, commitDate);
        _commitDate = commitDate;
    }

    /**
     * @return Дата вступления в силу.
     */
    public Date getActionDate()
    {
        return _actionDate;
    }

    /**
     * @param actionDate Дата вступления в силу.
     */
    public void setActionDate(Date actionDate)
    {
        dirty(_actionDate, actionDate);
        _actionDate = actionDate;
    }

    /**
     * @return Дата проведения.
     */
    public Date getCommitDateSystem()
    {
        return _commitDateSystem;
    }

    /**
     * @param commitDateSystem Дата проведения.
     */
    public void setCommitDateSystem(Date commitDateSystem)
    {
        dirty(_commitDateSystem, commitDateSystem);
        _commitDateSystem = commitDateSystem;
    }

    /**
     * @return Текстовый параграф.
     */
    public String getTextParagraph()
    {
        return _textParagraph;
    }

    /**
     * @param textParagraph Текстовый параграф.
     */
    public void setTextParagraph(String textParagraph)
    {
        dirty(_textParagraph, textParagraph);
        _textParagraph = textParagraph;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Сохраненная печатная форма.
     */
    public DatabaseFile getPrintForm()
    {
        return _printForm;
    }

    /**
     * @param printForm Сохраненная печатная форма.
     */
    public void setPrintForm(DatabaseFile printForm)
    {
        dirty(_printForm, printForm);
        _printForm = printForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PrPracticeOrderGen)
        {
            setNumber(((PrPracticeOrder)another).getNumber());
            setCreateDate(((PrPracticeOrder)another).getCreateDate());
            setState(((PrPracticeOrder)another).getState());
            setType(((PrPracticeOrder)another).getType());
            setCommitDate(((PrPracticeOrder)another).getCommitDate());
            setActionDate(((PrPracticeOrder)another).getActionDate());
            setCommitDateSystem(((PrPracticeOrder)another).getCommitDateSystem());
            setTextParagraph(((PrPracticeOrder)another).getTextParagraph());
            setExecutor(((PrPracticeOrder)another).getExecutor());
            setEducationYear(((PrPracticeOrder)another).getEducationYear());
            setPrintForm(((PrPracticeOrder)another).getPrintForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeOrderGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeOrder.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "createDate":
                    return obj.getCreateDate();
                case "state":
                    return obj.getState();
                case "type":
                    return obj.getType();
                case "commitDate":
                    return obj.getCommitDate();
                case "actionDate":
                    return obj.getActionDate();
                case "commitDateSystem":
                    return obj.getCommitDateSystem();
                case "textParagraph":
                    return obj.getTextParagraph();
                case "executor":
                    return obj.getExecutor();
                case "educationYear":
                    return obj.getEducationYear();
                case "printForm":
                    return obj.getPrintForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "state":
                    obj.setState((OrderStates) value);
                    return;
                case "type":
                    obj.setType((PrPracticeOrderType) value);
                    return;
                case "commitDate":
                    obj.setCommitDate((Date) value);
                    return;
                case "actionDate":
                    obj.setActionDate((Date) value);
                    return;
                case "commitDateSystem":
                    obj.setCommitDateSystem((Date) value);
                    return;
                case "textParagraph":
                    obj.setTextParagraph((String) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "printForm":
                    obj.setPrintForm((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "createDate":
                        return true;
                case "state":
                        return true;
                case "type":
                        return true;
                case "commitDate":
                        return true;
                case "actionDate":
                        return true;
                case "commitDateSystem":
                        return true;
                case "textParagraph":
                        return true;
                case "executor":
                        return true;
                case "educationYear":
                        return true;
                case "printForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "createDate":
                    return true;
                case "state":
                    return true;
                case "type":
                    return true;
                case "commitDate":
                    return true;
                case "actionDate":
                    return true;
                case "commitDateSystem":
                    return true;
                case "textParagraph":
                    return true;
                case "executor":
                    return true;
                case "educationYear":
                    return true;
                case "printForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "createDate":
                    return Date.class;
                case "state":
                    return OrderStates.class;
                case "type":
                    return PrPracticeOrderType.class;
                case "commitDate":
                    return Date.class;
                case "actionDate":
                    return Date.class;
                case "commitDateSystem":
                    return Date.class;
                case "textParagraph":
                    return String.class;
                case "executor":
                    return String.class;
                case "educationYear":
                    return EducationYear.class;
                case "printForm":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeOrder> _dslPath = new Path<PrPracticeOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeOrder");
    }
            

    /**
     * @return Номер.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getState()
     */
    public static OrderStates.Path<OrderStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getType()
     */
    public static PrPracticeOrderType.Path<PrPracticeOrderType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Дата подписания.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getCommitDate()
     */
    public static PropertyPath<Date> commitDate()
    {
        return _dslPath.commitDate();
    }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getActionDate()
     */
    public static PropertyPath<Date> actionDate()
    {
        return _dslPath.actionDate();
    }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getCommitDateSystem()
     */
    public static PropertyPath<Date> commitDateSystem()
    {
        return _dslPath.commitDateSystem();
    }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getTextParagraph()
     */
    public static PropertyPath<String> textParagraph()
    {
        return _dslPath.textParagraph();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getPrintForm()
     */
    public static DatabaseFile.Path<DatabaseFile> printForm()
    {
        return _dslPath.printForm();
    }

    public static class Path<E extends PrPracticeOrder> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<Date> _createDate;
        private OrderStates.Path<OrderStates> _state;
        private PrPracticeOrderType.Path<PrPracticeOrderType> _type;
        private PropertyPath<Date> _commitDate;
        private PropertyPath<Date> _actionDate;
        private PropertyPath<Date> _commitDateSystem;
        private PropertyPath<String> _textParagraph;
        private PropertyPath<String> _executor;
        private EducationYear.Path<EducationYear> _educationYear;
        private DatabaseFile.Path<DatabaseFile> _printForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(PrPracticeOrderGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(PrPracticeOrderGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getState()
     */
        public OrderStates.Path<OrderStates> state()
        {
            if(_state == null )
                _state = new OrderStates.Path<OrderStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getType()
     */
        public PrPracticeOrderType.Path<PrPracticeOrderType> type()
        {
            if(_type == null )
                _type = new PrPracticeOrderType.Path<PrPracticeOrderType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Дата подписания.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getCommitDate()
     */
        public PropertyPath<Date> commitDate()
        {
            if(_commitDate == null )
                _commitDate = new PropertyPath<Date>(PrPracticeOrderGen.P_COMMIT_DATE, this);
            return _commitDate;
        }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getActionDate()
     */
        public PropertyPath<Date> actionDate()
        {
            if(_actionDate == null )
                _actionDate = new PropertyPath<Date>(PrPracticeOrderGen.P_ACTION_DATE, this);
            return _actionDate;
        }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getCommitDateSystem()
     */
        public PropertyPath<Date> commitDateSystem()
        {
            if(_commitDateSystem == null )
                _commitDateSystem = new PropertyPath<Date>(PrPracticeOrderGen.P_COMMIT_DATE_SYSTEM, this);
            return _commitDateSystem;
        }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getTextParagraph()
     */
        public PropertyPath<String> textParagraph()
        {
            if(_textParagraph == null )
                _textParagraph = new PropertyPath<String>(PrPracticeOrderGen.P_TEXT_PARAGRAPH, this);
            return _textParagraph;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(PrPracticeOrderGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Сохраненная печатная форма.
     * @see ru.tandemservice.unipractice.order.entity.PrPracticeOrder#getPrintForm()
     */
        public DatabaseFile.Path<DatabaseFile> printForm()
        {
            if(_printForm == null )
                _printForm = new DatabaseFile.Path<DatabaseFile>(L_PRINT_FORM, this);
            return _printForm;
        }

        public Class getEntityClass()
        {
            return PrPracticeOrder.class;
        }

        public String getEntityName()
        {
            return "prPracticeOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
