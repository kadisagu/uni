/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author rsizonenko
 * @since 22.10.2014
 */
@Configuration
public class ProductionTrainingResultsReportPub extends BusinessComponentManager {
}
