/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 21.10.2014
 */
public class StPreallocationEndDateDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PRACTICE_ASSIGNMENT_END_DATE = "practiceAssignmentEndDate";
    private static final String EDUCATION_LEVEL_HIGHSCHOOL = "educationLevelHighSchool";
    public static final String EDU_YEAR = "eduYear";
    public static final String PRACTICE_KIND = "practiceKind";
    public static final String START_ASSIGNMENT_DATE = "startAssignmentDate";

    public StPreallocationEndDateDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String educationYear = context.get(EDU_YEAR);
        String practiceKind = context.get(PRACTICE_KIND);
        EducationLevelsHighSchool educationLevelsHighSchool = context.get(EDUCATION_LEVEL_HIGHSCHOOL);
        Date startDate = context.get(START_ASSIGNMENT_DATE);

        List<DataWrapper> dataWrapperList = new ArrayList<>();
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "p")
                .order(property("p", PrPracticeAssignment.P_DATE_END))
                .column("p");

        if(educationLevelsHighSchool!=null)
            dql.where(eqValue(property("p", PrPracticeAssignment.practice().student().educationOrgUnit().educationLevelHighSchool()), educationLevelsHighSchool));
        if (educationYear != null)
            dql.where(eqValue(property("p", PrPracticeAssignment.practice().year().title()), educationYear));
        if (practiceKind != null)
            dql.where(eqValue(property("p", PrPracticeAssignment.practice().registryElementPart().registryElement().parent().title()), practiceKind));
        if (startDate != null)
            dql.where(betweenDays(PrPracticeAssignment.dateEnd().fromAlias("p"), startDate, CoreDateUtils.getYearFirstTimeMoment(3000)));

        Set keys = input.getPrimaryKeys();
        if (keys != null && !keys.isEmpty())
        {
            if (keys.size() == 1)
                dql.where(eq(property("p", PrPracticeAssignment.id()), commonValue(keys.iterator().next())));
            else
                dql.where(in(property("p", PrPracticeAssignment.id()), keys));
        }

        List<PrPracticeAssignment> prPracticeAssignmentList = dql.createStatement(context.getSession()).list();

        List<Date> includedDate = new ArrayList<>();
        for (PrPracticeAssignment practiceAssignment : prPracticeAssignmentList)
        {
            if(!includedDate.contains(practiceAssignment.getDateEnd()))
            {
                DataWrapper wrapper = new DataWrapper();
                wrapper.setId(practiceAssignment.getId());
                wrapper.put(PRACTICE_ASSIGNMENT_END_DATE, practiceAssignment.getDateEnd());
                dataWrapperList.add(wrapper);
            }
            includedDate.add(practiceAssignment.getDateEnd());

        }

        return ListOutputBuilder.get(input, dataWrapperList).pageable(false).build();
    }
}
