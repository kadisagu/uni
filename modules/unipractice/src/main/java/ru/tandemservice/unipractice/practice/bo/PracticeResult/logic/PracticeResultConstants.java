package ru.tandemservice.unipractice.practice.bo.PracticeResult.logic;


public class PracticeResultConstants {

    public static final String PRACTICE_ASSIGNMENT_ID = "prPracticeAssignment.id";
    public static final String PRACTICE_RESULT_ID = "practiceResultId";
    public static final String GRADE_SCALE_DS = "gradeScaleDS";
    public static final String ACTIVITY_ELEMENT_PART_ID = "activityElementPartId";
    public static final String EMPLOYMENT_FORM_DS = "employmentFormDS";
}
