package ru.tandemservice.unipractice.practice.bo.PracticeResult.ui.Pub;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeResult;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;
import ru.tandemservice.unipractice.practice.bo.PracticeResult.PracticeResultManager;
import ru.tandemservice.unipractice.practice.bo.PracticeResult.ui.AddEdit.PracticeResultAddEdit;
import ru.tandemservice.unipractice.practice.bo.PracticeResult.ui.ScanCopyAdd.PracticeResultScanCopyAdd;

import static ru.tandemservice.unipractice.practice.bo.PracticeResult.logic.PracticeResultConstants.PRACTICE_ASSIGNMENT_ID;
import static ru.tandemservice.unipractice.practice.bo.PracticeResult.logic.PracticeResultConstants.PRACTICE_RESULT_ID;

@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "prPracticeAssignmentId")
})
public class PracticeResultPubUI extends UIPresenter {

    private Long prPracticeAssignmentId;
    private PrPracticeResult prPracticeResult;
    private PrPracticeAssignment prPracticeAssignment;

    @Override
    public void onComponentRefresh() {
        prPracticeAssignment = DataAccessServices.dao().getNotNull(PrPracticeAssignment.class, prPracticeAssignmentId);
        prPracticeResult = PracticeResultManager.instance().practiceResultDao().getPracticeResultByAssignmentId(prPracticeAssignmentId);
    }

    public void onClickEdit(){
        _uiSupport.getRootUI().getActivationBuilder().asRegion(PracticeResultAddEdit.class)
                .parameter(PRACTICE_ASSIGNMENT_ID, prPracticeAssignmentId)
                .parameter(PRACTICE_RESULT_ID, prPracticeResult.getId())
                .activate();
    }

    public void onClickApply(){
        _uiSupport.getRootUI().getActivationBuilder().asRegion(PracticeResultAddEdit.class)
                .parameter(PRACTICE_ASSIGNMENT_ID, prPracticeAssignmentId)
                .activate();
    }

    public void onClickDelete(){
        DataAccessServices.dao().delete(prPracticeResult);
        _uiSupport.doRefresh();
    }

    public void onClickAddScanCopy(){
        _uiSupport.getRootUI().getActivationBuilder().asRegion(PracticeResultScanCopyAdd.class).parameter(PRACTICE_RESULT_ID, prPracticeResult.getId()).activate();
    }

    public void onClickDeleteScanCopy(){
        PracticeResultManager.instance().practiceResultDao().deleteScanCopy(prPracticeResult);
        _uiSupport.doRefresh();
    }

    public void onClickDownloadScanCopy(){
        DatabaseFile scanCopy = prPracticeResult.getScanCopy();
        if (scanCopy == null)
            throw new ApplicationException("Нет загруженной копии.");
        if (scanCopy.getContent() == null)
            throw new ApplicationException("Файл скан-копии пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(scanCopy), false);
    }

    public Long getPrPracticeAssignmentId() {
        return prPracticeAssignmentId;
    }

    public void setPrPracticeAssignmentId(Long prPracticeAssignmentId) {
        this.prPracticeAssignmentId = prPracticeAssignmentId;
    }

    public PrPracticeResult getPrPracticeResult() {
        return prPracticeResult;
    }

    public void setPrPracticeResult(PrPracticeResult prPracticeResult) {
        this.prPracticeResult = prPracticeResult;
    }

    public String getActualDates() {
        String result = "";
        if (prPracticeResult != null) {
            DateFormatter dateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;
            result = dateFormatter.format(prPracticeResult.getDateStart());
            result += " - " + dateFormatter.format(prPracticeResult.getDateEnd());
        }
        return result;
    }

    public boolean isResultsButtonsVisible()
	{
        return prPracticeAssignment.getState().getCode().equals(PrPracticeAssignmentStateCodes.ACCEPTED);
    }

	private static final ImmutableList<String> appropriateScanCopyStates = ImmutableList.of(
			PrPracticeAssignmentStateCodes.ACCEPTED, PrPracticeAssignmentStateCodes.FOR_APPROVAL, PrPracticeAssignmentStateCodes.FINISHED);

	public boolean isScanCopyButtonsVisible()
	{
		return appropriateScanCopyStates.contains(prPracticeAssignment.getState().getCode());
	}

    public boolean getResultExists() {
        return prPracticeResult != null;
    }

    public boolean isScanCopyExists(){
        return getResultExists() && prPracticeResult.getScanCopy() != null;
    }

    public boolean isResultsWithoutScanCopy(){
        return getResultExists() && prPracticeResult.getScanCopy() == null;
    }

    public PrPracticeAssignment getPrPracticeAssignment()
    {
        return prPracticeAssignment;
    }

    public void setPrPracticeAssignment(PrPracticeAssignment prPracticeAssignment)
    {
        this.prPracticeAssignment = prPracticeAssignment;
    }
}
