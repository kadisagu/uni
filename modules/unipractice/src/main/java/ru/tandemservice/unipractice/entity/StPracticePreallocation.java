package ru.tandemservice.unipractice.entity;

import com.google.common.collect.Lists;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.sec.ISecLocalEntityOwner;
import ru.tandemservice.unipractice.entity.gen.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Предварительное распределение студентов
 */
public class StPracticePreallocation extends StPracticePreallocationGen implements ISecLocalEntityOwner
{
    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return getOrgUnit() != null ? Lists.<IEntity>newArrayList(getOrgUnit()) : Collections.<IEntity>emptyList();
    }
}