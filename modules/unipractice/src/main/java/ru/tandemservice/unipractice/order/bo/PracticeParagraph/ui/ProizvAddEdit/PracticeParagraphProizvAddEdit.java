package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ProizvAddEdit;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSubselectType;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.PracticeParagraphManager;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Configuration
public class PracticeParagraphProizvAddEdit extends BusinessComponentManager
{

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRACTICE_ASSIGNMENT_DS, practiceAssignmentColumnListDS(), PracticeParagraphManager.instance().practiceAssignmentDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(RESPONSIBLE_ORG_UNIT_DS, PracticeParagraphManager.instance().practiceParagraphResponsibleOrgUnitDSHandler()))
                .addDataSource(selectDS(START_DATE_DS, PracticeParagraphManager.instance().practiceParagraphStartDateDSHandler()))
                .addDataSource(selectDS(END_DATE_DS, PracticeParagraphManager.instance().practiceParagraphEndDateDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint practiceAssignmentColumnListDS()
    {
        StudentGen.Path<Student> studentPath = PrPracticeAssignment.practice().student();
        return columnListExtPointBuilder(PRACTICE_ASSIGNMENT_DS)
                .addColumn(checkboxColumn("checkbox"))
                .addColumn(textColumn(FIO_C, studentPath.person().identityCard().fullFio()))
                .addColumn(textColumn(GROUP_C, studentPath.group().title()))
                .addColumn(textColumn(FORMATIVE_ORG_UNIT_C, studentPath.educationOrgUnit().formativeOrgUnit().fullTitle()))
                .addColumn(textColumn(RESPONSIBLE_ORG_UNIT_C, PrPracticeAssignment.practice().registryElementPart().registryElement().owner().title()))
                .addColumn(textColumn(PRACTICE_BASE_C, PrPracticeAssignment.practiceBase().title()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        final EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                Long eduYearId = context.getNotNull(EDU_YEAR);

                DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "pr")
                        .column(property("pr", PrPracticeAssignment.practice().student().educationOrgUnit().formativeOrgUnit().id()))
                        .where(notExists(PrPracticeExtract.class, PrPracticeExtract.L_ENTITY, (Object) property("pr")))
                        .where(eq(property("pr", PrPracticeAssignment.practice().year().educationYear().id()), value(eduYearId)));

                // Фильтрация с учётом типа направления на практику
                String practiceType = context.get(PRACTICE_CODE);
                if(!StringUtils.isEmpty(practiceType))
                    sub.where(eq(property("pr", PrPracticeAssignment.practice().registryElementPart().registryElement().parent().code()), value(practiceType)));

                // Оргюнит должен быть одним из всех ответственных подразделений практик из доступных направлений
                IDQLExpression availableAssignmentsCondition = eqSubquery(property(alias), DQLSubselectType.any, sub.buildQuery());

                IDQLExpression assignmentsWithParagraphId = null;
                Long paragraphId = context.get(PARAGRAPH_ID);
                if (paragraphId != null) {
                    DQLSelectBuilder sub2 = new DQLSelectBuilder().fromEntity(PrPracticeExtract.class, "ext")
                            .column(property(PrPracticeExtract.entity().practice().student().educationOrgUnit().formativeOrgUnit().id().fromAlias("ext")));
                    sub2.where(eq(property(PrPracticeExtract.paragraph().id().fromAlias("ext")), value(paragraphId)));
                    assignmentsWithParagraphId = eqSubquery(property(alias), DQLSubselectType.any, sub2.buildQuery());
                }

                IDQLExpression finalCondition = availableAssignmentsCondition;

                if (assignmentsWithParagraphId != null) {
                    finalCondition = or(finalCondition, assignmentsWithParagraphId);
                }
                dql.where(finalCondition);
            }
        };
        handler.order(OrgUnit.title());
        handler.filter(OrgUnit.title());
        return handler;
    }
}