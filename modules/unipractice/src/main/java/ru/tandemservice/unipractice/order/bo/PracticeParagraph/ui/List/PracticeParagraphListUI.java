package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes.*;
import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.util.ParagraphUtils.getParagraphEditComponentByOrderType;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Input({
    @Bind(key = UIPresenter.PUBLISHER_ID, binding = "order.id")
})
public class PracticeParagraphListUI extends UIPresenter
{
    private PrPracticeOrder order = new PrPracticeOrder();
    private CommonPostfixPermissionModel _secModel;

    @Override
    public void onComponentRefresh()
    {
        if (order.getId() != null) {
            order = DataAccessServices.dao().getNotNull(PrPracticeOrder.class, order.getId());
        }
        setSecModel(new CommonPostfixPermissionModel("prPracticeOrder"));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if(dataSource.getName().equals(PARAGRAPH_LIST_DS)){
            dataSource.put(ORDER, order);
        }
    }

    @SuppressWarnings("unused")
    public void onEditEntityFromList() {
        Class<? extends BusinessComponentManager> editClass = getParagraphEditComponentByOrderType(order.getType().getCode());
        _uiSupport.getRootUI().getActivationBuilder().asRegion(editClass)
                .parameter(ORDER_ID, order.getId())
                .parameter(PARAGRAPH_ID, getListenerParameterAsLong())
                .activate();
    }

    @SuppressWarnings("unused")
    public void onDeleteEntityFromList() {
        PracticeOrderManager.instance().practiceOrderDao().deleteParagraph(getListenerParameterAsLong());
    }

    public boolean isDesiredOrderType(){
        switch (order.getType().getCode()) {
            case PREDDIPLOM:
            case PROIZVODSTV:
                return false;
            case PREDDIPLOM_CHANGE:
            case PROIZVODSTV_CHANGE:
                return true;
            default: return false;
        }
    }

    public PrPracticeOrder getOrder() {
        return order;
    }

    public void setOrder(PrPracticeOrder order) {
        this.order = order;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}