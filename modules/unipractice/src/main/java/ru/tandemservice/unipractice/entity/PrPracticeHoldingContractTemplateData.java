package ru.tandemservice.unipractice.entity;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.PrPracticeHoldingContractTemplateManager;
import ru.tandemservice.unipractice.entity.gen.*;

/**
 * Данные шаблона версии об организации и проведении практики
 */
public class PrPracticeHoldingContractTemplateData extends PrPracticeHoldingContractTemplateDataGen
{
    @Override public ICtrContractTemplateManager getManager() { return PrPracticeHoldingContractTemplateManager.instance(); }
}