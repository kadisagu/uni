/* $Id$ */
package ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.wrapper;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;

import java.util.Date;

/**
 * @author nvankov
 * @since 10/16/14
 */
public class DateWrapper extends DataWrapper implements Comparable<DateWrapper>
{
    private Date _date;

    public DateWrapper(Date date)
    {
        super(date.getTime(), DateFormatter.DEFAULT_DATE_FORMATTER.format(date));
        _date = date;
    }

    public Date getDate()
    {
        return _date;
    }

    @Override
    public int compareTo(DateWrapper o)
    {
        return getDate().compareTo(o.getDate());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        DateWrapper that = (DateWrapper) o;

        if (_date != null ? !_date.equals(that._date) : that._date != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return _date.hashCode();
    }
}
