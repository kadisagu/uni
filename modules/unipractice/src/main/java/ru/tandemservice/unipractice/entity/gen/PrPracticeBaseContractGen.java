package ru.tandemservice.unipractice.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.unipractice.entity.PrPracticeBaseContract;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь базы прохождения практики с договором об организации и проведении практики студентов
 *
 * Связывает договор об организации и проведении практики студентов и базы проведения практики
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeBaseContractGen extends EntityBase
 implements INaturalIdentifiable<PrPracticeBaseContractGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.PrPracticeBaseContract";
    public static final String ENTITY_NAME = "prPracticeBaseContract";
    public static final int VERSION_HASH = -239098929;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTRACT_OBJECT = "contractObject";
    public static final String L_PRACTICE_BASE = "practiceBase";

    private CtrContractObject _contractObject;     // Договор
    private PrPracticeBaseExt _practiceBase;     // База прохождения практики

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор. Свойство не может быть null.
     */
    @NotNull
    public CtrContractObject getContractObject()
    {
        return _contractObject;
    }

    /**
     * @param contractObject Договор. Свойство не может быть null.
     */
    public void setContractObject(CtrContractObject contractObject)
    {
        dirty(_contractObject, contractObject);
        _contractObject = contractObject;
    }

    /**
     * @return База прохождения практики. Свойство не может быть null.
     */
    @NotNull
    public PrPracticeBaseExt getPracticeBase()
    {
        return _practiceBase;
    }

    /**
     * @param practiceBase База прохождения практики. Свойство не может быть null.
     */
    public void setPracticeBase(PrPracticeBaseExt practiceBase)
    {
        dirty(_practiceBase, practiceBase);
        _practiceBase = practiceBase;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PrPracticeBaseContractGen)
        {
            if (withNaturalIdProperties)
            {
                setContractObject(((PrPracticeBaseContract)another).getContractObject());
                setPracticeBase(((PrPracticeBaseContract)another).getPracticeBase());
            }
        }
    }

    public INaturalId<PrPracticeBaseContractGen> getNaturalId()
    {
        return new NaturalId(getContractObject(), getPracticeBase());
    }

    public static class NaturalId extends NaturalIdBase<PrPracticeBaseContractGen>
    {
        private static final String PROXY_NAME = "PrPracticeBaseContractNaturalProxy";

        private Long _contractObject;
        private Long _practiceBase;

        public NaturalId()
        {}

        public NaturalId(CtrContractObject contractObject, PrPracticeBaseExt practiceBase)
        {
            _contractObject = ((IEntity) contractObject).getId();
            _practiceBase = ((IEntity) practiceBase).getId();
        }

        public Long getContractObject()
        {
            return _contractObject;
        }

        public void setContractObject(Long contractObject)
        {
            _contractObject = contractObject;
        }

        public Long getPracticeBase()
        {
            return _practiceBase;
        }

        public void setPracticeBase(Long practiceBase)
        {
            _practiceBase = practiceBase;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PrPracticeBaseContractGen.NaturalId) ) return false;

            PrPracticeBaseContractGen.NaturalId that = (NaturalId) o;

            if( !equals(getContractObject(), that.getContractObject()) ) return false;
            if( !equals(getPracticeBase(), that.getPracticeBase()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getContractObject());
            result = hashCode(result, getPracticeBase());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getContractObject());
            sb.append("/");
            sb.append(getPracticeBase());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeBaseContractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeBaseContract.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeBaseContract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "contractObject":
                    return obj.getContractObject();
                case "practiceBase":
                    return obj.getPracticeBase();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "contractObject":
                    obj.setContractObject((CtrContractObject) value);
                    return;
                case "practiceBase":
                    obj.setPracticeBase((PrPracticeBaseExt) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "contractObject":
                        return true;
                case "practiceBase":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "contractObject":
                    return true;
                case "practiceBase":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "contractObject":
                    return CtrContractObject.class;
                case "practiceBase":
                    return PrPracticeBaseExt.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeBaseContract> _dslPath = new Path<PrPracticeBaseContract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeBaseContract");
    }
            

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseContract#getContractObject()
     */
    public static CtrContractObject.Path<CtrContractObject> contractObject()
    {
        return _dslPath.contractObject();
    }

    /**
     * @return База прохождения практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseContract#getPracticeBase()
     */
    public static PrPracticeBaseExt.Path<PrPracticeBaseExt> practiceBase()
    {
        return _dslPath.practiceBase();
    }

    public static class Path<E extends PrPracticeBaseContract> extends EntityPath<E>
    {
        private CtrContractObject.Path<CtrContractObject> _contractObject;
        private PrPracticeBaseExt.Path<PrPracticeBaseExt> _practiceBase;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseContract#getContractObject()
     */
        public CtrContractObject.Path<CtrContractObject> contractObject()
        {
            if(_contractObject == null )
                _contractObject = new CtrContractObject.Path<CtrContractObject>(L_CONTRACT_OBJECT, this);
            return _contractObject;
        }

    /**
     * @return База прохождения практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseContract#getPracticeBase()
     */
        public PrPracticeBaseExt.Path<PrPracticeBaseExt> practiceBase()
        {
            if(_practiceBase == null )
                _practiceBase = new PrPracticeBaseExt.Path<PrPracticeBaseExt>(L_PRACTICE_BASE, this);
            return _practiceBase;
        }

        public Class getEntityClass()
        {
            return PrPracticeBaseContract.class;
        }

        public String getEntityName()
        {
            return "prPracticeBaseContract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
