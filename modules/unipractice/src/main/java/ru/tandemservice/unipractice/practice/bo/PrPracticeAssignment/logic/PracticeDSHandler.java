package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.PRACTICE;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.STUDENT_ID;

public class PracticeDSHandler extends DefaultComboDataSourceHandler {

    private static final String ALIAS = "e";

    public PracticeDSHandler(String name) {
        super(name, EppStudentWorkPlanElement.class, EppStudentWorkPlanElement.registryElementPart().registryElement().parent().title());
    }

//    select b.*
//    from epp_studentepv_slot_t m
//    left join epp_reg_element_part_t p on m.activityelementpart_id = p.id
//    left join epp_reg_base_t b on b.id = p.registryelement_id
//    left join epp_reg_action_practice_t pr on pr.id = b.id
//    left join pr_practice_assignment_t a on m.id = a.practice_id
//            where
//    m.student_id = 1476308389219011733
//    and m.removaldate_p is null
//    and pr.id is not null
//    and a.id is null

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {
        super.prepareConditions(ep);

        EppStudentWorkPlanElement slot = ep.context.get(PRACTICE);
        if (slot != null) {
            // просто вернем данный EppStudentWorkPlanElement
            ep.dqlBuilder.where(eq(property(ALIAS), value(slot)));
        } else {
            // Проверка, что МСРП является практикой (eppStudentEpvSlot.activityElementPart.registryElement instanceOf eppRegistryPractice)
            ep.dqlBuilder.joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.registryElementPart().registryElement().fromAlias(ALIAS), "re");
            ep.dqlBuilder.where(instanceOf("re", EppRegistryPractice.class));

            // Фильтрация по студенту
            ep.dqlBuilder.where(eq(property(EppStudentWorkPlanElement.student().id().fromAlias(ALIAS)), commonValue(ep.context.get(STUDENT_ID))));

            // Выбор из актуальных
            ep.dqlBuilder.where(isNull(property(EppStudentWorkPlanElement.removalDate().fromAlias(ALIAS))));

            // Исключаем уже добавленные студенту практики
            DQLSelectBuilder neQuery = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "pa")
                    .where(eq(property("pa", PrPracticeAssignment.L_PRACTICE), property(ALIAS)));

            ep.dqlBuilder.where(notExists(neQuery.buildQuery()));
        }

    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order(property(EppStudentWorkPlanElement.year().educationYear().title().fromAlias(ALIAS)), OrderDirection.desc);
        ep.dqlBuilder.order(property(EppStudentWorkPlanElement.term().title().fromAlias(ALIAS)), OrderDirection.asc);
        ep.dqlBuilder.order(property(EppStudentWorkPlanElement.registryElementPart().registryElement().parent().title().fromAlias(ALIAS)), OrderDirection.asc);
    }
}