/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.List.ProductionTrainingResultsReportList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 27.10.2014
 */
public class ProductionTrainingResultsReportListDSHandler  extends DefaultSearchDataSourceHandler{



    public ProductionTrainingResultsReportListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        DQLSelectBuilder dqlbuilder = new DQLSelectBuilder().fromEntity(ProductionTrainingResultsReport.class, "r")
                .order(property("r", ProductionTrainingResultsReport.formingDate()), input.getEntityOrder().getDirection())

                .column("r");

        final EducationYear educationYear = (EducationYear) context.get(ProductionTrainingResultsReportList.EDUCATION_YEAR);

        if (educationYear != null) {
            dqlbuilder.where((eq(property(ProductionTrainingResultsReport.educationYear().fromAlias("r")),
                    value(educationYear.getTitle()))));
        }

        DSOutput output = DQLSelectOutputBuilder.get(input, dqlbuilder, context.getSession())
                .build();
        
        return output;
    }
}
