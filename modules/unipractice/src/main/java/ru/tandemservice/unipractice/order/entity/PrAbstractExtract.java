package ru.tandemservice.unipractice.order.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.order.entity.gen.PrAbstractExtractGen;

/**
 * Абстрактная выписка на абитуриента
 */
public abstract class PrAbstractExtract extends PrAbstractExtractGen implements IAbstractExtract
{
    public abstract PrPracticeAssignment getPracticeAssignment();

    @Override
    public void setType(ICatalogItem iCatalogItem)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setParagraph(IAbstractParagraph paragraph)
    {
        setParagraph((PrAbstractParagraph) paragraph);
    }

    @Override
    public void setState(ICatalogItem state)
    {
        setState((ExtractStates) state);
    }

    @Override
    public ICatalogItem getType()
    {
        return getParagraph().getOrder().getType();
    }

    @Override
    public String getTitle()
    {
        if (getParagraph() == null) {
            return this.getClass().getSimpleName();
        }
        return "Выписка " + getNumber() + "/" + getParagraph().getNumber() + " | " + getParagraph().getOrder().getTitle();
    }
}