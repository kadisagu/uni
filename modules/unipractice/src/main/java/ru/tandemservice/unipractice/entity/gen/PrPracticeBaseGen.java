package ru.tandemservice.unipractice.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * База прохождения практики
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeBaseGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.PrPracticeBase";
    public static final String ENTITY_NAME = "prPracticeBase";
    public static final int VERSION_HASH = -428380188;
    private static IEntityMeta ENTITY_META;

    public static final String L_KIND = "kind";
    public static final String P_ARCHIVAL = "archival";
    public static final String P_TITLE = "title";

    private PrPracticeBaseKind _kind;     // Вид базы практики
    private boolean _archival;     // В архиве

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид базы практики. Свойство не может быть null.
     */
    @NotNull
    public PrPracticeBaseKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Вид базы практики. Свойство не может быть null.
     */
    public void setKind(PrPracticeBaseKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    /**
     * @return В архиве. Свойство не может быть null.
     */
    @NotNull
    public boolean isArchival()
    {
        return _archival;
    }

    /**
     * @param archival В архиве. Свойство не может быть null.
     */
    public void setArchival(boolean archival)
    {
        dirty(_archival, archival);
        _archival = archival;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PrPracticeBaseGen)
        {
            setKind(((PrPracticeBase)another).getKind());
            setArchival(((PrPracticeBase)another).isArchival());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeBaseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeBase.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("PrPracticeBase is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "kind":
                    return obj.getKind();
                case "archival":
                    return obj.isArchival();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "kind":
                    obj.setKind((PrPracticeBaseKind) value);
                    return;
                case "archival":
                    obj.setArchival((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "kind":
                        return true;
                case "archival":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "kind":
                    return true;
                case "archival":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "kind":
                    return PrPracticeBaseKind.class;
                case "archival":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeBase> _dslPath = new Path<PrPracticeBase>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeBase");
    }
            

    /**
     * @return Вид базы практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBase#getKind()
     */
    public static PrPracticeBaseKind.Path<PrPracticeBaseKind> kind()
    {
        return _dslPath.kind();
    }

    /**
     * @return В архиве. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBase#isArchival()
     */
    public static PropertyPath<Boolean> archival()
    {
        return _dslPath.archival();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unipractice.entity.PrPracticeBase#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends PrPracticeBase> extends EntityPath<E>
    {
        private PrPracticeBaseKind.Path<PrPracticeBaseKind> _kind;
        private PropertyPath<Boolean> _archival;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид базы практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBase#getKind()
     */
        public PrPracticeBaseKind.Path<PrPracticeBaseKind> kind()
        {
            if(_kind == null )
                _kind = new PrPracticeBaseKind.Path<PrPracticeBaseKind>(L_KIND, this);
            return _kind;
        }

    /**
     * @return В архиве. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBase#isArchival()
     */
        public PropertyPath<Boolean> archival()
        {
            if(_archival == null )
                _archival = new PropertyPath<Boolean>(PrPracticeBaseGen.P_ARCHIVAL, this);
            return _archival;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unipractice.entity.PrPracticeBase#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(PrPracticeBaseGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return PrPracticeBase.class;
        }

        public String getEntityName()
        {
            return "prPracticeBase";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
