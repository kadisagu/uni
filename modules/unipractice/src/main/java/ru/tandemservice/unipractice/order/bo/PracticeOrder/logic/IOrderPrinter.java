package ru.tandemservice.unipractice.order.bo.PracticeOrder.logic;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

public interface IOrderPrinter {
    RtfDocument print(PrPracticeOrder prPracticeOrder);
}
