package ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unipractice.order.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes.*;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.EDIT_DELETE_PARAGRAPH_DISABLED;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.ORDER;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.wrapper.PrPracticeParagraphWrapper.*;

public class PracticeParagraphDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    private static final String PARAGRAPH = "par";
    private static final String EXTRACT = "extract";

    public PracticeParagraphDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        PrPracticeOrder order = context.get(ORDER);

        switch (order.getType().getCode()) {
            case PREDDIPLOM:
            case PROIZVODSTV:
                return getOutputForBaseOrders(input, order);
            case PREDDIPLOM_CHANGE:
            case PROIZVODSTV_CHANGE:
                return getOutputForChangeOrders(input, order);
            default: return null;
        }
    }

    private DSOutput getOutputForChangeOrders(DSInput input, PrPracticeOrder order) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PrPracticeChangeParagraph.class, PARAGRAPH).column(PARAGRAPH);
        builder.where(eq(
                property(PrPracticeChangeParagraph.order().fromAlias(PARAGRAPH)),
                value(order)));
        builder.order(property(PrPracticeChangeParagraph.number().fromAlias(PARAGRAPH)));
        DSOutput output = DQLSelectOutputBuilder.get(input, builder, getSession()).pageable(false).build();

        DQLSelectBuilder extBuilder = new DQLSelectBuilder().fromEntity(PrPracticeChangeExtract.class, EXTRACT).column(EXTRACT);
        extBuilder.where(eq(
                property(PrPracticeChangeExtract.paragraph().order().fromAlias(EXTRACT)),
                value(order)));
        List<PrPracticeChangeExtract> orderExtracts = extBuilder.createStatement(getSession()).list();

        List<DataWrapper> wrappedList = DataWrapper.wrap(output);
        for (DataWrapper wrappedItem : wrappedList) {
            PrPracticeChangeParagraph changeParagraph = wrappedItem.getWrapped();

            List<PrPracticeChangeExtract> paragraphExtracts = new ArrayList<>();
            for (PrPracticeChangeExtract extract : orderExtracts) {
                if (extract.getParagraph().getId().equals(changeParagraph.getId())) {
                    paragraphExtracts.add(extract);
                }
            }

            wrappedItem.setProperty(TITLE_FULL_C, "Параграф №" + changeParagraph.getNumber());

            Set<OrgUnit> formativeUnits = CommonBaseEntityUtil.getPropertiesSet(paragraphExtracts, PrPracticeChangeExtract.L_ENTITY + "."
                    + PrPracticeExtract.entity().practice().student().educationOrgUnit().formativeOrgUnit());
            wrappedItem.setProperty(FORMATIVE_ORG_UNIT_LIST, formativeUnits);

            DateFormatter dateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;
            PrPracticeOrder targetOrder = paragraphExtracts.get(0).getEntity().getParagraph().getOrder();
            wrappedItem.setProperty(CHANGEABLE_ORDER_NUMBER_C,
                    targetOrder.getType().getTitle()
                            + " №" + targetOrder.getNumber() + " от "
                            + dateFormatter.format(targetOrder.getCommitDate()));
            wrappedItem.setProperty(TARGET_ORDER_ID, targetOrder.getId());

            Set<OrgUnit> responsibleOrgUnits = CommonBaseEntityUtil.getPropertiesSet(paragraphExtracts, PrPracticeChangeExtract.L_ENTITY + "."
                    + PrPracticeExtract.entity().practice().registryElementPart().registryElement().owner());
            wrappedItem.setProperty(RESPONSIBLE_ORG_UNIT_LIST, responsibleOrgUnits);

            List<PrPracticeExtract> propertiesList = CommonBaseEntityUtil.getPropertiesList(paragraphExtracts, PrPracticeChangeExtract.entity());
            Set<ParagraphDatesUtils.DatePair> datePairs = ParagraphDatesUtils.getDatePairs(propertiesList);
            String beginDate = "";
            String endDate = "";
            for (ParagraphDatesUtils.DatePair datePair : datePairs) {
                beginDate += dateFormatter.format(datePair.startDate) + "\n";
                endDate += dateFormatter.format(datePair.endDate) + "\n";
            }
            wrappedItem.setProperty(PRACTICE_BEGIN_DATE_C, beginDate);
            wrappedItem.setProperty(PRACTICE_END_DATE_C, endDate);

            wrappedItem.setProperty(AMOUNT_OF_STUDENTS_C, paragraphExtracts.size());

            wrappedItem.setProperty(EDIT_DELETE_PARAGRAPH_DISABLED, !changeParagraph.getOrder().getState().getCode().equals(OrderStatesCodes.FORMING));
        }
        return output;
    }

    private DSOutput getOutputForBaseOrders(DSInput input, PrPracticeOrder order) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PrPracticeParagraph.class, PARAGRAPH).column(PARAGRAPH);
        builder.where(eq(
                property(PrPracticeParagraph.order().fromAlias(PARAGRAPH)),
                value(order)));
        builder.order(property(PrPracticeParagraph.number().fromAlias(PARAGRAPH)));
        DSOutput output = DQLSelectOutputBuilder.get(input, builder, getSession()).pageable(false).build();

        DQLSelectBuilder extBuilder = new DQLSelectBuilder().fromEntity(PrPracticeExtract.class, EXTRACT).column(EXTRACT);
        extBuilder.where(eq(
                property(PrPracticeExtract.paragraph().order().fromAlias(EXTRACT)),
                value(order)));
        List<PrPracticeExtract> orderExtracts = extBuilder.createStatement(getSession()).list();

        List<DataWrapper> wrappedList = DataWrapper.wrap(output);
        for (DataWrapper wrappedItem : wrappedList) {
            PrPracticeParagraph paragraph = wrappedItem.getWrapped();
            List<PrPracticeExtract> paragraphExtracts = new ArrayList<>();
            for (PrPracticeExtract extract : orderExtracts) {
                if (extract.getParagraph().getId().equals(paragraph.getId())) {
                    paragraphExtracts.add(extract);
                }
            }

            wrappedItem.setProperty(TITLE_FULL_C, "Параграф №" + paragraph.getNumber());

            Set<OrgUnit> formativeUnits = CommonBaseEntityUtil.getPropertiesSet(paragraphExtracts, PrPracticeExtract.entity()
                    .practice().student().educationOrgUnit().formativeOrgUnit());
            wrappedItem.setProperty(FORMATIVE_ORG_UNIT_LIST, formativeUnits);

            Set<OrgUnit> responsibleOrgUnits = CommonBaseEntityUtil.getPropertiesSet(paragraphExtracts, PrPracticeExtract.entity()
                    .practice().registryElementPart().registryElement().owner());
            wrappedItem.setProperty(RESPONSIBLE_ORG_UNIT_LIST, responsibleOrgUnits);

            Set<ParagraphDatesUtils.DatePair> datePairs = ParagraphDatesUtils.getDatePairs(paragraphExtracts);
            String beginDate = "";
            String endDate = "";
            DateFormatter dateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;
            for (ParagraphDatesUtils.DatePair datePair : datePairs) {
                beginDate += dateFormatter.format(datePair.startDate) + "\n";
                endDate += dateFormatter.format(datePair.endDate) + "\n";
            }
            wrappedItem.setProperty(PRACTICE_BEGIN_DATE_C, beginDate);
            wrappedItem.setProperty(PRACTICE_END_DATE_C, endDate);

            wrappedItem.setProperty(AMOUNT_OF_STUDENTS_C, paragraphExtracts.size());
            wrappedItem.setProperty(EDIT_DELETE_PARAGRAPH_DISABLED, !paragraph.getOrder().getState().getCode().equals(OrderStatesCodes.FORMING));
        }
        return output;
    }
}
