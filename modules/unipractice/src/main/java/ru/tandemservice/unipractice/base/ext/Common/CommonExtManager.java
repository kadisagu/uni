/* $Id:$ */
package ru.tandemservice.unipractice.base.ext.Common;

import org.apache.commons.collections15.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.ModuleStatusReportBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unipractice.entity.*;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 05.03.2015
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
    {
        @Autowired
        private CommonManager _commonManager;

        @Bean
        public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
        {
            return itemListExtension(_commonManager.moduleStatusExtPoint())
                    .add("unipractice",
                         () -> {

                             IUniBaseDao dao = IUniBaseDao.instance.get();
                             List<String> result = new ArrayList<>();
                             String alias = "a";


                             final List<Object[]> preallocReportsList = dao.getList(new DQLSelectBuilder().fromDataSource(new DQLSelectBuilder().fromEntity(StPracticePreallocation.class, alias)
                                     .column(DQLFunctions.year(property(alias, StPracticePreallocation.formingDate())), "year")
                                     .column(DQLFunctions.count(property(alias, StPracticePreallocation.id())), "count")
                                     .group(DQLFunctions.year(property(alias, StPracticePreallocation.formingDate())))
                                     .buildQuery(), "f")
                                     .column(property("f.year"))
                                     .column(property("f.count"))
                                     .order(property("f.year")));

                             final List<Object[]> resultsReportsList = dao.getList(new DQLSelectBuilder().fromDataSource(new DQLSelectBuilder().fromEntity(ProductionTrainingResultsReport.class, alias)
                                     .column(DQLFunctions.year(property(alias, ProductionTrainingResultsReport.formingDate())), "year")
                                     .column(DQLFunctions.count(property(alias, ProductionTrainingResultsReport.id())), "count")
                                     .group(DQLFunctions.year(property(alias, ProductionTrainingResultsReport.formingDate())))
                                     .buildQuery(), "f")
                                     .column(property("f.year"))
                                     .column(property("f.count"))
                                     .order(property("f.year")));


                             // todo Число направлений на практику:

                             Date cDate = new Date();

                             result.add("Число баз практики (внешних): " + dao.getCount(PrPracticeBaseExt.class));
                             result.add("Число баз практики (внутренних): " + dao.getCount(PrPracticeBaseInt.class));
                             result.add("Число договоров с базами практики: " + dao.getCount(PrPracticeBaseContract.class));
                             result.add("Число приказов (согласованных):" + dao.getCount(new DQLSelectBuilder().fromEntity(PrPracticeOrder.class, alias)
                                             .where(or(eq(property(alias, PrPracticeOrder.type().code()), value(PrPracticeOrderTypeCodes.PREDDIPLOM)),
                                                     (eq(property(alias, PrPracticeOrder.type().code()), value(PrPracticeOrderTypeCodes.PROIZVODSTV))
                                                     )))
                                             .where(eq(property(alias, PrPracticeOrder.state().code()), value(OrderStatesCodes.ACCEPTED)))
                                             .where(eq(DQLFunctions.year(property(alias, PrPracticeOrder.createDate())), value(CoreDateUtils.getYear(cDate))))
                             ));
                             result.add("Число приказов (проведенных): " + dao.getCount(new DQLSelectBuilder().fromEntity(PrPracticeOrder.class, alias)
                                             .where(or(eq(property(alias, PrPracticeOrder.type().code()), value(PrPracticeOrderTypeCodes.PREDDIPLOM)),
                                                     (eq(property(alias, PrPracticeOrder.type().code()), value(PrPracticeOrderTypeCodes.PROIZVODSTV))
                                                     )))
                                             .where(eq(property(alias, PrPracticeOrder.state().code()), value(OrderStatesCodes.FINISHED)))
                                             .where(eq(DQLFunctions.year(property(alias, PrPracticeOrder.createDate())), value(CoreDateUtils.getYear(cDate))))
                             ));
                             result.add("Число приказов об изменении (согласованных): " + dao.getCount(new DQLSelectBuilder().fromEntity(PrPracticeOrder.class, alias)
                                             .where(or(eq(property(alias, PrPracticeOrder.type().code()), value(PrPracticeOrderTypeCodes.PREDDIPLOM_CHANGE)),
                                                     (eq(property(alias, PrPracticeOrder.type().code()), value(PrPracticeOrderTypeCodes.PROIZVODSTV_CHANGE))
                                                     )))
                                             .where(eq(property(alias, PrPracticeOrder.state().code()), value(OrderStatesCodes.ACCEPTED)))
                                             .where(eq(DQLFunctions.year(property(alias, PrPracticeOrder.createDate())), value(CoreDateUtils.getYear(cDate))))
                             ));
                             result.add("Число приказов об изменении (проведенных): " + dao.getCount(new DQLSelectBuilder().fromEntity(PrPracticeOrder.class, alias)
                                             .where(or(eq(property(alias, PrPracticeOrder.type().code()), value(PrPracticeOrderTypeCodes.PREDDIPLOM_CHANGE)),
                                                     (eq(property(alias, PrPracticeOrder.type().code()), value(PrPracticeOrderTypeCodes.PROIZVODSTV_CHANGE))
                                                     )))
                                             .where(eq(property(alias, PrPracticeOrder.state().code()), value(OrderStatesCodes.FINISHED)))
                                             .where(eq(DQLFunctions.year(property(alias, PrPracticeOrder.createDate())), value(CoreDateUtils.getYear(cDate))))
                             ));
                             result.add("Число отчетов (\"Предварительное распределение студентов\"):");
                             result.addAll(CollectionUtils.collect(preallocReportsList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER));
                             result.add("Число отчетов (\"Итоги производственного обучения\"):");
                             result.addAll(CollectionUtils.collect(resultsReportsList, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER));



                             return result;
                         }
                    ).create();
        }

}
