/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.RegApplications;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.PrPracticeBaseManager;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseRequestDSHandler;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;

/**
 * @author nvankov
 * @since 10/22/14
 */
@Configuration
public class PrPracticeBaseRegApplications extends BusinessComponentManager
{
    public static final String PR_PRACTICE_BASE_KIND_DS = "prPracticeBaseKindDS";
    public static final String PR_PRACTICE_REQUESTS_DS = "prPracticeRequestsDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PR_PRACTICE_REQUESTS_DS, prPracticeBaseRequestCL(), prPracticeBaseRequestDSHandler()))
                .addDataSource(selectDS(PR_PRACTICE_BASE_KIND_DS, PrPracticeBaseManager.instance().prPracticeBaseKindDSHandler()).addColumn(PrPracticeBaseKind.P_TITLE))
                .create();
    }

    @Bean
    public ColumnListExtPoint prPracticeBaseRequestCL() {
        return columnListExtPointBuilder(PR_PRACTICE_REQUESTS_DS)
                .addColumn(checkboxColumn("select").disabled("ui:selectRequestDisabled"))
                .addColumn(textColumn("kind", "request.kind.title"))
                .addColumn(textColumn("orgUnit", "orgUnitTitle"))
                .addColumn(textColumn("guaranteeLetter", "guaranteeLetter"))
                .addColumn(actionColumn("guaranteeLetterDownload", new Icon("template_save", "Скачать гарантийное письмо"),"onClickDownloadGuaranteeLetter").disabled("ui:downloadGuaranteeLetterDisabled"))

                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                        .alert(new FormattedMessage(PR_PRACTICE_REQUESTS_DS + ".delete.alert")))
                .create();
    }



    @Bean
    public IDefaultSearchDataSourceHandler prPracticeBaseRequestDSHandler() {
        return new PrPracticeBaseRequestDSHandler(getName());
    }
}



    