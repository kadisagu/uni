/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.tapsupport.component.list.SearchList;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.logic.ProductionTrainingResultsReportListDSHandler;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 22.10.2014
 */
@Configuration
public class ProductionTrainingResultsReportList extends BusinessComponentManager {

    public static final String PRODUCT_TRAINING_RESULTS_DS = "productionTrainingResultsDS";
    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String EDUCATION_YEAR = "educationYear";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRODUCT_TRAINING_RESULTS_DS, productTrainingResultsReportDSColumsList(), productionTrainingResRepDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint productTrainingResultsReportDSColumsList()
    {
        return columnListExtPointBuilder(PRODUCT_TRAINING_RESULTS_DS)
                .addColumn(indicatorColumn("report").defaultIndicatorItem(new IndicatorColumn.Item("report")))
                .addColumn(publisherColumn("formDate", ProductionTrainingResultsReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("eduYear", ProductionTrainingResultsReport.educationYear()))
                .addColumn(textColumn("detachedExtOrgUnit", ProductionTrainingResultsReport.detachedExtOrgUnit()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete")
                        .alert(FormattedMessage.with().template("productionTrainingResultsDS.delete.alert").parameter(ProductionTrainingResultsReport.formingDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).create()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> productionTrainingResRepDSHandler()
    {
        return new ProductionTrainingResultsReportListDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler educationYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationYear.class)
                .filter(EducationYear.title())
                .order(EducationYear.title());
    }
}
