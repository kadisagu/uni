/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author rsizonenko
 * @since 22.10.2014
 */
@Configuration
public class ProductionTrainingResultsReportAdd extends BusinessComponentManager {

    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String EXT_ORG_UNIT_DS = "extOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_YEAR_DS).handler(educationYearDSHandler()))
                .addDataSource(selectDS(EXT_ORG_UNIT_DS).handler(extOrgUnitDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationYear.class)
                .filter(EducationYear.title())
                .order(EducationYear.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> extOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), ExternalOrgUnit.class)
                .filter(ExternalOrgUnit.title())
                .order(ExternalOrgUnit.title());
    }
}
