package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.PrPracticeBaseManager;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;

import static ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseConstants.*;

@Configuration
public class PrPracticeBaseAddEdit extends BusinessComponentManager {

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(
                        selectDS(EXTERNAL_ORGANIZATION_DS, PrPracticeBaseManager.instance().externalOrganizationDSHandler())
                                .addColumn(ExternalOrgUnit.P_LEGAL_FORM_WITH_TITLE)
                )
                .addDataSource(
                        selectDS(INTERNAL_ORGANIZATION_DS, PrPracticeBaseManager.instance().internalOrganizationDSHandler())
                                .addColumn(OrgUnit.P_FULL_TITLE)
                )
                .addDataSource(
                        selectDS(PR_PRACTICE_BASE_KIND_DS, PrPracticeBaseManager.instance().prPracticeBaseKindDSHandler())
                                .addColumn(PrPracticeBaseKind.P_TITLE)
                )
                .create();
    }
}
