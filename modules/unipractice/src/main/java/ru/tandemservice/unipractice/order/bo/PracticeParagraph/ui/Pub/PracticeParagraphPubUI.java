package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.Pub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.ParagraphDatesUtils;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.AbstractPub.AbstractPracticeParagraphPubUI;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;
import ru.tandemservice.unipractice.order.entity.PrPracticeParagraph;

import java.util.List;

import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Input({
    @Bind(key = ORDER_ID, binding = ORDER_ID),
    @Bind(key = PARAGRAPH_ID, binding = PARAGRAPH_ID)
})
public class PracticeParagraphPubUI extends AbstractPracticeParagraphPubUI<PrPracticeParagraph, PrPracticeExtract> {

    @Override
    public void onComponentRefresh() {
        if (paragraphId != null ) {
            paragraph = DataAccessServices.dao().getNotNull(PrPracticeParagraph.class, paragraphId);
        }
        if (orderId != null){
            order = DataAccessServices.dao().getNotNull(PrPracticeOrder.class, orderId);
        }
        List<PrPracticeExtract> extracts = _uiConfig.getDataSource(PRACTICE_EXTRACT_LIST_DS).getRecords();
        String[] datesFormatted = ParagraphDatesUtils.getSortedFormattedDates(extracts);
        startDates = datesFormatted[0];
        endDates = datesFormatted[1];
        formativeOrgUnits = CommonBaseEntityUtil.getPropertiesSet(extracts, PrPracticeExtract.entity().practice().student().educationOrgUnit().formativeOrgUnit());
        responsibleOrgUnits = CommonBaseEntityUtil.getPropertiesSet(extracts, PrPracticeExtract.entity().practice().registryElementPart().registryElement().owner());

        setSecModel(new CommonPostfixPermissionModel("prPracticeOrder"));
    }

    @Override
    public String getPageTitle() {
        String orderType = "";
        switch (order.getType().getCode()){
            case PrPracticeOrderTypeCodes.PREDDIPLOM:
                orderType = _uiConfig.getProperty("ui.toPreDip");
                break;
            case PrPracticeOrderTypeCodes.PROIZVODSTV:
                orderType = _uiConfig.getProperty("ui.toProizv");
                break;
        }
        String number = "";
        if(order.getNumber() != null){
            number = " № " + order.getNumber();
        }
        String date = "";
        if (order.getCommitDate() != null) {
            String from = _uiConfig.getProperty("ui.from");
            date = " " + from + " " + simpleDateFormat.format(order.getCommitDate());
        }
        return String.format(_uiConfig.getProperty("ui.pageTitle"), orderType, number, date);
    }
}
