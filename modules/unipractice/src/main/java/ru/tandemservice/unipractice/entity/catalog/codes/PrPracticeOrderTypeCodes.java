package ru.tandemservice.unipractice.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип приказа на практику"
 * Имя сущности : prPracticeOrderType
 * Файл data.xml : unipractice.catalog.data.xml
 */
public interface PrPracticeOrderTypeCodes
{
    /** Константа кода (code) элемента : О направлении студентов на преддипломную практику  (title) */
    String PREDDIPLOM = "preddiplom";
    /** Константа кода (code) элемента : О направлении студентов на производственную практику  (title) */
    String PROIZVODSTV = "proizvodstv";
    /** Константа кода (code) элемента : Об изменении приказа «О направлении студентов на преддипломную практику» (title) */
    String PREDDIPLOM_CHANGE = "preddiplom_change";
    /** Константа кода (code) элемента : Об изменении приказа «О направлении студентов на производственную практику» (title) */
    String PROIZVODSTV_CHANGE = "proizvodstv_change";

    Set<String> CODES = ImmutableSet.of(PREDDIPLOM, PROIZVODSTV, PREDDIPLOM_CHANGE, PROIZVODSTV_CHANGE);
}
