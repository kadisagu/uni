/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso;
import ru.tandemservice.unipractice.entity.*;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;
import ru.tandemservice.unipractice.entity.catalog.PracticeScriptItem;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;
import ru.tandemservice.unipractice.entity.catalog.codes.PracticeScriptItemCodes;
import ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.Add.ProductionTrainingResultsReportAddUI;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 22.10.2014
 */
public class ProductionTrainingResultsReportDao extends UniBaseDao implements IProductionTrainingResultsReportDao {

    public static List<String> PROFILE_SPECIALITIES = Arrays.asList(
            "190301.65", "190302.65", "270201.65", "270204.65", "190303.65", "190401.65",
            "190402.65", "190701.65", "190300.65", "190300.65", "190300.65", "190300.65",
            "190300.65", "190401.65", "190401.65", "190401.65", "190401.65", "190401.65",
            "190401.65", "190901.65", "190901.65", "190901.65", "190901.65", "271501.65",
            "271501.65", "271501.65", "271501.65", "271501.65", "140400.62", "140400.62"
    );

    @Override
    public long createReport(ProductionTrainingResultsReportAddUI model) {

        ProductionTrainingResultsReport report = new ProductionTrainingResultsReport();
        report.setEducationYear(model.getEducationYear().getTitle());
        if (model.isEnableDetachedExtOrgUnit())
            report.setDetachedExtOrgUnit(model.getExternalOrgUnit().getTitle());

        report.setFormingDate(new Date());

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model));
        content.setFilename("TrainingResultReport.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);

        save(content);

        report.setContent(content);
        save(report);
        return report.getId();
    }


    private byte[] buildReport(final ProductionTrainingResultsReportAddUI model)
    {
        final ExternalOrgUnit externalOrgUnit = model.getExternalOrgUnit();

        boolean withDetachedOrgUnit = model.isEnableDetachedExtOrgUnit();

        IScriptItem templateDocument = UniDaoFacade.getCoreDao()
                .getCatalogItem(PracticeScriptItem.class,
                        withDetachedOrgUnit ?
                        PracticeScriptItemCodes.PRODUCTION_TRAINING_RESULTS_REPORT
                        : PracticeScriptItemCodes.PRODUCTION_TRAINING_RESULTS_REPORT_WITHOUT_DETACHED_ORG_UNIT);

        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PrPracticeResult.class, "pr")
                .joinPath(DQLJoinType.inner, PrPracticeResult.practiceAssignment().fromAlias("pr"), "pa")
                .where(eq(property(PrPracticeAssignment.state().code().fromAlias("pa")), value(PrPracticeAssignmentStateCodes.FINISHED)))
                .where(eq(property(PrPracticeAssignment.practice().year().educationYear().fromAlias("pa")), value(model.getEducationYear())))
                .column("pr");


        List<PrPracticeResult> fromQueryDataList = getList(builder);

        final Comparator<EduProgramSubject> subjectComparator = new Comparator<EduProgramSubject>() {
            @Override
            public int compare(EduProgramSubject o1, EduProgramSubject o2) {
                int res = Boolean.compare(PROFILE_SPECIALITIES.contains(o1.getCode()), PROFILE_SPECIALITIES.contains(o2.getCode()));
                if (res != 0) return -res;
                return o1.getTitleWithCode().compareToIgnoreCase(o2.getTitleWithCode());
            }
        };


        Map<Boolean, Map<EduProgramSubject, Map<PrPracticeBaseKind, Set<PrPracticeResult>>>> dataMap = new TreeMap<>(new Comparator<Boolean>() {
            @Override
            public int compare(Boolean o1, Boolean o2) {
                return o1.compareTo(o2);
            }
        });

        for (PrPracticeResult result : fromQueryDataList) {
            PrPracticeBase base = result.getPracticeAssignment().getPracticeBase();
            PrPracticeBaseKind kind = base.getKind();
            EduProgramSubject subject = result.getPracticeAssignment().getPractice().getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
            Boolean isProfile = PROFILE_SPECIALITIES.contains(subject.getCode());

            Map<EduProgramSubject, Map<PrPracticeBaseKind, Set<PrPracticeResult>>> subjectMap =  SafeMap.safeGet(dataMap, isProfile, TreeMap.class, subjectComparator);
            Map<PrPracticeBaseKind, Set<PrPracticeResult>> baseMap =  SafeMap.safeGet(subjectMap, subject, HashMap.class);
            Set<PrPracticeResult> results = SafeMap.safeGet(baseMap, kind, HashSet.class);

            results.add(result);
        }

        String eduYearTitle = model.getEducationYear().getTitle();

        String[][] tTable;

        if (withDetachedOrgUnit) {
            tTable = buildWithDetachedOrgUnit(dataMap, externalOrgUnit);
            new RtfInjectModifier().put("year", eduYearTitle).put("extOrgUnit", externalOrgUnit.getTitle()).modify(document);
        }
        else {
            tTable = buildWithOutDetachedOrgUnit(dataMap);
            new RtfInjectModifier().put("year", eduYearTitle).modify(document);
        }

        RtfTableModifier modifier = new RtfTableModifier();

        modifier.put("T", tTable);
        modifier.put("T", new IRtfRowIntercepter() {

            boolean currentRowVerticalMerge = false;
            boolean currentRowHorizontalNext = false;

            @Override
            public void beforeModify(RtfTable table, int currentRowIndex) {
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                if (colIndex == 0)
                {
                    currentRowVerticalMerge = !StringUtils.isNumeric(value) && !value.isEmpty();
                    currentRowHorizontalNext = value.isEmpty();
                }

                RtfString string = new RtfString();
                if (currentRowVerticalMerge)
                {
                    string.boldBegin();
                    switch (colIndex)
                    {
                        case (0) : cell.setMergeType(MergeType.HORIZONTAL_MERGED_FIRST); string.append(IRtfData.QC); break;
                        case (1) :
                        case (2) : cell.setMergeType(MergeType.HORIZONTAL_MERGED_NEXT); string.append(IRtfData.QC); break;
                    }
                }
                else
                {
                    if (colIndex < 3) {
                        if (currentRowHorizontalNext) {
                            cell.setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                        } else {
                            cell.setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                        }
                    }
                }

                if (value.equals("0"))
                    value = "";

                string.append(value).boldEnd();

                return string.toList();
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
            }
        });

        modifier.modify(document);


        return RtfUtil.toByteArray(document);

    }

    @SuppressWarnings("ConstantConditions")
    private String[][] buildWithDetachedOrgUnit(Map<Boolean, Map<EduProgramSubject, Map<PrPracticeBaseKind, Set<PrPracticeResult>>>> dataMap, ExternalOrgUnit externalOrgUnit) {
        final List<PrPracticeBaseKind> kinds = getList(PrPracticeBaseKind.class, PrPracticeBaseKind.P_TITLE);

        // счетчик
        int num = 1;

        // итоговая сводка по универу
        int uniTotalStudents = 0;
        int uniTargetAdm = 0;

        int uniRrwTotal = 0;
        int uniRrwTarget = 0;
        int uniRrwORM = 0;
        int uniRrwNoORM = 0;

        int uniOtherTotal = 0;
        int uniOtherTarget = 0;
        int uniOtherORM = 0;
        int uniOtherNoORM = 0;
        int uniORM = 0;

        int uniGosTotalStudents = 0;
        int uniGosTargetAdm = 0;

        int uniGosRrwTotal = 0;
        int uniGosRrwTarget = 0;
        int uniGosRrwORM = 0;
        int uniGosRrwNoORM = 0;

        int uniGosOtherTotal = 0;
        int uniGosOtherTarget = 0;
        int uniGosOtherORM = 0;
        int uniGosOtherNoORM = 0;
        int uniGosORM = 0;


        int uniFgosTotalStudents = 0;
        int uniFgosTargetAdm = 0;

        int uniFgosRrwTotal = 0;
        int uniFgosRrwTarget = 0;
        int uniFgosRrwORM = 0;
        int uniFgosRrwNoORM = 0;

        int uniFgosOtherTotal = 0;
        int uniFgosOtherTarget = 0;
        int uniFgosOtherORM = 0;
        int uniFgosOtherNoORM = 0;
        int uniFgosORM = 0;

        List<String[]> tTable = new ArrayList<>();
        for (Map.Entry<Boolean, Map<EduProgramSubject, Map<PrPracticeBaseKind, Set<PrPracticeResult>>>> profileEntry : dataMap.entrySet()) {

            int groupTotalStudents = 0;
            int groupTargetAdm = 0;

            int groupRrwTotal = 0;
            int groupRrwTarget = 0;
            int groupRrwORM = 0;
            int groupRrwNoORM = 0;

            int groupOtherTotal = 0;
            int groupOtherTarget = 0;
            int groupOtherORM = 0;
            int groupOtherNoORM = 0;
            int groupORM = 0;

            int groupGosTotalStudents = 0;
            int groupGosTargetAdm = 0;

            int groupGosRrwTotal = 0;
            int groupGosRrwTarget = 0;
            int groupGosRrwORM = 0;
            int groupGosRrwNoORM = 0;

            int groupGosOtherTotal = 0;
            int groupGosOtherTarget = 0;
            int groupGosOtherORM = 0;
            int groupGosOtherNoORM = 0;
            int groupGosORM = 0;

            int groupFgosTotalStudents = 0;
            int groupFgosTargetAdm = 0;

            int groupFgosRrwTotal = 0;
            int groupFgosRrwTarget = 0;
            int groupFgosRrwORM = 0;
            int groupFgosRrwNoORM = 0;

            int groupFgosOtherTotal = 0;
            int groupFgosOtherTarget = 0;
            int groupFgosOtherORM = 0;
            int groupFgosOtherNoORM = 0;
            int groupFgosORM = 0;



            for (Map.Entry<EduProgramSubject, Map<PrPracticeBaseKind, Set<PrPracticeResult>>> subjectEntry : profileEntry.getValue().entrySet()) {

                EduProgramSubject subject = subjectEntry.getKey();

                Set<Student> totalStudents = new HashSet<>();
                Set<Student> targetAdm = new HashSet<>();

                int subjRrwTotal = 0;
                int subjRrwTarget = 0;
                int subjRrwORM = 0;
                int subjRrwNoORM = 0;

                int subjOtherTotal = 0;
                int subjOtherTarget = 0;
                int subjOtherORM = 0;
                int subjOtherNoORM = 0;

                int subjORM = 0;
                // rrw == r zh d

                boolean firstRow = true;



                for (PrPracticeBaseKind baseKind : kinds)  {

                    Set<PrPracticeResult> results = SafeMap.safeGet(subjectEntry.getValue(), baseKind, HashSet.class);



                    Set<Student> rrwTotal = new HashSet<>();
                    Set<Student> rrwTarget = new HashSet<>();
                    Set<Student> rrwORM = new HashSet<>();
                    Set<Student> rrwNoORM = new HashSet<>();


                    Set<Student> otherStudents = new HashSet<>();
                    Set<Student> otherTarget = new HashSet<>();
                    Set<Student> otherORM = new HashSet<>();
                    Set<Student> otherNoORM = new HashSet<>();

                    Set<Student> totalORM = new HashSet<>();

                    for (PrPracticeResult result : results) {

                        final Student student = result.getPracticeAssignment().getPractice().getStudent();
                        totalStudents.add(student);



                        final PrPracticeBase practiceBase = result.getPracticeAssignment().getPracticeBase();

                        if (practiceBase instanceof PrPracticeBaseExt && ((PrPracticeBaseExt) practiceBase).getOrgUnit().getId().equals(externalOrgUnit.getId()))
                        {
                            rrwTotal.add(student);
                            if (student.isTargetAdmission()) {
                                targetAdm.add(student);
                                rrwTarget.add(student);
                            }
                            if (result.isPracticePayed()) {
                                rrwORM.add(student);
                                totalORM.add(student);
                            }
                            else rrwNoORM.add(student);


                        } else {
                            otherStudents.add(student);
                            if (student.isTargetAdmission()) {
                                targetAdm.add(student);
                                otherTarget.add(student);
                            }
                            if (result.isPracticePayed()) {
                                otherORM.add(student);
                                totalORM.add(student);
                            }
                            else otherNoORM.add(student);
                        }
                    }


                    subjRrwTotal += rrwTotal.size();
                    subjRrwTarget += rrwTarget.size();
                    subjRrwORM += rrwORM.size();
                    subjRrwNoORM += rrwNoORM.size();
                    subjOtherTotal += otherStudents.size();
                    subjOtherTarget += otherTarget.size();
                    subjOtherORM += otherORM.size();
                    subjOtherNoORM += otherNoORM.size();

                    subjORM += totalORM.size();


                    String[] row = new String[]{
                            firstRow ? String.valueOf(num++) : "",
                            firstRow ? subject.getCode() : "",
                            firstRow ? subject.getTitle() : "",
                            String.valueOf(totalStudents.size()),
                            String.valueOf(targetAdm.size()),
                            baseKind.getTitle(),
                            String.valueOf(rrwTotal.size()),
                            String.valueOf(rrwTarget.size()),
                            String.valueOf(rrwORM.size()),
                            String.valueOf(rrwNoORM.size()),
                            String.valueOf(otherStudents.size()),
                            String.valueOf(otherTarget.size()),
                            String.valueOf(otherORM.size()),
                            String.valueOf(otherNoORM.size()),
                            String.valueOf(totalORM.size()),
                            ""


                    };
                    firstRow = false;

                    tTable.add(row);
                }

                groupTotalStudents += totalStudents.size();
                groupTargetAdm += targetAdm.size();
                groupRrwTotal += subjRrwTotal;
                groupRrwTarget += subjRrwTarget;
                groupRrwORM += subjRrwORM;
                groupRrwNoORM += subjRrwNoORM;
                groupOtherTotal += subjOtherTotal;
                groupOtherTarget += subjOtherTarget;
                groupOtherORM += subjOtherORM;
                groupOtherNoORM += subjOtherNoORM;
                groupORM += subjORM;

                if (subject instanceof EduProgramSubjectOkso)
                {
                    groupGosTotalStudents += totalStudents.size();
                    groupGosTargetAdm += targetAdm.size();
                    groupGosRrwTotal += subjRrwTotal;
                    groupGosRrwTarget += subjRrwTarget;
                    groupGosRrwORM += subjRrwORM;
                    groupGosRrwNoORM += subjRrwNoORM;
                    groupGosOtherTotal += subjOtherTotal;
                    groupGosOtherTarget += subjOtherTarget;
                    groupGosOtherORM += subjOtherORM;
                    groupGosOtherNoORM += subjOtherNoORM;
                    groupGosORM += subjORM;
                } else {
                    groupFgosTotalStudents += totalStudents.size();
                    groupFgosTargetAdm += targetAdm.size();
                    groupFgosRrwTotal += subjRrwTotal;
                    groupFgosRrwTarget += subjRrwTarget;
                    groupFgosRrwORM += subjRrwORM;
                    groupFgosRrwNoORM += subjRrwNoORM;
                    groupFgosOtherTotal += subjOtherTotal;
                    groupFgosOtherTarget += subjOtherTarget;
                    groupFgosOtherORM += subjOtherORM;
                    groupFgosOtherNoORM += subjOtherNoORM;
                    groupFgosORM += subjORM;
                }

                String[] row = new String[]{
                        "ВСЕГО по специальности (направлению)",
                        "",
                        "",
                        String.valueOf(totalStudents.size()),
                        String.valueOf(targetAdm.size()),
                        "",
                        String.valueOf(subjRrwTotal),
                        String.valueOf(subjRrwTarget),
                        String.valueOf(subjRrwORM),
                        String.valueOf(subjRrwNoORM),
                        String.valueOf(subjOtherTotal),
                        String.valueOf(subjOtherTarget),
                        String.valueOf(subjOtherORM),
                        String.valueOf(subjOtherNoORM),
                        String.valueOf(subjORM),
                        ""


                };
                tTable.add(row);

            }

            String[] row = new String[]{
                    profileEntry.getKey() ? "ИТОГО по ж.д. специальностям, в т.ч. " : "ИТОГО по специальностям (направлениям) др.УМО: В т.ч.: ",
                    "",
                    "",
                    String.valueOf(groupTotalStudents),
                    String.valueOf(groupTargetAdm),
                    "",
                    String.valueOf(groupRrwTotal),
                    String.valueOf(groupRrwTarget),
                    String.valueOf(groupRrwORM),
                    String.valueOf(groupRrwNoORM),
                    String.valueOf(groupOtherTotal),
                    String.valueOf(groupOtherTarget),
                    String.valueOf(groupOtherORM),
                    String.valueOf(groupOtherNoORM),
                    String.valueOf(groupORM),
                    ""
            };
            tTable.add(row);

            String[] gosRow = new String[]{
                    profileEntry.getKey() ? "по специальностям  ГОС" : "- по спец-тям (напр.) ГОС",
                    "",
                    "",
                    String.valueOf(groupGosTotalStudents),
                    String.valueOf(groupGosTargetAdm),
                    "",
                    String.valueOf(groupGosRrwTotal),
                    String.valueOf(groupGosRrwTarget),
                    String.valueOf(groupGosRrwORM),
                    String.valueOf(groupGosRrwNoORM),
                    String.valueOf(groupGosOtherTotal),
                    String.valueOf(groupGosOtherTarget),
                    String.valueOf(groupGosOtherORM),
                    String.valueOf(groupGosOtherNoORM),
                    String.valueOf(groupGosORM),
                    ""
            };
            tTable.add(gosRow);

            String[] fgosRow = new String[]{
                    profileEntry.getKey() ? "по специальностям  ФГОС" : "- по спец-тям (напр.) ФГОС ",
                    "",
                    "",
                    String.valueOf(groupFgosTotalStudents),
                    String.valueOf(groupFgosTargetAdm),
                    "",
                    String.valueOf(groupFgosRrwTotal),
                    String.valueOf(groupFgosRrwTarget),
                    String.valueOf(groupFgosRrwORM),
                    String.valueOf(groupFgosRrwNoORM),
                    String.valueOf(groupFgosOtherTotal),
                    String.valueOf(groupFgosOtherTarget),
                    String.valueOf(groupFgosOtherORM),
                    String.valueOf(groupFgosOtherNoORM),
                    String.valueOf(groupFgosORM),
                    ""
            };
            tTable.add(fgosRow);



            uniTotalStudents += groupTotalStudents;
            uniTargetAdm += groupTargetAdm;
            uniRrwTotal += groupRrwTotal;
            uniRrwTarget += groupRrwTarget;
            uniRrwORM += groupRrwORM;
            uniRrwNoORM += groupRrwNoORM;
            uniOtherTotal += groupOtherTotal;
            uniOtherTarget += groupOtherTarget;
            uniOtherORM += groupOtherORM;
            uniOtherNoORM += groupOtherNoORM;
            uniORM += groupORM;
            uniGosTotalStudents += groupGosTotalStudents;
            uniGosTargetAdm += groupGosTargetAdm;
            uniGosRrwTotal += groupGosRrwTotal;
            uniGosRrwTarget += groupGosRrwTarget;
            uniGosRrwORM += groupGosRrwORM;
            uniGosRrwNoORM += groupGosRrwNoORM;
            uniGosOtherTotal += groupGosOtherTotal;
            uniGosOtherTarget += groupGosOtherTarget;
            uniGosOtherORM += groupGosOtherORM;
            uniGosOtherNoORM += groupGosOtherNoORM;
            uniGosORM += groupGosORM;
            uniFgosTotalStudents += groupFgosTotalStudents;
            uniFgosTargetAdm += groupFgosTargetAdm;
            uniFgosRrwTotal += groupFgosRrwTotal;
            uniFgosRrwTarget += groupFgosRrwTarget;
            uniFgosRrwORM += groupFgosRrwORM;
            uniFgosRrwNoORM += groupFgosRrwNoORM;
            uniFgosOtherTotal += groupFgosOtherTotal;
            uniFgosOtherTarget += groupFgosOtherTarget;
            uniFgosOtherORM += groupFgosOtherORM;
            uniFgosOtherNoORM += groupFgosOtherNoORM;
            uniFgosORM += groupFgosORM;


        }

        String[] row = new String[]{
                "ВСЕГО  по вузу: В т.ч. :",
                "",
                "",
                String.valueOf(uniTotalStudents),
                String.valueOf(uniTargetAdm),
                "",
                String.valueOf(uniRrwTotal),
                String.valueOf(uniRrwTarget),
                String.valueOf(uniRrwORM),
                String.valueOf(uniRrwNoORM),
                String.valueOf(uniOtherTotal),
                String.valueOf(uniOtherTarget),
                String.valueOf(uniOtherORM),
                String.valueOf(uniOtherNoORM),
                String.valueOf(uniORM),
                ""
        };
        tTable.add(row);

        String[] gosRow = new String[]{
                "- по спец-тям (напр.) ГОС",
                "",
                "",
                String.valueOf(uniGosTotalStudents),
                String.valueOf(uniGosTargetAdm),
                "",
                String.valueOf(uniGosRrwTotal),
                String.valueOf(uniGosRrwTarget),
                String.valueOf(uniGosRrwORM),
                String.valueOf(uniGosRrwNoORM),
                String.valueOf(uniGosOtherTotal),
                String.valueOf(uniGosOtherTarget),
                String.valueOf(uniGosOtherORM),
                String.valueOf(uniGosOtherNoORM),
                String.valueOf(uniGosORM),
                ""
        };
        tTable.add(gosRow);

        String[] fgosRow = new String[]{
                "- по спец-тям (напр.) ФГОС ",
                "",
                "",
                String.valueOf(uniFgosTotalStudents),
                String.valueOf(uniFgosTargetAdm),
                "",
                String.valueOf(uniFgosRrwTotal),
                String.valueOf(uniFgosRrwTarget),
                String.valueOf(uniFgosRrwORM),
                String.valueOf(uniFgosRrwNoORM),
                String.valueOf(uniFgosOtherTotal),
                String.valueOf(uniFgosOtherTarget),
                String.valueOf(uniFgosOtherORM),
                String.valueOf(uniFgosOtherNoORM),
                String.valueOf(uniFgosORM),
                ""
        };
        tTable.add(fgosRow);

        return tTable.toArray(new String[tTable.size()][]);



    }


    private String[][] buildWithOutDetachedOrgUnit(Map<Boolean, Map<EduProgramSubject, Map<PrPracticeBaseKind, Set<PrPracticeResult>>>> dataMap) {
        final List<PrPracticeBaseKind> kinds = getList(PrPracticeBaseKind.class, PrPracticeBaseKind.P_TITLE);

        // счетчик
        int num = 1;

        // итоговая сводка по универу
        int uniTotalStudents = 0;
        int uniTargetAdm = 0;


        int uniOtherTotal = 0;
        int uniOtherTarget = 0;
        int uniOtherORM = 0;
        int uniOtherNoORM = 0;
        int uniORM = 0;

        int uniGosTotalStudents = 0;
        int uniGosTargetAdm = 0;



        int uniGosOtherTotal = 0;
        int uniGosOtherTarget = 0;
        int uniGosOtherORM = 0;
        int uniGosOtherNoORM = 0;
        int uniGosORM = 0;


        int uniFgosTotalStudents = 0;
        int uniFgosTargetAdm = 0;


        int uniFgosOtherTotal = 0;
        int uniFgosOtherTarget = 0;
        int uniFgosOtherORM = 0;
        int uniFgosOtherNoORM = 0;
        int uniFgosORM = 0;

        List<String[]> tTable = new ArrayList<>();
        for (Map.Entry<Boolean, Map<EduProgramSubject, Map<PrPracticeBaseKind, Set<PrPracticeResult>>>> profileEntry : dataMap.entrySet()) {

            int groupTotalStudents = 0;
            int groupTargetAdm = 0;

            int groupOtherTotal = 0;
            int groupOtherTarget = 0;
            int groupOtherORM = 0;
            int groupOtherNoORM = 0;
            int groupORM = 0;

            int groupGosTotalStudents = 0;
            int groupGosTargetAdm = 0;



            int groupGosOtherTotal = 0;
            int groupGosOtherTarget = 0;
            int groupGosOtherORM = 0;
            int groupGosOtherNoORM = 0;
            int groupGosORM = 0;

            int groupFgosTotalStudents = 0;
            int groupFgosTargetAdm = 0;



            int groupFgosOtherTotal = 0;
            int groupFgosOtherTarget = 0;
            int groupFgosOtherORM = 0;
            int groupFgosOtherNoORM = 0;
            int groupFgosORM = 0;



            for (Map.Entry<EduProgramSubject, Map<PrPracticeBaseKind, Set<PrPracticeResult>>> subjectEntry : profileEntry.getValue().entrySet()) {

                EduProgramSubject subject = subjectEntry.getKey();

                Set<Student> totalStudents = new HashSet<>();
                Set<Student> targetAdm = new HashSet<>();



                int subjOtherTotal = 0;
                int subjOtherTarget = 0;
                int subjOtherORM = 0;
                int subjOtherNoORM = 0;

                int subjORM = 0;
                // rrw == r zh d

                boolean firstRow = true;



                for (PrPracticeBaseKind baseKind : kinds)  {

                    Set<PrPracticeResult> results = SafeMap.safeGet(subjectEntry.getValue(), baseKind, HashSet.class);





                    Set<Student> otherStudents = new HashSet<>();
                    Set<Student> otherTarget = new HashSet<>();
                    Set<Student> otherORM = new HashSet<>();
                    Set<Student> otherNoORM = new HashSet<>();

                    Set<Student> totalORM = new HashSet<>();

                    for (PrPracticeResult result : results) {

                        final Student student = result.getPracticeAssignment().getPractice().getStudent();
                        totalStudents.add(student);



                            otherStudents.add(student);
                            if (student.isTargetAdmission()) {
                                targetAdm.add(student);
                                otherTarget.add(student);
                            }
                            if (result.isPracticePayed()) {
                                otherORM.add(student);
                                totalORM.add(student);
                            }
                            else otherNoORM.add(student);

                    }



                    subjOtherTotal += otherStudents.size();
                    subjOtherTarget += otherTarget.size();
                    subjOtherORM += otherORM.size();
                    subjOtherNoORM += otherNoORM.size();

                    subjORM += totalORM.size();


                    String[] row = new String[]{
                            firstRow ? String.valueOf(num++) : "",
                            firstRow ? subject.getCode() : "",
                            firstRow ? subject.getTitle() : "",
                            String.valueOf(totalStudents.size()),
                            String.valueOf(targetAdm.size()),
                            baseKind.getTitle(),
                            String.valueOf(otherStudents.size()),
                            String.valueOf(otherTarget.size()),
                            String.valueOf(otherORM.size()),
                            String.valueOf(otherNoORM.size()),
                            String.valueOf(totalORM.size()),
                            ""


                    };
                    firstRow = false;

                    tTable.add(row);
                }

                groupTotalStudents += totalStudents.size();
                groupTargetAdm += targetAdm.size();
                groupOtherTotal += subjOtherTotal;
                groupOtherTarget += subjOtherTarget;
                groupOtherORM += subjOtherORM;
                groupOtherNoORM += subjOtherNoORM;
                groupORM += subjORM;

                if (subject instanceof EduProgramSubjectOkso)
                {
                    groupGosTotalStudents += totalStudents.size();
                    groupGosTargetAdm += targetAdm.size();
                    groupGosOtherTotal += subjOtherTotal;
                    groupGosOtherTarget += subjOtherTarget;
                    groupGosOtherORM += subjOtherORM;
                    groupGosOtherNoORM += subjOtherNoORM;
                    groupGosORM += subjORM;
                } else {
                    groupFgosTotalStudents += totalStudents.size();
                    groupFgosTargetAdm += targetAdm.size();
                    groupFgosOtherTotal += subjOtherTotal;
                    groupFgosOtherTarget += subjOtherTarget;
                    groupFgosOtherORM += subjOtherORM;
                    groupFgosOtherNoORM += subjOtherNoORM;
                    groupFgosORM += subjORM;
                }

                String[] row = new String[]{
                        "ВСЕГО по специальности (направлению)",
                        "",
                        "",
                        String.valueOf(totalStudents.size()),
                        String.valueOf(targetAdm.size()),
                        "",
                        String.valueOf(subjOtherTotal),
                        String.valueOf(subjOtherTarget),
                        String.valueOf(subjOtherORM),
                        String.valueOf(subjOtherNoORM),
                        String.valueOf(subjORM),
                        ""


                };
                tTable.add(row);

            }

            String[] row = new String[]{
                    profileEntry.getKey() ? "ИТОГО по ж.д. специальностям, в т.ч. " : "ИТОГО по специальностям (направлениям) др.УМО: В т.ч.: ",
                    "",
                    "",
                    String.valueOf(groupTotalStudents),
                    String.valueOf(groupTargetAdm),
                    "",
                    String.valueOf(groupOtherTotal),
                    String.valueOf(groupOtherTarget),
                    String.valueOf(groupOtherORM),
                    String.valueOf(groupOtherNoORM),
                    String.valueOf(groupORM),
                    ""
            };
            tTable.add(row);

            String[] gosRow = new String[]{
                    profileEntry.getKey() ? "по специальностям  ГОС" : "- по спец-тям (напр.) ГОС",
                    "",
                    "",
                    String.valueOf(groupGosTotalStudents),
                    String.valueOf(groupGosTargetAdm),
                    "",
                    String.valueOf(groupGosOtherTotal),
                    String.valueOf(groupGosOtherTarget),
                    String.valueOf(groupGosOtherORM),
                    String.valueOf(groupGosOtherNoORM),
                    String.valueOf(groupGosORM),
                    ""
            };
            tTable.add(gosRow);

            String[] fgosRow = new String[]{
                    profileEntry.getKey() ? "по специальностям  ФГОС" : "- по спец-тям (напр.) ФГОС ",
                    "",
                    "",
                    String.valueOf(groupFgosTotalStudents),
                    String.valueOf(groupFgosTargetAdm),
                    "",
                    String.valueOf(groupFgosOtherTotal),
                    String.valueOf(groupFgosOtherTarget),
                    String.valueOf(groupFgosOtherORM),
                    String.valueOf(groupFgosOtherNoORM),
                    String.valueOf(groupFgosORM),
                    ""
            };
            tTable.add(fgosRow);



            uniTotalStudents += groupTotalStudents;
            uniTargetAdm += groupTargetAdm;
            uniOtherTotal += groupOtherTotal;
            uniOtherTarget += groupOtherTarget;
            uniOtherORM += groupOtherORM;
            uniOtherNoORM += groupOtherNoORM;
            uniORM += groupORM;
            uniGosTotalStudents += groupGosTotalStudents;
            uniGosTargetAdm += groupGosTargetAdm;
            uniGosOtherTotal += groupGosOtherTotal;
            uniGosOtherTarget += groupGosOtherTarget;
            uniGosOtherORM += groupGosOtherORM;
            uniGosOtherNoORM += groupGosOtherNoORM;
            uniGosORM += groupGosORM;
            uniFgosTotalStudents += groupFgosTotalStudents;
            uniFgosTargetAdm += groupFgosTargetAdm;
            uniFgosOtherTotal += groupFgosOtherTotal;
            uniFgosOtherTarget += groupFgosOtherTarget;
            uniFgosOtherORM += groupFgosOtherORM;
            uniFgosOtherNoORM += groupFgosOtherNoORM;
            uniFgosORM += groupFgosORM;


        }

        String[] row = new String[]{
                "ВСЕГО  по вузу: В т.ч. :",
                "",
                "",
                String.valueOf(uniTotalStudents),
                String.valueOf(uniTargetAdm),
                "",
                String.valueOf(uniOtherTotal),
                String.valueOf(uniOtherTarget),
                String.valueOf(uniOtherORM),
                String.valueOf(uniOtherNoORM),
                String.valueOf(uniORM),
                ""
        };
        tTable.add(row);

        String[] gosRow = new String[]{
                "- по спец-тям (напр.) ГОС",
                "",
                "",
                String.valueOf(uniGosTotalStudents),
                String.valueOf(uniGosTargetAdm),
                "",
                String.valueOf(uniGosOtherTotal),
                String.valueOf(uniGosOtherTarget),
                String.valueOf(uniGosOtherORM),
                String.valueOf(uniGosOtherNoORM),
                String.valueOf(uniGosORM),
                ""
        };
        tTable.add(gosRow);

        String[] fgosRow = new String[]{
                "- по спец-тям (напр.) ФГОС ",
                "",
                "",
                String.valueOf(uniFgosTotalStudents),
                String.valueOf(uniFgosTargetAdm),
                "",
                String.valueOf(uniFgosOtherTotal),
                String.valueOf(uniFgosOtherTarget),
                String.valueOf(uniFgosOtherORM),
                String.valueOf(uniFgosOtherNoORM),
                String.valueOf(uniFgosORM),
                ""
        };
        tTable.add(fgosRow);

        return tTable.toArray(new String[tTable.size()][]);


    }
}



