package ru.tandemservice.unipractice.order.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.order.entity.PrAbstractExtract;
import ru.tandemservice.unipractice.order.entity.PrAbstractParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абстрактная выписка на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrAbstractExtractGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.order.entity.PrAbstractExtract";
    public static final String ENTITY_NAME = "prAbstractExtract";
    public static final int VERSION_HASH = -1006218291;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARAGRAPH = "paragraph";
    public static final String P_NUMBER = "number";
    public static final String P_CREATE_DATE = "createDate";
    public static final String L_STATE = "state";
    public static final String L_PRACTICE_BASE = "practiceBase";

    private PrAbstractParagraph _paragraph;     // Абстрактный параграф на практику
    private Integer _number;     // Номер выписки в параграфе
    private Date _createDate;     // Дата формирования
    private ExtractStates _state;     // Состояние выписки
    private PrPracticeBase _practiceBase;     // База практики

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абстрактный параграф на практику.
     */
    public PrAbstractParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Абстрактный параграф на практику.
     */
    public void setParagraph(PrAbstractParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    /**
     * @return Номер выписки в параграфе.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер выписки в параграфе.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     */
    @NotNull
    public ExtractStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние выписки. Свойство не может быть null.
     */
    public void setState(ExtractStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * Фиксируется по направлению в момент проведения выписки, т.е. сохраняет утвержденную данным приказом базу практики.
     *
     * @return База практики. Свойство не может быть null.
     */
    @NotNull
    public PrPracticeBase getPracticeBase()
    {
        return _practiceBase;
    }

    /**
     * @param practiceBase База практики. Свойство не может быть null.
     */
    public void setPracticeBase(PrPracticeBase practiceBase)
    {
        dirty(_practiceBase, practiceBase);
        _practiceBase = practiceBase;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PrAbstractExtractGen)
        {
            setParagraph(((PrAbstractExtract)another).getParagraph());
            setNumber(((PrAbstractExtract)another).getNumber());
            setCreateDate(((PrAbstractExtract)another).getCreateDate());
            setState(((PrAbstractExtract)another).getState());
            setPracticeBase(((PrAbstractExtract)another).getPracticeBase());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrAbstractExtractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrAbstractExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("PrAbstractExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "paragraph":
                    return obj.getParagraph();
                case "number":
                    return obj.getNumber();
                case "createDate":
                    return obj.getCreateDate();
                case "state":
                    return obj.getState();
                case "practiceBase":
                    return obj.getPracticeBase();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "paragraph":
                    obj.setParagraph((PrAbstractParagraph) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "state":
                    obj.setState((ExtractStates) value);
                    return;
                case "practiceBase":
                    obj.setPracticeBase((PrPracticeBase) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "paragraph":
                        return true;
                case "number":
                        return true;
                case "createDate":
                        return true;
                case "state":
                        return true;
                case "practiceBase":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "paragraph":
                    return true;
                case "number":
                    return true;
                case "createDate":
                    return true;
                case "state":
                    return true;
                case "practiceBase":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "paragraph":
                    return PrAbstractParagraph.class;
                case "number":
                    return Integer.class;
                case "createDate":
                    return Date.class;
                case "state":
                    return ExtractStates.class;
                case "practiceBase":
                    return PrPracticeBase.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrAbstractExtract> _dslPath = new Path<PrAbstractExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrAbstractExtract");
    }
            

    /**
     * @return Абстрактный параграф на практику.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getParagraph()
     */
    public static PrAbstractParagraph.Path<PrAbstractParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getState()
     */
    public static ExtractStates.Path<ExtractStates> state()
    {
        return _dslPath.state();
    }

    /**
     * Фиксируется по направлению в момент проведения выписки, т.е. сохраняет утвержденную данным приказом базу практики.
     *
     * @return База практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getPracticeBase()
     */
    public static PrPracticeBase.Path<PrPracticeBase> practiceBase()
    {
        return _dslPath.practiceBase();
    }

    public static class Path<E extends PrAbstractExtract> extends EntityPath<E>
    {
        private PrAbstractParagraph.Path<PrAbstractParagraph> _paragraph;
        private PropertyPath<Integer> _number;
        private PropertyPath<Date> _createDate;
        private ExtractStates.Path<ExtractStates> _state;
        private PrPracticeBase.Path<PrPracticeBase> _practiceBase;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абстрактный параграф на практику.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getParagraph()
     */
        public PrAbstractParagraph.Path<PrAbstractParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new PrAbstractParagraph.Path<PrAbstractParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(PrAbstractExtractGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(PrAbstractExtractGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getState()
     */
        public ExtractStates.Path<ExtractStates> state()
        {
            if(_state == null )
                _state = new ExtractStates.Path<ExtractStates>(L_STATE, this);
            return _state;
        }

    /**
     * Фиксируется по направлению в момент проведения выписки, т.е. сохраняет утвержденную данным приказом базу практики.
     *
     * @return База практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.order.entity.PrAbstractExtract#getPracticeBase()
     */
        public PrPracticeBase.Path<PrPracticeBase> practiceBase()
        {
            if(_practiceBase == null )
                _practiceBase = new PrPracticeBase.Path<PrPracticeBase>(L_PRACTICE_BASE, this);
            return _practiceBase;
        }

        public Class getEntityClass()
        {
            return PrAbstractExtract.class;
        }

        public String getEntityName()
        {
            return "prAbstractExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
