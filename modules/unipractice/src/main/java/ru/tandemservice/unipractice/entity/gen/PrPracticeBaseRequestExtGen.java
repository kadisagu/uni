package ru.tandemservice.unipractice.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import ru.tandemservice.unipractice.entity.PrPracticeBaseRequest;
import ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Заявка на регистрацию внешней базы практики
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeBaseRequestExtGen extends PrPracticeBaseRequest
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt";
    public static final String ENTITY_NAME = "prPracticeBaseRequestExt";
    public static final int VERSION_HASH = -220475149;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_GUARANTEE_LETTER = "guaranteeLetter";
    public static final String P_GUARANTEE_LETTER_DATE = "guaranteeLetterDate";
    public static final String L_GUARANTEE_LETTER_COPY = "guaranteeLetterCopy";
    public static final String P_ORG_UNIT_TITLE = "orgUnitTitle";
    public static final String P_ORG_UNIT_SHORT_TITLE = "orgUnitShortTitle";
    public static final String L_ORG_UNIT_LEGAL_FORM = "orgUnitLegalForm";

    private ExternalOrgUnit _orgUnit;     // Организация
    private String _guaranteeLetter;     // Номер гарантийного письма
    private Date _guaranteeLetterDate;     // Дата гарантийного письма
    private DatabaseFile _guaranteeLetterCopy;     // Скан-копия гарантийного письма
    private String _orgUnitTitle;     // Название организации
    private String _orgUnitShortTitle;     // Сокращенное название организации
    private LegalForm _orgUnitLegalForm;     // ОПФ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Организация.
     */
    public ExternalOrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Организация.
     */
    public void setOrgUnit(ExternalOrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Номер гарантийного письма.
     */
    @Length(max=255)
    public String getGuaranteeLetter()
    {
        return _guaranteeLetter;
    }

    /**
     * @param guaranteeLetter Номер гарантийного письма.
     */
    public void setGuaranteeLetter(String guaranteeLetter)
    {
        dirty(_guaranteeLetter, guaranteeLetter);
        _guaranteeLetter = guaranteeLetter;
    }

    /**
     * @return Дата гарантийного письма.
     */
    public Date getGuaranteeLetterDate()
    {
        return _guaranteeLetterDate;
    }

    /**
     * @param guaranteeLetterDate Дата гарантийного письма.
     */
    public void setGuaranteeLetterDate(Date guaranteeLetterDate)
    {
        dirty(_guaranteeLetterDate, guaranteeLetterDate);
        _guaranteeLetterDate = guaranteeLetterDate;
    }

    /**
     * @return Скан-копия гарантийного письма.
     */
    public DatabaseFile getGuaranteeLetterCopy()
    {
        return _guaranteeLetterCopy;
    }

    /**
     * @param guaranteeLetterCopy Скан-копия гарантийного письма.
     */
    public void setGuaranteeLetterCopy(DatabaseFile guaranteeLetterCopy)
    {
        dirty(_guaranteeLetterCopy, guaranteeLetterCopy);
        _guaranteeLetterCopy = guaranteeLetterCopy;
    }

    /**
     * @return Название организации.
     */
    @Length(max=255)
    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    /**
     * @param orgUnitTitle Название организации.
     */
    public void setOrgUnitTitle(String orgUnitTitle)
    {
        dirty(_orgUnitTitle, orgUnitTitle);
        _orgUnitTitle = orgUnitTitle;
    }

    /**
     * @return Сокращенное название организации.
     */
    @Length(max=255)
    public String getOrgUnitShortTitle()
    {
        return _orgUnitShortTitle;
    }

    /**
     * @param orgUnitShortTitle Сокращенное название организации.
     */
    public void setOrgUnitShortTitle(String orgUnitShortTitle)
    {
        dirty(_orgUnitShortTitle, orgUnitShortTitle);
        _orgUnitShortTitle = orgUnitShortTitle;
    }

    /**
     * @return ОПФ.
     */
    public LegalForm getOrgUnitLegalForm()
    {
        return _orgUnitLegalForm;
    }

    /**
     * @param orgUnitLegalForm ОПФ.
     */
    public void setOrgUnitLegalForm(LegalForm orgUnitLegalForm)
    {
        dirty(_orgUnitLegalForm, orgUnitLegalForm);
        _orgUnitLegalForm = orgUnitLegalForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PrPracticeBaseRequestExtGen)
        {
            setOrgUnit(((PrPracticeBaseRequestExt)another).getOrgUnit());
            setGuaranteeLetter(((PrPracticeBaseRequestExt)another).getGuaranteeLetter());
            setGuaranteeLetterDate(((PrPracticeBaseRequestExt)another).getGuaranteeLetterDate());
            setGuaranteeLetterCopy(((PrPracticeBaseRequestExt)another).getGuaranteeLetterCopy());
            setOrgUnitTitle(((PrPracticeBaseRequestExt)another).getOrgUnitTitle());
            setOrgUnitShortTitle(((PrPracticeBaseRequestExt)another).getOrgUnitShortTitle());
            setOrgUnitLegalForm(((PrPracticeBaseRequestExt)another).getOrgUnitLegalForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeBaseRequestExtGen> extends PrPracticeBaseRequest.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeBaseRequestExt.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeBaseRequestExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
                case "guaranteeLetter":
                    return obj.getGuaranteeLetter();
                case "guaranteeLetterDate":
                    return obj.getGuaranteeLetterDate();
                case "guaranteeLetterCopy":
                    return obj.getGuaranteeLetterCopy();
                case "orgUnitTitle":
                    return obj.getOrgUnitTitle();
                case "orgUnitShortTitle":
                    return obj.getOrgUnitShortTitle();
                case "orgUnitLegalForm":
                    return obj.getOrgUnitLegalForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((ExternalOrgUnit) value);
                    return;
                case "guaranteeLetter":
                    obj.setGuaranteeLetter((String) value);
                    return;
                case "guaranteeLetterDate":
                    obj.setGuaranteeLetterDate((Date) value);
                    return;
                case "guaranteeLetterCopy":
                    obj.setGuaranteeLetterCopy((DatabaseFile) value);
                    return;
                case "orgUnitTitle":
                    obj.setOrgUnitTitle((String) value);
                    return;
                case "orgUnitShortTitle":
                    obj.setOrgUnitShortTitle((String) value);
                    return;
                case "orgUnitLegalForm":
                    obj.setOrgUnitLegalForm((LegalForm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
                case "guaranteeLetter":
                        return true;
                case "guaranteeLetterDate":
                        return true;
                case "guaranteeLetterCopy":
                        return true;
                case "orgUnitTitle":
                        return true;
                case "orgUnitShortTitle":
                        return true;
                case "orgUnitLegalForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
                case "guaranteeLetter":
                    return true;
                case "guaranteeLetterDate":
                    return true;
                case "guaranteeLetterCopy":
                    return true;
                case "orgUnitTitle":
                    return true;
                case "orgUnitShortTitle":
                    return true;
                case "orgUnitLegalForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return ExternalOrgUnit.class;
                case "guaranteeLetter":
                    return String.class;
                case "guaranteeLetterDate":
                    return Date.class;
                case "guaranteeLetterCopy":
                    return DatabaseFile.class;
                case "orgUnitTitle":
                    return String.class;
                case "orgUnitShortTitle":
                    return String.class;
                case "orgUnitLegalForm":
                    return LegalForm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeBaseRequestExt> _dslPath = new Path<PrPracticeBaseRequestExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeBaseRequestExt");
    }
            

    /**
     * @return Организация.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Номер гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getGuaranteeLetter()
     */
    public static PropertyPath<String> guaranteeLetter()
    {
        return _dslPath.guaranteeLetter();
    }

    /**
     * @return Дата гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getGuaranteeLetterDate()
     */
    public static PropertyPath<Date> guaranteeLetterDate()
    {
        return _dslPath.guaranteeLetterDate();
    }

    /**
     * @return Скан-копия гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getGuaranteeLetterCopy()
     */
    public static DatabaseFile.Path<DatabaseFile> guaranteeLetterCopy()
    {
        return _dslPath.guaranteeLetterCopy();
    }

    /**
     * @return Название организации.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getOrgUnitTitle()
     */
    public static PropertyPath<String> orgUnitTitle()
    {
        return _dslPath.orgUnitTitle();
    }

    /**
     * @return Сокращенное название организации.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getOrgUnitShortTitle()
     */
    public static PropertyPath<String> orgUnitShortTitle()
    {
        return _dslPath.orgUnitShortTitle();
    }

    /**
     * @return ОПФ.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getOrgUnitLegalForm()
     */
    public static LegalForm.Path<LegalForm> orgUnitLegalForm()
    {
        return _dslPath.orgUnitLegalForm();
    }

    public static class Path<E extends PrPracticeBaseRequestExt> extends PrPracticeBaseRequest.Path<E>
    {
        private ExternalOrgUnit.Path<ExternalOrgUnit> _orgUnit;
        private PropertyPath<String> _guaranteeLetter;
        private PropertyPath<Date> _guaranteeLetterDate;
        private DatabaseFile.Path<DatabaseFile> _guaranteeLetterCopy;
        private PropertyPath<String> _orgUnitTitle;
        private PropertyPath<String> _orgUnitShortTitle;
        private LegalForm.Path<LegalForm> _orgUnitLegalForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Организация.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Номер гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getGuaranteeLetter()
     */
        public PropertyPath<String> guaranteeLetter()
        {
            if(_guaranteeLetter == null )
                _guaranteeLetter = new PropertyPath<String>(PrPracticeBaseRequestExtGen.P_GUARANTEE_LETTER, this);
            return _guaranteeLetter;
        }

    /**
     * @return Дата гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getGuaranteeLetterDate()
     */
        public PropertyPath<Date> guaranteeLetterDate()
        {
            if(_guaranteeLetterDate == null )
                _guaranteeLetterDate = new PropertyPath<Date>(PrPracticeBaseRequestExtGen.P_GUARANTEE_LETTER_DATE, this);
            return _guaranteeLetterDate;
        }

    /**
     * @return Скан-копия гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getGuaranteeLetterCopy()
     */
        public DatabaseFile.Path<DatabaseFile> guaranteeLetterCopy()
        {
            if(_guaranteeLetterCopy == null )
                _guaranteeLetterCopy = new DatabaseFile.Path<DatabaseFile>(L_GUARANTEE_LETTER_COPY, this);
            return _guaranteeLetterCopy;
        }

    /**
     * @return Название организации.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getOrgUnitTitle()
     */
        public PropertyPath<String> orgUnitTitle()
        {
            if(_orgUnitTitle == null )
                _orgUnitTitle = new PropertyPath<String>(PrPracticeBaseRequestExtGen.P_ORG_UNIT_TITLE, this);
            return _orgUnitTitle;
        }

    /**
     * @return Сокращенное название организации.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getOrgUnitShortTitle()
     */
        public PropertyPath<String> orgUnitShortTitle()
        {
            if(_orgUnitShortTitle == null )
                _orgUnitShortTitle = new PropertyPath<String>(PrPracticeBaseRequestExtGen.P_ORG_UNIT_SHORT_TITLE, this);
            return _orgUnitShortTitle;
        }

    /**
     * @return ОПФ.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt#getOrgUnitLegalForm()
     */
        public LegalForm.Path<LegalForm> orgUnitLegalForm()
        {
            if(_orgUnitLegalForm == null )
                _orgUnitLegalForm = new LegalForm.Path<LegalForm>(L_ORG_UNIT_LEGAL_FORM, this);
            return _orgUnitLegalForm;
        }

        public Class getEntityClass()
        {
            return PrPracticeBaseRequestExt.class;
        }

        public String getEntityName()
        {
            return "prPracticeBaseRequestExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
