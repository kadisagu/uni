/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddRegPracticeBase;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.logic.SimilarExternalOrgUnitDSHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;
import ru.tandemservice.unipractice.entity.PrPracticeBaseInt;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.PrPracticeAssignmentManager;

import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.F_FORMATIVE_ORG_UNIT_DS;
import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.PRACTICE_BASE_DS;

/**
 * @author Andrey Avetisov
 * @since 15.10.2014
 */
@Configuration
public class PrPracticeAssignmentAddRegPracticeBase extends BusinessComponentManager
{
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String EXTERNAL_ORG_UNIT_DS = "externalOrgUnitDS";
    public static final String PRACTICE_BASE_KIND_DS = "practiceBaseKindDS";
    public static final String LEGAL_FORM_DS = "legalFormDS";
    public static final String SIMILAR_EXTERNAL_ORG_UNIT_DS = "similarExternalOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PRACTICE_BASE_KIND_DS, practiceBaseKindDSHandler()).addColumn(PrPracticeBaseKind.P_TITLE))
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(OrgUnit.P_FULL_TITLE))
                .addDataSource(selectDS(EXTERNAL_ORG_UNIT_DS, externalOrgUnitDSHandler()).addColumn(ExternalOrgUnit.P_LEGAL_FORM_WITH_TITLE))
                .addDataSource(selectDS(LEGAL_FORM_DS, legalFormDSHandler()).addColumn(LegalForm.P_TITLE))
                .addDataSource(searchListDS(SIMILAR_EXTERNAL_ORG_UNIT_DS, similarExternalOrgUnitDS(), similarExternalOrgUnitDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint similarExternalOrgUnitDS()
    {
        return columnListExtPointBuilder(SIMILAR_EXTERNAL_ORG_UNIT_DS)
                .addColumn(radioColumn("select"))
                .addColumn(publisherColumn("title", ExternalOrgUnit.title()))
                .addColumn(textColumn("shortTitle", ExternalOrgUnit.shortTitle()))
                .addColumn(textColumn("legalForm", ExternalOrgUnit.legalForm().title()))
                .addColumn(textColumn("factAddress", ExternalOrgUnit.factAddress().title()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.notExists(new DQLSelectBuilder()
                                                        .fromEntity(PrPracticeBaseInt.class, "b").column("b.id")
                                                        .where(DQLExpressions.eq(DQLExpressions.property("b", PrPracticeBaseInt.orgUnit()), DQLExpressions.property(alias)))
                                                        .buildQuery()
                ));
            }
        }
                .where(OrgUnit.archival(), false)
                .order(OrgUnit.fullTitle())
                .filter(OrgUnit.fullTitle())
                .pageable(true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> externalOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), ExternalOrgUnit.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.notExists(new DQLSelectBuilder()
                                                           .fromEntity(PrPracticeBaseExt.class, "b").column("b.id")
                                                           .where(DQLExpressions.eq(DQLExpressions.property("b", PrPracticeBaseInt.orgUnit()), DQLExpressions.property(alias)))
                                                           .buildQuery()
                ));
            }
        }

                .where(ExternalOrgUnit.active(), true)
                .order(ExternalOrgUnit.legalForm().shortTitle())
                .order(ExternalOrgUnit.title())
                .filter(ExternalOrgUnit.legalForm().shortTitle())
                .filter(ExternalOrgUnit.title())
                .pageable(true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> practiceBaseKindDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), PrPracticeBaseKind.class)
                .order(PrPracticeBaseKind.title())
                .filter(PrPracticeBaseKind.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> legalFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), LegalForm.class)
                .order(LegalForm.shortTitle())
                .filter(LegalForm.shortTitle());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> similarExternalOrgUnitDSHandler()
    {
        return new SimilarExternalOrgUnitDSHandler(getName());
    }
}
