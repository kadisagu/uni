/* $Id$ */
package ru.tandemservice.unipractice.session.dao;

import com.google.common.collect.Maps;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeResult;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.SessionMarkableDocHolder;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.practice.dao.ISessionPracticeDao;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 05.11.2014
 */
public class SessionPracticeDao extends UniBaseDao implements ISessionPracticeDao
{
    private class PracticeResult implements IPracticeResult
    {
        private boolean _closed;
        private SessionMarkGradeValueCatalogItem _mark;

        @Override
        public boolean isClosed()
        {
            return _closed;
        }

        @Override
        public SessionMarkGradeValueCatalogItem getMark()
        {
            return _mark;
        }

        public PracticeResult(boolean closed, SessionMarkGradeValueCatalogItem mark)
        {
            _closed = closed;
            _mark = mark;
        }
    }

    @Override
    public boolean showPracticeTutorMarkColumnInBulletin(SessionMarkableDocHolder.IMarkableDoc document)
    {
        return document.getMarkingProperties().getRegistryElementPart().getRegistryElement() instanceof EppRegistryPractice;
    }

    @Override
    public Map<Long, IPracticeResult> getPracticeTutorMarks(Collection<Long> studentWpeCActionIds)
    {
        if (studentWpeCActionIds.isEmpty())
            return Collections.emptyMap();

        final List<Object[]> items = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "e")
                .column("e.id")
                .column(property("r", PrPracticeResult.tutorMark()))
                .column(property("a", PrPracticeAssignment.state().code()))
                .joinEntity("e", DQLJoinType.inner, PrPracticeAssignment.class, "a",
                            eq(property("e", EppStudentWpeCAction.studentWpe()), property("a", PrPracticeAssignment.practice())))
                .joinEntity("a", DQLJoinType.inner, PrPracticeResult.class, "r",
                            eq(property("a.id"), property("r", PrPracticeResult.practiceAssignment())))
                .where(in(property("e.id"), studentWpeCActionIds))
                .createStatement(getSession()).list();

        final Map<Long, IPracticeResult> resultMap = Maps.newHashMapWithExpectedSize(items.size());
        for (Object[] item : items)
        {
            final Long slotId = (Long) item[0];
            final String stateCode = (String) item[2];
            final SessionMarkGradeValueCatalogItem mark = (SessionMarkGradeValueCatalogItem) item[1];

            resultMap.put(slotId, new PracticeResult(PrPracticeAssignmentStateCodes.FINISHED.equals(stateCode), mark));
        }
        return resultMap;
    }
}