package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.PrPracticeBaseManager;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.AddEdit.PrPracticeBaseAddEdit;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.RegApplications.PrPracticeBaseRegApplications;

import java.util.Arrays;
import java.util.List;

import static ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseConstants.*;

public class PrPracticeBaseListUI extends UIPresenter {

    private static final Long ACTIVE = 0L;
    private static final Long ARCHIVE = 1L;

    private static final List<IEntity> PRACTICE_BASE_STATUS_LIST = Arrays.asList(new IEntity[]{new IdentifiableWrapper(ACTIVE, "активна"), new IdentifiableWrapper(ARCHIVE, "в архиве")});

    public void onClickAddPrPracticeBase() {
        _uiActivation.asRegionDialog(PrPracticeBaseAddEdit.class)
                .activate();
    }

    public void onClickAppRegPracticeBase()
    {
        _uiActivation.asRegion(PrPracticeBaseRegApplications.class).activate();
    }

    public void onDeleteEntityFromList() {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickSendToArchive() {
        PrPracticeBaseManager.instance().prPracticeBaseDao().sendToArchive(getListenerParameterAsLong());
    }

    public void onClickSendFromArchive() {
        PrPracticeBaseManager.instance().prPracticeBaseDao().sendFromArchive(getListenerParameterAsLong());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (PR_PRACTICE_BASE_DS.equals(dataSource.getName())) {
            dataSource.put(PR_PRACTICE_BASE_KIND_FILTER, _uiSettings.get(PR_PRACTICE_BASE_KIND_FILTER));

            IdentifiableWrapper wrapper = _uiSettings.get(PR_PRACTICE_BASE_STATUS_FILTER);
            if (wrapper != null) {
                Boolean archive = wrapper.getId().equals(ARCHIVE);
                dataSource.put(PR_PRACTICE_BASE_STATUS_FILTER, archive);
            }
        }
    }

    public List<IEntity> getPrPracticeBaseStatuses() {
        return PRACTICE_BASE_STATUS_LIST;
    }
}
