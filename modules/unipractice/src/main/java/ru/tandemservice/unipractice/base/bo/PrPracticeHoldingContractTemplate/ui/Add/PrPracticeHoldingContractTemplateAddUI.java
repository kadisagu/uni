/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplateUI;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContractTemplate.PrPracticeHoldingContractTemplateManager;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;

import java.util.Date;

/**
 * @author azhebko
 * @since 07.11.2014
 */
@Input({@Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTEXT_ID, binding = "practiceBase.id", required = true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_KIND_ID, binding="versionCreateData.contractKindHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_PRINT_TEMPLATE_ID, binding="versionCreateData.printTemplateHolder.id")
})
public class PrPracticeHoldingContractTemplateAddUI extends UIPresenter
{
    private PrPracticeBaseExt _practiceBase = new PrPracticeBaseExt();
    private CtrContractVersion _contractVersion = new CtrContractVersion();

    private final CtrContractVersionCreateData versionCreateData = new CtrContractVersionCreateData();

    public CtrContractVersionCreateData getVersionCreateData()
    {
        return versionCreateData;
    }

    @Override
    public void onComponentRefresh()
    {
        getVersionCreateData().doRefresh();
        _practiceBase = IUniBaseDao.instance.get().getNotNull(PrPracticeBaseExt.class, _practiceBase.getId());
    }

    public void onClickApply()
    {
        if (getContractVersion().getDurationEndDate() != null && getContractVersion().getDurationEndDate().before(getContractVersion().getDurationBeginDate()))
        {
            UserContext.getInstance().getErrorCollector().add("Дата начала действия договора должна быть меньше даты окончания.", "durationBeginDate", "durationEndDate");
            return;
        }

        PrPracticeHoldingContractTemplateManager.instance().dao().doSavePracticeHoldingContract(getContractVersion(), getPracticeBase(), getVersionCreateData(), _contractVersion.getDocStartDate());
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, getContractVersion().getId()).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickClose()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickBack()
    {
        this.deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.FALSE));
    }

    public PrPracticeBaseExt getPracticeBase() { return _practiceBase; }
    public CtrContractVersion getContractVersion() { return _contractVersion; }

}