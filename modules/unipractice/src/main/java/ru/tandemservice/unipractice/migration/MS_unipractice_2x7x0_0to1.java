package ru.tandemservice.unipractice.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipractice_2x7x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность prPracticeBaseContract

        if (tool.tableExists("pr_practice_base_contract_t"))
            return;

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("pr_practice_base_contract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("contractobject_id", DBType.LONG).setNullable(false), 
				new DBColumn("practicebase_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);
		}

        // гарантировать наличие кода сущности
        short practiceBaseContractCode = tool.entityCodes().ensure("prPracticeBaseContract");
        Map<Long, Long> contractVersion2PracticeBaseMap = new HashMap<>();

        Statement statement = tool.getConnection().createStatement();
        statement.execute("" +
            "select v.contract_id, pr_tmpl.practicebase_id from pr_practice_holding_ctmpldt_t pr_tmpl " +
            "inner join ctr_version_template_data_t tmpl on tmpl.id = pr_tmpl.id " +
            "inner join ctr_contractver_t v on v.id = tmpl.owner_id ");

        ResultSet resultSet = statement.getResultSet();

        while (resultSet.next())
            contractVersion2PracticeBaseMap.put(resultSet.getLong(1), resultSet.getLong(2));

        PreparedStatement preparedStatement = tool.prepareStatement("insert into pr_practice_base_contract_t (id, discriminator, contractobject_id, practicebase_id) values (?, ?, ?, ?) ");
        preparedStatement.setShort(2, practiceBaseContractCode);
        for (Map.Entry<Long, Long> e: contractVersion2PracticeBaseMap.entrySet())
        {
            preparedStatement.setLong(1, EntityIDGenerator.generateNewId(practiceBaseContractCode));
            preparedStatement.setLong(3, e.getKey());
            preparedStatement.setLong(4, e.getValue());
            preparedStatement.execute();
        }

        ////////////////////////////////////////////////////////////////////////////////
		// сущность prPracticeHoldingContractTemplateData

		// удалено свойство practiceBase
		{
			// удалить колонку
			tool.dropColumn("pr_practice_holding_ctmpldt_t", "practicebase_id");
		}
    }
}