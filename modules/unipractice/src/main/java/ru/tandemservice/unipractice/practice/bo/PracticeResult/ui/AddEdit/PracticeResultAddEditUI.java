package ru.tandemservice.unipractice.practice.bo.PracticeResult.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeResult;

import java.util.Date;

import static ru.tandemservice.unipractice.practice.bo.PracticeResult.logic.PracticeResultConstants.*;

@Input({
    @Bind(key = PRACTICE_RESULT_ID, binding = PRACTICE_RESULT_ID),
    @Bind(key = PRACTICE_ASSIGNMENT_ID, binding = PRACTICE_ASSIGNMENT_ID, required = true)
})
public class PracticeResultAddEditUI extends UIPresenter {

    private PrPracticeAssignment prPracticeAssignment = new PrPracticeAssignment();

    private Long practiceResultId;
    private PrPracticeResult prPracticeResult;

    @Override
    public void onComponentRefresh() {
        prPracticeAssignment = DataAccessServices.dao().get(PrPracticeAssignment.class, prPracticeAssignment.getId());
        if(practiceResultId != null){
            prPracticeResult = DataAccessServices.dao().get(PrPracticeResult.class, practiceResultId);
        } else {
            prPracticeResult = new PrPracticeResult();
            prPracticeResult.setDateStart(prPracticeAssignment.getDateStart());
            prPracticeResult.setDateEnd(prPracticeAssignment.getDateEnd());
            prPracticeResult.setAcceptanceDate(new Date());
            prPracticeResult.setPracticeAssignment(prPracticeAssignment);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if(dataSource.getName().equals(GRADE_SCALE_DS)){
            dataSource.put(ACTIVITY_ELEMENT_PART_ID, prPracticeAssignment.getPractice().getRegistryElementPart().getId());
        }
    }

    public void onClickSave(){
        if(prPracticeResult.getDateStart().after(prPracticeResult.getDateEnd()))
            _uiSupport.error("Фактическая дата прохождения практики «с» не может быть позже даты «по»", "startDate", "endDate");
        if(prPracticeResult.getCompleted() != null && prPracticeResult.getCompleted() && prPracticeResult.getAcceptanceDate() != null && prPracticeResult.getAcceptanceDate().before(prPracticeResult.getDateEnd()))
            _uiSupport.error("Дата приема результатов практики не может быть раньше фактической даты окончания прохождения практики", "acceptanceDate", "endDate");
        if(getUserContext().getErrorCollector().hasErrors())
            return;
        DataAccessServices.dao().saveOrUpdate(prPracticeResult);
        deactivate();
    }

    public PrPracticeAssignment getPrPracticeAssignment() {
        return prPracticeAssignment;
    }

    public void setPrPracticeAssignment(PrPracticeAssignment prPracticeAssignment) {
        this.prPracticeAssignment = prPracticeAssignment;
    }

    public Long getPracticeResultId() {
        return practiceResultId;
    }

    public void setPracticeResultId(Long practiceResultId) {
        this.practiceResultId = practiceResultId;
    }

    public PrPracticeResult getPrPracticeResult() {
        return prPracticeResult;
    }

    public void setPrPracticeResult(PrPracticeResult prPracticeResult) {
        this.prPracticeResult = prPracticeResult;
    }
}
