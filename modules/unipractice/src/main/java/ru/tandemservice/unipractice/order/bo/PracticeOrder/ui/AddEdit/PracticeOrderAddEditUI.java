package ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import java.util.Date;

import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderConstants.PRACTICE_ORDER_ID;

@Input({
        @Bind(key = PRACTICE_ORDER_ID, binding = PRACTICE_ORDER_ID)
})
public class PracticeOrderAddEditUI extends UIPresenter {

    private PrPracticeOrder practiceOrder;

    private Long practiceOrderId;

    @Override
    public void onComponentRefresh() {
        if (practiceOrderId != null) {
            practiceOrder = DataAccessServices.dao().getNotNull(PrPracticeOrder.class, practiceOrderId);
        } else {
            practiceOrder = new PrPracticeOrder();
            OrderStates state = DataAccessServices.dao().getByCode(OrderStates.class, OrderStatesCodes.FORMING);
            practiceOrder.setState(state);
            practiceOrder.setCreateDate(new Date());
        }
    }

    public String getPermissionKey() {
        if (isNewEntity()) {
            return "addPrPracticeOrder";
        } else {
            return "edit_prPracticeOrder";
        }
    }

    public boolean isNewEntity() {
        return practiceOrderId == null;
    }

    public void onClickApply() {
        if (validate().hasErrors()) {
            return;
        }
        DataAccessServices.dao().saveOrUpdate(practiceOrder);
        deactivate();
    }

    protected ErrorCollector validate()
    {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        if (!PracticeOrderManager.instance().practiceOrderDao().isOrderNumberValid(practiceOrder))
            errorCollector.add("Номер приказа должен быть уникален в рамках календарного года среди всех приказов о направлении студентов на практику.", "number");

        return errorCollector;
    }

    public PrPracticeOrder getPracticeOrder() {
        return practiceOrder;
    }

    public void setPracticeOrder(PrPracticeOrder practiceOrder) {
        this.practiceOrder = practiceOrder;
    }

    public void setPracticeOrderId(Long practiceOrderId) {
        this.practiceOrderId = practiceOrderId;
    }

    public Long getPracticeOrderId() {
        return practiceOrderId;
    }

    public boolean isEducationYearEditDisabled()
    {
        return practiceOrder.getParagraphCount() > 0;
    }

    public boolean isOrderTypeEditDisabled()
    {
        return practiceOrder.getParagraphCount() > 0;
    }
}
