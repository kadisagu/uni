package ru.tandemservice.unipractice.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unipractice.entity.catalog.gen.PrPracticeOrderTypeGen;

/**
 * Тип приказа на практику
 */
public class PrPracticeOrderType extends PrPracticeOrderTypeGen implements IDynamicCatalogItem
{
}