/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.StudentPreallocationManager;

/**
 * @author Andrey Avetisov
 * @since 15.10.2014
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
       })
public class StudentPreallocationPubUI extends UIPresenter
{
    private Long _reportId;
    private StPracticePreallocation _report;

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(_reportId);
    }

    public StPracticePreallocation getReport()
    {
        return _report;
    }

    public void setReport(StPracticePreallocation report)
    {
        _report = report;
    }

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return new CommonPostfixPermissionModel( _report.getOrgUnit() != null ? OrgUnitSecModel.getPostfix(_report.getOrgUnit()) : null);
    }



    public String getViewKey()
    {
        return getSec().getPermission(_report.getOrgUnit() != null ? "orgUnit_viewStPracticePreallocationReportPermissionKey" : "stPracticePreallocationReportPermissionKey");
    }

    @Override
    public ISecured getSecuredObject()
    {
        return getReport().getOrgUnit() != null ? getReport().getOrgUnit() : super.getSecuredObject();
    }

    public void onClickPrint()
    {

        if (getReport().getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer().document(getReport().getContent()),
                false);

    }
}
