/* $Id: PrPracticeAssignmentAddGuaranteeUI.java 302 2014-10-08 11:38:56Z aavetisov $ */
package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddGuarantee;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeAssignmentState;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.PrPracticeAssignmentManager;

import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.PR_PRACTICE_ASSIGNMENT_ID;

/**
 * @author Andrey Avetisov
 * @since 08.10.2014
 */

@Input({
               @Bind(key = PR_PRACTICE_ASSIGNMENT_ID, binding = PR_PRACTICE_ASSIGNMENT_ID)
       })
public class PrPracticeAssignmentAddGuaranteeUI extends UIPresenter
{
    private PrPracticeAssignment prPracticeAssignment;

    private Long prPracticeAssignmentId;
    private IUploadFile _scanCopyUploadFile;

    @Override
    public void onComponentRefresh() {
        if (prPracticeAssignmentId != null) {
            prPracticeAssignment = DataAccessServices.dao().getNotNull(PrPracticeAssignment.class, prPracticeAssignmentId);
        } else {
            prPracticeAssignment = new PrPracticeAssignment();
            PrPracticeAssignmentState state = DataAccessServices.dao().getByCode(PrPracticeAssignmentState.class, PrPracticeAssignmentStateCodes.FORMING);
            prPracticeAssignment.setState(state);
        }
    }

    public Long getPrPracticeAssignmentId()
    {
        return prPracticeAssignmentId;
    }

    public void setPrPracticeAssignmentId(Long prPracticeAssignmentId)
    {
        this.prPracticeAssignmentId = prPracticeAssignmentId;
    }

    public PrPracticeAssignment getPrPracticeAssignment()
    {
        return prPracticeAssignment;
    }

    public void setPrPracticeAssignment(PrPracticeAssignment prPracticeAssignment)
    {
        this.prPracticeAssignment = prPracticeAssignment;
    }


    public IUploadFile getScanCopyUploadFile()
    {
        return _scanCopyUploadFile;
    }

    public void setScanCopyUploadFile(IUploadFile scanCopyUploadFile)
    {
        _scanCopyUploadFile = scanCopyUploadFile;
    }



    public void onClickApply(){
        if(getScanCopyUploadFile() != null){
            PrPracticeAssignmentManager.instance().practiceAssignmentDao().addGuaranteeLetterToPracticeAssignment(getPrPracticeAssignment(), getScanCopyUploadFile());
        }
        deactivate();
    }

}
