package ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic;

import org.tandemframework.core.entity.IEntity;

import java.util.Collection;
import java.util.List;

public interface IPrPracticeBaseDao {

    void sendToArchive(Long prPracticeBaseId);
    void sendFromArchive(Long prPracticeBaseId);

    void createPracticeBaseByRequests(List<PrPracticeBaseRequestWrapper> wrapperList);
}
