/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeReportPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author nvankov
 * @since 10/28/14
 */
@Configuration
public class PrPracticeReportPersonManager extends BusinessObjectManager
{
    public static PrPracticeReportPersonManager instance()
    {
        return instance(PrPracticeReportPersonManager.class);
    }
}



    