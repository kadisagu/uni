package ru.tandemservice.unipractice.order.bo.PracticeOrder.logic;

import org.hibernate.Session;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unipractice.order.entity.PrAbstractParagraph;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import java.util.Date;
import java.util.List;

import static org.tandemframework.core.CoreStringUtils.escapeLike;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderConstants.*;

public class PracticeOrderDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    public static final String F_CREATE_DATE = "createDate";
    public static final String F_COMMIT_DATE = "commitDate";
    public static final String F_ORDER_NUMBER = "orderNumber";
    public static final String F_ORDER_TYPE = "orderType";
    public static final String F_ORDER_STATE = "orderState";
    public static final String F_EDUCATION_YEAR = "educationYear";
    private static final String ORDER = "o";

    public PracticeOrderDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PrPracticeOrder.class, ORDER).column(ORDER);

        builder.column(
                new DQLSelectBuilder().fromEntity(PrAbstractParagraph.class, "p")
                        .column(DQLFunctions.countStar())
                        .where(eq(property(PrAbstractParagraph.order().fromAlias("p")), property(ORDER, "id")))
                        .buildQuery()
        );

        Date createDate = context.get(F_CREATE_DATE);
        if (createDate != null) {
            builder.where(inDay(PrPracticeOrder.createDate().fromAlias(ORDER), createDate));
        }

        Date commitDate = context.get(F_COMMIT_DATE);
        if (commitDate != null) {
            builder.where(inDay(PrPracticeOrder.commitDate().fromAlias(ORDER), commitDate));
        }

        String orderNumber = context.get(F_ORDER_NUMBER);
        if (orderNumber != null) {
            builder.where(likeUpper(
                    property(PrPracticeOrder.number().fromAlias(ORDER)),
                    value(escapeLike(orderNumber))));
        }

        Object orderType = context.get(F_ORDER_TYPE);
        if (orderType != null) {
            builder.where(eq(property(PrPracticeOrder.type().fromAlias(ORDER)), commonValue(orderType)));
        }

        Object orderState = context.get(F_ORDER_STATE);
        if (orderState != null) {
            builder.where(eq(property(PrPracticeOrder.state().fromAlias(ORDER)), commonValue(orderState)));
        }

        Object educationYear = context.get(F_EDUCATION_YEAR);
        if (educationYear != null) {
            builder.where(eq(property(PrPracticeOrder.educationYear().fromAlias(ORDER)), commonValue(educationYear)));
        }

        builder.order(property(ORDER, input.getEntityOrder().getKeyString()), input.getEntityOrder().getDirection());

        return buildOutput(input, builder, context.getSession());
    }

    private DSOutput buildOutput(DSInput input, DQLSelectBuilder builder, Session session) {
        DQLExecutionContext qdlContext = new DQLExecutionContext(session);
        DSOutput output = new DSOutput(input);

        Number count = builder.createCountStatement(qdlContext).uniqueResult();
        int number = count == null ? 0 : count.intValue();
        output.setTotalSize(number);

        IDQLStatement stmt = builder.createStatement(qdlContext);
        stmt.setFirstResult(output.getStartRecord());
        stmt.setMaxResults(output.getCountRecord());
        List<Object[]> list = stmt.list();

        for (Object[] objects : list) {
            PrPracticeOrder order = (PrPracticeOrder) objects[0];
            DataWrapper dataWrapper = new DataWrapper(order.getId(), "", order);
            Long paragraphCount = (Long) objects[1];
            dataWrapper.setProperty(VIEW_PROPERTY_PARAGRAPH_COUNT, paragraphCount);
            dataWrapper.setProperty(VIEW_PROPERTY_PRINTING_DISABLED, paragraphCount == 0);
            dataWrapper.setProperty(VIEW_PROPERTY_EDIT_DELETE_ENABLED, !order.getState().getCode().equals(OrderStatesCodes.FORMING));
            output.addRecord(dataWrapper);
        }
        return output;
    }

}
