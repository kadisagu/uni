package ru.tandemservice.unipractice.order.bo.PracticeOrder.logic;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderPrintForm;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderPrintFormCodes.*;
import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes.PREDDIPLOM;
import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes.PROIZVODSTV;

public class OrderPrinter implements IOrderPrinter {

    public static final String PARAGRAPHS = "PARAGRAPHS";
    public static final String SUBPARAGRAPHS = "SUBPARAGRAPHS";
    private static DateFormatter defaultDateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;

    private PrPracticeOrderPrintForm orderPrintForm;
    private PrPracticeOrderPrintForm paragraphPrintForm;
    private PrPracticeOrderPrintForm subParagraphPrintForm;

    public RtfDocument print(PrPracticeOrder prPracticeOrder)
    {
        checkOrderType(prPracticeOrder);
        RtfDocument template = new RtfReader().read(orderPrintForm.getCurrentTemplate());
        RtfInjectModifier modifier = new RtfInjectModifier();

        TopOrgUnit topOrgUnit = TopOrgUnit.getInstance();

        modifier.put("highSchoolTitle", topOrgUnit.getTitle());
        modifier.put("commitDate", defaultDateFormatter.format(prPracticeOrder.getCommitDate()));
        modifier.put("orderNumber", prPracticeOrder.getNumber());
        modifier.put("highSchoolCity", topOrgUnit.getTerritorialTitle());
        modifier.put("year", prPracticeOrder.getEducationYear().getTitle());

        injectParagraphs(template, prPracticeOrder);

        RtfTableModifier tableModifier = new RtfTableModifier();
        appendVisas(tableModifier, prPracticeOrder);
        tableModifier.modify(template);

        modifier.modify(template);
        return template;
    }

    private void checkOrderType(PrPracticeOrder prPracticeOrder) {
        switch (prPracticeOrder.getType().getCode()) {
            case PREDDIPLOM:
                orderPrintForm = UniDaoFacade.getCoreDao().getCatalogItem(PrPracticeOrderPrintForm.class, PRE_GRADUATION_PRACTICE_ORDER);
                paragraphPrintForm = UniDaoFacade.getCoreDao().getCatalogItem(PrPracticeOrderPrintForm.class, PARAGRAPH_PRE_GRADUATION_PRACTICE_ORDER);
                subParagraphPrintForm = UniDaoFacade.getCoreDao().getCatalogItem(PrPracticeOrderPrintForm.class, SUB_PARAGRAPH_PRE_GRADUATION_PRACTICE_ORDER);
                break;
            case PROIZVODSTV:
                orderPrintForm = UniDaoFacade.getCoreDao().getCatalogItem(PrPracticeOrderPrintForm.class, INDUSTRIAL_PRACTICE_ORDER);
                paragraphPrintForm = UniDaoFacade.getCoreDao().getCatalogItem(PrPracticeOrderPrintForm.class, PARAGRAPH_INDUSTRIAL_PRACTICE_ORDER);
                subParagraphPrintForm = UniDaoFacade.getCoreDao().getCatalogItem(PrPracticeOrderPrintForm.class, SUB_PARAGRAPH_INDUSTRIAL_PRACTICE_ORDER);
                break;
            default:
                throw new ApplicationException("Неподдерживаемый тип приказа: " + prPracticeOrder.getType().getTitle());
        }
    }

    public void injectParagraphs(final RtfDocument document, PrPracticeOrder order) {
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);
        if (rtfSearchResult.isFound()) {
            List<IRtfElement> parList = new ArrayList<>();
            for (IAbstractParagraph paragraph : order.getParagraphList()) {
                RtfDocument paragraphPart = createParagraphDocument(paragraph);

                injectSubParagraphs(paragraphPart, paragraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    @SuppressWarnings("unchecked")
    private void injectSubParagraphs(RtfDocument document, IAbstractParagraph paragraph) {
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, SUBPARAGRAPHS);
        if (rtfSearchResult.isFound()) {
            List<IRtfElement> subList = new ArrayList<>();

            ListMultimap<Long, PrPracticeExtract> resMap = ArrayListMultimap.create();
            List<PrPracticeExtract> flatExtractList = paragraph.getExtractList();
            for (PrPracticeExtract e : flatExtractList) {
                Long groupId = e.getEntity().getPractice().getStudent().getGroup().getId();
                resMap.put(groupId, e);
            }

            for (Long groupId : resMap.keySet()) {
                RtfDocument paragraphPart = createSubParagraphDocument(resMap.get(groupId));

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                subList.add(rtfGroup);
            }

            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), subList);
        }
    }

    @SuppressWarnings("unchecked")
    public RtfDocument createParagraphDocument(IAbstractParagraph paragraph) {
        List<IAbstractExtract> extractList = paragraph.getExtractList();
        if (extractList.size() == 0)
            throw new ApplicationException("Невозможно распечатать пустой параграф (№" + paragraph.getNumber() + ").");

        byte[] paragraphTemplate = paragraphPrintForm.getCurrentTemplate();

        RtfDocument paragraphPart = new RtfReader().read(paragraphTemplate);

        RtfInjectModifier modifier = new RtfInjectModifier();
        PrPracticeExtract extract = (PrPracticeExtract) paragraph.getExtractList().get(0);
        modifier.put("formativeOrgUnit", extract.getEntity().getPractice().getStudent().getEducationOrgUnit().getFormativeOrgUnit().getTitle());
        modifier.put("tutorOrgUnit", extract.getEntity().getPractice().getRegistryElementPart().getRegistryElement().getOwner().getTitle());
        modifier.put("beginDate", defaultDateFormatter.format(extract.getEntity().getDateStart()));
        modifier.put("endDate", defaultDateFormatter.format(extract.getEntity().getDateEnd()));

        modifier.modify(paragraphPart);
        return paragraphPart;
    }

    private RtfDocument createSubParagraphDocument(List<PrPracticeExtract> extracts) {
        byte[] paragraphTemplate = subParagraphPrintForm.getCurrentTemplate();

        RtfDocument paragraphPart = new RtfReader().read(paragraphTemplate);

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("group", extracts.get(0).getEntity().getPractice().getStudent().getGroup().getTitle());

        RtfTableModifier tableModifier = new RtfTableModifier();
        List<String[]> studentsData = new ArrayList<>();
        for (PrPracticeExtract extract : extracts) {
            String[] row = new String[2];
            row[0] = extract.getEntity().getPractice().getStudent().getFullFio();
            row[1] = extract.getEntity().getPracticeBase().getTitle();
            studentsData.add(row);
        }
        tableModifier.put("T", Iterables.toArray(studentsData, String[].class));

        modifier.modify(paragraphPart);
        tableModifier.modify(paragraphPart);
        return paragraphPart;
    }

    private void appendVisas(RtfTableModifier tableModifier, PrPracticeOrder order)
    {
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        List<String[]> visaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());

        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), "", "", "", str.toString()});
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
    }

}
