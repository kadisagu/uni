package ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderPrintForm;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderPrintFormCodes;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.AddEdit.PracticeOrderAddEdit;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import java.util.Collection;
import java.util.List;

import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes.PREDDIPLOM;
import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes.PROIZVODSTV;
import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderConstants.PRACTICE_ORDER_DS;
import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderConstants.PRACTICE_ORDER_ID;
import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderDSHandler.*;

public class PracticeOrderListUI extends UIPresenter {

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (PRACTICE_ORDER_DS.equals(dataSource.getName())) {
            dataSource.put(F_CREATE_DATE, _uiSettings.get(F_CREATE_DATE));
            dataSource.put(F_COMMIT_DATE, _uiSettings.get(F_COMMIT_DATE));
            dataSource.put(F_ORDER_NUMBER, _uiSettings.get(F_ORDER_NUMBER));
            dataSource.put(F_ORDER_TYPE, _uiSettings.get(F_ORDER_TYPE));
            dataSource.put(F_ORDER_STATE, _uiSettings.get(F_ORDER_STATE));
            dataSource.put(F_EDUCATION_YEAR, _uiSettings.get(F_EDUCATION_YEAR));
        }
    }

    @SuppressWarnings({"unused"})
    public void onClickPrintOrder() {
        PrPracticeOrder prPracticeOrder = DataAccessServices.dao().get(getListenerParameterAsLong());
        PrPracticeOrderPrintForm printForm = null;
        switch (prPracticeOrder.getType().getCode()) {
            case PREDDIPLOM:
                printForm = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.PRE_GRADUATION_PRACTICE_ORDER);
                break;
            case PROIZVODSTV:
                printForm = DataAccessServices.dao().getByCode(PrPracticeOrderPrintForm.class, PrPracticeOrderPrintFormCodes.INDUSTRIAL_PRACTICE_ORDER);
                break;
        }
        if (printForm != null)
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(printForm, "prPracticeOrder", prPracticeOrder);
    }

    @SuppressWarnings({"unused"})
    public void onClickAddPracticeOrder() {
        _uiActivation.asDesktopRoot(PracticeOrderAddEdit.class).activate();
    }

    @SuppressWarnings({"unused"})
    public void onClickSendOrdersForApproval() {
        IPrincipalContext principalContext = checkPrincipal();

        Collection<IEntity> selectedMarkList = getConfig().<BaseSearchListDataSource>getDataSource(PRACTICE_ORDER_DS).getOptionColumnSelectedObjects("checkbox");
        List<Long> idList = CommonBaseEntityUtil.getIdList(selectedMarkList);
        _uiSupport.setRefreshScheduled(true);
        PracticeOrderManager.instance().practiceOrderDao().doSendToCoordination(idList, (IPersistentPersonable) principalContext);
    }

    private IPrincipalContext checkPrincipal() {
        IPrincipalContext principalContext = getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        return principalContext;
    }

    public void onClickCommitOrders() {
        Collection<IEntity> selectedMarkList = getConfig().<BaseSearchListDataSource>getDataSource(PRACTICE_ORDER_DS).getOptionColumnSelectedObjects("checkbox");
        List<Long> idList = CommonBaseEntityUtil.getIdList(selectedMarkList);
        _uiSupport.setRefreshScheduled(true);
        PracticeOrderManager.instance().practiceOrderDao().doCommit(idList);
    }

    @SuppressWarnings({"unused"})
    public void onEditEntityFromList() {
        _uiActivation.asDesktopRoot(PracticeOrderAddEdit.class).parameter(PRACTICE_ORDER_ID, getListenerParameterAsLong()).activate();
    }

    @SuppressWarnings({"unused"})
    public void onDeleteEntityFromList() {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
