package ru.tandemservice.unipractice.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipractice_2x6x9_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.9")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность stPracticePreallocation

		// создана новая сущность
        {
            // создать таблицу
            if (!tool.tableExists("st_practice_preallocation_t"))
            {
                DBTable dbt = new DBTable("st_practice_preallocation_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("educationyear_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("practicekind_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("practicebegindate_p", DBType.DATE).setNullable(false),
                                          new DBColumn("practiceenddate_p", DBType.DATE).setNullable(false),
                                          new DBColumn("educationlevelshighschool_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("course_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("group_p", DBType.createVarchar(255))
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("stPracticePreallocation");

            }

        }
    }
}