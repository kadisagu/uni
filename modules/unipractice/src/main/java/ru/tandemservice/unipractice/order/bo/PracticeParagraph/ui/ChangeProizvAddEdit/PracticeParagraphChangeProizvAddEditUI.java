package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ChangeProizvAddEdit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.IPracticeOrderDao;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.AbstractAddEdit.AbstractPracticeParagraphChangeAddEditUI;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Input({
    @Bind(key = ORDER_ID, binding = ORDER_ID),
    @Bind(key = PARAGRAPH_ID, binding = PARAGRAPH_ID)
})
public class PracticeParagraphChangeProizvAddEditUI extends AbstractPracticeParagraphChangeAddEditUI
{
    private OrgUnit _formativeOrgUnit;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        if (paragraphId != null) {
            IPracticeOrderDao orderDao = PracticeOrderManager.instance().practiceOrderDao();
            List<PrPracticeExtract> extracts = orderDao.getExtractsByChangeParagraphId(paragraphId);
            _formativeOrgUnit = extracts.get(0).getPracticeAssignment().getPractice().getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        dataSource.put(PRACTICE_ORDER_TYPE, PrPracticeOrderTypeCodes.PROIZVODSTV);
        dataSource.put(FORMATIVE_ORG_UNIT_F, _formativeOrgUnit != null ? _formativeOrgUnit.getId() : null);
    }

    @Override
    protected ErrorCollector validateParagraph(List<PrPracticeExtract> extracts) {
        ErrorCollector errorCollector = super.validateParagraph(extracts);

        Set<Long> formativeOrgUnits = new HashSet<>();
        for (PrPracticeExtract extract : extracts) {
            formativeOrgUnits.add(extract.getPracticeAssignment().getPractice().getStudent().getEducationOrgUnit().getFormativeOrgUnit().getId());
        }
        if (formativeOrgUnits.size() > 1) {
            errorCollector.add("В параграф включены студенты с разных формирующих подразделений, корректная работа с формой невозможна. Исключите студентов из параграфа на карточке параграфа.");
        }
        return errorCollector;

    }

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }
}