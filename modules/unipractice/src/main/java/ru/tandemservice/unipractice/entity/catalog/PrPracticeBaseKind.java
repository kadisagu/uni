package ru.tandemservice.unipractice.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unipractice.entity.catalog.gen.PrPracticeBaseKindGen;

/**
 * Виды баз практик
 */
public class PrPracticeBaseKind extends PrPracticeBaseKindGen implements IDynamicCatalogItem
{
    public boolean isExternal()
    {
        return !isInternal();
    }
}