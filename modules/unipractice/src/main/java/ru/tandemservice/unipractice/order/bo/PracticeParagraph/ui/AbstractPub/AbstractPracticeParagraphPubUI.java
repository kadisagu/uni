package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.AbstractPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.Pub.PracticeOrderPub;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants;
import ru.tandemservice.unipractice.order.entity.*;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.util.ParagraphUtils.getParagraphEditComponentByOrderType;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

public abstract class AbstractPracticeParagraphPubUI<P extends PrAbstractParagraph, E extends PrAbstractExtract> extends UIPresenter {

    protected P paragraph;
    protected Long paragraphId;
    protected Long orderId;
    protected PrPracticeOrder order;
    protected SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateFormatter.PATTERN_DEFAULT);
    protected String startDates;
    protected String endDates;
    protected Set formativeOrgUnits;
    protected Set responsibleOrgUnits;
    protected CommonPostfixPermissionModel _secModel;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);
        if(dataSource.getName().equals(PracticeParagraphConstants.PRACTICE_EXTRACT_LIST_DS)){
            dataSource.put(PracticeParagraphConstants.PARAGRAPH_ID, paragraph.getId());
        }
    }

    @SuppressWarnings({"unused"})
    public Map getPublisherStudentTabParametersMap(){
        E current = _uiConfig.getDataSource(PracticeParagraphConstants.PRACTICE_EXTRACT_LIST_DS).getCurrent();
        Long studentId;
        if(current instanceof PrPracticeChangeExtract){
            studentId = ((PrPracticeChangeExtract)current).getEntity().getPracticeAssignment().getPractice().getStudent().getId();
        } else {
            studentId = ((PrPracticeExtract)current).getEntity().getPractice().getStudent().getId();
        }
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("publisherId", studentId);
        parameters.put("selectedStudentTab", "studentPracticeTab");
        return parameters;
    }

    public P getParagraph() {
        return paragraph;
    }

    public void setParagraph(P paragraph) {
        this.paragraph = paragraph;
    }

    public PrPracticeOrder getOrder() {
        return order;
    }

    public boolean isEditable()
    {
        return OrderStatesCodes.FORMING.equals(order.getState().getCode());
    }

    public void setOrder(PrPracticeOrder order) {
        this.order = order;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getParagraphId() {
        return paragraphId;
    }

    public void setParagraphId(Long paragraphId) {
        this.paragraphId = paragraphId;
    }

    @SuppressWarnings("unused")
    public String getDeletePracticeParagraphAlert() {
        return String.format(_uiConfig.getProperty("ui.delete.alert"));
    }

    @SuppressWarnings("unused")
    public void onDeleteEntityFromList(){
        if (_uiConfig.getDataSource(PRACTICE_EXTRACT_LIST_DS).getRecords().size() == 1) {
            throw new ApplicationException("Параграф приказа не может быть пустым. При необходимости удалите параграф приказа и создайте новый.");
        }
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        _uiSupport.doRefresh();
    }

    public void onDeletePracticeParagraph(){
        for(Object extract : _uiConfig.getDataSource(PRACTICE_EXTRACT_LIST_DS).getRecords()){
            DataAccessServices.dao().delete(((IEntity) extract).getId());
        }
        DataAccessServices.dao().delete(paragraph);
        _uiActivation.asCurrent(PracticeOrderPub.class).parameter(PUBLISHER_ID, order.getId()).activate();
    }

    public void onEditPracticeParagraph(){
        Class<? extends BusinessComponentManager> editClass = getParagraphEditComponentByOrderType(order.getType().getCode());
        _uiActivation.asRegion(editClass)
                .parameter(ORDER_ID, order.getId())
                .parameter(PARAGRAPH_ID, paragraph.getId())
                .activate();
    }

    public String getPageTitle(){
        return getOrderTitle(order);
    }

    public String getOrderTitle(PrPracticeOrder order) {
        String orderType = "";
        String base = "";
        switch (order.getType().getCode()){
            case PrPracticeOrderTypeCodes.PREDDIPLOM:
                orderType = _uiConfig.getProperty("ui.toPreDip");
                base = _uiConfig.getProperty("ui.changeOrderTitle");
                break;
            case PrPracticeOrderTypeCodes.PROIZVODSTV:
                orderType = _uiConfig.getProperty("ui.toProizv");
                base = _uiConfig.getProperty("ui.changeOrderTitle");
                break;
            case PrPracticeOrderTypeCodes.PREDDIPLOM_CHANGE:
                orderType = _uiConfig.getProperty("ui.toPreDip");
                base = _uiConfig.getProperty("ui.changeOrderParagraphTitle");
                break;
            case PrPracticeOrderTypeCodes.PROIZVODSTV_CHANGE:
                orderType = _uiConfig.getProperty("ui.toProizv");
                base = _uiConfig.getProperty("ui.changeOrderParagraphTitle");
                break;
        }
        String number = "";
        if(order.getNumber() != null){
            number = " № " + order.getNumber();
        }
        String date = "";
        if (order.getCommitDate() != null) {
            String from = _uiConfig.getProperty("ui.from");
            date = " " + from + " " + simpleDateFormat.format(order.getCommitDate());
        }
        return String.format(base, orderType, number, date);
    }

    @SuppressWarnings("unused")
    public String getStartDates(){
        return startDates;
    }

    @SuppressWarnings("unused")
    public String getEndDates(){
        return endDates;
    }

    @SuppressWarnings("unused")
    public Set getFormativeOrgUnits(){
        return formativeOrgUnits;
    }

    @SuppressWarnings("unused")
    public Set getResponsibleOrgUnits(){
        return responsibleOrgUnits;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}
