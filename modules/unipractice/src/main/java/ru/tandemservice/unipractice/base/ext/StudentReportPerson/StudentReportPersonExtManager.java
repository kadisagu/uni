/* $Id$ */
package ru.tandemservice.unipractice.base.ext.StudentReportPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author nvankov
 * @since 10/28/14
 */
@Configuration
public class StudentReportPersonExtManager extends BusinessObjectExtensionManager
{
}
