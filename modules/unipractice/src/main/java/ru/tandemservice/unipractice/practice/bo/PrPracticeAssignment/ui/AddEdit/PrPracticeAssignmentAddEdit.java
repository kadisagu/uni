package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.IFormatter;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.PrPracticeAssignmentManager;

import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.*;

@Configuration
public class PrPracticeAssignmentAddEdit extends BusinessComponentManager {

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(
                        selectDS(PRACTICE_DS, PrPracticeAssignmentManager.instance().practiceDSHandler())
                                .addColumn("title", null, new IFormatter<EppStudentWorkPlanElement>() {
                                    @Override
                                    public String format(EppStudentWorkPlanElement slot) {
                                        String practiceTitle = slot.getRegistryElementPart().getRegistryElement().getParent().getTitle();
                                        String term = slot.getTerm().getTitle();
                                        String year = slot.getYear().getEducationYear().getTitle();
                                        return practiceTitle + " (" + term + " семестр " + year + ")";
                                    }
                                })
                )
                .addDataSource(
                        selectDS(PRACTICE_BASE_DS, PrPracticeAssignmentManager.instance().practiceBaseDSHandler())
                                .addColumn(PrPracticeBase.P_TITLE)
                )
                .addDataSource(
                        selectDS(TUTOR_DS, PrPracticeAssignmentManager.instance().tutorDSHandler())
                                .addColumn(PpsEntry.P_FULL_FIO)
                                .addColumn(PpsEntry.orgUnit().shortTitle().s())
                                .addColumn(PpsEntry.titleInfo().s())
                )
                .create();
    }
}
