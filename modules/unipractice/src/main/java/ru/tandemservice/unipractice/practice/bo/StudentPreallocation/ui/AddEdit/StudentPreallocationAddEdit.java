/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic.StPreallocationEndDateDSHandler;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic.StPreallocationStartDateDSHandler;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 09.10.2014
 */
@Configuration
public class StudentPreallocationAddEdit extends BusinessComponentManager
{
    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String PRACTICE_KIND_DS = "practiceKindDS";
    public static final String EDUCATION_LEVELS_HIGHTSCHOOL_DS = "educationLevelsHighSchoolDS";
    public static final String GROUP_DS = "groupDS";
    public static final String PRACTICE_BEGIN_DATE_DS = "practiceBeginDateDS";
    public static final String PRACTICE_END_DATE_DS = "practiceEndDateDS";
    public static final String EDU_YEAR = "eduYear";
    public static final String PRACTICE_KIND = "practiceKind";
    public static final String EDUCATION_LEVEL_HIGHSCHOOL = "educationLevelHighSchool";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_YEAR_DS).handler(educationYearDSHandler()))
                .addDataSource(selectDS(PRACTICE_KIND_DS).handler(practiceKindDSHandler()).addColumn(EppRegistryStructure.P_TITLE))
                .addDataSource(selectDS(EDUCATION_LEVELS_HIGHTSCHOOL_DS).handler(educationLevelHighSchoolDSHandler()).addColumn(EducationLevelsHighSchool.P_FULL_TITLE))
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(selectDS(GROUP_DS).handler(groupDSHandler()).addColumn(Group.P_TITLE))
                .addDataSource(selectDS(PRACTICE_BEGIN_DATE_DS, startDateDSHandler()).addColumn(DataWrapper.P_ID, DataWrapper.P_ID, source -> {
                    PrPracticeAssignment prPracticeAssignment = DataAccessServices.dao().get(PrPracticeAssignment.class, (long)source);
                    return DateFormatter.DEFAULT_DATE_FORMATTER.format(prPracticeAssignment.getDateStart());
                }))
                .addDataSource(selectDS(PRACTICE_END_DATE_DS, endDateDSHandler()).addColumn(DataWrapper.P_ID, DataWrapper.P_ID, source -> {
                    PrPracticeAssignment prPracticeAssignment = DataAccessServices.dao().get(PrPracticeAssignment.class, (long)source);
                    return DateFormatter.DEFAULT_DATE_FORMATTER.format(prPracticeAssignment.getDateEnd());
                }))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationYear.class)
                .filter(EducationYear.title())
                .order(EducationYear.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceKindDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EppRegistryStructure.class, EppRegistryStructure.title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                ep.dqlBuilder.where(eq(
                        property(EppRegistryStructure.parent().code().fromAlias("e")),
                        value(EppRegistryStructureCodes.REGISTRY_PRACTICE)));
            }
        };
    }
    @Bean
    public IBusinessHandler<DSInput, DSOutput> startDateDSHandler()
    {
        return new StPreallocationStartDateDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> endDateDSHandler()
    {
        return new StPreallocationEndDateDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> educationLevelHighSchoolDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationLevelsHighSchool.class, EducationLevelsHighSchool.fullTitle());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EducationLevelsHighSchool educationLevelsHighSchool = context.get(EDUCATION_LEVEL_HIGHSCHOOL);
                if (null != educationLevelsHighSchool) {

                    dql.where(
                            exists(
                                    new DQLSelectBuilder()
                                            .fromEntity(Student.class, "inclS")
                                            .column(property("inclS.id"))
                                            .where(eq(property("inclS", Student.group()), property(alias)))
                                            .where(or(
                                                    eqValue(property("inclS", Student.educationOrgUnit().educationLevelHighSchool()), educationLevelsHighSchool)
                                            ))
                                            .buildQuery()
                            )
                    );

                }
            }
        }
                .where(Group.archival(), false)
                .filter(Group.title())
                .order(Group.title())
                .pageable(true);
    }

}
