package ru.tandemservice.unipractice.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Итоги производственного обучения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ProductionTrainingResultsReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport";
    public static final String ENTITY_NAME = "productionTrainingResultsReport";
    public static final int VERSION_HASH = -1372906332;
    private static IEntityMeta ENTITY_META;

    public static final String P_EDUCATION_YEAR = "educationYear";
    public static final String P_DETACHED_EXT_ORG_UNIT = "detachedExtOrgUnit";

    private String _educationYear;     // Учебный год
    private String _detachedExtOrgUnit;     // Обособить организцаию

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(String educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Обособить организцаию.
     */
    @Length(max=255)
    public String getDetachedExtOrgUnit()
    {
        return _detachedExtOrgUnit;
    }

    /**
     * @param detachedExtOrgUnit Обособить организцаию.
     */
    public void setDetachedExtOrgUnit(String detachedExtOrgUnit)
    {
        dirty(_detachedExtOrgUnit, detachedExtOrgUnit);
        _detachedExtOrgUnit = detachedExtOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ProductionTrainingResultsReportGen)
        {
            setEducationYear(((ProductionTrainingResultsReport)another).getEducationYear());
            setDetachedExtOrgUnit(((ProductionTrainingResultsReport)another).getDetachedExtOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ProductionTrainingResultsReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ProductionTrainingResultsReport.class;
        }

        public T newInstance()
        {
            return (T) new ProductionTrainingResultsReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return obj.getEducationYear();
                case "detachedExtOrgUnit":
                    return obj.getDetachedExtOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationYear":
                    obj.setEducationYear((String) value);
                    return;
                case "detachedExtOrgUnit":
                    obj.setDetachedExtOrgUnit((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                        return true;
                case "detachedExtOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return true;
                case "detachedExtOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationYear":
                    return String.class;
                case "detachedExtOrgUnit":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ProductionTrainingResultsReport> _dslPath = new Path<ProductionTrainingResultsReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ProductionTrainingResultsReport");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport#getEducationYear()
     */
    public static PropertyPath<String> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Обособить организцаию.
     * @see ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport#getDetachedExtOrgUnit()
     */
    public static PropertyPath<String> detachedExtOrgUnit()
    {
        return _dslPath.detachedExtOrgUnit();
    }

    public static class Path<E extends ProductionTrainingResultsReport> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _educationYear;
        private PropertyPath<String> _detachedExtOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport#getEducationYear()
     */
        public PropertyPath<String> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new PropertyPath<String>(ProductionTrainingResultsReportGen.P_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Обособить организцаию.
     * @see ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport#getDetachedExtOrgUnit()
     */
        public PropertyPath<String> detachedExtOrgUnit()
        {
            if(_detachedExtOrgUnit == null )
                _detachedExtOrgUnit = new PropertyPath<String>(ProductionTrainingResultsReportGen.P_DETACHED_EXT_ORG_UNIT, this);
            return _detachedExtOrgUnit;
        }

        public Class getEntityClass()
        {
            return ProductionTrainingResultsReport.class;
        }

        public String getEntityName()
        {
            return "productionTrainingResultsReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
