/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;

/**
 * @author Andrey Avetisov
 * @since 14.10.2014
 */
public interface IStudentPreallocationDao extends INeedPersistenceSupport, ICommonDAO
{
    /**
     * Метод сохраняет отчет «Предварительное распределение студентов»
     *
     * @param practicePreallocation  сохраняемый отчет
     */
    void savePracticePreallocation(StPracticePreallocation practicePreallocation);
}
