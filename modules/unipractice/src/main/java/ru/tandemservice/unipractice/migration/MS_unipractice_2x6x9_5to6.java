/* $Id$ */
package ru.tandemservice.unipractice.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author azhebko
 * @since 11.11.2014
 */
public class MS_unipractice_2x6x9_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.9")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        /* stub. see MS_unipractice_2x7x0_0to1 */
    }
}