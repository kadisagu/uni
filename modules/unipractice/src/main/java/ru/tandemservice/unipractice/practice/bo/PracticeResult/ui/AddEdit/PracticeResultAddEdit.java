package ru.tandemservice.unipractice.practice.bo.PracticeResult.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSubselectType;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unipractice.entity.catalog.PrEmploymentForm;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.practice.bo.PracticeResult.logic.PracticeResultConstants.*;

@Configuration
public class PracticeResultAddEdit extends BusinessComponentManager{

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(
                        selectDS(GRADE_SCALE_DS, gradeScaleDSHandler())
                                .addColumn(EppGradeScale.CATALOG_ITEM_TITLE)
                )
                .addDataSource(
                        selectDS(EMPLOYMENT_FORM_DS, employmentFormDS())
                                .addColumn(PrEmploymentForm.P_SHORT_TITLE))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> gradeScaleDSHandler(){
        EntityComboDataSourceHandler handler =  new EntityComboDataSourceHandler(getName(), SessionMarkGradeValueCatalogItem.class){

            final String F_CONTROL_ACTION = "f";

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                final Long activityElementPartId = context.get(ACTIVITY_ELEMENT_PART_ID);
                super.applyWhereConditions(alias, dql, context);

                DQLSelectBuilder controlActionBuilder = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElementPartFControlAction.class, F_CONTROL_ACTION)
                        .column(property(EppRegistryElementPartFControlAction.gradeScale().id().fromAlias(F_CONTROL_ACTION)));

                controlActionBuilder.where(eqValue(property(EppRegistryElementPartFControlAction.part().id().fromAlias(F_CONTROL_ACTION)), activityElementPartId));
                dql.where(eqSubquery(property(SessionMarkGradeValueCatalogItem.scale().id().fromAlias("e")), DQLSubselectType.any, controlActionBuilder.buildQuery()));
            }
        };
        handler.order(SessionMarkGradeValueCatalogItem.scale().id());
        handler.order(SessionMarkGradeValueCatalogItem.priority());
        return handler;
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> employmentFormDS(){
        return new EntityComboDataSourceHandler(getName(), PrEmploymentForm.class)
                .order(PrEmploymentForm.title());
    }
}
