package ru.tandemservice.unipractice.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unipractice.entity.PrPracticeBaseRequest;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeBaseKind;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Заявка на регистрацию базы практики
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeBaseRequestGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.PrPracticeBaseRequest";
    public static final String ENTITY_NAME = "prPracticeBaseRequest";
    public static final int VERSION_HASH = 1520893952;
    private static IEntityMeta ENTITY_META;

    public static final String L_KIND = "kind";

    private PrPracticeBaseKind _kind;     // Виды баз прохождения практики

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Виды баз прохождения практики. Свойство не может быть null.
     */
    @NotNull
    public PrPracticeBaseKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Виды баз прохождения практики. Свойство не может быть null.
     */
    public void setKind(PrPracticeBaseKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PrPracticeBaseRequestGen)
        {
            setKind(((PrPracticeBaseRequest)another).getKind());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeBaseRequestGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeBaseRequest.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("PrPracticeBaseRequest is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "kind":
                    return obj.getKind();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "kind":
                    obj.setKind((PrPracticeBaseKind) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "kind":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "kind":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "kind":
                    return PrPracticeBaseKind.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeBaseRequest> _dslPath = new Path<PrPracticeBaseRequest>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeBaseRequest");
    }
            

    /**
     * @return Виды баз прохождения практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequest#getKind()
     */
    public static PrPracticeBaseKind.Path<PrPracticeBaseKind> kind()
    {
        return _dslPath.kind();
    }

    public static class Path<E extends PrPracticeBaseRequest> extends EntityPath<E>
    {
        private PrPracticeBaseKind.Path<PrPracticeBaseKind> _kind;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Виды баз прохождения практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseRequest#getKind()
     */
        public PrPracticeBaseKind.Path<PrPracticeBaseKind> kind()
        {
            if(_kind == null )
                _kind = new PrPracticeBaseKind.Path<PrPracticeBaseKind>(L_KIND, this);
            return _kind;
        }

        public Class getEntityClass()
        {
            return PrPracticeBaseRequest.class;
        }

        public String getEntityName()
        {
            return "prPracticeBaseRequest";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
