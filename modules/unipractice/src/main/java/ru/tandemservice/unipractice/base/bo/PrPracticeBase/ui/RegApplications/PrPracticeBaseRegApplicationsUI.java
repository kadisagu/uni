/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.RegApplications;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.PrPracticeBaseManager;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseRequestWrapper;

import java.util.Collection;
import java.util.List;

/**
 * @author nvankov
 * @since 10/22/14
 */
public class PrPracticeBaseRegApplicationsUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(PrPracticeBaseRegApplications.PR_PRACTICE_REQUESTS_DS.equals(dataSource.getName()))
            dataSource.put("prPracticeBaseKindId", _uiSettings.getEntityId("prPracticeBaseKindFilter"));

    }

    public void onClickAdd()
    {
        Collection<IEntity> selectedObjects = ((BaseSearchListDataSource) _uiConfig.getDataSource(PrPracticeBaseRegApplications.PR_PRACTICE_REQUESTS_DS)).getOptionColumnSelectedObjects("select");

        List<PrPracticeBaseRequestWrapper> wrapperList = Lists.newArrayList();

        for(IEntity entity : selectedObjects)
        {
            if(entity instanceof PrPracticeBaseRequestWrapper)
            {
                wrapperList.add((PrPracticeBaseRequestWrapper) entity);
            }
            else
                throw new IllegalStateException();
        }

        PrPracticeBaseManager.instance().prPracticeBaseDao().createPracticeBaseByRequests(wrapperList);
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public boolean isSelectRequestDisabled()
    {
        PrPracticeBaseRequestWrapper wrap = ((BaseSearchListDataSource) _uiConfig.getDataSource(PrPracticeBaseRegApplications.PR_PRACTICE_REQUESTS_DS)).getCurrent();
        return wrap.isBaseExist();
    }

    public boolean isDownloadGuaranteeLetterDisabled()
    {
        PrPracticeBaseRequestWrapper wrap = ((BaseSearchListDataSource) _uiConfig.getDataSource(PrPracticeBaseRegApplications.PR_PRACTICE_REQUESTS_DS)).getCurrent();
        return !wrap.isHaveGuaranteeLetter();
    }

    public void onClickDownloadGuaranteeLetter()
    {
        PrPracticeBaseRequestWrapper wrap =  ((BaseSearchListDataSource) _uiConfig.getDataSource(PrPracticeBaseRegApplications.PR_PRACTICE_REQUESTS_DS)).getRecordById(getListenerParameterAsLong());
        if (wrap.getRequestExt().getGuaranteeLetterCopy().getContent() == null)
            throw new ApplicationException("Файл гарантийного письма пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(wrap.getRequestExt().getGuaranteeLetterCopy()), false);
    }
}
