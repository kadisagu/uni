package ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic;

import com.google.common.collect.Lists;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ExternalOrgUnitManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unipractice.entity.*;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Transactional
public class PrPracticeBaseDao extends UniBaseDao implements IPrPracticeBaseDao {

    @Override
    public void sendToArchive(Long prPracticeBaseId) {
        PrPracticeBase base = getNotNull(prPracticeBaseId);
        base.setArchival(true);
        saveOrUpdate(base);
    }

    @Override
    public void sendFromArchive(Long prPracticeBaseId) {
        PrPracticeBase base = getNotNull(prPracticeBaseId);
        base.setArchival(false);
        saveOrUpdate(base);
    }

    @Override
    public void createPracticeBaseByRequests(List<PrPracticeBaseRequestWrapper> wrapperList)
    {
        for(PrPracticeBaseRequestWrapper wrapper : wrapperList)
        {
            if(wrapper.isRequestInt())
            {
                if(existsEntity(new DQLSelectBuilder().fromEntity(PrPracticeBaseInt.class, "pb")
                        .where(or(
                                eq(property("pb", PrPracticeBaseInt.orgUnit()), value(wrapper.getRequestInt().getOrgUnit())))).buildQuery()))
                {
                    UserContext.getInstance().getErrorCollector().addError("Для подразделения " + wrapper.getOrgUnitTitle() + " уже зарегистрирована база прохождения практики");
                }
                else
                {
                    PrPracticeBaseInt prPracticeBase = new PrPracticeBaseInt();
                    prPracticeBase.setKind(wrapper.getRequestInt().getKind());
                    prPracticeBase.setOrgUnit(wrapper.getRequestInt().getOrgUnit());
                    prPracticeBase.setArchival(false);
                    save(prPracticeBase);
                    delete(wrapper.getRequestInt().getId());
                }
            }
            else
            {
                if(wrapper.getRequestExt().getOrgUnit() != null && existsEntity(new DQLSelectBuilder().fromEntity(PrPracticeBaseExt.class, "pb")
                        .where(or(
                                eq(property("pb", PrPracticeBaseExt.orgUnit()), value(wrapper.getRequestExt().getOrgUnit())))).buildQuery()))
                {
                    UserContext.getInstance().getErrorCollector().addError("Для подразделения " + wrapper.getOrgUnitTitle() + " уже зарегистрирована база прохождения практики");
                }
                else
                {
                    PrPracticeBaseExt prPracticeBase = new PrPracticeBaseExt();
                    prPracticeBase.setKind(wrapper.getRequestExt().getKind());
                    if(wrapper.getRequestExt().getOrgUnit() != null)
                    {
                        prPracticeBase.setOrgUnit(wrapper.getRequestExt().getOrgUnit());
                    }
                    else
                    {
                        ExternalOrgUnit externalOrgUnit = new ExternalOrgUnit();
                        externalOrgUnit.setTitle(wrapper.getRequestExt().getOrgUnitTitle());
                        externalOrgUnit.setShortTitle(wrapper.getRequestExt().getOrgUnitShortTitle());
                        externalOrgUnit.setLegalForm(wrapper.getRequestExt().getOrgUnitLegalForm());                        ;
                        prPracticeBase.setOrgUnit(ExternalOrgUnitManager.instance().dao().saveExternalOrgUnit(externalOrgUnit, null, null));
                    }
                    prPracticeBase.setArchival(false);
                    save(prPracticeBase);
                    delete(wrapper.getRequestExt().getId());
                }
            }
        }
    }

}
