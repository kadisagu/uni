package ru.tandemservice.unipractice.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.entity.PrPracticeBaseExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * База прохождения практики (внешняя организация)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeBaseExtGen extends PrPracticeBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.PrPracticeBaseExt";
    public static final String ENTITY_NAME = "prPracticeBaseExt";
    public static final int VERSION_HASH = 1039808902;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";

    private ExternalOrgUnit _orgUnit;     // Организация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Организация. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ExternalOrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Организация. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrgUnit(ExternalOrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PrPracticeBaseExtGen)
        {
            setOrgUnit(((PrPracticeBaseExt)another).getOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeBaseExtGen> extends PrPracticeBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeBaseExt.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeBaseExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((ExternalOrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return ExternalOrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeBaseExt> _dslPath = new Path<PrPracticeBaseExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeBaseExt");
    }
            

    /**
     * @return Организация. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseExt#getOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    public static class Path<E extends PrPracticeBaseExt> extends PrPracticeBase.Path<E>
    {
        private ExternalOrgUnit.Path<ExternalOrgUnit> _orgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Организация. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.entity.PrPracticeBaseExt#getOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

        public Class getEntityClass()
        {
            return PrPracticeBaseExt.class;
        }

        public String getEntityName()
        {
            return "prPracticeBaseExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
