/* $Id: Controller.java 194 2014-09-08 08:24:36Z nvankov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.VisaList;

/**
 * @author vip_delete
 * @since 28.07.2009
 */
public class Controller extends ru.tandemservice.unimv.component.visa.VisaList.Controller
{
}
