package ru.tandemservice.unipractice.practice.bo.PracticeResult.logic;

import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unipractice.entity.PrPracticeResult;

import java.io.IOException;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class PracticeResultDao extends UniBaseDao implements IPracticeResultDao {
    private static final String ALIAS = "prResult";

    @Override
    public PrPracticeResult getPracticeResultByAssignmentId(Long prPracticeAssignmentId) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PrPracticeResult.class, ALIAS)
                .column(ALIAS);
        builder.where(eq(
                property(PrPracticeResult.practiceAssignment().id().fromAlias(ALIAS)),
                value(prPracticeAssignmentId)));
        return builder.createStatement(getSession()).uniqueResult();
    }

    @Override
    public void deleteScanCopy(PrPracticeResult prPracticeResult) {
        Long scanCopyId = prPracticeResult.getScanCopy().getId();
        prPracticeResult.setScanCopy(null);
        update(prPracticeResult);
        delete(scanCopyId);
    }

    @Override
    public void saveScanCopy(PrPracticeResult practiceResult, IUploadFile scanCopyUploadFile) {
        DatabaseFile scanCopy = saveScanCopyDatabaseFile(scanCopyUploadFile);
        practiceResult.setScanCopy(scanCopy);
        saveOrUpdate(practiceResult);
    }

    private DatabaseFile saveScanCopyDatabaseFile(IUploadFile file){
        DatabaseFile scanCopy = new DatabaseFile();
        try {
            scanCopy.setContent(IOUtils.toByteArray(file.getStream()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        scanCopy.setContentType(CommonBaseUtil.getContentType(file));
        scanCopy.setFilename(file.getFileName());
        saveOrUpdate(scanCopy);
        return scanCopy;
    }
}
