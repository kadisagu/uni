package ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic;

public class PracticeParagraphConstants {

    public static final String PARAGRAPH_ID = "paragraphId";
    public static final String ORDER_ID = "orderId";
    public static final String ORDER = "order";

    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String ORDER_TO_CHANGE_DS = "orderToChangeDS";
    public static final String PARAGRAPH_LIST_DS = "prParagraphListDS";
    public static final String PRACTICE_EXTRACT_LIST_DS = "practiceExtractListDS";
    public static final String PRACTICE_ASSIGNMENT_DS = "prPracticeAssignmentDS";
    public static final String PRACTICE_EXTRACT_DS = "prPracticeExtractDS";

    public static final String RESPONSIBLE_ORG_UNIT_DS = "responsibleOrgUnitDS";
    public static final String START_DATE_DS = "startDateDS";
    public static final String END_DATE_DS = "endDateDS";

    public static final String FIO_C = "fio";
    public static final String FORMATIVE_ORG_UNIT_C = "formativeOrgUnit";
    public static final String GROUP_C = "group";
    public static final String PRACTICE_BASE_C = "practiceBase";
    public static final String RESPONSIBLE_ORG_UNIT_C = "responsibleOrgUnit";

    public static final String EDU_YEAR = "eduYear";
    public static final String ORDER_TO_CHANGE_F = "orderToChange";
    public static final String RESPONSIBLE_ORG_UNIT_F = "responsibleOrgUnit";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String FORMATIVE_ORG_UNIT_F = "formativeOrgUnit";
    public static final String PRACTICE_CODE = "practiceCode";
    public static final String PRACTICE_ORDER_TYPE = "practiceOrderType";
    public static final String EDIT_DELETE_PARAGRAPH_DISABLED = "editDeleteParagraphDisabled";
}
