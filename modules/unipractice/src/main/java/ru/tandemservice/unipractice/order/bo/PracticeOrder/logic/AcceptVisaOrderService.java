/* $Id: AcceptVisaOrderService.java 194 2014-09-08 08:24:36Z nvankov $ */
package ru.tandemservice.unipractice.order.bo.PracticeOrder.logic;

import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;

/**
 * @author oleyba
 * @since 7/10/14
 */
public class AcceptVisaOrderService extends ru.tandemservice.unimove.service.AcceptVisaOrderService
{
    @Override
    protected void doExecute() throws Exception
    {
        PracticeOrderManager.instance().practiceOrderDao().doExecuteAcceptVisaOrderService(getOrder());
    }
}
