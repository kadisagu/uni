package ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Выводит блок "Список студентов" на карточке параграфа приказа "О направлении студентов на преддипломную/производственную практику"
 */
public class ParagraphExtractsDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    private static final String BASE = "base";

    public ParagraphExtractsDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PrPracticeExtract.class, BASE).column(BASE);

        Long paragraphId = context.get(PracticeParagraphConstants.PARAGRAPH_ID);
        builder.where(eq(
                property(PrPracticeExtract.paragraph().id().fromAlias(BASE)),
                value(paragraphId)
        ));

        IDQLExpression order = property(BASE, PrPracticeExtract.number());
        builder.order(order);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(false).build();
    }
}
