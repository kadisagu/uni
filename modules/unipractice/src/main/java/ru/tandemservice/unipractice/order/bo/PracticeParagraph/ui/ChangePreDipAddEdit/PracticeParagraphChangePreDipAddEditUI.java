package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ChangePreDipAddEdit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.AbstractAddEdit.AbstractPracticeParagraphChangeAddEditUI;

import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Input({
    @Bind(key = ORDER_ID, binding = ORDER_ID),
    @Bind(key = PARAGRAPH_ID, binding = PARAGRAPH_ID)
})
public class PracticeParagraphChangePreDipAddEditUI extends AbstractPracticeParagraphChangeAddEditUI {

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        dataSource.put(PRACTICE_ORDER_TYPE, PrPracticeOrderTypeCodes.PREDDIPLOM);
    }
}