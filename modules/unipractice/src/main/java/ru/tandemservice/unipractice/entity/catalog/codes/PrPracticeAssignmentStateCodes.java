package ru.tandemservice.unipractice.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние направления на практику"
 * Имя сущности : prPracticeAssignmentState
 * Файл data.xml : unipractice.catalog.data.xml
 */
public interface PrPracticeAssignmentStateCodes
{
    /** Константа кода (code) элемента : Формируется (title) */
    String FORMING = "1";
    /** Константа кода (code) элемента : На согласовании (title) */
    String ACCEPTABLE = "2";
    /** Константа кода (code) элемента : Согласовано (title) */
    String ACCEPTED = "3";
    /** Константа кода (code) элемента : На проверке результатов (title) */
    String FOR_APPROVAL = "4";
    /** Константа кода (code) элемента : Закрыто (title) */
    String FINISHED = "5";

    Set<String> CODES = ImmutableSet.of(FORMING, ACCEPTABLE, ACCEPTED, FOR_APPROVAL, FINISHED);
}
