package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.AbstractAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.IPracticeOrderDao;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.wrapper.DateWrapper;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import java.util.*;

import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

public abstract class AbstractPracticeParagraphAddEditUI extends UIPresenter {
    private Long orderId;
    private PrPracticeOrder practiceOrder;
    protected Long paragraphId;
    private boolean paragraphNotValid;
    private boolean selectedColumnIsSet;
    private OrgUnit _respOrgUnit;
    private DateWrapper _startDate;
    private DateWrapper _endDate;

    @Override
    public void onComponentRefresh() {
        selectedColumnIsSet = false;
        practiceOrder = DataAccessServices.dao().get(orderId);
        if (paragraphId != null) {
            IPracticeOrderDao orderDao = PracticeOrderManager.instance().practiceOrderDao();
            List<PrPracticeAssignment> assignments = orderDao.getAssignmentsByParagraphId(paragraphId);
            paragraphNotValid = validateParagraph(assignments).hasErrors();
            _respOrgUnit = assignments.get(0).getPractice().getRegistryElementPart().getRegistryElement().getOwner();
            _startDate = new DateWrapper(CoreDateUtils.getDayFirstTimeMoment(assignments.get(0).getDateStart()));
            _endDate = new DateWrapper(CoreDateUtils.getDayFirstTimeMoment(assignments.get(0).getDateEnd()));
            _uiConfig.getDataSource(PRACTICE_ASSIGNMENT_DS).put(PARAGRAPH_ID, paragraphId);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        Long respOUId = _respOrgUnit != null ? _respOrgUnit.getId() : null;
        Date startDate = _startDate != null ? _startDate.getDate() : null;
        Date endDate = _endDate != null ? _endDate.getDate() : null;

        dataSource.put(EDU_YEAR, practiceOrder.getEducationYear().getId());
        dataSource.put(PARAGRAPH_ID, paragraphId);

        if(!RESPONSIBLE_ORG_UNIT_DS.equals(dataSource.getName()))
            dataSource.put(RESPONSIBLE_ORG_UNIT_F, respOUId);

        if(PRACTICE_ASSIGNMENT_DS.equals(dataSource.getName()))
        {
            dataSource.put(START_DATE, startDate);
            dataSource.put(END_DATE, endDate);
        }

        if(END_DATE_DS.equals(dataSource.getName()))
        {
            dataSource.put(START_DATE, startDate);
        }
    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (!selectedColumnIsSet && PRACTICE_ASSIGNMENT_DS.equals(dataSource.getName())) {
            final CheckboxColumn checkBoxColumn = (CheckboxColumn)((BaseSearchListDataSource) dataSource).getLegacyDataSource().getColumn("checkbox");
            IPracticeOrderDao orderDao = PracticeOrderManager.instance().practiceOrderDao();
            List<PrPracticeAssignment> assignments = orderDao.getAssignmentsByParagraphId(paragraphId);
            Collection<IEntity> selectedEntities = new ArrayList<>(assignments.size());
            for (PrPracticeAssignment assignment : assignments) {
                selectedEntities.add(assignment);
            }
            checkBoxColumn.setSelectedObjects(selectedEntities);
            selectedColumnIsSet = true;
        }
    }

    public void onClickApply() {
        //validate
        if (validate().hasErrors()) {
            return;
        }

        IPracticeOrderDao orderDao = PracticeOrderManager.instance().practiceOrderDao();

        Collection<IEntity> selectedMarkList = getConfig().<BaseSearchListDataSource>getDataSource(PRACTICE_ASSIGNMENT_DS)
                .getOptionColumnSelectedObjects("checkbox");
        List<PrPracticeAssignment> assignments = new ArrayList<>(selectedMarkList.size());
        for (IEntity entity : selectedMarkList) {
            assignments.add((PrPracticeAssignment) entity);
        }

        if (paragraphId != null) {
            //edit existing paragraph
            orderDao.editParagraph(paragraphId, assignments);
        } else {
            //create paragraph and add assignments
            orderDao.createParagraph(orderId, assignments);
        }
        deactivate();
    }

    protected ErrorCollector validateParagraph(List<PrPracticeAssignment> assignments) {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        Set<Long> responsibleOrgUnits = new HashSet<>();
        Set<Date> startDates = new HashSet<>();
        Set<Date> endDates = new HashSet<>();
        for (PrPracticeAssignment assignment : assignments) {
            responsibleOrgUnits.add(assignment.getPractice().getRegistryElementPart().getRegistryElement().getOwner().getId());
            startDates.add(assignment.getDateStart());
            endDates.add(assignment.getDateEnd());
        }
        if (responsibleOrgUnits.size() > 1) {
            errorCollector.add("В параграф включены студенты с разных выпускающих подразделений, корректная работа с формой невозможна. Исключите студентов из параграфа на карточке параграфа.");
        }
        if (startDates.size() > 1 || endDates.size() > 1) {
            errorCollector.add("В параграф включены студенты с разными периодами прохождения практики, корректная работа с формой невозможна. Исключите студентов из параграфа на карточке параграфа.");
        }
        return errorCollector;
    }

    protected ErrorCollector validate()
    {
        IPracticeOrderDao orderDao = PracticeOrderManager.instance().practiceOrderDao();
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();

        //если в параграфе нет ни одной выписки: «Параграф не может быть пустым.»
        Collection<IEntity> selectedMarkList = getConfig().<BaseSearchListDataSource>getDataSource(PRACTICE_ASSIGNMENT_DS)
                .getOptionColumnSelectedObjects("checkbox");
        Collection<Long> assignmentIds = CommonDAO.ids(selectedMarkList);
        if ( assignmentIds.isEmpty() ) {
            errorCollector.add("Параграф не может быть пустым.");
        }

        //если состояние приказа отлично от «Формируется»: "Приказ уже не в состоянии формирования. Изменение приказа невозможно."
        if ( !orderDao.isOrderInFormingState(orderId) ) {
            errorCollector.add("Приказ уже не в состоянии формирования. Изменение приказа невозможно.");
        }

        //если для выбранного направления на практику уже есть выписка о направлении на преддипломную практику, и она не в этом параграфе:
        // "Студент «<ФИО>» уже в приказе <номер приказа, если задан>.");
        Map<String, String> duplicates = orderDao.getExtractsDuplicates(paragraphId, assignmentIds);
        if ( !duplicates.isEmpty() ) {
            for (Map.Entry<String, String> duplicate : duplicates.entrySet()) {
                errorCollector.add(String.format("Студент «%s» уже в приказе %s.", duplicate.getKey(), duplicate.getValue()));
            }
        }

        return errorCollector;
    }

    @SuppressWarnings("unused")
    public boolean isDisableFilters() {
        return paragraphId != null;
    }

    @SuppressWarnings("unused")
    public boolean isParagraphNotValid() {
        return paragraphNotValid;
    }

    // getters and setters
    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getParagraphId() {
        return paragraphId;
    }

    public void setParagraphId(Long paragraphId) {
        this.paragraphId = paragraphId;
    }

    public OrgUnit getRespOrgUnit()
    {
        return _respOrgUnit;
    }

    public void setRespOrgUnit(OrgUnit respOrgUnit)
    {
        _respOrgUnit = respOrgUnit;
    }

    public DateWrapper getStartDate()
    {
        return _startDate;
    }

    public void setStartDate(DateWrapper startDate)
    {
        _startDate = startDate;
    }

    public DateWrapper getEndDate()
    {
        return _endDate;
    }

    public void setEndDate(DateWrapper endDate)
    {
        _endDate = endDate;
    }
    // !getters and setters

}
