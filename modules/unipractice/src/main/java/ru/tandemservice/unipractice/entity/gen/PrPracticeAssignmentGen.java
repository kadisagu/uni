package ru.tandemservice.unipractice.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.PrPracticeBase;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeAssignmentState;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeAssignmentGen extends EntityBase
 implements INaturalIdentifiable<PrPracticeAssignmentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.PrPracticeAssignment";
    public static final String ENTITY_NAME = "prPracticeAssignment";
    public static final int VERSION_HASH = -2011956685;
    private static IEntityMeta ENTITY_META;

    public static final String L_PRACTICE = "practice";
    public static final String L_PRACTICE_BASE = "practiceBase";
    public static final String L_TUTOR = "tutor";
    public static final String P_DATE_START = "dateStart";
    public static final String P_DATE_END = "dateEnd";
    public static final String L_STATE = "state";
    public static final String L_GUARANTEE_COPY = "guaranteeCopy";
    public static final String P_GUARANTEE_NUMBER = "guaranteeNumber";
    public static final String P_GUARANTEE_DATE = "guaranteeDate";
    public static final String P_COMMENT = "comment";

    private EppStudentWorkPlanElement _practice;     // Практика из РУП студента
    private PrPracticeBase _practiceBase;     // База практики
    private PpsEntry _tutor;     // Руководитель
    private Date _dateStart;     // Дата начала практики
    private Date _dateEnd;     // Дата окончания практики
    private PrPracticeAssignmentState _state;     // Состояние
    private DatabaseFile _guaranteeCopy;     // Скан-копия гарантийного письма
    private String _guaranteeNumber;     // Номер гарантийного письма
    private Date _guaranteeDate;     // Дата гарантийного письма
    private String _comment;     // Примечание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Практика из РУП студента. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppStudentWorkPlanElement getPractice()
    {
        return _practice;
    }

    /**
     * @param practice Практика из РУП студента. Свойство не может быть null и должно быть уникальным.
     */
    public void setPractice(EppStudentWorkPlanElement practice)
    {
        dirty(_practice, practice);
        _practice = practice;
    }

    /**
     * @return База практики. Свойство не может быть null.
     */
    @NotNull
    public PrPracticeBase getPracticeBase()
    {
        return _practiceBase;
    }

    /**
     * @param practiceBase База практики. Свойство не может быть null.
     */
    public void setPracticeBase(PrPracticeBase practiceBase)
    {
        dirty(_practiceBase, practiceBase);
        _practiceBase = practiceBase;
    }

    /**
     * @return Руководитель. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getTutor()
    {
        return _tutor;
    }

    /**
     * @param tutor Руководитель. Свойство не может быть null.
     */
    public void setTutor(PpsEntry tutor)
    {
        dirty(_tutor, tutor);
        _tutor = tutor;
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     */
    @NotNull
    public Date getDateStart()
    {
        return _dateStart;
    }

    /**
     * @param dateStart Дата начала практики. Свойство не может быть null.
     */
    public void setDateStart(Date dateStart)
    {
        dirty(_dateStart, dateStart);
        _dateStart = dateStart;
    }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     */
    @NotNull
    public Date getDateEnd()
    {
        return _dateEnd;
    }

    /**
     * @param dateEnd Дата окончания практики. Свойство не может быть null.
     */
    public void setDateEnd(Date dateEnd)
    {
        dirty(_dateEnd, dateEnd);
        _dateEnd = dateEnd;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public PrPracticeAssignmentState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(PrPracticeAssignmentState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Скан-копия гарантийного письма.
     */
    public DatabaseFile getGuaranteeCopy()
    {
        return _guaranteeCopy;
    }

    /**
     * @param guaranteeCopy Скан-копия гарантийного письма.
     */
    public void setGuaranteeCopy(DatabaseFile guaranteeCopy)
    {
        dirty(_guaranteeCopy, guaranteeCopy);
        _guaranteeCopy = guaranteeCopy;
    }

    /**
     * @return Номер гарантийного письма.
     */
    @Length(max=255)
    public String getGuaranteeNumber()
    {
        return _guaranteeNumber;
    }

    /**
     * @param guaranteeNumber Номер гарантийного письма.
     */
    public void setGuaranteeNumber(String guaranteeNumber)
    {
        dirty(_guaranteeNumber, guaranteeNumber);
        _guaranteeNumber = guaranteeNumber;
    }

    /**
     * @return Дата гарантийного письма.
     */
    public Date getGuaranteeDate()
    {
        return _guaranteeDate;
    }

    /**
     * @param guaranteeDate Дата гарантийного письма.
     */
    public void setGuaranteeDate(Date guaranteeDate)
    {
        dirty(_guaranteeDate, guaranteeDate);
        _guaranteeDate = guaranteeDate;
    }

    /**
     * @return Примечание.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Примечание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PrPracticeAssignmentGen)
        {
            if (withNaturalIdProperties)
            {
                setPractice(((PrPracticeAssignment)another).getPractice());
            }
            setPracticeBase(((PrPracticeAssignment)another).getPracticeBase());
            setTutor(((PrPracticeAssignment)another).getTutor());
            setDateStart(((PrPracticeAssignment)another).getDateStart());
            setDateEnd(((PrPracticeAssignment)another).getDateEnd());
            setState(((PrPracticeAssignment)another).getState());
            setGuaranteeCopy(((PrPracticeAssignment)another).getGuaranteeCopy());
            setGuaranteeNumber(((PrPracticeAssignment)another).getGuaranteeNumber());
            setGuaranteeDate(((PrPracticeAssignment)another).getGuaranteeDate());
            setComment(((PrPracticeAssignment)another).getComment());
        }
    }

    public INaturalId<PrPracticeAssignmentGen> getNaturalId()
    {
        return new NaturalId(getPractice());
    }

    public static class NaturalId extends NaturalIdBase<PrPracticeAssignmentGen>
    {
        private static final String PROXY_NAME = "PrPracticeAssignmentNaturalProxy";

        private Long _practice;

        public NaturalId()
        {}

        public NaturalId(EppStudentWorkPlanElement practice)
        {
            _practice = ((IEntity) practice).getId();
        }

        public Long getPractice()
        {
            return _practice;
        }

        public void setPractice(Long practice)
        {
            _practice = practice;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PrPracticeAssignmentGen.NaturalId) ) return false;

            PrPracticeAssignmentGen.NaturalId that = (NaturalId) o;

            if( !equals(getPractice(), that.getPractice()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPractice());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPractice());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeAssignmentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeAssignment.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeAssignment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "practice":
                    return obj.getPractice();
                case "practiceBase":
                    return obj.getPracticeBase();
                case "tutor":
                    return obj.getTutor();
                case "dateStart":
                    return obj.getDateStart();
                case "dateEnd":
                    return obj.getDateEnd();
                case "state":
                    return obj.getState();
                case "guaranteeCopy":
                    return obj.getGuaranteeCopy();
                case "guaranteeNumber":
                    return obj.getGuaranteeNumber();
                case "guaranteeDate":
                    return obj.getGuaranteeDate();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "practice":
                    obj.setPractice((EppStudentWorkPlanElement) value);
                    return;
                case "practiceBase":
                    obj.setPracticeBase((PrPracticeBase) value);
                    return;
                case "tutor":
                    obj.setTutor((PpsEntry) value);
                    return;
                case "dateStart":
                    obj.setDateStart((Date) value);
                    return;
                case "dateEnd":
                    obj.setDateEnd((Date) value);
                    return;
                case "state":
                    obj.setState((PrPracticeAssignmentState) value);
                    return;
                case "guaranteeCopy":
                    obj.setGuaranteeCopy((DatabaseFile) value);
                    return;
                case "guaranteeNumber":
                    obj.setGuaranteeNumber((String) value);
                    return;
                case "guaranteeDate":
                    obj.setGuaranteeDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "practice":
                        return true;
                case "practiceBase":
                        return true;
                case "tutor":
                        return true;
                case "dateStart":
                        return true;
                case "dateEnd":
                        return true;
                case "state":
                        return true;
                case "guaranteeCopy":
                        return true;
                case "guaranteeNumber":
                        return true;
                case "guaranteeDate":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "practice":
                    return true;
                case "practiceBase":
                    return true;
                case "tutor":
                    return true;
                case "dateStart":
                    return true;
                case "dateEnd":
                    return true;
                case "state":
                    return true;
                case "guaranteeCopy":
                    return true;
                case "guaranteeNumber":
                    return true;
                case "guaranteeDate":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "practice":
                    return EppStudentWorkPlanElement.class;
                case "practiceBase":
                    return PrPracticeBase.class;
                case "tutor":
                    return PpsEntry.class;
                case "dateStart":
                    return Date.class;
                case "dateEnd":
                    return Date.class;
                case "state":
                    return PrPracticeAssignmentState.class;
                case "guaranteeCopy":
                    return DatabaseFile.class;
                case "guaranteeNumber":
                    return String.class;
                case "guaranteeDate":
                    return Date.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PrPracticeAssignment> _dslPath = new Path<PrPracticeAssignment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeAssignment");
    }
            

    /**
     * @return Практика из РУП студента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getPractice()
     */
    public static EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> practice()
    {
        return _dslPath.practice();
    }

    /**
     * @return База практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getPracticeBase()
     */
    public static PrPracticeBase.Path<PrPracticeBase> practiceBase()
    {
        return _dslPath.practiceBase();
    }

    /**
     * @return Руководитель. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getTutor()
     */
    public static PpsEntry.Path<PpsEntry> tutor()
    {
        return _dslPath.tutor();
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getDateStart()
     */
    public static PropertyPath<Date> dateStart()
    {
        return _dslPath.dateStart();
    }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getDateEnd()
     */
    public static PropertyPath<Date> dateEnd()
    {
        return _dslPath.dateEnd();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getState()
     */
    public static PrPracticeAssignmentState.Path<PrPracticeAssignmentState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Скан-копия гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getGuaranteeCopy()
     */
    public static DatabaseFile.Path<DatabaseFile> guaranteeCopy()
    {
        return _dslPath.guaranteeCopy();
    }

    /**
     * @return Номер гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getGuaranteeNumber()
     */
    public static PropertyPath<String> guaranteeNumber()
    {
        return _dslPath.guaranteeNumber();
    }

    /**
     * @return Дата гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getGuaranteeDate()
     */
    public static PropertyPath<Date> guaranteeDate()
    {
        return _dslPath.guaranteeDate();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends PrPracticeAssignment> extends EntityPath<E>
    {
        private EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> _practice;
        private PrPracticeBase.Path<PrPracticeBase> _practiceBase;
        private PpsEntry.Path<PpsEntry> _tutor;
        private PropertyPath<Date> _dateStart;
        private PropertyPath<Date> _dateEnd;
        private PrPracticeAssignmentState.Path<PrPracticeAssignmentState> _state;
        private DatabaseFile.Path<DatabaseFile> _guaranteeCopy;
        private PropertyPath<String> _guaranteeNumber;
        private PropertyPath<Date> _guaranteeDate;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Практика из РУП студента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getPractice()
     */
        public EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement> practice()
        {
            if(_practice == null )
                _practice = new EppStudentWorkPlanElement.Path<EppStudentWorkPlanElement>(L_PRACTICE, this);
            return _practice;
        }

    /**
     * @return База практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getPracticeBase()
     */
        public PrPracticeBase.Path<PrPracticeBase> practiceBase()
        {
            if(_practiceBase == null )
                _practiceBase = new PrPracticeBase.Path<PrPracticeBase>(L_PRACTICE_BASE, this);
            return _practiceBase;
        }

    /**
     * @return Руководитель. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getTutor()
     */
        public PpsEntry.Path<PpsEntry> tutor()
        {
            if(_tutor == null )
                _tutor = new PpsEntry.Path<PpsEntry>(L_TUTOR, this);
            return _tutor;
        }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getDateStart()
     */
        public PropertyPath<Date> dateStart()
        {
            if(_dateStart == null )
                _dateStart = new PropertyPath<Date>(PrPracticeAssignmentGen.P_DATE_START, this);
            return _dateStart;
        }

    /**
     * @return Дата окончания практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getDateEnd()
     */
        public PropertyPath<Date> dateEnd()
        {
            if(_dateEnd == null )
                _dateEnd = new PropertyPath<Date>(PrPracticeAssignmentGen.P_DATE_END, this);
            return _dateEnd;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getState()
     */
        public PrPracticeAssignmentState.Path<PrPracticeAssignmentState> state()
        {
            if(_state == null )
                _state = new PrPracticeAssignmentState.Path<PrPracticeAssignmentState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Скан-копия гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getGuaranteeCopy()
     */
        public DatabaseFile.Path<DatabaseFile> guaranteeCopy()
        {
            if(_guaranteeCopy == null )
                _guaranteeCopy = new DatabaseFile.Path<DatabaseFile>(L_GUARANTEE_COPY, this);
            return _guaranteeCopy;
        }

    /**
     * @return Номер гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getGuaranteeNumber()
     */
        public PropertyPath<String> guaranteeNumber()
        {
            if(_guaranteeNumber == null )
                _guaranteeNumber = new PropertyPath<String>(PrPracticeAssignmentGen.P_GUARANTEE_NUMBER, this);
            return _guaranteeNumber;
        }

    /**
     * @return Дата гарантийного письма.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getGuaranteeDate()
     */
        public PropertyPath<Date> guaranteeDate()
        {
            if(_guaranteeDate == null )
                _guaranteeDate = new PropertyPath<Date>(PrPracticeAssignmentGen.P_GUARANTEE_DATE, this);
            return _guaranteeDate;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unipractice.entity.PrPracticeAssignment#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(PrPracticeAssignmentGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return PrPracticeAssignment.class;
        }

        public String getEntityName()
        {
            return "prPracticeAssignment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
