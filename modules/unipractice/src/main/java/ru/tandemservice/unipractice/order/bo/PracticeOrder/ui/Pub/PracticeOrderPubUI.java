package ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderPrintForm;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderPrintFormCodes;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.AddEdit.PracticeOrderAddEdit;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Map;

import static ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeOrderTypeCodes.*;
import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderConstants.PRACTICE_ORDER_ID;
import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.util.ParagraphUtils.getParagraphEditComponentByOrderType;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.ORDER_ID;

@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "prPracticeOrder.id", required = true)
})
public class PracticeOrderPubUI extends UIPresenter implements IVisaOwnerModel {
    private CommonPostfixPermissionModel _secModel;
    private PrPracticeOrder prPracticeOrder = new PrPracticeOrder();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private OrderStates _state;

    private Long _visingStatus;

    private String _selectedTab;

    @Override
    public void onComponentRefresh() {
        setSecModel(new CommonPostfixPermissionModel(getSecPostfix()));
        setPrPracticeOrder(DataAccessServices.dao().getNotNull(PrPracticeOrder.class, getPrPracticeOrder().getId()));
        setState(getPrPracticeOrder().getState());
        setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(getPrPracticeOrder()));
    }

    public void onClickSendToCoordination() {
        _uiSupport.setRefreshScheduled(true);
        IPrincipalContext principalContext = getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        PracticeOrderManager.instance().practiceOrderDao().doSendToCoordination(Arrays.asList(getPrPracticeOrder().getId()), (IPersistentPersonable) principalContext);
    }

    public void addParagraph() {
        Class<? extends BusinessComponentManager> editClass = getParagraphEditComponentByOrderType(prPracticeOrder.getType().getCode());
        _uiActivation.asRegion(editClass)
                .parameter(ORDER_ID, prPracticeOrder.getId())
                .activate();
    }

    public void editOrder() {
        _uiActivation.asRegion(PracticeOrderAddEdit.class)
                .parameter(PRACTICE_ORDER_ID, prPracticeOrder.getId())
                .activate();
    }

    public void onClickReject() {
        _uiSupport.setRefreshScheduled(true);
        PracticeOrderManager.instance().practiceOrderDao().doReject(getPrPracticeOrder());
    }

    public void onClickCommit() {
        _uiSupport.setRefreshScheduled(true);
        PracticeOrderManager.instance().practiceOrderDao().doCommit(Arrays.asList(getPrPracticeOrder().getId()));
    }

    public void onClickSendToFormative() {
        _uiSupport.setRefreshScheduled(true);
        PracticeOrderManager.instance().practiceOrderDao().doSendToFormative(getPrPracticeOrder().getId());
    }

    public void onClickRollback() {
        _uiSupport.setRefreshScheduled(true);
        PracticeOrderManager.instance().practiceOrderDao().doRollback(getPrPracticeOrder());
    }

    public void deleteOrder() {
        PracticeOrderManager.instance().practiceOrderDao().deleteOrder(prPracticeOrder.getId());
        deactivate();
    }

    public void onClickPrintOrder() {
        _uiSupport.setRefreshScheduled(true);
        PracticeOrderManager.instance().practiceOrderDao().getDownloadPrintForm(prPracticeOrder.getId());
    }

    @SuppressWarnings("unused")
    public boolean isNeedVising() {
        return _visingStatus != null;
    }

    @Override
    public IAbstractDocument getDocument() {
        return prPracticeOrder;
    }

    @Override
    public boolean isVisaListModificable() {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(prPracticeOrder.getState().getCode());
    }

    @SuppressWarnings("unused")
    public Map<String, Object> getVisaListParameters() {
        return new ParametersMap().add("visaOwnerModel", this);
    }

    public String getSecPostfix() {
        return "prPracticeOrder";
    }

    @SuppressWarnings("unused")
    public String getParagraphListBcName() {
        return PracticeOrderManager.instance().orderTypeExtPoint().getItem(getPrPracticeOrder().getType().getCode()).getSimpleName();
    }

    public PrPracticeOrder getPrPracticeOrder() {
        return prPracticeOrder;
    }

    public void setPrPracticeOrder(PrPracticeOrder prPracticeOrder) {
        this.prPracticeOrder = prPracticeOrder;
    }

    public Format getDateFormat() {
        return simpleDateFormat;
    }

    public CommonPostfixPermissionModel getSecModel() {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel) {
        _secModel = secModel;
    }

    public OrderStates getState() {
        return _state;
    }

    public void setState(OrderStates state) {
        _state = state;
    }

    public Long getVisingStatus() {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus) {
        _visingStatus = visingStatus;
    }

    public String getSelectedTab() {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        _selectedTab = selectedTab;
    }
}
