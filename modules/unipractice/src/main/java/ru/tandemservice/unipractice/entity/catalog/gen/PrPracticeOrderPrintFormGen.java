package ru.tandemservice.unipractice.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderPrintForm;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Печатные формы приказов о направлении на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PrPracticeOrderPrintFormGen extends ScriptItem
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.catalog.PrPracticeOrderPrintForm";
    public static final String ENTITY_NAME = "prPracticeOrderPrintForm";
    public static final int VERSION_HASH = -2016753652;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PrPracticeOrderPrintFormGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PrPracticeOrderPrintFormGen> extends ScriptItem.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PrPracticeOrderPrintForm.class;
        }

        public T newInstance()
        {
            return (T) new PrPracticeOrderPrintForm();
        }
    }
    private static final Path<PrPracticeOrderPrintForm> _dslPath = new Path<PrPracticeOrderPrintForm>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PrPracticeOrderPrintForm");
    }
            

    public static class Path<E extends PrPracticeOrderPrintForm> extends ScriptItem.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return PrPracticeOrderPrintForm.class;
        }

        public String getEntityName()
        {
            return "prPracticeOrderPrintForm";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
