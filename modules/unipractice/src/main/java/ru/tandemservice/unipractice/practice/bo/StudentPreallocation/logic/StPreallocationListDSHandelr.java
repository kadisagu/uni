/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;

/**
 * @author Andrey Avetisov
 * @since 09.10.2014
 */
public class StPreallocationListDSHandelr extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String ORG_UNIT_ID = "orgUnitId";
    private boolean _pageable;

    public StPreallocationListDSHandelr(String ownerId, boolean pageable)
    {
        super(ownerId);
        _pageable = pageable;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long orgUnitId = context.get(ORG_UNIT_ID);
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StPracticePreallocation.class, "p").column("p");
        if (orgUnitId!=null)
            builder.where(DQLExpressions.eqValue(DQLExpressions.property("p", StPracticePreallocation.L_ORG_UNIT), orgUnitId));

        builder.order(DQLExpressions.property("p", StPracticePreallocation.P_FORMING_DATE), input.getEntityOrder().getDirection());

        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession())
                .pageable(_pageable)
                .build();



        if (!_pageable)
        {
            output.setCountRecord(Math.max(output.getTotalSize(), 5));
        }
        return output;

    }
}
