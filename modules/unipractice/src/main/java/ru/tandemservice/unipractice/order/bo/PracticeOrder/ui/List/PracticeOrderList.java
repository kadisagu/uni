package ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderDSHandler;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.ui.Pub.PracticeOrderPub;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import static ru.tandemservice.unipractice.order.bo.PracticeOrder.logic.PracticeOrderConstants.*;

@Configuration
public class PracticeOrderList extends BusinessComponentManager {
    @Bean
    public ColumnListExtPoint practiceOrderDS() {
        return columnListExtPointBuilder(PRACTICE_ORDER_DS)
                .addColumn(checkboxColumn("checkbox"))
                .addColumn(publisherColumn(CREATE_DATE_C, PrPracticeOrder.createDate())
                                .businessComponent(PracticeOrderPub.class)
                                .formatter(DateFormatter.DATE_FORMATTER_WITH_TIME)
                                .order()
                )
                .addColumn(textColumn(COMMIT_DATE_C, PrPracticeOrder.commitDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(textColumn(NUMBER_C, PrPracticeOrder.number()).order())
                .addColumn(textColumn(TYPE_C, PrPracticeOrder.type().shortTitle()).order())
                .addColumn(textColumn(COMMIT_DATE_SYSTEM_C, PrPracticeOrder.commitDateSystem()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn(EDUCATION_YEAR_C, PrPracticeOrder.educationYear().title()).order())
                .addColumn(textColumn(PARAGRAPH_COUNT_C, "paragraphCount").clickable(false))
                .addColumn(textColumn(STATE_C, PrPracticeOrder.state().title()).order())
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrintOrder").permissionKey("printPrPracticeOrder").disabled(VIEW_PROPERTY_PRINTING_DISABLED))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("editPrPracticeOrder").disabled(VIEW_PROPERTY_EDIT_DELETE_ENABLED))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER,
                        alert(PRACTICE_ORDER_DS + ".delete.alert")).permissionKey("deletePrPracticeOrder").disabled(VIEW_PROPERTY_EDIT_DELETE_ENABLED))
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRACTICE_ORDER_DS, practiceOrderDS(), practiceOrderDSHandler()))
                .addDataSource(selectDS(ORDER_TYPE_DS, PracticeOrderManager.instance().orderTypeDSHandler()))
                .addDataSource(selectDS(ORDER_STATE_DS, PracticeOrderManager.instance().orderStateDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR_DS, PracticeOrderManager.instance().educationYearDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> practiceOrderDSHandler() {
        return new PracticeOrderDSHandler(getName());
    }

}
