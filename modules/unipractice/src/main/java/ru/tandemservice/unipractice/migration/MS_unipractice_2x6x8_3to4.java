package ru.tandemservice.unipractice.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipractice_2x6x8_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность practiceScriptItem

		// создана новая сущность
		{
			// у сущности нет своей таблицы - ничего делать не надо
			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("practiceScriptItem");

		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность productionTrainingResultsReport

        // создана новая сущность
        if (!tool.tableExists("pr_rep_prod_training_res_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("pr_rep_prod_training_res_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                    new DBColumn("educationyear_p", DBType.createVarchar(255)).setNullable(false),
                    new DBColumn("detachedextorgunit_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("productionTrainingResultsReport");

        }



    }
}

