package ru.tandemservice.unipractice.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.catalog.entity.LegalForm;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unipractice.entity.gen.PrPracticeBaseGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.coalesce;

/**
 * База прохождения практики
 */
public abstract class PrPracticeBase extends PrPracticeBaseGen implements ITitled
{
    public static final String P_ACTIVE = "active";

    public boolean isActive() {
        return !isArchival();
    }

    @Override
    @EntityDSLSupport
    public abstract String getTitle();

    /**
     * Этот ужас нужен для фильтрации и сортировок по названию базы практики
     */
    @Zlo(value = "Выпилить это! Кэшировать название в отдельное поле или еще как-то.")
    public static void addJoinsForUseTitle(DQLSelectBuilder dql, String practiceBaseAlias, String extOuAlias, String extOuParentAlias, String legalFormAlias, String intOuAlias)
    {
        dql.joinEntity(practiceBaseAlias, DQLJoinType.left, PrPracticeBaseExt.class, "ext", eq(practiceBaseAlias, "ext"));
        dql.joinPath(DQLJoinType.left, PrPracticeBaseExt.orgUnit().fromAlias("ext"), extOuAlias);
        dql.joinPath(DQLJoinType.left, ExternalOrgUnit.parent().fromAlias(extOuAlias), extOuParentAlias);
        dql.joinPath(DQLJoinType.left, ExternalOrgUnit.legalForm().fromAlias(extOuAlias), legalFormAlias);

        dql.joinEntity(practiceBaseAlias, DQLJoinType.left, PrPracticeBaseInt.class, "int", eq(practiceBaseAlias, "int"));
        dql.joinPath(DQLJoinType.left, PrPracticeBaseInt.orgUnit().fromAlias("int"), intOuAlias);
    }

    /**
     * Этот ужас нужен для фильтрации и сортировок по названию базы практики
     */
    @Zlo(value = "Выпилить это! Кэшировать название в отдельное поле или еще как-то.")
    public static IDQLExpression createTitleDQLExpression(String extOuAlias, String extOuParentAlias, String legalFormAlias, String intOuAlias)
    {
        final IDQLExpression extPrBaseTitleProperty = DQLFunctions.trim(DQLFunctions.concat(
                property(legalFormAlias, LegalForm.shortTitle()), value(" «"), property(extOuAlias, ExternalOrgUnit.title()), value("»"),
                coalesce(DQLFunctions.concat(value(" ("), property(extOuParentAlias, ExternalOrgUnit.title()), value(")")), value(""))
        ));

        final IDQLExpression intPrBaseTitleProperty = property(intOuAlias, OrgUnit.fullTitle());

        return coalesce(intPrBaseTitleProperty, extPrBaseTitleProperty);
    }
}