package ru.tandemservice.unipractice.entity.catalog;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unipractice.entity.catalog.gen.PrEmploymentFormGen;

/**
 * Форма трудоустройства на время прохождения практики
 */
public class PrEmploymentForm extends PrEmploymentFormGen implements IDynamicCatalogItem
{
}