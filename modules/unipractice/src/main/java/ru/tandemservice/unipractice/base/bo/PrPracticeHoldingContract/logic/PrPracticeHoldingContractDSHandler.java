/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeHoldingContract.logic;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.gen.CtrContractVersionGen;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unipractice.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unipractice.entity.PrPracticeBaseContract;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author azhebko
 * @since 07.11.2014
 */
public class PrPracticeHoldingContractDSHandler extends EntityComboDataSourceHandler
{
    public static final String BIND_CONTRACT_NUMBER = "contractNumber";
    public static final String BIND_PRACTICE_BASE = "practiceBase";

    public static final String VP_PRACTICE_BASE = "vpPracticeBase";
    public static final String VP_PRACTICE_BASE_CONTRACT = "vpPracticeBaseContract";

    public PrPracticeHoldingContractDSHandler(String ownerId) { super(ownerId, CtrContractVersion.class); }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);

        dql.where(eq(property(alias, CtrContractVersion.contract().type().code()), value(CtrContractTypeCodes.DOGOVOR_OB_ORGANIZATSII_I_PROVEDENII_PRAKTIKI_STUDENTOV)));

        String contractNumber = context.get(BIND_CONTRACT_NUMBER);
        if (!StringUtils.isEmpty(contractNumber))
            dql.where(likeUpper(property(alias, CtrContractVersion.contract().number()), value(CoreStringUtils.escapeLike(contractNumber))));

        Long practiceBase = context.get(BIND_PRACTICE_BASE);
        if (practiceBase != null)
        {
            dql.where(exists(new DQLSelectBuilder()
                .fromEntity(PrPracticeBaseContract.class, "bc")
                .where(eq(property("bc", PrPracticeBaseContract.contractObject()), property(alias, CtrContractVersion.contract())))
                .where(eq(property("bc", PrPracticeBaseContract.practiceBase()), value(practiceBase)))
                .buildQuery()));
        }
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DSOutput dsOutput = super.execute(input, context);
        Collection<CtrContractObject> contractObjects = CollectionUtils.collect(dsOutput.<CtrContractVersion>getRecordList(), CtrContractVersionGen::getContract);

        final Map<CtrContractObject, PrPracticeBaseContract> contractPracticeBaseMap = new HashMap<>();
        for (PrPracticeBaseContract practiceBaseContract: IUniBaseDao.instance.get().getList(PrPracticeBaseContract.class, PrPracticeBaseContract.contractObject(), contractObjects))
             contractPracticeBaseMap.put(practiceBaseContract.getContractObject(), practiceBaseContract);

        return dsOutput.transform((CtrContractVersion version) -> {
            DataWrapper result = new DataWrapper(version);
            PrPracticeBaseContract practiceBaseContract = contractPracticeBaseMap.get(version.getContract());
            result.put(VP_PRACTICE_BASE, new DataWrapper(practiceBaseContract.getPracticeBase()));
            result.put(VP_PRACTICE_BASE_CONTRACT, practiceBaseContract);
            return result;
        });
    }
}