package ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class ParagraphChangeExtractsDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    private static final String CH_EXTR = "changeExtract";

    public ParagraphChangeExtractsDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        Long paragraphId = context.get(PracticeParagraphConstants.PARAGRAPH_ID);

        DQLSelectBuilder changeExtracts = new DQLSelectBuilder().fromEntity(PrPracticeChangeExtract.class, CH_EXTR).column(CH_EXTR);
        changeExtracts.where(eq(
                property(PrPracticeChangeExtract.paragraph().id().fromAlias(CH_EXTR)),
                value(paragraphId)
        ));

        IDQLExpression order = property(CH_EXTR, PrPracticeChangeExtract.number());
        changeExtracts.order(order);

        return DQLSelectOutputBuilder.get(input, changeExtracts, context.getSession()).pageable(false).build();
    }
}
