package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.PreDipAddEdit;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.AbstractAddEdit.AbstractPracticeParagraphAddEditUI;

import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Input({
    @Bind(key = ORDER_ID, binding = ORDER_ID),
    @Bind(key = PARAGRAPH_ID, binding = PARAGRAPH_ID)
})
public class PracticeParagraphPreDipAddEditUI extends AbstractPracticeParagraphAddEditUI {
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(PRACTICE_CODE, EppRegistryStructureCodes.REGISTRY_PRACTICE_PRE_DIPLOMA);
    }
}