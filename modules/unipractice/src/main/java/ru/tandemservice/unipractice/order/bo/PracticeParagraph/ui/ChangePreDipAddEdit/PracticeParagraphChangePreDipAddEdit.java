package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ChangePreDipAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.PracticeParagraphManager;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;

import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Configuration
public class PracticeParagraphChangePreDipAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PRACTICE_EXTRACT_DS, practiceAssignmentColumnListDS(), PracticeParagraphManager.instance().practiceChangeAssignmentDSHandler()))
                .addDataSource(selectDS(ORDER_TO_CHANGE_DS, PracticeParagraphManager.instance().orderToChangeDSHandler()))
                .addDataSource(selectDS(RESPONSIBLE_ORG_UNIT_DS, PracticeParagraphManager.instance().practiceParagraphChangeResponsibleOrgUnitDSHandler()))
                .addDataSource(selectDS(START_DATE_DS, PracticeParagraphManager.instance().practiceParagraphChangeStartDateDSHandler()))
                .addDataSource(selectDS(END_DATE_DS, PracticeParagraphManager.instance().practiceParagraphChangeEndDateDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint practiceAssignmentColumnListDS() {
        StudentGen.Path<Student> studentPath = PrPracticeExtract.entity().practice().student();
        return columnListExtPointBuilder(PRACTICE_EXTRACT_DS)
                .addColumn(checkboxColumn("checkbox"))
                .addColumn(textColumn(FIO_C, studentPath.person().identityCard().fullFio()))
                .addColumn(textColumn(GROUP_C, studentPath.group().title()))
                .addColumn(textColumn(FORMATIVE_ORG_UNIT_C, studentPath.educationOrgUnit().formativeOrgUnit().fullTitle()))
                .addColumn(textColumn(RESPONSIBLE_ORG_UNIT_C, PrPracticeExtract.entity().practice().registryElementPart().registryElement().owner().title()))
                .addColumn(textColumn(PRACTICE_BASE_C, PrPracticeExtract.entity().practiceBase().title()))
                .create();
    }
}