/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic.IStudentPreallocationDao;
import ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic.StudentPreallocationDao;

/**
 * @author Andrey Avetisov
 * @since 09.10.2014
 */
@Configuration
public class StudentPreallocationManager extends BusinessObjectManager
{
    public static StudentPreallocationManager instance() {
        return instance(StudentPreallocationManager.class);
    }
    @Bean
    public IStudentPreallocationDao studentPreallocationDao() {
        return new StudentPreallocationDao();
    }

    public String getPermissionKey()
    {
        return "stPracticePreallocationReportPermissionKey";
    }
}
