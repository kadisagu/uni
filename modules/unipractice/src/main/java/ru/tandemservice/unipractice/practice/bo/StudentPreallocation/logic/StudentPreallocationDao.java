/* $Id$ */
package ru.tandemservice.unipractice.practice.bo.StudentPreallocation.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;

/**
 * @author Andrey Avetisov
 * @since 14.10.2014
 */
public class StudentPreallocationDao extends UniBaseDao implements IStudentPreallocationDao
{
    @Override
    public void savePracticePreallocation(StPracticePreallocation practicePreallocation)
    {
        save(practicePreallocation.getContent());
        getSession().flush();
        save(practicePreallocation);
    }
}
