package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.ChangePub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.ParagraphDatesUtils;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.AbstractPub.AbstractPracticeParagraphPubUI;
import ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeChangeParagraph;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import java.util.List;

import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.ORDER_ID;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.PARAGRAPH_ID;

@Input({
        @Bind(key = ORDER_ID, binding = ORDER_ID),
        @Bind(key = PARAGRAPH_ID, binding = PARAGRAPH_ID)
})
public class PracticeParagraphChangePubUI extends AbstractPracticeParagraphPubUI<PrPracticeChangeParagraph, PrPracticeChangeExtract> {

    private PrPracticeOrder changeOrder;

    @Override
    @SuppressWarnings("unchecked")
    public void onComponentRefresh() {
        if (paragraphId != null ) {
            paragraph = DataAccessServices.dao().getNotNull(PrPracticeChangeParagraph.class, paragraphId);
        }
        if (orderId != null){
            order = DataAccessServices.dao().getNotNull(PrPracticeOrder.class, orderId);
        }
        List<PrPracticeChangeExtract> changeExtracts = (List<PrPracticeChangeExtract>) paragraph.getExtractList();
        List<PrPracticeExtract> extracts = CommonBaseEntityUtil.getPropertiesList(changeExtracts, PrPracticeChangeExtract.entity());
        changeOrder = extracts.get(0).getParagraph().getOrder();
        String[] datesFormatted = ParagraphDatesUtils.getSortedFormattedDates(extracts);
        startDates = datesFormatted[0];
        endDates = datesFormatted[1];
        formativeOrgUnits = CommonBaseEntityUtil.getPropertiesSet(extracts, PrPracticeExtract.entity().practice().student().educationOrgUnit().formativeOrgUnit());
        responsibleOrgUnits = CommonBaseEntityUtil.getPropertiesSet(extracts, PrPracticeExtract.entity().practice().registryElementPart().registryElement().owner());
        setSecModel(new CommonPostfixPermissionModel("prPracticeOrder"));
    }

    public String getChangeOrderTitle(){
        return getOrderTitle(changeOrder);
    }

    public PrPracticeOrder getChangeOrder() {
        return changeOrder;
    }
}
