package ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.catalog.PrPracticeAssignmentState;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;

import static ru.tandemservice.unipractice.practice.bo.PrPracticeAssignment.logic.PracticeAssignmentConstants.*;

@Input({
        @Bind(key = PR_PRACTICE_ASSIGNMENT_ID, binding = PR_PRACTICE_ASSIGNMENT_ID),
        @Bind(key = STUDENT_ID, binding = STUDENT_ID)
})
public class PrPracticeAssignmentAddEditUI extends UIPresenter {
    private PrPracticeAssignment prPracticeAssignment;

    private Long prPracticeAssignmentId;
    private Long studentId;

    @Override
    public void onComponentRefresh() {
        if (prPracticeAssignmentId != null) {
            prPracticeAssignment = DataAccessServices.dao().getNotNull(PrPracticeAssignment.class, prPracticeAssignmentId);
        } else {
            prPracticeAssignment = new PrPracticeAssignment();
            PrPracticeAssignmentState state = DataAccessServices.dao().getByCode(PrPracticeAssignmentState.class, PrPracticeAssignmentStateCodes.FORMING);
            prPracticeAssignment.setState(state);
        }
    }

    public boolean isNewEntity() {
        return prPracticeAssignmentId == null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (PRACTICE_DS.equals(dataSource.getName())) {
            dataSource.put(STUDENT_ID, studentId);
            if (!isNewEntity()) {
                dataSource.put(PRACTICE, prPracticeAssignment.getPractice());
            }
        } else if (TUTOR_DS.equals(dataSource.getName())) {
            Long orgUnitId = null;
            if (prPracticeAssignment.getPractice() != null && prPracticeAssignment.getPractice().getRegistryElementPart() != null &&
                    prPracticeAssignment.getPractice().getRegistryElementPart().getRegistryElement() != null &&
                    prPracticeAssignment.getPractice().getRegistryElementPart().getRegistryElement().getOwner() != null) {
                orgUnitId = prPracticeAssignment.getPractice().getRegistryElementPart().getRegistryElement().getOwner().getId();
            }
            dataSource.put(ORG_UNIT_ID, orgUnitId);
        }
    }

    public void onChoosePractice()
    {
        if(prPracticeAssignment.getPractice() != null)
        {
            prPracticeAssignment.setDateStart((prPracticeAssignment.getPractice().getSourceRow().getBeginDate()));
            prPracticeAssignment.setDateEnd((prPracticeAssignment.getPractice().getSourceRow().getEndDate()));
        }
    }

    public void onClickApply() {
        if (validate().hasErrors()) {
            return;
        }
        DataAccessServices.dao().saveOrUpdate(prPracticeAssignment);
        deactivate();
    }

    protected ErrorCollector validate()
    {
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        if (CommonBaseDateUtil.isAfter(prPracticeAssignment.getDateStart(), prPracticeAssignment.getDateEnd()))
            errorCollector.add("Дата начала должна быть не больше даты окончания.", "startDate", "endDate");

        return errorCollector;
    }

    public PrPracticeAssignment getPrPracticeAssignment() {
        return prPracticeAssignment;
    }

    public void setPrPracticeAssignment(PrPracticeAssignment prPracticeAssignment) {
        this.prPracticeAssignment = prPracticeAssignment;
    }

    public void setPrPracticeAssignmentId(Long prPracticeAssignmentId) {
        this.prPracticeAssignmentId = prPracticeAssignmentId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }
}
