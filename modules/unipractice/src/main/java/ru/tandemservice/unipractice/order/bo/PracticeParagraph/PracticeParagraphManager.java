package ru.tandemservice.unipractice.order.bo.PracticeParagraph;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSubselectType;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unipractice.entity.PrPracticeAssignment;
import ru.tandemservice.unipractice.entity.catalog.codes.PrPracticeAssignmentStateCodes;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PrPracticeAssignmentChangeDSHandler;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PrPracticeAssignmentDSHandler;
import ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.wrapper.DateWrapper;
import ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeOrder;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unipractice.order.bo.PracticeParagraph.logic.PracticeParagraphConstants.*;

@Configuration
public class PracticeParagraphManager extends BusinessObjectManager
{
    public static PracticeParagraphManager instance()
    {
        return instance(PracticeParagraphManager.class);
    }

    @Bean
    public IDefaultSearchDataSourceHandler practiceAssignmentDSHandler()
    {
        return new PrPracticeAssignmentDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceParagraphResponsibleOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                Long currentParagraphId = context.get(PARAGRAPH_ID);

                DQLSelectBuilder subSubBuilder = new DQLSelectBuilder().fromEntity(PrPracticeExtract.class, "pe")
                        .where(eq(property("pe", PrPracticeExtract.entity()), property("pr")));

                if(currentParagraphId != null)
                    subSubBuilder.where(ne(property("pe", PrPracticeExtract.paragraph().id()), value(currentParagraphId)));

                Long eduYearId = context.getNotNull(EDU_YEAR);

                DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "pr")
                        .column(property("pr", PrPracticeAssignment.practice().registryElementPart().registryElement().owner().id()))
                        .where(notExists(subSubBuilder.buildQuery()))
                        .where(eq(property("pr", PrPracticeAssignment.state().code()), value(PrPracticeAssignmentStateCodes.ACCEPTED)))
                        .where(eq(property("pr", PrPracticeAssignment.practice().year().educationYear().id()), value(eduYearId)));

                Long formativeOrgUnitId = context.get(FORMATIVE_ORG_UNIT_F);
                if(formativeOrgUnitId != null)
                    sub.where(eq(property("pr", PrPracticeAssignment.practice().student().educationOrgUnit().formativeOrgUnit().id()), value(formativeOrgUnitId)));

                // Фильтрация с учётом типа направления на практику
                String practiceType = context.get(PRACTICE_CODE);
                if(!StringUtils.isEmpty(practiceType))
                {
                    sub.where(eq(property("pr", PrPracticeAssignment.practice().registryElementPart().registryElement().parent().code()), value(practiceType)));
                }

                // Оргюнит должен быть одним из всех ответственных подразделений практик из доступных направлений
                dql.where(eqSubquery(property(alias), DQLSubselectType.any, sub.buildQuery()));
            }
        }
                .order(OrgUnit.title())
                .filter(OrgUnit.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceParagraphStartDateDSHandler()
    {
        return getPracticeDateHandler(PrPracticeAssignment.dateStart());
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceParagraphEndDateDSHandler()
    {
        return getPracticeDateHandler(PrPracticeAssignment.dateEnd());
    }

    private DefaultComboDataSourceHandler getPracticeDateHandler( final PropertyPath<Date> path)
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "ppa")
                        .column(property("ppa", path))
                        .order(property("ppa", path))
                        .where(eq(property("ppa", PrPracticeAssignment.state().code()), value(PrPracticeAssignmentStateCodes.ACCEPTED)));

                Long eduYearId = context.getNotNull(EDU_YEAR);
                dql.where(eq(property("ppa", PrPracticeAssignment.practice().year().educationYear().id()), value(eduYearId)));

                IDQLExpression extractsWithoutAssignmentsCondition = notExists(PrPracticeExtract.class, PrPracticeExtract.L_ENTITY, (Object) property("ppa"));

                // Фильтрация с учётом выбранного формирующего орг юнита
                Long formativeOrgUnitId = context.get(FORMATIVE_ORG_UNIT_F);
                IDQLExpression formativeOrgUnitCondition = null;
                if (formativeOrgUnitId != null)
                {
                    formativeOrgUnitCondition = eq(
                            property(PrPracticeAssignment.practice().student().educationOrgUnit().formativeOrgUnit().fromAlias("ppa")),
                            value(formativeOrgUnitId));
                }

                // Фильтрация с учётом выбранного ответсвенного орг юнита
                Long responsibleOrgUnitId = context.get(RESPONSIBLE_ORG_UNIT_F);
                IDQLExpression responsibleOrgUnitCondition = eq(
                        property(PrPracticeAssignment.practice().registryElementPart().registryElement().owner().id().fromAlias("ppa")),
                        value(responsibleOrgUnitId));

                // Фильтрация с учётом даты начала практики
                IDQLExpression startDateCondition = null;
                Date startDate = context.get(START_DATE);
                if (startDate != null)
                {
                    startDateCondition = inDay(PrPracticeAssignment.dateStart().fromAlias("ppa"), startDate);
                }

                // Фильтрация с учётом типа направления на практику

                String practiceType = context.get(PRACTICE_CODE);

                if(!StringUtils.isEmpty(practiceType))
                    dql.where(eq(property("ppa", PrPracticeAssignment.practice().registryElementPart().registryElement().parent().code()), value(practiceType)));

                IDQLExpression assignmentsWithParagraphIdCondition = null;
                Long paragraphId = context.get(PARAGRAPH_ID);
                if (paragraphId != null)
                {
                    dql.joinEntity("ppa", DQLJoinType.inner, PrPracticeExtract.class, "ext", eq(property(PrPracticeExtract.entity().fromAlias("ext")), property("ppa")));
                    assignmentsWithParagraphIdCondition = eq(property(PrPracticeExtract.paragraph().fromAlias("ext")), value(paragraphId));
                }

                IDQLExpression finalCondition = and(extractsWithoutAssignmentsCondition, responsibleOrgUnitCondition);
                if (formativeOrgUnitCondition != null)
                {
                    finalCondition = and(finalCondition, formativeOrgUnitCondition);
                }
                if (startDateCondition != null)
                {
                    finalCondition = and(finalCondition, startDateCondition);
                }
                if (assignmentsWithParagraphIdCondition != null)
                {
                    finalCondition = or(finalCondition, assignmentsWithParagraphIdCondition);
                }
                dql.where(finalCondition);

                List<Date> dateList = Lists.newArrayList(Sets.newHashSet(dql.createStatement(context.getSession()).<Date>list()));

                List<DateWrapper> resultList = Lists.newArrayList();

                for (Date date : dateList)
                {
                    resultList.add(new DateWrapper(CoreDateUtils.getDayFirstTimeMoment(date)));
                }

                Collections.sort(resultList);

                context.put(UIDefines.COMBO_OBJECT_LIST, resultList);

                return super.execute(input, context);
            }
        };
    }

    @Bean
    public IDefaultSearchDataSourceHandler practiceChangeAssignmentDSHandler() {
        return new PrPracticeAssignmentChangeDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceParagraphChangeResponsibleOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                Long changeOrderId = context.get(ORDER_TO_CHANGE_F);
                DQLSelectBuilder sub1 = new DQLSelectBuilder().fromEntity(PrPracticeExtract.class, "extract")
                        .column(property(PrPracticeExtract.entity().practice().registryElementPart().registryElement().owner().id().fromAlias("extract")));
                sub1.where(eq(
                        property(PrPracticeExtract.paragraph().order().id().fromAlias("extract")), value(changeOrderId)));
                IDQLExpression condition = eqSubquery(property(alias), DQLSubselectType.any, sub1.buildQuery());
                dql.where(condition);
            }
        }
        .order(OrgUnit.title())
        .filter(OrgUnit.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceParagraphChangeStartDateDSHandler()
    {
        return getPracticeChangeDateHandler(PrPracticeAssignment.dateStart());
    }

    @Bean
    public IDefaultComboDataSourceHandler practiceParagraphChangeEndDateDSHandler()
    {
        return getPracticeChangeDateHandler(PrPracticeAssignment.dateEnd());
    }

    private DefaultComboDataSourceHandler getPracticeChangeDateHandler( final PropertyPath<Date> path)
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(PrPracticeAssignment.class, "ppa")
                        .column(property("ppa", path))
                        .order(property("ppa", path));

                Long changeOrderId = context.get(ORDER_TO_CHANGE_F);
                DQLSelectBuilder sub1 = new DQLSelectBuilder().fromEntity(PrPracticeExtract.class, "extract")
                        .column(property(PrPracticeExtract.entity().id().fromAlias("extract")));
                sub1.where(eq(
                        property(PrPracticeExtract.paragraph().order().id().fromAlias("extract")), value(changeOrderId)));
                IDQLExpression condition = eqSubquery(property("ppa"), DQLSubselectType.any, sub1.buildQuery());

                // Фильтрация с учётом выбранного ответсвенного орг юнита
                Long responsibleOrgUnitId = context.get(RESPONSIBLE_ORG_UNIT_F);
                IDQLExpression responsibleOrgUnitCondition = eq(
                        property(PrPracticeAssignment.practice().registryElementPart().registryElement().owner().id().fromAlias("ppa")),
                        value(responsibleOrgUnitId));

                // Фильтрация с учётом даты начала практики
                IDQLExpression startDateCondition = null;
                Date startDate = context.get(START_DATE);
                if (startDate != null) {
                    startDateCondition = inDay(PrPracticeAssignment.dateStart().fromAlias("ppa"), startDate);
                }

                dql.where(condition);
                dql.where(responsibleOrgUnitCondition);
                dql.where(startDateCondition);

                List<Date> dateList = Lists.newArrayList(Sets.newHashSet(dql.createStatement(context.getSession()).<Date>list()));

                List<DateWrapper> resultList = Lists.newArrayList();

                for (Date date : dateList)
                {
                    resultList.add(new DateWrapper(CoreDateUtils.getDayFirstTimeMoment(date)));
                }

                Collections.sort(resultList);

                context.put(UIDefines.COMBO_OBJECT_LIST, resultList);

                return super.execute(input, context);
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler orderToChangeDSHandler() {
        return new EntityComboDataSourceHandler(getName(), PrPracticeOrder.class) {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                Long eduYearId = context.getNotNull(EDU_YEAR);
                Long paragraphId = context.get(PARAGRAPH_ID);

                dql.where(eq(property(alias, PrPracticeOrder.educationYear().id()), value(eduYearId)));

                DQLSelectBuilder subSubBuilder = new DQLSelectBuilder().fromEntity(PrPracticeChangeExtract.class, "ppce")
                        .where(eq(property("ppce", PrPracticeChangeExtract.entity().id()), property("ppe", PrPracticeExtract.id())));

                if(paragraphId != null)
                    subSubBuilder.where(ne(property("ppce", PrPracticeChangeExtract.paragraph().id()), value(paragraphId)));

                DQLSelectBuilder subBulder = new DQLSelectBuilder().fromEntity(PrPracticeExtract.class, "ppe")
                        .where(eq(property("ppe", PrPracticeExtract.paragraph().order().id()), property(alias, PrPracticeOrder.id())))
                        .where(ne(
                                property(PrPracticeExtract.practiceBase().fromAlias("ppe")),
                                property(PrPracticeExtract.entity().practiceBase().fromAlias("ppe"))))
                        .where(notExists(subSubBuilder.buildQuery()));

                dql.where(exists(subBulder.buildQuery()));

                String practiceOrderType = context.getNotNull(PRACTICE_ORDER_TYPE);
                dql.where(eq(
                        property(PrPracticeOrder.type().code().fromAlias(alias)),
                        value(practiceOrderType)));
                dql.where(eq(
                        property(PrPracticeOrder.state().code().fromAlias(alias)),
                        value(OrderStatesCodes.FINISHED)));
            }
        }
                .order(PrPracticeOrder.number());
    }

}
