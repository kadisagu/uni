package ru.tandemservice.unipractice.order.bo.PracticeParagraph.ui.AbstractPub;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.block.IBlockListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

public abstract class AbstractPracticeParagraphPub extends BusinessComponentManager {

    protected static final String ORDER_INFO_BLOCK = "OrderInfoBlock";
    protected static final String ORDER_PARAGRAPH_INFO_BLOCK = "orderParagraphInfoBlock";
    protected static final String STUDENTS_LIST_BLOCK = "studentsListBlock";

    public PresenterExtPoint presenterExtPoint(IPresenterExtPointBuilder builder) {
        return builder.create();
    }

    public ColumnListExtPoint practiceExtractListDS(IColumnListExtPointBuilder builder) {
        return builder.create();
    }

    public BlockListExtPoint practiceParagraphChangeBlockListExtPoint(IBlockListExtPointBuilder builder){
        return builder
                .addBlock(htmlBlock(ORDER_INFO_BLOCK, "OrderInfoBlock").create())
                .addBlock(htmlBlock(ORDER_PARAGRAPH_INFO_BLOCK, "OrderParagraphInfoBlock").create())
                .addBlock(htmlBlock(STUDENTS_LIST_BLOCK, "StudentsListBlock").create())
                .create();
    }

}
