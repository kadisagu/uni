package ru.tandemservice.unipractice.entity;

import ru.tandemservice.unipractice.entity.gen.PrPracticeBaseExtGen;

/**
 * База прохождения практики (внешняя организация)
 */
public class PrPracticeBaseExt extends PrPracticeBaseExtGen
{
    @Override
    public String getTitle()
    {
        if (getOrgUnit() == null) {
            return this.getClass().getSimpleName();
        }
        return this.getOrgUnit().getLegalFormWithTitle();
    }

    public String getOrgUnitSettlementTitle()
    {
        return getOrgUnit().getLegalAddress() != null
                ? (getOrgUnit().getLegalAddress().getSettlement() != null ? getOrgUnit().getLegalAddress().getSettlement().getTitle() : null)
                : (getOrgUnit().getFactAddress() != null
                ? (getOrgUnit().getFactAddress().getSettlement() != null ? getOrgUnit().getFactAddress().getSettlement().getTitle() : null)
                : null);
    }
}