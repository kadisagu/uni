package ru.tandemservice.unipractice.practice.bo.PracticeResult.logic;

import org.apache.tapestry.request.IUploadFile;
import ru.tandemservice.unipractice.entity.PrPracticeResult;

public interface IPracticeResultDao {
    PrPracticeResult getPracticeResultByAssignmentId(Long prPracticeAssignmentId);

    void deleteScanCopy(PrPracticeResult prPracticeResult);

    void saveScanCopy(PrPracticeResult practiceResult, IUploadFile scanCopyUploadFile);
}
