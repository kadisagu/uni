/* $Id$ */
package ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unipractice.entity.PrPracticeBaseRequest;
import ru.tandemservice.unipractice.entity.PrPracticeBaseRequestExt;
import ru.tandemservice.unipractice.entity.PrPracticeBaseRequestInt;

/**
 * @author nvankov
 * @since 10/22/14
 */
public class PrPracticeBaseRequestWrapper extends DataWrapper
{
    private PrPracticeBaseRequestInt _requestInt;
    private PrPracticeBaseRequestExt _requestExt;
    private boolean _baseExist;

    public PrPracticeBaseRequestWrapper(PrPracticeBaseRequestInt requestInt, boolean baseExist)
    {
        if(requestInt == null) throw new NullPointerException();
        setId(requestInt.getId());
        _requestInt = requestInt;
        _baseExist = baseExist;
    }

    public PrPracticeBaseRequestWrapper(PrPracticeBaseRequestExt requestExt, boolean baseExist)
    {
        if(requestExt == null) throw new NullPointerException();
        setId(requestExt.getId());
        _requestExt = requestExt;
        _baseExist = baseExist;
    }

    public PrPracticeBaseRequestInt getRequestInt()
    {
        return _requestInt;
    }

    public PrPracticeBaseRequestExt getRequestExt()
    {
        return _requestExt;
    }

    public PrPracticeBaseRequest getRequest()
    {
        return isRequestInt() ? _requestInt : _requestExt;
    }

    public String getOrgUnitTitle()
    {
        return isRequestInt() ? _requestInt.getOrgUnit().getFullTitle() :
                (_requestExt.getOrgUnit() != null ? _requestExt.getOrgUnit().getTitleWithLegalForm()
                        : _requestExt.getOrgUnitTitle() + " (" + _requestExt.getOrgUnitLegalForm().getShortTitle() + ")");
    }

    //"№<номер> от <дд.мм.гггг>"
    public String getGuaranteeLetter()
    {
        StringBuilder guaranteeLetter = new StringBuilder();
        if(!isRequestInt())
        {
            if(!StringUtils.isEmpty(_requestExt.getGuaranteeLetter()))
                guaranteeLetter.append(_requestExt.getGuaranteeLetter());
            if(_requestExt.getGuaranteeLetterDate() != null)
            {
                guaranteeLetter.append(!StringUtils.isEmpty(_requestExt.getGuaranteeLetter()) ? " " : "");
                guaranteeLetter.append("от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_requestExt.getGuaranteeLetterDate()));
            }
        }
        return  guaranteeLetter.toString();
    }

    public boolean isRequestInt()
    {
        return _requestInt != null;
    }

    public boolean isBaseExist()
    {
        return _baseExist;
    }

    public boolean isHaveGuaranteeLetter()
    {
        return !isRequestInt() && _requestExt.getGuaranteeLetterCopy() != null;
    }
}
