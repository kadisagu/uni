package ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.logic.PrPracticeBaseConstants;
import ru.tandemservice.unipractice.base.bo.PrPracticeBase.ui.AddEdit.PrPracticeBaseAddEdit;
import ru.tandemservice.unipractice.entity.PrPracticeBase;

@State(@Bind(key = IUIPresenter.PUBLISHER_ID, binding = PrPracticeBaseConstants.PR_PRACTICE_BASE_ID, required = true))
public class PrPracticeBasePubUI extends UIPresenter {

    private Long prPracticeBaseId;
    private PrPracticeBase prPracticeBase;

    @Override
    public void onComponentRefresh() {
        if (prPracticeBaseId != null ) {
            prPracticeBase = DataAccessServices.dao().getNotNull(PrPracticeBase.class, prPracticeBaseId);
        }
    }

    @SuppressWarnings("unused")
    public String getPageTitle() {
        return _uiConfig.getProperty("ui.pageTitle") + " " +
                _uiConfig.getProperty("ui.openQuote") + prPracticeBase.getTitle() + _uiConfig.getProperty("ui.closeQuote");
    }

    @SuppressWarnings("unused")
    public String getDeletePracticeBaseAlert() {
        return String.format(_uiConfig.getProperty("ui.delete.alert"), prPracticeBase.getTitle());
    }

    public void editPrPracticeBase(){
        _uiActivation.asRegionDialog(PrPracticeBaseAddEdit.class).parameter(PrPracticeBaseConstants.PR_PRACTICE_BASE_ID, prPracticeBaseId).activate();
    }

    public void deletePrPracticeBase(){
        DataAccessServices.dao().delete(prPracticeBaseId);
        deactivate();
    }

    public Long getPrPracticeBaseId() {
        return prPracticeBaseId;
    }

    public void setPrPracticeBaseId(Long prPracticeBaseId) {
        this.prPracticeBaseId = prPracticeBaseId;
    }

    public PrPracticeBase getPrPracticeBase() {
        return prPracticeBase;
    }

    public void setPrPracticeBase(PrPracticeBase prPracticeBase) {
        this.prPracticeBase = prPracticeBase;
    }

    public boolean isPracticeBaseContractTabVisible() { return prPracticeBase.getKind().isExternal(); }
}
