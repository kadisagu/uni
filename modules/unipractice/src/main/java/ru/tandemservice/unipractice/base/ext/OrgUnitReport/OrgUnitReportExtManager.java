/* $Id:$ */
package ru.tandemservice.unipractice.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uni.component.reports.FormativeAndTerritorialOrgUnitReportVisibleResolver;

/**
 * @author rsizonenko
 * @since 05.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    /** Код блока отчетов на подразделении "Отчеты модуля «Практики студентов»" **/
    public static final String UNI_PRACTICE_ORG_UNIT_STUDENT_REPORT_BLOCK = "uniPracticeOrgUnitStudentReportBlock";

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportBlockDefinition> builder = itemListExtension(_orgUnitReportManager.blockListExtPoint());

        return builder
                .add(UNI_PRACTICE_ORG_UNIT_STUDENT_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Отчеты модуля «Практики студентов»", UNI_PRACTICE_ORG_UNIT_STUDENT_REPORT_BLOCK, new FormativeAndTerritorialOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {

        final IItemListExtensionBuilder<OrgUnitReportDefinition> itemList = itemListExtension(_orgUnitReportManager.reportListExtPoint());

        return itemList
                .add("stPracticePreallocation", new OrgUnitReportDefinition("Предварительное распределение студентов", "stPracticePreallocation", UNI_PRACTICE_ORG_UNIT_STUDENT_REPORT_BLOCK, "StudentPreallocationList", "orgUnit_viewStPracticePreallocationReportPermissionKey"))
                .create();
    }

}
