package ru.tandemservice.unipractice.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unipractice.entity.StPracticePreallocation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Предварительное распределение студентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StPracticePreallocationGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipractice.entity.StPracticePreallocation";
    public static final String ENTITY_NAME = "stPracticePreallocation";
    public static final int VERSION_HASH = 317377967;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_EDUCATION_YEAR = "educationYear";
    public static final String P_PRACTICE_KIND = "practiceKind";
    public static final String P_PRACTICE_BEGIN_DATE = "practiceBeginDate";
    public static final String P_PRACTICE_END_DATE = "practiceEndDate";
    public static final String P_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String P_COURSE = "course";
    public static final String P_GROUP = "group";

    private OrgUnit _orgUnit;     // Подразделение
    private String _educationYear;     // Учебный год
    private String _practiceKind;     // Вид практики
    private Date _practiceBeginDate;     // Дата начала практики
    private Date _practiceEndDate;     // Дата окончания парктики
    private String _educationLevelsHighSchool;     // Направление подготовки (специальность)
    private String _course;     // Курс
    private String _group;     // Группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(String educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Вид практики. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeKind()
    {
        return _practiceKind;
    }

    /**
     * @param practiceKind Вид практики. Свойство не может быть null.
     */
    public void setPracticeKind(String practiceKind)
    {
        dirty(_practiceKind, practiceKind);
        _practiceKind = practiceKind;
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     */
    @NotNull
    public Date getPracticeBeginDate()
    {
        return _practiceBeginDate;
    }

    /**
     * @param practiceBeginDate Дата начала практики. Свойство не может быть null.
     */
    public void setPracticeBeginDate(Date practiceBeginDate)
    {
        dirty(_practiceBeginDate, practiceBeginDate);
        _practiceBeginDate = practiceBeginDate;
    }

    /**
     * @return Дата окончания парктики. Свойство не может быть null.
     */
    @NotNull
    public Date getPracticeEndDate()
    {
        return _practiceEndDate;
    }

    /**
     * @param practiceEndDate Дата окончания парктики. Свойство не может быть null.
     */
    public void setPracticeEndDate(Date practiceEndDate)
    {
        dirty(_practiceEndDate, practiceEndDate);
        _practiceEndDate = practiceEndDate;
    }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки (специальность). Свойство не может быть null.
     */
    public void setEducationLevelsHighSchool(String educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StPracticePreallocationGen)
        {
            setOrgUnit(((StPracticePreallocation)another).getOrgUnit());
            setEducationYear(((StPracticePreallocation)another).getEducationYear());
            setPracticeKind(((StPracticePreallocation)another).getPracticeKind());
            setPracticeBeginDate(((StPracticePreallocation)another).getPracticeBeginDate());
            setPracticeEndDate(((StPracticePreallocation)another).getPracticeEndDate());
            setEducationLevelsHighSchool(((StPracticePreallocation)another).getEducationLevelsHighSchool());
            setCourse(((StPracticePreallocation)another).getCourse());
            setGroup(((StPracticePreallocation)another).getGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StPracticePreallocationGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StPracticePreallocation.class;
        }

        public T newInstance()
        {
            return (T) new StPracticePreallocation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
                case "educationYear":
                    return obj.getEducationYear();
                case "practiceKind":
                    return obj.getPracticeKind();
                case "practiceBeginDate":
                    return obj.getPracticeBeginDate();
                case "practiceEndDate":
                    return obj.getPracticeEndDate();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((String) value);
                    return;
                case "practiceKind":
                    obj.setPracticeKind((String) value);
                    return;
                case "practiceBeginDate":
                    obj.setPracticeBeginDate((Date) value);
                    return;
                case "practiceEndDate":
                    obj.setPracticeEndDate((Date) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
                case "educationYear":
                        return true;
                case "practiceKind":
                        return true;
                case "practiceBeginDate":
                        return true;
                case "practiceEndDate":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
                case "educationYear":
                    return true;
                case "practiceKind":
                    return true;
                case "practiceBeginDate":
                    return true;
                case "practiceEndDate":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return OrgUnit.class;
                case "educationYear":
                    return String.class;
                case "practiceKind":
                    return String.class;
                case "practiceBeginDate":
                    return Date.class;
                case "practiceEndDate":
                    return Date.class;
                case "educationLevelsHighSchool":
                    return String.class;
                case "course":
                    return String.class;
                case "group":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StPracticePreallocation> _dslPath = new Path<StPracticePreallocation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StPracticePreallocation");
    }
            

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getEducationYear()
     */
    public static PropertyPath<String> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Вид практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getPracticeKind()
     */
    public static PropertyPath<String> practiceKind()
    {
        return _dslPath.practiceKind();
    }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getPracticeBeginDate()
     */
    public static PropertyPath<Date> practiceBeginDate()
    {
        return _dslPath.practiceBeginDate();
    }

    /**
     * @return Дата окончания парктики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getPracticeEndDate()
     */
    public static PropertyPath<Date> practiceEndDate()
    {
        return _dslPath.practiceEndDate();
    }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getEducationLevelsHighSchool()
     */
    public static PropertyPath<String> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends StPracticePreallocation> extends StorableReport.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _educationYear;
        private PropertyPath<String> _practiceKind;
        private PropertyPath<Date> _practiceBeginDate;
        private PropertyPath<Date> _practiceEndDate;
        private PropertyPath<String> _educationLevelsHighSchool;
        private PropertyPath<String> _course;
        private PropertyPath<String> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getEducationYear()
     */
        public PropertyPath<String> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new PropertyPath<String>(StPracticePreallocationGen.P_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Вид практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getPracticeKind()
     */
        public PropertyPath<String> practiceKind()
        {
            if(_practiceKind == null )
                _practiceKind = new PropertyPath<String>(StPracticePreallocationGen.P_PRACTICE_KIND, this);
            return _practiceKind;
        }

    /**
     * @return Дата начала практики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getPracticeBeginDate()
     */
        public PropertyPath<Date> practiceBeginDate()
        {
            if(_practiceBeginDate == null )
                _practiceBeginDate = new PropertyPath<Date>(StPracticePreallocationGen.P_PRACTICE_BEGIN_DATE, this);
            return _practiceBeginDate;
        }

    /**
     * @return Дата окончания парктики. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getPracticeEndDate()
     */
        public PropertyPath<Date> practiceEndDate()
        {
            if(_practiceEndDate == null )
                _practiceEndDate = new PropertyPath<Date>(StPracticePreallocationGen.P_PRACTICE_END_DATE, this);
            return _practiceEndDate;
        }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getEducationLevelsHighSchool()
     */
        public PropertyPath<String> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new PropertyPath<String>(StPracticePreallocationGen.P_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(StPracticePreallocationGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unipractice.entity.StPracticePreallocation#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(StPracticePreallocationGen.P_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return StPracticePreallocation.class;
        }

        public String getEntityName()
        {
            return "stPracticePreallocation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
