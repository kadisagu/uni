/* $Id: PracticeExtractComponentDao.java 246 2014-09-12 08:38:50Z nvankov $ */
package ru.tandemservice.unipractice.order.bo.PracticeOrder.logic;

import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unipractice.order.bo.PracticeOrder.PracticeOrderManager;
import ru.tandemservice.unipractice.order.entity.PrPracticeChangeExtract;
import ru.tandemservice.unipractice.order.entity.PrPracticeExtract;

import java.util.Map;

/**
 * @author nvankov
 * @since 9/12/14
 */
public class PracticeChangeExtractComponentDao implements IExtractComponentDao<PrPracticeChangeExtract>
{
    @Override
    public void doCommit(PrPracticeChangeExtract extract, Map parameters)
    {
        PracticeOrderManager.instance().practiceOrderDao().doCommit(extract);
    }

    @Override
    public void doRollback(PrPracticeChangeExtract extract, Map parameters)
    {
        PracticeOrderManager.instance().practiceOrderDao().doRollback(extract);
    }
}