/* $Id:$ */
package ru.tandemservice.unipractice.practice.bo.ProductionTrainingResultsReport.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unipractice.entity.ProductionTrainingResultsReport;

/**
 * @author rsizonenko
 * @since 22.10.2014
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id", required = true)
})
public class ProductionTrainingResultsReportPubUI extends UIPresenter {


    ProductionTrainingResultsReport report = new ProductionTrainingResultsReport();

    @Override
    public void onComponentRefresh() {
        report = IUniBaseDao.instance.get().get(ProductionTrainingResultsReport.class ,report.getId());
    }

    public void onClickPrint()
    {
        if (getReport().getContent().getContent() == null)
            throw new ApplicationException("Файл печатной формы пуст.");
        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(getReport().getContent()),
                false
        );
    }

    public ProductionTrainingResultsReport getReport() {
        return report;
    }

    public void setReport(ProductionTrainingResultsReport report) {
        this.report = report;
    }
}
