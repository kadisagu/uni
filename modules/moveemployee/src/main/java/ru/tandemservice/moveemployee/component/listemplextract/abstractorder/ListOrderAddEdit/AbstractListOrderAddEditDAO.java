/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderAddEdit;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToTypeRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.uni.dao.UniDao;

import java.util.Date;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
public abstract class AbstractListOrderAddEditDAO<T extends EmployeeListOrder, Model extends AbstractListOrderAddEditModel<T>> extends UniDao<Model> implements IAbstractListOrderAddEditDAO<T, Model>
{
    @Override
    @SuppressWarnings({"unchecked"})
    public void prepare(final Model model)
    {
        if (model.getOrderId() != null)
        {
            // форма редактирования
            model.setOrder((T)getNotNull(model.getOrderId()));
            for (EmplListOrderToBasicRelation rel : getList(EmplListOrderToBasicRelation.class, EmplListOrderToBasicRelation.L_ORDER, model.getOrder()))
                model.getBasicList().add(rel.getBasic());
        }
        else
        {
            // форма добавления
            EmployeeExtractType extractType = getNotNull(EmployeeExtractType.class, model.getOrderTypeId());
            T order = createNewInstance(extractType.getFormationEntity());
            order.setCreateDate(new Date());
            order.setType(extractType);
            model.setOrder(order);
        }

        model.setReasonListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                List<EmployeeOrderReasons> list = getFirstList(EmployeeReasonToTypeRel.class, model.getOrder().getType());
                return new ListResult<>(list);
            }
        });

        model.setBasicListModel(new FullCheckSelectModel(ICatalogItem.CATALOG_ITEM_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getOrder().getReason() == null) return ListResult.getEmpty();

                Criteria c = getSession().createCriteria(EmployeeReasonToBasicRel.class, "r");
                c.createAlias("r." + IEntityRelation.L_SECOND, "s");
                c.add(Restrictions.eq("r." + IEntityRelation.L_FIRST, model.getOrder().getReason()));
                c.setProjection(Projections.property("r." + IEntityRelation.L_SECOND));
                if (filter != null) c.add(Restrictions.like("s." + EmployeeOrderBasics.P_TITLE, "%" + filter));
                c.addOrder(Order.asc(("s." + EmployeeOrderBasics.P_TITLE)));
                return new ListResult<>(c.list());
            }
        });

        if (null != model.getOrder().getId())
        {
            Criteria c = getSession().createCriteria(EmplListOrderToBasicRelation.class, "r");
            c.createAlias("r." + EmplListOrderToBasicRelation.L_BASIC, "b");
            c.add(Restrictions.eq("r." + EmplListOrderToBasicRelation.L_ORDER, model.getOrder()));
            c.addOrder(Order.asc("b." + EmployeeOrderBasics.P_TITLE));
            List<EmplListOrderToBasicRelation> relationList = c.list();

            model.getBasicList().clear();
            for (EmplListOrderToBasicRelation relation : relationList)
                model.getBasicList().add(relation.getBasic());

            model.getCurrentBasicMap().clear();
            for (EmplListOrderToBasicRelation relation : relationList)
            {
                model.getCurrentBasicMap().put(relation.getBasic().getId(), relation.getComment());
            }
        }
    }

    protected abstract T createNewInstance(String formationEntity);

    @Override
    public void update(Model model)
    {
        getSession().saveOrUpdate(model.getOrder());
        updateBasicList(model);
    }

    private void updateBasicList(Model model)
    {
        if (null != model.getOrderId())
        {
            for (EmplListOrderToBasicRelation relation : getList(EmplListOrderToBasicRelation.class, EmplListOrderToBasicRelation.L_ORDER, model.getOrder()))
                delete(relation);
            getSession().flush();
        }

        for (EmployeeOrderBasics basic : model.getBasicList())
        {
            EmplListOrderToBasicRelation relation = new EmplListOrderToBasicRelation();
            relation.setBasic(basic);
            relation.setOrder(model.getOrder());
            relation.setComment(model.getCurrentBasicMap().get(basic.getId()));
            save(relation);
        }
    }
}