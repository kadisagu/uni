/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.employee.EmployeePostOrdersPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 *         Created on: 07.11.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setExtractCantBeAdded(MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeePostHasNotFinishedExtracts(model.getEmployeePost()));
        model.setSextractCantBeAdded(MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeePostHasNotFinishedExtracts(model.getEmployeePost()));
    }

    @Override
    public void prepareCustomDataSource(Model model, int index)
    {
        switch (index)
        {
            case IDAO.MODULAR_PROJECTS_DATASOUCE_IDX:
                prepareModularProjectsDataSource(model);
                break;
            case IDAO.MODULAR_EXTRACTS_DATASOUCE_IDX:
                prepareModularExtractsDataSource(model);
                break;
            case IDAO.SINGLE_PROJECTS_DATASOUCE_IDX:
                prepareSingleProjectsDataSource(model);
                break;
            case IDAO.SINGLE_EXTRACTS_DATASOUCE_IDX:
                prepareSingleExtractsDataSource(model);
                break;
            case IDAO.LIST_ORDERS_DATASOUCE_IDX:
                prepareListOrdersDataSource(model);
                break;
            default:
                throw new IllegalStateException();  // Unsupported list type
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareModularProjectsDataSource(Model model)
    {
        MQBuilder employeePostAddSubBuilder = new MQBuilder(EmployeePostAddExtract.ENTITY_CLASS, "epe", new String[]{EmployeePostAddExtract.P_ID});
        employeePostAddSubBuilder.add(MQExpression.eq("epe", EmployeePostAddExtract.L_EMPLOYEE_POST, model.getEmployeePost()));

        MQBuilder employeeTransferSubBuilder = new MQBuilder(EmployeeTransferExtract.ENTITY_CLASS, "ete", new String[]{EmployeeTransferExtract.P_ID});
        employeeTransferSubBuilder.add(MQExpression.eq("ete", EmployeeTransferExtract.L_EMPLOYEE_POST, model.getEmployeePost()));

        MQBuilder sciDegPaySubBuilder = new MQBuilder(ScienceDegreePaymentExtract.ENTITY_CLASS, "dpe", new String[]{ScienceDegreePaymentExtract.P_ID});
        sciDegPaySubBuilder.add(MQExpression.isNotNull("dpe", ScienceDegreePaymentExtract.employeePostNew()));
        sciDegPaySubBuilder.add(MQExpression.eq("dpe", ScienceDegreePaymentExtract.employeePostNew(), model.getEmployeePost()));

        MQBuilder sciStatPaySubBuilder = new MQBuilder(ScienceStatusPaymentExtract.ENTITY_CLASS, "spe", new String[]{ScienceStatusPaymentExtract.P_ID});
        sciStatPaySubBuilder.add(MQExpression.isNotNull("spe", ScienceStatusPaymentExtract.employeePostNew()));
        sciStatPaySubBuilder.add(MQExpression.eq("spe", ScienceStatusPaymentExtract.employeePostNew(), model.getEmployeePost()));

        MQBuilder builder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.isNull("e", IAbstractExtract.L_PARAGRAPH));
        AbstractExpression expr1 = MQExpression.eq("e", IAbstractExtract.L_ENTITY, model.getEmployeePost());
        AbstractExpression expr2 = MQExpression.in("e", IAbstractExtract.P_ID, employeePostAddSubBuilder);
        AbstractExpression expr3 = MQExpression.in("e", IAbstractExtract.P_ID, employeeTransferSubBuilder);
        AbstractExpression expr4 = MQExpression.in("e", IAbstractExtract.P_ID, sciDegPaySubBuilder);
        AbstractExpression expr5 = MQExpression.in("e", IAbstractExtract.P_ID, sciStatPaySubBuilder);
        builder.add(MQExpression.or(expr1, expr2, expr3, expr4, expr5));

        new OrderDescriptionRegistry("e").applyOrder(builder, model.getModularProjectsModel().getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getModularProjectsModel().getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareModularExtractsDataSource(Model model)
    {
        MQBuilder employeePostAddSubBuilder = new MQBuilder(EmployeePostAddExtract.ENTITY_CLASS, "epe", new String[]{EmployeePostAddExtract.P_ID});
        employeePostAddSubBuilder.add(MQExpression.eq("epe", EmployeePostAddExtract.L_EMPLOYEE_POST, model.getEmployeePost()));

        MQBuilder employeeTransferSubBuilder = new MQBuilder(EmployeeTransferExtract.ENTITY_CLASS, "ete", new String[]{EmployeeTransferExtract.P_ID});
        employeeTransferSubBuilder.add(MQExpression.eq("ete", EmployeeTransferExtract.L_EMPLOYEE_POST, model.getEmployeePost()));

        MQBuilder sciDegPaySubBuilder = new MQBuilder(ScienceDegreePaymentExtract.ENTITY_CLASS, "dpe", new String[]{ScienceDegreePaymentExtract.P_ID});
        sciDegPaySubBuilder.add(MQExpression.isNotNull("dpe", ScienceDegreePaymentExtract.employeePostNew()));
        sciDegPaySubBuilder.add(MQExpression.eq("dpe", ScienceDegreePaymentExtract.employeePostNew(), model.getEmployeePost()));

        MQBuilder sciStatPaySubBuilder = new MQBuilder(ScienceStatusPaymentExtract.ENTITY_CLASS, "spe", new String[]{ScienceStatusPaymentExtract.P_ID});
        sciStatPaySubBuilder.add(MQExpression.isNotNull("spe", ScienceStatusPaymentExtract.employeePostNew()));
        sciStatPaySubBuilder.add(MQExpression.eq("spe", ScienceStatusPaymentExtract.employeePostNew(), model.getEmployeePost()));

        MQBuilder builder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.isNotNull("e", IAbstractExtract.L_PARAGRAPH));
        AbstractExpression expr1 = MQExpression.eq("e", IAbstractExtract.L_ENTITY, model.getEmployeePost());
        AbstractExpression expr2 = MQExpression.in("e", IAbstractExtract.P_ID, employeePostAddSubBuilder);
        AbstractExpression expr3 = MQExpression.in("e", IAbstractExtract.P_ID, employeeTransferSubBuilder);
        AbstractExpression expr4 = MQExpression.in("e", IAbstractExtract.P_ID, sciDegPaySubBuilder);
        AbstractExpression expr5 = MQExpression.in("e", IAbstractExtract.P_ID, sciStatPaySubBuilder);
        builder.add(MQExpression.or(expr1, expr2, expr3, expr4, expr5));

        new OrderDescriptionRegistry("e").applyOrder(builder, model.getModularExtractsModel().getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getModularExtractsModel().getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareSingleProjectsDataSource(Model model)
    {
        MQBuilder employeePostAddSubBuilder = new MQBuilder(EmployeePostAddSExtract.ENTITY_CLASS, "epse", new String[]{EmployeePostAddSExtract.P_ID});
        employeePostAddSubBuilder.add(MQExpression.eq("epse", EmployeePostAddSExtract.L_EMPLOYEE_POST, model.getEmployeePost()));

        MQBuilder employeeTransferSubBuilder = new MQBuilder(EmployeeTransferSExtract.ENTITY_CLASS, "etse", new String[]{EmployeeTransferSExtract.P_ID});
        employeeTransferSubBuilder.add(MQExpression.eq("etse", EmployeeTransferSExtract.L_EMPLOYEE_POST, model.getEmployeePost()));

        MQBuilder builder = new MQBuilder(SingleEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.in("e", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        AbstractExpression expr1 = MQExpression.eq("e", IAbstractExtract.L_ENTITY, model.getEmployeePost());
        AbstractExpression expr2 = MQExpression.in("e", IAbstractExtract.P_ID, employeePostAddSubBuilder);
        AbstractExpression expr3 = MQExpression.in("e", IAbstractExtract.P_ID, employeeTransferSubBuilder);
        builder.add(MQExpression.or(expr1, expr2, expr3));

        new OrderDescriptionRegistry("e").applyOrder(builder, model.getSingleProjectsModel().getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getSingleProjectsModel().getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareSingleExtractsDataSource(Model model)
    {
        MQBuilder employeePostAddSubBuilder = new MQBuilder(EmployeePostAddSExtract.ENTITY_CLASS, "epse", new String[]{EmployeePostAddSExtract.P_ID});
        employeePostAddSubBuilder.add(MQExpression.eq("epse", EmployeePostAddSExtract.L_EMPLOYEE_POST, model.getEmployeePost()));

        MQBuilder employeeTransferSubBuilder = new MQBuilder(EmployeeTransferSExtract.ENTITY_CLASS, "etse", new String[]{EmployeeTransferSExtract.P_ID});
        employeeTransferSubBuilder.add(MQExpression.eq("etse", EmployeeTransferSExtract.L_EMPLOYEE_POST, model.getEmployeePost()));

        MQBuilder builder = new MQBuilder(SingleEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.notIn("e", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        AbstractExpression expr1 = MQExpression.eq("e", IAbstractExtract.L_ENTITY, model.getEmployeePost());
        AbstractExpression expr2 = MQExpression.in("e", IAbstractExtract.P_ID, employeePostAddSubBuilder);
        AbstractExpression expr3 = MQExpression.in("e", IAbstractExtract.P_ID, employeeTransferSubBuilder);
        builder.add(MQExpression.or(expr1, expr2, expr3));

        new OrderDescriptionRegistry("e").applyOrder(builder, model.getSingleExtractsModel().getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getSingleExtractsModel().getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListOrdersDataSource(Model model)
    {
        MQBuilder employeePostAddSubBuilder = new MQBuilder(EmployeeTransferEmplListExtract.ENTITY_CLASS, "t", new String[]{EmployeeTransferEmplListExtract.P_ID});
        employeePostAddSubBuilder.add(MQExpression.eq("t", EmployeeTransferEmplListExtract.L_EMPLOYEE_POST, model.getEmployeePost()));

        MQBuilder builder = new MQBuilder(ListEmployeeExtract.ENTITY_CLASS, "e");
        AbstractExpression eq = MQExpression.eq("e", IAbstractExtract.L_ENTITY, model.getEmployeePost());
        AbstractExpression expr1 = MQExpression.in("e", IAbstractExtract.P_ID, employeePostAddSubBuilder);
        builder.add(MQExpression.or(eq, expr1));

        new OrderDescriptionRegistry("e").applyOrder(builder, model.getListOrdersModel().getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getListOrdersModel().getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareOrdersDataSource(Model model)
    {
        DQLSelectBuilder sEmployeePostAddSubBuilder = new DQLSelectBuilder().fromEntity(EmployeePostAddSExtract.class, "epse").column(property(EmployeePostAddSExtract.id().fromAlias("epse")))
                .where(eqValue(property(EmployeePostAddSExtract.employeePost().fromAlias("epse")), model.getEmployeePost()));

        DQLSelectBuilder sEmployeeTransferSubBuilder = new DQLSelectBuilder().fromEntity(EmployeeTransferSExtract.class, "etse").column(property(EmployeeTransferSExtract.id().fromAlias("etse")))
                .where(eqValue(property(EmployeeTransferSExtract.employeePost().fromAlias("etse")), model.getEmployeePost()));

        DQLSelectBuilder modEmployeePostAddSubBuilder = new DQLSelectBuilder().fromEntity(EmployeePostAddExtract.class, "epe").column(property(EmployeePostAddExtract.id().fromAlias("epe")))
                .where(eqValue(property(EmployeePostAddExtract.employeePost().fromAlias("epe")), model.getEmployeePost()));

        DQLSelectBuilder modEmployeeTransferSubBuilder = new DQLSelectBuilder().fromEntity(EmployeeTransferExtract.class, "ete").column(property(EmployeeTransferExtract.id().fromAlias("ete")))
                .where(eqValue(property(EmployeeTransferExtract.employeePost().fromAlias("ete")), model.getEmployeePost()));

        DQLSelectBuilder sciDegPaySubBuilder = new DQLSelectBuilder().fromEntity(ScienceDegreePaymentExtract.class, "dpe").column(property(ScienceDegreePaymentExtract.id().fromAlias("dpe")))
                .where(isNotNull(property(ScienceDegreePaymentExtract.employeePostNew().fromAlias("dpe"))))
                .where(eqValue(property(ScienceDegreePaymentExtract.employeePostNew().fromAlias("dpe")), model.getEmployeePost()));

        DQLSelectBuilder sciStatPaySubBuilder = new DQLSelectBuilder().fromEntity(ScienceStatusPaymentExtract.class, "spe").column(property(ScienceStatusPaymentExtract.id().fromAlias("spe")))
                .where(isNotNull(property(ScienceStatusPaymentExtract.employeePostNew().fromAlias("spe"))))
                .where(eqValue(property(ScienceStatusPaymentExtract.employeePostNew().fromAlias("spe")), model.getEmployeePost()));

        DQLSelectBuilder employeePostAddSubBuilder = new DQLSelectBuilder().fromEntity(EmployeeTransferEmplListExtract.class, "t").column(property(EmployeeTransferEmplListExtract.id().fromAlias("t")))
                .where(eqValue(property(EmployeeTransferEmplListExtract.employeePost().fromAlias("t")), model.getEmployeePost()));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, "e")
                .where(eqValue(property(AbstractEmployeeExtract.state().code().fromAlias("e")), UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        IDQLExpression expr1 = eqValue(property(AbstractEmployeeExtract.entity().fromAlias("e")), model.getEmployeePost());
        IDQLExpression expr2 = in(property(AbstractEmployeeExtract.id().fromAlias("e")), sEmployeePostAddSubBuilder.buildQuery());
        IDQLExpression expr3 = in(property(AbstractEmployeeExtract.id().fromAlias("e")), sEmployeeTransferSubBuilder.buildQuery());
        IDQLExpression expr4 = in(property(AbstractEmployeeExtract.id().fromAlias("e")), sciDegPaySubBuilder.buildQuery());
        IDQLExpression expr5 = in(property(AbstractEmployeeExtract.id().fromAlias("e")), sciStatPaySubBuilder.buildQuery());
        IDQLExpression expr6 = in(property(AbstractEmployeeExtract.id().fromAlias("e")), modEmployeePostAddSubBuilder.buildQuery());
        IDQLExpression expr7 = in(property(AbstractEmployeeExtract.id().fromAlias("e")), modEmployeeTransferSubBuilder.buildQuery());
        IDQLExpression expr8 = in(property(AbstractEmployeeExtract.id().fromAlias("e")), employeePostAddSubBuilder.buildQuery());
        builder.where(or(expr1, expr2, expr3, expr4, expr5, expr6, expr7, expr8));

        new DQLOrderDescriptionRegistry(AbstractEmployeeExtract.class, "e").applyOrder(builder, model.getOrdersModel().getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getOrdersModel().getDataSource(), builder, getSession());

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getOrdersModel().getDataSource()))
        {
            if (wrapper.getEntity() instanceof ModularEmployeeExtract)
            {
                IAbstractParagraph paragraph = ((IAbstractExtract) wrapper.getEntity()).getParagraph();

                wrapper.setViewProperty(IDAO.P_PARAGRAPH_NUMBER, "");
                wrapper.setViewProperty(IDAO.P_EXTRACT_NUMBER, paragraph == null ? "" : paragraph.getNumber());
            } else
            {
                IAbstractExtract extract = (IAbstractExtract) wrapper.getEntity();
                wrapper.setViewProperty(IDAO.P_EXTRACT_NUMBER, extract == null ? "" : (null != extract.getNumber() ? extract.getNumber() : ""));

                if (wrapper.getEntity() instanceof SingleEmployeeExtract)
                    wrapper.setViewProperty(IDAO.P_PARAGRAPH_NUMBER, "");
                else
                {
                    IAbstractParagraph paragraph = extract == null ? null : extract.getParagraph();
                    wrapper.setViewProperty(IDAO.P_PARAGRAPH_NUMBER, paragraph == null ? "" : paragraph.getNumber());
                }
            }
        }
    }

}