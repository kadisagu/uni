/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.commons;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.*;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author dseleznev
 * Created on: 21.01.2009
 */
public class CommonExtractUtil
{
    public static void createPostBoundedModel(final IUniBaseDao coreDao, final Session session, boolean isAddForm, final IEmployeePostModel<? extends IPostAssignExtract> model)
    {
        model.setPostTypesList(coreDao.getCatalogItemList(PostType.class));
        model.setEtksLevelsList(coreDao.getCatalogItemListOrderByCode(EtksLevels.class));
        model.setWeekWorkLoadsList(coreDao.getCatalogItemListOrderByCode(EmployeeWeekWorkLoad.class));
        model.setWorkWeekDurationsList(coreDao.getCatalogItemListOrderByCode(EmployeeWorkWeekDuration.class));
        model.setLabourContractTypesList(coreDao.getCatalogItemList(LabourContractType.class));

        model.setOrgUnitListModel(new OrgUnitAutocompleteModel());
        model.setPostRelationListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getExtract() == null ||  model.getExtract().getOrgUnit() == null)
                {
                    return ListResult.getEmpty();
                }

                int maxCount = UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnitCount(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), filter, null, null);
                List<PostBoundedWithQGandQL> resultList = new ArrayList<>();
                for (OrgUnitTypePostRelation postRelation : UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnit(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), filter, null, null, 50))
                    resultList.add(postRelation.getPostBoundedWithQGandQL());
                return new ListResult<>(resultList, maxCount);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = coreDao.get(PostBoundedWithQGandQL.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity))
                    return entity;
                if (model instanceof IEmployeePostExtract)
                    model.getExtract().setPostBoundedWithQGandQL(null);
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((PostBoundedWithQGandQL) value).getFullTitleWithSalary();
            }

        });

        model.setRaisingCoefficientListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getExtract().getPostBoundedWithQGandQL())
                {
                    return ListResult.getEmpty();
                }
                MQBuilder builder = new MQBuilder(SalaryRaisingCoefficient.ENTITY_CLASS, "rc");
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_POST, model.getExtract().getPostBoundedWithQGandQL().getPost()));
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_QUALIFICATION_LEVEL, model.getExtract().getPostBoundedWithQGandQL().getQualificationLevel()));
                builder.add(MQExpression.like("rc", SalaryRaisingCoefficient.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("rc", SalaryRaisingCoefficient.P_RAISING_COEFFICIENT);
                builder.addOrder("rc", SalaryRaisingCoefficient.P_TITLE);
                return new ListResult<>(builder.getResultList(session));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = coreDao.get(SalaryRaisingCoefficient.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                model.getExtract().setRaisingCoefficient(null);
                model.getExtract().setSalary(0d);
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((SalaryRaisingCoefficient) value).getFullTitle();
            }
        });
    }

    public static void createPaymentsBonusModel(final IUniBaseDao coreDao, final Session session, boolean isAddForm, IPaymentsBonusModel<? extends AbstractEmployeeExtract> model, boolean createRequiredPayments)
    {
        model.setPaymentTypesList(HierarchyUtil.listHierarchyNodesWithParents(coreDao.getList(PaymentType.class), true));
        model.setFinancingSourcesList(coreDao.getCatalogItemList(FinancingSource.class));
        model.setFinancingSourceItemsListModel(new BaseSingleSelectModel(FinancingSourceItem.P_FULL_TITLE)
        {
            @Override
            public ListResult<FinancingSourceItem> findValues(String filter)
            {
                return MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getFinancinSourceItemsListResult(filter);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return UniDaoFacade.getCoreDao().get(FinancingSourceItem.class, (Long)primaryKey);
            }
        });

        if (isAddForm)
        {
            if (model.getBonusNumbers().isEmpty())
            {
                // TODO: payments from staffListAllocationItem implementation
                List<Payment> paymentsToAddList = createRequiredPayments ? getRequiredPaymentsList(session) : new ArrayList<Payment>();

                if (paymentsToAddList.isEmpty())
                {
                    model.getBonusNumbers().add(1);
                    model.getBonusMap().put(1, new EmployeeBonus());
                    model.getPaymentListsMap().put(1, getPaymentAutocompleteModel(model, session));
                }
                else
                {
                    int index = 0;
                    for(Payment payment : paymentsToAddList)
                    {
                        index++;

                        EmployeeBonus bonus = new EmployeeBonus();
                        bonus.setPaymentType(payment.getType());
                        bonus.setPayment(payment);
                        bonus.setValue(null != payment.getValue() ? payment.getValue() : 0d);
                        bonus.setFinancingSource(payment.getFinancingSource());

                        model.getBonusNumbers().add(index);
                        model.getBonusMap().put(index, bonus);
                        model.getPaymentListsMap().put(index, getPaymentAutocompleteModel(model, session));
                    }
                }
            }
        }

        if (!isAddForm)
        {
            model.getBonusNumbers().clear();
            model.getBonusMap().clear();

            int i = 1;
            for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(model.getExtract()))
            {
                model.getPaymentListsMap().put(i, getPaymentAutocompleteModel(model, session));
                model.getBonusMap().put(i, bonus);
                model.getBonusNumbers().add(i++);
            }
        }
    }

    public static void reFillPaymentsWithStaffList(final IUniBaseDao coreDao, final Session session, IPaymentsBonusModel<? extends AbstractEmployeeExtract> model)
    {
        if (!(model.getExtract() instanceof IPostAssignExtract)) return;

        IPostAssignExtract extract = (IPostAssignExtract)model.getExtract();
        List<StaffListPostPayment> postPaymentsList = UniempDaoFacade.getStaffListDAO().getStaffListPostPaymentsList(extract.getOrgUnit(), extract.getPostBoundedWithQGandQL());
        if (null != postPaymentsList && !postPaymentsList.isEmpty())
        {
            model.getBonusNumbers().clear();
            model.getBonusMap().clear();

            int i = 1;
            for (StaffListPostPayment payment : postPaymentsList)
            {
                EmployeeBonus bonus = new EmployeeBonus();
                bonus.setPaymentType(payment.getPayment().getType());
                bonus.setPayment(payment.getPayment());
                bonus.setValue(payment.getAmount());
                bonus.setFinancingSource(payment.getFinancingSource());

                model.getPaymentListsMap().put(i, getPaymentAutocompleteModel(model, session));
                model.getBonusMap().put(i, bonus);
                model.getBonusNumbers().add(i++);
            }
        }
    }

    public static List<Payment> getRequiredPaymentsList(final Session session)
    {
        MQBuilder builder = new MQBuilder(Payment.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE));
        builder.add(MQExpression.eq("p", Payment.P_REQUIRED, Boolean.TRUE));
        return builder.getResultList(session);
    }

    public static ISelectModel getPaymentAutocompleteModel(final IPaymentsBonusModel<? extends AbstractEmployeeExtract> model, final Session session)
    {
        return new BaseSingleSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                return new ListResult(session.createCriteria(Payment.class).add(Restrictions.eq(Payment.L_TYPE, model.getCurrentEmployeeBonus().getPaymentType())).addOrder(Order.asc(Payment.P_TITLE)).list());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = UniDaoFacade.getCoreDao().get(Payment.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                return null;
            }
        };
    }

    public static void updateBonuses(final IUniBaseDao coreDao, final IPaymentsBonusModel<? extends AbstractEmployeeExtract> model, final Session session)
    {
        for (EmployeeBonus bonus : model.getBonusesToDel())
            coreDao.delete(bonus);

        if (!model.getBonusMap().isEmpty())
            for (EmployeeBonus bonus : model.getBonusMap().values())
            {
                if(bonus.getPayment().isOneTimePayment())
                {
                    bonus.setEndDate(null);
                    bonus.setBeginDate(bonus.getAssignDate());
                }
                bonus.setExtract(model.getExtract());
                session.saveOrUpdate(bonus);
            }
    }

    public static void addEmployeeBonus(final IPaymentsBonusModel<? extends AbstractEmployeeExtract> model, ISelectModel paymentAutocompleteModel)
    {
        model.getBonusMap().put(model.getBonusNumbers().size() + 1, new EmployeeBonus());
        model.getPaymentListsMap().put(model.getBonusNumbers().size() + 1, paymentAutocompleteModel);
        model.getBonusNumbers().add(model.getBonusNumbers().size() + 1);
    }

    public static void deleteEmployeeBonus(final IPaymentsBonusModel<? extends AbstractEmployeeExtract> model, Object bonusId, ISelectModel paymentAutocompleteModel)
    {
        if(null != model.getBonusMap().get(bonusId).getId())
            model.getBonusesToDel().add(model.getBonusMap().get(bonusId));

        model.getPaymentListsMap().clear();
        model.getBonusMap().remove(bonusId);
        //model.getPaymentListsMap().remove(bonusId);
        model.getBonusNumbers().remove(((Integer)bonusId) - 1);

        int i = 1;
        List<Integer> newBonusNumbersList = new ArrayList<>();
        for (Integer number : model.getBonusNumbers())
        {
            EmployeeBonus bonus = model.getBonusMap().get(number);
            model.getBonusMap().remove(number);
            model.getBonusMap().put(i, bonus);
            model.getPaymentListsMap().put(i, paymentAutocompleteModel);
            newBonusNumbersList.add(i++);
        }
        model.setBonusNumbers(newBonusNumbersList);
    }

    public static String getBonusListStr(AbstractEmployeeExtract extract)
    {
        String sFormatted = getFormattedBonusListStrHtml(extract);
        if (!sFormatted.isEmpty())
            return sFormatted;

        StringBuilder result = new StringBuilder();
        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
        {
            if (result.length() > 0) result.append(";<br/>");
            result.append(bonus.getPayment().getTitle());
            result.append(", ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()));
            result.append(" (").append(bonus.getPayment().getPaymentUnit().getTitle()).append(")");
            result.append(", ").append(StringUtils.lowerCase(bonus.getFinancingSourceTitle()));
            if (null != bonus.getFinancingSourceItem())
                result.append(" (").append(bonus.getFinancingSourceItem().getTitle()).append(")");
        }
        return result.toString();
    }

    public static String getBonusListStrRtf(AbstractEmployeeExtract extract)
    {
        StringBuilder result = new StringBuilder();
        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
        {
            if (result.length() > 0) result.append("; ");
            result.append(bonus.getPayment().getTitle()).append(" в размере ");
            result.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()));

            if(UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                result.append(" части от должностного оклада");
            else if(UniempDefines.PAYMENT_UNIT_RUBLES.equals(bonus.getPayment().getPaymentUnit().getCode()))
                result.append(" руб.");
            else if(UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                result.append("% от должностного оклада");
            else if(UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                result.append("% от заработной платы с учетом всех выплат");

            if(UniempDefines.FINANCING_SOURCE_BUDGET.equals(bonus.getFinancingSourceCode()))
                result.append(" с оплатой из средств федерального бюджета");
            else if(UniempDefines.FINANCING_SOURCE_OFF_BUDGET.equals(bonus.getFinancingSourceCode()))
                result.append(" из внебюджетных средств");

            if (null != bonus.getFinancingSourceItem())
            {
                result.append(" ");
                if(null != bonus.getFinancingSourceItem().getOrgUnit())
                    result.append(CommonExtractPrintUtil.getOrgUnitPrintingTitle(bonus.getFinancingSourceItem().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, false, true));
                else
                    result.append(bonus.getFinancingSourceItem().getTitle());
            }
        }
        return result.toString();
    }

    public static String getListOrderBonusListStrRtf(AbstractEmployeeExtract extract)
    {
        StringBuilder result = new StringBuilder();
        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
        {
            if (result.length() > 0) result.append("; ");
            result.append(bonus.getPayment().getShortTitle()).append(" в размере ");
            result.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()));

            if(UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                result.append(" части от должностного оклада");
            else if(UniempDefines.PAYMENT_UNIT_RUBLES.equals(bonus.getPayment().getPaymentUnit().getCode()))
                result.append(" руб.");
            else if(UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                result.append("%");
            else if(UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                result.append("%");

            if(UniempDefines.FINANCING_SOURCE_BUDGET.equals(bonus.getFinancingSourceCode()))
                result.append(" (бюдджет)");
            else if(UniempDefines.FINANCING_SOURCE_OFF_BUDGET.equals(bonus.getFinancingSourceCode()))
                result.append(" (внебюджетные средства)");
        }
        return result.toString();
    }

    public static String getFormattedBonusListStrRtf(AbstractEmployeeExtract extract)
    {
        return getFormattedBonusListStr(extract, false);
    }

    public static String getFormattedBonusListStrHtml(AbstractEmployeeExtract extract)
    {
        return getFormattedBonusListStr(extract, true);
    }

    protected static String getFormattedBonusListStr(AbstractEmployeeExtract extract, boolean forHtml)
    {
        StringBuilder result = new StringBuilder();

        String sSeparator = ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSettings.DAO.getSeparator();
        if(null == sSeparator) sSeparator = "; ";

        if (forHtml) sSeparator += "<br/>";

        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
        {
            if (result.length() > 0) result.append(sSeparator);
            result.append(expandPaymentFormatString(
                    MoveEmployeeDaoFacade.getMoveEmployeeDao().getPaymentFormatString(extract.getType().getCode(), bonus.getPayment()), bonus, false));
        }

        return result.toString();
    }

    public static String getFormattedBonusListStr(List<EmployeePayment> paymentList, String documentCode, boolean forHtml)
    {
        StringBuilder result = new StringBuilder();

        String sSeparator = ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSettings.DAO.getSeparator();
        if(null == sSeparator) sSeparator = "; ";

        if (forHtml) sSeparator += "<br/>";

        for (EmployeePayment payment : paymentList)
        {
            if (result.length() > 0) result.append(sSeparator);
            result.append(expandPaymentFormatString(
                    MoveEmployeeDaoFacade.getMoveEmployeeDao().getPaymentFormatString(documentCode, payment.getPayment()), payment, false));
        }

        return result.toString();
    }

    public static String expandPaymentFormatString(String sFormatString, EmployeeBonus bonus, boolean allowExampleOutput)
    {
        if(null == sFormatString)
        {
            StringBuilder simpleFormatted = new StringBuilder(bonus.getPayment().getTitle());
            simpleFormatted.append(" в размере ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()));
            simpleFormatted.append(" ").append(bonus.getPayment().getPaymentUnit().getShortTitle().toLowerCase()).append(" ");
            simpleFormatted.append(" из ");
            if (bonus.getFinancingSource() != null)
                simpleFormatted.append(bonus.getFinancingSource().getGenitiveCaseTitle() != null ? bonus.getFinancingSource().getGenitiveCaseTitle() : bonus.getFinancingSource().getTitle());
            else
                simpleFormatted.append("____________________________________________");
            if(null != bonus.getFinancingSourceItem())
                simpleFormatted.append(" ").append(null != bonus.getFinancingSourceItem().getGenitiveCaseTitle() ? bonus.getFinancingSourceItem().getGenitiveCaseTitle() : bonus.getFinancingSourceItem().getTitle());
            return simpleFormatted.toString();
        }

        List<PaymentPrintFormatLabel> labels = UniDaoFacade.getCoreDao().getList(PaymentPrintFormatLabel.class);
        Map<Integer, PaymentPrintFormatLabel> pos2label = new HashMap<>();
        for(PaymentPrintFormatLabel label : labels)
        {
            int iIdx = 0;
            while(-1 < (iIdx = sFormatString.indexOf(label.getLabel(), iIdx)))
            {
                pos2label.put(iIdx, label);
                iIdx++;
            }
        }

        StringBuilder retVal = new StringBuilder();
        String sPrevNumber = null;
        for(int i = 0; sFormatString.length() > i; i++)
            if (pos2label.containsKey(i))
            {
                CoreCollectionUtils.Pair<String, Boolean> pair = expandSingleLabel(pos2label.get(i), bonus, sPrevNumber,
                        !allowExampleOutput, allowExampleOutput);
                retVal.append(pair.getX());
                sPrevNumber = pair.getY() ? pair.getX() : null;
                i += pos2label.get(i).getLabel().length() - 1;
            }
            else
                retVal.append(sFormatString.charAt(i));

        return retVal.toString();
    }

    public static String expandPaymentFormatString(String sFormatString, EmployeePayment payment, boolean allowExampleOutput)
    {
        if(null == sFormatString)
        {
            StringBuilder simpleFormatted = new StringBuilder(payment.getPayment().getTitle());
            simpleFormatted.append(" в размере ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(payment.getPaymentValue()));
            simpleFormatted.append(" ").append(payment.getPayment().getPaymentUnit().getShortTitle().toLowerCase()).append(" ");
            simpleFormatted.append(" из ");
            if (payment.getFinancingSource() != null)
                simpleFormatted.append(payment.getFinancingSource().getGenitiveCaseTitle() != null ? payment.getFinancingSource().getGenitiveCaseTitle() : payment.getFinancingSource().getTitle());
            else
                simpleFormatted.append("____________________________________________");
            if(null != payment.getFinancingSourceItem())
                simpleFormatted.append(" ").append(null != payment.getFinancingSourceItem().getGenitiveCaseTitle() ? payment.getFinancingSourceItem().getGenitiveCaseTitle() : payment.getFinancingSourceItem().getTitle());
            return simpleFormatted.toString();
        }

        List<PaymentPrintFormatLabel> labels = UniDaoFacade.getCoreDao().getList(PaymentPrintFormatLabel.class);
        Map<Integer, PaymentPrintFormatLabel> pos2label = new HashMap<>();
        for(PaymentPrintFormatLabel label : labels)
        {
            int iIdx = 0;
            while(-1 < (iIdx = sFormatString.indexOf(label.getLabel(), iIdx)))
            {
                pos2label.put(iIdx, label);
                iIdx++;
            }
        }

        StringBuilder retVal = new StringBuilder();
        String sPrevNumber = null;
        for(int i = 0; sFormatString.length() > i; i++)
            if (pos2label.containsKey(i))
            {
                CoreCollectionUtils.Pair<String, Boolean> pair = expandSingleLabel(pos2label.get(i), payment, sPrevNumber,
                        !allowExampleOutput, allowExampleOutput);
                retVal.append(pair.getX());
                sPrevNumber = pair.getY() ? pair.getX() : null;
                i += pos2label.get(i).getLabel().length() - 1;
            }
            else
                retVal.append(sFormatString.charAt(i));

        return retVal.toString();
    }

    public static CoreCollectionUtils.Pair<String, Boolean> expandSingleLabel(PaymentPrintFormatLabel label, EmployeeBonus bonus, String sPrevNumber, boolean eatPlaceholders, boolean allowExampleOutput)
    {
        if (null == bonus)
            return new CoreCollectionUtils.Pair<>(
                    allowExampleOutput && null != label.getPropertyExample() ? label.getPropertyExample()
                            : eatPlaceholders ? "" : label.getLabel(), false);

        Object val = FastBeanUtils.getValue(bonus, label.getPropertyPath());

        if (null == val)
            return new CoreCollectionUtils.Pair<>(eatPlaceholders ? "" : label.getLabel(), false);

        else if (val instanceof String || val instanceof Boolean)
            return new CoreCollectionUtils.Pair<>(String.format(label.getPropertyFormat(), val), false);

        else if (val instanceof Long || val instanceof Integer || val instanceof Double || val instanceof Float)
            return new CoreCollectionUtils.Pair<>(String.format(label.getPropertyFormat(), val), true);

        else if (val instanceof Date)
            return new CoreCollectionUtils.Pair<>(
                    new SimpleDateFormat(label.getPropertyFormat(), RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(val),
                    false);

        else if (val instanceof ITitledWithCases && val instanceof ITitledWithPluralCases
                && null != label.getNumberDependentDataCase())
            return new CoreCollectionUtils.Pair<>(
                    ((ITitledWithCases)val).getCaseTitle(getCaseForNumber(sPrevNumber, label.getNumberDependentDataCase())),
                    false);

        else if (val instanceof ITitled)
            return new CoreCollectionUtils.Pair<>(((ITitled)val).getTitle(), false);

        return new CoreCollectionUtils.Pair<>("", false);
    }

    public static CoreCollectionUtils.Pair<String, Boolean> expandSingleLabel(PaymentPrintFormatLabel label, EmployeePayment payment, String sPrevNumber, boolean eatPlaceholders, boolean allowExampleOutput)
    {
        if (null == payment)
            return new CoreCollectionUtils.Pair<>(
                    allowExampleOutput && null != label.getPropertyExample() ? label.getPropertyExample()
                            : eatPlaceholders ? "" : label.getLabel(), false);

        Object val = FastBeanUtils.getValue(payment, label.getEmployeePaymentPropertyPath());

        if (null == val)
            return new CoreCollectionUtils.Pair<>(eatPlaceholders ? "" : label.getLabel(), false);

        else if (val instanceof String || val instanceof Boolean)
            return new CoreCollectionUtils.Pair<>(String.format(label.getPropertyFormat(), val), false);

        else if (val instanceof Long || val instanceof Integer || val instanceof Double || val instanceof Float)
            return new CoreCollectionUtils.Pair<>(String.format(label.getPropertyFormat(), val), true);

        else if (val instanceof Date)
            return new CoreCollectionUtils.Pair<>(
                    new SimpleDateFormat(label.getPropertyFormat(), RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(val),
                    false);

        else if (val instanceof ITitledWithCases && val instanceof ITitledWithPluralCases
                && null != label.getNumberDependentDataCase())
            return new CoreCollectionUtils.Pair<>(
                    ((ITitledWithCases)val).getCaseTitle(getCaseForNumber(sPrevNumber, label.getNumberDependentDataCase())),
                    false);

        else if (val instanceof ITitled)
            return new CoreCollectionUtils.Pair<>(((ITitled)val).getTitle(), false);

        return new CoreCollectionUtils.Pair<>("", false);
    }

    public static GrammaCase getCaseForNumber(String sNumber, String sCase)
    {
        GrammaCase desiredCase = GrammaCase.valueOf(sCase);

        if (null == sNumber)
            return desiredCase;

        if (sNumber.contains(".") || sNumber.contains(","))
            return GrammaCase.GENITIVE;

        Double number = Double.valueOf(sNumber);

        boolean isTeens = 10 < number % 100 && 20 > number % 100;
        boolean is234 = 1 < number % 10 && 5 > number % 10;
        boolean is1 = 1 == number % 10;

        switch(desiredCase)
        {
            case NOMINATIVE:
                return !isTeens && is1 ? GrammaCase.NOMINATIVE : !isTeens && is234 ? GrammaCase.GENITIVE
                        : GrammaCase.GENITIVE_PL;
            case GENITIVE:
                return !isTeens && is1 ? GrammaCase.GENITIVE : GrammaCase.GENITIVE_PL;
            case DATIVE:
                return !isTeens && is1 ? GrammaCase.DATIVE : GrammaCase.DATIVE_PL;
            case ACCUSATIVE:
                return !isTeens && is1 ? GrammaCase.ACCUSATIVE : !isTeens && is234 ? GrammaCase.GENITIVE
                        : GrammaCase.GENITIVE_PL;
            case INSTRUMENTAL:
                return !isTeens && is1 ? GrammaCase.INSTRUMENTAL : GrammaCase.INSTRUMENTAL_PL;
            case PREPOSITIONAL:
                return !isTeens && is1 ? GrammaCase.PREPOSITIONAL : GrammaCase.PREPOSITIONAL_PL;
        }

        return desiredCase;
    }

    public static void onPaymentChange(final IPaymentsBonusModel<? extends AbstractEmployeeExtract> model, Integer bonusIdx, Double summStaffRate)
    {
        if (summStaffRate == null)
            summStaffRate = 1d;

        EmployeeBonus bonus = model.getBonusMap().get(bonusIdx);
        if (null != bonus.getPayment())
        {
            double paymentValue = 0d;
            if (model.getExtract() instanceof IPostAssignExtract)
            {
                IPostAssignExtract extract = (IPostAssignExtract)model.getExtract();
                PostBoundedWithQGandQL postBounded = extract.getPostBoundedWithQGandQL();
                paymentValue = UniempDaoFacade.getUniempDAO().getPaymentBaseValueForPost(bonus.getPayment(), postBounded);
            }

            if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(bonus.getPayment().getPaymentUnit().getCode()))
                bonus.setValue(paymentValue * summStaffRate);
            else
                bonus.setValue(paymentValue);

            bonus.setFinancingSource(bonus.getPayment().getFinancingSource());
        }
        else
        {
            bonus.setValue(0d);
            bonus.setFinancingSource(null);
        }
    }

    public static void onStaffRateForPaymentChange(final IPaymentsBonusModel<? extends AbstractEmployeeExtract> model, Double summStaffRate)
    {
        for(Integer bonusIdx : model.getBonusNumbers())
            if(null != model.getBonusMap().get(bonusIdx).getPayment() && UniempDefines.PAYMENT_UNIT_RUBLES.equals(model.getBonusMap().get(bonusIdx).getPayment().getPaymentUnit().getCode()))
                onPaymentChange(model, bonusIdx, summStaffRate);
    }

    public static String getModifiedFio(Person person, GrammaCase rusCase)
    {
        if(null == rusCase) return person.getFullFio();

        IdentityCard identityCard = person.getIdentityCard();
        boolean isMaleSex = identityCard.getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex));
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));
        return str.toString();
    }

    /**
     * @return Возвращает строку формата "ФАМИЛИЯ Имя Отчество"
     */
    public static String getModifiedFioUpperCaseLastName(Person person, GrammaCase rusCase)
    {
        if(null == rusCase) return person.getFullFio();

        IdentityCard identityCard = person.getIdentityCard();
        boolean isMaleSex = identityCard.getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex).toUpperCase());
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));
        return str.toString();
    }

    /**
     * @return Возвращает строку формата Фамилия И.О.
     */
    public static String getModifiedFioInitials(Person person, GrammaCase rusCase)
    {
        if(null == rusCase) return person.getFullFio();

        IdentityCard identityCard = person.getIdentityCard();
        boolean isMaleSex = identityCard.getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex));
        str.append(" ").append(identityCard.getFirstName().toCharArray()[0]).append(".");
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(identityCard.getMiddleName().toCharArray()[0]).append(".");
        return str.toString();
    }

    /**
     * @return Возвращает строку формата Фамилия И.О.
     */
    public static String getModifiedFioInitials(IdentityCard identityCard, GrammaCase rusCase)
    {
        if(null == rusCase) return identityCard.getFullFio();

        boolean isMaleSex = identityCard.getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex));
        str.append(" ").append(identityCard.getFirstName().toCharArray()[0]).append(".");
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(identityCard.getMiddleName().toCharArray()[0]).append(".");
        return str.toString();
    }

    public static PostType getAccessibleEmployeePostType(Employee employee, EmployeePost employeePost, Session session)
    {
        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_NAME, "ep");
        builder.add(MQExpression.eq("ep", EmployeePost.L_EMPLOYEE, employee));
        if(null != employeePost) builder.add(MQExpression.notEq("ep", EmployeePost.P_ID, employeePost.getId()));
        builder.add(MQExpression.eq("ep", EmployeePost.L_POST_TYPE + "." + PostType.P_CODE, UniDefines.POST_TYPE_MAIN_JOB));

        if(builder.getResultCount(session) > 0) return UniDaoFacade.getCoreDao().getCatalogItem(PostType.class, UniDefines.POST_TYPE_SECOND_JOB_INNER);
        else return UniDaoFacade.getCoreDao().getCatalogItem(PostType.class, UniDefines.POST_TYPE_MAIN_JOB);
    }
}