/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e17.AddEdit;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 09.09.2011
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<RevocationFromHolidayExtract>
{
    private List<HolidayType> _employeeHolidayList; //Список отпусков сотрудника
    private DynamicListDataSource _holidayDataSource; //ДатаСоурсе для сечлиста Предоставляемый отпуск
    private Map<IdentifiableWrapper, Date> _beginDayMap = new HashMap<IdentifiableWrapper, Date>(); //Мапа хранящая введенное значение даты начала части отпуска
    private Map<IdentifiableWrapper, Date> _endDayMap = new HashMap<IdentifiableWrapper, Date>(); //Мапа хранящая введенное значение даты окончания части отпуска
    private Map<IdentifiableWrapper, Integer> _durationDayMap = new HashMap<IdentifiableWrapper, Integer>(); //Мапа хранящая введенное значение кол-ва дней части отпуска
    private List<IdentifiableWrapper> _partHolidayWrapperList = new ArrayList<IdentifiableWrapper>(); //Мапа хранящая врапперы строк частей отпуска
    private Object _dummy; //Заглушка для задезабленных полей
    private DynamicListDataSource _dummyDataSource; //Заглушка для датаСоурса
    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add_item.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Location getLocation()
        {
            return null;  //Body of implemented method
        }
    };

    private IAsset _assetDisabled = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add_item_disabled.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Location getLocation()
        {
            return null;  //Body of implemented method
        }
    };

    //Getters & Setters

    public String getBeginDayId()
    {
        return getHolidayDataSource().getCurrentEntity().getId() + "_beginDay";
    }

    public String getEndDayId()
    {
        return getHolidayDataSource().getCurrentEntity().getId() + "_endDay";
    }

    public String getDurationDayId()
    {
        return getHolidayDataSource().getCurrentEntity().getId() + "_durationDayId";
    }

    public Date getBeginDay()
    {
        return getBeginDayMap().get(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity());
    }

    public void setBeginDay(Date beginDay)
    {
        getBeginDayMap().put(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity(), beginDay);
    }

    public Date getEndDay()
    {
        return getEndDayMap().get(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity());
    }

    public void setEndDay(Date endDay)
    {
        getEndDayMap().put(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity(), endDay);
    }

    public Integer getDurationDay()
    {
        return getDurationDayMap().get(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity());
    }

    public void setDurationDay(Integer durationDay)
    {
        getDurationDayMap().put(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity(), durationDay);
    }

    public IAsset getAssetDisabled()
    {
        return _assetDisabled;
    }

    public void setAssetDisabled(IAsset assetDisabled)
    {
        _assetDisabled = assetDisabled;
    }

    public DynamicListDataSource getDummyDataSource()
    {
        return _dummyDataSource;
    }

    public void setDummyDataSource(DynamicListDataSource dummyDataSource)
    {
        _dummyDataSource = dummyDataSource;
    }

    public DynamicListDataSource getHolidayDataSource()
    {
        return _holidayDataSource;
    }

    public void setHolidayDataSource(DynamicListDataSource holidayDataSource)
    {
        _holidayDataSource = holidayDataSource;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    public List<HolidayType> getEmployeeHolidayList()
    {
        return _employeeHolidayList;
    }

    public void setEmployeeHolidayList(List<HolidayType> employeeHolidayList)
    {
        _employeeHolidayList = employeeHolidayList;
    }

    public Map<IdentifiableWrapper, Date> getBeginDayMap()
    {
        return _beginDayMap;
    }

    public void setBeginDayMap(Map<IdentifiableWrapper, Date> beginDayMap)
    {
        _beginDayMap = beginDayMap;
    }

    public Map<IdentifiableWrapper, Date> getEndDayMap()
    {
        return _endDayMap;
    }

    public void setEndDayMap(Map<IdentifiableWrapper, Date> endDayMap)
    {
        _endDayMap = endDayMap;
    }

    public Map<IdentifiableWrapper, Integer> getDurationDayMap()
    {
        return _durationDayMap;
    }

    public void setDurationDayMap(Map<IdentifiableWrapper, Integer> durationDayMap)
    {
        _durationDayMap = durationDayMap;
    }

    public List<IdentifiableWrapper> getPartHolidayWrapperList()
    {
        return _partHolidayWrapperList;
    }

    public void setPartHolidayWrapperList(List<IdentifiableWrapper> partHolidayWrapperList)
    {
        _partHolidayWrapperList = partHolidayWrapperList;
    }

    public Object getDummy()
    {
        return _dummy;
    }

    public void setDummy(Object dummy)
    {
        _dummy = dummy;
    }
}