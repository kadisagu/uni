/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.commons;

import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * @author dseleznev
 * Created on: 11.11.2008
 */
public class ExtractListUtil
{
    public static void initExtractListModel(ExtractListModel model, String secPostfix)
    {
        IUniBaseDao dao = UniDaoFacade.getCoreDao();
        model.setSecModel(new CommonPostfixPermissionModel(secPostfix));
        model.setExtractTypeList(HierarchyUtil.listHierarchyNodesWithParents(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularExtractTypeList(), CommonCatalogUtil.CATALOG_CODE_COMPARATOR, false));
        model.setExtractStateList(dao.getCatalogItemList(ExtractStates.class));
    }

    // Column Data Base
    public static OrderDescriptionRegistry getOrderRegistry()
    {
        OrderDescriptionRegistry orderSettings = new OrderDescriptionRegistry("e");
        orderSettings.setOrders(ModularEmployeeExtract.EMPLOYEE_FIO_KEY, new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME));
        return orderSettings;
    }
}