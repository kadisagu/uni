/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e10;

import java.util.Map;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class EmployeeSurnameChangeExtractDao extends UniBaseDao implements IExtractComponentDao<EmployeeSurnameChangeExtract>
{
    @Override
    public void doCommit(EmployeeSurnameChangeExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        IdentityCard oldCard = extract.getIdentityCardOld();
        extract.setIdentityCardOld(oldCard);
        
        IdentityCard icard = new IdentityCard();
        icard.update(oldCard);
        icard.setCardType(extract.getIcCardType());
        icard.setFirstName(extract.getFirstName());
        icard.setLastName(extract.getLastName());
        icard.setMiddleName(extract.getMiddleName());
        icard.setNumber(extract.getIcNumber());
        icard.setSeria(extract.getIcSeria());
        icard.setIssuanceDate(extract.getIcIssuanceDate());
        icard.setIssuancePlace(extract.getIcIssuancePlace());

        if (oldCard.getPhoto() != null)                           // фото копируется из предыдущего
        {
            DatabaseFile photoNew = new DatabaseFile();
            photoNew.update(oldCard.getPhoto());
            save(photoNew);
            icard.setPhoto(photoNew);
        }

        if (oldCard.getAddress() != null)                         // адрес копируется из предыдущего
        {
            AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(icard, AddressBaseUtils.getSameAddress(oldCard.getAddress()), IdentityCard.L_ADDRESS);
        }
        save(icard);

        icard.getPerson().setIdentityCard(icard);
        update(icard.getPerson());

        extract.setIdentityCardNew(icard);
        update(extract);
    }

    @Override
    public void doRollback(EmployeeSurnameChangeExtract extract, Map parameters)
    {
        IdentityCard icardNew = extract.getIdentityCardNew();
        IdentityCard icardOld = extract.getIdentityCardOld();

        DatabaseFile photo = icardNew.getPhoto();
        if (photo != null && photo.equals(icardOld.getPhoto()))   // если в обоих УЛ ссылка на одну фотографию - создается новый объект и присваивается восстанавливаемому УЛ
        {
            DatabaseFile photoNew = new DatabaseFile();
            photoNew.update(icardNew.getPhoto());
            save(photoNew);
            icardOld.setPhoto(photoNew);
        }

        if (icardNew.getAddress() != null && icardNew.getAddress().equals(icardOld.getAddress()))  // если в обоих УЛ ссылка на один адрес - создается новый объект и присваивается восстанавливаемому УЛ
        {
            AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(icardOld, AddressBaseUtils.getSameAddress(icardNew.getAddress()), IdentityCard.L_ADDRESS);
        }

        update(icardOld);
        extract.getEmployee().getPerson().setIdentityCard(icardOld);
        update(extract.getEmployee().getPerson());

        extract.setIdentityCardNew(null);
        update(extract);

        delete(icardNew);
    }
}