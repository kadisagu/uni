/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;

import java.util.List;

/**
 * Create by ashaburov
 * Date 11.07.11
 *
 * <p>Интерфейс Модели, которая обслуживает выписки где назначаются ставки сотрудникам,<p>
 * т.е. используется SearchList Ставки.
 */
public interface IStaffRateListModel<T extends AbstractEmployeeExtract> extends ICommonEmployeeExtractAddEditModel<T>
{
    /** Геттер для DataSource сечлиста Ставка. */
    DynamicListDataSource<FinancingSourceDetails> getStaffRateDataSource();
    /** Сеттер для DataSource сечлиста Ставка. */
    void setStaffRateDataSource(DynamicListDataSource<FinancingSourceDetails> staffRateDataSource);

    /** Геттер для списка отображаемых(добавленных) долей ставки в сечлисте Ставка. */
    List<FinancingSourceDetails> getStaffRateItemList();
    /** Сеттер для списка отображаемых(добавленных) долей ставки в сечлисте Ставка. */
    void setStaffRateItemList(List<FinancingSourceDetails> staffRateItemList);

    /** Геттер для Модели автокомплитселекта Источник финансирования (детально)*/
    ISingleSelectModel getFinancingSourceItemModel();

    /** Геттер для списка source'ов селекта Источник финансирования */
    List<FinancingSource> getFinancingSourceModel();
    /** Сеттер для списка source'ов селекта Источник финансирования */
    void setFinancingSourceModel(List<FinancingSource> financingSourceModel);

    /** Геттер для Модели мультиселекта Кадровая расстановка*/
    IMultiSelectModel getEmployeeHRModel();

    /** Геттер для списка релешенов долей ставки в выписке и ставок Штатной расстановки */
    List<FinancingSourceDetailsToAllocItem> getDetailsToAllocItemList();
    /** Сеттер для списка релешенов долей ставки в выписке и ставок Штатной расстановки */
    void setDetailsToAllocItemList(List<FinancingSourceDetailsToAllocItem> detailsToAllocItemList);

    /** Геттер для поля определяющего необходимо ли отоброжать колонку Кадровая расстановка в сечлисте Ставка<p>
     * по умолчанию <tt>false</tt> */
    boolean isThereAnyActiveStaffList();
    /** Сеттер для поля определяющего необходимо ли отоброжать колонку Кадровая расстановка в сечлисте Ставка<p>
     * по умолчанию <tt>false</tt> */
    void setThereAnyActiveStaffList(boolean thereAnyActiveStaffList);

    /** Геттер определяющий необходимо ли обновить DataSource сечлиста Ставка<p>
     * по умолчанию <tt>true</tt> */
    boolean isNeedUpdateDataSource();
    /** Сеттер определяющий необходимо ли обновить DataSource сечлиста Ставка<p>
     * по умолчанию <tt>true</tt> */
    void setNeedUpdateDataSource(boolean needUpdateDataSource);
}
