/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e8.ListParagraphPub;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphPub.AbstractListParagraphPubController;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class Controller extends AbstractListParagraphPubController<PayForHolidayDayProvideHoldayEmplListExtract, Model, IDAO>
{
    @Override
    public void prepareColumn(DynamicListDataSource dataSource, Model model)
    {
        super.prepareColumn(dataSource, model);
        dataSource.addColumn(new SimpleColumn("Нерабочий праздничный день", PayForHolidayDayProvideHoldayEmplListExtract.holidayDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
    }
}
