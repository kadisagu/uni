/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e7;

import org.tandemframework.core.context.UserContext;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 08.12.2010
 */
public class EmployeeAddPaymentsSExtractDao extends UniBaseDao implements IExtractComponentDao<EmployeeAddPaymentsSExtract>
{
    @Override
    public void doCommit(EmployeeAddPaymentsSExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());
        if (null == contract && null != extract.getLabourContractType() && null != extract.getLabourContractNumber() && null != extract.getLabourContractDate())
        {
            contract = new EmployeeLabourContract();
            contract.setEmployeePost(extract.getEntity());
            contract.setType(extract.getLabourContractType());
            contract.setDate(extract.getLabourContractDate());
            contract.setBeginDate(extract.getBeginDate());
            contract.setEndDate(extract.getEndDate());
            contract.setNumber(extract.getLabourContractNumber());
            extract.setLabourContract(contract);
            getSession().save(contract);
        }

        if (null != extract.getCollateralAgreementNumber() && null != extract.getTransferDate())
        {
            ContractCollateralAgreement agreement = new ContractCollateralAgreement();
            agreement.setContract(contract);
            agreement.setDate(extract.getTransferDate());
            agreement.setNumber(extract.getCollateralAgreementNumber());
            agreement.setDescription("Дополнительное соглашение к трудовому договору");
            extract.setCollateralAgreement(agreement);
            getSession().save(agreement);
        }

        extract.setRaisingCoefficientOld(extract.getEntity().getRaisingCoefficient());
        if ( extract.getEntity().getSalary() != null )
            extract.setSalaryOld(extract.getEntity().getSalary());

        extract.getEntity().setRaisingCoefficient(extract.getRaisingCoefficient());
        extract.getEntity().setSalary(extract.getSalary());

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());

//        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> staffRateMap;
//        Double staffRateValue = null;
//        Double summStaffRate = 0d;
//        for (EmployeePostStaffRateItem item : staffRateItemList)
//            summStaffRate += item.getStaffRate();
//        if (staffRateItemList.size() > 1)
//        {
//            staffRateMap = new LinkedHashMap<>();
//            for (EmployeePostStaffRateItem item : staffRateItemList)
//                staffRateMap.put(new CoreCollectionUtils.Pair<>(item.getFinancingSource(), item.getFinancingSourceItem()), item.getStaffRate());
//        }
//        else
//        {
//            staffRateValue = staffRateItemList.get(0).getStaffRate();
//        }

        Double salary = 0d;
        if (extract.getRaisingCoefficient() != null)
            salary = extract.getRaisingCoefficient().getRecommendedSalary();
        else if (extract.getEntity() != null && extract.getEntity().getRaisingCoefficient() != null)
            salary = extract.getEntity().getRaisingCoefficient().getRecommendedSalary();
        else if (extract.getEntity() != null && extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary() != null)
            salary = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary();
        List<EmployeePayment> paymentList = CommonExtractCommitUtil.createOrAssignEmployeePaymentsList(getSession(), extract, extract.getEntity(), salary, staffRateItemList);

        CommonExtractCommitUtil.validateEmployeePayment(extract.getEntity(), paymentList, getSession());

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;

        for (EmployeePayment payment : paymentList)
            getSession().saveOrUpdate(payment);


        update(extract.getEntity());
        update(extract);
    }

    @Override
    public void doRollback(EmployeeAddPaymentsSExtract extract, Map parameters)
    {
        if (null != extract.getCollateralAgreement())
        {
            delete(extract.getCollateralAgreement());
        }

        if (null != extract.getLabourContract())
        {
            delete(extract.getLabourContract());
        }

        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
        {
            EmployeePayment payment = bonus.getEntity();
            bonus.setEntity(null);
            update(bonus);
            delete(payment);
        }

        extract.getEntity().setRaisingCoefficient(extract.getRaisingCoefficientOld());
        extract.getEntity().setSalary(extract.getSalaryOld());
        update(extract.getEntity());
    }
}