/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentStringReprFormatAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author esych
 * Created on: 17.01.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component), component);
    }

    public void onClickCopyLabel(IBusinessComponent component)
    {
        getDao().copyLabelToFormatString(getModel(component), component.<Long>getListenerParameter());
    }

    public void onClickGenerateExampleString(IBusinessComponent component)
    {
        getDao().setFormatStringExample(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }
}
