/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import java.util.List;
import java.util.Map;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;

/**
 * @author dseleznev
 * Created on: 21.01.2009
 */
public interface IPaymentsBonusModel<T extends AbstractEmployeeExtract> extends ICommonEmployeeExtractAddEditModel<T>
{
    // Filling of selects

    List<HSelectOption> getPaymentTypesList();

    void setPaymentTypesList(List<HSelectOption> paymentTypesList);


    List<FinancingSource> getFinancingSourcesList();

    void setFinancingSourcesList(List<FinancingSource> financingSourcesList);

    ISelectModel getFinancingSourceItemsListModel();

    void setFinancingSourceItemsListModel(ISelectModel financingSourceItemsListModel);


    // Additional variables 
    
    Integer getCurrentBonusNumber();

    void setCurrentBonusNumber(Integer currentBonusNumber);

    List<Integer> getBonusNumbers();

    void setBonusNumbers(List<Integer> bonusNumbers);

    Map<Integer, EmployeeBonus> getBonusMap();

    void setBonusMap(Map<Integer, EmployeeBonus> bonusMap);

    Map<Integer, ISelectModel> getPaymentListsMap();

    void setPaymentListsMap(Map<Integer, ISelectModel> paymentListsMap);

    EmployeeBonus getCurrentEmployeeBonus();

    ISelectModel getCurrentPaymentsList();

    String getCurrentBonusId();

    List<EmployeeBonus> getBonusesToDel();

    void setBonusesToDel(List<EmployeeBonus> bonusesToDel);
}