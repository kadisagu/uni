/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;

import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
public interface IAbstractListOrderPubDAO<T extends EmployeeListOrder, Model extends AbstractListOrderPubModel<T>> extends IUniDao<Model>
{
    EmployeeExtractType getExtractType(EmployeeListOrder order);

    void doSendToCoordination(Model model, IPersistentPersonable initiator);

    void doSendToFormative(Model model);

    void doReject(Model model);

    void doCommit(Model model);

    void doRollback(Model model);
}