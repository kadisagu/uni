/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e7.AddEdit;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.util.List;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 08.12.2010
 */
public class DAO extends CommonSingleEmployeeExtractAddEditDAO<EmployeeAddPaymentsSExtract, Model> implements IDAO
{
    @Override
    protected EmployeeAddPaymentsSExtract createNewInstance()
    {
        return new EmployeeAddPaymentsSExtract();
    }

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

//        CommonEmployeeExtractUtil.createPaymentsBonusModel(model);
        model.setLabourContractTypesList(getCatalogItemList(LabourContractType.class));

        model.setRaisingCoefficientListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getEmployeePost() || null == model.getEmployeePost().getPostRelation())
                {
                    return ListResult.getEmpty();
                }

                return new ListResult<SalaryRaisingCoefficient>(MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getRaisingCoefficientsList(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), filter));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get(SalaryRaisingCoefficient.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                model.getExtract().setRaisingCoefficient(null);
                model.getExtract().setSalary(0d);
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((SalaryRaisingCoefficient)value).getFullTitle();
            }
        });

        if (model.isAddForm())
        {
            if (null != model.getEmployeePost())
            {
                model.getExtract().setRaisingCoefficient(model.getEmployeePost().getRaisingCoefficient());
                if(model.getEmployeePost().getSalary() != null)
                {
                    model.getExtract().setSalary(model.getEmployeePost().getSalary());
                }
            }

            EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getEmployeePost());
            if (null != contract)
            {
                model.getExtract().setLabourContractNumber(contract.getNumber());
                model.getExtract().setLabourContractDate(contract.getBeginDate());
                model.getExtract().setLabourContractType(contract.getType());
            }
        }
        else
        {
            model.setEmployeePost(model.getExtract().getEntity());

            //подготавливаем блок с выплатами
            preparePaymentBlock(model);
        }

        model.setPaymentModel(CommonEmployeeExtractUtil.getPaymentModelForPaymentBlock());
        model.setFinSrcPaymentModel(CommonEmployeeExtractUtil.getFinSrcModelForPaymentBlock());
        model.setFinSrcItemPaymentModel(CommonEmployeeExtractUtil.getFinSrcItemModelForPaymentBlock(model.getPaymentDataSource()));
    }

    protected void preparePaymentBlock(Model model)
    {
        DQLSelectBuilder payBuilder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "b").column("b");
        payBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeBonus.extract().fromAlias("b")), model.getExtract()));

        model.setPaymentList(payBuilder.createStatement(getSession()).<EmployeeBonus>list());

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        List<StaffListPostPayment> paymentList = CommonEmployeeExtractUtil.checkEnabledAddStaffListPaymentButton(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), model.getEmployeePost().getOrgUnit(), model.getPaymentList(), staffRateItemList);

        if (!paymentList.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);
    }

    @Override
    public void preparePaymentDataSource(Model model)
    {
        CommonEmployeeExtractUtil.preparePaymentDataSourceForPaymentBlock(model.getPaymentList(), model.getPaymentDataSource());
    }

//    @Override
//    public void prepareListDataSource(Model model)
//    {
//        CommonEmployeeExtractUtil.fillPaymentsBonusDataSource(model);
//    }


    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if(model.getPaymentList().size() == 0)
            errors.add("Необходимо указать хотя бы одну выплату.");

        CommonEmployeeExtractUtil.validateForPaymentBlock(model.getPaymentList(), errors);
    }

    @Override
    public void update(Model model)
    {
        if (null != model.getEmployeePost().getId())
        {
            model.getExtract().setEntity(model.getEmployeePost());
            model.getExtract().setRaisingCoefficientOld(model.getEmployeePost().getRaisingCoefficient());
            if(model.getEmployeePost().getSalary() != null)
            {
                model.getExtract().setSalaryOld(model.getEmployeePost().getSalary());
            }
        }

        super.update(model);
//        CommonEmployeeExtractUtil.updateBonuses(model, getSession());

        //PAYMENTS
        //если редактируем приказ, то удаляем созданные ранее выплаты в выписке
        if (model.isEditForm())
        {
            for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getSavedPaymentsFromPaymentBlockDQLBuilder(model.getExtract()).createStatement(getSession()).<EmployeeBonus>list())
                delete(bonus);
        }

        //сохраняем выплаты
        for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getPaymentsForSaveFromPaymentBlock(model.getPaymentList()))
            save(bonus);
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }
}