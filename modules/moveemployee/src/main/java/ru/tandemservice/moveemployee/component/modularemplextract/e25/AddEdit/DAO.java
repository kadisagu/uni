/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e25.AddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.codes.EmployeeExtractGroupCodes;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.EmployeeExtractGroup;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.01.2012
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<RepealExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected RepealExtract createNewInstance()
    {
        return new RepealExtract();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareEmployeeStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getEmployeeStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getEmployeeStaffRateDataSource(), staffRateItems);
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        model.setGroupModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EmployeeExtractGroup.ENTITY_CLASS, "b");
                builder.add(MQExpression.notEq("b", EmployeeExtractGroup.code().s(), UniempDefines.EMPLOYEE_EXTRACT_GROUP_REPEAL));
                if (o != null)
                    builder.add(MQExpression.eq("b", EmployeeExtractGroup.id().s(), o));
                builder.addOrder("b", EmployeeExtractGroup.title().s());

                return new MQListResultBuilder(builder);
            }
        });

        model.setExtractModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getEmployeeExtractGroup() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                DQLSelectBuilder sEmployeePostAddSubBuilder = new DQLSelectBuilder().fromEntity(EmployeePostAddSExtract.class, "epse").column(property(EmployeePostAddSExtract.id().fromAlias("epse")))
                        .where(eqValue(property(EmployeePostAddSExtract.employeePost().fromAlias("epse")), model.getEmployeePost()));

                DQLSelectBuilder sEmployeeTransferSubBuilder = new DQLSelectBuilder().fromEntity(EmployeeTransferSExtract.class, "etse").column(property(EmployeeTransferSExtract.id().fromAlias("etse")))
                        .where(eqValue(property(EmployeeTransferSExtract.employeePost().fromAlias("etse")), model.getEmployeePost()));

                DQLSelectBuilder modEmployeePostAddSubBuilder = new DQLSelectBuilder().fromEntity(EmployeePostAddExtract.class, "epe").column(property(EmployeePostAddExtract.id().fromAlias("epe")))
                        .where(eqValue(property(EmployeePostAddExtract.employeePost().fromAlias("epe")), model.getEmployeePost()));

                DQLSelectBuilder modEmployeeTransferSubBuilder = new DQLSelectBuilder().fromEntity(EmployeeTransferExtract.class, "ete").column(property(EmployeeTransferExtract.id().fromAlias("ete")))
                        .where(eqValue(property(EmployeeTransferExtract.employeePost().fromAlias("ete")), model.getEmployeePost()));

                DQLSelectBuilder sciDegPaySubBuilder = new DQLSelectBuilder().fromEntity(ScienceDegreePaymentExtract.class, "dpe").column(property(ScienceDegreePaymentExtract.id().fromAlias("dpe")))
                        .where(isNotNull(property(ScienceDegreePaymentExtract.employeePostNew().fromAlias("dpe"))))
                        .where(eqValue(property(ScienceDegreePaymentExtract.employeePostNew().fromAlias("dpe")), model.getEmployeePost()));

                DQLSelectBuilder sciStatPaySubBuilder = new DQLSelectBuilder().fromEntity(ScienceStatusPaymentExtract.class, "spe").column(property(ScienceStatusPaymentExtract.id().fromAlias("spe")))
                        .where(isNotNull(property(ScienceStatusPaymentExtract.employeePostNew().fromAlias("spe"))))
                        .where(eqValue(property(ScienceStatusPaymentExtract.employeePostNew().fromAlias("spe")), model.getEmployeePost()));

                DQLSelectBuilder employeePostAddSubBuilder = new DQLSelectBuilder().fromEntity(EmployeeTransferEmplListExtract.class, "t").column(property(EmployeeTransferEmplListExtract.id().fromAlias("t")))
                        .where(eqValue(property(EmployeeTransferEmplListExtract.employeePost().fromAlias("t")), model.getEmployeePost()));

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, "b")
                .where(or(
                        eq(property(AbstractEmployeeExtract.entity().fromAlias("b")), value(model.getEmployeePost())),
                        in(property(AbstractEmployeeExtract.id().fromAlias("b")), sEmployeePostAddSubBuilder.buildQuery()),
                        in(property(AbstractEmployeeExtract.id().fromAlias("b")), sEmployeeTransferSubBuilder.buildQuery()),
                        in(property(AbstractEmployeeExtract.id().fromAlias("b")), modEmployeePostAddSubBuilder.buildQuery()),
                        in(property(AbstractEmployeeExtract.id().fromAlias("b")), modEmployeeTransferSubBuilder.buildQuery()),
                        in(property(AbstractEmployeeExtract.id().fromAlias("b")), sciDegPaySubBuilder.buildQuery()),
                        in(property(AbstractEmployeeExtract.id().fromAlias("b")), sciStatPaySubBuilder.buildQuery()),
                        in(property(AbstractEmployeeExtract.id().fromAlias("b")), employeePostAddSubBuilder.buildQuery())))
                .where(eq(property(AbstractEmployeeExtract.state().code().fromAlias("b")), value(UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED)))
                .where(eq(property(AbstractEmployeeExtract.type().group().fromAlias("b")), value(model.getExtract().getEmployeeExtractGroup())))
                .where(isNotNull(property(AbstractEmployeeExtract.paragraph().fromAlias("b"))))
                .where(isNotNull(property(AbstractEmployeeExtract.paragraph().order().fromAlias("b"))));
                if (o != null)
                    builder.where(eq(property(AbstractEmployeeExtract.id().fromAlias("b")), commonValue(o, PropertyType.LONG)));
                builder.order(property(AbstractEmployeeExtract.paragraph().order().commitDate().fromAlias("b")));

                return new DQLListResultBuilder(builder, 50);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                AbstractEmployeeExtract extract = (AbstractEmployeeExtract) value;
                StringBuilder resultBuilder = new StringBuilder();
                resultBuilder.append("Приказ №");
                resultBuilder.append(extract.getParagraph().getOrder().getNumber());
                resultBuilder.append(" от ");
                resultBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));
                resultBuilder.append(" «");
                resultBuilder.append(extract.getType().getTitle());
                resultBuilder.append("»");
                if (extract.getParagraph().getOrder() instanceof EmployeeListOrder)
                {
                    resultBuilder.append(" пар.");
                    resultBuilder.append(extract.getParagraph().getNumber());
                    if (extract.getParagraph().getOrder() instanceof ParagraphEmployeeListOrder)
                    {
                        resultBuilder.append(" пункт ");
                        resultBuilder.append(extract.getNumber());
                    }
                }

                return resultBuilder.toString();
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        MQBuilder builder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", AbstractEmployeeExtract.type().group().s(), model.getExtract().getEmployeeExtractGroup()));
        builder.add(MQExpression.eq("b", AbstractEmployeeExtract.entity().s(), model.getEmployeePost()));
        builder.add(MQExpression.eq("b", AbstractEmployeeExtract.state().code().s(), UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        builder.add(MQExpression.great("b", AbstractEmployeeExtract.paragraph().order().commitDateSystem().s(), model.getExtract().getAbstractEmployeeExtract().getParagraph().getOrder().getCommitDateSystem()));
        builder.addDescOrder("b", AbstractEmployeeExtract.paragraph().order().commitDateSystem().s());

        List<Object> extractList = builder.getResultList(getSession());
        if (!extractList.isEmpty() && !model.getExtract().getEmployeeExtractGroup().getCode().equals(EmployeeExtractGroupCodes.OTHER))
        {
            AbstractEmployeeExtract extract = (AbstractEmployeeExtract) extractList.get(0);
            errors.add("Выбранную выписку нельзя отменить, т.к. после нее был проведен приказ " + extract.getType().getTitle() +
                    " №" + extract.getParagraph().getOrder().getNumber() +
                    " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()) +
                    ".");
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());
    }
}