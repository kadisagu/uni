package ru.tandemservice.moveemployee.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.moveemployee.entity.gen.EmployeeModularOrderGen;
import ru.tandemservice.unimove.IAbstractExtract;

public class EmployeeModularOrder extends EmployeeModularOrderGen
{
    public static final String[] ORDER_TYPE_TITLE_KEY = new String[]{IAbstractExtract.L_TYPE, ICatalogItem.CATALOG_ITEM_TITLE};

    @Override
    public String getTitle()
    {
        return "Сборный приказ" + (getNumber() == null ? "" : " №" + getNumber()) + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate());
    }

    @Override
    public ICatalogItem getType()
    {
        return null;
    }

    //возвращает строку в формате "Сборный приказ №___ от xx.xx.xxxx (дата формирования xx.xx.xxxx xx:xx)"
    public String getTitleDate()
    {
        return "Сборный приказ" +
                (this.getNumber() == null ? "" : " №" + this.getNumber()) +
                (this.getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(this.getCommitDate())) +
                " (дата формирования "+ DateFormatter.DEFAULT_DATE_FORMATTER.format(this.getCreateDate()) + ")";
    }
}