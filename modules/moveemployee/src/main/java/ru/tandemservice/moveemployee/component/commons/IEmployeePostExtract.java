/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;

/**
 * @author dseleznev
 * Created on: 21.01.2009
 */
public interface IEmployeePostExtract
{
    PostBoundedWithQGandQL getPostBoundedWithQGandQL();

    void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL);
}