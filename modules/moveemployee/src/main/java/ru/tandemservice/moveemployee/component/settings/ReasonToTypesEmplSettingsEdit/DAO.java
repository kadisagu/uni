/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.settings.ReasonToTypesEmplSettingsEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToTypeRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.uni.dao.UniDao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author alikhanov
 * @since 18.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReason(get(EmployeeOrderReasons.class, model.getEmployeeOrderReasonsId()));
        model.setSelectedTypesList(model.getReason().getDocumentTypes());
        model.setEmployeeExtractTypesListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EmployeeExtractType.ENTITY_CLASS, "extr");
                builder.add(MQExpression.isNotNull("extr", EmployeeExtractType.L_PARENT));
                builder.add(MQExpression.eq("extr", EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
                builder.add(MQExpression.like("extr", EmployeeExtractType.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("extr", EmployeeExtractType.parent().title().s());
                builder.addOrder("extr", EmployeeExtractType.title().s());
                return new ListResult<>(builder.<EmployeeExtractType>getResultList(getSession()));
            }

            @Override
            public String getLabelFor(Object object, int i)
            {
                EmployeeExtractType extractType = (EmployeeExtractType) object;
                return extractType.getTitleWithParent();
            }
        });
    }

    @Override
    public void update(Model model)
    {
        List<Long> checkedReasonsList = new ArrayList<>();
        MQBuilder builder = new MQBuilder(EmployeeReasonToTypeRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", IEntityRelation.L_FIRST, model.getReason()));
        List<EmployeeReasonToTypeRel> relList = builder.getResultList(getSession());

        for(EmployeeReasonToTypeRel rel : relList)
        {
            if(!model.getSelectedTypesList().contains(rel))
                getSession().delete(rel);
            else
                checkedReasonsList.add(rel.getSecond().getId());
        }

        for(EmployeeExtractType basic : model.getSelectedTypesList())
        {
            if(!checkedReasonsList.contains(basic.getId()))
            {
                EmployeeReasonToTypeRel rel = new EmployeeReasonToTypeRel();
                rel.setFirst(model.getReason());
                rel.setSecond(basic);
                getSession().save(rel);
            }
        }

    }
}