/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.encouragementPrintFormatSettings.EncouragementPrintFormatEdit;

import org.tandemframework.core.component.Input;

import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

/**
 * Create by ashaburov
 * Date 29.09.11
 */
@Input(keys = "id", bindings = "id")
public class Model
{
    Long _id; //Идетификатор типа поощрений и наград
    EncouragementType _entity; // Тип Поощрений и наград, который будем редактировать
    String _printFormat; //Формат печати

    //Getters & Setter

    public String getPrintFormat()
    {
        return _printFormat;
    }

    public void setPrintFormat(String printFormat)
    {
        _printFormat = printFormat;
    }

    public EncouragementType getEntity()
    {
        return _entity;
    }

    public void setEntity(EncouragementType entity)
    {
        _entity = entity;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }
}
