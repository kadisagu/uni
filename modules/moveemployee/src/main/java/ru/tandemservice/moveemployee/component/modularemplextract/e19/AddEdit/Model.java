/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e19.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeActingExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.11.2011
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<EmployeeActingExtract>
{
    private ISingleSelectModel _missingEmployeePostModel;
    private ISingleSelectModel _missingOrgUnitModel;
    private ISingleSelectModel _missingPostModel;

    //Getters & Setters

    public ISingleSelectModel getMissingEmployeePostModel()
    {
        return _missingEmployeePostModel;
    }

    public void setMissingEmployeePostModel(ISingleSelectModel missingEmployeePostModel)
    {
        _missingEmployeePostModel = missingEmployeePostModel;
    }

    public ISingleSelectModel getMissingOrgUnitModel()
    {
        return _missingOrgUnitModel;
    }

    public void setMissingOrgUnitModel(ISingleSelectModel missingOrgUnitModel)
    {
        _missingOrgUnitModel = missingOrgUnitModel;
    }

    public ISingleSelectModel getMissingPostModel()
    {
        return _missingPostModel;
    }

    public void setMissingPostModel(ISingleSelectModel missingPostModel)
    {
        _missingPostModel = missingPostModel;
    }
}