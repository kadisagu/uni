/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e8;

import org.hibernate.Session;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.listemplextract.IOrderParagraphListCustomizer;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class PayForHolidayDayProvideHoldayEmplListExtractCustomizer implements IOrderParagraphListCustomizer
{
    @Override
    public void customizeParagraphList(DynamicListDataSource dataSource, IBusinessComponent component)
    {

    }

    @Override
    public void wrap(DynamicListDataSource dataSource, Session session)
    {

    }
}
