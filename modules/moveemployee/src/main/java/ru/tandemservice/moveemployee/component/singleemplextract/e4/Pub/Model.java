/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e4.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 18.09.2009
 */
public class Model extends SingleEmployeeExtractPubModel<EmployeeTransferSExtract>
{
    private boolean _employeePPS;
    private String _attributesBeforePage;
    private String _emplStaffRateStr;
    private String _staffRateStr;

    //Getters & Setters

    public String getEmplStaffRateStr()
    {
        return _emplStaffRateStr;
    }

    public void setEmplStaffRateStr(String emplStaffRateStr)
    {
        _emplStaffRateStr = emplStaffRateStr;
    }

    public String getStaffRateStr()
    {
        return _staffRateStr;
    }

    public void setStaffRateStr(String staffRateStr)
    {
        _staffRateStr = staffRateStr;
    }

    public boolean isEmployeePPS()
    {
        return _employeePPS;
    }

    public void setEmployeePPS(boolean employeePPS)
    {
        this._employeePPS = employeePPS;
    }

    public String getAttributesBeforePage()
    {
        return _attributesBeforePage;
    }

    public void setAttributesBeforePage(String attributesBeforePage)
    {
        this._attributesBeforePage = attributesBeforePage;
    }
}