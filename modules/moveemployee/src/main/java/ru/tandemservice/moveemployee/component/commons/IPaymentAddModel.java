/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.Payment;

/**
 * @author dseleznev
 * Created on: 14.10.2010
 * 
 * Данный интерфейс описывает набор методов, необходимых для работы базовых механизмов 
 * добавления выплат в выписке по назначению выплат сотруднику.
 * 
 * Используется для выписок о назначении выплат сотрудникам.
 */
public interface IPaymentAddModel<T extends AbstractEmployeeExtract> extends ICommonEmployeeExtractAddEditModel<T>
{
    /**
     * Геттер для списка добавленных в выписку выплат
     */
    DynamicListDataSource<EmployeeBonus> getBonusesDataSource();

    /**
     * Сеттер для списка добавленных в выписку выплат
     */
    void setBonusesDataSource(DynamicListDataSource<EmployeeBonus> bonusesDataSource);


    // МЕТОДЫ ДЛЯ БЛОКА ДОБАВЛЕНИЯ ВЫПЛАТЫ

    /**
     * Геттер для добавляемой единичной надбавки
     */
    EmployeeBonus getNewEmployeeBonus();

    /**
     * Сеттер для добавляемой единичной надбавки
     */
    void setNewEmployeeBonus(EmployeeBonus newEmployeeBonus);


    /**
     * Геттер для модели списка выплат
     */
    ISelectModel getPaymentListModel();

    /**
     * Сеттер для модели списка выплат
     */
    void setPaymentListModel(ISelectModel paymentListModel);


    /**
     * Геттер для списка источников финансирования
     */
    List<FinancingSource> getFinancingSourcesList();

    /**
     * Сеттер для списка источников финансирования
     */
    void setFinancingSourcesList(List<FinancingSource> financingSourcesList);


    /**
     * Геттер для списка источников финансирования (детально)
     */
    ISelectModel getFinancingSourceItemsListModel();

    /**
     * Сеттер для списка источников финансирования (детально)
     */
    void setFinancingSourceItemsListModel(ISelectModel financingSourceItemsListModel);


    /**
     * Метод для определения видимости финансирующего подразделения 
     */
    boolean isFinancingOrgUnitVisible();



    // ВСПОМОГАТЕЛЬНЫЕ МЕТОДЫ


    /**
     * Геттер для полного списка добавленных выплат
     */
    List<EmployeeBonus> getBonusesList();

    /**
     * Сеттер для полного списка добавленных выплат
     */
    void setBonusesList(List<EmployeeBonus> bonusesList);


    /**
     * Геттер для бонусов, которые необходимо удалить (на случай, если удаляются уже сохраненные выплаты)
     */
    List<EmployeeBonus> getBonusesToDel();

    /**
     * Сеттер для бонусов, которые необходимо удалить (на случай, если удаляются уже сохраненные выплаты)
     */
    void setBonusesToDel(List<EmployeeBonus> bonusesToDel);


    /**
     * Геттер для списка уже добавленных сотруднику выплат
     */
    List<Payment> getAlreadyAddedPayments();

    /**
     * Сеттер для списка уже добавленных сотруднику выплат
     */
    void setAlreadyAddedPayments(List<Payment> alreadyAddedPayments);
}