package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальный приказ по кадровому составу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeSingleExtractOrderGen extends AbstractEmployeeOrder
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder";
    public static final String ENTITY_NAME = "employeeSingleExtractOrder";
    public static final int VERSION_HASH = 2006992605;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeSingleExtractOrderGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeSingleExtractOrderGen> extends AbstractEmployeeOrder.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeSingleExtractOrder.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeSingleExtractOrder();
        }
    }
    private static final Path<EmployeeSingleExtractOrder> _dslPath = new Path<EmployeeSingleExtractOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeSingleExtractOrder");
    }
            

    public static class Path<E extends EmployeeSingleExtractOrder> extends AbstractEmployeeOrder.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EmployeeSingleExtractOrder.class;
        }

        public String getEntityName()
        {
            return "employeeSingleExtractOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
