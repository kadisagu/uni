/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSeparatorEdit;

import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;

import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * @author esych
 * Created on: 19.01.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setSeparator(ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSettings.DAO.getSeparator());
    }

    @Override
    public void update(Model model)
    {
        if (null != model.getSeparator())
        {
            IDataSettings settings = DataSettingsFacade.getSettings("general",
                MoveEmployeeDefines.PAYMENT_PRINT_FORMAT_SETTINGS_PREFIX);
            settings.set(MoveEmployeeDefines.PAYMENT_PRINT_FORMAT_SEPARATOR_SETTING, model.getSeparator());
            DataSettingsFacade.saveSettings(settings);
        }
    }
}
