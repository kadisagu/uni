package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.IContractAndAssigmentExtract;
import ru.tandemservice.moveemployee.entity.gen.IncDecCombinationExtractGen;

/**
 * Выписка из сборного приказа по кадровому составу. Увеличение (уменьшение) работы по свомещению
 */
public class IncDecCombinationExtract extends IncDecCombinationExtractGen implements IContractAndAssigmentExtract
{
}