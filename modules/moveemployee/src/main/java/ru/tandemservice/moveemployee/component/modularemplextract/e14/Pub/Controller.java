/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e14.Pub;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubController;
import ru.tandemservice.moveemployee.entity.ContractTerminationExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 18.03.2011
 */
public class Controller extends ModularEmployeeExtractPubController<ContractTerminationExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getModel(component).setAttributesBeforePage(getClass().getPackage().getName() + ".AttributesBefore");
        super.onRefreshComponent(component);

    }
}