/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e11;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.PaymentAssigningExtract;
import ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class PaymentAssigningExtractDao extends UniBaseDao implements IExtractComponentDao<PaymentAssigningExtract>
{
    @Override
    public void doCommit(PaymentAssigningExtract extract, Map parameters)
    {
        //Проверять при проведении приказа что выплаты, отмеченные как «снимаемые»,
        //должны включать дату проведения приказа в промежуток времени «Назначена с даты» - «Назначена по дату»
        //затем формируем соответствующую ошибку
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();
        List<EmployeePayment> removePaymentList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getRemovePaymentsListExtracts(extract);
        List<EmployeePayment> errPaymentList = new ArrayList<>();

        for (EmployeePayment payment : removePaymentList)
            if (extract.getParagraph().getOrder().getCommitDate() != null && payment.getEndDate() != null && extract.getParagraph().getOrder().getCommitDate().after(payment.getEndDate()))
                errPaymentList.add(payment);

        if (errPaymentList.size() != 0)
        {
            String errText = "Невозможно провести приказ поскольку выплаты ";

            if (errPaymentList.size() == 1)
                errText = "Невозможно провести приказ поскольку выплата " + ( errPaymentList.get(0).getPayment().getNominativeCaseTitle() != null ? errPaymentList.get(0).getPayment().getNominativeCaseTitle() : errPaymentList.get(0).getPayment().getFullTitle()) +
                " в размере " +(errPaymentList.get(0).getPayment().getValue() != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(errPaymentList.get(0).getPayment().getValue()) : "") +
                " " + (errPaymentList.get(0).getPayment().getPaymentUnit().getGenitiveCaseTitle() != null ? StringUtils.uncapitalize(errPaymentList.get(0).getPayment().getPaymentUnit().getGenitiveCaseTitle()) : "") +
                " c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(errPaymentList.get(0).getBeginDate()) +
                (errPaymentList.get(0).getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(errPaymentList.get(0).getEndDate()): "") +
                " уже не действует.";
            else
            {
                int i = 0;

                for (EmployeePayment payment : errPaymentList)
                {
                    i++;

                    errText += StringUtils.uncapitalize(payment.getTitle()) +
                    (i + 1 <= errPaymentList.size() ? ", " : "");
                }

                errText += " уже не действуют.";
            }

            errCollector.add(errText);
        }

        if (errCollector.hasErrors())
            return;

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());

        Double salary = 0d;
        if (extract.getEntity() != null && extract.getEntity().getRaisingCoefficient() != null)
            salary = extract.getEntity().getRaisingCoefficient().getRecommendedSalary();
        else if (extract.getEntity() != null && extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary() != null)
            salary = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary();
        List<EmployeePayment> paymentList = CommonExtractCommitUtil.createOrAssignEmployeePaymentsList(getSession(), extract, extract.getEntity(), salary, staffRateItemList);

        CommonExtractCommitUtil.validateEmployeePayment(extract.getEntity(), paymentList, getSession());

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;

        for (EmployeePayment payment : paymentList)
            getSession().saveOrUpdate(payment);

        for (EmployeePayment payment : removePaymentList)
        {
            payment.setEndDate(extract.getParagraph().getOrder().getCommitDate());
            update(payment);
        }


        update(extract);
    }

    @Override
    public void doRollback(PaymentAssigningExtract extract, Map parameters)
    {
        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
        {
            if (null != bonus.getEntity())
            {
                Long paymentId = bonus.getEntity().getId();
                bonus.setEntity(null);
                update(bonus);
                delete(paymentId);
            }
        }

        MQBuilder builder = new MQBuilder(RemoveEmployeePaymentToExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", RemoveEmployeePaymentToExtractRelation.paymentAssigningExtract().id().s(), extract.getId()));

        Map<Long, RemoveEmployeePaymentToExtractRelation> relationMap = new HashMap<>();

        for (RemoveEmployeePaymentToExtractRelation relation : builder.<RemoveEmployeePaymentToExtractRelation>getResultList(getSession()))
            relationMap.put(relation.getEmployeePayment().getId(), relation);

        List<EmployeePayment> removePaymentList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getRemovePaymentsListExtracts(extract);

        for (EmployeePayment payment : removePaymentList)
        {
            payment.setEndDate(relationMap.get(payment.getId()).getEndDateOld());
            update(payment);
        }
    }

//    private List<EmployeePayment> createOrAssignEmployeePaymentsList(PaymentAssigningExtract extract, EmployeePost post, List<EmployeePostStaffRateItem> staffRateItemList)
//    {
//        List<EmployeePayment> paymentsList = new ArrayList<>();
//
//        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> staffRateMap = null;
//        Double staffRateValue = null;
//        Double summStaffRate = 0d;
//        for (EmployeePostStaffRateItem item : staffRateItemList)
//            summStaffRate += item.getStaffRate();
//        if (staffRateItemList.size() > 1)
//        {
//            staffRateMap = new LinkedHashMap<>();
//            for (EmployeePostStaffRateItem item : staffRateItemList)
//                staffRateMap.put(new CoreCollectionUtils.Pair<>(item.getFinancingSource(), item.getFinancingSourceItem()), item.getStaffRate());
//        }
//        else
//        {
//            staffRateValue = staffRateItemList.get(0).getStaffRate();
//        }
//
//        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
//        {
//            EmployeePayment payment = null != bonus.getEntity() ? bonus.getEntity() : new EmployeePayment();
//            payment.setEmployeePost(post);
//            payment.setAssignDate(bonus.getAssignDate());
//            payment.setBeginDate(bonus.getBeginDate());
//            payment.setEndDate(bonus.getEndDate());
//            payment.setPayment(bonus.getPayment());
//            payment.setFinancingSource(bonus.getFinancingSource());
//            payment.setFinancingSourceItem(bonus.getFinancingSourceItem());
//            payment.setPaymentValue(bonus.getValueProxy());
//
//            //при расчете выплаты используется та часть доли ставки, которая совпадает с выплатой по ИФ и ИФ(д)
//            //если ставка состоит из одной доли, то используется последняя
//            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcPairKey = new CoreCollectionUtils.Pair<>(bonus.getFinancingSource(), bonus.getFinancingSourceItem());
//            Double staffRate = staffRateValue != null ? staffRateValue : (staffRateMap.get(finSrcPairKey) != null ? staffRateMap.get(finSrcPairKey) : 0d);
//            Double rate = bonus.getPayment().isDoesntDependOnStaffRate() ? 1d : (bonus.isDependOnSummStaffRate() ? summStaffRate : staffRate);
//
//            if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(bonus.getPayment().getPaymentUnit().getCode()))
//                payment.setAmount(bonus.getValue() * rate);
//            else
//            {
//                double salary;
//                Double baseSalary = 0d;
//                if (extract.getEntity().getRaisingCoefficient() != null)
//                    baseSalary = extract.getEntity().getRaisingCoefficient().getRecommendedSalary();
//                else if (extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary() != null)
//                    baseSalary = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary();
//
//                salary = baseSalary * rate;
//
//                if (UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
//                    payment.setAmount(Math.round(salary * bonus.getValue() * 100) / 100d);
//                else
//                    payment.setAmount(Math.round(salary * bonus.getValue()) / 100d);
//            }
//
//            paymentsList.add(payment);
//            bonus.setEntity(payment);
//            update(bonus);
//        }
//        return paymentsList;
//    }
}