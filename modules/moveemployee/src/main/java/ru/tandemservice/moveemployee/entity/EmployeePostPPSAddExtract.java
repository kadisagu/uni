package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.entity.gen.EmployeePostPPSAddExtractGen;

/**
 * Выписка из сборного приказа по кадровому составу. О назначении на должность
 */
public class EmployeePostPPSAddExtract extends EmployeePostPPSAddExtractGen implements IPostAssignExtract
{
    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }
}