package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.moveemployee.entity.gen.EmployeeReasonToTypeRelGen;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;

public class EmployeeReasonToTypeRel extends EmployeeReasonToTypeRelGen implements IEntityRelation<EmployeeOrderReasons, EmployeeExtractType>
{
}