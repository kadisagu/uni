/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.list.EmployeeListOrderAdd;

import java.util.List;

import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;

/**
 * @author dseleznev
 * Created on: 29.10.2009
 */
public class Model
{
    private List<EmployeeExtractType> _typeList;
    private EmployeeExtractType _type;

    public List<EmployeeExtractType> getTypeList()
    {
        return _typeList;
    }

    public void setTypeList(List<EmployeeExtractType> typeList)
    {
        _typeList = typeList;
    }

    public EmployeeExtractType getType()
    {
        return _type;
    }

    public void setType(EmployeeExtractType type)
    {
        _type = type;
    }
}