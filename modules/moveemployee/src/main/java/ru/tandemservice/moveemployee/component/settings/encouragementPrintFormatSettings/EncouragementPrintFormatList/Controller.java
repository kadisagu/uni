/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.encouragementPrintFormatSettings.EncouragementPrintFormatList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

/**
 * Create by ashaburov
 * Date 29.09.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));

        prepareDataSource(component);
    }

    public void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EncouragementType> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Тип поощрения", EncouragementType.P_TITLE));
        dataSource.addColumn(new SimpleColumn("Формат вывода", "printFormat"));
        dataSource.addColumn(new ActionColumn("Редактирование", ActionColumn.EDIT, "onClickEdit"));

        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.ENCOURAGEMENT_PRINT_FORMAT_EDIT,
                                                         new ParametersMap().add("id", component.getListenerParameter())));
    }
}
