/**
 *$Id$
 */
package ru.tandemservice.moveemployee.base.bo.MoveemployeeContract.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Create by ashaburov
 * Date 16.05.12
 */
public interface IContractDao extends INeedPersistenceSupport
{
    /**
     * Поднимает ежегодные отпуска указанного сотрудника, период которых пересикается с указанным периодом дат.
     * @return Список Отпусков сотрудников.
     */
    List<EmployeeHoliday> getEmployeeHolidayList(EmployeePost post, Date begin, Date end);

    /**
     * Сохраняет приказ, выписки и их параграфы.
     */
    void doSave(EmployeeListOrder order, List<EmploymentDismissalEmplListExtract> extractList);

    /**
     * Создает и сохраняет рилейшены выписок и оснований.
     * @param extractList список выписок
     * @param basicsList список оснований
     * @param basicNoticeMap мапа примечаний к основаниям
     */
    void updateBasicList(List<EmploymentDismissalEmplListExtract> extractList, List<EmployeeOrderBasics> basicsList, Map<EmployeePost, Map<String, String>> basicNoticeMap);
}
