package ru.tandemservice.moveemployee.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.gen.EmployeeSingleExtractOrderGen;

/**
 * Сборный приказ по кадровому составу под одну выписку
 */
public class EmployeeSingleExtractOrder extends EmployeeSingleExtractOrderGen
{
    public static final String L_EXTRACT = "extract";
    
    @Override
    public String getTitle()
    {
        return "Индивидуальный приказ" + (getNumber() == null ? "" : " №" + getNumber()) + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCreateDate());
    }
    
    public SingleEmployeeExtract getExtract()
    {
        return MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractBySingleExtractOrder(this);
    }

    @Override
    public ICatalogItem getType()
    {
        return null;
    }
}