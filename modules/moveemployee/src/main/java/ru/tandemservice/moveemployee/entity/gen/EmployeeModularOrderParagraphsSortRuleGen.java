package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Правило задания порядка пунктов сборного приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeModularOrderParagraphsSortRuleGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule";
    public static final String ENTITY_NAME = "employeeModularOrderParagraphsSortRule";
    public static final int VERSION_HASH = -1062395687;
    private static IEntityMeta ENTITY_META;

    public static final String P_DAO_NAME = "daoName";
    public static final String P_TITLE = "title";
    public static final String P_DESCRIPTION = "description";
    public static final String P_ACTIVE = "active";

    private String _daoName;     // Сортировщик пунктов приказа
    private String _title;     // Название
    private String _description;     // Описание
    private boolean _active;     // Активное

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сортировщик пунктов приказа. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getDaoName()
    {
        return _daoName;
    }

    /**
     * @param daoName Сортировщик пунктов приказа. Свойство не может быть null и должно быть уникальным.
     */
    public void setDaoName(String daoName)
    {
        dirty(_daoName, daoName);
        _daoName = daoName;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Описание. Свойство не может быть null.
     */
    @NotNull
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание. Свойство не может быть null.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Активное. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Активное. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeModularOrderParagraphsSortRuleGen)
        {
            setDaoName(((EmployeeModularOrderParagraphsSortRule)another).getDaoName());
            setTitle(((EmployeeModularOrderParagraphsSortRule)another).getTitle());
            setDescription(((EmployeeModularOrderParagraphsSortRule)another).getDescription());
            setActive(((EmployeeModularOrderParagraphsSortRule)another).isActive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeModularOrderParagraphsSortRuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeModularOrderParagraphsSortRule.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeModularOrderParagraphsSortRule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "daoName":
                    return obj.getDaoName();
                case "title":
                    return obj.getTitle();
                case "description":
                    return obj.getDescription();
                case "active":
                    return obj.isActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "daoName":
                    obj.setDaoName((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "daoName":
                        return true;
                case "title":
                        return true;
                case "description":
                        return true;
                case "active":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "daoName":
                    return true;
                case "title":
                    return true;
                case "description":
                    return true;
                case "active":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "daoName":
                    return String.class;
                case "title":
                    return String.class;
                case "description":
                    return String.class;
                case "active":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeModularOrderParagraphsSortRule> _dslPath = new Path<EmployeeModularOrderParagraphsSortRule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeModularOrderParagraphsSortRule");
    }
            

    /**
     * @return Сортировщик пунктов приказа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule#getDaoName()
     */
    public static PropertyPath<String> daoName()
    {
        return _dslPath.daoName();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Активное. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends EmployeeModularOrderParagraphsSortRule> extends EntityPath<E>
    {
        private PropertyPath<String> _daoName;
        private PropertyPath<String> _title;
        private PropertyPath<String> _description;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сортировщик пунктов приказа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule#getDaoName()
     */
        public PropertyPath<String> daoName()
        {
            if(_daoName == null )
                _daoName = new PropertyPath<String>(EmployeeModularOrderParagraphsSortRuleGen.P_DAO_NAME, this);
            return _daoName;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EmployeeModularOrderParagraphsSortRuleGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Описание. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EmployeeModularOrderParagraphsSortRuleGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Активное. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(EmployeeModularOrderParagraphsSortRuleGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return EmployeeModularOrderParagraphsSortRule.class;
        }

        public String getEntityName()
        {
            return "employeeModularOrderParagraphsSortRule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
