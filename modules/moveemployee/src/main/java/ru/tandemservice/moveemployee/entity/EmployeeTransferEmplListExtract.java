package ru.tandemservice.moveemployee.entity;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.component.commons.ITransferEmployeePostExtract;
import ru.tandemservice.moveemployee.entity.gen.EmployeeTransferEmplListExtractGen;

/**
 * Выписка из списочного приказа по кадровому составу. О переводе
 */
public class EmployeeTransferEmplListExtract extends EmployeeTransferEmplListExtractGen implements IPostAssignExtract, ITransferEmployeePostExtract
{
    public static final String P_STAFF_RATES_STR = "staffRatesStr";

    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }

    @Override
    public EmployeePost getEmployeePostOld()
    {
        return getEmployeePost();
    }
}