/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e1.Pub;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.entity.EmployeeAddExtract;

/**
 * @author dseleznev
 * Created on: 20.11.2008
 */
public class Model extends ModularEmployeeExtractPubModel<EmployeeAddExtract>
{
    private String _staffRateStr;

    //Getters & Setters

    public String getStaffRateStr()
    {
        return _staffRateStr;
    }

    public void setStaffRateStr(String staffRateStr)
    {
        _staffRateStr = staffRateStr;
    }
}