/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.commons;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * @author dseleznev
 * Created on: 11.11.2008
 */
public class ExtractListModel
{
    public static final String EXTRACT_TYPE = "extractType";
    public static final String EXTRACT_STATE = "extractState";

    @SuppressWarnings("unchecked")
    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;
    private CommonPostfixPermissionModel _secModel;

    // filters
    private List<HSelectOption> _extractTypeList;
    private List<ExtractStates> _extractStateList;

    // Getters & Setters
    @SuppressWarnings("unchecked")
    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    @SuppressWarnings("unchecked")
    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this._secModel = secModel;
    }

    public List<HSelectOption> getExtractTypeList()
    {
        return _extractTypeList;
    }

    public void setExtractTypeList(List<HSelectOption> extractTypeList)
    {
        _extractTypeList = extractTypeList;
    }

    public List<ExtractStates> getExtractStateList()
    {
        return _extractStateList;
    }

    public void setExtractStateList(List<ExtractStates> extractStateList)
    {
        _extractStateList = extractStateList;
    }
}