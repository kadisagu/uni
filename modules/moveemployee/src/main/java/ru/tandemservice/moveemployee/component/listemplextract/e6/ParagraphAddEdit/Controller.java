/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e6.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class Controller extends AbstractListParagraphAddEditController<ChangeFinancingSourceEmplListExtract, IDAO, Model>
{
    @SuppressWarnings("unchecked")
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareEmployeeDataSource(component);

        Model model = getModel(component);
        if (model.isEditForm())
            ((BlockColumn) model.getEmployeeDataSource().getColumn("staffRate")).setValueMap(model.getValueMap());
    }

    private void prepareEmployeeDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getEmployeeDataSource() != null)
            return;

        DynamicListDataSource<EmployeePost> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareEmployeeDataSource(model);
        });

        CheckboxColumn checkboxColumn = new CheckboxColumn("checkColumn");
        if (model.getSelectEmployeeList() != null)
        {
            List<IEntity> entityList = new ArrayList<>();
            for (EmployeePost employeePost : model.getSelectEmployeeList())
                entityList.add(employeePost);
            checkboxColumn.setSelectedObjects(entityList);
        }
        dataSource.addColumn(checkboxColumn);
        dataSource.addColumn(new SimpleColumn("ФИО", EmployeePost.person().fio().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Табельный номер", EmployeePost.employee().employeeCode().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Должность", EmployeePost.postRelation().postBoundedWithQGandQL().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", new String[]{EmployeePost.L_ORG_UNIT, OrgUnit.P_TITLE_WITH_TYPE}).setClickable(false));
        dataSource.addColumn(new BlockColumn("staffRate", "Ставка").setClickable(false).setOrderable(false));

        model.setEmployeeDataSource(dataSource);
    }

    public void onChangeOrgUnit(IBusinessComponent component)
    {
        Model model = getModel(component);

//        model.setTempEmployeeList(model.getEmployeeDataSource().getEntityList());
        model.getEmployeeDataSource().refresh();
    }


}
