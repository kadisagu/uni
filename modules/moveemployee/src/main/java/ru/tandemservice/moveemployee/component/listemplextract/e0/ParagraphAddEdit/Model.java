/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e0.ParagraphAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
@Input( {
    @Bind(key = AbstractListParagraphAddEditModel.PARAMETER_ORDER_ID, binding = "orderId")
    })
public class Model
{
    private Long _orderId;
    private ParagraphEmployeeListOrder _order;
    private EmployeeExtractType _extractType;
    private ISingleSelectModel _extractTypeModel;


    //Getters & Setters

    public ParagraphEmployeeListOrder getOrder()
    {
        return _order;
    }

    public void setOrder(ParagraphEmployeeListOrder order)
    {
        _order = order;
    }

    public EmployeeExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(EmployeeExtractType extractType)
    {
        _extractType = extractType;
    }

    public ISingleSelectModel getExtractTypeModel()
    {
        return _extractTypeModel;
    }

    public void setExtractTypeModel(ISingleSelectModel extractTypeModel)
    {
        _extractTypeModel = extractTypeModel;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }
}
