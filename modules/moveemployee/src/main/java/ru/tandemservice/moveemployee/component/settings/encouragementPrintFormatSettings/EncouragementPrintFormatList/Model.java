/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.encouragementPrintFormatSettings.EncouragementPrintFormatList;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

/**
 * Create by ashaburov
 * Date 29.09.11
 */
public class Model
{
    DynamicListDataSource<EncouragementType> _dataSource; //Датасорс сечлиста

    //Getters & Setters

    public DynamicListDataSource<EncouragementType> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EncouragementType> dataSource)
    {
        _dataSource = dataSource;
    }
}
