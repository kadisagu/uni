/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e6.ParagraphAddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract;
import ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.FinancingSourceSingleSelectModel;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class DAO extends AbstractListParagraphAddEditDAO<ChangeFinancingSourceEmplListExtract, Model> implements IDAO
{
    protected static OrderDescriptionRegistry orderRegistry = new OrderDescriptionRegistry("b");

    @Override
    protected ChangeFinancingSourceEmplListExtract createNewExtractInstance(Model model)
    {
        return new ChangeFinancingSourceEmplListExtract();
    }

    @Override
    public void prepareEmployeeDataSource(Model model)
    {
        List<OrgUnit> orgUnitList = new ArrayList<>();
        if (model.getOrgUnit() != null)
            orgUnitList.add(model.getOrgUnit());
        if (model.getChildOrgUnit() != null && model.getChildOrgUnit())
            orgUnitList.addAll(OrgUnitManager.instance().dao().getChildOrgUnits(model.getOrgUnit(), true, false));

        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "b");
        builder.add(MQExpression.in("b", EmployeePost.orgUnit().s(), orgUnitList));
        builder.add(MQExpression.eq("b", EmployeePost.postStatus().active().s(), Boolean.TRUE));

        orderRegistry.applyOrder(builder, model.getEmployeeDataSource().getEntityOrder());

        List<EmployeePost> employeePostList = builder.getResultList(getSession());

        Map<EmployeePost, List<EmployeePostStaffRateItem>> postStaffRateMap = new HashMap<>();
        MQBuilder staffRateBuilder = new MQBuilder(EmployeePostStaffRateItem.ENTITY_CLASS, "sr");
        staffRateBuilder.add(MQExpression.in("sr", EmployeePostStaffRateItem.employeePost().s(), employeePostList));
        for (EmployeePostStaffRateItem item : staffRateBuilder.<EmployeePostStaffRateItem>getResultList(getSession()))
        {
            List<EmployeePostStaffRateItem> itemList = postStaffRateMap.get(item.getEmployeePost());
            if (itemList == null)
                itemList = new ArrayList<>();
            itemList.add(item);
            postStaffRateMap.put(item.getEmployeePost(), itemList);
        }

        HashMap<Long, String> staffRateMap = new HashMap<>();
        for (EmployeePost post : builder.<EmployeePost>getResultList(getSession()))
        {
            if (postStaffRateMap.get(post) != null && postStaffRateMap.get(post).size() == 1)
            {
                StringBuilder stringBuilder = new StringBuilder(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(postStaffRateMap.get(post).get(0).getStaffRate()));
                stringBuilder.append(" - ");
                stringBuilder.append(postStaffRateMap.get(post).get(0).getFinancingSource().getTitle());

                if (postStaffRateMap.get(post).get(0).getFinancingSourceItem() != null)
                {
                    stringBuilder.append(" (");
                    stringBuilder.append(postStaffRateMap.get(post).get(0).getFinancingSourceItem().getTitle());
                    stringBuilder.append(")");
                }

                staffRateMap.put(post.getId(), stringBuilder.toString());
            }
            else if (postStaffRateMap.get(post) == null)
                staffRateMap.put(post.getId(), "<ставка отсутствует>");
            else
                staffRateMap.put(post.getId(), null);
        }
        model.setStaffRateMap(staffRateMap);

        UniBaseUtils.createPage(model.getEmployeeDataSource(), builder, getSession());
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setFinancingSourceModel(new FinancingSourceSingleSelectModel());

        model.setFinancingSourceItemModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", FinancingSourceItem.financingSource().s(), model.getFinancingSource()));
                if (o != null)
                    builder.add(MQExpression.eq("b", FinancingSourceItem.id().s(), o));

                return new MQListResultBuilder(builder);
            }
        });

        model.setOrgUnitModel(new OrgUnitAutocompleteModel());

        model.setStaffRateModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                EmployeePost currentEntity = (EmployeePost) model.getEmployeeDataSource().getCurrentEntity();

                MQBuilder builder = new MQBuilder(EmployeePostStaffRateItem.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", EmployeePostStaffRateItem.employeePost().s(), currentEntity));
                if (set != null)
                    builder.add(MQExpression.in("b", EmployeePostStaffRateItem.id().s(), set));

                return new MQListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EmployeePostStaffRateItem item = (EmployeePostStaffRateItem) value;

                StringBuilder stringBuilder = new StringBuilder(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()));
                stringBuilder.append(" - ");
                stringBuilder.append(item.getFinancingSource().getTitle());

                if (item.getFinancingSourceItem() != null)
                {
                    stringBuilder.append(" (");
                    stringBuilder.append(item.getFinancingSourceItem().getTitle());
                    stringBuilder.append(")");
                }

                return stringBuilder.toString();
            }
        });

        if (model.isEditForm())
        {
            model.setSelectEmployeeList(new ArrayList<>());
            for (ChangeFinancingSourceEmplListExtract extract : (List<ChangeFinancingSourceEmplListExtract>) model.getParagraph().getExtractList())
                model.getSelectEmployeeList().add(extract.getEntity());

            Map<EmployeePost, List<EmployeePostStaffRateItem>> postStaffRateMap = new HashMap<>();
            MQBuilder staffRateBuilder = new MQBuilder(EmployeePostStaffRateItem.ENTITY_CLASS, "b");
            staffRateBuilder.add(MQExpression.in("b", EmployeePostStaffRateItem.employeePost().s(), model.getSelectEmployeeList()));
            for (EmployeePostStaffRateItem item : staffRateBuilder.<EmployeePostStaffRateItem>getResultList(getSession()))
            {
                List<EmployeePostStaffRateItem> itemList = postStaffRateMap.get(item.getEmployeePost());
                if (itemList == null)
                    itemList = new ArrayList<>();
                itemList.add(item);
                postStaffRateMap.put(item.getEmployeePost(), itemList);
            }

            MQBuilder builder = new MQBuilder(StaffRateToFinSrcChangeListExtractRelation.ENTITY_CLASS, "b");
            builder.add(MQExpression.in("b", StaffRateToFinSrcChangeListExtractRelation.listExtract().s(), model.getParagraph().getExtractList()));

            Map<EmployeePost, List<EmployeePostStaffRateItem>> relationMap = new HashMap<>();
            for (StaffRateToFinSrcChangeListExtractRelation relation : builder.<StaffRateToFinSrcChangeListExtractRelation>getResultList(getSession()))
            {
                List<EmployeePostStaffRateItem> relationList = relationMap.get(relation.getListExtract().getEntity());
                if (relationList == null)
                    relationList = new ArrayList<>();
                relationList.add(relation.getEmployeePostStaffRateItem());
                relationMap.put(relation.getListExtract().getEntity(), relationList);
            }

            Map<Long, List<EmployeePostStaffRateItem>> valueMap = new HashMap<>();
            for (EmployeePost post : model.getSelectEmployeeList())
                if (postStaffRateMap.get(post) != null && postStaffRateMap.get(post).size() > 1)
                    valueMap.put(post.getId(), relationMap.get(post));

            model.setValueMap(valueMap);

            ChangeFinancingSourceEmplListExtract extract = (ChangeFinancingSourceEmplListExtract) model.getParagraph().getFirstExtract();
            model.setOrgUnit(extract.getOrgUnit());
            model.setFinancingSource(extract.getFinancingSource());
            model.setFinancingSourceItem(extract.getFinancingSourceItem());
            model.setChildOrgUnit(extract.isChildOrgUnit());
            model.setChangeDate(extract.getChangeDate());
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getEmployeeDataSource().getSelectedEntities().isEmpty())
            errors.add("Необходимо выбрать сотрудников.");
    }

    @Override
    public void update(Model model)
    {
        model.setToSaveExtractList(new ArrayList<>());

        ExtractStates extractStates = getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER);

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("staffRate");
        final Map<Long, List<EmployeePostStaffRateItem>> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Map<EmployeePost, List<EmployeePostStaffRateItem>> postStaffRateMap = new HashMap<>();
        MQBuilder staffRateBuilder = new MQBuilder(EmployeePostStaffRateItem.ENTITY_CLASS, "b");
        staffRateBuilder.add(MQExpression.in("b", EmployeePostStaffRateItem.employeePost().s(), model.getEmployeeDataSource().getSelectedEntities()));
        for (EmployeePostStaffRateItem item : staffRateBuilder.<EmployeePostStaffRateItem>getResultList(getSession()))
        {
            List<EmployeePostStaffRateItem> itemList = postStaffRateMap.get(item.getEmployeePost());
            if (itemList == null)
                itemList = new ArrayList<>();
            itemList.add(item);
            postStaffRateMap.put(item.getEmployeePost(), itemList);
        }

        if (model.isEditForm())
        {
            MQBuilder builder = new MQBuilder(StaffRateToFinSrcChangeListExtractRelation.ENTITY_CLASS, "b");
            builder.add(MQExpression.in("b", StaffRateToFinSrcChangeListExtractRelation.listExtract().s(), model.getParagraph().getExtractList()));

            for (StaffRateToFinSrcChangeListExtractRelation relation : builder.<StaffRateToFinSrcChangeListExtractRelation>getResultList(getSession()))
                delete(relation);
        }

        for (EmployeePost employeePost : (List<EmployeePost>) model.getEmployeeDataSource().getSelectedEntities())
        {
            if (model.isEditForm())
            {
                if (!model.getSelectEmployeeList().contains(employeePost))
                    if (MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeePostHasNotFinishedExtracts(employeePost))
                        throw new ApplicationException("Нельзя добавить параграф, так как у сотрудника '" + employeePost.getPerson().getFio() + "' уже есть непроведенные выписки.");
            }
            else
            if (MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeePostHasNotFinishedExtracts(employeePost))
                throw new ApplicationException("Нельзя добавить параграф, так как у сотрудника '" + employeePost.getPerson().getFio() + "' уже есть непроведенные выписки.");

            if (postStaffRateMap.get(employeePost) == null)
                throw new ApplicationException("У сотрудника " + employeePost.getPerson().getFio() + " не указана ставка.");

            if (postStaffRateMap.get(employeePost) != null && postStaffRateMap.get(employeePost).size() > 1 && (staffRateMap.get(employeePost.getId()) == null || staffRateMap.get(employeePost.getId()).isEmpty()))
                throw new ApplicationException("У сотрудника " + employeePost.getPerson().getFio() + " не указана ставка.");

            ChangeFinancingSourceEmplListExtract extract = new ChangeFinancingSourceEmplListExtract();
            extract.setEntity(employeePost);
            extract.setEmployee(employeePost.getEmployee());
            extract.setParagraph(model.getParagraph());
            extract.setState(extractStates);
            extract.setEmployeeFioModified(employeePost.getPerson().getFio());
            extract.setType(model.getExtractType());
            extract.setCreateDate(model.getCreateDate());
            extract.setReason(model.getReason());

            extract.setChangeDate(model.getChangeDate());
            extract.setFinancingSource(model.getFinancingSource());
            extract.setFinancingSourceItem(model.getFinancingSourceItem());
            extract.setOrgUnit(model.getOrgUnit());
            extract.setChildOrgUnit(model.getChildOrgUnit());

            model.getToSaveExtractList().add(extract);
        }

        super.update(model);

        for (ChangeFinancingSourceEmplListExtract extract : model.getToSaveExtractList())
        {
            EmployeePost employeePost = extract.getEntity();

            if (staffRateMap.get(employeePost.getId()) != null && !staffRateMap.get(employeePost.getId()).isEmpty())
            {
                for (EmployeePostStaffRateItem item : staffRateMap.get(employeePost.getId()))
                {
                    StaffRateToFinSrcChangeListExtractRelation relation = new StaffRateToFinSrcChangeListExtractRelation();
                    relation.setListExtract(extract);
                    relation.setEmployeePostStaffRateItem(item);
                    relation.setFinancingSource(item.getFinancingSource());
                    relation.setFinancingSourceItem(item.getFinancingSourceItem());
                    relation.setStaffRate(item.getStaffRate());

                    save(relation);
                }
            }
            else
            {
                EmployeePostStaffRateItem item = postStaffRateMap.get(employeePost).get(0);

                StaffRateToFinSrcChangeListExtractRelation relation = new StaffRateToFinSrcChangeListExtractRelation();
                relation.setListExtract(extract);
                relation.setEmployeePostStaffRateItem(item);
                relation.setFinancingSource(item.getFinancingSource());
                relation.setFinancingSourceItem(item.getFinancingSourceItem());
                relation.setStaffRate(item.getStaffRate());

                save(relation);
            }
        }
    }
}