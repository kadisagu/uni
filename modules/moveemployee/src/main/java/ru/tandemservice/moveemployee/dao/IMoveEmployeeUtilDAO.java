/**
 * $Id$
 */
package ru.tandemservice.moveemployee.dao;

import java.util.List;

import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;

/**
 * @author dseleznev
 * Created on: 07.06.2010
 */
public interface IMoveEmployeeUtilDAO extends IUniBaseDao
{
    String MOVE_EMPLOYEE_UTIL_DAO_BEAN_NAME = "moveEmployeeUtilDao";

    List<Payment> getRequiredPaymentsList();

    List<EmployeeBonus> getExtractEmployeeBonusesList(AbstractEmployeeExtract extract);

    List<SalaryRaisingCoefficient> getRaisingCoefficientsList(PostBoundedWithQGandQL post, String filter);

    List<OrgUnitTypePostRelation> getPostsList(OrgUnitType orgUnitType, String filter);

    List<Payment> getAvailablePaymentsList(List<Payment> alreadyAddedPayments, String filter);

    ListResult<FinancingSourceItem> getFinancinSourceItemsListResult(String filter);

    String getStaffRateDetailsString(AbstractEmployeeExtract extract, boolean budget);
}