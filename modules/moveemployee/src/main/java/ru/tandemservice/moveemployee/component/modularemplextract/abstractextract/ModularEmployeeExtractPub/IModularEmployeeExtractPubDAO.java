/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;

import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 20.11.2008
 */
public interface IModularEmployeeExtractPubDAO<T extends ModularEmployeeExtract, Model extends ModularEmployeeExtractPubModel<T>> extends IUniDao<Model>
{
    void doSendToCoordination(Model model, IPersistentPersonable initiator);

    void doSendToFormative(Model model);

    void doRollback(Model model);

    /**
    * Вызывается с карточки выписки отображающейся как приказ
    */
    public void doSendToCoordinationIndividualExt(Model model, IPersistentPersonable initiator);

    /**
    * Вызывается с карточки выписки отображающейся как приказ
    */
    public void doCommitIndividualExt(Model model);

    /**
    * Вызывается с карточки выписки отображающейся как приказ
    */
    public void doRejectIndividualExt(Model model);

    /**
    * Вызывается с карточки выписки отображающейся как приказ
    */
    public void doSendToFormativeIndividualExt(Model model);

    /**
     * Производится проверка состояния приказа.<p/>
     * Если приказ не в состоянии формируется, то кидается ошибка.
     */
    public void validateStateOrder(ModularEmployeeExtract extract, ErrorCollector errors);
}