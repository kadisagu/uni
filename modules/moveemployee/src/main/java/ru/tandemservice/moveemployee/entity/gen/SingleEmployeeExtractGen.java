package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка (индивидуальный приказ по кадрам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SingleEmployeeExtractGen extends AbstractEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.SingleEmployeeExtract";
    public static final String ENTITY_NAME = "singleEmployeeExtract";
    public static final int VERSION_HASH = 1014028049;
    private static IEntityMeta ENTITY_META;

    public static final String L_REASON = "reason";
    public static final String L_EMPLOYEE = "employee";
    public static final String P_TEXT_PARAGRAPH = "textParagraph";

    private EmployeeOrderReasons _reason;     // Причина приказа по кадрам
    private Employee _employee;     // Кадровый ресурс
    private String _textParagraph;     // Текстовый параграф

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null.
     */
    @NotNull
    public EmployeeOrderReasons getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина приказа по кадрам. Свойство не может быть null.
     */
    public void setReason(EmployeeOrderReasons reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     */
    @NotNull
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Кадровый ресурс. Свойство не может быть null.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    /**
     * @return Текстовый параграф.
     */
    public String getTextParagraph()
    {
        return _textParagraph;
    }

    /**
     * @param textParagraph Текстовый параграф.
     */
    public void setTextParagraph(String textParagraph)
    {
        dirty(_textParagraph, textParagraph);
        _textParagraph = textParagraph;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof SingleEmployeeExtractGen)
        {
            setReason(((SingleEmployeeExtract)another).getReason());
            setEmployee(((SingleEmployeeExtract)another).getEmployee());
            setTextParagraph(((SingleEmployeeExtract)another).getTextParagraph());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SingleEmployeeExtractGen> extends AbstractEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SingleEmployeeExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("SingleEmployeeExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return obj.getReason();
                case "employee":
                    return obj.getEmployee();
                case "textParagraph":
                    return obj.getTextParagraph();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reason":
                    obj.setReason((EmployeeOrderReasons) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
                case "textParagraph":
                    obj.setTextParagraph((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                        return true;
                case "employee":
                        return true;
                case "textParagraph":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return true;
                case "employee":
                    return true;
                case "textParagraph":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return EmployeeOrderReasons.class;
                case "employee":
                    return Employee.class;
                case "textParagraph":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SingleEmployeeExtract> _dslPath = new Path<SingleEmployeeExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SingleEmployeeExtract");
    }
            

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.SingleEmployeeExtract#getReason()
     */
    public static EmployeeOrderReasons.Path<EmployeeOrderReasons> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.SingleEmployeeExtract#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.moveemployee.entity.SingleEmployeeExtract#getTextParagraph()
     */
    public static PropertyPath<String> textParagraph()
    {
        return _dslPath.textParagraph();
    }

    public static class Path<E extends SingleEmployeeExtract> extends AbstractEmployeeExtract.Path<E>
    {
        private EmployeeOrderReasons.Path<EmployeeOrderReasons> _reason;
        private Employee.Path<Employee> _employee;
        private PropertyPath<String> _textParagraph;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.SingleEmployeeExtract#getReason()
     */
        public EmployeeOrderReasons.Path<EmployeeOrderReasons> reason()
        {
            if(_reason == null )
                _reason = new EmployeeOrderReasons.Path<EmployeeOrderReasons>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.SingleEmployeeExtract#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.moveemployee.entity.SingleEmployeeExtract#getTextParagraph()
     */
        public PropertyPath<String> textParagraph()
        {
            if(_textParagraph == null )
                _textParagraph = new PropertyPath<String>(SingleEmployeeExtractGen.P_TEXT_PARAGRAPH, this);
            return _textParagraph;
        }

        public Class getEntityClass()
        {
            return SingleEmployeeExtract.class;
        }

        public String getEntityName()
        {
            return "singleEmployeeExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
