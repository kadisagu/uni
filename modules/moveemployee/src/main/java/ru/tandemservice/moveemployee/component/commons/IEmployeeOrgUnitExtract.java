/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author dseleznev
 * Created on: 21.01.2009
 */
public interface IEmployeeOrgUnitExtract
{
    OrgUnit getOrgUnit();

    void setOrgUnit(OrgUnit orgUnit);
}