package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.entity.gen.AbstractEmployeeParagraphGen;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractEmployeeParagraph extends AbstractEmployeeParagraphGen implements IAbstractParagraph<AbstractEmployeeOrder>
{
    @Override
    @SuppressWarnings({"unchecked"})
    public List<? extends IAbstractExtract> getExtractList()
    {
        if (getId() == null)
            return new ArrayList<>();

        return UniDaoFacade.getCoreDao().getList(AbstractEmployeeExtract.class, IAbstractExtract.L_PARAGRAPH, this, IAbstractExtract.P_NUMBER);
    }

    public static final String P_EXTRACT_COUNT = "extractCount";
    @Override
    public int getExtractCount()
    {
        return UniDaoFacade.getCoreDao().getCount(AbstractEmployeeExtract.class, IAbstractExtract.L_PARAGRAPH, this);
    }
}