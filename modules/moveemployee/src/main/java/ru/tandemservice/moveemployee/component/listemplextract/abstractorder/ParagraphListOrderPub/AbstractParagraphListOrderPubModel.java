/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ParagraphListOrderPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

import java.util.Map;

/**
 * @author ashaburov
 * @since 11.10.2011
 */
@State( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orderId") })
public abstract class AbstractParagraphListOrderPubModel<T extends ParagraphEmployeeListOrder> implements IVisaOwnerModel
{
    public static final String FIRST_EXTRACT = "firstExtract";
    public static final String EXTRACT_COUNT = "extractCount";

    private IDataSettings _settings;
    private CommonPostfixPermissionModel _secModel;
    private T _order;
    private Long _orderId;
    private String _selectedTab;
    private Long _visingStatus;
    private String _basicsTitle;
    private Boolean _haveActiveParagraph;

    @SuppressWarnings({"unchecked"})
    private DynamicListDataSource _dataSource;
    private String _searchListSettingsKey;

    private String _actionsPage;
    private String _paramsPage;

    private boolean _multipleParagraphAddEnabled = false;

    public void setHaveActiveParagraph(Boolean haveActiveParagraph)
    {
        _haveActiveParagraph = haveActiveParagraph;
    }

    public Boolean getHaveActiveParagraph()
    {
        return _haveActiveParagraph;
    }

    private Map<String, Object> _visaListParameters = ParametersMap.createWith("visaOwnerModel", this);

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }


    // IVisaOwnerModel

    @Override
    public IAbstractDocument getDocument()
    {
        return _order;
    }

    @Override
    public boolean isVisaListModificable()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(_order.getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        return "employeeListOrder";
    }


    // Getters & Setters

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public T getOrder()
    {
        return _order;
    }

    public void setOrder(T order)
    {
        _order = order;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        _visingStatus = visingStatus;
    }

    public String getBasicsTitle()
    {
        return _basicsTitle;
    }

    public void setBasicsTitle(String basicsTitle)
    {
        _basicsTitle = basicsTitle;
    }

    @SuppressWarnings({"unchecked"})
    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    @SuppressWarnings({"unchecked"})
    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public String getSearchListSettingsKey()
    {
        return _searchListSettingsKey;
    }

    public void setSearchListSettingsKey(String searchListSettingsKey)
    {
        _searchListSettingsKey = searchListSettingsKey;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }

    public String getActionsPage()
    {
        return _actionsPage;
    }

    public void setActionsPage(String actionsPage)
    {
        _actionsPage = actionsPage;
    }

    public String getParamsPage()
    {
        return _paramsPage;
    }

    public void setParamsPage(String paramsPage)
    {
        _paramsPage = paramsPage;
    }

    public boolean isMultipleParagraphAddEnabled()
    {
        return _multipleParagraphAddEnabled;
    }

    public void setMultipleParagraphAddEnabled(boolean multipleParagraphAddEnabled)
    {
        this._multipleParagraphAddEnabled = multipleParagraphAddEnabled;
    }
}