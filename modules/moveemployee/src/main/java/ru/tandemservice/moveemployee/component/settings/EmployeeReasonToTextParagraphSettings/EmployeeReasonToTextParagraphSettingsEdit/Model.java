/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeReasonToTextParagraphSettings.EmployeeReasonToTextParagraphSettingsEdit;

import org.tandemframework.core.component.Input;

import ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;

/**
 * Create by: ashaburov
 * Date: 19.04.11
 */
@Input(keys = "employeeOrderReasonId", bindings = "employeeOrderReasonId")
public class Model
{
    private Long _employeeOrderReasonId;
    private EmployeeReasonToTextParagraphRel _setting;
    private EmployeeOrderReasons _reason;
    private String _textParagraph;


    //Getters & Setters


    public EmployeeOrderReasons getReason()
    {
        return _reason;
    }

    public void setReason(EmployeeOrderReasons reason)
    {
        _reason = reason;
    }

    public String getTextParagraph()
    {
        return _textParagraph;
    }

    public void setTextParagraph(String textParagraph)
    {
        _textParagraph = textParagraph;
    }

    public Long getEmployeeOrderReasonId()
    {
        return _employeeOrderReasonId;
    }

    public void setEmployeeOrderReasonId(Long employeeOrderReasonId)
    {
        _employeeOrderReasonId = employeeOrderReasonId;
    }

    public EmployeeReasonToTextParagraphRel getSetting()
    {
        return _setting;
    }

    public void setSetting(EmployeeReasonToTextParagraphRel setting)
    {
        _setting = setting;
    }
}
