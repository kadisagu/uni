/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e3;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.base.ui.formatters.EmployeeCodeFormatter;
import ru.tandemservice.moveemployee.component.listemplextract.CommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.ICommonListOrderPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 27.04.2011
 */
public class EmploymentDismissalEmplListExtractPrint implements IPrintFormCreator<EmployeeListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);

        ICommonListOrderPrint commonListOrderPrint = (ICommonListOrderPrint) ApplicationRuntime.getBean("employeeListOrder_orderPrint");
        commonListOrderPrint.appendVisas(tableModifier, order);

        tableModifier.put("T", getPreparedOrderData(order));
        tableModifier.modify(document);

        return document;
    }

    protected String[][] getPreparedOrderData(EmployeeListOrder order)
    {
        List<EmploymentDismissalEmplListExtract> extractList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractList(order);
        if(null == extractList || extractList.isEmpty()) return new String[][]{};

        List<String[]> resultList = new ArrayList<String[]>();

        for(EmploymentDismissalEmplListExtract extract : extractList)
        {
            EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEmployeePost());

            String[] line = new String[10];

            line[0] = extract.getEmployeeFioModified();
            line[1] = new EmployeeCodeFormatter().format(extract.getEmployee().getEmployeeCode());
            line[2] = extract.getEmployeePost().getOrgUnit() != null ? extract.getEmployeePost().getOrgUnit().getTitle() : "-";
            line[3] = extract.getEmployeePost().getPostRelation() != null ? extract.getEmployeePost().getPostRelation().getTitle() : "-";
            line[4] = contract != null ? contract.getNumber() : "-";
            line[5] = contract != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getDate()) : "-";
            line[6] = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDismissalDate());
            line[7] = extract.getContractTerminationBasic();
            line[8] = extract.getBasicListStr();
            line[9] = "";
            resultList.add(line);
        }

        return resultList.toArray(new String[][]{});
    }
}
