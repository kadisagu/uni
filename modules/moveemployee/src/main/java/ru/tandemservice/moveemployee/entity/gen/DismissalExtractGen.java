package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.moveemployee.entity.DismissalExtract;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Увольнение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DismissalExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.DismissalExtract";
    public static final String ENTITY_NAME = "dismissalExtract";
    public static final int VERSION_HASH = 1927543248;
    private static IEntityMeta ENTITY_META;

    public static final String P_DISMISSAL_DATE = "dismissalDate";
    public static final String L_DISMISSAL_REASON = "dismissalReason";
    public static final String P_HOLIDAY_NOT_SUPPOSED = "holidayNotSupposed";
    public static final String P_NOT_FULFIL = "notFulfil";
    public static final String P_COMPENSATING_FOR = "CompensatingFor";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String P_HOLIDAY_PERIOD_BEGIN = "holidayPeriodBegin";
    public static final String P_HOLIDAY_PERIOD_END = "holidayPeriodEnd";
    public static final String P_ADDITIONAL_CONDITIONS = "additionalConditions";
    public static final String L_RELATIVE_TYPE = "relativeType";
    public static final String P_RELATIVE_FIO = "relativeFio";
    public static final String P_IDENTITY_CARD = "identityCard";
    public static final String L_EMPLOYEE_POST_STATUS_OLD = "employeePostStatusOld";
    public static final String P_DISMISSAL_DATE_OLD = "dismissalDateOld";

    private Date _dismissalDate;     // Дата увольнения
    private EmployeeDismissReasons _dismissalReason;     // Причина увольнения
    private boolean _holidayNotSupposed;     // Отпуск не положен
    private Double _notFulfil;     // Не отработано, дней
    private Double _CompensatingFor;     // Компенсация за, дней
    private FinancingSource _financingSource;     // Источник финансирования
    private Date _holidayPeriodBegin;     // Отпуск за период (с)
    private Date _holidayPeriodEnd;     // Отпуск за период (по)
    private String _additionalConditions;     // Дополнительные условия
    private RelationDegree _relativeType;     // Степень родства
    private String _relativeFio;     // ФИО родственника (в дательном падеже)
    private String _identityCard;     // Сведения об удостоверении личности родственника
    private EmployeePostStatus _employeePostStatusOld;     // Статус на должности до увольнения
    private Date _dismissalDateOld;     // Дата увольнения до увольнения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата увольнения. Свойство не может быть null.
     */
    @NotNull
    public Date getDismissalDate()
    {
        return _dismissalDate;
    }

    /**
     * @param dismissalDate Дата увольнения. Свойство не может быть null.
     */
    public void setDismissalDate(Date dismissalDate)
    {
        dirty(_dismissalDate, dismissalDate);
        _dismissalDate = dismissalDate;
    }

    /**
     * @return Причина увольнения.
     */
    public EmployeeDismissReasons getDismissalReason()
    {
        return _dismissalReason;
    }

    /**
     * @param dismissalReason Причина увольнения.
     */
    public void setDismissalReason(EmployeeDismissReasons dismissalReason)
    {
        dirty(_dismissalReason, dismissalReason);
        _dismissalReason = dismissalReason;
    }

    /**
     * @return Отпуск не положен. Свойство не может быть null.
     */
    @NotNull
    public boolean isHolidayNotSupposed()
    {
        return _holidayNotSupposed;
    }

    /**
     * @param holidayNotSupposed Отпуск не положен. Свойство не может быть null.
     */
    public void setHolidayNotSupposed(boolean holidayNotSupposed)
    {
        dirty(_holidayNotSupposed, holidayNotSupposed);
        _holidayNotSupposed = holidayNotSupposed;
    }

    /**
     * @return Не отработано, дней.
     */
    public Double getNotFulfil()
    {
        return _notFulfil;
    }

    /**
     * @param notFulfil Не отработано, дней.
     */
    public void setNotFulfil(Double notFulfil)
    {
        dirty(_notFulfil, notFulfil);
        _notFulfil = notFulfil;
    }

    /**
     * @return Компенсация за, дней.
     */
    public Double getCompensatingFor()
    {
        return _CompensatingFor;
    }

    /**
     * @param CompensatingFor Компенсация за, дней.
     */
    public void setCompensatingFor(Double CompensatingFor)
    {
        dirty(_CompensatingFor, CompensatingFor);
        _CompensatingFor = CompensatingFor;
    }

    /**
     * @return Источник финансирования.
     */
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Отпуск за период (с).
     */
    public Date getHolidayPeriodBegin()
    {
        return _holidayPeriodBegin;
    }

    /**
     * @param holidayPeriodBegin Отпуск за период (с).
     */
    public void setHolidayPeriodBegin(Date holidayPeriodBegin)
    {
        dirty(_holidayPeriodBegin, holidayPeriodBegin);
        _holidayPeriodBegin = holidayPeriodBegin;
    }

    /**
     * @return Отпуск за период (по).
     */
    public Date getHolidayPeriodEnd()
    {
        return _holidayPeriodEnd;
    }

    /**
     * @param holidayPeriodEnd Отпуск за период (по).
     */
    public void setHolidayPeriodEnd(Date holidayPeriodEnd)
    {
        dirty(_holidayPeriodEnd, holidayPeriodEnd);
        _holidayPeriodEnd = holidayPeriodEnd;
    }

    /**
     * @return Дополнительные условия.
     */
    @Length(max=255)
    public String getAdditionalConditions()
    {
        return _additionalConditions;
    }

    /**
     * @param additionalConditions Дополнительные условия.
     */
    public void setAdditionalConditions(String additionalConditions)
    {
        dirty(_additionalConditions, additionalConditions);
        _additionalConditions = additionalConditions;
    }

    /**
     * @return Степень родства.
     */
    public RelationDegree getRelativeType()
    {
        return _relativeType;
    }

    /**
     * @param relativeType Степень родства.
     */
    public void setRelativeType(RelationDegree relativeType)
    {
        dirty(_relativeType, relativeType);
        _relativeType = relativeType;
    }

    /**
     * @return ФИО родственника (в дательном падеже).
     */
    @Length(max=255)
    public String getRelativeFio()
    {
        return _relativeFio;
    }

    /**
     * @param relativeFio ФИО родственника (в дательном падеже).
     */
    public void setRelativeFio(String relativeFio)
    {
        dirty(_relativeFio, relativeFio);
        _relativeFio = relativeFio;
    }

    /**
     * @return Сведения об удостоверении личности родственника.
     */
    @Length(max=255)
    public String getIdentityCard()
    {
        return _identityCard;
    }

    /**
     * @param identityCard Сведения об удостоверении личности родственника.
     */
    public void setIdentityCard(String identityCard)
    {
        dirty(_identityCard, identityCard);
        _identityCard = identityCard;
    }

    /**
     * @return Статус на должности до увольнения.
     */
    public EmployeePostStatus getEmployeePostStatusOld()
    {
        return _employeePostStatusOld;
    }

    /**
     * @param employeePostStatusOld Статус на должности до увольнения.
     */
    public void setEmployeePostStatusOld(EmployeePostStatus employeePostStatusOld)
    {
        dirty(_employeePostStatusOld, employeePostStatusOld);
        _employeePostStatusOld = employeePostStatusOld;
    }

    /**
     * @return Дата увольнения до увольнения.
     */
    public Date getDismissalDateOld()
    {
        return _dismissalDateOld;
    }

    /**
     * @param dismissalDateOld Дата увольнения до увольнения.
     */
    public void setDismissalDateOld(Date dismissalDateOld)
    {
        dirty(_dismissalDateOld, dismissalDateOld);
        _dismissalDateOld = dismissalDateOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof DismissalExtractGen)
        {
            setDismissalDate(((DismissalExtract)another).getDismissalDate());
            setDismissalReason(((DismissalExtract)another).getDismissalReason());
            setHolidayNotSupposed(((DismissalExtract)another).isHolidayNotSupposed());
            setNotFulfil(((DismissalExtract)another).getNotFulfil());
            setCompensatingFor(((DismissalExtract)another).getCompensatingFor());
            setFinancingSource(((DismissalExtract)another).getFinancingSource());
            setHolidayPeriodBegin(((DismissalExtract)another).getHolidayPeriodBegin());
            setHolidayPeriodEnd(((DismissalExtract)another).getHolidayPeriodEnd());
            setAdditionalConditions(((DismissalExtract)another).getAdditionalConditions());
            setRelativeType(((DismissalExtract)another).getRelativeType());
            setRelativeFio(((DismissalExtract)another).getRelativeFio());
            setIdentityCard(((DismissalExtract)another).getIdentityCard());
            setEmployeePostStatusOld(((DismissalExtract)another).getEmployeePostStatusOld());
            setDismissalDateOld(((DismissalExtract)another).getDismissalDateOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DismissalExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DismissalExtract.class;
        }

        public T newInstance()
        {
            return (T) new DismissalExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dismissalDate":
                    return obj.getDismissalDate();
                case "dismissalReason":
                    return obj.getDismissalReason();
                case "holidayNotSupposed":
                    return obj.isHolidayNotSupposed();
                case "notFulfil":
                    return obj.getNotFulfil();
                case "CompensatingFor":
                    return obj.getCompensatingFor();
                case "financingSource":
                    return obj.getFinancingSource();
                case "holidayPeriodBegin":
                    return obj.getHolidayPeriodBegin();
                case "holidayPeriodEnd":
                    return obj.getHolidayPeriodEnd();
                case "additionalConditions":
                    return obj.getAdditionalConditions();
                case "relativeType":
                    return obj.getRelativeType();
                case "relativeFio":
                    return obj.getRelativeFio();
                case "identityCard":
                    return obj.getIdentityCard();
                case "employeePostStatusOld":
                    return obj.getEmployeePostStatusOld();
                case "dismissalDateOld":
                    return obj.getDismissalDateOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dismissalDate":
                    obj.setDismissalDate((Date) value);
                    return;
                case "dismissalReason":
                    obj.setDismissalReason((EmployeeDismissReasons) value);
                    return;
                case "holidayNotSupposed":
                    obj.setHolidayNotSupposed((Boolean) value);
                    return;
                case "notFulfil":
                    obj.setNotFulfil((Double) value);
                    return;
                case "CompensatingFor":
                    obj.setCompensatingFor((Double) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "holidayPeriodBegin":
                    obj.setHolidayPeriodBegin((Date) value);
                    return;
                case "holidayPeriodEnd":
                    obj.setHolidayPeriodEnd((Date) value);
                    return;
                case "additionalConditions":
                    obj.setAdditionalConditions((String) value);
                    return;
                case "relativeType":
                    obj.setRelativeType((RelationDegree) value);
                    return;
                case "relativeFio":
                    obj.setRelativeFio((String) value);
                    return;
                case "identityCard":
                    obj.setIdentityCard((String) value);
                    return;
                case "employeePostStatusOld":
                    obj.setEmployeePostStatusOld((EmployeePostStatus) value);
                    return;
                case "dismissalDateOld":
                    obj.setDismissalDateOld((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dismissalDate":
                        return true;
                case "dismissalReason":
                        return true;
                case "holidayNotSupposed":
                        return true;
                case "notFulfil":
                        return true;
                case "CompensatingFor":
                        return true;
                case "financingSource":
                        return true;
                case "holidayPeriodBegin":
                        return true;
                case "holidayPeriodEnd":
                        return true;
                case "additionalConditions":
                        return true;
                case "relativeType":
                        return true;
                case "relativeFio":
                        return true;
                case "identityCard":
                        return true;
                case "employeePostStatusOld":
                        return true;
                case "dismissalDateOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dismissalDate":
                    return true;
                case "dismissalReason":
                    return true;
                case "holidayNotSupposed":
                    return true;
                case "notFulfil":
                    return true;
                case "CompensatingFor":
                    return true;
                case "financingSource":
                    return true;
                case "holidayPeriodBegin":
                    return true;
                case "holidayPeriodEnd":
                    return true;
                case "additionalConditions":
                    return true;
                case "relativeType":
                    return true;
                case "relativeFio":
                    return true;
                case "identityCard":
                    return true;
                case "employeePostStatusOld":
                    return true;
                case "dismissalDateOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dismissalDate":
                    return Date.class;
                case "dismissalReason":
                    return EmployeeDismissReasons.class;
                case "holidayNotSupposed":
                    return Boolean.class;
                case "notFulfil":
                    return Double.class;
                case "CompensatingFor":
                    return Double.class;
                case "financingSource":
                    return FinancingSource.class;
                case "holidayPeriodBegin":
                    return Date.class;
                case "holidayPeriodEnd":
                    return Date.class;
                case "additionalConditions":
                    return String.class;
                case "relativeType":
                    return RelationDegree.class;
                case "relativeFio":
                    return String.class;
                case "identityCard":
                    return String.class;
                case "employeePostStatusOld":
                    return EmployeePostStatus.class;
                case "dismissalDateOld":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DismissalExtract> _dslPath = new Path<DismissalExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DismissalExtract");
    }
            

    /**
     * @return Дата увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getDismissalDate()
     */
    public static PropertyPath<Date> dismissalDate()
    {
        return _dslPath.dismissalDate();
    }

    /**
     * @return Причина увольнения.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getDismissalReason()
     */
    public static EmployeeDismissReasons.Path<EmployeeDismissReasons> dismissalReason()
    {
        return _dslPath.dismissalReason();
    }

    /**
     * @return Отпуск не положен. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#isHolidayNotSupposed()
     */
    public static PropertyPath<Boolean> holidayNotSupposed()
    {
        return _dslPath.holidayNotSupposed();
    }

    /**
     * @return Не отработано, дней.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getNotFulfil()
     */
    public static PropertyPath<Double> notFulfil()
    {
        return _dslPath.notFulfil();
    }

    /**
     * @return Компенсация за, дней.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getCompensatingFor()
     */
    public static PropertyPath<Double> CompensatingFor()
    {
        return _dslPath.CompensatingFor();
    }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Отпуск за период (с).
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getHolidayPeriodBegin()
     */
    public static PropertyPath<Date> holidayPeriodBegin()
    {
        return _dslPath.holidayPeriodBegin();
    }

    /**
     * @return Отпуск за период (по).
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getHolidayPeriodEnd()
     */
    public static PropertyPath<Date> holidayPeriodEnd()
    {
        return _dslPath.holidayPeriodEnd();
    }

    /**
     * @return Дополнительные условия.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getAdditionalConditions()
     */
    public static PropertyPath<String> additionalConditions()
    {
        return _dslPath.additionalConditions();
    }

    /**
     * @return Степень родства.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getRelativeType()
     */
    public static RelationDegree.Path<RelationDegree> relativeType()
    {
        return _dslPath.relativeType();
    }

    /**
     * @return ФИО родственника (в дательном падеже).
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getRelativeFio()
     */
    public static PropertyPath<String> relativeFio()
    {
        return _dslPath.relativeFio();
    }

    /**
     * @return Сведения об удостоверении личности родственника.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getIdentityCard()
     */
    public static PropertyPath<String> identityCard()
    {
        return _dslPath.identityCard();
    }

    /**
     * @return Статус на должности до увольнения.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getEmployeePostStatusOld()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusOld()
    {
        return _dslPath.employeePostStatusOld();
    }

    /**
     * @return Дата увольнения до увольнения.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getDismissalDateOld()
     */
    public static PropertyPath<Date> dismissalDateOld()
    {
        return _dslPath.dismissalDateOld();
    }

    public static class Path<E extends DismissalExtract> extends ModularEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _dismissalDate;
        private EmployeeDismissReasons.Path<EmployeeDismissReasons> _dismissalReason;
        private PropertyPath<Boolean> _holidayNotSupposed;
        private PropertyPath<Double> _notFulfil;
        private PropertyPath<Double> _CompensatingFor;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private PropertyPath<Date> _holidayPeriodBegin;
        private PropertyPath<Date> _holidayPeriodEnd;
        private PropertyPath<String> _additionalConditions;
        private RelationDegree.Path<RelationDegree> _relativeType;
        private PropertyPath<String> _relativeFio;
        private PropertyPath<String> _identityCard;
        private EmployeePostStatus.Path<EmployeePostStatus> _employeePostStatusOld;
        private PropertyPath<Date> _dismissalDateOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getDismissalDate()
     */
        public PropertyPath<Date> dismissalDate()
        {
            if(_dismissalDate == null )
                _dismissalDate = new PropertyPath<Date>(DismissalExtractGen.P_DISMISSAL_DATE, this);
            return _dismissalDate;
        }

    /**
     * @return Причина увольнения.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getDismissalReason()
     */
        public EmployeeDismissReasons.Path<EmployeeDismissReasons> dismissalReason()
        {
            if(_dismissalReason == null )
                _dismissalReason = new EmployeeDismissReasons.Path<EmployeeDismissReasons>(L_DISMISSAL_REASON, this);
            return _dismissalReason;
        }

    /**
     * @return Отпуск не положен. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#isHolidayNotSupposed()
     */
        public PropertyPath<Boolean> holidayNotSupposed()
        {
            if(_holidayNotSupposed == null )
                _holidayNotSupposed = new PropertyPath<Boolean>(DismissalExtractGen.P_HOLIDAY_NOT_SUPPOSED, this);
            return _holidayNotSupposed;
        }

    /**
     * @return Не отработано, дней.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getNotFulfil()
     */
        public PropertyPath<Double> notFulfil()
        {
            if(_notFulfil == null )
                _notFulfil = new PropertyPath<Double>(DismissalExtractGen.P_NOT_FULFIL, this);
            return _notFulfil;
        }

    /**
     * @return Компенсация за, дней.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getCompensatingFor()
     */
        public PropertyPath<Double> CompensatingFor()
        {
            if(_CompensatingFor == null )
                _CompensatingFor = new PropertyPath<Double>(DismissalExtractGen.P_COMPENSATING_FOR, this);
            return _CompensatingFor;
        }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Отпуск за период (с).
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getHolidayPeriodBegin()
     */
        public PropertyPath<Date> holidayPeriodBegin()
        {
            if(_holidayPeriodBegin == null )
                _holidayPeriodBegin = new PropertyPath<Date>(DismissalExtractGen.P_HOLIDAY_PERIOD_BEGIN, this);
            return _holidayPeriodBegin;
        }

    /**
     * @return Отпуск за период (по).
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getHolidayPeriodEnd()
     */
        public PropertyPath<Date> holidayPeriodEnd()
        {
            if(_holidayPeriodEnd == null )
                _holidayPeriodEnd = new PropertyPath<Date>(DismissalExtractGen.P_HOLIDAY_PERIOD_END, this);
            return _holidayPeriodEnd;
        }

    /**
     * @return Дополнительные условия.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getAdditionalConditions()
     */
        public PropertyPath<String> additionalConditions()
        {
            if(_additionalConditions == null )
                _additionalConditions = new PropertyPath<String>(DismissalExtractGen.P_ADDITIONAL_CONDITIONS, this);
            return _additionalConditions;
        }

    /**
     * @return Степень родства.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getRelativeType()
     */
        public RelationDegree.Path<RelationDegree> relativeType()
        {
            if(_relativeType == null )
                _relativeType = new RelationDegree.Path<RelationDegree>(L_RELATIVE_TYPE, this);
            return _relativeType;
        }

    /**
     * @return ФИО родственника (в дательном падеже).
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getRelativeFio()
     */
        public PropertyPath<String> relativeFio()
        {
            if(_relativeFio == null )
                _relativeFio = new PropertyPath<String>(DismissalExtractGen.P_RELATIVE_FIO, this);
            return _relativeFio;
        }

    /**
     * @return Сведения об удостоверении личности родственника.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getIdentityCard()
     */
        public PropertyPath<String> identityCard()
        {
            if(_identityCard == null )
                _identityCard = new PropertyPath<String>(DismissalExtractGen.P_IDENTITY_CARD, this);
            return _identityCard;
        }

    /**
     * @return Статус на должности до увольнения.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getEmployeePostStatusOld()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusOld()
        {
            if(_employeePostStatusOld == null )
                _employeePostStatusOld = new EmployeePostStatus.Path<EmployeePostStatus>(L_EMPLOYEE_POST_STATUS_OLD, this);
            return _employeePostStatusOld;
        }

    /**
     * @return Дата увольнения до увольнения.
     * @see ru.tandemservice.moveemployee.entity.DismissalExtract#getDismissalDateOld()
     */
        public PropertyPath<Date> dismissalDateOld()
        {
            if(_dismissalDateOld == null )
                _dismissalDateOld = new PropertyPath<Date>(DismissalExtractGen.P_DISMISSAL_DATE_OLD, this);
            return _dismissalDateOld;
        }

        public Class getEntityClass()
        {
            return DismissalExtract.class;
        }

        public String getEntityName()
        {
            return "dismissalExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
