package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWeekWorkLoad;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. О переводе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeTransferSExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract";
    public static final String ENTITY_NAME = "employeeTransferSExtract";
    public static final int VERSION_HASH = -979937197;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_TEMPORARY_TRANSFER = "temporaryTransfer";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_COMPETITION_TYPE = "competitionType";
    public static final String L_POST_TYPE = "postType";
    public static final String L_WEEK_WORK_LOAD = "weekWorkLoad";
    public static final String L_WORK_WEEK_DURATION = "workWeekDuration";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String P_SALARY = "salary";
    public static final String P_HOURLY_PAID = "hourlyPaid";
    public static final String P_OPTIONAL_CONDITION = "optionalCondition";
    public static final String L_LABOUR_CONTRACT_TYPE = "labourContractType";
    public static final String P_LABOUR_CONTRACT_NUMBER = "labourContractNumber";
    public static final String P_LABOUR_CONTRACT_DATE = "labourContractDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_COLLATERAL_AGREEMENT_NUMBER = "collateralAgreementNumber";
    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String L_OLD_EMPLOYEE_POST_STATUS = "oldEmployeePostStatus";
    public static final String P_HOURS_AMOUNT = "hoursAmount";
    public static final String P_NO_MORE = "noMore";
    public static final String P_FREELANCE = "freelance";

    private EmployeePost _employeePost;     // Сотрудник, который переводится на новую должность
    private boolean _temporaryTransfer;     // Перевести временно
    private OrgUnit _orgUnit;     // Принимающее подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private CompetitionAssignmentType _competitionType;     // Типы конкурсного назначения на должность
    private PostType _postType;     // Тип назначения на должность
    private EmployeeWeekWorkLoad _weekWorkLoad;     // Продолжительность рабочего времени
    private EmployeeWorkWeekDuration _workWeekDuration;     // Продолжительность трудовой недели
    private EtksLevels _etksLevels;     // Разряд ЕТКС
    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private double _salary;     // Сумма оплаты
    private boolean _hourlyPaid;     // Почасовая оплата
    private String _optionalCondition;     // Произвольная строка условий приема
    private LabourContractType _labourContractType;     // Тип трудового договора
    private String _labourContractNumber;     // Номер трудового договора
    private Date _labourContractDate;     // Дата трудового договора
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private String _collateralAgreementNumber;     // Номер доп. соглашения
    private Date _transferDate;     // Дата доп. соглашения
    private EmployeePostStatus _oldEmployeePostStatus;     // Статус на должности до перевода
    private Integer _hoursAmount;     // Учебная нагрузка (час)
    private Boolean _noMore;     // не более
    private boolean _freelance;     // Вне штата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник, который переводится на новую должность. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник, который переводится на новую должность. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Перевести временно. Свойство не может быть null.
     */
    @NotNull
    public boolean isTemporaryTransfer()
    {
        return _temporaryTransfer;
    }

    /**
     * @param temporaryTransfer Перевести временно. Свойство не может быть null.
     */
    public void setTemporaryTransfer(boolean temporaryTransfer)
    {
        dirty(_temporaryTransfer, temporaryTransfer);
        _temporaryTransfer = temporaryTransfer;
    }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Принимающее подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Типы конкурсного назначения на должность.
     */
    public CompetitionAssignmentType getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Типы конкурсного назначения на должность.
     */
    public void setCompetitionType(CompetitionAssignmentType competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     */
    @NotNull
    public PostType getPostType()
    {
        return _postType;
    }

    /**
     * @param postType Тип назначения на должность. Свойство не может быть null.
     */
    public void setPostType(PostType postType)
    {
        dirty(_postType, postType);
        _postType = postType;
    }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     */
    @NotNull
    public EmployeeWeekWorkLoad getWeekWorkLoad()
    {
        return _weekWorkLoad;
    }

    /**
     * @param weekWorkLoad Продолжительность рабочего времени. Свойство не может быть null.
     */
    public void setWeekWorkLoad(EmployeeWeekWorkLoad weekWorkLoad)
    {
        dirty(_weekWorkLoad, weekWorkLoad);
        _weekWorkLoad = weekWorkLoad;
    }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     */
    @NotNull
    public EmployeeWorkWeekDuration getWorkWeekDuration()
    {
        return _workWeekDuration;
    }

    /**
     * @param workWeekDuration Продолжительность трудовой недели. Свойство не может быть null.
     */
    public void setWorkWeekDuration(EmployeeWorkWeekDuration workWeekDuration)
    {
        dirty(_workWeekDuration, workWeekDuration);
        _workWeekDuration = workWeekDuration;
    }

    /**
     * @return Разряд ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Разряд ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Почасовая оплата. Свойство не может быть null.
     */
    @NotNull
    public boolean isHourlyPaid()
    {
        return _hourlyPaid;
    }

    /**
     * @param hourlyPaid Почасовая оплата. Свойство не может быть null.
     */
    public void setHourlyPaid(boolean hourlyPaid)
    {
        dirty(_hourlyPaid, hourlyPaid);
        _hourlyPaid = hourlyPaid;
    }

    /**
     * @return Произвольная строка условий приема.
     */
    @Length(max=255)
    public String getOptionalCondition()
    {
        return _optionalCondition;
    }

    /**
     * @param optionalCondition Произвольная строка условий приема.
     */
    public void setOptionalCondition(String optionalCondition)
    {
        dirty(_optionalCondition, optionalCondition);
        _optionalCondition = optionalCondition;
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     */
    @NotNull
    public LabourContractType getLabourContractType()
    {
        return _labourContractType;
    }

    /**
     * @param labourContractType Тип трудового договора. Свойство не может быть null.
     */
    public void setLabourContractType(LabourContractType labourContractType)
    {
        dirty(_labourContractType, labourContractType);
        _labourContractType = labourContractType;
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLabourContractNumber()
    {
        return _labourContractNumber;
    }

    /**
     * @param labourContractNumber Номер трудового договора. Свойство не может быть null.
     */
    public void setLabourContractNumber(String labourContractNumber)
    {
        dirty(_labourContractNumber, labourContractNumber);
        _labourContractNumber = labourContractNumber;
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     */
    @NotNull
    public Date getLabourContractDate()
    {
        return _labourContractDate;
    }

    /**
     * @param labourContractDate Дата трудового договора. Свойство не может быть null.
     */
    public void setLabourContractDate(Date labourContractDate)
    {
        dirty(_labourContractDate, labourContractDate);
        _labourContractDate = labourContractDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Номер доп. соглашения.
     */
    @Length(max=255)
    public String getCollateralAgreementNumber()
    {
        return _collateralAgreementNumber;
    }

    /**
     * @param collateralAgreementNumber Номер доп. соглашения.
     */
    public void setCollateralAgreementNumber(String collateralAgreementNumber)
    {
        dirty(_collateralAgreementNumber, collateralAgreementNumber);
        _collateralAgreementNumber = collateralAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения.
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата доп. соглашения.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Статус на должности до перевода.
     */
    public EmployeePostStatus getOldEmployeePostStatus()
    {
        return _oldEmployeePostStatus;
    }

    /**
     * @param oldEmployeePostStatus Статус на должности до перевода.
     */
    public void setOldEmployeePostStatus(EmployeePostStatus oldEmployeePostStatus)
    {
        dirty(_oldEmployeePostStatus, oldEmployeePostStatus);
        _oldEmployeePostStatus = oldEmployeePostStatus;
    }

    /**
     * @return Учебная нагрузка (час).
     */
    public Integer getHoursAmount()
    {
        return _hoursAmount;
    }

    /**
     * @param hoursAmount Учебная нагрузка (час).
     */
    public void setHoursAmount(Integer hoursAmount)
    {
        dirty(_hoursAmount, hoursAmount);
        _hoursAmount = hoursAmount;
    }

    /**
     * @return не более.
     */
    public Boolean getNoMore()
    {
        return _noMore;
    }

    /**
     * @param noMore не более.
     */
    public void setNoMore(Boolean noMore)
    {
        dirty(_noMore, noMore);
        _noMore = noMore;
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreelance()
    {
        return _freelance;
    }

    /**
     * @param freelance Вне штата. Свойство не может быть null.
     */
    public void setFreelance(boolean freelance)
    {
        dirty(_freelance, freelance);
        _freelance = freelance;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeTransferSExtractGen)
        {
            setEmployeePost(((EmployeeTransferSExtract)another).getEmployeePost());
            setTemporaryTransfer(((EmployeeTransferSExtract)another).isTemporaryTransfer());
            setOrgUnit(((EmployeeTransferSExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((EmployeeTransferSExtract)another).getPostBoundedWithQGandQL());
            setCompetitionType(((EmployeeTransferSExtract)another).getCompetitionType());
            setPostType(((EmployeeTransferSExtract)another).getPostType());
            setWeekWorkLoad(((EmployeeTransferSExtract)another).getWeekWorkLoad());
            setWorkWeekDuration(((EmployeeTransferSExtract)another).getWorkWeekDuration());
            setEtksLevels(((EmployeeTransferSExtract)another).getEtksLevels());
            setRaisingCoefficient(((EmployeeTransferSExtract)another).getRaisingCoefficient());
            setSalary(((EmployeeTransferSExtract)another).getSalary());
            setHourlyPaid(((EmployeeTransferSExtract)another).isHourlyPaid());
            setOptionalCondition(((EmployeeTransferSExtract)another).getOptionalCondition());
            setLabourContractType(((EmployeeTransferSExtract)another).getLabourContractType());
            setLabourContractNumber(((EmployeeTransferSExtract)another).getLabourContractNumber());
            setLabourContractDate(((EmployeeTransferSExtract)another).getLabourContractDate());
            setBeginDate(((EmployeeTransferSExtract)another).getBeginDate());
            setEndDate(((EmployeeTransferSExtract)another).getEndDate());
            setCollateralAgreementNumber(((EmployeeTransferSExtract)another).getCollateralAgreementNumber());
            setTransferDate(((EmployeeTransferSExtract)another).getTransferDate());
            setOldEmployeePostStatus(((EmployeeTransferSExtract)another).getOldEmployeePostStatus());
            setHoursAmount(((EmployeeTransferSExtract)another).getHoursAmount());
            setNoMore(((EmployeeTransferSExtract)another).getNoMore());
            setFreelance(((EmployeeTransferSExtract)another).isFreelance());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeTransferSExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeTransferSExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeTransferSExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "temporaryTransfer":
                    return obj.isTemporaryTransfer();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "competitionType":
                    return obj.getCompetitionType();
                case "postType":
                    return obj.getPostType();
                case "weekWorkLoad":
                    return obj.getWeekWorkLoad();
                case "workWeekDuration":
                    return obj.getWorkWeekDuration();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "salary":
                    return obj.getSalary();
                case "hourlyPaid":
                    return obj.isHourlyPaid();
                case "optionalCondition":
                    return obj.getOptionalCondition();
                case "labourContractType":
                    return obj.getLabourContractType();
                case "labourContractNumber":
                    return obj.getLabourContractNumber();
                case "labourContractDate":
                    return obj.getLabourContractDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "collateralAgreementNumber":
                    return obj.getCollateralAgreementNumber();
                case "transferDate":
                    return obj.getTransferDate();
                case "oldEmployeePostStatus":
                    return obj.getOldEmployeePostStatus();
                case "hoursAmount":
                    return obj.getHoursAmount();
                case "noMore":
                    return obj.getNoMore();
                case "freelance":
                    return obj.isFreelance();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "temporaryTransfer":
                    obj.setTemporaryTransfer((Boolean) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((CompetitionAssignmentType) value);
                    return;
                case "postType":
                    obj.setPostType((PostType) value);
                    return;
                case "weekWorkLoad":
                    obj.setWeekWorkLoad((EmployeeWeekWorkLoad) value);
                    return;
                case "workWeekDuration":
                    obj.setWorkWeekDuration((EmployeeWorkWeekDuration) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "hourlyPaid":
                    obj.setHourlyPaid((Boolean) value);
                    return;
                case "optionalCondition":
                    obj.setOptionalCondition((String) value);
                    return;
                case "labourContractType":
                    obj.setLabourContractType((LabourContractType) value);
                    return;
                case "labourContractNumber":
                    obj.setLabourContractNumber((String) value);
                    return;
                case "labourContractDate":
                    obj.setLabourContractDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "collateralAgreementNumber":
                    obj.setCollateralAgreementNumber((String) value);
                    return;
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "oldEmployeePostStatus":
                    obj.setOldEmployeePostStatus((EmployeePostStatus) value);
                    return;
                case "hoursAmount":
                    obj.setHoursAmount((Integer) value);
                    return;
                case "noMore":
                    obj.setNoMore((Boolean) value);
                    return;
                case "freelance":
                    obj.setFreelance((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "temporaryTransfer":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "competitionType":
                        return true;
                case "postType":
                        return true;
                case "weekWorkLoad":
                        return true;
                case "workWeekDuration":
                        return true;
                case "etksLevels":
                        return true;
                case "raisingCoefficient":
                        return true;
                case "salary":
                        return true;
                case "hourlyPaid":
                        return true;
                case "optionalCondition":
                        return true;
                case "labourContractType":
                        return true;
                case "labourContractNumber":
                        return true;
                case "labourContractDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "collateralAgreementNumber":
                        return true;
                case "transferDate":
                        return true;
                case "oldEmployeePostStatus":
                        return true;
                case "hoursAmount":
                        return true;
                case "noMore":
                        return true;
                case "freelance":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "temporaryTransfer":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "competitionType":
                    return true;
                case "postType":
                    return true;
                case "weekWorkLoad":
                    return true;
                case "workWeekDuration":
                    return true;
                case "etksLevels":
                    return true;
                case "raisingCoefficient":
                    return true;
                case "salary":
                    return true;
                case "hourlyPaid":
                    return true;
                case "optionalCondition":
                    return true;
                case "labourContractType":
                    return true;
                case "labourContractNumber":
                    return true;
                case "labourContractDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "collateralAgreementNumber":
                    return true;
                case "transferDate":
                    return true;
                case "oldEmployeePostStatus":
                    return true;
                case "hoursAmount":
                    return true;
                case "noMore":
                    return true;
                case "freelance":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "temporaryTransfer":
                    return Boolean.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "competitionType":
                    return CompetitionAssignmentType.class;
                case "postType":
                    return PostType.class;
                case "weekWorkLoad":
                    return EmployeeWeekWorkLoad.class;
                case "workWeekDuration":
                    return EmployeeWorkWeekDuration.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "salary":
                    return Double.class;
                case "hourlyPaid":
                    return Boolean.class;
                case "optionalCondition":
                    return String.class;
                case "labourContractType":
                    return LabourContractType.class;
                case "labourContractNumber":
                    return String.class;
                case "labourContractDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "collateralAgreementNumber":
                    return String.class;
                case "transferDate":
                    return Date.class;
                case "oldEmployeePostStatus":
                    return EmployeePostStatus.class;
                case "hoursAmount":
                    return Integer.class;
                case "noMore":
                    return Boolean.class;
                case "freelance":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeTransferSExtract> _dslPath = new Path<EmployeeTransferSExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeTransferSExtract");
    }
            

    /**
     * @return Сотрудник, который переводится на новую должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Перевести временно. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#isTemporaryTransfer()
     */
    public static PropertyPath<Boolean> temporaryTransfer()
    {
        return _dslPath.temporaryTransfer();
    }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Типы конкурсного назначения на должность.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getCompetitionType()
     */
    public static CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getPostType()
     */
    public static PostType.Path<PostType> postType()
    {
        return _dslPath.postType();
    }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getWeekWorkLoad()
     */
    public static EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> weekWorkLoad()
    {
        return _dslPath.weekWorkLoad();
    }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getWorkWeekDuration()
     */
    public static EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> workWeekDuration()
    {
        return _dslPath.workWeekDuration();
    }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Почасовая оплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#isHourlyPaid()
     */
    public static PropertyPath<Boolean> hourlyPaid()
    {
        return _dslPath.hourlyPaid();
    }

    /**
     * @return Произвольная строка условий приема.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getOptionalCondition()
     */
    public static PropertyPath<String> optionalCondition()
    {
        return _dslPath.optionalCondition();
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getLabourContractType()
     */
    public static LabourContractType.Path<LabourContractType> labourContractType()
    {
        return _dslPath.labourContractType();
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getLabourContractNumber()
     */
    public static PropertyPath<String> labourContractNumber()
    {
        return _dslPath.labourContractNumber();
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getLabourContractDate()
     */
    public static PropertyPath<Date> labourContractDate()
    {
        return _dslPath.labourContractDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Номер доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getCollateralAgreementNumber()
     */
    public static PropertyPath<String> collateralAgreementNumber()
    {
        return _dslPath.collateralAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Статус на должности до перевода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getOldEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
    {
        return _dslPath.oldEmployeePostStatus();
    }

    /**
     * @return Учебная нагрузка (час).
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getHoursAmount()
     */
    public static PropertyPath<Integer> hoursAmount()
    {
        return _dslPath.hoursAmount();
    }

    /**
     * @return не более.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getNoMore()
     */
    public static PropertyPath<Boolean> noMore()
    {
        return _dslPath.noMore();
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#isFreelance()
     */
    public static PropertyPath<Boolean> freelance()
    {
        return _dslPath.freelance();
    }

    public static class Path<E extends EmployeeTransferSExtract> extends SingleEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<Boolean> _temporaryTransfer;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private CompetitionAssignmentType.Path<CompetitionAssignmentType> _competitionType;
        private PostType.Path<PostType> _postType;
        private EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> _weekWorkLoad;
        private EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> _workWeekDuration;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private PropertyPath<Double> _salary;
        private PropertyPath<Boolean> _hourlyPaid;
        private PropertyPath<String> _optionalCondition;
        private LabourContractType.Path<LabourContractType> _labourContractType;
        private PropertyPath<String> _labourContractNumber;
        private PropertyPath<Date> _labourContractDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _collateralAgreementNumber;
        private PropertyPath<Date> _transferDate;
        private EmployeePostStatus.Path<EmployeePostStatus> _oldEmployeePostStatus;
        private PropertyPath<Integer> _hoursAmount;
        private PropertyPath<Boolean> _noMore;
        private PropertyPath<Boolean> _freelance;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник, который переводится на новую должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Перевести временно. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#isTemporaryTransfer()
     */
        public PropertyPath<Boolean> temporaryTransfer()
        {
            if(_temporaryTransfer == null )
                _temporaryTransfer = new PropertyPath<Boolean>(EmployeeTransferSExtractGen.P_TEMPORARY_TRANSFER, this);
            return _temporaryTransfer;
        }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Типы конкурсного назначения на должность.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getCompetitionType()
     */
        public CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new CompetitionAssignmentType.Path<CompetitionAssignmentType>(L_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getPostType()
     */
        public PostType.Path<PostType> postType()
        {
            if(_postType == null )
                _postType = new PostType.Path<PostType>(L_POST_TYPE, this);
            return _postType;
        }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getWeekWorkLoad()
     */
        public EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> weekWorkLoad()
        {
            if(_weekWorkLoad == null )
                _weekWorkLoad = new EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad>(L_WEEK_WORK_LOAD, this);
            return _weekWorkLoad;
        }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getWorkWeekDuration()
     */
        public EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> workWeekDuration()
        {
            if(_workWeekDuration == null )
                _workWeekDuration = new EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration>(L_WORK_WEEK_DURATION, this);
            return _workWeekDuration;
        }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(EmployeeTransferSExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Почасовая оплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#isHourlyPaid()
     */
        public PropertyPath<Boolean> hourlyPaid()
        {
            if(_hourlyPaid == null )
                _hourlyPaid = new PropertyPath<Boolean>(EmployeeTransferSExtractGen.P_HOURLY_PAID, this);
            return _hourlyPaid;
        }

    /**
     * @return Произвольная строка условий приема.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getOptionalCondition()
     */
        public PropertyPath<String> optionalCondition()
        {
            if(_optionalCondition == null )
                _optionalCondition = new PropertyPath<String>(EmployeeTransferSExtractGen.P_OPTIONAL_CONDITION, this);
            return _optionalCondition;
        }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getLabourContractType()
     */
        public LabourContractType.Path<LabourContractType> labourContractType()
        {
            if(_labourContractType == null )
                _labourContractType = new LabourContractType.Path<LabourContractType>(L_LABOUR_CONTRACT_TYPE, this);
            return _labourContractType;
        }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getLabourContractNumber()
     */
        public PropertyPath<String> labourContractNumber()
        {
            if(_labourContractNumber == null )
                _labourContractNumber = new PropertyPath<String>(EmployeeTransferSExtractGen.P_LABOUR_CONTRACT_NUMBER, this);
            return _labourContractNumber;
        }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getLabourContractDate()
     */
        public PropertyPath<Date> labourContractDate()
        {
            if(_labourContractDate == null )
                _labourContractDate = new PropertyPath<Date>(EmployeeTransferSExtractGen.P_LABOUR_CONTRACT_DATE, this);
            return _labourContractDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeTransferSExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeTransferSExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Номер доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getCollateralAgreementNumber()
     */
        public PropertyPath<String> collateralAgreementNumber()
        {
            if(_collateralAgreementNumber == null )
                _collateralAgreementNumber = new PropertyPath<String>(EmployeeTransferSExtractGen.P_COLLATERAL_AGREEMENT_NUMBER, this);
            return _collateralAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(EmployeeTransferSExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Статус на должности до перевода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getOldEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
        {
            if(_oldEmployeePostStatus == null )
                _oldEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_OLD_EMPLOYEE_POST_STATUS, this);
            return _oldEmployeePostStatus;
        }

    /**
     * @return Учебная нагрузка (час).
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getHoursAmount()
     */
        public PropertyPath<Integer> hoursAmount()
        {
            if(_hoursAmount == null )
                _hoursAmount = new PropertyPath<Integer>(EmployeeTransferSExtractGen.P_HOURS_AMOUNT, this);
            return _hoursAmount;
        }

    /**
     * @return не более.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#getNoMore()
     */
        public PropertyPath<Boolean> noMore()
        {
            if(_noMore == null )
                _noMore = new PropertyPath<Boolean>(EmployeeTransferSExtractGen.P_NO_MORE, this);
            return _noMore;
        }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract#isFreelance()
     */
        public PropertyPath<Boolean> freelance()
        {
            if(_freelance == null )
                _freelance = new PropertyPath<Boolean>(EmployeeTransferSExtractGen.P_FREELANCE, this);
            return _freelance;
        }

        public Class getEntityClass()
        {
            return EmployeeTransferSExtract.class;
        }

        public String getEntityName()
        {
            return "employeeTransferSExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
