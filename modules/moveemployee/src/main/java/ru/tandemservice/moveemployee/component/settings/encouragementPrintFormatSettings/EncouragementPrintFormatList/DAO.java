/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.encouragementPrintFormatSettings.EncouragementPrintFormatList;

import java.util.HashMap;
import java.util.Map;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;

import ru.tandemservice.moveemployee.entity.EncouragementToPrintFormatRelation;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

/**
 * Create by ashaburov
 * Date 29.09.11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder typeBuilder = new MQBuilder(EncouragementType.ENTITY_CLASS, "et");
        typeBuilder.addOrder("et", EncouragementType.P_TITLE, OrderDirection.asc);

        UniBaseUtils.createPage(model.getDataSource(), typeBuilder, getSession());

        MQBuilder relationBuilder = new MQBuilder(EncouragementToPrintFormatRelation.ENTITY_CLASS, "pf");

        Map<EncouragementType, String> typeToPrintFormatMap = new HashMap<EncouragementType, String>();
        for (EncouragementToPrintFormatRelation relation : relationBuilder.<EncouragementToPrintFormatRelation>getResultList(getSession()))
            typeToPrintFormatMap.put(relation.getEncouragementType(), relation.getPrintFormat());

        for (ViewWrapper<EncouragementType> wrapper : ViewWrapper.<EncouragementType>getPatchedList(model.getDataSource()))
            wrapper.setViewProperty("printFormat", typeToPrintFormatMap.get(wrapper.getEntity()));
    }
}
