package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.entity.gen.EmployeeAddSExtractGen;

/**
 * Выписка из сборного приказа по кадровому составу. О приеме сотрудника
 */
public class EmployeeAddSExtract extends EmployeeAddSExtractGen implements IPostAssignExtract
{
    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }
}