/**
 *$Id:$
 */
package ru.tandemservice.moveemployee.base.bo.MoveEmployeeSettingsPaymentStringFormatDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.moveemployee.base.bo.MoveEmployeeSettingsPaymentStringFormatDocument.logic.IPaymentStringFormatDocumentSettingsDAO;
import ru.tandemservice.moveemployee.base.bo.MoveEmployeeSettingsPaymentStringFormatDocument.logic.PaymentStringFormatDocumentSettingsDAO;

/**
 * @author Alexander Shaburov
 * @since 19.10.12
 */
@Configuration
public class MoveEmployeeSettingsPaymentStringFormatDocumentManager extends BusinessObjectManager
{
    public static MoveEmployeeSettingsPaymentStringFormatDocumentManager instance()
    {
        return instance(MoveEmployeeSettingsPaymentStringFormatDocumentManager.class);
    }

    @Bean
    public IPaymentStringFormatDocumentSettingsDAO dao()
    {
        return new PaymentStringFormatDocumentSettingsDAO();
    }
}
