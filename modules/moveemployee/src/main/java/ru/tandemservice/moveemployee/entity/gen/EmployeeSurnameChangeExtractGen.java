package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О смене фамилии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeSurnameChangeExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract";
    public static final String ENTITY_NAME = "employeeSurnameChangeExtract";
    public static final int VERSION_HASH = 1802079628;
    private static IEntityMeta ENTITY_META;

    public static final String P_LAST_NAME = "lastName";
    public static final String P_FIRST_NAME = "firstName";
    public static final String P_MIDDLE_NAME = "middleName";
    public static final String L_IC_CARD_TYPE = "icCardType";
    public static final String P_IC_SERIA = "icSeria";
    public static final String P_IC_NUMBER = "icNumber";
    public static final String P_IC_ISSUANCE_DATE = "icIssuanceDate";
    public static final String P_IC_ISSUANCE_PLACE = "icIssuancePlace";
    public static final String L_IDENTITY_CARD_NEW = "identityCardNew";
    public static final String L_IDENTITY_CARD_OLD = "identityCardOld";

    private String _lastName;     // Новая фамилия
    private String _firstName;     // Новое имя
    private String _middleName;     // Новое отчество
    private IdentityCardType _icCardType;     // Тип нового удостоверения личности
    private String _icSeria;     // Серия нового удостоверения личности
    private String _icNumber;     // Номер нового удостоверения личности
    private Date _icIssuanceDate;     // Дата выдачи нового удостоверения личности
    private String _icIssuancePlace;     // Кем выдано новое удостоверение личности
    private IdentityCard _identityCardNew;     // Новое удостоверение личности
    private IdentityCard _identityCardOld;     // Активное удостоверение личности на момент проведения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Новая фамилия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLastName()
    {
        return _lastName;
    }

    /**
     * @param lastName Новая фамилия. Свойство не может быть null.
     */
    public void setLastName(String lastName)
    {
        dirty(_lastName, lastName);
        _lastName = lastName;
    }

    /**
     * @return Новое имя. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFirstName()
    {
        return _firstName;
    }

    /**
     * @param firstName Новое имя. Свойство не может быть null.
     */
    public void setFirstName(String firstName)
    {
        dirty(_firstName, firstName);
        _firstName = firstName;
    }

    /**
     * @return Новое отчество.
     */
    @Length(max=255)
    public String getMiddleName()
    {
        return _middleName;
    }

    /**
     * @param middleName Новое отчество.
     */
    public void setMiddleName(String middleName)
    {
        dirty(_middleName, middleName);
        _middleName = middleName;
    }

    /**
     * @return Тип нового удостоверения личности. Свойство не может быть null.
     */
    @NotNull
    public IdentityCardType getIcCardType()
    {
        return _icCardType;
    }

    /**
     * @param icCardType Тип нового удостоверения личности. Свойство не может быть null.
     */
    public void setIcCardType(IdentityCardType icCardType)
    {
        dirty(_icCardType, icCardType);
        _icCardType = icCardType;
    }

    /**
     * @return Серия нового удостоверения личности.
     */
    @Length(max=255)
    public String getIcSeria()
    {
        return _icSeria;
    }

    /**
     * @param icSeria Серия нового удостоверения личности.
     */
    public void setIcSeria(String icSeria)
    {
        dirty(_icSeria, icSeria);
        _icSeria = icSeria;
    }

    /**
     * @return Номер нового удостоверения личности.
     */
    @Length(max=255)
    public String getIcNumber()
    {
        return _icNumber;
    }

    /**
     * @param icNumber Номер нового удостоверения личности.
     */
    public void setIcNumber(String icNumber)
    {
        dirty(_icNumber, icNumber);
        _icNumber = icNumber;
    }

    /**
     * @return Дата выдачи нового удостоверения личности.
     */
    public Date getIcIssuanceDate()
    {
        return _icIssuanceDate;
    }

    /**
     * @param icIssuanceDate Дата выдачи нового удостоверения личности.
     */
    public void setIcIssuanceDate(Date icIssuanceDate)
    {
        dirty(_icIssuanceDate, icIssuanceDate);
        _icIssuanceDate = icIssuanceDate;
    }

    /**
     * @return Кем выдано новое удостоверение личности.
     */
    @Length(max=255)
    public String getIcIssuancePlace()
    {
        return _icIssuancePlace;
    }

    /**
     * @param icIssuancePlace Кем выдано новое удостоверение личности.
     */
    public void setIcIssuancePlace(String icIssuancePlace)
    {
        dirty(_icIssuancePlace, icIssuancePlace);
        _icIssuancePlace = icIssuancePlace;
    }

    /**
     * @return Новое удостоверение личности.
     */
    public IdentityCard getIdentityCardNew()
    {
        return _identityCardNew;
    }

    /**
     * @param identityCardNew Новое удостоверение личности.
     */
    public void setIdentityCardNew(IdentityCard identityCardNew)
    {
        dirty(_identityCardNew, identityCardNew);
        _identityCardNew = identityCardNew;
    }

    /**
     * @return Активное удостоверение личности на момент проведения. Свойство не может быть null.
     */
    @NotNull
    public IdentityCard getIdentityCardOld()
    {
        return _identityCardOld;
    }

    /**
     * @param identityCardOld Активное удостоверение личности на момент проведения. Свойство не может быть null.
     */
    public void setIdentityCardOld(IdentityCard identityCardOld)
    {
        dirty(_identityCardOld, identityCardOld);
        _identityCardOld = identityCardOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeSurnameChangeExtractGen)
        {
            setLastName(((EmployeeSurnameChangeExtract)another).getLastName());
            setFirstName(((EmployeeSurnameChangeExtract)another).getFirstName());
            setMiddleName(((EmployeeSurnameChangeExtract)another).getMiddleName());
            setIcCardType(((EmployeeSurnameChangeExtract)another).getIcCardType());
            setIcSeria(((EmployeeSurnameChangeExtract)another).getIcSeria());
            setIcNumber(((EmployeeSurnameChangeExtract)another).getIcNumber());
            setIcIssuanceDate(((EmployeeSurnameChangeExtract)another).getIcIssuanceDate());
            setIcIssuancePlace(((EmployeeSurnameChangeExtract)another).getIcIssuancePlace());
            setIdentityCardNew(((EmployeeSurnameChangeExtract)another).getIdentityCardNew());
            setIdentityCardOld(((EmployeeSurnameChangeExtract)another).getIdentityCardOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeSurnameChangeExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeSurnameChangeExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeSurnameChangeExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "lastName":
                    return obj.getLastName();
                case "firstName":
                    return obj.getFirstName();
                case "middleName":
                    return obj.getMiddleName();
                case "icCardType":
                    return obj.getIcCardType();
                case "icSeria":
                    return obj.getIcSeria();
                case "icNumber":
                    return obj.getIcNumber();
                case "icIssuanceDate":
                    return obj.getIcIssuanceDate();
                case "icIssuancePlace":
                    return obj.getIcIssuancePlace();
                case "identityCardNew":
                    return obj.getIdentityCardNew();
                case "identityCardOld":
                    return obj.getIdentityCardOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "lastName":
                    obj.setLastName((String) value);
                    return;
                case "firstName":
                    obj.setFirstName((String) value);
                    return;
                case "middleName":
                    obj.setMiddleName((String) value);
                    return;
                case "icCardType":
                    obj.setIcCardType((IdentityCardType) value);
                    return;
                case "icSeria":
                    obj.setIcSeria((String) value);
                    return;
                case "icNumber":
                    obj.setIcNumber((String) value);
                    return;
                case "icIssuanceDate":
                    obj.setIcIssuanceDate((Date) value);
                    return;
                case "icIssuancePlace":
                    obj.setIcIssuancePlace((String) value);
                    return;
                case "identityCardNew":
                    obj.setIdentityCardNew((IdentityCard) value);
                    return;
                case "identityCardOld":
                    obj.setIdentityCardOld((IdentityCard) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "lastName":
                        return true;
                case "firstName":
                        return true;
                case "middleName":
                        return true;
                case "icCardType":
                        return true;
                case "icSeria":
                        return true;
                case "icNumber":
                        return true;
                case "icIssuanceDate":
                        return true;
                case "icIssuancePlace":
                        return true;
                case "identityCardNew":
                        return true;
                case "identityCardOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "lastName":
                    return true;
                case "firstName":
                    return true;
                case "middleName":
                    return true;
                case "icCardType":
                    return true;
                case "icSeria":
                    return true;
                case "icNumber":
                    return true;
                case "icIssuanceDate":
                    return true;
                case "icIssuancePlace":
                    return true;
                case "identityCardNew":
                    return true;
                case "identityCardOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "lastName":
                    return String.class;
                case "firstName":
                    return String.class;
                case "middleName":
                    return String.class;
                case "icCardType":
                    return IdentityCardType.class;
                case "icSeria":
                    return String.class;
                case "icNumber":
                    return String.class;
                case "icIssuanceDate":
                    return Date.class;
                case "icIssuancePlace":
                    return String.class;
                case "identityCardNew":
                    return IdentityCard.class;
                case "identityCardOld":
                    return IdentityCard.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeSurnameChangeExtract> _dslPath = new Path<EmployeeSurnameChangeExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeSurnameChangeExtract");
    }
            

    /**
     * @return Новая фамилия. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getLastName()
     */
    public static PropertyPath<String> lastName()
    {
        return _dslPath.lastName();
    }

    /**
     * @return Новое имя. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getFirstName()
     */
    public static PropertyPath<String> firstName()
    {
        return _dslPath.firstName();
    }

    /**
     * @return Новое отчество.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getMiddleName()
     */
    public static PropertyPath<String> middleName()
    {
        return _dslPath.middleName();
    }

    /**
     * @return Тип нового удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcCardType()
     */
    public static IdentityCardType.Path<IdentityCardType> icCardType()
    {
        return _dslPath.icCardType();
    }

    /**
     * @return Серия нового удостоверения личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcSeria()
     */
    public static PropertyPath<String> icSeria()
    {
        return _dslPath.icSeria();
    }

    /**
     * @return Номер нового удостоверения личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcNumber()
     */
    public static PropertyPath<String> icNumber()
    {
        return _dslPath.icNumber();
    }

    /**
     * @return Дата выдачи нового удостоверения личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcIssuanceDate()
     */
    public static PropertyPath<Date> icIssuanceDate()
    {
        return _dslPath.icIssuanceDate();
    }

    /**
     * @return Кем выдано новое удостоверение личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcIssuancePlace()
     */
    public static PropertyPath<String> icIssuancePlace()
    {
        return _dslPath.icIssuancePlace();
    }

    /**
     * @return Новое удостоверение личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIdentityCardNew()
     */
    public static IdentityCard.Path<IdentityCard> identityCardNew()
    {
        return _dslPath.identityCardNew();
    }

    /**
     * @return Активное удостоверение личности на момент проведения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIdentityCardOld()
     */
    public static IdentityCard.Path<IdentityCard> identityCardOld()
    {
        return _dslPath.identityCardOld();
    }

    public static class Path<E extends EmployeeSurnameChangeExtract> extends ModularEmployeeExtract.Path<E>
    {
        private PropertyPath<String> _lastName;
        private PropertyPath<String> _firstName;
        private PropertyPath<String> _middleName;
        private IdentityCardType.Path<IdentityCardType> _icCardType;
        private PropertyPath<String> _icSeria;
        private PropertyPath<String> _icNumber;
        private PropertyPath<Date> _icIssuanceDate;
        private PropertyPath<String> _icIssuancePlace;
        private IdentityCard.Path<IdentityCard> _identityCardNew;
        private IdentityCard.Path<IdentityCard> _identityCardOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новая фамилия. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getLastName()
     */
        public PropertyPath<String> lastName()
        {
            if(_lastName == null )
                _lastName = new PropertyPath<String>(EmployeeSurnameChangeExtractGen.P_LAST_NAME, this);
            return _lastName;
        }

    /**
     * @return Новое имя. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getFirstName()
     */
        public PropertyPath<String> firstName()
        {
            if(_firstName == null )
                _firstName = new PropertyPath<String>(EmployeeSurnameChangeExtractGen.P_FIRST_NAME, this);
            return _firstName;
        }

    /**
     * @return Новое отчество.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getMiddleName()
     */
        public PropertyPath<String> middleName()
        {
            if(_middleName == null )
                _middleName = new PropertyPath<String>(EmployeeSurnameChangeExtractGen.P_MIDDLE_NAME, this);
            return _middleName;
        }

    /**
     * @return Тип нового удостоверения личности. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcCardType()
     */
        public IdentityCardType.Path<IdentityCardType> icCardType()
        {
            if(_icCardType == null )
                _icCardType = new IdentityCardType.Path<IdentityCardType>(L_IC_CARD_TYPE, this);
            return _icCardType;
        }

    /**
     * @return Серия нового удостоверения личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcSeria()
     */
        public PropertyPath<String> icSeria()
        {
            if(_icSeria == null )
                _icSeria = new PropertyPath<String>(EmployeeSurnameChangeExtractGen.P_IC_SERIA, this);
            return _icSeria;
        }

    /**
     * @return Номер нового удостоверения личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcNumber()
     */
        public PropertyPath<String> icNumber()
        {
            if(_icNumber == null )
                _icNumber = new PropertyPath<String>(EmployeeSurnameChangeExtractGen.P_IC_NUMBER, this);
            return _icNumber;
        }

    /**
     * @return Дата выдачи нового удостоверения личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcIssuanceDate()
     */
        public PropertyPath<Date> icIssuanceDate()
        {
            if(_icIssuanceDate == null )
                _icIssuanceDate = new PropertyPath<Date>(EmployeeSurnameChangeExtractGen.P_IC_ISSUANCE_DATE, this);
            return _icIssuanceDate;
        }

    /**
     * @return Кем выдано новое удостоверение личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIcIssuancePlace()
     */
        public PropertyPath<String> icIssuancePlace()
        {
            if(_icIssuancePlace == null )
                _icIssuancePlace = new PropertyPath<String>(EmployeeSurnameChangeExtractGen.P_IC_ISSUANCE_PLACE, this);
            return _icIssuancePlace;
        }

    /**
     * @return Новое удостоверение личности.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIdentityCardNew()
     */
        public IdentityCard.Path<IdentityCard> identityCardNew()
        {
            if(_identityCardNew == null )
                _identityCardNew = new IdentityCard.Path<IdentityCard>(L_IDENTITY_CARD_NEW, this);
            return _identityCardNew;
        }

    /**
     * @return Активное удостоверение личности на момент проведения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract#getIdentityCardOld()
     */
        public IdentityCard.Path<IdentityCard> identityCardOld()
        {
            if(_identityCardOld == null )
                _identityCardOld = new IdentityCard.Path<IdentityCard>(L_IDENTITY_CARD_OLD, this);
            return _identityCardOld;
        }

        public Class getEntityClass()
        {
            return EmployeeSurnameChangeExtract.class;
        }

        public String getEntityName()
        {
            return "employeeSurnameChangeExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
