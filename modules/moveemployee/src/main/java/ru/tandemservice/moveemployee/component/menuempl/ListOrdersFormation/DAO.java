/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.menuempl.ListOrdersFormation;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.IMoveEmployeeMQBuilder;
import ru.tandemservice.moveemployee.component.commons.MoveEmployeeMQBuilder;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 30.10.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderDescriptionRegistry = new OrderDescriptionRegistry(IMoveEmployeeMQBuilder.ORDER_ALIAS);

    static
    {
        //_orderDescriptionRegistry.setOrders(Model.ORG_UNI_FULL_TITLE, new OrderDescription(IMoveStudentMQBuilder.ORDER_ALIAS, new String[]{StudentListOrder.L_ORG_UNIT, OrgUnit.P_TITLE}), new OrderDescription(IMoveStudentMQBuilder.ORDER_ALIAS, new String[]{StudentListOrder.L_ORG_UNIT, OrgUnit.L_ORG_UNIT_TYPE, OrgUnitType.P_TITLE}));
    }

    @Override
    public void prepare(Model model)
    {
        model.setOrderStateList(getCatalogItemList(OrderStates.class));
        model.getOrderStateList().remove(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

        MQBuilder builder = new MQBuilder(EmployeeExtractType.ENTITY_CLASS, "e");
        builder.add(MQExpression.or(
                MQExpression.eq("e", EmployeeExtractType.P_ACTIVE, Boolean.TRUE),
                MQExpression.in("e", EmployeeExtractType.CATALOG_ITEM_CODE,
                        new MQBuilder(EmployeeListOrder.ENTITY_CLASS, "ext", new String[]{EmployeeListOrder.type().code().s()}))));
        builder.add(MQExpression.eq("e", EmployeeExtractType.L_PARENT + "." + EmployeeExtractType.P_CODE, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_LIST_ORDER_CODE));
        model.setOrderTypeList(builder.<EmployeeExtractType>getResultList(getSession()));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        // init datasource
        IMoveEmployeeMQBuilder builder = MoveEmployeeMQBuilder.createOrderMQBuilder(EmployeeListOrder.class, model.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.notEq(IMoveEmployeeMQBuilder.STATE_ALIAS, ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

        // apply filters
        builder.applyOrderCommitDate();
        builder.applyOrderCreateDate();
        builder.applyOrderNumber();
        builder.applyOrderState();
        builder.applyListOrderType();

        // apply order
        _orderDescriptionRegistry.applyOrder(builder.getMQBuilder(), model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder.getMQBuilder(), getSession());

        // get statictics for view properties
        String hql = "select " + IAbstractParagraph.L_ORDER + ".id, count(*) from " + EmployeeListParagraph.ENTITY_CLASS + " group by " + IAbstractParagraph.L_ORDER;
        Map<Long, Integer> id2count = new HashMap<>();
        for (Object[] row : (List<Object[]>) getSession().createQuery(hql).list())
        {
            Long id = (Long) row[0];
            Number count = (Number) row[1];
            id2count.put(id, count == null ? 0 : count.intValue());
        }

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            Integer value = id2count.get(wrapper.getId());
            wrapper.setViewProperty(AbstractEmployeeOrder.P_COUNT_PARAGRAPH, value == null ? 0 : value);
            wrapper.setViewProperty("disabledPrint", value == null);
        }
    }

    @Override
    public void deleteRow(IBusinessComponent context)
    {
        IAbstractOrder order = get((Long) context.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
    }
}