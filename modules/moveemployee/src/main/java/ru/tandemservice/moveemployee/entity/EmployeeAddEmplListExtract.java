package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.component.commons.ISingleExtractParagraphExtract;
import ru.tandemservice.moveemployee.entity.gen.EmployeeAddEmplListExtractGen;

/**
 * Выписка из списочного приказа по кадровому составу. О приеме сотрудника
 */
public class EmployeeAddEmplListExtract extends EmployeeAddEmplListExtractGen implements IPostAssignExtract, ISingleExtractParagraphExtract
{
    public static final String P_STAFF_RATES_STR = "staffRatesStr";

    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }
}