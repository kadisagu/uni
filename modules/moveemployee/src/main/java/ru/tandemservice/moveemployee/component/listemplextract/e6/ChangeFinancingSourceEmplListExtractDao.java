/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e6;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract;
import ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class ChangeFinancingSourceEmplListExtractDao extends UniBaseDao implements IExtractComponentDao<ChangeFinancingSourceEmplListExtract>
{
    @Override
    public void doCommit(ChangeFinancingSourceEmplListExtract extract, Map parameters)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if (errorCollector.hasErrors())
            return;

        MQBuilder relationBuilder = new MQBuilder(StaffRateToFinSrcChangeListExtractRelation.ENTITY_CLASS, "b");
        relationBuilder.add(MQExpression.eq("b", StaffRateToFinSrcChangeListExtractRelation.listExtract().s(), extract));

        List<StaffRateToFinSrcChangeListExtractRelation> relationList = relationBuilder.<StaffRateToFinSrcChangeListExtractRelation>getResultList(getSession());

        if (relationList.size() == 1)
            if (relationList.get(0).getFinancingSource().equals(relationList.get(0).getEmployeePostStaffRateItem().getFinancingSource()))
                if (relationList.get(0).getFinancingSourceItem() != null &&
                        relationList.get(0).getFinancingSourceItem().equals(relationList.get(0).getEmployeePostStaffRateItem().getFinancingSourceItem()))
                    return;

        StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(extract.getEntity().getOrgUnit());

        Double staffRate = 0d;
        for (StaffRateToFinSrcChangeListExtractRelation relation : relationList)
            staffRate += relation.getEmployeePostStaffRateItem().getStaffRate();

        StaffListItem actualStaffListItem = null;
        if (activeStaffList != null)
        {
            MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", StaffListItem.staffList(), activeStaffList));
            builder.add(MQExpression.eq("b", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL(), extract.getEntity().getPostRelation().getPostBoundedWithQGandQL()));
            builder.add(MQExpression.eq("b", StaffListItem.financingSource(), extract.getFinancingSource()));
            builder.add(MQExpression.eq("b", StaffListItem.financingSourceItem(), extract.getFinancingSourceItem()));

            StaffListItem staffListItem = (StaffListItem) builder.uniqueResult(getSession());
            actualStaffListItem = staffListItem;

            MQBuilder allocBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
            allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem(), staffListItem));
            allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost(), extract.getEntity()));
            allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.combination(), false));
            allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.financingSource(), extract.getFinancingSource()));
            allocBuilder.add(MQExpression.eq("b", StaffListAllocationItem.financingSourceItem(), extract.getFinancingSourceItem()));

            Double selfStaffRate = 0d;
            for (StaffListAllocationItem item : allocBuilder.<StaffListAllocationItem>getResultList(getSession()))
                selfStaffRate += item.getStaffRate();

            if (staffListItem == null || staffListItem.getStaffRate() - staffListItem.getOccStaffRate() + selfStaffRate  < staffRate)
                errorCollector.add("Невозможно провести приказ, так как для указанных должностей и источников финансирования не хватает вакантных ставок по штатному расписанию.");
        }

        if (errorCollector.hasErrors())
            return;

        for (StaffRateToFinSrcChangeListExtractRelation relation : relationList)
        {
            EmployeePostStaffRateItem staffRateItem = relation.getEmployeePostStaffRateItem();
            relation.setEmployeePostStaffRateItem(null);
            delete(staffRateItem);
            update(relation);
        }

        getSession().flush();

        if (activeStaffList != null)
        {
            SalaryRaisingCoefficient raisingCoefficient = null;
            for (StaffRateToFinSrcChangeListExtractRelation relation : relationList)
            {
                MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), activeStaffList));
                builder.add(MQExpression.eq("b", StaffListAllocationItem.employeePost().s(), extract.getEntity()));
                builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), extract.getEntity().getPostRelation().getPostBoundedWithQGandQL()));
                builder.add(MQExpression.eq("b", StaffListAllocationItem.financingSource().s(), relation.getFinancingSource()));
                builder.add(MQExpression.eq("b", StaffListAllocationItem.financingSourceItem().s(), relation.getFinancingSourceItem()));
                builder.add(MQExpression.eq("b", StaffListAllocationItem.combination().s(), false));
                builder.add(MQExpression.isNull("b", StaffListAllocationItem.combinationPost().s()));

                StaffListAllocationItem allocationItem = (StaffListAllocationItem) builder.uniqueResult(getSession());

                if (allocationItem != null)
                {
                    raisingCoefficient = allocationItem.getRaisingCoefficient();

                    allocationItem.setEmployeePost(null);
                    update(allocationItem);

                    relation.setStaffListAllocationItem(allocationItem);
                    update(relation);
                }
            }

            StaffListAllocationItem allocationItem = new StaffListAllocationItem();
            allocationItem.setStaffListItem(actualStaffListItem);
            allocationItem.setEmployeePost(extract.getEntity());
            allocationItem.setStaffRate(staffRate);
            allocationItem.setCombination(false);
            allocationItem.setFinancingSource(extract.getFinancingSource());
            allocationItem.setFinancingSourceItem(extract.getFinancingSourceItem());
            allocationItem.setRaisingCoefficient(raisingCoefficient);
            if (allocationItem.getRaisingCoefficient() != null)
                allocationItem.setMonthBaseSalaryFund(allocationItem.getRaisingCoefficient().getRecommendedSalary() * allocationItem.getStaffRate());
            else if (allocationItem.getStaffListItem() != null)
                allocationItem.setMonthBaseSalaryFund(allocationItem.getStaffListItem().getSalary() * allocationItem.getStaffRate());

            extract.setStaffListAllocationItem(allocationItem);

            save(allocationItem);

            StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());
        }

        EmployeePostStaffRateItem staffRateItem = new EmployeePostStaffRateItem();
        staffRateItem.setEmployeePost(extract.getEntity());
        staffRateItem.setStaffRate(staffRate);
        staffRateItem.setFinancingSource(extract.getFinancingSource());
        staffRateItem.setFinancingSourceItem(extract.getFinancingSourceItem());

        extract.setEmployeePostStaffRateItem(staffRateItem);

        save(staffRateItem);

        update(extract);
    }

    @Override
    public void doRollback(ChangeFinancingSourceEmplListExtract extract, Map parameters)
    {
        MQBuilder relationBuilder = new MQBuilder(StaffRateToFinSrcChangeListExtractRelation.ENTITY_CLASS, "b");
        relationBuilder.add(MQExpression.eq("b", StaffRateToFinSrcChangeListExtractRelation.listExtract().s(), extract));

        List<StaffRateToFinSrcChangeListExtractRelation> relationList = relationBuilder.<StaffRateToFinSrcChangeListExtractRelation>getResultList(getSession());

        if (extract.getStaffListAllocationItem() != null)
        {
            StaffListAllocationItem allocationItem = extract.getStaffListAllocationItem();
            extract.setStaffListAllocationItem(null);
            delete(allocationItem);
        }

        if (extract.getEmployeePostStaffRateItem() != null)
        {
            EmployeePostStaffRateItem staffRateItem = extract.getEmployeePostStaffRateItem();
            extract.setEmployeePostStaffRateItem(null);
            delete(staffRateItem);
        }

        update(extract);

        getSession().flush();

        for (StaffRateToFinSrcChangeListExtractRelation relation : relationList)
        {
            if (relation.getStaffListAllocationItem() != null)
            {
                relation.getStaffListAllocationItem().setEmployeePost(extract.getEntity());
                update(relation.getStaffListAllocationItem());
            }

            if (relation.getEmployeePostStaffRateItem() != null)
                continue;

            EmployeePostStaffRateItem rateItem = new EmployeePostStaffRateItem();
            rateItem.setEmployeePost(extract.getEntity());
            rateItem.setFinancingSource(relation.getFinancingSource());
            rateItem.setFinancingSourceItem(relation.getFinancingSourceItem());
            rateItem.setStaffRate(relation.getStaffRate());

            save(rateItem);

            relation.setEmployeePostStaffRateItem(rateItem);
            update(relation);
        }
    }
}
