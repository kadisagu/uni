/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e5.AddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendar;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.List;
import java.util.Map;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 23.09.2009
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<EmployeeHolidaySExtract>
{
    private ISelectModel _holidayTypesListModel;

    private ISingleSelectModel _vacationScheduleModel;
    private ISingleSelectModel _vacationScheduleItemModel;

    private Boolean _hasVacationSchedule;

    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;
    private Map<String, EmployeePostStatus> _postStatusesMap;
    private List<EmployeePostStatus> _postStatusesList;
    private IndustrialCalendar _calendar;
    
    private boolean _needMainHoliday;
    private boolean _needSecondHoliday;
    private boolean _needFirstPeriod = false;

    public final String[] HOLIDAY_TYPES_FOR_PERIOD = new String[]
    {
        UniempDefines.HOLIDAY_TYPE_ANNUAL,
        UniempDefines.HOLIDAY_TYPE_HARMFUL_CONDITIONS,
        UniempDefines.HOLIDAY_TYPE_SOME_AREAS_OF_PUBLIC_CULTURE,
        UniempDefines.HOLIDAY_TYPE_NON_NORMED_WORK_DAY,
        UniempDefines.HOLIDAY_TYPE_EDGE_NORTH,
        UniempDefines.HOLIDAY_TYPE_WORKING_AT_POLLUTED_REGION,
        UniempDefines.HOLIDAY_TYPE_CHERNOBYL_LIQUIDATION,
        UniempDefines.HOLIDAY_TYPE_STATE_EMPLOYEE
    };

    //Getters & Setters

    public Boolean getHasVacationSchedule()
    {
        return _hasVacationSchedule;
    }

    public void setHasVacationSchedule(Boolean hasVacationSchedule)
    {
        _hasVacationSchedule = hasVacationSchedule;
    }

    public ISingleSelectModel getVacationScheduleModel()
    {
        return _vacationScheduleModel;
    }

    public void setVacationScheduleModel(ISingleSelectModel vacationScheduleModel)
    {
        _vacationScheduleModel = vacationScheduleModel;
    }

    public ISingleSelectModel getVacationScheduleItemModel()
    {
        return _vacationScheduleItemModel;
    }

    public void setVacationScheduleItemModel(ISingleSelectModel vacationScheduleItemModel)
    {
        _vacationScheduleItemModel = vacationScheduleItemModel;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public boolean isNeedFirstPeriod()
    {
        return _needFirstPeriod;
    }

    public void setNeedFirstPeriod(boolean needFirstPeriod)
    {
        _needFirstPeriod = needFirstPeriod;
    }

    public ISelectModel getHolidayTypesListModel()
    {
        return _holidayTypesListModel;
    }

    public void setHolidayTypesListModel(ISelectModel holidayTypesListModel)
    {
        this._holidayTypesListModel = holidayTypesListModel;
    }

    public boolean isNeedMainHoliday()
    {
        return _needMainHoliday;
    }

    public void setNeedMainHoliday(boolean needMainHoliday)
    {
        this._needMainHoliday = needMainHoliday;
    }

    public boolean isNeedSecondHoliday()
    {
        return _needSecondHoliday;
    }

    public void setNeedSecondHoliday(boolean needSecondHoliday)
    {
        this._needSecondHoliday = needSecondHoliday;
    }

    public Map<String, EmployeePostStatus> getPostStatusesMap()
    {
        return _postStatusesMap;
    }

    public void setPostStatusesMap(Map<String, EmployeePostStatus> postStatusesMap)
    {
        this._postStatusesMap = postStatusesMap;
    }

    public List<EmployeePostStatus> getPostStatusesList()
    {
        return _postStatusesList;
    }

    public void setPostStatusesList(List<EmployeePostStatus> postStatusesList)
    {
        this._postStatusesList = postStatusesList;
    }

    public IndustrialCalendar getCalendar()
    {
        return _calendar;
    }

    public void setCalendar(IndustrialCalendar calendar)
    {
        _calendar = calendar;
    }
}