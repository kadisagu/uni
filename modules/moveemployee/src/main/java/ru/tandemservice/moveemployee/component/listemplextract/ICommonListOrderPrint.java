/* $Id$ */
package ru.tandemservice.moveemployee.component.listemplextract;

import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;

/**
 * Create by ashaburov
 * Date 23.01.12
 */
public interface ICommonListOrderPrint
{
    public void appendVisas(RtfTableModifier tableModifier, EmployeeListOrder order);
}
