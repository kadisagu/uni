package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.entity.gen.RemovalCombinationExtractGen;

/**
 * Выписка из сборного приказа по кадровому составу. Снятие доплат за работу по совмещению (за увеличение объема работ)
 */
public class RemovalCombinationExtract extends RemovalCombinationExtractGen
{
}