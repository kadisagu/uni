/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.exception.ApplicationException;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public abstract class SingleEmployeeExtractPubDAO<T extends SingleEmployeeExtract, Model extends SingleEmployeeExtractPubModel<T>> extends UniDao<Model> implements ISingleEmployeeExtractPubDAO<T, Model>
{

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(Model model)
    {
        model.setExtract((T)getNotNull(model.getExtractId()));
        model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getExtract().getParagraph().getOrder()));
        model.setSecModel(new CommonPostfixPermissionModel("employeeSingleOrder"));
    }
    
    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
        AbstractEmployeeOrder order = model.getExtract().getParagraph().getOrder();
        
        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(order.getNumber()))
            throw new ApplicationException("Нельзя отправить приказ на согласование без номера.");

        if (order.getCommitDate() == null)
            throw new ApplicationException("Нельзя отправить приказ на согласование без даты приказа.");

        ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
        sendToCoordinationService.init(order, initiator);
        sendToCoordinationService.execute();
    }
    
    @Override
    public void doSendToFormative(Model model)
    {
        AbstractEmployeeOrder order = model.getExtract().getParagraph().getOrder();
        
        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на формирование, так как он на согласовании.");

        //2. надо сменить состояние на формируется
        order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        update(order);
    }
    
    @Override
    public void doReject(Model model)
    {
        IAbstractDocument document = model.getExtract().getParagraph().getOrder();
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(document);
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        } else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(document, UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(document);
            touchService.execute();
        }
    }

    @Override
    public void doCommit(Model model)
    {
        AbstractEmployeeOrder order = model.getExtract().getParagraph().getOrder();
        
        if (order.getNumber() == null)
            throw new ApplicationException("Нельзя проводить приказ без номера.");

        if (order.getCommitDate() == null)
            throw new ApplicationException("Нельзя проводить приказ без даты приказа.");

        try
        {
            MoveDaoFacade.getMoveDao().doCommitOrder(order, UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
        }
        catch (RuntimeException e)
        {
            order.setCommitDateSystem(null);
            throw e;
        }
    }

    @Override
    public void doRollback(Model model)
    {
        MoveDaoFacade.getMoveDao().doRollbackOrder(model.getExtract().getParagraph().getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER), null);
    }
}