/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.utils.system;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.util.system.UniSystemUtils;

/**
 * Create by: ashaburov
 * Date 27.04.2011
 */
public class ListExtractComponentGenerator
{
    private static String _tandemUniPath = UniSystemUtils.getModulesDir().getAbsolutePath();
    private static String _templatePath = _tandemUniPath + "/moveemployee/src/main/java/ru/tandemservice/moveemployee/utils/system/listordertemplates";

    public static void main(String[] args) throws Exception
    {
        for (String index : args)
        {
            int extractIndex = Integer.parseInt(index);
            String[] extractData = ListExtractData.EXTRACT_LIST[extractIndex - 1];
            String entityName = extractData[0];
            boolean multipleParagraphAddEnabled = "true".equals(extractData[2]);

            //            if (entityName.endsWith("Order"))
            //                createOrderComponent(entityName, extractIndex);
            //            else
            createExtractComponent(entityName, extractIndex, multipleParagraphAddEnabled);
        }
    }

    @SuppressWarnings("unused")
    private static void createOrderComponent(String entityName, int extractIndex) throws Exception
    {
        String orderSimpleClass = StringUtils.capitalize(entityName);

        String componentPath = _tandemUniPath + "/movestudent/src/main/java/ru/tandemservice/movestudent/component/listextract/e" + extractIndex;

        UniSystemUtils.initVelocity();

        VelocityContext context = new VelocityContext();
        context.put("entityName", entityName);
        context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        context.put("entityClassName", orderSimpleClass);
        context.put("extractIndex", extractIndex);

        UniSystemUtils.createComponentByTemplate(componentPath + "/ListOrderAddEdit", ListExtractComponentGenerator._templatePath + "/ListOrderAddEdit", context, false);

        UniSystemUtils.createComponentByTemplate(componentPath + "/ListOrderPub", ListExtractComponentGenerator._templatePath + "/ListOrderPub", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + orderSimpleClass + "Print.java", ListExtractComponentGenerator._templatePath + "/orderPrint.vm", context, false);
    }

    private static void createExtractComponent(String entityName, int extractIndex, boolean multipleParagraphAddEnabled) throws Exception
    {
        String extractSimpleClass = StringUtils.capitalize(entityName);

        String componentPath = _tandemUniPath + "/moveemployee/src/main/java/ru/tandemservice/moveemployee/component/listemplextract/e" + extractIndex;

        UniSystemUtils.initVelocity();

        VelocityContext context = new VelocityContext();
        context.put("entityName", entityName);
        context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        context.put("entityClassName", extractSimpleClass);
        context.put("extractIndex", extractIndex);

        UniSystemUtils.createComponentByTemplate(componentPath + "/ExtractAddEdit", _templatePath + "/ExtractAddEdit", context, false);

        //        if(multipleParagraphAddEnabled)
        //            UniSystemUtils.createComponentByTemplate(componentPath + "/MultipleParagraphAdd", _templatePath + "/MultipleParagraphAdd", context, false);

        //        UniSystemUtils.createComponentByTemplate(componentPath + "/ParagraphPub", _templatePath + "/ParagraphPub", context, false);

        UniSystemUtils.createComponentByTemplate(componentPath + "/ListExtractPub", _templatePath + "/ListExtractPub", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Dao.java", _templatePath + "/extractDao.vm", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Print.java", _templatePath + "/orderPrint.vm", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Customizer.java", _templatePath + "/customizer.vm", context, false);
    }
}