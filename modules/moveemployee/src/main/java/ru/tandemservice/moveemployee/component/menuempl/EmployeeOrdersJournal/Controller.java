/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.menuempl.EmployeeOrdersJournal;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;

/**
 * @author alikhanov
 * @since 16.08.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "EmployeeOrdersJournal.filter"));
        prepareListDataSource(component);
        getDao().prepare(model);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<IAbstractOrder> dataSource = UniBaseUtils.createDataSource(component, getDao());
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", IAbstractOrder.P_CREATE_DATE);

        linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                 if ((((ViewWrapper) entity).getEntity() instanceof EmployeeListOrder))
                 {
                     // публикаторы для списочных приказов существует два, они определены, по id система сама их найдет
                     return null;
                 }
                else if ((((ViewWrapper) entity).getEntity() instanceof EmployeeModularOrder))
                    return MoveEmployeeUtils.getModularOrderPubComponent();
                 else if ((((ViewWrapper) entity).getEntity() instanceof EmployeeSingleExtractOrder))
                    return MoveEmployeeUtils.getSingleOrderPubComponent(((EmployeeExtractType) entity.getProperty(EmployeeSingleExtractOrder.L_EXTRACT + "." + IAbstractExtract.L_TYPE)).getIndex());
                return null;
            }
            @Override
            public Object getParameters(IEntity entity)
            {
                 if ((((ViewWrapper) entity).getEntity() instanceof EmployeeListOrder)||(((ViewWrapper) entity).getEntity() instanceof EmployeeModularOrder))
                    return (Long)((ViewWrapper) entity).getEntity().getId();
                 else if ((((ViewWrapper) entity).getEntity() instanceof EmployeeSingleExtractOrder))
                    return (Long) ((EmployeeSingleExtractOrder) ((ViewWrapper) entity).getEntity()).getExtract().getId();
                return null;
            }
        });
        dataSource.addColumn(linkColumn); /*
        ActionColumn publisherColumn = new ActionColumn("Дата формирования", ActionColumn.EDIT, "onClickPub");
        dataSource.addColumn(publisherColumn);  */
        dataSource.addColumn(new SimpleColumn("Дата приказа", AbstractEmployeeOrder.P_COMMIT_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", IAbstractOrder.P_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn(IAbstractOrder.P_COMMIT_DATE_SYSTEM, "Дата проведения", IAbstractOrder.P_COMMIT_DATE_SYSTEM, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Количество выписок", AbstractEmployeeOrder.P_EXTRACT_COUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("print_menuList_EmployeeOrdersJournal").setDisabledProperty("disabledPrint"));

        dataSource.setOrder(IAbstractOrder.P_COMMIT_DATE_SYSTEM, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        DataSettingsFacade.saveSettings(getModel(component).getSettings());
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }
    /*
        public void onClickPub(IBusinessComponent component)
    {
        IAbstractOrder order = getDao().getNotNull((Long) component.getListenerParameter());
        if (order instanceof EmployeeListOrder)
        {
            EmployeeListOrder listOrder = (EmployeeListOrder) order;
            int orderTypeIndex = listOrder.getType().getIndex();
            deactivate(component);
            activateInRoot(component, new ComponentActivator(MoveEmployeeUtils.getListOrderPubComponent(orderTypeIndex), new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, order.getId())));
        }
        else if (order instanceof EmployeeModularOrder)
        {
            deactivate(component);
            activateInRoot(component, new PublisherActivator((EmployeeModularOrder) order));
        }
        else if (order instanceof EmployeeSingleExtractOrder)
        {
            deactivate(component);
            activateInRoot(component, new PublisherActivator(((EmployeeSingleExtractOrder) order).getExtract()));
        }
    }
    */
    public void onClickPrint(IBusinessComponent component)
    {
        IAbstractOrder order = getDao().getNotNull((Long) component.getListenerParameter());
        if (order instanceof EmployeeListOrder)
            activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_LIST_ORDER_PRINT, new ParametersMap()
                    .add("orderId", order.getId())
            ));
        else if (order instanceof EmployeeModularOrder)
            activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_MODULAR_ORDER_PRINT, new ParametersMap()
                    .add("orderId", order.getId())
            ));
        else if (order instanceof EmployeeSingleExtractOrder)
            activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.MODULAR_EMPLOYEE_EXTRACT_PRINT, new ParametersMap()
                    .add("extractId", ((EmployeeSingleExtractOrder) order).getExtract().getId())
            ));
    }


}