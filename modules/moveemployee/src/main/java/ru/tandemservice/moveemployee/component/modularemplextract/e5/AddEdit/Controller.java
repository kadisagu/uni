/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 14.11.2008
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<EmployeeHolidayExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        prepareStaffRateDataSource(component);
        prepareHolidayDataSource(component);
    }

    public void onChangeHolidayType(IBusinessComponent component)
    {
        Model model = getModel(component);
        HolidayType holidayType = model.getExtract().getHolidayType();
        if (null != holidayType)
        {
            if (UniempDefines.HOLIDAY_TYPE_ANNUAL.equals(holidayType.getCode()))
            {
                model.getExtract().setEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
                if (getDao().hasVacationSchedule(model.getEmployeePost()))
                    model.setShowVacationSchedule(true);
                else
                    model.setShowVacationSchedule(false);
            }
            else
                model.setShowVacationSchedule(false);

            if (UniempDefines.HOLIDAY_TYPE_WITHOUT_SALARY.equals(holidayType.getCode()) || UniempDefines.HOLIDAY_TYPE_ENTRANCE_EXAMS_WITHOUT_SALARY.equals(holidayType.getCode()))
                model.getExtract().setEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR));
            else if (UniempDefines.HOLIDAY_TYPE_PREGNANCY_AND_CHILD_BORN.equals(holidayType.getCode()))
                model.getExtract().setEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY));
            else if (UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_1_5.equals(holidayType.getCode()) || UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_3.equals(holidayType.getCode()))
                model.getExtract().setEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE));
            else
                model.getExtract().setEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        } else
        {
            model.getExtract().setEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        }
    }

    public void onChangeVacationScheduleItem(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeHolidayExtract extract = model.getExtract();

        if (extract.getVacationScheduleItem() == null)
            return;

        if (model.getMainRowList().size() != 1 || model.getMainRowList().get(0).getRowList().size() != 1)
            return;


        Row row = model.getMainRowList().get(0).getRowList().get(0);

        if (model.getStartHolidayMap().get(row) == null && model.getFinishHolidayMap().get(row) == null && model.getDurationHolidayMap().get(row) == null)
        {
            Date startDate = extract.getVacationScheduleItem().getFactDate() != null ? extract.getVacationScheduleItem().getFactDate() : extract.getVacationScheduleItem().getPlanDate();
            model.getStartHolidayMap().put(row, startDate);
            model.getFinishHolidayMap().put(row, UniempUtil.getEndDate(model.getEmployeePost().getWorkWeekDuration(), extract.getVacationScheduleItem().getDaysAmount(), startDate));
            model.getDurationHolidayMap().put(row, extract.getVacationScheduleItem().getDaysAmount());
        }
    }

    public void onChangeEndHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);

        Row row = component.getListenerParameter();

        Date startHoliday = model.getStartHolidayMap().get(row);
        Date finishHoliday = model.getFinishHolidayMap().get(row);
        Integer durationHoliday = model.getDurationHolidayMap().get(row);

        if (model.getExtract().getHolidayType() != null && startHoliday != null && finishHoliday != null &&
                (durationHoliday == null || durationHoliday == 0))
        {
//            IEmployeeDAO dao = UniDaoFacade.getEmployeeDao();
            EmployeeWorkWeekDuration workWeekDuration = model.getExtract().getEntity() != null ? model.getExtract().getEntity().getWorkWeekDuration() : model.getEmployeePost().getWorkWeekDuration();
            Integer holidayDuration;
            if (UniempUtil.getAnnualHolidayTypes().contains(model.getExtract().getHolidayType().getCode()) && model.getExtract().getHolidayType().isCompensable())
                holidayDuration = EmployeeManager.instance().dao().getEmployeeHolidayDuration(workWeekDuration, startHoliday, finishHoliday);
            else
                holidayDuration = (int) CommonBaseDateUtil.getBetweenPeriod(startHoliday, finishHoliday, Calendar.DAY_OF_YEAR) + 1;

            if (holidayDuration != null)
                model.getDurationHolidayMap().put(row, holidayDuration);
        }

        onChangeDuration(model);
    }

    public void onChangeDurationHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);

        Row row = component.getListenerParameter();

        Date startHoliday = model.getStartHolidayMap().get(row);
        Date finishHoliday = model.getFinishHolidayMap().get(row);
        Integer durationHoliday = model.getDurationHolidayMap().get(row);

        if (startHoliday != null && durationHoliday != null  &&
                finishHoliday == null && model.getExtract().getHolidayType() != null)
        {
            GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            int counter = 0;
            int duration = durationHoliday;

            date.setTime(startHoliday);

            if (duration > 0)
            {
                do
                {
                    if (UniempUtil.getAnnualHolidayTypes().contains(model.getExtract().getHolidayType().getCode()) && model.getExtract().getHolidayType().isCompensable())
                    {
                        if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                            counter++;
                    }
                    else
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);
            }

            model.getFinishHolidayMap().put(row, date.getTime());
        }

        onChangeDuration(model);
    }

    public void onChangeDuration(Model model)
    {
        Integer result = 0;
        for (MainRow mainRow : model.getMainRowList())
            for (Row row : mainRow.getRowList())
            {
                result += model.getDurationHolidayMap().get(row) != null ? model.getDurationHolidayMap().get(row) : 0;
            }

        model.getExtract().setTotalDuration(result);
    }

    private void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setStaffRateDataSource(dataSource);
    }

    public void prepareHolidayDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getHolidayDataSource() != null)
            return;

        DynamicListDataSource<MainRow> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareHolidayDataSource(model);
        });

        dataSource.addColumn(new BlockColumn("beginPeriod", "Дата начала периода"));
        dataSource.addColumn(new BlockColumn("endPeriod", "Дата окончания периода"));
        dataSource.addColumn(new BlockColumn("startHoliday", "Дата начала отпуска"));
        dataSource.addColumn(new BlockColumn("finishHoliday", "Дата окончания отпуска"));
        dataSource.addColumn(new BlockColumn("durationHoliday", "Длительность отпуска"));
        dataSource.addColumn(new ActionColumn("Добавить период", ActionColumn.ADD, "onClickAddPeriod").setDisplayHeader(true).setDisableHandler(entity -> {
            MainRow mainRow = (MainRow) entity;

            boolean result = mainRow.getType().equals(Model.FULL_HOLIDAY) || mainRow.getType().equals(Model.PERIODS_OF_HOLIDAY);
            return !result;
        }));
        dataSource.addColumn(new ActionColumn("Добавить отпуск", ActionColumn.ADD, "onClickAddHoliday").setDisplayHeader(true).setDisableHandler(entity -> {
            MainRow mainRow = (MainRow) entity;

            boolean result = mainRow.getType().equals(Model.FULL_HOLIDAY) || mainRow.getType().equals(Model.HOLIDAYS_OF_PERIOD);
            return !result;
        }));
        dataSource.addColumn(new BlockColumn("delete", ""));

        model.setHolidayDataSource(dataSource);
    }

    public void onClickAddMainHolidayRow(IBusinessComponent component)
    {
        Model model = getModel(component);

        List<Row> rowList = new ArrayList<>();
        MainRow mainRow = new MainRow(MainRow.getNewMainRowId(model.getMainRowList()), rowList, Model.FULL_HOLIDAY);
        rowList.add(new Row(Row.getNewRowId(model.getMainRowList()), mainRow, Model.FULL_HOLIDAY));
        model.getMainRowList().add(mainRow);

        model.getHolidayDataSource().refresh();
    }

    public void onClickAddPeriod(IBusinessComponent component)
    {
        Model model = getModel(component);

        MainRow mainRow = null;
        for (MainRow row : model.getMainRowList())
            if (row.getId().equals(component.getListenerParameter()))
            {
                mainRow = row;
                break;
            }

        if (mainRow == null)
            throw new IllegalStateException();

        mainRow.setType(Model.PERIODS_OF_HOLIDAY);
        mainRow.getRowList().add(new Row(Row.getNewRowId(model.getMainRowList()), mainRow, Model.PERIODS_OF_HOLIDAY));

        model.getHolidayDataSource().refresh();
    }

    public void onClickAddHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);

        MainRow mainRow = null;
        for (MainRow row : model.getMainRowList())
            if (row.getId().equals(component.getListenerParameter()))
            {
                mainRow = row;
                break;
            }

        if (mainRow == null)
            throw new IllegalStateException();

        mainRow.setType(Model.HOLIDAYS_OF_PERIOD);
        mainRow.getRowList().add(new Row(Row.getNewRowId(model.getMainRowList()), mainRow, Model.HOLIDAYS_OF_PERIOD));

        model.getHolidayDataSource().refresh();
    }

    public void onClickDeleteRow(IBusinessComponent component)
    {
        Model model = getModel(component);

        Long id = component.getListenerParameter();

        for (MainRow mainRow : model.getMainRowList())
            for (Row row : mainRow.getRowList())
                if (row.getId().equals(id))
                {
                    if (row.getType().equals(Model.FULL_HOLIDAY))
                        model.getMainRowList().remove(mainRow);
                    else
                    {
                        mainRow.getRowList().remove(row);
                        if (mainRow.getRowList().size() == 1)
                            mainRow.setType(Model.FULL_HOLIDAY);
                    }


                    model.getHolidayDataSource().refresh();
                    return;
                }
    }
}