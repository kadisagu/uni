/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.settings.ReasonToTypesEmplSettingsPub;

import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationPub.AbstractRelationPubController;

/**
 * @author dseleznev
 * Created on: 26.01.2009
 */
public class Controller extends AbstractRelationPubController<IDAO, Model>
{
    @Override
    protected String getRelationAddingComponentName()
    {
        return IMoveEmployeeComponents.REASON_TO_TYPES_SETTINGS_ADD;
    }

    @Override
    protected String getDeleteRowPermissionKey()
    {
        return null;
    }
}