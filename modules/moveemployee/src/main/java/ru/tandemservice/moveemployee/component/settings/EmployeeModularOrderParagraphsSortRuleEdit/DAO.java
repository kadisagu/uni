/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeModularOrderParagraphsSortRuleEdit;

import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setRule(getNotNull(EmployeeModularOrderParagraphsSortRule.class, model.getRule().getId()));
    }

    @Override
    public void update(Model model)
    {
        update(model.getRule());
    }
}