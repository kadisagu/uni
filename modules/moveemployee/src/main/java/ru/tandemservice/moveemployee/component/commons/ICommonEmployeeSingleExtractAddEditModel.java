/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import java.util.List;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public interface ICommonEmployeeSingleExtractAddEditModel<T extends SingleEmployeeExtract>
{
    T getExtract();
    
    void setExtract(T extract);

    IMultiSelectModel getBasicListModel();

    void setBasicListModel(IMultiSelectModel basicListModel);

    List<EmployeeOrderBasics> getSelectedBasicList();

    void setSelectedBasicList(List<EmployeeOrderBasics> selectedBasicList);
}