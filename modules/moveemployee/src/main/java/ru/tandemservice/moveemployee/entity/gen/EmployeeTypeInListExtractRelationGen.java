package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.EmployeeTypeInListExtractRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выбранных типов должностей в списочном параграфе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeTypeInListExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeTypeInListExtractRelation";
    public static final String ENTITY_NAME = "employeeTypeInListExtractRelation";
    public static final int VERSION_HASH = -614456543;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_LIST_PARAGRAPH = "employeeListParagraph";
    public static final String L_EMPLOYEE_TYPE = "employeeType";

    private EmployeeListParagraph _employeeListParagraph;     // Параграф списочного приказа по сотруднику
    private EmployeeType _employeeType;     // Тип должности/профессии сотрудника

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Параграф списочного приказа по сотруднику. Свойство не может быть null.
     */
    @NotNull
    public EmployeeListParagraph getEmployeeListParagraph()
    {
        return _employeeListParagraph;
    }

    /**
     * @param employeeListParagraph Параграф списочного приказа по сотруднику. Свойство не может быть null.
     */
    public void setEmployeeListParagraph(EmployeeListParagraph employeeListParagraph)
    {
        dirty(_employeeListParagraph, employeeListParagraph);
        _employeeListParagraph = employeeListParagraph;
    }

    /**
     * @return Тип должности/профессии сотрудника. Свойство не может быть null.
     */
    @NotNull
    public EmployeeType getEmployeeType()
    {
        return _employeeType;
    }

    /**
     * @param employeeType Тип должности/профессии сотрудника. Свойство не может быть null.
     */
    public void setEmployeeType(EmployeeType employeeType)
    {
        dirty(_employeeType, employeeType);
        _employeeType = employeeType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeTypeInListExtractRelationGen)
        {
            setEmployeeListParagraph(((EmployeeTypeInListExtractRelation)another).getEmployeeListParagraph());
            setEmployeeType(((EmployeeTypeInListExtractRelation)another).getEmployeeType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeTypeInListExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeTypeInListExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeTypeInListExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeListParagraph":
                    return obj.getEmployeeListParagraph();
                case "employeeType":
                    return obj.getEmployeeType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeListParagraph":
                    obj.setEmployeeListParagraph((EmployeeListParagraph) value);
                    return;
                case "employeeType":
                    obj.setEmployeeType((EmployeeType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeListParagraph":
                        return true;
                case "employeeType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeListParagraph":
                    return true;
                case "employeeType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeListParagraph":
                    return EmployeeListParagraph.class;
                case "employeeType":
                    return EmployeeType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeTypeInListExtractRelation> _dslPath = new Path<EmployeeTypeInListExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeTypeInListExtractRelation");
    }
            

    /**
     * @return Параграф списочного приказа по сотруднику. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTypeInListExtractRelation#getEmployeeListParagraph()
     */
    public static EmployeeListParagraph.Path<EmployeeListParagraph> employeeListParagraph()
    {
        return _dslPath.employeeListParagraph();
    }

    /**
     * @return Тип должности/профессии сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTypeInListExtractRelation#getEmployeeType()
     */
    public static EmployeeType.Path<EmployeeType> employeeType()
    {
        return _dslPath.employeeType();
    }

    public static class Path<E extends EmployeeTypeInListExtractRelation> extends EntityPath<E>
    {
        private EmployeeListParagraph.Path<EmployeeListParagraph> _employeeListParagraph;
        private EmployeeType.Path<EmployeeType> _employeeType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Параграф списочного приказа по сотруднику. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTypeInListExtractRelation#getEmployeeListParagraph()
     */
        public EmployeeListParagraph.Path<EmployeeListParagraph> employeeListParagraph()
        {
            if(_employeeListParagraph == null )
                _employeeListParagraph = new EmployeeListParagraph.Path<EmployeeListParagraph>(L_EMPLOYEE_LIST_PARAGRAPH, this);
            return _employeeListParagraph;
        }

    /**
     * @return Тип должности/профессии сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTypeInListExtractRelation#getEmployeeType()
     */
        public EmployeeType.Path<EmployeeType> employeeType()
        {
            if(_employeeType == null )
                _employeeType = new EmployeeType.Path<EmployeeType>(L_EMPLOYEE_TYPE, this);
            return _employeeType;
        }

        public Class getEntityClass()
        {
            return EmployeeTypeInListExtractRelation.class;
        }

        public String getEntityName()
        {
            return "employeeTypeInListExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
