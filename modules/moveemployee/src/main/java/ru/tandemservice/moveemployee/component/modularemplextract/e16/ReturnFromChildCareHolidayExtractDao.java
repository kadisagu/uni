/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e16;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 08.04.2011
 */
public class ReturnFromChildCareHolidayExtractDao extends UniBaseDao implements IExtractComponentDao<ReturnFromChildCareHolidayExtract>
{
    @Override
    public void doCommit(ReturnFromChildCareHolidayExtract extract, Map parameters)
    {
        List<EmployeePostStaffRateItem> staffRateItemList = CommonExtractCommitUtil.createEmployeePostStaffRateItems(extract, extract.getEntity());

        List<StaffListAllocationItem> allocItemList = CommonExtractCommitUtil.createStaffListAllocationItem(extract, extract.getEntity());

        UniempDaoFacade.getUniempDAO().validateStaffRates(UserContext.getInstance().getErrorCollector(), extract.getEmployee(), extract.getEntity(), staffRateItemList, false,false);

        CommonExtractCommitUtil.validateAllocationItem(extract, extract.getEntity().getOrgUnit(), extract.getEntity().getPostRelation().getPostBoundedWithQGandQL());

        if (UserContext.getInstance().getErrorCollector().hasErrors())
            return;

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        extract.getEntity().setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_ACTIVE));

        if (extract.getCompetitionType() != null)
            extract.getEntity().setCompetitionType(extract.getCompetitionType());

        MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", EmployeeHoliday.employeePost().s(), extract.getEntity()));
        builder.add(MQExpression.eq("b", EmployeeHoliday.holidayType().s(), extract.getHolidayType()));
        builder.add(MQExpression.lessOrEq("b", EmployeeHoliday.startDate().s(), extract.getReturnDate()));
        builder.add(MQExpression.greatOrEq("b", EmployeeHoliday.finishDate().s(), extract.getReturnDate()));

        if (builder.getResultCount(getSession()) == 1)
        {
            EmployeeHoliday holiday = (EmployeeHoliday) builder.uniqueResult(getSession());
            if (holiday != null)
            {
                extract.setHolidayEndOld(holiday.getFinishDate());
                extract.setChangeHoliday(holiday);
                GregorianCalendar date = new GregorianCalendar();
                date.setTime(extract.getReturnDate());
                date.add(Calendar.DAY_OF_MONTH, -1);
                holiday.setFinishDate(date.getTime());

                update(holiday);
            }
        }

        getSession().saveOrUpdate(extract.getEntity());

        List<FinancingSourceDetails> staffRateItems = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
        List<FinancingSourceDetailsToAllocItem> relList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(staffRateItems);
        if (staffRateItems.size() != 0 && relList.size() != 0)
        {
            for (FinancingSourceDetailsToAllocItem rel : relList)
                if (!rel.isHasNewAllocItem())
                {
                    if (rel.getChoseStaffListAllocationItem() != null)
                        delete(rel.getChoseStaffListAllocationItem());
                    rel.setChoseStaffListAllocationItem(null);
                    update(rel);
                }
            for (StaffListAllocationItem oldAllocationItem : UniempDaoFacade.getUniempDAO().getEmployeePostAllocationItemList(extract.getEntity()))
                delete(oldAllocationItem);
            for ( StaffListAllocationItem allocationItem : allocItemList)
            {
                save(allocationItem);
                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());
            }
        }

        for (EmployeePostStaffRateItem oldStaffRateItem : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity()))
            delete(oldStaffRateItem);
        getSession().flush();
        for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
            save(staffRateItem);

        update(extract);
    }

    @Override
    public void doRollback(ReturnFromChildCareHolidayExtract extract, Map parameters)
    {
        if (extract.getCompetitionTypeOld() != null)
            extract.getEntity().setCompetitionType(extract.getCompetitionTypeOld());
        extract.getEntity().setPostStatus(extract.getStateOld());

        EmployeeHoliday changeHoliday = extract.getChangeHoliday();
        if (changeHoliday != null)
        {
            extract.setChangeHoliday(null);
            changeHoliday.setFinishDate(extract.getHolidayEndOld());
            update(changeHoliday);
        }

        MoveEmployeeDaoFacade.getMoveEmployeeDao().rollbackStaffListAllocItemChanges(extract);

        update(extract.getEntity());
        update(extract);
    }
}