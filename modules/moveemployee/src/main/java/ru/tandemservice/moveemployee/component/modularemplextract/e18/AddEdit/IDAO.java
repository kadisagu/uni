/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e18.AddEdit;

import java.util.List;

import org.tandemframework.core.entity.IdentifiableWrapper;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.ICommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 15.09.2011
 */
public interface IDAO extends ICommonModularEmployeeExtractAddEditDAO<TransferAnnualHolidayExtract, Model>
{
    List<IdentifiableWrapper> prepareHolidayModel(Model model);

    void prepareHolidayDataSource(Model model);

    void prepareFactHolidayDataSource(Model model);
}