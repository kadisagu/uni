/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

/**
 * Create by ashaburov
 * Date 11.10.11
 */
public interface IEmployeeListParagraphPrintFormatter
{
    String formatSingleEmployee(EmployeePost post, int extractNumber);
}
