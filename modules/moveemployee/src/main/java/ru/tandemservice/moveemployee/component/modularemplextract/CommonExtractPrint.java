/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IEmployeeOrgUnitExtract;
import ru.tandemservice.moveemployee.component.commons.IEmployeePostExtract;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author dseleznev
 *         Created on: 14.01.2009
 */
public class CommonExtractPrint
{
    public static RtfDocument createModularExtract(byte[] template, ModularEmployeeExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createModularExtractInjectModifier(extract);
        modifier.modify(document);
        return document;
    }

    public static RtfInjectModifier createOrderInjectModifier(EmployeeModularOrder order)
    {
        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("commitDate", order.getCommitDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        modifier.put("orderNumber", order.getNumber());
        //modifier.put("executor", order.getExecutor());
        return modifier;
    }

    public static RtfInjectModifier createModularExtractInjectModifier(ModularEmployeeExtract extract)
    {
        IEntityMeta meta = EntityRuntime.getMeta(extract.getId());

        final RtfInjectModifier modifier = new RtfInjectModifier();
        CommonExtractPrintUtil.injectCommonExtractData(modifier, extract);
        injectAcademy(modifier);

        // reason
        modifier.put("reason", extract.getReason().getTitle());
        modifier.put("reasonPrint", extract.getReason().getPrintTitle() == null ? extract.getReason().getTitle() : extract.getReason().getPrintTitle());
        modifier.put("reasonPrintUncapitalize", extract.getReason().getPrintTitle() == null ? "" : StringUtils.uncapitalize(extract.getReason().getPrintTitle()));
        modifier.put("reasonPrintCase", extract.getReason().getPrintTitle() == null ? "" : extract.getReason().getPrintTitle());

        // listBasics
        modifier.put("listBasics", extract.getBasicListStr());


        // fio
        modifier.put("fio", extract.getEmployeeFioModified());
        modifier.put("fio_Mod", extract.getEmployeeFioModified());

        String fio = extract.getEmployeeFioModified();
        int indexOfSpace = fio.indexOf(" ");
        String fioCapitalized = indexOfSpace <= 0 ? fio.toUpperCase() : (fio.substring(0, indexOfSpace).toUpperCase() + fio.substring(indexOfSpace));
        modifier.put("Fio", fioCapitalized);
        modifier.put("Fio_Mod", fioCapitalized);

        CommonExtractPrintUtil.injectCommonEmployeeData(modifier, extract.getEmployee());

        // OrgUnit title
        OrgUnit orgUnit = null;
        if (extract instanceof IEmployeeOrgUnitExtract)
            orgUnit = ((IEmployeeOrgUnitExtract) extract).getOrgUnit();
        else if (null != extract.getEntity())
            orgUnit = extract.getEntity().getOrgUnit();

        CommonExtractPrintUtil.injectOrgUnit(modifier, orgUnit, null);


        // post title
        PostBoundedWithQGandQL post = null;
        if (extract instanceof IEmployeePostExtract)
            post = ((IEmployeePostExtract) extract).getPostBoundedWithQGandQL();
        else if (null != extract.getEntity())
            post = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL();

        CommonExtractPrintUtil.injectPost(modifier, post, null);


        // SPECIAL CODES //

        // orderType
        modifier.put("orderType", extract.getType().getTitle());

        // beginDate
        if (meta.getProperty("beginDate") != null)
            modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) extract.getProperty("beginDate")));

        if (meta.getProperty("postType") != null)
        {
            PostType postType = (PostType) extract.getProperty("postType");
            if (UniDefines.POST_TYPE_SECOND_JOB.equals(postType.getCode()) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(postType.getCode()) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(postType.getCode()))
                CommonExtractPrintUtil.injectDifferentVariations(modifier, "postType", "по совместительству");
            else
                CommonExtractPrintUtil.injectDifferentVariations(modifier, "postType", null);
        }

        // employment period
        if (meta.getProperty("beginDate") != null && extract.getProperty("beginDate") != null)
        {
            Date beginDate = (Date) extract.getProperty("beginDate");
            Date endDate = null != meta.getProperty("endDate") ? (Date) extract.getProperty("endDate") : null;
            CompetitionAssignmentType competitionAssignmentType = null != meta.getProperty("competitionType") ? (CompetitionAssignmentType) extract.getProperty("competitionType") : null;

            StringBuilder periodBuilder = new StringBuilder("c ");
            StringBuilder periodAltBuilder = new StringBuilder("c ");
            StringBuilder monthStrPeriodBuilder = new StringBuilder("c ");
            StringBuilder monthStrPeriodAltBuilder = new StringBuilder("c ");

            periodBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate));
            monthStrPeriodBuilder.append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(beginDate));
            periodAltBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate));
            monthStrPeriodAltBuilder.append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(beginDate));

            if (null != endDate)
            {
                periodBuilder.append(" по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
                monthStrPeriodBuilder.append(" г. по ").append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(endDate));

                if (null == competitionAssignmentType)
                {
                    periodAltBuilder.append(" по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
                    monthStrPeriodAltBuilder.append(" г. по ").append(DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(endDate));
                }
            }

            if (null != competitionAssignmentType &&
                    (UniDefines.COMPETITION_ASSIGNMENT_TYPE_BEFORE_COMPETITION.equals(competitionAssignmentType.getCode())
                            || UniDefines.COMPETITION_ASSIGNMENT_TYPE_BEFORE_ELECTION.equals(competitionAssignmentType.getCode())))
            {
                periodAltBuilder.append(" ").append(StringUtils.uncapitalize(competitionAssignmentType.getTitle()));
                monthStrPeriodAltBuilder.append(" ").append(StringUtils.uncapitalize(competitionAssignmentType.getTitle()));
            }

            modifier.put("employmentPeriod", periodBuilder.toString());
            modifier.put("employmentPeriodAlt", periodAltBuilder.toString());
            modifier.put("employmentPeriodMonthStr", monthStrPeriodBuilder.append(" г.").toString());
            modifier.put("employmentPeriodMonthStrAlt", monthStrPeriodAltBuilder.toString());
        }

        // staffRates
        if (meta.getProperty("budgetStaffRate") != null)
        {
            modifier.put("budgetStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getProperty("budgetStaffRate")));
        }
        if (meta.getProperty("offBudgetStaffRate") != null)
        {
            modifier.put("offBudgetStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getProperty("offBudgetStaffRate")));
        }
        if (meta.getProperty("budgetStaffRate") != null && meta.getProperty("offBudgetStaffRate") != null)
        {
            Double sumStaffRate = (Double) extract.getProperty("budgetStaffRate") + (Double) extract.getProperty("offBudgetStaffRate");
            modifier.put("sumStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(sumStaffRate));
        }

        // etksLevel
        if (meta.getProperty("etksLevel") != null)
            modifier.put("etksLevel", (String) ((IEntity) extract.getProperty("etksLevels")).getProperty("title"));
        else
            modifier.put("etksLevel", "__");

        // salary
        if (meta.getProperty("salary") != null)
        {
            double salary = (Double) extract.getProperty("salary");
            long rub = (long) salary;
            long kop = Math.round((salary - rub) * 100);
            modifier.put("salaryRub", String.valueOf(rub));
            modifier.put("salaryKop", (kop < 10 ? "0" : "") + String.valueOf(kop));
            modifier.put("salary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(salary));
        }

        // labour contract number
        if (meta.getProperty("labourContractNumber") != null)
            modifier.put("labourContractNumber", (String) extract.getProperty("labourContractNumber"));

        // labour contract date
        if (meta.getProperty("labourContractDate") != null)
            modifier.put("labourContractDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) extract.getProperty("labourContractDate")));

        // bonuses
        String bonusesRtfString = getBonusListRtfStr(extract);
        if (bonusesRtfString.length() > 0)
        {
            modifier.put("bonuses", bonusesRtfString);
            modifier.put("bonusesRequired", StringUtils.uncapitalize(bonusesRtfString));
        } else
        {
            modifier.put("bonuses", "");
            modifier.put("bonusesRequired", ".");
        }

        String formattedBonusListString = getFormattedBonusListStr(extract);
        if (formattedBonusListString.isEmpty())
        {
            modifier.put("bonusListFormatted", bonusesRtfString);
            modifier.put("bonusListFormatted_lower", changeFirstCharLower(bonusesRtfString));
            modifier.put("bonusListFormattedRequired", bonusesRtfString.isEmpty() ? "." : StringUtils.uncapitalize(bonusesRtfString));
        }
        else
        {
            modifier.put("bonusListFormatted", formattedBonusListString);
            modifier.put("bonusListFormatted_lower", changeFirstCharLower(formattedBonusListString));
            modifier.put("bonusListFormattedRequired", StringUtils.uncapitalize(formattedBonusListString));
        }

        String formattedBonusListShortString = getFormattedBonusListShortStr(extract);
        if (formattedBonusListShortString.isEmpty())
        {
            modifier.put("bonusListFormattedShort", bonusesRtfString);
        }
        else
        {
            modifier.put("bonusListFormattedShort", formattedBonusListShortString);
        }

        if (extract.getTextParagraph() != null)
            modifier.put("textParagraph", extract.getTextParagraph());
        else
            modifier.put("textParagraph", "");

        return modifier;
    }

    private static String changeFirstCharLower(String s)
    {
        if (s.length() == 1)
            return s.toLowerCase();

        if (s.length() > 1)
        {
            String firstChar = s.substring(0, 1).toLowerCase();
            String remain = s.substring(1, s.length());
            return firstChar + remain;
        }

        return "";
    }

    private static String getFormattedBonusListShortStr(ModularEmployeeExtract extract)
    {
        //int size = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract).size();
        String sRetVal = CommonExtractUtil.getFormattedBonusListStrRtf(extract);
        return sRetVal.isEmpty() ? "" : StringUtils.uncapitalize(sRetVal);
    }

    private static String getFormattedBonusListStr(ModularEmployeeExtract extract)
    {
        int size = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract).size();
        String sRetVal = CommonExtractUtil.getFormattedBonusListStrRtf(extract);
        return sRetVal.isEmpty() ? "" : ((1 == size ? "Установить выплату " : 1 < size ? "Установить выплаты " : "")
            + sRetVal);
    }

    private static String getBonusListRtfStr(ModularEmployeeExtract extract)
    {
        List<EmployeeBonus> bonusesList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract);
        if (bonusesList.size() == 1)
        {
            EmployeeBonus bonus = bonusesList.get(0);
            StringBuilder result = new StringBuilder("Установить выплату \"");
            result.append(bonus.getPayment().getNominativeCaseTitle() != null ? bonus.getPayment().getNominativeCaseTitle() : bonus.getPayment().getTitle()).append("\" в размере ");
            result.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue())).append(" ");
            if ((bonus.getValue() == 1d || bonus.getValue() % 10 == 1) && (bonus.getValue() != 11 && bonus.getValue() % 100 != 11))
                result.append(bonus.getPayment().getPaymentUnit().getGenitiveCaseTitle() != null ? bonus.getPayment().getPaymentUnit().getGenitiveCaseTitle() : bonus.getPayment().getPaymentUnit().getTitle());
            else
                result.append(bonus.getPayment().getPaymentUnit().getPluralGenitiveCaseTitle() != null ? bonus.getPayment().getPaymentUnit().getPluralGenitiveCaseTitle() : bonus.getPayment().getPaymentUnit().getTitle());
            String financingSource = bonus.getFinancingSourceTitle();
            if (!financingSource.isEmpty())
                result.append(" из ").append(bonus.getFinancingSource().getGenitiveCaseTitle() != null ? bonus.getFinancingSource().getGenitiveCaseTitle() : bonus.getFinancingSourceTitle());
            if (null != bonus.getFinancingSourceItem())
            {
                result.append(" ");
                if (null != bonus.getFinancingSourceItem().getOrgUnit())
                    result.append(CommonExtractPrintUtil.getOrgUnitPrintingTitle(bonus.getFinancingSourceItem().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, false, true));
                else
                    result.append(bonus.getFinancingSourceItem().getGenitiveCaseTitle() != null ? bonus.getFinancingSourceItem().getGenitiveCaseTitle() : bonus.getFinancingSourceItem().getTitle());
            }
            return result.append(".").toString();
        } else if (bonusesList.size() > 1)
        {
            StringBuilder result = new StringBuilder("Установить выплаты: \n");
            for (EmployeeBonus bonus : bonusesList)
            {
                if (bonusesList.indexOf(bonus) > 0)
                    result.append("; ");
                result.append(bonus.getPayment().getNominativeCaseTitle() != null ? bonus.getPayment().getNominativeCaseTitle() : bonus.getPayment().getTitle());
                result.append(" в размере ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()));
                if ((bonus.getValue() == 1d || bonus.getValue() % 10 == 1) && (bonus.getValue() != 11 && bonus.getValue() % 100 != 11))
                    result.append(" ").append(bonus.getPayment().getPaymentUnit().getGenitiveCaseTitle() != null ? bonus.getPayment().getPaymentUnit().getGenitiveCaseTitle() : bonus.getPayment().getPaymentUnit().getTitle());
                else
                    result.append(" ").append(bonus.getPayment().getPaymentUnit().getPluralGenitiveCaseTitle() != null ? bonus.getPayment().getPaymentUnit().getPluralGenitiveCaseTitle() : bonus.getPayment().getPaymentUnit().getTitle());

                if (!StringUtils.isEmpty(bonus.getFinancingSourceCode()))
                {
                    result.append(" из ").append(bonus.getFinancingSource().getGenitiveCaseTitle() != null ? bonus.getFinancingSource().getGenitiveCaseTitle() : bonus.getFinancingSourceTitle());
                }
                if (null != bonus.getFinancingSourceItem())
                {
                    result.append(" ");
                    if (null != bonus.getFinancingSourceItem().getOrgUnit())
                        result.append(CommonExtractPrintUtil.getOrgUnitPrintingTitle(bonus.getFinancingSourceItem().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, false, true));
                    else
                        result.append(bonus.getFinancingSourceItem().getGenitiveCaseTitle() != null ? bonus.getFinancingSourceItem().getGenitiveCaseTitle() : bonus.getFinancingSourceItem().getTitle());
                }
            }
            return result.append(".").toString();
        }
        return "";
    }

    public static RtfTableModifier createModularExtractTableModifier(ModularEmployeeExtract extract)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(extract);
        List<String[]> visaData = new ArrayList<>();
        List<String[]> primaryVisaData = new ArrayList<>();
        List<String[]> secondaryVisaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());

        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            if (visa.getGroupMemberVising().getCode().equals("2"))
                primaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
            else
                secondaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        tableModifier.put("PRIMARY_VISAS", primaryVisaData.toArray(new String[primaryVisaData.size()][]));
        tableModifier.put("SECONDARY_VISAS", secondaryVisaData.toArray(new String[secondaryVisaData.size()][]));
        return tableModifier;
    }

    public static RtfInjectModifier createModularOrderInjectModifier(EmployeeModularOrder order)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();

        // Общие данные о печатном документе
        Date currentTime = new Date();
        injectModifier.put("docPrintingTime", new SimpleDateFormat("HH:mm").format(currentTime));
        injectModifier.put("docPrintingDate", new SimpleDateFormat("dd.MM.yyyy").format(currentTime));
        injectModifier.put("docPrintingTimeSec", new SimpleDateFormat("HH:mm:ss").format(currentTime));
        injectModifier.put("docPrintingDateTime", new SimpleDateFormat("dd.MM.yyyy HH:mm").format(currentTime));
        injectModifier.put("docPrintingDateTimeSec", new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(currentTime));
        injectModifier.put("docPrintingDateMonthStr", new SimpleDateFormat("dd MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(currentTime));
        injectModifier.put("docPrintingDateTimeMonthStr", new SimpleDateFormat("dd MMMMM yyyy HH:mm", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(currentTime));
        injectModifier.put("docPrintingDateTimeSecMonthStr", new SimpleDateFormat("dd MMMMM yyyy HH:mm:ss", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(currentTime));

        injectAcademy(injectModifier);

        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        if (principalContext instanceof EmployeePost)
        {
            EmployeePost docPrintingPost = (EmployeePost) principalContext;
            injectModifier.put("docPrintingEmployeeFio", docPrintingPost.getPerson().getFio());
            injectModifier.put("docPrintingEmployeeFullFio", docPrintingPost.getPerson().getFullFio());
            CommonExtractPrintUtil.injectDifferentVariations(injectModifier, "docPrintingEmployeePhone", docPrintingPost.getPhone());
        }


        injectModifier.put("commitDate", order.getCommitDate() == null ? "          " : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        injectModifier.put("orderNumber", StringUtils.isEmpty(order.getNumber()) ? "          " : order.getNumber());

        // Данные об исполнителе
        injectModifier.put("executor", "");
        injectModifier.put("executorPhone", "");
        injectModifier.put("executorStr", "");

        String executor = order.getExecutor();
        if (StringUtils.isNotEmpty(executor))
        {
            int phoneStart = executor.indexOf(" т. ");
            if (phoneStart == -1)
            {
                injectModifier.put("executor", executor);
                injectModifier.put("executorPhone", "");
                injectModifier.put("executorStr", "Исполнитель: " + executor);
            } else
            {
                injectModifier.put("executor", executor.substring(0, phoneStart));
                injectModifier.put("executorPhone", executor.substring(phoneStart + 1));
                injectModifier.put("executorStr", "Исполнитель: " + executor.substring(0, phoneStart) + " (" + executor.substring(phoneStart + 1) + ")");
            }
        }

        return injectModifier;
    }

    public static RtfTableModifier createModularOrderTableModifier(EmployeeModularOrder order)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        List<String[]> visaData = new ArrayList<>();
        List<String[]> primaryVisaData = new ArrayList<>();
        List<String[]> secondaryVisaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());

        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            if (visa.getGroupMemberVising().getCode().equals("2"))
                primaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
            else
                secondaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        tableModifier.put("PRIMARY_VISAS", primaryVisaData.toArray(new String[primaryVisaData.size()][]));
        tableModifier.put("SECONDARY_VISAS", secondaryVisaData.toArray(new String[secondaryVisaData.size()][]));
        return tableModifier;
    }

    private static void injectAcademy(RtfInjectModifier injectModifier)
    {
        TopOrgUnit academy = TopOrgUnit.getInstance();
        injectModifier.put("topOrgUnitFullTitle", academy.getTitle());
	    injectModifier.put("topOrgUnitCityTitle",
	                       (null != academy.getPostalAddress() && null != academy.getPostalAddress().getSettlement()) ? academy.getPostalAddress().getSettlement().getTitle() :
			                       (null != academy.getLegalAddress() && null != academy.getLegalAddress().getSettlement()) ? academy.getLegalAddress().getSettlement().getTitle() :
					                       (null != academy.getAddress() && null != academy.getAddress().getSettlement()) ? academy.getAddress().getSettlement().getTitle() : "");
    }
}