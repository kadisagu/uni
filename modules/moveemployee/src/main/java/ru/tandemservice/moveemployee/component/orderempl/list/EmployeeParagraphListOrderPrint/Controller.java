/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.list.EmployeeParagraphListOrderPrint;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.listemplextract.CommonListOrderPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author ashaburov
 * Created on: 11.10.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    @SuppressWarnings("unchecked")
    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        Model model = getModel(component);

        deactivate(component);

        String stateCode = model.getOrder().getState().getCode();
        if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode))
        {
            // выписка уже проведена и печатная форма сохранена
            return new CommonBaseRenderer().rtf().document(model.getData()).fileName("EmployeeListOrder.rtf");
        } else
        {
            EmployeeListParagraph paragraph = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFirstParagraph(model.getOrder());
            if(null == paragraph) throw new ApplicationException("Невозможно отправить на печать пустой приказ.");
            ListEmployeeExtract extract = paragraph.getFirstExtract();
            if(null == extract) throw new ApplicationException("Невозможно отправить на печать пустой приказ.");

            IPrintFormCreator formCreator;
            if (ApplicationRuntime.containsBean("employeeListOrder_" + extract.getType().getParent().getCode() + "_print"))
            {
                formCreator = (IPrintFormCreator) ApplicationRuntime.getBean("employeeListOrder_" + extract.getType().getParent().getCode() + "_print");
                RtfDocument document = formCreator.createPrintForm(model.getData(), model.getOrder());
                return new CommonBaseRenderer().rtf().document(document).fileName("EmployeeListOrder.rtf");
            }
            else
                formCreator = (IPrintFormCreator) ApplicationRuntime.getBean("employeeListOrder" + extract.getType().getIndex() + "_orderPrint");

            RtfDocument document = formCreator.createPrintForm(model.getData(), model.getOrder());

            CommonListOrderPrint.injectParagraphs(document, model.getOrder(), null);

            return new CommonBaseRenderer().rtf().document(document).fileName("EmployeeListOrder.rtf");
        }
    }
}