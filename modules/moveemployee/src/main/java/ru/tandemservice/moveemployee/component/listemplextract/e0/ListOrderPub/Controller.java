/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e0.ListOrderPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderPub.AbstractListOrderPubController;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;

/**
 * @author ListExtractComponentGenerator
 * @since 07.05.2009
 */
public class Controller extends AbstractListOrderPubController<EmployeeListOrder, IDAO, Model>
{
    @Override
    @SuppressWarnings("unchecked")
    protected void prepareListDataSource(DynamicListDataSource dataSource, IBusinessComponent component)
    {
        EmployeeExtractType extractType = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractTypeByOrderType(getModel(component).getOrder().getType());

        MoveEmployeeUtils.getListOrderParagraphListCustomizer(extractType.getIndex()).customizeParagraphList(dataSource, component);
    }
}