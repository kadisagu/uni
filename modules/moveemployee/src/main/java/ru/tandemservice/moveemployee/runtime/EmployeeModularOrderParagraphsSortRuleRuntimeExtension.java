/**
 *$Id$
 */
package ru.tandemservice.moveemployee.runtime;

import org.tandemframework.core.runtime.IRuntimeExtension;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public class EmployeeModularOrderParagraphsSortRuleRuntimeExtension implements IRuntimeExtension
{
    private IEmployeeModularOrderParagraphsSortRuleRuntimeExtensionDAO _dao;

    public IEmployeeModularOrderParagraphsSortRuleRuntimeExtensionDAO getDao()
    {
        return _dao;
    }

    public void setDao(IEmployeeModularOrderParagraphsSortRuleRuntimeExtensionDAO dao)
    {
        _dao = dao;
    }

    @Override
    public void init(Object object)
    {
        _dao.createRuleList();
    }

    @Override
    public void destroy()
    {
    }
}