/**
 *$Id:$
 */
package ru.tandemservice.moveemployee.base.bo.MoveEmployeeSettingsPaymentStringFormatDocument.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings;

import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 19.10.12
 */
@Configuration
public class MoveEmployeeSettingsPaymentStringFormatDocumentList extends BusinessComponentManager
{
    // dataSource
    public static final String SEARCH_LIST_DS = "searchListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SEARCH_LIST_DS, columnListExtPoint(), searchListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnListExtPoint()
    {
        return columnListExtPointBuilder(SEARCH_LIST_DS)
                .addColumn(textColumn("title", PaymentStringFormatDocumentSettings.title()).treeable(true))
                .addColumn(toggleColumn("active", PaymentStringFormatDocumentSettings.active())
                        .toggleOffListener("onChangeActive")
                        .toggleOnListener("onChangeActive"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> searchListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<PaymentStringFormatDocumentSettings> list = new DQLSelectBuilder().fromEntity(PaymentStringFormatDocumentSettings.class, "b")
                        .createStatement(context.getSession()).list();

                Collections.sort(list, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);

                return ListOutputBuilder.get(input, list).pageable(true).build();
            }
        };
    }
}
