package ru.tandemservice.moveemployee.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.moveemployee.entity.gen.EmployeeHolidaySExtractGen;

import java.util.Date;

/**
 * Выписка из индивидуального приказа по кадровому составу. Об отпуске
 */
public class EmployeeHolidaySExtract extends EmployeeHolidaySExtractGen
{
    public static final String P_FIRST_PERIOD = "firstPeriod";
    public static final String P_SECOND_PERIOD = "secondPeriod";
    public static final String P_MAIN_HOLIDAY_PERIOD = "mainHolidayPeriod";
    public static final String P_SECOND_HOLIDAY_PERIOD = "secondHolidayPeriod";
    public static final String P_SUM_HOLIDAY_PERIOD = "sumHolidayPeriod";
    
    public String getFirstPeriod()
    {
        return getFormattedPeriod(getFirstPeriodBeginDate(), getFirstPeriodEndDate());
    }
    
    public String getSecondPeriod()
    {
        return getFormattedPeriod(getSecondPeriodBeginDate(), getSecondPeriodEndDate());
    }
    
    public String getMainHolidayPeriod()
    {
        return getFormattedPeriodWithDaysAmount(getMainHolidayBeginDate(), getMainHolidayEndDate(), getMainHolidayDuration());
    }
    
    public String getSecondHolidayPeriod()
    {
        return getFormattedPeriodWithDaysAmount(getSecondHolidayBeginDate(), getSecondHolidayEndDate(), getSecondHolidayDuration());
    }
    
    public String getSumHolidayPeriod()
    {
        return getFormattedPeriodWithDaysAmount(getBeginDate(), getEndDate(), getDuration());
    }
    
    private String getFormattedPeriod(Date date1, Date date2)
    {
        if(null == date1 && null == date2) return null;

        StringBuilder builder = new StringBuilder("c ");
        builder.append(null != date1 ? DateFormatter.DEFAULT_DATE_FORMATTER.format(date1) : "__.__.20__").append(" по ");
        builder.append(null != date2 ? DateFormatter.DEFAULT_DATE_FORMATTER.format(date2) : "__.__.20__");
        return builder.toString();
    }
    
    private String getFormattedPeriodWithDaysAmount(Date date1, Date date2, Integer daysAmount)
    {
        String formattedPeriod = getFormattedPeriod(date1, date2);
        if(null == formattedPeriod) return null;
        return formattedPeriod + " (" + CommonBaseDateUtil.getDaysCountWithName(daysAmount) + ")";
    }
}