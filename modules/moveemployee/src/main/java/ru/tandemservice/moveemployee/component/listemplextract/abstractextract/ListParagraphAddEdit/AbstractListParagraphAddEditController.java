/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;

import java.util.List;

/**
 * @author ashaburov
 * Created on: 11.10.2011
 */
public abstract class AbstractListParagraphAddEditController<T extends ListEmployeeExtract, IDAO extends IAbstractListParagraphAddEditDAO<T, Model>, Model extends AbstractListParagraphAddEditModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        getDao().validate(model, errors);
        if (errors.hasErrors()) return;

        getDao().update(model);

        List<EmployeeExtractType> extractTypeList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractTypeListByOrderType(model.getOrder().getType());

        if (model.isAddForm() && extractTypeList.size() > 1)
        {
            // форма добавления
            deactivate(component, 2);
            EmployeeExtractType extractType = model.getParagraph().getFirstExtract().getType();
            activateInRoot(component, new ComponentActivator(MoveEmployeeUtils.getListParagraphPubComponent(extractType), new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, model.getParagraph().getId())));
        }
        else
            deactivate(component);
    }

    public void onClickDeactivate(IBusinessComponent component)
    {
        Model model = getModel(component);
        List<EmployeeExtractType> extractTypeList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractTypeListByOrderType(model.getOrder().getType());

        if (model.isAddForm() && extractTypeList.size() > 1)
            deactivate(component, 2);
        else
            deactivate(component);
    }

    public void onChangeReason(IBusinessComponent component)
    {

    }
}