/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.settings.UsedEmployeeExtractTypes;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 12.12.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        List<EmployeeExtractType> result = getList(EmployeeExtractType.class);
        Collections.sort(result, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        model.getDataSource().setCountRow(result.size());
        UniBaseUtils.createPage(model.getDataSource(), result);
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            wrapper.setViewProperty(Model.ACTIVATION_DISABLED, isActiveParent((EmployeeExtractType)wrapper.getEntity()));
        }
    }

    @Override
    public void updateActive(long extractTypeId)
    {
        EmployeeExtractType parentExtractType = get(EmployeeExtractType.class, extractTypeId);
        if (parentExtractType != null)
        {
            EmployeeExtractType elderParentExtractType = parentExtractType.getParent();
            parentExtractType.setActive(!parentExtractType.isActive());
            update(parentExtractType);
            for (EmployeeExtractType child : getChildren(parentExtractType, new ArrayList<EmployeeExtractType>()))
            {
                if (child != null && !parentExtractType.isActive() && child.isActive())
                {
                    child.setActive(false);
                    update(child);
                }
            }
            if (getChildren(parentExtractType, new ArrayList<EmployeeExtractType>()).size() == 1 && !getChildren(parentExtractType, new ArrayList<EmployeeExtractType>()).get(0).isActive() && parentExtractType.isActive())
            {
                getChildren(parentExtractType, new ArrayList<EmployeeExtractType>()).get(0).setActive(true);
                update(getChildren(parentExtractType, new ArrayList<EmployeeExtractType>()).get(0));
            }
            int activeAmount = 0;
            if (elderParentExtractType != null)
            {
                if (!elderParentExtractType.isActive() && parentExtractType.isActive())
                    elderParentExtractType.setActive(true);
                for (EmployeeExtractType child : getChildren(elderParentExtractType, new ArrayList<EmployeeExtractType>()))
                {
                    if (child.isActive()) activeAmount++;
                }
                if (activeAmount == 0) elderParentExtractType.setActive(false);
                update(elderParentExtractType);
            }
        }
    }

    private List<EmployeeExtractType> getChildren(EmployeeExtractType extractType, List<EmployeeExtractType> children)
    {
        DQLSelectBuilder childBuild = new DQLSelectBuilder().fromEntity(EmployeeExtractType.ENTITY_CLASS, "stext")
                .column(DQLExpressions.property("stext"))
                .where(DQLExpressions.eq(DQLExpressions.property(EmployeeExtractType.parent().id().fromAlias("stext")), DQLExpressions.value(extractType.getId())));
        List<EmployeeExtractType> result = childBuild.createStatement(getSession()).list();
        children.addAll(result);
        for (EmployeeExtractType child : result)
            getChildren(child, children);
        return children;
    }

    private boolean isActiveParent(EmployeeExtractType extractType)
    {
        boolean flag = false;
        if (null == extractType.getParent())
            flag = true;
        return flag;
    }
}