/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import java.util.Date;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * @author dseleznev
 * Created on: 16.09.2009
 */
public interface ILabourContractExtract extends IAbstractExtract<EmployeePost, AbstractEmployeeParagraph>
{
    Date getBeginDate();

    Date getEndDate();

    LabourContractType getLabourContractType();

    String getLabourContractNumber();
    
    Date getLabourContractDate();
}