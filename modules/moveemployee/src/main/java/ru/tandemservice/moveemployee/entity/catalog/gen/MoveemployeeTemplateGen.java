package ru.tandemservice.moveemployee.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шаблон выписки из сборного приказа (приказы по кадрам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MoveemployeeTemplateGen extends EntityBase
 implements INaturalIdentifiable<MoveemployeeTemplateGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate";
    public static final String ENTITY_NAME = "moveemployeeTemplate";
    public static final int VERSION_HASH = -1868346804;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_TYPE = "type";
    public static final String P_INDEX = "index";
    public static final String P_PATH = "path";
    public static final String P_DOCUMENT = "document";
    public static final String P_EDIT_DATE = "editDate";
    public static final String P_COMMENT = "comment";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private EmployeeExtractType _type;     // Тип для которого создан этот шаблон. Может быть null, если это вспомогательный шаблон
    private int _index;     // Индекс шаблона внутри типа
    private String _path;     // Путь в classpath до шаблона по умолчанию
    private byte[] _document;     // Шаблон в zip текущий. Может быть null, если используется шаблон по умолчанию
    private Date _editDate;     // Дата редактирования
    private String _comment;     // комментарий
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Тип для которого создан этот шаблон. Может быть null, если это вспомогательный шаблон.
     */
    public EmployeeExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип для которого создан этот шаблон. Может быть null, если это вспомогательный шаблон.
     */
    public void setType(EmployeeExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Индекс шаблона внутри типа. Свойство не может быть null.
     */
    @NotNull
    public int getIndex()
    {
        return _index;
    }

    /**
     * @param index Индекс шаблона внутри типа. Свойство не может быть null.
     */
    public void setIndex(int index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Путь в classpath до шаблона по умолчанию. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getPath()
    {
        return _path;
    }

    /**
     * @param path Путь в classpath до шаблона по умолчанию. Свойство не может быть null и должно быть уникальным.
     */
    public void setPath(String path)
    {
        dirty(_path, path);
        _path = path;
    }

    /**
     * @return Шаблон в zip текущий. Может быть null, если используется шаблон по умолчанию.
     */
    public byte[] getDocument()
    {
        return _document;
    }

    /**
     * @param document Шаблон в zip текущий. Может быть null, если используется шаблон по умолчанию.
     */
    public void setDocument(byte[] document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Дата редактирования.
     */
    public Date getEditDate()
    {
        return _editDate;
    }

    /**
     * @param editDate Дата редактирования.
     */
    public void setEditDate(Date editDate)
    {
        dirty(_editDate, editDate);
        _editDate = editDate;
    }

    /**
     * @return комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MoveemployeeTemplateGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((MoveemployeeTemplate)another).getCode());
            }
            setType(((MoveemployeeTemplate)another).getType());
            setIndex(((MoveemployeeTemplate)another).getIndex());
            setPath(((MoveemployeeTemplate)another).getPath());
            setDocument(((MoveemployeeTemplate)another).getDocument());
            setEditDate(((MoveemployeeTemplate)another).getEditDate());
            setComment(((MoveemployeeTemplate)another).getComment());
            setTitle(((MoveemployeeTemplate)another).getTitle());
        }
    }

    public INaturalId<MoveemployeeTemplateGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<MoveemployeeTemplateGen>
    {
        private static final String PROXY_NAME = "MoveemployeeTemplateNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof MoveemployeeTemplateGen.NaturalId) ) return false;

            MoveemployeeTemplateGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MoveemployeeTemplateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MoveemployeeTemplate.class;
        }

        public T newInstance()
        {
            return (T) new MoveemployeeTemplate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "type":
                    return obj.getType();
                case "index":
                    return obj.getIndex();
                case "path":
                    return obj.getPath();
                case "document":
                    return obj.getDocument();
                case "editDate":
                    return obj.getEditDate();
                case "comment":
                    return obj.getComment();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "type":
                    obj.setType((EmployeeExtractType) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "path":
                    obj.setPath((String) value);
                    return;
                case "document":
                    obj.setDocument((byte[]) value);
                    return;
                case "editDate":
                    obj.setEditDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "type":
                        return true;
                case "index":
                        return true;
                case "path":
                        return true;
                case "document":
                        return true;
                case "editDate":
                        return true;
                case "comment":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "type":
                    return true;
                case "index":
                    return true;
                case "path":
                    return true;
                case "document":
                    return true;
                case "editDate":
                    return true;
                case "comment":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "type":
                    return EmployeeExtractType.class;
                case "index":
                    return Integer.class;
                case "path":
                    return String.class;
                case "document":
                    return byte[].class;
                case "editDate":
                    return Date.class;
                case "comment":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MoveemployeeTemplate> _dslPath = new Path<MoveemployeeTemplate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MoveemployeeTemplate");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Тип для которого создан этот шаблон. Может быть null, если это вспомогательный шаблон.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getType()
     */
    public static EmployeeExtractType.Path<EmployeeExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Индекс шаблона внутри типа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Путь в classpath до шаблона по умолчанию. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getPath()
     */
    public static PropertyPath<String> path()
    {
        return _dslPath.path();
    }

    /**
     * @return Шаблон в zip текущий. Может быть null, если используется шаблон по умолчанию.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getDocument()
     */
    public static PropertyPath<byte[]> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Дата редактирования.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getEditDate()
     */
    public static PropertyPath<Date> editDate()
    {
        return _dslPath.editDate();
    }

    /**
     * @return комментарий.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends MoveemployeeTemplate> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EmployeeExtractType.Path<EmployeeExtractType> _type;
        private PropertyPath<Integer> _index;
        private PropertyPath<String> _path;
        private PropertyPath<byte[]> _document;
        private PropertyPath<Date> _editDate;
        private PropertyPath<String> _comment;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(MoveemployeeTemplateGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Тип для которого создан этот шаблон. Может быть null, если это вспомогательный шаблон.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getType()
     */
        public EmployeeExtractType.Path<EmployeeExtractType> type()
        {
            if(_type == null )
                _type = new EmployeeExtractType.Path<EmployeeExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Индекс шаблона внутри типа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(MoveemployeeTemplateGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Путь в classpath до шаблона по умолчанию. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getPath()
     */
        public PropertyPath<String> path()
        {
            if(_path == null )
                _path = new PropertyPath<String>(MoveemployeeTemplateGen.P_PATH, this);
            return _path;
        }

    /**
     * @return Шаблон в zip текущий. Может быть null, если используется шаблон по умолчанию.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getDocument()
     */
        public PropertyPath<byte[]> document()
        {
            if(_document == null )
                _document = new PropertyPath<byte[]>(MoveemployeeTemplateGen.P_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Дата редактирования.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getEditDate()
     */
        public PropertyPath<Date> editDate()
        {
            if(_editDate == null )
                _editDate = new PropertyPath<Date>(MoveemployeeTemplateGen.P_EDIT_DATE, this);
            return _editDate;
        }

    /**
     * @return комментарий.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(MoveemployeeTemplateGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MoveemployeeTemplateGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return MoveemployeeTemplate.class;
        }

        public String getEntityName()
        {
            return "moveemployeeTemplate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
