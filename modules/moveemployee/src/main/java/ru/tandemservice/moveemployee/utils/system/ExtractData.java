/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.utils.system;

/**
 * @author dseleznev
 * Created on: 26.11.2008
 */
public class ExtractData
{
    //список всех entity выписок из сборного приказа
    public static final String[][] EXTRACT_LIST = new String[][]{
        /* 1*/{"employeeAddExtract", "О приеме сотрудника"},
        /* 2*/{"employeeTempAddExtract", "О приеме сотрудника на время отсутствия другого"},
        /* 3*/{"employeePostAddExtract", "О назначении на должность"},
        /* 4*/{"employeeTransferExtract", "О переводе"},
        /* 5*/{"employeeHolidayExtract", "Об отпуске"},
        /* 6*/{"voluntaryTerminationOwnWishExtract", "Об увольнении сотрудника по собственному желанию"},
        /* 7*/{"agreedDismissalExtract", "Об увольнении сотрудника по соглашению сторон"},
        /* 8*/{"employeeRetiringExtract", "Об увольнении сотрудника по собственному желанию при выходе на пенсию"},
        /* 9*/{"dismissalWEndOfLabourContrExtract", "Об увольнении сотрудника по истечению срока действия ТД"},
        /*10*/{"employeeSurnameChangeExtract", "О смене фамилии"},
        /*11*/{"paymentAssigningExtract", "Об установлении выплат"},
        /*12*/{"employeePostPPSAddExtract", "О назначении на должность в связи с прохождением конкурсного отбора"},
        /*13*/{"dismissalExtract", "Увольнение"},
        /*14*/{"contractTerminationExtract", "О прекращении трудового договора"},
        /*15*/{"prolongationAnnualHolidayExtract", "О продлении ежегодного отпуска"},
        /*16*/{"returnFromChildCareHolidayExtract", "О выходе из отпуска по уходу за ребенком"},
        /*17*/{"revocationFromHolidayExtract", "Об отзыве из отпуска"},
        /*18*/{"transferAnnualHolidayExtract", "О переносе ежегодного отпуска"},
        /*19*/{"employeeActingExtract", "Исполнение обязанностей временно отсутствующего работника"},
        /*20*/{"transferHolidayDueDiseaseExtract", "Перенос части отпуска в связи с болезнью"},
        /*21*/{"incDecWorkExtract", "Увеличение (уменьшение) объема выполняемой работы"},
        /*22*/{"combinationPostPaymentExtract", "Установление доплат за работу по совмещению"},
        /*23*/{"incDecCombinationExtract", "Увеличение (уменьшение) работы по совмещению"},
        /*24*/{"removalCombinationExtract", "Снятие доплат за работу по совмещению (за увеличение объема работ)"},
        /*25*/{"repealExtract", "Отмена приказа"},
        /*26*/{"changeHolidayExtract", "Изменение приказа об отпуске"},
        /*27*/{"scienceDegreePaymentExtract", "Об установлении доплат в связи с присуждением ученой степени"},
        /*28*/{"scienceStatusPaymentExtract", "Выписка из сборного приказа по кадровому составу. Об установлении доплат в связи с присвоением ученого звания"}
    };
}