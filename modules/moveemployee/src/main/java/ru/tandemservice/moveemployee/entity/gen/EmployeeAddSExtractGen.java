package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWeekWorkLoad;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.EmployeeAddSExtract;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. О приеме сотрудника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeAddSExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeAddSExtract";
    public static final String ENTITY_NAME = "employeeAddSExtract";
    public static final int VERSION_HASH = 934934082;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_COMPETITION_TYPE = "competitionType";
    public static final String L_POST_TYPE = "postType";
    public static final String L_WEEK_WORK_LOAD = "weekWorkLoad";
    public static final String L_WORK_WEEK_DURATION = "workWeekDuration";
    public static final String L_MAIN_WORK = "mainWork";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String P_SALARY = "salary";
    public static final String P_HOURLY_PAID = "hourlyPaid";
    public static final String P_OPTIONAL_CONDITION = "optionalCondition";
    public static final String L_LABOUR_CONTRACT_TYPE = "labourContractType";
    public static final String P_LABOUR_CONTRACT_NUMBER = "labourContractNumber";
    public static final String P_LABOUR_CONTRACT_DATE = "labourContractDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_TRIAL_PERIOD = "trialPeriod";
    public static final String P_HOURS_AMOUNT = "hoursAmount";
    public static final String P_FREELANCE = "freelance";

    private OrgUnit _orgUnit;     // Принимающее подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private CompetitionAssignmentType _competitionType;     // Типы конкурсного назначения на должность
    private PostType _postType;     // Тип назначения на должность
    private EmployeeWeekWorkLoad _weekWorkLoad;     // Продолжительность рабочего времени
    private EmployeeWorkWeekDuration _workWeekDuration;     // Продолжительность трудовой недели
    private EmployeePost _mainWork;     // Основное место работы
    private EtksLevels _etksLevels;     // Разряд ЕТКС
    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private double _salary;     // Сумма оплаты
    private boolean _hourlyPaid;     // Почасовая оплата
    private String _optionalCondition;     // Произвольная строка условий приема
    private LabourContractType _labourContractType;     // Тип трудового договора
    private String _labourContractNumber;     // Номер трудового договора
    private Date _labourContractDate;     // Дата трудового договора
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private int _trialPeriod;     // Продолжительность испытательного срока
    private Integer _hoursAmount;     // Учебная нагрузка в течении учебного года
    private boolean _freelance;     // Вне штата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Принимающее подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Типы конкурсного назначения на должность.
     */
    public CompetitionAssignmentType getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Типы конкурсного назначения на должность.
     */
    public void setCompetitionType(CompetitionAssignmentType competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     */
    @NotNull
    public PostType getPostType()
    {
        return _postType;
    }

    /**
     * @param postType Тип назначения на должность. Свойство не может быть null.
     */
    public void setPostType(PostType postType)
    {
        dirty(_postType, postType);
        _postType = postType;
    }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     */
    @NotNull
    public EmployeeWeekWorkLoad getWeekWorkLoad()
    {
        return _weekWorkLoad;
    }

    /**
     * @param weekWorkLoad Продолжительность рабочего времени. Свойство не может быть null.
     */
    public void setWeekWorkLoad(EmployeeWeekWorkLoad weekWorkLoad)
    {
        dirty(_weekWorkLoad, weekWorkLoad);
        _weekWorkLoad = weekWorkLoad;
    }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     */
    @NotNull
    public EmployeeWorkWeekDuration getWorkWeekDuration()
    {
        return _workWeekDuration;
    }

    /**
     * @param workWeekDuration Продолжительность трудовой недели. Свойство не может быть null.
     */
    public void setWorkWeekDuration(EmployeeWorkWeekDuration workWeekDuration)
    {
        dirty(_workWeekDuration, workWeekDuration);
        _workWeekDuration = workWeekDuration;
    }

    /**
     * @return Основное место работы.
     */
    public EmployeePost getMainWork()
    {
        return _mainWork;
    }

    /**
     * @param mainWork Основное место работы.
     */
    public void setMainWork(EmployeePost mainWork)
    {
        dirty(_mainWork, mainWork);
        _mainWork = mainWork;
    }

    /**
     * @return Разряд ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Разряд ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Почасовая оплата. Свойство не может быть null.
     */
    @NotNull
    public boolean isHourlyPaid()
    {
        return _hourlyPaid;
    }

    /**
     * @param hourlyPaid Почасовая оплата. Свойство не может быть null.
     */
    public void setHourlyPaid(boolean hourlyPaid)
    {
        dirty(_hourlyPaid, hourlyPaid);
        _hourlyPaid = hourlyPaid;
    }

    /**
     * @return Произвольная строка условий приема.
     */
    @Length(max=255)
    public String getOptionalCondition()
    {
        return _optionalCondition;
    }

    /**
     * @param optionalCondition Произвольная строка условий приема.
     */
    public void setOptionalCondition(String optionalCondition)
    {
        dirty(_optionalCondition, optionalCondition);
        _optionalCondition = optionalCondition;
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     */
    @NotNull
    public LabourContractType getLabourContractType()
    {
        return _labourContractType;
    }

    /**
     * @param labourContractType Тип трудового договора. Свойство не может быть null.
     */
    public void setLabourContractType(LabourContractType labourContractType)
    {
        dirty(_labourContractType, labourContractType);
        _labourContractType = labourContractType;
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLabourContractNumber()
    {
        return _labourContractNumber;
    }

    /**
     * @param labourContractNumber Номер трудового договора. Свойство не может быть null.
     */
    public void setLabourContractNumber(String labourContractNumber)
    {
        dirty(_labourContractNumber, labourContractNumber);
        _labourContractNumber = labourContractNumber;
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     */
    @NotNull
    public Date getLabourContractDate()
    {
        return _labourContractDate;
    }

    /**
     * @param labourContractDate Дата трудового договора. Свойство не может быть null.
     */
    public void setLabourContractDate(Date labourContractDate)
    {
        dirty(_labourContractDate, labourContractDate);
        _labourContractDate = labourContractDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Продолжительность испытательного срока. Свойство не может быть null.
     */
    @NotNull
    public int getTrialPeriod()
    {
        return _trialPeriod;
    }

    /**
     * @param trialPeriod Продолжительность испытательного срока. Свойство не может быть null.
     */
    public void setTrialPeriod(int trialPeriod)
    {
        dirty(_trialPeriod, trialPeriod);
        _trialPeriod = trialPeriod;
    }

    /**
     * @return Учебная нагрузка в течении учебного года.
     */
    public Integer getHoursAmount()
    {
        return _hoursAmount;
    }

    /**
     * @param hoursAmount Учебная нагрузка в течении учебного года.
     */
    public void setHoursAmount(Integer hoursAmount)
    {
        dirty(_hoursAmount, hoursAmount);
        _hoursAmount = hoursAmount;
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreelance()
    {
        return _freelance;
    }

    /**
     * @param freelance Вне штата. Свойство не может быть null.
     */
    public void setFreelance(boolean freelance)
    {
        dirty(_freelance, freelance);
        _freelance = freelance;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeAddSExtractGen)
        {
            setOrgUnit(((EmployeeAddSExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((EmployeeAddSExtract)another).getPostBoundedWithQGandQL());
            setCompetitionType(((EmployeeAddSExtract)another).getCompetitionType());
            setPostType(((EmployeeAddSExtract)another).getPostType());
            setWeekWorkLoad(((EmployeeAddSExtract)another).getWeekWorkLoad());
            setWorkWeekDuration(((EmployeeAddSExtract)another).getWorkWeekDuration());
            setMainWork(((EmployeeAddSExtract)another).getMainWork());
            setEtksLevels(((EmployeeAddSExtract)another).getEtksLevels());
            setRaisingCoefficient(((EmployeeAddSExtract)another).getRaisingCoefficient());
            setSalary(((EmployeeAddSExtract)another).getSalary());
            setHourlyPaid(((EmployeeAddSExtract)another).isHourlyPaid());
            setOptionalCondition(((EmployeeAddSExtract)another).getOptionalCondition());
            setLabourContractType(((EmployeeAddSExtract)another).getLabourContractType());
            setLabourContractNumber(((EmployeeAddSExtract)another).getLabourContractNumber());
            setLabourContractDate(((EmployeeAddSExtract)another).getLabourContractDate());
            setBeginDate(((EmployeeAddSExtract)another).getBeginDate());
            setEndDate(((EmployeeAddSExtract)another).getEndDate());
            setTrialPeriod(((EmployeeAddSExtract)another).getTrialPeriod());
            setHoursAmount(((EmployeeAddSExtract)another).getHoursAmount());
            setFreelance(((EmployeeAddSExtract)another).isFreelance());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeAddSExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeAddSExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeAddSExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "competitionType":
                    return obj.getCompetitionType();
                case "postType":
                    return obj.getPostType();
                case "weekWorkLoad":
                    return obj.getWeekWorkLoad();
                case "workWeekDuration":
                    return obj.getWorkWeekDuration();
                case "mainWork":
                    return obj.getMainWork();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "salary":
                    return obj.getSalary();
                case "hourlyPaid":
                    return obj.isHourlyPaid();
                case "optionalCondition":
                    return obj.getOptionalCondition();
                case "labourContractType":
                    return obj.getLabourContractType();
                case "labourContractNumber":
                    return obj.getLabourContractNumber();
                case "labourContractDate":
                    return obj.getLabourContractDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "trialPeriod":
                    return obj.getTrialPeriod();
                case "hoursAmount":
                    return obj.getHoursAmount();
                case "freelance":
                    return obj.isFreelance();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((CompetitionAssignmentType) value);
                    return;
                case "postType":
                    obj.setPostType((PostType) value);
                    return;
                case "weekWorkLoad":
                    obj.setWeekWorkLoad((EmployeeWeekWorkLoad) value);
                    return;
                case "workWeekDuration":
                    obj.setWorkWeekDuration((EmployeeWorkWeekDuration) value);
                    return;
                case "mainWork":
                    obj.setMainWork((EmployeePost) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "hourlyPaid":
                    obj.setHourlyPaid((Boolean) value);
                    return;
                case "optionalCondition":
                    obj.setOptionalCondition((String) value);
                    return;
                case "labourContractType":
                    obj.setLabourContractType((LabourContractType) value);
                    return;
                case "labourContractNumber":
                    obj.setLabourContractNumber((String) value);
                    return;
                case "labourContractDate":
                    obj.setLabourContractDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "trialPeriod":
                    obj.setTrialPeriod((Integer) value);
                    return;
                case "hoursAmount":
                    obj.setHoursAmount((Integer) value);
                    return;
                case "freelance":
                    obj.setFreelance((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "competitionType":
                        return true;
                case "postType":
                        return true;
                case "weekWorkLoad":
                        return true;
                case "workWeekDuration":
                        return true;
                case "mainWork":
                        return true;
                case "etksLevels":
                        return true;
                case "raisingCoefficient":
                        return true;
                case "salary":
                        return true;
                case "hourlyPaid":
                        return true;
                case "optionalCondition":
                        return true;
                case "labourContractType":
                        return true;
                case "labourContractNumber":
                        return true;
                case "labourContractDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "trialPeriod":
                        return true;
                case "hoursAmount":
                        return true;
                case "freelance":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "competitionType":
                    return true;
                case "postType":
                    return true;
                case "weekWorkLoad":
                    return true;
                case "workWeekDuration":
                    return true;
                case "mainWork":
                    return true;
                case "etksLevels":
                    return true;
                case "raisingCoefficient":
                    return true;
                case "salary":
                    return true;
                case "hourlyPaid":
                    return true;
                case "optionalCondition":
                    return true;
                case "labourContractType":
                    return true;
                case "labourContractNumber":
                    return true;
                case "labourContractDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "trialPeriod":
                    return true;
                case "hoursAmount":
                    return true;
                case "freelance":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "competitionType":
                    return CompetitionAssignmentType.class;
                case "postType":
                    return PostType.class;
                case "weekWorkLoad":
                    return EmployeeWeekWorkLoad.class;
                case "workWeekDuration":
                    return EmployeeWorkWeekDuration.class;
                case "mainWork":
                    return EmployeePost.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "salary":
                    return Double.class;
                case "hourlyPaid":
                    return Boolean.class;
                case "optionalCondition":
                    return String.class;
                case "labourContractType":
                    return LabourContractType.class;
                case "labourContractNumber":
                    return String.class;
                case "labourContractDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "trialPeriod":
                    return Integer.class;
                case "hoursAmount":
                    return Integer.class;
                case "freelance":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeAddSExtract> _dslPath = new Path<EmployeeAddSExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeAddSExtract");
    }
            

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Типы конкурсного назначения на должность.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getCompetitionType()
     */
    public static CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getPostType()
     */
    public static PostType.Path<PostType> postType()
    {
        return _dslPath.postType();
    }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getWeekWorkLoad()
     */
    public static EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> weekWorkLoad()
    {
        return _dslPath.weekWorkLoad();
    }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getWorkWeekDuration()
     */
    public static EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> workWeekDuration()
    {
        return _dslPath.workWeekDuration();
    }

    /**
     * @return Основное место работы.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getMainWork()
     */
    public static EmployeePost.Path<EmployeePost> mainWork()
    {
        return _dslPath.mainWork();
    }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Почасовая оплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#isHourlyPaid()
     */
    public static PropertyPath<Boolean> hourlyPaid()
    {
        return _dslPath.hourlyPaid();
    }

    /**
     * @return Произвольная строка условий приема.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getOptionalCondition()
     */
    public static PropertyPath<String> optionalCondition()
    {
        return _dslPath.optionalCondition();
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getLabourContractType()
     */
    public static LabourContractType.Path<LabourContractType> labourContractType()
    {
        return _dslPath.labourContractType();
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getLabourContractNumber()
     */
    public static PropertyPath<String> labourContractNumber()
    {
        return _dslPath.labourContractNumber();
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getLabourContractDate()
     */
    public static PropertyPath<Date> labourContractDate()
    {
        return _dslPath.labourContractDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Продолжительность испытательного срока. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getTrialPeriod()
     */
    public static PropertyPath<Integer> trialPeriod()
    {
        return _dslPath.trialPeriod();
    }

    /**
     * @return Учебная нагрузка в течении учебного года.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getHoursAmount()
     */
    public static PropertyPath<Integer> hoursAmount()
    {
        return _dslPath.hoursAmount();
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#isFreelance()
     */
    public static PropertyPath<Boolean> freelance()
    {
        return _dslPath.freelance();
    }

    public static class Path<E extends EmployeeAddSExtract> extends SingleEmployeeExtract.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private CompetitionAssignmentType.Path<CompetitionAssignmentType> _competitionType;
        private PostType.Path<PostType> _postType;
        private EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> _weekWorkLoad;
        private EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> _workWeekDuration;
        private EmployeePost.Path<EmployeePost> _mainWork;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private PropertyPath<Double> _salary;
        private PropertyPath<Boolean> _hourlyPaid;
        private PropertyPath<String> _optionalCondition;
        private LabourContractType.Path<LabourContractType> _labourContractType;
        private PropertyPath<String> _labourContractNumber;
        private PropertyPath<Date> _labourContractDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Integer> _trialPeriod;
        private PropertyPath<Integer> _hoursAmount;
        private PropertyPath<Boolean> _freelance;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Типы конкурсного назначения на должность.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getCompetitionType()
     */
        public CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new CompetitionAssignmentType.Path<CompetitionAssignmentType>(L_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getPostType()
     */
        public PostType.Path<PostType> postType()
        {
            if(_postType == null )
                _postType = new PostType.Path<PostType>(L_POST_TYPE, this);
            return _postType;
        }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getWeekWorkLoad()
     */
        public EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> weekWorkLoad()
        {
            if(_weekWorkLoad == null )
                _weekWorkLoad = new EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad>(L_WEEK_WORK_LOAD, this);
            return _weekWorkLoad;
        }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getWorkWeekDuration()
     */
        public EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> workWeekDuration()
        {
            if(_workWeekDuration == null )
                _workWeekDuration = new EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration>(L_WORK_WEEK_DURATION, this);
            return _workWeekDuration;
        }

    /**
     * @return Основное место работы.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getMainWork()
     */
        public EmployeePost.Path<EmployeePost> mainWork()
        {
            if(_mainWork == null )
                _mainWork = new EmployeePost.Path<EmployeePost>(L_MAIN_WORK, this);
            return _mainWork;
        }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(EmployeeAddSExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Почасовая оплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#isHourlyPaid()
     */
        public PropertyPath<Boolean> hourlyPaid()
        {
            if(_hourlyPaid == null )
                _hourlyPaid = new PropertyPath<Boolean>(EmployeeAddSExtractGen.P_HOURLY_PAID, this);
            return _hourlyPaid;
        }

    /**
     * @return Произвольная строка условий приема.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getOptionalCondition()
     */
        public PropertyPath<String> optionalCondition()
        {
            if(_optionalCondition == null )
                _optionalCondition = new PropertyPath<String>(EmployeeAddSExtractGen.P_OPTIONAL_CONDITION, this);
            return _optionalCondition;
        }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getLabourContractType()
     */
        public LabourContractType.Path<LabourContractType> labourContractType()
        {
            if(_labourContractType == null )
                _labourContractType = new LabourContractType.Path<LabourContractType>(L_LABOUR_CONTRACT_TYPE, this);
            return _labourContractType;
        }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getLabourContractNumber()
     */
        public PropertyPath<String> labourContractNumber()
        {
            if(_labourContractNumber == null )
                _labourContractNumber = new PropertyPath<String>(EmployeeAddSExtractGen.P_LABOUR_CONTRACT_NUMBER, this);
            return _labourContractNumber;
        }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getLabourContractDate()
     */
        public PropertyPath<Date> labourContractDate()
        {
            if(_labourContractDate == null )
                _labourContractDate = new PropertyPath<Date>(EmployeeAddSExtractGen.P_LABOUR_CONTRACT_DATE, this);
            return _labourContractDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeAddSExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeAddSExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Продолжительность испытательного срока. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getTrialPeriod()
     */
        public PropertyPath<Integer> trialPeriod()
        {
            if(_trialPeriod == null )
                _trialPeriod = new PropertyPath<Integer>(EmployeeAddSExtractGen.P_TRIAL_PERIOD, this);
            return _trialPeriod;
        }

    /**
     * @return Учебная нагрузка в течении учебного года.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#getHoursAmount()
     */
        public PropertyPath<Integer> hoursAmount()
        {
            if(_hoursAmount == null )
                _hoursAmount = new PropertyPath<Integer>(EmployeeAddSExtractGen.P_HOURS_AMOUNT, this);
            return _hoursAmount;
        }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddSExtract#isFreelance()
     */
        public PropertyPath<Boolean> freelance()
        {
            if(_freelance == null )
                _freelance = new PropertyPath<Boolean>(EmployeeAddSExtractGen.P_FREELANCE, this);
            return _freelance;
        }

        public Class getEntityClass()
        {
            return EmployeeAddSExtract.class;
        }

        public String getEntityName()
        {
            return "employeeAddSExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
