package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из списочного приказа по кадровому составу. Произвести оплату за работу в нерабочий праздничный день
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PayForHolidayDayEmplListExtractGen extends ListEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract";
    public static final String ENTITY_NAME = "payForHolidayDayEmplListExtract";
    public static final int VERSION_HASH = -2037066300;
    private static IEntityMeta ENTITY_META;

    public static final String P_HOLIDAY_DATE = "holidayDate";
    public static final String P_CHILD_ORG_UNIT = "childOrgUnit";
    public static final String P_WORK_TIME = "workTime";
    public static final String P_TIME_AMOUNT = "timeAmount";
    public static final String P_NIGHT_TIME_AMOUNT = "nightTimeAmount";
    public static final String P_WORK_TYPE = "workType";

    private Date _holidayDate;     // Дата нерабочего/праздничного дня
    private boolean _childOrgUnit;     // Учитывать дочерние подразделения
    private String _workTime;     // Часы работы в нерабочий день
    private double _timeAmount;     // Количество часов
    private double _nightTimeAmount;     // Количество часов, в том числе ночных
    private String _workType;     // Вид работ(б/гр или п/гр)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата нерабочего/праздничного дня. Свойство не может быть null.
     */
    @NotNull
    public Date getHolidayDate()
    {
        return _holidayDate;
    }

    /**
     * @param holidayDate Дата нерабочего/праздничного дня. Свойство не может быть null.
     */
    public void setHolidayDate(Date holidayDate)
    {
        dirty(_holidayDate, holidayDate);
        _holidayDate = holidayDate;
    }

    /**
     * @return Учитывать дочерние подразделения. Свойство не может быть null.
     */
    @NotNull
    public boolean isChildOrgUnit()
    {
        return _childOrgUnit;
    }

    /**
     * @param childOrgUnit Учитывать дочерние подразделения. Свойство не может быть null.
     */
    public void setChildOrgUnit(boolean childOrgUnit)
    {
        dirty(_childOrgUnit, childOrgUnit);
        _childOrgUnit = childOrgUnit;
    }

    /**
     * @return Часы работы в нерабочий день. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getWorkTime()
    {
        return _workTime;
    }

    /**
     * @param workTime Часы работы в нерабочий день. Свойство не может быть null.
     */
    public void setWorkTime(String workTime)
    {
        dirty(_workTime, workTime);
        _workTime = workTime;
    }

    /**
     * @return Количество часов. Свойство не может быть null.
     */
    @NotNull
    public double getTimeAmount()
    {
        return _timeAmount;
    }

    /**
     * @param timeAmount Количество часов. Свойство не может быть null.
     */
    public void setTimeAmount(double timeAmount)
    {
        dirty(_timeAmount, timeAmount);
        _timeAmount = timeAmount;
    }

    /**
     * @return Количество часов, в том числе ночных. Свойство не может быть null.
     */
    @NotNull
    public double getNightTimeAmount()
    {
        return _nightTimeAmount;
    }

    /**
     * @param nightTimeAmount Количество часов, в том числе ночных. Свойство не может быть null.
     */
    public void setNightTimeAmount(double nightTimeAmount)
    {
        dirty(_nightTimeAmount, nightTimeAmount);
        _nightTimeAmount = nightTimeAmount;
    }

    /**
     * @return Вид работ(б/гр или п/гр). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getWorkType()
    {
        return _workType;
    }

    /**
     * @param workType Вид работ(б/гр или п/гр). Свойство не может быть null.
     */
    public void setWorkType(String workType)
    {
        dirty(_workType, workType);
        _workType = workType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PayForHolidayDayEmplListExtractGen)
        {
            setHolidayDate(((PayForHolidayDayEmplListExtract)another).getHolidayDate());
            setChildOrgUnit(((PayForHolidayDayEmplListExtract)another).isChildOrgUnit());
            setWorkTime(((PayForHolidayDayEmplListExtract)another).getWorkTime());
            setTimeAmount(((PayForHolidayDayEmplListExtract)another).getTimeAmount());
            setNightTimeAmount(((PayForHolidayDayEmplListExtract)another).getNightTimeAmount());
            setWorkType(((PayForHolidayDayEmplListExtract)another).getWorkType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PayForHolidayDayEmplListExtractGen> extends ListEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PayForHolidayDayEmplListExtract.class;
        }

        public T newInstance()
        {
            return (T) new PayForHolidayDayEmplListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "holidayDate":
                    return obj.getHolidayDate();
                case "childOrgUnit":
                    return obj.isChildOrgUnit();
                case "workTime":
                    return obj.getWorkTime();
                case "timeAmount":
                    return obj.getTimeAmount();
                case "nightTimeAmount":
                    return obj.getNightTimeAmount();
                case "workType":
                    return obj.getWorkType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "holidayDate":
                    obj.setHolidayDate((Date) value);
                    return;
                case "childOrgUnit":
                    obj.setChildOrgUnit((Boolean) value);
                    return;
                case "workTime":
                    obj.setWorkTime((String) value);
                    return;
                case "timeAmount":
                    obj.setTimeAmount((Double) value);
                    return;
                case "nightTimeAmount":
                    obj.setNightTimeAmount((Double) value);
                    return;
                case "workType":
                    obj.setWorkType((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "holidayDate":
                        return true;
                case "childOrgUnit":
                        return true;
                case "workTime":
                        return true;
                case "timeAmount":
                        return true;
                case "nightTimeAmount":
                        return true;
                case "workType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "holidayDate":
                    return true;
                case "childOrgUnit":
                    return true;
                case "workTime":
                    return true;
                case "timeAmount":
                    return true;
                case "nightTimeAmount":
                    return true;
                case "workType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "holidayDate":
                    return Date.class;
                case "childOrgUnit":
                    return Boolean.class;
                case "workTime":
                    return String.class;
                case "timeAmount":
                    return Double.class;
                case "nightTimeAmount":
                    return Double.class;
                case "workType":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PayForHolidayDayEmplListExtract> _dslPath = new Path<PayForHolidayDayEmplListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PayForHolidayDayEmplListExtract");
    }
            

    /**
     * @return Дата нерабочего/праздничного дня. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getHolidayDate()
     */
    public static PropertyPath<Date> holidayDate()
    {
        return _dslPath.holidayDate();
    }

    /**
     * @return Учитывать дочерние подразделения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#isChildOrgUnit()
     */
    public static PropertyPath<Boolean> childOrgUnit()
    {
        return _dslPath.childOrgUnit();
    }

    /**
     * @return Часы работы в нерабочий день. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getWorkTime()
     */
    public static PropertyPath<String> workTime()
    {
        return _dslPath.workTime();
    }

    /**
     * @return Количество часов. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getTimeAmount()
     */
    public static PropertyPath<Double> timeAmount()
    {
        return _dslPath.timeAmount();
    }

    /**
     * @return Количество часов, в том числе ночных. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getNightTimeAmount()
     */
    public static PropertyPath<Double> nightTimeAmount()
    {
        return _dslPath.nightTimeAmount();
    }

    /**
     * @return Вид работ(б/гр или п/гр). Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getWorkType()
     */
    public static PropertyPath<String> workType()
    {
        return _dslPath.workType();
    }

    public static class Path<E extends PayForHolidayDayEmplListExtract> extends ListEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _holidayDate;
        private PropertyPath<Boolean> _childOrgUnit;
        private PropertyPath<String> _workTime;
        private PropertyPath<Double> _timeAmount;
        private PropertyPath<Double> _nightTimeAmount;
        private PropertyPath<String> _workType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата нерабочего/праздничного дня. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getHolidayDate()
     */
        public PropertyPath<Date> holidayDate()
        {
            if(_holidayDate == null )
                _holidayDate = new PropertyPath<Date>(PayForHolidayDayEmplListExtractGen.P_HOLIDAY_DATE, this);
            return _holidayDate;
        }

    /**
     * @return Учитывать дочерние подразделения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#isChildOrgUnit()
     */
        public PropertyPath<Boolean> childOrgUnit()
        {
            if(_childOrgUnit == null )
                _childOrgUnit = new PropertyPath<Boolean>(PayForHolidayDayEmplListExtractGen.P_CHILD_ORG_UNIT, this);
            return _childOrgUnit;
        }

    /**
     * @return Часы работы в нерабочий день. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getWorkTime()
     */
        public PropertyPath<String> workTime()
        {
            if(_workTime == null )
                _workTime = new PropertyPath<String>(PayForHolidayDayEmplListExtractGen.P_WORK_TIME, this);
            return _workTime;
        }

    /**
     * @return Количество часов. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getTimeAmount()
     */
        public PropertyPath<Double> timeAmount()
        {
            if(_timeAmount == null )
                _timeAmount = new PropertyPath<Double>(PayForHolidayDayEmplListExtractGen.P_TIME_AMOUNT, this);
            return _timeAmount;
        }

    /**
     * @return Количество часов, в том числе ночных. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getNightTimeAmount()
     */
        public PropertyPath<Double> nightTimeAmount()
        {
            if(_nightTimeAmount == null )
                _nightTimeAmount = new PropertyPath<Double>(PayForHolidayDayEmplListExtractGen.P_NIGHT_TIME_AMOUNT, this);
            return _nightTimeAmount;
        }

    /**
     * @return Вид работ(б/гр или п/гр). Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract#getWorkType()
     */
        public PropertyPath<String> workType()
        {
            if(_workType == null )
                _workType = new PropertyPath<String>(PayForHolidayDayEmplListExtractGen.P_WORK_TYPE, this);
            return _workType;
        }

        public Class getEntityClass()
        {
            return PayForHolidayDayEmplListExtract.class;
        }

        public String getEntityName()
        {
            return "payForHolidayDayEmplListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
