/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e9.AddEdit;

import java.util.List;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.DismissalWEndOfLabourContrExtract;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<DismissalWEndOfLabourContrExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected DismissalWEndOfLabourContrExtract createNewInstance()
    {
        return new DismissalWEndOfLabourContrExtract();
    }
    
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.isAddForm()) model.getExtract().setEntity(model.getEmployeePost());
        else model.setEmployeePost(model.getExtract().getEntity());
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setOldEmployeePostStatus(model.getExtract().getEntity().getPostStatus());
        super.update(model);
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }
}