/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListMultipleExtractAdd;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
public abstract class AbstractListMultipleExtractAddDAO<T extends ListEmployeeExtract, Model extends AbstractListMultipleExtractAddModel<T>> extends UniDao<Model> implements IAbstractListMultipleExtractAddDAO<T, Model>
{
    private static OrderDescriptionRegistry orderRegistry = new OrderDescriptionRegistry("p");

    static
    {
        orderRegistry.setOrders(Employee.EMPLOYEE_FIO, new OrderDescription("p", Employee.EMPLOYEE_LAST_NAME), new OrderDescription("p", Employee.EMPLOYEE_FIRST_NAME), new OrderDescription("p", Employee.EMPLOYEE_MIDDLE_NAME));
    }

    @Override
    public void prepare(Model model)
    {
        model.setOrder((EmployeeListOrder)getNotNull(model.getOrderId()));
        model.setExtractType(getNotNull(EmployeeExtractType.class, model.getExtractType().getId()));

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(Employee.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", Employee.P_ARCHIVAL, Boolean.FALSE));

        patchListDataSource(builder, model);

        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());
        List<Employee> list = builder.getResultList(getSession());
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);
    }

    protected abstract void patchListDataSource(MQBuilder builder, Model model);

    @Override
    @SuppressWarnings({"unchecked"})
    public void update(Model model)
    {
        // V A L I D A T E

        final Session session = getSession();

        List<EmployeeListParagraph> addedParagraphsList = new ArrayList<EmployeeListParagraph>();

        session.refresh(model.getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");

        List<ViewWrapper> viewlist = new ArrayList<ViewWrapper>((Collection)((CheckboxColumn)model.getDataSource().getColumn("checkbox")).getSelectedObjects());
        List<Employee> list = new ArrayList<Employee>();
        for (ViewWrapper viewWrapper : viewlist)
            list.add((Employee)viewWrapper.getEntity());
        if (list.size() == 0) throw new ApplicationException("Параграф не может быть пустым.");
        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        List<Employee> employeeList = getEmployeeList(model, list).getResultList(session);

        Map<Long, List<Employee>> employeeMap = new HashMap<Long, List<Employee>>();

        // пока мы были на форме state мог очень сильно поменяться, поэтому надо все данные проверить на актуаность
        for (Employee employee : employeeList)
        {
            List<Employee> singleEmployeeList = employeeMap.get(employee.getId());
            if (null == singleEmployeeList) singleEmployeeList = new ArrayList<Employee>();
            singleEmployeeList.add(employee);
            employeeMap.put(employee.getId(), singleEmployeeList);

            // студент не должен быть в другом приказе
            /*TODO: может быть создан как на Employee, так и на EmployeePost
            if (!MoveEmployeeDaoFacade.getMoveEmployeeDao().isMoveAccessible(employee))
                throw new ApplicationException("Нельзя добавить параграф для группы " + employee.getTitle() + ", так как у сотрудника '" + employee.getPerson().getFullFio() + "' уже есть непроведенные выписки.");*/
        }

        // C R E A T E   P A R A G R A P H S
        int paragraphCount = model.getOrder().getParagraphCount() + 1;
        for (Employee group : list)
        {
            List<Employee> singleEmployeeList = employeeMap.get(group.getId());
            Collections.sort(singleEmployeeList, ITitled.TITLED_COMPARATOR);

            if (singleEmployeeList.size() != 0)
            {
                EmployeeListParagraph paragraph = createAndFillParagraph(model, paragraphCount++);
                session.save(paragraph);

                for (Employee employee : singleEmployeeList)
                {
                    // создаем выписку и заполняем в ней стандартные поля
                    T extract = createAndFillExtract(employee, model, group);
                    session.save(extract);
                }

                // Перенумеруем все выписки, так чтобы сортировка была по ФИО
                List<ListEmployeeExtract> extractList = getList(ListEmployeeExtract.class, IAbstractExtract.L_PARAGRAPH, model.getParagraph());
                extractList.sort(ListEmployeeExtract.FULL_FIO_AND_ID_COMPARATOR);

                // нумеруем выписки с единицы
                int counter = 1;
                for (ListEmployeeExtract extract : extractList)
                {
                    extract.setNumber(counter++);
                    update(extract);
                }
                addedParagraphsList.add(paragraph);
            }
        }
        model.setAddedParagraphs(addedParagraphsList);
    }

    protected MQBuilder getEmployeeList(Model model, List<Employee> groupsList)
    {
        MQBuilder builder = new MQBuilder(Employee.ENTITY_CLASS, "e");
        //builder.add(MQExpression.in("e", Employee.L_GROUP, groupsList));
        builder.add(MQExpression.eq("e", Employee.P_ARCHIVAL, Boolean.FALSE));
        //builder.add(MQExpression.eq("e", Employee.L_STATUS + "." + StudentStatus.P_ACTIVE, Boolean.TRUE));
        return builder;
    }

    // конструктор выписки
    protected abstract T createNewInstance(Model model);

    // заполняем специфические параметры выписки
    protected abstract void fillExtract(T extract, Model model, Employee employee);

    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    private T createAndFillExtract(Employee employee, Model model, Employee group)
    {
        T extract = createNewInstance(model);

        // сохраняем данные в модели
        model.setExtract(extract);

        // в student уже есть все данные для проведения выписки
        extract.setEmployee(employee);
        extract.setParagraph(model.getParagraph());
        extract.setType(model.getExtractType());
        extract.setCreateDate(model.getOrder().getCreateDate());
        extract.setNumber(null); // нет номера. перенумеруем в конце метода update

        // Хочу иметь возможность взять данные entity в этом методе
        fillExtract(extract, model, group);

        //обновляем fio в нужном падеже
        syncEmployeeTitle(model);

        //обновляем текстовые значения старых данных
        syncPreCommitData(model);

        return extract;
    }

    private EmployeeListParagraph createAndFillParagraph(Model model, int paragraphNumber)
    {
        EmployeeListParagraph paragraph = new EmployeeListParagraph();

        // сохраняем данные в модели
        model.setParagraph(paragraph);
        paragraph.setOrder(model.getOrder());
        paragraph.setNumber(paragraphNumber);

        return paragraph;
    }

    // TODO: doesn't need this any more
    private void syncPreCommitData(Model model)
    {
        // T extract = model.getExtract();
        // Employee employee = extract.getEmployee();
        // EmployeePost employeePost = extract.getEntity();

        /*extract.setStudentTitleStr(student.getPerson().getFullFio());
        extract.setStudentStatusStr(student.getStatus().getTitle());
        extract.setPersonalNumberStr(Integer.toString(student.getPersonalNumber()));
        extract.setCourseStr(student.getCourse().getTitle());
        extract.setGroupStr(student.getGroup() == null ? "" : student.getGroup().getTitle());
        extract.setCompensationTypeStr(student.getCompensationType().getShortTitle());
        extract.setFormativeOrgUnitStr(student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle());
        extract.setTerritorialOrgUnitStr(student.getEducationOrgUnit().getTerritorialOrgUnit() == null ? "" : student.getEducationOrgUnit().getTerritorialOrgUnit().getShortTitle());
        extract.setEducationLevelHighSchoolStr(student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle());
        extract.setDevelopFormStr(student.getEducationOrgUnit().getDevelopForm().getTitle());
        extract.setDevelopConditionStr(student.getEducationOrgUnit().getDevelopCondition().getTitle());
        extract.setDevelopTechStr(student.getEducationOrgUnit().getDevelopTech().getTitle());
        extract.setDevelopPeriodStr(student.getEducationOrgUnit().getDevelopPeriod().getTitle());*/
    }

    // TODO: doesn't need this any more
    private void syncEmployeeTitle(Model model)
    {
//        GrammaCase rusCase = getEmployeeTitleCase();
//        if (rusCase != null)
//        {
//            IdentityCard identityCard = model.getExtract().getEntity().getPerson().getIdentityCard();
//            boolean isMaleSex = identityCard.getSex().isMale();

//            StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex));
//            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
//            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
//                str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));
            //model.getExtract().setStudentTitle(str.toString());
//       } else
//                   model.getExtract().setStudentTitle(model.getExtract().getEntity().getPerson().getFullFio());
    }
}