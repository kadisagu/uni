/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e17.Pub;

import java.util.List;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 09.09.2011
 */
public class DAO extends ModularEmployeeExtractPubDAO<RevocationFromHolidayExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        //генерим строку Частей отпуска, если они есть
        if (model.getExtract().isProvideUnusedHoliday())
        {
            MQBuilder builder = new MQBuilder(ProvideDateToExtractRelation.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", ProvideDateToExtractRelation.L_REVOCATION_FROM_HOLIDAY_EXTRACT, model.getExtract()));
            builder.addOrder("b", ProvideDateToExtractRelation.P_PROVIDE_BEGIN_DAY, OrderDirection.asc);

            String resultStr = "";
            List<ProvideDateToExtractRelation> relationList = builder.<ProvideDateToExtractRelation>getResultList(getSession());
            for (ProvideDateToExtractRelation relation : relationList)
            {
                resultStr += "c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(relation.getProvideBeginDay()) +
                        " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(relation.getProvideEndDay()) +
                        ", длительность - " + relation.getProvideDurationDay() + "; " + (relationList.indexOf(relation) == relationList.size() - 1 ? "" : "<br/>");
            }

            model.setProvidePeriodsStr(resultStr);
        }
    }
}