/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e15.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 24.03.2011
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<ProlongationAnnualHolidayExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        prepareStaffRateDataSource(component);
    }

    public void onChangeHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);
        GregorianCalendar nextDate = (GregorianCalendar)GregorianCalendar.getInstance();
        if (model.getExtract().getHoliday() != null)
        {
            //утановка даты начала продления отпуска
            nextDate.setTime(model.getExtract().getHoliday().getFinishDate());
            nextDate.add(Calendar.DATE, 1);
            model.getExtract().setProlongationFrom(nextDate.getTime());

            //установка даты начала отпуска
            model.getExtract().setHolidayBegin(model.getExtract().getHoliday().getStartDate());

            //установка длительности отпуска
            if (model.getExtract().getHolidayBegin() != null && model.getExtract().getHolidayEnd() != null)
            {
                if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                {
                    EmployeeWorkWeekDuration workWeekDuration = model.getExtract().getEntity() != null ? model.getExtract().getEntity().getWorkWeekDuration() :model.getEmployeePost().getWorkWeekDuration();
                    model.getExtract().setHolidayDuration(EmployeeManager.instance().dao().getEmployeeHolidayDuration(workWeekDuration, model.getExtract().getHolidayBegin(), model.getExtract().getHolidayEnd()));
                }
                else
                    model.getExtract().setHolidayDuration((int) CommonBaseDateUtil.getBetweenPeriod(model.getExtract().getHolidayBegin(), model.getExtract().getHolidayEnd(), Calendar.DAY_OF_YEAR) + 1);
            }
        }

        //установка количества дней продления
        if (model.getExtract().getProlongationFrom() != null && model.getExtract().getProlongationTo() != null &&
                model.getExtract().getAmountDaysProlongation() == 0)
        {
            if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
            {
                EmployeeWorkWeekDuration workWeekDuration = model.getExtract().getEntity() != null ? model.getExtract().getEntity().getWorkWeekDuration() :model.getEmployeePost().getWorkWeekDuration();
                model.getExtract().setAmountDaysProlongation(EmployeeManager.instance().dao().getEmployeeHolidayDuration(workWeekDuration, model.getExtract().getProlongationFrom(), model.getExtract().getProlongationTo()));
            }
            else
                model.getExtract().setAmountDaysProlongation((int) CommonBaseDateUtil.getBetweenPeriod(model.getExtract().getProlongationFrom(), model.getExtract().getProlongationTo(), Calendar.DAY_OF_YEAR) + 1);
        }
    }

    public void onChangeAmountDaysProlongation(IBusinessComponent component)
    {
        Model model = getModel(component);

        //установка значения Продлить по
        if (model.getExtract().getProlongationFrom() != null && (Integer) model.getExtract().getAmountDaysProlongation() != null &&
                model.getExtract().getProlongationTo() == null)
        {
            GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            int counter = 0;
            try
            {
                @SuppressWarnings("unused")
                int duration = model.getExtract().getAmountDaysProlongation();
            }
            catch (Exception e)
            {
                return;
            }
            int duration = model.getExtract().getAmountDaysProlongation();

            date.setTime(model.getExtract().getProlongationFrom());

            if (duration > 0)
            {
                do
                {
                    if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                    {
                        if (!getDao().isIndustrialCalendarHoliday(workWeekDuration, date.getTime()))
                            counter++;
                    }
                    else
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);

                model.getExtract().setProlongationTo(date.getTime());
                model.getExtract().setHolidayEnd(model.getExtract().getProlongationTo());
            }

            //установка длительности отпуска
            if (model.getExtract().getHolidayBegin() != null && model.getExtract().getHolidayEnd() != null)
            {
                if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                    model.getExtract().setHolidayDuration(getDao().getEmployeeHolidayDuration(workWeekDuration, model.getExtract().getHolidayBegin(), model.getExtract().getHolidayEnd()));
                else
                    model.getExtract().setHolidayDuration((int) CommonBaseDateUtil.getBetweenPeriod(model.getExtract().getHolidayBegin(), model.getExtract().getHolidayEnd(), Calendar.DAY_OF_YEAR) + 1);
            }
        }
    }

    public void onChangeProlongation(IBusinessComponent component )
    {
        Model model = getModel(component);

        //установака количества дней продления
        if (model.getExtract().getProlongationFrom() != null && model.getExtract().getProlongationTo() != null &&
                model.getExtract().getAmountDaysProlongation() == 0)
        {
            if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
            {
                EmployeeWorkWeekDuration workWeekDuration = model.getExtract().getEntity() != null ? model.getExtract().getEntity().getWorkWeekDuration() :model.getEmployeePost().getWorkWeekDuration();
                model.getExtract().setAmountDaysProlongation(EmployeeManager.instance().dao().getEmployeeHolidayDuration(workWeekDuration, model.getExtract().getProlongationFrom(), model.getExtract().getProlongationTo()));
            }
            else
                model.getExtract().setAmountDaysProlongation((int) CommonBaseDateUtil.getBetweenPeriod(model.getExtract().getProlongationFrom(), model.getExtract().getProlongationTo(), Calendar.DAY_OF_YEAR) + 1);
        }

        //установка даты окончания отпуска
        if (model.getExtract().getProlongationTo() != null)
            model.getExtract().setHolidayEnd(model.getExtract().getProlongationTo());

        //установка длительности отпуска
        if (model.getExtract().getHolidayBegin() != null && model.getExtract().getHolidayEnd() != null)
        {
            if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
            {
                EmployeeWorkWeekDuration workWeekDuration = model.getExtract().getEntity() != null ? model.getExtract().getEntity().getWorkWeekDuration() :model.getEmployeePost().getWorkWeekDuration();
                model.getExtract().setHolidayDuration(EmployeeManager.instance().dao().getEmployeeHolidayDuration(workWeekDuration, model.getExtract().getHolidayBegin(), model.getExtract().getHolidayEnd()));
            }
            else
                model.getExtract().setHolidayDuration((int) CommonBaseDateUtil.getBetweenPeriod(model.getExtract().getHolidayBegin(), model.getExtract().getHolidayEnd(), Calendar.DAY_OF_YEAR) + 1);
        }

    }

    private void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setStaffRateDataSource(dataSource);
    }
}