/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.menuempl.ModularOrdersFormation;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * @author dseleznev
 *         Created on: 10.11.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "ModularEmplOrdersFormation.filter"));

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<IAbstractOrder> dataSource = UniBaseUtils.createDataSource(component, getDao());
        //при переходе по ссылке приказа передаются различные параметры и активируются различные компоненты,
        //если переходим на выписку, то параметры определяют отображать выписку как приказ или нет
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", IAbstractOrder.P_CREATE_DATE);
        linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                EmployeeModularOrder order = (EmployeeModularOrder) ((ViewWrapper) entity).getEntity();
                if (model.getIndividualOrderMap().get(order.getId()))
                    return MoveEmployeeUtils.getModularEmployeeExtractPubComponent(model.getIndexExtractOrderMap().get(order.getId()).getType().getIndex());
                else
                    return MoveEmployeeUtils.getModularOrderPubComponent();
            }

            @Override
            public Object getParameters(IEntity entity)
            {
                EmployeeModularOrder order = (EmployeeModularOrder) ((ViewWrapper) entity).getEntity();
                if (model.getIndividualOrderMap().get(order.getId()))
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, model.getIndexExtractOrderMap().get(order.getId()).getId()).
                                        add("extractIndividual", true);
                else
                    return (Long)((ViewWrapper) entity).getEntity().getId();
            }
        });
        dataSource.addColumn(linkColumn);
//        dataSource.addColumn(new SimpleColumn("Дата формирования", IAbstractOrder.P_CREATE_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Дата приказа", EmployeeModularOrder.P_COMMIT_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", IAbstractOrder.P_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", new String[]{IAbstractOrder.L_STATE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во проектов приказа", EmployeeModularOrder.P_EXTRACT_COUNT).setClickable(false).setOrderable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("print_menuList_modularEmployeeOrdersFormation"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("edit_menuList_modularEmployeeOrdersFormation").setDisabledProperty(AbstractEmployeeOrder.P_READONLY));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey("delete_menuList_modularEmployeeOrdersFormation").setDisabledProperty(AbstractEmployeeOrder.P_NO_DELETE));

        dataSource.setOrder(IAbstractOrder.P_CREATE_DATE, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        DataSettingsFacade.saveSettings(getModel(component).getSettings());
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickAdd(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_MODULAR_ORDER_ADD));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", component.getListenerParameter())
        ));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_MODULAR_ORDER_EDIT, new ParametersMap()
                .add("orderId", component.getListenerParameter())
        ));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        EmployeeModularOrder order = UniDaoFacade.getCoreDao().get((Long) component.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrder(order, UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
    }
}