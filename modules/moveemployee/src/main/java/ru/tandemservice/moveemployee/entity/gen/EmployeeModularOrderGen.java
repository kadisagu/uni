package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сборный приказ по кадровому составу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeModularOrderGen extends AbstractEmployeeOrder
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeModularOrder";
    public static final String ENTITY_NAME = "employeeModularOrder";
    public static final int VERSION_HASH = -1851710798;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARAGRAPHS_SORT_RULE = "paragraphsSortRule";

    private EmployeeModularOrderParagraphsSortRule _paragraphsSortRule;     // Порядок пунктов приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Порядок пунктов приказа. Свойство не может быть null.
     */
    @NotNull
    public EmployeeModularOrderParagraphsSortRule getParagraphsSortRule()
    {
        return _paragraphsSortRule;
    }

    /**
     * @param paragraphsSortRule Порядок пунктов приказа. Свойство не может быть null.
     */
    public void setParagraphsSortRule(EmployeeModularOrderParagraphsSortRule paragraphsSortRule)
    {
        dirty(_paragraphsSortRule, paragraphsSortRule);
        _paragraphsSortRule = paragraphsSortRule;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeModularOrderGen)
        {
            setParagraphsSortRule(((EmployeeModularOrder)another).getParagraphsSortRule());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeModularOrderGen> extends AbstractEmployeeOrder.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeModularOrder.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeModularOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "paragraphsSortRule":
                    return obj.getParagraphsSortRule();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "paragraphsSortRule":
                    obj.setParagraphsSortRule((EmployeeModularOrderParagraphsSortRule) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "paragraphsSortRule":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "paragraphsSortRule":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "paragraphsSortRule":
                    return EmployeeModularOrderParagraphsSortRule.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeModularOrder> _dslPath = new Path<EmployeeModularOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeModularOrder");
    }
            

    /**
     * @return Порядок пунктов приказа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrder#getParagraphsSortRule()
     */
    public static EmployeeModularOrderParagraphsSortRule.Path<EmployeeModularOrderParagraphsSortRule> paragraphsSortRule()
    {
        return _dslPath.paragraphsSortRule();
    }

    public static class Path<E extends EmployeeModularOrder> extends AbstractEmployeeOrder.Path<E>
    {
        private EmployeeModularOrderParagraphsSortRule.Path<EmployeeModularOrderParagraphsSortRule> _paragraphsSortRule;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Порядок пунктов приказа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeModularOrder#getParagraphsSortRule()
     */
        public EmployeeModularOrderParagraphsSortRule.Path<EmployeeModularOrderParagraphsSortRule> paragraphsSortRule()
        {
            if(_paragraphsSortRule == null )
                _paragraphsSortRule = new EmployeeModularOrderParagraphsSortRule.Path<EmployeeModularOrderParagraphsSortRule>(L_PARAGRAPHS_SORT_RULE, this);
            return _paragraphsSortRule;
        }

        public Class getEntityClass()
        {
            return EmployeeModularOrder.class;
        }

        public String getEntityName()
        {
            return "employeeModularOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
