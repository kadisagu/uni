/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e5;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.listemplextract.CommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.ICommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.IListParagraphPrintFormCreator;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class EmployeePaymentEmplListExtractPrint implements IPrintFormCreator<EmployeeListOrder>, IListParagraphPrintFormCreator<EmployeePaymentEmplListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);

        ICommonListOrderPrint commonListOrderPrint = (ICommonListOrderPrint) ApplicationRuntime.getBean("employeeListOrder_orderPrint");
        commonListOrderPrint.appendVisas(tableModifier, order);

        tableModifier.modify(document);

        return document;
    }

    @SuppressWarnings("unchecked")
    private String[][] getPreparedOrderData(IAbstractParagraph paragraph)
    {
        List<EmployeePaymentEmplListExtract> extractList = paragraph.getExtractList();
        if(null == extractList || extractList.isEmpty()) return new String[][]{};

        List<String[]> resultList = new ArrayList<String[]>();

        Integer i = 0;
        for (EmployeePaymentEmplListExtract extract : extractList)
        {
            String[] line = new String[4];

            i++;
            line[0] = i.toString();
            line[1] = CommonExtractUtil.getModifiedFioUpperCaseLastName(extract.getEmployee().getPerson(), GrammaCase.DATIVE);
            line[2] = CommonExtractPrintUtil.getPostPrintingTitle(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.DATIVE_CASE, false) + " " + CommonExtractPrintUtil.getOrgUnitPrintingTitle(extract.getEntity().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, true, false);
            line[3] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getAmountEmployee());

            resultList.add(line);
        }

        return resultList.toArray(new String[][]{});
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph paragraph, EmployeePaymentEmplListExtract firstExtract)
    {
        RtfInjectModifier modifier = CommonListOrderPrint.createListOrderInjectModifier((EmployeeListOrder) paragraph.getOrder());
        CommonExtractPrintUtil.injectCommonExtractData(modifier, firstExtract);

        String paymentTypeStr = "";
        if (firstExtract.getPayment().isOneTimePayment())
            paymentTypeStr += "Выплатить единовременную";
        else
            paymentTypeStr += "Установить с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getAssignmentBeginDate()) +
                    (firstExtract.getAssignmentEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getAssignmentEndDate()) : "") +
                    " ежемесячную";
        modifier.put("paymentTypeStr", paymentTypeStr);

        modifier.put("paymentName", firstExtract.getPayment().getTitle());

        String financeSourceStr = "из ";
        financeSourceStr += firstExtract.getFinancingSource().getGenitiveCaseTitle() != null ? firstExtract.getFinancingSource().getGenitiveCaseTitle() : firstExtract.getFinancingSource().getTitle();
        if (firstExtract.getFinancingSourceItem() != null)
            financeSourceStr += " " + (firstExtract.getFinancingSourceItem().getGenitiveCaseTitle() != null ? firstExtract.getFinancingSourceItem().getGenitiveCaseTitle() : firstExtract.getFinancingSourceItem().getTitle());
        modifier.put("financeSourceStr", financeSourceStr);

        modifier.put("listBasics", firstExtract.getBasicListStr());

        return modifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph paragraph, EmployeePaymentEmplListExtract firstExtract)
    {
        RtfTableModifier modifier = new RtfTableModifier();

        modifier.put("TT", getPreparedOrderData(paragraph));

        return modifier;
    }
}
