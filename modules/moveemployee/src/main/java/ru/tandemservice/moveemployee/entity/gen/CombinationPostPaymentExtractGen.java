package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Установление доплат за работу по совмещению
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CombinationPostPaymentExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract";
    public static final String ENTITY_NAME = "combinationPostPaymentExtract";
    public static final int VERSION_HASH = 2076493498;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMBINATION_POST_TYPE = "combinationPostType";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_MISSING_EMPLOYEE_POST = "missingEmployeePost";
    public static final String L_SALARY_RAISING_COEFFICIENT = "salaryRaisingCoefficient";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String P_SALARY = "salary";
    public static final String L_CONTRACT_TYPE = "contractType";
    public static final String P_CONTRACT_NUMBER = "contractNumber";
    public static final String P_CONTRACT_DATE = "contractDate";
    public static final String P_CONTRACT_BEGIN_DATE = "contractBeginDate";
    public static final String P_CONTRACT_END_DATE = "contractEndDate";
    public static final String P_CONTRACT_ADD_AGREEMENT_NUMBER = "contractAddAgreementNumber";
    public static final String P_CONTRACT_ADD_AGREEMENT_DATE = "contractAddAgreementDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_FREELANCE = "freelance";
    public static final String L_CREATE_COMBINATION_POST = "createCombinationPost";
    public static final String L_CREATE_EMPLOYEE_CONTRACT = "createEmployeeContract";
    public static final String L_CREATE_COLLATERAL_AGREEMENT = "createCollateralAgreement";

    private CombinationPostType _combinationPostType;     // Тип совмещения
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность
    private EmployeePost _missingEmployeePost;     // Сотрудник, на время отсутствия которого совмещается должность
    private SalaryRaisingCoefficient _salaryRaisingCoefficient;     // Повышающий коэффициент
    private EtksLevels _etksLevels;     // Разряд ЕТКС
    private double _salary;     // Сумма оплаты
    private LabourContractType _contractType;     // Тип трудового договора
    private String _contractNumber;     // Номер трудового договора
    private Date _contractDate;     // Дата трудового договора
    private Date _contractBeginDate;     // Дата начала
    private Date _contractEndDate;     // Дата окончания
    private String _contractAddAgreementNumber;     // Номер доп. соглашения
    private Date _contractAddAgreementDate;     // Дата доп. соглашения
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private boolean _freelance;     // Вне штата
    private CombinationPost _createCombinationPost;     // Должность по совмещению созданная при проведении выписки
    private EmployeeLabourContract _createEmployeeContract;     // Трудовой договор сотрудника созданный при проведении выписки
    private ContractCollateralAgreement _createCollateralAgreement;     // Доп. соглашение созданное при проведении выписки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип совмещения. Свойство не может быть null.
     */
    @NotNull
    public CombinationPostType getCombinationPostType()
    {
        return _combinationPostType;
    }

    /**
     * @param combinationPostType Тип совмещения. Свойство не может быть null.
     */
    public void setCombinationPostType(CombinationPostType combinationPostType)
    {
        dirty(_combinationPostType, combinationPostType);
        _combinationPostType = combinationPostType;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Сотрудник, на время отсутствия которого совмещается должность.
     */
    public EmployeePost getMissingEmployeePost()
    {
        return _missingEmployeePost;
    }

    /**
     * @param missingEmployeePost Сотрудник, на время отсутствия которого совмещается должность.
     */
    public void setMissingEmployeePost(EmployeePost missingEmployeePost)
    {
        dirty(_missingEmployeePost, missingEmployeePost);
        _missingEmployeePost = missingEmployeePost;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getSalaryRaisingCoefficient()
    {
        return _salaryRaisingCoefficient;
    }

    /**
     * @param salaryRaisingCoefficient Повышающий коэффициент.
     */
    public void setSalaryRaisingCoefficient(SalaryRaisingCoefficient salaryRaisingCoefficient)
    {
        dirty(_salaryRaisingCoefficient, salaryRaisingCoefficient);
        _salaryRaisingCoefficient = salaryRaisingCoefficient;
    }

    /**
     * @return Разряд ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Разряд ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     */
    @NotNull
    public LabourContractType getContractType()
    {
        return _contractType;
    }

    /**
     * @param contractType Тип трудового договора. Свойство не может быть null.
     */
    public void setContractType(LabourContractType contractType)
    {
        dirty(_contractType, contractType);
        _contractType = contractType;
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер трудового договора. Свойство не может быть null.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     */
    @NotNull
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата трудового договора. Свойство не может быть null.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getContractBeginDate()
    {
        return _contractBeginDate;
    }

    /**
     * @param contractBeginDate Дата начала. Свойство не может быть null.
     */
    public void setContractBeginDate(Date contractBeginDate)
    {
        dirty(_contractBeginDate, contractBeginDate);
        _contractBeginDate = contractBeginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getContractEndDate()
    {
        return _contractEndDate;
    }

    /**
     * @param contractEndDate Дата окончания.
     */
    public void setContractEndDate(Date contractEndDate)
    {
        dirty(_contractEndDate, contractEndDate);
        _contractEndDate = contractEndDate;
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractAddAgreementNumber()
    {
        return _contractAddAgreementNumber;
    }

    /**
     * @param contractAddAgreementNumber Номер доп. соглашения. Свойство не может быть null.
     */
    public void setContractAddAgreementNumber(String contractAddAgreementNumber)
    {
        dirty(_contractAddAgreementNumber, contractAddAgreementNumber);
        _contractAddAgreementNumber = contractAddAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    public Date getContractAddAgreementDate()
    {
        return _contractAddAgreementDate;
    }

    /**
     * @param contractAddAgreementDate Дата доп. соглашения. Свойство не может быть null.
     */
    public void setContractAddAgreementDate(Date contractAddAgreementDate)
    {
        dirty(_contractAddAgreementDate, contractAddAgreementDate);
        _contractAddAgreementDate = contractAddAgreementDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreelance()
    {
        return _freelance;
    }

    /**
     * @param freelance Вне штата. Свойство не может быть null.
     */
    public void setFreelance(boolean freelance)
    {
        dirty(_freelance, freelance);
        _freelance = freelance;
    }

    /**
     * @return Должность по совмещению созданная при проведении выписки.
     */
    public CombinationPost getCreateCombinationPost()
    {
        return _createCombinationPost;
    }

    /**
     * @param createCombinationPost Должность по совмещению созданная при проведении выписки.
     */
    public void setCreateCombinationPost(CombinationPost createCombinationPost)
    {
        dirty(_createCombinationPost, createCombinationPost);
        _createCombinationPost = createCombinationPost;
    }

    /**
     * @return Трудовой договор сотрудника созданный при проведении выписки.
     */
    public EmployeeLabourContract getCreateEmployeeContract()
    {
        return _createEmployeeContract;
    }

    /**
     * @param createEmployeeContract Трудовой договор сотрудника созданный при проведении выписки.
     */
    public void setCreateEmployeeContract(EmployeeLabourContract createEmployeeContract)
    {
        dirty(_createEmployeeContract, createEmployeeContract);
        _createEmployeeContract = createEmployeeContract;
    }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     */
    public ContractCollateralAgreement getCreateCollateralAgreement()
    {
        return _createCollateralAgreement;
    }

    /**
     * @param createCollateralAgreement Доп. соглашение созданное при проведении выписки.
     */
    public void setCreateCollateralAgreement(ContractCollateralAgreement createCollateralAgreement)
    {
        dirty(_createCollateralAgreement, createCollateralAgreement);
        _createCollateralAgreement = createCollateralAgreement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof CombinationPostPaymentExtractGen)
        {
            setCombinationPostType(((CombinationPostPaymentExtract)another).getCombinationPostType());
            setOrgUnit(((CombinationPostPaymentExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((CombinationPostPaymentExtract)another).getPostBoundedWithQGandQL());
            setMissingEmployeePost(((CombinationPostPaymentExtract)another).getMissingEmployeePost());
            setSalaryRaisingCoefficient(((CombinationPostPaymentExtract)another).getSalaryRaisingCoefficient());
            setEtksLevels(((CombinationPostPaymentExtract)another).getEtksLevels());
            setSalary(((CombinationPostPaymentExtract)another).getSalary());
            setContractType(((CombinationPostPaymentExtract)another).getContractType());
            setContractNumber(((CombinationPostPaymentExtract)another).getContractNumber());
            setContractDate(((CombinationPostPaymentExtract)another).getContractDate());
            setContractBeginDate(((CombinationPostPaymentExtract)another).getContractBeginDate());
            setContractEndDate(((CombinationPostPaymentExtract)another).getContractEndDate());
            setContractAddAgreementNumber(((CombinationPostPaymentExtract)another).getContractAddAgreementNumber());
            setContractAddAgreementDate(((CombinationPostPaymentExtract)another).getContractAddAgreementDate());
            setBeginDate(((CombinationPostPaymentExtract)another).getBeginDate());
            setEndDate(((CombinationPostPaymentExtract)another).getEndDate());
            setFreelance(((CombinationPostPaymentExtract)another).isFreelance());
            setCreateCombinationPost(((CombinationPostPaymentExtract)another).getCreateCombinationPost());
            setCreateEmployeeContract(((CombinationPostPaymentExtract)another).getCreateEmployeeContract());
            setCreateCollateralAgreement(((CombinationPostPaymentExtract)another).getCreateCollateralAgreement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CombinationPostPaymentExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CombinationPostPaymentExtract.class;
        }

        public T newInstance()
        {
            return (T) new CombinationPostPaymentExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPostType":
                    return obj.getCombinationPostType();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "missingEmployeePost":
                    return obj.getMissingEmployeePost();
                case "salaryRaisingCoefficient":
                    return obj.getSalaryRaisingCoefficient();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "salary":
                    return obj.getSalary();
                case "contractType":
                    return obj.getContractType();
                case "contractNumber":
                    return obj.getContractNumber();
                case "contractDate":
                    return obj.getContractDate();
                case "contractBeginDate":
                    return obj.getContractBeginDate();
                case "contractEndDate":
                    return obj.getContractEndDate();
                case "contractAddAgreementNumber":
                    return obj.getContractAddAgreementNumber();
                case "contractAddAgreementDate":
                    return obj.getContractAddAgreementDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "freelance":
                    return obj.isFreelance();
                case "createCombinationPost":
                    return obj.getCreateCombinationPost();
                case "createEmployeeContract":
                    return obj.getCreateEmployeeContract();
                case "createCollateralAgreement":
                    return obj.getCreateCollateralAgreement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "combinationPostType":
                    obj.setCombinationPostType((CombinationPostType) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "missingEmployeePost":
                    obj.setMissingEmployeePost((EmployeePost) value);
                    return;
                case "salaryRaisingCoefficient":
                    obj.setSalaryRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "contractType":
                    obj.setContractType((LabourContractType) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
                case "contractBeginDate":
                    obj.setContractBeginDate((Date) value);
                    return;
                case "contractEndDate":
                    obj.setContractEndDate((Date) value);
                    return;
                case "contractAddAgreementNumber":
                    obj.setContractAddAgreementNumber((String) value);
                    return;
                case "contractAddAgreementDate":
                    obj.setContractAddAgreementDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "freelance":
                    obj.setFreelance((Boolean) value);
                    return;
                case "createCombinationPost":
                    obj.setCreateCombinationPost((CombinationPost) value);
                    return;
                case "createEmployeeContract":
                    obj.setCreateEmployeeContract((EmployeeLabourContract) value);
                    return;
                case "createCollateralAgreement":
                    obj.setCreateCollateralAgreement((ContractCollateralAgreement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPostType":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "missingEmployeePost":
                        return true;
                case "salaryRaisingCoefficient":
                        return true;
                case "etksLevels":
                        return true;
                case "salary":
                        return true;
                case "contractType":
                        return true;
                case "contractNumber":
                        return true;
                case "contractDate":
                        return true;
                case "contractBeginDate":
                        return true;
                case "contractEndDate":
                        return true;
                case "contractAddAgreementNumber":
                        return true;
                case "contractAddAgreementDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "freelance":
                        return true;
                case "createCombinationPost":
                        return true;
                case "createEmployeeContract":
                        return true;
                case "createCollateralAgreement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPostType":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "missingEmployeePost":
                    return true;
                case "salaryRaisingCoefficient":
                    return true;
                case "etksLevels":
                    return true;
                case "salary":
                    return true;
                case "contractType":
                    return true;
                case "contractNumber":
                    return true;
                case "contractDate":
                    return true;
                case "contractBeginDate":
                    return true;
                case "contractEndDate":
                    return true;
                case "contractAddAgreementNumber":
                    return true;
                case "contractAddAgreementDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "freelance":
                    return true;
                case "createCombinationPost":
                    return true;
                case "createEmployeeContract":
                    return true;
                case "createCollateralAgreement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPostType":
                    return CombinationPostType.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "missingEmployeePost":
                    return EmployeePost.class;
                case "salaryRaisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "salary":
                    return Double.class;
                case "contractType":
                    return LabourContractType.class;
                case "contractNumber":
                    return String.class;
                case "contractDate":
                    return Date.class;
                case "contractBeginDate":
                    return Date.class;
                case "contractEndDate":
                    return Date.class;
                case "contractAddAgreementNumber":
                    return String.class;
                case "contractAddAgreementDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "freelance":
                    return Boolean.class;
                case "createCombinationPost":
                    return CombinationPost.class;
                case "createEmployeeContract":
                    return EmployeeLabourContract.class;
                case "createCollateralAgreement":
                    return ContractCollateralAgreement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CombinationPostPaymentExtract> _dslPath = new Path<CombinationPostPaymentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CombinationPostPaymentExtract");
    }
            

    /**
     * @return Тип совмещения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getCombinationPostType()
     */
    public static CombinationPostType.Path<CombinationPostType> combinationPostType()
    {
        return _dslPath.combinationPostType();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Сотрудник, на время отсутствия которого совмещается должность.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getMissingEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> missingEmployeePost()
    {
        return _dslPath.missingEmployeePost();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getSalaryRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> salaryRaisingCoefficient()
    {
        return _dslPath.salaryRaisingCoefficient();
    }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractType()
     */
    public static LabourContractType.Path<LabourContractType> contractType()
    {
        return _dslPath.contractType();
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractBeginDate()
     */
    public static PropertyPath<Date> contractBeginDate()
    {
        return _dslPath.contractBeginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractEndDate()
     */
    public static PropertyPath<Date> contractEndDate()
    {
        return _dslPath.contractEndDate();
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractAddAgreementNumber()
     */
    public static PropertyPath<String> contractAddAgreementNumber()
    {
        return _dslPath.contractAddAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractAddAgreementDate()
     */
    public static PropertyPath<Date> contractAddAgreementDate()
    {
        return _dslPath.contractAddAgreementDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#isFreelance()
     */
    public static PropertyPath<Boolean> freelance()
    {
        return _dslPath.freelance();
    }

    /**
     * @return Должность по совмещению созданная при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getCreateCombinationPost()
     */
    public static CombinationPost.Path<CombinationPost> createCombinationPost()
    {
        return _dslPath.createCombinationPost();
    }

    /**
     * @return Трудовой договор сотрудника созданный при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getCreateEmployeeContract()
     */
    public static EmployeeLabourContract.Path<EmployeeLabourContract> createEmployeeContract()
    {
        return _dslPath.createEmployeeContract();
    }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getCreateCollateralAgreement()
     */
    public static ContractCollateralAgreement.Path<ContractCollateralAgreement> createCollateralAgreement()
    {
        return _dslPath.createCollateralAgreement();
    }

    public static class Path<E extends CombinationPostPaymentExtract> extends ModularEmployeeExtract.Path<E>
    {
        private CombinationPostType.Path<CombinationPostType> _combinationPostType;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private EmployeePost.Path<EmployeePost> _missingEmployeePost;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _salaryRaisingCoefficient;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private PropertyPath<Double> _salary;
        private LabourContractType.Path<LabourContractType> _contractType;
        private PropertyPath<String> _contractNumber;
        private PropertyPath<Date> _contractDate;
        private PropertyPath<Date> _contractBeginDate;
        private PropertyPath<Date> _contractEndDate;
        private PropertyPath<String> _contractAddAgreementNumber;
        private PropertyPath<Date> _contractAddAgreementDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _freelance;
        private CombinationPost.Path<CombinationPost> _createCombinationPost;
        private EmployeeLabourContract.Path<EmployeeLabourContract> _createEmployeeContract;
        private ContractCollateralAgreement.Path<ContractCollateralAgreement> _createCollateralAgreement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип совмещения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getCombinationPostType()
     */
        public CombinationPostType.Path<CombinationPostType> combinationPostType()
        {
            if(_combinationPostType == null )
                _combinationPostType = new CombinationPostType.Path<CombinationPostType>(L_COMBINATION_POST_TYPE, this);
            return _combinationPostType;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Сотрудник, на время отсутствия которого совмещается должность.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getMissingEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> missingEmployeePost()
        {
            if(_missingEmployeePost == null )
                _missingEmployeePost = new EmployeePost.Path<EmployeePost>(L_MISSING_EMPLOYEE_POST, this);
            return _missingEmployeePost;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getSalaryRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> salaryRaisingCoefficient()
        {
            if(_salaryRaisingCoefficient == null )
                _salaryRaisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_SALARY_RAISING_COEFFICIENT, this);
            return _salaryRaisingCoefficient;
        }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(CombinationPostPaymentExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractType()
     */
        public LabourContractType.Path<LabourContractType> contractType()
        {
            if(_contractType == null )
                _contractType = new LabourContractType.Path<LabourContractType>(L_CONTRACT_TYPE, this);
            return _contractType;
        }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(CombinationPostPaymentExtractGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(CombinationPostPaymentExtractGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractBeginDate()
     */
        public PropertyPath<Date> contractBeginDate()
        {
            if(_contractBeginDate == null )
                _contractBeginDate = new PropertyPath<Date>(CombinationPostPaymentExtractGen.P_CONTRACT_BEGIN_DATE, this);
            return _contractBeginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractEndDate()
     */
        public PropertyPath<Date> contractEndDate()
        {
            if(_contractEndDate == null )
                _contractEndDate = new PropertyPath<Date>(CombinationPostPaymentExtractGen.P_CONTRACT_END_DATE, this);
            return _contractEndDate;
        }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractAddAgreementNumber()
     */
        public PropertyPath<String> contractAddAgreementNumber()
        {
            if(_contractAddAgreementNumber == null )
                _contractAddAgreementNumber = new PropertyPath<String>(CombinationPostPaymentExtractGen.P_CONTRACT_ADD_AGREEMENT_NUMBER, this);
            return _contractAddAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getContractAddAgreementDate()
     */
        public PropertyPath<Date> contractAddAgreementDate()
        {
            if(_contractAddAgreementDate == null )
                _contractAddAgreementDate = new PropertyPath<Date>(CombinationPostPaymentExtractGen.P_CONTRACT_ADD_AGREEMENT_DATE, this);
            return _contractAddAgreementDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(CombinationPostPaymentExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(CombinationPostPaymentExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#isFreelance()
     */
        public PropertyPath<Boolean> freelance()
        {
            if(_freelance == null )
                _freelance = new PropertyPath<Boolean>(CombinationPostPaymentExtractGen.P_FREELANCE, this);
            return _freelance;
        }

    /**
     * @return Должность по совмещению созданная при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getCreateCombinationPost()
     */
        public CombinationPost.Path<CombinationPost> createCombinationPost()
        {
            if(_createCombinationPost == null )
                _createCombinationPost = new CombinationPost.Path<CombinationPost>(L_CREATE_COMBINATION_POST, this);
            return _createCombinationPost;
        }

    /**
     * @return Трудовой договор сотрудника созданный при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getCreateEmployeeContract()
     */
        public EmployeeLabourContract.Path<EmployeeLabourContract> createEmployeeContract()
        {
            if(_createEmployeeContract == null )
                _createEmployeeContract = new EmployeeLabourContract.Path<EmployeeLabourContract>(L_CREATE_EMPLOYEE_CONTRACT, this);
            return _createEmployeeContract;
        }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract#getCreateCollateralAgreement()
     */
        public ContractCollateralAgreement.Path<ContractCollateralAgreement> createCollateralAgreement()
        {
            if(_createCollateralAgreement == null )
                _createCollateralAgreement = new ContractCollateralAgreement.Path<ContractCollateralAgreement>(L_CREATE_COLLATERAL_AGREEMENT, this);
            return _createCollateralAgreement;
        }

        public Class getEntityClass()
        {
            return CombinationPostPaymentExtract.class;
        }

        public String getEntityName()
        {
            return "combinationPostPaymentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
