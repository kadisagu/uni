package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;
import ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь фактических частей отпуска в выписке «О переносе ежегодного отпуска» и самой выписки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferHolidayDateToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation";
    public static final String ENTITY_NAME = "transferHolidayDateToExtractRelation";
    public static final int VERSION_HASH = -1961778017;
    private static IEntityMeta ENTITY_META;

    public static final String L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT = "transferAnnualHolidayExtract";
    public static final String P_BEGIN_DAY = "beginDay";
    public static final String P_END_DAY = "endDay";
    public static final String P_DURATION_DAY = "durationDay";

    private TransferAnnualHolidayExtract _transferAnnualHolidayExtract;     // Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска
    private Date _beginDay;     // Факт. дата начала
    private Date _endDay;     // Факт. дата окончания
    private Integer _durationDay;     // Факт. длительность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     */
    @NotNull
    public TransferAnnualHolidayExtract getTransferAnnualHolidayExtract()
    {
        return _transferAnnualHolidayExtract;
    }

    /**
     * @param transferAnnualHolidayExtract Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     */
    public void setTransferAnnualHolidayExtract(TransferAnnualHolidayExtract transferAnnualHolidayExtract)
    {
        dirty(_transferAnnualHolidayExtract, transferAnnualHolidayExtract);
        _transferAnnualHolidayExtract = transferAnnualHolidayExtract;
    }

    /**
     * @return Факт. дата начала.
     */
    public Date getBeginDay()
    {
        return _beginDay;
    }

    /**
     * @param beginDay Факт. дата начала.
     */
    public void setBeginDay(Date beginDay)
    {
        dirty(_beginDay, beginDay);
        _beginDay = beginDay;
    }

    /**
     * @return Факт. дата окончания.
     */
    public Date getEndDay()
    {
        return _endDay;
    }

    /**
     * @param endDay Факт. дата окончания.
     */
    public void setEndDay(Date endDay)
    {
        dirty(_endDay, endDay);
        _endDay = endDay;
    }

    /**
     * @return Факт. длительность.
     */
    public Integer getDurationDay()
    {
        return _durationDay;
    }

    /**
     * @param durationDay Факт. длительность.
     */
    public void setDurationDay(Integer durationDay)
    {
        dirty(_durationDay, durationDay);
        _durationDay = durationDay;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TransferHolidayDateToExtractRelationGen)
        {
            setTransferAnnualHolidayExtract(((TransferHolidayDateToExtractRelation)another).getTransferAnnualHolidayExtract());
            setBeginDay(((TransferHolidayDateToExtractRelation)another).getBeginDay());
            setEndDay(((TransferHolidayDateToExtractRelation)another).getEndDay());
            setDurationDay(((TransferHolidayDateToExtractRelation)another).getDurationDay());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferHolidayDateToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferHolidayDateToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new TransferHolidayDateToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "transferAnnualHolidayExtract":
                    return obj.getTransferAnnualHolidayExtract();
                case "beginDay":
                    return obj.getBeginDay();
                case "endDay":
                    return obj.getEndDay();
                case "durationDay":
                    return obj.getDurationDay();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "transferAnnualHolidayExtract":
                    obj.setTransferAnnualHolidayExtract((TransferAnnualHolidayExtract) value);
                    return;
                case "beginDay":
                    obj.setBeginDay((Date) value);
                    return;
                case "endDay":
                    obj.setEndDay((Date) value);
                    return;
                case "durationDay":
                    obj.setDurationDay((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "transferAnnualHolidayExtract":
                        return true;
                case "beginDay":
                        return true;
                case "endDay":
                        return true;
                case "durationDay":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "transferAnnualHolidayExtract":
                    return true;
                case "beginDay":
                    return true;
                case "endDay":
                    return true;
                case "durationDay":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "transferAnnualHolidayExtract":
                    return TransferAnnualHolidayExtract.class;
                case "beginDay":
                    return Date.class;
                case "endDay":
                    return Date.class;
                case "durationDay":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferHolidayDateToExtractRelation> _dslPath = new Path<TransferHolidayDateToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferHolidayDateToExtractRelation");
    }
            

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation#getTransferAnnualHolidayExtract()
     */
    public static TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> transferAnnualHolidayExtract()
    {
        return _dslPath.transferAnnualHolidayExtract();
    }

    /**
     * @return Факт. дата начала.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation#getBeginDay()
     */
    public static PropertyPath<Date> beginDay()
    {
        return _dslPath.beginDay();
    }

    /**
     * @return Факт. дата окончания.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation#getEndDay()
     */
    public static PropertyPath<Date> endDay()
    {
        return _dslPath.endDay();
    }

    /**
     * @return Факт. длительность.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation#getDurationDay()
     */
    public static PropertyPath<Integer> durationDay()
    {
        return _dslPath.durationDay();
    }

    public static class Path<E extends TransferHolidayDateToExtractRelation> extends EntityPath<E>
    {
        private TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> _transferAnnualHolidayExtract;
        private PropertyPath<Date> _beginDay;
        private PropertyPath<Date> _endDay;
        private PropertyPath<Integer> _durationDay;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation#getTransferAnnualHolidayExtract()
     */
        public TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> transferAnnualHolidayExtract()
        {
            if(_transferAnnualHolidayExtract == null )
                _transferAnnualHolidayExtract = new TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract>(L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, this);
            return _transferAnnualHolidayExtract;
        }

    /**
     * @return Факт. дата начала.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation#getBeginDay()
     */
        public PropertyPath<Date> beginDay()
        {
            if(_beginDay == null )
                _beginDay = new PropertyPath<Date>(TransferHolidayDateToExtractRelationGen.P_BEGIN_DAY, this);
            return _beginDay;
        }

    /**
     * @return Факт. дата окончания.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation#getEndDay()
     */
        public PropertyPath<Date> endDay()
        {
            if(_endDay == null )
                _endDay = new PropertyPath<Date>(TransferHolidayDateToExtractRelationGen.P_END_DAY, this);
            return _endDay;
        }

    /**
     * @return Факт. длительность.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation#getDurationDay()
     */
        public PropertyPath<Integer> durationDay()
        {
            if(_durationDay == null )
                _durationDay = new PropertyPath<Integer>(TransferHolidayDateToExtractRelationGen.P_DURATION_DAY, this);
            return _durationDay;
        }

        public Class getEntityClass()
        {
            return TransferHolidayDateToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "transferHolidayDateToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
