/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e20.AddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.Date;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 06.12.2011
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<TransferHolidayDueDiseaseExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        model.setEmployeeHolidayModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", EmployeeHoliday.employeePost().s(), model.getEmployeePost()));
                builder.add(MQExpression.eq("b", EmployeeHoliday.holidayType().code().s(), UniempDefines.HOLIDAY_TYPE_ANNUAL));
                builder.addDescOrder("b", EmployeeHoliday.startDate().s());

                if (o != null)
                    builder.add(MQExpression.eq("b", EmployeeHoliday.id().s(), o));


                return new MQListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EmployeeHoliday employeeHoliday = (EmployeeHoliday) value;

//                StringBuilder result = new StringBuilder("Ежегодный отпуск с ");
//                result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeHoliday.getStartDate()));
//                result.append(" по ");
//                result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeHoliday.getFinishDate()));
//
//                if (employeeHoliday.getBeginDate() != null && employeeHoliday.getEndDate() != null)
//                {
//                    result.append(" за период с ");
//                    result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeHoliday.getBeginDate()));
//                    result.append(" по ");
//                    result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeHoliday.getEndDate()));
//                }

                return employeeHoliday.getFormattedNameBeginEndPeriodDateString();
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (model.getExtract().getTransferDaysAmount() > model.getExtract().getEmployeeHoliday().getDuration())
            errors.add("Количество дней переносимой части отпуска не может превышать длительность отпуска.", "transferDaysAmount");

        if (model.getExtract().getDateToTransfer() != null)
        {
            Date beginDate = model.getExtract().getDateToTransfer();
            Date endDate = UniempUtil.getEndDate(model.getEmployeePost().getWorkWeekDuration(), model.getExtract().getTransferDaysAmount(), model.getExtract().getDateToTransfer());

            MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", EmployeeHoliday.employeePost().s(), model.getEmployeePost()));
            builder.add(MQExpression.notEq("b", EmployeeHoliday.id().s(), model.getExtract().getEmployeeHoliday().getId()));
            for (EmployeeHoliday holiday : builder.<EmployeeHoliday>getResultList(getSession()))
            {
                if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                    errors.add("Перенесенная часть отпуска сотрудника пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                            "dateToTransfer");
                else {
                    if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                        errors.add("Перенесенная часть отпуска сотрудника пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                "dateToTransfer");
                }
            }
        }

        if (model.getExtract().getDateToTransfer() == null && (model.getExtract().getTransferToAnotherDay() == null || !model.getExtract().getTransferToAnotherDay()))
            errors.add("Необходимо указать дату, на которую переносится часть отпуска, либо указать «Перенести на другой срок».",
                    "dateToTransfer");

        if (model.getExtract().getTransferDaysAmount() < model.getExtract().getEmployeeHoliday().getDuration())
            if (model.getExtract().getDateToTransfer() != null)
            {
                Date endDate = UniempUtil.getEndDate(model.getEmployeePost().getWorkWeekDuration(), model.getExtract().getTransferDaysAmount() * -1, model.getExtract().getEmployeeHoliday().getFinishDate());
                if (model.getExtract().getDateToTransfer().equals(endDate) || CommonBaseDateUtil.isBefore(model.getExtract().getDateToTransfer(), endDate))
                    errors.add("Дата переноса указана некорректно.", "dateToTransfer");
            }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setStartHistory(model.getExtract().getEmployeeHoliday().getStartDate());
        model.getExtract().setFinishHistory(model.getExtract().getEmployeeHoliday().getFinishDate());
        model.getExtract().setBeginHistory(model.getExtract().getEmployeeHoliday().getBeginDate());
        model.getExtract().setEndHistory(model.getExtract().getEmployeeHoliday().getEndDate());

        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());
    }

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected TransferHolidayDueDiseaseExtract createNewInstance()
    {
        return new TransferHolidayDueDiseaseExtract();
    }
}