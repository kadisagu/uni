/**
 * $Id$
 */
package ru.tandemservice.moveemployee.dao;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.component.commons.IContractAndAssigmentExtract;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 07.11.2008
 */
public interface IMoveEmployeeDAO
{
    String MOVE_EMPLOYEE_DAO_BEAN_NAME = "moveEmployeeDao";

    List<EmployeeExtractType> getModularEmployeeExtractAsOrderTypeList();

    List<EmployeeExtractType> getModularEmployeePostExtractAsOrderTypeList();

    List<EmployeeExtractType> getModularEmployeeExtractIndividualTypeList();

    List<EmployeeExtractType> getModularEmployeePostExtractIndividualTypeList();

    List<EmployeeExtractType> getModularEmployeeExtractTypeList();

    List<EmployeeExtractType> getModularEmployeePostExtractTypeList();

    List<EmployeeExtractType> getSingleEmployeeExtractTypeList();

    List<EmployeeExtractType> getSingleEmployeePostExtractTypeList();

    List<EmployeeExtractType> getSingleExtractTypeList();

    boolean isEmployeeHasFormativeExtracts(EmployeePost post);

    boolean isEmployeePostHasNotFinishedExtracts(EmployeePost post);

    boolean isEmployeeHasFormativeExtracts(Employee employee);

    Integer getExtractIndex(Long extractId);

    List<EmployeeBonus> getExtractEmployeeBonusesList(AbstractEmployeeExtract extract);

    boolean isMoveAccessible(EmployeePost employeePost);

    byte[] getTemplate(EmployeeExtractType extractType, int templateIndex);

    void saveExtractText(AbstractEmployeeExtract extract, int textCode);

    void saveModularOrderText(EmployeeModularOrder order);

    void saveListOrderText(EmployeeListOrder order);

    /**
     * Сохраняет печатную форму Списочного приказа формируемого параграфами.
     */
    void saveListOrderText(ParagraphEmployeeListOrder order);

    void deleteListExtract(Long extractId);

    List<EmployeeExtractType> getModularExtractTypeList();

    List<EmpExtractToBasicRelation> getExtractBasicsList(ModularEmployeeExtract extract);

    List<EmpSingleExtractToBasicRelation> getSingleExtractBasicsList(SingleEmployeeExtract extract);

    SingleEmployeeExtract getExtractBySingleExtractOrder(EmployeeSingleExtractOrder order);

    List<EmpListExtractToBasicRelation> getListExtractBasicsList(ListEmployeeExtract extract);

    EmployeeListParagraph getFirstParagraph(EmployeeListOrder order);

    <T extends ListEmployeeExtract> List<T> getExtractList(EmployeeListOrder order);

    EmployeeExtractType getExtractTypeByOrderType(EmployeeExtractType orderType);

    List<EmployeeExtractType> getExtractTypeListByOrderType(EmployeeExtractType orderType);

    public List<EmployeeOrderBasics> getReasonToBasicsList(final EmployeeOrderReasons reason);

    public List<EmployeeExtractType> getReasonToDocumentTypeList(final EmployeeOrderReasons reason);

    public String getPaymentFormatString(String documentCode, Payment payment);

    /**
     * Проверяет нужно ли отображать приказ, как Формирующийся индивидуально.
     * @param order - проверяемый приказ
     * @return <tt><b>true</b></tt>, если в приказе один параграф и тип выписки помечен как Формируется индивидуально<p>
     * из настройки Правила формирования сборных приказов (CreationExtractAsOrderSettings), иначе <tt><b>false</b></tt>.
     */
    public boolean isIndividualExtractOrder(EmployeeModularOrder order);

    /**
     *  Возвращает единственную выписку сборного приказа.
     * @param order - приказ
     * @return выписку из приказа, если она единственная в приказе, иначе <tt><b>null</b></tt>
     */
    public ModularEmployeeExtract getOrdersModularEmployeeExtract(EmployeeModularOrder order);

    /**
     * Проверяет является ли выписка формирующейся индивидуально.
     * @param extract - выписка
     * @return <tt><b>true</b></tt>, если тип выписки помечен как формирующийся индивидуально<p>
     * из настройки Правила формирования сборных приказов (CreationExtractAsOrderSettings), иначе <tt><b>false</b></tt>.
     */
    public boolean isExtractIndividual(ModularEmployeeExtract extract);

    /**
     * @param extract
     * @return Возвращает список выплат помечанных в приказе "Об установлении выплат" на снятие.
     */
    public List<EmployeePayment> getRemovePaymentsListExtracts(PaymentAssigningExtract extract);

    /**
     * @return Возвращает список ставок созданных в выписке.
     */
    public List<FinancingSourceDetails> getExtractFinancingSourceDetails(AbstractEmployeeExtract extract);

    /**
     * @return Возвращает список релейшенов на выбранные ставоки Штатного расписания в приказе для отдельной доли ставки.
     */
    public List<FinancingSourceDetailsToAllocItem> getFinSrcDetToAllocItemRelation(FinancingSourceDetails financingSourceDetails);

    /**
     * @return Возвращает список релейшенов на выбранные ставоки Штатного расписания в приказе для списка долей ставок.
     */
    public List<FinancingSourceDetailsToAllocItem> getFinSrcDetToAllocItemRelation(List<FinancingSourceDetails> financingSourceDetailsList);

    /**
     * @return Возвращает список частей Предостовляемого отпуска, указанного в приказе Об отзывае из отпуска.
     */
    public List<ProvideDateToExtractRelation> getProvideHolidayFromRevocationHolidayExtract(RevocationFromHolidayExtract extract);

    /**
     * @return Возвращает список выбранных частей отпуска в приказе О переносе ежегодного отпуска.
     */
    public List<PeriodPartToExtractRelation> getPeriodPartFromExtractRelation(TransferAnnualHolidayExtract extract);

    /**
     * @return Возвращает список дат на которые переносится отпуск в приказе О переносе ежегодного отпуска.
     */
    public List<TransferHolidayDateToExtractRelation> getTransferHolidayDateFromExtractRelation(TransferAnnualHolidayExtract extract);

    /**
     * @return Возвращает список частей нового отпуска в приказе О переносе ежегодного отпуска.
     */
    public List<NewVacationScheduleToExtractRelation> getNewVacationScheduleFromExtractRelation(TransferAnnualHolidayExtract extract);

    /**
     * @return Возвращает список Тип поощрений, выбранных в выписке О поощрении работника.
     */
    public List<EncouragementType> getEncouragementTypeFromExtractList(EmployeeEncouragementSExtract extract);

    /**
     * @return Возвращает мапу с Типом поощрения и его печатной формой (из настройки).<p/>
     * Если печатная форма в настройке не указана, значение в мапе <tt>null</tt>
     */
    public Map<EncouragementType, String> getEncouragementTypePrintFormat(List<EncouragementType> list);

    /**
     * @return Возвращает список релейшенов на созданные отпуска в выписке Об отпуске.
     */
    public List<HolidayInEmpHldyExtractRelation> getHolidayInEmpHldyExtractRelation(EmployeeHolidayExtract extract);

    /**
     * @return Возвращает список релейшенов на созданные отпуска в выписке Изменение приказа об отпуске.
     */
    public List<HolidayInEmployeeExtractRelation> getHolidayInEmployeeExtractRelation(AbstractEmployeeExtract extract);

    /**
     * @return Возвращает список релейшенов в выписке на ставки должности по совмещению, созданных в выписке.
     */
    public List<CombinationPostStaffRateExtractRelation> getCombinationPostStaffRateExtractRelationList(AbstractEmployeeExtract extract);

    /**
     * @return Возвращает списко релейшенов в выписке на выбранные элементы Штатной расстановки.
     */
    public List<CombinationPostStaffRateExtAllocItemRelation> getCombinationPostStaffRateExtAllocItemRelationList(CombinationPostStaffRateExtractRelation relation);

    /**
     * Исключает выплаты, которые созданы в рамках указанной выписки.
     * @param paymentList выплаты, которые учитываются
     * @param beginDate дата актуальности выплат
     * @return Возвращает список выплат, актуальных в момент проведения выписки, в которой назначаются выплаты.<p/>
     */
    public List<EmployeePayment> getCommitExtractActualEmployeePaymentList(AbstractEmployeeExtract extract, List<Payment> paymentList, Date beginDate);

    //обновляет источники финансирования при коммите
    void updateStaffListAllocItemChanges(AbstractEmployeeExtract extract, EmployeePost post, List<StaffListAllocationItem> allocItemList);

    //откатывает источники финансирования
    @Transactional
    void rollbackStaffListAllocItemChanges(AbstractEmployeeExtract extract);

    void createOrUpdateContractAndAgreement(IContractAndAssigmentExtract extract, String comment);
}