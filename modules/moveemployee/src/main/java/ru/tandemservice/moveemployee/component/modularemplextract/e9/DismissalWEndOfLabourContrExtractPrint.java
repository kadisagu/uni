/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e9;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.entity.DismissalWEndOfLabourContrExtract;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 14.01.2009
 */
public class DismissalWEndOfLabourContrExtractPrint implements IPrintFormCreator<DismissalWEndOfLabourContrExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, DismissalWEndOfLabourContrExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("terminationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getTerminationDate()));
        modifier.put("terminationDateMonthStr", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getTerminationDate()));
        modifier.put("holidayDaysAmount", String.valueOf(extract.getHolidayDaysAmount()));

        StringBuilder employmentPeriod = new StringBuilder("c ");
        EmployeeLabourContract labourContract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());
        if (null != labourContract)
        {
            employmentPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(labourContract.getBeginDate()));
            employmentPeriod.append(" по ");

            if (null != labourContract.getEndDate())
                employmentPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(labourContract.getEndDate()));
            else
                employmentPeriod.append("__________");
        }
        else
            employmentPeriod.append("__________ по __________");
        modifier.put("employmentPeriod", employmentPeriod.toString());

        modifier.modify(document);
        return document;
    }
}