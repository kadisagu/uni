/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e5.Pub;

import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 23.09.2009
 */
public class DAO extends SingleEmployeeExtractPubDAO<EmployeeHolidaySExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setStaffRateStr(CommonEmployeeExtractUtil.getExtractFinancingSourceDetailsString(model.getExtract(), getSession(), false, null));

        if (model.getExtract().getVacationScheduleItem() != null)
        {
            model.setVacationScheduleStr(model.getExtract().getVacationSchedule().getSimpleTitleWithYear());
            model.setVacationScheduleItemStr(model.getExtract().getVacationScheduleItem().getPlanedVacationTitle());
        }
    }
}