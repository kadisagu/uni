/**
 *$Id:$
 */
package ru.tandemservice.moveemployee.base.bo.MoveEmployeeSettingsPaymentStringFormatDocument.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 19.10.12
 */
public class PaymentStringFormatDocumentSettingsDAO extends UniBaseDao implements IPaymentStringFormatDocumentSettingsDAO
{

    @Override
    public void doUpdateSettingsEntity()
    {
        List<EmployeeExtractType> extractTypeList = getCatalogItemList(EmployeeExtractType.class);

        Map<String, PaymentStringFormatDocumentSettings> settingsMap = new HashMap<>();
        for (PaymentStringFormatDocumentSettings settings : getList(PaymentStringFormatDocumentSettings.class)) {
            settingsMap.put(settings.getCode(), settings);
        }

        for (EmployeeExtractType type : extractTypeList) {
            doGetSettings(type, settingsMap);
        }
    }

    private PaymentStringFormatDocumentSettings doGetSettings(EmployeeExtractType type, Map<String, PaymentStringFormatDocumentSettings> settingsMap) {
        PaymentStringFormatDocumentSettings settings = settingsMap.get(type.getCode());
        if (null != settings) return settings;

        settings = new PaymentStringFormatDocumentSettings();
        settingsMap.put(type.getCode(), settings);

        settings.setTitle(type.getTitle());
        settings.setCode(type.getCode());
        settings.setActive(false);
        if (type.getParent() != null) {
            PaymentStringFormatDocumentSettings parent = doGetSettings(type.getParent(), settingsMap);
            settings.setParent(parent);
        }

        save(settings);

        return settings;
    }

    @Override
    public void doChangeActive(PaymentStringFormatDocumentSettings entity)
    {
        entity.setActive(!entity.isActive());

        if (entity.isActive())
        {
            PaymentStringFormatDocumentSettings parent = entity.getParent();
            while (parent != null)
            {
                parent.setActive(true);

                update(parent);

                parent = parent.getParent();
            }
        }
        else
        {
            for (PaymentStringFormatDocumentSettings child : getChildElementList(entity))
            {
                child.setActive(false);

                update(child);
            }
        }

        update(entity);
    }

    protected List<PaymentStringFormatDocumentSettings> getChildElementList(PaymentStringFormatDocumentSettings element)
    {
        List<PaymentStringFormatDocumentSettings> resultList = new ArrayList<>();

        List<PaymentStringFormatDocumentSettings> list = new DQLSelectBuilder().fromEntity(PaymentStringFormatDocumentSettings.class, "b")
                .where(eq(property(PaymentStringFormatDocumentSettings.parent().id().fromAlias("b")), value(element.getId())))
                .createStatement(getSession()).list();

        resultList.addAll(list);

        for (PaymentStringFormatDocumentSettings settings : list)
            resultList.addAll(getChildElementList(settings));

        return resultList;
    }
}
