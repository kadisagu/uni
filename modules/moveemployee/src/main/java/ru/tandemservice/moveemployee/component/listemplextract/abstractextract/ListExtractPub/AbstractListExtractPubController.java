/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
public class AbstractListExtractPubController<T extends ListEmployeeExtract, Model extends AbstractListExtractPubModel<T>, IDAO extends IAbstractListExtractPubDAO<T, Model>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
        
        model.setAttributesPage(getClass().getPackage().getName() + ".Attributes");
    }

    public void onClickDelete(IBusinessComponent component)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().deleteListExtract(getModel(component).getExtractId());
        deactivate(component);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        Model model = getModel(component);
        int extractIndex = model.getExtract().getType().getIndex();
        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListExtractAddEditComponent(extractIndex), new ParametersMap()
                .add("extractId", model.getExtract().getId())));
    }
}