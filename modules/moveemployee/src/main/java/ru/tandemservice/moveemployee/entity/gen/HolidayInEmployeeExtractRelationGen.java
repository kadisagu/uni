package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь созданных отпусков в выписках по кадрам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class HolidayInEmployeeExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation";
    public static final String ENTITY_NAME = "holidayInEmployeeExtractRelation";
    public static final int VERSION_HASH = 1716081744;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_HOLIDAY_EXTRACT = "employeeHolidayExtract";
    public static final String L_HOLIDAY_TYPE = "holidayType";
    public static final String P_BEGIN_PERIOD = "beginPeriod";
    public static final String P_END_PERIOD = "endPeriod";
    public static final String P_START_HOLIDAY = "startHoliday";
    public static final String P_FINISH_HOLIDAY = "finishHoliday";
    public static final String P_DURATION_HOLIDAY = "durationHoliday";
    public static final String P_MAIN_ROW_ID = "mainRowId";
    public static final String P_ROW_ID = "rowId";
    public static final String P_ROW_TYPE = "rowType";

    private AbstractEmployeeExtract _employeeHolidayExtract;     // Выписка по кадрам
    private HolidayType _holidayType;     // Вид отпуска
    private Date _beginPeriod;     // Дата начала периода
    private Date _endPeriod;     // Дата окончания периода
    private Date _startHoliday;     // Дата начала отпуска
    private Date _finishHoliday;     // Дата окончания отпуска
    private Integer _durationHoliday;     // Длительность отпуска
    private long _mainRowId;     // Id главной строки
    private long _rowId;     // Id подстроки
    private String _rowType;     // Id подстроки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка по кадрам. Свойство не может быть null.
     */
    @NotNull
    public AbstractEmployeeExtract getEmployeeHolidayExtract()
    {
        return _employeeHolidayExtract;
    }

    /**
     * @param employeeHolidayExtract Выписка по кадрам. Свойство не может быть null.
     */
    public void setEmployeeHolidayExtract(AbstractEmployeeExtract employeeHolidayExtract)
    {
        dirty(_employeeHolidayExtract, employeeHolidayExtract);
        _employeeHolidayExtract = employeeHolidayExtract;
    }

    /**
     * @return Вид отпуска.
     */
    public HolidayType getHolidayType()
    {
        return _holidayType;
    }

    /**
     * @param holidayType Вид отпуска.
     */
    public void setHolidayType(HolidayType holidayType)
    {
        dirty(_holidayType, holidayType);
        _holidayType = holidayType;
    }

    /**
     * @return Дата начала периода.
     */
    public Date getBeginPeriod()
    {
        return _beginPeriod;
    }

    /**
     * @param beginPeriod Дата начала периода.
     */
    public void setBeginPeriod(Date beginPeriod)
    {
        dirty(_beginPeriod, beginPeriod);
        _beginPeriod = beginPeriod;
    }

    /**
     * @return Дата окончания периода.
     */
    public Date getEndPeriod()
    {
        return _endPeriod;
    }

    /**
     * @param endPeriod Дата окончания периода.
     */
    public void setEndPeriod(Date endPeriod)
    {
        dirty(_endPeriod, endPeriod);
        _endPeriod = endPeriod;
    }

    /**
     * @return Дата начала отпуска.
     */
    public Date getStartHoliday()
    {
        return _startHoliday;
    }

    /**
     * @param startHoliday Дата начала отпуска.
     */
    public void setStartHoliday(Date startHoliday)
    {
        dirty(_startHoliday, startHoliday);
        _startHoliday = startHoliday;
    }

    /**
     * @return Дата окончания отпуска.
     */
    public Date getFinishHoliday()
    {
        return _finishHoliday;
    }

    /**
     * @param finishHoliday Дата окончания отпуска.
     */
    public void setFinishHoliday(Date finishHoliday)
    {
        dirty(_finishHoliday, finishHoliday);
        _finishHoliday = finishHoliday;
    }

    /**
     * @return Длительность отпуска.
     */
    public Integer getDurationHoliday()
    {
        return _durationHoliday;
    }

    /**
     * @param durationHoliday Длительность отпуска.
     */
    public void setDurationHoliday(Integer durationHoliday)
    {
        dirty(_durationHoliday, durationHoliday);
        _durationHoliday = durationHoliday;
    }

    /**
     * @return Id главной строки. Свойство не может быть null.
     */
    @NotNull
    public long getMainRowId()
    {
        return _mainRowId;
    }

    /**
     * @param mainRowId Id главной строки. Свойство не может быть null.
     */
    public void setMainRowId(long mainRowId)
    {
        dirty(_mainRowId, mainRowId);
        _mainRowId = mainRowId;
    }

    /**
     * @return Id подстроки. Свойство не может быть null.
     */
    @NotNull
    public long getRowId()
    {
        return _rowId;
    }

    /**
     * @param rowId Id подстроки. Свойство не может быть null.
     */
    public void setRowId(long rowId)
    {
        dirty(_rowId, rowId);
        _rowId = rowId;
    }

    /**
     * @return Id подстроки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRowType()
    {
        return _rowType;
    }

    /**
     * @param rowType Id подстроки. Свойство не может быть null.
     */
    public void setRowType(String rowType)
    {
        dirty(_rowType, rowType);
        _rowType = rowType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof HolidayInEmployeeExtractRelationGen)
        {
            setEmployeeHolidayExtract(((HolidayInEmployeeExtractRelation)another).getEmployeeHolidayExtract());
            setHolidayType(((HolidayInEmployeeExtractRelation)another).getHolidayType());
            setBeginPeriod(((HolidayInEmployeeExtractRelation)another).getBeginPeriod());
            setEndPeriod(((HolidayInEmployeeExtractRelation)another).getEndPeriod());
            setStartHoliday(((HolidayInEmployeeExtractRelation)another).getStartHoliday());
            setFinishHoliday(((HolidayInEmployeeExtractRelation)another).getFinishHoliday());
            setDurationHoliday(((HolidayInEmployeeExtractRelation)another).getDurationHoliday());
            setMainRowId(((HolidayInEmployeeExtractRelation)another).getMainRowId());
            setRowId(((HolidayInEmployeeExtractRelation)another).getRowId());
            setRowType(((HolidayInEmployeeExtractRelation)another).getRowType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends HolidayInEmployeeExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) HolidayInEmployeeExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new HolidayInEmployeeExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeHolidayExtract":
                    return obj.getEmployeeHolidayExtract();
                case "holidayType":
                    return obj.getHolidayType();
                case "beginPeriod":
                    return obj.getBeginPeriod();
                case "endPeriod":
                    return obj.getEndPeriod();
                case "startHoliday":
                    return obj.getStartHoliday();
                case "finishHoliday":
                    return obj.getFinishHoliday();
                case "durationHoliday":
                    return obj.getDurationHoliday();
                case "mainRowId":
                    return obj.getMainRowId();
                case "rowId":
                    return obj.getRowId();
                case "rowType":
                    return obj.getRowType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeHolidayExtract":
                    obj.setEmployeeHolidayExtract((AbstractEmployeeExtract) value);
                    return;
                case "holidayType":
                    obj.setHolidayType((HolidayType) value);
                    return;
                case "beginPeriod":
                    obj.setBeginPeriod((Date) value);
                    return;
                case "endPeriod":
                    obj.setEndPeriod((Date) value);
                    return;
                case "startHoliday":
                    obj.setStartHoliday((Date) value);
                    return;
                case "finishHoliday":
                    obj.setFinishHoliday((Date) value);
                    return;
                case "durationHoliday":
                    obj.setDurationHoliday((Integer) value);
                    return;
                case "mainRowId":
                    obj.setMainRowId((Long) value);
                    return;
                case "rowId":
                    obj.setRowId((Long) value);
                    return;
                case "rowType":
                    obj.setRowType((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeHolidayExtract":
                        return true;
                case "holidayType":
                        return true;
                case "beginPeriod":
                        return true;
                case "endPeriod":
                        return true;
                case "startHoliday":
                        return true;
                case "finishHoliday":
                        return true;
                case "durationHoliday":
                        return true;
                case "mainRowId":
                        return true;
                case "rowId":
                        return true;
                case "rowType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeHolidayExtract":
                    return true;
                case "holidayType":
                    return true;
                case "beginPeriod":
                    return true;
                case "endPeriod":
                    return true;
                case "startHoliday":
                    return true;
                case "finishHoliday":
                    return true;
                case "durationHoliday":
                    return true;
                case "mainRowId":
                    return true;
                case "rowId":
                    return true;
                case "rowType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeHolidayExtract":
                    return AbstractEmployeeExtract.class;
                case "holidayType":
                    return HolidayType.class;
                case "beginPeriod":
                    return Date.class;
                case "endPeriod":
                    return Date.class;
                case "startHoliday":
                    return Date.class;
                case "finishHoliday":
                    return Date.class;
                case "durationHoliday":
                    return Integer.class;
                case "mainRowId":
                    return Long.class;
                case "rowId":
                    return Long.class;
                case "rowType":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<HolidayInEmployeeExtractRelation> _dslPath = new Path<HolidayInEmployeeExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "HolidayInEmployeeExtractRelation");
    }
            

    /**
     * @return Выписка по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getEmployeeHolidayExtract()
     */
    public static AbstractEmployeeExtract.Path<AbstractEmployeeExtract> employeeHolidayExtract()
    {
        return _dslPath.employeeHolidayExtract();
    }

    /**
     * @return Вид отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getHolidayType()
     */
    public static HolidayType.Path<HolidayType> holidayType()
    {
        return _dslPath.holidayType();
    }

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getBeginPeriod()
     */
    public static PropertyPath<Date> beginPeriod()
    {
        return _dslPath.beginPeriod();
    }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getEndPeriod()
     */
    public static PropertyPath<Date> endPeriod()
    {
        return _dslPath.endPeriod();
    }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getStartHoliday()
     */
    public static PropertyPath<Date> startHoliday()
    {
        return _dslPath.startHoliday();
    }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getFinishHoliday()
     */
    public static PropertyPath<Date> finishHoliday()
    {
        return _dslPath.finishHoliday();
    }

    /**
     * @return Длительность отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getDurationHoliday()
     */
    public static PropertyPath<Integer> durationHoliday()
    {
        return _dslPath.durationHoliday();
    }

    /**
     * @return Id главной строки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getMainRowId()
     */
    public static PropertyPath<Long> mainRowId()
    {
        return _dslPath.mainRowId();
    }

    /**
     * @return Id подстроки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getRowId()
     */
    public static PropertyPath<Long> rowId()
    {
        return _dslPath.rowId();
    }

    /**
     * @return Id подстроки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getRowType()
     */
    public static PropertyPath<String> rowType()
    {
        return _dslPath.rowType();
    }

    public static class Path<E extends HolidayInEmployeeExtractRelation> extends EntityPath<E>
    {
        private AbstractEmployeeExtract.Path<AbstractEmployeeExtract> _employeeHolidayExtract;
        private HolidayType.Path<HolidayType> _holidayType;
        private PropertyPath<Date> _beginPeriod;
        private PropertyPath<Date> _endPeriod;
        private PropertyPath<Date> _startHoliday;
        private PropertyPath<Date> _finishHoliday;
        private PropertyPath<Integer> _durationHoliday;
        private PropertyPath<Long> _mainRowId;
        private PropertyPath<Long> _rowId;
        private PropertyPath<String> _rowType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getEmployeeHolidayExtract()
     */
        public AbstractEmployeeExtract.Path<AbstractEmployeeExtract> employeeHolidayExtract()
        {
            if(_employeeHolidayExtract == null )
                _employeeHolidayExtract = new AbstractEmployeeExtract.Path<AbstractEmployeeExtract>(L_EMPLOYEE_HOLIDAY_EXTRACT, this);
            return _employeeHolidayExtract;
        }

    /**
     * @return Вид отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getHolidayType()
     */
        public HolidayType.Path<HolidayType> holidayType()
        {
            if(_holidayType == null )
                _holidayType = new HolidayType.Path<HolidayType>(L_HOLIDAY_TYPE, this);
            return _holidayType;
        }

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getBeginPeriod()
     */
        public PropertyPath<Date> beginPeriod()
        {
            if(_beginPeriod == null )
                _beginPeriod = new PropertyPath<Date>(HolidayInEmployeeExtractRelationGen.P_BEGIN_PERIOD, this);
            return _beginPeriod;
        }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getEndPeriod()
     */
        public PropertyPath<Date> endPeriod()
        {
            if(_endPeriod == null )
                _endPeriod = new PropertyPath<Date>(HolidayInEmployeeExtractRelationGen.P_END_PERIOD, this);
            return _endPeriod;
        }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getStartHoliday()
     */
        public PropertyPath<Date> startHoliday()
        {
            if(_startHoliday == null )
                _startHoliday = new PropertyPath<Date>(HolidayInEmployeeExtractRelationGen.P_START_HOLIDAY, this);
            return _startHoliday;
        }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getFinishHoliday()
     */
        public PropertyPath<Date> finishHoliday()
        {
            if(_finishHoliday == null )
                _finishHoliday = new PropertyPath<Date>(HolidayInEmployeeExtractRelationGen.P_FINISH_HOLIDAY, this);
            return _finishHoliday;
        }

    /**
     * @return Длительность отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getDurationHoliday()
     */
        public PropertyPath<Integer> durationHoliday()
        {
            if(_durationHoliday == null )
                _durationHoliday = new PropertyPath<Integer>(HolidayInEmployeeExtractRelationGen.P_DURATION_HOLIDAY, this);
            return _durationHoliday;
        }

    /**
     * @return Id главной строки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getMainRowId()
     */
        public PropertyPath<Long> mainRowId()
        {
            if(_mainRowId == null )
                _mainRowId = new PropertyPath<Long>(HolidayInEmployeeExtractRelationGen.P_MAIN_ROW_ID, this);
            return _mainRowId;
        }

    /**
     * @return Id подстроки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getRowId()
     */
        public PropertyPath<Long> rowId()
        {
            if(_rowId == null )
                _rowId = new PropertyPath<Long>(HolidayInEmployeeExtractRelationGen.P_ROW_ID, this);
            return _rowId;
        }

    /**
     * @return Id подстроки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation#getRowType()
     */
        public PropertyPath<String> rowType()
        {
            if(_rowType == null )
                _rowType = new PropertyPath<String>(HolidayInEmployeeExtractRelationGen.P_ROW_TYPE, this);
            return _rowType;
        }

        public Class getEntityClass()
        {
            return HolidayInEmployeeExtractRelation.class;
        }

        public String getEntityName()
        {
            return "holidayInEmployeeExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
