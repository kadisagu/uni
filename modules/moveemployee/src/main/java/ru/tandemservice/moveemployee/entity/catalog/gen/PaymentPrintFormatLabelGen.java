package ru.tandemservice.moveemployee.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Метки данных для строкового и табличного представления доплаты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PaymentPrintFormatLabelGen extends EntityBase
 implements INaturalIdentifiable<PaymentPrintFormatLabelGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel";
    public static final String ENTITY_NAME = "paymentPrintFormatLabel";
    public static final int VERSION_HASH = 1047974724;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_LABEL = "label";
    public static final String P_PROPERTY_PATH = "propertyPath";
    public static final String P_EMPLOYEE_PAYMENT_PROPERTY_PATH = "employeePaymentPropertyPath";
    public static final String P_PROPERTY_FORMAT = "propertyFormat";
    public static final String P_PROPERTY_EXAMPLE = "propertyExample";
    public static final String P_NUMBER_DEPENDENT_DATA_CASE = "numberDependentDataCase";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _label;     // Метка
    private String _propertyPath;     // Путь к свойству сущности Выплата в выписке, которое должно отображаться вместо метки
    private String _employeePaymentPropertyPath;     // Путь к свойству сущности Выплата Сотрудника, которое должно отображаться вместо метки
    private String _propertyFormat;     // Формат вывода свойства
    private String _propertyExample;     // Пример вывода свойства
    private String _numberDependentDataCase;     // Падеж для значений, зависящих от чисел
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Метка. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLabel()
    {
        return _label;
    }

    /**
     * @param label Метка. Свойство не может быть null.
     */
    public void setLabel(String label)
    {
        dirty(_label, label);
        _label = label;
    }

    /**
     * @return Путь к свойству сущности Выплата в выписке, которое должно отображаться вместо метки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPropertyPath()
    {
        return _propertyPath;
    }

    /**
     * @param propertyPath Путь к свойству сущности Выплата в выписке, которое должно отображаться вместо метки. Свойство не может быть null.
     */
    public void setPropertyPath(String propertyPath)
    {
        dirty(_propertyPath, propertyPath);
        _propertyPath = propertyPath;
    }

    /**
     * @return Путь к свойству сущности Выплата Сотрудника, которое должно отображаться вместо метки.
     */
    @Length(max=255)
    public String getEmployeePaymentPropertyPath()
    {
        return _employeePaymentPropertyPath;
    }

    /**
     * @param employeePaymentPropertyPath Путь к свойству сущности Выплата Сотрудника, которое должно отображаться вместо метки.
     */
    public void setEmployeePaymentPropertyPath(String employeePaymentPropertyPath)
    {
        dirty(_employeePaymentPropertyPath, employeePaymentPropertyPath);
        _employeePaymentPropertyPath = employeePaymentPropertyPath;
    }

    /**
     * @return Формат вывода свойства.
     */
    @Length(max=255)
    public String getPropertyFormat()
    {
        return _propertyFormat;
    }

    /**
     * @param propertyFormat Формат вывода свойства.
     */
    public void setPropertyFormat(String propertyFormat)
    {
        dirty(_propertyFormat, propertyFormat);
        _propertyFormat = propertyFormat;
    }

    /**
     * @return Пример вывода свойства.
     */
    @Length(max=255)
    public String getPropertyExample()
    {
        return _propertyExample;
    }

    /**
     * @param propertyExample Пример вывода свойства.
     */
    public void setPropertyExample(String propertyExample)
    {
        dirty(_propertyExample, propertyExample);
        _propertyExample = propertyExample;
    }

    /**
     * @return Падеж для значений, зависящих от чисел.
     */
    @Length(max=255)
    public String getNumberDependentDataCase()
    {
        return _numberDependentDataCase;
    }

    /**
     * @param numberDependentDataCase Падеж для значений, зависящих от чисел.
     */
    public void setNumberDependentDataCase(String numberDependentDataCase)
    {
        dirty(_numberDependentDataCase, numberDependentDataCase);
        _numberDependentDataCase = numberDependentDataCase;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PaymentPrintFormatLabelGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((PaymentPrintFormatLabel)another).getCode());
            }
            setLabel(((PaymentPrintFormatLabel)another).getLabel());
            setPropertyPath(((PaymentPrintFormatLabel)another).getPropertyPath());
            setEmployeePaymentPropertyPath(((PaymentPrintFormatLabel)another).getEmployeePaymentPropertyPath());
            setPropertyFormat(((PaymentPrintFormatLabel)another).getPropertyFormat());
            setPropertyExample(((PaymentPrintFormatLabel)another).getPropertyExample());
            setNumberDependentDataCase(((PaymentPrintFormatLabel)another).getNumberDependentDataCase());
            setTitle(((PaymentPrintFormatLabel)another).getTitle());
        }
    }

    public INaturalId<PaymentPrintFormatLabelGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<PaymentPrintFormatLabelGen>
    {
        private static final String PROXY_NAME = "PaymentPrintFormatLabelNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PaymentPrintFormatLabelGen.NaturalId) ) return false;

            PaymentPrintFormatLabelGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PaymentPrintFormatLabelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PaymentPrintFormatLabel.class;
        }

        public T newInstance()
        {
            return (T) new PaymentPrintFormatLabel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "label":
                    return obj.getLabel();
                case "propertyPath":
                    return obj.getPropertyPath();
                case "employeePaymentPropertyPath":
                    return obj.getEmployeePaymentPropertyPath();
                case "propertyFormat":
                    return obj.getPropertyFormat();
                case "propertyExample":
                    return obj.getPropertyExample();
                case "numberDependentDataCase":
                    return obj.getNumberDependentDataCase();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "label":
                    obj.setLabel((String) value);
                    return;
                case "propertyPath":
                    obj.setPropertyPath((String) value);
                    return;
                case "employeePaymentPropertyPath":
                    obj.setEmployeePaymentPropertyPath((String) value);
                    return;
                case "propertyFormat":
                    obj.setPropertyFormat((String) value);
                    return;
                case "propertyExample":
                    obj.setPropertyExample((String) value);
                    return;
                case "numberDependentDataCase":
                    obj.setNumberDependentDataCase((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "label":
                        return true;
                case "propertyPath":
                        return true;
                case "employeePaymentPropertyPath":
                        return true;
                case "propertyFormat":
                        return true;
                case "propertyExample":
                        return true;
                case "numberDependentDataCase":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "label":
                    return true;
                case "propertyPath":
                    return true;
                case "employeePaymentPropertyPath":
                    return true;
                case "propertyFormat":
                    return true;
                case "propertyExample":
                    return true;
                case "numberDependentDataCase":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "label":
                    return String.class;
                case "propertyPath":
                    return String.class;
                case "employeePaymentPropertyPath":
                    return String.class;
                case "propertyFormat":
                    return String.class;
                case "propertyExample":
                    return String.class;
                case "numberDependentDataCase":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PaymentPrintFormatLabel> _dslPath = new Path<PaymentPrintFormatLabel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PaymentPrintFormatLabel");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Метка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getLabel()
     */
    public static PropertyPath<String> label()
    {
        return _dslPath.label();
    }

    /**
     * @return Путь к свойству сущности Выплата в выписке, которое должно отображаться вместо метки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getPropertyPath()
     */
    public static PropertyPath<String> propertyPath()
    {
        return _dslPath.propertyPath();
    }

    /**
     * @return Путь к свойству сущности Выплата Сотрудника, которое должно отображаться вместо метки.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getEmployeePaymentPropertyPath()
     */
    public static PropertyPath<String> employeePaymentPropertyPath()
    {
        return _dslPath.employeePaymentPropertyPath();
    }

    /**
     * @return Формат вывода свойства.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getPropertyFormat()
     */
    public static PropertyPath<String> propertyFormat()
    {
        return _dslPath.propertyFormat();
    }

    /**
     * @return Пример вывода свойства.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getPropertyExample()
     */
    public static PropertyPath<String> propertyExample()
    {
        return _dslPath.propertyExample();
    }

    /**
     * @return Падеж для значений, зависящих от чисел.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getNumberDependentDataCase()
     */
    public static PropertyPath<String> numberDependentDataCase()
    {
        return _dslPath.numberDependentDataCase();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends PaymentPrintFormatLabel> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _label;
        private PropertyPath<String> _propertyPath;
        private PropertyPath<String> _employeePaymentPropertyPath;
        private PropertyPath<String> _propertyFormat;
        private PropertyPath<String> _propertyExample;
        private PropertyPath<String> _numberDependentDataCase;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(PaymentPrintFormatLabelGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Метка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getLabel()
     */
        public PropertyPath<String> label()
        {
            if(_label == null )
                _label = new PropertyPath<String>(PaymentPrintFormatLabelGen.P_LABEL, this);
            return _label;
        }

    /**
     * @return Путь к свойству сущности Выплата в выписке, которое должно отображаться вместо метки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getPropertyPath()
     */
        public PropertyPath<String> propertyPath()
        {
            if(_propertyPath == null )
                _propertyPath = new PropertyPath<String>(PaymentPrintFormatLabelGen.P_PROPERTY_PATH, this);
            return _propertyPath;
        }

    /**
     * @return Путь к свойству сущности Выплата Сотрудника, которое должно отображаться вместо метки.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getEmployeePaymentPropertyPath()
     */
        public PropertyPath<String> employeePaymentPropertyPath()
        {
            if(_employeePaymentPropertyPath == null )
                _employeePaymentPropertyPath = new PropertyPath<String>(PaymentPrintFormatLabelGen.P_EMPLOYEE_PAYMENT_PROPERTY_PATH, this);
            return _employeePaymentPropertyPath;
        }

    /**
     * @return Формат вывода свойства.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getPropertyFormat()
     */
        public PropertyPath<String> propertyFormat()
        {
            if(_propertyFormat == null )
                _propertyFormat = new PropertyPath<String>(PaymentPrintFormatLabelGen.P_PROPERTY_FORMAT, this);
            return _propertyFormat;
        }

    /**
     * @return Пример вывода свойства.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getPropertyExample()
     */
        public PropertyPath<String> propertyExample()
        {
            if(_propertyExample == null )
                _propertyExample = new PropertyPath<String>(PaymentPrintFormatLabelGen.P_PROPERTY_EXAMPLE, this);
            return _propertyExample;
        }

    /**
     * @return Падеж для значений, зависящих от чисел.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getNumberDependentDataCase()
     */
        public PropertyPath<String> numberDependentDataCase()
        {
            if(_numberDependentDataCase == null )
                _numberDependentDataCase = new PropertyPath<String>(PaymentPrintFormatLabelGen.P_NUMBER_DEPENDENT_DATA_CASE, this);
            return _numberDependentDataCase;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(PaymentPrintFormatLabelGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return PaymentPrintFormatLabel.class;
        }

        public String getEntityName()
        {
            return "paymentPrintFormatLabel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
