/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.catalog.employeeExtractType.EmployeeExtractTypePub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author AutoGenerator
 * Created on 18.11.2008
 */
public class DAO extends DefaultCatalogPubDAO<EmployeeExtractType, Model> implements IDAO
{
    @Override
    protected void prepareListItemDataSource(Model model)
    {
        List<EmployeeExtractType> result;
        String title = model.getSettings().get("title");

        if (StringUtils.isNotEmpty(title))
        {
            result = new ArrayList<>();

            DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(EmployeeExtractType.class, "ci").column("ci")
                    .where(DQLExpressions.eq(DQLExpressions.property(EmployeeExtractType.active().fromAlias("ci")), DQLExpressions.value(Boolean.TRUE)))
                    .where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EmployeeExtractType.title().fromAlias("ci"))), DQLExpressions.value(CoreStringUtils.escapeLike(title))));
            List<EmployeeExtractType> preResult = b.createStatement(getSession()).list();

            for(EmployeeExtractType extrType : preResult)
            {
                EmployeeExtractType parent = extrType.getParent();
                while(null != parent)
                {
                    result.add(parent);
                    parent = parent.getParent();
                }
            }
            result.addAll(preResult);
        }
        else
        {
            result = getList(EmployeeExtractType.class, EmployeeExtractType.P_ACTIVE, Boolean.TRUE);
        }
        Collections.sort(result, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        model.getDataSource().setCountRow(result.size());
        UniBaseUtils.createPage(model.getDataSource(), result);
    }
}