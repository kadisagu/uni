/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e8.AddEdit;

import java.util.List;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 30.09.2011
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<EmployeeEncouragementSExtract>
{
    private ISingleSelectModel _financingSourceModel; //Модель для селекта Источник финансирования
    private ISingleSelectModel _financingSourceItemModel; //Модель для селекта Источник финансирования (детально)
    private IMultiSelectModel _encouragementTypeModel; //Модель для мультиселекта Тип поощрения
    private List<EncouragementType> _encouragementTypeList; //Список выбранных типов поощрений в мультиселеке Тип поощрения

    //Getters & Setters

    public List<EncouragementType> getEncouragementTypeList()
    {
        return _encouragementTypeList;
    }

    public void setEncouragementTypeList(List<EncouragementType> encouragementTypeList)
    {
        _encouragementTypeList = encouragementTypeList;
    }

    public IMultiSelectModel getEncouragementTypeModel()
    {
        return _encouragementTypeModel;
    }

    public void setEncouragementTypeModel(IMultiSelectModel encouragementTypeModel)
    {
        _encouragementTypeModel = encouragementTypeModel;
    }

    public ISingleSelectModel getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(ISingleSelectModel financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }
}