/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e28;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.ScienceStatusPaymentExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 19.03.2012
 */
public class ScienceStatusPaymentExtractPrint implements IPrintFormCreator<ScienceStatusPaymentExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ScienceStatusPaymentExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        InflectorVariant variant = IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE);
        String decAcademicStatusTitle = DeclinableManager.instance().dao().getPropertyValue(extract.getPersonAcademicStatus().getAcademicStatus(), ScienceStatus.P_TITLE, variant);
        modifier.put("conferredAcademicStatus_G", decAcademicStatusTitle != null ? decAcademicStatusTitle : extract.getPersonAcademicStatus().getAcademicStatus().getTitle());

        StringBuilder newSalPerBuilder = new StringBuilder();
        newSalPerBuilder.append("с ");
        newSalPerBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginPayDate()));
        if (extract.getEndPayDate() != null)
        {
            newSalPerBuilder.append(" по ");
            newSalPerBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndPayDate()));
        }
        modifier.put("newSalaryPeriod", newSalPerBuilder.toString());

        Map<TripletKey<FinancingSource, FinancingSourceItem, Double>, Object> financinSourceMap = new HashMap<TripletKey<FinancingSource, FinancingSourceItem, Double>, Object>();
        List<FinancingSourceDetails> staffRateDetails = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
        for (FinancingSourceDetails details : staffRateDetails)
            financinSourceMap.put(new TripletKey<FinancingSource, FinancingSourceItem, Double>(details.getFinancingSource(), details.getFinancingSourceItem(), details.getStaffRate()), null);
        String financeSources = "";
        if (financinSourceMap.size() == 1)
        {
            TripletKey<FinancingSource, FinancingSourceItem, Double> tripletKey = financinSourceMap.keySet().iterator().next();
            financeSources += (tripletKey.getFirst().getGenitiveCaseTitle() != null ? tripletKey.getFirst().getGenitiveCaseTitle() : tripletKey.getFirst().getTitle()) +
                    (tripletKey.getSecond() != null ?
                            (tripletKey.getSecond().getGenitiveCaseTitle() != null ? " " + tripletKey.getSecond().getGenitiveCaseTitle() : " " + tripletKey.getSecond().getTitle())
                            : "");
        }
        else
        {
            int i = 0;
            for (TripletKey<FinancingSource, FinancingSourceItem, Double> tripletKey : financinSourceMap.keySet())
            {
                i++;
                financeSources += (tripletKey.getFirst().getGenitiveCaseTitle() != null ? tripletKey.getFirst().getGenitiveCaseTitle() : tripletKey.getFirst().getTitle()) +
                        (tripletKey.getSecond() != null ?
                                (tripletKey.getSecond().getGenitiveCaseTitle() != null ? " " + tripletKey.getSecond().getGenitiveCaseTitle() : " " + tripletKey.getSecond().getTitle())
                                : "") +
                        " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(tripletKey.getThird()) + " ставки)" +
                        (i < financinSourceMap.size() ? ", " : "");
            }
        }
        modifier.put("financingSource_G", financeSources);

        RtfTableModifier tableModifier = extendPrint(extract, modifier);

        if (tableModifier != null)
            tableModifier.modify(document);

        modifier.modify(document);
        return document;
    }

    protected RtfTableModifier extendPrint(ScienceStatusPaymentExtract extract, RtfInjectModifier modifier)
    {
        return null;
    }
}