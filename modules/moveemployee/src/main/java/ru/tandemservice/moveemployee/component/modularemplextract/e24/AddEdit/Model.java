/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e24.AddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.RemovalCombinationExtract;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 29.12.2011
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<RemovalCombinationExtract>
{
    DynamicListDataSource _employeeStaffRateDataSource;
    DynamicListDataSource _combinationStaffRateDataSource;

    ISingleSelectModel _combinationPostModel;

    List<CombinationPostStaffRateItem> _combinationPostStaffRateItemList = new ArrayList<>();

    //Getters & Setters

    public List<CombinationPostStaffRateItem> getCombinationPostStaffRateItemList()
    {
        return _combinationPostStaffRateItemList;
    }

    public void setCombinationPostStaffRateItemList(List<CombinationPostStaffRateItem> combinationPostStaffRateItemList)
    {
        _combinationPostStaffRateItemList = combinationPostStaffRateItemList;
    }

    public DynamicListDataSource getEmployeeStaffRateDataSource()
    {
        return _employeeStaffRateDataSource;
    }

    public void setEmployeeStaffRateDataSource(DynamicListDataSource employeeStaffRateDataSource)
    {
        _employeeStaffRateDataSource = employeeStaffRateDataSource;
    }

    public DynamicListDataSource getCombinationStaffRateDataSource()
    {
        return _combinationStaffRateDataSource;
    }

    public void setCombinationStaffRateDataSource(DynamicListDataSource combinationStaffRateDataSource)
    {
        _combinationStaffRateDataSource = combinationStaffRateDataSource;
    }

    public ISingleSelectModel getCombinationPostModel()
    {
        return _combinationPostModel;
    }

    public void setCombinationPostModel(ISingleSelectModel combinationPostModel)
    {
        _combinationPostModel = combinationPostModel;
    }
}