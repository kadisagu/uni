package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.entity.gen.EmployeeAddPaymentsSExtractGen;

/**
 * Выписка из индивидуального приказа по кадровому составу. Об установлении работнику доплат
 */
public class EmployeeAddPaymentsSExtract extends EmployeeAddPaymentsSExtractGen
{
    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }
}