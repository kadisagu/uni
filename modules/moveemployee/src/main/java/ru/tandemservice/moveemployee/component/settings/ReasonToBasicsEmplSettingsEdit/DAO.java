/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.settings.ReasonToBasicsEmplSettingsEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.uni.dao.UniDao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author alikhanov
 * @since 17.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReason(get(EmployeeOrderReasons.class, model.getEmployeeOrderReasonsId()));
        model.setSelectedBasicsList(model.getReason().getBasics());
        model.setEmployeeBasicsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EmployeeOrderBasics.ENTITY_CLASS, "bas");
                builder.add(MQExpression.like("bas", EmployeeOrderBasics.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("bas", EmployeeOrderBasics.P_TITLE);
                return new ListResult<>(builder.<EmployeeOrderBasics>getResultList(getSession()));
            }
        });
    }

    @Override
    public void update(Model model)
    {
        List<Long> checkedReasonsList = new ArrayList<>();
        MQBuilder builder = new MQBuilder(EmployeeReasonToBasicRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", IEntityRelation.L_FIRST, model.getReason()));
        List<EmployeeReasonToBasicRel> relList = builder.getResultList(getSession());

        for(EmployeeReasonToBasicRel rel : relList)
        {
            if(!model.getSelectedBasicsList().contains(rel))
                getSession().delete(rel);
            else
                checkedReasonsList.add(rel.getSecond().getId());
        }

        for(EmployeeOrderBasics basic : model.getSelectedBasicsList())
        {
            if(!checkedReasonsList.contains(basic.getId()))
            {
                EmployeeReasonToBasicRel rel = new EmployeeReasonToBasicRel();
                rel.setFirst(model.getReason());
                rel.setSecond(basic);
                getSession().save(rel);
            }
        }

    }
}
