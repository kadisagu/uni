/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.tandemframework.shared.employeebase.catalog.entity.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * @author dseleznev
 * Created on: 16.09.2009
 */
public interface IPostAssignExtract extends IAbstractExtract<EmployeePost, AbstractEmployeeParagraph>, IEmployeePostExtract, IEmployeeOrgUnitExtract, ILabourContractExtract
{
    Employee getEmployee();

    void setEmployee(Employee employee);

    boolean isFreelance();

    CompetitionAssignmentType getCompetitionType();

    void setCompetitionType(CompetitionAssignmentType competitionType);

    PostType getPostType();

    void setPostType(PostType postType);

    EmployeeWeekWorkLoad getWeekWorkLoad();

    void setWeekWorkLoad(EmployeeWeekWorkLoad weekWorkLoad);

    EmployeeWorkWeekDuration getWorkWeekDuration();

    void setWorkWeekDuration(EmployeeWorkWeekDuration workWeekDuration);

    EtksLevels getEtksLevels();

    void setEtksLevels(EtksLevels etksLevels);

    SalaryRaisingCoefficient getRaisingCoefficient();

    void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient);

    double getSalary();

    void setSalary(double salary);
}