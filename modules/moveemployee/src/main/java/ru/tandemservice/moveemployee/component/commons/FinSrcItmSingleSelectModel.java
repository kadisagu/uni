/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;

import java.util.List;

/**
 * Create by ashaburov
 * Date 03.08.11
 *
 * <p>Класс, являющийся наследником <tt>BaseSingleSelectModel</tt> и выполняющий те же функции.<p>
 * Дополнительно содержит поля, от которых зависит какие данные будут подняты в методе <tt>findValues</tt>.<p>
 * Используется для того, что бы выводимые в мультиселекте строки зависили не только от фильтра.
 */
public class FinSrcItmSingleSelectModel extends BaseSingleSelectModel
{
    OrgUnit _orgUnit;
    PostBoundedWithQGandQL _postBoundedWithQGandQL;
    FinancingSource _financingSource;
    List<FinancingSourceItem> _resultList;
    boolean _hasActiveStaffList;

    public FinSrcItmSingleSelectModel(OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, FinancingSource financingSource, boolean hasActiveStaffList)
    {
        _orgUnit = orgUnit;
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
        _financingSource = financingSource;
        _hasActiveStaffList = hasActiveStaffList;
    }

    @Override
    public Object getValue(Object primaryKey)
    {
        findValues("");

        for (FinancingSourceItem item : _resultList)
            if (item.getId().equals(primaryKey))
                return item;

        return null;
    }

    @Override
    public ListResult findValues(String filter)
    {
        if (_hasActiveStaffList)
            _resultList = UniempDaoFacade.getStaffListDAO().getFinancingSourcesItemStaffListItems(_orgUnit, _postBoundedWithQGandQL, _financingSource, filter);
        else
            _resultList = UniempDaoFacade.getUniempDAO().getFinSrcItm(_financingSource.getId(), filter);

        return new ListResult<>(_resultList);
    }
}
