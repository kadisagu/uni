/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e1.MultipleExtractAdd;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListMultipleExtractAdd.AbstractListMultipleExtractAddDAO;
import ru.tandemservice.moveemployee.entity.EmployeeAddEmplListExtract;

import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 11.06.2009
 */
public class DAO extends AbstractListMultipleExtractAddDAO<EmployeeAddEmplListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        /*model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourseOldListModel(new UniFullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourse() == null) return ListResult.getEmpty();

                MQBuilder subBuilder = new MQBuilder(Group.ENTITY_CLASS, "g");
                subBuilder.add(MQExpression.eq("g", Group.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrder().getOrgUnit()));
                subBuilder.add(MQExpression.eq("g", Group.P_ARCHIVAL, Boolean.FALSE));
                subBuilder.add(MQExpression.eq("g", Group.L_COURSE, model.getCourse()));

                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s", new String[]{Student.L_COURSE});
                builder.add(MQExpression.eq("s", Student.L_STATUS + "." + StudentStatus.P_ACTIVE, Boolean.TRUE));
                builder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
                builder.add(MQExpression.in("s", Student.L_GROUP, subBuilder));
                builder.addOrder("s", Student.L_COURSE + "." + Course.P_CODE);
                builder.setNeedDistinct(true);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
        model.setCourseNewListModel(new UniFullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getCourse() == null || model.getCourseOld() == null) return ListResult.getEmpty();
                int minCourse = Integer.parseInt(model.getCourseOld().getCode());
                List<Course> courseList = new ArrayList(model.getCourseList());
                for (Iterator<Course> iter = courseList.iterator(); iter.hasNext();)
                    if (Integer.parseInt(iter.next().getCode()) <= minCourse)
                        iter.remove();
                return new ListResult<>(courseList);
            }
        });*/
    }

    @Override
    protected MQBuilder getEmployeeList(Model model, List<Employee> groupsList)
    {
        return super.getEmployeeList(model, groupsList);
        //.add(MQExpression.eq("s", Employee.L_COURSE, model.getCourseOld()));
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        //builder.add(MQExpression.eq("p", Group.L_COURSE, model.getCourse()));
    }

    @Override
    protected EmployeeAddEmplListExtract createNewInstance(Model model)
    {
        return new EmployeeAddEmplListExtract();
    }

    @Override
    protected void fillExtract(EmployeeAddEmplListExtract extract, Model model, Employee employee)
    {
        /* TODO
         * extract.setGroup(group);
        extract.setCourse(model.getCourse());
        extract.setCourseOld(model.getCourseOld());
        extract.setCourseNew(model.getCourseNew());
        extract.setGroupChangeCourse(model.isGroupChangeCourse());*/
    }
}