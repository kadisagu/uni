/**
 *$Id$
 */
package ru.tandemservice.moveemployee.dao.ModularOrder;

import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public class EmployeeModularOrderParagraphsSorter1 extends UniBaseDao implements IEmployeeModularOrderParagraphsSortDAO
{
    /*
     * Сортировщик правила "Ручная нумерация"
     */

    /*
     * Выписки остортированы пользователем,
     * дополнительная сортировка не требуется
     */
    public void sortExtracts(EmployeeModularOrder order)
    {

    }
}