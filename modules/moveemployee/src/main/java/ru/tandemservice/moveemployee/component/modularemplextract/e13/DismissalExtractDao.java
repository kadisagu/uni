/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e13;

import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.DismissalExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemInner;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 14.03.2011
 */
public class DismissalExtractDao extends UniBaseDao implements IExtractComponentDao<DismissalExtract>
{
    @Override
    public void doCommit(DismissalExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        extract.getEntity().setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_FIRED));
        extract.getEntity().setDismissalDate(extract.getDismissalDate());

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());
        if (staffRateItemList.size() > 0)
        {
            EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEntity(), staffRateItemList);
            empHistItem.setDismissalDate(extract.getDismissalDate());
            empHistItem.setCurrent(false);
            getSession().saveOrUpdate(empHistItem);
        }

        update(extract.getEntity());
        update(extract);
    }

    @Override
    public void doRollback(DismissalExtract extract, Map parameters)
    {
        extract.getEntity().setDismissalDate(extract.getDismissalDateOld());
        extract.getEntity().setPostStatus(extract.getEmployeePostStatusOld());

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());
        if (staffRateItemList.size() > 0)
        {
            EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEntity(), staffRateItemList);
            empHistItem.setDismissalDate(null);
            empHistItem.setCurrent(true);
            getSession().saveOrUpdate(empHistItem);
        }

        update(extract.getEntity());
    }
}