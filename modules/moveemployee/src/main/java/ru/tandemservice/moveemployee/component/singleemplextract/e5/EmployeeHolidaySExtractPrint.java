/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e5;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniemp.UniempDefines;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 23.09.2009
 */
public class EmployeeHolidaySExtractPrint implements IPrintFormCreator<EmployeeHolidaySExtract>
{
    @Override
    @SuppressWarnings("deprecation")
    public RtfDocument createPrintForm(byte[] template, EmployeeHolidaySExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder)extract.getParagraph().getOrder());
        
        List<String> fieldsToRemove = new ArrayList<>();
        RtfTableModifier tableModifier = new RtfTableModifier();
        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        
        formatSingleDate(modifier, extract.getFirstPeriodBeginDate(), "periodBegin");
        formatSingleDate(modifier, extract.getFirstPeriodEndDate(), "periodEnd");
        
        if(null != extract.getSecondPeriodBeginDate() || null != extract.getSecondPeriodEndDate())
        {
            String[] secondPeriodDateArr = new String[] { "за период работы с", "“", "", "”", "", "20", "", "г.", "по", "“", "", "”", "", "20", "", "г." };
            
            if(null != extract.getSecondPeriodBeginDate())
            {
                secondPeriodDateArr[2] = RussianDateFormatUtils.getDayString(extract.getSecondPeriodBeginDate(), true);
                secondPeriodDateArr[4] = RussianDateFormatUtils.getMonthName(extract.getSecondPeriodBeginDate(), false);
                secondPeriodDateArr[6] = RussianDateFormatUtils.getYearString(extract.getSecondPeriodBeginDate(), true);
            }
            
            if(null != extract.getSecondPeriodEndDate())
            {
                secondPeriodDateArr[10] = RussianDateFormatUtils.getDayString(extract.getSecondPeriodEndDate(), true);
                secondPeriodDateArr[12] = RussianDateFormatUtils.getMonthName(extract.getSecondPeriodEndDate(), false);
                secondPeriodDateArr[14] = RussianDateFormatUtils.getYearString(extract.getSecondPeriodEndDate(), true);
            }
            
            tableModifier.put("SECOND_PERIOD", new String[][]{secondPeriodDateArr});
        }
        else UniRtfUtil.removeTableByName(document, "SECOND_PERIOD", true, false);

        UniRtfUtil.removeTableByName(document, "THIRD_PERIOD", true, false);

        if(null != extract.getMainHolidayDuration()) modifier.put("mainDaysAmount", String.valueOf(extract.getMainHolidayDuration()));
        else modifier.put("mainDaysAmount", "");
        if(null != extract.getSecondHolidayDuration()) modifier.put("secondDaysAmount", String.valueOf(extract.getSecondHolidayDuration()));
        else modifier.put("secondDaysAmount", "");
        modifier.put("sumDaysAmount", String.valueOf(extract.getDuration()));
        
        formatSingleDate(modifier, extract.getMainHolidayBeginDate(), "mainStart");
        formatSingleDate(modifier, extract.getMainHolidayEndDate(), "mainEnd");
        formatSingleDate(modifier, extract.getSecondHolidayBeginDate(), "secondStart");
        formatSingleDate(modifier, extract.getSecondHolidayEndDate(), "secondEnd");
        formatSingleDate(modifier, extract.getBeginDate(), "sumStart");
        formatSingleDate(modifier, extract.getEndDate(), "sumEnd");
        
        if(!UniempDefines.HOLIDAY_TYPE_ANNUAL.equals(extract.getHolidayType().getCode()))
            modifier.put("anotherHolidayType", extract.getHolidayType().getTitle());
        else
            modifier.put("anotherHolidayType", "");
        
        if(null != extract.getOptionalCondition()) modifier.put("additionalCondition", extract.getOptionalCondition());
        else fieldsToRemove.add("additionalCondition");
        
        if(extract.getBasicListStr().length() > 5) modifier.put("basicsStr", "Основание: " + extract.getBasicListStr());
        else fieldsToRemove.add("basicsStr");
        
        UniRtfUtil.removeParagraphsByNamesFromAnywhere(document, fieldsToRemove, true, false);
        
        tableModifier.modify(document);
        modifier.modify(document);
        return document;
    }
    
    private void formatSingleDate(RtfInjectModifier modifier, Date date, String keyPrefix)
    {
        if (null != date)
        {
            modifier.put(keyPrefix + "Day", RussianDateFormatUtils.getDayString(date, true));
            modifier.put(keyPrefix + "MonthStr", RussianDateFormatUtils.getMonthName(date, false));
            modifier.put(keyPrefix + "Yr", RussianDateFormatUtils.getYearString(date, true));
        }
        else
        {
            modifier.put(keyPrefix + "Day", "");
            modifier.put(keyPrefix + "MonthStr", "");
            modifier.put(keyPrefix + "Yr", "");
        }
    }
}