/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e16.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IListenerParametersResolver;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 08.04.2011
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<ReturnFromChildCareHolidayExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareStaffRateDataSource(component);
        super.onRefreshComponent(component);
        hasShowEmployeeHRColumn(component);
    }

    public void onClickFreelance(IBusinessComponent component)
    {
        hasShowEmployeeHRColumn(component);
    }

    public void onChangeRateOrHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);

        Double multiplier = CommonEmployeeExtractUtil.getSummStaffRateExtract(model.getStaffRateDataSource(), model.getStaffRateItemList());

        if (model.getExtract().getHolidayType() != null)
            if (UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_1_5.equals(model.getExtract().getHolidayType().getCode()) &&
                    multiplier < 1)
                model.setSavePaymentDisable(false);
            else
            {
                model.setSavePaymentDisable(true);
                model.getExtract().setSavePayment(false);
            }
    }

    public void onChangePayment(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getExtract().getPayment() != null)
        {
            model.getExtract().setStartDate(model.getExtract().getPayment().getBeginDate());
            model.getExtract().setFinishDate(model.getExtract().getPayment().getEndDate());
        }
    }

    public void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (!model.isNeedUpdateDataSource() && model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<FinancingSourceDetails> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        }, 5);

        dataSource.addColumn(new BlockColumn("staffRate", "Ставка"));
        dataSource.addColumn(new BlockColumn("financingSource", "Источник финансирования"));
        dataSource.addColumn(new BlockColumn("financingSourceItem", "Источник финансирования (детально)"));
        if (model.isThereAnyActiveStaffList())
            dataSource.addColumn(new BlockColumn("employeeHR", "Кадровая расстановка"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem");
        actionColumn.setDisableSecondSubmit(false);
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setStaffRateDataSource(dataSource);
    }

    public void onChangeFinSrc(IBusinessComponent component)
    {
//        Model model = getModel(component);
//
//        Long itemId = component.getListenerParameter() != null ?
//                (Long) component.getListenerParameter() :
//                model.getStaffRateDataSource().getCurrentEntity().getId();

//        CommonEmployeeExtractUtil.fillFinSrcItmModel(itemId, model, model.getEmployeePost().getOrgUnit(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL());
//
//        if (model.isThereAnyActiveStaffList())
//            prepareEmployeeHR(component);
    }

    public void onClickAddStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        FinancingSourceDetails item = new FinancingSourceDetails();

        short discriminator = EntityRuntime.getMeta(EmployeePostStaffRateItem.class).getEntityCode();
        long id = EntityIDGenerator.generateNewId(discriminator);

        item.setId(id);
        item.setExtract(model.getExtract());

        //добавляем новую не заполненную ставку в список ставок
        model.getStaffRateItemList().add(item);

        getDao().prepareStaffRateDataSource(model);

//        if (model.isThereAnyActiveStaffList())
//            prepareEmployeeHR(component);
    }

    public void onClickDeleteItem(IBusinessComponent component)
    {
        Model model = getModel(component);

        FinancingSourceDetails item =(FinancingSourceDetails) component.getListenerParameter();

        model.getStaffRateItemList().remove(item);

        getDao().prepareStaffRateDataSource(model);

        onChangeRateOrHoliday(component);
    }

//    public void prepareEmployeeHR(IBusinessComponent component)
//    {
//        Model model = getModel(component);
//
//        CommonEmployeeExtractUtil.prepareEmployeeHRModel(model, model.getEmployeePost().getOrgUnit(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL());
//    }

    /**
     * Определяем необходимо ли показывать колонку Кадровая расстановка в сечлисте Ставка,<p>
     * если колонка показывается, то обновляем DataSource и заполняем модели мультиселектов в колонке
     */
    public void hasShowEmployeeHRColumn(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (CommonEmployeeExtractUtil.hasNeedShowEmployeeHRColumn(model, model.getExtract().isFreelance(), model.getEmployeePost().getOrgUnit()))
        {
            prepareStaffRateDataSource(component);
//            prepareEmployeeHR(component);

            //определяем какие источники финансирования показывать
            CommonEmployeeExtractUtil.fillFinSrcModel(model, model.getExtract().isFreelance(), model.getEmployeePost().getOrgUnit(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL());

            if (model.isEditForm())
                CommonEmployeeExtractUtil.fillEmpHRColumn(model, model.getEmployeePost().getOrgUnit(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL());
        }
        else
        {
            prepareStaffRateDataSource(component);
//            prepareEmployeeHR(component);

            //определяем какие источники финансирования показывать
            CommonEmployeeExtractUtil.fillFinSrcModel(model, model.getExtract().isFreelance(), model.getEmployeePost().getOrgUnit(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL());
        }
    }
}