/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentStringReprFormatAddEdit;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author esych
 * Created on: 17.01.2011
 */
public abstract interface IDAO extends IUniDao<Model>
{
    void prepare(Model model, IBusinessComponent component);
    void copyLabelToFormatString(Model model, Long lLabelID);
    void setFormatStringExample(Model model);
}
