/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeReasonToTextParagraphSettings.EmployeeReasonToTextParagraphSettingsList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;

/**
 * Create by: ashaburov
 * Date: 18.04.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareDataSource(component);
    }

    public void prepareDataSource(IBusinessComponent component)
    {
        if (getModel(component).getDataSource() != null) return;

        DynamicListDataSource<EmployeeOrderReasons> dataSource = new DynamicListDataSource<EmployeeOrderReasons>(component, this);
        dataSource.addColumn(new SimpleColumn("Название причины", EmployeeOrderReasons.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Текстовый параграф", Model.P_TEXT_PARAGRAPH).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));

        getModel(component).setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.REASON_TO_TEXT_PARAGRAPH_EDIT, new ParametersMap().add("employeeOrderReasonId", component.getListenerParameter())));
    }
}
