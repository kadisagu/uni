/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSeparatorEdit;

/**
 * @author esych
 * Created on: 19.01.2011
 */
public class Model
{
    private String _separator;

    public String getSeparator()
    {
        return _separator;
    }

    public void setSeparator(String value)
    {
        _separator = value;
    }
}
