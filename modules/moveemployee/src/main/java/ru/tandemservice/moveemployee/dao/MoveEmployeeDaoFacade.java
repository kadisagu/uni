/**
 *  $Id$ 
 */
package ru.tandemservice.moveemployee.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author dseleznev
 * Created on: 11.11.2008
 */
public class MoveEmployeeDaoFacade
{
    private static IMoveEmployeeDAO _moveEmployeeDao;
    private static IMoveEmployeeUtilDAO _moveEmployeeUtilDao;

    public static IMoveEmployeeDAO getMoveEmployeeDao()
    {
        if (_moveEmployeeDao == null)
            _moveEmployeeDao = (IMoveEmployeeDAO)ApplicationRuntime.getBean(IMoveEmployeeDAO.MOVE_EMPLOYEE_DAO_BEAN_NAME);
        return _moveEmployeeDao;
    }
    
    public static IMoveEmployeeUtilDAO getMoveEmployeeUtilDao()
    {
        if (_moveEmployeeUtilDao == null)
            _moveEmployeeUtilDao = (IMoveEmployeeUtilDAO)ApplicationRuntime.getBean(IMoveEmployeeUtilDAO.MOVE_EMPLOYEE_UTIL_DAO_BEAN_NAME);
        return _moveEmployeeUtilDao;
    }
}