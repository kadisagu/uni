/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.settings.ReasonToDismissReasonAddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;

/**
 * @author dseleznev
 * Created on: 22.09.2009
 */
@Input(keys = "employeeOrderReasonId", bindings = "employeeOrderReasonId")
public class Model
{
    private Long _employeeOrderReasonId;
    private EmployeeOrderReasons _reason;
    private EmployeeDismissReasons _dismissReason;
    private EmployeeReasonToDismissReasonRel _relation;

    private ISelectModel _dismissReasonsListModel;

    public Long getEmployeeOrderReasonId()
    {
        return _employeeOrderReasonId;
    }

    public void setEmployeeOrderReasonId(Long employeeOrderReasonId)
    {
        this._employeeOrderReasonId = employeeOrderReasonId;
    }

    public EmployeeOrderReasons getReason()
    {
        return _reason;
    }

    public void setReason(EmployeeOrderReasons reason)
    {
        this._reason = reason;
    }

    public EmployeeDismissReasons getDismissReason()
    {
        return _dismissReason;
    }

    public void setDismissReason(EmployeeDismissReasons dismissReason)
    {
        this._dismissReason = dismissReason;
    }

    public EmployeeReasonToDismissReasonRel getRelation()
    {
        return _relation;
    }

    public void setRelation(EmployeeReasonToDismissReasonRel relation)
    {
        this._relation = relation;
    }

    public ISelectModel getDismissReasonsListModel()
    {
        return _dismissReasonsListModel;
    }

    public void setDismissReasonsListModel(ISelectModel dismissReasonsListModel)
    {
        this._dismissReasonsListModel = dismissReasonsListModel;
    }
}