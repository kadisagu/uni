/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e14.AddEdit;

import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.ContractTerminationExtract;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 18.03.2011
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<ContractTerminationExtract>
{
    private List<FinancingSource> _financingSourcesList;
    private List<RelationDegree> _relativeTypesList;

    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;
    private boolean _needRelativeData = false;
    private boolean _holidayNotSupposed = false;

    //Getters & Setters


    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public boolean isHolidayNotSupposed()
    {
        return _holidayNotSupposed;
    }

    public void setHolidayNotSupposed(boolean holidayNotSupposed)
    {
        _holidayNotSupposed = holidayNotSupposed;
    }

    public List<FinancingSource> getFinancingSourcesList()
    {
        return _financingSourcesList;
    }

    public void setFinancingSourcesList(List<FinancingSource> financingSourcesList)
    {
        _financingSourcesList = financingSourcesList;
    }

    public boolean isNeedRelativeData()
    {
        return _needRelativeData;
    }

    public void setNeedRelativeData(boolean needRelativeData)
    {
        _needRelativeData = needRelativeData;
    }

    public List<RelationDegree> getRelativeTypesList()
    {
        return _relativeTypesList;
    }

    public void setRelativeTypesList(List<RelationDegree> relativeTypesList)
    {
        _relativeTypesList = relativeTypesList;
    }
}