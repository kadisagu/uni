/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e18;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 15.09.2011
 */
public class TransferAnnualHolidayExtractPrint implements IPrintFormCreator<TransferAnnualHolidayExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, TransferAnnualHolidayExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        //если в приказе выбран График отпусков
        if (extract.getEmployeeHoliday() instanceof VacationScheduleItem)
        {
            String result = "за " + ((VacationScheduleItem) extract.getEmployeeHoliday()).getVacationSchedule().getYear()
                    + "г. согласно графику отпусков ";

            List<PeriodPartToExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getPeriodPartFromExtractRelation(extract);

            for (PeriodPartToExtractRelation relation : relationList)
            {
                result += "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((VacationScheduleItem) relation.getPart()).getPlanDate()) + " на " +
                        NumberConvertingUtil.getCalendarDaysWithName(((VacationScheduleItem) relation.getPart()).getDaysAmount());

                if (relationList.indexOf(relation) + 1 == relationList.size() - 1)
                    result += " и ";
                else if (relationList.indexOf(relation) == relationList.size() - 1)
                    result += ".";
                else
                    result += ", ";
            }

            modifier.put("vacationScheduleStr", result);
        }
        else
            modifier.put("vacationScheduleStr", "");

        if (extract.getEmployeeHoliday() instanceof EmployeeHoliday)
        {
            String result = "за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) extract.getEmployeeHoliday()).getBeginDate()) +
                    " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) extract.getEmployeeHoliday()).getEndDate()) + " ";

            List<PeriodPartToExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getPeriodPartFromExtractRelation(extract);

            for (PeriodPartToExtractRelation relation : relationList)
            {
                result += "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) relation.getPart()).getStartDate()) +
                        " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) relation.getPart()).getFinishDate()) +
                        " на " + NumberConvertingUtil.getCalendarDaysWithName(((EmployeeHoliday) relation.getPart()).getDuration());

                if (relationList.indexOf(relation) + 1 == relationList.size() - 1)
                    result += " и ";
                else if (relationList.indexOf(relation) == relationList.size() - 1)
                    result += ".";
                else
                    result += ", ";
            }

            modifier.put("employeeHolidayStr", result);
        }
        else if (extract.getEmployeeHoliday() instanceof NewPartInTransferAnnualHolidayExtract)
        {
            String result = "за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginHolidayDate()) +
                    " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndHolidayDate()) + " ";

            List<NewVacationScheduleToExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getNewVacationScheduleFromExtractRelation(extract);

            for (NewVacationScheduleToExtractRelation relation : relationList)
            {
                result += "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(relation.getBeginDay()) +
                        " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(relation.getEndDay()) +
                        " на " + NumberConvertingUtil.getCalendarDaysWithName(relation.getDurationDay());

                if (relationList.indexOf(relation) + 1 == relationList.size() - 1)
                    result += " и ";
                else if (relationList.indexOf(relation) == relationList.size() - 1)
                    result += ".";
                else
                    result += ", ";
            }

            modifier.put("employeeHolidayStr", result);
        }
        else
            modifier.put("employeeHolidayStr", "");

        List<TransferHolidayDateToExtractRelation> transferHolidayList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getTransferHolidayDateFromExtractRelation(extract);

        if (!transferHolidayList.isEmpty())
        {
            String title = "2. ПРЕДОСТАВИТЬ";
            String body = ", " +
                    CommonExtractPrintUtil.getPostPrintingTitle(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.DATIVE_CASE, true) + " " +
                    CommonExtractPrintUtil.getOrgUnitPrintingTitle(extract.getEntity().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, false, true) +
                    " ежегодный отпуск ";

            if (extract.getEmployeeHoliday() instanceof VacationScheduleItem)
                body += "за " + ((VacationScheduleItem) extract.getEmployeeHoliday()).getVacationSchedule().getYear() + "г. согласно графику отпусков ";
            else if (extract.getEmployeeHoliday() instanceof EmployeeHoliday)
                body += "за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) extract.getEmployeeHoliday()).getBeginDate()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) extract.getEmployeeHoliday()).getEndDate()) + " ";
            else if (extract.getEmployeeHoliday() instanceof NewPartInTransferAnnualHolidayExtract)
                body += "за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginHolidayDate()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndHolidayDate()) + " ";

            for (TransferHolidayDateToExtractRelation relation : transferHolidayList)
            {
                body += "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(relation.getBeginDay()) +
                        " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(relation.getEndDay()) +
                        " на " + NumberConvertingUtil.getCalendarDaysWithName(relation.getDurationDay());

                if (transferHolidayList.indexOf(relation) + 1 == transferHolidayList.size() - 1)
                    body += " и ";
                else if (transferHolidayList.indexOf(relation) == transferHolidayList.size() - 1)
                    body += ".";
                else
                    body += ", ";
            }
            modifier.put("secondPartTitle", title);
            modifier.put("secondPartBody", body);
        }
        else
        {
            List<String> fieldList = new ArrayList<String>(2);
            fieldList.add("secondPartTitle");
            fieldList.add("secondPartBody");
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, fieldList, false, true);
        }

        if (extract.getOptionalCondition() != null)
            modifier.put("optionalCondition", extract.getOptionalCondition());
        else
            modifier.put("optionalCondition", "");

        modifier.modify(document);
        return document;
    }
}