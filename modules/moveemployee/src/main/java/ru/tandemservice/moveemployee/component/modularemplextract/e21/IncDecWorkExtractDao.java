/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e21;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.moveemployee.entity.IncDecWorkExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.catalog.codes.CombinationPostTypeCodes;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 14.12.2011
 */
public class IncDecWorkExtractDao extends UniBaseDao implements IExtractComponentDao<IncDecWorkExtract>
{
    public void doCommit(IncDecWorkExtract extract, Map parameters)
    {
        PostBoundedWithQGandQL postBoundedWithQGandQL;
        if (extract.getCombinationPost() != null)
            postBoundedWithQGandQL = extract.getCombinationPost().getPostBoundedWithQGandQL();
        else
            postBoundedWithQGandQL = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL();

        CommonExtractCommitUtil.validateCombinationPostAllocItems(extract, extract.getOrgUnit(), postBoundedWithQGandQL, extract.getCombinationPost());

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (errorCollector.hasErrors())
            return;

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        MQBuilder staffRateBuilder = new MQBuilder(CombinationPostStaffRateExtractRelation.ENTITY_CLASS, "sr");
        staffRateBuilder.add(MQExpression.eq("sr", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract().s(), extract));
        List<CombinationPostStaffRateExtractRelation> staffRateExtractRelationList = staffRateBuilder.getResultList(getSession());

        MQBuilder allocItemBuilder = new MQBuilder(CombinationPostStaffRateExtAllocItemRelation.ENTITY_CLASS, "ai");
        allocItemBuilder.add(MQExpression.in("ai", CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation().s(), staffRateBuilder));
        List<CombinationPostStaffRateExtAllocItemRelation> allocItemRelationList = allocItemBuilder.getResultList(getSession());

        for (CombinationPostStaffRateExtAllocItemRelation relation : allocItemRelationList)
        {
            if (!relation.isHasNewAllocItem() && relation.getChoseStaffListAllocationItem() != null)
                delete(relation.getChoseStaffListAllocationItem());
            relation.setChoseStaffListAllocationItem(null);
            update(relation);
        }

        if (extract.getCombinationPost() != null)
        {
            Calendar endDate = GregorianCalendar.getInstance();
            endDate.setTime(extract.getBeginDate());
            endDate.add(Calendar.DAY_OF_MONTH, -1);

            extract.setEndDateOld(extract.getCombinationPost().getEndDate());

            extract.getCombinationPost().setEndDate(endDate.getTime());
        }

        CombinationPost combinationPost = new CombinationPost();
        combinationPost.setCombinationPostType(IUniBaseDao.instance.get().getCatalogItem(CombinationPostType.class, CombinationPostTypeCodes.INC_WORK));
        combinationPost.setEmployeePost(extract.getEntity());
        combinationPost.setOrgUnit(extract.getOrgUnit());
        combinationPost.setPostBoundedWithQGandQL(postBoundedWithQGandQL);
        combinationPost.setMissingEmployeePost(extract.getMissingEmployeePost());
        combinationPost.setBeginDate(extract.getBeginDate());
        combinationPost.setEndDate(extract.getEndDate());
        combinationPost.setSalaryRaisingCoefficient(extract.getSalaryRaisingCoefficient());
        combinationPost.setEtksLevels(extract.getEtksLevels());
        combinationPost.setSalary(extract.getSalary());
        combinationPost.setOrderNumber(extract.getParagraph().getOrder().getNumber());
        combinationPost.setCommitDateSystem(extract.getParagraph().getOrder().getCommitDate());
        combinationPost.setCollateralAgreementNumber(extract.getContractAddAgreementNumber());
        combinationPost.setCollateralAgreementDate(extract.getContractAddAgreementDate());
        combinationPost.setFreelance(extract.isFreelance());

        extract.setCreateCombinationPost(combinationPost);

        save(combinationPost);

        for (CombinationPostStaffRateExtractRelation relation : staffRateExtractRelationList)
        {
            CombinationPostStaffRateItem rateItem = new CombinationPostStaffRateItem();
            rateItem.setCombinationPost(combinationPost);
            rateItem.setFinancingSource(relation.getFinancingSource());
            rateItem.setFinancingSourceItem(relation.getFinancingSourceItem());
            rateItem.setStaffRate(relation.getStaffRate());

            save(rateItem);

            if (!allocItemRelationList.isEmpty())
            {
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(extract.getOrgUnit(), postBoundedWithQGandQL, relation.getFinancingSource(), relation.getFinancingSourceItem());

                StaffListAllocationItem allocItem = new StaffListAllocationItem();
                allocItem.setCombination(true);
                allocItem.setCombinationPost(combinationPost);
                allocItem.setStaffRate(relation.getStaffRate());
                allocItem.setFinancingSource(relation.getFinancingSource());
                allocItem.setFinancingSourceItem(relation.getFinancingSourceItem());
                allocItem.setStaffListItem(staffListItem);
                allocItem.setEmployeePost(extract.getEntity());
                allocItem.setRaisingCoefficient(extract.getSalaryRaisingCoefficient());
                if (allocItem.getRaisingCoefficient() != null)
                    allocItem.setMonthBaseSalaryFund(allocItem.getRaisingCoefficient().getRecommendedSalary() * allocItem.getStaffRate());
                else if (allocItem.getStaffListItem() != null)
                    allocItem.setMonthBaseSalaryFund(allocItem.getStaffListItem().getSalary() * allocItem.getStaffRate());

                save(allocItem);

                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocItem, getSession());
            }
        }

        MoveEmployeeDaoFacade.getMoveEmployeeDao().createOrUpdateContractAndAgreement(extract, "Увеличение (уменьшение) объема выполняемой работы");

        update(extract);

    }

    public void doRollback(IncDecWorkExtract extract, Map parameters)
    {
        if (extract.getCombinationPost() != null)
        {
            extract.getCombinationPost().setEndDate(extract.getEndDateOld());
            extract.setEndDateOld(null);
            update(extract.getCombinationPost());
        }

        if (extract.getCreateCollateralAgreement() != null)
        {
            delete(extract.getCreateCollateralAgreement());
            extract.setCreateCollateralAgreement(null);
        }

        if (extract.getCreateEmployeeContract() != null)
        {
            delete(extract.getCreateEmployeeContract());
            extract.setCreateEmployeeContract(null);
        }

        MQBuilder allocBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "a");
        allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.combination().s(), true));
        allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.combinationPost().s(), extract.getCreateCombinationPost()));

        for (StaffListAllocationItem allocItem : allocBuilder.<StaffListAllocationItem>getResultList(getSession()))
            delete(allocItem);

        delete(extract.getCreateCombinationPost());
        extract.setCreateCombinationPost(null);

        update(extract);

        PostBoundedWithQGandQL postBoundedWithQGandQL;
        if (extract.getCombinationPost() != null)
            postBoundedWithQGandQL = extract.getCombinationPost().getPostBoundedWithQGandQL();
        else
            postBoundedWithQGandQL = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL();

        MQBuilder staffRateBuilder = new MQBuilder(CombinationPostStaffRateExtractRelation.ENTITY_CLASS, "sr");
        staffRateBuilder.add(MQExpression.eq("sr", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract().s(), extract));

        MQBuilder allocItemBuilder = new MQBuilder(CombinationPostStaffRateExtAllocItemRelation.ENTITY_CLASS, "ai");
        allocItemBuilder.add(MQExpression.in("ai", CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation().s(), staffRateBuilder));

        for (CombinationPostStaffRateExtAllocItemRelation relation : allocItemBuilder.<CombinationPostStaffRateExtAllocItemRelation>getResultList(getSession()))
        {
            if (!relation.isHasNewAllocItem())
            {
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(extract.getOrgUnit(), postBoundedWithQGandQL, relation.getCombinationPostStaffRateExtractRelation().getFinancingSource(), relation.getCombinationPostStaffRateExtractRelation().getFinancingSourceItem());

                StaffListAllocationItem allocationItem = new StaffListAllocationItem();

                allocationItem.setStaffRate(relation.getStaffRateHistory());
                allocationItem.setFinancingSource(relation.getCombinationPostStaffRateExtractRelation().getFinancingSource());
                allocationItem.setFinancingSourceItem(relation.getCombinationPostStaffRateExtractRelation().getFinancingSourceItem());
                allocationItem.setStaffListItem(staffListItem);
                allocationItem.setEmployeePost(relation.getEmployeePostHistory());
                if (relation.getCombinationPostHistory() != null && relation.isHasCombinationPost())
                {
                    allocationItem.setCombination(true);
                    allocationItem.setCombinationPost(relation.getCombinationPostHistory());
                }
                if (allocationItem.getEmployeePost() != null)
                    allocationItem.setRaisingCoefficient(relation.getEmployeePostHistory().getRaisingCoefficient());
                if (allocationItem.getRaisingCoefficient() != null)
                    allocationItem.setMonthBaseSalaryFund(allocationItem.getRaisingCoefficient().getRecommendedSalary() * allocationItem.getStaffRate());
                else if (allocationItem.getStaffListItem() != null)
                    allocationItem.setMonthBaseSalaryFund(allocationItem.getStaffListItem().getSalary() * allocationItem.getStaffRate());

                save(allocationItem);
                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());

                if (relation.getChoseStaffListAllocationItem() == null)
                    relation.setChoseStaffListAllocationItem(allocationItem);

                update(relation);
            }
        }
    }
}