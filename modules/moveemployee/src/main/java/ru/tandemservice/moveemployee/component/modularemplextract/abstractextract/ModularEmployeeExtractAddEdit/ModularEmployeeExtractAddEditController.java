/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;

import java.util.Date;

/**
 * @author dseleznev
 * Created on: 13.11.2008
 */
public abstract class ModularEmployeeExtractAddEditController<T extends ModularEmployeeExtract, IDAO extends IModularEmployeeExtractAddEditDAO<T, Model>, Model extends ModularEmployeeExtractAddEditModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public final void onChangeExtractType(IBusinessComponent component)
    {
        ModularEmployeeExtractAddEditModel<T> model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getModularExtractAddEditComponent(model.getExtractType().getIndex()), new ParametersMap().add("employeePostId", model.getEmployeePost().getId()).add("extractTypeId", model.getExtractType().getId())));
    }

    public final synchronized void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        Model model = getModel(component);

        getDao().validate(model, errors);

        if (errors.hasErrors()) return;

        getDao().update(model);

        if (model.isAddForm())
        {
            if (model.getExtractAsOrder() != null && model.getExtractAsOrder())
            {
                //если выписка в приказе одна(текущая) и ее тип помечен как формирующийся индивидуально,
                //то переходим на выписку и передаем параметр для отображения выписки как приказа
                //иначе переходим на приказ
                if (model.getOrder().getParagraphCount() == 1 && MoveEmployeeDaoFacade.getMoveEmployeeDao().isExtractIndividual(model.getExtract()))
                {
                    deactivate(component, 2);//назад на 2-е страницы
                    activateInRoot(component, new PublisherActivator(model.getExtract()).setParameters(new ParametersMap().
                            add("extractIndividual", true).
                            add(PublisherActivator.PUBLISHER_ID_KEY, model.getExtract().getId())
                    ));
                }
                else
                {
                    deactivate(component, 2);//назад на 2-е страницы
                    activateInRoot(component, new PublisherActivator(model.getOrder()));
                }
            }
            else
            {
                deactivate(component, 2);//назад на 2-е страницы
                activateInRoot(component, new PublisherActivator(model.getExtract()));
            }
        }
        else
            deactivate(component);
    }
    
    public void onClickDeactivate(IBusinessComponent component)
    {
        if (getModel(component).isAddForm())
            deactivate(component, 2);
        else
            deactivate(component);
    }

    public void onChangeOrder(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getSelectOrder() != null)
        {
            EmployeeModularOrder order;

            if (model.getSelectOrder().getId().equals(ModularEmployeeExtractAddEditModel.NEW_MODULAR_ORDER_ID))
            {
                order = new EmployeeModularOrder();
                order.setCreateDate(new Date());
                order.setExecutor(getDao().getName());
            }
            else
                order = getDao().get(EmployeeModularOrder.class, model.getSelectOrder().getId());

            model.setExecutorStr(order.getExecutor());
            model.getExtract().setCreateDate(order.getCreateDate());

            model.setOrder(order);
            if (model.getOrder().getNumber() != null)
                model.setNumber(model.getOrder().getNumber());
            if (model.getOrder().getCommitDate() != null)
                model.setCommitDate(model.getOrder().getCommitDate());
        }

        //если выбранный приказ содержит выписки, то поле number нередоктируемое
        //если выбранный приказ содержит выписки, то поле commitDate нередоктируемое
        if (model.getOrder().getParagraphCount() > 0)
        {
            model.setDisableCommitDate(true);
            model.setDisableOrderNumber(true);
        }
        else
        {
            model.setDisableOrderNumber(false);
            model.setDisableCommitDate(false);
        }
    }
}