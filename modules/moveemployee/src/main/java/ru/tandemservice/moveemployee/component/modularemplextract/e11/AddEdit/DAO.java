/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e11.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.PaymentAssigningExtract;
import ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<PaymentAssigningExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected PaymentAssigningExtract createNewInstance()
    {
        return new PaymentAssigningExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isAddForm())
            model.getExtract().setEntity(model.getEmployeePost());
        else
            model.setEmployeePost(model.getExtract().getEntity());

//        CommonEmployeeExtractUtil.createPaymentsBonusModel(model);

        if (model.isEditForm())
        {
            //подготавливаем блок с выплатами
            preparePaymentBlock(model);
        }

        model.setRemovePaymentsList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getRemovePaymentsListExtracts(model.getExtract()));

        model.setRemovePaymentsModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                //поднимаем выплаты сотрудника
                List<EmployeePayment> resultList = new ArrayList<>();

                MQBuilder builder = new MQBuilder(EmployeePayment.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", EmployeePayment.employeePost().s(), model.getExtract().getEntity()));
                builder.add(MQExpression.eq("b", EmployeePayment.payment().oneTimePayment().s(), false));
                builder.add(MQExpression.less("b", EmployeePayment.beginDate().s(), model.getExtract().getCreateDate()));
                builder.add(MQExpression.like("b", EmployeePayment.payment().title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("b", EmployeePayment.endDate().s());

                List<EmployeePayment> paymentList = builder.getResultList(getSession());
                List<EmployeePayment> firstList = new ArrayList<>();
                List<EmployeePayment> secondList = new ArrayList<>();

                //первыми идут выплаты, действующие на момент формирования приказа, затем все остальные
                for (EmployeePayment payment : paymentList)
                {
                    if (payment.getEndDate() != null && payment.getEndDate().after(model.getExtract().getCreateDate()))
                        firstList.add(payment);
                    else
                        secondList.add(payment);
                }

                resultList.addAll(firstList);
                resultList.addAll(secondList);


                return new ListResult<>(resultList);
            }
        });

        model.setPaymentModel(CommonEmployeeExtractUtil.getPaymentModelForPaymentBlock());
        model.setFinSrcPaymentModel(CommonEmployeeExtractUtil.getFinSrcModelForPaymentBlock());
        model.setFinSrcItemPaymentModel(CommonEmployeeExtractUtil.getFinSrcItemModelForPaymentBlock(model.getPaymentDataSource()));
    }

    protected void preparePaymentBlock(Model model)
    {
        DQLSelectBuilder payBuilder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "b").column("b");
        payBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeBonus.extract().fromAlias("b")), model.getExtract()));

        model.setPaymentList(payBuilder.createStatement(getSession()).<EmployeeBonus>list());

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        List<StaffListPostPayment> paymentList = CommonEmployeeExtractUtil.checkEnabledAddStaffListPaymentButton(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), model.getEmployeePost().getOrgUnit(), model.getPaymentList(), staffRateItemList);

        if (!paymentList.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);
    }

    @Override
    public void preparePaymentDataSource(Model model)
    {
        CommonEmployeeExtractUtil.preparePaymentDataSourceForPaymentBlock(model.getPaymentList(), model.getPaymentDataSource());
    }

//    @Override
//    public void prepareListDataSource(Model model)
//    {
//        CommonEmployeeExtractUtil.fillPaymentsBonusDataSource(model);
//    }

    @Override
    public void update(Model model)
    {
        super.update(model);
//        CommonEmployeeExtractUtil.updateBonuses(model, getSession());

        //поднимаем и удаляем все связи
        MQBuilder builder = new MQBuilder(RemoveEmployeePaymentToExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", RemoveEmployeePaymentToExtractRelation.paymentAssigningExtract().id().s(), model.getExtract().getId()));

        for (RemoveEmployeePaymentToExtractRelation relation : builder.<RemoveEmployeePaymentToExtractRelation>getResultList(getSession()))
            delete(relation);

        getSession().flush();

        //затем создаем новые
        for (EmployeePayment payment : model.getRemovePaymentsList())
        {
            RemoveEmployeePaymentToExtractRelation relation = new RemoveEmployeePaymentToExtractRelation();
            relation.setEndDateOld(payment.getEndDate());
            relation.setPaymentAssigningExtract(model.getExtract());
            relation.setEmployeePayment(payment);
            getSession().saveOrUpdate(relation);
        }

        //PAYMENTS
        //если редактируем приказ, то удаляем созданные ранее выплаты в выписке
        if (model.isEditForm())
        {
            for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getSavedPaymentsFromPaymentBlockDQLBuilder(model.getExtract()).createStatement(getSession()).<EmployeeBonus>list())
                delete(bonus);
        }

        //сохраняем выплаты
        for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getPaymentsForSaveFromPaymentBlock(model.getPaymentList()))
            save(bonus);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if(model.getPaymentList().isEmpty())
            errors.add("Необходимо указать хотя бы одну выплату.");

        CommonEmployeeExtractUtil.validateForPaymentBlock(model.getPaymentList(), errors);
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }
}