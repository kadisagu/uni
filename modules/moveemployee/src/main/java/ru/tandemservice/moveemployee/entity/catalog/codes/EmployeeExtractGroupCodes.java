package ru.tandemservice.moveemployee.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Группы приказов по движению кадрового состава"
 * Имя сущности : employeeExtractGroup
 * Файл data.xml : moveemployee.data.xml
 */
public interface EmployeeExtractGroupCodes
{
    /** Константа кода (code) элемента : Прочие (не изменяющие данные сотрудника) (title) */
    String OTHER = "0";
    /** Константа кода (code) элемента : Отпуск (title) */
    String HOLIDAY = "1";
    /** Константа кода (code) элемента : Назначение на должность, смена должности (title) */
    String ADD_EMPLOYEE_POST = "2";
    /** Константа кода (code) элемента : Смена фамилии, имени, отчества (title) */
    String CHANGE_FIO = "3";
    /** Константа кода (code) элемента : Увольнение (title) */
    String DISMISSAL = "4";
    /** Константа кода (code) элемента : Установление доплат (title) */
    String ADD_PAYMENT = "5";
    /** Константа кода (code) элемента : Поощрение работника (title) */
    String ENCOURAGEMENT = "6";
    /** Константа кода (code) элемента : Совмещение/ увеличение объема работ (title) */
    String COMBINATION = "7";
    /** Константа кода (code) элемента : Отмена приказа (title) */
    String REPEAL = "8";
    /** Константа кода (code) элемента : Исполнение обязанностей (title) */
    String EMPLOYEE_ACTING = "9";
    /** Константа кода (code) элемента : Оплата за нерабочие праздничные дни (title) */
    String PAY_FOR_HOLIDAY = "10";

    Set<String> CODES = ImmutableSet.of(OTHER, HOLIDAY, ADD_EMPLOYEE_POST, CHANGE_FIO, DISMISSAL, ADD_PAYMENT, ENCOURAGEMENT, COMBINATION, REPEAL, EMPLOYEE_ACTING, PAY_FOR_HOLIDAY);
}
