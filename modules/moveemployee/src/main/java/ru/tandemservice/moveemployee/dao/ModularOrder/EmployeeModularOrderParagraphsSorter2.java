/**
 *$Id$
 */
package ru.tandemservice.moveemployee.dao.ModularOrder;

import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;

import java.util.Comparator;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public class EmployeeModularOrderParagraphsSorter2 extends ComparatorBasedEmployeeModularOrderParagraphsSorter
{
    /*
     * Сортировщик правила "Группировка по типу выписки"
     */

    /*
     * Группирует выписки по типу,
     * сортирует группы по названию,
     * внутри группы сортирует по ФИО сотрудника
     * В рамках ФИО по id
     */
    private static final Comparator<ModularEmployeeExtract> EXTRACT_TYPE_COMPARATOR = CommonCollator.<ModularEmployeeExtract>
            comparing(e -> e.getType().getTitle()).thenComparing(CommonCollator.comparing(e -> e.getEmployee().getFio(), true));

    @Override
    protected Comparator<ModularEmployeeExtract> getComparator()
    {
        return EXTRACT_TYPE_COMPARATOR;
    }
}