/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;

/**
 * @author esych
 * Created on: 17.01.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component), component);
    }

    public void onClickEditSeparator(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.PAYMENT_PRINT_FORMAT_SEPARATOR_EDIT));
    }

    public void onClickEditCases(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.PAYMENT_PRINT_FORMAT_CASES_EDIT,
                                                         new ParametersMap().add("Id", component.getListenerParameter())));
    }

    public void onClickAddStringFormat(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.PAYMENT_STRING_REPR_FORMAT_ADDEDIT));
    }

    public void onClickEditStringFormat(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.PAYMENT_STRING_REPR_FORMAT_ADDEDIT,
                                                         new ParametersMap().add("formatId", component.getListenerParameter())));
    }

    public void onClickDeleteStringFormat(IBusinessComponent component)
    {
        getDao().deleteStringFormat((Long)component.getListenerParameter());
    }

    public void onClickToggleStringFormatActive(IBusinessComponent component)
    {
        getDao().updateToggleStringFormatActive(getModel(component), (Long)component.getListenerParameter());
    }
}
