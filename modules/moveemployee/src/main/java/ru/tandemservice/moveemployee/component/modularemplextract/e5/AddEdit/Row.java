/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit;

import java.util.List;

/**
 * Create by ashaburov
 * Date 17.10.11
 */
public class Row
{
    private Long _id;

    private MainRow _mainRow;

    private String _type;

    //Methods

    public static Long getNewRowId(List<MainRow> mainRowList)
    {
        Long newId = 1L;
        boolean repeat;
        do
        {
            repeat = false;
            for (MainRow mainRow : mainRowList)
                for (Row row : mainRow.getRowList())
                    if (row.getId().equals(newId))
                    {
                        repeat = true;
                        newId++;
                    }
        }
        while (repeat);
        return newId;
    }

    @Override
    public boolean equals(Object obj)
    {
        return this.getId().equals(((Row) obj).getId());
    }

    @Override
    public int hashCode()
    {
        return this.getId().hashCode();
    }

    //Constructor

    public Row(Long id, MainRow mainRow, String type)
    {
        _id = id;
        _mainRow = mainRow;
        _type = type;
    }

    //Getters & Setters

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public MainRow getMainRow()
    {
        return _mainRow;
    }

    public void setMainRow(MainRow mainRow)
    {
        _mainRow = mainRow;
    }

    public String getType()
    {
        return _type;
    }

    public void setType(String type)
    {
        _type = type;
    }
}
