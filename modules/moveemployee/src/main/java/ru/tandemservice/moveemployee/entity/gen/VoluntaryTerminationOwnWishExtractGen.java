package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Об увольнении сотрудника по собственному желанию
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class VoluntaryTerminationOwnWishExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract";
    public static final String ENTITY_NAME = "voluntaryTerminationOwnWishExtract";
    public static final int VERSION_HASH = 1532324890;
    private static IEntityMeta ENTITY_META;

    public static final String P_TERMINATION_DATE = "terminationDate";
    public static final String L_OLD_EMPLOYEE_POST_STATUS = "oldEmployeePostStatus";
    public static final String P_PAYMENT_CHARGE = "paymentCharge";
    public static final String P_HOLIDAY_DAYS_AMOUNT = "holidayDaysAmount";

    private Date _terminationDate;     // Дата увольнения
    private EmployeePostStatus _oldEmployeePostStatus;     // Статус на должности до увольнения
    private double _paymentCharge;     // Выплата/удержание
    private int _holidayDaysAmount;     // Неотработанная часть отпуска

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата увольнения. Свойство не может быть null.
     */
    @NotNull
    public Date getTerminationDate()
    {
        return _terminationDate;
    }

    /**
     * @param terminationDate Дата увольнения. Свойство не может быть null.
     */
    public void setTerminationDate(Date terminationDate)
    {
        dirty(_terminationDate, terminationDate);
        _terminationDate = terminationDate;
    }

    /**
     * @return Статус на должности до увольнения. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getOldEmployeePostStatus()
    {
        return _oldEmployeePostStatus;
    }

    /**
     * @param oldEmployeePostStatus Статус на должности до увольнения. Свойство не может быть null.
     */
    public void setOldEmployeePostStatus(EmployeePostStatus oldEmployeePostStatus)
    {
        dirty(_oldEmployeePostStatus, oldEmployeePostStatus);
        _oldEmployeePostStatus = oldEmployeePostStatus;
    }

    /**
     * @return Выплата/удержание. Свойство не может быть null.
     */
    @NotNull
    public double getPaymentCharge()
    {
        return _paymentCharge;
    }

    /**
     * @param paymentCharge Выплата/удержание. Свойство не может быть null.
     */
    public void setPaymentCharge(double paymentCharge)
    {
        dirty(_paymentCharge, paymentCharge);
        _paymentCharge = paymentCharge;
    }

    /**
     * @return Неотработанная часть отпуска. Свойство не может быть null.
     */
    @NotNull
    public int getHolidayDaysAmount()
    {
        return _holidayDaysAmount;
    }

    /**
     * @param holidayDaysAmount Неотработанная часть отпуска. Свойство не может быть null.
     */
    public void setHolidayDaysAmount(int holidayDaysAmount)
    {
        dirty(_holidayDaysAmount, holidayDaysAmount);
        _holidayDaysAmount = holidayDaysAmount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof VoluntaryTerminationOwnWishExtractGen)
        {
            setTerminationDate(((VoluntaryTerminationOwnWishExtract)another).getTerminationDate());
            setOldEmployeePostStatus(((VoluntaryTerminationOwnWishExtract)another).getOldEmployeePostStatus());
            setPaymentCharge(((VoluntaryTerminationOwnWishExtract)another).getPaymentCharge());
            setHolidayDaysAmount(((VoluntaryTerminationOwnWishExtract)another).getHolidayDaysAmount());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends VoluntaryTerminationOwnWishExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) VoluntaryTerminationOwnWishExtract.class;
        }

        public T newInstance()
        {
            return (T) new VoluntaryTerminationOwnWishExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "terminationDate":
                    return obj.getTerminationDate();
                case "oldEmployeePostStatus":
                    return obj.getOldEmployeePostStatus();
                case "paymentCharge":
                    return obj.getPaymentCharge();
                case "holidayDaysAmount":
                    return obj.getHolidayDaysAmount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "terminationDate":
                    obj.setTerminationDate((Date) value);
                    return;
                case "oldEmployeePostStatus":
                    obj.setOldEmployeePostStatus((EmployeePostStatus) value);
                    return;
                case "paymentCharge":
                    obj.setPaymentCharge((Double) value);
                    return;
                case "holidayDaysAmount":
                    obj.setHolidayDaysAmount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "terminationDate":
                        return true;
                case "oldEmployeePostStatus":
                        return true;
                case "paymentCharge":
                        return true;
                case "holidayDaysAmount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "terminationDate":
                    return true;
                case "oldEmployeePostStatus":
                    return true;
                case "paymentCharge":
                    return true;
                case "holidayDaysAmount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "terminationDate":
                    return Date.class;
                case "oldEmployeePostStatus":
                    return EmployeePostStatus.class;
                case "paymentCharge":
                    return Double.class;
                case "holidayDaysAmount":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<VoluntaryTerminationOwnWishExtract> _dslPath = new Path<VoluntaryTerminationOwnWishExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "VoluntaryTerminationOwnWishExtract");
    }
            

    /**
     * @return Дата увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract#getTerminationDate()
     */
    public static PropertyPath<Date> terminationDate()
    {
        return _dslPath.terminationDate();
    }

    /**
     * @return Статус на должности до увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract#getOldEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
    {
        return _dslPath.oldEmployeePostStatus();
    }

    /**
     * @return Выплата/удержание. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract#getPaymentCharge()
     */
    public static PropertyPath<Double> paymentCharge()
    {
        return _dslPath.paymentCharge();
    }

    /**
     * @return Неотработанная часть отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract#getHolidayDaysAmount()
     */
    public static PropertyPath<Integer> holidayDaysAmount()
    {
        return _dslPath.holidayDaysAmount();
    }

    public static class Path<E extends VoluntaryTerminationOwnWishExtract> extends ModularEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _terminationDate;
        private EmployeePostStatus.Path<EmployeePostStatus> _oldEmployeePostStatus;
        private PropertyPath<Double> _paymentCharge;
        private PropertyPath<Integer> _holidayDaysAmount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract#getTerminationDate()
     */
        public PropertyPath<Date> terminationDate()
        {
            if(_terminationDate == null )
                _terminationDate = new PropertyPath<Date>(VoluntaryTerminationOwnWishExtractGen.P_TERMINATION_DATE, this);
            return _terminationDate;
        }

    /**
     * @return Статус на должности до увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract#getOldEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
        {
            if(_oldEmployeePostStatus == null )
                _oldEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_OLD_EMPLOYEE_POST_STATUS, this);
            return _oldEmployeePostStatus;
        }

    /**
     * @return Выплата/удержание. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract#getPaymentCharge()
     */
        public PropertyPath<Double> paymentCharge()
        {
            if(_paymentCharge == null )
                _paymentCharge = new PropertyPath<Double>(VoluntaryTerminationOwnWishExtractGen.P_PAYMENT_CHARGE, this);
            return _paymentCharge;
        }

    /**
     * @return Неотработанная часть отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationOwnWishExtract#getHolidayDaysAmount()
     */
        public PropertyPath<Integer> holidayDaysAmount()
        {
            if(_holidayDaysAmount == null )
                _holidayDaysAmount = new PropertyPath<Integer>(VoluntaryTerminationOwnWishExtractGen.P_HOLIDAY_DAYS_AMOUNT, this);
            return _holidayDaysAmount;
        }

        public Class getEntityClass()
        {
            return VoluntaryTerminationOwnWishExtract.class;
        }

        public String getEntityName()
        {
            return "voluntaryTerminationOwnWishExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
