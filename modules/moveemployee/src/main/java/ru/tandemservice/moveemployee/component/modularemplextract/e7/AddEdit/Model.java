/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e7.AddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.AgreedDismissalExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<AgreedDismissalExtract>
{
    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;

    //Getters & Setters

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }
}