/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e1;

import org.hibernate.Session;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.listemplextract.IOrderParagraphListCustomizer;
import ru.tandemservice.moveemployee.entity.EmployeeAddEmplListExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 10.12.2009
 */
public class EmployeeAddEmplListExtractCustomizer implements IOrderParagraphListCustomizer
{
    @Override
    public void customizeParagraphList(DynamicListDataSource dataSource, IBusinessComponent component)
    {
        dataSource.addColumn(new SimpleColumn("Подразделение", new String[]{EmployeeAddEmplListExtract.L_ORG_UNIT, OrgUnit.P_FULL_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Должность", new String[]{EmployeeAddEmplListExtract.L_POST_BOUNDED_WITH_Q_GAND_Q_L, PostBoundedWithQGandQL.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Ставка", "staffRateSumm").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Оклад", EmployeeAddEmplListExtract.P_SALARY,  DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Принять с", EmployeeAddEmplListExtract.P_BEGIN_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Принять по", EmployeeAddEmplListExtract.P_END_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
    }

    @Override
    public void wrap(DynamicListDataSource dataSource, Session session)
    {
        MQBuilder finSrcDetBuilder = new MQBuilder(FinancingSourceDetails.ENTITY_CLASS, "b");
        List<FinancingSourceDetails> detailsList = finSrcDetBuilder.getResultList(session);

        Map<Long, Double> id2staffRateSummMap = new HashMap<Long, Double>();
        for (FinancingSourceDetails details : detailsList)
            if (!id2staffRateSummMap.containsKey(details.getExtract().getId()))
                id2staffRateSummMap.put(details.getExtract().getId(), details.getStaffRate());
            else
            {
                Double value = id2staffRateSummMap.get(details.getExtract().getId());
                value += details.getStaffRate();
                id2staffRateSummMap.put(details.getExtract().getId(), value);
            }

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            wrapper.setViewProperty("staffRateSumm", id2staffRateSummMap.get(wrapper.getId()));
        }
    }
}