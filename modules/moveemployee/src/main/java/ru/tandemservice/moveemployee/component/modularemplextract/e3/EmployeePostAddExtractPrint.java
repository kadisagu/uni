/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e3;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeePostAddExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 14.01.2009
 */
public class EmployeePostAddExtractPrint implements IPrintFormCreator<EmployeePostAddExtract>
{
    @Override
    @SuppressWarnings("deprecation")
    public RtfDocument createPrintForm(byte[] template, EmployeePostAddExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        List<FinancingSourceDetails> staffRateDetails = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
        RtfTableModifier visaModifier = CommonExtractPrint.createModularExtractTableModifier(extract);
        modifier.put("fioI", extract.getEmployeeFioModified1());

        CommonExtractPrintUtil.injectStaffRates(modifier, staffRateDetails);
        CommonExtractPrintUtil.injectRsvpuPayments(modifier, extract);
        CommonExtractPrintUtil.injectTrialPeriods(modifier, null);

        if(null != extract.getEmployeePost())
        {
            CommonExtractPrintUtil.injectPost(modifier, extract.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), "Old");
            CommonExtractPrintUtil.injectOrgUnit(modifier, extract.getEmployeePost().getOrgUnit(), "Old");
        }

        String postTypeCode = extract.getPostType().getCode();
        if(UniDefines.POST_TYPE_SECOND_JOB.equals(postTypeCode) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(postTypeCode) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(postTypeCode))
            modifier.put("secondJob", " по совместительству");
        else
            modifier.put("secondJob", "");

        if(null != extract.getCompetitionType())
            modifier.put("competitionStr", " " + StringUtils.uncapitalize(extract.getCompetitionType().getTitle()) + ",");
        else
            modifier.put("competitionStr", "");

        //алгоритм определяет какой источник финансирования указан у долей ставки в выписке
        //TODO НЕОБХОДИМ РЕФАКТОРИНГ КОГДА ПОЯВИТСЯ НОВЫЙ ИСТОЧНИК ФИНАНСИРОВАНИЯ
        Boolean budget = null;
        for (FinancingSourceDetails details : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract))
        {
            boolean currentFinSrc = details.getFinancingSource().getCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET);

            if (budget != null && !budget.equals(currentFinSrc))
            {
                budget = null;
                break;
            }

            if (budget == null && currentFinSrc)
                budget = true;

            if (budget == null && !currentFinSrc)
                budget = false;
        }

        if(budget == null)
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, null, "");
        }
        else if (budget)
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, true, "");
        }
        else
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, false, "");
        }

        //ученая степень (для УГМА)
        PersonAcademicDegree personAcademicDegree = UniempDaoFacade.getUniempDAO().getEmployeeUpperAcademicDegree(extract.getEmployee());
        if (personAcademicDegree != null && personAcademicDegree.getAcademicDegree() != null)
            modifier.put("personAcademicDegree", ", " + personAcademicDegree.getAcademicDegree().getShortTitle().toUpperCase());
        else
            modifier.put("personAcademicDegree", "");

        //дата начала (для УГМА)
        modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));

        //дата окончания, если указана (для УГМА)
        if (extract.getEndDate() != null)
            modifier.put("endDate", " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));
        else
            modifier.put("endDate", "");

        //пункт 2 шаблона для УГМА
        if (extract.isPrintPayments())
        {
            modifier.put("allotStr", "2. Установить ");

            String personAcademicDegreePart2;
            if (personAcademicDegree != null && personAcademicDegree.getAcademicDegree() != null)
            personAcademicDegreePart2 = ", " + personAcademicDegree.getAcademicDegree().getShortTitle().toUpperCase();
            else
            personAcademicDegreePart2 = "";

            String fio_I = extract.getEmployeeFioModified1();
            int indexOfSpace = fio_I.indexOf(" ");
            String Fio_I = indexOfSpace <= 0 ? fio_I.toUpperCase() : (fio_I.substring(0, indexOfSpace).toUpperCase() + fio_I.substring(indexOfSpace));
            modifier.put("Fio_I", Fio_I + personAcademicDegreePart2);

            modifier.put("text", " заработную плату в размере:");

            modifier.put("minRateStr", "Минимальный оклад ");

            modifier.put("nextText", "по профессиональной квалификационной группе должностей в размере ");

            modifier.put("minSalaryPQG",
                    extract.getEmployeePost() != null ?
                            extract.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getProfQualificationGroup().getMinSalary().toString() :
                            extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getProfQualificationGroup().getMinSalary().toString());

            modifier.put("rubStr", " рублей");

            modifier.put("upCoeffStr", "Повышающий коэффициент к окладу ");

            if (extract.getRaisingCoefficient() != null)
                modifier.put("upCoeffTitle", extract.getRaisingCoefficient().getTitle());
            else
                modifier.put("upCoeffTitle", "______________");

            modifier.put("atRateStr", " в размере ");

            if (extract.getRaisingCoefficient() != null)
                modifier.put("upCoeffCount", ((Double) extract.getRaisingCoefficient().getRaisingCoefficient()).toString());
            else
                modifier.put("upCoeffCount", "______________");

            modifier.put("salaryStr", "Оклад с повышающим коэффициентом ______________ ");

            if (extract.getRaisingCoefficient() != null)
                modifier.put("salaryCount", ((Double) extract.getRaisingCoefficient().getRecommendedSalary()).toString());
            else
                modifier.put("salaryCount", "______________");

            modifier.put("persUpCoeffStr", "Персональный повышающий коэффициент ______________ в размере ______________");

            modifier.put("stimulatePaymentStr", "Стимулирующие выплаты:");
            modifier.put("compensativePaymentStr","Компенсационные выплаты:");

            //Формируем список стимулирующих и компенсационных выплат
            List<EmployeeBonus> bonusesList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract);

            List<String[]> stimulatingPaymentsList = new ArrayList<>();
            List<String[]> compensativePaymentsList = new ArrayList<>();

            //List<String[]> result = new ArrayList<>();
            for (EmployeeBonus bonus : bonusesList)
            {
                String[] bonusArr = new String[5];
                bonusArr[0] = bonus.getPayment().getTitle();
                bonusArr[1] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()) + " " + bonus.getPayment().getPaymentUnit().getShortTitle();
                //result.add(bonusArr);

                PaymentType parentPaymentType = bonus.getPayment().getType();
                while (null != parentPaymentType.getParent())
                    parentPaymentType = parentPaymentType.getParent();

                if (UniempDefines.PAYMENT_TYPE_STLIMULATION.equals(parentPaymentType.getCode()))
                    stimulatingPaymentsList.add(bonusArr);
                else
                    compensativePaymentsList.add(bonusArr);
            }

            if (stimulatingPaymentsList.size() != 0)
                visaModifier.put("STIMULATING_PAYMENTS", stimulatingPaymentsList.toArray(new String[][] {}));
            else
                visaModifier.put("STIMULATING_PAYMENTS", new String[][] {{"",""}});
            if (compensativePaymentsList.size() != 0)
                visaModifier.put("COMPENSATIVE_PAYMENTS", compensativePaymentsList.toArray(new String[][] {}));
            else
                visaModifier.put("COMPENSATIVE_PAYMENTS", new String[][] {{"",""}});
            CommonExtractUtil.getBonusListStrRtf(extract);
        }
        else
        {
            List<String> fieldList = new ArrayList<>();
            fieldList.add("allotStr");
            fieldList.add("Fio_I");
            fieldList.add("text");
            fieldList.add("minRateStr");
            fieldList.add("minSalaryPQG");
            fieldList.add("rubStr");
            fieldList.add("upCoeffStr");
            fieldList.add("upCoeffTitle");
            fieldList.add("atRateStr");
            fieldList.add("upCoeffCount");
            fieldList.add("salaryStr");
            fieldList.add("salaryCount");
            fieldList.add("persUpCoeffStr");
            fieldList.add("stimulatePaymentStr");
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, fieldList, true, true);
            UniRtfUtil.removeTableByName(document, "STIMULATING_PAYMENTS" , true, true);
            UniRtfUtil.removeTableByName(document, "COMPENSATIVE_PAYMENTS" , true, true);
        }

        modifier.modify(document);


        visaModifier.modify(document);

        return document;
    }
}