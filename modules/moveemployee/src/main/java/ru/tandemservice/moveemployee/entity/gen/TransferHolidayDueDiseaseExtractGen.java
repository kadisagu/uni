package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.gen.IAbstractDocumentGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Перенос части отпуска в связи с болезнью
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferHolidayDueDiseaseExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract";
    public static final String ENTITY_NAME = "transferHolidayDueDiseaseExtract";
    public static final int VERSION_HASH = 521007892;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_HOLIDAY = "employeeHoliday";
    public static final String P_TRANSFER_DAYS_AMOUNT = "transferDaysAmount";
    public static final String P_DATE_TO_TRANSFER = "dateToTransfer";
    public static final String P_TRANSFER_TO_ANOTHER_DAY = "transferToAnotherDay";
    public static final String P_START_HISTORY = "startHistory";
    public static final String P_FINISH_HISTORY = "finishHistory";
    public static final String P_BEGIN_HISTORY = "beginHistory";
    public static final String P_END_HISTORY = "endHistory";
    public static final String L_CREATE_HOLIDAY = "createHoliday";
    public static final String P_DELETE_HOLIDAY = "deleteHoliday";
    public static final String P_HOLIDAY_START_OLD = "holidayStartOld";
    public static final String P_HOLIDAY_FINISH_OLD = "holidayFinishOld";
    public static final String P_HOLIDAY_DURATION_OLD = "holidayDurationOld";
    public static final String P_HOLIDAY_TITLE_OLD = "holidayTitleOld";
    public static final String P_HOLIDAY_BEGIN_OLD = "holidayBeginOld";
    public static final String P_HOLIDAY_END_OLD = "holidayEndOld";
    public static final String L_HOLIDAY_TYPE_OLD = "holidayTypeOld";
    public static final String L_HOLIDAY_EXTRACT_OLD = "holidayExtractOld";
    public static final String P_HOLIDAY_EXTRACT_NUMBER_OLD = "holidayExtractNumberOld";
    public static final String P_HOLIDAY_EXTRACT_DATE_OLD = "holidayExtractDateOld";

    private EmployeeHoliday _employeeHoliday;     // Переносимый отпуск
    private int _transferDaysAmount;     // Количество дней переносимой части отпуска
    private Date _dateToTransfer;     // Перенести на дату
    private Boolean _transferToAnotherDay;     // Перенести на другой срок
    private Date _startHistory;     // Дата начала выбранного отпуска. Сохраненное для печати
    private Date _finishHistory;     // Дата окончания выбранного отпуска. Сохраненное для печати
    private Date _beginHistory;     // Дата начала периода выбранного отпуска. Сохраненное для печати
    private Date _endHistory;     // Дата окончания периода выбранного отпуска. Сохраненное для печати
    private EmployeeHoliday _createHoliday;     // Отпуск, созданный при проведении приказа
    private Boolean _deleteHoliday;     // При проведении приказа отпуск был удален
    private Date _holidayStartOld;     // Дата начала выбранного отпуска
    private Date _holidayFinishOld;     // Дата окончания выбранного отпуска
    private Integer _holidayDurationOld;     // Длительность выбранного отпуска
    private String _holidayTitleOld;     // Название выбранного отпуска
    private Date _holidayBeginOld;     // Дата начала периода выбранного отпуска
    private Date _holidayEndOld;     // Дата окончания периода выбранного отпуска
    private HolidayType _holidayTypeOld;     // Вид выбранного отпуска
    private IAbstractDocument _holidayExtractOld;     // Выписка приказа об отпуске выбранного отпуска
    private String _holidayExtractNumberOld;     // Номер приказа об отпуске выбранного отпуска
    private Date _holidayExtractDateOld;     // Дата приказа об отпуске выбранного отпуска

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Переносимый отпуск.
     */
    public EmployeeHoliday getEmployeeHoliday()
    {
        return _employeeHoliday;
    }

    /**
     * @param employeeHoliday Переносимый отпуск.
     */
    public void setEmployeeHoliday(EmployeeHoliday employeeHoliday)
    {
        dirty(_employeeHoliday, employeeHoliday);
        _employeeHoliday = employeeHoliday;
    }

    /**
     * @return Количество дней переносимой части отпуска. Свойство не может быть null.
     */
    @NotNull
    public int getTransferDaysAmount()
    {
        return _transferDaysAmount;
    }

    /**
     * @param transferDaysAmount Количество дней переносимой части отпуска. Свойство не может быть null.
     */
    public void setTransferDaysAmount(int transferDaysAmount)
    {
        dirty(_transferDaysAmount, transferDaysAmount);
        _transferDaysAmount = transferDaysAmount;
    }

    /**
     * @return Перенести на дату.
     */
    public Date getDateToTransfer()
    {
        return _dateToTransfer;
    }

    /**
     * @param dateToTransfer Перенести на дату.
     */
    public void setDateToTransfer(Date dateToTransfer)
    {
        dirty(_dateToTransfer, dateToTransfer);
        _dateToTransfer = dateToTransfer;
    }

    /**
     * @return Перенести на другой срок.
     */
    public Boolean getTransferToAnotherDay()
    {
        return _transferToAnotherDay;
    }

    /**
     * @param transferToAnotherDay Перенести на другой срок.
     */
    public void setTransferToAnotherDay(Boolean transferToAnotherDay)
    {
        dirty(_transferToAnotherDay, transferToAnotherDay);
        _transferToAnotherDay = transferToAnotherDay;
    }

    /**
     * @return Дата начала выбранного отпуска. Сохраненное для печати.
     */
    public Date getStartHistory()
    {
        return _startHistory;
    }

    /**
     * @param startHistory Дата начала выбранного отпуска. Сохраненное для печати.
     */
    public void setStartHistory(Date startHistory)
    {
        dirty(_startHistory, startHistory);
        _startHistory = startHistory;
    }

    /**
     * @return Дата окончания выбранного отпуска. Сохраненное для печати.
     */
    public Date getFinishHistory()
    {
        return _finishHistory;
    }

    /**
     * @param finishHistory Дата окончания выбранного отпуска. Сохраненное для печати.
     */
    public void setFinishHistory(Date finishHistory)
    {
        dirty(_finishHistory, finishHistory);
        _finishHistory = finishHistory;
    }

    /**
     * @return Дата начала периода выбранного отпуска. Сохраненное для печати.
     */
    public Date getBeginHistory()
    {
        return _beginHistory;
    }

    /**
     * @param beginHistory Дата начала периода выбранного отпуска. Сохраненное для печати.
     */
    public void setBeginHistory(Date beginHistory)
    {
        dirty(_beginHistory, beginHistory);
        _beginHistory = beginHistory;
    }

    /**
     * @return Дата окончания периода выбранного отпуска. Сохраненное для печати.
     */
    public Date getEndHistory()
    {
        return _endHistory;
    }

    /**
     * @param endHistory Дата окончания периода выбранного отпуска. Сохраненное для печати.
     */
    public void setEndHistory(Date endHistory)
    {
        dirty(_endHistory, endHistory);
        _endHistory = endHistory;
    }

    /**
     * @return Отпуск, созданный при проведении приказа.
     */
    public EmployeeHoliday getCreateHoliday()
    {
        return _createHoliday;
    }

    /**
     * @param createHoliday Отпуск, созданный при проведении приказа.
     */
    public void setCreateHoliday(EmployeeHoliday createHoliday)
    {
        dirty(_createHoliday, createHoliday);
        _createHoliday = createHoliday;
    }

    /**
     * @return При проведении приказа отпуск был удален.
     */
    public Boolean getDeleteHoliday()
    {
        return _deleteHoliday;
    }

    /**
     * @param deleteHoliday При проведении приказа отпуск был удален.
     */
    public void setDeleteHoliday(Boolean deleteHoliday)
    {
        dirty(_deleteHoliday, deleteHoliday);
        _deleteHoliday = deleteHoliday;
    }

    /**
     * @return Дата начала выбранного отпуска.
     */
    public Date getHolidayStartOld()
    {
        return _holidayStartOld;
    }

    /**
     * @param holidayStartOld Дата начала выбранного отпуска.
     */
    public void setHolidayStartOld(Date holidayStartOld)
    {
        dirty(_holidayStartOld, holidayStartOld);
        _holidayStartOld = holidayStartOld;
    }

    /**
     * @return Дата окончания выбранного отпуска.
     */
    public Date getHolidayFinishOld()
    {
        return _holidayFinishOld;
    }

    /**
     * @param holidayFinishOld Дата окончания выбранного отпуска.
     */
    public void setHolidayFinishOld(Date holidayFinishOld)
    {
        dirty(_holidayFinishOld, holidayFinishOld);
        _holidayFinishOld = holidayFinishOld;
    }

    /**
     * @return Длительность выбранного отпуска.
     */
    public Integer getHolidayDurationOld()
    {
        return _holidayDurationOld;
    }

    /**
     * @param holidayDurationOld Длительность выбранного отпуска.
     */
    public void setHolidayDurationOld(Integer holidayDurationOld)
    {
        dirty(_holidayDurationOld, holidayDurationOld);
        _holidayDurationOld = holidayDurationOld;
    }

    /**
     * @return Название выбранного отпуска.
     */
    @Length(max=255)
    public String getHolidayTitleOld()
    {
        return _holidayTitleOld;
    }

    /**
     * @param holidayTitleOld Название выбранного отпуска.
     */
    public void setHolidayTitleOld(String holidayTitleOld)
    {
        dirty(_holidayTitleOld, holidayTitleOld);
        _holidayTitleOld = holidayTitleOld;
    }

    /**
     * @return Дата начала периода выбранного отпуска.
     */
    public Date getHolidayBeginOld()
    {
        return _holidayBeginOld;
    }

    /**
     * @param holidayBeginOld Дата начала периода выбранного отпуска.
     */
    public void setHolidayBeginOld(Date holidayBeginOld)
    {
        dirty(_holidayBeginOld, holidayBeginOld);
        _holidayBeginOld = holidayBeginOld;
    }

    /**
     * @return Дата окончания периода выбранного отпуска.
     */
    public Date getHolidayEndOld()
    {
        return _holidayEndOld;
    }

    /**
     * @param holidayEndOld Дата окончания периода выбранного отпуска.
     */
    public void setHolidayEndOld(Date holidayEndOld)
    {
        dirty(_holidayEndOld, holidayEndOld);
        _holidayEndOld = holidayEndOld;
    }

    /**
     * @return Вид выбранного отпуска.
     */
    public HolidayType getHolidayTypeOld()
    {
        return _holidayTypeOld;
    }

    /**
     * @param holidayTypeOld Вид выбранного отпуска.
     */
    public void setHolidayTypeOld(HolidayType holidayTypeOld)
    {
        dirty(_holidayTypeOld, holidayTypeOld);
        _holidayTypeOld = holidayTypeOld;
    }

    /**
     * @return Выписка приказа об отпуске выбранного отпуска.
     */
    public IAbstractDocument getHolidayExtractOld()
    {
        return _holidayExtractOld;
    }

    /**
     * @param holidayExtractOld Выписка приказа об отпуске выбранного отпуска.
     */
    public void setHolidayExtractOld(IAbstractDocument holidayExtractOld)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && holidayExtractOld!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractDocument.class);
            IEntityMeta actual =  holidayExtractOld instanceof IEntity ? EntityRuntime.getMeta((IEntity) holidayExtractOld) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_holidayExtractOld, holidayExtractOld);
        _holidayExtractOld = holidayExtractOld;
    }

    /**
     * @return Номер приказа об отпуске выбранного отпуска.
     */
    @Length(max=255)
    public String getHolidayExtractNumberOld()
    {
        return _holidayExtractNumberOld;
    }

    /**
     * @param holidayExtractNumberOld Номер приказа об отпуске выбранного отпуска.
     */
    public void setHolidayExtractNumberOld(String holidayExtractNumberOld)
    {
        dirty(_holidayExtractNumberOld, holidayExtractNumberOld);
        _holidayExtractNumberOld = holidayExtractNumberOld;
    }

    /**
     * @return Дата приказа об отпуске выбранного отпуска.
     */
    public Date getHolidayExtractDateOld()
    {
        return _holidayExtractDateOld;
    }

    /**
     * @param holidayExtractDateOld Дата приказа об отпуске выбранного отпуска.
     */
    public void setHolidayExtractDateOld(Date holidayExtractDateOld)
    {
        dirty(_holidayExtractDateOld, holidayExtractDateOld);
        _holidayExtractDateOld = holidayExtractDateOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferHolidayDueDiseaseExtractGen)
        {
            setEmployeeHoliday(((TransferHolidayDueDiseaseExtract)another).getEmployeeHoliday());
            setTransferDaysAmount(((TransferHolidayDueDiseaseExtract)another).getTransferDaysAmount());
            setDateToTransfer(((TransferHolidayDueDiseaseExtract)another).getDateToTransfer());
            setTransferToAnotherDay(((TransferHolidayDueDiseaseExtract)another).getTransferToAnotherDay());
            setStartHistory(((TransferHolidayDueDiseaseExtract)another).getStartHistory());
            setFinishHistory(((TransferHolidayDueDiseaseExtract)another).getFinishHistory());
            setBeginHistory(((TransferHolidayDueDiseaseExtract)another).getBeginHistory());
            setEndHistory(((TransferHolidayDueDiseaseExtract)another).getEndHistory());
            setCreateHoliday(((TransferHolidayDueDiseaseExtract)another).getCreateHoliday());
            setDeleteHoliday(((TransferHolidayDueDiseaseExtract)another).getDeleteHoliday());
            setHolidayStartOld(((TransferHolidayDueDiseaseExtract)another).getHolidayStartOld());
            setHolidayFinishOld(((TransferHolidayDueDiseaseExtract)another).getHolidayFinishOld());
            setHolidayDurationOld(((TransferHolidayDueDiseaseExtract)another).getHolidayDurationOld());
            setHolidayTitleOld(((TransferHolidayDueDiseaseExtract)another).getHolidayTitleOld());
            setHolidayBeginOld(((TransferHolidayDueDiseaseExtract)another).getHolidayBeginOld());
            setHolidayEndOld(((TransferHolidayDueDiseaseExtract)another).getHolidayEndOld());
            setHolidayTypeOld(((TransferHolidayDueDiseaseExtract)another).getHolidayTypeOld());
            setHolidayExtractOld(((TransferHolidayDueDiseaseExtract)another).getHolidayExtractOld());
            setHolidayExtractNumberOld(((TransferHolidayDueDiseaseExtract)another).getHolidayExtractNumberOld());
            setHolidayExtractDateOld(((TransferHolidayDueDiseaseExtract)another).getHolidayExtractDateOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferHolidayDueDiseaseExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferHolidayDueDiseaseExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferHolidayDueDiseaseExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    return obj.getEmployeeHoliday();
                case "transferDaysAmount":
                    return obj.getTransferDaysAmount();
                case "dateToTransfer":
                    return obj.getDateToTransfer();
                case "transferToAnotherDay":
                    return obj.getTransferToAnotherDay();
                case "startHistory":
                    return obj.getStartHistory();
                case "finishHistory":
                    return obj.getFinishHistory();
                case "beginHistory":
                    return obj.getBeginHistory();
                case "endHistory":
                    return obj.getEndHistory();
                case "createHoliday":
                    return obj.getCreateHoliday();
                case "deleteHoliday":
                    return obj.getDeleteHoliday();
                case "holidayStartOld":
                    return obj.getHolidayStartOld();
                case "holidayFinishOld":
                    return obj.getHolidayFinishOld();
                case "holidayDurationOld":
                    return obj.getHolidayDurationOld();
                case "holidayTitleOld":
                    return obj.getHolidayTitleOld();
                case "holidayBeginOld":
                    return obj.getHolidayBeginOld();
                case "holidayEndOld":
                    return obj.getHolidayEndOld();
                case "holidayTypeOld":
                    return obj.getHolidayTypeOld();
                case "holidayExtractOld":
                    return obj.getHolidayExtractOld();
                case "holidayExtractNumberOld":
                    return obj.getHolidayExtractNumberOld();
                case "holidayExtractDateOld":
                    return obj.getHolidayExtractDateOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    obj.setEmployeeHoliday((EmployeeHoliday) value);
                    return;
                case "transferDaysAmount":
                    obj.setTransferDaysAmount((Integer) value);
                    return;
                case "dateToTransfer":
                    obj.setDateToTransfer((Date) value);
                    return;
                case "transferToAnotherDay":
                    obj.setTransferToAnotherDay((Boolean) value);
                    return;
                case "startHistory":
                    obj.setStartHistory((Date) value);
                    return;
                case "finishHistory":
                    obj.setFinishHistory((Date) value);
                    return;
                case "beginHistory":
                    obj.setBeginHistory((Date) value);
                    return;
                case "endHistory":
                    obj.setEndHistory((Date) value);
                    return;
                case "createHoliday":
                    obj.setCreateHoliday((EmployeeHoliday) value);
                    return;
                case "deleteHoliday":
                    obj.setDeleteHoliday((Boolean) value);
                    return;
                case "holidayStartOld":
                    obj.setHolidayStartOld((Date) value);
                    return;
                case "holidayFinishOld":
                    obj.setHolidayFinishOld((Date) value);
                    return;
                case "holidayDurationOld":
                    obj.setHolidayDurationOld((Integer) value);
                    return;
                case "holidayTitleOld":
                    obj.setHolidayTitleOld((String) value);
                    return;
                case "holidayBeginOld":
                    obj.setHolidayBeginOld((Date) value);
                    return;
                case "holidayEndOld":
                    obj.setHolidayEndOld((Date) value);
                    return;
                case "holidayTypeOld":
                    obj.setHolidayTypeOld((HolidayType) value);
                    return;
                case "holidayExtractOld":
                    obj.setHolidayExtractOld((IAbstractDocument) value);
                    return;
                case "holidayExtractNumberOld":
                    obj.setHolidayExtractNumberOld((String) value);
                    return;
                case "holidayExtractDateOld":
                    obj.setHolidayExtractDateOld((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                        return true;
                case "transferDaysAmount":
                        return true;
                case "dateToTransfer":
                        return true;
                case "transferToAnotherDay":
                        return true;
                case "startHistory":
                        return true;
                case "finishHistory":
                        return true;
                case "beginHistory":
                        return true;
                case "endHistory":
                        return true;
                case "createHoliday":
                        return true;
                case "deleteHoliday":
                        return true;
                case "holidayStartOld":
                        return true;
                case "holidayFinishOld":
                        return true;
                case "holidayDurationOld":
                        return true;
                case "holidayTitleOld":
                        return true;
                case "holidayBeginOld":
                        return true;
                case "holidayEndOld":
                        return true;
                case "holidayTypeOld":
                        return true;
                case "holidayExtractOld":
                        return true;
                case "holidayExtractNumberOld":
                        return true;
                case "holidayExtractDateOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    return true;
                case "transferDaysAmount":
                    return true;
                case "dateToTransfer":
                    return true;
                case "transferToAnotherDay":
                    return true;
                case "startHistory":
                    return true;
                case "finishHistory":
                    return true;
                case "beginHistory":
                    return true;
                case "endHistory":
                    return true;
                case "createHoliday":
                    return true;
                case "deleteHoliday":
                    return true;
                case "holidayStartOld":
                    return true;
                case "holidayFinishOld":
                    return true;
                case "holidayDurationOld":
                    return true;
                case "holidayTitleOld":
                    return true;
                case "holidayBeginOld":
                    return true;
                case "holidayEndOld":
                    return true;
                case "holidayTypeOld":
                    return true;
                case "holidayExtractOld":
                    return true;
                case "holidayExtractNumberOld":
                    return true;
                case "holidayExtractDateOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    return EmployeeHoliday.class;
                case "transferDaysAmount":
                    return Integer.class;
                case "dateToTransfer":
                    return Date.class;
                case "transferToAnotherDay":
                    return Boolean.class;
                case "startHistory":
                    return Date.class;
                case "finishHistory":
                    return Date.class;
                case "beginHistory":
                    return Date.class;
                case "endHistory":
                    return Date.class;
                case "createHoliday":
                    return EmployeeHoliday.class;
                case "deleteHoliday":
                    return Boolean.class;
                case "holidayStartOld":
                    return Date.class;
                case "holidayFinishOld":
                    return Date.class;
                case "holidayDurationOld":
                    return Integer.class;
                case "holidayTitleOld":
                    return String.class;
                case "holidayBeginOld":
                    return Date.class;
                case "holidayEndOld":
                    return Date.class;
                case "holidayTypeOld":
                    return HolidayType.class;
                case "holidayExtractOld":
                    return IAbstractDocument.class;
                case "holidayExtractNumberOld":
                    return String.class;
                case "holidayExtractDateOld":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferHolidayDueDiseaseExtract> _dslPath = new Path<TransferHolidayDueDiseaseExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferHolidayDueDiseaseExtract");
    }
            

    /**
     * @return Переносимый отпуск.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getEmployeeHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> employeeHoliday()
    {
        return _dslPath.employeeHoliday();
    }

    /**
     * @return Количество дней переносимой части отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getTransferDaysAmount()
     */
    public static PropertyPath<Integer> transferDaysAmount()
    {
        return _dslPath.transferDaysAmount();
    }

    /**
     * @return Перенести на дату.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getDateToTransfer()
     */
    public static PropertyPath<Date> dateToTransfer()
    {
        return _dslPath.dateToTransfer();
    }

    /**
     * @return Перенести на другой срок.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getTransferToAnotherDay()
     */
    public static PropertyPath<Boolean> transferToAnotherDay()
    {
        return _dslPath.transferToAnotherDay();
    }

    /**
     * @return Дата начала выбранного отпуска. Сохраненное для печати.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getStartHistory()
     */
    public static PropertyPath<Date> startHistory()
    {
        return _dslPath.startHistory();
    }

    /**
     * @return Дата окончания выбранного отпуска. Сохраненное для печати.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getFinishHistory()
     */
    public static PropertyPath<Date> finishHistory()
    {
        return _dslPath.finishHistory();
    }

    /**
     * @return Дата начала периода выбранного отпуска. Сохраненное для печати.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getBeginHistory()
     */
    public static PropertyPath<Date> beginHistory()
    {
        return _dslPath.beginHistory();
    }

    /**
     * @return Дата окончания периода выбранного отпуска. Сохраненное для печати.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getEndHistory()
     */
    public static PropertyPath<Date> endHistory()
    {
        return _dslPath.endHistory();
    }

    /**
     * @return Отпуск, созданный при проведении приказа.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getCreateHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> createHoliday()
    {
        return _dslPath.createHoliday();
    }

    /**
     * @return При проведении приказа отпуск был удален.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getDeleteHoliday()
     */
    public static PropertyPath<Boolean> deleteHoliday()
    {
        return _dslPath.deleteHoliday();
    }

    /**
     * @return Дата начала выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayStartOld()
     */
    public static PropertyPath<Date> holidayStartOld()
    {
        return _dslPath.holidayStartOld();
    }

    /**
     * @return Дата окончания выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayFinishOld()
     */
    public static PropertyPath<Date> holidayFinishOld()
    {
        return _dslPath.holidayFinishOld();
    }

    /**
     * @return Длительность выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayDurationOld()
     */
    public static PropertyPath<Integer> holidayDurationOld()
    {
        return _dslPath.holidayDurationOld();
    }

    /**
     * @return Название выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayTitleOld()
     */
    public static PropertyPath<String> holidayTitleOld()
    {
        return _dslPath.holidayTitleOld();
    }

    /**
     * @return Дата начала периода выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayBeginOld()
     */
    public static PropertyPath<Date> holidayBeginOld()
    {
        return _dslPath.holidayBeginOld();
    }

    /**
     * @return Дата окончания периода выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayEndOld()
     */
    public static PropertyPath<Date> holidayEndOld()
    {
        return _dslPath.holidayEndOld();
    }

    /**
     * @return Вид выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayTypeOld()
     */
    public static HolidayType.Path<HolidayType> holidayTypeOld()
    {
        return _dslPath.holidayTypeOld();
    }

    /**
     * @return Выписка приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayExtractOld()
     */
    public static IAbstractDocumentGen.Path<IAbstractDocument> holidayExtractOld()
    {
        return _dslPath.holidayExtractOld();
    }

    /**
     * @return Номер приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayExtractNumberOld()
     */
    public static PropertyPath<String> holidayExtractNumberOld()
    {
        return _dslPath.holidayExtractNumberOld();
    }

    /**
     * @return Дата приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayExtractDateOld()
     */
    public static PropertyPath<Date> holidayExtractDateOld()
    {
        return _dslPath.holidayExtractDateOld();
    }

    public static class Path<E extends TransferHolidayDueDiseaseExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeeHoliday.Path<EmployeeHoliday> _employeeHoliday;
        private PropertyPath<Integer> _transferDaysAmount;
        private PropertyPath<Date> _dateToTransfer;
        private PropertyPath<Boolean> _transferToAnotherDay;
        private PropertyPath<Date> _startHistory;
        private PropertyPath<Date> _finishHistory;
        private PropertyPath<Date> _beginHistory;
        private PropertyPath<Date> _endHistory;
        private EmployeeHoliday.Path<EmployeeHoliday> _createHoliday;
        private PropertyPath<Boolean> _deleteHoliday;
        private PropertyPath<Date> _holidayStartOld;
        private PropertyPath<Date> _holidayFinishOld;
        private PropertyPath<Integer> _holidayDurationOld;
        private PropertyPath<String> _holidayTitleOld;
        private PropertyPath<Date> _holidayBeginOld;
        private PropertyPath<Date> _holidayEndOld;
        private HolidayType.Path<HolidayType> _holidayTypeOld;
        private IAbstractDocumentGen.Path<IAbstractDocument> _holidayExtractOld;
        private PropertyPath<String> _holidayExtractNumberOld;
        private PropertyPath<Date> _holidayExtractDateOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Переносимый отпуск.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getEmployeeHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> employeeHoliday()
        {
            if(_employeeHoliday == null )
                _employeeHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_EMPLOYEE_HOLIDAY, this);
            return _employeeHoliday;
        }

    /**
     * @return Количество дней переносимой части отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getTransferDaysAmount()
     */
        public PropertyPath<Integer> transferDaysAmount()
        {
            if(_transferDaysAmount == null )
                _transferDaysAmount = new PropertyPath<Integer>(TransferHolidayDueDiseaseExtractGen.P_TRANSFER_DAYS_AMOUNT, this);
            return _transferDaysAmount;
        }

    /**
     * @return Перенести на дату.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getDateToTransfer()
     */
        public PropertyPath<Date> dateToTransfer()
        {
            if(_dateToTransfer == null )
                _dateToTransfer = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_DATE_TO_TRANSFER, this);
            return _dateToTransfer;
        }

    /**
     * @return Перенести на другой срок.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getTransferToAnotherDay()
     */
        public PropertyPath<Boolean> transferToAnotherDay()
        {
            if(_transferToAnotherDay == null )
                _transferToAnotherDay = new PropertyPath<Boolean>(TransferHolidayDueDiseaseExtractGen.P_TRANSFER_TO_ANOTHER_DAY, this);
            return _transferToAnotherDay;
        }

    /**
     * @return Дата начала выбранного отпуска. Сохраненное для печати.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getStartHistory()
     */
        public PropertyPath<Date> startHistory()
        {
            if(_startHistory == null )
                _startHistory = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_START_HISTORY, this);
            return _startHistory;
        }

    /**
     * @return Дата окончания выбранного отпуска. Сохраненное для печати.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getFinishHistory()
     */
        public PropertyPath<Date> finishHistory()
        {
            if(_finishHistory == null )
                _finishHistory = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_FINISH_HISTORY, this);
            return _finishHistory;
        }

    /**
     * @return Дата начала периода выбранного отпуска. Сохраненное для печати.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getBeginHistory()
     */
        public PropertyPath<Date> beginHistory()
        {
            if(_beginHistory == null )
                _beginHistory = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_BEGIN_HISTORY, this);
            return _beginHistory;
        }

    /**
     * @return Дата окончания периода выбранного отпуска. Сохраненное для печати.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getEndHistory()
     */
        public PropertyPath<Date> endHistory()
        {
            if(_endHistory == null )
                _endHistory = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_END_HISTORY, this);
            return _endHistory;
        }

    /**
     * @return Отпуск, созданный при проведении приказа.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getCreateHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> createHoliday()
        {
            if(_createHoliday == null )
                _createHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_CREATE_HOLIDAY, this);
            return _createHoliday;
        }

    /**
     * @return При проведении приказа отпуск был удален.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getDeleteHoliday()
     */
        public PropertyPath<Boolean> deleteHoliday()
        {
            if(_deleteHoliday == null )
                _deleteHoliday = new PropertyPath<Boolean>(TransferHolidayDueDiseaseExtractGen.P_DELETE_HOLIDAY, this);
            return _deleteHoliday;
        }

    /**
     * @return Дата начала выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayStartOld()
     */
        public PropertyPath<Date> holidayStartOld()
        {
            if(_holidayStartOld == null )
                _holidayStartOld = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_HOLIDAY_START_OLD, this);
            return _holidayStartOld;
        }

    /**
     * @return Дата окончания выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayFinishOld()
     */
        public PropertyPath<Date> holidayFinishOld()
        {
            if(_holidayFinishOld == null )
                _holidayFinishOld = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_HOLIDAY_FINISH_OLD, this);
            return _holidayFinishOld;
        }

    /**
     * @return Длительность выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayDurationOld()
     */
        public PropertyPath<Integer> holidayDurationOld()
        {
            if(_holidayDurationOld == null )
                _holidayDurationOld = new PropertyPath<Integer>(TransferHolidayDueDiseaseExtractGen.P_HOLIDAY_DURATION_OLD, this);
            return _holidayDurationOld;
        }

    /**
     * @return Название выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayTitleOld()
     */
        public PropertyPath<String> holidayTitleOld()
        {
            if(_holidayTitleOld == null )
                _holidayTitleOld = new PropertyPath<String>(TransferHolidayDueDiseaseExtractGen.P_HOLIDAY_TITLE_OLD, this);
            return _holidayTitleOld;
        }

    /**
     * @return Дата начала периода выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayBeginOld()
     */
        public PropertyPath<Date> holidayBeginOld()
        {
            if(_holidayBeginOld == null )
                _holidayBeginOld = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_HOLIDAY_BEGIN_OLD, this);
            return _holidayBeginOld;
        }

    /**
     * @return Дата окончания периода выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayEndOld()
     */
        public PropertyPath<Date> holidayEndOld()
        {
            if(_holidayEndOld == null )
                _holidayEndOld = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_HOLIDAY_END_OLD, this);
            return _holidayEndOld;
        }

    /**
     * @return Вид выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayTypeOld()
     */
        public HolidayType.Path<HolidayType> holidayTypeOld()
        {
            if(_holidayTypeOld == null )
                _holidayTypeOld = new HolidayType.Path<HolidayType>(L_HOLIDAY_TYPE_OLD, this);
            return _holidayTypeOld;
        }

    /**
     * @return Выписка приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayExtractOld()
     */
        public IAbstractDocumentGen.Path<IAbstractDocument> holidayExtractOld()
        {
            if(_holidayExtractOld == null )
                _holidayExtractOld = new IAbstractDocumentGen.Path<IAbstractDocument>(L_HOLIDAY_EXTRACT_OLD, this);
            return _holidayExtractOld;
        }

    /**
     * @return Номер приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayExtractNumberOld()
     */
        public PropertyPath<String> holidayExtractNumberOld()
        {
            if(_holidayExtractNumberOld == null )
                _holidayExtractNumberOld = new PropertyPath<String>(TransferHolidayDueDiseaseExtractGen.P_HOLIDAY_EXTRACT_NUMBER_OLD, this);
            return _holidayExtractNumberOld;
        }

    /**
     * @return Дата приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract#getHolidayExtractDateOld()
     */
        public PropertyPath<Date> holidayExtractDateOld()
        {
            if(_holidayExtractDateOld == null )
                _holidayExtractDateOld = new PropertyPath<Date>(TransferHolidayDueDiseaseExtractGen.P_HOLIDAY_EXTRACT_DATE_OLD, this);
            return _holidayExtractDateOld;
        }

        public Class getEntityClass()
        {
            return TransferHolidayDueDiseaseExtract.class;
        }

        public String getEntityName()
        {
            return "transferHolidayDueDiseaseExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
