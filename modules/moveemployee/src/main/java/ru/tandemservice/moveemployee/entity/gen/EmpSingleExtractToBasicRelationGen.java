package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmpSingleExtractToBasicRelation;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки с основанием. В выписке есть список оснований. (индивидуальный приказ по кадрам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmpSingleExtractToBasicRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmpSingleExtractToBasicRelation";
    public static final String ENTITY_NAME = "empSingleExtractToBasicRelation";
    public static final int VERSION_HASH = 1487003812;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_BASIC = "basic";
    public static final String P_COMMENT = "comment";

    private SingleEmployeeExtract _extract;     // Выписка (индивидуальный приказ по кадрам)
    private EmployeeOrderBasics _basic;     // Основание выписки
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка (индивидуальный приказ по кадрам).
     */
    public SingleEmployeeExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка (индивидуальный приказ по кадрам).
     */
    public void setExtract(SingleEmployeeExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Основание выписки.
     */
    public EmployeeOrderBasics getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание выписки.
     */
    public void setBasic(EmployeeOrderBasics basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmpSingleExtractToBasicRelationGen)
        {
            setExtract(((EmpSingleExtractToBasicRelation)another).getExtract());
            setBasic(((EmpSingleExtractToBasicRelation)another).getBasic());
            setComment(((EmpSingleExtractToBasicRelation)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmpSingleExtractToBasicRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmpSingleExtractToBasicRelation.class;
        }

        public T newInstance()
        {
            return (T) new EmpSingleExtractToBasicRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "basic":
                    return obj.getBasic();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((SingleEmployeeExtract) value);
                    return;
                case "basic":
                    obj.setBasic((EmployeeOrderBasics) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "basic":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "basic":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return SingleEmployeeExtract.class;
                case "basic":
                    return EmployeeOrderBasics.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmpSingleExtractToBasicRelation> _dslPath = new Path<EmpSingleExtractToBasicRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmpSingleExtractToBasicRelation");
    }
            

    /**
     * @return Выписка (индивидуальный приказ по кадрам).
     * @see ru.tandemservice.moveemployee.entity.EmpSingleExtractToBasicRelation#getExtract()
     */
    public static SingleEmployeeExtract.Path<SingleEmployeeExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Основание выписки.
     * @see ru.tandemservice.moveemployee.entity.EmpSingleExtractToBasicRelation#getBasic()
     */
    public static EmployeeOrderBasics.Path<EmployeeOrderBasics> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.moveemployee.entity.EmpSingleExtractToBasicRelation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends EmpSingleExtractToBasicRelation> extends EntityPath<E>
    {
        private SingleEmployeeExtract.Path<SingleEmployeeExtract> _extract;
        private EmployeeOrderBasics.Path<EmployeeOrderBasics> _basic;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка (индивидуальный приказ по кадрам).
     * @see ru.tandemservice.moveemployee.entity.EmpSingleExtractToBasicRelation#getExtract()
     */
        public SingleEmployeeExtract.Path<SingleEmployeeExtract> extract()
        {
            if(_extract == null )
                _extract = new SingleEmployeeExtract.Path<SingleEmployeeExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Основание выписки.
     * @see ru.tandemservice.moveemployee.entity.EmpSingleExtractToBasicRelation#getBasic()
     */
        public EmployeeOrderBasics.Path<EmployeeOrderBasics> basic()
        {
            if(_basic == null )
                _basic = new EmployeeOrderBasics.Path<EmployeeOrderBasics>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.moveemployee.entity.EmpSingleExtractToBasicRelation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EmpSingleExtractToBasicRelationGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return EmpSingleExtractToBasicRelation.class;
        }

        public String getEntityName()
        {
            return "empSingleExtractToBasicRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
