/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.single.EmployeeSingleOrderEdit;

import org.tandemframework.core.component.Input;

import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;

/**
 * @author dseleznev
 * Created on: 02.06.2009
 */
@Input(keys = "orderId", bindings = "orderId")
public class Model
{
    private Long _orderId;
    private EmployeeSingleExtractOrder _order;
    
    private boolean _disableCommitDate;
    private boolean _disableOrderNumber;

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        this._orderId = orderId;
    }

    public EmployeeSingleExtractOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeSingleExtractOrder order)
    {
        this._order = order;
    }

    public boolean isDisableCommitDate()
    {
        return _disableCommitDate;
    }

    public void setDisableCommitDate(boolean disableCommitDate)
    {
        this._disableCommitDate = disableCommitDate;
    }

    public boolean isDisableOrderNumber()
    {
        return _disableOrderNumber;
    }

    public void setDisableOrderNumber(boolean disableOrderNumber)
    {
        this._disableOrderNumber = disableOrderNumber;
    }
}