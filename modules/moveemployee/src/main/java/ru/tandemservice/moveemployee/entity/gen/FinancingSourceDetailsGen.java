package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент разбивки ставки сотрудника по источникам финансирования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FinancingSourceDetailsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.FinancingSourceDetails";
    public static final String ENTITY_NAME = "financingSourceDetails";
    public static final int VERSION_HASH = 1541039959;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String P_STAFF_RATE = "staffRate";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";

    private AbstractEmployeeExtract _extract;     // Выписка
    private double _staffRate;     // Доля ставки
    private FinancingSource _financingSource;     // Источник финансирования
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (детально)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка. Свойство не может быть null.
     */
    @NotNull
    public AbstractEmployeeExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка. Свойство не может быть null.
     */
    public void setExtract(AbstractEmployeeExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Доля ставки. Свойство не может быть null.
     */
    @NotNull
    public double getStaffRate()
    {
        return _staffRate;
    }

    /**
     * @param staffRate Доля ставки. Свойство не может быть null.
     */
    public void setStaffRate(double staffRate)
    {
        dirty(_staffRate, staffRate);
        _staffRate = staffRate;
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     */
    @NotNull
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования. Свойство не может быть null.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FinancingSourceDetailsGen)
        {
            setExtract(((FinancingSourceDetails)another).getExtract());
            setStaffRate(((FinancingSourceDetails)another).getStaffRate());
            setFinancingSource(((FinancingSourceDetails)another).getFinancingSource());
            setFinancingSourceItem(((FinancingSourceDetails)another).getFinancingSourceItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FinancingSourceDetailsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FinancingSourceDetails.class;
        }

        public T newInstance()
        {
            return (T) new FinancingSourceDetails();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "staffRate":
                    return obj.getStaffRate();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractEmployeeExtract) value);
                    return;
                case "staffRate":
                    obj.setStaffRate((Double) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "staffRate":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "staffRate":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return AbstractEmployeeExtract.class;
                case "staffRate":
                    return Double.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FinancingSourceDetails> _dslPath = new Path<FinancingSourceDetails>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FinancingSourceDetails");
    }
            

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetails#getExtract()
     */
    public static AbstractEmployeeExtract.Path<AbstractEmployeeExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Доля ставки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetails#getStaffRate()
     */
    public static PropertyPath<Double> staffRate()
    {
        return _dslPath.staffRate();
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetails#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (детально).
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetails#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    public static class Path<E extends FinancingSourceDetails> extends EntityPath<E>
    {
        private AbstractEmployeeExtract.Path<AbstractEmployeeExtract> _extract;
        private PropertyPath<Double> _staffRate;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetails#getExtract()
     */
        public AbstractEmployeeExtract.Path<AbstractEmployeeExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractEmployeeExtract.Path<AbstractEmployeeExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Доля ставки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetails#getStaffRate()
     */
        public PropertyPath<Double> staffRate()
        {
            if(_staffRate == null )
                _staffRate = new PropertyPath<Double>(FinancingSourceDetailsGen.P_STAFF_RATE, this);
            return _staffRate;
        }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetails#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (детально).
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetails#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

        public Class getEntityClass()
        {
            return FinancingSourceDetails.class;
        }

        public String getEntityName()
        {
            return "financingSourceDetails";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
