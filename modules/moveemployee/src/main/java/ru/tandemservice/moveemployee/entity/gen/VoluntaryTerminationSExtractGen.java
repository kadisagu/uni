package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. Об увольнении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class VoluntaryTerminationSExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract";
    public static final String ENTITY_NAME = "voluntaryTerminationSExtract";
    public static final int VERSION_HASH = 52145561;
    private static IEntityMeta ENTITY_META;

    public static final String P_TERMINATION_DATE = "terminationDate";
    public static final String L_DISMISS_REASON = "dismissReason";
    public static final String P_PAYMENT_CHARGE = "paymentCharge";
    public static final String P_HOLIDAY_DAYS_AMOUNT = "holidayDaysAmount";
    public static final String P_HOLIDAY_PERIOD_BEGIN = "holidayPeriodBegin";
    public static final String P_HOLIDAY_PERIOD_END = "holidayPeriodEnd";
    public static final String P_EXCLUDE_FROM_EMPLOYEE_LIST = "excludeFromEmployeeList";
    public static final String L_RELATIVE_TYPE = "relativeType";
    public static final String P_RELATIVE_FIO = "relativeFio";
    public static final String P_IDENTITY_CARD = "identityCard";
    public static final String P_OLD_CONTRACT_END_DATE = "oldContractEndDate";
    public static final String L_OLD_EMPLOYEE_POST_STATUS = "oldEmployeePostStatus";
    public static final String L_OLD_CONTRACT = "oldContract";

    private Date _terminationDate;     // Дата увольнения
    private EmployeeDismissReasons _dismissReason;     // Причина увольнения
    private double _paymentCharge;     // Выплата/удержание
    private double _holidayDaysAmount;     // Неиспользованная часть отпуска
    private Date _holidayPeriodBegin;     // Отпуск за период (с)
    private Date _holidayPeriodEnd;     // Отпуск за период (по)
    private boolean _excludeFromEmployeeList;     // Исключить из списочного состава
    private RelationDegree _relativeType;     // Степень родства
    private String _relativeFio;     // ФИО родственника (в дательном падеже)
    private String _identityCard;     // Сведения об удостоверении личности родственника
    private Date _oldContractEndDate;     // Дата окончания трудового договора до увольнения
    private EmployeePostStatus _oldEmployeePostStatus;     // Статус на должности до увольнения
    private EmployeeLabourContract _oldContract;     // Изменяемый договор

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата увольнения. Свойство не может быть null.
     */
    @NotNull
    public Date getTerminationDate()
    {
        return _terminationDate;
    }

    /**
     * @param terminationDate Дата увольнения. Свойство не может быть null.
     */
    public void setTerminationDate(Date terminationDate)
    {
        dirty(_terminationDate, terminationDate);
        _terminationDate = terminationDate;
    }

    /**
     * @return Причина увольнения. Свойство не может быть null.
     */
    @NotNull
    public EmployeeDismissReasons getDismissReason()
    {
        return _dismissReason;
    }

    /**
     * @param dismissReason Причина увольнения. Свойство не может быть null.
     */
    public void setDismissReason(EmployeeDismissReasons dismissReason)
    {
        dirty(_dismissReason, dismissReason);
        _dismissReason = dismissReason;
    }

    /**
     * @return Выплата/удержание. Свойство не может быть null.
     */
    @NotNull
    public double getPaymentCharge()
    {
        return _paymentCharge;
    }

    /**
     * @param paymentCharge Выплата/удержание. Свойство не может быть null.
     */
    public void setPaymentCharge(double paymentCharge)
    {
        dirty(_paymentCharge, paymentCharge);
        _paymentCharge = paymentCharge;
    }

    /**
     * @return Неиспользованная часть отпуска. Свойство не может быть null.
     */
    @NotNull
    public double getHolidayDaysAmount()
    {
        return _holidayDaysAmount;
    }

    /**
     * @param holidayDaysAmount Неиспользованная часть отпуска. Свойство не может быть null.
     */
    public void setHolidayDaysAmount(double holidayDaysAmount)
    {
        dirty(_holidayDaysAmount, holidayDaysAmount);
        _holidayDaysAmount = holidayDaysAmount;
    }

    /**
     * @return Отпуск за период (с).
     */
    public Date getHolidayPeriodBegin()
    {
        return _holidayPeriodBegin;
    }

    /**
     * @param holidayPeriodBegin Отпуск за период (с).
     */
    public void setHolidayPeriodBegin(Date holidayPeriodBegin)
    {
        dirty(_holidayPeriodBegin, holidayPeriodBegin);
        _holidayPeriodBegin = holidayPeriodBegin;
    }

    /**
     * @return Отпуск за период (по).
     */
    public Date getHolidayPeriodEnd()
    {
        return _holidayPeriodEnd;
    }

    /**
     * @param holidayPeriodEnd Отпуск за период (по).
     */
    public void setHolidayPeriodEnd(Date holidayPeriodEnd)
    {
        dirty(_holidayPeriodEnd, holidayPeriodEnd);
        _holidayPeriodEnd = holidayPeriodEnd;
    }

    /**
     * @return Исключить из списочного состава. Свойство не может быть null.
     */
    @NotNull
    public boolean isExcludeFromEmployeeList()
    {
        return _excludeFromEmployeeList;
    }

    /**
     * @param excludeFromEmployeeList Исключить из списочного состава. Свойство не может быть null.
     */
    public void setExcludeFromEmployeeList(boolean excludeFromEmployeeList)
    {
        dirty(_excludeFromEmployeeList, excludeFromEmployeeList);
        _excludeFromEmployeeList = excludeFromEmployeeList;
    }

    /**
     * @return Степень родства.
     */
    public RelationDegree getRelativeType()
    {
        return _relativeType;
    }

    /**
     * @param relativeType Степень родства.
     */
    public void setRelativeType(RelationDegree relativeType)
    {
        dirty(_relativeType, relativeType);
        _relativeType = relativeType;
    }

    /**
     * @return ФИО родственника (в дательном падеже).
     */
    @Length(max=255)
    public String getRelativeFio()
    {
        return _relativeFio;
    }

    /**
     * @param relativeFio ФИО родственника (в дательном падеже).
     */
    public void setRelativeFio(String relativeFio)
    {
        dirty(_relativeFio, relativeFio);
        _relativeFio = relativeFio;
    }

    /**
     * @return Сведения об удостоверении личности родственника.
     */
    @Length(max=255)
    public String getIdentityCard()
    {
        return _identityCard;
    }

    /**
     * @param identityCard Сведения об удостоверении личности родственника.
     */
    public void setIdentityCard(String identityCard)
    {
        dirty(_identityCard, identityCard);
        _identityCard = identityCard;
    }

    /**
     * @return Дата окончания трудового договора до увольнения.
     */
    public Date getOldContractEndDate()
    {
        return _oldContractEndDate;
    }

    /**
     * @param oldContractEndDate Дата окончания трудового договора до увольнения.
     */
    public void setOldContractEndDate(Date oldContractEndDate)
    {
        dirty(_oldContractEndDate, oldContractEndDate);
        _oldContractEndDate = oldContractEndDate;
    }

    /**
     * @return Статус на должности до увольнения. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getOldEmployeePostStatus()
    {
        return _oldEmployeePostStatus;
    }

    /**
     * @param oldEmployeePostStatus Статус на должности до увольнения. Свойство не может быть null.
     */
    public void setOldEmployeePostStatus(EmployeePostStatus oldEmployeePostStatus)
    {
        dirty(_oldEmployeePostStatus, oldEmployeePostStatus);
        _oldEmployeePostStatus = oldEmployeePostStatus;
    }

    /**
     * @return Изменяемый договор.
     */
    public EmployeeLabourContract getOldContract()
    {
        return _oldContract;
    }

    /**
     * @param oldContract Изменяемый договор.
     */
    public void setOldContract(EmployeeLabourContract oldContract)
    {
        dirty(_oldContract, oldContract);
        _oldContract = oldContract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof VoluntaryTerminationSExtractGen)
        {
            setTerminationDate(((VoluntaryTerminationSExtract)another).getTerminationDate());
            setDismissReason(((VoluntaryTerminationSExtract)another).getDismissReason());
            setPaymentCharge(((VoluntaryTerminationSExtract)another).getPaymentCharge());
            setHolidayDaysAmount(((VoluntaryTerminationSExtract)another).getHolidayDaysAmount());
            setHolidayPeriodBegin(((VoluntaryTerminationSExtract)another).getHolidayPeriodBegin());
            setHolidayPeriodEnd(((VoluntaryTerminationSExtract)another).getHolidayPeriodEnd());
            setExcludeFromEmployeeList(((VoluntaryTerminationSExtract)another).isExcludeFromEmployeeList());
            setRelativeType(((VoluntaryTerminationSExtract)another).getRelativeType());
            setRelativeFio(((VoluntaryTerminationSExtract)another).getRelativeFio());
            setIdentityCard(((VoluntaryTerminationSExtract)another).getIdentityCard());
            setOldContractEndDate(((VoluntaryTerminationSExtract)another).getOldContractEndDate());
            setOldEmployeePostStatus(((VoluntaryTerminationSExtract)another).getOldEmployeePostStatus());
            setOldContract(((VoluntaryTerminationSExtract)another).getOldContract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends VoluntaryTerminationSExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) VoluntaryTerminationSExtract.class;
        }

        public T newInstance()
        {
            return (T) new VoluntaryTerminationSExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "terminationDate":
                    return obj.getTerminationDate();
                case "dismissReason":
                    return obj.getDismissReason();
                case "paymentCharge":
                    return obj.getPaymentCharge();
                case "holidayDaysAmount":
                    return obj.getHolidayDaysAmount();
                case "holidayPeriodBegin":
                    return obj.getHolidayPeriodBegin();
                case "holidayPeriodEnd":
                    return obj.getHolidayPeriodEnd();
                case "excludeFromEmployeeList":
                    return obj.isExcludeFromEmployeeList();
                case "relativeType":
                    return obj.getRelativeType();
                case "relativeFio":
                    return obj.getRelativeFio();
                case "identityCard":
                    return obj.getIdentityCard();
                case "oldContractEndDate":
                    return obj.getOldContractEndDate();
                case "oldEmployeePostStatus":
                    return obj.getOldEmployeePostStatus();
                case "oldContract":
                    return obj.getOldContract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "terminationDate":
                    obj.setTerminationDate((Date) value);
                    return;
                case "dismissReason":
                    obj.setDismissReason((EmployeeDismissReasons) value);
                    return;
                case "paymentCharge":
                    obj.setPaymentCharge((Double) value);
                    return;
                case "holidayDaysAmount":
                    obj.setHolidayDaysAmount((Double) value);
                    return;
                case "holidayPeriodBegin":
                    obj.setHolidayPeriodBegin((Date) value);
                    return;
                case "holidayPeriodEnd":
                    obj.setHolidayPeriodEnd((Date) value);
                    return;
                case "excludeFromEmployeeList":
                    obj.setExcludeFromEmployeeList((Boolean) value);
                    return;
                case "relativeType":
                    obj.setRelativeType((RelationDegree) value);
                    return;
                case "relativeFio":
                    obj.setRelativeFio((String) value);
                    return;
                case "identityCard":
                    obj.setIdentityCard((String) value);
                    return;
                case "oldContractEndDate":
                    obj.setOldContractEndDate((Date) value);
                    return;
                case "oldEmployeePostStatus":
                    obj.setOldEmployeePostStatus((EmployeePostStatus) value);
                    return;
                case "oldContract":
                    obj.setOldContract((EmployeeLabourContract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "terminationDate":
                        return true;
                case "dismissReason":
                        return true;
                case "paymentCharge":
                        return true;
                case "holidayDaysAmount":
                        return true;
                case "holidayPeriodBegin":
                        return true;
                case "holidayPeriodEnd":
                        return true;
                case "excludeFromEmployeeList":
                        return true;
                case "relativeType":
                        return true;
                case "relativeFio":
                        return true;
                case "identityCard":
                        return true;
                case "oldContractEndDate":
                        return true;
                case "oldEmployeePostStatus":
                        return true;
                case "oldContract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "terminationDate":
                    return true;
                case "dismissReason":
                    return true;
                case "paymentCharge":
                    return true;
                case "holidayDaysAmount":
                    return true;
                case "holidayPeriodBegin":
                    return true;
                case "holidayPeriodEnd":
                    return true;
                case "excludeFromEmployeeList":
                    return true;
                case "relativeType":
                    return true;
                case "relativeFio":
                    return true;
                case "identityCard":
                    return true;
                case "oldContractEndDate":
                    return true;
                case "oldEmployeePostStatus":
                    return true;
                case "oldContract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "terminationDate":
                    return Date.class;
                case "dismissReason":
                    return EmployeeDismissReasons.class;
                case "paymentCharge":
                    return Double.class;
                case "holidayDaysAmount":
                    return Double.class;
                case "holidayPeriodBegin":
                    return Date.class;
                case "holidayPeriodEnd":
                    return Date.class;
                case "excludeFromEmployeeList":
                    return Boolean.class;
                case "relativeType":
                    return RelationDegree.class;
                case "relativeFio":
                    return String.class;
                case "identityCard":
                    return String.class;
                case "oldContractEndDate":
                    return Date.class;
                case "oldEmployeePostStatus":
                    return EmployeePostStatus.class;
                case "oldContract":
                    return EmployeeLabourContract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<VoluntaryTerminationSExtract> _dslPath = new Path<VoluntaryTerminationSExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "VoluntaryTerminationSExtract");
    }
            

    /**
     * @return Дата увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getTerminationDate()
     */
    public static PropertyPath<Date> terminationDate()
    {
        return _dslPath.terminationDate();
    }

    /**
     * @return Причина увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getDismissReason()
     */
    public static EmployeeDismissReasons.Path<EmployeeDismissReasons> dismissReason()
    {
        return _dslPath.dismissReason();
    }

    /**
     * @return Выплата/удержание. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getPaymentCharge()
     */
    public static PropertyPath<Double> paymentCharge()
    {
        return _dslPath.paymentCharge();
    }

    /**
     * @return Неиспользованная часть отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getHolidayDaysAmount()
     */
    public static PropertyPath<Double> holidayDaysAmount()
    {
        return _dslPath.holidayDaysAmount();
    }

    /**
     * @return Отпуск за период (с).
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getHolidayPeriodBegin()
     */
    public static PropertyPath<Date> holidayPeriodBegin()
    {
        return _dslPath.holidayPeriodBegin();
    }

    /**
     * @return Отпуск за период (по).
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getHolidayPeriodEnd()
     */
    public static PropertyPath<Date> holidayPeriodEnd()
    {
        return _dslPath.holidayPeriodEnd();
    }

    /**
     * @return Исключить из списочного состава. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#isExcludeFromEmployeeList()
     */
    public static PropertyPath<Boolean> excludeFromEmployeeList()
    {
        return _dslPath.excludeFromEmployeeList();
    }

    /**
     * @return Степень родства.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getRelativeType()
     */
    public static RelationDegree.Path<RelationDegree> relativeType()
    {
        return _dslPath.relativeType();
    }

    /**
     * @return ФИО родственника (в дательном падеже).
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getRelativeFio()
     */
    public static PropertyPath<String> relativeFio()
    {
        return _dslPath.relativeFio();
    }

    /**
     * @return Сведения об удостоверении личности родственника.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getIdentityCard()
     */
    public static PropertyPath<String> identityCard()
    {
        return _dslPath.identityCard();
    }

    /**
     * @return Дата окончания трудового договора до увольнения.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getOldContractEndDate()
     */
    public static PropertyPath<Date> oldContractEndDate()
    {
        return _dslPath.oldContractEndDate();
    }

    /**
     * @return Статус на должности до увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getOldEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
    {
        return _dslPath.oldEmployeePostStatus();
    }

    /**
     * @return Изменяемый договор.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getOldContract()
     */
    public static EmployeeLabourContract.Path<EmployeeLabourContract> oldContract()
    {
        return _dslPath.oldContract();
    }

    public static class Path<E extends VoluntaryTerminationSExtract> extends SingleEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _terminationDate;
        private EmployeeDismissReasons.Path<EmployeeDismissReasons> _dismissReason;
        private PropertyPath<Double> _paymentCharge;
        private PropertyPath<Double> _holidayDaysAmount;
        private PropertyPath<Date> _holidayPeriodBegin;
        private PropertyPath<Date> _holidayPeriodEnd;
        private PropertyPath<Boolean> _excludeFromEmployeeList;
        private RelationDegree.Path<RelationDegree> _relativeType;
        private PropertyPath<String> _relativeFio;
        private PropertyPath<String> _identityCard;
        private PropertyPath<Date> _oldContractEndDate;
        private EmployeePostStatus.Path<EmployeePostStatus> _oldEmployeePostStatus;
        private EmployeeLabourContract.Path<EmployeeLabourContract> _oldContract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getTerminationDate()
     */
        public PropertyPath<Date> terminationDate()
        {
            if(_terminationDate == null )
                _terminationDate = new PropertyPath<Date>(VoluntaryTerminationSExtractGen.P_TERMINATION_DATE, this);
            return _terminationDate;
        }

    /**
     * @return Причина увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getDismissReason()
     */
        public EmployeeDismissReasons.Path<EmployeeDismissReasons> dismissReason()
        {
            if(_dismissReason == null )
                _dismissReason = new EmployeeDismissReasons.Path<EmployeeDismissReasons>(L_DISMISS_REASON, this);
            return _dismissReason;
        }

    /**
     * @return Выплата/удержание. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getPaymentCharge()
     */
        public PropertyPath<Double> paymentCharge()
        {
            if(_paymentCharge == null )
                _paymentCharge = new PropertyPath<Double>(VoluntaryTerminationSExtractGen.P_PAYMENT_CHARGE, this);
            return _paymentCharge;
        }

    /**
     * @return Неиспользованная часть отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getHolidayDaysAmount()
     */
        public PropertyPath<Double> holidayDaysAmount()
        {
            if(_holidayDaysAmount == null )
                _holidayDaysAmount = new PropertyPath<Double>(VoluntaryTerminationSExtractGen.P_HOLIDAY_DAYS_AMOUNT, this);
            return _holidayDaysAmount;
        }

    /**
     * @return Отпуск за период (с).
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getHolidayPeriodBegin()
     */
        public PropertyPath<Date> holidayPeriodBegin()
        {
            if(_holidayPeriodBegin == null )
                _holidayPeriodBegin = new PropertyPath<Date>(VoluntaryTerminationSExtractGen.P_HOLIDAY_PERIOD_BEGIN, this);
            return _holidayPeriodBegin;
        }

    /**
     * @return Отпуск за период (по).
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getHolidayPeriodEnd()
     */
        public PropertyPath<Date> holidayPeriodEnd()
        {
            if(_holidayPeriodEnd == null )
                _holidayPeriodEnd = new PropertyPath<Date>(VoluntaryTerminationSExtractGen.P_HOLIDAY_PERIOD_END, this);
            return _holidayPeriodEnd;
        }

    /**
     * @return Исключить из списочного состава. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#isExcludeFromEmployeeList()
     */
        public PropertyPath<Boolean> excludeFromEmployeeList()
        {
            if(_excludeFromEmployeeList == null )
                _excludeFromEmployeeList = new PropertyPath<Boolean>(VoluntaryTerminationSExtractGen.P_EXCLUDE_FROM_EMPLOYEE_LIST, this);
            return _excludeFromEmployeeList;
        }

    /**
     * @return Степень родства.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getRelativeType()
     */
        public RelationDegree.Path<RelationDegree> relativeType()
        {
            if(_relativeType == null )
                _relativeType = new RelationDegree.Path<RelationDegree>(L_RELATIVE_TYPE, this);
            return _relativeType;
        }

    /**
     * @return ФИО родственника (в дательном падеже).
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getRelativeFio()
     */
        public PropertyPath<String> relativeFio()
        {
            if(_relativeFio == null )
                _relativeFio = new PropertyPath<String>(VoluntaryTerminationSExtractGen.P_RELATIVE_FIO, this);
            return _relativeFio;
        }

    /**
     * @return Сведения об удостоверении личности родственника.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getIdentityCard()
     */
        public PropertyPath<String> identityCard()
        {
            if(_identityCard == null )
                _identityCard = new PropertyPath<String>(VoluntaryTerminationSExtractGen.P_IDENTITY_CARD, this);
            return _identityCard;
        }

    /**
     * @return Дата окончания трудового договора до увольнения.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getOldContractEndDate()
     */
        public PropertyPath<Date> oldContractEndDate()
        {
            if(_oldContractEndDate == null )
                _oldContractEndDate = new PropertyPath<Date>(VoluntaryTerminationSExtractGen.P_OLD_CONTRACT_END_DATE, this);
            return _oldContractEndDate;
        }

    /**
     * @return Статус на должности до увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getOldEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
        {
            if(_oldEmployeePostStatus == null )
                _oldEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_OLD_EMPLOYEE_POST_STATUS, this);
            return _oldEmployeePostStatus;
        }

    /**
     * @return Изменяемый договор.
     * @see ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract#getOldContract()
     */
        public EmployeeLabourContract.Path<EmployeeLabourContract> oldContract()
        {
            if(_oldContract == null )
                _oldContract = new EmployeeLabourContract.Path<EmployeeLabourContract>(L_OLD_CONTRACT, this);
            return _oldContract;
        }

        public Class getEntityClass()
        {
            return VoluntaryTerminationSExtract.class;
        }

        public String getEntityName()
        {
            return "voluntaryTerminationSExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
