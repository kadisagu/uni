/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

/**
 * Интерфейс для выписок О переводе. Выписки хранящие Переведенного и Вновь созданного сотрудника.
 *
 * @author Alexander Shaburov
 * @since 28.11.12
 */
public interface ITransferEmployeePostExtract
{
    /**
     * Сотрудник, на которого была создана выписка. Переводимый сотрудник.
     */
    EmployeePost getEmployeePostOld();
}
