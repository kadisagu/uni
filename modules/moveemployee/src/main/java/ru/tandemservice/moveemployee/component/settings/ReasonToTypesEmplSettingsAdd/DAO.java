/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.settings.ReasonToTypesEmplSettingsAdd;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToTypeRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationAdd.AbstractRelationAddDAO;

import java.util.Collections;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 26.01.2009
 */
public class DAO extends AbstractRelationAddDAO<Model> implements IDAO
{
    @Override
    @SuppressWarnings({"unchecked"})
    protected Class getRelationSecondEntityClass()
    {
        return EmployeeExtractType.class;
    }

    @Override
    protected String getRelationSecondEntityClassSortProperty()
    {
        return EmployeeExtractType.P_TITLE;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    protected Class getRelationEntityClass()
    {
        return EmployeeReasonToTypeRel.class;
    }

    @Override
    public void prepare(Model model)
    {
        model.setFirst(getNotNull(model.getFirstId()));
        model.setHierarchical(isHierarchical());

        MQBuilder builder = new MQBuilder(EmployeeExtractType.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
        builder.add(MQExpression.isNotNull("s", EmployeeExtractType.L_PARENT));
        List<EmployeeExtractType> list = builder.getResultList(getSession());

        Criteria c = getSession().createCriteria(getRelationEntityClass());
        c.add(Restrictions.eq(IEntityRelation.L_FIRST, model.getFirst()));
        c.setProjection(Projections.property(IEntityRelation.L_SECOND));
        List usedList = c.list();

        list.removeAll(usedList);

        Collections.sort(list, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);

        model.setSecondHierarchyList(HierarchyUtil.listHierarchyNodesWithParents(list, false));
    }
}