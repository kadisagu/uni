/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e1;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;

import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeAddExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uniemp.UniempDefines;

/**
 * @author dseleznev
 * Created on: 14.01.2009
 */
public class EmployeeAddExtractPrint implements IPrintFormCreator<EmployeeAddExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeAddExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        List<FinancingSourceDetails> staffRateDetails = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
        modifier.put("fioI", extract.getEmployeeFioModified1());
        CommonExtractPrintUtil.injectStaffRates(modifier, staffRateDetails);
        CommonExtractPrintUtil.injectTrialPeriods(modifier, (Integer)extract.getProperty("trialPeriod"));
        CommonExtractPrintUtil.injectRsvpuPayments(modifier, extract);
        
        if(null != extract.getCompetitionType())
            modifier.put("competitionStr", " " + StringUtils.uncapitalize(extract.getCompetitionType().getTitle()) + ",");
        else
            modifier.put("competitionStr", "");

        //алгоритм определяет какой источник финансирования указан у долей ставки в выписке
        //TODO НЕОБХОДИМ РЕФАКТОРИНГ КОГДА ПОЯВИТСЯ НОВЫЙ ИСТОЧНИК ФИНАНСИРОВАНИЯ
        Boolean budget = null;
        for (FinancingSourceDetails details : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract))
        {
            boolean currentFinSrc = details.getFinancingSource().getCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET);

            if (budget != null && !budget.equals(currentFinSrc))
            {
                budget = null;
                break;
            }

            if (budget == null && currentFinSrc)
                budget = true;

            if (budget == null && !currentFinSrc)
                budget = false;
        }

        if (budget == null)
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, null, "");
        }
        else if (budget)
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, true, "");
        }
        else
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, false, "");
        }
        modifier.modify(document);

        RtfTableModifier visaModifier = CommonExtractPrint.createModularExtractTableModifier(extract);
        visaModifier.modify(document);

        return document;
    }
}