package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Используемые типы документов в настройке Форматы печати выплат сотрудникам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PaymentStringFormatDocumentSettingsGen extends EntityBase
 implements INaturalIdentifiable<PaymentStringFormatDocumentSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings";
    public static final String ENTITY_NAME = "paymentStringFormatDocumentSettings";
    public static final int VERSION_HASH = 799828507;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_ACTIVE = "active";
    public static final String L_PARENT = "parent";

    private String _code;     // Код(тип) документа
    private String _title;     // Название
    private boolean _active;     // Используется
    private PaymentStringFormatDocumentSettings _parent;     // Используемые типы документов в настройке Форматы печати выплат сотрудникам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Код совпадает с кодами типов документов из соответствующих справочников.
     *
     * @return Код(тип) документа. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Код(тип) документа. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Используется. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    /**
     * @return Используемые типы документов в настройке Форматы печати выплат сотрудникам.
     */
    public PaymentStringFormatDocumentSettings getParent()
    {
        return _parent;
    }

    /**
     * @param parent Используемые типы документов в настройке Форматы печати выплат сотрудникам.
     */
    public void setParent(PaymentStringFormatDocumentSettings parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PaymentStringFormatDocumentSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((PaymentStringFormatDocumentSettings)another).getCode());
            }
            setTitle(((PaymentStringFormatDocumentSettings)another).getTitle());
            setActive(((PaymentStringFormatDocumentSettings)another).isActive());
            setParent(((PaymentStringFormatDocumentSettings)another).getParent());
        }
    }

    public INaturalId<PaymentStringFormatDocumentSettingsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<PaymentStringFormatDocumentSettingsGen>
    {
        private static final String PROXY_NAME = "PaymentStringFormatDocumentSettingsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PaymentStringFormatDocumentSettingsGen.NaturalId) ) return false;

            PaymentStringFormatDocumentSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PaymentStringFormatDocumentSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PaymentStringFormatDocumentSettings.class;
        }

        public T newInstance()
        {
            return (T) new PaymentStringFormatDocumentSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "active":
                    return obj.isActive();
                case "parent":
                    return obj.getParent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
                case "parent":
                    obj.setParent((PaymentStringFormatDocumentSettings) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "active":
                        return true;
                case "parent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "active":
                    return true;
                case "parent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "active":
                    return Boolean.class;
                case "parent":
                    return PaymentStringFormatDocumentSettings.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PaymentStringFormatDocumentSettings> _dslPath = new Path<PaymentStringFormatDocumentSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PaymentStringFormatDocumentSettings");
    }
            

    /**
     * Код совпадает с кодами типов документов из соответствующих справочников.
     *
     * @return Код(тип) документа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    /**
     * @return Используемые типы документов в настройке Форматы печати выплат сотрудникам.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings#getParent()
     */
    public static PaymentStringFormatDocumentSettings.Path<PaymentStringFormatDocumentSettings> parent()
    {
        return _dslPath.parent();
    }

    public static class Path<E extends PaymentStringFormatDocumentSettings> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<Boolean> _active;
        private PaymentStringFormatDocumentSettings.Path<PaymentStringFormatDocumentSettings> _parent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Код совпадает с кодами типов документов из соответствующих справочников.
     *
     * @return Код(тип) документа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(PaymentStringFormatDocumentSettingsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(PaymentStringFormatDocumentSettingsGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(PaymentStringFormatDocumentSettingsGen.P_ACTIVE, this);
            return _active;
        }

    /**
     * @return Используемые типы документов в настройке Форматы печати выплат сотрудникам.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings#getParent()
     */
        public PaymentStringFormatDocumentSettings.Path<PaymentStringFormatDocumentSettings> parent()
        {
            if(_parent == null )
                _parent = new PaymentStringFormatDocumentSettings.Path<PaymentStringFormatDocumentSettings>(L_PARENT, this);
            return _parent;
        }

        public Class getEntityClass()
        {
            return PaymentStringFormatDocumentSettings.class;
        }

        public String getEntityName()
        {
            return "paymentStringFormatDocumentSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
