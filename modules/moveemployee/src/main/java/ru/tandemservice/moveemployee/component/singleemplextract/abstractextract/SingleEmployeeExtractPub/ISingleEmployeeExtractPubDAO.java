/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;

import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public interface ISingleEmployeeExtractPubDAO<T extends SingleEmployeeExtract, Model extends SingleEmployeeExtractPubModel<T>> extends IUniDao<Model>
{
    void doSendToCoordination(Model model, IPersistentPersonable initiator);

    void doSendToFormative(Model model);

    void doCommit(Model model);
    
    void doRollback(Model model);
    
    void doReject(Model model);
}