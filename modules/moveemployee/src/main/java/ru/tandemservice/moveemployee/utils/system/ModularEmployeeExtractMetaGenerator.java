/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.utils.system;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.dom.DOMComment;
import org.dom4j.dom.DOMDocument;
import org.dom4j.dom.DOMElement;
import org.dom4j.dom.DOMText;

import ru.tandemservice.uni.util.system.UniSystemUtils;
import ru.tandemservice.unimove.service.AcceptVisaExtractService;
import ru.tandemservice.unimove.service.InitCoordinationVisaExtractService;
import ru.tandemservice.unimove.service.RejectVisaExtractService;
import ru.tandemservice.unimv.UnimvDefines;

/**
 * Генерирует описание dao в       moveemployee.modularemplextract.spring.xml
 * Генерирует описание component в moveemployee.modularemplextract.components.xml

 * @author dseleznev
 * Created on: 26.11.2008
 */
public class ModularEmployeeExtractMetaGenerator
{
    private static final File moduleDir = UniSystemUtils.getModulesDir();

    public static void main(String[] args)
    {
        generateSpringDescription();

        generateComponentDescription();
    }

    private static void generateSpringDescription()
    {
        Document document = createDaoDocument("ru.tandemservice.moveemployee.component.modularemplextract.e");

        String dir = moduleDir.getAbsolutePath() + "/moveemployee/src/main/resources/moveemployee/";
        String fileName = "moveemployee.modularemplextract.spring.xml";

        UniSystemUtils.writeDocument(document, dir, fileName);
    }

    private static void generateComponentDescription()
    {
        Document document = createComponentDocument();

        String dir = moduleDir.getAbsolutePath() + "/moveemployee/src/main/resources/moveemployee/";
        String fileName = "moveemployee.modularemplextract.components.xml";

        UniSystemUtils.writeDocument(document, dir, fileName);

    }

    private static Document createDaoDocument(String extractPackage)
    {
        Document document = new DOMDocument();
        document.setXMLEncoding("UTF-8");
        document.addComment("DO NOT EDIT");
        document.addComment("Autogenerated by " + ModularEmployeeExtractMetaGenerator.class.getName());
        Namespace namespace = Namespace.get("http://www.springframework.org/schema/beans");
        Namespace tandemUtil = Namespace.get("tandem-util", "http://www.tandemframework.org/schema/tandem-util");

        Element rootElement = new DOMElement("beans");
        rootElement.add(namespace);
        rootElement.add(Namespace.get("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
        rootElement.add(tandemUtil);
        rootElement.add(Namespace.get("util", "http://www.springframework.org/schema/util"));
        rootElement.addAttribute("xsi:schemaLocation", "http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd http://www.tandemframework.org/schema/tandem-util http://www.tandemframework.org/schema/tandem-util/spring-tandem-util.xsd http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-2.5.xsd");
        rootElement.addAttribute("default-init-method", "init");
        document.add(rootElement);

        // create dao descriptions

        for (int i = 0; i < ExtractData.EXTRACT_LIST.length; i++)
        {
            String extractName = ExtractData.EXTRACT_LIST[i][0];
            int entityIndex = i + 1;

            DOMComment comment = new DOMComment(entityIndex + ". " + ExtractData.EXTRACT_LIST[i][1]);
            rootElement.add(comment);

            // add print

            Element bean = new DOMElement("bean", namespace);
            bean.addAttribute("id", extractName + "_extractPrint");
            bean.addAttribute("class", "ru.tandemservice.moveemployee.component.modularemplextract.e" + entityIndex + "." + StringUtils.capitalize(extractName) + "Print");
            rootElement.add(bean);

            // add dao

            bean = new DOMElement("bean", namespace);
            bean.addAttribute("id", extractName + "_extractDao");
            bean.addAttribute("parent", "baseTransactionProxy");

            //add proxyInterfaces
            {
                Element property = new DOMElement("commonbase/property", namespace);
                property.addAttribute("name", "proxyInterfaces");

                Element list = new DOMElement("list", namespace);

                Element value = new DOMElement("value", namespace);
                value.add(new DOMText("ru.tandemservice.unimove.dao.IExtractComponentDao"));

                list.add(value);
                property.add(list);
                bean.add(property);
            }

            //add target
            {
                Element property = new DOMElement("commonbase/property", namespace);
                property.addAttribute("name", "target");

                Element beanTarget = new DOMElement("bean", namespace);
                beanTarget.addAttribute("class", extractPackage + (i + 1) + "." + StringUtils.capitalize(extractName) + "Dao");

                Element beanProperty = new DOMElement("commonbase/property", namespace);
                beanProperty.addAttribute("name", "sessionFactory");
                beanProperty.addAttribute("ref", "sessionFactory");

                beanTarget.add(beanProperty);
                property.add(beanTarget);
                bean.add(property);
            }
            rootElement.add(bean);
        }

        // visa service description

        rootElement.add(createSetElement(UnimvDefines.VISING_ENTITY_SET, tandemUtil, namespace));
        rootElement.add(createMapElement(UnimvDefines.VISA_INIT_SERVICE_MAP, InitCoordinationVisaExtractService.class.getSimpleName(), tandemUtil, namespace));
        rootElement.add(createMapElement(UnimvDefines.VISA_ACCEPT_SERVICE_MAP, AcceptVisaExtractService.class.getSimpleName(), tandemUtil, namespace));
        rootElement.add(createMapElement(UnimvDefines.VISA_REJECT_SERVICE_MAP, RejectVisaExtractService.class.getSimpleName(), tandemUtil, namespace));

        return document;
    }

    private static Element createSetElement(String setName, Namespace tandemUtil, Namespace namespace)
    {
        Element element = new DOMElement("set", tandemUtil);
        element.addAttribute("name", setName);

        for (int i = 0; i < ExtractData.EXTRACT_LIST.length; i++)
        {
            String extractName = ExtractData.EXTRACT_LIST[i][0];
            Element entry = new DOMElement("value", namespace);
            entry.addText(extractName);
            element.add(entry);
        }

        return element;
    }

    private static Element createMapElement(String mapName, String className, Namespace tandemUtil, Namespace namespace)
    {
        Element element = new DOMElement("map", tandemUtil);
        element.addAttribute("name", mapName);

        for (int i = 0; i < ExtractData.EXTRACT_LIST.length; i++)
        {
            String extractName = ExtractData.EXTRACT_LIST[i][0];

            Element entry = new DOMElement("entry", namespace);
            entry.addAttribute("key", extractName);

            Element bean = new DOMElement("ref", namespace);
            bean.addAttribute("bean", StringUtils.uncapitalize(className));

            entry.add(bean);
            element.add(entry);
        }

        return element;
    }

    private static Document createComponentDocument()
    {
        Document document = new DOMDocument();
        document.setXMLEncoding("UTF-8");
        document.addComment("DO NOT EDIT");
        document.addComment("Autogenerated by " + ModularEmployeeExtractMetaGenerator.class.getName());
        Namespace namespace = Namespace.get("http://www.tandemframework.org/meta/components");

        Element rootElement = new DOMElement("components-config");
        rootElement.add(namespace);
        rootElement.add(Namespace.get("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
        rootElement.add(Namespace.get("components", "http://www.tandemframework.org/meta/components"));
        rootElement.addAttribute("xsi:schemaLocation", "http://www.tandemframework.org/meta/components http://www.tandemframework.org/schema/meta/meta-components.xsd");
        rootElement.addAttribute("name", "moveemployee-modularextract-components");
        rootElement.addAttribute("label", "Компоненты выписок из сборного приказа");
        document.add(rootElement);
       
        for (int i = 0; i < ExtractData.EXTRACT_LIST.length; i++)
        {
            String extractName = ExtractData.EXTRACT_LIST[i][0];
            int entityIndex = i + 1;

            DOMComment comment = new DOMComment(entityIndex + ". " + ExtractData.EXTRACT_LIST[i][1]);
            rootElement.add(comment);

            Element entityView = new DOMElement("publisher", namespace);
            entityView.addAttribute("name", extractName);
            entityView.addAttribute("component", "ru.tandemservice.moveemployee.component.modularemplextract.e" + entityIndex + ".Pub");
            rootElement.add(entityView);
        }

        for (int i = 0; i < ExtractData.EXTRACT_LIST.length; i++)
        {
            int entityIndex = i + 1;

            // AddEdit
            {
                Element component = new DOMElement("component", namespace);
                component.addAttribute("package", "ru.tandemservice.moveemployee.component.modularemplextract.e" + entityIndex + ".AddEdit");
                component.addAttribute("title", "выписки «" + ExtractData.EXTRACT_LIST[i][1] + "»");

                Element description = new DOMElement("description", namespace);
                description.addText(entityIndex + ". " + ExtractData.EXTRACT_LIST[i][1] + " " + ExtractData.EXTRACT_LIST[i][0]);
                component.add(description);

                rootElement.add(component);
            }
            // Pub
            {
                Element component = new DOMElement("component", namespace);
                component.addAttribute("package", "ru.tandemservice.moveemployee.component.modularemplextract.e" + entityIndex + ".Pub");
                component.addAttribute("title", "Выписка «" + ExtractData.EXTRACT_LIST[i][1] + "»");

                Element description = new DOMElement("description", namespace);
                description.addText(entityIndex + ". " + ExtractData.EXTRACT_LIST[i][1] + " " + ExtractData.EXTRACT_LIST[i][0]);
                component.add(description);

                Element extpoint = new DOMElement("ext-point-def", namespace);
                extpoint.addAttribute("name", "extractTabList");
                extpoint.addAttribute("type", "tabList");

                component.add(extpoint);
                rootElement.add(component);
            }
        }

        return document;
    }
}
