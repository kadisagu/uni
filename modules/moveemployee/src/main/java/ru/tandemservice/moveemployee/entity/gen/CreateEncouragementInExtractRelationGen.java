package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.CreateEncouragementInExtractRelation;
import ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь созданных Поощрений/наград при проведении выписки «О поощрении работника» и самой выписки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CreateEncouragementInExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.CreateEncouragementInExtractRelation";
    public static final String ENTITY_NAME = "createEncouragementInExtractRelation";
    public static final int VERSION_HASH = -554596954;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_ENCOURAGEMENT_S_EXTRACT = "employeeEncouragementSExtract";
    public static final String L_EMPLOYEE_ENCOURAGEMENT = "employeeEncouragement";

    private EmployeeEncouragementSExtract _employeeEncouragementSExtract;     // Выписка из индивидуального приказа по кадровому составу. О поощрении работника
    private EmployeeEncouragement _employeeEncouragement;     // Поощрение/награда

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из индивидуального приказа по кадровому составу. О поощрении работника. Свойство не может быть null.
     */
    @NotNull
    public EmployeeEncouragementSExtract getEmployeeEncouragementSExtract()
    {
        return _employeeEncouragementSExtract;
    }

    /**
     * @param employeeEncouragementSExtract Выписка из индивидуального приказа по кадровому составу. О поощрении работника. Свойство не может быть null.
     */
    public void setEmployeeEncouragementSExtract(EmployeeEncouragementSExtract employeeEncouragementSExtract)
    {
        dirty(_employeeEncouragementSExtract, employeeEncouragementSExtract);
        _employeeEncouragementSExtract = employeeEncouragementSExtract;
    }

    /**
     * @return Поощрение/награда. Свойство не может быть null.
     */
    @NotNull
    public EmployeeEncouragement getEmployeeEncouragement()
    {
        return _employeeEncouragement;
    }

    /**
     * @param employeeEncouragement Поощрение/награда. Свойство не может быть null.
     */
    public void setEmployeeEncouragement(EmployeeEncouragement employeeEncouragement)
    {
        dirty(_employeeEncouragement, employeeEncouragement);
        _employeeEncouragement = employeeEncouragement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CreateEncouragementInExtractRelationGen)
        {
            setEmployeeEncouragementSExtract(((CreateEncouragementInExtractRelation)another).getEmployeeEncouragementSExtract());
            setEmployeeEncouragement(((CreateEncouragementInExtractRelation)another).getEmployeeEncouragement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CreateEncouragementInExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CreateEncouragementInExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new CreateEncouragementInExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeEncouragementSExtract":
                    return obj.getEmployeeEncouragementSExtract();
                case "employeeEncouragement":
                    return obj.getEmployeeEncouragement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeEncouragementSExtract":
                    obj.setEmployeeEncouragementSExtract((EmployeeEncouragementSExtract) value);
                    return;
                case "employeeEncouragement":
                    obj.setEmployeeEncouragement((EmployeeEncouragement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeEncouragementSExtract":
                        return true;
                case "employeeEncouragement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeEncouragementSExtract":
                    return true;
                case "employeeEncouragement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeEncouragementSExtract":
                    return EmployeeEncouragementSExtract.class;
                case "employeeEncouragement":
                    return EmployeeEncouragement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CreateEncouragementInExtractRelation> _dslPath = new Path<CreateEncouragementInExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CreateEncouragementInExtractRelation");
    }
            

    /**
     * @return Выписка из индивидуального приказа по кадровому составу. О поощрении работника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreateEncouragementInExtractRelation#getEmployeeEncouragementSExtract()
     */
    public static EmployeeEncouragementSExtract.Path<EmployeeEncouragementSExtract> employeeEncouragementSExtract()
    {
        return _dslPath.employeeEncouragementSExtract();
    }

    /**
     * @return Поощрение/награда. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreateEncouragementInExtractRelation#getEmployeeEncouragement()
     */
    public static EmployeeEncouragement.Path<EmployeeEncouragement> employeeEncouragement()
    {
        return _dslPath.employeeEncouragement();
    }

    public static class Path<E extends CreateEncouragementInExtractRelation> extends EntityPath<E>
    {
        private EmployeeEncouragementSExtract.Path<EmployeeEncouragementSExtract> _employeeEncouragementSExtract;
        private EmployeeEncouragement.Path<EmployeeEncouragement> _employeeEncouragement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из индивидуального приказа по кадровому составу. О поощрении работника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreateEncouragementInExtractRelation#getEmployeeEncouragementSExtract()
     */
        public EmployeeEncouragementSExtract.Path<EmployeeEncouragementSExtract> employeeEncouragementSExtract()
        {
            if(_employeeEncouragementSExtract == null )
                _employeeEncouragementSExtract = new EmployeeEncouragementSExtract.Path<EmployeeEncouragementSExtract>(L_EMPLOYEE_ENCOURAGEMENT_S_EXTRACT, this);
            return _employeeEncouragementSExtract;
        }

    /**
     * @return Поощрение/награда. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreateEncouragementInExtractRelation#getEmployeeEncouragement()
     */
        public EmployeeEncouragement.Path<EmployeeEncouragement> employeeEncouragement()
        {
            if(_employeeEncouragement == null )
                _employeeEncouragement = new EmployeeEncouragement.Path<EmployeeEncouragement>(L_EMPLOYEE_ENCOURAGEMENT, this);
            return _employeeEncouragement;
        }

        public Class getEntityClass()
        {
            return CreateEncouragementInExtractRelation.class;
        }

        public String getEntityName()
        {
            return "createEncouragementInExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
