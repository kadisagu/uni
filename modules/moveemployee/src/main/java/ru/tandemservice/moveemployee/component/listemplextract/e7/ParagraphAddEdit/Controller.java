/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e7.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract;

import java.math.BigDecimal;
import java.util.*;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class Controller extends AbstractListParagraphAddEditController<PayForHolidayDayEmplListExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareEmployeeDataSource(component);
    }

    private void prepareEmployeeDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getEmployeeDataSource() != null)
            return;

        DynamicListDataSource<EmployeePost> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareEmployeeDataSource(model);
        });

        CheckboxColumn checkboxColumn = new CheckboxColumn("checkColumn");
        if (model.getSelectEmployee() != null)
        {
            List<IEntity> entityList = new ArrayList<>();
            for (EmployeePost employeePost : model.getSelectEmployee())
                entityList.add(employeePost);
            checkboxColumn.setSelectedObjects(entityList);
        }
        dataSource.addColumn(checkboxColumn);
        dataSource.addColumn(new SimpleColumn("Табельный номер", EmployeePost.employee().employeeCode().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ФИО", EmployeePost.person().fullFio().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Должность", EmployeePost.postRelation().postBoundedWithQGandQL().fullTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", new String[]{EmployeePost.L_ORG_UNIT, OrgUnit.P_TITLE_WITH_TYPE}).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип назначения на должность", EmployeePost.postType().title().s()).setClickable(false));
        dataSource.addColumn(new BlockColumn("workTime","Часы работы в нерабочий день").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("timeAmount", "Количество часов").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("nightTime", "В том числе ночных").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("workType", "Вид работ").setClickable(false).setOrderable(false));

        model.setEmployeeDataSource(dataSource);
    }

    public void refreshEmployeeList(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setTempEmployeeList(model.getEmployeeDataSource().getEntityList());
        model.getEmployeeDataSource().refresh();
    }

    public void onChangeWorkTime(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder workTimeHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("workTime");
        final Map<Long, String> workTimeMap = (null == workTimeHolder ? Collections.emptyMap() : workTimeHolder.getValueMap());

        final IValueMapHolder timeAmountHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("timeAmount");
        final Map<Long, Double> timeAmountMap = (null == timeAmountHolder ? Collections.emptyMap() : timeAmountHolder.getValueMap());

        final IValueMapHolder nightTimeAmountHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("nightTime");
        final Map<Long, Double> nightTimeAmountMap = (null == nightTimeAmountHolder ? Collections.emptyMap() : nightTimeAmountHolder.getValueMap());

        String str = workTimeMap.get(component.getListenerParameter());
        if (str == null)
            return;

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        StringBuilder workTimeBuilder = new StringBuilder(str);

        Calendar beginDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();

        Integer hourBegin = Integer.valueOf(workTimeBuilder.substring(0, 2));
        Integer minuteBegin = Integer.valueOf(workTimeBuilder.substring(3, 5));
        Integer hourEnd = Integer.valueOf(workTimeBuilder.substring(8, 10));
        Integer minuteEnd = Integer.valueOf(workTimeBuilder.substring(11, 13));

        if ((hourBegin > 24 || hourBegin < 0 || hourEnd > 24 || hourEnd < 0) ||
                (minuteBegin > 59 || minuteBegin < 0 || minuteEnd > 59 || minuteEnd < 0) ||
                (hourBegin == 24 && minuteBegin != 0) || (hourEnd == 24 && minuteEnd != 0))
        {
            workTimeMap.put(component.<Long>getListenerParameter(), "00:00 - 00:00");
            timeAmountMap.put(component.<Long>getListenerParameter(), 0d);
            nightTimeAmountMap.put(component.<Long>getListenerParameter(), 0d);
            errorCollector.add("Время введено некорректно.", "workTimeId_" + component.getListenerParameter());
//            errorCollector.hasErrors();
            return;
        }

        if (hourBegin == 24 && minuteBegin == 0)
        {
            hourBegin = 23;
            minuteBegin = 59;
        }
        if (hourEnd == 24 && minuteEnd == 0)
        {
            hourEnd = 23;
            minuteEnd = 59;
        }

        beginDate.set(Calendar.HOUR_OF_DAY, hourBegin);
        beginDate.set(Calendar.MINUTE, minuteBegin);
        beginDate.set(Calendar.SECOND, 0);
        beginDate.set(Calendar.MILLISECOND, 0);

        endDate.set(Calendar.HOUR_OF_DAY, hourEnd);
        endDate.set(Calendar.MINUTE, minuteEnd);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);

        if (beginDate.getTime().getTime() > endDate.getTime().getTime())
        {
            workTimeMap.put(component.<Long>getListenerParameter(), "00:00 - 00:00");
            timeAmountMap.put(component.<Long>getListenerParameter(), 0d);
            nightTimeAmountMap.put(component.<Long>getListenerParameter(), 0d);
            errorCollector.add("Время окончания работы должно быть больше времени начала.", "workTimeId_" + component.getListenerParameter());
//            errorCollector.hasErrors();
            return;
        }


        //вычесляем количество часов всего
        double hourDiff = CommonBaseDateUtil.getBetweenPeriod(beginDate.getTime(), endDate.getTime(), Calendar.HOUR_OF_DAY);
        BigDecimal diff = new BigDecimal(hourDiff);
        diff = diff.setScale(1, BigDecimal.ROUND_HALF_UP);
        Double timeAmount = diff.doubleValue();

        timeAmountMap.put(component.<Long>getListenerParameter(), timeAmount);

        Calendar time22 = Calendar.getInstance();
        time22.set(Calendar.HOUR_OF_DAY, 22);
        time22.set(Calendar.MINUTE, 0);
        time22.set(Calendar.SECOND, 0);
        time22.set(Calendar.MILLISECOND, 0);
        Calendar time6 = Calendar.getInstance();
        time6.set(Calendar.HOUR_OF_DAY, 6);
        time6.set(Calendar.MINUTE, 0);
        time6.set(Calendar.SECOND, 0);
        time6.set(Calendar.MILLISECOND, 0);

        //вычесляем количество ночных часов
        if (CommonBaseDateUtil.isBetween(beginDate.getTime(), time6.getTime(), time22.getTime()) && CommonBaseDateUtil.isBetween(endDate.getTime(), time6.getTime(), time22.getTime()))
            nightTimeAmountMap.put(component.<Long>getListenerParameter(), 0d);

        else if (endDate.getTime().getTime() < time6.getTime().getTime())
            nightTimeAmountMap.put(component.<Long>getListenerParameter(), timeAmount);
        else if (beginDate.getTime().getTime() > time22.getTime().getTime())
            nightTimeAmountMap.put(component.<Long>getListenerParameter(), timeAmount);

        else {
            if (CommonBaseDateUtil.isBetween(beginDate.getTime(), time6.getTime(), time22.getTime()) && !CommonBaseDateUtil.isBetween(endDate.getTime(), time6.getTime(), time22.getTime()))
            {
                hourDiff = CommonBaseDateUtil.getBetweenPeriod(time22.getTime(), endDate.getTime(), Calendar.HOUR_OF_DAY);
                diff = new BigDecimal(hourDiff);
                diff = diff.setScale(1, BigDecimal.ROUND_HALF_UP);
                Double timeNightAmount = diff.doubleValue();
                nightTimeAmountMap.put(component.<Long>getListenerParameter(), timeNightAmount);
            }
            else {
                if (!CommonBaseDateUtil.isBetween(beginDate.getTime(), time6.getTime(), time22.getTime()) && CommonBaseDateUtil.isBetween(endDate.getTime(), time6.getTime(), time22.getTime())) {
                    hourDiff = CommonBaseDateUtil.getBetweenPeriod(beginDate.getTime(), time6.getTime(), Calendar.HOUR_OF_DAY);
                    diff = new BigDecimal(hourDiff);
                    diff = diff.setScale(1, BigDecimal.ROUND_HALF_UP);

                    Double timeNightAmount = diff.doubleValue();
                    nightTimeAmountMap.put(component.<Long>getListenerParameter(), timeNightAmount);
                }

                else if (beginDate.getTime().getTime() < time6.getTime().getTime() && endDate.getTime().getTime() > time22.getTime().getTime()) {
                    double firstHourDiff = CommonBaseDateUtil.getBetweenPeriod(beginDate.getTime(), time6.getTime(), Calendar.HOUR_OF_DAY);
                    BigDecimal firstDiff = new BigDecimal(firstHourDiff);
                    firstDiff = firstDiff.setScale(2, BigDecimal.ROUND_HALF_UP);
                    Double firstTimeNightAmount = firstDiff.doubleValue();

                    double secondHourDiff = CommonBaseDateUtil.getBetweenPeriod(time22.getTime(), endDate.getTime(), Calendar.HOUR_OF_DAY);
                    BigDecimal secondDiff = new BigDecimal(secondHourDiff);
                    secondDiff = secondDiff.setScale(2, BigDecimal.ROUND_HALF_UP);
                    Double secondTimeNightAmount = secondDiff.doubleValue();

                    diff = new BigDecimal(firstTimeNightAmount + secondTimeNightAmount);
                    diff = diff.setScale(1, BigDecimal.ROUND_HALF_UP);
                    Double timeNightAmount = diff.doubleValue();

                    nightTimeAmountMap.put(component.<Long>getListenerParameter(), timeNightAmount);
                }
            }
        }
    }
}
