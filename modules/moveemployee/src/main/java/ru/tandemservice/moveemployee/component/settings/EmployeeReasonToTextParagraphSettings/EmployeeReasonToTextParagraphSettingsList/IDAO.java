/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeReasonToTextParagraphSettings.EmployeeReasonToTextParagraphSettingsList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * Create by: ashaburov
 * Date: 18.04.11
 */
public interface IDAO extends IUniDao<Model>
{
}
