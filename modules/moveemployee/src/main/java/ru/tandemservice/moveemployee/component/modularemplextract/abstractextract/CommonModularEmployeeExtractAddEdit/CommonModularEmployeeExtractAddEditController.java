/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractAddEdit.ModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * @author dseleznev
 * Created on: 13.11.2008
 */
public abstract class CommonModularEmployeeExtractAddEditController<T extends ModularEmployeeExtract, IDAO extends ICommonModularEmployeeExtractAddEditDAO<T, Model>, Model extends CommonModularEmployeeExtractAddEditModel<T>> extends ModularEmployeeExtractAddEditController<T, IDAO, Model>
{
    public void onChangeReason(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepareTextParagraph(model);
    }

    public void onChangeBasics(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeLabourContract contract = getDao().get(EmployeeLabourContract.class, EmployeeLabourContract.employeePost().s(), model.getEmployeePost());

        if (contract == null)
            return;

        for (EmployeeOrderBasics basic : model.getSelectedBasicList())
        {
            if (basic.isLaborContract() && model.getCurrentBasicMap().get(basic.getId()) == null)
            {
                StringBuilder basicBuilder = new StringBuilder("№").append(contract.getNumber()).append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getDate()));
                model.getCurrentBasicMap().put(basic.getId(), basicBuilder.toString());
            }
        }
    }
}