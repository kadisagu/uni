/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.employee.EmployeeOrdersPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.component.commons.MoveEmployeeColumns;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

/**
 * @author dseleznev
 *         Created on: 07.11.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getModel(component).setEmployee((Employee) model.getPersonRoleModel().getSecuredObject());

        model.getModularProjectsModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveEmployee_projects.filter"));
        model.getModularExtractsModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveEmployee_extracts.filter"));
        model.getSingleProjectsModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveEmployee_sprojects.filter"));
        model.getSingleExtractsModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveEmployee_sextracts.filter"));
        model.getListOrdersModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveEmployee_lorders.filter"));
        model.getOrdersModel().setSettings(UniBaseUtils.getDataSettings(component, "MoveEmployee_orders.filter"));

        model.getModularProjectsModel().setSecModel(new CommonPostfixPermissionModel("employee_projects"));
        model.getModularExtractsModel().setSecModel(new CommonPostfixPermissionModel("employee_extracts"));
        model.getSingleProjectsModel().setSecModel(new CommonPostfixPermissionModel("employee_sprojects"));
        model.getSingleExtractsModel().setSecModel(new CommonPostfixPermissionModel("employee_sextracts"));
        model.getListOrdersModel().setSecModel(new CommonPostfixPermissionModel("employee_lorders"));
        model.getOrdersModel().setSecModel(new CommonPostfixPermissionModel("employee_orders"));

        getDao().prepare(getModel(component));

        prepareModularProjectsDataSource(component);
        prepareModularExtractsDataSource(component);
        prepareSingleProjectsDataSource(component);
        prepareSingleExtractsDataSource(component);
        prepareListOrdersDataSource(component);
        prepareOrdersDataSource(model, component);
    }

    @SuppressWarnings("unchecked")
    public DynamicListDataSource<IEntity> prepareCommonDataSource(IBusinessComponent component, final int updateMethodIndex)
    {
        final IDAO dao = getDao();
        final Model model = getModel(component);

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            dao.prepareCustomDataSource(model, updateMethodIndex);
        });

        dataSource.addColumn(MoveEmployeeColumns.getCreateDateColumn().setOrderable(false));
        return dataSource;
    }

    @SuppressWarnings("unchecked")
    public DynamicListDataSource<IEntity> prepareModularDataSource(IBusinessComponent component, final int updateMethodIndex)
    {
        final IDAO dao = getDao();
        final Model model = getModel(component);

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            dao.prepareCustomDataSource(model, updateMethodIndex);
        });

        //при переходе по ссылке выписки передаются различные параметры,
        //параметры определяют отображать выписку как приказ или нет
        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", IAbstractOrder.P_CREATE_DATE);
        linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                ModularEmployeeExtract extract = getDao().get(ModularEmployeeExtract.class, entity.getId());
                return MoveEmployeeUtils.getModularEmployeeExtractPubComponent(extract.getType().getIndex());
            }

            @Override
            public Object getParameters(IEntity entity)
            {
                ModularEmployeeExtract extract = getDao().get(ModularEmployeeExtract.class, entity.getId());
                if (MoveEmployeeDaoFacade.getMoveEmployeeDao().isExtractIndividual(extract) &&
                        (extract.getParagraph() != null && extract.getParagraph().getOrder() != null && extract.getParagraph().getOrder().getParagraphCount() == 1))
                    return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, extract.getId()).
                            add("extractIndividual", true);
                else
                    return entity.getId();
            }
        });
        dataSource.addColumn(linkColumn);
        //dataSource.addColumn(MoveEmployeeColumns.getCreateDateColumn().setOrderable(false));
        return dataSource;
    }

    public void prepareModularProjectsDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getModularProjectsModel().getDataSource() != null) return;

        DynamicListDataSource<IEntity> dataSource = prepareModularDataSource(component, IDAO.MODULAR_PROJECTS_DATASOUCE_IDX);
        dataSource.addColumn(MoveEmployeeColumns.getExtractTypeColumn("Тип приказа").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractStateColumn("Состояние проекта приказа").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractPrintColumn(this, model.getModularProjectsModel().getSecModel().getPermission("print_extract")));
        // Disable edit for archived employee
        if (getModel(component).isAccessible())
        {
            dataSource.addColumn(MoveEmployeeColumns.getExtractEditColumn(this, model.getModularProjectsModel().getSecModel().getPermission("edit_extract")));
            dataSource.addColumn(MoveEmployeeColumns.getExtractDeleteColumn(this, model.getModularProjectsModel().getSecModel().getPermission("delete_extract")));
        }
        model.getModularProjectsModel().setDataSource(dataSource);
    }

    public void prepareModularExtractsDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getModularExtractsModel().getDataSource() != null) return;
        DynamicListDataSource<IEntity> dataSource = prepareModularDataSource(component, IDAO.MODULAR_EXTRACTS_DATASOUCE_IDX);
        dataSource.addColumn(MoveEmployeeColumns.getExtractOrderNumberColumn().setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getModularExtractNumberColumn());
        dataSource.addColumn(MoveEmployeeColumns.getExtractCommitDateColumn().setOrderable(true));
        dataSource.addColumn(MoveEmployeeColumns.getExtractCommitDateSystemColumn().setOrderable(true));
        dataSource.addColumn(MoveEmployeeColumns.getExtractTypeColumn("Тип приказа").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractStateColumn("Состояние выписки").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractPrintColumn(this, model.getModularExtractsModel().getSecModel().getPermission("print_extract")));
        dataSource.addColumn(MoveEmployeeColumns.getExtractDeleteColumn(this, model.getModularExtractsModel().getSecModel().getPermission("delete_extract")));
        dataSource.setOrder(MoveEmployeeColumns.COMMIT_DATE_SYSTEM_COLUMN, OrderDirection.desc);
        model.getModularExtractsModel().setDataSource(dataSource);
    }

    public void prepareSingleProjectsDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getSingleProjectsModel().getDataSource() != null) return;

        DynamicListDataSource<IEntity> dataSource = prepareCommonDataSource(component, IDAO.SINGLE_PROJECTS_DATASOUCE_IDX);
        dataSource.addColumn(MoveEmployeeColumns.getExtractOrderNumberColumn().setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractTypeColumn("Тип приказа").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractOrderStateColumn("Состояние приказа").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractPrintColumn(this, model.getSingleProjectsModel().getSecModel().getPermission("print_extract")));
        // Disable edit for archived employee
        if (getModel(component).isAccessible())
        {
            dataSource.addColumn(MoveEmployeeColumns.getExtractEditColumn(this, model.getSingleProjectsModel().getSecModel().getPermission("edit_extract")));
            dataSource.addColumn(MoveEmployeeColumns.getExtractDeleteColumn(this, model.getSingleProjectsModel().getSecModel().getPermission("delete_extract")));
        }

        model.getSingleProjectsModel().setDataSource(dataSource);
    }

    public void prepareSingleExtractsDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getSingleExtractsModel().getDataSource() != null) return;
        DynamicListDataSource<IEntity> dataSource = prepareCommonDataSource(component, IDAO.SINGLE_EXTRACTS_DATASOUCE_IDX);
        dataSource.addColumn(MoveEmployeeColumns.getExtractOrderNumberColumn().setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractCommitDateColumn().setOrderable(true));
        dataSource.addColumn(MoveEmployeeColumns.getExtractCommitDateSystemColumn().setOrderable(true));
        dataSource.addColumn(MoveEmployeeColumns.getExtractTypeColumn("Тип приказа").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractOrderStateColumn("Состояние приказа").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractPrintColumn(this, model.getSingleExtractsModel().getSecModel().getPermission("print_extract")));
        dataSource.addColumn(MoveEmployeeColumns.getExtractDeleteColumn(this, model.getSingleExtractsModel().getSecModel().getPermission("delete_extract")));
        dataSource.setOrder(MoveEmployeeColumns.COMMIT_DATE_SYSTEM_COLUMN, OrderDirection.desc);
        model.getSingleExtractsModel().setDataSource(dataSource);
    }

    public void prepareListOrdersDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getListOrdersModel().getDataSource() != null) return;
        DynamicListDataSource<IEntity> dataSource = prepareCommonDataSource(component, IDAO.LIST_ORDERS_DATASOUCE_IDX);
        dataSource.addColumn(MoveEmployeeColumns.getExtractOrderNumberColumn().setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractCommitDateColumn().setOrderable(true));
        dataSource.addColumn(MoveEmployeeColumns.getExtractCommitDateSystemColumn().setOrderable(true));
        dataSource.addColumn(MoveEmployeeColumns.getExtractTypeColumn("Тип приказа").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractOrderStateColumn("Состояние приказа").setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getOrderPrintColumn(this, model.getListOrdersModel().getSecModel().getPermission("print_extract")));
        dataSource.addColumn(MoveEmployeeColumns.getExtractDeleteColumn(this, model.getListOrdersModel().getSecModel().getPermission("delete_extract")));
        dataSource.setOrder(MoveEmployeeColumns.COMMIT_DATE_SYSTEM_COLUMN, OrderDirection.desc);
        model.getListOrdersModel().setDataSource(dataSource);
    }

    private void prepareOrdersDataSource(final Model model, IBusinessComponent component)
    {
        if (model.getOrdersModel().getDataSource() != null) return;

        DynamicListDataSource<AbstractEmployeeExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareOrdersDataSource(model);
        });
        @SuppressWarnings("unchecked")
        AbstractColumn linkColumn = new PublisherLinkColumn("Тип приказа", new String[]{IAbstractExtract.L_TYPE, ICatalogItem.CATALOG_ITEM_TITLE}).setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                IAbstractExtract extract = (IAbstractExtract) ((ViewWrapper) entity).getEntity();
                if (extract instanceof ModularEmployeeExtract || extract instanceof SingleEmployeeExtract) return null;
                // int index = ((EmployeeExtractType) extract.getType()).getIndex();
                //if (extract instanceof ListEmployeeStudentExtract) return MoveStudentUtils.getListExtractPubComponent(index);
                return null;
            }
        }).setOrderable(false);
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(MoveEmployeeColumns.getExtractCommitDateColumn());
        dataSource.addColumn(MoveEmployeeColumns.getExtractOrderNumberColumn());
        dataSource.addColumn(MoveEmployeeColumns.getExtractCommitDateSystemColumn());
        dataSource.addColumn(new SimpleColumn("№ параграфа приказа", IDAO.P_PARAGRAPH_NUMBER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("№ пункта приказа", IDAO.P_EXTRACT_NUMBER).setClickable(false).setOrderable(false));
        dataSource.addColumn(MoveEmployeeColumns.getExtractPrintColumn(this, model.getOrdersModel().getSecModel().getPermission("print_extract")));
        dataSource.setOrder(MoveEmployeeColumns.COMMIT_DATE_SYSTEM_COLUMN, OrderDirection.desc);
        model.getOrdersModel().setDataSource(dataSource);
    }

    public void onClickAddExtract(IBusinessComponent component)
    {
        component.createChildRegion("moveEmployee", new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_POST_MODULAR_EXTRACTS_ADD, new ParametersMap().add("employeePostId", null).add("employeeId", getModel(component).getEmployee().getId()).add("extractAsOrder", false)));
    }

    public void onClickAddExtractAsOrder(IBusinessComponent component)
    {
        component.createChildRegion("moveEmployee", new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_POST_MODULAR_EXTRACTS_ADD, new ParametersMap().add("employeePostId", null).add("employeeId", getModel(component).getEmployee().getId()).add("extractAsOrder", true)));
    }

    public void onClickAddSExtract(IBusinessComponent component)
    {
        component.createChildRegion("moveEmployee", new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_POST_SINGLE_EXTRACTS_ADD, new ParametersMap().add("employeePostId", null).add("employeeId", getModel(component).getEmployee().getId())));
    }

    public void onClickExtractEdit(IBusinessComponent component)
    {
        Long extractId = (Long) component.getListenerParameter();
        AbstractEmployeeExtract extract = getDao().get(extractId);
        component.createChildRegion("moveEmployee", new ComponentActivator(MoveEmployeeUtils.getExtractAddEditComponent(extract.getType()), new ParametersMap().add("extractId", extractId)));
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        Long extractId = (Long) component.getListenerParameter();
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.MODULAR_EMPLOYEE_EXTRACT_PRINT, new ParametersMap().add("extractId", extractId)));
    }

    public void onClickOrderPrint(IBusinessComponent component)
    {
        ListEmployeeExtract extract = getDao().getNotNull((Long) component.getListenerParameter());
        AbstractEmployeeOrder order = extract.getParagraph().getOrder();
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_LIST_ORDER_PRINT, new ParametersMap().add("orderId", order.getId())));
    }

    public void onClickExtractDelete(IBusinessComponent component)
    {
        AbstractEmployeeExtract extract = getDao().get((Long) component.getListenerParameter());
        if (extract instanceof ListEmployeeExtract || null == extract.getParagraph())
            MoveDaoFacade.getMoveDao().deleteExtractWithParagraph(extract);
        else
            MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(extract.getParagraph().getOrder());
        getModel(component).setExtractCantBeAdded(MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeeHasFormativeExtracts(getModel(component).getEmployee()));
        getModel(component).setSextractCantBeAdded(MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeeHasFormativeExtracts(getModel(component).getEmployee()));
    }
}