package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.entity.gen.NewPartInTransferAnnualHolidayExtractGen;

/**
 * Объект используемый, если в выписке О переносе ежегодного отпуска выбран -Новый период-
 */
public class NewPartInTransferAnnualHolidayExtract extends NewPartInTransferAnnualHolidayExtractGen
{
}