package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.VacationScheduleItemToEmployeeExtractRelation;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь созданных строк в графике отпусков сотрудника и выписки «Об отпуске»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class VacationScheduleItemToEmployeeExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.VacationScheduleItemToEmployeeExtractRelation";
    public static final String ENTITY_NAME = "vacationScheduleItemToEmployeeExtractRelation";
    public static final int VERSION_HASH = -867470252;
    private static IEntityMeta ENTITY_META;

    public static final String L_ABSTRACT_EMPLOYEE_EXTRACT = "abstractEmployeeExtract";
    public static final String L_VACATION_SCHEDULE_ITEM = "vacationScheduleItem";

    private AbstractEmployeeExtract _abstractEmployeeExtract;     // Выписка по кадрам
    private VacationScheduleItem _vacationScheduleItem;     // Строка графика отпусков

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка по кадрам. Свойство не может быть null.
     */
    @NotNull
    public AbstractEmployeeExtract getAbstractEmployeeExtract()
    {
        return _abstractEmployeeExtract;
    }

    /**
     * @param abstractEmployeeExtract Выписка по кадрам. Свойство не может быть null.
     */
    public void setAbstractEmployeeExtract(AbstractEmployeeExtract abstractEmployeeExtract)
    {
        dirty(_abstractEmployeeExtract, abstractEmployeeExtract);
        _abstractEmployeeExtract = abstractEmployeeExtract;
    }

    /**
     * @return Строка графика отпусков. Свойство не может быть null.
     */
    @NotNull
    public VacationScheduleItem getVacationScheduleItem()
    {
        return _vacationScheduleItem;
    }

    /**
     * @param vacationScheduleItem Строка графика отпусков. Свойство не может быть null.
     */
    public void setVacationScheduleItem(VacationScheduleItem vacationScheduleItem)
    {
        dirty(_vacationScheduleItem, vacationScheduleItem);
        _vacationScheduleItem = vacationScheduleItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof VacationScheduleItemToEmployeeExtractRelationGen)
        {
            setAbstractEmployeeExtract(((VacationScheduleItemToEmployeeExtractRelation)another).getAbstractEmployeeExtract());
            setVacationScheduleItem(((VacationScheduleItemToEmployeeExtractRelation)another).getVacationScheduleItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends VacationScheduleItemToEmployeeExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) VacationScheduleItemToEmployeeExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new VacationScheduleItemToEmployeeExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "abstractEmployeeExtract":
                    return obj.getAbstractEmployeeExtract();
                case "vacationScheduleItem":
                    return obj.getVacationScheduleItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "abstractEmployeeExtract":
                    obj.setAbstractEmployeeExtract((AbstractEmployeeExtract) value);
                    return;
                case "vacationScheduleItem":
                    obj.setVacationScheduleItem((VacationScheduleItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "abstractEmployeeExtract":
                        return true;
                case "vacationScheduleItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "abstractEmployeeExtract":
                    return true;
                case "vacationScheduleItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "abstractEmployeeExtract":
                    return AbstractEmployeeExtract.class;
                case "vacationScheduleItem":
                    return VacationScheduleItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<VacationScheduleItemToEmployeeExtractRelation> _dslPath = new Path<VacationScheduleItemToEmployeeExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "VacationScheduleItemToEmployeeExtractRelation");
    }
            

    /**
     * @return Выписка по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VacationScheduleItemToEmployeeExtractRelation#getAbstractEmployeeExtract()
     */
    public static AbstractEmployeeExtract.Path<AbstractEmployeeExtract> abstractEmployeeExtract()
    {
        return _dslPath.abstractEmployeeExtract();
    }

    /**
     * @return Строка графика отпусков. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VacationScheduleItemToEmployeeExtractRelation#getVacationScheduleItem()
     */
    public static VacationScheduleItem.Path<VacationScheduleItem> vacationScheduleItem()
    {
        return _dslPath.vacationScheduleItem();
    }

    public static class Path<E extends VacationScheduleItemToEmployeeExtractRelation> extends EntityPath<E>
    {
        private AbstractEmployeeExtract.Path<AbstractEmployeeExtract> _abstractEmployeeExtract;
        private VacationScheduleItem.Path<VacationScheduleItem> _vacationScheduleItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VacationScheduleItemToEmployeeExtractRelation#getAbstractEmployeeExtract()
     */
        public AbstractEmployeeExtract.Path<AbstractEmployeeExtract> abstractEmployeeExtract()
        {
            if(_abstractEmployeeExtract == null )
                _abstractEmployeeExtract = new AbstractEmployeeExtract.Path<AbstractEmployeeExtract>(L_ABSTRACT_EMPLOYEE_EXTRACT, this);
            return _abstractEmployeeExtract;
        }

    /**
     * @return Строка графика отпусков. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.VacationScheduleItemToEmployeeExtractRelation#getVacationScheduleItem()
     */
        public VacationScheduleItem.Path<VacationScheduleItem> vacationScheduleItem()
        {
            if(_vacationScheduleItem == null )
                _vacationScheduleItem = new VacationScheduleItem.Path<VacationScheduleItem>(L_VACATION_SCHEDULE_ITEM, this);
            return _vacationScheduleItem;
        }

        public Class getEntityClass()
        {
            return VacationScheduleItemToEmployeeExtractRelation.class;
        }

        public String getEntityName()
        {
            return "vacationScheduleItemToEmployeeExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
