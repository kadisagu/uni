/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e11.AddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPaymentAddModel;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.PaymentAssigningExtract;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<PaymentAssigningExtract> implements IPaymentAddModel<PaymentAssigningExtract>
{
    private final String _paymentsPage = Model.class.getPackage().getName() + ".PaymentBlock";

    private IMultiSelectModel _removePaymentsModel;
    private List<EmployeePayment> _removePaymentsList;

    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;

    private PaymentType _paymentType;

    private List<HSelectOption> _paymentTypesList;
    private List<FinancingSource> _financingSourcesList;
    private ISelectModel _financingSourceItemsListModel;
    private ISelectModel _paymentListModel;

    private EmployeeBonus _newEmployeeBonus;

    private DynamicListDataSource<EmployeeBonus> _bonusesDataSource;
    private List<EmployeeBonus> _bonusesList = new ArrayList<>();
    private List<EmployeeBonus> _bonusesToDel = new ArrayList<>();

    private List<Payment> _alreadyAddedPayments;

    //payments fields
    private DynamicListDataSource<EmployeeBonus> _paymentDataSource;
    private ISingleSelectModel _paymentModel;
    private ISingleSelectModel _finSrcPaymentModel;
    private ISingleSelectModel _finSrcItemPaymentModel;
    private List<EmployeeBonus> _paymentList = new ArrayList<>();
    private boolean _addStaffListPaymentsButtonVisible = false;

    //Calculate

    public String getAssignDateId()
    {
        return "assignDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getBeginDateId()
    {
        return "beginDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getEndDateId()
    {
        return "endDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getPaymentId()
    {
        return "payment_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getAmountId()
    {
        return "amount_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcId()
    {
        return "finSrc_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItemId()
    {
        return "finSrcItem_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }
    //end payments fields


    //Getters & Setters

    public String getPaymentsPage()
    {
        return _paymentsPage;
    }

    public DynamicListDataSource<EmployeeBonus> getPaymentDataSource()
    {
        return _paymentDataSource;
    }

    public void setPaymentDataSource(DynamicListDataSource<EmployeeBonus> paymentDataSource)
    {
        _paymentDataSource = paymentDataSource;
    }

    public ISingleSelectModel getPaymentModel()
    {
        return _paymentModel;
    }

    public void setPaymentModel(ISingleSelectModel paymentModel)
    {
        _paymentModel = paymentModel;
    }

    public ISingleSelectModel getFinSrcPaymentModel()
    {
        return _finSrcPaymentModel;
    }

    public void setFinSrcPaymentModel(ISingleSelectModel finSrcPaymentModel)
    {
        _finSrcPaymentModel = finSrcPaymentModel;
    }

    public ISingleSelectModel getFinSrcItemPaymentModel()
    {
        return _finSrcItemPaymentModel;
    }

    public void setFinSrcItemPaymentModel(ISingleSelectModel finSrcItemPaymentModel)
    {
        _finSrcItemPaymentModel = finSrcItemPaymentModel;
    }

    public List<EmployeeBonus> getPaymentList()
    {
        return _paymentList;
    }

    public void setPaymentList(List<EmployeeBonus> paymentList)
    {
        _paymentList = paymentList;
    }

    public boolean isAddStaffListPaymentsButtonVisible()
    {
        return _addStaffListPaymentsButtonVisible;
    }

    public void setAddStaffListPaymentsButtonVisible(boolean addStaffListPaymentsButtonVisible)
    {
        _addStaffListPaymentsButtonVisible = addStaffListPaymentsButtonVisible;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public List<EmployeePayment> getRemovePaymentsList()
    {
        return _removePaymentsList;
    }

    public void setRemovePaymentsList(List<EmployeePayment> removePaymentsList)
    {
        _removePaymentsList = removePaymentsList;
    }

    public IMultiSelectModel getRemovePaymentsModel()
    {
        return _removePaymentsModel;
    }

    public void setRemovePaymentsModel(IMultiSelectModel removePaymentsModel)
    {
        _removePaymentsModel = removePaymentsModel;
    }

    public PaymentType getPaymentType()
    {
        return _paymentType;
    }

    public void setPaymentType(PaymentType paymentType)
    {
        this._paymentType = paymentType;
    }

    public List<HSelectOption> getPaymentTypesList()
    {
        return _paymentTypesList;
    }

    public void setPaymentTypesList(List<HSelectOption> paymentTypesList)
    {
        this._paymentTypesList = paymentTypesList;
    }

    @Override
    public List<FinancingSource> getFinancingSourcesList()
    {
        return _financingSourcesList;
    }

    @Override
    public void setFinancingSourcesList(List<FinancingSource> financingSourcesList)
    {
        this._financingSourcesList = financingSourcesList;
    }

    @Override
    public ISelectModel getFinancingSourceItemsListModel()
    {
        return _financingSourceItemsListModel;
    }

    @Override
    public void setFinancingSourceItemsListModel(ISelectModel financingSourceItemsListModel)
    {
        this._financingSourceItemsListModel = financingSourceItemsListModel;
    }

    @Override
    public EmployeeBonus getNewEmployeeBonus()
    {
        return _newEmployeeBonus;
    }

    @Override
    public void setNewEmployeeBonus(EmployeeBonus newEmployeeBonus)
    {
        this._newEmployeeBonus = newEmployeeBonus;
    }

    @Override
    public DynamicListDataSource<EmployeeBonus> getBonusesDataSource()
    {
        return _bonusesDataSource;
    }

    @Override
    public void setBonusesDataSource(DynamicListDataSource<EmployeeBonus> bonusesDataSource)
    {
        this._bonusesDataSource = bonusesDataSource;
    }

    @Override
    public List<EmployeeBonus> getBonusesList()
    {
        return _bonusesList;
    }

    @Override
    public void setBonusesList(List<EmployeeBonus> bonusesList)
    {
        this._bonusesList = bonusesList;
    }

    @Override
    public List<EmployeeBonus> getBonusesToDel()
    {
        return _bonusesToDel;
    }

    @Override
    public void setBonusesToDel(List<EmployeeBonus> bonusesToDel)
    {
        this._bonusesToDel = bonusesToDel;
    }

    @Override
    public List<Payment> getAlreadyAddedPayments()
    {
        return _alreadyAddedPayments;
    }

    @Override
    public void setAlreadyAddedPayments(List<Payment> alreadyAddedPayments)
    {
        this._alreadyAddedPayments = alreadyAddedPayments;
    }

    @Override
    public ISelectModel getPaymentListModel()
    {
        return _paymentListModel;
    }

    @Override
    public void setPaymentListModel(ISelectModel paymentListModel)
    {
        this._paymentListModel = paymentListModel;
    }

    @Override
    public boolean isFinancingOrgUnitVisible()
    {
        return CommonEmployeeExtractUtil.isFinancingOrgUnitVisible(this);
    }
}