package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказ по кадрам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AbstractEmployeeOrderGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder";
    public static final String ENTITY_NAME = "abstractEmployeeOrder";
    public static final int VERSION_HASH = 966717948;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_COMMIT_DATE = "commitDate";
    public static final String P_COMMIT_DATE_SYSTEM = "commitDateSystem";
    public static final String P_NUMBER = "number";
    public static final String L_STATE = "state";
    public static final String P_TEXT_PARAGRAPH = "textParagraph";
    public static final String P_EXECUTOR = "executor";

    private Date _createDate;     // Дата формирования
    private Date _commitDate;     // Дата вступления в силу
    private Date _commitDateSystem;     // Дата проведения
    private String _number;     // Номер
    private OrderStates _state;     // Состояние приказа
    private String _textParagraph;     // Текстовый параграф
    private String _executor;     // Исполнитель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null и должно быть уникальным.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата вступления в силу.
     */
    public Date getCommitDate()
    {
        return _commitDate;
    }

    /**
     * @param commitDate Дата вступления в силу.
     */
    public void setCommitDate(Date commitDate)
    {
        dirty(_commitDate, commitDate);
        _commitDate = commitDate;
    }

    /**
     * @return Дата проведения.
     */
    public Date getCommitDateSystem()
    {
        return _commitDateSystem;
    }

    /**
     * @param commitDateSystem Дата проведения.
     */
    public void setCommitDateSystem(Date commitDateSystem)
    {
        dirty(_commitDateSystem, commitDateSystem);
        _commitDateSystem = commitDateSystem;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     */
    @NotNull
    public OrderStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние приказа. Свойство не может быть null.
     */
    public void setState(OrderStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Текстовый параграф.
     */
    public String getTextParagraph()
    {
        return _textParagraph;
    }

    /**
     * @param textParagraph Текстовый параграф.
     */
    public void setTextParagraph(String textParagraph)
    {
        dirty(_textParagraph, textParagraph);
        _textParagraph = textParagraph;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AbstractEmployeeOrderGen)
        {
            setCreateDate(((AbstractEmployeeOrder)another).getCreateDate());
            setCommitDate(((AbstractEmployeeOrder)another).getCommitDate());
            setCommitDateSystem(((AbstractEmployeeOrder)another).getCommitDateSystem());
            setNumber(((AbstractEmployeeOrder)another).getNumber());
            setState(((AbstractEmployeeOrder)another).getState());
            setTextParagraph(((AbstractEmployeeOrder)another).getTextParagraph());
            setExecutor(((AbstractEmployeeOrder)another).getExecutor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AbstractEmployeeOrderGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AbstractEmployeeOrder.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("AbstractEmployeeOrder is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "commitDate":
                    return obj.getCommitDate();
                case "commitDateSystem":
                    return obj.getCommitDateSystem();
                case "number":
                    return obj.getNumber();
                case "state":
                    return obj.getState();
                case "textParagraph":
                    return obj.getTextParagraph();
                case "executor":
                    return obj.getExecutor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "commitDate":
                    obj.setCommitDate((Date) value);
                    return;
                case "commitDateSystem":
                    obj.setCommitDateSystem((Date) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "state":
                    obj.setState((OrderStates) value);
                    return;
                case "textParagraph":
                    obj.setTextParagraph((String) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "commitDate":
                        return true;
                case "commitDateSystem":
                        return true;
                case "number":
                        return true;
                case "state":
                        return true;
                case "textParagraph":
                        return true;
                case "executor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "commitDate":
                    return true;
                case "commitDateSystem":
                    return true;
                case "number":
                    return true;
                case "state":
                    return true;
                case "textParagraph":
                    return true;
                case "executor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "commitDate":
                    return Date.class;
                case "commitDateSystem":
                    return Date.class;
                case "number":
                    return String.class;
                case "state":
                    return OrderStates.class;
                case "textParagraph":
                    return String.class;
                case "executor":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AbstractEmployeeOrder> _dslPath = new Path<AbstractEmployeeOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AbstractEmployeeOrder");
    }
            

    /**
     * @return Дата формирования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getCommitDate()
     */
    public static PropertyPath<Date> commitDate()
    {
        return _dslPath.commitDate();
    }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getCommitDateSystem()
     */
    public static PropertyPath<Date> commitDateSystem()
    {
        return _dslPath.commitDateSystem();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getState()
     */
    public static OrderStates.Path<OrderStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getTextParagraph()
     */
    public static PropertyPath<String> textParagraph()
    {
        return _dslPath.textParagraph();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    public static class Path<E extends AbstractEmployeeOrder> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _commitDate;
        private PropertyPath<Date> _commitDateSystem;
        private PropertyPath<String> _number;
        private OrderStates.Path<OrderStates> _state;
        private PropertyPath<String> _textParagraph;
        private PropertyPath<String> _executor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(AbstractEmployeeOrderGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getCommitDate()
     */
        public PropertyPath<Date> commitDate()
        {
            if(_commitDate == null )
                _commitDate = new PropertyPath<Date>(AbstractEmployeeOrderGen.P_COMMIT_DATE, this);
            return _commitDate;
        }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getCommitDateSystem()
     */
        public PropertyPath<Date> commitDateSystem()
        {
            if(_commitDateSystem == null )
                _commitDateSystem = new PropertyPath<Date>(AbstractEmployeeOrderGen.P_COMMIT_DATE_SYSTEM, this);
            return _commitDateSystem;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(AbstractEmployeeOrderGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getState()
     */
        public OrderStates.Path<OrderStates> state()
        {
            if(_state == null )
                _state = new OrderStates.Path<OrderStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getTextParagraph()
     */
        public PropertyPath<String> textParagraph()
        {
            if(_textParagraph == null )
                _textParagraph = new PropertyPath<String>(AbstractEmployeeOrderGen.P_TEXT_PARAGRAPH, this);
            return _textParagraph;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(AbstractEmployeeOrderGen.P_EXECUTOR, this);
            return _executor;
        }

        public Class getEntityClass()
        {
            return AbstractEmployeeOrder.class;
        }

        public String getEntityName()
        {
            return "abstractEmployeeOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
