/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.moveemployee.base.ext.Employee.ui.PostView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.Date;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
public class EmployeePostViewExtUI extends UIAddon
{
    public EmployeePostViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public String getOrderTitle()
    {
        EmployeePost employeePost = ((EmployeePostViewUI) getPresenter()).getEmployeePost();
        AbstractEmployeeOrder order = null;

        // dao prepare
        MQBuilder builder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "ee");
        builder.add(MQExpression.eq("ee", IAbstractExtract.L_ENTITY, employeePost));
        builder.add(MQExpression.eq("ee", IAbstractExtract.L_STATE + "." + ExtractStates.P_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        builder.addOrder("ee", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + AbstractEmployeeOrder.P_COMMIT_DATE, OrderDirection.asc);
        for (AbstractEmployeeExtract extract : builder.<AbstractEmployeeExtract>getResultList(getSession()))
        {
            if (extract instanceof IPostAssignExtract)
            {
                order = extract.getParagraph().getOrder();
                break;
            }
        }
        String number;
        Date date;
        if (order == null)
        {
            number = employeePost.getOrderNumber();
            date = employeePost.getOrderDate();
        } else
        {
            number = order.getNumber();
            date = order.getCommitDate();
        }
        if (null == number || null == date)
            return "";
        return "Приказ №" + number + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(date);
    }
}
