/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 11.11.2008
 */
public interface IDAO extends IUniDao<Model>
{
    void doSendToCoordination(Model model, IPersistentPersonable initiator);

    void doSendToFormative(Model model);

    void doReject(Model model);

    void doCommit(Model model);

    void doRollback(Model model);

    void deleteExtract(Model model, Long extractId);

    void deleteOrder(Model model);

    void deleteOrderWithExtracts(Model model);

    /**
     * Определяет, есть ли в приказе выписки, формирующиеся индивидуально
     * @param model - модель
     * @return <tt><b>true</b></tt>, если в списке выписок, содержащихся в приказе<p>
     * есть выписка, тип которой помечен как формирующийся индивидуально,<p>
     * иначе <tt><b>false</b></tt>
     */
    public boolean isAddExtractsDisabled(Model model);

    /**
     * Поднимает параграф вверху по списку
     * @param extractId id выписки
     */
    void doParagraphUp(Long extractId);

    /**
     * Опускает параграф вниз по списку
     * @param extractId id выписки
     */
    void doParagraphDown(Long extractId);
}
