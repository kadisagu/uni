/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.menuempl.ModularOrdersFinished;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularParagraph;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

import java.util.*;

/**
 * @author dseleznev
 *         Created on: 27.01.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        // init datasource
        MQBuilder builder = new MQBuilder(EmployeeModularOrder.ENTITY_CLASS, "o");
        builder.add(MQExpression.eq("o", IAbstractOrder.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

        // order filters
        Date commitSystemDate = model.getSettings().get("commitDateSystem");
        Date commitDate = model.getSettings().get("commitDate");
        Date createDate = model.getSettings().get("createDate");
        String number = model.getSettings().get("number");

        // apply filters
        if (createDate != null)
            builder.add(UniMQExpression.eqDate("o", IAbstractOrder.P_CREATE_DATE, createDate));
        if (commitDate != null)
            builder.add(UniMQExpression.eqDate("o", EmployeeModularOrder.P_COMMIT_DATE, commitDate));
        if (commitSystemDate != null)
            builder.add(UniMQExpression.eqDate("o", IAbstractOrder.P_COMMIT_DATE_SYSTEM, commitSystemDate));
        if (StringUtils.isNotEmpty(number))
            builder.add(MQExpression.like("o", IAbstractOrder.P_NUMBER, CoreStringUtils.escapeLike(number)));

        new OrderDescriptionRegistry("o").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());

        List<EmployeeModularOrder> orderList = builder.getResultList(getSession());

        List<EmployeeExtractType> employeeExtractIndividualTypeList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeePostExtractIndividualTypeList();
        employeeExtractIndividualTypeList.addAll(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeeExtractIndividualTypeList());

        MQBuilder extractBuilder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "omee");
        extractBuilder.add(MQExpression.in("omee", ModularEmployeeExtract.paragraph().order().s(), orderList));
        List<ModularEmployeeExtract> employeeExtractList = extractBuilder.getResultList(getSession());

        List<EmployeeModularOrder> individualOrderList = new ArrayList<>();
        for (ModularEmployeeExtract extract : employeeExtractList)
            if (employeeExtractIndividualTypeList.contains(extract.getType()))
                individualOrderList.add((EmployeeModularOrder) extract.getParagraph().getOrder());

        model.setIndividualOrderMap(new HashMap<>());
        for (EmployeeModularOrder order : orderList)
        {
            if (order.getParagraphCount() == 1)
                if (individualOrderList.contains(order))
                {
                    model.getIndividualOrderMap().put(order.getId(), true);
                    continue;
                }
            model.getIndividualOrderMap().put(order.getId(), false);
        }

        model.setIndexExtractOrderMap(new HashMap<>());
        for (ModularEmployeeExtract extract : employeeExtractList)
            model.getIndexExtractOrderMap().put(extract.getParagraph().getOrder().getId(), extract);

        // get statictics for view properties
        String hql = "select " + IAbstractParagraph.L_ORDER + ".id, count(*) from " + EmployeeModularParagraph.ENTITY_CLASS + " where " + IAbstractParagraph.L_ORDER + " is not null group by " + IAbstractParagraph.L_ORDER;
        Map<Long, Integer> id2count = new HashMap<>();
        for (Object[] row : (List<Object[]>) getSession().createQuery(hql).list())
        {
            Long id = (Long) row[0];
            Number count = (Number) row[1];
            id2count.put(id, count == null ? 0 : count.intValue());
        }

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            Integer value = id2count.get(wrapper.getId());
            wrapper.setViewProperty(EmployeeModularOrder.P_EXTRACT_COUNT, value == null ? 0 : value);
        }
    }

    @Override
    public void deleteRow(IBusinessComponent context)
    {
        IAbstractOrder order = getNotNull((Long) context.getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
    }
}
