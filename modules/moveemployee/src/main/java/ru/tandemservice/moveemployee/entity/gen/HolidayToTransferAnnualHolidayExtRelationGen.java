package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.HolidayToTransferAnnualHolidayExtRelation;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.gen.IChoseRowTransferAnnualHolidayExtractGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь созданных ежегодных или планируемых отпусков в выписке «О переносе ежегодного отпуска» и самой выписки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class HolidayToTransferAnnualHolidayExtRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.HolidayToTransferAnnualHolidayExtRelation";
    public static final String ENTITY_NAME = "holidayToTransferAnnualHolidayExtRelation";
    public static final int VERSION_HASH = -841556349;
    private static IEntityMeta ENTITY_META;

    public static final String L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT = "transferAnnualHolidayExtract";
    public static final String L_NEW_OBJECT = "newObject";

    private TransferAnnualHolidayExtract _transferAnnualHolidayExtract;     // Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска
    private IChoseRowTransferAnnualHolidayExtract _newObject;     // Интерфейс объектов выбранных в полях выписки О переносе ежегодного отпуска и самой выписки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     */
    @NotNull
    public TransferAnnualHolidayExtract getTransferAnnualHolidayExtract()
    {
        return _transferAnnualHolidayExtract;
    }

    /**
     * @param transferAnnualHolidayExtract Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     */
    public void setTransferAnnualHolidayExtract(TransferAnnualHolidayExtract transferAnnualHolidayExtract)
    {
        dirty(_transferAnnualHolidayExtract, transferAnnualHolidayExtract);
        _transferAnnualHolidayExtract = transferAnnualHolidayExtract;
    }

    /**
     * @return Интерфейс объектов выбранных в полях выписки О переносе ежегодного отпуска и самой выписки.
     */
    public IChoseRowTransferAnnualHolidayExtract getNewObject()
    {
        return _newObject;
    }

    /**
     * @param newObject Интерфейс объектов выбранных в полях выписки О переносе ежегодного отпуска и самой выписки.
     */
    public void setNewObject(IChoseRowTransferAnnualHolidayExtract newObject)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && newObject!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IChoseRowTransferAnnualHolidayExtract.class);
            IEntityMeta actual =  newObject instanceof IEntity ? EntityRuntime.getMeta((IEntity) newObject) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_newObject, newObject);
        _newObject = newObject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof HolidayToTransferAnnualHolidayExtRelationGen)
        {
            setTransferAnnualHolidayExtract(((HolidayToTransferAnnualHolidayExtRelation)another).getTransferAnnualHolidayExtract());
            setNewObject(((HolidayToTransferAnnualHolidayExtRelation)another).getNewObject());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends HolidayToTransferAnnualHolidayExtRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) HolidayToTransferAnnualHolidayExtRelation.class;
        }

        public T newInstance()
        {
            return (T) new HolidayToTransferAnnualHolidayExtRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "transferAnnualHolidayExtract":
                    return obj.getTransferAnnualHolidayExtract();
                case "newObject":
                    return obj.getNewObject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "transferAnnualHolidayExtract":
                    obj.setTransferAnnualHolidayExtract((TransferAnnualHolidayExtract) value);
                    return;
                case "newObject":
                    obj.setNewObject((IChoseRowTransferAnnualHolidayExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "transferAnnualHolidayExtract":
                        return true;
                case "newObject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "transferAnnualHolidayExtract":
                    return true;
                case "newObject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "transferAnnualHolidayExtract":
                    return TransferAnnualHolidayExtract.class;
                case "newObject":
                    return IChoseRowTransferAnnualHolidayExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<HolidayToTransferAnnualHolidayExtRelation> _dslPath = new Path<HolidayToTransferAnnualHolidayExtRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "HolidayToTransferAnnualHolidayExtRelation");
    }
            

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayToTransferAnnualHolidayExtRelation#getTransferAnnualHolidayExtract()
     */
    public static TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> transferAnnualHolidayExtract()
    {
        return _dslPath.transferAnnualHolidayExtract();
    }

    /**
     * @return Интерфейс объектов выбранных в полях выписки О переносе ежегодного отпуска и самой выписки.
     * @see ru.tandemservice.moveemployee.entity.HolidayToTransferAnnualHolidayExtRelation#getNewObject()
     */
    public static IChoseRowTransferAnnualHolidayExtractGen.Path<IChoseRowTransferAnnualHolidayExtract> newObject()
    {
        return _dslPath.newObject();
    }

    public static class Path<E extends HolidayToTransferAnnualHolidayExtRelation> extends EntityPath<E>
    {
        private TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> _transferAnnualHolidayExtract;
        private IChoseRowTransferAnnualHolidayExtractGen.Path<IChoseRowTransferAnnualHolidayExtract> _newObject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayToTransferAnnualHolidayExtRelation#getTransferAnnualHolidayExtract()
     */
        public TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> transferAnnualHolidayExtract()
        {
            if(_transferAnnualHolidayExtract == null )
                _transferAnnualHolidayExtract = new TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract>(L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, this);
            return _transferAnnualHolidayExtract;
        }

    /**
     * @return Интерфейс объектов выбранных в полях выписки О переносе ежегодного отпуска и самой выписки.
     * @see ru.tandemservice.moveemployee.entity.HolidayToTransferAnnualHolidayExtRelation#getNewObject()
     */
        public IChoseRowTransferAnnualHolidayExtractGen.Path<IChoseRowTransferAnnualHolidayExtract> newObject()
        {
            if(_newObject == null )
                _newObject = new IChoseRowTransferAnnualHolidayExtractGen.Path<IChoseRowTransferAnnualHolidayExtract>(L_NEW_OBJECT, this);
            return _newObject;
        }

        public Class getEntityClass()
        {
            return HolidayToTransferAnnualHolidayExtRelation.class;
        }

        public String getEntityName()
        {
            return "holidayToTransferAnnualHolidayExtRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
