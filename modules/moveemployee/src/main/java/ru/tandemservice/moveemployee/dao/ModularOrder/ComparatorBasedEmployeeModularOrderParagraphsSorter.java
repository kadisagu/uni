/**
 *$Id$
 */
package ru.tandemservice.moveemployee.dao.ModularOrder;

import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 01.04.2013
 */
public abstract class ComparatorBasedEmployeeModularOrderParagraphsSorter extends UniBaseDao implements IEmployeeModularOrderParagraphsSortDAO
{
    /*
     * Сортировщик, основанный на компараторе.
     * Работающие на основе сравнинвания выписок сортировщики наследуются от этого
     */

    public void sortExtracts(EmployeeModularOrder order)
    {
        List<ModularEmployeeExtract> extracts = getList(ModularEmployeeExtract.class, AbstractEmployeeExtract.paragraph().order(), order);
        List<ModularEmployeeExtract> extractsToUpdate = new ArrayList<>();

        Collections.sort(extracts, getComparator());

        int shift = order.getParagraphCount() + 1;
        int number = 1;

        List<Integer> unchangedNumbers = new ArrayList<>();

        for (ModularEmployeeExtract extract : extracts)
        {
            AbstractEmployeeParagraph paragraph = extract.getParagraph();
            if (paragraph.getNumber() == number)
            {
                unchangedNumbers.add(paragraph.getNumber());

            } else
            {
                paragraph.setNumber(number + shift);
                update(paragraph);
                extractsToUpdate.add(extract);
            }

            number++;
        }

        getSession().flush();

        number = 1;
        for (ModularEmployeeExtract extract : extractsToUpdate)
        {
            while (unchangedNumbers.contains(number)) {number++;}

            AbstractEmployeeParagraph paragraph = extract.getParagraph();
            paragraph.setNumber(number++);
            update(paragraph);
        }
    }

    protected abstract Comparator<ModularEmployeeExtract> getComparator();
}