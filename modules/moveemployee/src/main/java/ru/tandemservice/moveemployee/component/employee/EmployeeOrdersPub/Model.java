/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.employee.EmployeeOrdersPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import ru.tandemservice.moveemployee.component.commons.ExtractListModel;

/**
 * @author dseleznev
 * Created on: 07.11.2008
 */
@State({
        @Bind(key = "employeeOrderSelectedTab", binding = "employeeOrderSelectedTab"),
        @Bind(key = ISecureRoleContext.SECURE_ROLE_CONTEXT, binding = "personRoleModel", required = true)
})
public class Model
{
    private Employee _employee;
    private ISecureRoleContext _personRoleModel;

    private ExtractListModel _modularProjectsModel = new ExtractListModel();
    private ExtractListModel _modularExtractsModel = new ExtractListModel();
    private ExtractListModel _singleProjectsModel = new ExtractListModel();
    private ExtractListModel _singleExtractsModel = new ExtractListModel();
    private ExtractListModel _listOrdersModel = new ExtractListModel();
    private ExtractListModel _ordersModel = new ExtractListModel();
    
    private String _employeeOrderSelectedTab;

    private boolean _extractCantBeAdded;
    private boolean _sextractCantBeAdded;

    public ISecureRoleContext getPersonRoleModel()
    {
        return _personRoleModel;
    }

    public void setPersonRoleModel(ISecureRoleContext personRoleModel)
    {
        _personRoleModel = personRoleModel;
    }

    public String getEmployeeOrderSelectedTab()
    {
        return _employeeOrderSelectedTab;
    }

    public void setEmployeeOrderSelectedTab(String employeeOrderSelectedTab)
    {
        this._employeeOrderSelectedTab = employeeOrderSelectedTab;
    }
    
    public boolean isAccessible()
    {
        return _personRoleModel.isAccessible();
    }

    public Employee getEmployee()
    {
        return _employee;
    }

    public void setEmployee(Employee employee)
    {
        this._employee = employee;
    }

    public boolean isExtractCantBeAdded()
    {
        return _extractCantBeAdded;
    }

    public void setExtractCantBeAdded(boolean extractCantBeAdded)
    {
        this._extractCantBeAdded = extractCantBeAdded;
    }

    public boolean isSextractCantBeAdded()
    {
        return _sextractCantBeAdded;
    }

    public void setSextractCantBeAdded(boolean sextractCantBeAdded)
    {
        this._sextractCantBeAdded = sextractCantBeAdded;
    }

    public ExtractListModel getModularProjectsModel()
    {
        return _modularProjectsModel;
    }

    public void setModularProjectsModel(ExtractListModel modularProjectsModel)
    {
        this._modularProjectsModel = modularProjectsModel;
    }

    public ExtractListModel getModularExtractsModel()
    {
        return _modularExtractsModel;
    }

    public void setModularExtractsModel(ExtractListModel modularExtractsModel)
    {
        this._modularExtractsModel = modularExtractsModel;
    }

    public ExtractListModel getSingleProjectsModel()
    {
        return _singleProjectsModel;
    }

    public void setSingleProjectsModel(ExtractListModel singleProjectsModel)
    {
        this._singleProjectsModel = singleProjectsModel;
    }

    public ExtractListModel getSingleExtractsModel()
    {
        return _singleExtractsModel;
    }

    public void setSingleExtractsModel(ExtractListModel singleExtractsModel)
    {
        this._singleExtractsModel = singleExtractsModel;
    }

    public ExtractListModel getListOrdersModel()
    {
        return _listOrdersModel;
    }

    public void setListOrdersModel(ExtractListModel listOrdersModel)
    {
        this._listOrdersModel = listOrdersModel;
    }

    public ExtractListModel getOrdersModel()
    {
        return _ordersModel;
    }

    public void setOrdersModel(ExtractListModel ordersModel)
    {
        this._ordersModel = ordersModel;
    }
}