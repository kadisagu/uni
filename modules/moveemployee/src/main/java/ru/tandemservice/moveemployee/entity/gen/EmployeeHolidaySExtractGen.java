package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. Об отпуске
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeHolidaySExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract";
    public static final String ENTITY_NAME = "employeeHolidaySExtract";
    public static final int VERSION_HASH = 2023571409;
    private static IEntityMeta ENTITY_META;

    public static final String P_FIRST_PERIOD_BEGIN_DATE = "firstPeriodBeginDate";
    public static final String P_FIRST_PERIOD_END_DATE = "firstPeriodEndDate";
    public static final String P_SECOND_PERIOD_BEGIN_DATE = "secondPeriodBeginDate";
    public static final String P_SECOND_PERIOD_END_DATE = "secondPeriodEndDate";
    public static final String P_MAIN_HOLIDAY_BEGIN_DATE = "mainHolidayBeginDate";
    public static final String P_MAIN_HOLIDAY_END_DATE = "mainHolidayEndDate";
    public static final String P_MAIN_HOLIDAY_DURATION = "mainHolidayDuration";
    public static final String P_MAIN_SECOND_HOLIDAY_BEGIN_DATE = "mainSecondHolidayBeginDate";
    public static final String P_MAIN_SECOND_HOLIDAY_END_DATE = "mainSecondHolidayEndDate";
    public static final String P_MAIN_SECOND_HOLIDAY_DURATION = "mainSecondHolidayDuration";
    public static final String L_HOLIDAY_TYPE = "holidayType";
    public static final String P_SECOND_HOLIDAY_BEGIN_DATE = "secondHolidayBeginDate";
    public static final String P_SECOND_HOLIDAY_END_DATE = "secondHolidayEndDate";
    public static final String P_SECOND_HOLIDAY_DURATION = "secondHolidayDuration";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_DURATION = "duration";
    public static final String P_CHILD_NUMBER = "childNumber";
    public static final String P_OPTIONAL_CONDITION = "optionalCondition";
    public static final String L_EMPLOYEE_FIRST_HOLIDAY = "employeeFirstHoliday";
    public static final String L_EMPLOYEE_SECOND_HOLIDAY = "employeeSecondHoliday";
    public static final String P_SECOND_JOB = "secondJob";
    public static final String L_VACATION_SCHEDULE = "vacationSchedule";
    public static final String L_VACATION_SCHEDULE_ITEM = "vacationScheduleItem";
    public static final String P_OLD_SCHEDULE_ITEM_FACT_DATE = "oldScheduleItemFactDate";
    public static final String P_OLD_SCHEDULE_ITEM_POSTPONE_DATE = "oldScheduleItemPostponeDate";
    public static final String P_OLD_SCHEDULE_ITEM_POSTPONE_BASIC = "oldScheduleItemPostponeBasic";
    public static final String L_NEW_EMPLOYEE_POST_STATUS = "newEmployeePostStatus";
    public static final String L_OLD_EMPLOYEE_POST_STATUS = "oldEmployeePostStatus";

    private Date _firstPeriodBeginDate;     // Дата начала периода
    private Date _firstPeriodEndDate;     // Дата окончания периода
    private Date _secondPeriodBeginDate;     // Дата начала второго периода
    private Date _secondPeriodEndDate;     // Дата окончания второго периода
    private Date _mainHolidayBeginDate;     // Дата начала основного отпуска
    private Date _mainHolidayEndDate;     // Дата окончания основного отпуска
    private Integer _mainHolidayDuration;     // Длительность основного отпуска, календарных дней
    private Date _mainSecondHolidayBeginDate;     // Дата начала второго периода основного отпуска
    private Date _mainSecondHolidayEndDate;     // Дата окончания второго периода основного отпуска
    private Integer _mainSecondHolidayDuration;     // Длительность второго периода основного отпуска, календарных дней
    private HolidayType _holidayType;     // Тип отпуска
    private Date _secondHolidayBeginDate;     // Дата начала дополнительного отпуска
    private Date _secondHolidayEndDate;     // Дата окончания дополнительного отпуска
    private Integer _secondHolidayDuration;     // Длительность дополнительного отпуска, календарных дней
    private Date _beginDate;     // Дата начала отпуска
    private Date _endDate;     // Дата окончания отпуска
    private int _duration;     // Длительность отпуска, календарных дней
    private Integer _childNumber;     // Номер ребенка
    private String _optionalCondition;     // Произвольная строка условий отпуска
    private EmployeeHoliday _employeeFirstHoliday;     // Отпуск сотрудника
    private EmployeeHoliday _employeeSecondHoliday;     // Отпуск сотрудника
    private Boolean _secondJob;     // Предоставить отпуск по работе по совместительству в тот же срок
    private VacationSchedule _vacationSchedule;     // График отпусков
    private VacationScheduleItem _vacationScheduleItem;     // Строка графика отпусков
    private Date _oldScheduleItemFactDate;     // Фактическая дата ухода в отпуск в строке графика отпусков сотрудника до проведения приказа
    private Date _oldScheduleItemPostponeDate;     // Предполагаемая дата переноса отпуска в строке графика отпусков сотрудника до проведения приказа
    private String _oldScheduleItemPostponeBasic;     // Основание для переноса отпуска в строке графика отпусков сотрудника до проведения приказа
    private EmployeePostStatus _newEmployeePostStatus;     // Статус на должности при уходе в отпуск
    private EmployeePostStatus _oldEmployeePostStatus;     // Статус на должности до ухода в отпуск

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала периода.
     */
    public Date getFirstPeriodBeginDate()
    {
        return _firstPeriodBeginDate;
    }

    /**
     * @param firstPeriodBeginDate Дата начала периода.
     */
    public void setFirstPeriodBeginDate(Date firstPeriodBeginDate)
    {
        dirty(_firstPeriodBeginDate, firstPeriodBeginDate);
        _firstPeriodBeginDate = firstPeriodBeginDate;
    }

    /**
     * @return Дата окончания периода.
     */
    public Date getFirstPeriodEndDate()
    {
        return _firstPeriodEndDate;
    }

    /**
     * @param firstPeriodEndDate Дата окончания периода.
     */
    public void setFirstPeriodEndDate(Date firstPeriodEndDate)
    {
        dirty(_firstPeriodEndDate, firstPeriodEndDate);
        _firstPeriodEndDate = firstPeriodEndDate;
    }

    /**
     * @return Дата начала второго периода.
     */
    public Date getSecondPeriodBeginDate()
    {
        return _secondPeriodBeginDate;
    }

    /**
     * @param secondPeriodBeginDate Дата начала второго периода.
     */
    public void setSecondPeriodBeginDate(Date secondPeriodBeginDate)
    {
        dirty(_secondPeriodBeginDate, secondPeriodBeginDate);
        _secondPeriodBeginDate = secondPeriodBeginDate;
    }

    /**
     * @return Дата окончания второго периода.
     */
    public Date getSecondPeriodEndDate()
    {
        return _secondPeriodEndDate;
    }

    /**
     * @param secondPeriodEndDate Дата окончания второго периода.
     */
    public void setSecondPeriodEndDate(Date secondPeriodEndDate)
    {
        dirty(_secondPeriodEndDate, secondPeriodEndDate);
        _secondPeriodEndDate = secondPeriodEndDate;
    }

    /**
     * @return Дата начала основного отпуска.
     */
    public Date getMainHolidayBeginDate()
    {
        return _mainHolidayBeginDate;
    }

    /**
     * @param mainHolidayBeginDate Дата начала основного отпуска.
     */
    public void setMainHolidayBeginDate(Date mainHolidayBeginDate)
    {
        dirty(_mainHolidayBeginDate, mainHolidayBeginDate);
        _mainHolidayBeginDate = mainHolidayBeginDate;
    }

    /**
     * @return Дата окончания основного отпуска.
     */
    public Date getMainHolidayEndDate()
    {
        return _mainHolidayEndDate;
    }

    /**
     * @param mainHolidayEndDate Дата окончания основного отпуска.
     */
    public void setMainHolidayEndDate(Date mainHolidayEndDate)
    {
        dirty(_mainHolidayEndDate, mainHolidayEndDate);
        _mainHolidayEndDate = mainHolidayEndDate;
    }

    /**
     * @return Длительность основного отпуска, календарных дней.
     */
    public Integer getMainHolidayDuration()
    {
        return _mainHolidayDuration;
    }

    /**
     * @param mainHolidayDuration Длительность основного отпуска, календарных дней.
     */
    public void setMainHolidayDuration(Integer mainHolidayDuration)
    {
        dirty(_mainHolidayDuration, mainHolidayDuration);
        _mainHolidayDuration = mainHolidayDuration;
    }

    /**
     * @return Дата начала второго периода основного отпуска.
     */
    public Date getMainSecondHolidayBeginDate()
    {
        return _mainSecondHolidayBeginDate;
    }

    /**
     * @param mainSecondHolidayBeginDate Дата начала второго периода основного отпуска.
     */
    public void setMainSecondHolidayBeginDate(Date mainSecondHolidayBeginDate)
    {
        dirty(_mainSecondHolidayBeginDate, mainSecondHolidayBeginDate);
        _mainSecondHolidayBeginDate = mainSecondHolidayBeginDate;
    }

    /**
     * @return Дата окончания второго периода основного отпуска.
     */
    public Date getMainSecondHolidayEndDate()
    {
        return _mainSecondHolidayEndDate;
    }

    /**
     * @param mainSecondHolidayEndDate Дата окончания второго периода основного отпуска.
     */
    public void setMainSecondHolidayEndDate(Date mainSecondHolidayEndDate)
    {
        dirty(_mainSecondHolidayEndDate, mainSecondHolidayEndDate);
        _mainSecondHolidayEndDate = mainSecondHolidayEndDate;
    }

    /**
     * @return Длительность второго периода основного отпуска, календарных дней.
     */
    public Integer getMainSecondHolidayDuration()
    {
        return _mainSecondHolidayDuration;
    }

    /**
     * @param mainSecondHolidayDuration Длительность второго периода основного отпуска, календарных дней.
     */
    public void setMainSecondHolidayDuration(Integer mainSecondHolidayDuration)
    {
        dirty(_mainSecondHolidayDuration, mainSecondHolidayDuration);
        _mainSecondHolidayDuration = mainSecondHolidayDuration;
    }

    /**
     * @return Тип отпуска. Свойство не может быть null.
     */
    @NotNull
    public HolidayType getHolidayType()
    {
        return _holidayType;
    }

    /**
     * @param holidayType Тип отпуска. Свойство не может быть null.
     */
    public void setHolidayType(HolidayType holidayType)
    {
        dirty(_holidayType, holidayType);
        _holidayType = holidayType;
    }

    /**
     * @return Дата начала дополнительного отпуска.
     */
    public Date getSecondHolidayBeginDate()
    {
        return _secondHolidayBeginDate;
    }

    /**
     * @param secondHolidayBeginDate Дата начала дополнительного отпуска.
     */
    public void setSecondHolidayBeginDate(Date secondHolidayBeginDate)
    {
        dirty(_secondHolidayBeginDate, secondHolidayBeginDate);
        _secondHolidayBeginDate = secondHolidayBeginDate;
    }

    /**
     * @return Дата окончания дополнительного отпуска.
     */
    public Date getSecondHolidayEndDate()
    {
        return _secondHolidayEndDate;
    }

    /**
     * @param secondHolidayEndDate Дата окончания дополнительного отпуска.
     */
    public void setSecondHolidayEndDate(Date secondHolidayEndDate)
    {
        dirty(_secondHolidayEndDate, secondHolidayEndDate);
        _secondHolidayEndDate = secondHolidayEndDate;
    }

    /**
     * @return Длительность дополнительного отпуска, календарных дней.
     */
    public Integer getSecondHolidayDuration()
    {
        return _secondHolidayDuration;
    }

    /**
     * @param secondHolidayDuration Длительность дополнительного отпуска, календарных дней.
     */
    public void setSecondHolidayDuration(Integer secondHolidayDuration)
    {
        dirty(_secondHolidayDuration, secondHolidayDuration);
        _secondHolidayDuration = secondHolidayDuration;
    }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала отпуска. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания отпуска. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Длительность отпуска, календарных дней. Свойство не может быть null.
     */
    @NotNull
    public int getDuration()
    {
        return _duration;
    }

    /**
     * @param duration Длительность отпуска, календарных дней. Свойство не может быть null.
     */
    public void setDuration(int duration)
    {
        dirty(_duration, duration);
        _duration = duration;
    }

    /**
     * @return Номер ребенка.
     */
    public Integer getChildNumber()
    {
        return _childNumber;
    }

    /**
     * @param childNumber Номер ребенка.
     */
    public void setChildNumber(Integer childNumber)
    {
        dirty(_childNumber, childNumber);
        _childNumber = childNumber;
    }

    /**
     * @return Произвольная строка условий отпуска.
     */
    public String getOptionalCondition()
    {
        return _optionalCondition;
    }

    /**
     * @param optionalCondition Произвольная строка условий отпуска.
     */
    public void setOptionalCondition(String optionalCondition)
    {
        dirty(_optionalCondition, optionalCondition);
        _optionalCondition = optionalCondition;
    }

    /**
     * @return Отпуск сотрудника.
     */
    public EmployeeHoliday getEmployeeFirstHoliday()
    {
        return _employeeFirstHoliday;
    }

    /**
     * @param employeeFirstHoliday Отпуск сотрудника.
     */
    public void setEmployeeFirstHoliday(EmployeeHoliday employeeFirstHoliday)
    {
        dirty(_employeeFirstHoliday, employeeFirstHoliday);
        _employeeFirstHoliday = employeeFirstHoliday;
    }

    /**
     * @return Отпуск сотрудника.
     */
    public EmployeeHoliday getEmployeeSecondHoliday()
    {
        return _employeeSecondHoliday;
    }

    /**
     * @param employeeSecondHoliday Отпуск сотрудника.
     */
    public void setEmployeeSecondHoliday(EmployeeHoliday employeeSecondHoliday)
    {
        dirty(_employeeSecondHoliday, employeeSecondHoliday);
        _employeeSecondHoliday = employeeSecondHoliday;
    }

    /**
     * @return Предоставить отпуск по работе по совместительству в тот же срок.
     */
    public Boolean getSecondJob()
    {
        return _secondJob;
    }

    /**
     * @param secondJob Предоставить отпуск по работе по совместительству в тот же срок.
     */
    public void setSecondJob(Boolean secondJob)
    {
        dirty(_secondJob, secondJob);
        _secondJob = secondJob;
    }

    /**
     * @return График отпусков.
     */
    public VacationSchedule getVacationSchedule()
    {
        return _vacationSchedule;
    }

    /**
     * @param vacationSchedule График отпусков.
     */
    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        dirty(_vacationSchedule, vacationSchedule);
        _vacationSchedule = vacationSchedule;
    }

    /**
     * @return Строка графика отпусков.
     */
    public VacationScheduleItem getVacationScheduleItem()
    {
        return _vacationScheduleItem;
    }

    /**
     * @param vacationScheduleItem Строка графика отпусков.
     */
    public void setVacationScheduleItem(VacationScheduleItem vacationScheduleItem)
    {
        dirty(_vacationScheduleItem, vacationScheduleItem);
        _vacationScheduleItem = vacationScheduleItem;
    }

    /**
     * @return Фактическая дата ухода в отпуск в строке графика отпусков сотрудника до проведения приказа.
     */
    public Date getOldScheduleItemFactDate()
    {
        return _oldScheduleItemFactDate;
    }

    /**
     * @param oldScheduleItemFactDate Фактическая дата ухода в отпуск в строке графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemFactDate(Date oldScheduleItemFactDate)
    {
        dirty(_oldScheduleItemFactDate, oldScheduleItemFactDate);
        _oldScheduleItemFactDate = oldScheduleItemFactDate;
    }

    /**
     * @return Предполагаемая дата переноса отпуска в строке графика отпусков сотрудника до проведения приказа.
     */
    public Date getOldScheduleItemPostponeDate()
    {
        return _oldScheduleItemPostponeDate;
    }

    /**
     * @param oldScheduleItemPostponeDate Предполагаемая дата переноса отпуска в строке графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemPostponeDate(Date oldScheduleItemPostponeDate)
    {
        dirty(_oldScheduleItemPostponeDate, oldScheduleItemPostponeDate);
        _oldScheduleItemPostponeDate = oldScheduleItemPostponeDate;
    }

    /**
     * @return Основание для переноса отпуска в строке графика отпусков сотрудника до проведения приказа.
     */
    @Length(max=255)
    public String getOldScheduleItemPostponeBasic()
    {
        return _oldScheduleItemPostponeBasic;
    }

    /**
     * @param oldScheduleItemPostponeBasic Основание для переноса отпуска в строке графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemPostponeBasic(String oldScheduleItemPostponeBasic)
    {
        dirty(_oldScheduleItemPostponeBasic, oldScheduleItemPostponeBasic);
        _oldScheduleItemPostponeBasic = oldScheduleItemPostponeBasic;
    }

    /**
     * @return Статус на должности при уходе в отпуск. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getNewEmployeePostStatus()
    {
        return _newEmployeePostStatus;
    }

    /**
     * @param newEmployeePostStatus Статус на должности при уходе в отпуск. Свойство не может быть null.
     */
    public void setNewEmployeePostStatus(EmployeePostStatus newEmployeePostStatus)
    {
        dirty(_newEmployeePostStatus, newEmployeePostStatus);
        _newEmployeePostStatus = newEmployeePostStatus;
    }

    /**
     * @return Статус на должности до ухода в отпуск. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getOldEmployeePostStatus()
    {
        return _oldEmployeePostStatus;
    }

    /**
     * @param oldEmployeePostStatus Статус на должности до ухода в отпуск. Свойство не может быть null.
     */
    public void setOldEmployeePostStatus(EmployeePostStatus oldEmployeePostStatus)
    {
        dirty(_oldEmployeePostStatus, oldEmployeePostStatus);
        _oldEmployeePostStatus = oldEmployeePostStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeHolidaySExtractGen)
        {
            setFirstPeriodBeginDate(((EmployeeHolidaySExtract)another).getFirstPeriodBeginDate());
            setFirstPeriodEndDate(((EmployeeHolidaySExtract)another).getFirstPeriodEndDate());
            setSecondPeriodBeginDate(((EmployeeHolidaySExtract)another).getSecondPeriodBeginDate());
            setSecondPeriodEndDate(((EmployeeHolidaySExtract)another).getSecondPeriodEndDate());
            setMainHolidayBeginDate(((EmployeeHolidaySExtract)another).getMainHolidayBeginDate());
            setMainHolidayEndDate(((EmployeeHolidaySExtract)another).getMainHolidayEndDate());
            setMainHolidayDuration(((EmployeeHolidaySExtract)another).getMainHolidayDuration());
            setMainSecondHolidayBeginDate(((EmployeeHolidaySExtract)another).getMainSecondHolidayBeginDate());
            setMainSecondHolidayEndDate(((EmployeeHolidaySExtract)another).getMainSecondHolidayEndDate());
            setMainSecondHolidayDuration(((EmployeeHolidaySExtract)another).getMainSecondHolidayDuration());
            setHolidayType(((EmployeeHolidaySExtract)another).getHolidayType());
            setSecondHolidayBeginDate(((EmployeeHolidaySExtract)another).getSecondHolidayBeginDate());
            setSecondHolidayEndDate(((EmployeeHolidaySExtract)another).getSecondHolidayEndDate());
            setSecondHolidayDuration(((EmployeeHolidaySExtract)another).getSecondHolidayDuration());
            setBeginDate(((EmployeeHolidaySExtract)another).getBeginDate());
            setEndDate(((EmployeeHolidaySExtract)another).getEndDate());
            setDuration(((EmployeeHolidaySExtract)another).getDuration());
            setChildNumber(((EmployeeHolidaySExtract)another).getChildNumber());
            setOptionalCondition(((EmployeeHolidaySExtract)another).getOptionalCondition());
            setEmployeeFirstHoliday(((EmployeeHolidaySExtract)another).getEmployeeFirstHoliday());
            setEmployeeSecondHoliday(((EmployeeHolidaySExtract)another).getEmployeeSecondHoliday());
            setSecondJob(((EmployeeHolidaySExtract)another).getSecondJob());
            setVacationSchedule(((EmployeeHolidaySExtract)another).getVacationSchedule());
            setVacationScheduleItem(((EmployeeHolidaySExtract)another).getVacationScheduleItem());
            setOldScheduleItemFactDate(((EmployeeHolidaySExtract)another).getOldScheduleItemFactDate());
            setOldScheduleItemPostponeDate(((EmployeeHolidaySExtract)another).getOldScheduleItemPostponeDate());
            setOldScheduleItemPostponeBasic(((EmployeeHolidaySExtract)another).getOldScheduleItemPostponeBasic());
            setNewEmployeePostStatus(((EmployeeHolidaySExtract)another).getNewEmployeePostStatus());
            setOldEmployeePostStatus(((EmployeeHolidaySExtract)another).getOldEmployeePostStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeHolidaySExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeHolidaySExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeHolidaySExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "firstPeriodBeginDate":
                    return obj.getFirstPeriodBeginDate();
                case "firstPeriodEndDate":
                    return obj.getFirstPeriodEndDate();
                case "secondPeriodBeginDate":
                    return obj.getSecondPeriodBeginDate();
                case "secondPeriodEndDate":
                    return obj.getSecondPeriodEndDate();
                case "mainHolidayBeginDate":
                    return obj.getMainHolidayBeginDate();
                case "mainHolidayEndDate":
                    return obj.getMainHolidayEndDate();
                case "mainHolidayDuration":
                    return obj.getMainHolidayDuration();
                case "mainSecondHolidayBeginDate":
                    return obj.getMainSecondHolidayBeginDate();
                case "mainSecondHolidayEndDate":
                    return obj.getMainSecondHolidayEndDate();
                case "mainSecondHolidayDuration":
                    return obj.getMainSecondHolidayDuration();
                case "holidayType":
                    return obj.getHolidayType();
                case "secondHolidayBeginDate":
                    return obj.getSecondHolidayBeginDate();
                case "secondHolidayEndDate":
                    return obj.getSecondHolidayEndDate();
                case "secondHolidayDuration":
                    return obj.getSecondHolidayDuration();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "duration":
                    return obj.getDuration();
                case "childNumber":
                    return obj.getChildNumber();
                case "optionalCondition":
                    return obj.getOptionalCondition();
                case "employeeFirstHoliday":
                    return obj.getEmployeeFirstHoliday();
                case "employeeSecondHoliday":
                    return obj.getEmployeeSecondHoliday();
                case "secondJob":
                    return obj.getSecondJob();
                case "vacationSchedule":
                    return obj.getVacationSchedule();
                case "vacationScheduleItem":
                    return obj.getVacationScheduleItem();
                case "oldScheduleItemFactDate":
                    return obj.getOldScheduleItemFactDate();
                case "oldScheduleItemPostponeDate":
                    return obj.getOldScheduleItemPostponeDate();
                case "oldScheduleItemPostponeBasic":
                    return obj.getOldScheduleItemPostponeBasic();
                case "newEmployeePostStatus":
                    return obj.getNewEmployeePostStatus();
                case "oldEmployeePostStatus":
                    return obj.getOldEmployeePostStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "firstPeriodBeginDate":
                    obj.setFirstPeriodBeginDate((Date) value);
                    return;
                case "firstPeriodEndDate":
                    obj.setFirstPeriodEndDate((Date) value);
                    return;
                case "secondPeriodBeginDate":
                    obj.setSecondPeriodBeginDate((Date) value);
                    return;
                case "secondPeriodEndDate":
                    obj.setSecondPeriodEndDate((Date) value);
                    return;
                case "mainHolidayBeginDate":
                    obj.setMainHolidayBeginDate((Date) value);
                    return;
                case "mainHolidayEndDate":
                    obj.setMainHolidayEndDate((Date) value);
                    return;
                case "mainHolidayDuration":
                    obj.setMainHolidayDuration((Integer) value);
                    return;
                case "mainSecondHolidayBeginDate":
                    obj.setMainSecondHolidayBeginDate((Date) value);
                    return;
                case "mainSecondHolidayEndDate":
                    obj.setMainSecondHolidayEndDate((Date) value);
                    return;
                case "mainSecondHolidayDuration":
                    obj.setMainSecondHolidayDuration((Integer) value);
                    return;
                case "holidayType":
                    obj.setHolidayType((HolidayType) value);
                    return;
                case "secondHolidayBeginDate":
                    obj.setSecondHolidayBeginDate((Date) value);
                    return;
                case "secondHolidayEndDate":
                    obj.setSecondHolidayEndDate((Date) value);
                    return;
                case "secondHolidayDuration":
                    obj.setSecondHolidayDuration((Integer) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "duration":
                    obj.setDuration((Integer) value);
                    return;
                case "childNumber":
                    obj.setChildNumber((Integer) value);
                    return;
                case "optionalCondition":
                    obj.setOptionalCondition((String) value);
                    return;
                case "employeeFirstHoliday":
                    obj.setEmployeeFirstHoliday((EmployeeHoliday) value);
                    return;
                case "employeeSecondHoliday":
                    obj.setEmployeeSecondHoliday((EmployeeHoliday) value);
                    return;
                case "secondJob":
                    obj.setSecondJob((Boolean) value);
                    return;
                case "vacationSchedule":
                    obj.setVacationSchedule((VacationSchedule) value);
                    return;
                case "vacationScheduleItem":
                    obj.setVacationScheduleItem((VacationScheduleItem) value);
                    return;
                case "oldScheduleItemFactDate":
                    obj.setOldScheduleItemFactDate((Date) value);
                    return;
                case "oldScheduleItemPostponeDate":
                    obj.setOldScheduleItemPostponeDate((Date) value);
                    return;
                case "oldScheduleItemPostponeBasic":
                    obj.setOldScheduleItemPostponeBasic((String) value);
                    return;
                case "newEmployeePostStatus":
                    obj.setNewEmployeePostStatus((EmployeePostStatus) value);
                    return;
                case "oldEmployeePostStatus":
                    obj.setOldEmployeePostStatus((EmployeePostStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "firstPeriodBeginDate":
                        return true;
                case "firstPeriodEndDate":
                        return true;
                case "secondPeriodBeginDate":
                        return true;
                case "secondPeriodEndDate":
                        return true;
                case "mainHolidayBeginDate":
                        return true;
                case "mainHolidayEndDate":
                        return true;
                case "mainHolidayDuration":
                        return true;
                case "mainSecondHolidayBeginDate":
                        return true;
                case "mainSecondHolidayEndDate":
                        return true;
                case "mainSecondHolidayDuration":
                        return true;
                case "holidayType":
                        return true;
                case "secondHolidayBeginDate":
                        return true;
                case "secondHolidayEndDate":
                        return true;
                case "secondHolidayDuration":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "duration":
                        return true;
                case "childNumber":
                        return true;
                case "optionalCondition":
                        return true;
                case "employeeFirstHoliday":
                        return true;
                case "employeeSecondHoliday":
                        return true;
                case "secondJob":
                        return true;
                case "vacationSchedule":
                        return true;
                case "vacationScheduleItem":
                        return true;
                case "oldScheduleItemFactDate":
                        return true;
                case "oldScheduleItemPostponeDate":
                        return true;
                case "oldScheduleItemPostponeBasic":
                        return true;
                case "newEmployeePostStatus":
                        return true;
                case "oldEmployeePostStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "firstPeriodBeginDate":
                    return true;
                case "firstPeriodEndDate":
                    return true;
                case "secondPeriodBeginDate":
                    return true;
                case "secondPeriodEndDate":
                    return true;
                case "mainHolidayBeginDate":
                    return true;
                case "mainHolidayEndDate":
                    return true;
                case "mainHolidayDuration":
                    return true;
                case "mainSecondHolidayBeginDate":
                    return true;
                case "mainSecondHolidayEndDate":
                    return true;
                case "mainSecondHolidayDuration":
                    return true;
                case "holidayType":
                    return true;
                case "secondHolidayBeginDate":
                    return true;
                case "secondHolidayEndDate":
                    return true;
                case "secondHolidayDuration":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "duration":
                    return true;
                case "childNumber":
                    return true;
                case "optionalCondition":
                    return true;
                case "employeeFirstHoliday":
                    return true;
                case "employeeSecondHoliday":
                    return true;
                case "secondJob":
                    return true;
                case "vacationSchedule":
                    return true;
                case "vacationScheduleItem":
                    return true;
                case "oldScheduleItemFactDate":
                    return true;
                case "oldScheduleItemPostponeDate":
                    return true;
                case "oldScheduleItemPostponeBasic":
                    return true;
                case "newEmployeePostStatus":
                    return true;
                case "oldEmployeePostStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "firstPeriodBeginDate":
                    return Date.class;
                case "firstPeriodEndDate":
                    return Date.class;
                case "secondPeriodBeginDate":
                    return Date.class;
                case "secondPeriodEndDate":
                    return Date.class;
                case "mainHolidayBeginDate":
                    return Date.class;
                case "mainHolidayEndDate":
                    return Date.class;
                case "mainHolidayDuration":
                    return Integer.class;
                case "mainSecondHolidayBeginDate":
                    return Date.class;
                case "mainSecondHolidayEndDate":
                    return Date.class;
                case "mainSecondHolidayDuration":
                    return Integer.class;
                case "holidayType":
                    return HolidayType.class;
                case "secondHolidayBeginDate":
                    return Date.class;
                case "secondHolidayEndDate":
                    return Date.class;
                case "secondHolidayDuration":
                    return Integer.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "duration":
                    return Integer.class;
                case "childNumber":
                    return Integer.class;
                case "optionalCondition":
                    return String.class;
                case "employeeFirstHoliday":
                    return EmployeeHoliday.class;
                case "employeeSecondHoliday":
                    return EmployeeHoliday.class;
                case "secondJob":
                    return Boolean.class;
                case "vacationSchedule":
                    return VacationSchedule.class;
                case "vacationScheduleItem":
                    return VacationScheduleItem.class;
                case "oldScheduleItemFactDate":
                    return Date.class;
                case "oldScheduleItemPostponeDate":
                    return Date.class;
                case "oldScheduleItemPostponeBasic":
                    return String.class;
                case "newEmployeePostStatus":
                    return EmployeePostStatus.class;
                case "oldEmployeePostStatus":
                    return EmployeePostStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeHolidaySExtract> _dslPath = new Path<EmployeeHolidaySExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeHolidaySExtract");
    }
            

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getFirstPeriodBeginDate()
     */
    public static PropertyPath<Date> firstPeriodBeginDate()
    {
        return _dslPath.firstPeriodBeginDate();
    }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getFirstPeriodEndDate()
     */
    public static PropertyPath<Date> firstPeriodEndDate()
    {
        return _dslPath.firstPeriodEndDate();
    }

    /**
     * @return Дата начала второго периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondPeriodBeginDate()
     */
    public static PropertyPath<Date> secondPeriodBeginDate()
    {
        return _dslPath.secondPeriodBeginDate();
    }

    /**
     * @return Дата окончания второго периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondPeriodEndDate()
     */
    public static PropertyPath<Date> secondPeriodEndDate()
    {
        return _dslPath.secondPeriodEndDate();
    }

    /**
     * @return Дата начала основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainHolidayBeginDate()
     */
    public static PropertyPath<Date> mainHolidayBeginDate()
    {
        return _dslPath.mainHolidayBeginDate();
    }

    /**
     * @return Дата окончания основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainHolidayEndDate()
     */
    public static PropertyPath<Date> mainHolidayEndDate()
    {
        return _dslPath.mainHolidayEndDate();
    }

    /**
     * @return Длительность основного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainHolidayDuration()
     */
    public static PropertyPath<Integer> mainHolidayDuration()
    {
        return _dslPath.mainHolidayDuration();
    }

    /**
     * @return Дата начала второго периода основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainSecondHolidayBeginDate()
     */
    public static PropertyPath<Date> mainSecondHolidayBeginDate()
    {
        return _dslPath.mainSecondHolidayBeginDate();
    }

    /**
     * @return Дата окончания второго периода основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainSecondHolidayEndDate()
     */
    public static PropertyPath<Date> mainSecondHolidayEndDate()
    {
        return _dslPath.mainSecondHolidayEndDate();
    }

    /**
     * @return Длительность второго периода основного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainSecondHolidayDuration()
     */
    public static PropertyPath<Integer> mainSecondHolidayDuration()
    {
        return _dslPath.mainSecondHolidayDuration();
    }

    /**
     * @return Тип отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getHolidayType()
     */
    public static HolidayType.Path<HolidayType> holidayType()
    {
        return _dslPath.holidayType();
    }

    /**
     * @return Дата начала дополнительного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondHolidayBeginDate()
     */
    public static PropertyPath<Date> secondHolidayBeginDate()
    {
        return _dslPath.secondHolidayBeginDate();
    }

    /**
     * @return Дата окончания дополнительного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondHolidayEndDate()
     */
    public static PropertyPath<Date> secondHolidayEndDate()
    {
        return _dslPath.secondHolidayEndDate();
    }

    /**
     * @return Длительность дополнительного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondHolidayDuration()
     */
    public static PropertyPath<Integer> secondHolidayDuration()
    {
        return _dslPath.secondHolidayDuration();
    }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Длительность отпуска, календарных дней. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getDuration()
     */
    public static PropertyPath<Integer> duration()
    {
        return _dslPath.duration();
    }

    /**
     * @return Номер ребенка.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getChildNumber()
     */
    public static PropertyPath<Integer> childNumber()
    {
        return _dslPath.childNumber();
    }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOptionalCondition()
     */
    public static PropertyPath<String> optionalCondition()
    {
        return _dslPath.optionalCondition();
    }

    /**
     * @return Отпуск сотрудника.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getEmployeeFirstHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> employeeFirstHoliday()
    {
        return _dslPath.employeeFirstHoliday();
    }

    /**
     * @return Отпуск сотрудника.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getEmployeeSecondHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> employeeSecondHoliday()
    {
        return _dslPath.employeeSecondHoliday();
    }

    /**
     * @return Предоставить отпуск по работе по совместительству в тот же срок.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondJob()
     */
    public static PropertyPath<Boolean> secondJob()
    {
        return _dslPath.secondJob();
    }

    /**
     * @return График отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getVacationSchedule()
     */
    public static VacationSchedule.Path<VacationSchedule> vacationSchedule()
    {
        return _dslPath.vacationSchedule();
    }

    /**
     * @return Строка графика отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getVacationScheduleItem()
     */
    public static VacationScheduleItem.Path<VacationScheduleItem> vacationScheduleItem()
    {
        return _dslPath.vacationScheduleItem();
    }

    /**
     * @return Фактическая дата ухода в отпуск в строке графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOldScheduleItemFactDate()
     */
    public static PropertyPath<Date> oldScheduleItemFactDate()
    {
        return _dslPath.oldScheduleItemFactDate();
    }

    /**
     * @return Предполагаемая дата переноса отпуска в строке графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOldScheduleItemPostponeDate()
     */
    public static PropertyPath<Date> oldScheduleItemPostponeDate()
    {
        return _dslPath.oldScheduleItemPostponeDate();
    }

    /**
     * @return Основание для переноса отпуска в строке графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOldScheduleItemPostponeBasic()
     */
    public static PropertyPath<String> oldScheduleItemPostponeBasic()
    {
        return _dslPath.oldScheduleItemPostponeBasic();
    }

    /**
     * @return Статус на должности при уходе в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getNewEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> newEmployeePostStatus()
    {
        return _dslPath.newEmployeePostStatus();
    }

    /**
     * @return Статус на должности до ухода в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOldEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
    {
        return _dslPath.oldEmployeePostStatus();
    }

    public static class Path<E extends EmployeeHolidaySExtract> extends SingleEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _firstPeriodBeginDate;
        private PropertyPath<Date> _firstPeriodEndDate;
        private PropertyPath<Date> _secondPeriodBeginDate;
        private PropertyPath<Date> _secondPeriodEndDate;
        private PropertyPath<Date> _mainHolidayBeginDate;
        private PropertyPath<Date> _mainHolidayEndDate;
        private PropertyPath<Integer> _mainHolidayDuration;
        private PropertyPath<Date> _mainSecondHolidayBeginDate;
        private PropertyPath<Date> _mainSecondHolidayEndDate;
        private PropertyPath<Integer> _mainSecondHolidayDuration;
        private HolidayType.Path<HolidayType> _holidayType;
        private PropertyPath<Date> _secondHolidayBeginDate;
        private PropertyPath<Date> _secondHolidayEndDate;
        private PropertyPath<Integer> _secondHolidayDuration;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Integer> _duration;
        private PropertyPath<Integer> _childNumber;
        private PropertyPath<String> _optionalCondition;
        private EmployeeHoliday.Path<EmployeeHoliday> _employeeFirstHoliday;
        private EmployeeHoliday.Path<EmployeeHoliday> _employeeSecondHoliday;
        private PropertyPath<Boolean> _secondJob;
        private VacationSchedule.Path<VacationSchedule> _vacationSchedule;
        private VacationScheduleItem.Path<VacationScheduleItem> _vacationScheduleItem;
        private PropertyPath<Date> _oldScheduleItemFactDate;
        private PropertyPath<Date> _oldScheduleItemPostponeDate;
        private PropertyPath<String> _oldScheduleItemPostponeBasic;
        private EmployeePostStatus.Path<EmployeePostStatus> _newEmployeePostStatus;
        private EmployeePostStatus.Path<EmployeePostStatus> _oldEmployeePostStatus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getFirstPeriodBeginDate()
     */
        public PropertyPath<Date> firstPeriodBeginDate()
        {
            if(_firstPeriodBeginDate == null )
                _firstPeriodBeginDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_FIRST_PERIOD_BEGIN_DATE, this);
            return _firstPeriodBeginDate;
        }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getFirstPeriodEndDate()
     */
        public PropertyPath<Date> firstPeriodEndDate()
        {
            if(_firstPeriodEndDate == null )
                _firstPeriodEndDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_FIRST_PERIOD_END_DATE, this);
            return _firstPeriodEndDate;
        }

    /**
     * @return Дата начала второго периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondPeriodBeginDate()
     */
        public PropertyPath<Date> secondPeriodBeginDate()
        {
            if(_secondPeriodBeginDate == null )
                _secondPeriodBeginDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_SECOND_PERIOD_BEGIN_DATE, this);
            return _secondPeriodBeginDate;
        }

    /**
     * @return Дата окончания второго периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondPeriodEndDate()
     */
        public PropertyPath<Date> secondPeriodEndDate()
        {
            if(_secondPeriodEndDate == null )
                _secondPeriodEndDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_SECOND_PERIOD_END_DATE, this);
            return _secondPeriodEndDate;
        }

    /**
     * @return Дата начала основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainHolidayBeginDate()
     */
        public PropertyPath<Date> mainHolidayBeginDate()
        {
            if(_mainHolidayBeginDate == null )
                _mainHolidayBeginDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_MAIN_HOLIDAY_BEGIN_DATE, this);
            return _mainHolidayBeginDate;
        }

    /**
     * @return Дата окончания основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainHolidayEndDate()
     */
        public PropertyPath<Date> mainHolidayEndDate()
        {
            if(_mainHolidayEndDate == null )
                _mainHolidayEndDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_MAIN_HOLIDAY_END_DATE, this);
            return _mainHolidayEndDate;
        }

    /**
     * @return Длительность основного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainHolidayDuration()
     */
        public PropertyPath<Integer> mainHolidayDuration()
        {
            if(_mainHolidayDuration == null )
                _mainHolidayDuration = new PropertyPath<Integer>(EmployeeHolidaySExtractGen.P_MAIN_HOLIDAY_DURATION, this);
            return _mainHolidayDuration;
        }

    /**
     * @return Дата начала второго периода основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainSecondHolidayBeginDate()
     */
        public PropertyPath<Date> mainSecondHolidayBeginDate()
        {
            if(_mainSecondHolidayBeginDate == null )
                _mainSecondHolidayBeginDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_MAIN_SECOND_HOLIDAY_BEGIN_DATE, this);
            return _mainSecondHolidayBeginDate;
        }

    /**
     * @return Дата окончания второго периода основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainSecondHolidayEndDate()
     */
        public PropertyPath<Date> mainSecondHolidayEndDate()
        {
            if(_mainSecondHolidayEndDate == null )
                _mainSecondHolidayEndDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_MAIN_SECOND_HOLIDAY_END_DATE, this);
            return _mainSecondHolidayEndDate;
        }

    /**
     * @return Длительность второго периода основного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getMainSecondHolidayDuration()
     */
        public PropertyPath<Integer> mainSecondHolidayDuration()
        {
            if(_mainSecondHolidayDuration == null )
                _mainSecondHolidayDuration = new PropertyPath<Integer>(EmployeeHolidaySExtractGen.P_MAIN_SECOND_HOLIDAY_DURATION, this);
            return _mainSecondHolidayDuration;
        }

    /**
     * @return Тип отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getHolidayType()
     */
        public HolidayType.Path<HolidayType> holidayType()
        {
            if(_holidayType == null )
                _holidayType = new HolidayType.Path<HolidayType>(L_HOLIDAY_TYPE, this);
            return _holidayType;
        }

    /**
     * @return Дата начала дополнительного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondHolidayBeginDate()
     */
        public PropertyPath<Date> secondHolidayBeginDate()
        {
            if(_secondHolidayBeginDate == null )
                _secondHolidayBeginDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_SECOND_HOLIDAY_BEGIN_DATE, this);
            return _secondHolidayBeginDate;
        }

    /**
     * @return Дата окончания дополнительного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondHolidayEndDate()
     */
        public PropertyPath<Date> secondHolidayEndDate()
        {
            if(_secondHolidayEndDate == null )
                _secondHolidayEndDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_SECOND_HOLIDAY_END_DATE, this);
            return _secondHolidayEndDate;
        }

    /**
     * @return Длительность дополнительного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondHolidayDuration()
     */
        public PropertyPath<Integer> secondHolidayDuration()
        {
            if(_secondHolidayDuration == null )
                _secondHolidayDuration = new PropertyPath<Integer>(EmployeeHolidaySExtractGen.P_SECOND_HOLIDAY_DURATION, this);
            return _secondHolidayDuration;
        }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Длительность отпуска, календарных дней. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getDuration()
     */
        public PropertyPath<Integer> duration()
        {
            if(_duration == null )
                _duration = new PropertyPath<Integer>(EmployeeHolidaySExtractGen.P_DURATION, this);
            return _duration;
        }

    /**
     * @return Номер ребенка.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getChildNumber()
     */
        public PropertyPath<Integer> childNumber()
        {
            if(_childNumber == null )
                _childNumber = new PropertyPath<Integer>(EmployeeHolidaySExtractGen.P_CHILD_NUMBER, this);
            return _childNumber;
        }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOptionalCondition()
     */
        public PropertyPath<String> optionalCondition()
        {
            if(_optionalCondition == null )
                _optionalCondition = new PropertyPath<String>(EmployeeHolidaySExtractGen.P_OPTIONAL_CONDITION, this);
            return _optionalCondition;
        }

    /**
     * @return Отпуск сотрудника.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getEmployeeFirstHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> employeeFirstHoliday()
        {
            if(_employeeFirstHoliday == null )
                _employeeFirstHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_EMPLOYEE_FIRST_HOLIDAY, this);
            return _employeeFirstHoliday;
        }

    /**
     * @return Отпуск сотрудника.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getEmployeeSecondHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> employeeSecondHoliday()
        {
            if(_employeeSecondHoliday == null )
                _employeeSecondHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_EMPLOYEE_SECOND_HOLIDAY, this);
            return _employeeSecondHoliday;
        }

    /**
     * @return Предоставить отпуск по работе по совместительству в тот же срок.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getSecondJob()
     */
        public PropertyPath<Boolean> secondJob()
        {
            if(_secondJob == null )
                _secondJob = new PropertyPath<Boolean>(EmployeeHolidaySExtractGen.P_SECOND_JOB, this);
            return _secondJob;
        }

    /**
     * @return График отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getVacationSchedule()
     */
        public VacationSchedule.Path<VacationSchedule> vacationSchedule()
        {
            if(_vacationSchedule == null )
                _vacationSchedule = new VacationSchedule.Path<VacationSchedule>(L_VACATION_SCHEDULE, this);
            return _vacationSchedule;
        }

    /**
     * @return Строка графика отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getVacationScheduleItem()
     */
        public VacationScheduleItem.Path<VacationScheduleItem> vacationScheduleItem()
        {
            if(_vacationScheduleItem == null )
                _vacationScheduleItem = new VacationScheduleItem.Path<VacationScheduleItem>(L_VACATION_SCHEDULE_ITEM, this);
            return _vacationScheduleItem;
        }

    /**
     * @return Фактическая дата ухода в отпуск в строке графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOldScheduleItemFactDate()
     */
        public PropertyPath<Date> oldScheduleItemFactDate()
        {
            if(_oldScheduleItemFactDate == null )
                _oldScheduleItemFactDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_OLD_SCHEDULE_ITEM_FACT_DATE, this);
            return _oldScheduleItemFactDate;
        }

    /**
     * @return Предполагаемая дата переноса отпуска в строке графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOldScheduleItemPostponeDate()
     */
        public PropertyPath<Date> oldScheduleItemPostponeDate()
        {
            if(_oldScheduleItemPostponeDate == null )
                _oldScheduleItemPostponeDate = new PropertyPath<Date>(EmployeeHolidaySExtractGen.P_OLD_SCHEDULE_ITEM_POSTPONE_DATE, this);
            return _oldScheduleItemPostponeDate;
        }

    /**
     * @return Основание для переноса отпуска в строке графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOldScheduleItemPostponeBasic()
     */
        public PropertyPath<String> oldScheduleItemPostponeBasic()
        {
            if(_oldScheduleItemPostponeBasic == null )
                _oldScheduleItemPostponeBasic = new PropertyPath<String>(EmployeeHolidaySExtractGen.P_OLD_SCHEDULE_ITEM_POSTPONE_BASIC, this);
            return _oldScheduleItemPostponeBasic;
        }

    /**
     * @return Статус на должности при уходе в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getNewEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> newEmployeePostStatus()
        {
            if(_newEmployeePostStatus == null )
                _newEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_NEW_EMPLOYEE_POST_STATUS, this);
            return _newEmployeePostStatus;
        }

    /**
     * @return Статус на должности до ухода в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract#getOldEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
        {
            if(_oldEmployeePostStatus == null )
                _oldEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_OLD_EMPLOYEE_POST_STATUS, this);
            return _oldEmployeePostStatus;
        }

        public Class getEntityClass()
        {
            return EmployeeHolidaySExtract.class;
        }

        public String getEntityName()
        {
            return "employeeHolidaySExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
