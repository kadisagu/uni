/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e1.Pub;

import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeAddExtract;

/**
 * @author dseleznev
 * Created on: 20.11.2008
 */
public class DAO extends ModularEmployeeExtractPubDAO<EmployeeAddExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setStaffRateStr(CommonEmployeeExtractUtil.getExtractFinancingSourceDetailsString(model.getExtract(), getSession(), true, null));
    }
}