/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSettings;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentUnit;

/**
 * @author esych
 * Created on: 17.01.2011
 */
public class Model
{
    private String _selectedTabId;

    private String _separator;

    private DynamicListDataSource<Payment> _paymentDataSource;
    private DynamicListDataSource<FinancingSource> _financingSourceDataSource;
    private DynamicListDataSource<FinancingSourceItem> _financingSourceItemDataSource;
    private DynamicListDataSource<PaymentUnit> _paymentUnitDataSource;

    private DynamicListDataSource<ViewWrapper<PaymentStringRepresentationFormat>> _stringReprFormatDataSource;

    public String getSelectedTabId()
    {
        return _selectedTabId;
    }

    public void setSelectedTabId(String value)
    {
        _selectedTabId = value;
    }

    public String getSeparator()
    {
        return _separator;
    }

    public void setSeparator(String value)
    {
        _separator = value;
    }

    public DynamicListDataSource<Payment> getPaymentDataSource()
    {
        return _paymentDataSource;
    }

    public void setPaymentDataSource(DynamicListDataSource<Payment> value)
    {
        _paymentDataSource = value;
    }

    public DynamicListDataSource<FinancingSource> getFinancingSourceDataSource()
    {
        return _financingSourceDataSource;
    }

    public void setFinancingSourceDataSource(DynamicListDataSource<FinancingSource> value)
    {
        _financingSourceDataSource = value;
    }

    public DynamicListDataSource<FinancingSourceItem> getFinancingSourceItemDataSource()
    {
        return _financingSourceItemDataSource;
    }

    public void setFinancingSourceItemDataSource(DynamicListDataSource<FinancingSourceItem> value)
    {
        _financingSourceItemDataSource = value;
    }

    public DynamicListDataSource<PaymentUnit> getPaymentUnitDataSource()
    {
        return _paymentUnitDataSource;
    }

    public void setPaymentUnitDataSource(DynamicListDataSource<PaymentUnit> value)
    {
        _paymentUnitDataSource = value;
    }

    public DynamicListDataSource<ViewWrapper<PaymentStringRepresentationFormat>> getStringReprFormatDataSource()
    {
        return _stringReprFormatDataSource;
    }

    public void setStringReprFormatDataSource(DynamicListDataSource<ViewWrapper<PaymentStringRepresentationFormat>> value)
    {
        _stringReprFormatDataSource = value;
    }
}
