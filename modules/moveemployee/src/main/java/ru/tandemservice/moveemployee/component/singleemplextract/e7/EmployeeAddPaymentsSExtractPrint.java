/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e7;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 08.12.2010
 */
public class EmployeeAddPaymentsSExtractPrint implements IPrintFormCreator<EmployeeAddPaymentsSExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeAddPaymentsSExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder)extract.getParagraph().getOrder());

        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        CommonExtractPrintUtil.injectPostType(modifier, extract.getEntity().getPostType());

        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());
        Double budgetStaffRate = 0.0d;
        Double offbudgetStaffRate = 0.0d;
        for (EmployeePostStaffRateItem item : staffRateItems)
        {
            if (item.getFinancingSource().getCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET))
                budgetStaffRate += item.getStaffRate();
            else
                offbudgetStaffRate += item.getStaffRate();
        }

        List<FinancingSourceDetails> fsFakeDetails = new ArrayList<FinancingSourceDetails>();
        FinancingSourceDetails budgetFakeDetails = new FinancingSourceDetails();
        budgetFakeDetails.setFinancingSource(UniDaoFacade.getCoreDao().getCatalogItem(FinancingSource.class, UniempDefines.FINANCING_SOURCE_BUDGET));
        budgetFakeDetails.setStaffRate(budgetStaffRate);

        FinancingSourceDetails offBudgetFakeDetails = new FinancingSourceDetails();
        offBudgetFakeDetails.setFinancingSource(UniDaoFacade.getCoreDao().getCatalogItem(FinancingSource.class, UniempDefines.FINANCING_SOURCE_OFF_BUDGET));
        offBudgetFakeDetails.setStaffRate(offbudgetStaffRate);

        fsFakeDetails.add(budgetFakeDetails);
        fsFakeDetails.add(offBudgetFakeDetails);

        CommonExtractPrintUtil.injectStaffRates(modifier, fsFakeDetails);
        CommonExtractPrintUtil.injectWorkConditions(modifier, extract.getEntity().getWeekWorkLoad(), extract.getEntity().getWorkWeekDuration(), budgetStaffRate + offbudgetStaffRate);

        if (null != extract.getHoursAmount())
        {
            modifier.put("hoursAmountWithHoursWord", String.valueOf(extract.getHoursAmount()) + " часов");
        }
        else
        {
            modifier.put("hoursAmountWithHoursWord", "");
        }

        modifier.put("payments", CommonExtractUtil.getBonusListStrRtf(extract));

        // Labour contract
        if (extract.getLabourContractDate() != null)
        {
            modifier.put("lcDay", RussianDateFormatUtils.getDayString(extract.getLabourContractDate(), true));
            modifier.put("lcMonthStr", RussianDateFormatUtils.getMonthName(extract.getLabourContractDate(), false));
            modifier.put("lcYear", RussianDateFormatUtils.getYearString(extract.getLabourContractDate(), true));
        }
        else
        {
            modifier.put("lcDay", "");
            modifier.put("lcMonthStr", "");
            modifier.put("lcYear", "");
        }

        RtfTableModifier tableModifier = new RtfTableModifier();
        List<EmployeeBonus> bonusesList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract);

        List<String[]> stimulatingPaymentsList = new ArrayList<String[]>();
        List<String[]> compensativePaymentsList = new ArrayList<String[]>();

        List<String[]> result = new ArrayList<String[]>();
        for (EmployeeBonus bonus : bonusesList)
        {
            String[] bonusArr = new String[5];
            bonusArr[0] = bonus.getPayment().getTitle();
            bonusArr[1] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()) + " " + bonus.getPayment().getPaymentUnit().getShortTitle();
            result.add(bonusArr);

            PaymentType parentPaymentType = bonus.getPayment().getType();
            while (null != parentPaymentType.getParent())
                parentPaymentType = parentPaymentType.getParent();

            if (UniempDefines.PAYMENT_TYPE_STLIMULATION.equals(parentPaymentType.getCode()))
                stimulatingPaymentsList.add(bonusArr);
            else
                compensativePaymentsList.add(bonusArr);
        }

        tableModifier.put("T", result.toArray(new String[][] {}));
        if(stimulatingPaymentsList.isEmpty()) stimulatingPaymentsList.add(new String[] {"", ""});
        if(compensativePaymentsList.isEmpty()) compensativePaymentsList.add(new String[] {"", ""});
        tableModifier.put("STIMULATING_PAYMENTS", stimulatingPaymentsList.toArray(new String[][] {}));
        tableModifier.put("COMPENSATIVE_PAYMENTS", compensativePaymentsList.toArray(new String[][] {}));

        extendModifier(modifier, tableModifier, extract);

        tableModifier.modify(document);
        modifier.modify(document);
        return document;
    }

    protected void extendModifier(RtfInjectModifier modifier, RtfTableModifier tableModifier, EmployeeAddPaymentsSExtract extract)
    {

    }
}