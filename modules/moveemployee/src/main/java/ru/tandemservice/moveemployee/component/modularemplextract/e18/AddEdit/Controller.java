/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e18.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 15.09.2011
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<TransferAnnualHolidayExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareHolidayDataSource(component);
        prepareFactHolidayDataSource(component);
    }

    public void prepareHolidayDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getHolidayDataSource() != null)
            return;

        DynamicListDataSource<IdentifiableWrapper> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareHolidayDataSource(model);
        });

        dataSource.addColumn(new BlockColumn("beginHolidayDay", "Дата начала"));
        dataSource.addColumn(new BlockColumn("endHolidayDay", "Дата окончания"));
        dataSource.addColumn(new BlockColumn("durationHolidayDay", "Длительность"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteHoliday");
        actionColumn.setDisabledProperty("disableDelete");
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setHolidayDataSource(dataSource);
    }

    public void prepareFactHolidayDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getFactHolidayDataSource() != null)
            return;

        DynamicListDataSource<IdentifiableWrapper> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareFactHolidayDataSource(model);
        });

        dataSource.addColumn(new BlockColumn("beginFactHolidayDay", "Дата начала"));
        dataSource.addColumn(new BlockColumn("endFactHolidayDay", "Дата окончания"));
        dataSource.addColumn(new BlockColumn("durationFactHolidayDay", "Длительность"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteFactHoliday");
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setFactHolidayDataSource(dataSource);
    }

    public void onChangeEmployeeHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);

        Object entity = model.getChoseHoliday().getId() != 0 ? getDao().get(model.getChoseHoliday().getId()) : null ;
        if (entity instanceof EmployeeHoliday || entity instanceof VacationScheduleItem)
            model.setShowHoliday(true);
        else
            model.setShowHoliday(false);

        model.setHolidayPartList(null);
    }

    @SuppressWarnings("unchecked")
    public void onChangeHolidayDay(IBusinessComponent component)
    {
        Model model = getModel(component);
        IdentifiableWrapper currentWrapper = ((ViewWrapper<IdentifiableWrapper>) component.getListenerParameter()).getEntity();

        //в зависимости от того что заполнено автозаполняем количество дней, либо конечную дату
        if (model.getBeginHolidayDayMap().get(currentWrapper) != null && model.getEndHolidayDayMap().get(currentWrapper) != null && (model.getDurationHolidayDayMap().get(currentWrapper) == null || model.getDurationHolidayDayMap().get(currentWrapper) == 0))
        {
            Integer holidayDuration;
            if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                holidayDuration = EmployeeManager.instance().dao().getEmployeeHolidayDuration(model.getEmployeePost().getWorkWeekDuration(), model.getBeginHolidayDayMap().get(currentWrapper), model.getEndHolidayDayMap().get(currentWrapper));
            else
                holidayDuration = (int) CommonBaseDateUtil.getBetweenPeriod(model.getBeginHolidayDayMap().get(currentWrapper), model.getEndHolidayDayMap().get(currentWrapper), Calendar.DAY_OF_YEAR) + 1;

            if (holidayDuration != null)
                model.getDurationHolidayDayMap().put(currentWrapper, holidayDuration);
            else
                model.getDurationHolidayDayMap().put(currentWrapper, 0);
        }
        else if (model.getBeginHolidayDayMap().get(currentWrapper) != null && model.getDurationHolidayDayMap().get(currentWrapper) != null && model.getEndHolidayDayMap().get(currentWrapper) == null)
        {
            GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            int counter = 0;
            int duration = model.getDurationHolidayDayMap().get(currentWrapper);

            date.setTime(model.getBeginHolidayDayMap().get(currentWrapper));

            if (duration > 0)
            {
                do
                {
                    if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                    {
                        if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                            counter++;
                    }
                    else
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);

                model.getEndHolidayDayMap().put(currentWrapper, date.getTime());
            }
        }
    }

    public void onClickAddHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);

        //добовляем новую часть отпуска
        IdentifiableWrapper newWrapper = new IdentifiableWrapper(getNewHolidayId(model), "");
        model.getPartHolidayList().add(newWrapper);

        model.getHolidayDataSource().refresh();
    }

    @SuppressWarnings("unchecked")
    public void onClickDeleteHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);
        IdentifiableWrapper currentWrapper = ((ViewWrapper<IdentifiableWrapper>) component.getListenerParameter()).getEntity();

        model.getPartHolidayList().remove(currentWrapper);
        model.getBeginHolidayDayMap().remove(currentWrapper);
        model.getEndHolidayDayMap().remove(currentWrapper);
        model.getDurationHolidayDayMap().remove(currentWrapper);

        model.getHolidayDataSource().refresh();
    }

    @SuppressWarnings("unchecked")
    public void onChangeFactHolidayDay(IBusinessComponent component)
    {
        Model model = getModel(component);
        IdentifiableWrapper currentWrapper = component.getListenerParameter();

        //в зависимости от того что заполнено автозаполняем количество дней, либо конечную дату
        if (model.getBeginFactHolidayDayMap().get(currentWrapper) != null && model.getEndFactHolidayDayMap().get(currentWrapper) != null && (model.getDurationFactHolidayDayMap().get(currentWrapper) == null || model.getDurationFactHolidayDayMap().get(currentWrapper) == 0))
        {
            Integer holidayDuration;
            if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                holidayDuration = EmployeeManager.instance().dao().getEmployeeHolidayDuration(model.getEmployeePost().getWorkWeekDuration(), model.getBeginFactHolidayDayMap().get(currentWrapper), model.getEndFactHolidayDayMap().get(currentWrapper));
            else
                holidayDuration = (int) CommonBaseDateUtil.getBetweenPeriod(model.getBeginFactHolidayDayMap().get(currentWrapper), model.getEndFactHolidayDayMap().get(currentWrapper), Calendar.DAY_OF_YEAR) + 1;

            if (holidayDuration != null)
                model.getDurationFactHolidayDayMap().put(currentWrapper, holidayDuration);
            else
                model.getDurationFactHolidayDayMap().put(currentWrapper, 0);
        }
        else if (model.getBeginFactHolidayDayMap().get(currentWrapper) != null && model.getDurationFactHolidayDayMap().get(currentWrapper) != null && model.getEndFactHolidayDayMap().get(currentWrapper) == null)
        {
            GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            int counter = 0;
            int duration = model.getDurationFactHolidayDayMap().get(currentWrapper);

            date.setTime(model.getBeginFactHolidayDayMap().get(currentWrapper));

            if (duration > 0)
            {
                do
                {
                    if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                    {
                        if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                            counter++;
                    }
                    else
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);

                model.getEndFactHolidayDayMap().put(currentWrapper, date.getTime());
            }
        }
    }

    public void onClickAddFactHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);

        //добовляем новую часть отпуска
        IdentifiableWrapper newWrapper = new IdentifiableWrapper(getNewFactHolidayId(model), "");
        model.getPartFactHolidayList().add(newWrapper);

        model.getFactHolidayDataSource().refresh();
    }

    @SuppressWarnings("unchecked")
    public void onClickDeleteFactHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);
        IdentifiableWrapper currentWrapper = component.getListenerParameter();

        model.getPartFactHolidayList().remove(currentWrapper);
        model.getBeginFactHolidayDayMap().remove(currentWrapper);
        model.getEndFactHolidayDayMap().remove(currentWrapper);
        model.getDurationFactHolidayDayMap().remove(currentWrapper);

        model.getFactHolidayDataSource().refresh();
    }

    private long getNewHolidayId(Model model)
    {
        Long result = ((long) model.getPartHolidayList().size()) * -1;

        boolean notExit;
        do
        {
            notExit = false;
            for (IdentifiableWrapper wrapper : model.getPartHolidayList())
                if (result.equals(wrapper.getId()))
                {
                    result--;
                    notExit = true;
                }
        }
        while (notExit);

        return result;
    }

    private long getNewFactHolidayId(Model model)
    {
        Long result = model.getPartFactHolidayList().size() == 0 ? 1 : (long) model.getPartFactHolidayList().size();

        boolean notExit;
        do
        {
            notExit = false;
            for (IdentifiableWrapper wrapper : model.getPartFactHolidayList())
                if (result.equals(wrapper.getId()))
                {
                    result++;
                    notExit = true;
                }
        }
        while (notExit);

        return result;
    }
}