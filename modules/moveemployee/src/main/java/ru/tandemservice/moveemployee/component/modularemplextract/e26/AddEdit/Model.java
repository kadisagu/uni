/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e26.AddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.MainRow;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.Row;
import ru.tandemservice.moveemployee.entity.ChangeHolidayExtract;
import ru.tandemservice.moveemployee.entity.HolidayInEmployeeExtractRelation;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.io.InputStream;
import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 17.02.2012
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<ChangeHolidayExtract>
{
    private String _changedExtractDataPage;
    private String _extractDataPage;

    //ChangedExtract fields
    private DynamicListDataSource _holidayDataSourceChanged;

    private String _vacationScheduleStr;
    private String _vacationScheduleItemStr;

    //Extract fields
    public static String PERIOD = "period";
    public static String HOLIDAY = "holiday";

    public static String FULL_HOLIDAY = "fullHoliday";
    public static String HOLIDAYS_OF_PERIOD = "holidaysOfPeriod";
    public static String PERIODS_OF_HOLIDAY = "periodsOfHoliday";

    private List<Item> _changedItemList;
    private List<Item> _itemList;

    private ISingleSelectModel _changedHolidayExtractModel;

    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;

    private ISingleSelectModel _holidayTypeModel;
    private ISingleSelectModel _employeePostStatusModel;

    private Boolean _showVacationSchedule = false;

    private ISingleSelectModel _vacationScheduleModel;
    private ISingleSelectModel _vacationScheduleItemModel;

    private Map<String, EmployeePostStatus> _postStatusesMap;

    private DynamicListDataSource<MainRow> _holidayDataSource;

    private List<HolidayInEmployeeExtractRelation> _holidayRelationList;

    private Map<Row, Date> _beginPeriodMap = new HashMap<>();
    private Map<Row, Date> _endPeriodMap = new HashMap<>();
    private Map<Row, Date> _startHolidayMap = new HashMap<>();
    private Map<Row, Date> _finishHolidayMap = new HashMap<>();
    private Map<Row, Integer> _durationHolidayMap = new HashMap<>();

    private List<MainRow> _mainRowList = new ArrayList<>();

    private Map<Row, Date> _beginPeriodChangedMap = new HashMap<>();
    private Map<Row, Date> _endPeriodChangedMap = new HashMap<>();
    private Map<Row, Date> _startHolidayChangedMap = new HashMap<>();
    private Map<Row, Date> _finishHolidayChangedMap = new HashMap<>();
    private Map<Row, Integer> _durationHolidayChangedMap = new HashMap<>();

    private List<MainRow> _mainRowChangedList = new ArrayList<>();

    private Row _currentRow;

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Location getLocation()
        {
            return null;  //Body of implemented method
        }
    };

    //Calculated Getters & Setters

    public Boolean getEnableDelete()
    {
        boolean result = getMainRowList().size() == 1 && getCurrentRow().getType().equals(FULL_HOLIDAY);
        return !result;
    }

    public Boolean getShowPeriodRow()
    {
        return getCurrentRow().getType().equals(FULL_HOLIDAY) || getCurrentRow().getType().equals(PERIODS_OF_HOLIDAY);
    }

    public Boolean getShowHolidayRow()
    {
        return getCurrentRow().getType().equals(FULL_HOLIDAY) || getCurrentRow().getType().equals(HOLIDAYS_OF_PERIOD);
    }

    public Date getBeginPeriod()
    {
        return getBeginPeriodMap().get(getCurrentRow());
    }
    public void setBeginPeriod(Date date)
    {
        getBeginPeriodMap().put(getCurrentRow(), date);
    }

    public Date getEndPeriod()
    {
        return getEndPeriodMap().get(getCurrentRow());
    }
    public void setEndPeriod(Date date)
    {
        getEndPeriodMap().put(getCurrentRow(), date);
    }

    public Date getStartHoliday()
    {
        return getStartHolidayMap().get(getCurrentRow());
    }
    public void setStartHoliday(Date date)
    {
        getStartHolidayMap().put(getCurrentRow(), date);
    }

    public Date getFinishHoliday()
    {
        return getFinishHolidayMap().get(getCurrentRow());
    }
    public void setFinishHoliday(Date date)
    {
        getFinishHolidayMap().put(getCurrentRow(), date);
    }

    public Integer getDurationHoliday()
    {
        return getDurationHolidayMap().get(getCurrentRow());
    }
    public void setDurationHoliday(Integer duration)
    {
        getDurationHolidayMap().put(getCurrentRow(), duration);
    }

    //Getters & Setters

    public Boolean getShowVacationSchedule()
    {
        return _showVacationSchedule;
    }

    public void setShowVacationSchedule(Boolean showVacationSchedule)
    {
        _showVacationSchedule = showVacationSchedule;
    }

    public String getVacationScheduleStr()
    {
        return _vacationScheduleStr;
    }

    public void setVacationScheduleStr(String vacationScheduleStr)
    {
        _vacationScheduleStr = vacationScheduleStr;
    }

    public String getVacationScheduleItemStr()
    {
        return _vacationScheduleItemStr;
    }

    public void setVacationScheduleItemStr(String vacationScheduleItemStr)
    {
        _vacationScheduleItemStr = vacationScheduleItemStr;
    }

    public ISingleSelectModel getVacationScheduleModel()
    {
        return _vacationScheduleModel;
    }

    public void setVacationScheduleModel(ISingleSelectModel vacationScheduleModel)
    {
        _vacationScheduleModel = vacationScheduleModel;
    }

    public ISingleSelectModel getVacationScheduleItemModel()
    {
        return _vacationScheduleItemModel;
    }

    public void setVacationScheduleItemModel(ISingleSelectModel vacationScheduleItemModel)
    {
        _vacationScheduleItemModel = vacationScheduleItemModel;
    }

    public List<MainRow> getMainRowChangedList()
    {
        return _mainRowChangedList;
    }

    public void setMainRowChangedList(List<MainRow> mainRowChangedList)
    {
        _mainRowChangedList = mainRowChangedList;
    }

    public Map<Row, Integer> getDurationHolidayChangedMap()
    {
        return _durationHolidayChangedMap;
    }

    public void setDurationHolidayChangedMap(Map<Row, Integer> durationHolidayChangedMap)
    {
        _durationHolidayChangedMap = durationHolidayChangedMap;
    }

    public Map<Row, Date> getFinishHolidayChangedMap()
    {
        return _finishHolidayChangedMap;
    }

    public void setFinishHolidayChangedMap(Map<Row, Date> finishHolidayChangedMap)
    {
        _finishHolidayChangedMap = finishHolidayChangedMap;
    }

    public Map<Row, Date> getStartHolidayChangedMap()
    {
        return _startHolidayChangedMap;
    }

    public void setStartHolidayChangedMap(Map<Row, Date> startHolidayChangedMap)
    {
        _startHolidayChangedMap = startHolidayChangedMap;
    }

    public Map<Row, Date> getEndPeriodChangedMap()
    {
        return _endPeriodChangedMap;
    }

    public void setEndPeriodChangedMap(Map<Row, Date> endPeriodChangedMap)
    {
        _endPeriodChangedMap = endPeriodChangedMap;
    }

    public Map<Row, Date> getBeginPeriodChangedMap()
    {
        return _beginPeriodChangedMap;
    }

    public void setBeginPeriodChangedMap(Map<Row, Date> beginPeriodChangedMap)
    {
        _beginPeriodChangedMap = beginPeriodChangedMap;
    }

    public List<Item> getItemList()
    {
        return _itemList;
    }

    public void setItemList(List<Item> itemList)
    {
        _itemList = itemList;
    }

    public List<Item> getChangedItemList()
    {
        return _changedItemList;
    }

    public void setChangedItemList(List<Item> changedItemList)
    {
        _changedItemList = changedItemList;
    }

    public ISingleSelectModel getChangedHolidayExtractModel()
    {
        return _changedHolidayExtractModel;
    }

    public void setChangedHolidayExtractModel(ISingleSelectModel changedHolidayExtractModel)
    {
        _changedHolidayExtractModel = changedHolidayExtractModel;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public String getChangedExtractDataPage()
    {
        return _changedExtractDataPage;
    }

    public void setChangedExtractDataPage(String changedExtractDataPage)
    {
        _changedExtractDataPage = changedExtractDataPage;
    }

    public String getExtractDataPage()
    {
        return _extractDataPage;
    }

    public void setExtractDataPage(String extractDataPage)
    {
        _extractDataPage = extractDataPage;
    }

    public DynamicListDataSource getHolidayDataSourceChanged()
    {
        return _holidayDataSourceChanged;
    }

    public void setHolidayDataSourceChanged(DynamicListDataSource holidayDataSourceChanged)
    {
        _holidayDataSourceChanged = holidayDataSourceChanged;
    }

    public ISingleSelectModel getHolidayTypeModel()
    {
        return _holidayTypeModel;
    }

    public void setHolidayTypeModel(ISingleSelectModel holidayTypeModel)
    {
        _holidayTypeModel = holidayTypeModel;
    }

    public ISingleSelectModel getEmployeePostStatusModel()
    {
        return _employeePostStatusModel;
    }

    public void setEmployeePostStatusModel(ISingleSelectModel employeePostStatusModel)
    {
        _employeePostStatusModel = employeePostStatusModel;
    }

    public Map<String, EmployeePostStatus> getPostStatusesMap()
    {
        return _postStatusesMap;
    }

    public void setPostStatusesMap(Map<String, EmployeePostStatus> postStatusesMap)
    {
        _postStatusesMap = postStatusesMap;
    }

    public DynamicListDataSource<MainRow> getHolidayDataSource()
    {
        return _holidayDataSource;
    }

    public void setHolidayDataSource(DynamicListDataSource<MainRow> holidayDataSource)
    {
        _holidayDataSource = holidayDataSource;
    }

    public List<HolidayInEmployeeExtractRelation> getHolidayRelationList()
    {
        return _holidayRelationList;
    }

    public void setHolidayRelationList(List<HolidayInEmployeeExtractRelation> holidayRelationList)
    {
        _holidayRelationList = holidayRelationList;
    }

    public Map<Row, Date> getBeginPeriodMap()
    {
        return _beginPeriodMap;
    }

    public void setBeginPeriodMap(Map<Row, Date> beginPeriodMap)
    {
        _beginPeriodMap = beginPeriodMap;
    }

    public Map<Row, Date> getEndPeriodMap()
    {
        return _endPeriodMap;
    }

    public void setEndPeriodMap(Map<Row, Date> endPeriodMap)
    {
        _endPeriodMap = endPeriodMap;
    }

    public Map<Row, Date> getStartHolidayMap()
    {
        return _startHolidayMap;
    }

    public void setStartHolidayMap(Map<Row, Date> startHolidayMap)
    {
        _startHolidayMap = startHolidayMap;
    }

    public Map<Row, Date> getFinishHolidayMap()
    {
        return _finishHolidayMap;
    }

    public void setFinishHolidayMap(Map<Row, Date> finishHolidayMap)
    {
        _finishHolidayMap = finishHolidayMap;
    }

    public Map<Row, Integer> getDurationHolidayMap()
    {
        return _durationHolidayMap;
    }

    public void setDurationHolidayMap(Map<Row, Integer> durationHolidayMap)
    {
        _durationHolidayMap = durationHolidayMap;
    }

    public List<MainRow> getMainRowList()
    {
        return _mainRowList;
    }

    public void setMainRowList(List<MainRow> mainRowList)
    {
        _mainRowList = mainRowList;
    }

    public Row getCurrentRow()
    {
        return _currentRow;
    }

    public void setCurrentRow(Row currentRow)
    {
        _currentRow = currentRow;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }
}