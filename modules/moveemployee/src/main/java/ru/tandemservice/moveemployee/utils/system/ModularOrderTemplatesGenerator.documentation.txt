Как пользоваться ModularOrderTemplateGenerator'ом

Надо насоздавать в папке <modules>/<moduleOwner>/utils/system/emplmodularordertemplates/printforms печатные формы
выписок.

названия файлов header.txt, table.txt, modularOrder.txt зарезервированы. все остальные файлы
считаются шаблонами выписок.

в шаблоне выписок можно использовать
1. русские буквы - это будет текст выписки
2. английские буквы и символ подчеркивания - это будут параметры выписки.
3. можно вставлять rtf-теги, примерно так: {\b\ul}пример{\b0\ulnone}
hint: чтобы в тексте выписки использовать английские слова можно использовать скобки:
 {my mega text} тогда это не будет распарсено как параметры выписки.