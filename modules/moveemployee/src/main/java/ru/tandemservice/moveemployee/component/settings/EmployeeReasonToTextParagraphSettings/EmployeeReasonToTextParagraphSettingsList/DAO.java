/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeReasonToTextParagraphSettings.EmployeeReasonToTextParagraphSettingsList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;

import ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

/**
 * Create by: ashaburov
 * Date: 18.04.11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        Map<Long, EmployeeReasonToTextParagraphRel> relsMap = new HashMap<Long, EmployeeReasonToTextParagraphRel>();
        List<EmployeeReasonToTextParagraphRel> relsList = getSession().createCriteria(EmployeeReasonToTextParagraphRel.class).list();
        for (EmployeeReasonToTextParagraphRel rel : relsList)
            relsMap.put(rel.getEmployeeOrderReason().getId(), rel);

        MQBuilder builder = new MQBuilder(EmployeeOrderReasons.ENTITY_CLASS, "r");
        builder.addOrder("r", EmployeeOrderReasons.P_TITLE);
        List<EmployeeOrderReasons> list = builder.<EmployeeOrderReasons>getResultList(getSession());
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            if (relsMap.containsKey(wrapper.getEntity().getId()))
            {
                wrapper.setViewProperty(Model.P_TEXT_PARAGRAPH, relsMap.get(wrapper.getEntity().getId()).getTextParagraph());

            }
            else
            {
                wrapper.setViewProperty(Model.P_TEXT_PARAGRAPH, "");

            }
        }
    }
}
