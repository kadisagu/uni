/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphPub;

import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author ashaburov
 * Created on: 11.10.2011
 */
public interface IAbstractListParagraphPubDAO<T extends ListEmployeeExtract, Model extends AbstractListParagraphPubModel<T>> extends IUniDao<Model>
{
}