/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e28.AddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.ScienceStatusPaymentExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 19.03.2012
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<ScienceStatusPaymentExtract>
{
    private DynamicListDataSource<EmployeePostStaffRateItem> _employeeStaffRateDataSource;
    private DynamicListDataSource<FinancingSourceDetails> _staffRateDataSource;
    private DynamicListDataSource<EmployeeBonus> _paymentDataSource;

    private ISingleSelectModel _scienceStatusModel;
    private ISingleSelectModel _postModel;
    private ISingleSelectModel _financingSourceModel;
    private ISingleSelectModel _financingSourceItemModel;
    private IMultiSelectModel _employeeHRModel;
    private ISingleSelectModel _raisingCoefficientModel;
    private ISingleSelectModel _etksLevelModel;
    private ISingleSelectModel _paymentModel;
    private ISingleSelectModel _finSrcPaymentModel;
    private ISingleSelectModel _finSrcItemPaymentModel;

    private List<FinancingSourceDetails> staffRateList = new ArrayList<>();
    private List<EmployeeBonus> paymentList = new ArrayList<>();

    private String _labourContractType;
    private String _labourContractNumber;
    private Date _labourContractDate;
    private Date _labourContractBeginDate;
    private Date _labourContractEndDate;

    private boolean _addStaffListPaymentsButtonVisible = true;

    private boolean _thereAnyActiveStaffList = false;

    private String _paymentPage = getClass().getPackage().getName() + ".PaymentBlock";

    private String _updateOnChangePost;

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;
        }

        @Override
        public Location getLocation()
        {
            return null;
        }
    };

    //Calculate

    public String getStaffRateColumnId()
    {
        return "staffRate_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcColumnId()
    {
        return "finSrc_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItmColumnId()
    {
        return "finSrcItm_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getEmployeeHRId()
    {
        return "employeeHR_Id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getAssignDateId()
    {
        return "assignDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getBeginDateId()
    {
        return "beginDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getEndDateId()
    {
        return "endDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getPaymentId()
    {
        return "payment_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getAmountId()
    {
        return "amount_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcId()
    {
        return "finSrc_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItemId()
    {
        return "finSrcItem_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    //Getters & Setters

    public DynamicListDataSource<EmployeePostStaffRateItem> getEmployeeStaffRateDataSource()
    {
        return _employeeStaffRateDataSource;
    }

    public void setEmployeeStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> employeeStaffRateDataSource)
    {
        _employeeStaffRateDataSource = employeeStaffRateDataSource;
    }

    public DynamicListDataSource<FinancingSourceDetails> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<FinancingSourceDetails> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public DynamicListDataSource<EmployeeBonus> getPaymentDataSource()
    {
        return _paymentDataSource;
    }

    public void setPaymentDataSource(DynamicListDataSource<EmployeeBonus> paymentDataSource)
    {
        _paymentDataSource = paymentDataSource;
    }

    public ISingleSelectModel getScienceStatusModel()
    {
        return _scienceStatusModel;
    }

    public void setScienceStatusModel(ISingleSelectModel scienceStatusModel)
    {
        _scienceStatusModel = scienceStatusModel;
    }

    public ISingleSelectModel getPostModel()
    {
        return _postModel;
    }

    public void setPostModel(ISingleSelectModel postModel)
    {
        _postModel = postModel;
    }

    public ISingleSelectModel getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(ISingleSelectModel financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public IMultiSelectModel getEmployeeHRModel()
    {
        return _employeeHRModel;
    }

    public void setEmployeeHRModel(IMultiSelectModel employeeHRModel)
    {
        _employeeHRModel = employeeHRModel;
    }

    public ISingleSelectModel getRaisingCoefficientModel()
    {
        return _raisingCoefficientModel;
    }

    public void setRaisingCoefficientModel(ISingleSelectModel raisingCoefficientModel)
    {
        _raisingCoefficientModel = raisingCoefficientModel;
    }

    public ISingleSelectModel getEtksLevelModel()
    {
        return _etksLevelModel;
    }

    public void setEtksLevelModel(ISingleSelectModel etksLevelModel)
    {
        _etksLevelModel = etksLevelModel;
    }

    public ISingleSelectModel getPaymentModel()
    {
        return _paymentModel;
    }

    public void setPaymentModel(ISingleSelectModel paymentModel)
    {
        _paymentModel = paymentModel;
    }

    public ISingleSelectModel getFinSrcPaymentModel()
    {
        return _finSrcPaymentModel;
    }

    public void setFinSrcPaymentModel(ISingleSelectModel finSrcPaymentModel)
    {
        _finSrcPaymentModel = finSrcPaymentModel;
    }

    public ISingleSelectModel getFinSrcItemPaymentModel()
    {
        return _finSrcItemPaymentModel;
    }

    public void setFinSrcItemPaymentModel(ISingleSelectModel finSrcItemPaymentModel)
    {
        _finSrcItemPaymentModel = finSrcItemPaymentModel;
    }

    public List<FinancingSourceDetails> getStaffRateList()
    {
        return staffRateList;
    }

    public void setStaffRateList(List<FinancingSourceDetails> staffRateList)
    {
        this.staffRateList = staffRateList;
    }

    public List<EmployeeBonus> getPaymentList()
    {
        return paymentList;
    }

    public void setPaymentList(List<EmployeeBonus> paymentList)
    {
        this.paymentList = paymentList;
    }

    public String getLabourContractType()
    {
        return _labourContractType;
    }

    public void setLabourContractType(String labourContractType)
    {
        _labourContractType = labourContractType;
    }

    public String getLabourContractNumber()
    {
        return _labourContractNumber;
    }

    public void setLabourContractNumber(String labourContractNumber)
    {
        _labourContractNumber = labourContractNumber;
    }

    public Date getLabourContractDate()
    {
        return _labourContractDate;
    }

    public void setLabourContractDate(Date labourContractDate)
    {
        _labourContractDate = labourContractDate;
    }

    public Date getLabourContractBeginDate()
    {
        return _labourContractBeginDate;
    }

    public void setLabourContractBeginDate(Date labourContractBeginDate)
    {
        _labourContractBeginDate = labourContractBeginDate;
    }

    public Date getLabourContractEndDate()
    {
        return _labourContractEndDate;
    }

    public void setLabourContractEndDate(Date labourContractEndDate)
    {
        _labourContractEndDate = labourContractEndDate;
    }

    public boolean isAddStaffListPaymentsButtonVisible()
    {
        return _addStaffListPaymentsButtonVisible;
    }

    public void setAddStaffListPaymentsButtonVisible(boolean addStaffListPaymentsButtonVisible)
    {
        _addStaffListPaymentsButtonVisible = addStaffListPaymentsButtonVisible;
    }

    public boolean isThereAnyActiveStaffList()
    {
        return _thereAnyActiveStaffList;
    }

    public void setThereAnyActiveStaffList(boolean thereAnyActiveStaffList)
    {
        _thereAnyActiveStaffList = thereAnyActiveStaffList;
    }

    public String getPaymentPage()
    {
        return _paymentPage;
    }

    public void setPaymentPage(String paymentPage)
    {
        _paymentPage = paymentPage;
    }

    public String getUpdateOnChangePost()
    {
        return _updateOnChangePost;
    }

    public void setUpdateOnChangePost(String updateOnChangePost)
    {
        _updateOnChangePost = updateOnChangePost;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }
}