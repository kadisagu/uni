/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.modular.ModularEmployeeExtractPrint;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeExtractTextRelation;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author dseleznev
 * Created on: 02.12.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setExtract(getNotNull(model.getExtractId()));

        if (UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(model.getExtract().getState().getCode()))
        {
            // выписка проведена => печатная форма сохранена
            Criteria c = getSession().createCriteria(EmployeeExtractTextRelation.class);
            c.add(Restrictions.eq(EmployeeExtractTextRelation.L_EXTRACT, model.getExtract()));
            c.add(Restrictions.eq(EmployeeExtractTextRelation.P_CODE, MoveEmployeeDefines.EXTRACT_TEXT_CODE));
            EmployeeExtractTextRelation rel = (EmployeeExtractTextRelation)c.uniqueResult();
            // В процессе деперсонализации БД связь могла быть удалена
            if (rel != null)
                model.setData(rel.getText());
            else
                prepareDefaultTemplate(model);
        }
        else
            prepareDefaultTemplate(model);

    }

    private void prepareDefaultTemplate(Model model)
    {
        model.setData(MoveEmployeeDaoFacade.getMoveEmployeeDao().getTemplate(model.getExtract().getType(), MoveEmployeeDefines.EXTRACT_TEXT_CODE));
    }
}