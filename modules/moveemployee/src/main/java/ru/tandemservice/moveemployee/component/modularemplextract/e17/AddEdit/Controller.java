/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e17.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 09.09.2011
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<RevocationFromHolidayExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareHolidayDataSource(component);
        prepareDummyDataSource(component);
    }

    public void prepareDummyDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getDummyDataSource() != null)
            return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareDumyDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Дата начала", null));
        dataSource.addColumn(new SimpleColumn("Дата окончания", null));
        dataSource.addColumn(new SimpleColumn("Длительность", null));
        dataSource.addColumn(new SimpleColumn("Удалить", null));

        model.setDummyDataSource(dataSource);
    }

    public void prepareHolidayDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getHolidayDataSource() != null)
            return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareHolidayDataSource(model);
        });

        dataSource.addColumn(new BlockColumn("beginDay", "Дата начала"));
        dataSource.addColumn(new BlockColumn("endDay", "Дата окончания"));
        dataSource.addColumn(new BlockColumn("durationDay", "Длительность"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteHoliday");
        actionColumn.setDisabledProperty("disableDelete");
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setHolidayDataSource(dataSource);
    }

    public void onChangeRevocationDay(IBusinessComponent component)
    {
        Model model = getModel(component);

        //в зависимости от того что заполнено автозаполняем количество дней, либо конечную дату
        if (model.getExtract().getEmployeeHoliday() != null && model.getExtract().getRevocationFromDay() != null && model.getExtract().getRevocationToDay() != null && (model.getExtract().getRevocationDayAmount() == null || model.getExtract().getRevocationDayAmount() == 0))
        {
            Integer holidayDuration;
            if (UniempUtil.getAnnualHolidayTypes().contains(model.getExtract().getEmployeeHoliday().getHolidayType().getCode()) && model.getExtract().getEmployeeHoliday().getHolidayType().isCompensable())
                holidayDuration = EmployeeManager.instance().dao().getEmployeeHolidayDuration(model.getEmployeePost().getWorkWeekDuration(), model.getExtract().getRevocationFromDay(), model.getExtract().getRevocationToDay());
            else
                holidayDuration = (int) CommonBaseDateUtil.getBetweenPeriod(model.getExtract().getRevocationFromDay(), model.getExtract().getRevocationToDay(), Calendar.DAY_OF_YEAR) + 1;

            if (holidayDuration != null)
                model.getExtract().setRevocationDayAmount(holidayDuration);
            else
                model.getExtract().setRevocationDayAmount(0);
        }
        else if (model.getExtract().getRevocationFromDay() != null && model.getExtract().getRevocationDayAmount() != null && model.getExtract().getRevocationToDay() == null
                && model.getExtract().getEmployeeHoliday() != null)
        {
            GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            int counter = 0;
            int duration = model.getExtract().getRevocationDayAmount();

            date.setTime(model.getExtract().getRevocationFromDay());

            if (duration > 0)
            {
                do
                {
                    if (UniempUtil.getAnnualHolidayTypes().contains(model.getExtract().getEmployeeHoliday().getHolidayType().getCode()) && model.getExtract().getEmployeeHoliday().getHolidayType().isCompensable())
                    {
                        if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                            counter++;
                    }
                    else
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);

                model.getExtract().setRevocationToDay(date.getTime());
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void onChangeProvideDay(IBusinessComponent component)
    {
        Model model = getModel(component);
        IdentifiableWrapper currentWrapper = ((ViewWrapper<IdentifiableWrapper>) component.getListenerParameter()).getEntity();

        //в зависимости от того что заполнено автозаполняем количество дней, либо конечную дату
        if (model.getExtract().getEmployeeHoliday() != null && model.getBeginDayMap().get(currentWrapper) != null && model.getEndDayMap().get(currentWrapper) != null && (model.getDurationDayMap().get(currentWrapper) == null || model.getDurationDayMap().get(currentWrapper) == 0))
        {
            Integer holidayDuration;
            if (UniempUtil.getAnnualHolidayTypes().contains(model.getExtract().getEmployeeHoliday().getHolidayType().getCode()) && model.getExtract().getEmployeeHoliday().getHolidayType().isCompensable())
                holidayDuration = EmployeeManager.instance().dao().getEmployeeHolidayDuration(model.getEmployeePost().getWorkWeekDuration(), model.getBeginDayMap().get(currentWrapper), model.getEndDayMap().get(currentWrapper));
            else
                holidayDuration = (int) CommonBaseDateUtil.getBetweenPeriod(model.getBeginDayMap().get(currentWrapper), model.getEndDayMap().get(currentWrapper), Calendar.DAY_OF_YEAR) + 1;

            if (holidayDuration != null)
                model.getDurationDayMap().put(currentWrapper, holidayDuration);
            else
                model.getDurationDayMap().put(currentWrapper, 0);
        }
        else if (model.getBeginDayMap().get(currentWrapper) != null && model.getDurationDayMap().get(currentWrapper) != null && model.getEndDayMap().get(currentWrapper) == null
                && model.getExtract().getEmployeeHoliday() != null && model.getExtract().getEmployeeHoliday() != null)
        {
            GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            int counter = 0;
            int duration = model.getDurationDayMap().get(currentWrapper);

            date.setTime(model.getBeginDayMap().get(currentWrapper));

            if (duration > 0)
            {
                do
                {
                    if (UniempUtil.getAnnualHolidayTypes().contains(model.getExtract().getEmployeeHoliday().getHolidayType().getCode()) && model.getExtract().getEmployeeHoliday().getHolidayType().isCompensable())
                    {
                        if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                            counter++;
                    }
                    else
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);

                model.getEndDayMap().put(currentWrapper, date.getTime());
            }
        }
    }

    public void onClickDeleteHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);

        IdentifiableWrapper currentWrapper = (IdentifiableWrapper) ((ViewWrapper) component.getListenerParameter()).getEntity();

        model.getPartHolidayWrapperList().remove(currentWrapper);
        model.getBeginDayMap().remove(currentWrapper);
        model.getEndDayMap().remove(currentWrapper);
        model.getDurationDayMap().remove(currentWrapper);

        model.getHolidayDataSource().refresh();
    }

    public void onClickAddHoliday(IBusinessComponent component)
    {
        Model model = getModel(component);

        IdentifiableWrapper newWrapper = new IdentifiableWrapper(getNewWrapperId(model), "");
        model.getPartHolidayWrapperList().add(newWrapper);

        model.getHolidayDataSource().refresh();
    }

    /**
     * Вспомогательный метод для вычесления id враппеера частей отпуска.
     * @return Возвращает id новой части отпуска.
     */
    private long getNewWrapperId(Model model)
    {
        Long result = (long) model.getPartHolidayWrapperList().size();

        boolean notExit;
        do
        {
            notExit = false;
            for (IdentifiableWrapper wrapper : model.getPartHolidayWrapperList())
                if (result.equals(wrapper.getId()))
                {
                    result++;
                    notExit = true;
                }
        }
        while (notExit);

        return result;
    }
}