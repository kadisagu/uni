/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;
import ru.tandemservice.moveemployee.entity.HolidayInEmpHldyExtractRelation;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.io.InputStream;
import java.util.*;

/**
 * @author dseleznev
 * Created on: 21.11.2008
 */
@Input( {
    @Bind(key = "employeeId", binding = "employee.id"),
    @Bind(key = "employeePostId", binding = "employeePost.id"), 
    @Bind(key = "extractTypeId", binding = "extractType.id"), 
    @Bind(key = "extractId", binding = "extractId") 
    })
public class Model extends CommonModularEmployeeExtractAddEditModel<EmployeeHolidayExtract>
{
    public static String FULL_HOLIDAY = "fullHoliday";
    public static String HOLIDAYS_OF_PERIOD = "holidaysOfPeriod";
    public static String PERIODS_OF_HOLIDAY = "periodsOfHoliday";

    private Boolean _showVacationSchedule = false;

    private ISingleSelectModel _vacationScheduleModel;
    private ISingleSelectModel _vacationScheduleItemModel;

    private ISingleSelectModel _holidayTypeModel;

    private Map<String, EmployeePostStatus> _postStatusesMap;
    private ISingleSelectModel _employeePostStatusModel;

    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;
    private DynamicListDataSource<MainRow> _holidayDataSource;

    private List<HolidayInEmpHldyExtractRelation> _holidayRelationList;

    private Map<Row, Date> _beginPeriodMap = new HashMap<>();
    private Map<Row, Date> _endPeriodMap = new HashMap<>();
    private Map<Row, Date> _startHolidayMap = new HashMap<>();
    private Map<Row, Date> _finishHolidayMap = new HashMap<>();
    private Map<Row, Integer> _durationHolidayMap = new HashMap<>();

    private List<MainRow> _mainRowList = new ArrayList<>();

    private Row _currentRow;

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Location getLocation()
        {
            return null;  //Body of implemented method
        }
    };

    //Calculated Getters & Setters

    public Boolean getEnableDelete()
    {
        boolean result = getMainRowList().size() == 1 && getCurrentRow().getType().equals(FULL_HOLIDAY);
        return !result;
    }

    public Boolean getShowPeriodRow()
    {
        return getCurrentRow().getType().equals(FULL_HOLIDAY) || getCurrentRow().getType().equals(PERIODS_OF_HOLIDAY);
    }

    public Boolean getShowHolidayRow()
    {
        return getCurrentRow().getType().equals(FULL_HOLIDAY) || getCurrentRow().getType().equals(HOLIDAYS_OF_PERIOD);
    }

    public Date getBeginPeriod()
    {
        return getBeginPeriodMap().get(getCurrentRow());
    }
    public void setBeginPeriod(Date date)
    {
        getBeginPeriodMap().put(getCurrentRow(), date);
    }

    public Date getEndPeriod()
    {
        return getEndPeriodMap().get(getCurrentRow());
    }
    public void setEndPeriod(Date date)
    {
        getEndPeriodMap().put(getCurrentRow(), date);
    }

    public Date getStartHoliday()
    {
        return getStartHolidayMap().get(getCurrentRow());
    }
    public void setStartHoliday(Date date)
    {
        getStartHolidayMap().put(getCurrentRow(), date);
    }

    public Date getFinishHoliday()
    {
        return getFinishHolidayMap().get(getCurrentRow());
    }
    public void setFinishHoliday(Date date)
    {
        getFinishHolidayMap().put(getCurrentRow(), date);
    }

    public Integer getDurationHoliday()
    {
        return getDurationHolidayMap().get(getCurrentRow());
    }
    public void setDurationHoliday(Integer duration)
    {
        getDurationHolidayMap().put(getCurrentRow(), duration);
    }

    //Getters & Setters

    public Boolean getShowVacationSchedule()
    {
        return _showVacationSchedule;
    }

    public void setShowVacationSchedule(Boolean showVacationSchedule)
    {
        _showVacationSchedule = showVacationSchedule;
    }

    public ISingleSelectModel getVacationScheduleModel()
    {
        return _vacationScheduleModel;
    }

    public void setVacationScheduleModel(ISingleSelectModel vacationScheduleModel)
    {
        _vacationScheduleModel = vacationScheduleModel;
    }

    public ISingleSelectModel getVacationScheduleItemModel()
    {
        return _vacationScheduleItemModel;
    }

    public void setVacationScheduleItemModel(ISingleSelectModel vacationScheduleItemModel)
    {
        _vacationScheduleItemModel = vacationScheduleItemModel;
    }

    public ISingleSelectModel getEmployeePostStatusModel()
    {
        return _employeePostStatusModel;
    }

    public void setEmployeePostStatusModel(ISingleSelectModel employeePostStatusModel)
    {
        _employeePostStatusModel = employeePostStatusModel;
    }

    public Map<String, EmployeePostStatus> getPostStatusesMap()
    {
        return _postStatusesMap;
    }

    public void setPostStatusesMap(Map<String, EmployeePostStatus> postStatusesMap)
    {
        _postStatusesMap = postStatusesMap;
    }

    public List<HolidayInEmpHldyExtractRelation> getHolidayRelationList()
    {
        return _holidayRelationList;
    }

    public void setHolidayRelationList(List<HolidayInEmpHldyExtractRelation> holidayRelationList)
    {
        _holidayRelationList = holidayRelationList;
    }

    public Map<Row, Integer> getDurationHolidayMap()
    {
        return _durationHolidayMap;
    }

    public void setDurationHolidayMap(Map<Row, Integer> durationHolidayMap)
    {
        _durationHolidayMap = durationHolidayMap;
    }

    public Map<Row, Date> getBeginPeriodMap()
    {
        return _beginPeriodMap;
    }

    public void setBeginPeriodMap(Map<Row, Date> beginPeriodMap)
    {
        _beginPeriodMap = beginPeriodMap;
    }

    public Map<Row, Date> getEndPeriodMap()
    {
        return _endPeriodMap;
    }

    public void setEndPeriodMap(Map<Row, Date> endPeriodMap)
    {
        _endPeriodMap = endPeriodMap;
    }

    public Map<Row, Date> getStartHolidayMap()
    {
        return _startHolidayMap;
    }

    public void setStartHolidayMap(Map<Row, Date> startHolidayMap)
    {
        _startHolidayMap = startHolidayMap;
    }

    public Map<Row, Date> getFinishHolidayMap()
    {
        return _finishHolidayMap;
    }

    public void setFinishHolidayMap(Map<Row, Date> finishHolidayMap)
    {
        _finishHolidayMap = finishHolidayMap;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    public List<MainRow> getMainRowList()
    {
        return _mainRowList;
    }

    public void setMainRowList(List<MainRow> mainRowList)
    {
        _mainRowList = mainRowList;
    }

    public Row getCurrentRow()
    {
        return _currentRow;
    }

    public void setCurrentRow(Row currentRow)
    {
        _currentRow = currentRow;
    }

    public DynamicListDataSource<MainRow> getHolidayDataSource()
    {
        return _holidayDataSource;
    }

    public void setHolidayDataSource(DynamicListDataSource<MainRow> holidayDataSource)
    {
        _holidayDataSource = holidayDataSource;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public ISingleSelectModel getHolidayTypeModel()
    {
        return _holidayTypeModel;
    }

    public void setHolidayTypeModel(ISingleSelectModel holidayTypeModel)
    {
        _holidayTypeModel = holidayTypeModel;
    }
}