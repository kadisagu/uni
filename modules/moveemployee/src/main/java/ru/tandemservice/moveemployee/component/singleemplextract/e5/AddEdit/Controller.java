/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e5.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 23.09.2009
 */
public class Controller extends CommonSingleEmployeeExtractAddEditController<EmployeeHolidaySExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        onChangeHolidayType(component);
        prepareStaffRateDataSource(component);
    }

    public void onChangeSecondPeriod(IBusinessComponent component)
    {

    }

    public void onChangeHolidayType(IBusinessComponent component)
    {
        Model model = getModel(component);
        HolidayType holidayType = model.getExtract().getHolidayType();
        if (null != holidayType && UniempDefines.HOLIDAY_TYPE_ANNUAL.equals(holidayType.getCode()))
        {
            model.setNeedMainHoliday(true);
            model.setNeedSecondHoliday(false);
            model.getExtract().setSecondHolidayBeginDate(null);
            model.getExtract().setSecondHolidayEndDate(null);
            model.getExtract().setSecondHolidayDuration(null);
        }
        else
        {
            if (model.getExtract().getVacationScheduleItem() != null)
                model.setNeedMainHoliday(true);
            else
                model.setNeedMainHoliday(false);
            model.setNeedSecondHoliday(null != holidayType);
        }
        if (holidayType == null || !holidayType.isChildCare())
        {
            model.getExtract().setChildNumber(null);
        }

        model.setNeedFirstPeriod(false);
        if (holidayType != null)
        {
            for (String code : model.HOLIDAY_TYPES_FOR_PERIOD)
                if (code.equals(holidayType.getCode()))
                    model.setNeedFirstPeriod(true);
        }

        if (model.isAddForm() && null != holidayType && (UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_1_5.equals(holidayType.getCode()) || UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_3.equals(holidayType.getCode())))
        {
            StringBuilder builder = new StringBuilder("Назначить ежемесячное пособие по уходу за ребенком до достижения им возраста 1,5 лет в размере __% среднего заработка из средств соцстраха с ");
            builder.append("__.__.20__ по __.__.20__ и ежемесячную компенсационную выплату в размере __,_ руб. из ФОТ до достижения им возраста 3-х лет с __.__.20__ по __.__.20__");
            model.getExtract().setOptionalCondition(builder.toString());
        }

        if (null != holidayType)
        {
            if (UniempDefines.HOLIDAY_TYPE_ANNUAL.equals(holidayType.getCode()))
                model.getExtract().setNewEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
            if (UniempDefines.HOLIDAY_TYPE_WITHOUT_SALARY.equals(holidayType.getCode()) || UniempDefines.HOLIDAY_TYPE_ENTRANCE_EXAMS_WITHOUT_SALARY.equals(holidayType.getCode()))
                model.getExtract().setNewEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR));
            else if (UniempDefines.HOLIDAY_TYPE_PREGNANCY_AND_CHILD_BORN.equals(holidayType.getCode()))
                model.getExtract().setNewEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY));
            else if (UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_1_5.equals(holidayType.getCode()) || UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_3.equals(holidayType.getCode()))
                model.getExtract().setNewEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE));
            else
                model.getExtract().setNewEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        } else
        {
            model.getExtract().setNewEmployeePostStatus(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        }

        onChangeDateOrDuration(component);
    }

    public void onChangeDateOrDuration(IBusinessComponent component)
    {
        Model model = getModel(component);
        EmployeeHolidaySExtract extract = model.getExtract();

        extract.setBeginDate(CommonBaseDateUtil.getMinDate(extract.getMainHolidayBeginDate(), extract.getMainHolidayEndDate(), extract.getSecondHolidayBeginDate(), extract.getSecondHolidayEndDate()));
        extract.setEndDate(CommonBaseDateUtil.getMaxDate(extract.getMainHolidayBeginDate(), extract.getMainHolidayEndDate(), extract.getSecondHolidayBeginDate(), extract.getSecondHolidayEndDate()));

//        IEmployeeDAO dao = UniDaoFacade.getEmployeeDao();
        EmployeeWorkWeekDuration duration = model.getEmployeePost().getWorkWeekDuration();
        if (extract.getMainHolidayDuration() == null && extract.getMainHolidayBeginDate() != null && extract.getMainHolidayEndDate() != null)
        {
            if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                extract.setMainHolidayDuration(EmployeeManager.instance().dao().getEmployeeHolidayDuration(duration, extract.getMainHolidayBeginDate(), extract.getMainHolidayEndDate()));
            else
                extract.setMainHolidayDuration((int) CommonBaseDateUtil.getBetweenPeriod(extract.getMainHolidayBeginDate(), extract.getMainHolidayEndDate(), Calendar.DAY_OF_YEAR) + 1);
        }
        if (extract.getSecondHolidayDuration() == null && extract.getSecondHolidayBeginDate() != null && extract.getSecondHolidayEndDate() != null && extract.getHolidayType() != null)
        {
            if (UniempUtil.getAnnualHolidayTypes().contains(extract.getHolidayType().getCode()) && extract.getHolidayType().isCompensable())
                extract.setSecondHolidayDuration(EmployeeManager.instance().dao().getEmployeeHolidayDuration(duration, extract.getSecondHolidayBeginDate(), extract.getSecondHolidayEndDate()));
            else
                extract.setSecondHolidayDuration((int) CommonBaseDateUtil.getBetweenPeriod(extract.getSecondHolidayBeginDate(), extract.getSecondHolidayEndDate(), Calendar.DAY_OF_YEAR) + 1);
        }

        int fullDuration = (extract.getMainHolidayDuration() != null ? extract.getMainHolidayDuration() : 0) +
        (extract.getSecondHolidayDuration() != null ? extract.getSecondHolidayDuration() : 0);
        extract.setDuration(fullDuration);
    }

    public void onChangeMainDuration(IBusinessComponent component)
    {
        Model model = getModel(component);
        EmployeeHolidaySExtract extract = model.getExtract();
        if (model.getExtract().getMainHolidayBeginDate() != null && model.getExtract().getMainHolidayDuration() != null &&
                model.getExtract().getMainHolidayEndDate() == null)
        {
            GregorianCalendar date = (GregorianCalendar) GregorianCalendar.getInstance();
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            int counter = 0;
            int duration = model.getExtract().getMainHolidayDuration();

            date.setTime(model.getExtract().getMainHolidayBeginDate());

            if (duration > 0)
            {
                do
                {
                    if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                    {
                        if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                            counter++;
                    }
                    else
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);

                model.getExtract().setMainHolidayEndDate(date.getTime());

                onChangeDateOrDuration(component);
            }
        }

        Integer fullDuration = (extract.getMainHolidayDuration() != null ? extract.getMainHolidayDuration() : 0) +
        (extract.getSecondHolidayDuration() != null ? extract.getSecondHolidayDuration() : 0);
        extract.setDuration(fullDuration);
    }

    public void onChangeSecondDuration(IBusinessComponent component)
    {
        Model model = getModel(component);
        EmployeeHolidaySExtract extract = model.getExtract();
        if (model.getExtract().getSecondHolidayBeginDate() != null && model.getExtract().getSecondHolidayDuration() != null &&
                model.getExtract().getSecondHolidayEndDate() == null && extract.getHolidayType() != null)
        {
            GregorianCalendar date = (GregorianCalendar) GregorianCalendar.getInstance();
            EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
            int counter = 0;
            int duration = model.getExtract().getSecondHolidayDuration();

            date.setTime(model.getExtract().getSecondHolidayBeginDate());

            if (duration > 0)
            {
                do
                {
                    if (UniempUtil.getAnnualHolidayTypes().contains(extract.getHolidayType().getCode()) && extract.getHolidayType().isCompensable())
                    {
                        if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                            counter++;
                    }
                    else
                        counter++;

                    if (counter != duration)
                        date.add(Calendar.DATE, 1);
                }
                while (counter < duration);

                model.getExtract().setSecondHolidayEndDate(date.getTime());

                onChangeDateOrDuration(component);
            }
        }

        Integer fullDuration = (extract.getMainHolidayDuration() != null ? extract.getMainHolidayDuration() : 0) +
        (extract.getSecondHolidayDuration() != null ? extract.getSecondHolidayDuration() : 0);
        extract.setDuration(fullDuration);
    }

    public void onChangeVacationScheduleItem(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeHolidaySExtract extract = model.getExtract();

        if (extract.getVacationScheduleItem() == null)
        {
            model.setNeedMainHoliday(false);
            return;
        }

        model.setNeedMainHoliday(true);

        if (extract.getMainHolidayBeginDate() == null && extract.getMainHolidayDuration() == null)
        {
            extract.setMainHolidayBeginDate(extract.getVacationScheduleItem().getFactDate() != null ? extract.getVacationScheduleItem().getFactDate() : extract.getVacationScheduleItem().getPlanDate());
            extract.setMainHolidayDuration(extract.getVacationScheduleItem().getDaysAmount());

            onChangeMainDuration(component);
        }
    }

    private void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setStaffRateDataSource(dataSource);
    }
}