/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeModularOrderParagraphsSortRuleSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        List<EmployeeModularOrderParagraphsSortRule> list = getList(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_DAO_NAME);
        DynamicListDataSource<EmployeeModularOrderParagraphsSortRule> dataSource = model.getDataSource();

        dataSource.setTotalSize(list.size());
        dataSource.createPage(list);
        dataSource.setCountRow(list.size());
    }

    @Override
    public void setActive(Long ruleId)
    {
        new DQLUpdateBuilder(EmployeeModularOrderParagraphsSortRule.class)
                .set(EmployeeModularOrderParagraphsSortRule.P_ACTIVE, DQLExpressions.value(Boolean.FALSE))
                .createStatement(getSession())
                .execute();

        EmployeeModularOrderParagraphsSortRule rule = get(EmployeeModularOrderParagraphsSortRule.class, ruleId);

        rule.setActive(true);

        update(rule);
    }
}