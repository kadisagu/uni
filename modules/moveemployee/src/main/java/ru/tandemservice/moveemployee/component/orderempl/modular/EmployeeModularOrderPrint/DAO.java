/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderPrint;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.EmployeeOrderTextRelation;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 *         Created on: 20.01.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder((EmployeeModularOrder) getNotNull(model.getOrderId()));

        if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(model.getOrder().getState().getCode()))
        {
            // приказ проведен => печатная форма сохранена
            Criteria c = getSession().createCriteria(EmployeeOrderTextRelation.class);
            c.add(Restrictions.eq(EmployeeOrderTextRelation.L_ORDER, model.getOrder()));
            EmployeeOrderTextRelation rel = (EmployeeOrderTextRelation) c.uniqueResult();
            if (rel == null)
                throw new ApplicationException("Невозможно напечатать приказ, так как при проведении приказа не была сохранена его печатная форма.");
            model.setData(rel.getText());
        } else
        {
            EmployeeExtractType modularOrderType = getCatalogItem(EmployeeExtractType.class, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_MODULAR_ORDER_CODE);
            model.setData(get(MoveemployeeTemplate.class, MoveemployeeTemplate.L_TYPE, modularOrderType).getContent());
            List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(model.getOrder());
            List<String[]> visaData = new ArrayList<String[]>();
            for (Visa visa : visaList)
            {
                visaData.add(new String[]{visa.getPossibleVisa().getTitle(), visa.getPossibleVisa().getEntity().getPerson().getFullFio()});
            }
            model.setVisaData(visaData.toArray(new String[visaData.size()][]));
        }
    }
}