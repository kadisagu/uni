/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeReasonToTextParagraphSettings.EmployeeReasonToTextParagraphSettingsEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * Create by: ashaburov
 * Date: 19.04.11
 */
public interface IDAO extends IUniDao<Model>
{
}
