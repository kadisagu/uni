/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e19.Pub;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.entity.EmployeeActingExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.11.2011
 */
public class Model extends ModularEmployeeExtractPubModel<EmployeeActingExtract>
{
    private String _missingDataStr;
    private String _actingDateStr;

    //Getters & Setters

    public String getActingDateStr()
    {
        return _actingDateStr;
    }

    public void setActingDateStr(String actingDateStr)
    {
        _actingDateStr = actingDateStr;
    }

    public String getMissingDataStr()
    {
        return _missingDataStr;
    }

    public void setMissingDataStr(String missingDataStr)
    {
        _missingDataStr = missingDataStr;
    }
}