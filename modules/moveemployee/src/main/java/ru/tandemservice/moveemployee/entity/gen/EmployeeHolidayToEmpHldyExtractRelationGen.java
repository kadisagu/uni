package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayToEmpHldyExtractRelation;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь созданных отпусков сотрудника в приказе «Об отпуске»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeHolidayToEmpHldyExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeHolidayToEmpHldyExtractRelation";
    public static final String ENTITY_NAME = "employeeHolidayToEmpHldyExtractRelation";
    public static final int VERSION_HASH = -1668381541;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_HOLIDAY_EXTRACT = "employeeHolidayExtract";
    public static final String L_EMPLOYEE_HOLIDAY = "employeeHoliday";

    private EmployeeHolidayExtract _employeeHolidayExtract;     // Выписка из сборного приказа по кадровому составу. Об отпуске
    private EmployeeHoliday _employeeHoliday;     // Отпуск сотрудника

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по кадровому составу. Об отпуске. Свойство не может быть null.
     */
    @NotNull
    public EmployeeHolidayExtract getEmployeeHolidayExtract()
    {
        return _employeeHolidayExtract;
    }

    /**
     * @param employeeHolidayExtract Выписка из сборного приказа по кадровому составу. Об отпуске. Свойство не может быть null.
     */
    public void setEmployeeHolidayExtract(EmployeeHolidayExtract employeeHolidayExtract)
    {
        dirty(_employeeHolidayExtract, employeeHolidayExtract);
        _employeeHolidayExtract = employeeHolidayExtract;
    }

    /**
     * @return Отпуск сотрудника. Свойство не может быть null.
     */
    @NotNull
    public EmployeeHoliday getEmployeeHoliday()
    {
        return _employeeHoliday;
    }

    /**
     * @param employeeHoliday Отпуск сотрудника. Свойство не может быть null.
     */
    public void setEmployeeHoliday(EmployeeHoliday employeeHoliday)
    {
        dirty(_employeeHoliday, employeeHoliday);
        _employeeHoliday = employeeHoliday;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeHolidayToEmpHldyExtractRelationGen)
        {
            setEmployeeHolidayExtract(((EmployeeHolidayToEmpHldyExtractRelation)another).getEmployeeHolidayExtract());
            setEmployeeHoliday(((EmployeeHolidayToEmpHldyExtractRelation)another).getEmployeeHoliday());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeHolidayToEmpHldyExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeHolidayToEmpHldyExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeHolidayToEmpHldyExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeHolidayExtract":
                    return obj.getEmployeeHolidayExtract();
                case "employeeHoliday":
                    return obj.getEmployeeHoliday();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeHolidayExtract":
                    obj.setEmployeeHolidayExtract((EmployeeHolidayExtract) value);
                    return;
                case "employeeHoliday":
                    obj.setEmployeeHoliday((EmployeeHoliday) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeHolidayExtract":
                        return true;
                case "employeeHoliday":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeHolidayExtract":
                    return true;
                case "employeeHoliday":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeHolidayExtract":
                    return EmployeeHolidayExtract.class;
                case "employeeHoliday":
                    return EmployeeHoliday.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeHolidayToEmpHldyExtractRelation> _dslPath = new Path<EmployeeHolidayToEmpHldyExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeHolidayToEmpHldyExtractRelation");
    }
            

    /**
     * @return Выписка из сборного приказа по кадровому составу. Об отпуске. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayToEmpHldyExtractRelation#getEmployeeHolidayExtract()
     */
    public static EmployeeHolidayExtract.Path<EmployeeHolidayExtract> employeeHolidayExtract()
    {
        return _dslPath.employeeHolidayExtract();
    }

    /**
     * @return Отпуск сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayToEmpHldyExtractRelation#getEmployeeHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> employeeHoliday()
    {
        return _dslPath.employeeHoliday();
    }

    public static class Path<E extends EmployeeHolidayToEmpHldyExtractRelation> extends EntityPath<E>
    {
        private EmployeeHolidayExtract.Path<EmployeeHolidayExtract> _employeeHolidayExtract;
        private EmployeeHoliday.Path<EmployeeHoliday> _employeeHoliday;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по кадровому составу. Об отпуске. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayToEmpHldyExtractRelation#getEmployeeHolidayExtract()
     */
        public EmployeeHolidayExtract.Path<EmployeeHolidayExtract> employeeHolidayExtract()
        {
            if(_employeeHolidayExtract == null )
                _employeeHolidayExtract = new EmployeeHolidayExtract.Path<EmployeeHolidayExtract>(L_EMPLOYEE_HOLIDAY_EXTRACT, this);
            return _employeeHolidayExtract;
        }

    /**
     * @return Отпуск сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayToEmpHldyExtractRelation#getEmployeeHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> employeeHoliday()
        {
            if(_employeeHoliday == null )
                _employeeHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_EMPLOYEE_HOLIDAY, this);
            return _employeeHoliday;
        }

        public Class getEntityClass()
        {
            return EmployeeHolidayToEmpHldyExtractRelation.class;
        }

        public String getEntityName()
        {
            return "employeeHolidayToEmpHldyExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
