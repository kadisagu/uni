/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e17;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.uniemp.UniempDefines;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 09.09.2011
 */
public class RevocationFromHolidayExtractPrint implements IPrintFormCreator<RevocationFromHolidayExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RevocationFromHolidayExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("revocationFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getRevocationFromDay()));

        String revocationDetails = extract.getRevocationToDay() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getRevocationToDay()) : "";
        revocationDetails += extract.getRevocationDayAmount() != null ? " на " + NumberConvertingUtil.getCalendarDaysWithName(extract.getRevocationDayAmount()) : "";
        modifier.put("revocationDetails", revocationDetails);

        int part = 2;
        //если отмечено Предоставить часть неиспользованного отпуска, то формируем эту часть
        if (extract.isProvideUnusedHoliday())
        {
            String title = part++ + ". ПРЕДОСТАВИТЬ";
            String FIO = extract.getEmployeeFioDative();

            List<ProvideDateToExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getProvideHolidayFromRevocationHolidayExtract(extract);
            String providePartStr = "";
            //подготоавливаем список частей в виде строки
            for (ProvideDateToExtractRelation relation : relationList)
            {
                providePartStr += NumberConvertingUtil.getCalendarDaysWithName(relation.getProvideDurationDay()) + " с " +
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(relation.getProvideBeginDay()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(relation.getProvideEndDay());

                if (relationList.indexOf(relation) + 1 == relationList.size() - 1)
                    providePartStr += " и ";
                else if (relationList.indexOf(relation) == relationList.size() - 1)
                    providePartStr += ".";
                else
                    providePartStr += ", ";
            }

            String body = ", " +
                    CommonExtractPrintUtil.getPostPrintingTitle(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.DATIVE_CASE, true) + " " +
                    CommonExtractPrintUtil.getOrgUnitPrintingTitle(extract.getEntity().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, false, true) + " часть" +
                    (extract.getEmployeeHoliday().getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL) ? " ежегодного" : "")  + " отпуска – " +
                    providePartStr;

            modifier.put("secondPartTitle", title);
            modifier.put("secondPartFIO", FIO);
            modifier.put("secondPartBody", body);
        }
        else
        {
            List<String> fieldList = new ArrayList<>(3);
            fieldList.add("secondPartTitle");
            fieldList.add("secondPartFIO");
            fieldList.add("secondPartBody");
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, fieldList, false, true);
        }

        if (extract.isPaymentUnusedHoliday())
        {
            String title = part + ". ВЫПЛАТИТЬ";
            String FIO = extract.getEmployeeFioDative();
            String body = ", " +
                    CommonExtractPrintUtil.getPostPrintingTitle(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.DATIVE_CASE, true) + " " +
                    CommonExtractPrintUtil.getOrgUnitPrintingTitle(extract.getEntity().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, false, true) +
                    " компенсацию за неиспользованную часть" +
                    (extract.getEmployeeHoliday().getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL) ? " ежегодного" : "") + " отпуска" +
                    ((extract.getEmployeeHoliday().getBeginDate() != null && extract.getEmployeeHoliday().getEndDate() != null) ?
                            " за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEmployeeHoliday().getBeginDate()) +
                            " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEmployeeHoliday().getEndDate()) : "") +
                    " " + NumberConvertingUtil.getCalendarDaysWithName(extract.getCompensationUnusedHoliday()) + ".";

            modifier.put("thirdPartTitle", title);
            modifier.put("thirdPartFIO", FIO);
            modifier.put("thirdPartBody", body);
        }
        else
        {
            List<String> fieldList = new ArrayList<>(3);
            fieldList.add("thirdPartTitle");
            fieldList.add("thirdPartFIO");
            fieldList.add("thirdPartBody");
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, fieldList, false, true);
        }

        if (extract.getOptionalCondition() != null)
            modifier.put("optionalCondition", extract.getOptionalCondition());
        else
            modifier.put("optionalCondition", "");

        modifier.modify(document);
        return document;
    }
}