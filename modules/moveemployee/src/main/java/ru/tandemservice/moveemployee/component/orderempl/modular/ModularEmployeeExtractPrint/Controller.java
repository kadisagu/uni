/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.modular.ModularEmployeeExtractPrint;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.document.RtfDocument;

import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author dseleznev
 * Created on: 02.12.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    @SuppressWarnings("unchecked")
    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        Model model = getModel(component);

        deactivate(component);

        if (UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(model.getExtract().getState().getCode()))
        {
            // выписка уже проведена и печатная форма сохранена
            return new CommonBaseRenderer().rtf().document(model.getData()).fileName("Extract.rtf");
        } else
        {
            String printName = EntityRuntime.getMeta(model.getExtractId()).getName() + "_extractPrint";
            IPrintFormCreator componentPrint = (IPrintFormCreator) ApplicationRuntime.getBean(printName);
            RtfDocument document = componentPrint.createPrintForm(model.getData(), model.getExtract());
            return new CommonBaseRenderer().rtf().document(document).fileName("Extract.rtf");
        }
    }
}