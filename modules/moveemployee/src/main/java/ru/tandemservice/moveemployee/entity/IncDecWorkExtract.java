package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.IContractAndAssigmentExtract;
import ru.tandemservice.moveemployee.entity.gen.IncDecWorkExtractGen;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;

import java.util.Date;

/**
 * Выписка из сборного приказа по кадровому составу. Увеличение (уменьшение) объема выполняемой работы
 */
public class IncDecWorkExtract extends IncDecWorkExtractGen implements IContractAndAssigmentExtract
{
}