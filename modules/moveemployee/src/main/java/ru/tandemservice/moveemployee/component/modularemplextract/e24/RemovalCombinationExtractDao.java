/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e24;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.RemovalCombinationExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 29.12.2011
 */
public class RemovalCombinationExtractDao extends UniBaseDao implements IExtractComponentDao<RemovalCombinationExtract>
{
    public void doCommit(RemovalCombinationExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        if (extract.getCombinationPost().getEndDate() != null)
        {
            if (extract.getRemovalDate().after(extract.getCombinationPost().getEndDate()))
            {
                extract.setChangeEndDate(false);
                extract.setEndDateOld(null);
            }
            else
            {
                extract.setChangeEndDate(true);
                extract.setEndDateOld(extract.getCombinationPost().getEndDate());
                extract.getCombinationPost().setEndDate(extract.getRemovalDate());
            }
        }
        else
        {
            extract.setChangeEndDate(true);
            extract.setEndDateOld(null);
            extract.getCombinationPost().setEndDate(extract.getRemovalDate());
        }

        update(extract.getCombinationPost());

        StaffList activaStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(extract.getCombinationPost().getOrgUnit());

        if (activaStaffList != null)
        {
            extract.setChangeStaffList(true);
            extract.setChangedStaffList(activaStaffList);

            MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), activaStaffList));
            builder.add(MQExpression.eq("b", StaffListAllocationItem.combination().s(), true));
            builder.add(MQExpression.eq("b", StaffListAllocationItem.combinationPost().s(), extract.getCombinationPost()));

            for (StaffListAllocationItem item : builder.<StaffListAllocationItem>getResultList(getSession()))
            {
                if (item.getEmployeePost() != null && item.getEmployeePost().equals(extract.getEntity()))
                {
                    item.setEmployeePost(null);
                    update(item);
                }
            }
        }
        else
        {
            extract.setChangeStaffList(false);
            extract.setChangedStaffList(null);
        }

        update(extract);
    }

    public void doRollback(RemovalCombinationExtract extract, Map parameters)
    {
        if (extract.getChangeEndDate())
        {
            extract.getCombinationPost().setEndDate(extract.getEndDateOld());
            extract.setEndDateOld(null);
            update(extract.getCombinationPost());
        }

        if (extract.getChangeStaffList())
        {
            MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", StaffListAllocationItem.staffListItem().staffList().s(), extract.getChangedStaffList()));
            builder.add(MQExpression.eq("b", StaffListAllocationItem.combination().s(),  true));
            builder.add(MQExpression.eq("b", StaffListAllocationItem.combinationPost().s(), extract.getCombinationPost()));

            for (StaffListAllocationItem item : builder.<StaffListAllocationItem>getResultList(getSession()))
            {
                if (item.getEmployeePost() == null)
                {
                    item.setEmployeePost(extract.getEntity());
                    update(item);
                }
            }
        }
    }
}