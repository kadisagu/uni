/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.menuempl.ModularOrdersFormation;

import java.util.List;
import java.util.Map;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * @author dseleznev
 * Created on: 10.11.2008
 */
@SuppressWarnings("unchecked")
public class Model
{
    private List<OrderStates> _orderStateList;
    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;
    private Map<Long, Boolean> _individualOrderMap;
    private Map<Long, ModularEmployeeExtract> _indexExtractOrderMap;

    //Getters & Setters

    public Map<Long, ModularEmployeeExtract> getIndexExtractOrderMap()
    {
        return _indexExtractOrderMap;
    }

    public void setIndexExtractOrderMap(Map<Long, ModularEmployeeExtract> indexExtractOrderMap)
    {
        _indexExtractOrderMap = indexExtractOrderMap;
    }

    public Map<Long, Boolean> getIndividualOrderMap()
    {
        return _individualOrderMap;
    }

    public void setIndividualOrderMap(Map<Long, Boolean> individualOrderMap)
    {
        _individualOrderMap = individualOrderMap;
    }

    public List<OrderStates> getOrderStateList()
    {
        return _orderStateList;
    }

    public void setOrderStateList(List<OrderStates> orderStateList)
    {
        _orderStateList = orderStateList;
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }
}