/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e4.ExtractAddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit.AbstractListExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 04.10.2011
 */
public class Model extends AbstractListExtractAddEditModel<EmployeeHolidayEmplListExtract>
{
    private Boolean _needMainHoliday;
    private Boolean _needSecondHoliday;
    private ISingleSelectModel _holidayTypesListModel;
    private ISingleSelectModel _postStatusesModel;
    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;
    private Map<String, EmployeePostStatus> _postStatusesMap;

    private ISingleSelectModel _orgUnitModel;
    private ISingleSelectModel _vacationScheduleModel;
    private OrgUnit _orgUnit;
    private VacationSchedule _vacationSchedule;
    private DynamicListDataSource _vacationScheduleDataSource;

    private ISingleSelectModel _addFormatModel;
    private IdentifiableWrapper _addFormat;

    private ISingleSelectModel _vacationScheduleSModel;
    private ISingleSelectModel _vacationScheduleItemSModel;

    private Boolean _hasVacationSchedule = false;

    private String _singleAddPage = Model.class.getPackage().getName() + ".SingleAdd";
    private String _multiAddPage = Model.class.getPackage().getName() + ".MultiAdd";

    //Calculated

    public String getPeriodBeginDayColumnId()
    {
        return "periodBeginDay_id_" + getVacationScheduleDataSource().getCurrentEntity().getId();
    }

    public String getPeriodEndDayColumnId()
    {
        return "periodEndDay_id_" + getVacationScheduleDataSource().getCurrentEntity().getId();
    }

    //Getters & Setters

    public ISingleSelectModel getVacationScheduleSModel()
    {
        return _vacationScheduleSModel;
    }

    public void setVacationScheduleSModel(ISingleSelectModel vacationScheduleSModel)
    {
        _vacationScheduleSModel = vacationScheduleSModel;
    }

    public ISingleSelectModel getVacationScheduleItemSModel()
    {
        return _vacationScheduleItemSModel;
    }

    public void setVacationScheduleItemSModel(ISingleSelectModel vacationScheduleItemSModel)
    {
        _vacationScheduleItemSModel = vacationScheduleItemSModel;
    }

    public Boolean getHasVacationSchedule()
    {
        return _hasVacationSchedule;
    }

    public void setHasVacationSchedule(Boolean hasVacationSchedule)
    {
        _hasVacationSchedule = hasVacationSchedule;
    }

    public ISingleSelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISingleSelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public ISingleSelectModel getVacationScheduleModel()
    {
        return _vacationScheduleModel;
    }

    public void setVacationScheduleModel(ISingleSelectModel vacationScheduleModel)
    {
        _vacationScheduleModel = vacationScheduleModel;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public VacationSchedule getVacationSchedule()
    {
        return _vacationSchedule;
    }

    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        _vacationSchedule = vacationSchedule;
    }

    public DynamicListDataSource getVacationScheduleDataSource()
    {
        return _vacationScheduleDataSource;
    }

    public void setVacationScheduleDataSource(DynamicListDataSource vacationScheduleDataSource)
    {
        _vacationScheduleDataSource = vacationScheduleDataSource;
    }

    public ISingleSelectModel getAddFormatModel()
    {
        return _addFormatModel;
    }

    public void setAddFormatModel(ISingleSelectModel addFormatModel)
    {
        _addFormatModel = addFormatModel;
    }

    public IdentifiableWrapper getAddFormat()
    {
        return _addFormat;
    }

    public void setAddFormat(IdentifiableWrapper addFormat)
    {
        _addFormat = addFormat;
    }

    public String getSingleAddPage()
    {
        return _singleAddPage;
    }

    public void setSingleAddPage(String singleAddPage)
    {
        _singleAddPage = singleAddPage;
    }

    public String getMultiAddPage()
    {
        return _multiAddPage;
    }

    public void setMultiAddPage(String multiAddPage)
    {
        _multiAddPage = multiAddPage;
    }

    public Map<String, EmployeePostStatus> getPostStatusesMap()
    {
        return _postStatusesMap;
    }

    public void setPostStatusesMap(Map<String, EmployeePostStatus> postStatusesMap)
    {
        _postStatusesMap = postStatusesMap;
    }

    public ISingleSelectModel getHolidayTypesListModel()
    {
        return _holidayTypesListModel;
    }

    public void setHolidayTypesListModel(ISingleSelectModel holidayTypesListModel)
    {
        _holidayTypesListModel = holidayTypesListModel;
    }

    public ISingleSelectModel getPostStatusesModel()
    {
        return _postStatusesModel;
    }

    public void setPostStatusesModel(ISingleSelectModel postStatusesModel)
    {
        _postStatusesModel = postStatusesModel;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public Boolean getNeedMainHoliday()
    {
        return _needMainHoliday;
    }

    public void setNeedMainHoliday(Boolean needMainHoliday)
    {
        _needMainHoliday = needMainHoliday;
    }

    public Boolean getNeedSecondHoliday()
    {
        return _needSecondHoliday;
    }

    public void setNeedSecondHoliday(Boolean needSecondHoliday)
    {
        _needSecondHoliday = needSecondHoliday;
    }
}
