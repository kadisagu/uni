/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.settings.ReasonToDismissReasonAddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author dseleznev
 * Created on: 22.09.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setReason(get(EmployeeOrderReasons.class, model.getEmployeeOrderReasonId()));

        MQBuilder builder = new MQBuilder(EmployeeReasonToDismissReasonRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", EmployeeReasonToDismissReasonRel.L_EMPLOYEE_ORDER_REASON, model.getReason()));
        model.setRelation((EmployeeReasonToDismissReasonRel)builder.uniqueResult(getSession()));

        if(null != model.getRelation()) model.setDismissReason(model.getRelation().getEmployeeDismissReason());

        model.setDismissReasonsListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EmployeeDismissReasons.ENTITY_CLASS, "r");
                builder.add(MQExpression.like("r", EmployeeDismissReasons.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("r", EmployeeDismissReasons.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(EmployeeDismissReasons.class, (Long)primaryKey);
            }
        });
    }

    @Override
    public void update(Model model)
    {
        if(null == model.getDismissReason() && null != model.getRelation()) delete(model.getRelation());
        else if(null != model.getRelation())
        {
            model.getRelation().setEmployeeDismissReason(model.getDismissReason());
            update(model.getRelation());
        }
        else
        {
            EmployeeReasonToDismissReasonRel rel = new EmployeeReasonToDismissReasonRel();
            rel.setEmployeeDismissReason(model.getDismissReason());
            rel.setEmployeeOrderReason(model.getReason());
            save(rel);
        }
    }
}