package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Об установлении доплат в связи с присуждением ученой степени
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScienceDegreePaymentExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract";
    public static final String ENTITY_NAME = "scienceDegreePaymentExtract";
    public static final int VERSION_HASH = 1183847661;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON_ACADEMIC_DEGREE = "personAcademicDegree";
    public static final String L_POST = "post";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String P_SALARY = "salary";
    public static final String P_ADD_AGREEMENT_NUMBER = "addAgreementNumber";
    public static final String P_ADD_AGREEMENT_DATE = "addAgreementDate";
    public static final String P_BEGIN_PAY_DATE = "beginPayDate";
    public static final String P_END_PAY_DATE = "endPayDate";
    public static final String P_FREELANCE = "freelance";
    public static final String L_EMPLOYEE_POST_NEW = "employeePostNew";
    public static final String L_CONTRACT_AGREEMENT_NEW = "contractAgreementNew";
    public static final String L_EMPLOYEE_POST_STATUS_OLD = "employeePostStatusOld";

    private PersonAcademicDegree _personAcademicDegree;     // Присужденная ученая степень
    private PostBoundedWithQGandQL _post;     // Должность, отнесенная к ПКГ и КУ
    private Date _beginDate;     // Дата начала работы в должности
    private EtksLevels _etksLevels;     // Разряд ЕТКС
    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private double _salary;     // Сумма оплаты
    private String _addAgreementNumber;     // Номер доп. соглашения
    private Date _addAgreementDate;     // Дата доп. соглашения
    private Date _beginPayDate;     // Дата начала назначения выплаты
    private Date _endPayDate;     // Дата окончания назначения выплаты
    private boolean _freelance;     // Вне штата
    private EmployeePost _employeePostNew;     // Новый Сотрудник на должности
    private ContractCollateralAgreement _contractAgreementNew;     // Новое доп. соглашение к трудовому договору
    private EmployeePostStatus _employeePostStatusOld;     // Статус на должности до перевода

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Присужденная ученая степень. Свойство не может быть null.
     */
    @NotNull
    public PersonAcademicDegree getPersonAcademicDegree()
    {
        return _personAcademicDegree;
    }

    /**
     * @param personAcademicDegree Присужденная ученая степень. Свойство не может быть null.
     */
    public void setPersonAcademicDegree(PersonAcademicDegree personAcademicDegree)
    {
        dirty(_personAcademicDegree, personAcademicDegree);
        _personAcademicDegree = personAcademicDegree;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ.
     */
    public PostBoundedWithQGandQL getPost()
    {
        return _post;
    }

    /**
     * @param post Должность, отнесенная к ПКГ и КУ.
     */
    public void setPost(PostBoundedWithQGandQL post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Дата начала работы в должности.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала работы в должности.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Разряд ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Разряд ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAddAgreementNumber()
    {
        return _addAgreementNumber;
    }

    /**
     * @param addAgreementNumber Номер доп. соглашения. Свойство не может быть null.
     */
    public void setAddAgreementNumber(String addAgreementNumber)
    {
        dirty(_addAgreementNumber, addAgreementNumber);
        _addAgreementNumber = addAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    public Date getAddAgreementDate()
    {
        return _addAgreementDate;
    }

    /**
     * @param addAgreementDate Дата доп. соглашения. Свойство не может быть null.
     */
    public void setAddAgreementDate(Date addAgreementDate)
    {
        dirty(_addAgreementDate, addAgreementDate);
        _addAgreementDate = addAgreementDate;
    }

    /**
     * @return Дата начала назначения выплаты.
     */
    public Date getBeginPayDate()
    {
        return _beginPayDate;
    }

    /**
     * @param beginPayDate Дата начала назначения выплаты.
     */
    public void setBeginPayDate(Date beginPayDate)
    {
        dirty(_beginPayDate, beginPayDate);
        _beginPayDate = beginPayDate;
    }

    /**
     * @return Дата окончания назначения выплаты.
     */
    public Date getEndPayDate()
    {
        return _endPayDate;
    }

    /**
     * @param endPayDate Дата окончания назначения выплаты.
     */
    public void setEndPayDate(Date endPayDate)
    {
        dirty(_endPayDate, endPayDate);
        _endPayDate = endPayDate;
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreelance()
    {
        return _freelance;
    }

    /**
     * @param freelance Вне штата. Свойство не может быть null.
     */
    public void setFreelance(boolean freelance)
    {
        dirty(_freelance, freelance);
        _freelance = freelance;
    }

    /**
     * @return Новый Сотрудник на должности.
     */
    public EmployeePost getEmployeePostNew()
    {
        return _employeePostNew;
    }

    /**
     * @param employeePostNew Новый Сотрудник на должности.
     */
    public void setEmployeePostNew(EmployeePost employeePostNew)
    {
        dirty(_employeePostNew, employeePostNew);
        _employeePostNew = employeePostNew;
    }

    /**
     * @return Новое доп. соглашение к трудовому договору.
     */
    public ContractCollateralAgreement getContractAgreementNew()
    {
        return _contractAgreementNew;
    }

    /**
     * @param contractAgreementNew Новое доп. соглашение к трудовому договору.
     */
    public void setContractAgreementNew(ContractCollateralAgreement contractAgreementNew)
    {
        dirty(_contractAgreementNew, contractAgreementNew);
        _contractAgreementNew = contractAgreementNew;
    }

    /**
     * @return Статус на должности до перевода.
     */
    public EmployeePostStatus getEmployeePostStatusOld()
    {
        return _employeePostStatusOld;
    }

    /**
     * @param employeePostStatusOld Статус на должности до перевода.
     */
    public void setEmployeePostStatusOld(EmployeePostStatus employeePostStatusOld)
    {
        dirty(_employeePostStatusOld, employeePostStatusOld);
        _employeePostStatusOld = employeePostStatusOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ScienceDegreePaymentExtractGen)
        {
            setPersonAcademicDegree(((ScienceDegreePaymentExtract)another).getPersonAcademicDegree());
            setPost(((ScienceDegreePaymentExtract)another).getPost());
            setBeginDate(((ScienceDegreePaymentExtract)another).getBeginDate());
            setEtksLevels(((ScienceDegreePaymentExtract)another).getEtksLevels());
            setRaisingCoefficient(((ScienceDegreePaymentExtract)another).getRaisingCoefficient());
            setSalary(((ScienceDegreePaymentExtract)another).getSalary());
            setAddAgreementNumber(((ScienceDegreePaymentExtract)another).getAddAgreementNumber());
            setAddAgreementDate(((ScienceDegreePaymentExtract)another).getAddAgreementDate());
            setBeginPayDate(((ScienceDegreePaymentExtract)another).getBeginPayDate());
            setEndPayDate(((ScienceDegreePaymentExtract)another).getEndPayDate());
            setFreelance(((ScienceDegreePaymentExtract)another).isFreelance());
            setEmployeePostNew(((ScienceDegreePaymentExtract)another).getEmployeePostNew());
            setContractAgreementNew(((ScienceDegreePaymentExtract)another).getContractAgreementNew());
            setEmployeePostStatusOld(((ScienceDegreePaymentExtract)another).getEmployeePostStatusOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScienceDegreePaymentExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScienceDegreePaymentExtract.class;
        }

        public T newInstance()
        {
            return (T) new ScienceDegreePaymentExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "personAcademicDegree":
                    return obj.getPersonAcademicDegree();
                case "post":
                    return obj.getPost();
                case "beginDate":
                    return obj.getBeginDate();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "salary":
                    return obj.getSalary();
                case "addAgreementNumber":
                    return obj.getAddAgreementNumber();
                case "addAgreementDate":
                    return obj.getAddAgreementDate();
                case "beginPayDate":
                    return obj.getBeginPayDate();
                case "endPayDate":
                    return obj.getEndPayDate();
                case "freelance":
                    return obj.isFreelance();
                case "employeePostNew":
                    return obj.getEmployeePostNew();
                case "contractAgreementNew":
                    return obj.getContractAgreementNew();
                case "employeePostStatusOld":
                    return obj.getEmployeePostStatusOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "personAcademicDegree":
                    obj.setPersonAcademicDegree((PersonAcademicDegree) value);
                    return;
                case "post":
                    obj.setPost((PostBoundedWithQGandQL) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "addAgreementNumber":
                    obj.setAddAgreementNumber((String) value);
                    return;
                case "addAgreementDate":
                    obj.setAddAgreementDate((Date) value);
                    return;
                case "beginPayDate":
                    obj.setBeginPayDate((Date) value);
                    return;
                case "endPayDate":
                    obj.setEndPayDate((Date) value);
                    return;
                case "freelance":
                    obj.setFreelance((Boolean) value);
                    return;
                case "employeePostNew":
                    obj.setEmployeePostNew((EmployeePost) value);
                    return;
                case "contractAgreementNew":
                    obj.setContractAgreementNew((ContractCollateralAgreement) value);
                    return;
                case "employeePostStatusOld":
                    obj.setEmployeePostStatusOld((EmployeePostStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "personAcademicDegree":
                        return true;
                case "post":
                        return true;
                case "beginDate":
                        return true;
                case "etksLevels":
                        return true;
                case "raisingCoefficient":
                        return true;
                case "salary":
                        return true;
                case "addAgreementNumber":
                        return true;
                case "addAgreementDate":
                        return true;
                case "beginPayDate":
                        return true;
                case "endPayDate":
                        return true;
                case "freelance":
                        return true;
                case "employeePostNew":
                        return true;
                case "contractAgreementNew":
                        return true;
                case "employeePostStatusOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "personAcademicDegree":
                    return true;
                case "post":
                    return true;
                case "beginDate":
                    return true;
                case "etksLevels":
                    return true;
                case "raisingCoefficient":
                    return true;
                case "salary":
                    return true;
                case "addAgreementNumber":
                    return true;
                case "addAgreementDate":
                    return true;
                case "beginPayDate":
                    return true;
                case "endPayDate":
                    return true;
                case "freelance":
                    return true;
                case "employeePostNew":
                    return true;
                case "contractAgreementNew":
                    return true;
                case "employeePostStatusOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "personAcademicDegree":
                    return PersonAcademicDegree.class;
                case "post":
                    return PostBoundedWithQGandQL.class;
                case "beginDate":
                    return Date.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "salary":
                    return Double.class;
                case "addAgreementNumber":
                    return String.class;
                case "addAgreementDate":
                    return Date.class;
                case "beginPayDate":
                    return Date.class;
                case "endPayDate":
                    return Date.class;
                case "freelance":
                    return Boolean.class;
                case "employeePostNew":
                    return EmployeePost.class;
                case "contractAgreementNew":
                    return ContractCollateralAgreement.class;
                case "employeePostStatusOld":
                    return EmployeePostStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScienceDegreePaymentExtract> _dslPath = new Path<ScienceDegreePaymentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScienceDegreePaymentExtract");
    }
            

    /**
     * @return Присужденная ученая степень. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getPersonAcademicDegree()
     */
    public static PersonAcademicDegree.Path<PersonAcademicDegree> personAcademicDegree()
    {
        return _dslPath.personAcademicDegree();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getPost()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Дата начала работы в должности.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getAddAgreementNumber()
     */
    public static PropertyPath<String> addAgreementNumber()
    {
        return _dslPath.addAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getAddAgreementDate()
     */
    public static PropertyPath<Date> addAgreementDate()
    {
        return _dslPath.addAgreementDate();
    }

    /**
     * @return Дата начала назначения выплаты.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getBeginPayDate()
     */
    public static PropertyPath<Date> beginPayDate()
    {
        return _dslPath.beginPayDate();
    }

    /**
     * @return Дата окончания назначения выплаты.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getEndPayDate()
     */
    public static PropertyPath<Date> endPayDate()
    {
        return _dslPath.endPayDate();
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#isFreelance()
     */
    public static PropertyPath<Boolean> freelance()
    {
        return _dslPath.freelance();
    }

    /**
     * @return Новый Сотрудник на должности.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getEmployeePostNew()
     */
    public static EmployeePost.Path<EmployeePost> employeePostNew()
    {
        return _dslPath.employeePostNew();
    }

    /**
     * @return Новое доп. соглашение к трудовому договору.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getContractAgreementNew()
     */
    public static ContractCollateralAgreement.Path<ContractCollateralAgreement> contractAgreementNew()
    {
        return _dslPath.contractAgreementNew();
    }

    /**
     * @return Статус на должности до перевода.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getEmployeePostStatusOld()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusOld()
    {
        return _dslPath.employeePostStatusOld();
    }

    public static class Path<E extends ScienceDegreePaymentExtract> extends ModularEmployeeExtract.Path<E>
    {
        private PersonAcademicDegree.Path<PersonAcademicDegree> _personAcademicDegree;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _post;
        private PropertyPath<Date> _beginDate;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private PropertyPath<Double> _salary;
        private PropertyPath<String> _addAgreementNumber;
        private PropertyPath<Date> _addAgreementDate;
        private PropertyPath<Date> _beginPayDate;
        private PropertyPath<Date> _endPayDate;
        private PropertyPath<Boolean> _freelance;
        private EmployeePost.Path<EmployeePost> _employeePostNew;
        private ContractCollateralAgreement.Path<ContractCollateralAgreement> _contractAgreementNew;
        private EmployeePostStatus.Path<EmployeePostStatus> _employeePostStatusOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Присужденная ученая степень. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getPersonAcademicDegree()
     */
        public PersonAcademicDegree.Path<PersonAcademicDegree> personAcademicDegree()
        {
            if(_personAcademicDegree == null )
                _personAcademicDegree = new PersonAcademicDegree.Path<PersonAcademicDegree>(L_PERSON_ACADEMIC_DEGREE, this);
            return _personAcademicDegree;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getPost()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> post()
        {
            if(_post == null )
                _post = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST, this);
            return _post;
        }

    /**
     * @return Дата начала работы в должности.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(ScienceDegreePaymentExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(ScienceDegreePaymentExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getAddAgreementNumber()
     */
        public PropertyPath<String> addAgreementNumber()
        {
            if(_addAgreementNumber == null )
                _addAgreementNumber = new PropertyPath<String>(ScienceDegreePaymentExtractGen.P_ADD_AGREEMENT_NUMBER, this);
            return _addAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getAddAgreementDate()
     */
        public PropertyPath<Date> addAgreementDate()
        {
            if(_addAgreementDate == null )
                _addAgreementDate = new PropertyPath<Date>(ScienceDegreePaymentExtractGen.P_ADD_AGREEMENT_DATE, this);
            return _addAgreementDate;
        }

    /**
     * @return Дата начала назначения выплаты.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getBeginPayDate()
     */
        public PropertyPath<Date> beginPayDate()
        {
            if(_beginPayDate == null )
                _beginPayDate = new PropertyPath<Date>(ScienceDegreePaymentExtractGen.P_BEGIN_PAY_DATE, this);
            return _beginPayDate;
        }

    /**
     * @return Дата окончания назначения выплаты.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getEndPayDate()
     */
        public PropertyPath<Date> endPayDate()
        {
            if(_endPayDate == null )
                _endPayDate = new PropertyPath<Date>(ScienceDegreePaymentExtractGen.P_END_PAY_DATE, this);
            return _endPayDate;
        }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#isFreelance()
     */
        public PropertyPath<Boolean> freelance()
        {
            if(_freelance == null )
                _freelance = new PropertyPath<Boolean>(ScienceDegreePaymentExtractGen.P_FREELANCE, this);
            return _freelance;
        }

    /**
     * @return Новый Сотрудник на должности.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getEmployeePostNew()
     */
        public EmployeePost.Path<EmployeePost> employeePostNew()
        {
            if(_employeePostNew == null )
                _employeePostNew = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST_NEW, this);
            return _employeePostNew;
        }

    /**
     * @return Новое доп. соглашение к трудовому договору.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getContractAgreementNew()
     */
        public ContractCollateralAgreement.Path<ContractCollateralAgreement> contractAgreementNew()
        {
            if(_contractAgreementNew == null )
                _contractAgreementNew = new ContractCollateralAgreement.Path<ContractCollateralAgreement>(L_CONTRACT_AGREEMENT_NEW, this);
            return _contractAgreementNew;
        }

    /**
     * @return Статус на должности до перевода.
     * @see ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract#getEmployeePostStatusOld()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusOld()
        {
            if(_employeePostStatusOld == null )
                _employeePostStatusOld = new EmployeePostStatus.Path<EmployeePostStatus>(L_EMPLOYEE_POST_STATUS_OLD, this);
            return _employeePostStatusOld;
        }

        public Class getEntityClass()
        {
            return ScienceDegreePaymentExtract.class;
        }

        public String getEntityName()
        {
            return "scienceDegreePaymentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
