/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractAddEdit.ISingleEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public interface ICommonSingleEmployeeExtractAddEditDAO<T extends SingleEmployeeExtract, Model extends CommonSingleEmployeeExtractAddEditModel<T>> extends ISingleEmployeeExtractAddEditDAO<T, Model>
{
    /**
     * Достает соответствующий Причине Текстовый параграф и кладет его в модель.
     * @param model
     */
    public void prepareTextParagraph(Model model);
}