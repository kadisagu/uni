package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.entity.gen.EmployeeTempAddSExtractGen;

/**
 * Выписка из индивидуального приказа по кадровому составу. О приеме сотрудника
 */
public class EmployeeTempAddSExtract extends EmployeeTempAddSExtractGen implements IPostAssignExtract
{
    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }
}