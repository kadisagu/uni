/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5.Pub;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.IModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;

/**
 * @author dseleznev
 * Created on: 03.12.2008
 */
public interface IDAO extends IModularEmployeeExtractPubDAO<EmployeeHolidayExtract, Model>
{
    public void prepareHolidayDataSource(Model model);
}