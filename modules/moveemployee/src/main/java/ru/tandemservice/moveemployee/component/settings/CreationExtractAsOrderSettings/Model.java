/**
 *:$
 */
package ru.tandemservice.moveemployee.component.settings.CreationExtractAsOrderSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * Create by: Shaburov
 * Date: 28.03.11
 */
public class Model
{
    private DynamicListDataSource _dataSource;

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }
}
