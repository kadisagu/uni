/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.settings.EmployeeOrderBasicsCommentSettings;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 21.09.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(EmployeeOrderBasics.ENTITY_CLASS, "o");
        new OrderDescriptionRegistry("o").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void updateCommentable(long basicId)
    {
        EmployeeOrderBasics basic = get(EmployeeOrderBasics.class, basicId);
        if (basic != null)
        {
            boolean isCommentable = !basic.isCommentable();
            basic.setCommentable(isCommentable);

            //если основание не требует примечания, то метки ТД и доп. соглашения необходимо выключить
            if (!isCommentable)
            {
                basic.setLaborContract(false);
                basic.setAgreementLaborContract(false);
            }

            update(basic);
        }
    }

    @Override
    public void updateLaborContract(long id)
    {
        EmployeeOrderBasics basic = get(EmployeeOrderBasics.class, id);

        if (basic != null)
        {
            boolean isContract = !basic.isLaborContract();

            //одновременно метки не могут быть активны
            if (isContract && basic.isAgreementLaborContract())
                basic.setAgreementLaborContract(false);

            if (isContract)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeOrderBasics.class, "b").column("b");
                builder.where(DQLExpressions.ne(DQLExpressions.property(EmployeeOrderBasics.id().fromAlias("b")), id));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeOrderBasics.laborContract().fromAlias("b")), true));

                List<EmployeeOrderBasics> basicList = builder.createStatement(getSession()).list();
                for (EmployeeOrderBasics item : basicList)
                {
                    item.setLaborContract(false);
                    update(item);
                }
            }

            basic.setLaborContract(isContract);

            update(basic);
        }
    }

    @Override
    public void updateAgreementLaborContract(long id)
    {
        EmployeeOrderBasics basic = get(EmployeeOrderBasics.class, id);

        if (basic != null)
        {
            boolean isAgreement = !basic.isAgreementLaborContract();

            //одновременно метки не могут быть активны
            if (isAgreement && basic.isLaborContract())
                basic.setLaborContract(false);

            if (isAgreement)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeOrderBasics.class, "b").column("b");
                builder.where(DQLExpressions.ne(DQLExpressions.property(EmployeeOrderBasics.id().fromAlias("b")), id));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeOrderBasics.agreementLaborContract().fromAlias("b")), true));

                List<EmployeeOrderBasics> basicList = builder.createStatement(getSession()).list();
                for (EmployeeOrderBasics item : basicList)
                {
                    item.setAgreementLaborContract(false);
                    update(item);
                }
            }

            basic.setAgreementLaborContract(isAgreement);

            update(basic);
        }
    }
}