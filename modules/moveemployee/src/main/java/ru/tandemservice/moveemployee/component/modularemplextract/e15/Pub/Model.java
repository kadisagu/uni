/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e15.Pub;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 24.03.2011
 */
public class Model extends ModularEmployeeExtractPubModel<ProlongationAnnualHolidayExtract>
{
    private String _staffRateStr;
    private String _attributesPostDataPage;
    private String _attributesTotalHolidayPage;

    //Getters & Setters

    public String getStaffRateStr()
    {
        return _staffRateStr;
    }

    public void setStaffRateStr(String staffRateStr)
    {
        _staffRateStr = staffRateStr;
    }

    public String getAttributesPostDataPage()
    {
        return _attributesPostDataPage;
    }

    public void setAttributesPostDataPage(String attributesPostDataPage)
    {
        _attributesPostDataPage = attributesPostDataPage;
    }

    public String getAttributesTotalHolidayPage()
    {
        return _attributesTotalHolidayPage;
    }

    public void setAttributesTotalHolidayPage(String attributesTotalHolidayPage)
    {
        _attributesTotalHolidayPage = attributesTotalHolidayPage;
    }
}