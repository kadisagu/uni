/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e8.ParagraphAddEdit;

import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<PayForHolidayDayProvideHoldayEmplListExtract, Model>
{
    public void prepareEmployeeDataSource(Model model);
}
