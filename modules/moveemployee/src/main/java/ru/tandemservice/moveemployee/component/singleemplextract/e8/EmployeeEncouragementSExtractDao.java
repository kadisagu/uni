/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e8;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.CreateEncouragementInExtractRelation;
import ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import ru.tandemservice.uniemp.entity.employee.EmployeeEncouragement;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 30.09.2011
 */
public class EmployeeEncouragementSExtractDao extends UniBaseDao implements IExtractComponentDao<EmployeeEncouragementSExtract>
{
    @Override
    public void doCommit(EmployeeEncouragementSExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        for (EncouragementType encouragementType : MoveEmployeeDaoFacade.getMoveEmployeeDao().getEncouragementTypeFromExtractList(extract))
        {
            EmployeeEncouragement encouragement = new EmployeeEncouragement();
            encouragement.setAssignDate(extract.getCreateDate());
            encouragement.setDocument("Приказ «" + extract.getType().getTitle() + "»");
            encouragement.setDocumentNumber(extract.getParagraph().getOrder().getNumber());
            encouragement.setEmployee(extract.getEntity().getEmployee());
            encouragement.setTitle(encouragementType.getTitle());
            encouragement.setType(encouragementType);

            save(encouragement);

            CreateEncouragementInExtractRelation relation = new CreateEncouragementInExtractRelation();
            relation.setEmployeeEncouragementSExtract(extract);
            relation.setEmployeeEncouragement(encouragement);

            save(relation);
        }
    }

    @Override
    public void doRollback(EmployeeEncouragementSExtract extract, Map parameters)
    {
        MQBuilder builder = new MQBuilder(CreateEncouragementInExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", CreateEncouragementInExtractRelation.L_EMPLOYEE_ENCOURAGEMENT_S_EXTRACT, extract));

        for (CreateEncouragementInExtractRelation relation : builder.<CreateEncouragementInExtractRelation>getResultList(getSession()))
        {
            EmployeeEncouragement employeeEncouragement = relation.getEmployeeEncouragement();
            delete(relation);
            delete(employeeEncouragement);
        }
    }
}