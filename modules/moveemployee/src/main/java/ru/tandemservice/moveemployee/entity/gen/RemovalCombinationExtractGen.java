package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.RemovalCombinationExtract;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Снятие доплат за работу по совмещению (за увеличение объема работ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RemovalCombinationExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.RemovalCombinationExtract";
    public static final String ENTITY_NAME = "removalCombinationExtract";
    public static final int VERSION_HASH = -1592393994;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMBINATION_POST = "combinationPost";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String L_CHANGED_STAFF_LIST = "changedStaffList";
    public static final String P_CHANGE_STAFF_LIST = "changeStaffList";
    public static final String P_CHANGE_END_DATE = "changeEndDate";
    public static final String P_END_DATE_OLD = "endDateOld";

    private CombinationPost _combinationPost;     // Совмещение/ увеличение объема работ в должности
    private Date _removalDate;     // Дата снятия доплаты
    private StaffList _changedStaffList;     // ШР активное на момент проведения приказа и в котором освобождаем ставки
    private Boolean _changeStaffList;     // Освобождаем ли ставку в ШР у выбранной должности
    private Boolean _changeEndDate;     // Меняем ли дату окончания у выбранной должности
    private Date _endDateOld;     // Дата окончания до проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Совмещение/ увеличение объема работ в должности. Свойство не может быть null.
     */
    @NotNull
    public CombinationPost getCombinationPost()
    {
        return _combinationPost;
    }

    /**
     * @param combinationPost Совмещение/ увеличение объема работ в должности. Свойство не может быть null.
     */
    public void setCombinationPost(CombinationPost combinationPost)
    {
        dirty(_combinationPost, combinationPost);
        _combinationPost = combinationPost;
    }

    /**
     * @return Дата снятия доплаты.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата снятия доплаты.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * @return ШР активное на момент проведения приказа и в котором освобождаем ставки.
     */
    public StaffList getChangedStaffList()
    {
        return _changedStaffList;
    }

    /**
     * @param changedStaffList ШР активное на момент проведения приказа и в котором освобождаем ставки.
     */
    public void setChangedStaffList(StaffList changedStaffList)
    {
        dirty(_changedStaffList, changedStaffList);
        _changedStaffList = changedStaffList;
    }

    /**
     * @return Освобождаем ли ставку в ШР у выбранной должности.
     */
    public Boolean getChangeStaffList()
    {
        return _changeStaffList;
    }

    /**
     * @param changeStaffList Освобождаем ли ставку в ШР у выбранной должности.
     */
    public void setChangeStaffList(Boolean changeStaffList)
    {
        dirty(_changeStaffList, changeStaffList);
        _changeStaffList = changeStaffList;
    }

    /**
     * @return Меняем ли дату окончания у выбранной должности.
     */
    public Boolean getChangeEndDate()
    {
        return _changeEndDate;
    }

    /**
     * @param changeEndDate Меняем ли дату окончания у выбранной должности.
     */
    public void setChangeEndDate(Boolean changeEndDate)
    {
        dirty(_changeEndDate, changeEndDate);
        _changeEndDate = changeEndDate;
    }

    /**
     * @return Дата окончания до проведения приказа.
     */
    public Date getEndDateOld()
    {
        return _endDateOld;
    }

    /**
     * @param endDateOld Дата окончания до проведения приказа.
     */
    public void setEndDateOld(Date endDateOld)
    {
        dirty(_endDateOld, endDateOld);
        _endDateOld = endDateOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RemovalCombinationExtractGen)
        {
            setCombinationPost(((RemovalCombinationExtract)another).getCombinationPost());
            setRemovalDate(((RemovalCombinationExtract)another).getRemovalDate());
            setChangedStaffList(((RemovalCombinationExtract)another).getChangedStaffList());
            setChangeStaffList(((RemovalCombinationExtract)another).getChangeStaffList());
            setChangeEndDate(((RemovalCombinationExtract)another).getChangeEndDate());
            setEndDateOld(((RemovalCombinationExtract)another).getEndDateOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RemovalCombinationExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RemovalCombinationExtract.class;
        }

        public T newInstance()
        {
            return (T) new RemovalCombinationExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPost":
                    return obj.getCombinationPost();
                case "removalDate":
                    return obj.getRemovalDate();
                case "changedStaffList":
                    return obj.getChangedStaffList();
                case "changeStaffList":
                    return obj.getChangeStaffList();
                case "changeEndDate":
                    return obj.getChangeEndDate();
                case "endDateOld":
                    return obj.getEndDateOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "combinationPost":
                    obj.setCombinationPost((CombinationPost) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "changedStaffList":
                    obj.setChangedStaffList((StaffList) value);
                    return;
                case "changeStaffList":
                    obj.setChangeStaffList((Boolean) value);
                    return;
                case "changeEndDate":
                    obj.setChangeEndDate((Boolean) value);
                    return;
                case "endDateOld":
                    obj.setEndDateOld((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPost":
                        return true;
                case "removalDate":
                        return true;
                case "changedStaffList":
                        return true;
                case "changeStaffList":
                        return true;
                case "changeEndDate":
                        return true;
                case "endDateOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPost":
                    return true;
                case "removalDate":
                    return true;
                case "changedStaffList":
                    return true;
                case "changeStaffList":
                    return true;
                case "changeEndDate":
                    return true;
                case "endDateOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPost":
                    return CombinationPost.class;
                case "removalDate":
                    return Date.class;
                case "changedStaffList":
                    return StaffList.class;
                case "changeStaffList":
                    return Boolean.class;
                case "changeEndDate":
                    return Boolean.class;
                case "endDateOld":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RemovalCombinationExtract> _dslPath = new Path<RemovalCombinationExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RemovalCombinationExtract");
    }
            

    /**
     * @return Совмещение/ увеличение объема работ в должности. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getCombinationPost()
     */
    public static CombinationPost.Path<CombinationPost> combinationPost()
    {
        return _dslPath.combinationPost();
    }

    /**
     * @return Дата снятия доплаты.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * @return ШР активное на момент проведения приказа и в котором освобождаем ставки.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getChangedStaffList()
     */
    public static StaffList.Path<StaffList> changedStaffList()
    {
        return _dslPath.changedStaffList();
    }

    /**
     * @return Освобождаем ли ставку в ШР у выбранной должности.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getChangeStaffList()
     */
    public static PropertyPath<Boolean> changeStaffList()
    {
        return _dslPath.changeStaffList();
    }

    /**
     * @return Меняем ли дату окончания у выбранной должности.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getChangeEndDate()
     */
    public static PropertyPath<Boolean> changeEndDate()
    {
        return _dslPath.changeEndDate();
    }

    /**
     * @return Дата окончания до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getEndDateOld()
     */
    public static PropertyPath<Date> endDateOld()
    {
        return _dslPath.endDateOld();
    }

    public static class Path<E extends RemovalCombinationExtract> extends ModularEmployeeExtract.Path<E>
    {
        private CombinationPost.Path<CombinationPost> _combinationPost;
        private PropertyPath<Date> _removalDate;
        private StaffList.Path<StaffList> _changedStaffList;
        private PropertyPath<Boolean> _changeStaffList;
        private PropertyPath<Boolean> _changeEndDate;
        private PropertyPath<Date> _endDateOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Совмещение/ увеличение объема работ в должности. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getCombinationPost()
     */
        public CombinationPost.Path<CombinationPost> combinationPost()
        {
            if(_combinationPost == null )
                _combinationPost = new CombinationPost.Path<CombinationPost>(L_COMBINATION_POST, this);
            return _combinationPost;
        }

    /**
     * @return Дата снятия доплаты.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(RemovalCombinationExtractGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * @return ШР активное на момент проведения приказа и в котором освобождаем ставки.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getChangedStaffList()
     */
        public StaffList.Path<StaffList> changedStaffList()
        {
            if(_changedStaffList == null )
                _changedStaffList = new StaffList.Path<StaffList>(L_CHANGED_STAFF_LIST, this);
            return _changedStaffList;
        }

    /**
     * @return Освобождаем ли ставку в ШР у выбранной должности.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getChangeStaffList()
     */
        public PropertyPath<Boolean> changeStaffList()
        {
            if(_changeStaffList == null )
                _changeStaffList = new PropertyPath<Boolean>(RemovalCombinationExtractGen.P_CHANGE_STAFF_LIST, this);
            return _changeStaffList;
        }

    /**
     * @return Меняем ли дату окончания у выбранной должности.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getChangeEndDate()
     */
        public PropertyPath<Boolean> changeEndDate()
        {
            if(_changeEndDate == null )
                _changeEndDate = new PropertyPath<Boolean>(RemovalCombinationExtractGen.P_CHANGE_END_DATE, this);
            return _changeEndDate;
        }

    /**
     * @return Дата окончания до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.RemovalCombinationExtract#getEndDateOld()
     */
        public PropertyPath<Date> endDateOld()
        {
            if(_endDateOld == null )
                _endDateOld = new PropertyPath<Date>(RemovalCombinationExtractGen.P_END_DATE_OLD, this);
            return _endDateOld;
        }

        public Class getEntityClass()
        {
            return RemovalCombinationExtract.class;
        }

        public String getEntityName()
        {
            return "removalCombinationExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
