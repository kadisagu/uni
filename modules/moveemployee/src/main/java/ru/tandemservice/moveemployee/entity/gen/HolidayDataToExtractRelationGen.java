package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.gen.IAbstractDocumentGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь данных из удаляемых отпусков в выписке «О переносе ежегодного отпуска»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class HolidayDataToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation";
    public static final String ENTITY_NAME = "holidayDataToExtractRelation";
    public static final int VERSION_HASH = -865213264;
    private static IEntityMeta ENTITY_META;

    public static final String L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT = "transferAnnualHolidayExtract";
    public static final String P_HOLIDAY_START = "holidayStart";
    public static final String P_HOLIDAY_FINISH = "holidayFinish";
    public static final String P_HOLIDAY_DURATION = "holidayDuration";
    public static final String P_HOLIDAY_TITLE = "holidayTitle";
    public static final String P_HOLIDAY_BEGIN = "holidayBegin";
    public static final String P_HOLIDAY_END = "holidayEnd";
    public static final String L_HOLIDAY_TYPE = "holidayType";
    public static final String L_HOLIDAY_EXTRACT = "holidayExtract";
    public static final String P_HOLIDAY_EXTRACT_NUMBER = "holidayExtractNumber";
    public static final String P_HOLIDAY_EXTRACT_DATE = "holidayExtractDate";

    private TransferAnnualHolidayExtract _transferAnnualHolidayExtract;     // Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска
    private Date _holidayStart;     // Дата начала отпуска
    private Date _holidayFinish;     // Дата окончания отпуска
    private Integer _holidayDuration;     // Длительность отпуска
    private String _holidayTitle;     // Название отпуска
    private Date _holidayBegin;     // Дата начала периода отпуска
    private Date _holidayEnd;     // Дата окончания периода отпуска
    private HolidayType _holidayType;     // Вид отпуска
    private IAbstractDocument _holidayExtract;     // Выписка приказа об отпуске
    private String _holidayExtractNumber;     // Номер приказа об отпуске
    private Date _holidayExtractDate;     // Дата приказа об отпуске

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     */
    @NotNull
    public TransferAnnualHolidayExtract getTransferAnnualHolidayExtract()
    {
        return _transferAnnualHolidayExtract;
    }

    /**
     * @param transferAnnualHolidayExtract Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     */
    public void setTransferAnnualHolidayExtract(TransferAnnualHolidayExtract transferAnnualHolidayExtract)
    {
        dirty(_transferAnnualHolidayExtract, transferAnnualHolidayExtract);
        _transferAnnualHolidayExtract = transferAnnualHolidayExtract;
    }

    /**
     * @return Дата начала отпуска.
     */
    public Date getHolidayStart()
    {
        return _holidayStart;
    }

    /**
     * @param holidayStart Дата начала отпуска.
     */
    public void setHolidayStart(Date holidayStart)
    {
        dirty(_holidayStart, holidayStart);
        _holidayStart = holidayStart;
    }

    /**
     * @return Дата окончания отпуска.
     */
    public Date getHolidayFinish()
    {
        return _holidayFinish;
    }

    /**
     * @param holidayFinish Дата окончания отпуска.
     */
    public void setHolidayFinish(Date holidayFinish)
    {
        dirty(_holidayFinish, holidayFinish);
        _holidayFinish = holidayFinish;
    }

    /**
     * @return Длительность отпуска.
     */
    public Integer getHolidayDuration()
    {
        return _holidayDuration;
    }

    /**
     * @param holidayDuration Длительность отпуска.
     */
    public void setHolidayDuration(Integer holidayDuration)
    {
        dirty(_holidayDuration, holidayDuration);
        _holidayDuration = holidayDuration;
    }

    /**
     * @return Название отпуска.
     */
    @Length(max=255)
    public String getHolidayTitle()
    {
        return _holidayTitle;
    }

    /**
     * @param holidayTitle Название отпуска.
     */
    public void setHolidayTitle(String holidayTitle)
    {
        dirty(_holidayTitle, holidayTitle);
        _holidayTitle = holidayTitle;
    }

    /**
     * @return Дата начала периода отпуска.
     */
    public Date getHolidayBegin()
    {
        return _holidayBegin;
    }

    /**
     * @param holidayBegin Дата начала периода отпуска.
     */
    public void setHolidayBegin(Date holidayBegin)
    {
        dirty(_holidayBegin, holidayBegin);
        _holidayBegin = holidayBegin;
    }

    /**
     * @return Дата окончания периода отпуска.
     */
    public Date getHolidayEnd()
    {
        return _holidayEnd;
    }

    /**
     * @param holidayEnd Дата окончания периода отпуска.
     */
    public void setHolidayEnd(Date holidayEnd)
    {
        dirty(_holidayEnd, holidayEnd);
        _holidayEnd = holidayEnd;
    }

    /**
     * @return Вид отпуска.
     */
    public HolidayType getHolidayType()
    {
        return _holidayType;
    }

    /**
     * @param holidayType Вид отпуска.
     */
    public void setHolidayType(HolidayType holidayType)
    {
        dirty(_holidayType, holidayType);
        _holidayType = holidayType;
    }

    /**
     * @return Выписка приказа об отпуске.
     */
    public IAbstractDocument getHolidayExtract()
    {
        return _holidayExtract;
    }

    /**
     * @param holidayExtract Выписка приказа об отпуске.
     */
    public void setHolidayExtract(IAbstractDocument holidayExtract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && holidayExtract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractDocument.class);
            IEntityMeta actual =  holidayExtract instanceof IEntity ? EntityRuntime.getMeta((IEntity) holidayExtract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_holidayExtract, holidayExtract);
        _holidayExtract = holidayExtract;
    }

    /**
     * @return Номер приказа об отпуске.
     */
    @Length(max=255)
    public String getHolidayExtractNumber()
    {
        return _holidayExtractNumber;
    }

    /**
     * @param holidayExtractNumber Номер приказа об отпуске.
     */
    public void setHolidayExtractNumber(String holidayExtractNumber)
    {
        dirty(_holidayExtractNumber, holidayExtractNumber);
        _holidayExtractNumber = holidayExtractNumber;
    }

    /**
     * @return Дата приказа об отпуске.
     */
    public Date getHolidayExtractDate()
    {
        return _holidayExtractDate;
    }

    /**
     * @param holidayExtractDate Дата приказа об отпуске.
     */
    public void setHolidayExtractDate(Date holidayExtractDate)
    {
        dirty(_holidayExtractDate, holidayExtractDate);
        _holidayExtractDate = holidayExtractDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof HolidayDataToExtractRelationGen)
        {
            setTransferAnnualHolidayExtract(((HolidayDataToExtractRelation)another).getTransferAnnualHolidayExtract());
            setHolidayStart(((HolidayDataToExtractRelation)another).getHolidayStart());
            setHolidayFinish(((HolidayDataToExtractRelation)another).getHolidayFinish());
            setHolidayDuration(((HolidayDataToExtractRelation)another).getHolidayDuration());
            setHolidayTitle(((HolidayDataToExtractRelation)another).getHolidayTitle());
            setHolidayBegin(((HolidayDataToExtractRelation)another).getHolidayBegin());
            setHolidayEnd(((HolidayDataToExtractRelation)another).getHolidayEnd());
            setHolidayType(((HolidayDataToExtractRelation)another).getHolidayType());
            setHolidayExtract(((HolidayDataToExtractRelation)another).getHolidayExtract());
            setHolidayExtractNumber(((HolidayDataToExtractRelation)another).getHolidayExtractNumber());
            setHolidayExtractDate(((HolidayDataToExtractRelation)another).getHolidayExtractDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends HolidayDataToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) HolidayDataToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new HolidayDataToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "transferAnnualHolidayExtract":
                    return obj.getTransferAnnualHolidayExtract();
                case "holidayStart":
                    return obj.getHolidayStart();
                case "holidayFinish":
                    return obj.getHolidayFinish();
                case "holidayDuration":
                    return obj.getHolidayDuration();
                case "holidayTitle":
                    return obj.getHolidayTitle();
                case "holidayBegin":
                    return obj.getHolidayBegin();
                case "holidayEnd":
                    return obj.getHolidayEnd();
                case "holidayType":
                    return obj.getHolidayType();
                case "holidayExtract":
                    return obj.getHolidayExtract();
                case "holidayExtractNumber":
                    return obj.getHolidayExtractNumber();
                case "holidayExtractDate":
                    return obj.getHolidayExtractDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "transferAnnualHolidayExtract":
                    obj.setTransferAnnualHolidayExtract((TransferAnnualHolidayExtract) value);
                    return;
                case "holidayStart":
                    obj.setHolidayStart((Date) value);
                    return;
                case "holidayFinish":
                    obj.setHolidayFinish((Date) value);
                    return;
                case "holidayDuration":
                    obj.setHolidayDuration((Integer) value);
                    return;
                case "holidayTitle":
                    obj.setHolidayTitle((String) value);
                    return;
                case "holidayBegin":
                    obj.setHolidayBegin((Date) value);
                    return;
                case "holidayEnd":
                    obj.setHolidayEnd((Date) value);
                    return;
                case "holidayType":
                    obj.setHolidayType((HolidayType) value);
                    return;
                case "holidayExtract":
                    obj.setHolidayExtract((IAbstractDocument) value);
                    return;
                case "holidayExtractNumber":
                    obj.setHolidayExtractNumber((String) value);
                    return;
                case "holidayExtractDate":
                    obj.setHolidayExtractDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "transferAnnualHolidayExtract":
                        return true;
                case "holidayStart":
                        return true;
                case "holidayFinish":
                        return true;
                case "holidayDuration":
                        return true;
                case "holidayTitle":
                        return true;
                case "holidayBegin":
                        return true;
                case "holidayEnd":
                        return true;
                case "holidayType":
                        return true;
                case "holidayExtract":
                        return true;
                case "holidayExtractNumber":
                        return true;
                case "holidayExtractDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "transferAnnualHolidayExtract":
                    return true;
                case "holidayStart":
                    return true;
                case "holidayFinish":
                    return true;
                case "holidayDuration":
                    return true;
                case "holidayTitle":
                    return true;
                case "holidayBegin":
                    return true;
                case "holidayEnd":
                    return true;
                case "holidayType":
                    return true;
                case "holidayExtract":
                    return true;
                case "holidayExtractNumber":
                    return true;
                case "holidayExtractDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "transferAnnualHolidayExtract":
                    return TransferAnnualHolidayExtract.class;
                case "holidayStart":
                    return Date.class;
                case "holidayFinish":
                    return Date.class;
                case "holidayDuration":
                    return Integer.class;
                case "holidayTitle":
                    return String.class;
                case "holidayBegin":
                    return Date.class;
                case "holidayEnd":
                    return Date.class;
                case "holidayType":
                    return HolidayType.class;
                case "holidayExtract":
                    return IAbstractDocument.class;
                case "holidayExtractNumber":
                    return String.class;
                case "holidayExtractDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<HolidayDataToExtractRelation> _dslPath = new Path<HolidayDataToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "HolidayDataToExtractRelation");
    }
            

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getTransferAnnualHolidayExtract()
     */
    public static TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> transferAnnualHolidayExtract()
    {
        return _dslPath.transferAnnualHolidayExtract();
    }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayStart()
     */
    public static PropertyPath<Date> holidayStart()
    {
        return _dslPath.holidayStart();
    }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayFinish()
     */
    public static PropertyPath<Date> holidayFinish()
    {
        return _dslPath.holidayFinish();
    }

    /**
     * @return Длительность отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayDuration()
     */
    public static PropertyPath<Integer> holidayDuration()
    {
        return _dslPath.holidayDuration();
    }

    /**
     * @return Название отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayTitle()
     */
    public static PropertyPath<String> holidayTitle()
    {
        return _dslPath.holidayTitle();
    }

    /**
     * @return Дата начала периода отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayBegin()
     */
    public static PropertyPath<Date> holidayBegin()
    {
        return _dslPath.holidayBegin();
    }

    /**
     * @return Дата окончания периода отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayEnd()
     */
    public static PropertyPath<Date> holidayEnd()
    {
        return _dslPath.holidayEnd();
    }

    /**
     * @return Вид отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayType()
     */
    public static HolidayType.Path<HolidayType> holidayType()
    {
        return _dslPath.holidayType();
    }

    /**
     * @return Выписка приказа об отпуске.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayExtract()
     */
    public static IAbstractDocumentGen.Path<IAbstractDocument> holidayExtract()
    {
        return _dslPath.holidayExtract();
    }

    /**
     * @return Номер приказа об отпуске.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayExtractNumber()
     */
    public static PropertyPath<String> holidayExtractNumber()
    {
        return _dslPath.holidayExtractNumber();
    }

    /**
     * @return Дата приказа об отпуске.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayExtractDate()
     */
    public static PropertyPath<Date> holidayExtractDate()
    {
        return _dslPath.holidayExtractDate();
    }

    public static class Path<E extends HolidayDataToExtractRelation> extends EntityPath<E>
    {
        private TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> _transferAnnualHolidayExtract;
        private PropertyPath<Date> _holidayStart;
        private PropertyPath<Date> _holidayFinish;
        private PropertyPath<Integer> _holidayDuration;
        private PropertyPath<String> _holidayTitle;
        private PropertyPath<Date> _holidayBegin;
        private PropertyPath<Date> _holidayEnd;
        private HolidayType.Path<HolidayType> _holidayType;
        private IAbstractDocumentGen.Path<IAbstractDocument> _holidayExtract;
        private PropertyPath<String> _holidayExtractNumber;
        private PropertyPath<Date> _holidayExtractDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getTransferAnnualHolidayExtract()
     */
        public TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> transferAnnualHolidayExtract()
        {
            if(_transferAnnualHolidayExtract == null )
                _transferAnnualHolidayExtract = new TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract>(L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, this);
            return _transferAnnualHolidayExtract;
        }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayStart()
     */
        public PropertyPath<Date> holidayStart()
        {
            if(_holidayStart == null )
                _holidayStart = new PropertyPath<Date>(HolidayDataToExtractRelationGen.P_HOLIDAY_START, this);
            return _holidayStart;
        }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayFinish()
     */
        public PropertyPath<Date> holidayFinish()
        {
            if(_holidayFinish == null )
                _holidayFinish = new PropertyPath<Date>(HolidayDataToExtractRelationGen.P_HOLIDAY_FINISH, this);
            return _holidayFinish;
        }

    /**
     * @return Длительность отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayDuration()
     */
        public PropertyPath<Integer> holidayDuration()
        {
            if(_holidayDuration == null )
                _holidayDuration = new PropertyPath<Integer>(HolidayDataToExtractRelationGen.P_HOLIDAY_DURATION, this);
            return _holidayDuration;
        }

    /**
     * @return Название отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayTitle()
     */
        public PropertyPath<String> holidayTitle()
        {
            if(_holidayTitle == null )
                _holidayTitle = new PropertyPath<String>(HolidayDataToExtractRelationGen.P_HOLIDAY_TITLE, this);
            return _holidayTitle;
        }

    /**
     * @return Дата начала периода отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayBegin()
     */
        public PropertyPath<Date> holidayBegin()
        {
            if(_holidayBegin == null )
                _holidayBegin = new PropertyPath<Date>(HolidayDataToExtractRelationGen.P_HOLIDAY_BEGIN, this);
            return _holidayBegin;
        }

    /**
     * @return Дата окончания периода отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayEnd()
     */
        public PropertyPath<Date> holidayEnd()
        {
            if(_holidayEnd == null )
                _holidayEnd = new PropertyPath<Date>(HolidayDataToExtractRelationGen.P_HOLIDAY_END, this);
            return _holidayEnd;
        }

    /**
     * @return Вид отпуска.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayType()
     */
        public HolidayType.Path<HolidayType> holidayType()
        {
            if(_holidayType == null )
                _holidayType = new HolidayType.Path<HolidayType>(L_HOLIDAY_TYPE, this);
            return _holidayType;
        }

    /**
     * @return Выписка приказа об отпуске.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayExtract()
     */
        public IAbstractDocumentGen.Path<IAbstractDocument> holidayExtract()
        {
            if(_holidayExtract == null )
                _holidayExtract = new IAbstractDocumentGen.Path<IAbstractDocument>(L_HOLIDAY_EXTRACT, this);
            return _holidayExtract;
        }

    /**
     * @return Номер приказа об отпуске.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayExtractNumber()
     */
        public PropertyPath<String> holidayExtractNumber()
        {
            if(_holidayExtractNumber == null )
                _holidayExtractNumber = new PropertyPath<String>(HolidayDataToExtractRelationGen.P_HOLIDAY_EXTRACT_NUMBER, this);
            return _holidayExtractNumber;
        }

    /**
     * @return Дата приказа об отпуске.
     * @see ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation#getHolidayExtractDate()
     */
        public PropertyPath<Date> holidayExtractDate()
        {
            if(_holidayExtractDate == null )
                _holidayExtractDate = new PropertyPath<Date>(HolidayDataToExtractRelationGen.P_HOLIDAY_EXTRACT_DATE, this);
            return _holidayExtractDate;
        }

        public Class getEntityClass()
        {
            return HolidayDataToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "holidayDataToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
