/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e7.AddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPaymentAddModel;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 08.12.2010
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<EmployeeAddPaymentsSExtract> implements IPaymentAddModel<EmployeeAddPaymentsSExtract>
{
    private final String _paymentsPage = Model.class.getPackage().getName() + ".PaymentBlock";

    private ISelectModel _raisingCoefficientListModel;

    private ISelectModel _paymentListModel;
    private List<FinancingSource> _financingSourcesList;
    private ISelectModel _financingSourceItemsListModel;
    private List<LabourContractType> _labourContractTypesList;

    private EmployeeBonus _newEmployeeBonus;

    private DynamicListDataSource<EmployeeBonus> _bonusesDataSource;
    private List<EmployeeBonus> _bonusesList = new ArrayList<>();
    private List<EmployeeBonus> _bonusesToDel = new ArrayList<>();

    private List<Payment> _alreadyAddedPayments;

    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;

    //payments fields
    private DynamicListDataSource<EmployeeBonus> _paymentDataSource;
    private ISingleSelectModel _paymentModel;
    private ISingleSelectModel _finSrcPaymentModel;
    private ISingleSelectModel _finSrcItemPaymentModel;
    private List<EmployeeBonus> _paymentList = new ArrayList<>();
    private boolean _addStaffListPaymentsButtonVisible = false;

    //Calculate

    public String getAssignDateId()
    {
        return "assignDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getBeginDateId()
    {
        return "beginDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getEndDateId()
    {
        return "endDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getPaymentId()
    {
        return "payment_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getAmountId()
    {
        return "amount_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcId()
    {
        return "finSrc_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItemId()
    {
        return "finSrcItem_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }
    //end payments fields

    //Getters & Setters

    public String getPaymentsPage()
    {
        return _paymentsPage;
    }

    public DynamicListDataSource<EmployeeBonus> getPaymentDataSource()
    {
        return _paymentDataSource;
    }

    public void setPaymentDataSource(DynamicListDataSource<EmployeeBonus> paymentDataSource)
    {
        _paymentDataSource = paymentDataSource;
    }

    public ISingleSelectModel getPaymentModel()
    {
        return _paymentModel;
    }

    public void setPaymentModel(ISingleSelectModel paymentModel)
    {
        _paymentModel = paymentModel;
    }

    public ISingleSelectModel getFinSrcPaymentModel()
    {
        return _finSrcPaymentModel;
    }

    public void setFinSrcPaymentModel(ISingleSelectModel finSrcPaymentModel)
    {
        _finSrcPaymentModel = finSrcPaymentModel;
    }

    public ISingleSelectModel getFinSrcItemPaymentModel()
    {
        return _finSrcItemPaymentModel;
    }

    public void setFinSrcItemPaymentModel(ISingleSelectModel finSrcItemPaymentModel)
    {
        _finSrcItemPaymentModel = finSrcItemPaymentModel;
    }

    public List<EmployeeBonus> getPaymentList()
    {
        return _paymentList;
    }

    public void setPaymentList(List<EmployeeBonus> paymentList)
    {
        _paymentList = paymentList;
    }

    public boolean isAddStaffListPaymentsButtonVisible()
    {
        return _addStaffListPaymentsButtonVisible;
    }

    public void setAddStaffListPaymentsButtonVisible(boolean addStaffListPaymentsButtonVisible)
    {
        _addStaffListPaymentsButtonVisible = addStaffListPaymentsButtonVisible;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    @Override
    public boolean isFinancingOrgUnitVisible()
    {
        return CommonEmployeeExtractUtil.isFinancingOrgUnitVisible(this);
    }

    @Override
    public ISelectModel getFinancingSourceItemsListModel()
    {
        return _financingSourceItemsListModel;
    }

    @Override
    public void setFinancingSourceItemsListModel(ISelectModel financingSourceItemsListModel)
    {
        this._financingSourceItemsListModel = financingSourceItemsListModel;
    }

    public ISelectModel getRaisingCoefficientListModel()
    {
        return _raisingCoefficientListModel;
    }

    public void setRaisingCoefficientListModel(ISelectModel raisingCoefficientListModel)
    {
        this._raisingCoefficientListModel = raisingCoefficientListModel;
    }

    @Override
    public ISelectModel getPaymentListModel()
    {
        return _paymentListModel;
    }

    @Override
    public void setPaymentListModel(ISelectModel paymentListModel)
    {
        this._paymentListModel = paymentListModel;
    }

    @Override
    public List<FinancingSource> getFinancingSourcesList()
    {
        return _financingSourcesList;
    }

    @Override
    public void setFinancingSourcesList(List<FinancingSource> financingSourcesList)
    {
        this._financingSourcesList = financingSourcesList;
    }

    public List<LabourContractType> getLabourContractTypesList()
    {
        return _labourContractTypesList;
    }

    public void setLabourContractTypesList(List<LabourContractType> labourContractTypesList)
    {
        this._labourContractTypesList = labourContractTypesList;
    }

    @Override
    public EmployeeBonus getNewEmployeeBonus()
    {
        return _newEmployeeBonus;
    }

    @Override
    public void setNewEmployeeBonus(EmployeeBonus newEmployeeBonus)
    {
        this._newEmployeeBonus = newEmployeeBonus;
    }

    @Override
    public DynamicListDataSource<EmployeeBonus> getBonusesDataSource()
    {
        return _bonusesDataSource;
    }

    @Override
    public void setBonusesDataSource(DynamicListDataSource<EmployeeBonus> bonusesDataSource)
    {
        this._bonusesDataSource = bonusesDataSource;
    }

    @Override
    public List<EmployeeBonus> getBonusesList()
    {
        return _bonusesList;
    }

    @Override
    public void setBonusesList(List<EmployeeBonus> bonusesList)
    {
        this._bonusesList = bonusesList;
    }

    @Override
    public List<EmployeeBonus> getBonusesToDel()
    {
        return _bonusesToDel;
    }

    @Override
    public void setBonusesToDel(List<EmployeeBonus> bonusesToDel)
    {
        this._bonusesToDel = bonusesToDel;
    }

    @Override
    public List<Payment> getAlreadyAddedPayments()
    {
        return _alreadyAddedPayments;
    }

    @Override
    public void setAlreadyAddedPayments(List<Payment> alreadyAddedPayments)
    {
        this._alreadyAddedPayments = alreadyAddedPayments;
    }
}