/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.menuempl.EmployeeOrdersJournal;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;


/**
 * @author alikhanov
 * @since 16.08.2010
 */
public class Model
{

    private ISelectModel _orderCategoryModel;
    private ISelectModel _orderTypeModel;
    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;

    // Getters & Setters

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getOrderCategoryModel()
    {
        return _orderCategoryModel;
    }

    public void setOrderCategoryModel(ISelectModel orderCategoryModel)
    {
        this._orderCategoryModel = orderCategoryModel;
    }

    public ISelectModel getOrderTypeModel()
    {
        return _orderTypeModel;
    }

    public void setOrderTypeModel(ISelectModel orderTypeModel)
    {
        this._orderTypeModel = orderTypeModel;
    }
}
