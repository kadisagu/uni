/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e5.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class Model extends AbstractListParagraphAddEditModel<EmployeePaymentEmplListExtract>
{
    private ISingleSelectModel _paymentModel;
    private ISingleSelectModel _financingSourceModel;
    private ISingleSelectModel _financingSourceItemModel;
    private IMultiSelectModel _orgUnitModel;
    private List<OrgUnit> _orgUnitList;
    private DynamicListDataSource<EmployeePost> _employeeDataSource;
    private Boolean _disabledAmount;
    private List<EmployeePost> _selectEmployee;
    private List<EmployeePost> _tempEmployeeList = new ArrayList<>();
    private Map<Long, Double> _employeeIdAmountMap;
    private List<EmployeePost> _employeePostList;

    private Boolean _childOrgUnit = false;
    private Payment _payment;
    private Double _amount;
    private FinancingSource _financingSource;
    private FinancingSourceItem _financingSourceItem;
    private Date _assignmentDate;
    private Date _assignmentBeginDate;
    private Date _assignmentEndDate;

    private Date _dummy;

    //Calculated Getters

    public String getAmountId()
    {
        return getEmployeeDataSource().getCurrentEntity().getId() + "_amount";
    }

    //Getters & Setters

    public Date getDummy()
    {
        return _dummy;
    }

    public void setDummy(Date dummy)
    {
        _dummy = dummy;
    }

    public List<EmployeePost> getEmployeePostList()
    {
        return _employeePostList;
    }

    public void setEmployeePostList(List<EmployeePost> employeePostList)
    {
        _employeePostList = employeePostList;
    }

    public Map<Long, Double> getEmployeeIdAmountMap()
    {
        return _employeeIdAmountMap;
    }

    public void setEmployeeIdAmountMap(Map<Long, Double> employeeIdAmountMap)
    {
        _employeeIdAmountMap = employeeIdAmountMap;
    }

    public List<EmployeePost> getTempEmployeeList()
    {
        return _tempEmployeeList;
    }

    public void setTempEmployeeList(List<EmployeePost> tempEmployeeList)
    {
        _tempEmployeeList = tempEmployeeList;
    }

    public List<EmployeePost> getSelectEmployee()
    {
        return _selectEmployee;
    }

    public void setSelectEmployee(List<EmployeePost> selectEmployee)
    {
        _selectEmployee = selectEmployee;
    }

    public Date getAssignmentBeginDate()
    {
        return _assignmentBeginDate;
    }

    public void setAssignmentBeginDate(Date assignmentBeginDate)
    {
        _assignmentBeginDate = assignmentBeginDate;
    }

    public Date getAssignmentEndDate()
    {
        return _assignmentEndDate;
    }

    public void setAssignmentEndDate(Date assignmentEndDate)
    {
        _assignmentEndDate = assignmentEndDate;
    }

    public Date getAssignmentDate()
    {
        return _assignmentDate;
    }

    public void setAssignmentDate(Date assignmentDate)
    {
        _assignmentDate = assignmentDate;
    }

    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    public void setFinancingSource(FinancingSource financingSource)
    {
        _financingSource = financingSource;
    }

    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        _financingSourceItem = financingSourceItem;
    }

    public Double getAmount()
    {
        return _amount;
    }

    public void setAmount(Double amount)
    {
        _amount = amount;
    }

    public Payment getPayment()
    {
        return _payment;
    }

    public void setPayment(Payment payment)
    {
        _payment = payment;
    }

    public Boolean getDisabledAmount()
    {
        return _disabledAmount;
    }

    public void setDisabledAmount(Boolean disabledAmount)
    {
        _disabledAmount = disabledAmount;
    }

    public ISingleSelectModel getPaymentModel()
    {
        return _paymentModel;
    }

    public void setPaymentModel(ISingleSelectModel paymentModel)
    {
        _paymentModel = paymentModel;
    }

    public ISingleSelectModel getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(ISingleSelectModel financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public IMultiSelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(IMultiSelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public List<OrgUnit> getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public Boolean getChildOrgUnit()
    {
        return _childOrgUnit;
    }

    public void setChildOrgUnit(Boolean childOrgUnit)
    {
        _childOrgUnit = childOrgUnit;
    }

    public DynamicListDataSource<EmployeePost> getEmployeeDataSource()
    {
        return _employeeDataSource;
    }

    public void setEmployeeDataSource(DynamicListDataSource<EmployeePost> employeeDataSource)
    {
        _employeeDataSource = employeeDataSource;
    }
}
