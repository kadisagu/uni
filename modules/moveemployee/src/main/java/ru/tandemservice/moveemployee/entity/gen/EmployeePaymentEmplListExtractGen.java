package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из списочного приказа по кадровому составу. О премировании работников
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeePaymentEmplListExtractGen extends ListEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract";
    public static final String ENTITY_NAME = "employeePaymentEmplListExtract";
    public static final int VERSION_HASH = 161559135;
    private static IEntityMeta ENTITY_META;

    public static final String P_ASSIGNMENT_DATE = "assignmentDate";
    public static final String P_ASSIGNMENT_BEGIN_DATE = "assignmentBeginDate";
    public static final String P_ASSIGNMENT_END_DATE = "assignmentEndDate";
    public static final String L_PAYMENT = "payment";
    public static final String P_AMOUNT_EMPLOYEE = "amountEmployee";
    public static final String P_AMOUNT_PAYMENT = "amountPayment";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";
    public static final String P_CHILD_ORG_UNIT = "childOrgUnit";
    public static final String L_CREATE_EMPLOYEE_PAYMENT = "createEmployeePayment";

    private Date _assignmentDate;     // Дата назначения выплаты
    private Date _assignmentBeginDate;     // Назначена с даты
    private Date _assignmentEndDate;     // Назначена по дату
    private Payment _payment;     // Выплата
    private Double _amountEmployee;     // Сумма выплаты сотруднику
    private Double _amountPayment;     // Размер выплаты
    private FinancingSource _financingSource;     // Источник финансирования (настройка названий)
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (настройка названий, детально)
    private Boolean _childOrgUnit;     // Учитывать дочерние подразделения
    private EmployeePayment _createEmployeePayment;     // Выплата сотруднику, созданная при проведении приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата назначения выплаты. Свойство не может быть null.
     */
    @NotNull
    public Date getAssignmentDate()
    {
        return _assignmentDate;
    }

    /**
     * @param assignmentDate Дата назначения выплаты. Свойство не может быть null.
     */
    public void setAssignmentDate(Date assignmentDate)
    {
        dirty(_assignmentDate, assignmentDate);
        _assignmentDate = assignmentDate;
    }

    /**
     * @return Назначена с даты.
     */
    public Date getAssignmentBeginDate()
    {
        return _assignmentBeginDate;
    }

    /**
     * @param assignmentBeginDate Назначена с даты.
     */
    public void setAssignmentBeginDate(Date assignmentBeginDate)
    {
        dirty(_assignmentBeginDate, assignmentBeginDate);
        _assignmentBeginDate = assignmentBeginDate;
    }

    /**
     * @return Назначена по дату.
     */
    public Date getAssignmentEndDate()
    {
        return _assignmentEndDate;
    }

    /**
     * @param assignmentEndDate Назначена по дату.
     */
    public void setAssignmentEndDate(Date assignmentEndDate)
    {
        dirty(_assignmentEndDate, assignmentEndDate);
        _assignmentEndDate = assignmentEndDate;
    }

    /**
     * @return Выплата. Свойство не может быть null.
     */
    @NotNull
    public Payment getPayment()
    {
        return _payment;
    }

    /**
     * @param payment Выплата. Свойство не может быть null.
     */
    public void setPayment(Payment payment)
    {
        dirty(_payment, payment);
        _payment = payment;
    }

    /**
     * @return Сумма выплаты сотруднику.
     */
    public Double getAmountEmployee()
    {
        return _amountEmployee;
    }

    /**
     * @param amountEmployee Сумма выплаты сотруднику.
     */
    public void setAmountEmployee(Double amountEmployee)
    {
        dirty(_amountEmployee, amountEmployee);
        _amountEmployee = amountEmployee;
    }

    /**
     * @return Размер выплаты.
     */
    public Double getAmountPayment()
    {
        return _amountPayment;
    }

    /**
     * @param amountPayment Размер выплаты.
     */
    public void setAmountPayment(Double amountPayment)
    {
        dirty(_amountPayment, amountPayment);
        _amountPayment = amountPayment;
    }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     */
    @NotNull
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования (настройка названий). Свойство не может быть null.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (настройка названий, детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    /**
     * @return Учитывать дочерние подразделения.
     */
    public Boolean getChildOrgUnit()
    {
        return _childOrgUnit;
    }

    /**
     * @param childOrgUnit Учитывать дочерние подразделения.
     */
    public void setChildOrgUnit(Boolean childOrgUnit)
    {
        dirty(_childOrgUnit, childOrgUnit);
        _childOrgUnit = childOrgUnit;
    }

    /**
     * @return Выплата сотруднику, созданная при проведении приказа.
     */
    public EmployeePayment getCreateEmployeePayment()
    {
        return _createEmployeePayment;
    }

    /**
     * @param createEmployeePayment Выплата сотруднику, созданная при проведении приказа.
     */
    public void setCreateEmployeePayment(EmployeePayment createEmployeePayment)
    {
        dirty(_createEmployeePayment, createEmployeePayment);
        _createEmployeePayment = createEmployeePayment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeePaymentEmplListExtractGen)
        {
            setAssignmentDate(((EmployeePaymentEmplListExtract)another).getAssignmentDate());
            setAssignmentBeginDate(((EmployeePaymentEmplListExtract)another).getAssignmentBeginDate());
            setAssignmentEndDate(((EmployeePaymentEmplListExtract)another).getAssignmentEndDate());
            setPayment(((EmployeePaymentEmplListExtract)another).getPayment());
            setAmountEmployee(((EmployeePaymentEmplListExtract)another).getAmountEmployee());
            setAmountPayment(((EmployeePaymentEmplListExtract)another).getAmountPayment());
            setFinancingSource(((EmployeePaymentEmplListExtract)another).getFinancingSource());
            setFinancingSourceItem(((EmployeePaymentEmplListExtract)another).getFinancingSourceItem());
            setChildOrgUnit(((EmployeePaymentEmplListExtract)another).getChildOrgUnit());
            setCreateEmployeePayment(((EmployeePaymentEmplListExtract)another).getCreateEmployeePayment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeePaymentEmplListExtractGen> extends ListEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeePaymentEmplListExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeePaymentEmplListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "assignmentDate":
                    return obj.getAssignmentDate();
                case "assignmentBeginDate":
                    return obj.getAssignmentBeginDate();
                case "assignmentEndDate":
                    return obj.getAssignmentEndDate();
                case "payment":
                    return obj.getPayment();
                case "amountEmployee":
                    return obj.getAmountEmployee();
                case "amountPayment":
                    return obj.getAmountPayment();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
                case "childOrgUnit":
                    return obj.getChildOrgUnit();
                case "createEmployeePayment":
                    return obj.getCreateEmployeePayment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "assignmentDate":
                    obj.setAssignmentDate((Date) value);
                    return;
                case "assignmentBeginDate":
                    obj.setAssignmentBeginDate((Date) value);
                    return;
                case "assignmentEndDate":
                    obj.setAssignmentEndDate((Date) value);
                    return;
                case "payment":
                    obj.setPayment((Payment) value);
                    return;
                case "amountEmployee":
                    obj.setAmountEmployee((Double) value);
                    return;
                case "amountPayment":
                    obj.setAmountPayment((Double) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
                case "childOrgUnit":
                    obj.setChildOrgUnit((Boolean) value);
                    return;
                case "createEmployeePayment":
                    obj.setCreateEmployeePayment((EmployeePayment) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "assignmentDate":
                        return true;
                case "assignmentBeginDate":
                        return true;
                case "assignmentEndDate":
                        return true;
                case "payment":
                        return true;
                case "amountEmployee":
                        return true;
                case "amountPayment":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
                case "childOrgUnit":
                        return true;
                case "createEmployeePayment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "assignmentDate":
                    return true;
                case "assignmentBeginDate":
                    return true;
                case "assignmentEndDate":
                    return true;
                case "payment":
                    return true;
                case "amountEmployee":
                    return true;
                case "amountPayment":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
                case "childOrgUnit":
                    return true;
                case "createEmployeePayment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "assignmentDate":
                    return Date.class;
                case "assignmentBeginDate":
                    return Date.class;
                case "assignmentEndDate":
                    return Date.class;
                case "payment":
                    return Payment.class;
                case "amountEmployee":
                    return Double.class;
                case "amountPayment":
                    return Double.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
                case "childOrgUnit":
                    return Boolean.class;
                case "createEmployeePayment":
                    return EmployeePayment.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeePaymentEmplListExtract> _dslPath = new Path<EmployeePaymentEmplListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeePaymentEmplListExtract");
    }
            

    /**
     * @return Дата назначения выплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAssignmentDate()
     */
    public static PropertyPath<Date> assignmentDate()
    {
        return _dslPath.assignmentDate();
    }

    /**
     * @return Назначена с даты.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAssignmentBeginDate()
     */
    public static PropertyPath<Date> assignmentBeginDate()
    {
        return _dslPath.assignmentBeginDate();
    }

    /**
     * @return Назначена по дату.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAssignmentEndDate()
     */
    public static PropertyPath<Date> assignmentEndDate()
    {
        return _dslPath.assignmentEndDate();
    }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getPayment()
     */
    public static Payment.Path<Payment> payment()
    {
        return _dslPath.payment();
    }

    /**
     * @return Сумма выплаты сотруднику.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAmountEmployee()
     */
    public static PropertyPath<Double> amountEmployee()
    {
        return _dslPath.amountEmployee();
    }

    /**
     * @return Размер выплаты.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAmountPayment()
     */
    public static PropertyPath<Double> amountPayment()
    {
        return _dslPath.amountPayment();
    }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    /**
     * @return Учитывать дочерние подразделения.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getChildOrgUnit()
     */
    public static PropertyPath<Boolean> childOrgUnit()
    {
        return _dslPath.childOrgUnit();
    }

    /**
     * @return Выплата сотруднику, созданная при проведении приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getCreateEmployeePayment()
     */
    public static EmployeePayment.Path<EmployeePayment> createEmployeePayment()
    {
        return _dslPath.createEmployeePayment();
    }

    public static class Path<E extends EmployeePaymentEmplListExtract> extends ListEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _assignmentDate;
        private PropertyPath<Date> _assignmentBeginDate;
        private PropertyPath<Date> _assignmentEndDate;
        private Payment.Path<Payment> _payment;
        private PropertyPath<Double> _amountEmployee;
        private PropertyPath<Double> _amountPayment;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;
        private PropertyPath<Boolean> _childOrgUnit;
        private EmployeePayment.Path<EmployeePayment> _createEmployeePayment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата назначения выплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAssignmentDate()
     */
        public PropertyPath<Date> assignmentDate()
        {
            if(_assignmentDate == null )
                _assignmentDate = new PropertyPath<Date>(EmployeePaymentEmplListExtractGen.P_ASSIGNMENT_DATE, this);
            return _assignmentDate;
        }

    /**
     * @return Назначена с даты.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAssignmentBeginDate()
     */
        public PropertyPath<Date> assignmentBeginDate()
        {
            if(_assignmentBeginDate == null )
                _assignmentBeginDate = new PropertyPath<Date>(EmployeePaymentEmplListExtractGen.P_ASSIGNMENT_BEGIN_DATE, this);
            return _assignmentBeginDate;
        }

    /**
     * @return Назначена по дату.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAssignmentEndDate()
     */
        public PropertyPath<Date> assignmentEndDate()
        {
            if(_assignmentEndDate == null )
                _assignmentEndDate = new PropertyPath<Date>(EmployeePaymentEmplListExtractGen.P_ASSIGNMENT_END_DATE, this);
            return _assignmentEndDate;
        }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getPayment()
     */
        public Payment.Path<Payment> payment()
        {
            if(_payment == null )
                _payment = new Payment.Path<Payment>(L_PAYMENT, this);
            return _payment;
        }

    /**
     * @return Сумма выплаты сотруднику.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAmountEmployee()
     */
        public PropertyPath<Double> amountEmployee()
        {
            if(_amountEmployee == null )
                _amountEmployee = new PropertyPath<Double>(EmployeePaymentEmplListExtractGen.P_AMOUNT_EMPLOYEE, this);
            return _amountEmployee;
        }

    /**
     * @return Размер выплаты.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getAmountPayment()
     */
        public PropertyPath<Double> amountPayment()
        {
            if(_amountPayment == null )
                _amountPayment = new PropertyPath<Double>(EmployeePaymentEmplListExtractGen.P_AMOUNT_PAYMENT, this);
            return _amountPayment;
        }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

    /**
     * @return Учитывать дочерние подразделения.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getChildOrgUnit()
     */
        public PropertyPath<Boolean> childOrgUnit()
        {
            if(_childOrgUnit == null )
                _childOrgUnit = new PropertyPath<Boolean>(EmployeePaymentEmplListExtractGen.P_CHILD_ORG_UNIT, this);
            return _childOrgUnit;
        }

    /**
     * @return Выплата сотруднику, созданная при проведении приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract#getCreateEmployeePayment()
     */
        public EmployeePayment.Path<EmployeePayment> createEmployeePayment()
        {
            if(_createEmployeePayment == null )
                _createEmployeePayment = new EmployeePayment.Path<EmployeePayment>(L_CREATE_EMPLOYEE_PAYMENT, this);
            return _createEmployeePayment;
        }

        public Class getEntityClass()
        {
            return EmployeePaymentEmplListExtract.class;
        }

        public String getEntityName()
        {
            return "employeePaymentEmplListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
