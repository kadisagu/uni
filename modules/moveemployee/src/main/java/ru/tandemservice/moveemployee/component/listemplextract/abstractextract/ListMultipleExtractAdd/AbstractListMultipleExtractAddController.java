/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListMultipleExtractAdd;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.log.InsertEntityEvent;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author dseleznev
 * Created on: 10.06.2009
 */
public abstract class AbstractListMultipleExtractAddController<T extends ListEmployeeExtract, IDAO extends IAbstractListMultipleExtractAddDAO<T, Model>, Model extends AbstractListMultipleExtractAddModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        // настройка действует в рамках типа параграфа
        model.setSearchListSettingsKey("EmployeeListMultipleParagraphAdd." + model.getExtractType().getCode() + ".");

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Employee> dataSource = UniBaseUtils.createDataSource(component, getDao());

        CheckboxColumn checkboxColumn = new CheckboxColumn("checkbox", "", true);
        if (model.getSelectedItemList() != null)
            checkboxColumn.setSelectedObjects(model.getSelectedItemList());

        dataSource.addColumn(checkboxColumn);

        prepareListDataSource(component, dataSource);
        model.setDataSource(dataSource);
    }

    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Employee> dataSource)
    {
        /*dataSource.addColumn(new SimpleColumn("Номер группы", Group.P_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", Model.EDUCATION_LEVEL_KEY).setClickable(false));*/
        dataSource.setOrder("title", OrderDirection.asc);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Debug.stopLogging();
        IEventServiceLock eventLock = CoreServices.eventService().lock();
        try
        {
            getDao().update(getModel(component));
        }
        finally
        {
            eventLock.release();
            Debug.resumeLogging();
        }

        for (EmployeeListParagraph paragraph : getModel(component).getAddedParagraphs())
        {
            CoreServices.eventService().fireEvent(new InsertEntityEvent("Создан параграф №" + paragraph.getNumber(), paragraph));
        }

        deactivate(component);
    }
}