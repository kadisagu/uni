/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e3;

import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemInner;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 27.04.2011
 */
public class EmploymentDismissalEmplListExtractDao extends UniBaseDao implements IExtractComponentDao<EmploymentDismissalEmplListExtract>
{
    @Override
    public void doCommit(EmploymentDismissalEmplListExtract extract, Map parameters)
    {
        extract.setDismissalDateOld(extract.getEmployeePost().getDismissalDate());
        extract.setEmployeePostStatusOld(extract.getEmployeePost().getPostStatus());

        extract.getEmployeePost().setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_FIRED));
        extract.getEmployeePost().setDismissalDate(extract.getDismissalDate());

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEmployeePost());
        if (staffRateItemList.size() > 0)
        {
            EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEmployeePost(), staffRateItemList);
            empHistItem.setDismissalDate(extract.getDismissalDate());
            empHistItem.setCurrent(false);
            getSession().saveOrUpdate(empHistItem);
        }

        if (null == extract.getEntity())
        {
            extract.setEntity(extract.getEmployeePost());
        }

        update(extract.getEmployeePost());
        update(extract);
    }

    @Override
    public void doRollback(EmploymentDismissalEmplListExtract extract, Map parameters)
    {
        extract.getEmployeePost().setDismissalDate(extract.getDismissalDateOld());
        extract.getEmployeePost().setPostStatus(extract.getEmployeePostStatusOld());

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEmployeePost());
        if (staffRateItemList.size() > 0)
        {
            EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEmployeePost(), staffRateItemList);
            empHistItem.setDismissalDate(null);
            empHistItem.setCurrent(true);
            getSession().saveOrUpdate(empHistItem);
        }

        update(extract.getEmployeePost());
    }
}