package ru.tandemservice.moveemployee.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uniemp.entity.catalog.EmployeeExtractGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип выписки по кадрам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeExtractTypeGen extends EntityBase
 implements INaturalIdentifiable<EmployeeExtractTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType";
    public static final String ENTITY_NAME = "employeeExtractType";
    public static final int VERSION_HASH = -1926556515;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_ACTIVE = "active";
    public static final String P_DESCRIPTION = "description";
    public static final String P_INDEX = "index";
    public static final String P_FORM_FOR_EMPLOYEE = "formForEmployee";
    public static final String L_PARENT = "parent";
    public static final String P_FORMATION_ENTITY = "formationEntity";
    public static final String L_GROUP = "group";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _active;     // Активный
    private String _description;     // Описание
    private Integer _index;     // Индекс
    private boolean _formForEmployee;     // Формировать для кадрового ресурса
    private EmployeeExtractType _parent;     // Тип выписки по кадрам
    private String _formationEntity;     // Название сущности типа приказа. Для разделения внутри типа
    private EmployeeExtractGroup _group;     // Группа приказов
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Активный. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Активный. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Индекс.
     */
    public Integer getIndex()
    {
        return _index;
    }

    /**
     * @param index Индекс.
     */
    public void setIndex(Integer index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Формировать для кадрового ресурса. Свойство не может быть null.
     */
    @NotNull
    public boolean isFormForEmployee()
    {
        return _formForEmployee;
    }

    /**
     * @param formForEmployee Формировать для кадрового ресурса. Свойство не может быть null.
     */
    public void setFormForEmployee(boolean formForEmployee)
    {
        dirty(_formForEmployee, formForEmployee);
        _formForEmployee = formForEmployee;
    }

    /**
     * @return Тип выписки по кадрам.
     */
    public EmployeeExtractType getParent()
    {
        return _parent;
    }

    /**
     * @param parent Тип выписки по кадрам.
     */
    public void setParent(EmployeeExtractType parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Название сущности типа приказа. Для разделения внутри типа.
     */
    @Length(max=255)
    public String getFormationEntity()
    {
        return _formationEntity;
    }

    /**
     * @param formationEntity Название сущности типа приказа. Для разделения внутри типа.
     */
    public void setFormationEntity(String formationEntity)
    {
        dirty(_formationEntity, formationEntity);
        _formationEntity = formationEntity;
    }

    /**
     * @return Группа приказов.
     */
    public EmployeeExtractGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа приказов.
     */
    public void setGroup(EmployeeExtractGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeExtractTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EmployeeExtractType)another).getCode());
            }
            setActive(((EmployeeExtractType)another).isActive());
            setDescription(((EmployeeExtractType)another).getDescription());
            setIndex(((EmployeeExtractType)another).getIndex());
            setFormForEmployee(((EmployeeExtractType)another).isFormForEmployee());
            setParent(((EmployeeExtractType)another).getParent());
            setFormationEntity(((EmployeeExtractType)another).getFormationEntity());
            setGroup(((EmployeeExtractType)another).getGroup());
            setTitle(((EmployeeExtractType)another).getTitle());
        }
    }

    public INaturalId<EmployeeExtractTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EmployeeExtractTypeGen>
    {
        private static final String PROXY_NAME = "EmployeeExtractTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EmployeeExtractTypeGen.NaturalId) ) return false;

            EmployeeExtractTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeExtractTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeExtractType.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeExtractType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "active":
                    return obj.isActive();
                case "description":
                    return obj.getDescription();
                case "index":
                    return obj.getIndex();
                case "formForEmployee":
                    return obj.isFormForEmployee();
                case "parent":
                    return obj.getParent();
                case "formationEntity":
                    return obj.getFormationEntity();
                case "group":
                    return obj.getGroup();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "formForEmployee":
                    obj.setFormForEmployee((Boolean) value);
                    return;
                case "parent":
                    obj.setParent((EmployeeExtractType) value);
                    return;
                case "formationEntity":
                    obj.setFormationEntity((String) value);
                    return;
                case "group":
                    obj.setGroup((EmployeeExtractGroup) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "active":
                        return true;
                case "description":
                        return true;
                case "index":
                        return true;
                case "formForEmployee":
                        return true;
                case "parent":
                        return true;
                case "formationEntity":
                        return true;
                case "group":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "active":
                    return true;
                case "description":
                    return true;
                case "index":
                    return true;
                case "formForEmployee":
                    return true;
                case "parent":
                    return true;
                case "formationEntity":
                    return true;
                case "group":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "active":
                    return Boolean.class;
                case "description":
                    return String.class;
                case "index":
                    return Integer.class;
                case "formForEmployee":
                    return Boolean.class;
                case "parent":
                    return EmployeeExtractType.class;
                case "formationEntity":
                    return String.class;
                case "group":
                    return EmployeeExtractGroup.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeExtractType> _dslPath = new Path<EmployeeExtractType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeExtractType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Индекс.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Формировать для кадрового ресурса. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#isFormForEmployee()
     */
    public static PropertyPath<Boolean> formForEmployee()
    {
        return _dslPath.formForEmployee();
    }

    /**
     * @return Тип выписки по кадрам.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getParent()
     */
    public static EmployeeExtractType.Path<EmployeeExtractType> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Название сущности типа приказа. Для разделения внутри типа.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getFormationEntity()
     */
    public static PropertyPath<String> formationEntity()
    {
        return _dslPath.formationEntity();
    }

    /**
     * @return Группа приказов.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getGroup()
     */
    public static EmployeeExtractGroup.Path<EmployeeExtractGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EmployeeExtractType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _active;
        private PropertyPath<String> _description;
        private PropertyPath<Integer> _index;
        private PropertyPath<Boolean> _formForEmployee;
        private EmployeeExtractType.Path<EmployeeExtractType> _parent;
        private PropertyPath<String> _formationEntity;
        private EmployeeExtractGroup.Path<EmployeeExtractGroup> _group;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EmployeeExtractTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(EmployeeExtractTypeGen.P_ACTIVE, this);
            return _active;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(EmployeeExtractTypeGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Индекс.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(EmployeeExtractTypeGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Формировать для кадрового ресурса. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#isFormForEmployee()
     */
        public PropertyPath<Boolean> formForEmployee()
        {
            if(_formForEmployee == null )
                _formForEmployee = new PropertyPath<Boolean>(EmployeeExtractTypeGen.P_FORM_FOR_EMPLOYEE, this);
            return _formForEmployee;
        }

    /**
     * @return Тип выписки по кадрам.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getParent()
     */
        public EmployeeExtractType.Path<EmployeeExtractType> parent()
        {
            if(_parent == null )
                _parent = new EmployeeExtractType.Path<EmployeeExtractType>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Название сущности типа приказа. Для разделения внутри типа.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getFormationEntity()
     */
        public PropertyPath<String> formationEntity()
        {
            if(_formationEntity == null )
                _formationEntity = new PropertyPath<String>(EmployeeExtractTypeGen.P_FORMATION_ENTITY, this);
            return _formationEntity;
        }

    /**
     * @return Группа приказов.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getGroup()
     */
        public EmployeeExtractGroup.Path<EmployeeExtractGroup> group()
        {
            if(_group == null )
                _group = new EmployeeExtractGroup.Path<EmployeeExtractGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EmployeeExtractTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EmployeeExtractType.class;
        }

        public String getEntityName()
        {
            return "employeeExtractType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
