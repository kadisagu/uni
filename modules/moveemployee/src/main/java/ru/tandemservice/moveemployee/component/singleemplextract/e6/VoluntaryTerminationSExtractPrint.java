/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e6;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 25.09.2009
 */
public class VoluntaryTerminationSExtractPrint implements IPrintFormCreator<VoluntaryTerminationSExtract>
{
    public static final Map<String, String> RELATIVE_TYPE_DATIVE_CASE_MAP = new HashMap<>();
    static 
    {
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.HUSBAND, "мужу");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.WIFE, "жене");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.FATHER, "отцу");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.MOTHER, "матери");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.SON, "сыну");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.DAUGHTER, "дочери");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.GRANDFATHER, "дедушке");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.GRANDMOTHER, "бабушке");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.GRANDSON, "внуку");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.GRANDDAUGHTER, "внучке");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.BROTHER, "брату");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.SISTER, "сестре");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.STEPFATHER, "отчиму");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.STEPMOTHER, "мачехе");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.STEPSON, "пасынку");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.STEPDAUGHTER, "падчерице");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.FATHER_IN_LAW_FOR_MALE, "тестю");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.MOTHER_IN_LAW_FOR_MALE, "теще");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.FATHER_IN_LAW_FOR_FEMALE, "свекру");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.MOTHER_IN_LAW_FOR_FEMALE, "свекрови");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.SON_IN_LAW, "зятю");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.DAUGHTER_IN_LAW, "невестке");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.ANOTHER, "родственнику");
        RELATIVE_TYPE_DATIVE_CASE_MAP.put(RelationDegreeCodes.TUTOR, "опекуну");
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, VoluntaryTerminationSExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder)extract.getParagraph().getOrder());
        
        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        
        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());
        if(null != contract)
        {
            modifier.put("lcDay", RussianDateFormatUtils.getDayString(contract.getDate(), true));
            modifier.put("lcMonthStr", RussianDateFormatUtils.getMonthName(contract.getDate(), false));
            modifier.put("lcYear", RussianDateFormatUtils.getYearString(contract.getDate(), true));
            modifier.put("labourContractNumber", contract.getNumber());
        }
        else
        {
            modifier.put("lcDay", "");
            modifier.put("lcMonthStr", "");
            modifier.put("lcYear", "");
            modifier.put("labourContractNumber", "");
        }

        modifier.put("dismissDay", RussianDateFormatUtils.getDayString(extract.getTerminationDate(), true));
        modifier.put("dismissMonthStr", RussianDateFormatUtils.getMonthName(extract.getTerminationDate(), false));
        modifier.put("dismissYr", RussianDateFormatUtils.getYearString(extract.getTerminationDate(), true));
        
        StringBuilder builder = new StringBuilder();

        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());
        Double staffRateSumm = 0.0d;
        for (EmployeePostStaffRateItem item : staffRateItems)
            staffRateSumm +=item.getStaffRate();
        if (staffRateItems.size() != 0 && staffRateSumm != 1)
            builder.append(" (").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateSumm)).append(" ставки)");
        
        if(UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(extract.getEntity().getPostType().getCode()))
            builder.append(builder.length() > 0 ? " " : "").append("на условиях внутривузовского совместительства");
        else if(UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(extract.getEntity().getPostType().getCode()))
            builder.append(builder.length() > 0 ? " " : "").append("на условиях внешнего совместительства");
        
        if(builder.length() > 0) modifier.put("postAdditionalAttrs", builder.toString());
        else modifier.put("postAdditionalAttrs", "");
        
        String remainHolidaysDbl = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(extract.getPaymentCharge());
        String remainHolidaysStr = new Double(extract.getPaymentCharge()).intValue() == extract.getPaymentCharge() ? String.valueOf(new Double(extract.getPaymentCharge()).intValue()) : remainHolidaysDbl;
        
        modifier.put("dismissReason", extract.getDismissReason().getTitle());
        modifier.put("lawArticleitem", extract.getDismissReason().getLawItem());
        modifier.put("debtDays", "0".equals(remainHolidaysStr) ? "-" : remainHolidaysStr);
        modifier.put("remainHolidays", 0 == extract.getHolidayDaysAmount() ? "-" : String.valueOf(extract.getHolidayDaysAmount()));
        
        if(extract.getHolidayDaysAmount() != 0)
        {
            StringBuilder holidayCompensationString = new StringBuilder("Выплатить компенсацию за неиспользованный отпуск ");
            if(null != extract.getHolidayPeriodBegin() && null != extract.getHolidayPeriodEnd())
            {
                holidayCompensationString.append("за период с ");
                holidayCompensationString.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getHolidayPeriodBegin()));
                holidayCompensationString.append(" по ");
                holidayCompensationString.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getHolidayPeriodEnd()));
                holidayCompensationString.append(" г. ");
            }
            holidayCompensationString.append("- ").append(NumberConvertingUtil.getCalendarDaysWithName(extract.getHolidayDaysAmount())).append(".");
            modifier.put("payCompensationForUnusedHolidays", holidayCompensationString.toString());
            modifier.put("exactMoneyForUnworkedDays", "");
        }
        else if(extract.getPaymentCharge() > 0)
        {
            modifier.put("exactMoneyForUnworkedDays", "Удержать за " +
                    DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getPaymentCharge()) + " " +
                    CommonBaseStringUtil.numberPostfixCase((long) extract.getPaymentCharge(), "не отработанный день", "не отработанных дня", "не отработанных дней") + ".");
            modifier.put("payCompensationForUnusedHolidays", "");
        }
        else
        {
            modifier.put("payCompensationForUnusedHolidays", "");
            modifier.put("exactMoneyForUnworkedDays", "");
        }
        modifier.put("addActionsSeparator", "");

        int counter = 0;
        List<String[]> addActions = new ArrayList<>();
        RtfTableModifier tableModifier = new RtfTableModifier();
        if(null != extract.getRelativeType() || null != extract.getRelativeFio() || null != extract.getIdentityCard()) counter = 1;
        if(extract.isExcludeFromEmployeeList()) addActions.add(new String[] {(counter > 0 ? counter + ". " : ""), "Исключить из списочного состава университета" + (counter++ > 0 ? ";" : ".")});
         
        if(null != extract.getRelativeType() || null != extract.getRelativeFio() || null != extract.getIdentityCard())
        {
            StringBuilder relativeBuilder = new StringBuilder();
            relativeBuilder.append("Выплатить неполученную ко дню смерти заработную плату");
            if(null != extract.getRelativeType()) relativeBuilder.append(" ").append(RELATIVE_TYPE_DATIVE_CASE_MAP.get(extract.getRelativeType().getCode()));
            if(null != extract.getRelativeFio()) relativeBuilder.append(" ").append(extract.getRelativeFio());
            if(null != extract.getIdentityCard()) relativeBuilder.append(" ").append(extract.getIdentityCard());
            addActions.add(new String[] {(counter > 1 ? counter + ". " : ""), relativeBuilder.append(".").toString()});
        }
        
        tableModifier.put("ADD_ACTIONS", addActions.toArray(new String[][] {}));
        
        tableModifier.modify(document);
        modifier.modify(document);
        return document;
    }
}