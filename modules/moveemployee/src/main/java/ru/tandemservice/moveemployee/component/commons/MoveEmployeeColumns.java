/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author dseleznev
 *         Created on: 07.11.2008
 */
@SuppressWarnings("unchecked")
public class MoveEmployeeColumns
{
    public static final String COMMIT_DATE_COLUMN = "commitDate";
    public static final String COMMIT_DATE_SYSTEM_COLUMN = "commitDateSystem";

    public static AbstractColumn getCreateDateColumn()
    {
        return new SimpleColumn(IAbstractExtract.P_CREATE_DATE, "Дата формирования", IAbstractExtract.P_CREATE_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME);
    }

    public static AbstractColumn getFullFioColumn()
    {
        return new SimpleColumn("ФИО сотрудника", ModularEmployeeExtract.EMPLOYEE_FIO_KEY).setClickable(false);
    }

    public static AbstractColumn getExtractOrderTypeColumn()
    {
        return new SimpleColumn("Тип приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, EmployeeListOrder.L_TYPE, EmployeeExtractType.P_TITLE}).setClickable(false);
    }

    public static AbstractColumn getExtractOrderNumberColumn()
    {
        return new SimpleColumn("№ приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_NUMBER}).setClickable(false);
    }

    public static AbstractColumn getParagraphNumberColumn()
    {
        return new SimpleColumn("№ параграфа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.P_NUMBER}).setClickable(false).setOrderable(false);
    }

    public static AbstractColumn getExtractNumberColumn()
    {
        return new SimpleColumn("№ пункта приказа", IAbstractExtract.P_NUMBER).setClickable(false).setOrderable(false);
    }

    public static AbstractColumn getExtractCommitDateColumn()
    {
        return new SimpleColumn(COMMIT_DATE_COLUMN, "Дата приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, AbstractEmployeeOrder.P_COMMIT_DATE}, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false);
    }

    public static AbstractColumn getExtractCommitDateSystemColumn()
    {
        return new SimpleColumn(COMMIT_DATE_SYSTEM_COLUMN, "Дата проведения", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_COMMIT_DATE_SYSTEM}, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false);
    }

    public static AbstractColumn getExtractTypeColumn(String title)
    {
        return new SimpleColumn(title, new String[]{IAbstractExtract.L_TYPE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false);
    }

    public static AbstractColumn getExtractStateColumn(String title)
    {
        return new SimpleColumn(title, new String[]{IAbstractExtract.L_STATE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false);
    }

    public static AbstractColumn getExtractOrderStateColumn(String title)
    {
        return new SimpleColumn(title, new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.L_STATE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false);
    }

    public static AbstractColumn getExtractPrintColumn(Object handler, String permissioneKey)
    {
        return CommonBaseUtil.getPrintColumn(handler.getClass().getPackage().getName() + ":onClickExtractPrint", "Печать").setPermissionKey(permissioneKey);
    }

    public static AbstractColumn getOrderPrintColumn(Object handler, String permissioneKey)
    {
        return CommonBaseUtil.getPrintColumn(handler.getClass().getPackage().getName() + ":onClickOrderPrint", "Печать").setPermissionKey(permissioneKey);
    }

    public static AbstractColumn getExtractEditColumn(Object handler, String permissioneKey)
    {
        return new ActionColumn("Редактировать", ActionColumn.EDIT, handler.getClass().getPackage().getName() + ":onClickExtractEdit").setPermissionKey(permissioneKey).setDisabledProperty(AbstractEmployeeExtract.P_NO_EDIT);
    }

    public static AbstractColumn getExtractDeleteColumn(Object handler, String permissioneKey)
    {
        return new ActionColumn("Удалить", ActionColumn.DELETE, handler.getClass().getPackage().getName() + ":onClickExtractDelete", "Удалить «{0}»?", IAbstractExtract.P_TITLE).setPermissionKey(permissioneKey).setDisabledProperty(AbstractEmployeeExtract.P_NO_DELETE);
    }

    // composite

    public static AbstractColumn getModularExtractNumberColumn()
    {
        return new SimpleColumn("№ пункта приказа", new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.P_NUMBER}).setClickable(false).setOrderable(false);
    }

    public static AbstractColumn getListExtractNumberColumn()
    {
        return new SimpleColumn("№ пункта в параграфе", new String[]{IAbstractExtract.P_NUMBER}).setClickable(false).setOrderable(false);
    }
}