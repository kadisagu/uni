/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 *         Created on: 11.11.2008
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "order.id"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model implements IVisaOwnerModel
{
    private boolean _addExtractDisable;

    private IDataSettings _settings;
    private CommonPostfixPermissionModel _secModel;
    private EmployeeModularOrder _order = new EmployeeModularOrder();
    private String _selectedTab;
    private Long _visingStatus;
    private DynamicListDataSource<IAbstractExtract> _dataSource;
    private Map<String, Object> _visaListParameters = ParametersMap.createWith("visaOwnerModel", this);

    // filters
    private List<HSelectOption> _extractTypeList;

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    // IVisaOwnerModel

    @Override
    public IAbstractDocument getDocument()
    {
        return _order;
    }

    @Override
    public boolean isVisaListModificable()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(_order.getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        return "employeeModularOrder";
    }

    // Getters & Setters


    public boolean isAddExtractDisable()
    {
        return _addExtractDisable;
    }

    public void setAddExtractDisable(boolean addExtractDisable)
    {
        _addExtractDisable = addExtractDisable;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public EmployeeModularOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeModularOrder order)
    {
        _order = order;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        _visingStatus = visingStatus;
    }

    public DynamicListDataSource<IAbstractExtract> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<IAbstractExtract> dataSource)
    {
        _dataSource = dataSource;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }

    public List<HSelectOption> getExtractTypeList()
    {
        return _extractTypeList;
    }

    public void setExtractTypeList(List<HSelectOption> extractTypeList)
    {
        _extractTypeList = extractTypeList;
    }
}