/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e8.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract;

import java.util.*;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class Controller extends AbstractListParagraphAddEditController<PayForHolidayDayProvideHoldayEmplListExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareEmployeeDataSource(component);
    }

    private void prepareEmployeeDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getEmployeeDataSource() != null)
            return;

        DynamicListDataSource<EmployeePost> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareEmployeeDataSource(model);
        });

        CheckboxColumn checkboxColumn = new CheckboxColumn("checkColumn");
        if (model.getSelectEmployee() != null)
        {
            List<IEntity> entityList = new ArrayList<>();
            for (EmployeePost employeePost : model.getSelectEmployee())
                entityList.add(employeePost);
            checkboxColumn.setSelectedObjects(entityList);
        }
        dataSource.addColumn(checkboxColumn);
        dataSource.addColumn(new SimpleColumn("Табельный номер", EmployeePost.employee().employeeCode().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ФИО", EmployeePost.person().fullFio().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Должность", EmployeePost.postRelation().postBoundedWithQGandQL().fullTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", new String[]{EmployeePost.L_ORG_UNIT, OrgUnit.P_TITLE_WITH_TYPE}).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип назначения на должность", EmployeePost.postType().title().s()).setClickable(false));
        dataSource.addColumn(new BlockColumn("holidayDay","Нерабочий праздничный день").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Копировать дату для всех", "clone", "onClickCopyDate"));

        model.setEmployeeDataSource(dataSource);
    }

    public void onClickCopyDate(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder holidayDayHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("holidayDay");
        final Map<Long, Date> holidayDayMap = (null == holidayDayHolder ? Collections.emptyMap() : holidayDayHolder.getValueMap());

        Date currentDate = holidayDayMap.get(component.getListenerParameter());

        if (currentDate == null)
            return;

        for (EmployeePost post : model.getEmployeeDataSource().getEntityList())
        {
            if (holidayDayMap.get(post.getId()) == null)
            {
                holidayDayMap.put(post.getId(), currentDate);
            }
        }

        model.getEmployeeDataSource().refresh();
    }

    public void refreshEmployeeList(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setTempEmployeeList(model.getEmployeeDataSource().getEntityList());
        model.getEmployeeDataSource().refresh();
    }
}
