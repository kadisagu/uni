/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
@Input( { 
    @Bind(key = AbstractListExtractAddEditModel.PARAMETER_ORDER_ID, binding = "orderId"), 
    @Bind(key = AbstractListExtractAddEditModel.PARAMETER_EXTRACT_TYPE_ID, binding = "extractType.id"), 
    @Bind(key = AbstractListExtractAddEditModel.PARAMETER_EXTRACT_ID, binding = "extractId") 
    })
public abstract class AbstractListExtractAddEditModel<T extends ListEmployeeExtract>
{
    public static final String PARAMETER_ORDER_ID = "orderId";
    public static final String PARAMETER_EXTRACT_TYPE_ID = "extractTypeId";
    public static final String PARAMETER_EXTRACT_ID = "extractId";

    private Long _orderId;
    private Long _extractId;
    private EmployeeListOrder _order;
    private EmployeeListParagraph _paragraph;
    private EmployeeExtractType _extractType = new EmployeeExtractType();
    private T _extract;
    private boolean _addForm;
    private boolean _editForm;

    private ISelectModel _employeeListModel;
    private ISelectModel _employeePostListModel;

    private IMultiSelectModel _basicListModel;
    private List<EmployeeOrderReasons> _reasonList;
    private List<EmployeeOrderBasics> _selectedBasicList = new ArrayList<>();

    private static final String _commonPage = AbstractListExtractAddEditModel.class.getPackage().getName() + ".CommonListEmployeeExtractAddEdit";
    private static final String _commentsPage = AbstractListExtractAddEditModel.class.getPackage().getName() + ".BasicCommentAddEdit";

    private EmployeeOrderBasics _currentBasic;
    private Map<Long, String> _currentBasicMap = new HashMap<>();

    private String _orderReasonUpdates = "basics, comments";


    public String getCommonPage()
    {
        return _commonPage;
    }

    public String getCommentsPage()
    {
        return _commentsPage;
    }

    public boolean isNeedComments()
    {
        for (EmployeeOrderBasics basic : _selectedBasicList)
            if (basic.isCommentable()) return true;
        return false;
    }

    public String getCurrentBasicTitle()
    {
        return _currentBasicMap.get(_currentBasic.getId());
    }

    public void setCurrentBasicTitle(String title)
    {
        _currentBasicMap.put(_currentBasic.getId(), title);
    }

    public String getCurrentBasicId()
    {
        return "basicId_" + _currentBasic.getId();
    }


    // Getters & Setters

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public EmployeeExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(EmployeeExtractType extractType)
    {
        _extractType = extractType;
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public EmployeeListOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeListOrder order)
    {
        this._order = order;
    }

    public EmployeeListParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EmployeeListParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        _extract = extract;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        this._addForm = addForm;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public ISelectModel getEmployeeListModel()
    {
        return _employeeListModel;
    }

    public void setEmployeeListModel(ISelectModel employeeListModel)
    {
        this._employeeListModel = employeeListModel;
    }

    public ISelectModel getEmployeePostListModel()
    {
        return _employeePostListModel;
    }

    public void setEmployeePostListModel(ISelectModel employeePostListModel)
    {
        this._employeePostListModel = employeePostListModel;
    }

    public IMultiSelectModel getBasicListModel()
    {
        return _basicListModel;
    }

    public void setBasicListModel(IMultiSelectModel basicListModel)
    {
        this._basicListModel = basicListModel;
    }

    public List<EmployeeOrderReasons> getReasonList()
    {
        return _reasonList;
    }

    public void setReasonList(List<EmployeeOrderReasons> reasonList)
    {
        this._reasonList = reasonList;
    }

    public List<EmployeeOrderBasics> getSelectedBasicList()
    {
        return _selectedBasicList;
    }

    public void setSelectedBasicList(List<EmployeeOrderBasics> selectedBasicList)
    {
        this._selectedBasicList = selectedBasicList;
    }

    public EmployeeOrderBasics getCurrentBasic()
    {
        return _currentBasic;
    }

    public void setCurrentBasic(EmployeeOrderBasics currentBasic)
    {
        this._currentBasic = currentBasic;
    }

    public Map<Long, String> getCurrentBasicMap()
    {
        return _currentBasicMap;
    }

    public void setCurrentBasicMap(Map<Long, String> currentBasicMap)
    {
        this._currentBasicMap = currentBasicMap;
    }

    public String getOrderReasonUpdates()
    {
        return _orderReasonUpdates;
    }

    public void setOrderReasonUpdates(String orderReasonUpdates)
    {
        this._orderReasonUpdates = orderReasonUpdates;
    }
}