/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
public abstract class AbstractListExtractAddEditDAO<T extends ListEmployeeExtract, Model extends AbstractListExtractAddEditModel<T>> extends UniDao<Model> implements IAbstractListExtractAddEditDAO<T, Model>
{
    private static OrderDescriptionRegistry orderRegistry = new OrderDescriptionRegistry("p");

    static
    {
        orderRegistry.setOrders(Employee.EMPLOYEE_FIO, new OrderDescription("p", Employee.EMPLOYEE_LAST_NAME), new OrderDescription("p", Employee.EMPLOYEE_FIRST_NAME), new OrderDescription("p", Employee.EMPLOYEE_MIDDLE_NAME));
    }

    @Override
    public void prepare(final Model model)
    {
        if (model.getExtractId() != null)
        {
            // форма редактирования
            model.setAddForm(false);
            model.setExtract(this.getNotNull(model.getExtractId()));
            model.setParagraph((EmployeeListParagraph)model.getExtract().getParagraph());
            model.setOrder((EmployeeListOrder)model.getParagraph().getOrder());
            model.setExtractType(model.getExtract().getType());
        }
        else
        {
            // форма создания
            model.setAddForm(true);

            model.setExtractType(getNotNull(EmployeeExtractType.class, model.getExtractType().getId()));

            if (null != model.getOrderId())
                model.setOrder(getNotNull(EmployeeListOrder.class, model.getOrderId()));

            model.setParagraph(new EmployeeListParagraph());
            model.getParagraph().setOrder(model.getOrder());
            model.getParagraph().setNumber(1);

            model.setExtract(createNewExtractInstance(model));
            model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
            model.getExtract().setType(model.getExtractType());
            model.getExtract().setParagraph(model.getParagraph());
            model.getExtract().setCreateDate(new Date());
        }

        model.setEditForm(!model.isAddForm());

        model.setReasonList(getFirstList(EmployeeReasonToTypeRel.class, model.getOrder().getType()));
        model.setBasicListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getExtract() == null || model.getExtract().getReason() == null)
                    return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(EmployeeReasonToBasicRel.ENTITY_CLASS, "r", new String[] { IEntityRelation.L_SECOND });
                builder.add(MQExpression.eq("r", IEntityRelation.L_FIRST, model.getExtract().getReason()));
                builder.add(MQExpression.like("r", IEntityRelation.L_SECOND + "." + EmployeeOrderBasics.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("r", IEntityRelation.L_SECOND + "." + EmployeeOrderBasics.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        if (model.isEditForm())
        {
            List<EmpListExtractToBasicRelation> relationList = getList(EmpListExtractToBasicRelation.class, EmpListExtractToBasicRelation.L_EXTRACT, model.getExtract());

            model.getSelectedBasicList().clear();
            for (EmpListExtractToBasicRelation relation : relationList)
                model.getSelectedBasicList().add(relation.getBasic());
            model.setSelectedBasicList(model.getSelectedBasicList());

            model.getCurrentBasicMap().clear();
            for (EmpListExtractToBasicRelation relation : relationList)
            {
                model.getCurrentBasicMap().put(relation.getBasic().getId(), relation.getComment());
            }
        }

        model.setEmployeeListModel(new CommonSingleSelectModel(Employee.P_FULL_FIO_WITH_BIRTH_YEAR)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                String[] fio = StringUtils.split(filter);
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Employee.class, "e").column(property("e"));

                if (o != null)
                {
                    if (o instanceof Collection)
                        builder.where(in(property(Employee.id().fromAlias("e")), (Collection) o));
                    else
                        builder.where(eq(property(Employee.id().fromAlias("e")), commonValue(o)));
                }

                builder.where(eq(property(Employee.archival().fromAlias("e")), value(false)));

                if (fio.length > 0) builder.where(like(upper(property(Employee.person().identityCard().lastName().fromAlias("e"))), value(CoreStringUtils.escapeLike(fio[0]))));
                if (fio.length > 1) builder.where(like(upper(property(Employee.person().identityCard().firstName().fromAlias("e"))), value(CoreStringUtils.escapeLike(fio[1]))));
                if (fio.length > 2) builder.where(like(upper(property(Employee.person().identityCard().middleName().fromAlias("e"))), value(CoreStringUtils.escapeLike(fio[2]))));

                builder.where(notExists(
                        new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, "ex")
                                .where(eq(property(AbstractEmployeeExtract.entity().employee().id().fromAlias("ex")), property(Employee.id().fromAlias("e"))))
                                .where(ne(property(AbstractEmployeeExtract.id().fromAlias("ex")), value(model.getExtract().getId())))
                                .where(ne(property(AbstractEmployeeExtract.state().code().fromAlias("ex")), value(UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED)))
                                .where(ne(property(AbstractEmployeeExtract.type().code().fromAlias("ex")), value("2.1.1")))
                                .where(notIn(property(AbstractEmployeeExtract.id().fromAlias("ex")),
                                        new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, "ext").column(DQLExpressions.property(AbstractEmployeeExtract.id().fromAlias("ext")))
                                                .where(eq(property(AbstractEmployeeExtract.entity().employee().id().fromAlias("ext")), property(Employee.id().fromAlias("e"))))
                                                .where(eq(property(AbstractEmployeeExtract.state().code().fromAlias("ext")), value(UnimoveDefines.CATALOG_EXTRACT_STATE_REJECTED)))
                                                .where(eq(property(AbstractEmployeeExtract.paragraph().order().state().code().fromAlias("ext")), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
                                                .buildQuery()))
                                .buildQuery()));

                builder.order(property(Employee.person().identityCard().lastName().fromAlias("e")));
                builder.order(property(Employee.person().identityCard().firstName().fromAlias("e")));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        model.setEmployeePostListModel(new CommonSingleSelectModel(EmployeePost.P_EXTRA_INFORMATIVE_TITLE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                String[] fio = StringUtils.split(filter);
                final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "p").column(property("p"));

                if (o != null)
                    builder.where(eq(property(EmployeePost.id().fromAlias("p")), commonValue(o, PropertyType.LONG)));

                builder.where(eq(property(EmployeePost.employee().archival().fromAlias("p")), value(false)));
                if (fio.length > 0) builder.where(like(upper(property(EmployeePost.employee().person().identityCard().lastName().fromAlias("p"))), value(CoreStringUtils.escapeLike(fio[0]))));
                if (fio.length > 1) builder.where(like(upper(property(EmployeePost.employee().person().identityCard().firstName().fromAlias("p"))), value(CoreStringUtils.escapeLike(fio[1]))));
                if (fio.length > 2) builder.where(like(upper(property(EmployeePost.employee().person().identityCard().middleName().fromAlias("p"))), value(CoreStringUtils.escapeLike(fio[2]))));
                builder.where(eq(property(EmployeePost.orgUnit().archival().fromAlias("p")), value(false)));
                builder.where(notExists(
                        new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, "ex").column(property(AbstractEmployeeExtract.id().fromAlias("ex")))
                                .where(eq(property(AbstractEmployeeExtract.entity().id().fromAlias("ex")), property(EmployeePost.id().fromAlias("p"))))
                                .where(ne(property(AbstractEmployeeExtract.id().fromAlias("ex")), value(model.getExtract().getId())))
                                .where(ne(property(AbstractEmployeeExtract.state().code().fromAlias("ex")), value(UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED)))
                                        //не берем выписки которые отменены и находятся в проведенных приказах(случай когда выписка отменена приказом об Отмене приказа)
                                .where(notIn(property(AbstractEmployeeExtract.id().fromAlias("ex")),
                                        new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, "ext").column(DQLExpressions.property(AbstractEmployeeExtract.id().fromAlias("ext")))
                                                .where(eq(property(AbstractEmployeeExtract.entity().id().fromAlias("ext")), property(EmployeePost.id().fromAlias("p"))))
                                                .where(eq(property(AbstractEmployeeExtract.state().code().fromAlias("ext")), value(UnimoveDefines.CATALOG_EXTRACT_STATE_REJECTED)))
                                                .where(eq(property(AbstractEmployeeExtract.paragraph().order().state().code().fromAlias("ext")), value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)))
                                                .buildQuery()
                                ))
                                .buildQuery()
                ));

                builder.order(property(EmployeePost.employee().person().identityCard().lastName().fromAlias("p")));
                builder.order(property(EmployeePost.employee().person().identityCard().firstName().fromAlias("p")));
                builder.order(property(EmployeePost.postRelation().postBoundedWithQGandQL().title().fromAlias("p")));
                builder.order(property(EmployeePost.orgUnit().orgUnitType().priority().fromAlias("p")));
                builder.order(property(EmployeePost.orgUnit().orgUnitType().title().fromAlias("p")));
                builder.order(property(EmployeePost.orgUnit().shortTitle().fromAlias("p")));

                return new DQLListResultBuilder(builder, 50);
            }
        });

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        // V A L I D A T E
        final Session session = getSession();

        session.refresh(model.getParagraph().getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");

        /* TODO проверять наличие непроведенных приказов по сотруднику (может быть создан как на Employee, так и на EmployeePost)
           throw new ApplicationException("Нельзя добавить параграф, так как у студента '" + employee.getPerson().getFullFio() + "' уже есть непроведенные выписки.");*/

        session.saveOrUpdate(model.getExtract());
        session.saveOrUpdate(model.getParagraph());
        updateBasicList(model);

        List<EmployeeListParagraph> paragraphList = getList(EmployeeListParagraph.class, IAbstractParagraph.L_ORDER,model.getOrder());
        List<ListEmployeeExtract> extractList = new ArrayList<>();
        for (EmployeeListParagraph item : paragraphList)
        {
            extractList.addAll((List<ListEmployeeExtract>)item.getExtractList());
        }
        extractList.sort(ListEmployeeExtract.FULL_FIO_AND_ID_COMPARATOR);

        // нумеруем выписки с единицы
        int counter = 1;
        for (ListEmployeeExtract extract : extractList)
        {
            extract.setNumber(counter++);
            update(extract);
        }
    }

    // конструктор выписки
    protected abstract T createNewExtractInstance(Model model);

    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    /*    // TODO: doesn't need this any more
        private void syncPreCommitData(Model model)
        {
            T extract = model.getExtractBuffer();
            Employee employee = extract.getEmployee();
            EmployeePost employeePost = extract.getEntity();

            /*extract.setStudentTitleStr(employee.getPerson().getFullFio());
            extract.setStudentStatusStr(employee.getStatus().getTitle());
            extract.setPersonalNumberStr(Integer.toString(employee.getPersonalNumber()));
            extract.setCourseStr(employee.getCourse().getTitle());/
        }*/

    private void updateBasicList(Model model)
    {
        if (model.isEditForm())
        {
            for (EmpListExtractToBasicRelation relation : getList(EmpListExtractToBasicRelation.class, EmpListExtractToBasicRelation.L_EXTRACT, model.getExtract()))
                delete(relation);
            getSession().flush();
        }

        for (EmployeeOrderBasics basic : model.getSelectedBasicList())
        {
            EmpListExtractToBasicRelation relation = new EmpListExtractToBasicRelation();
            relation.setBasic(basic);
            relation.setExtract(model.getExtract());
            relation.setComment(model.getCurrentBasicMap().get(basic.getId()));
            save(relation);
        }
    }
}