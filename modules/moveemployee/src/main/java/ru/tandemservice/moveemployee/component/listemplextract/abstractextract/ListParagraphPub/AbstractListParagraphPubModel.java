/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphPub;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;

/**
 * @author ashaburov
 * Created on: 11.10.2011
 */
@Input( { @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "paragraphId") })
public class AbstractListParagraphPubModel<T extends ListEmployeeExtract>
{
    private Long _paragraphId;
    private EmployeeListParagraph _paragraph;
    private List<T> _extractList;
    private CommonPostfixPermissionModel _secModel;
    private DynamicListDataSource<T> _extractsDataSource;
    private String _searchListSettingsKey;
    
    private String _attributesPage;
    private String _formTitle;

    //Getters & Setters

    public String getSearchListSettingsKey()
    {
        return _searchListSettingsKey;
    }

    public void setSearchListSettingsKey(String searchListSettingsKey)
    {
        _searchListSettingsKey = searchListSettingsKey;
    }

    public DynamicListDataSource<T> getExtractsDataSource()
    {
        return _extractsDataSource;
    }

    public void setExtractsDataSource(DynamicListDataSource<T> extractsDataSource)
    {
        _extractsDataSource = extractsDataSource;
    }

    public EmployeeListParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EmployeeListParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public Long getParagraphId()
    {
        return _paragraphId;
    }

    public void setParagraphId(Long paragraphId)
    {
        _paragraphId = paragraphId;
    }

    public List<T> getExtractList()
    {
        return _extractList;
    }

    public void setExtractList(List<T> extractList)
    {
        _extractList = extractList;
    }

    public String getFormTitle()
    {
        return _formTitle;
    }

    public void setFormTitle(String formTitle)
    {
        _formTitle = formTitle;
    }

    public String getAttributesPage()
    {
        return _attributesPage;
    }

    public void setAttributesPage(String attributesPage)
    {
        this._attributesPage = attributesPage;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }
}