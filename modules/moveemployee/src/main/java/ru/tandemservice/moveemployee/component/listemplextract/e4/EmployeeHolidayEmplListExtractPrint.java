/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e4;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.listemplextract.CommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.ICommonListOrderPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.uniemp.UniempDefines;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 04.10.2011
 */
public class EmployeeHolidayEmplListExtractPrint implements IPrintFormCreator<EmployeeListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
        injectModifier.put("orderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);

        ICommonListOrderPrint commonListOrderPrint = (ICommonListOrderPrint) ApplicationRuntime.getBean("employeeListOrder_orderPrint");
        commonListOrderPrint.appendVisas(tableModifier, order);

        tableModifier.put("T", getPreparedOrderData(order));

        IRtfRowIntercepter rowIntercepter = getRowIntercepter();
        if (rowIntercepter != null)
            tableModifier.put("T", rowIntercepter);

        tableModifier.modify(document);

        return document;
    }

    protected String[][] getPreparedOrderData(EmployeeListOrder order)
    {
        List<EmployeeHolidayEmplListExtract> extractList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractList(order);
        if(null == extractList || extractList.isEmpty()) return new String[][]{};

        List<String[]> resultList = new ArrayList<String[]>();

        for(EmployeeHolidayEmplListExtract extract : extractList)
        {
            String[] line = new String[11];

            line[0] = extract.getEmployeeFioModified();
            line[1] = extract.getEmployee().getEmployeeCode();
            line[2] = extract.getEmployeePost().getOrgUnit().getTitle();
            line[3] = extract.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getTitle();
            line[4] = extract.getMainHolidayDuration() != null ? extract.getMainHolidayDuration().toString() : "";
            line[5] = !extract.getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL) ? extract.getHolidayType().getTitle() + ", " + extract.getSecondHolidayDuration().toString() : "";
            line[6] = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPeriodBeginDate());
            line[7] = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPeriodEndDate());
            line[8] = String.valueOf(extract.getDuration());
            line[9] = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate());
            line[10] = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate());

            resultList.add(line);
        }

        return resultList.toArray(new String[][]{});
    }

    protected IRtfRowIntercepter getRowIntercepter()
    {
        return null;
    }
}
