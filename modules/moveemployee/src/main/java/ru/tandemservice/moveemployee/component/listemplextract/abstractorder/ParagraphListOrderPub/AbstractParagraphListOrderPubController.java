/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ParagraphListOrderPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

import java.util.List;

/**
 * @author ashaburov
 * @since 11.10.2011
 */
@SuppressWarnings("unchecked")
public abstract class AbstractParagraphListOrderPubController<T extends ParagraphEmployeeListOrder, IDAO extends IAbstractParagraphListOrderPubDAO<T, Model>, Model extends AbstractParagraphListOrderPubModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setActionsPage("ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ParagraphListOrderPub.ParagraphListOrderPubActions");
        model.setParamsPage("ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ParagraphListOrderPub.ParagraphListOrderPubParams");

        getDao().prepare(model);

        // настройка действует в рамках типа приказа
        model.setSearchListSettingsKey("EmployeeListOrder." + model.getOrder().getType().getCode() + ".");

        prepareListDataSource(component);
    }

    private void rebuildListDataSource(IBusinessComponent component)
    {
        getModel(component).setDataSource(null);
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<ListEmployeeExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());

        AbstractColumn linkColumn = new PublisherLinkColumn("Название параграфа", new String[]{IAbstractExtract.L_PARAGRAPH , EmployeeListParagraph.P_TITLE}).setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                return MoveEmployeeUtils.getListParagraphPubComponent(((EmployeeListParagraph) ((ListEmployeeExtract) entity).getParagraph()).getFirstExtract().getType());
            }

            @Override
            public Object getParameters(IEntity entity)
            {
                return ((ListEmployeeExtract) entity).getParagraph().getId();
            }
        }).setOrderable(false);

        IMergeRowIdResolver mergeRowIdResolver = entity -> ((ListEmployeeExtract) entity).getParagraph().getId().toString();

        dataSource.addColumn(linkColumn.setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver));
        dataSource.addColumn(new SimpleColumn("Тип параграфа", new String[]{IAbstractExtract.L_PARAGRAPH, EmployeeListParagraph.P_FIRST_EXTRACT, IAbstractExtract.L_TYPE, EmployeeExtractType.P_TITLE}).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver));
        dataSource.addColumn(new SimpleColumn("Количество сотрудников", new String[]{IAbstractExtract.L_PARAGRAPH, EmployeeListParagraph.P_EXTRACT_COUNT}).setClickable(false).setOrderable(false).setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver));

        dataSource.addColumn(new SimpleColumn("Табельный номер", ListEmployeeExtract.employee().employeeCode().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("ФИО", ListEmployeeExtract.employeeFioModified().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Должность", new String[]{IAbstractExtract.L_ENTITY, EmployeePost.postRelation().postBoundedWithQGandQL().title().s()}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", new String[]{IAbstractExtract.L_ENTITY, EmployeePost.orgUnit().title().s()}).setClickable(false).setOrderable(false));

        if (!model.getOrder().isReadonly())
        {
            dataSource.addColumn(new ActionColumn("Редактировать параграф", ActionColumn.EDIT, "onClickEditParagraph").setPermissionKey(model.getSecModel().getPermission("editExtract")).setDisabledProperty(ListEmployeeExtract.P_NO_EDIT).setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver));
            dataSource.addColumn(new ActionColumn("Удалить параграф", ActionColumn.DELETE, "onClickDeleteParagraph", "Удалить параграф?").setPermissionKey(model.getSecModel().getPermission("deleteExtract")).setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver));
        }

        model.setDataSource(dataSource);
    }

    // Event Listeners

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendToCoordination(getModel(component), null);
        else
            getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
        rebuildListDataSource(component);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        T order = getModel(component).getOrder();
        int orderTypeIndex = order.getType().getIndex();

        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListOrderAddEditComponent(orderTypeIndex), new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())));
    }

    public void onClickAddParagraph(IBusinessComponent component)
    {
        Model model = getModel(component);
        List<EmployeeExtractType> extractTypeList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractTypeListByOrderType(model.getOrder().getType());

        //если у приказа несколько параграфов, то переходим на форму выбора типа параграфа,
        // если нет, то переходим сразу на форму добавления параграфа, передавая id единственного типа параграфа
        if (extractTypeList.size() == 1)
        {
            component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListParagraphAddEditComponent(extractTypeList.get(0).getIndex()), new ParametersMap()
                    .add(AbstractListParagraphAddEditModel.PARAMETER_ORDER_ID, model.getOrder().getId())
                    .add(AbstractListParagraphAddEditModel.PARAMETER_EXTRACT_TYPE_ID, extractTypeList.get(0).getId())
                    .add(AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_ID, null)));
        }
        else
        {
            component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListParagraphAddEditComponent(model.getOrder().getType().getIndex()), new ParametersMap()
                    .add(AbstractListParagraphAddEditModel.PARAMETER_ORDER_ID, model.getOrder().getId())));
        }
    }

    public void onClickAddMultipleExtract(IBusinessComponent component)
    { //TODO
        /*T order = getModel(component).getOrder();
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_LIST_MULTIPLE_EXTRACT_ADD, new ParametersMap()
                .add("orderId", order.getId())
        ));*/
    }

    public void onClickEditParagraph(IBusinessComponent component)
    {
        ListEmployeeExtract extract = getDao().getNotNull(ListEmployeeExtract.class, (Long) component.getListenerParameter());
        Model model = getModel(component);
//        EmployeeExtractType extractType = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractTypeByOrderType(model.getOrder().getType());
        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListParagraphAddEditComponent(extract.getType().getIndex()), new ParametersMap()
                .add(AbstractListParagraphAddEditModel.PARAMETER_ORDER_ID, model.getOrder().getId())
                .add(AbstractListParagraphAddEditModel.PARAMETER_EXTRACT_TYPE_ID, extract.getType().getId())
                .add(AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_ID, extract.getParagraph().getId())));
    }

    public void onClickReject(IBusinessComponent component)
    {
        getDao().doReject(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickCommit(IBusinessComponent component)
    {
        getDao().doCommit(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickRollback(IBusinessComponent component)
    {
        getDao().doRollback(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickDeleteOrder(IBusinessComponent component)
    {
        deactivate(component);
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(getModel(component).getOrder());
    }

    public void onClickPrintOrder(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_PARAGRAPH_LIST_ORDER_PRINT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())));
    }

    public void onClickDeleteParagraph(IBusinessComponent component)
    {
        ListEmployeeExtract extract = getDao().get(ListEmployeeExtract.class, component.<Long>getListenerParameter());
        MoveDaoFacade.getMoveDao().deleteParagraph(extract.getParagraph(), null);
        rebuildListDataSource(component);
    }
}