/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e27.AddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 06.03.2012
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<ScienceDegreePaymentExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        model.setUpdateOnChangePost("postAddFields, paymentBlock, freelance");

        if (model.isEditForm())
        {
            if (model.getExtract().getPost() != null && !model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSourceDetails.class, "b").column("b");
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSourceDetails.extract().fromAlias("b")), model.getExtract()));

                model.setStaffRateList(builder.createStatement(getSession()).<FinancingSourceDetails>list());
            }

            preparePaymentBlock(model);
        }

        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getEmployeePost());
        if (contract != null)
        {
            model.setLabourContractType(contract.getType().getTitle());
            model.setLabourContractNumber(contract.getNumber());
            model.setLabourContractDate(contract.getDate());
            model.setLabourContractBeginDate(contract.getBeginDate());
            model.setLabourContractEndDate(contract.getEndDate());
        }

        model.setScienceDegreeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "b").column("b");
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonAcademicDegree.person().fromAlias("b")), model.getEmployeePost().getPerson()));
                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(PersonAcademicDegree.academicDegree().title().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(PersonAcademicDegree.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(PersonAcademicDegree.academicDegree().title().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                PersonAcademicDegree degree = (PersonAcademicDegree) value;

                StringBuilder resultBuilder = new StringBuilder()
                        .append(degree.getAcademicDegree().getTitle());

                if (degree.getDate() != null)
                    resultBuilder
                            .append("; дата присуждения ")
                            .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(degree.getDate()));

                return resultBuilder.toString();
            }
        });

        model.setPostModel(new CommonSingleSelectModel(PostBoundedWithQGandQL.fullTitle())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnitPostRelation.class, "b").column(DQLExpressions.property(OrgUnitPostRelation.orgUnitTypePostRelation().postBoundedWithQGandQL().fromAlias("b")));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(OrgUnitPostRelation.orgUnit().fromAlias("b")), model.getEmployeePost().getOrgUnit()));
                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(OrgUnitPostRelation.orgUnitTypePostRelation().postBoundedWithQGandQL().title().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(OrgUnitPostRelation.orgUnitTypePostRelation().postBoundedWithQGandQL().id().fromAlias("b")), o));
                builder.predicate(DQLPredicateType.distinct);
                builder.order(DQLExpressions.property(OrgUnitPostRelation.orgUnitTypePostRelation().postBoundedWithQGandQL().title().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }
        });

        model.setFinancingSourceModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getPost() == null || model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
                    return new SimpleListResultBuilder<>(new ArrayList<>());


                if (model.isThereAnyActiveStaffList())
                {
                    StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEmployeePost().getOrgUnit());

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListItem.class, "sli").column(DQLExpressions.property(StaffListItem.financingSource().fromAlias("sli")));
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListItem.staffList().fromAlias("sli")), activeStaffList));
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().fromAlias("sli")), model.getExtract().getPost()));
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(StaffListItem.financingSource().title().fromAlias("sli"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                    builder.predicate(DQLPredicateType.distinct);
                    if (o != null)
                        builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListItem.financingSource().id().fromAlias("sli")), o));

                    return new DQLListResultBuilder(builder);
                }
                else
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSource.class, "fs").column("fs");
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(FinancingSource.title().fromAlias("fs"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                    if (o != null)
                        builder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSource.id().fromAlias("fs")), o));

                    return new DQLListResultBuilder(builder);
                }
            }
        });

        model.setFinancingSourceItemModel(new CommonSingleSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getPost() == null || model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                FinancingSource financingSource = finSrcMap.get(id);

                if (financingSource == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());


                if (model.isThereAnyActiveStaffList())
                {
                    StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEmployeePost().getOrgUnit());

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListItem.class, "sl").column(DQLExpressions.property(StaffListItem.financingSourceItem().fromAlias("sl")));
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListItem.staffList().fromAlias("sl")), staffList));
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().fromAlias("sl")), model.getExtract().getPost()));
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListItem.financingSource().fromAlias("sl")), financingSource));
                    builder.where(DQLExpressions.isNotNull(DQLExpressions.property(StaffListItem.financingSourceItem().fromAlias("sl"))));
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(StaffListItem.financingSourceItem().title().fromAlias("sl"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                    if (o != null)
                        builder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListItem.financingSourceItem().id().fromAlias("sl")), o));
                    builder.predicate(DQLPredicateType.distinct);

                    return new DQLListResultBuilder(builder);
                }
                else
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSourceItem.class, "fsi").column("fsi");
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSourceItem.financingSource().id().fromAlias("fsi")), financingSource.getId()));
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(FinancingSourceItem.title().fromAlias("fsi"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                    if (o != null)
                        builder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSourceItem.id().fromAlias("fsi")), o));

                    return new DQLListResultBuilder(builder);
                }
            }
        });

        model.setEmployeeHRModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> objects = findValues("").getObjects();
                List<IdentifiableWrapper> resultList = new ArrayList<>();

                for (IdentifiableWrapper wrapper : objects)
                {
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);
                }

                return resultList;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                if (model.getExtract().getPost() == null || model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
                    return ListResult.getEmpty();

                String newStaffRateStr = "<Новая ставка> - ";
                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
                final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

                FinancingSource financingSource = finSrcMap.get(id);
                FinancingSourceItem financingSourceItem = finSrcItmMap.get(id);

                if (financingSource == null)
                    return ListResult.getEmpty();

                Set<IdentifiableWrapper> resultList = new HashSet<>();
                List<StaffListAllocationItem> staffListAllocList = new ArrayList<>();

                StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEmployeePost().getOrgUnit());

                DQLSelectBuilder allocBuilder = new DQLSelectBuilder().fromEntity(StaffListAllocationItem.class, "a").column("a");
                allocBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListAllocationItem.staffListItem().staffList().fromAlias("a")), staffList));
                allocBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().fromAlias("a")), model.getExtract().getPost()));
                allocBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListAllocationItem.financingSource().fromAlias("a")), financingSource));
                allocBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListAllocationItem.financingSourceItem().fromAlias("a")), financingSourceItem));
                allocBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(StaffListAllocationItem.employeePost().postStatus().active().fromAlias("a")), false));

                staffListAllocList.addAll(allocBuilder.createStatement(getSession()).<StaffListAllocationItem>list());

//                if (allocationItemList.size() > 0)
//                    if (allocationItemList.get(0).getStaffListItem().getStaffRate() - allocationItemList.get(0).getStaffListItem().getOccStaffRate() <= 0)
//                        staffListAllocList = new LinkedList<StaffListAllocationItem>();

                if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(model.getEmployeePost().getOrgUnit(), model.getExtract().getPost(), financingSource, financingSourceItem))
                {
                    Double summStaffRate = 0.0d;
                    for (StaffListAllocationItem item : staffListAllocList)
                        summStaffRate += item.getStaffRate();

                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getEmployeePost().getOrgUnit(), model.getExtract().getPost(), financingSource, financingSourceItem);
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate() - summStaffRate;
                    if (diff > 0 && newStaffRateStr.contains(filter))
                        resultList.add(new IdentifiableWrapper(0L, newStaffRateStr + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                if (model.isEditForm())
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(FinancingSourceDetails.class, "srr").column("srr");
                    subBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSourceDetails.extract().fromAlias("srr")), model.getExtract()));
                    subBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSourceDetails.financingSource().fromAlias("srr")), financingSource));
                    subBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSourceDetails.financingSourceItem().fromAlias("srr")), financingSourceItem));

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSourceDetailsToAllocItem.class, "srar").column("srar");
                    builder.where(DQLExpressions.in(DQLExpressions.property(FinancingSourceDetailsToAllocItem.financingSourceDetails().fromAlias("srar")), subBuilder.createStatement(getSession()).list()));

                    for (FinancingSourceDetailsToAllocItem relation : builder.createStatement(getSession()).<FinancingSourceDetailsToAllocItem>list())
                        if (!relation.isHasNewAllocItem() && relation.getChoseStaffListAllocationItem() != null)
                            if (relation.getChoseStaffListAllocationItem().getStaffListItem().getStaffList().getOrgUnit().equals(model.getEmployeePost().getOrgUnit()))
                                if (relation.getChoseStaffListAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().equals(model.getExtract().getPost()))
                                    resultList.add(new IdentifiableWrapper(relation.getChoseStaffListAllocationItem().getId(), relation.getChoseStaffListAllocationItem().getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(relation.getChoseStaffListAllocationItem().getStaffRate()) + " - " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPostType().getShortTitle() + " " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPostStatus().getShortTitle()));
                }

//                if (model.getExtract().getCombinationPost() != null)
//                {
//                    MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "slai");
//                    builder.add(MQExpression.eq("slai", StaffListAllocationItem.combination().s(), true));
//                    builder.add(MQExpression.eq("slai", StaffListAllocationItem.combinationPost().s(), model.getExtract().getCombinationPost()));
//                    builder.add(MQExpression.eq("slai", StaffListAllocationItem.financingSource().s(), financingSource));
//                    builder.add(MQExpression.eq("slai", StaffListAllocationItem.financingSourceItem().s(), financingSourceItem));
//
//                    for (StaffListAllocationItem item : builder.<StaffListAllocationItem>getResultList(getSession()))
//                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
//                }

                for (StaffListAllocationItem item : staffListAllocList)
                {
                    if (item.getEmployeePost().getPerson().getFullFio().contains(filter))
                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                return new ListResult<>(resultList);
            }
        });

        model.setRaisingCoefficientModel(new FullCheckSelectModel(SalaryRaisingCoefficient.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getExtract().getPost() == null || model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
                    return ListResult.getEmpty();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SalaryRaisingCoefficient.class, "rc").column("rc");
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(SalaryRaisingCoefficient.post().fromAlias("rc")), model.getExtract().getPost().getPost()));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(SalaryRaisingCoefficient.qualificationLevel().fromAlias("rc")), model.getExtract().getPost().getQualificationLevel()));
                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(SalaryRaisingCoefficient.title().fromAlias("rc"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                builder.order(DQLExpressions.property(SalaryRaisingCoefficient.raisingCoefficient().fromAlias("rc")));
                builder.order(DQLExpressions.property(SalaryRaisingCoefficient.title().fromAlias("rc")));
                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });

        model.setEtksLevelModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EtksLevels.class, "l").column("l");
                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EtksLevels.title().fromAlias("l"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                builder.order(DQLExpressions.property(EtksLevels.code().fromAlias("l")));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(EtksLevels.id().fromAlias("l")), o));

                List<EtksLevels> list = builder.createStatement(getSession()).list();
                Collections.sort(list, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);

                return new SimpleListResultBuilder<>(list);
            }
        });

        model.setPaymentModel(CommonEmployeeExtractUtil.getPaymentModelForPaymentBlock());
        model.setFinSrcPaymentModel(CommonEmployeeExtractUtil.getFinSrcModelForPaymentBlock());
        model.setFinSrcItemPaymentModel(CommonEmployeeExtractUtil.getFinSrcItemModelForPaymentBlock(model.getPaymentDataSource()));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (model.getExtract().getPost() != null && !model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
        {
            CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateList());

            CommonEmployeeExtractUtil.validateStaffRate(model.getStaffRateDataSource(), model.getStaffRateList(), null, errors);
        }

        CommonEmployeeExtractUtil.validateForPaymentBlock(model.getPaymentList(), errors);

        if (!model.getPaymentList().isEmpty())
            if (model.getExtract().getBeginPayDate() == null)
                errors.add("Поле «Дата начала» обязательно для заполнения.", "beginPayDate");

        if (model.getExtract().getEndPayDate() != null && model.getExtract().getBeginPayDate().getTime() > model.getExtract().getEndPayDate().getTime())
            errors.add("Дата начала должна быть меньше даты окончания.", "begiPayDate", "endPayDate");

        //проверка на непересечение дат одинковых выплат
        EmployeeBonus beforePayment = null;
        for (EmployeeBonus payment : model.getPaymentList())
        {
            if (beforePayment == null)
            {
                beforePayment = payment;
                continue;
            }

            if (beforePayment.getPayment().getCode().equals(payment.getPayment().getCode()))
            {
                Date begin1 = payment.getBeginDate();
                Date end1 = payment.getEndDate();
                Date begin2 = beforePayment.getBeginDate();
                Date end2 = beforePayment.getEndDate();

                if (end2 == null || end1 == null)
                    continue;

                if (CommonBaseDateUtil.isBetween(begin1, begin2, end2) || CommonBaseDateUtil.isBetween(end1, begin2, end2))
                    errors.add("Периоды «Назначена с даты» - «Назначена по дату» для одинаковых выплат не должны пересекаться.",
                            "beginDate_Id_" + payment.getId(), "endDate_Id_" + payment.getId(), "beginDate_Id_" + beforePayment.getId(), "endDate_Id_" + beforePayment.getId());
                else {
                    if (CommonBaseDateUtil.isBetween(begin2, begin1, end1) || CommonBaseDateUtil.isBetween(end2, begin1, end1))
                        errors.add("Периоды «Назначена с даты» - «Назначена по дату» для одинаковых выплат не должны пересекаться.",
                                "beginDate_Id_" + payment.getId(), "endDate_Id_" + payment.getId(), "beginDate_Id_" + beforePayment.getId(), "endDate_Id_" + beforePayment.getId());
                }
            }

            beforePayment = payment;
        }

        //делаем проверки ставок ШР
        if (model.isThereAnyActiveStaffList() && !errors.hasErrors() && model.getExtract().getPost() != null && !model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
        {
//            final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
//            final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());
//            final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
//            final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());
//            final IValueMapHolder finSrcItemHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
//            final Map<Long, FinancingSourceItem> finSrcItemMap = (null == finSrcItemHolder ? Collections.emptyMap() : finSrcItemHolder.getValueMap());
            final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
            final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());

            //если была доступна колонка Кадровой расстановки, достаем из враперов объекты Штатной расстановки
            final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
            //Мапа, указывающая на то выбрана ли строчка <Новая ставка> в мультиселекте
            Map<Long, Boolean> hasNewStaffRateMap = new HashMap<>();
            if (model.isThereAnyActiveStaffList())
            {
                for (IEntity entity : model.getStaffRateList())
                {
                    List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                    for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
                    {
                        if (wrapper.getId() != 0L)
                        {
                            StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                            allocationItemList.add(item);
                        }

                    }

                    allocItemsMap.put(entity.getId(), allocationItemList);

                    hasNewStaffRateMap.put(entity.getId(), empHRMap.get(entity.getId()).contains(new IdentifiableWrapper(0L, "")));
                }
            }

            for (FinancingSourceDetails staffRateItem : model.getStaffRateList())
            {
                PostBoundedWithQGandQL post = model.getExtract().getPost() != null ? model.getExtract().getPost() : model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getEmployeePost().getOrgUnit(), post, staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());
                Double diff = 0.0d;
                if (staffListItem != null)
                    diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
                BigDecimal x = new BigDecimal(diff);
                x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
                diff = x.doubleValue();

                for (StaffListAllocationItem allocationItem : UniempDaoFacade.getStaffListDAO().getStaffListItemAllocations(staffListItem))
                    if (allocationItem.getEmployeePost() != null && !allocationItem.getEmployeePost().getPostStatus().isActive() &&
                            !allocItemsMap.get(staffRateItem.getId()).contains(allocationItem))
                        diff -= allocationItem.getStaffRate();

                Double sumStaffRateAllocItems = 0.0d;
                for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
                {
                    sumStaffRateAllocItems += allocItem.getStaffRate();

//                    if (allocItem.getEmployeePost().equals(model.getEmployeePost())
//                            && allocItem.getEmployeePost().getPostStatus().isActive())
//                        diff += allocItem.getStaffRate();
                }

                if ((staffRateItem.getStaffRate() > sumStaffRateAllocItems && !hasNewStaffRateMap.get(staffRateItem.getId())) ||
                        (staffRateItem.getStaffRate() > diff + sumStaffRateAllocItems && hasNewStaffRateMap.get(staffRateItem.getId())))
                    errors.add("Размер ставки " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() +
                            " превышает суммарную ставку, выбранную в поле «Кадровая расстановка».",
                            "staffRate_id_" + staffRateItem.getId());

                if (staffRateItem.getStaffRate() <= sumStaffRateAllocItems && hasNewStaffRateMap.get(staffRateItem.getId()))
                    errors.add("Необходимо точнее указать занимаемые ставки в поле «Кадровая расстановка» для "
                            + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() + ".",
                            "employeeHR_Id_" + staffRateItem.getId());
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());

        super.update(model);

        //если редактируем приказ, то удаляем созданные ранее релейшены и доли ставки, что бы не возникло ошибок уникальности набору полей
        if (model.isEditForm())
        {
            List<FinancingSourceDetails> staffRateList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(model.getExtract());
            List<FinancingSourceDetailsToAllocItem> relList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(staffRateList);

            for (FinancingSourceDetailsToAllocItem rel : relList)
                delete(rel);

            for (FinancingSourceDetails item : staffRateList)
                delete(item);

            getSession().flush();
        }

        //сохраняем доли ставки
        if (model.getExtract().getPost() != null && !model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
        {
            final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
            final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());
            //достаем из враперов объекты Штатной расстановки
            final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
            //если была доступна колонка Кадровой расстановки, то заполняем мапы
            if (model.isThereAnyActiveStaffList())
            {
                for (IEntity entity : model.getStaffRateList())
                {
                    List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                    for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
                    {
                        if (wrapper.getId() != 0L)
                        {
                            StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                            allocationItemList.add(item);
                        }
                        else
                        {
                            StaffListAllocationItem allocationItem = new StaffListAllocationItem();
                            allocationItem.setId(0L);
                            allocationItemList.add(allocationItem);
                        }
                    }

                    allocItemsMap.put(entity.getId(), allocationItemList);
                }
            }

            CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateList());


            for (FinancingSourceDetails item : model.getStaffRateList())
            {
                Long id = item.getId();//т.к. после save id поменяется, мы его сохраняем что бы достать из мапы соответствующий ставке набор allocItem

                save(item);

                if (model.isThereAnyActiveStaffList())
                    for (StaffListAllocationItem allocationItem : allocItemsMap.get(id))
                    {
                        FinancingSourceDetailsToAllocItem relation = new FinancingSourceDetailsToAllocItem();
                        if (allocationItem.getId() != 0L)
                        {
                            relation.setChoseStaffListAllocationItem(allocationItem);
                            relation.setHasNewAllocItem(false);
                            relation.setEmployeePostHistory(allocationItem.getEmployeePost());
                            relation.setStaffRateHistory(allocationItem.getStaffRate());
                        }
                        else
                            relation.setHasNewAllocItem(true);
                        relation.setHasBeenSelected(false);
                        relation.setFinancingSourceDetails(item);

                        save(relation);
                    }
            }

        }
        else//если должность не менялась, то и поле Сумма оплаты не появлялось, поэтому заполняем его по умолчанию данными с Cотруника
            model.getExtract().setSalary(model.getEmployeePost().getSalary() == null ? 0 : model.getEmployeePost().getSalary());


        //PAYMENTS
        //если редактируем приказ, то удаляем созданные ранее выплаты в выписке
        if (model.isEditForm())
        {
            for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getSavedPaymentsFromPaymentBlockDQLBuilder(model.getExtract()).createStatement(getSession()).<EmployeeBonus>list())
                delete(bonus);
        }

        //сохраняем выплаты
        for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getPaymentsForSaveFromPaymentBlock(model.getPaymentList()))
            save(bonus);
    }

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected ScienceDegreePaymentExtract createNewInstance()
    {
        return new ScienceDegreePaymentExtract();
    }

    protected void preparePaymentBlock(Model model)
    {
        DQLSelectBuilder payBuilder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "b").column("b");
        payBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeBonus.extract().fromAlias("b")), model.getExtract()));

        model.setPaymentList(payBuilder.createStatement(getSession()).<EmployeeBonus>list());

        PostBoundedWithQGandQL post = model.getExtract().getPost() != null ? model.getExtract().getPost() : model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

        List staffRateItemList;
        if (model.getExtract().getPost() != null && !model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()))
            staffRateItemList = model.getStaffRateList();
        else
            staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());

        List<StaffListPostPayment> paymentList = CommonEmployeeExtractUtil.checkEnabledAddStaffListPaymentButton(post, model.getEmployeePost().getOrgUnit(), model.getPaymentList(), staffRateItemList);

        if (!paymentList.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);
    }

    @Override
    public Map<FinancingSourceDetails, List<FinancingSourceDetailsToAllocItem>> getStaffRateAllocItemMap(Model model)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSourceDetailsToAllocItem.class, "b").column("b");
        builder.where(DQLExpressions.in(DQLExpressions.property(FinancingSourceDetailsToAllocItem.financingSourceDetails().fromAlias("b")), model.getStaffRateList()));
        List<FinancingSourceDetailsToAllocItem> relationList = builder.createStatement(getSession()).list();

        Map<FinancingSourceDetails, List<FinancingSourceDetailsToAllocItem>> resultMap = new HashMap<>();
        for (FinancingSourceDetails relation : model.getStaffRateList())
        {
            List<FinancingSourceDetailsToAllocItem> allocItemRelationList = resultMap.get(relation);
            if (allocItemRelationList == null)
                allocItemRelationList = new ArrayList<>();

            for (FinancingSourceDetailsToAllocItem allocItemRelation : relationList)
            {
                if (allocItemRelation.getFinancingSourceDetails().equals(relation))
                    allocItemRelationList.add(allocItemRelation);

                resultMap.put(relation, allocItemRelationList);
            }
        }

        return resultMap;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareStaffRateDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getStaffRateList());
    }

    @Override
    public void prepareEmployeeStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getEmployeeStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getEmployeeStaffRateDataSource(), staffRateItems);
    }

    @Override
    public void preparePaymentDataSource(Model model)
    {
        CommonEmployeeExtractUtil.preparePaymentDataSourceForPaymentBlock(model.getPaymentList(), model.getPaymentDataSource());
    }
}