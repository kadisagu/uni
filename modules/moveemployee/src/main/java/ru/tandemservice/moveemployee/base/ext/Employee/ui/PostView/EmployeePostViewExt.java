/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.moveemployee.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.block.BlockListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "moveemployee" + EmployeePostViewExtUI.class.getSimpleName();

    @Autowired
    private EmployeePostView _employeePostView;

    public static final String EMPLOYEE_POST_ORDER_TAB = "employeePostOrderTab";
    public static final String EMPLOYEE_POST_ORDER_PUB = "employeePostOrderPub";

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_employeePostView.employeePostPubTabPanelExtPoint())
                .addTab(componentTab(EMPLOYEE_POST_ORDER_TAB, "ru.tandemservice.moveemployee.component.employee.EmployeePostOrdersPub").permissionKey("viewTabEmployeePostOrders").parameters("mvel:['" + ISecureRoleContext.SECURE_ROLE_CONTEXT + "':presenter.secureRoleContext]"))
                .create();
    }

    @Bean
    public BlockListExtension blockListExtension()
    {
        return blockListExtensionBuilder(_employeePostView.employeePostPubBlockListExtPoint())
                .addBlock(htmlBlock(EMPLOYEE_POST_ORDER_PUB, "EmployeePostOrderPub"))
                .create();
    }
}
