/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e4.ExtractAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.ui.formatters.EmployeeCodeFormatter;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit.AbstractListExtractAddEditController;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author ListExtractComponentGenerator
 * @since 04.10.2011
 */
public class Controller extends AbstractListExtractAddEditController<EmployeeHolidayEmplListExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        onChangeHolidayType(component);
        prepareStaffRateDataSource(component);
        prepareVacationScheduleDataSource(component);
    }

    public void onChangeHolidayType(IBusinessComponent component)
    {
        Model model = getModel(component);
        HolidayType holidayType = model.getExtract().getHolidayType();
        if (null != holidayType && UniempDefines.HOLIDAY_TYPE_ANNUAL.equals(holidayType.getCode()))
        {
            model.setNeedMainHoliday(true);
            model.setNeedSecondHoliday(false);
            model.getExtract().setSecondHolidayBeginDate(null);
            model.getExtract().setSecondHolidayEndDate(null);
            model.getExtract().setSecondHolidayDuration(null);
        }
        else
        {
            if (model.getExtract().getVacationScheduleItem() != null)
                model.setNeedMainHoliday(true);
            else
                model.setNeedMainHoliday(false);
            model.setNeedSecondHoliday(null != holidayType);
        }

        if (null != holidayType)
        {
            if (UniempDefines.HOLIDAY_TYPE_ANNUAL.equals(holidayType.getCode()))
                model.getExtract().setEmployeePostStatusNew(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
            if (UniempDefines.HOLIDAY_TYPE_WITHOUT_SALARY.equals(holidayType.getCode()) || UniempDefines.HOLIDAY_TYPE_ENTRANCE_EXAMS_WITHOUT_SALARY.equals(holidayType.getCode()))
                model.getExtract().setEmployeePostStatusNew(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR));
            else if (UniempDefines.HOLIDAY_TYPE_PREGNANCY_AND_CHILD_BORN.equals(holidayType.getCode()))
                model.getExtract().setEmployeePostStatusNew(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY));
            else if (UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_1_5.equals(holidayType.getCode()) || UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_3.equals(holidayType.getCode()))
                model.getExtract().setEmployeePostStatusNew(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE));
            else
                model.getExtract().setEmployeePostStatusNew(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        }
        else
        {
            model.getExtract().setEmployeePostStatusNew(model.getPostStatusesMap().get(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        }

        onChangeDateOrDuration(component);
    }

    public void onChangeVacationScheduleItem(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeHolidayEmplListExtract extract = model.getExtract();

        if (extract.getVacationScheduleItem() == null)
        {
            model.setNeedMainHoliday(false);
            return;
        }

        model.setNeedMainHoliday(true);

        if (extract.getMainHolidayBeginDate() == null && extract.getMainHolidayDuration() == null)
        {
            extract.setMainHolidayBeginDate(extract.getVacationScheduleItem().getFactDate() != null ? extract.getVacationScheduleItem().getFactDate() : extract.getVacationScheduleItem().getPlanDate());
            extract.setMainHolidayDuration(extract.getVacationScheduleItem().getDaysAmount());

            component.setListenerParameter("mainDuration");
            onChangeDateOrDuration(component);
        }
    }

    public void onChangeDateOrDuration(IBusinessComponent component)
    {
        Model model = getModel(component);
        EmployeeHolidayEmplListExtract extract = model.getExtract();
        if (extract.getEmployeePost() != null && extract.getHolidayType() != null)
        {
            Date beginDate;
            Date endDate;
            Integer duration;

            if (component.getListenerParameter() != null && ((String) component.getListenerParameter()).contains("main"))
            {
                beginDate = extract.getMainHolidayBeginDate();
                endDate = extract.getMainHolidayEndDate();
                duration = extract.getMainHolidayDuration();
            }
            else
            {
                beginDate = extract.getSecondHolidayBeginDate();
                endDate = extract.getSecondHolidayEndDate();
                duration = extract.getSecondHolidayDuration();
            }



            if (component.getListenerParameter() != null && ((String) component.getListenerParameter()).contains("Date"))
                if (duration == null && beginDate != null && endDate != null)
                {
                    Integer holidayDuration = EmployeeManager.instance().dao().getEmployeeHolidayDuration(extract.getEmployeePost().getWorkWeekDuration(), beginDate, endDate);
                    if (component.getListenerParameter() != null && ((String) component.getListenerParameter()).contains("main"))
                    {
                        if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                            extract.setMainHolidayDuration(holidayDuration);
                        else
                            extract.setMainHolidayDuration((int) CommonBaseDateUtil.getBetweenPeriod(beginDate, endDate, Calendar.DAY_OF_YEAR) + 1);
                    }
                    else
                    {
                        if (UniempUtil.getAnnualHolidayTypes().contains(extract.getHolidayType().getCode()) && extract.getHolidayType().isCompensable())
                            extract.setSecondHolidayDuration(holidayDuration);
                        else
                            extract.setSecondHolidayDuration((int) CommonBaseDateUtil.getBetweenPeriod(beginDate, endDate, Calendar.DAY_OF_YEAR) + 1);
                    }
                }

            if (component.getListenerParameter() != null && ((String) component.getListenerParameter()).contains("Duration"))
                if (endDate == null && beginDate != null && duration != null)
                {
                    GregorianCalendar date = (GregorianCalendar) GregorianCalendar.getInstance();
                    EmployeeWorkWeekDuration workWeekDuration = extract.getEmployeePost().getWorkWeekDuration();
                    int counter = 0;
                    int dur = duration;

                    date.setTime(beginDate);

                    if (dur > 0)
                    {
                        do
                        {
                            if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                                counter++;

                            if (counter != dur)
                                date.add(Calendar.DATE, 1);
                        }
                        while (counter < dur);

                        if (component.getListenerParameter() != null && ((String) component.getListenerParameter()).contains("main"))
                        {
                            if (IUniBaseDao.instance.get().getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL).isCompensable())
                                extract.setMainHolidayEndDate(date.getTime());
                            else
                            {
                                date.setTime(beginDate);
                                date.add(Calendar.DATE, duration - 1);
                                extract.setMainHolidayEndDate(date.getTime());
                            }
                        }
                        else
                        {
                            if (UniempUtil.getAnnualHolidayTypes().contains(extract.getHolidayType().getCode()) && extract.getHolidayType().isCompensable())
                                extract.setSecondHolidayEndDate(date.getTime());
                            else
                            {
                                date.setTime(beginDate);
                                date.add(Calendar.DATE, duration - 1);
                                extract.setSecondHolidayEndDate(date.getTime());
                            }
                        }
                    }
                }

        }
        int fullDuration = (extract.getMainHolidayDuration() != null ? extract.getMainHolidayDuration() : 0) +
                (extract.getSecondHolidayDuration() != null ? extract.getSecondHolidayDuration() : 0);

        extract.setBeginDate(CommonBaseDateUtil.getMinDate(extract.getMainHolidayBeginDate(), extract.getMainHolidayEndDate(), extract.getSecondHolidayBeginDate(), extract.getSecondHolidayEndDate()));
        extract.setEndDate(CommonBaseDateUtil.getMaxDate(extract.getMainHolidayBeginDate(), extract.getMainHolidayEndDate(), extract.getSecondHolidayBeginDate(), extract.getSecondHolidayEndDate()));
        extract.setDuration(fullDuration);
    }

    public void onChangeEmployeePost(IBusinessComponent component)
    {
        Model model = getModel(component);

        //нужно ли отображать поля связанные с Графиком отпусков
        if (getDao().hasVacationSchedule(model.getExtract().getEmployeePost()))
            model.setHasVacationSchedule(true);
        else
            model.setHasVacationSchedule(false);

        model.getStaffRateDataSource().refresh();
    }

    private void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()).setOrderable(false));

        model.setStaffRateDataSource(dataSource);
    }

    protected void prepareVacationScheduleDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getVacationScheduleDataSource() != null)
            return;

        DynamicListDataSource<VacationScheduleItem> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareVacationScheduleDataSource(model);
        });

        dataSource.addColumn(new CheckboxColumn("selected", "Выбор", false));
        dataSource.addColumn(new SimpleColumn("Подразделение", VacationScheduleItem.employeePost().orgUnit().fullTitle().s()));
        dataSource.addColumn(new SimpleColumn("Должность", VacationScheduleItem.employeePost().postRelation().postBoundedWithQGandQL().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ФИО сотрудника", VacationScheduleItem.employeePost().employee().person().fullFio().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Табельный номер", VacationScheduleItem.employeePost().employee().employeeCode().s(), new EmployeeCodeFormatter()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во дней", VacationScheduleItem.daysAmount().s()).setClickable(false));
        dataSource.addColumn(new BlockColumn("periodBeginDayColumn", "Дата начала периода").setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn("periodEndDayColumn", "Дата окончания периода").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("План. дата начала", VacationScheduleItem.planDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Факт. дата начала", VacationScheduleItem.factDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Основание переноса отпуска", VacationScheduleItem.postponeBasic().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата переноса отпуска", VacationScheduleItem.postponeDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));

        model.setVacationScheduleDataSource(dataSource);
    }

    public void onChangeSelects(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (component.getListenerParameter().equals("orgUnit"))
            model.setVacationSchedule(null);
        model.getVacationScheduleDataSource().refresh();
    }

    @Override
    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getAddFormat().getId().equals(1L))
            super.onClickApply(component);
        else if (model.getAddFormat().getId().equals(2L))
        {
            ErrorCollector errors = component.getUserContext().getErrorCollector();

            getDao().validate(model, errors);
            if (errors.hasErrors()) return;

            getDao().update(model);
            deactivate(component);
        }
    }
}
