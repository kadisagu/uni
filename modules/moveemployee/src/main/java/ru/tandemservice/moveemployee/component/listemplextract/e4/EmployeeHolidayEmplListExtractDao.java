/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e4;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 04.10.2011
 */
public class EmployeeHolidayEmplListExtractDao extends UniBaseDao implements IExtractComponentDao<EmployeeHolidayEmplListExtract>
{
    @Override
    public void doCommit(EmployeeHolidayEmplListExtract extract, Map parameters)
    {
        extract.getEmployeePost().setPostStatus(extract.getEmployeePostStatusNew());

        MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
        empHolidayBuilder.add(MQExpression.eq("eh", EmployeeHoliday.L_EMPLOYEE_POST, extract.getEmployeePost()));
        List<EmployeeHoliday> employeeHolidayList = empHolidayBuilder.getResultList(getSession());

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        if (extract.getMainHolidayBeginDate() != null && extract.getMainHolidayEndDate() != null && extract.getMainHolidayDuration() != null)
        {
            for (EmployeeHoliday holiday : employeeHolidayList)
            {
                Date beginDate = extract.getMainHolidayBeginDate();
                Date endDate = extract.getMainHolidayEndDate();

                if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                    errorCollector.add("Ежегодный отпуск сотрудника " + CommonExtractUtil.getModifiedFioInitials(extract.getEmployeePost().getPerson(), GrammaCase.GENITIVE) + " пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".");
                else {
                    if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                        errorCollector.add("Ежегодный отпуск сотрудника " + CommonExtractUtil.getModifiedFioInitials(extract.getEmployeePost().getPerson(), GrammaCase.GENITIVE) + " пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".");
                }
            }

            if (errorCollector.hasErrors())
                return;

            HolidayType holidayType = getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL);

            EmployeeHoliday mainHoliday = new EmployeeHoliday();
            mainHoliday.setTitle(holidayType.getTitle());
            mainHoliday.setHolidayType(holidayType);
            mainHoliday.setEmployeePost(extract.getEmployeePost());
            mainHoliday.setBeginDate(extract.getPeriodBeginDate());
            mainHoliday.setEndDate(extract.getPeriodEndDate());
            mainHoliday.setStartDate(extract.getMainHolidayBeginDate());
            mainHoliday.setFinishDate(extract.getMainHolidayEndDate());
            mainHoliday.setDuration(extract.getMainHolidayDuration());
            mainHoliday.setHolidayExtract(extract);
            mainHoliday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
            mainHoliday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

            save(mainHoliday);

            extract.setEmployeeFirstHoliday(mainHoliday);

            if (extract.getVacationScheduleItem() != null)
            {
                extract.setOldScheduleItemFactDate(extract.getVacationScheduleItem().getFactDate());
                extract.getVacationScheduleItem().setFactDate(mainHoliday.getStartDate());
                if (extract.getVacationScheduleItem().getPlanDate() != null && !extract.getVacationScheduleItem().getPlanDate().equals(mainHoliday.getStartDate()))
                {
                    StringBuilder basicBuilder = new StringBuilder().
                            append("Приказ №").
                            append(extract.getParagraph().getOrder().getNumber()).
                            append(" от ").
                            append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));
                    if (extract.getVacationScheduleItem().getPostponeBasic() != null)
                    {
                        extract.setOldScheduleItemPostponeBasic(extract.getVacationScheduleItem().getPostponeBasic());
                        extract.getVacationScheduleItem().setPostponeBasic(extract.getVacationScheduleItem().getPostponeBasic() + "; " + basicBuilder.toString());
                    }
                    else
                    {
                        extract.setOldScheduleItemPostponeBasic(extract.getVacationScheduleItem().getPostponeBasic());
                        extract.setOldScheduleItemPostponeDate(extract.getVacationScheduleItem().getPostponeDate());
                        extract.getVacationScheduleItem().setPostponeBasic(basicBuilder.toString());
                        extract.getVacationScheduleItem().setPostponeDate(extract.getParagraph().getOrder().getCommitDate());
                    }
                }
            }
        }

        if (!extract.getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL))
        {
            for (EmployeeHoliday holiday : employeeHolidayList)
            {
                Date beginDate = extract.getSecondHolidayBeginDate();
                Date endDate = extract.getSecondHolidayEndDate();

                if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                    errorCollector.add("Отпуск " + extract.getHolidayType().getTitle() + " сотрудника " + CommonExtractUtil.getModifiedFioInitials(extract.getEmployeePost().getPerson(), GrammaCase.GENITIVE) +
                            " пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".");
                else {
                    if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                        errorCollector.add("Отпуск " + extract.getHolidayType().getTitle() + " сотрудника " + CommonExtractUtil.getModifiedFioInitials(extract.getEmployeePost().getPerson(), GrammaCase.GENITIVE) +
                                " пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".");
                }
            }

            if (errorCollector.hasErrors())
                return;

            EmployeeHoliday secondHoliday = new EmployeeHoliday();
            secondHoliday.setTitle(extract.getHolidayType().getTitle());
            secondHoliday.setHolidayType(extract.getHolidayType());
            secondHoliday.setEmployeePost(extract.getEmployeePost());
            secondHoliday.setBeginDate(extract.getPeriodBeginDate() != null ? extract.getPeriodBeginDate() : null);
            secondHoliday.setEndDate(extract.getPeriodEndDate() != null ? extract.getPeriodEndDate() : null);
            secondHoliday.setStartDate(extract.getSecondHolidayBeginDate());
            secondHoliday.setFinishDate(extract.getSecondHolidayEndDate());
            secondHoliday.setDuration(extract.getSecondHolidayDuration());
            secondHoliday.setHolidayExtract(extract);
            secondHoliday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
            secondHoliday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

            save(secondHoliday);

            extract.setEmployeeSecondHoliday(secondHoliday);
        }

        if (extract.getEntity() == null)
            extract.setEntity(extract.getEmployeePost());

        update(extract);
        update(extract.getEmployeePost());
    }

    @Override
    public void doRollback(EmployeeHolidayEmplListExtract extract, Map parameters)
    {
        if (extract.getEmployeeFirstHoliday() != null)
        {
            EmployeeHoliday employeeFirstHoliday = extract.getEmployeeFirstHoliday();
            extract.setEmployeeFirstHoliday(null);
            delete(employeeFirstHoliday);

            if (extract.getVacationScheduleItem() != null)
            {
                extract.getVacationScheduleItem().setFactDate(extract.getOldScheduleItemFactDate());
                extract.setOldScheduleItemFactDate(null);

                if (extract.getOldScheduleItemPostponeBasic() == null)
                {
                    extract.getVacationScheduleItem().setPostponeDate(extract.getOldScheduleItemPostponeDate());
                    extract.setOldScheduleItemPostponeDate(null);
                }

                extract.getVacationScheduleItem().setPostponeBasic(extract.getOldScheduleItemPostponeBasic());
                extract.setOldScheduleItemPostponeBasic(null);
            }
        }

        if (extract.getEmployeeSecondHoliday() != null)
        {
            EmployeeHoliday employeeSecondHoliday = extract.getEmployeeSecondHoliday();
            extract.setEmployeeSecondHoliday(null);
            delete(employeeSecondHoliday);
        }

        extract.getEmployeePost().setPostStatus(extract.getEmployeePostStatusOld());
        update(extract);
        update(extract.getEmployeePost());
    }
}
