/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.list.EmployeeListOrderAdd;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author dseleznev
 * Created on: 29.10.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        MQBuilder builder = new MQBuilder(EmployeeExtractType.ENTITY_CLASS, "t");
        builder.add(MQExpression.eq("t", EmployeeExtractType.L_PARENT + "." + EmployeeExtractType.P_CODE, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_LIST_ORDER_CODE));
        builder.add(MQExpression.eq("t", EmployeeExtractType.P_ACTIVE, Boolean.TRUE));

        model.setTypeList(builder.<EmployeeExtractType>getResultList(getSession()));
    }
}