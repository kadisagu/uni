/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeModularOrderParagraphsSortRuleSettings;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public interface IDAO extends IUniDao<Model>
{
    public void setActive(Long ruleId);
}