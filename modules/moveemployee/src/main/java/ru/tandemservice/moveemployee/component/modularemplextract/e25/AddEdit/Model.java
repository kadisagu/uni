/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e25.AddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.RepealExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.01.2012
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<RepealExtract>
{
    private DynamicListDataSource _employeeStaffRateDataSource;

    private ISingleSelectModel _groupModel;
    private ISingleSelectModel _extractModel;

    //Getters & Setters

    public DynamicListDataSource getEmployeeStaffRateDataSource()
    {
        return _employeeStaffRateDataSource;
    }

    public void setEmployeeStaffRateDataSource(DynamicListDataSource employeeStaffRateDataSource)
    {
        _employeeStaffRateDataSource = employeeStaffRateDataSource;
    }

    public ISingleSelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISingleSelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public ISingleSelectModel getExtractModel()
    {
        return _extractModel;
    }

    public void setExtractModel(ISingleSelectModel extractModel)
    {
        _extractModel = extractModel;
    }
}