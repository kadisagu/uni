package ru.tandemservice.moveemployee.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.moveemployee.entity.gen.*;

/**
 * Используемые типы документов в настройке Форматы печати выплат сотрудникам
 */
public class PaymentStringFormatDocumentSettings extends PaymentStringFormatDocumentSettingsGen implements IHierarchyItem, ITitled
{
    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }
}