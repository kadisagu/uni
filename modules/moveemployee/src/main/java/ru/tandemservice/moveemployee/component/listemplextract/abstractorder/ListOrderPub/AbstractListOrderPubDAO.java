/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderPub;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
public abstract class AbstractListOrderPubDAO<T extends EmployeeListOrder, Model extends AbstractListOrderPubModel<T>> extends UniDao<Model> implements IAbstractListOrderPubDAO<T, Model>
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(Model model)
    {
        model.setOrder((T)getNotNull(model.getOrderId()));
        model.setSecModel(new CommonPostfixPermissionModel(model.getSecPostfix()));
        model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getOrder()));
        List<String> basicsTitle = new ArrayList<String>();
        for (EmplListOrderToBasicRelation rel : getList(EmplListOrderToBasicRelation.class, EmplListOrderToBasicRelation.L_ORDER, model.getOrder()))
            basicsTitle.add(rel.getBasic().getTitle());
        Collections.sort(basicsTitle);
        initActiveParagraphCount(model);
        model.setBasicsTitle(StringUtils.join(basicsTitle, ", "));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(ListEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER, model.getOrder()));
        builder.addOrder("e", IAbstractExtract.P_NUMBER);

        List<ListEmployeeExtract> list = builder.getResultList(getSession());
        model.getDataSource().setCountRow(list.size());
        UniBaseUtils.createPage(model.getDataSource(), list);
        wrap(model.getDataSource(), model.getOrder().getType(), getSession());
    }

    protected abstract void wrap(DynamicListDataSource dataSource, EmployeeExtractType orderType, Session session);

    private void initActiveParagraphCount(Model model)
    {
        MQBuilder c = new MQBuilder(EmployeeExtractType.ENTITY_CLASS, "t");
        c.add(MQExpression.and(MQExpression.eq("t", EmployeeExtractType.L_PARENT, model.getOrder().getType()),
                MQExpression.eq("t", EmployeeExtractType.P_ACTIVE, Boolean.TRUE)));
        model.setHaveActiveParagraph(c.getResultCount(getSession()) == 0);
    }

    @Override
    public EmployeeExtractType getExtractType(EmployeeListOrder order)
    {
        MQBuilder builder = new MQBuilder(EmployeeExtractType.ENTITY_CLASS, "t");
        builder.add(MQExpression.eq("t", EmployeeExtractType.L_PARENT, order.getType()));
        builder.add(MQExpression.eq("t", EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
        if (builder.getResultCount(getSession()) > 1)
            throw new IllegalStateException(); // Employee list order can include just one type of paragraph
        return (EmployeeExtractType)builder.uniqueResult(getSession());
    }

    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(model.getOrder().getNumber()))
            throw new ApplicationException("Нельзя отправить приказ на согласование без номера.");

        if (model.getOrder().getCommitDate() == null)
            throw new ApplicationException("Нельзя отправить приказ на согласование без даты приказа.");

        //2. надо сохранить печатную форму приказа
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveListOrderText(model.getOrder());

        //3. отправляем на согласование
        ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
        sendToCoordinationService.init(model.getOrder(), initiator);
        sendToCoordinationService.execute();
    }

    @Override
    public void doSendToFormative(Model model)
    {
        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на формирование, так как он на согласовании.");

        //2. надо сменить состояние на формируется
        model.getOrder().setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        update(model.getOrder());
    }

    @Override
    public void doReject(Model model)
    {
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        }
        else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(model.getOrder(), UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(model.getOrder());
            touchService.execute();
        }
    }

    @Override
    public void doCommit(Model model)
    {
        if (model.getOrder().getNumber() == null)
            throw new ApplicationException("Нельзя проводить приказ без номера.");

        if (model.getOrder().getCommitDate() == null)
            throw new ApplicationException("Нельзя проводить приказ без даты приказа.");

        //обновляем печатную форму
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveListOrderText(model.getOrder());

        MoveDaoFacade.getMoveDao().doCommitOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
    }

    @Override
    public void doRollback(Model model)
    {
        MoveDaoFacade.getMoveDao().doRollbackOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER), null);
    }
}