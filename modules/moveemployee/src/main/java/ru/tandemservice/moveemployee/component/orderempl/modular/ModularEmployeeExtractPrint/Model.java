/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.modular.ModularEmployeeExtractPrint;

import org.tandemframework.core.component.State;

import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;

/**
 * @author dseleznev
 * Created on: 02.12.2008
 */
@State(keys = "extractId", bindings = "extractId")
public class Model
{
    private Long _extractId;
    private AbstractEmployeeExtract _extract;
    private byte[] _data;

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public AbstractEmployeeExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(AbstractEmployeeExtract extract)
    {
        _extract = extract;
    }

    public byte[] getData()
    {
        return _data;
    }

    public void setData(byte[] data)
    {
        _data = data;
    }
}