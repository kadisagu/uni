/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentStringReprFormatAddEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;

import ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat;
import ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel;

/**
 * @author esych
 * Created on: 17.01.2011
 */
@Input({@org.tandemframework.core.component.Bind(key = "formatId", binding = "format.id")})
public class Model
{
    private PaymentStringRepresentationFormat _format = new PaymentStringRepresentationFormat();
    private List<HSelectOption> _extractTypeList;
    private List<HSelectOption> _paymentTypeList;
    private ISelectModel _paymentModel;
    private DynamicListDataSource<PaymentPrintFormatLabel> _labelDataSource;

    public PaymentStringRepresentationFormat getFormat()
    {
        return _format;
    }

    public void setFormat(PaymentStringRepresentationFormat value)
    {
        _format = value;
    }

    public List<HSelectOption> getExtractTypeList()
    {
        return _extractTypeList;
    }

    public void setExtractTypeList(List<HSelectOption> value)
    {
        _extractTypeList = value;
    }

    public List<HSelectOption> getPaymentTypeList()
    {
        return _paymentTypeList;
    }

    public void setPaymentTypeList(List<HSelectOption> value)
    {
        _paymentTypeList = value;
    }

    public ISelectModel getPaymentModel()
    {
        return _paymentModel;
    }

    public void setPaymentModel(ISelectModel value)
    {
        _paymentModel = value;
    }

    public DynamicListDataSource<PaymentPrintFormatLabel> getLabelDataSource()
    {
        return _labelDataSource;
    }

    public void setLabelDataSource(DynamicListDataSource<PaymentPrintFormatLabel> value)
    {
        _labelDataSource = value;
    }
}
