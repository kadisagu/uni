/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e1.Pub;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.IModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeAddExtract;

/**
 * @author dseleznev
 * Created on: 20.11.2008
 */
public interface IDAO extends IModularEmployeeExtractPubDAO<EmployeeAddExtract, Model>
{
}