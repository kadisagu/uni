/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractAddEdit.SingleEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public abstract class CommonSingleEmployeeExtractAddEditController<T extends SingleEmployeeExtract, IDAO extends ICommonSingleEmployeeExtractAddEditDAO<T, Model>, Model extends CommonSingleEmployeeExtractAddEditModel<T>> extends SingleEmployeeExtractAddEditController<T, IDAO, Model>
{
    public void onChangeReason(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepareTextParagraph(model);
    }

    public void onChangeBasics(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeLabourContract contract = getDao().get(EmployeeLabourContract.class, EmployeeLabourContract.employeePost().s(), model.getEmployeePost());

        if (contract == null)
            return;

        for (EmployeeOrderBasics basic : model.getSelectedBasicList())
        {
            if (basic.isLaborContract() && model.getCurrentBasicMap().get(basic.getId()) == null)
            {
                StringBuilder basicBuilder = new StringBuilder("№").append(contract.getNumber()).append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getDate()));
                model.getCurrentBasicMap().put(basic.getId(), basicBuilder.toString());
            }
        }
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        super.onRefreshComponent(component);
        model.setDisableCommitDate(!CoreServices.securityService().check(model.getOrder(), component.getUserContext().getPrincipalContext(), "editCommitDate_employeeSingleOrder"));
        model.setDisableOrderNumber(!CoreServices.securityService().check(model.getOrder(), component.getUserContext().getPrincipalContext(), "editOrderNumber_employeeSingleOrder"));
    }
}