/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e27.Pub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubController;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 06.03.2012
 */
public class Controller extends ModularEmployeeExtractPubController<ScienceDegreePaymentExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        preparePaymentDataSource(component);
    }

    public void preparePaymentDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getPaymentDataSource() != null)
            return;

        DynamicListDataSource<EmployeeBonus> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().preparePaymentDataSource(model);
        }, 5);

        dataSource.addColumn(new SimpleColumn("Дата назначения", EmployeeBonus.assignDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Назначена с даты", EmployeeBonus.beginDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Назначена по дату", EmployeeBonus.endDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Выплата", EmployeeBonus.payment().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Размер выплаты", EmployeeBonus.value(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формат ввода", EmployeeBonus.payment().paymentUnit().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeeBonus.financingSource().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeeBonus.financingSourceItem().title()).setClickable(false).setOrderable(false));

        model.setPaymentDataSource(dataSource);
    }
}