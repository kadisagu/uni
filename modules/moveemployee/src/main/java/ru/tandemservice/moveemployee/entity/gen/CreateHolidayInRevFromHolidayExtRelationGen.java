package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.CreateHolidayInRevFromHolidayExtRelation;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь созданных отпусков в выписке «Об отзыве из отпуска» и самой выписки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CreateHolidayInRevFromHolidayExtRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.CreateHolidayInRevFromHolidayExtRelation";
    public static final String ENTITY_NAME = "createHolidayInRevFromHolidayExtRelation";
    public static final int VERSION_HASH = 1770792973;
    private static IEntityMeta ENTITY_META;

    public static final String L_REVOCATION_FROM_HOLIDAY_EXTRACT = "revocationFromHolidayExtract";
    public static final String L_EMPLOYEE_HOLIDAY = "employeeHoliday";

    private RevocationFromHolidayExtract _revocationFromHolidayExtract;     // Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска
    private EmployeeHoliday _employeeHoliday;     // Отпуск сотрудника

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска. Свойство не может быть null.
     */
    @NotNull
    public RevocationFromHolidayExtract getRevocationFromHolidayExtract()
    {
        return _revocationFromHolidayExtract;
    }

    /**
     * @param revocationFromHolidayExtract Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска. Свойство не может быть null.
     */
    public void setRevocationFromHolidayExtract(RevocationFromHolidayExtract revocationFromHolidayExtract)
    {
        dirty(_revocationFromHolidayExtract, revocationFromHolidayExtract);
        _revocationFromHolidayExtract = revocationFromHolidayExtract;
    }

    /**
     * @return Отпуск сотрудника. Свойство не может быть null.
     */
    @NotNull
    public EmployeeHoliday getEmployeeHoliday()
    {
        return _employeeHoliday;
    }

    /**
     * @param employeeHoliday Отпуск сотрудника. Свойство не может быть null.
     */
    public void setEmployeeHoliday(EmployeeHoliday employeeHoliday)
    {
        dirty(_employeeHoliday, employeeHoliday);
        _employeeHoliday = employeeHoliday;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CreateHolidayInRevFromHolidayExtRelationGen)
        {
            setRevocationFromHolidayExtract(((CreateHolidayInRevFromHolidayExtRelation)another).getRevocationFromHolidayExtract());
            setEmployeeHoliday(((CreateHolidayInRevFromHolidayExtRelation)another).getEmployeeHoliday());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CreateHolidayInRevFromHolidayExtRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CreateHolidayInRevFromHolidayExtRelation.class;
        }

        public T newInstance()
        {
            return (T) new CreateHolidayInRevFromHolidayExtRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "revocationFromHolidayExtract":
                    return obj.getRevocationFromHolidayExtract();
                case "employeeHoliday":
                    return obj.getEmployeeHoliday();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "revocationFromHolidayExtract":
                    obj.setRevocationFromHolidayExtract((RevocationFromHolidayExtract) value);
                    return;
                case "employeeHoliday":
                    obj.setEmployeeHoliday((EmployeeHoliday) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "revocationFromHolidayExtract":
                        return true;
                case "employeeHoliday":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "revocationFromHolidayExtract":
                    return true;
                case "employeeHoliday":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "revocationFromHolidayExtract":
                    return RevocationFromHolidayExtract.class;
                case "employeeHoliday":
                    return EmployeeHoliday.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CreateHolidayInRevFromHolidayExtRelation> _dslPath = new Path<CreateHolidayInRevFromHolidayExtRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CreateHolidayInRevFromHolidayExtRelation");
    }
            

    /**
     * @return Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreateHolidayInRevFromHolidayExtRelation#getRevocationFromHolidayExtract()
     */
    public static RevocationFromHolidayExtract.Path<RevocationFromHolidayExtract> revocationFromHolidayExtract()
    {
        return _dslPath.revocationFromHolidayExtract();
    }

    /**
     * @return Отпуск сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreateHolidayInRevFromHolidayExtRelation#getEmployeeHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> employeeHoliday()
    {
        return _dslPath.employeeHoliday();
    }

    public static class Path<E extends CreateHolidayInRevFromHolidayExtRelation> extends EntityPath<E>
    {
        private RevocationFromHolidayExtract.Path<RevocationFromHolidayExtract> _revocationFromHolidayExtract;
        private EmployeeHoliday.Path<EmployeeHoliday> _employeeHoliday;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreateHolidayInRevFromHolidayExtRelation#getRevocationFromHolidayExtract()
     */
        public RevocationFromHolidayExtract.Path<RevocationFromHolidayExtract> revocationFromHolidayExtract()
        {
            if(_revocationFromHolidayExtract == null )
                _revocationFromHolidayExtract = new RevocationFromHolidayExtract.Path<RevocationFromHolidayExtract>(L_REVOCATION_FROM_HOLIDAY_EXTRACT, this);
            return _revocationFromHolidayExtract;
        }

    /**
     * @return Отпуск сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreateHolidayInRevFromHolidayExtRelation#getEmployeeHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> employeeHoliday()
        {
            if(_employeeHoliday == null )
                _employeeHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_EMPLOYEE_HOLIDAY, this);
            return _employeeHoliday;
        }

        public Class getEntityClass()
        {
            return CreateHolidayInRevFromHolidayExtRelation.class;
        }

        public String getEntityName()
        {
            return "createHolidayInRevFromHolidayExtRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
