/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e28;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.moveemployee.entity.ScienceStatusPaymentExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 19.03.2012
 */
public class ScienceStatusPaymentExtractDao extends UniBaseDao implements IExtractComponentDao<ScienceStatusPaymentExtract>
{
    public void doCommit(ScienceStatusPaymentExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        EmployeePost post;
        EmployeeLabourContract contract;
        List<EmployeePostStaffRateItem> staffRateItemList = null;
        //если указана Должность и она отлична от должности Сотрудника, то создаем новую должность, а старую переводим в состояние Переведен
        if (extract.getPost() != null && !extract.getPost().equals(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL()))
        {
            extract.setEmployeePostStatusOld(extract.getEntity().getPostStatus());

            extract.getEntity().setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_TRANSFERED));
            extract.getEntity().setDismissalDate(extract.getParagraph().getOrder().getCommitDate());

            update(extract.getEntity());

            post = createOrAssignEmployeePost(extract);

            contract = createOrAssignEmployeeLabourContract(extract, post);

            staffRateItemList = CommonExtractCommitUtil.createEmployeePostStaffRateItems(extract, post);

            EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), post, staffRateItemList);

            EmploymentHistoryItemInner empHistItemOld = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEntity(), UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity()));

            List<StaffListAllocationItem> allocItemList = CommonExtractCommitUtil.createStaffListAllocationItem(extract, post);

            UniempDaoFacade.getUniempDAO().validateStaffRates(UserContext.getInstance().getErrorCollector(), extract.getEmployee(), post, staffRateItemList, extract.getEmployeePostNew() == null, false);

            CommonExtractCommitUtil.validateAllocationItem(extract, post.getOrgUnit(), post.getPostRelation().getPostBoundedWithQGandQL());

            if (UserContext.getInstance().getErrorCollector().hasErrors())
                return;

            empHistItemOld.setCurrent(false);
            empHistItemOld.setDismissalDate(extract.getBeginDate());
            saveOrUpdate(empHistItemOld);

            saveOrUpdate(post);

            saveOrUpdate(contract);

            saveOrUpdate(empHistItem);

            extract.setEmployeePostNew(post);
            update(extract);

            List<FinancingSourceDetails> staffRateItems = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
            List<FinancingSourceDetailsToAllocItem> relList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(staffRateItems);
            if (staffRateItems.size() != 0 && relList.size() != 0)
            {
                for (FinancingSourceDetailsToAllocItem rel : relList)
                if (!rel.isHasNewAllocItem())
                {
                    if (rel.getChoseStaffListAllocationItem() != null)
                    {
                        rel.getChoseStaffListAllocationItem().setEmployeePost(null);
                        update(rel.getChoseStaffListAllocationItem());
                    }
                }

                for (StaffListAllocationItem allocationItem : allocItemList)
                {
                    save(allocationItem);
                    StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());
                }
            }

            //если выписка проводится повторно, то ранее созданные ставки для Сотрудника удаляем
            List<EmployeePostStaffRateItem> itemOldList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(post);
            if (!itemOldList.isEmpty())
            {
                for (EmployeePostStaffRateItem staffRateItemOld : itemOldList)
                    delete(staffRateItemOld);

                getSession().flush();
            }

            for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
                save(staffRateItem);
        }
        else
        {
            post = extract.getEntity();//то работаем со старым сотрудником на должности
            contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());//берем старый ТД сотрудника
            if (contract == null)
            {
                UserContext.getInstance().getErrorCollector().add("Невозможно провести приказ, так как не указаны данные трудового договора на карточке сотрудника " + extract.getEntity().getFullTitle() + ". Укажите данные трудового договора на карточке сотрудника.");
                throw new ApplicationException();
            }
        }

        //создаем дополнительное соглашение к ТД сотрудника
        ContractCollateralAgreement agreement = new ContractCollateralAgreement();

        if(null != contract.getId())
        {
            List<ContractCollateralAgreement> agrmntsList = UniempDaoFacade.getUniempDAO().getContractCollateralAgreementsList(contract);
            for(ContractCollateralAgreement agrmnt : agrmntsList)
            {
                if(agrmnt.getNumber().equals(extract.getAddAgreementNumber()) && agrmnt.getDate().equals(extract.getAddAgreementDate()))
                {
                    agreement = agrmnt;
                    break;
                }
            }
        }

        if (null == agreement.getId())
        {
            agreement.setContract(contract);
            agreement.setDate(extract.getAddAgreementDate());
            agreement.setNumber(extract.getAddAgreementNumber());
            agreement.setDescription("Доп. соглашение к трудовому договору № " + contract.getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getBeginDate()) + "г.");

            extract.setContractAgreementNew(agreement);
        }
        saveOrUpdate(agreement);

        savePayments(extract, post, staffRateItemList);
    }

    public void doRollback(ScienceStatusPaymentExtract extract, Map parameters)
    {
        EmployeePost post = null;
        if (extract.getPost() != null && !extract.getPost().equals(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL()))
        {
            extract.getEntity().setPostStatus(extract.getEmployeePostStatusOld());
            extract.getEntity().setDismissalDate(null);
            update(extract.getEntity());

            extract.getEmployeePostNew().setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_POSSIBLE));
            update(extract.getEmployeePostNew());

            post = extract.getEmployeePostNew();


            EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEmployeePostNew(), UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEmployeePostNew()));
            if (null != empHistItem.getId())
                delete(empHistItem);

            EmploymentHistoryItemInner empHistItemOld = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEntity(), UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity()));
            if(null != empHistItemOld.getId())
            {
                empHistItemOld.setDismissalDate(null);
                empHistItemOld.setCurrent(true);
                update(empHistItemOld);
            }

            List<FinancingSourceDetails> staffRateItems = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
            List<FinancingSourceDetailsToAllocItem> relList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(staffRateItems);

            if (staffRateItems.size() != 0 && relList.size() != 0)
            {
                for (StaffListAllocationItem oldAllocationItem : UniempDaoFacade.getUniempDAO().getEmployeePostAllocationItemList(extract.getEmployeePostNew()))
                    delete(oldAllocationItem);

                for (FinancingSourceDetailsToAllocItem relation : relList)
                {
                    if (!relation.isHasNewAllocItem())
                    {
                        relation.getChoseStaffListAllocationItem().setEmployeePost(relation.getEmployeePostHistory());

                        update(relation.getChoseStaffListAllocationItem());
                    }

                }
            }
        }
        else
            post = extract.getEntity();

        if (extract.getContractAgreementNew() != null)
        {
            ContractCollateralAgreement contractAgreementNew = extract.getContractAgreementNew();

            extract.setContractAgreementNew(null);

            delete(contractAgreementNew);
        }

        removePayments(extract);
    }

    protected EmployeePost createOrAssignEmployeePost(ScienceStatusPaymentExtract extract)
    {
        EmployeePost oldPost = extract.getEntity();
        OrgUnitTypePostRelation postRelation = CommonExtractCommitUtil.getPostRelation(getSession(), oldPost.getOrgUnit(), extract.getPost());

        EmployeePost post = extract.getEmployeePostNew() == null ? new EmployeePost() : extract.getEmployeePostNew();

        post.setEmployee(oldPost.getEmployee());
        post.setOrgUnit(oldPost.getOrgUnit());
        post.setPostRelation(postRelation);
        post.setPostDate(extract.getBeginDate());
        post.setDismissalDate(null);
        post.setEtksLevels(extract.getEtksLevels());
        post.setPostType(oldPost.getPostType());
        post.setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_ACTIVE));
        post.setRaisingCoefficient(extract.getRaisingCoefficient());
        post.setSalary(extract.getSalary());
        post.setWeekWorkLoad(oldPost.getWeekWorkLoad());
        post.setWorkWeekDuration(oldPost.getWorkWeekDuration());
        post.setCompetitionType(oldPost.getCompetitionType());

        return post;
    }

    protected EmployeeLabourContract createOrAssignEmployeeLabourContract(ScienceStatusPaymentExtract extract, EmployeePost newPost)
    {
        EmployeeLabourContract oldContract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());

        if (oldContract == null)
        {
            UserContext.getInstance().getErrorCollector().add("Невозможно провести приказ, так как не указаны данные трудового договора на карточке сотрудника " + extract.getEntity().getFullTitle() + ". Укажите данные трудового договора на карточке сотрудника.");
            throw new ApplicationException();
        }

        if (extract.getEmployeePostNew() != null)
            return UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEmployeePostNew());
        else
        {
            EmployeeLabourContract contract = new EmployeeLabourContract();

            contract.update(oldContract);

            contract.setEmployeePost(newPost);

            return contract;
        }
    }

    protected void savePayments(ScienceStatusPaymentExtract extract, EmployeePost post, List<EmployeePostStaffRateItem> staffRateItemList)
    {
        List<EmployeePayment> paymentCurrentList = UniempDaoFacade.getUniempDAO().getEmployeePostPaymentList(extract.getEntity());

        Double salary = 0d;
        if (extract.getPost() != null && !extract.getPost().equals(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL()))
        {
            if (extract.getRaisingCoefficient() != null)
                salary = extract.getRaisingCoefficient().getRecommendedSalary();
            else if (extract.getPost().getSalary() != null)
                salary = extract.getPost().getSalary();
        }
        else
        {
            if (extract.getEntity().getRaisingCoefficient() != null)
                salary = extract.getEntity().getRaisingCoefficient().getRecommendedSalary();
            else if (extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary() != null)
                salary = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary();
        }

        if (staffRateItemList == null)
            staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());

        List<EmployeePayment> paymentToSaveList = CommonExtractCommitUtil.createOrAssignEmployeePaymentsList(getSession(), extract, post, salary, staffRateItemList);

        CommonExtractCommitUtil.validateEmployeePayment(extract.getEntity(), paymentToSaveList, getSession());

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;

        if (extract.getPost() == null || extract.getPost().equals(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL()))
        {
            ErrorCollector errors = UserContext.getInstance().getErrorCollector();

            for (EmployeePayment payment1 : paymentToSaveList)
                for (EmployeePayment payment2 : paymentCurrentList)
                {
                    if (payment1.getPayment().getCode().equals(payment2.getPayment().getCode()))
                    {
                        Date begin1 = payment1.getBeginDate();
                        Date end1 = payment1.getEndDate();
                        Date begin2 = payment2.getBeginDate();
                        Date end2 = payment2.getEndDate();

                        if (end2 == null || end1 == null)
                            continue;

                        StringBuilder errBuilder = new StringBuilder("Период выплаты ")
                                .append(payment1.getPayment().getTitle())
                                .append(" с ")
                                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(payment1.getBeginDate()))
                                .append(payment1.getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(payment1.getEndDate()) : "")
                                .append(" пересекается с выплатой ")
                                .append(payment2.getPayment().getTitle())
                                .append(" с ")
                                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(payment2.getBeginDate()))
                                .append(payment2.getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(payment2.getEndDate()) : "")
                                .append(".");

                        if (CommonBaseDateUtil.isBetween(begin1, begin2, end2) || CommonBaseDateUtil.isBetween(end1, begin2, end2))
                            errors.add(errBuilder.toString());
                        else {
                            if (CommonBaseDateUtil.isBetween(begin2, begin1, end1) || CommonBaseDateUtil.isBetween(end2, begin1, end1))
                                errors.add(errBuilder.toString());
                        }
                    }
                }

            if (errors.hasErrors())
                return;
        }

        for (EmployeePayment payment : paymentToSaveList)
            saveOrUpdate(payment);
    }

    protected void removePayments(ScienceStatusPaymentExtract extract)
    {
        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
            delete(bonus.getEntity());
    }
}