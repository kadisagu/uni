/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import java.util.List;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWeekWorkLoad;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;

/**
 * @author dseleznev
 * Created on: 21.01.2009
 */
public interface IEmployeePostModel<T extends AbstractEmployeeExtract> extends ICommonEmployeeExtractAddEditModel<T>
{
    // Post types select model getter and setter

    List<PostType> getPostTypesList();

    void setPostTypesList(List<PostType> postTypesList);

    // OrgUnit select model getter and setter 

    ISelectModel getOrgUnitListModel();

    void setOrgUnitListModel(ISelectModel orgUnitListModel);

    // Post relations select model getter and setter

    ISelectModel getPostRelationListModel();

    void setPostRelationListModel(ISelectModel postRelationListModel);

    // CompetitionType model

    List<CompetitionAssignmentType> getCompetitionTypeList();

    void setCompetitionTypeList(List<CompetitionAssignmentType> competitionTypeList);

    // Week work Loads list

    List<EmployeeWeekWorkLoad> getWeekWorkLoadsList();

    void setWeekWorkLoadsList(List<EmployeeWeekWorkLoad> weekWorkLoadsList);

    // Work week durations list

    List<EmployeeWorkWeekDuration> getWorkWeekDurationsList();

    void setWorkWeekDurationsList(List<EmployeeWorkWeekDuration> workWeekDurationsList);

    // Etks levels select model getter and setter

    List<EtksLevels> getEtksLevelsList();

    void setEtksLevelsList(List<EtksLevels> etksLevelsList);

    // Raising coefficients list

    ISelectModel getRaisingCoefficientListModel();

    void setRaisingCoefficientListModel(ISelectModel raisingCoefficientListModel);

    // Labour contract types select model getter and setter

    List<LabourContractType> getLabourContractTypesList();

    void setLabourContractTypesList(List<LabourContractType> labourContractTypesList);
}