/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e24.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.RemovalCombinationExtract;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 29.12.2011
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<RemovalCombinationExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareEmployeeStaffRateDataSource(component);
        prepareCombinationStaffRateDataSource(component);
    }

    public void prepareEmployeeStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getEmployeeStaffRateDataSource() != null)
            return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareEmployeeStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setEmployeeStaffRateDataSource(dataSource);
    }

    public void prepareCombinationStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getCombinationStaffRateDataSource() != null)
            return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareCombinationStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setCombinationStaffRateDataSource(dataSource);
    }

    public void onChangeCombinationPost(IBusinessComponent component)
    {
        Model model = getModel(component);
        CombinationPost combinationPost = model.getExtract().getCombinationPost();
        if (combinationPost != null)
        {
            model.setCombinationPostStaffRateItemList(UniempDaoFacade.getUniempDAO().getCombinationPostStaffRateItemList(combinationPost));

            model.getCombinationStaffRateDataSource().refresh();
        }
    }
}