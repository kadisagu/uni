/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e27.Pub;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

import java.util.Collections;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 06.03.2012
 */
public class DAO extends ModularEmployeeExtractPubDAO<ScienceDegreePaymentExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        StringBuilder labourContractBuilder = new StringBuilder()
                .append("№")
                .append(model.getExtract().getAddAgreementNumber())
                .append(" от ")
                .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getAddAgreementDate()));
        model.setLabourContractStr(labourContractBuilder.toString());

        model.setStaffRateStr(CommonEmployeeExtractUtil.getExtractFinancingSourceDetailsString(model.getExtract(), getSession(), true, null));
        model.setEmplStaffRateStr(CommonEmployeeExtractUtil.getExtractFinancingSourceDetailsString(model.getExtract(), getSession(), false, null));

        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getExtract().getEntity());
        if (contract != null)
        {
            model.setLabourContractType(contract.getType().getTitle());
            model.setLabourContractNumber(contract.getNumber());
            model.setLabourContractDate(contract.getDate());
            model.setLabourContractBeginDate(contract.getBeginDate());
            model.setLabourContractEndDate(contract.getEndDate());
        }
    }

    @Override
    public void preparePaymentDataSource(Model model)
    {
        DQLSelectBuilder payBuilder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "b").column("b");
        payBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeBonus.extract().fromAlias("b")), model.getExtract()));

        List<EmployeeBonus> bonusList = payBuilder.createStatement(getSession()).list();

        Collections.sort(bonusList, CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR);
        UniBaseUtils.createPage(model.getPaymentDataSource(), bonusList);
    }
}