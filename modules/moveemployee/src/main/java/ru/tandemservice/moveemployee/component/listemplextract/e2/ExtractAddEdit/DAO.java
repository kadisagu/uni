/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e2.ExtractAddEdit;

import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit.AbstractListExtractAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ListExtractComponentGenerator
 * @since 06.05.2009
 */
public class DAO extends AbstractListExtractAddEditDAO<EmployeeTransferEmplListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setCompetitionTypeList(getCatalogItemList(CompetitionAssignmentType.class));
        CommonExtractUtil.createPostBoundedModel(this, getSession(), model.isAddForm(), model);
        model.setPostRelationListModel(CommonEmployeeExtractUtil.getPostRelationListModel(model.getExtract()));
//        CommonExtractUtil.createPaymentsBonusModel(this, getSession(), model.isAddForm(), model, true);

        if (model.isEditForm())
        {
            List<FinancingSourceDetails> financingSourceDetailsList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(model.getExtract());
            model.setStaffRateItemList(financingSourceDetailsList);
            CommonEmployeeExtractUtil.prepareColumnsStaffRateSearchList(financingSourceDetailsList, model.getStaffRateDataSource());

            //поднимаем релейшены долей ставок и выбранных ставок ШР
            model.setDetailsToAllocItemList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(financingSourceDetailsList));

            //определяем какие источники финансирования показывать
            CommonEmployeeExtractUtil.fillFinSrcModel(model, model.getExtract().isFreelance(), model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL());

            //PAYMENTS
            //подготавливаем блок с выплатами
            preparePaymentBlock(model);
        }

        model.setWeekWorkLoadsList(getCatalogItemListOrderByCode(EmployeeWeekWorkLoad.class));
        model.setWorkWeekDurationsList(getCatalogItemListOrderByCode(EmployeeWorkWeekDuration.class));

        model.setRaisingCoefficientListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getExtract().getPostBoundedWithQGandQL())
                {
                    return ListResult.getEmpty();
                }
                MQBuilder builder = new MQBuilder(SalaryRaisingCoefficient.ENTITY_CLASS, "rc");
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_POST, model.getExtract().getPostBoundedWithQGandQL().getPost()));
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_QUALIFICATION_LEVEL, model.getExtract().getPostBoundedWithQGandQL().getQualificationLevel()));
                builder.add(MQExpression.like("rc", SalaryRaisingCoefficient.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("rc", SalaryRaisingCoefficient.P_RAISING_COEFFICIENT);
                builder.addOrder("rc", SalaryRaisingCoefficient.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = get(SalaryRaisingCoefficient.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                model.getExtract().setRaisingCoefficient(null);
                model.getExtract().setSalary(0d);
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((SalaryRaisingCoefficient) value).getFullTitle();
            }

        });

        model.setFinancingSourceItemModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                for (IEntity item : findValues("").getObjects())
                    if (item.getId().equals(primaryKey))
                        return item;

                return null;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<FinancingSourceItem> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                FinancingSource financingSource = finSrcMap.get(id);
                if (model.isThereAnyActiveStaffList())
                {
                    return new ListResult<>(UniempDaoFacade.getStaffListDAO().getFinancingSourcesItemStaffListItems(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), financingSource, filter));
                } else
                    return new ListResult<>(UniempDaoFacade.getUniempDAO().getFinSrcItm(financingSource.getId(), filter));
            }
        });

        model.setEmployeeHRModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> resultList = new ArrayList<>();

                for (IdentifiableWrapper wrapper : findValues("").getObjects())
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);

                return resultList;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
                final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();
                OrgUnit orgUnit = model.getExtract().getOrgUnit();
                PostBoundedWithQGandQL post = model.getExtract().getPostBoundedWithQGandQL();

                if (finSrcMap.get(id) == null || orgUnit == null || post == null)
                    return ListResult.getEmpty();

                String NEW_STAFF_RATE_STR = "<Новая ставка> - ";

                List<IdentifiableWrapper> resultList = new ArrayList<>();

                List<StaffListAllocationItem> staffListAllocList = UniempDaoFacade.getStaffListDAO().getOccupiedStaffListAllocationItem(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id));

                if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id)))
                {
                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id));
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
                    for (StaffListAllocationItem item : staffListAllocList)
                        if (!item.getEmployeePost().getPostStatus().isActive())
                            diff -= item.getStaffRate();
                    if (diff > 0 && NEW_STAFF_RATE_STR.contains(filter))
                        resultList.add(new IdentifiableWrapper(0L, NEW_STAFF_RATE_STR + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                for (StaffListAllocationItem item : staffListAllocList)
                {
                    if (item.getEmployeePost().getPerson().getFullFio().contains(filter))
                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                return new ListResult<>(resultList);
            }
        });

        if (model.isAddForm())
        {
            model.getExtract().setPostType(getCatalogItem(PostType.class, UniDefines.POST_TYPE_MAIN_JOB));
            model.getExtract().setWeekWorkLoad((EmployeeWeekWorkLoad)getSession().createCriteria(EmployeeWeekWorkLoad.class).add(Restrictions.eq(EmployeeWeekWorkLoad.P_DFLT, Boolean.TRUE)).uniqueResult());
            model.getExtract().setWorkWeekDuration((EmployeeWorkWeekDuration)getSession().createCriteria(EmployeeWorkWeekDuration.class).add(Restrictions.eq(EmployeeWorkWeekDuration.P_DFLT, Boolean.TRUE)).uniqueResult());
        }
        else
        {
            model.setPostPPS(EmployeeTypeCodes.EDU_STAFF.equals(model.getExtract().getPostBoundedWithQGandQL().getPost().getEmployeeType().getCode()));
        }

        // PAYMENTS
        model.setPaymentModel(CommonEmployeeExtractUtil.getPaymentModelForPaymentBlock());
        model.setFinSrcPaymentModel(CommonEmployeeExtractUtil.getFinSrcModelForPaymentBlock());
        model.setFinSrcItemPaymentModel(CommonEmployeeExtractUtil.getFinSrcItemModelForPaymentBlock(model.getPaymentDataSource()));
    }

    @Override
    public void update(Model model)
    {
        // VALIDATE
        if ((null == model.getExtract().getCollateralAgreementNumber())
                != (null == model.getExtract().getTransferDate()))
        {
            ErrorCollector errors = ContextLocal.getUserContext().getErrorCollector();
            errors.add("Для дополнительного соглашения необходимо заполнить поля «Дата», «Номер»", "transferDate", "collateralAgreementNumber");
            return;
        }
        super.update(model);
//        CommonExtractUtil.updateBonuses(this, model, getSession());

        CommonEmployeeExtractUtil.updateStaffRateList(model, model.isEditForm(), getSession());

        //PAYMENTS
        //если редактируем приказ, то удаляем созданные ранее выплаты в выписке
        if (model.isEditForm())
        {
            for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getSavedPaymentsFromPaymentBlockDQLBuilder(model.getExtract()).createStatement(getSession()).<EmployeeBonus>list())
                delete(bonus);
        }

        //сохраняем выплаты
        for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getPaymentsForSaveFromPaymentBlock(model.getPaymentList()))
            save(bonus);
        // END PAYMENTS
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateItemList());

        if (model.isThereAnyActiveStaffList())
        {
            final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
            final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());
            //достаем из враперов объекты Штатной расстановки
            final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
            final List<StaffListAllocationItem> allocationItems = new ArrayList<>();

            for (IEntity entity : model.getStaffRateItemList())
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = get(StaffListAllocationItem.class, wrapper.getId());
                        allocationItemList.add(item);
                        allocationItems.add(item);
                    }
                }

                allocItemsMap.put(entity.getId(), allocationItemList);
            }

            final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSourceDetailsToAllocItem.class, "b").column(property("b"))
                    .where(eq(property(FinancingSourceDetailsToAllocItem.financingSourceDetails().extract().paragraph().order().fromAlias("b")), value(model.getExtract().getParagraph().getOrder())));
            if (model.isEditForm())
                builder.where(ne(property(FinancingSourceDetailsToAllocItem.financingSourceDetails().extract().fromAlias("b")), value(model.getExtract())));
            builder.where(in(property(FinancingSourceDetailsToAllocItem.choseStaffListAllocationItem().fromAlias("b")), allocationItems));
            final List<FinancingSourceDetailsToAllocItem> relAllocItemList = builder.createStatement(getSession()).list();

            if (!relAllocItemList.isEmpty())
            {
                final Map<StaffListAllocationItem, FinancingSourceDetailsToAllocItem> allocItemMap = new HashMap<>();
                for (FinancingSourceDetailsToAllocItem item : relAllocItemList)
                    allocItemMap.put(item.getChoseStaffListAllocationItem(), item);

                for (FinancingSourceDetails details : model.getStaffRateItemList())
                {
                    for (StaffListAllocationItem allocationItem : allocItemsMap.get(details.getId()))
                    {
                        if (allocItemMap.containsKey(allocationItem))
                            errors.add("Ставка сотрудника " + allocationItem.getEmployeePost().getPerson().getFio() + " уже выбрана в поле «Кадровая расстановка» " +
                                    "в параграфе №" + allocItemMap.get(allocationItem).getFinancingSourceDetails().getExtract().getNumber() + " формируемого списочного приказа.",
                                    "employeeHR_Id_" + details.getId());
                    }
                }
            }
        }

        CommonEmployeeExtractUtil.validateStaffRate(model.getStaffRateDataSource(), model.getStaffRateItemList(), model.getExtract().getPostType().getCode(), errors);

        validateFinSrcDetToAllocItem(model, errors);

        //PAYMENTS
        CommonEmployeeExtractUtil.validateForPaymentBlock(model.getPaymentList(), errors);
    }

    //выносим проверку в метод для переопределения в проекте
    protected void validateFinSrcDetToAllocItem(Model model, ErrorCollector errors)
    {
        CommonEmployeeExtractUtil.validateFinSrcDetToAllocItem(model, model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), errors);
    }

    @Override
    protected EmployeeTransferEmplListExtract createNewExtractInstance(Model model)
    {
        return new EmployeeTransferEmplListExtract();
    }

//    @Override
//    public void addPaymentAutocompleteModel(Model model)
//    {
//        CommonExtractUtil.addEmployeeBonus(model, CommonExtractUtil.getPaymentAutocompleteModel(model, getSession()));
//    }
//
//    @Override
//    public void deletePaymentAutocompleteModel(Model model, Integer paymentId)
//    {
//        CommonExtractUtil.deleteEmployeeBonus(model, paymentId, CommonExtractUtil.getPaymentAutocompleteModel(model, getSession()));
//    }

    @Override
    public void refreshPostType(Model model)
    {
        model.getExtract().setPostType(CommonExtractUtil.getAccessibleEmployeePostType(model.getExtract().getEmployee(), model.getExtract().getEmployeePost(), getSession()));

        prepareEmplStaffRateDataSource(model);
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getStaffRateItemList());

        if (model.isNeedUpdateDataSource())
        {
            CommonEmployeeExtractUtil.prepareColumnsStaffRateSearchList(model.getStaffRateItemList(), model.getStaffRateDataSource());
            model.setNeedUpdateDataSource(false);
        }
    }

    @Override
    public void prepareEmplStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = new ArrayList<>();
        if (model.getExtract().getEmployeePost() != null)
            staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getExtract().getEmployeePost());

        model.getEmplStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getEmplStaffRateDataSource(), staffRateItems);
    }

    // PAYMENT METHODS

    protected void preparePaymentBlock(Model model)
    {
        DQLSelectBuilder payBuilder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "b").column("b");
        payBuilder.where(eqValue(property(EmployeeBonus.extract().fromAlias("b")), model.getExtract()));

        model.setPaymentList(payBuilder.createStatement(getSession()).<EmployeeBonus>list());

        List<StaffListPostPayment> paymentList = CommonEmployeeExtractUtil.checkEnabledAddStaffListPaymentButton(model.getExtract().getPostBoundedWithQGandQL(), model.getExtract().getOrgUnit(), model.getPaymentList(), model.getStaffRateItemList());

        if (!paymentList.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);
    }

    @Override
    public void preparePaymentDataSource(Model model)
    {
        CommonEmployeeExtractUtil.preparePaymentDataSourceForPaymentBlock(model.getPaymentList(), model.getPaymentDataSource());
    }

    // END PAYMENT METHODS
}