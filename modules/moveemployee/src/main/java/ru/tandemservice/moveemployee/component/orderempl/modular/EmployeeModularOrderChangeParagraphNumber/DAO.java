/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderChangeParagraphNumber;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author Alexander Zhebko
 * @since 27.03.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setExtract(get(ModularEmployeeExtract.class, model.getId()));
        model.setNewNumber(model.getExtract().getParagraph().getNumber());
    }

    @Override
    public void update(Model model)
    {
        AbstractEmployeeParagraph paragraph = model.getExtract().getParagraph();
        AbstractEmployeeOrder order = paragraph.getOrder();

        int oldNumber = paragraph.getNumber();
        int newNumber = model.getNewNumber();

        newNumber = Math.min(newNumber, order.getParagraphCount());

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(AbstractEmployeeParagraph.class, "p")
                .where(DQLExpressions.eq(DQLExpressions.property(AbstractEmployeeParagraph.order().fromAlias("p")), DQLExpressions.value(order)));

        int numberShift;

        if (oldNumber <= newNumber)
        {
            builder.where(DQLExpressions.gt(DQLExpressions.property(AbstractEmployeeParagraph.number().fromAlias("p")), DQLExpressions.value(oldNumber)));
            builder.where(DQLExpressions.le(DQLExpressions.property(AbstractEmployeeParagraph.number().fromAlias("p")), DQLExpressions.value(newNumber)));
            builder.order(DQLExpressions.property(AbstractEmployeeParagraph.number().fromAlias("p")), OrderDirection.asc);
            numberShift = -1;

        } else
        {
            builder.where(DQLExpressions.ge(DQLExpressions.property(AbstractEmployeeParagraph.number().fromAlias("p")), DQLExpressions.value(newNumber)));
            builder.where(DQLExpressions.lt(DQLExpressions.property(AbstractEmployeeParagraph.number().fromAlias("p")), DQLExpressions.value(oldNumber)));
            builder.order(DQLExpressions.property(AbstractEmployeeParagraph.number().fromAlias("p")), OrderDirection.desc);
            numberShift = 1;
        }

        paragraph.setNumber(-1);
        update(paragraph);
        getSession().flush();

        for (AbstractEmployeeParagraph currentParagraph: builder.createStatement(getSession()).<AbstractEmployeeParagraph>list())
        {
            currentParagraph.setNumber(currentParagraph.getNumber() + numberShift);
            update(currentParagraph);
        }

        getSession().flush();

        paragraph.setNumber(newNumber);
        update(paragraph);

        ((EmployeeModularOrder) order).setParagraphsSortRule(get(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_DAO_NAME, MoveEmployeeDefines.MANUAL_SORT));
        update(order);
    }
}