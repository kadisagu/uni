/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.encouragementPrintFormatSettings.EncouragementPrintFormatEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.moveemployee.entity.EncouragementToPrintFormatRelation;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

/**
 * Create by ashaburov
 * Date 29.09.11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntity(get(EncouragementType.class, model.getId()));

        MQBuilder builder = new MQBuilder(EncouragementToPrintFormatRelation.ENTITY_CLASS, "b", new String[]{EncouragementToPrintFormatRelation.P_PRINT_FORMAT});
        builder.add(MQExpression.eq("b", EncouragementToPrintFormatRelation.L_ENCOURAGEMENT_TYPE, model.getEntity()));

        model.setPrintFormat((String) builder.uniqueResult(getSession()));
    }

    @Override
    public void update(Model model)
    {
        MQBuilder builder = new MQBuilder(EncouragementToPrintFormatRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", EncouragementToPrintFormatRelation.L_ENCOURAGEMENT_TYPE, model.getEntity()));

        EncouragementToPrintFormatRelation relation = (EncouragementToPrintFormatRelation) builder.uniqueResult(getSession());

        if (relation != null)
            relation.setPrintFormat(model.getPrintFormat());
        else
        {
            relation = new EncouragementToPrintFormatRelation();
            relation.setEncouragementType(model.getEntity());
            relation.setPrintFormat(model.getPrintFormat());
        }

        saveOrUpdate(relation);
    }
}
