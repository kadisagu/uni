/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e17;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.CreateHolidayInRevFromHolidayExtRelation;
import ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 09.09.2011
 */
public class RevocationFromHolidayExtractDao extends UniBaseDao implements IExtractComponentDao<RevocationFromHolidayExtract>
{
    @Override
    public void doCommit(RevocationFromHolidayExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        Calendar newDate = GregorianCalendar.getInstance();
        List<EmployeeHoliday> newHolidayList = new ArrayList<>();
        extract.setHolidayStartOld(extract.getEmployeeHoliday().getStartDate());
        extract.setHolidayFinishOld(extract.getEmployeeHoliday().getFinishDate());
        extract.setHolidayDurationOld(extract.getEmployeeHoliday().getDuration());
        extract.setHolidayBeginOld(extract.getEmployeeHoliday().getBeginDate());
        extract.setHolidayEndOld(extract.getEmployeeHoliday().getEndDate());
        extract.setHolidayTitleOld(extract.getEmployeeHoliday().getTitle());
        extract.setHolidayTypeOld(extract.getEmployeeHoliday().getHolidayType());
        extract.setHolidayDeleteOld(false);
        extract.setHolidayExtractOld(extract.getEmployeeHoliday().getHolidayExtract());
        extract.setHolidayExtractNumberOld(extract.getEmployeeHoliday().getHolidayExtractNumber());
        extract.setHolidayExtractDateOld(extract.getEmployeeHoliday().getHolidayExtractDate());

        if (extract.getEmployeeHoliday().getStartDate().equals(extract.getRevocationFromDay()))
        {
            if (extract.getRevocationToDay() != null && extract.getRevocationToDay().before(extract.getEmployeeHoliday().getFinishDate()))
            {
                newDate.setTime(extract.getRevocationToDay());
                newDate.add(Calendar.DATE, 1);
                extract.getEmployeeHoliday().setStartDate(newDate.getTime());
                extract.getEmployeeHoliday().setDuration(EmployeeManager.instance().dao().getEmployeeHolidayDuration(extract.getEntity().getWorkWeekDuration(), extract.getEmployeeHoliday().getStartDate(), extract.getEmployeeHoliday().getFinishDate()));
                extract.getEmployeeHoliday().setHolidayExtract(extract);
                extract.getEmployeeHoliday().setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                extract.getEmployeeHoliday().setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

                saveOrUpdate(extract.getEmployeeHoliday());
            }
            else
            {
                delete(extract.getEmployeeHoliday());
                extract.setHolidayDeleteOld(true);
            }
        }
        else
        {
            newDate.setTime(extract.getRevocationFromDay());
            newDate.add(Calendar.DATE, -1);
            extract.getEmployeeHoliday().setFinishDate(newDate.getTime());
            extract.getEmployeeHoliday().setDuration(EmployeeManager.instance().dao().getEmployeeHolidayDuration(extract.getEntity().getWorkWeekDuration(), extract.getEmployeeHoliday().getStartDate(), extract.getEmployeeHoliday().getFinishDate()));
            extract.getEmployeeHoliday().setHolidayExtract(extract);
            extract.getEmployeeHoliday().setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
            extract.getEmployeeHoliday().setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

            saveOrUpdate(extract.getEmployeeHoliday());

            if (extract.getRevocationToDay() != null && extract.getRevocationToDay().before(extract.getHolidayFinishOld()))
            {
                EmployeeHoliday newHoliday = new EmployeeHoliday();
                newHoliday.setTitle(extract.getEmployeeHoliday().getTitle());
                if (extract.getEmployeeHoliday().getBeginDate() != null)
                    newHoliday.setBeginDate(extract.getEmployeeHoliday().getBeginDate());
                if (extract.getEmployeeHoliday().getEndDate() != null)
                    newHoliday.setEndDate(extract.getEmployeeHoliday().getEndDate());
                newHoliday.setHolidayType(extract.getEmployeeHoliday().getHolidayType());

                Calendar newStartDate = GregorianCalendar.getInstance();
                newStartDate.setTime(extract.getRevocationToDay());
                newStartDate.add(Calendar.DATE, 1);

                newHoliday.setStartDate(newStartDate.getTime());
                newHoliday.setFinishDate(extract.getHolidayFinishOld());
                newHoliday.setDuration(EmployeeManager.instance().dao().getEmployeeHolidayDuration(extract.getEntity().getWorkWeekDuration(), newHoliday.getStartDate(), newHoliday.getFinishDate()));

                newHoliday.setEmployeePost(extract.getEntity());
                newHoliday.setHolidayExtract(extract);
                newHoliday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                newHoliday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

                save(newHoliday);
                newHolidayList.add(newHoliday);
            }
        }

        saveOrUpdate(extract);

        if (extract.isProvideUnusedHoliday())
        {
            List<ProvideDateToExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getProvideHolidayFromRevocationHolidayExtract(extract);
            for (ProvideDateToExtractRelation relation : relationList)
            {
                EmployeeHoliday newHoliday = new EmployeeHoliday();
                newHoliday.setTitle(extract.getEmployeeHoliday().getTitle());
                if (extract.getEmployeeHoliday().getBeginDate() != null)
                    newHoliday.setBeginDate(extract.getEmployeeHoliday().getBeginDate());
                if (extract.getEmployeeHoliday().getEndDate() != null)
                    newHoliday.setEndDate(extract.getEmployeeHoliday().getEndDate());
                newHoliday.setHolidayType(extract.getEmployeeHoliday().getHolidayType());
                newHoliday.setStartDate(relation.getProvideBeginDay());
                newHoliday.setFinishDate(relation.getProvideEndDay());
                newHoliday.setDuration(relation.getProvideDurationDay());
                newHoliday.setEmployeePost(extract.getEntity());
                newHoliday.setHolidayExtract(extract);
                newHoliday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                newHoliday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

                save(newHoliday);
                newHolidayList.add(newHoliday);
            }
        }

        for (EmployeeHoliday holiday : newHolidayList)
        {
            CreateHolidayInRevFromHolidayExtRelation relation = new CreateHolidayInRevFromHolidayExtRelation();
            relation.setEmployeeHoliday(holiday);
            relation.setRevocationFromHolidayExtract(extract);

            save(relation);
        }
    }

    @Override
    public void doRollback(RevocationFromHolidayExtract extract, Map parameters)
    {
        if (extract.getHolidayDeleteOld())
        {
            EmployeeHoliday newHoliday = new EmployeeHoliday();
            newHoliday.setTitle(extract.getHolidayTitleOld());
            if (extract.getHolidayBeginOld() != null)
                newHoliday.setBeginDate(extract.getHolidayBeginOld());
            if (extract.getHolidayEndOld() != null)
                newHoliday.setEndDate(extract.getHolidayEndOld());
            newHoliday.setHolidayType(extract.getHolidayTypeOld());
            newHoliday.setStartDate(extract.getHolidayStartOld());
            newHoliday.setFinishDate(extract.getHolidayFinishOld());
            newHoliday.setDuration(extract.getHolidayDurationOld());
            newHoliday.setEmployeePost(extract.getEntity());
            newHoliday.setHolidayExtract(extract.getHolidayExtractOld());
            newHoliday.setHolidayExtractNumber(extract.getHolidayExtractNumberOld());
            newHoliday.setHolidayExtractDate(extract.getHolidayExtractDateOld());

            extract.setEmployeeHoliday(newHoliday);

            save(newHoliday);
            saveOrUpdate(extract);
        }
        else
        {
            extract.getEmployeeHoliday().setStartDate(extract.getHolidayStartOld());
            extract.getEmployeeHoliday().setFinishDate(extract.getHolidayFinishOld());
            extract.getEmployeeHoliday().setDuration(extract.getHolidayDurationOld());
            extract.getEmployeeHoliday().setHolidayExtract(extract.getHolidayExtractOld());
            extract.getEmployeeHoliday().setHolidayExtractNumber(extract.getHolidayExtractNumberOld());
            extract.getEmployeeHoliday().setHolidayExtractDate(extract.getHolidayExtractDateOld());

            saveOrUpdate(extract.getEmployeeHoliday());
        }

        MQBuilder builder = new MQBuilder(CreateHolidayInRevFromHolidayExtRelation.ENTITY_CLASS, "b", new String[]{CreateHolidayInRevFromHolidayExtRelation.L_EMPLOYEE_HOLIDAY});
        builder.add(MQExpression.eq("b", CreateHolidayInRevFromHolidayExtRelation.L_REVOCATION_FROM_HOLIDAY_EXTRACT, extract));

        List<EmployeeHoliday> employeeHolidayList = builder.getResultList(getSession());
        for (EmployeeHoliday holiday : employeeHolidayList)
        {
            delete(holiday);
        }
    }
}