/**
 *$Id:$
 */
package ru.tandemservice.moveemployee.runtime;

import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;
import ru.tandemservice.uni.dao.UniBaseDao;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public class EmployeeModularOrderParagraphsSortRuleRuntimeExtensionDAO extends UniBaseDao implements IEmployeeModularOrderParagraphsSortRuleRuntimeExtensionDAO
{
    @Override
    public void createRuleList()
    {
        if (!existsEntity(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_DAO_NAME, "employeeModularOrderParagraphsSorter1"))
        {
            EmployeeModularOrderParagraphsSortRule rule = new EmployeeModularOrderParagraphsSortRule();
            rule.setDaoName("employeeModularOrderParagraphsSorter1");
            rule.setTitle("Ручная нумерация");
            rule.setDescription("Предполагает изменение порядка следования выписок в приказе пользователем");

            save(rule);
        }

        if (!existsEntity(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_DAO_NAME, "employeeModularOrderParagraphsSorter2"))
        {
            EmployeeModularOrderParagraphsSortRule rule = new EmployeeModularOrderParagraphsSortRule();
            rule.setDaoName("employeeModularOrderParagraphsSorter2");
            rule.setTitle("Группировка по типу выписки");
            rule.setDescription("Группы выписок сортируются по названию типа, в группе сортировка осуществляется по ФИО сотрудника");
            rule.setActive(true);

            save(rule);
        }
    }
}