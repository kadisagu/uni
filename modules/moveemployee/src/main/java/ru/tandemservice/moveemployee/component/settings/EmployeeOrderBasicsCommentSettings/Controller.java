/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.settings.EmployeeOrderBasicsCommentSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.EnabledPropertyEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author dseleznev
 * Created on: 21.09.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    @SuppressWarnings("unchecked")
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", EmployeeOrderBasics.P_TITLE).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Требует примечания", EmployeeOrderBasics.commentable().s()).setListener("onClickCommentable"));
        dataSource.addColumn(new ToggleColumn("Трудовой договор", EmployeeOrderBasics.laborContract().s()).setListener("onClickLaborContract").setDisableHandler(new EnabledPropertyEntityHandler(EmployeeOrderBasics.commentable().s())));
        dataSource.addColumn(new ToggleColumn("Доп. соглашение к ТД", EmployeeOrderBasics.agreementLaborContract().s()).setListener("onClickAgreementLaborContract").setDisableHandler(new EnabledPropertyEntityHandler(EmployeeOrderBasics.commentable().s())));
        model.setDataSource(dataSource);
    }

    public void onClickCommentable(IBusinessComponent component)
    {
        getDao().updateCommentable((Long)component.getListenerParameter());
    }

    public void onClickLaborContract(IBusinessComponent component)
    {
        getDao().updateLaborContract((Long) component.getListenerParameter());
    }

    public void onClickAgreementLaborContract(IBusinessComponent component)
    {
        getDao().updateAgreementLaborContract((Long) component.getListenerParameter());
    }
}