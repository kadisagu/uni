/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e4.ExtractAddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit.AbstractListExtractAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.uniemp.util.UniempUtil;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ListExtractComponentGenerator
 * @since 04.10.2011
 */
public class DAO extends AbstractListExtractAddEditDAO<EmployeeHolidayEmplListExtract, Model> implements IDAO
{
    private List<String> employeePostStatuses = Arrays.asList(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR,
        EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR,
        EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY,
        EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE);

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        Map<String, EmployeePostStatus> postStatusesMap = new HashMap<>();
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE));
        model.setPostStatusesMap(postStatusesMap);

        if (model.isEditForm())
        {
            model.setAddFormat(new IdentifiableWrapper(1L, "Отпуск сотруднику"));

            if (model.getExtract().getVacationScheduleItem() != null)
                model.setHasVacationSchedule(true);
            else
                if (hasVacationSchedule(model.getExtract().getEmployeePost()))
                    model.setHasVacationSchedule(true);
        }

        model.setAddFormatModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                List<IdentifiableWrapper> resultList = new ArrayList<>();
                List<IdentifiableWrapper> wrapperList = new ArrayList<>();
                wrapperList.add(new IdentifiableWrapper(1L, "Отпуск сотруднику"));
                wrapperList.add(new IdentifiableWrapper(2L, "Отпуск сотрудникам согласно Графику отпусков"));

                for (IdentifiableWrapper wrapper : wrapperList)
                {
                    if (o != null)
                        if (wrapper.getId().equals(o))
                        {
                            resultList.clear();
                            resultList.add(wrapper);
                            return new SimpleListResultBuilder<>(resultList);
                        }
                    if (wrapper.getTitle().contains(filter))
                        resultList.add(wrapper);
                }

                return new SimpleListResultBuilder<>(resultList);
            }
        });

        model.setVacationScheduleSModel(new CommonSingleSelectModel(VacationSchedule.P_SIMPLE_TITLE_WITH_YEAR)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (o != null)
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(VacationSchedule.class, "s").column("s");
                    subBuilder.where(eqValue(property(VacationSchedule.id().fromAlias("s")), o));
                    return new DQLListResultBuilder(subBuilder);
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "si").column(property(VacationScheduleItem.vacationSchedule().fromAlias("si")));
                builder.where(eqValue(property(VacationScheduleItem.employeePost().fromAlias("si")), model.getExtract().getEmployeePost()));
                builder.where(ge(property(VacationScheduleItem.vacationSchedule().year().fromAlias("si")), value(Calendar.getInstance().get(Calendar.YEAR))));
                builder.where(eqValue(property(VacationScheduleItem.vacationSchedule().state().code().fromAlias("si")), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
                builder.order(property(VacationScheduleItem.vacationSchedule().year().fromAlias("si")), OrderDirection.desc);
                builder.predicate(DQLPredicateType.distinct);

                return new DQLListResultBuilder(builder);
            }
        });

        model.setVacationScheduleItemSModel(new CommonSingleSelectModel(VacationScheduleItem.P_PLANED_VACATION_TITLE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getVacationSchedule() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
                builder.where(eqValue(property(VacationScheduleItem.vacationSchedule().fromAlias("i")), model.getExtract().getVacationSchedule()));
                builder.where(eqValue(property(VacationScheduleItem.employeePost().fromAlias("i")), model.getExtract().getEmployeePost()));
                if (o != null)
                    builder.where(eqValue(property(VacationScheduleItem.id().fromAlias("i")), o));
                builder.order(property(VacationScheduleItem.planDate().fromAlias("i")), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }
        });

        model.setHolidayTypesListModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                for (IEntity entity : findValues("").getObjects())
                {
                    if (entity.getId().equals(primaryKey))
                        return entity;
                }
                return null;
            }

            @Override
            public ListResult<HolidayType> findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(HolidayType.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", HolidayType.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("b", HolidayType.P_TITLE, OrderDirection.asc);


                return new ListResult<>(builder.<HolidayType>getResultList(getSession()));
            }
        });

        model.setPostStatusesModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EmployeePostStatus.ENTITY_CLASS, "b");
                builder.add(MQExpression.in("b", EmployeePostStatus.P_CODE, employeePostStatuses));
                builder.add(MQExpression.like("b", EmployeePostStatus.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("b", EmployeePostStatus.P_TITLE, OrderDirection.asc);
                if (o != null)
                    builder.add(MQExpression.eq("b", EmployeePostStatus.P_ID, o));

                return new MQListResultBuilder(builder);
            }
        });

        model.setOrgUnitModel(new OrgUnitAutocompleteModel());
        model.setVacationScheduleModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getOrgUnit() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                MQBuilder builder = new MQBuilder(VacationSchedule.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", VacationSchedule.L_ORG_UNIT, model.getOrgUnit()));
                builder.add(MQExpression.eq("b", VacationSchedule.state().code().s(), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
                if (o != null)
                    builder.add(MQExpression.eq("b", VacationSchedule.P_ID, o));

                return new MQListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return "График отпусков за " + ((VacationSchedule) value).getYear() + "г. от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((VacationSchedule) value).getCreateDate());
            }
        });
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems;
        if (model.getExtract().getEmployeePost() == null)
            staffRateItems = new ArrayList<>();
        else
            staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getExtract().getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareVacationScheduleDataSource(Model model)
    {
        List<VacationScheduleItem> scheduleItemList;
        if (model.getVacationSchedule() == null)
            scheduleItemList = new ArrayList<>();
        else
        {
            MQBuilder builder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", VacationScheduleItem.L_VACATION_SCHEDULE, model.getVacationSchedule()));
            builder.add(MQExpression.eq("b", VacationScheduleItem.employeePost().employee().archival(), false));
            scheduleItemList = builder.getResultList(getSession());
        }

        UniBaseUtils.createPage(model.getVacationScheduleDataSource(), scheduleItemList);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        EmployeeHolidayEmplListExtract extract = model.getExtract();

        if (model.getAddFormat().getId().equals(1L))
        {
            if ((model.getExtract().getPeriodBeginDate() == null || model.getExtract().getPeriodEndDate() == null) &&
                    (model.getExtract().getPeriodBeginDate() != null || model.getExtract().getPeriodEndDate() != null))
                errors.add("Поля периода должны быть заполнены, либо оставаться пустыми.", "periodBeginDate", "periodEndDate");

            if (null != extract.getPeriodBeginDate() && null != extract.getPeriodEndDate() && extract.getPeriodBeginDate().getTime() > extract.getPeriodEndDate().getTime())
                errors.add("Дата начала периода не может быть больше даты его окончания", "periodBeginDate", "periodEndDate", "periodBeginDateReq", "periodEndDateReq");

            if (null != extract.getMainHolidayBeginDate() && null != extract.getMainHolidayEndDate() && extract.getMainHolidayBeginDate().getTime() >= extract.getMainHolidayEndDate().getTime())
                errors.add("Дата начала основного отпуска должна быть меньше даты его окончания", "mainHolidayBeginDate", "mainHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq");

            if (null != extract.getSecondHolidayBeginDate() && null != extract.getSecondHolidayEndDate() && extract.getSecondHolidayBeginDate().getTime() > extract.getSecondHolidayEndDate().getTime())
                errors.add("Дата начала отпуска должна быть меньше даты его окончания", "secondHolidayBeginDate", "secondHolidayEndDate");

            Date[] beginDates = new Date[]{extract.getMainHolidayBeginDate(), extract.getSecondHolidayBeginDate()};
            Date[] endDates = new Date[]{extract.getMainHolidayEndDate(), extract.getSecondHolidayEndDate()};
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                {
                    if (i == j)
                        continue;

                    Date beginFirst = beginDates[i];
                    Date endFirst = endDates[i];
                    Date beginSecond = beginDates[j];
                    Date endSecond = endDates[j];

                    if (beginFirst == null || endFirst == null || beginSecond == null || endSecond == null)
                        continue;

                    if (CommonBaseDateUtil.isBetween(beginFirst, beginSecond, endSecond) || CommonBaseDateUtil.isBetween(endFirst, beginSecond, endSecond))
                        errors.add("Периоды ежегодного и дополнительного отпусков не могут пересекаться.", "mainHolidayBeginDate", "mainHolidayEndDate", "secondHolidayBeginDate", "secondHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq");
                }

            MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
            empHolidayBuilder.add(MQExpression.eq("eh", EmployeeHoliday.L_EMPLOYEE_POST, extract.getEmployeePost()));
            for (int i = 0; i < 2; i++)
                for (EmployeeHoliday holiday : empHolidayBuilder.<EmployeeHoliday>getResultList(getSession()))
                {
                    Date beginDate = beginDates[i];
                    Date endDate = endDates[i];

                    if (beginDate == null || endDate == null)
                        continue;

                    if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                        errors.add((i == 0 ? "Ежегодный отпуск" : "Отпуск") + " пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                i == 0 ? new String[]{"mainHolidayBeginDate", "mainHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq"} : new String[]{"secondHolidayBeginDate", "secondHolidayEndDate"});
                    else {
                        if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                            errors.add((i == 0 ? "Ежегодный отпуск" : "Отпуск") + " пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                    i == 0 ? new String[]{"mainHolidayBeginDate", "mainHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq"} : new String[]{"secondHolidayBeginDate", "secondHolidayEndDate"});
                    }
                }

            if (model.getNeedSecondHoliday())
            {
                if ((model.getExtract().getMainHolidayBeginDate() != null ||
                        model.getExtract().getMainHolidayEndDate() != null ||
                        model.getExtract().getMainHolidayDuration() != null) &&
                        (model.getExtract().getMainHolidayBeginDate() == null ||
                                model.getExtract().getMainHolidayEndDate() == null ||
                                model.getExtract().getMainHolidayDuration() == null))
                    errors.add("Все поля о ежегодном отпуске должны быть заполнены, либо оставаться пустыми.",
                            "mainHolidayBeginDate", "mainHolidayEndDate", "mainHolidayDuration");

                if (null != extract.getMainHolidayEndDate() && null != extract.getSecondHolidayBeginDate() && extract.getSecondHolidayBeginDate().getTime() > extract.getMainHolidayEndDate().getTime()
                        && CommonBaseDateUtil.getDifferenceBetweenDatesInDays(extract.getMainHolidayEndDate(), extract.getSecondHolidayBeginDate()) != 2)
                    errors.add("Период отпуска по одному приказу не должен прерываться",
                            "mainHolidayEndDate", "secondHolidayBeginDate", "mainHolidayEndDateReq");

                if ((extract.getMainHolidayBeginDate() != null && extract.getMainHolidayEndDate() != null && extract.getMainHolidayDuration() != null)
                        && (extract.getPeriodBeginDate() == null || extract.getPeriodEndDate() == null))
                    errors.add("При указании Ежегодного отпуска период отпуска обязателен для заполнения.",
                            "periodBeginDate", "priodEndDate");
            }

            for (TripletKey<Date, Date, EmployeePost> tripletKey : getParagraphsHolidayDatesList(model.getOrder(), model.getExtract()))
            {
                if (!model.getExtract().getEmployeePost().equals(tripletKey.getThird()))
                    continue;

                Date beginFirst = model.getExtract().getBeginDate();
                Date endFirst = model.getExtract().getEndDate();
                Date beginSecond = tripletKey.getFirst();
                Date endSecond = tripletKey.getSecond();

                if (beginFirst == null || endFirst == null || beginSecond == null || endSecond == null)
                    continue;

                if (CommonBaseDateUtil.isBetween(beginFirst, beginSecond, endSecond) || CommonBaseDateUtil.isBetween(endFirst, beginSecond, endSecond))
                    errors.add("Отпуск пересекается с отпуском с " +
                            DateFormatter.DEFAULT_DATE_FORMATTER.format(beginSecond) +
                            " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(endSecond) + ", указанным в другом параграфе списочного приказа.",
                            "mainHolidayBeginDate", "mainHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq", "secondHolidayBeginDate", "secondHolidayEndDate");
                else {
                    if (CommonBaseDateUtil.isBetween(beginSecond, beginFirst, endFirst) || CommonBaseDateUtil.isBetween(endSecond, beginFirst, endFirst))
                        errors.add("Отпуск пересекается с отпуском с " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(beginSecond) +
                                " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(endSecond) + ", указанным в другом параграфе списочного приказа.",
                                "mainHolidayBeginDate", "mainHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq", "secondHolidayBeginDate", "secondHolidayEndDate");
                }
            }

            //если выбран график отпусков
            //проверяем пересечение планируемых отпусков внутри выбранного Графика отпусков
            if (extract.getVacationScheduleItem() != null)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
                builder.where(ne(property(VacationScheduleItem.id().fromAlias("i")), value(extract.getVacationScheduleItem().getId())));
                builder.where(eqValue(property(VacationScheduleItem.employeePost().fromAlias("i")), model.getExtract().getEmployeePost()));
                builder.where(eqValue(property(VacationScheduleItem.vacationSchedule().fromAlias("i")), extract.getVacationSchedule()));

                Date beginDate = extract.getMainHolidayBeginDate();
                Date endDate = extract.getMainHolidayEndDate();

                if (beginDate == null || endDate == null)
                    return;

                for (VacationScheduleItem item : builder.createStatement(getSession()).<VacationScheduleItem>list())
                {
                    Date beginItemDate = item.getFactDate() != null ? item.getFactDate() : item.getPlanDate();
                    Date endItemDate = UniempUtil.getEndDate(model.getExtract().getEmployeePost().getWorkWeekDuration(), item.getDaysAmount(), beginItemDate);

                    if (beginItemDate == null || endItemDate == null)
                        continue;

                    StringBuilder error = new StringBuilder("Предоставляемый отпуск пересекается с планируемым отпуском с ").
                            append(DateFormatter.DEFAULT_DATE_FORMATTER.format(beginItemDate)).
                            append(" на ").
                            append(NumberConvertingUtil.getCalendarDaysWithName(item.getDaysAmount())).
                            append(" из графика отпусков на ").
                            append(item.getVacationSchedule().getYear()).
                            append(" год от ").
                            append(DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getVacationSchedule().getCreateDate())).
                            append(".");

                    if (CommonBaseDateUtil.isBetween(beginDate, beginItemDate, endItemDate) || CommonBaseDateUtil.isBetween(endDate, beginItemDate, endItemDate))
                        errors.add(error.toString(), "mainHolidayBeginDateReq", "mainHolidayEndDateReq", "mainHolidayDurationReq");
                    else {
                        if (CommonBaseDateUtil.isBetween(beginItemDate, beginDate, endDate) || CommonBaseDateUtil.isBetween(endItemDate, beginDate, endDate))
                            errors.add(error.toString(), "mainHolidayBeginDateReq", "mainHolidayEndDateReq", "mainHolidayDurationReq");
                    }
                }
            }
        }
        else if (model.getAddFormat().getId().equals(2L))
        {
            if (model.getVacationScheduleDataSource().getSelectedEntities().isEmpty())
                errors.add("В списке «Сотрудники, включаемые в списочный приказ» не выбрано ни одного сотрудника.");

            final IValueMapHolder periodBeginDHolder = (IValueMapHolder) model.getVacationScheduleDataSource().getColumn("periodBeginDayColumn");
            final IValueMapHolder periodEndDHolder = (IValueMapHolder) model.getVacationScheduleDataSource().getColumn("periodEndDayColumn");

            final Map<Long, Date> periodBeginDMap = (null == periodBeginDHolder ? Collections.emptyMap() : periodBeginDHolder.getValueMap());
            final Map<Long, Date> periodEndDMap = (null == periodEndDHolder ? Collections.emptyMap() : periodEndDHolder.getValueMap());

            List<String> fieldsBeginDErrorId = new ArrayList<>();
            List<String> fieldsEndDErrorId = new ArrayList<>();
            List<String> fieldsCompareDErrorId = new ArrayList<>();

            for (VacationScheduleItem item : (List<VacationScheduleItem>) model.getVacationScheduleDataSource().getSelectedEntities())
            {
                for (VacationScheduleItem item1 : (List<VacationScheduleItem>) model.getVacationScheduleDataSource().getSelectedEntities())
                {
                    if (item.equals(item1))
                        continue;

                    if (item.getEmployeePost().equals(item1.getEmployeePost()))
                        errors.add("Предоставляемый отпуск сотрудника " + item.getEmployeePost().getFio() + " не должен прерываться.");
                }

                Date periodBeginDay = periodBeginDMap.get(item.getId());
                Date periodEndDay = periodEndDMap.get(item.getId());

                if(null == periodBeginDay)
                {
                    fieldsBeginDErrorId.add("periodBeginDay_id_"+item.getId());
                }

                if(null == periodEndDay)
                {
                    fieldsEndDErrorId.add("periodEndDay_id_"+item.getId());
                }

                if(null != periodBeginDay && null != periodEndDay && (periodBeginDay.compareTo(periodEndDay) > 0))
                {
                    fieldsCompareDErrorId.add("periodBeginDay_id_"+item.getId());
                    fieldsCompareDErrorId.add("periodEndDay_id_"+item.getId());
                }

                if (MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeePostHasNotFinishedExtracts(item.getEmployeePost()))
                errors.add("Нельзя добавить параграф, так как у сотрудника '" + item.getEmployeePost().getPerson().getFio() + "' уже есть непроведенные выписки.");
            }

            if(!fieldsBeginDErrorId.isEmpty())
            {
                    errors.add("В списке «Сотрудники, включаемые в списочный приказ» у некоторых сотрудников не указана дата начала периода отпуска.", fieldsBeginDErrorId.toArray(new String[fieldsBeginDErrorId.size()]));
            }

            if(!fieldsEndDErrorId.isEmpty())
            {
                    errors.add("В списке «Сотрудники, включаемые в списочный приказ»  у некоторых сотрудников не указана дата окончания периода отпуска.", fieldsEndDErrorId.toArray(new String[fieldsEndDErrorId.size()]));
            }

            if (!fieldsCompareDErrorId.isEmpty())
            {
                    errors.add("Дата окончания периода, за который предоставляется отпуск, не может быть меньше даты начала периода.", fieldsCompareDErrorId.toArray(new String[fieldsCompareDErrorId.size()]));
            }

            List<Person> errPersonList = new ArrayList<>();
            for (VacationScheduleItem item : (List<VacationScheduleItem>) model.getVacationScheduleDataSource().getSelectedEntities())
                if (item.getPlanDate() == null && item.getFactDate() == null)
                    errPersonList.add(item.getEmployeePost().getPerson());

            if (errPersonList.size() == 1)
                errors.add("У выбранного сотрудника " + CommonExtractUtil.getModifiedFio(errPersonList.get(0), GrammaCase.GENITIVE) + " в Графике отпусков не указана дата начала отпуска.");
            else if (errPersonList.size() > 1)
                errors.add("У некоторых из выбранных сотрудников в Графике отпусков не указана дата начала отпуска.");

            for (TripletKey<Date, Date, EmployeePost> tripletKey : getParagraphsHolidayDatesList(model.getOrder(), null))
                for (VacationScheduleItem item : (List<VacationScheduleItem>) model.getVacationScheduleDataSource().getSelectedEntities())
                {
                    if (!tripletKey.getThird().equals(item.getEmployeePost()))
                        continue;

                    Date beginFirst = tripletKey.getFirst();
                    Date endFirst = tripletKey.getSecond();
                    Date beginSecond = item.getFactDate() != null ? item.getFactDate() : item.getPlanDate();
                    Date endSecond = getEndDate(item.getEmployeePost().getWorkWeekDuration(), item.getDaysAmount(), beginSecond);

                    if (beginFirst == null || endFirst == null || beginSecond == null || endSecond == null)
                        continue;

                    if (CommonBaseDateUtil.isBetween(beginFirst, beginSecond, endSecond) || CommonBaseDateUtil.isBetween(endFirst, beginSecond, endSecond))
                        errors.add("Отпуск сотрудника " + CommonExtractUtil.getModifiedFio(item.getEmployeePost().getPerson(), GrammaCase.GENITIVE) + " пересекается с отпуском с " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(tripletKey.getFirst()) +
                                " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(tripletKey.getSecond()) + ", указанным в другом параграфе списочного приказа.");
                    else {
                        if (CommonBaseDateUtil.isBetween(beginSecond, beginFirst, endFirst) || CommonBaseDateUtil.isBetween(endSecond, beginFirst, endFirst))
                            errors.add("Отпуск сотрудника " + CommonExtractUtil.getModifiedFio(item.getEmployeePost().getPerson(), GrammaCase.GENITIVE) + " пересекается с отпуском с " +
                                    DateFormatter.DEFAULT_DATE_FORMATTER.format(tripletKey.getFirst()) +
                                    " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(tripletKey.getSecond()) + ", указанным в другом параграфе списочного приказа.");
                    }
                }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        if (model.getAddFormat().getId().equals(1L))
        {
            doSaveOrUpdateExtract(model);
        }
        else if (model.getAddFormat().getId().equals(2L))
        {
            getSession().refresh(model.getParagraph().getOrder());
            if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
                throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");

            if (null != model.getOrderId())
                    model.setOrder(getNotNull(EmployeeListOrder.class, model.getOrderId()));

            doSaveExtractList(model);

            List<EmployeeListParagraph> paragraphList = getList(EmployeeListParagraph.class, IAbstractParagraph.L_ORDER,model.getOrder());
            List<ListEmployeeExtract> extractList = new ArrayList<>();
            for (EmployeeListParagraph item : paragraphList)
            {
                extractList.addAll((List<ListEmployeeExtract>)item.getExtractList());
            }
            extractList.sort(ListEmployeeExtract.FULL_FIO_AND_ID_COMPARATOR);

            // нумеруем выписки с единицы
            int counter = 1;
            for (ListEmployeeExtract extract : extractList)
            {
                extract.setNumber(counter++);
                update(extract);
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected List<EmployeeHolidayEmplListExtract> doSaveExtractList(Model model)
    {
        List<EmployeeHolidayEmplListExtract> savedExtractList = new ArrayList<>();

        final IValueMapHolder periodBeginDHolder = (IValueMapHolder) model.getVacationScheduleDataSource().getColumn("periodBeginDayColumn");
        final IValueMapHolder periodEndDHolder = (IValueMapHolder) model.getVacationScheduleDataSource().getColumn("periodEndDayColumn");

        final Map<Long, Date> periodBeginDMap = (null == periodBeginDHolder ? Collections.emptyMap() : periodBeginDHolder.getValueMap());
        final Map<Long, Date> periodEndDMap = (null == periodEndDHolder ? Collections.emptyMap() : periodEndDHolder.getValueMap());

        EmployeePostStatus postStatusNew = getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_REGULAR);
        HolidayType holidayType = getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL);
        EmployeeExtractType extractType = getNotNull(EmployeeExtractType.class, model.getExtractType().getId());
        ExtractStates extractStates = getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER);

        int i = getCount(
                new DQLSelectBuilder().fromEntity(EmployeeListParagraph.class, "p").column(property(EmployeeListParagraph.id().fromAlias("p")))
                        .where(eq(property(EmployeeListParagraph.order().fromAlias("p")), value(model.getOrder())))
        ) + 1;
        for (VacationScheduleItem item : (List<VacationScheduleItem>) model.getVacationScheduleDataSource().getSelectedEntities())
        {
            EmployeeHolidayEmplListExtract extract = createNewExtractInstance(model);

            EmployeeListParagraph paragraph = new EmployeeListParagraph();
            paragraph.setOrder(model.getOrder());
            paragraph.setNumber(i++);

            extract.setState(extractStates);
            extract.setType(extractType);
            extract.setParagraph(paragraph);
            extract.setCreateDate(new Date());
            extract.setReason(model.getExtract().getReason());
            extract.setEmployeePost(item.getEmployeePost());
            extract.setEmployeeFioModified(item.getEmployeePost().getPerson().getFullFio());
            extract.setEmployee(item.getEmployeePost().getEmployee());

            extract.setVacationSchedule(item.getVacationSchedule());
            extract.setVacationScheduleItem(item);
            extract.setHolidayType(holidayType);
            extract.setPeriodBeginDate(periodBeginDMap.get(item.getId()));
            extract.setPeriodEndDate(periodEndDMap.get(item.getId()));
            extract.setMainHolidayBeginDate(item.getFactDate() != null ? item.getFactDate() : item.getPlanDate());
            extract.setMainHolidayEndDate(getEndDate(item.getEmployeePost().getWorkWeekDuration(), item.getDaysAmount(), extract.getMainHolidayBeginDate()));
            extract.setMainHolidayDuration(item.getDaysAmount());
            extract.setBeginDate(extract.getMainHolidayBeginDate());
            extract.setEndDate(extract.getMainHolidayEndDate());
            extract.setDuration(extract.getMainHolidayDuration());
            extract.setEmployeePostStatusNew(postStatusNew);
            extract.setEmployeePostStatusOld(item.getEmployeePost().getPostStatus());

            saveOrUpdate(extract);
            saveOrUpdate(paragraph);

            for (EmployeeOrderBasics basic : model.getSelectedBasicList())
            {
                EmpListExtractToBasicRelation relation = new EmpListExtractToBasicRelation();
                relation.setBasic(basic);
                relation.setExtract(extract);
                relation.setComment(model.getCurrentBasicMap().get(basic.getId()));
                save(relation);
            }

            savedExtractList.add(extract);
        }

        getSession().flush();

        return savedExtractList;
    }

    protected EmployeeHolidayEmplListExtract doSaveOrUpdateExtract(Model model)
    {
        model.getExtract().setEmployeeFioModified(model.getExtract().getEmployeePost().getPerson().getFullFio());
        model.getExtract().setEmployee(model.getExtract().getEmployeePost().getEmployee());
        model.getExtract().setEmployeePostStatusOld(model.getExtract().getEmployeePost().getPostStatus());

        super.update(model);

        return model.getExtract();
    }

    @Override
    protected EmployeeHolidayEmplListExtract createNewExtractInstance(Model model)
    {
        return new EmployeeHolidayEmplListExtract();
    }

    /**
     * @return Возвращает Дату окончания по Дате начала и Длительности, с учетом производственного календаря.
     */
    protected Date getEndDate(EmployeeWorkWeekDuration workWeekDuration, int duration, Date beginDate)
    {
        GregorianCalendar date = (GregorianCalendar) GregorianCalendar.getInstance();
        int counter = 0;

        date.setTime(beginDate);

        if (duration > 0)
        {
            do
            {
                if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                    counter++;

                if (counter != duration)
                    date.add(Calendar.DATE, 1);
            }
            while (counter < duration);


            return date.getTime();
        }

        return null;
    }

    /**
     * @return Возвращает список ключей, содержащих дни начала, конца отпусков и сотрудника из уже созданных параграфов данного приказа.
     */
    protected List<TripletKey<Date, Date, EmployeePost>> getParagraphsHolidayDatesList(AbstractEmployeeOrder order, EmployeeHolidayEmplListExtract thisExtract)
    {
        MQBuilder builder = new MQBuilder(EmployeeHolidayEmplListExtract.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", EmployeeHolidayEmplListExtract.paragraph().order().s(), order));
        if (thisExtract != null)
            builder.add(MQExpression.notEq("b", EmployeeHolidayEmplListExtract.P_ID, thisExtract.getId()));

        List<TripletKey<Date, Date, EmployeePost>> resultList = new ArrayList<>();
        for (EmployeeHolidayEmplListExtract extract : builder.<EmployeeHolidayEmplListExtract>getResultList(getSession()))
            resultList.add(new TripletKey<>(extract.getBeginDate(), extract.getEndDate(), extract.getEmployeePost()));

        return resultList;
    }

    @Override
    public boolean hasVacationSchedule(EmployeePost employeePost)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
        builder.where(eqValue(property(VacationScheduleItem.employeePost().fromAlias("i")), employeePost));
        builder.where(ge(property(VacationScheduleItem.vacationSchedule().year().fromAlias("i")), value(Calendar.getInstance().get(Calendar.YEAR))));
        builder.where(eqValue(property(VacationScheduleItem.vacationSchedule().state().code().fromAlias("i")), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
        return !builder.createStatement(getSession()).list().isEmpty();
    }
}