package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.entity.gen.*;

/**
 * Выписка из сборного приказа по кадровому составу. Исполнение обязанностей временно отсутствующего работника
 */
public class EmployeeActingExtract extends EmployeeActingExtractGen
{
}