/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract;

import org.hibernate.Session;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * @author dseleznev
 * Created on: 10.12.2009
 */
public interface IOrderParagraphListCustomizer
{
    @SuppressWarnings("unchecked")
    void customizeParagraphList(DynamicListDataSource dataSource, IBusinessComponent component);

    void wrap(DynamicListDataSource dataSource, Session session);
}