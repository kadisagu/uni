/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e6;

import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemInner;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 25.09.2009
 */
public class VoluntaryTerminationSExtractDao extends UniBaseDao implements IExtractComponentDao<VoluntaryTerminationSExtract>
{
    @Override
    public void doCommit(VoluntaryTerminationSExtract extract, Map parameters)
    {
        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        extract.setOldEmployeePostStatus(extract.getEntity().getPostStatus());
        extract.getEntity().setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_FIRED));
        extract.getEntity().setDismissalDate(extract.getTerminationDate());

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());
        if (staffRateItemList.size() > 0)
        {
            EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEntity(), staffRateItemList);
            empHistItem.setDismissalDate(extract.getTerminationDate());
            empHistItem.setCurrent(false);
            getSession().saveOrUpdate(empHistItem);
        }

        if (contract != null) {
            extract.setOldContract(contract);
            extract.setOldContractEndDate(contract.getEndDate());
            contract.setEndDate(extract.getTerminationDate());
            update(contract);
        }

        update(extract.getEntity());
        update(extract);
    }

    @Override
    public void doRollback(VoluntaryTerminationSExtract extract, Map parameters)
    {
        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());
        EmployeeLabourContract oldContract = extract.getOldContract();

        extract.getEntity().setDismissalDate(null);
        extract.getEntity().setPostStatus(extract.getOldEmployeePostStatus());

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());
        if (staffRateItemList.size() > 0)
        {
            EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEntity(), staffRateItemList);
            empHistItem.setDismissalDate(null);
            empHistItem.setCurrent(true);
            getSession().saveOrUpdate(empHistItem);
        }

        if (oldContract != null && (contract.getId().equals(oldContract.getId()))) {
            contract.setEndDate(extract.getOldContractEndDate());
            update(contract);
        }

        update(extract.getEntity());

    }
}