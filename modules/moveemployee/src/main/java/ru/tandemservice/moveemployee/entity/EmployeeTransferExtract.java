package ru.tandemservice.moveemployee.entity;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.component.commons.ITransferEmployeePostExtract;
import ru.tandemservice.moveemployee.entity.gen.EmployeeTransferExtractGen;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWeekWorkLoad;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;

public class EmployeeTransferExtract extends EmployeeTransferExtractGen implements IPostAssignExtract, ITransferEmployeePostExtract
{

    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }

    @Override
    public CompetitionAssignmentType getCompetitionType()
    {
        return null;
    }

    @Override
    public void setCompetitionType(CompetitionAssignmentType competitionType)
    {
    }

    @Override
    public EmployeeWeekWorkLoad getWeekWorkLoad()
    {
        return null;
    }

    @Override
    public void setWeekWorkLoad(EmployeeWeekWorkLoad weekWorkLoad)
    {
    }

    @Override
    public EmployeeWorkWeekDuration getWorkWeekDuration()
    {
        return null;
    }

    @Override
    public void setWorkWeekDuration(EmployeeWorkWeekDuration workWeekDuration)
    {
    }

    @Override
    public EmployeePost getEmployeePostOld()
    {
        return getEmployeePost();
    }
}