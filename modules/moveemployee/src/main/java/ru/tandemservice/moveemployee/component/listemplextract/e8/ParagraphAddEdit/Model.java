/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e8.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract;

import java.util.*;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class Model extends AbstractListParagraphAddEditModel<PayForHolidayDayProvideHoldayEmplListExtract>
{
    private DynamicListDataSource<EmployeePost> _employeeDataSource;

    private IMultiSelectModel _orgUnitModel;
    private IMultiSelectModel _employeeTypeModel;
    private IMultiSelectModel _postModel;
    private ISingleSelectModel _woorkTypeModel;

    private List<OrgUnit> _orgUnitList;
    private List<EmployeeType> _employeeTypeList;
    private List<PostBoundedWithQGandQL> _postList;

    private List<EmployeePost> _employeePostList;
    private List<EmployeePost> _selectEmployee;
    private List<EmployeePost> _tempEmployeeList = new ArrayList<>();
    private Boolean _childOrgUnit = false;

    private Map<Long, Date> _employeeIdHolidayDayMap = new HashMap<>();

    //Calculate methods

    public String getHolidayDayId()
    {
        return "holidayDayId_" + getEmployeeDataSource().getCurrentEntity().getId();
    }

    //Getters & Setters

    public DynamicListDataSource<EmployeePost> getEmployeeDataSource()
    {
        return _employeeDataSource;
    }

    public void setEmployeeDataSource(DynamicListDataSource<EmployeePost> employeeDataSource)
    {
        _employeeDataSource = employeeDataSource;
    }

    public IMultiSelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(IMultiSelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public IMultiSelectModel getEmployeeTypeModel()
    {
        return _employeeTypeModel;
    }

    public void setEmployeeTypeModel(IMultiSelectModel employeeTypeModel)
    {
        _employeeTypeModel = employeeTypeModel;
    }

    public IMultiSelectModel getPostModel()
    {
        return _postModel;
    }

    public void setPostModel(IMultiSelectModel postModel)
    {
        _postModel = postModel;
    }

    public ISingleSelectModel getWoorkTypeModel()
    {
        return _woorkTypeModel;
    }

    public void setWoorkTypeModel(ISingleSelectModel woorkTypeModel)
    {
        _woorkTypeModel = woorkTypeModel;
    }

    public List<OrgUnit> getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public List<EmployeeType> getEmployeeTypeList()
    {
        return _employeeTypeList;
    }

    public void setEmployeeTypeList(List<EmployeeType> employeeTypeList)
    {
        _employeeTypeList = employeeTypeList;
    }

    public List<PostBoundedWithQGandQL> getPostList()
    {
        return _postList;
    }

    public void setPostList(List<PostBoundedWithQGandQL> postList)
    {
        _postList = postList;
    }

    public List<EmployeePost> getEmployeePostList()
    {
        return _employeePostList;
    }

    public void setEmployeePostList(List<EmployeePost> employeePostList)
    {
        _employeePostList = employeePostList;
    }

    public List<EmployeePost> getSelectEmployee()
    {
        return _selectEmployee;
    }

    public void setSelectEmployee(List<EmployeePost> selectEmployee)
    {
        _selectEmployee = selectEmployee;
    }

    public List<EmployeePost> getTempEmployeeList()
    {
        return _tempEmployeeList;
    }

    public void setTempEmployeeList(List<EmployeePost> tempEmployeeList)
    {
        _tempEmployeeList = tempEmployeeList;
    }

    public Boolean getChildOrgUnit()
    {
        return _childOrgUnit;
    }

    public void setChildOrgUnit(Boolean childOrgUnit)
    {
        _childOrgUnit = childOrgUnit;
    }

    public Map<Long, Date> getEmployeeIdHolidayDayMap()
    {
        return _employeeIdHolidayDayMap;
    }

    public void setEmployeeIdHolidayDayMap(Map<Long, Date> employeeIdHolidayDayMap)
    {
        _employeeIdHolidayDayMap = employeeIdHolidayDayMap;
    }
}
