/**
 * $Id$
 */
package ru.tandemservice.moveemployee.dao;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 07.06.2010
 */
public class MoveEmployeeUtilDAO extends UniBaseDao implements IMoveEmployeeUtilDAO
{
    @Override
    public List<Payment> getRequiredPaymentsList()
    {
        MQBuilder builder = new MQBuilder(Payment.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE));
        builder.add(MQExpression.eq("p", Payment.P_REQUIRED, Boolean.TRUE));
        return builder.getResultList(getSession());
    }

    @Override
    public List<EmployeeBonus> getExtractEmployeeBonusesList(AbstractEmployeeExtract extract)
    {
        MQBuilder builder = new MQBuilder(EmployeeBonus.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", EmployeeBonus.L_EXTRACT, extract));
        //builder.addOrder(alias, property)
        return builder.getResultList(getSession());
    }

    @Override
    public List<SalaryRaisingCoefficient> getRaisingCoefficientsList(PostBoundedWithQGandQL post, String filter)
    {
        MQBuilder builder = new MQBuilder(SalaryRaisingCoefficient.ENTITY_CLASS, "rc");
        builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_POST, post.getPost()));
        builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_PROF_QUALIFICATION_GROUP, post.getProfQualificationGroup()));
        builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_QUALIFICATION_LEVEL, post.getQualificationLevel()));
        builder.add(MQExpression.like("rc", SalaryRaisingCoefficient.P_TITLE, CoreStringUtils.escapeLike(filter)));
        builder.addOrder("rc", SalaryRaisingCoefficient.P_RAISING_COEFFICIENT);
        builder.addOrder("rc", SalaryRaisingCoefficient.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<OrgUnitTypePostRelation> getPostsList(OrgUnitType orgUnitType, String filter)
    {
        Query q = getSession().createQuery("select prel." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + " from " + OrgUnitTypePostRelation.ENTITY_CLASS + " prel where prel." + OrgUnitTypePostRelation.L_ORG_UNIT_TYPE + "=:orgUnitType and upper(prel." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE + ") like :filter order by prel." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.L_POST + "." + Post.P_TITLE);
        q.setParameter("orgUnitType", orgUnitType);
        q.setParameter("filter", CoreStringUtils.escapeLike(filter));
        return q.list();
    }

    @Override
    public List<Payment> getAvailablePaymentsList(List<Payment> alreadyAddedPayments, String filter)
    {
        MQBuilder builder = new MQBuilder(Payment.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", Payment.P_ACTIVE, Boolean.TRUE));
        builder.add(MQExpression.like("p", Payment.P_TITLE, CoreStringUtils.escapeLike(filter)));
        builder.addOrder("p", Payment.P_TITLE);

        List<Payment> result = new ArrayList<Payment>();
        for(Payment payment : builder.<Payment>getResultList(getSession()))
            if(!alreadyAddedPayments.contains(payment))
                result.add(payment);

        return result;
    }

    @Override
    public ListResult<FinancingSourceItem> getFinancinSourceItemsListResult(String filter)
    {
        MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "fs");
        if (!StringUtils.isEmpty(filter))
            builder.add(MQExpression.like("fs", FinancingSourceItem.title().s(), CoreStringUtils.escapeLike(filter)));
        builder.addOrder("fs", FinancingSourceItem.financingSource().code().s());
        builder.addOrder("fs", FinancingSourceItem.title().s());
        return new ListResult<FinancingSourceItem>(builder.<FinancingSourceItem> getResultList(getSession(), 0, 50), builder.getResultCount(getSession()));
    }

    /**
     * Возвращает строковое представление ставки (бюджет, или вне бюджета)
     * с детализацией источников финансирования по долям ставок.
     */
    @Override
    public String getStaffRateDetailsString(AbstractEmployeeExtract extract, boolean budget)
    {
        MQBuilder builder = new MQBuilder(FinancingSourceDetails.ENTITY_CLASS, "fsd");
        builder.add(MQExpression.eq("fsd", FinancingSourceDetails.extract().s(), extract));
        builder.addDescOrder("fsd", FinancingSourceDetails.staffRate().s());

        List<FinancingSourceDetails> budgetDetails = new ArrayList<FinancingSourceDetails>();
        List<FinancingSourceDetails> offBudgetDetails = new ArrayList<FinancingSourceDetails>();
        for (FinancingSourceDetails item : builder.<FinancingSourceDetails>getResultList(getSession()))
        {
            if (null == item.getFinancingSource() || !UniempDefines.FINANCING_SOURCE_BUDGET.equals(item.getFinancingSource().getCode()))
                offBudgetDetails.add(item);
            else
                budgetDetails.add(item);
        }

        if (budget && budgetDetails.size() == 1)
        {
            FinancingSourceItem item = budgetDetails.get(0).getFinancingSourceItem();
            return null == item ? "" : " " + (null != item.getGenitiveCaseTitle() ? item.getGenitiveCaseTitle() : item.getTitle());
        } else if (!budget && offBudgetDetails.size() == 1)
        {
            FinancingSourceItem item = offBudgetDetails.get(0).getFinancingSourceItem();
            return null == item ? "" : " " + (null != item.getGenitiveCaseTitle() ? item.getGenitiveCaseTitle() : ((null != item.getOrgUnit() && null != item.getOrgUnit().getGenitiveCaseTitle()) ? item.getOrgUnit().getGenitiveCaseTitle() : item.getTitle()));
        } else
        {
            StringBuilder resultBuilder = new StringBuilder((budget ? budgetDetails : offBudgetDetails).size() > 0 ? " (" : "");
            for (FinancingSourceDetails item : (budget ? budgetDetails : offBudgetDetails))
            {
                if (resultBuilder.length() > 3) resultBuilder.append(", ");
                FinancingSourceItem finSrcItem = item.getFinancingSourceItem();
                if (null != finSrcItem)
                {
                    resultBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate())).append(" ставки из ");
                    resultBuilder.append(null != finSrcItem.getGenitiveCaseTitle() ? finSrcItem.getGenitiveCaseTitle() : ((null != finSrcItem.getOrgUnit() && null != finSrcItem.getOrgUnit().getGenitiveCaseTitle()) ? finSrcItem.getOrgUnit().getGenitiveCaseTitle() : finSrcItem.getTitle()));
                }
            }
            resultBuilder.append(resultBuilder.length() > 0 ? ")" : "");
            return resultBuilder.toString();
        }
    }
}