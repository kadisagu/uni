package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.PaymentAssigningExtract;
import ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выплат помеченных на снятие в приказе и приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RemoveEmployeePaymentToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation";
    public static final String ENTITY_NAME = "removeEmployeePaymentToExtractRelation";
    public static final int VERSION_HASH = 284005407;
    private static IEntityMeta ENTITY_META;

    public static final String P_END_DATE_OLD = "endDateOld";
    public static final String L_PAYMENT_ASSIGNING_EXTRACT = "paymentAssigningExtract";
    public static final String L_EMPLOYEE_PAYMENT = "employeePayment";

    private Date _endDateOld;     // Назначена по дату до проведения приказа
    private PaymentAssigningExtract _paymentAssigningExtract;     // Приказ «Об установлении выплат»
    private EmployeePayment _employeePayment;     // Выплата помеченная на снятие

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Назначена по дату до проведения приказа.
     */
    public Date getEndDateOld()
    {
        return _endDateOld;
    }

    /**
     * @param endDateOld Назначена по дату до проведения приказа.
     */
    public void setEndDateOld(Date endDateOld)
    {
        dirty(_endDateOld, endDateOld);
        _endDateOld = endDateOld;
    }

    /**
     * @return Приказ «Об установлении выплат». Свойство не может быть null.
     */
    @NotNull
    public PaymentAssigningExtract getPaymentAssigningExtract()
    {
        return _paymentAssigningExtract;
    }

    /**
     * @param paymentAssigningExtract Приказ «Об установлении выплат». Свойство не может быть null.
     */
    public void setPaymentAssigningExtract(PaymentAssigningExtract paymentAssigningExtract)
    {
        dirty(_paymentAssigningExtract, paymentAssigningExtract);
        _paymentAssigningExtract = paymentAssigningExtract;
    }

    /**
     * @return Выплата помеченная на снятие. Свойство не может быть null.
     */
    @NotNull
    public EmployeePayment getEmployeePayment()
    {
        return _employeePayment;
    }

    /**
     * @param employeePayment Выплата помеченная на снятие. Свойство не может быть null.
     */
    public void setEmployeePayment(EmployeePayment employeePayment)
    {
        dirty(_employeePayment, employeePayment);
        _employeePayment = employeePayment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RemoveEmployeePaymentToExtractRelationGen)
        {
            setEndDateOld(((RemoveEmployeePaymentToExtractRelation)another).getEndDateOld());
            setPaymentAssigningExtract(((RemoveEmployeePaymentToExtractRelation)another).getPaymentAssigningExtract());
            setEmployeePayment(((RemoveEmployeePaymentToExtractRelation)another).getEmployeePayment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RemoveEmployeePaymentToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RemoveEmployeePaymentToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new RemoveEmployeePaymentToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "endDateOld":
                    return obj.getEndDateOld();
                case "paymentAssigningExtract":
                    return obj.getPaymentAssigningExtract();
                case "employeePayment":
                    return obj.getEmployeePayment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "endDateOld":
                    obj.setEndDateOld((Date) value);
                    return;
                case "paymentAssigningExtract":
                    obj.setPaymentAssigningExtract((PaymentAssigningExtract) value);
                    return;
                case "employeePayment":
                    obj.setEmployeePayment((EmployeePayment) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "endDateOld":
                        return true;
                case "paymentAssigningExtract":
                        return true;
                case "employeePayment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "endDateOld":
                    return true;
                case "paymentAssigningExtract":
                    return true;
                case "employeePayment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "endDateOld":
                    return Date.class;
                case "paymentAssigningExtract":
                    return PaymentAssigningExtract.class;
                case "employeePayment":
                    return EmployeePayment.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RemoveEmployeePaymentToExtractRelation> _dslPath = new Path<RemoveEmployeePaymentToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RemoveEmployeePaymentToExtractRelation");
    }
            

    /**
     * @return Назначена по дату до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation#getEndDateOld()
     */
    public static PropertyPath<Date> endDateOld()
    {
        return _dslPath.endDateOld();
    }

    /**
     * @return Приказ «Об установлении выплат». Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation#getPaymentAssigningExtract()
     */
    public static PaymentAssigningExtract.Path<PaymentAssigningExtract> paymentAssigningExtract()
    {
        return _dslPath.paymentAssigningExtract();
    }

    /**
     * @return Выплата помеченная на снятие. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation#getEmployeePayment()
     */
    public static EmployeePayment.Path<EmployeePayment> employeePayment()
    {
        return _dslPath.employeePayment();
    }

    public static class Path<E extends RemoveEmployeePaymentToExtractRelation> extends EntityPath<E>
    {
        private PropertyPath<Date> _endDateOld;
        private PaymentAssigningExtract.Path<PaymentAssigningExtract> _paymentAssigningExtract;
        private EmployeePayment.Path<EmployeePayment> _employeePayment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Назначена по дату до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation#getEndDateOld()
     */
        public PropertyPath<Date> endDateOld()
        {
            if(_endDateOld == null )
                _endDateOld = new PropertyPath<Date>(RemoveEmployeePaymentToExtractRelationGen.P_END_DATE_OLD, this);
            return _endDateOld;
        }

    /**
     * @return Приказ «Об установлении выплат». Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation#getPaymentAssigningExtract()
     */
        public PaymentAssigningExtract.Path<PaymentAssigningExtract> paymentAssigningExtract()
        {
            if(_paymentAssigningExtract == null )
                _paymentAssigningExtract = new PaymentAssigningExtract.Path<PaymentAssigningExtract>(L_PAYMENT_ASSIGNING_EXTRACT, this);
            return _paymentAssigningExtract;
        }

    /**
     * @return Выплата помеченная на снятие. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RemoveEmployeePaymentToExtractRelation#getEmployeePayment()
     */
        public EmployeePayment.Path<EmployeePayment> employeePayment()
        {
            if(_employeePayment == null )
                _employeePayment = new EmployeePayment.Path<EmployeePayment>(L_EMPLOYEE_PAYMENT, this);
            return _employeePayment;
        }

        public Class getEntityClass()
        {
            return RemoveEmployeePaymentToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "removeEmployeePaymentToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
