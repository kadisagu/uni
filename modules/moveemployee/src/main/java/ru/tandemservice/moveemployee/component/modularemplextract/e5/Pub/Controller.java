/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5.Pub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubController;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;

/**
 * @author dseleznev
 * Created on: 03.12.2008
 */
public class Controller extends ModularEmployeeExtractPubController<EmployeeHolidayExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareHolidayDataSource(component);
    }

    public void prepareHolidayDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getHolidayDataSource() != null)
            return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
                getDao().prepareHolidayDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Отпуск за период", Model.PERIOD).setFormatter(RowCollectionFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Отпуск", Model.HOLIDAY).setFormatter(RowCollectionFormatter.INSTANCE).setClickable(false).setOrderable(false));

        model.setHolidayDataSource(dataSource);
    }
}