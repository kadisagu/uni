/**
 * $Id$
 */
package ru.tandemservice.moveemployee;

/**
 * @author dseleznev
 * Created on: 11.11.2008
 */
public interface IMoveEmployeeComponents
{
    String MODULAR_EMPLOYEE_EXTRACT_PRINT = "ru.tandemservice.moveemployee.component.orderempl.modular.ModularEmployeeExtractPrint";
    String REASON_PRINT_TITLE_SETTINGS_EDIT = "ru.tandemservice.moveemployee.component.settings.ReasonPrintTitleEmplSettingsEdit";
    String REASON_TO_BASICS_SETTINGS_PUB = "ru.tandemservice.moveemployee.component.settings.ReasonToBasicsEmplSettingsPub";
    String REASON_TO_BASICS_SETTINGS_ADD = "ru.tandemservice.moveemployee.component.settings.ReasonToBasicsEmplSettingsAdd";
    String REASON_TO_BASICS_SETTINGS_EDIT = "ru.tandemservice.moveemployee.component.settings.ReasonToBasicsEmplSettingsEdit";
    String REASON_TO_TYPES_SETTINGS_EDIT = "ru.tandemservice.moveemployee.component.settings.ReasonToTypesEmplSettingsEdit";
    String REASON_TO_TYPES_SETTINGS_PUB = "ru.tandemservice.moveemployee.component.settings.ReasonToTypesEmplSettingsPub";
    String REASON_TO_TYPES_SETTINGS_ADD = "ru.tandemservice.moveemployee.component.settings.ReasonToTypesEmplSettingsAdd";
    String REASON_TO_TEXT_PARAGRAPH_EDIT = "ru.tandemservice.moveemployee.component.settings.EmployeeReasonToTextParagraphSettings.EmployeeReasonToTextParagraphSettingsEdit";
    String EMPLOYEE_MODULAR_ORDER_ADD = "ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderAdd";
    String EMPLOYEE_MODULAR_ORDER_EDIT = "ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderEdit";
    String EMPLOYEE_MODULAR_ORDER_ADD_EXTRACTS = "ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderAddExtracts";
    String EMPLOYEE_MODULAR_ORDER_PRINT = "ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderPrint";
    String STUDENT_ORDER_SET_EXECUTOR = "ru.tandemservice.moveemployee.component.orderempl.EmployeeOrderSetExecutor";

    String EMPLOYEE_LIST_ORDER_ADD = "ru.tandemservice.moveemployee.component.orderempl.list.EmployeeListOrderAdd";
    String EMPLOYEE_LIST_ORDER_PRINT = "ru.tandemservice.moveemployee.component.orderempl.list.EmployeeListOrderPrint";
    String EMPLOYEE_PARAGRAPH_LIST_ORDER_PRINT = "ru.tandemservice.moveemployee.component.orderempl.list.EmployeeParagraphListOrderPrint";

    String EMPLOYEE_SINGLE_ORDER_EDIT = "ru.tandemservice.moveemployee.component.orderempl.single.EmployeeSingleOrderEdit";

    String EMPLOYEE_POST_MODULAR_EXTRACTS_ADD = "ru.tandemservice.moveemployee.component.modularemplextract.e0.AddEdit";
    String EMPLOYEE_POST_SINGLE_EXTRACTS_ADD = "ru.tandemservice.moveemployee.component.singleemplextract.e0.AddEdit";

    String ORDER_REASON_TO_DISMISS_ORDER_REL_ADD_EDIT = "ru.tandemservice.moveemployee.component.settings.ReasonToDismissReasonAddEdit";

    String PAYMENT_PRINT_FORMAT_CASES_EDIT = "ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatCasesEdit";
    String PAYMENT_STRING_REPR_FORMAT_ADDEDIT = "ru.tandemservice.moveemployee.component.settings.PaymentStringReprFormatAddEdit";
    String PAYMENT_PRINT_FORMAT_SEPARATOR_EDIT = "ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSeparatorEdit";

    String ENCOURAGEMENT_PRINT_FORMAT_EDIT = "ru.tandemservice.moveemployee.component.settings.encouragementPrintFormatSettings.EncouragementPrintFormatEdit";
}
