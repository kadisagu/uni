package ru.tandemservice.moveemployee.entity;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.component.commons.ITransferEmployeePostExtract;
import ru.tandemservice.moveemployee.entity.gen.EmployeeTransferSExtractGen;

/**
 * Выписка из индивидуального приказа по кадровому составу. О переводе
 */
public class EmployeeTransferSExtract extends EmployeeTransferSExtractGen implements IPostAssignExtract, ITransferEmployeePostExtract
{
    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }

    @Override
    public EmployeePost getEmployeePostOld()
    {
        return getEmployeePost();
    }
}