/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e23.Pub;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.moveemployee.entity.IncDecCombinationExtract;

import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 23.12.2011
 */
public class DAO extends ModularEmployeeExtractPubDAO<IncDecCombinationExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        StringBuilder staffRateBuilder = new StringBuilder();
        List<CombinationPostStaffRateExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getCombinationPostStaffRateExtractRelationList(model.getExtract());
        if (relationList.size() > 0)
        {
            Double value = 0d;
            for (CombinationPostStaffRateExtractRelation relation : relationList)
                value += relation.getStaffRate();

            staffRateBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value));
            staffRateBuilder.append("<p/>");

            for (CombinationPostStaffRateExtractRelation relation : relationList)
            {
                staffRateBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(relation.getStaffRate()));
                staffRateBuilder.append(" - ");
                staffRateBuilder.append(relation.getFinancingSource().getTitle());
                if (relation.getFinancingSourceItem() != null)
                    staffRateBuilder.append(" (" + relation.getFinancingSourceItem().getTitle() + ")");

                staffRateBuilder.append("<p/>");
            }
        }

        model.setStaffRateStr(staffRateBuilder.toString());
    }
}