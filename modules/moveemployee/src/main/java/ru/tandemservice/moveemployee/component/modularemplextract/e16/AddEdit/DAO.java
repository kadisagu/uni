/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e16.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;

import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 08.04.2011
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<ReturnFromChildCareHolidayExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected ReturnFromChildCareHolidayExtract createNewInstance()
    {
        return new ReturnFromChildCareHolidayExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if(null == model.getExtract().getEntity() && null != model.getEmployeePost())
            model.getExtract().setEntity(model.getEmployeePost());
        else if(null == model.getEmployeePost() || null == model.getEmployeePost().getId())
            model.setEmployeePost(model.getExtract().getEntity());

        //заполняем ФИО в дательном падеже
        IdentityCard identityCard = null != model.getEmployeePost().getId() ? model.getEmployeePost().getPerson().getIdentityCard() : model.getEmployee().getPerson().getIdentityCard();
        boolean isMaleSex = identityCard.getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.DATIVE, isMaleSex));
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), GrammaCase.DATIVE, isMaleSex));
        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), GrammaCase.DATIVE, isMaleSex));
        model.getExtract().setDativeFio(str.toString());

        //заполняем select Тип конкурсного назначения на должность
        model.setAssignmentTypesList(getCatalogItemList(CompetitionAssignmentType.class));
        if (model.getEmployeePost().getCompetitionType() != null)
                model.getExtract().setCompetitionType(model.getEmployeePost().getCompetitionType());

        //определяем какие источники финансирования показывать
        CommonEmployeeExtractUtil.fillFinSrcModel(model, model.getExtract().isFreelance(), model.getEmployeePost().getOrgUnit(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL());

        if (model.isEditForm())
        {
            List<FinancingSourceDetails> financingSourceDetailsList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(model.getExtract());
            model.setStaffRateItemList(financingSourceDetailsList);
            CommonEmployeeExtractUtil.prepareColumnsStaffRateSearchList(financingSourceDetailsList, model.getStaffRateDataSource());

            //поднимаем релейшены долей ставок и выбранных ставок ШР
            model.setDetailsToAllocItemList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(financingSourceDetailsList));
        }

        //заполняем Ставки
        if (model.isAddForm())
        {
            List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
            List<FinancingSourceDetails> detailsList = new ArrayList<>();
            //переводим данные из одной сущности в другую
            for (EmployeePostStaffRateItem item : staffRateItems)
            {
                FinancingSourceDetails details = new FinancingSourceDetails();
                short discriminator = EntityRuntime.getMeta(FinancingSourceDetails.class).getEntityCode();
                long id = EntityIDGenerator.generateNewId(discriminator);
                details.setId(id);
                details.setExtract(model.getExtract());
                details.setFinancingSource(item.getFinancingSource());
                details.setFinancingSourceItem(item.getFinancingSourceItem());
                details.setStaffRate(item.getStaffRate());
                detailsList.add(details);
            }


            CommonEmployeeExtractUtil.prepareColumnsStaffRateSearchList(detailsList, model.getStaffRateDataSource());

            model.setStaffRateItemList(detailsList);

            prepareStaffRateDataSource(model);
        }

        //заполняем select Вид отпуска
        MQBuilder holidayTypebuilder = new MQBuilder(HolidayType.ENTITY_CLASS, "ht");
        holidayTypebuilder.add(MQExpression.in("ht", HolidayType.code().s(), UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_1_5, UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_3));
        model.setHolidayTypeList(holidayTypebuilder.<HolidayType>getResultList(getSession()));

        //заполняем Дата окончания по умолчанию
        if (model.getEmployeePost().getDismissalDate() != null)
            model.getExtract().setEndDate(model.getEmployeePost().getDismissalDate());

        //заполняем paymentModel
        MQBuilder employeePaymentbuilder = new MQBuilder(EmployeePayment.ENTITY_CLASS, "ep");
        employeePaymentbuilder.add(MQExpression.eq("ep", EmployeePayment.employeePost().s(), model.getEmployeePost()));
        employeePaymentbuilder.addDescOrder("ep", EmployeePayment.assignDate().s());
        model.setPaymentList(employeePaymentbuilder.<EmployeePayment>getResultList(getSession()));

        //заполняем модель Ист.фин. в списке Ставка
        model.setFinancingSourceItemModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                for (IEntity item : findValues("").getObjects())
                    if (item.getId().equals(primaryKey))
                        return item;

                return null;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<FinancingSourceItem> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                FinancingSource financingSource = finSrcMap.get(id);
                if (model.isThereAnyActiveStaffList())
                {
                    return new ListResult<>(UniempDaoFacade.getStaffListDAO().getFinancingSourcesItemStaffListItems(model.getEmployeePost().getOrgUnit(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), financingSource, filter));
                } else
                    return new ListResult<>(UniempDaoFacade.getUniempDAO().getFinSrcItm(financingSource.getId(), filter));
            }
        });

        //заполняем модель Кадровая расстановка в списке Ставка
        model.setEmployeeHRModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> resultList = new ArrayList<>();

                for (IdentifiableWrapper wrapper : findValues("").getObjects())
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);

                return resultList;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
                final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();
                OrgUnit orgUnit = model.getEmployeePost().getOrgUnit();
                PostBoundedWithQGandQL post = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

                if (finSrcMap.get(id) == null || orgUnit == null || post == null)
                    return ListResult.getEmpty();

                String NEW_STAFF_RATE_STR = "<Новая ставка> - ";

                List<IdentifiableWrapper> resultList = new ArrayList<>();

                List<StaffListAllocationItem> staffListAllocList = UniempDaoFacade.getStaffListDAO().getOccupiedStaffListAllocationItem(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id));

                if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id)))
                {
                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id));
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
                    for (StaffListAllocationItem item : staffListAllocList)
                        if (!item.getEmployeePost().getPostStatus().isActive())
                            diff -= item.getStaffRate();
                    if (diff > 0 && NEW_STAFF_RATE_STR.contains(filter))
                        resultList.add(new IdentifiableWrapper(0L, NEW_STAFF_RATE_STR + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                for (StaffListAllocationItem item : staffListAllocList)
                {
                    if (item.getEmployeePost().getPerson().getFullFio().contains(filter))
                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                return new ListResult<>(resultList);
            }
        });

        //устанавливаем доступноть чекбокса
        if (model.getExtract().getHolidayType() != null)
            if (UniempDefines.HOLIDAY_TYPE_WOMAN_WHO_HAS_A_CHILD_ABOVE_1_5.equals(model.getExtract().getHolidayType().getCode()) &&
                    (CommonEmployeeExtractUtil.getSummStaffRateExtract(model.getStaffRateDataSource(), model.getStaffRateItemList())) < 1)
                model.setSavePaymentDisable(false);
            else
                model.setSavePaymentDisable(true);
        else
            model.setSavePaymentDisable(true);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateItemList());

        CommonEmployeeExtractUtil.validateStaffRate(model.getStaffRateDataSource(), model.getStaffRateItemList(), model.getEmployeePost().getPostType().getCode(), errors);

        CommonEmployeeExtractUtil.validateFinSrcDetToAllocItem(model, model.getEmployeePost().getOrgUnit(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), errors);
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.isAddForm())
        {
            if (model.getEmployeePost().getCompetitionType() != null)
                model.getExtract().setCompetitionTypeOld(model.getEmployeePost().getCompetitionType());

            model.getExtract().setStateOld(model.getEmployeePost().getPostStatus());
        }

        if (model.getExtract().getSavePayment() == null || !model.getExtract().getSavePayment())
        {
            model.getExtract().setPayment(null);
            model.getExtract().setStartDate(null);
            model.getExtract().setFinishDate(null);
        }

        CommonEmployeeExtractUtil.updateStaffRateList(model, model.isEditForm(), getSession());
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getStaffRateItemList());

        if (model.isNeedUpdateDataSource())
        {
            CommonEmployeeExtractUtil.prepareColumnsStaffRateSearchList(model.getStaffRateItemList(), model.getStaffRateDataSource());
            model.setNeedUpdateDataSource(false);
        }
    }
}