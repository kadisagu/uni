/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderAdd;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.moveemployee.component.commons.ExtractListUtil;
import ru.tandemservice.moveemployee.dao.ModularOrder.IEmployeeModularOrderParagraphsSortDAO;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;
import ru.tandemservice.moveemployee.entity.EmployeeModularParagraph;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.Date;
import java.util.List;

/**
 * @author dseleznev
 *         Created on: 10.11.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("e");
    
    static
    {
        _orderSettings.setOrders(ModularEmployeeExtract.EMPLOYEE_FIO_KEY, new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME));
    }

    @Override
    public void prepare(Model model)
    {
        // init main params
        model.getOrder().setCreateDate(new Date());
        model.getOrder().setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        ExtractListUtil.initExtractListModel(model, "employeeModularOrderAdd");
        model.setEmployeePostModel(new OrderExecutorSelectModel());
        model.setParagraphsSortRuleList(getList(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_DAO_NAME));
        model.getOrder().setParagraphsSortRule(get(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_ACTIVE, Boolean.TRUE));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        Date extractCreationDate = (Date) model.getSettings().get("extractCreateDate");
        String employeeLastName = (String) model.getSettings().get("lastName");
        EmployeeExtractType extractType = (EmployeeExtractType) model.getSettings().get("extractType");

        MQBuilder builder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "e");
        builder.addJoinFetch("e", ModularEmployeeExtract.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD, "idCard");
        builder.add(MQExpression.eq("e", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));

        if (null != extractCreationDate)
            builder.add(UniMQExpression.eqDate("e", IAbstractExtract.P_CREATE_DATE, extractCreationDate));
        if (null != employeeLastName)
            builder.add(MQExpression.like("e", ModularEmployeeExtract.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, "%" + employeeLastName));
        if (null != extractType)
            builder.add(MQExpression.eq("e", IAbstractExtract.L_TYPE, extractType));

        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        String orderNumber = model.getOrder().getNumber();
        if (orderNumber != null)
        {
            orderNumber = orderNumber.trim();
            List<AbstractEmployeeOrder> orderList = getList(AbstractEmployeeOrder.class, IAbstractOrder.P_NUMBER, (Object) orderNumber);
            if (orderList.size() > 0)
                errors.add("Номер приказа должен быть уникальным в рамках всех типов приказов по кадровому составу.", "number");
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        save(model.getOrder());

        // save executor
        String executor = OrderExecutorSelectModel.getExecutor(model.getEmployeePost());
        if (executor != null) model.getOrder().setExecutor(executor);

        int count = 1;
        for (IEntity entity : ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects())
        {
            IAbstractExtract extract = (IAbstractExtract) entity;
            if (extract.getParagraph() != null)
                throw new ApplicationException(extract.getTitle() + " уже добавлена в приказ №" + extract.getParagraph().getOrder().getNumber() + ".");

            if (!UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED.equals(extract.getState().getCode()))
                throw new ApplicationException(extract.getTitle() + " не согласован.");

            //create paragraph
            EmployeeModularParagraph paragraph = new EmployeeModularParagraph();
            paragraph.setNumber(count++);
            paragraph.setOrder(model.getOrder());
            save(paragraph);

            //update extract
            extract.setNumber(1);
            extract.setParagraph(paragraph);
            extract.setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
            update(extract);
        }

        IEmployeeModularOrderParagraphsSortDAO sorter = (IEmployeeModularOrderParagraphsSortDAO) ApplicationRuntime.getBean(model.getOrder().getParagraphsSortRule().getDaoName());
        sorter.sortExtracts(model.getOrder());
    }
}