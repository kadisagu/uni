/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e24;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.entity.RemovalCombinationExtract;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.codes.CombinationPostTypeCodes;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 29.12.2011
 */
public class RemovalCombinationExtractPrint implements IPrintFormCreator<RemovalCombinationExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RemovalCombinationExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("secondEmploymentEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getRemovalDate()));

        StringBuilder typeStr = new StringBuilder();
        if (extract.getCombinationPost().getCombinationPostType().getCode().equals(CombinationPostTypeCodes.INC_WORK))
            modifier.put("secondEmploymentTypeStr", "увеличение объема выполняемой работы по одноименной должности");
        else
            modifier.put("secondEmploymentTypeStr", "работу по совмещению должности");

        CommonExtractPrintUtil.injectPost(modifier, extract.getCombinationPost().getPostBoundedWithQGandQL(), "combination");

        CommonExtractPrintUtil.injectOrgUnit(modifier, extract.getCombinationPost().getOrgUnit(), "combination");

        List<CombinationPostStaffRateItem> combinationPostStaffRateItemList = UniempDaoFacade.getUniempDAO().getCombinationPostStaffRateItemList(extract.getCombinationPost());
        Double staffRate = 0d;
        for (CombinationPostStaffRateItem item :combinationPostStaffRateItemList)
            staffRate += item.getStaffRate();

            BigDecimal x = new java.math.BigDecimal(staffRate);
            x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
            staffRate = x.doubleValue();

            staffRate = staffRate * 100;

            modifier.put("procStaffRate", String.valueOf(staffRate.longValue()) + "% от");

        if (extract.getCombinationPost().getSalaryRaisingCoefficient() != null)
            modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getCombinationPost().getSalaryRaisingCoefficient().getRecommendedSalary()));
        else
            modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getCombinationPost().getPostBoundedWithQGandQL().getSalary()));



        modifier.modify(document);
        return document;
    }
}