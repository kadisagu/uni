/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e13.AddEdit;

import java.util.List;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.DismissalExtract;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 14.03.2011
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<DismissalExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected DismissalExtract createNewInstance()
    {
        return new DismissalExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);


        model.setOrderReasonUpdates(model.getOrderReasonUpdates() + ", dismissalReason, lawItem, relativeBlock");

        model.setFinancingSourcesList(getCatalogItemList(FinancingSource.class));

        if (null != model.getExtract().getDismissalReason())
            model.setNeedRelativeData(model.getExtract().getDismissalReason().isSalaryDelegating());

        model.setRelativeTypesList(getCatalogItemListOrderByUserCode(RelationDegree.class));

        if (model.getExtract().getEntity() != null)
        {
            model.setEmployeePost(model.getExtract().getEntity());
        }

        //доп условия
        if (model.getEmployeePost().getInsteadOfEmployeePost() != null)
        {
            String strAccept;

            IdentityCard identityCard =
                null != model.getEmployeePost().getInsteadOfEmployeePost().getId() ? model.getEmployeePost().getInsteadOfEmployeePost().getPerson().getIdentityCard()
                        : model.getEmployeePost().getInsteadOfEmployeePost().getEmployee().getPerson().getIdentityCard();

                boolean isMaleSex = identityCard.getSex().isMale();

                if (isMaleSex)
                    strAccept =  "принятого";
                else
                    strAccept =  "принятую";

                model.getExtract().setAdditionalConditions(strAccept + " временно на период _ " + getGenitiveFioInsteadOfEmployeePost(identityCard));
        }
    }

    @Override
    public EmployeeDismissReasons getEmployeeDismissalReason(EmployeeOrderReasons reason)
    {
        MQBuilder builder = new MQBuilder(EmployeeReasonToDismissReasonRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", EmployeeReasonToDismissReasonRel.L_EMPLOYEE_ORDER_REASON, reason));
        EmployeeReasonToDismissReasonRel rel = (EmployeeReasonToDismissReasonRel)builder.uniqueResult(getSession());
        if(rel != null) return rel.getEmployeeDismissReason();
        return null;
    }

    public String getGenitiveFioInsteadOfEmployeePost(IdentityCard identityCard)
    {
        String result;

        result = PersonManager.instance().declinationDao().getCalculatedFIODeclination(identityCard, InflectorVariantCodes.RU_GENITIVE);
        if (result != null)
            return result;
        else
            return null;
    }

    @Override
    public void update(Model model)
    {
        DismissalExtract extract = model.getExtract();

        extract.setEmployeePostStatusOld(model.getEmployeePost().getPostStatus());
        extract.setDismissalDateOld(model.getEmployeePost().getDismissalDate());

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());

        if(null != extract.getDismissalReason() && !extract.getDismissalReason().isSalaryDelegating())
        {
            extract.setRelativeType(null);
            extract.setRelativeFio(null);
            extract.setIdentityCard(null);
        }

        super.update(model);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        DismissalExtract extract = model.getExtract();
        if(null == extract.getDismissalReason())
            errors.add("Необходимо настроить связь причины приказа с причиной увольнения");
        super.validate(model, errors);
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }
}
