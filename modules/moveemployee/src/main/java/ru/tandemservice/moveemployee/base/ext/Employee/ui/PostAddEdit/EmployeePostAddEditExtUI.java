/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.moveemployee.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
public class EmployeePostAddEditExtUI extends UIAddon
{
    public EmployeePostAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

//    @Override
//    public void onValidateExt()
//    {
//        EmployeePost employeePost = ((EmployeePostAddEditUI) getPresenter()).getEmployeePost();
//
//        int count = 0;
//        if (null == employeePost.getOrderNumber())
//            count++;
//        if (null == employeePost.getOrderDate())
//            count++;
//        if (1 == count)
//            ContextLocal.getErrorCollector().add("Поля «Номер приказа» и «Дата проведения приказа» обязательны для заполнения одновременно.", "orderNumber", "orderDate");
//    }

    public boolean isHasNoOrder()
    {
        EmployeePost employeePost = ((EmployeePostAddEditUI) getPresenter()).getEmployeePost();
        if (employeePost.getId() == null) return true;

        MQBuilder builder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "ee");
        builder.add(MQExpression.eq("ee", IAbstractExtract.L_ENTITY, employeePost));
        builder.add(MQExpression.eq("ee", IAbstractExtract.L_STATE + "." + ExtractStates.P_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        builder.addOrder("ee", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + AbstractEmployeeOrder.P_COMMIT_DATE, OrderDirection.asc);

        for (AbstractEmployeeExtract extract : builder.<AbstractEmployeeExtract>getResultList(getSession()))
            if (extract instanceof IPostAssignExtract)
                return false;
        return true;
    }
}
