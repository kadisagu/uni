/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e6.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.catalog.entity.HolidayDuration;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel;
import ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.List;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 25.09.2009
 */
public class DAO extends CommonSingleEmployeeExtractAddEditDAO<VoluntaryTerminationSExtract, Model> implements IDAO
{
    @Override
    protected VoluntaryTerminationSExtract createNewInstance()
    {
        return new VoluntaryTerminationSExtract();
    }

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setOrderReasonUpdates(model.getOrderReasonUpdates() + ", dismissReason, lawItem, relativeBlock");
        model.setRelativeTypesList(getCatalogItemListOrderByUserCode(RelationDegree.class));

        if(!model.getReasonList().isEmpty() && model.getReasonList().size() == 1)
        {
            model.getExtract().setDismissReason(getEmployeeDismissReason(model.getExtract().getReason()));
            model.setNeedRelativeData(null != model.getExtract().getDismissReason() && model.getExtract().getDismissReason().isSalaryDelegating());
        }

        if(null != model.getExtract().getDismissReason())
            model.setNeedRelativeData(model.getExtract().getDismissReason().isSalaryDelegating());

        if(model.isAddForm())
            model.getExtract().setEntity(model.getEmployeePost());
        else
            model.setEmployeePost(model.getExtract().getEntity());
    }

    @Override
    public void update(Model model)
    {
        VoluntaryTerminationSExtract extract = model.getExtract();
        extract.setOldEmployeePostStatus(extract.getEntity().getPostStatus());
        if(null != extract.getDismissReason() && !extract.getDismissReason().isSalaryDelegating())
        {
            extract.setRelativeType(null);
            extract.setRelativeFio(null);
            extract.setIdentityCard(null);
        }
        //TODO: validation
        super.update(model);
    }

    @Override
    public EmployeeDismissReasons getEmployeeDismissReason(EmployeeOrderReasons reason)
    {
        MQBuilder builder = new MQBuilder(EmployeeReasonToDismissReasonRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", EmployeeReasonToDismissReasonRel.L_EMPLOYEE_ORDER_REASON, reason));
        EmployeeReasonToDismissReasonRel rel = (EmployeeReasonToDismissReasonRel)builder.uniqueResult(getSession());
        if(rel != null) return rel.getEmployeeDismissReason();
        return null;
    }

    @Override
    public void fillPaymentChargeAndHolidayDaysAmount(VoluntaryTerminationSExtract extract)
    {
        if (null == extract.getTerminationDate()) return;
        if (!(UniDefines.POST_TYPE_MAIN_JOB.equals(extract.getEntity().getPostType().getCode()) || extract.getEntity().isMainJob())) return;

        // TODO: Probably, we should take into account holidays of other post(s), if employee was transferred to the current post
        MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
        builder.add(MQExpression.eq("eh", EmployeeHoliday.employeePost().s(), extract.getEntity()));
        builder.addOrder("eh", EmployeeHoliday.startDate().s());

        int usedHolidaySumDaysAmount = 0;
        for (EmployeeHoliday holiday : builder.<EmployeeHoliday> getResultList(getSession()))
        {
            if (holiday.getHolidayType().isCompensable())
                usedHolidaySumDaysAmount += holiday.getDuration();
        }

        int normalHolidayDuration = 28;
        HolidayDuration employeeHolidayDuration = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getPost().getEmployeeType().getHolidayDuration();
        if (null != employeeHolidayDuration)
            normalHolidayDuration = employeeHolidayDuration.getDaysAmount();

        Integer daysAmountForTheWholeWorkPeriod = CommonBaseDateUtil.getDifferenceBetweenDatesInDays(extract.getEntity().getPostDate(), extract.getTerminationDate());
        double fullHolidaySumDaysAmount = normalHolidayDuration * (daysAmountForTheWholeWorkPeriod / 365d);

        double difference = fullHolidaySumDaysAmount - usedHolidaySumDaysAmount;
        if (difference > 0)
        {
            extract.setHolidayDaysAmount(difference);
            extract.setPaymentCharge(0d);
        }
        else
        {
            extract.setPaymentCharge(-difference);
            extract.setHolidayDaysAmount(0d);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        VoluntaryTerminationSExtract extract = model.getExtract();
        if(null == extract.getDismissReason())
            errors.add("Необходимо настроить связь причины приказа с причиной увольнения");
        super.validate(model, errors);
    }


    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }
}