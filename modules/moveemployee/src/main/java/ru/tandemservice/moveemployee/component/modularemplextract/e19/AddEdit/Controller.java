/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e19.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.EmployeeActingExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.11.2011
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<EmployeeActingExtract, IDAO, Model>
{
    public void onChangeMissingEmployeePost(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getExtract().getMissingEmployeePost() != null)
        {
            model.getExtract().setMissingOrgUnit(model.getExtract().getMissingEmployeePost().getOrgUnit());
            model.getExtract().setMissingPost(model.getExtract().getMissingEmployeePost().getPostRelation().getPostBoundedWithQGandQL());
        }
    }
}