package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. О поощрении работника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeEncouragementSExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract";
    public static final String ENTITY_NAME = "employeeEncouragementSExtract";
    public static final int VERSION_HASH = -132975247;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENCOURAGEMENT_REASON = "encouragementReason";
    public static final String P_AMOUNT = "amount";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";

    private String _encouragementReason;     // Мотив поощрения
    private Double _amount;     // Сумма, руб
    private FinancingSource _financingSource;     // Источник финансирования (настройка названий)
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (настройка названий, детально)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Мотив поощрения. Свойство не может быть null.
     */
    @NotNull
    public String getEncouragementReason()
    {
        return _encouragementReason;
    }

    /**
     * @param encouragementReason Мотив поощрения. Свойство не может быть null.
     */
    public void setEncouragementReason(String encouragementReason)
    {
        dirty(_encouragementReason, encouragementReason);
        _encouragementReason = encouragementReason;
    }

    /**
     * @return Сумма, руб.
     */
    public Double getAmount()
    {
        return _amount;
    }

    /**
     * @param amount Сумма, руб.
     */
    public void setAmount(Double amount)
    {
        dirty(_amount, amount);
        _amount = amount;
    }

    /**
     * @return Источник финансирования (настройка названий).
     */
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования (настройка названий).
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (настройка названий, детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeEncouragementSExtractGen)
        {
            setEncouragementReason(((EmployeeEncouragementSExtract)another).getEncouragementReason());
            setAmount(((EmployeeEncouragementSExtract)another).getAmount());
            setFinancingSource(((EmployeeEncouragementSExtract)another).getFinancingSource());
            setFinancingSourceItem(((EmployeeEncouragementSExtract)another).getFinancingSourceItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeEncouragementSExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeEncouragementSExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeEncouragementSExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "encouragementReason":
                    return obj.getEncouragementReason();
                case "amount":
                    return obj.getAmount();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "encouragementReason":
                    obj.setEncouragementReason((String) value);
                    return;
                case "amount":
                    obj.setAmount((Double) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "encouragementReason":
                        return true;
                case "amount":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "encouragementReason":
                    return true;
                case "amount":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "encouragementReason":
                    return String.class;
                case "amount":
                    return Double.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeEncouragementSExtract> _dslPath = new Path<EmployeeEncouragementSExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeEncouragementSExtract");
    }
            

    /**
     * @return Мотив поощрения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract#getEncouragementReason()
     */
    public static PropertyPath<String> encouragementReason()
    {
        return _dslPath.encouragementReason();
    }

    /**
     * @return Сумма, руб.
     * @see ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract#getAmount()
     */
    public static PropertyPath<Double> amount()
    {
        return _dslPath.amount();
    }

    /**
     * @return Источник финансирования (настройка названий).
     * @see ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    public static class Path<E extends EmployeeEncouragementSExtract> extends SingleEmployeeExtract.Path<E>
    {
        private PropertyPath<String> _encouragementReason;
        private PropertyPath<Double> _amount;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Мотив поощрения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract#getEncouragementReason()
     */
        public PropertyPath<String> encouragementReason()
        {
            if(_encouragementReason == null )
                _encouragementReason = new PropertyPath<String>(EmployeeEncouragementSExtractGen.P_ENCOURAGEMENT_REASON, this);
            return _encouragementReason;
        }

    /**
     * @return Сумма, руб.
     * @see ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract#getAmount()
     */
        public PropertyPath<Double> amount()
        {
            if(_amount == null )
                _amount = new PropertyPath<Double>(EmployeeEncouragementSExtractGen.P_AMOUNT, this);
            return _amount;
        }

    /**
     * @return Источник финансирования (настройка названий).
     * @see ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

        public Class getEntityClass()
        {
            return EmployeeEncouragementSExtract.class;
        }

        public String getEntityName()
        {
            return "employeeEncouragementSExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
