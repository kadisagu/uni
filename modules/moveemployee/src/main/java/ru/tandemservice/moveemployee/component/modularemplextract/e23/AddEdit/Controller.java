/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e23.AddEdit;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.moveemployee.entity.IncDecCombinationExtract;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.*;

import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 23.12.2011
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<IncDecCombinationExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareEmployeeStaffRateDataSource(component);
        prepareStaffRateDataSource(component);

        Model model = getModel(component);

        if (model.isEditForm())
        {
            hasActiveStaffList(component);
            fillStaffRateValueMaps(model);
        }
    }

    public void onClickFreelance(IBusinessComponent component)
    {
        hasActiveStaffList(component);
    }

    @Override
    public void onChangeBasics(IBusinessComponent component)
    {
        Model model = getModel(component);

        Date date = model.getExtract().getContractDate();
        String number = model.getExtract().getContractNumber();
        Date agreementDate = model.getExtract().getContractAddAgreementDate();
        String agreementNumber = model.getExtract().getContractAddAgreementNumber();

        CommonEmployeeExtractUtil.fillExtractBasics(model.getSelectedBasicList(), model.getCurrentBasicMap(), date, number, agreementDate, agreementNumber);
    }

    public void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<CombinationPostStaffRateExtractRelation> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        }, 5);

        dataSource.addColumn(new BlockColumn("staffRate", "Ставка"));
        dataSource.addColumn(new BlockColumn("financingSource", "Источник финансирования"));
        dataSource.addColumn(new BlockColumn("financingSourceItem", "Источник финансирования (детально)"));
        if (model.isThereAnyActiveStaffList())
            dataSource.addColumn(new BlockColumn("employeeHR", "Кадровая расстановка"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem");
        actionColumn.setDisableSecondSubmit(false);
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setStaffRateDataSource(dataSource);
    }

    private void prepareEmployeeStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getEmployeeStaffRateDataSource() != null)
            return;


        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareEmployeeStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setEmployeeStaffRateDataSource(dataSource);
    }

    public void onClickAddStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        CombinationPostStaffRateExtractRelation item = new CombinationPostStaffRateExtractRelation();

        short discriminator = EntityRuntime.getMeta(CombinationPostStaffRateExtractRelation.class).getEntityCode();
        long id = EntityIDGenerator.generateNewId(discriminator);

        item.setId(id);
        item.setAbstractEmployeeExtract(model.getExtract());

        model.getCombinationPostStaffRateItemList().add(item);

        model.getStaffRateDataSource().refresh();
    }

    public void onClickDeleteItem(IBusinessComponent component)
    {
        Model model = getModel(component);

        CombinationPostStaffRateExtractRelation item = (CombinationPostStaffRateExtractRelation) component.getListenerParameter();

        model.getCombinationPostStaffRateItemList().remove(item);

        model.getStaffRateDataSource().refresh();

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double staffRateSumm = 0d;
        for (CombinationPostStaffRateExtractRelation staffRateItem : model.getCombinationPostStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(staffRateItem.getId()))
            {
                Object value = staffRateMap.get(staffRateItem.getId());
                val = ((Number)value).doubleValue();
            }

            staffRateSumm += val;
        }

        if (model.getExtract().getSalaryRaisingCoefficient() != null)
            model.getExtract().setSalary(model.getExtract().getSalaryRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (model.getExtract().getCombinationPost() != null && model.getExtract().getCombinationPost().getPostBoundedWithQGandQL().getSalary() != null)
            model.getExtract().setSalary(model.getExtract().getCombinationPost().getPostBoundedWithQGandQL().getSalary() * staffRateSumm);
    }

    public void onChangeOrgUnit(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.getExtract().setSalaryRaisingCoefficient(null);
        model.getExtract().setCombinationPost(null);
        model.getExtract().setMissingEmployeePost(null);

        if (model.getCombinationPostStaffRateItemList().size() > 0)
        {
            model.setCombinationPostStaffRateItemList(new ArrayList<>());
            model.getStaffRateDataSource().refresh();
        }

        hasActiveStaffList(component);
    }

    @SuppressWarnings("unchecked")
    public void onChangePost(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.getExtract().setSalaryRaisingCoefficient(null);

        if (model.getExtract().getCombinationPost() != null)
            model.getExtract().setMissingEmployeePost(model.getExtract().getCombinationPost().getMissingEmployeePost());

        if (model.getCombinationPostStaffRateItemList().size() > 0)
        {
            model.setCombinationPostStaffRateItemList(new ArrayList<>());
            model.getStaffRateDataSource().refresh();
        }

        if (model.getExtract().getCombinationPost() != null)
        {
            Map<CombinationPostStaffRateItem, List<StaffListAllocationItem>> combinationPostStaffRateAllocMap = getDao().getCombinationPostStaffRateAllocMap(model);
            Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, List<StaffListAllocationItem>> pairAllocListMap = new HashMap<>();

            for (Map.Entry<CombinationPostStaffRateItem, List<StaffListAllocationItem>> entry : combinationPostStaffRateAllocMap.entrySet())
                pairAllocListMap.put(new CoreCollectionUtils.Pair<>(entry.getKey().getFinancingSource(), entry.getKey().getFinancingSourceItem()), entry.getValue());

            for (Map.Entry<CombinationPostStaffRateItem, List<StaffListAllocationItem>> entry : combinationPostStaffRateAllocMap.entrySet())
            {
                short discriminator = EntityRuntime.getMeta(CombinationPostStaffRateExtractRelation.class).getEntityCode();
                long id = EntityIDGenerator.generateNewId(discriminator);

                CombinationPostStaffRateExtractRelation relation = new CombinationPostStaffRateExtractRelation();
                relation.setId(id);
                relation.setStaffRate(entry.getKey().getStaffRate());
                relation.setFinancingSource(entry.getKey().getFinancingSource());
                relation.setFinancingSourceItem(entry.getKey().getFinancingSourceItem());
                relation.setAbstractEmployeeExtract(model.getExtract());

                model.getCombinationPostStaffRateItemList().add(relation);
            }

            model.getStaffRateDataSource().refresh();

            fillStaffRateValueMaps(model);

            model.setSummStaffRateBefore(0d);
            for (CombinationPostStaffRateExtractRelation relation : model.getCombinationPostStaffRateItemList())
                model.setSummStaffRateBefore(model.getSummStaffRateBefore() + relation.getStaffRate());

            if (model.isThereAnyActiveStaffList())
            {
                Map<Long, List<IdentifiableWrapper>> idWrapperMap = new HashMap<>();
                for (CombinationPostStaffRateExtractRelation relation : model.getCombinationPostStaffRateItemList())
                {
                    List<IdentifiableWrapper> wrapperList = new ArrayList<>();

                    for (StaffListAllocationItem allocationItem : pairAllocListMap.get(new CoreCollectionUtils.Pair<>(relation.getFinancingSource(), relation.getFinancingSourceItem())))
                        wrapperList.add(new IdentifiableWrapper(allocationItem.getId(), allocationItem.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allocationItem.getStaffRate()) + " - " + allocationItem.getEmployeePost().getPostType().getShortTitle() +  " " + allocationItem.getEmployeePost().getPostStatus().getShortTitle()));

                    idWrapperMap.put(relation.getId(), wrapperList);
                }

                if (idWrapperMap.size() > 0)
                    ((BlockColumn) model.getStaffRateDataSource().getColumn(3)).setValueMap(idWrapperMap);
            }
        }
    }

    public void onChangeStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double staffRateSumm = 0d;
        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                val = ((Number)value).doubleValue();
            }

            staffRateSumm += val;
        }

        if (model.getExtract().getSalaryRaisingCoefficient() != null)
            model.getExtract().setSalary(model.getExtract().getSalaryRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (model.getExtract().getCombinationPost() != null && model.getExtract().getCombinationPost().getPostBoundedWithQGandQL().getSalary() != null)
            model.getExtract().setSalary(model.getExtract().getCombinationPost().getPostBoundedWithQGandQL().getSalary() * staffRateSumm);
    }

    public void onChangeFinSrc(IBusinessComponent component)
    {

    }

    public void onChangeRaisingCoefficient(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double staffRateSumm = 0d;
        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                val = ((Number) value).doubleValue();
            }

            staffRateSumm += val;
        }

        if (model.getExtract().getSalaryRaisingCoefficient() != null)
            model.getExtract().setSalary(model.getExtract().getSalaryRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (model.getExtract().getCombinationPost() != null && model.getExtract().getCombinationPost().getPostBoundedWithQGandQL().getSalary() != null)
            model.getExtract().setSalary(model.getExtract().getCombinationPost().getPostBoundedWithQGandQL().getSalary() * staffRateSumm);
    }

    public void hasActiveStaffList(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getExtract().getOrgUnit() != null &&
                UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getExtract().getOrgUnit()) != null && !model.getExtract().isFreelance())
        {
            if (!model.isThereAnyActiveStaffList())
            {
                model.setThereAnyActiveStaffList(true);
                model.setStaffRateDataSource(null);
                prepareStaffRateDataSource(component);
                prepareEmpHRColumn(component);
            }
        }
        else
            if (model.isThereAnyActiveStaffList())
            {
                model.setThereAnyActiveStaffList(false);
                model.setStaffRateDataSource(null);
                prepareStaffRateDataSource(component);
            }
    }

    @SuppressWarnings("unchecked")
    public void prepareEmpHRColumn(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.isAddForm() || !model.isThereAnyActiveStaffList())
            return;

        StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getExtract().getOrgUnit());
        if (model.getExtract().getCombinationPost() != null &&
                activeStaffList != null)
        {
            prepareStaffRateDataSource(component);

            fillStaffRateValueMaps(model);
            Map<Long, List<IdentifiableWrapper>> idWrapperMap = new HashMap<>();
            for (CombinationPostStaffRateExtractRelation rateItem : model.getCombinationPostStaffRateItemList())
            {
                List<IdentifiableWrapper> wrapperList = new ArrayList<>();

                Map<CombinationPostStaffRateExtractRelation, List<CombinationPostStaffRateExtAllocItemRelation>> staffRateAllocItemMap = getDao().getStaffRateAllocItemMap(model);

                List<CombinationPostStaffRateExtAllocItemRelation> allocRelList = staffRateAllocItemMap.get(rateItem) != null ? staffRateAllocItemMap.get(rateItem) : new ArrayList<>();
                for (CombinationPostStaffRateExtAllocItemRelation allocationItem : allocRelList)
                if (!allocationItem.isHasNewAllocItem() && allocationItem.getChoseStaffListAllocationItem() != null && allocationItem.getChoseStaffListAllocationItem().getEmployeePost() != null)
                    wrapperList.add(new IdentifiableWrapper(allocationItem.getChoseStaffListAllocationItem().getId(), allocationItem.getChoseStaffListAllocationItem().getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allocationItem.getChoseStaffListAllocationItem().getStaffRate()) + " - " + allocationItem.getChoseStaffListAllocationItem().getEmployeePost().getPostType().getShortTitle() +  " " + allocationItem.getChoseStaffListAllocationItem().getEmployeePost().getPostStatus().getShortTitle()));
                else if (allocationItem.isHasNewAllocItem())
                {
                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getExtract().getOrgUnit(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL(), rateItem.getFinancingSource(), rateItem.getFinancingSourceItem());
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();

                    List<StaffListAllocationItem> staffListAllocList = UniempDaoFacade.getStaffListDAO().getOccupiedStaffListAllocationItem(model.getExtract().getOrgUnit(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL(), rateItem.getFinancingSource(), rateItem.getFinancingSourceItem());
                    for (StaffListAllocationItem item : staffListAllocList)
                        if (!item.getEmployeePost().getPostStatus().isActive())
                            diff -= item.getStaffRate();

                    if (diff > 0)
                        wrapperList.add(new IdentifiableWrapper(0L, "<Новая ставка> - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                idWrapperMap.put(rateItem.getId(), wrapperList);
            }
            if (idWrapperMap.size() > 0)
                ((BlockColumn) model.getStaffRateDataSource().getColumn(3)).setValueMap(idWrapperMap);

        }
    }

    @SuppressWarnings("unchecked")
    public void fillStaffRateValueMaps(Model model)
    {
        Map<Long, Double> staffRateMap = new HashMap<>();
        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
            staffRateMap.put(item.getId(), item.getStaffRate());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(0)).setValueMap(staffRateMap);

        Map<Long, FinancingSource> finSrcMap = new HashMap<>();
        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
            finSrcMap.put(item.getId(), item.getFinancingSource());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(1)).setValueMap(finSrcMap);

        Map<Long, FinancingSourceItem> finSrcItmMap = new HashMap<>();
        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
            finSrcItmMap.put(item.getId(), item.getFinancingSourceItem());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(2)).setValueMap(finSrcItmMap);
    }
}