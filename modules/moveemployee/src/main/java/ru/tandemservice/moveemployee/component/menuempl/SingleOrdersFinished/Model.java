/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.menuempl.SingleOrdersFinished;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public class Model
{
    private List<EmployeeExtractType> _orderTypesList;
    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;

    public List<EmployeeExtractType> getOrderTypesList()
    {
        return _orderTypesList;
    }

    public void setOrderTypesList(List<EmployeeExtractType> orderTypesList)
    {
        this._orderTypesList = orderTypesList;
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }
}