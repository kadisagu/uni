/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractParagraph;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.Date;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public abstract class SingleEmployeeExtractAddEditDAO<T extends SingleEmployeeExtract, Model extends SingleEmployeeExtractAddEditModel<T>> extends UniDao<Model> implements ISingleEmployeeExtractAddEditDAO<T, Model>
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(Model model)
    {
        model.setAddForm(model.getExtractId() == null);
        model.setEditForm(!model.isAddForm());
        
        if (null != model.getEmployeeId())
            model.setEmployee(getNotNull(Employee.class, model.getEmployeeId()));
        
        if (null != model.getEmployeePost().getId())
            model.setEmployeePost(getNotNull(EmployeePost.class, model.getEmployeePost().getId()));

        if (null != model.getEmployeePost().getId() && null == model.getEmployee())
            model.setEmployee(model.getEmployeePost().getEmployee());

        if (null != model.getExtract())
            model.getExtract().setEmployee(model.getEmployee());

        // Making decision whereas the extract is being creating (Employee, or EmployeePost)
        // and filling up the list of extract types according to this decision.
        // TODO: 
        if (null != model.getEmployeePost().getId())
            model.setExtractTypeList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getSingleEmployeePostExtractTypeList());
        else
            model.setExtractTypeList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getSingleEmployeeExtractTypeList());

        if (null != model.getExtractTypeId())
            model.setExtractType(get(EmployeeExtractType.class, model.getExtractTypeId()));

        if (model.isEditForm())
        {
            model.setExtract((T)get(model.getExtractId()));
            model.setExtractType((EmployeeExtractType)model.getExtract().getType());
        }
        else
            model.setExtract(createNewInstance());

        if (model.getExtract() != null)
        {
            //TODO: syncPreCommitData(model);
            if (model.isAddForm())
            {
                model.getExtract().setCreateDate(new Date());
                model.getExtract().setEmployee(model.getEmployee());
                model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE));

                GrammaCase rusCase = getEmployeeTitleCase();
                if (rusCase != null)
                {
                    IdentityCard identityCard = null != model.getEmployeePost().getId() ? model.getEmployeePost().getPerson().getIdentityCard() : model.getEmployee().getPerson().getIdentityCard();
                    boolean isMaleSex = identityCard.getSex().isMale();

                    StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex));
                    str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
                    if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
                        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));
                    model.getExtract().setEmployeeFioModified(str.toString());
                }
                else
                    model.getExtract().setEmployeeFioModified(model.getExtract().getEntity().getPerson().getFullFio());

                EmployeeSingleExtractOrder order = new EmployeeSingleExtractOrder();
                model.setOrder(order);

                EmployeeSingleExtractParagraph paragraph = new EmployeeSingleExtractParagraph();
                paragraph.setOrder(order);
                model.setParagraph(paragraph);
                
                model.getExtract().setParagraph(model.getParagraph());
            }
            else
            {
                model.setParagraph((EmployeeSingleExtractParagraph)model.getExtract().getParagraph());
                model.setOrder((EmployeeSingleExtractOrder)model.getParagraph().getOrder());
            }
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        // TODO: We can`t use such validation scheme because we can create extract for employee and employeePost objects simultaneously
        if (model.isAddForm() && null != model.getEmployeePost().getId() && !MoveEmployeeDaoFacade.getMoveEmployeeDao().isMoveAccessible(model.getEmployeePost()))
            throw new ApplicationException("Нельзя добавить выписку, так как у сотрудника уже есть непроведенные выписки.");
    }

    @Override
    public void update(Model model)
    {
        // TODO: syncPreCommitData(model);
        if (model.isAddForm())
            model.getExtract().setType(model.getExtractType());

        model.getOrder().setCreateDate(model.getExtract().getCreateDate());
        
        getSession().saveOrUpdate(model.getOrder());
        getSession().saveOrUpdate(model.getParagraph());
        getSession().saveOrUpdate(model.getExtract());
    }

    protected abstract GrammaCase getEmployeeTitleCase();

    protected abstract T createNewInstance();
}