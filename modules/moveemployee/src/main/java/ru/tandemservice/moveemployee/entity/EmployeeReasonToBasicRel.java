package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.moveemployee.entity.gen.EmployeeReasonToBasicRelGen;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;

public class EmployeeReasonToBasicRel extends EmployeeReasonToBasicRelGen implements IEntityRelation<EmployeeOrderReasons, EmployeeOrderBasics>
{
}