/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e11.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IListenerParametersResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.PaymentAssigningExtract;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<PaymentAssigningExtract, IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        preparePaymentDataSource(component);
        super.onRefreshComponent(component);
//        preparePaymentsListDataSource(component);
        prepareStaffRateDataSource(component);

        checkEnabledAddStaffListPaymentButton(component);
    }

//    public void onPaymentChange(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onPaymentChange(getModel(component), null);
//    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

//    public void preparePaymentsListDataSource(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.createPaymentsBonusDataSource(getModel(component), component, this);
//    }
//
//    public void onClickPaymentUp(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickPaymentUpOrDown(getModel(component), (Long)component.getListenerParameter(), true);
//    }
//
//    public void onClickPaymentDown(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickPaymentUpOrDown(getModel(component), (Long)component.getListenerParameter(), false);
//    }
//
//    public void onClickAddBonus(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickAddBonus(getModel(component));
//    }
//
//    public void onClickDeleteBonus(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickRemoveBonus(getModel(component), (Long)component.getListenerParameter());
//    }

    private void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getStaffRateDataSource() != null)
            return;


        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setStaffRateDataSource(dataSource);
    }

    //PAYMENTS METHODS

    public void preparePaymentDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getPaymentDataSource() != null)
            return;

        DynamicListDataSource<EmployeeBonus> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().preparePaymentDataSource(model);
        }, 5);

        IListenerParametersResolver parametersResolver = (entity, valueEntity) -> entity;

        dataSource.addColumn(new BlockColumn("assignDate", "Дата назначения"));
        dataSource.addColumn(new BlockColumn("beginDate", "Назначена с даты"));
        dataSource.addColumn(new BlockColumn("endDate", "Назначена по дату"));
        dataSource.addColumn(new BlockColumn("payment", "Выплата"));
        dataSource.addColumn(new BlockColumn("amount", "Размер выплаты"));
        dataSource.addColumn(new SimpleColumn("Формат ввода", EmployeeBonus.payment().paymentUnit().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn("finSrc", "Источник финансирования"));
        dataSource.addColumn(new BlockColumn("finSrcItem", "Источник финансирования (детально)"));
        ActionColumn upColumn = new ActionColumn("Вверх", "up", "onUpPayment");
        upColumn.setParametersResolver(parametersResolver);
        dataSource.addColumn(upColumn);
        ActionColumn downColumn = new ActionColumn("Вниз", "down", "onDownPayment");
        downColumn.setParametersResolver(parametersResolver);
        dataSource.addColumn(downColumn);
        ActionColumn delColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onDeletePayment");
        delColumn.setParametersResolver(parametersResolver);
        dataSource.addColumn(delColumn);

        model.setPaymentDataSource(dataSource);
    }

    public void onClickAddSinglePayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus bonus = CommonEmployeeExtractUtil.onClickAddSinglePayment(model.getPaymentList(), model.getExtract());

        model.getPaymentDataSource().refresh();
    }

    public void onClickAddStaffListPayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        List<StaffListPostPayment> staffListPaymentsList = checkEnabledAddStaffListPaymentButton(component);

        List<EmployeeBonus> employeeBonusList = CommonEmployeeExtractUtil.onClickAddStaffListPayment(model.getPaymentList(), staffListPaymentsList, model.getExtract());

        model.setAddStaffListPaymentsButtonVisible(false);

        model.getPaymentDataSource().refresh();
    }

    public void onChangePayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus bonus = component.getListenerParameter();

        bonus.setValueProxy(CommonEmployeeExtractUtil.getPaymentValueForPaymentBlock(bonus.getPayment(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()));

        checkEnabledAddStaffListPaymentButton(component);
    }

    public void onUpPayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus currentBonus = component.<EmployeeBonus>getListenerParameter();

        CommonEmployeeExtractUtil.onUpPayment(currentBonus, model.getPaymentList());

        model.getPaymentDataSource().refresh();
    }

    public void onDownPayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus currentBonus = component.<EmployeeBonus>getListenerParameter();

        CommonEmployeeExtractUtil.onDownPayment(currentBonus, model.getPaymentList());

        model.getPaymentDataSource().refresh();
    }

    public void onDeletePayment(IBusinessComponent component)
    {
        Model model = getModel(component);
        EmployeeBonus currentBonus = component.<EmployeeBonus>getListenerParameter();

        CommonEmployeeExtractUtil.onDeletePayment(currentBonus, model.getPaymentList());

        checkEnabledAddStaffListPaymentButton(component);

        model.getPaymentDataSource().refresh();
    }

    /**
     * Проверяет и выстовляет доступность кнопки "Добавить выплаты, согласно штатному расписанию".
     * @return Возвращает доступные для добавления выплаты ШР, согласно ШР.
     */
    protected List<StaffListPostPayment> checkEnabledAddStaffListPaymentButton(IBusinessComponent component)
    {
        Model model = getModel(component);

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        List<StaffListPostPayment> paymentList = CommonEmployeeExtractUtil.checkEnabledAddStaffListPaymentButton(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), model.getEmployeePost().getOrgUnit(), model.getPaymentList(), staffRateItemList);

        if (!paymentList.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);

        return paymentList;
    }
}