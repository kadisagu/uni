package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.entity.gen.EmployeeBonusGen;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;

public class EmployeeBonus extends EmployeeBonusGen
{
    public static final String L_PAYMENT_TYPE = "paymentType";
    
    private PaymentType _paymentType;
    private boolean _includeBonus; 

    public PaymentType getPaymentType()
    {
        if(null == _paymentType && null != getPayment())
            return getPayment().getType();
        return _paymentType;
    }

    public void setPaymentType(PaymentType paymentType)
    {
        this._paymentType = paymentType;
    }

    public String getFinancingSourceCode()
    {
        if (null != getFinancingSource())
            return getFinancingSource().getCode();
        else
            return "";
    }

    public String getFinancingSourceTitle()
    {
        if (null != getFinancingSource())
            return getFinancingSource().getTitle();
        else
            return "";
    }
    
    public Double getValueProxy()
    {
        return getValue();
    }
    
    public void setValueProxy(Double value)
    {
        setValue(null != value ? value : 0d);
    }

    public boolean isIncludeBonus()
    {
        return _includeBonus;
    }

    public void setIncludeBonus(boolean includeBonus)
    {
        this._includeBonus = includeBonus;
    }
}