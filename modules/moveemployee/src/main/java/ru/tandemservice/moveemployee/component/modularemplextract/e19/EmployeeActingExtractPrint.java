/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e19;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeActingExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.11.2011
 */
public class EmployeeActingExtractPrint implements IPrintFormCreator<EmployeeActingExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeActingExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        //для ННГАСУ
        String[] fio = extract.getEmployeeFioModified().split(" ");
        final StringBuilder fioShortBuilder = new StringBuilder(fio[0]);
        if (fio.length > 1)
            fioShortBuilder.append(" ").append(fio[1].charAt(0)).append(".");
        if (fio.length > 2)
            fioShortBuilder.append(" ").append(fio[2].charAt(0)).append(".");
        modifier.put("FioShort", fioShortBuilder.toString());

        modifier.put("dateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        modifier.put("dateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));

        modifier.put("absPost_G", extract.getMissingPost().getPost().getGenitiveCaseTitle() != null ? extract.getMissingPost().getPost().getGenitiveCaseTitle() : extract.getMissingPost().getTitle());
        modifier.put("absOrgUnit_G", extract.getMissingOrgUnit().getGenitiveCaseTitle() != null ? extract.getMissingOrgUnit().getGenitiveCaseTitle() : extract.getMissingOrgUnit().getTitle());

        String absFio = PersonManager.instance().declinationDao().getCalculatedFIODeclination(extract.getMissingEmployeePost().getPerson().getIdentityCard(), InflectorVariantCodes.RU_GENITIVE);
        modifier.put("absFio_G", absFio + (!extract.isSaveMainJob() ? "." : ""));

        String[] absFioShort = absFio.split(" ");
        final StringBuilder absFioShortBuilder = new StringBuilder(absFioShort[0]);
        if (absFioShort.length > 1)
            absFioShortBuilder.append(" ").append(absFioShort[1].charAt(0)).append(".");
        if (absFioShort.length > 2)
            absFioShortBuilder.append(" ").append(absFioShort[2].charAt(0)).append(".");
        modifier.put("absFioShort_G", absFioShortBuilder.toString());

        modifier.put("conditions", extract.isSaveMainJob() ? " без освобождения от основной работы." : "");

        modifier.modify(document);
        return document;
    }
}