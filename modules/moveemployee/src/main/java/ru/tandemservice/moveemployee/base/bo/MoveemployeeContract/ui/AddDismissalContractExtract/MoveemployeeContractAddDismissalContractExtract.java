/**
 *$Id$
 */
package ru.tandemservice.moveemployee.base.bo.MoveemployeeContract.ui.AddDismissalContractExtract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToTypeRel;
import ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Create by ashaburov
 * Date 15.05.12
 */
@Configuration
public class MoveemployeeContractAddDismissalContractExtract extends BusinessComponentManager
{
    public static final String REASON_DS = "reasonDS";
    public static final String BASICS_DS = "basicsDS";
    public static final String EMPLOYEE_DS = "employeeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REASON_DS, reasonDSHandler()))
                .addDataSource(selectDS(BASICS_DS, basicsDSHandler()))
                .addDataSource(searchListDS(EMPLOYEE_DS, employeeDSColumn(), employeeDSHandler()).create())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler reasonDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput dsInput, ExecutionContext context)
            {
                String filter = dsInput.getComboFilterByValue();

                EmployeeExtractType extractType = DataAccessServices.dao().getNotNull(EmployeeExtractType.class, EmployeeExtractType.code(), "2.3.1");

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeReasonToTypeRel.class, "b").column(property(EmployeeReasonToTypeRel.first().fromAlias("b")));
                builder.where(eqValue(property(EmployeeReasonToTypeRel.second().fromAlias("b")), extractType));
                builder.where(likeUpper(property(EmployeeReasonToTypeRel.first().title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter))));
                builder.order(property(EmployeeReasonToTypeRel.first().title().fromAlias("b")));

                List<Object> list = builder.createStatement(context.getSession()).list();
                context.put(UIDefines.COMBO_OBJECT_LIST, list);

                return super.execute(dsInput, context);
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler basicsDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput dsInput, ExecutionContext context)
            {
                String filter = dsInput.getComboFilterByValue();

                EmployeeOrderReasons reasons = context.get(MoveemployeeContractAddDismissalContractExtractUI.EXTRACT_REASON);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeReasonToBasicRel.class, "b").column(property(EmployeeReasonToBasicRel.second().fromAlias("b")));
                builder.where(eqValue(property(EmployeeReasonToBasicRel.first().fromAlias("b")), reasons));
                builder.where(likeUpper(property(EmployeeReasonToBasicRel.second().title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter))));
                builder.order(property(EmployeeReasonToBasicRel.second().title().fromAlias("b")));

                List<Object> list = builder.createStatement(context.getSession()).list();
                context.put(UIDefines.COMBO_OBJECT_LIST, list);

                return super.execute(dsInput, context);
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeeDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput dsInput, ExecutionContext context)
            {
                List<EmploymentDismissalEmplListExtract> contractList = context.get(MoveemployeeContractAddDismissalContractExtractUI.EXTRACT_LIST);

                return ListOutputBuilder.get(dsInput, contractList).build();
            }
        };
    }

    @Bean
    public ColumnListExtPoint employeeDSColumn()
    {
        return columnListExtPointBuilder(EMPLOYEE_DS)
                .addColumn(checkboxColumn("checkbox"))
                .addColumn(textColumn("empCode", EmploymentDismissalEmplListExtract.employeePost().employee().employeeCode()))
                .addColumn(textColumn("fio", EmploymentDismissalEmplListExtract.employeePost().person().fio()))
                .addColumn(textColumn("orgUnit", EmploymentDismissalEmplListExtract.employeePost().orgUnit().titleWithType()))
                .addColumn(textColumn("post", EmploymentDismissalEmplListExtract.employeePost().postRelation().postBoundedWithQGandQL().fullTitle()))
                .addColumn(blockColumn("dismissalDate", "dismissalDate"))
                .addColumn(blockColumn("notFulfilDay", "notFulfilDay"))
                .addColumn(blockColumn("compensatingForDay", "compensatingForDay"))
                .create();
    }
}
