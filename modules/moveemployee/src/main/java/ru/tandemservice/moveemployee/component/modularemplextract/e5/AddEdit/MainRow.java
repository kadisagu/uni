/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit;

import java.util.List;

import org.tandemframework.core.entity.IEntity;

/**
 * Create by ashaburov
 * Date 17.10.11
 */
public class MainRow implements IEntity
{
    private Long _id;

    private List<Row> _rowList;

    private String _type;

    //Methods

    public static Long getNewMainRowId(List<MainRow> mainRowList)
    {
        Long newId = 1L;
        boolean repeat;
        do
        {
            repeat = false;
            for (MainRow mainRow : mainRowList)
                if (mainRow.getId().equals(newId))
                    {
                        repeat = true;
                        newId++;
                    }
        }
        while (repeat);
        return newId;
    }

    @Override
    public boolean equals(Object obj)
    {
        return this.getId().equals(((MainRow) obj).getId());
    }

    //Constructor

    public MainRow(Long id, List<Row> rowList, String type)
    {
        _id = id;
        _rowList = rowList;
        _type = type;
    }

    //Getters & Setters

    public String getType()
    {
        return _type;
    }

    public void setType(String type)
    {
        _type = type;
    }

    @Override
    public Long getId()
    {
        return _id;
    }

    @Override
    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public Object getProperty(Object propertyPath)
    {
        return null;  //Body of implemented method
    }

    @Override
    public void setProperty(String propertyPath, Object propertyValue)
    {
        //Body of implemented method
    }

    public List<Row> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<Row> rowList)
    {
        _rowList = rowList;
    }
}
