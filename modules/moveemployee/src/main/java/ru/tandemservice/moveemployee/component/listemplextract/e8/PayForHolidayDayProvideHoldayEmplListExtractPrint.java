/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e8;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.listemplextract.CommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.ICommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.IListParagraphPrintFormCreator;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class PayForHolidayDayProvideHoldayEmplListExtractPrint implements IPrintFormCreator<EmployeeListOrder>, IListParagraphPrintFormCreator<PayForHolidayDayProvideHoldayEmplListExtract>
{
    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph paragraph, PayForHolidayDayProvideHoldayEmplListExtract firstExtract)
    {
        RtfInjectModifier modifier = CommonListOrderPrint.createListOrderInjectModifier((EmployeeListOrder) paragraph.getOrder());
        CommonExtractPrintUtil.injectCommonExtractData(modifier, firstExtract);

        modifier.put("listBasics", firstExtract.getBasicListStr());

        return modifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph paragraph, PayForHolidayDayProvideHoldayEmplListExtract firstExtract)
    {
        final RtfTableModifier modifier = new RtfTableModifier();

        modifier.put("employee", getPreparedOrderData(paragraph));

        return modifier;
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);

        ICommonListOrderPrint commonListOrderPrint = (ICommonListOrderPrint) ApplicationRuntime.getBean("employeeListOrder_orderPrint");
        commonListOrderPrint.appendVisas(tableModifier, order);

        tableModifier.modify(document);

        return document;
    }

    @SuppressWarnings("unchecked")
    public String[][] getPreparedOrderData(IAbstractParagraph paragraph)
    {
        List<PayForHolidayDayProvideHoldayEmplListExtract> extractList = paragraph.getExtractList();
        if(null == extractList || extractList.isEmpty()) return new String[][]{};

        List<String[]> resultList = new ArrayList<String[]>();

        int i = 1;
        for (PayForHolidayDayProvideHoldayEmplListExtract extract : extractList)
        {

            String[] line = new String[1];

            StringBuilder lineBuilder = new StringBuilder();

            lineBuilder.append(i);
            lineBuilder.append(". ");
            lineBuilder.append(extract.getEntity().getEmployee().getEmployeeCode());
            lineBuilder.append(" ");
            lineBuilder.append(CommonExtractUtil.getModifiedFio(extract.getEntity().getEmployee().getPerson(), GrammaCase.DATIVE));
            lineBuilder.append(", ");
            lineBuilder.append(CommonExtractPrintUtil.getPostPrintingTitle(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.DATIVE_CASE, true));
            lineBuilder.append(" ");
            lineBuilder.append(CommonExtractPrintUtil.getOrgUnitPrintingTitle(extract.getEntity().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, false, true));
            lineBuilder.append(" - за ");
            lineBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getHolidayDate()));
            lineBuilder.append(" г.");

            line[0] = lineBuilder.toString();

            resultList.add(line);

            i++;
        }

        return resultList.toArray(new String[][]{});
    }
}
