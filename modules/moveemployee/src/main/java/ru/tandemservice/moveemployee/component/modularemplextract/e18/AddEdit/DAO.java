/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e18.AddEdit;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 15.09.2011
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<TransferAnnualHolidayExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected TransferAnnualHolidayExtract createNewInstance()
    {
        return new TransferAnnualHolidayExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        model.setHolidayList(prepareHolidayModel(model));

        if (model.isEditForm())
        {
            //определяем показывать селект Отпуск или Формы для добавления Нового планируемого отпуска
            //заполняем селект Отпуск сохраненным значением
            Object entity = model.getExtract().getEmployeeHoliday();
            if (entity instanceof EmployeeHoliday)
            {
                model.setShowHoliday(true);
                IdentifiableWrapper newWrapper = new IdentifiableWrapper(((EmployeeHoliday) entity).getId(), "Ежегодный отпуск за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) entity).getBeginDate()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) entity).getEndDate()));
                model.setChoseHoliday(newWrapper);
            }
            else if (entity instanceof VacationScheduleItem)
            {
                model.setShowHoliday(true);
                IdentifiableWrapper newWrapper = new IdentifiableWrapper(((VacationScheduleItem) entity).getId(), "График отпусков за " + ((VacationScheduleItem) entity).getVacationSchedule().getYear() + " год");
                model.setChoseHoliday(newWrapper);
            }
            else
            {
                model.setShowHoliday(false);
                model.setChoseHoliday(new IdentifiableWrapper(((IEntity) entity).getId(), "-Новый период-"));
            }

            //если в селекте Отпуск за период выбран Ежегодный или Планируемый отпуск,
            //то заполняем селект Отпуск сохраненными значениями
            if (model.getShowHoliday())
            {
                MQBuilder partBuilder = new MQBuilder(PeriodPartToExtractRelation.ENTITY_CLASS, "p");
                partBuilder.add(MQExpression.eq("p", PeriodPartToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, model.getExtract()));

                List<IdentifiableWrapper> partHolidayList = new ArrayList<IdentifiableWrapper>();

                for (PeriodPartToExtractRelation relation : partBuilder.<PeriodPartToExtractRelation>getResultList(getSession()))
                {
                    if (relation.getPart() instanceof EmployeeHoliday)
                        partHolidayList.add(new IdentifiableWrapper(((EmployeeHoliday) relation.getPart()).getId(), ((EmployeeHoliday) relation.getPart()).getFormattedNameBeginEndPeriodDateString()));
                    else if (relation.getPart() instanceof VacationScheduleItem)
                        partHolidayList.add(new IdentifiableWrapper(((VacationScheduleItem) relation.getPart()).getId(), "Отпуск с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((VacationScheduleItem) relation.getPart()).getPlanDate()) + " на " + ((VacationScheduleItem) relation.getPart()).getDaysAmount()));
                }

                model.setHolidayPartList(partHolidayList);
            }
            //если выбран -Новый период-,
            //то заполняем сохраненные строки раздела Отменяемый отпуск
            else
            {
                MQBuilder scheduleBuilder = new MQBuilder(NewVacationScheduleToExtractRelation.ENTITY_CLASS, "s");
                scheduleBuilder.add(MQExpression.eq("s", NewVacationScheduleToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, model.getExtract()));

                List<IdentifiableWrapper> partHolidayList = new ArrayList<IdentifiableWrapper>();
                long id = 1;
                List<NewVacationScheduleToExtractRelation> relationList = scheduleBuilder.getResultList(getSession());
                for (NewVacationScheduleToExtractRelation relation : relationList)
                {
                    IdentifiableWrapper newWrapper = new IdentifiableWrapper(id++, "");
                    partHolidayList.add(newWrapper);

                    model.getBeginHolidayDayMap().put(newWrapper, relation.getBeginDay());
                    model.getEndHolidayDayMap().put(newWrapper, relation.getEndDay());
                    model.getDurationHolidayDayMap().put(newWrapper, relation.getDurationDay());
                }
                model.setPartHolidayList(partHolidayList);
            }

            //заполняем строки раздела Предостовляемый отпуск
            MQBuilder factHolidayBuilder = new MQBuilder(TransferHolidayDateToExtractRelation.ENTITY_CLASS, "fh");
            factHolidayBuilder.add(MQExpression.eq("fh", TransferHolidayDateToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, model.getExtract()));

            List<TransferHolidayDateToExtractRelation> relationList = factHolidayBuilder.getResultList(getSession());

            List<IdentifiableWrapper> partFactHolidayList = new ArrayList<IdentifiableWrapper>();
            long id = -1;
            for (TransferHolidayDateToExtractRelation relation : relationList)
            {
                IdentifiableWrapper newWrapper = new IdentifiableWrapper(id--, "");
                partFactHolidayList.add(newWrapper);

                model.getBeginFactHolidayDayMap().put(newWrapper, relation.getBeginDay());
                model.getEndFactHolidayDayMap().put(newWrapper, relation.getEndDay());
                model.getDurationFactHolidayDayMap().put(newWrapper, relation.getDurationDay());
            }

            model.setPartFactHolidayList(partFactHolidayList);
        }

        model.setHolidayPartModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> resultList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : findValues("").getObjects())
                {
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);
                }
                return resultList;
            }

            @Override
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                if (model.getChoseHoliday() == null || model.getChoseHoliday().getId() == 0)
                    return ListResult.getEmpty();

                //если выбранн Ежегодный отпуск, то достаем все ежегодные отпуска с таким же периодом
                Object entity = get(model.getChoseHoliday().getId());
                if (entity instanceof EmployeeHoliday)
                {
                    MQBuilder holidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "h");
                    holidayBuilder.add(MQExpression.eq("h", EmployeeHoliday.employeePost().s(), model.getEmployeePost()));
                    holidayBuilder.add(MQExpression.great("h", EmployeeHoliday.startDate().s(), model.getExtract().getCreateDate()));
                    holidayBuilder.add(MQExpression.eq("h", EmployeeHoliday.holidayType().code().s(), UniempDefines.HOLIDAY_TYPE_ANNUAL));
                    holidayBuilder.add(MQExpression.eq("h", EmployeeHoliday.beginDate().s(), ((EmployeeHoliday) entity).getBeginDate()));
                    holidayBuilder.add(MQExpression.eq("h", EmployeeHoliday.endDate().s(), ((EmployeeHoliday) entity).getEndDate()));

                    List<IdentifiableWrapper> resultList = new ArrayList<>();

                    for (EmployeeHoliday holiday : holidayBuilder.<EmployeeHoliday>getResultList(getSession()))
                    {
                        IdentifiableWrapper newWrapper = new IdentifiableWrapper(holiday.getId(), holiday.getFormattedNameBeginEndPeriodDateString());

                        resultList.add(newWrapper);
                    }

                    return new ListResult<>(resultList);
                }
                //если выбран График планируемых отпусков, то достаем все планируемые отпуска
                else if (entity instanceof VacationScheduleItem)
                {
                    MQBuilder scheduleBuilder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "s");
                    scheduleBuilder.add(MQExpression.eq("s", VacationScheduleItem.employeePost().s(), model.getEmployeePost()));
                    scheduleBuilder.add(MQExpression.eq("s", VacationScheduleItem.vacationSchedule().s(), ((VacationScheduleItem) entity).getVacationSchedule()));

                    List<IdentifiableWrapper> resultList = new ArrayList<>();

                    for (VacationScheduleItem scheduleItem : scheduleBuilder.<VacationScheduleItem>getResultList(getSession()))
                    {
                        IdentifiableWrapper newWrapper = new IdentifiableWrapper(scheduleItem.getId(), "Отпуск с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(scheduleItem.getPlanDate()) + " на " + scheduleItem.getDaysAmount());

                        resultList.add(newWrapper);
                    }

                    return new ListResult<>(resultList);
                }

                return ListResult.getEmpty();

            }
        });

        //если списки частей пустые, то что бы отобразилась первая строчка заполняем их
        if (model.getPartHolidayList().isEmpty())
        {
            IdentifiableWrapper wrapper = new IdentifiableWrapper(-1L, "");
            model.getPartHolidayList().add(wrapper);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareHolidayDataSource(Model model)
    {
        model.getHolidayDataSource().setCountRow(model.getPartHolidayList().size());
        UniBaseUtils.createPage(model.getHolidayDataSource(), model.getPartHolidayList());

        for (ViewWrapper<IdentifiableWrapper> wrapper : ViewWrapper.<IdentifiableWrapper>getPatchedList(model.getHolidayDataSource()))
        {
            if (model.getPartHolidayList().size() == 1)
                wrapper.setViewProperty("disableDelete", true);
            else
                wrapper.setViewProperty("disableDelete", false);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareFactHolidayDataSource(Model model)
    {
        model.getFactHolidayDataSource().setCountRow(model.getPartFactHolidayList().size());
        UniBaseUtils.createPage(model.getFactHolidayDataSource(), model.getPartFactHolidayList());
    }

    @Override
    public List<IdentifiableWrapper> prepareHolidayModel(Model model)
    {
        //достаем ежегодные отпуска сотрудника, которые еще не начались
        MQBuilder holidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "h");
        holidayBuilder.add(MQExpression.eq("h", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
        holidayBuilder.add(MQExpression.eq("h", EmployeeHoliday.holidayType().code().s(), UniempDefines.HOLIDAY_TYPE_ANNUAL));
        holidayBuilder.add(MQExpression.great("h", EmployeeHoliday.startDate().s(), model.getExtract().getCreateDate()));

        List<EmployeeHoliday> holidayList = holidayBuilder.getResultList(getSession());
        Map<CoreCollectionUtils.Pair<Date, Date>, List<EmployeeHoliday>> periodHolidayMap = new HashMap<>();

        //сортируем в мапу по уникальным периодам на которые созданы отпуска
        for (EmployeeHoliday holiday : holidayList)
        {
            CoreCollectionUtils.Pair<Date, Date> pairDateKey = new CoreCollectionUtils.Pair<>(holiday.getBeginDate(), holiday.getEndDate());
            List<EmployeeHoliday> employeeHolidayList = periodHolidayMap.get(pairDateKey);
            if (employeeHolidayList == null)
                employeeHolidayList = new ArrayList<>();
            employeeHolidayList.add(holiday);
            periodHolidayMap.put(pairDateKey, employeeHolidayList);
        }

        //достаем планируемые отпуска сотрудника, этого и последующих годов
        MQBuilder vacationScheduleBuilder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vs");
        vacationScheduleBuilder.add(MQExpression.eq("vs", VacationScheduleItem.L_EMPLOYEE_POST, model.getEmployeePost()));
        Calendar currentYear = GregorianCalendar.getInstance();
        currentYear.setTime(model.getExtract().getCreateDate());
        vacationScheduleBuilder.add(MQExpression.greatOrEq("vs", VacationScheduleItem.vacationSchedule().year().s(), currentYear.get(Calendar.YEAR)));

        List<VacationScheduleItem> scheduleItemList = vacationScheduleBuilder.getResultList(getSession());
        Map<Integer, List<VacationScheduleItem>> scheduleYearMap = new HashMap<>();

        //сортируем в мапу по годам на которые созданые планируемые отпуска
        for (VacationScheduleItem scheduleItem : scheduleItemList)
        {
            int year = scheduleItem.getVacationSchedule().getYear();
            List<VacationScheduleItem> vacationScheduleItemList = scheduleYearMap.get(year);
            if (vacationScheduleItemList == null)
                vacationScheduleItemList = new ArrayList<>();
            vacationScheduleItemList.add(scheduleItem);
            scheduleYearMap.put(year, vacationScheduleItemList);
        }

        //формируем конечный список элементов селекта
        List<IdentifiableWrapper> resultHolidayList = new ArrayList<>();
        if (model.getExtract().getEmployeeHoliday() instanceof NewPartInTransferAnnualHolidayExtract)
            resultHolidayList.add(new IdentifiableWrapper(((NewPartInTransferAnnualHolidayExtract) model.getExtract().getEmployeeHoliday()).getId(), "-Новый период-"));
        else
            resultHolidayList.add(new IdentifiableWrapper(0L, "-Новый период-"));

        for (Map.Entry<CoreCollectionUtils.Pair<Date, Date>, List<EmployeeHoliday>> entry : periodHolidayMap.entrySet())
        {
            if (model.getExtract().getEmployeeHoliday() instanceof EmployeeHoliday)
            {
                CoreCollectionUtils.Pair<Date, Date> pair = new CoreCollectionUtils.Pair<>(((EmployeeHoliday) model.getExtract().getEmployeeHoliday()).getBeginDate(), ((EmployeeHoliday) model.getExtract().getEmployeeHoliday()).getEndDate());
                if (pair.equals(entry.getKey()))
                {
                    IdentifiableWrapper newWrapper = new IdentifiableWrapper(((EmployeeHoliday) model.getExtract().getEmployeeHoliday()).getId(), "Ежегодный отпуск за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entry.getKey().getX()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entry.getKey().getY()));
                    resultHolidayList.add(newWrapper);
                }
            }
            else
            {
                IdentifiableWrapper newWrapper = new IdentifiableWrapper(entry.getValue().get(0).getId(), "Ежегодный отпуск за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entry.getKey().getX()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entry.getKey().getY()));
                resultHolidayList.add(newWrapper);
            }
        }

        for (Map.Entry<Integer, List<VacationScheduleItem>> entry : scheduleYearMap.entrySet())
        {
            if (model.getExtract().getEmployeeHoliday() instanceof VacationScheduleItem)
            {
                if (((VacationScheduleItem) model.getExtract().getEmployeeHoliday()).getVacationSchedule().getYear() == entry.getKey())
                {
                    IdentifiableWrapper newWrapper = new IdentifiableWrapper(((VacationScheduleItem) model.getExtract().getEmployeeHoliday()).getId(), "График отпусков за " + entry.getKey() + " год");
                    resultHolidayList.add(newWrapper);
                }
            }
            else
            {
                IdentifiableWrapper newWrapper = new IdentifiableWrapper(entry.getValue().get(0).getId(), "График отпусков за " + entry.getKey() + " год");
                resultHolidayList.add(newWrapper);
            }
        }

        return resultHolidayList;
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        //если выбран -Новый период-,
        //то валидируем поля для создания нового Планируемого отпуска
        if (!model.getShowHoliday())
        {
            //проверяем, что даты начла не больше даты окончаний
            if (model.getExtract().getBeginHolidayDate().after(model.getExtract().getEndHolidayDate()))
                errors.add("Дата начала должна быть меньше даты окончания.", "endDate", "beginDate");
            for (IdentifiableWrapper wrapper : model.getPartHolidayList())
                if (model.getBeginHolidayDayMap().get(wrapper).after(model.getEndHolidayDayMap().get(wrapper)))
                    errors.add("Дата начала должна быть меньше даты окончания.", wrapper.getId() + "_beginDayId", wrapper.getId() + "_endDayId");

            //проверяем, что даты не пересекаются
            for (IdentifiableWrapper firstWrapper : model.getPartHolidayList())
                for (IdentifiableWrapper secondWrapper : model.getPartHolidayList())
                {
                    if (firstWrapper.equals(secondWrapper))
                        continue;

                    Date beginFirstWrapper = model.getBeginHolidayDayMap().get(firstWrapper);
                    Date endFirstWrapper = model.getEndHolidayDayMap().get(firstWrapper);
                    Date beginSecondWrapper = model.getBeginHolidayDayMap().get(secondWrapper);
                    Date endSecondWrapper = model.getEndHolidayDayMap().get(secondWrapper);

                    if (CommonBaseDateUtil.isBetween(beginFirstWrapper, beginSecondWrapper, endSecondWrapper) || CommonBaseDateUtil.isBetween(endFirstWrapper, beginSecondWrapper, endSecondWrapper))
                        errors.add("Части отпуска не должны пересекаться.", firstWrapper.getId() + "_beginDayId", firstWrapper.getId() + "_endDayId", secondWrapper.getId() + "_beginDayId", secondWrapper.getId() + "_endDayId");
                }

            //проверяем, что части отпуска не пересекаются с существующими отпусками сотрудника
            MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
            empHolidayBuilder.add(MQExpression.eq("eh", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
            for (IdentifiableWrapper wrapper : model.getPartHolidayList())
                for (EmployeeHoliday holiday : empHolidayBuilder.<EmployeeHoliday>getResultList(getSession()))
                {
                    Date beginDate = model.getBeginHolidayDayMap().get(wrapper);
                    Date endDate = model.getEndHolidayDayMap().get(wrapper);

                    if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                        errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                wrapper.getId() + "_beginDayId", wrapper.getId() + "_endDayId");
                    else {
                        if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                            errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                    wrapper.getId() + "_beginDayId", wrapper.getId() + "_endDayId");
                    }
                }

            //проверяем, что части отпуску не пересекаются с планируемыми отпусками сотрудника
            MQBuilder planedHolidayBuilder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi");
            planedHolidayBuilder.add(MQExpression.eq("vsi", VacationScheduleItem.L_EMPLOYEE_POST, model.getEmployeePost()));
            for (IdentifiableWrapper wrapper : model.getPartHolidayList())
                for (VacationScheduleItem scheduleItem : planedHolidayBuilder.<VacationScheduleItem>getResultList(getSession()))
                {
                    Date beginDate = model.getBeginHolidayDayMap().get(wrapper);
                    Date endDate = model.getEndHolidayDayMap().get(wrapper);

                    Date startHoliday = scheduleItem.getFactDate() != null ? scheduleItem.getFactDate() : scheduleItem.getPlanDate();
                    Date finishHoliday = null;

                    if (startHoliday == null)
                        continue;

                    //вычесляем дату окончания планируемого отпуска
                    GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
                    EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
                    int counter = 0;
                    int duration = scheduleItem.getDaysAmount();

                    date.setTime(startHoliday);

                    if (duration > 0)
                    {
                        do
                        {
                            if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                                counter++;

                            if (counter != duration)
                                date.add(Calendar.DATE, 1);
                        }
                        while (counter < duration);

                        finishHoliday = date.getTime();
                    }

                    if (finishHoliday == null)
                        continue;

                    if (CommonBaseDateUtil.isBetween(beginDate, startHoliday, finishHoliday) || CommonBaseDateUtil.isBetween(endDate, startHoliday, finishHoliday))
                        errors.add("Указанный отпуск пересекается с планируемым отпуском с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startHoliday) + " по " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(finishHoliday) + " из графика отпусков №" + scheduleItem.getVacationSchedule().getNumber() + " за " +
                                scheduleItem.getVacationSchedule().getYear() + " год от " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(scheduleItem.getVacationSchedule().getCreateDate()) + ".",
                                wrapper.getId() + "_beginDayId", wrapper.getId() + "_endDayId");
                    else {
                        if (CommonBaseDateUtil.isBetween(startHoliday, beginDate, endDate) || CommonBaseDateUtil.isBetween(finishHoliday, beginDate, endDate))
                            errors.add("Указанный отпуск пересекается с планируемым отпуском с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startHoliday) + " по " +
                                    DateFormatter.DEFAULT_DATE_FORMATTER.format(finishHoliday) + " из графика отпусков №" + scheduleItem.getVacationSchedule().getNumber() + " за " +
                                    scheduleItem.getVacationSchedule().getYear() + " год от " +
                                    DateFormatter.DEFAULT_DATE_FORMATTER.format(scheduleItem.getVacationSchedule().getCreateDate()) + ".",
                                    wrapper.getId() + "_beginDayId", wrapper.getId() + "_endDayId");
                    }
                }
        }

        //валидируем поля Предостовляемого отпуска
        //если заполнено хотя бы одно из трех полей, то остальные два тоже должны быть заполнены
        for (IdentifiableWrapper wrapper : model.getPartFactHolidayList())
        {
            if (model.getBeginFactHolidayDayMap().get(wrapper) == null && model.getEndFactHolidayDayMap().get(wrapper) == null && model.getDurationFactHolidayDayMap().get(wrapper) != null)
                errors.add("Не заполнены поля «Дата начала» и «Дата окончания».", wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");
            else if (model.getBeginFactHolidayDayMap().get(wrapper) != null && model.getEndFactHolidayDayMap().get(wrapper) == null && model.getDurationFactHolidayDayMap().get(wrapper) == null)
                errors.add("Не заполнены поля «Дата окончания» и «Длительность».", wrapper.getId() + "_durationDay", wrapper.getId() + "_endDay");
            else if (model.getBeginFactHolidayDayMap().get(wrapper) == null && model.getEndFactHolidayDayMap().get(wrapper) != null && model.getDurationFactHolidayDayMap().get(wrapper) == null)
                errors.add("Не заполнены поля «Дата начала» и «Длительность».", wrapper.getId() + "_beginDay", wrapper.getId() + "_durationDay");

        }

        //проверяем, что даты не пересекаются
        for (IdentifiableWrapper firstWrapper : model.getPartFactHolidayList())
            for (IdentifiableWrapper secondWrapper : model.getPartFactHolidayList())
            {
                if (firstWrapper.equals(secondWrapper))
                    continue;

                Date beginFirstWrapper = model.getBeginFactHolidayDayMap().get(firstWrapper);
                Date endFirstWrapper = model.getEndFactHolidayDayMap().get(firstWrapper);
                Date beginSecondWrapper = model.getBeginFactHolidayDayMap().get(secondWrapper);
                Date endSecondWrapper = model.getEndFactHolidayDayMap().get(secondWrapper);

                if (CommonBaseDateUtil.isBetween(beginFirstWrapper, beginSecondWrapper, endSecondWrapper) || CommonBaseDateUtil.isBetween(endFirstWrapper, beginSecondWrapper, endSecondWrapper))
                    errors.add("Части отпуска не должны пересекаться.", firstWrapper.getId() + "_beginDay", firstWrapper.getId() + "_endDay", secondWrapper.getId() + "_beginDay", secondWrapper.getId() + "_endDay");
            }

        //проверяем, что части отпуска не пересекаются с существующими отпусками сотрудника
        MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "ehb");
        empHolidayBuilder.add(MQExpression.eq("ehb", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
        if (model.getShowHoliday())
            empHolidayBuilder.add(MQExpression.notIn("ehb", EmployeeHoliday.P_ID, ids(model.getHolidayPartList())));
        for (IdentifiableWrapper wrapper : model.getPartFactHolidayList())
        {
            if (model.getBeginFactHolidayDayMap().get(wrapper) == null && model.getEndFactHolidayDayMap().get(wrapper) == null)
                continue;

            for (EmployeeHoliday holiday : empHolidayBuilder.<EmployeeHoliday>getResultList(getSession()))
            {
                Date beginDate = model.getBeginFactHolidayDayMap().get(wrapper);
                Date endDate = model.getEndFactHolidayDayMap().get(wrapper);

                if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                    errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                            wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");
                else {
                    if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                        errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");
                }
            }
        }

        //проверяем, что части отпуску не пересекаются с планируемыми отпусками сотрудника
        MQBuilder planedHolidayBuilder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsib");
        planedHolidayBuilder.add(MQExpression.eq("vsib", VacationScheduleItem.L_EMPLOYEE_POST, model.getEmployeePost()));
        if (model.getShowHoliday())
            planedHolidayBuilder.add(MQExpression.notIn("vsib", VacationScheduleItem.P_ID, ids(model.getHolidayPartList())));
        for (IdentifiableWrapper wrapper : model.getPartFactHolidayList())
        {
            if (model.getBeginFactHolidayDayMap().get(wrapper) == null && model.getEndFactHolidayDayMap().get(wrapper) == null)
                continue;

            for (VacationScheduleItem scheduleItem : planedHolidayBuilder.<VacationScheduleItem>getResultList(getSession()))
            {
                Date beginDate = model.getBeginFactHolidayDayMap().get(wrapper);
                Date endDate = model.getEndFactHolidayDayMap().get(wrapper);

                Date startHoliday = scheduleItem.getFactDate() != null ? scheduleItem.getFactDate() : scheduleItem.getPlanDate();
                Date finishHoliday = null;

                if (startHoliday == null)
                    continue;

                //вычесляем дату окончания планируемого отпуска
                GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
                EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
                int counter = 0;
                int duration = scheduleItem.getDaysAmount();

                date.setTime(startHoliday);

                if (duration > 0)
                {
                    do
                    {
                        if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                            counter++;

                        if (counter != duration)
                            date.add(Calendar.DATE, 1);
                    }
                    while (counter < duration);

                    finishHoliday = date.getTime();
                }

                if (finishHoliday == null)
                    continue;

                if (CommonBaseDateUtil.isBetween(beginDate, startHoliday, finishHoliday) || CommonBaseDateUtil.isBetween(endDate, startHoliday, finishHoliday))
                    errors.add("Указанный отпуск пересекается с планируемым отпуском с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startHoliday) + " по " +
                            DateFormatter.DEFAULT_DATE_FORMATTER.format(finishHoliday) + " из графика отпусков №" + scheduleItem.getVacationSchedule().getNumber() + " за " +
                            scheduleItem.getVacationSchedule().getYear() + " год от " +
                            DateFormatter.DEFAULT_DATE_FORMATTER.format(scheduleItem.getVacationSchedule().getCreateDate()) + ".",
                            wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");
                else {
                    if (CommonBaseDateUtil.isBetween(startHoliday, beginDate, endDate) || CommonBaseDateUtil.isBetween(finishHoliday, beginDate, endDate))
                        errors.add("Указанный отпуск пересекается с планируемым отпуском с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startHoliday) + " по " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(finishHoliday) + " из графика отпусков №" + scheduleItem.getVacationSchedule().getNumber() + " за " +
                                scheduleItem.getVacationSchedule().getYear() + " год от " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(scheduleItem.getVacationSchedule().getCreateDate()) + ".",
                                wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");
                }
            }
        }
    }

    @Override
    public void update(Model model)
    {
        //выбран -Новый период-
        if (!model.getShowHoliday())
        {
            //создаем объект для сохранения его в поле Отпуск за период
            NewPartInTransferAnnualHolidayExtract newPartRelation = new NewPartInTransferAnnualHolidayExtract();
            save(newPartRelation);
            model.getExtract().setEmployeeHoliday(newPartRelation);
        }
        //выбран либо Ежегодный отпуск, либо Планируемый отпуск
        else
        {
            //заполняем поле Отпуск за период
            IChoseRowTransferAnnualHolidayExtract holiday = get(EmployeeHoliday.class, model.getChoseHoliday().getId());
            if (holiday != null)
                model.getExtract().setEmployeeHoliday(holiday);
            IChoseRowTransferAnnualHolidayExtract scheduleItem = get(VacationScheduleItem.class, model.getChoseHoliday().getId());
            if (scheduleItem != null)
                model.getExtract().setEmployeeHoliday(scheduleItem);

            model.getExtract().setBeginHolidayDate(null);
            model.getExtract().setEndHolidayDate(null);
            model.getExtract().setHolidayTitle(null);
        }

        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());

        //заполняем поля для карточки выписки
        //заполняем поле Отпуск за период
        if (model.getExtract().getEmployeeHoliday() instanceof EmployeeHoliday)
            model.getExtract().setPeriodHolidayStr("Ежегодный отпуск за период с " +
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) model.getExtract().getEmployeeHoliday()).getBeginDate()) +
                    " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(((EmployeeHoliday) model.getExtract().getEmployeeHoliday()).getEndDate()));
        else if (model.getExtract().getEmployeeHoliday() instanceof VacationScheduleItem)
            model.getExtract().setPeriodHolidayStr("График отпусков за " + ((VacationScheduleItem) model.getExtract().getEmployeeHoliday()).getVacationSchedule().getYear() + " год");
        else if (model.getExtract().getEmployeeHoliday() instanceof NewPartInTransferAnnualHolidayExtract)
            model.getExtract().setPeriodHolidayStr("-Новый период-");

        model.getExtract().setShowHoliday(model.getExtract().getEmployeeHoliday() instanceof EmployeeHoliday || model.getExtract().getEmployeeHoliday() instanceof VacationScheduleItem);
        //заполняем поле Отпуск
        if (model.getExtract().getEmployeeHoliday() instanceof EmployeeHoliday || model.getExtract().getEmployeeHoliday() instanceof VacationScheduleItem)
        {
            String resultStr = "";
            for (IdentifiableWrapper wrapper : model.getHolidayPartList())
            {
                resultStr += wrapper.getTitle() + ";" + (model.getHolidayPartList().indexOf(wrapper) == model.getHolidayPartList().size() - 1 ? "" : "<br/>");
            }

            model.getExtract().setHolidayStr(resultStr);
        }
        else if (model.getExtract().getEmployeeHoliday() instanceof NewPartInTransferAnnualHolidayExtract)
        {
            String resultStr = "";
            for (IdentifiableWrapper wrapper : model.getPartHolidayList())
            {
                resultStr += "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getBeginHolidayDayMap().get(wrapper)) +
                        " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEndHolidayDayMap().get(wrapper)) +
                        " длительность " + NumberConvertingUtil.getCalendarDaysWithName(model.getDurationHolidayDayMap().get(wrapper)) + ";" +
                        (model.getPartHolidayList().indexOf(wrapper) == model.getPartHolidayList().size() - 1 ? "" : "<br/>");
            }
            model.getExtract().setHolidayStr(resultStr);
        }

        //заполняем поле Предоставляемый отпуск
        model.getExtract().setShowFactHoliday(model.getPartFactHolidayList().size() != 0);
        if (model.getPartFactHolidayList().size() != 0)
        {
            String resultStr = "";
            for (IdentifiableWrapper wrapper : model.getPartFactHolidayList())
            {
                resultStr += "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getBeginFactHolidayDayMap().get(wrapper)) +
                        " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEndFactHolidayDayMap().get(wrapper)) +
                        " длительность " + NumberConvertingUtil.getCalendarDaysWithName(model.getDurationFactHolidayDayMap().get(wrapper)) + ";" +
                        (model.getPartFactHolidayList().indexOf(wrapper) == model.getPartFactHolidayList().size() - 1 ? "" : "<br/>");
            }
            model.getExtract().setFactHolidayStr(resultStr);

        }

        if (model.isEditForm())
        {
            MQBuilder partBuilder = new MQBuilder(PeriodPartToExtractRelation.ENTITY_CLASS, "p");
            partBuilder.add(MQExpression.eq("p", PeriodPartToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, model.getExtract()));

            //удаляем старые релейшены на выбранные Части отпуска (объекты селекта Отпуск)
            for (PeriodPartToExtractRelation relation : partBuilder.<PeriodPartToExtractRelation>getResultList(getSession()))
                delete(relation);

            MQBuilder factHolidayBuilder = new MQBuilder(TransferHolidayDateToExtractRelation.ENTITY_CLASS, "fh");
            factHolidayBuilder.add(MQExpression.eq("fh", TransferHolidayDateToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, model.getExtract()));

            //удаляем старые релейшены на части предостовляемого отпуска (строки раздела Предоставляемый отпуск)
            for (TransferHolidayDateToExtractRelation relation : factHolidayBuilder.<TransferHolidayDateToExtractRelation>getResultList(getSession()))
                delete(relation);

            MQBuilder scheduleBuilder = new MQBuilder(NewVacationScheduleToExtractRelation.ENTITY_CLASS, "s");
            scheduleBuilder.add(MQExpression.eq("s", NewVacationScheduleToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, model.getExtract()));

            //удаляем старые релейшены на части нового планируемого отпуска (строки раздела Отменяемый отпуск, используется, если в селекте выбран -Новый период-)
            for (NewVacationScheduleToExtractRelation relation : scheduleBuilder.<NewVacationScheduleToExtractRelation>getResultList(getSession()))
                delete(relation);
        }

        //выбран -Новый период-
        if (!model.getShowHoliday())
        {
            //создаем релейшены для частей нового Планируемого отпуска
            for (IdentifiableWrapper wrapper : model.getPartHolidayList())
            {
                NewVacationScheduleToExtractRelation newScheduleRelation = new NewVacationScheduleToExtractRelation();
                newScheduleRelation.setTransferAnnualHolidayExtract(model.getExtract());
                newScheduleRelation.setBeginDay(model.getBeginHolidayDayMap().get(wrapper));
                newScheduleRelation.setEndDay(model.getEndHolidayDayMap().get(wrapper));
                newScheduleRelation.setDurationDay(model.getDurationHolidayDayMap().get(wrapper));

                save(newScheduleRelation);
            }
        }
        //выбран либо Ежегодный отпуск, либо Планируемый отпуск
        else
        {
            //создаем релейшены на выбранные элементы в поле Отпуск
            for (IdentifiableWrapper row : model.getHolidayPartList())
            {
                PeriodPartToExtractRelation relation = new PeriodPartToExtractRelation();
                relation.setTransferAnnualHolidayExtract(model.getExtract());
                relation.setPart((IChoseRowTransferAnnualHolidayExtract) get(row.getId()));

                save(relation);
            }
        }

        //создаем релейшены на части Предоставляемого отпуска
        for (IdentifiableWrapper wrapper : model.getPartFactHolidayList())
        {
            if (model.getBeginFactHolidayDayMap().get(wrapper) == null && model.getEndFactHolidayDayMap().get(wrapper) == null && model.getDurationFactHolidayDayMap().get(wrapper) == null)
                continue;

            TransferHolidayDateToExtractRelation relation = new TransferHolidayDateToExtractRelation();
            relation.setTransferAnnualHolidayExtract(model.getExtract());
            relation.setBeginDay(model.getBeginFactHolidayDayMap().get(wrapper));
            relation.setEndDay(model.getEndFactHolidayDayMap().get(wrapper));
            relation.setDurationDay(model.getDurationFactHolidayDayMap().get(wrapper));

            save(relation);
        }
    }
}