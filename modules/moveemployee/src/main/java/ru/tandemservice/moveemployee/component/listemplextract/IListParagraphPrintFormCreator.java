/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * Create by ashaburov
 * Date 11.10.11
 */
public interface IListParagraphPrintFormCreator<T>
{
    RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph paragraph, T firstExtract);

    RtfTableModifier createParagraphTableModifier(IAbstractParagraph paragraph, T firstExtract);
}
