/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

import java.util.Map;

/**
 * @author dseleznev
 *         Created on: 20.11.2008
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "extractId"),
        @Bind(key = "selectedTab", binding = "selectedTab"),
        @Bind(key = "extractIndividual", binding = "extractIndividual")
})
public abstract class ModularEmployeeExtractPubModel<T extends ModularEmployeeExtract> implements IVisaOwnerModel
{
    private String _tabLable;
    private String _stickerValue;
    private String _commonDataInfoTableCaption;
    private String _infoPropertyTitleStateExt;
    private String _infoPropertyValueStateExt;

    private Long _extractId;
    private T _extract;

    private String _extractUserActionsAddEditPage = ModularEmployeeExtractPubModel.class.getPackage().getName() + ".ExtractUserActionsAddEdit";
    private String _extractIndividualUserActionsAddEditPage = ModularEmployeeExtractPubModel.class.getPackage().getName() + ".ExtractIndividualUserActionsAddEdit";

    private Boolean _extractIndividual = false;
    private Boolean _orderCanBeEdited;

    private boolean _debugMode;
    private String _selectedTab;
    private String _attributesPage;
    private String _additionalAttrsPage;
    private Long _visingStatus;
    private CommonPostfixPermissionModel _secModel;
    private Map<String, Object> _visaListParameters = ParametersMap.createWith("visaOwnerModel", this);

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    // IVisaOwnerModel

    @Override
    public IAbstractDocument getDocument()
    {
        if (_extractIndividual)
            return _extract.getParagraph().getOrder();
        else
            return _extract;
    }

    @Override
    public boolean isVisaListModificable()
    {
        if (_extractIndividual)
            return UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE.equals(_extract.getParagraph().getOrder().getState().getCode());
        else
            return UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE.equals(_extract.getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        if (_extractIndividual)
            return "employeeModularOrder";
        else
            return "modularEmployeeExtract";
    }

    // Getters & Setters


    public String getInfoPropertyValueStateExt()
    {
        return _infoPropertyValueStateExt;
    }

    public void setInfoPropertyValueStateExt(String infoPropertyValueStateExt)
    {
        _infoPropertyValueStateExt = infoPropertyValueStateExt;
    }

    public String getInfoPropertyTitleStateExt()
    {
        return _infoPropertyTitleStateExt;
    }

    public void setInfoPropertyTitleStateExt(String infoPropertyTitleStateExt)
    {
        _infoPropertyTitleStateExt = infoPropertyTitleStateExt;
    }

    public String getCommonDataInfoTableCaption()
    {
        return _commonDataInfoTableCaption;
    }

    public void setCommonDataInfoTableCaption(String commonDataInfoTableCaption)
    {
        _commonDataInfoTableCaption = commonDataInfoTableCaption;
    }

    public String getTabLable()
    {
        return _tabLable;
    }

    public void setTabLable(String tabLable)
    {
        _tabLable = tabLable;
    }

    public String getStickerValue()
    {
        return _stickerValue;
    }

    public void setStickerValue(String stickerValue)
    {
        _stickerValue = stickerValue;
    }

    public Boolean getOrderCanBeEdited()
    {
        return _orderCanBeEdited;
    }

    public void setOrderCanBeEdited(Boolean orderCanBeEdited)
    {
        _orderCanBeEdited = orderCanBeEdited;
    }

    public Boolean getExtractIndividual()
    {
        return _extractIndividual;
    }

    public void setExtractIndividual(Boolean extractIndividual)
    {
        _extractIndividual = extractIndividual;
    }

    public String getExtractUserActionsAddEditPage()
    {
        return _extractUserActionsAddEditPage;
    }

    public void setExtractUserActionsAddEditPage(String extractUserActionsAddEditPage)
    {
        _extractUserActionsAddEditPage = extractUserActionsAddEditPage;
    }

    public String getExtractIndividualUserActionsAddEditPage()
    {
        return _extractIndividualUserActionsAddEditPage;
    }

    public void setExtractIndividualUserActionsAddEditPage(String extractIndividualUserActionsAddEditPage)
    {
        _extractIndividualUserActionsAddEditPage = extractIndividualUserActionsAddEditPage;
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        _extract = extract;
    }

    public boolean isDebugMode()
    {
        return _debugMode;
    }

    public void setDebugMode(boolean debugMode)
    {
        _debugMode = debugMode;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getAttributesPage()
    {
        return _attributesPage;
    }

    public void setAttributesPage(String attributesPage)
    {
        _attributesPage = attributesPage;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        _visingStatus = visingStatus;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public String getAdditionalAttrsPage()
    {
        return _additionalAttrsPage;
    }

    public void setAdditionalAttrsPage(String additionalAttrsPage)
    {
        this._additionalAttrsPage = additionalAttrsPage;
    }

}