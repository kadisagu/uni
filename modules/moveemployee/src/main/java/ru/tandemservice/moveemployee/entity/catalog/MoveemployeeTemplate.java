package ru.tandemservice.moveemployee.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.moveemployee.entity.catalog.gen.MoveemployeeTemplateGen;

public class MoveemployeeTemplate extends MoveemployeeTemplateGen implements ITemplateDocument
{
    @Override
    public byte[] getContent()
    {
        return CommonBaseUtil.getTemplateContent(this);
    }
}