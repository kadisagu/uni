/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e4;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeTransferExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniemp.UniempDefines;

import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 14.01.2009
 */
public class EmployeeTransferExtractPrint implements IPrintFormCreator<EmployeeTransferExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeTransferExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        List<FinancingSourceDetails> staffRateDetails = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
        CommonExtractPrintUtil.injectStaffRates(modifier, staffRateDetails);
        CommonExtractPrintUtil.injectPost(modifier, extract.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), "Old");
        CommonExtractPrintUtil.injectPost(modifier, extract.getPostBoundedWithQGandQL(), null);
        CommonExtractPrintUtil.injectOrgUnit(modifier, extract.getEmployeePost().getOrgUnit(), "Old");
        CommonExtractPrintUtil.injectOrgUnit(modifier, extract.getOrgUnit(), null);

        PostType postType = extract.getEmployeePost().getPostType();
        if(UniDefines.POST_TYPE_SECOND_JOB.equals(postType.getCode()) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(postType.getCode()) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(postType.getCode()))
            modifier.put("secondJobCase", ", совместителя,");
        else
            modifier.put("secondJobCase", "");

        modifier.put("transferDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getTransferDate()));
        modifier.put("transferDateMonthStr", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getTransferDate()));
        CommonExtractPrintUtil.injectOrgUnit(modifier, extract.getOrgUnit(), "New");
        CommonExtractPrintUtil.injectPost(modifier, extract.getPostBoundedWithQGandQL(), "New");
        modifier.put("targetOrgUnit", CommonExtractPrintUtil.getOrgUnitPrintingTitle(extract.getOrgUnit(), CommonExtractPrintUtil.NOMINATIVE_CASE, false, true));
        modifier.put("TargetOrgUnit", CommonExtractPrintUtil.getOrgUnitPrintingTitle(extract.getOrgUnit(), CommonExtractPrintUtil.NOMINATIVE_CASE, true, false));
        modifier.put("targetPost", extract.getPostBoundedWithQGandQL().getPost().getTitle().toLowerCase());

        //алгоритм определяет какой источник финансирования указан у долей ставки в выписке
        //TODO НЕОБХОДИМ РЕФАКТОРИНГ КОГДА ПОЯВИТСЯ НОВЫЙ ИСТОЧНИК ФИНАНСИРОВАНИЯ
        Boolean budget = null;
        for (FinancingSourceDetails details : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract))
        {
            boolean currentFinSrc = details.getFinancingSource().getCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET);

            if (budget != null && !budget.equals(currentFinSrc))
            {
                budget = null;
                break;
            }

            if (budget == null && currentFinSrc)
                budget = true;

            if (budget == null && !currentFinSrc)
                budget = false;
        }

        if(budget == null)
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, null, "");
        }
        else if (budget)
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, true, "");
        }
        else
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, false, "");
        }

        CommonExtractPrintUtil.injectRsvpuPayments(modifier, extract);

        RtfTableModifier visaModifier = CommonExtractPrint.createModularExtractTableModifier(extract);
        visaModifier.modify(document);
        modifier.modify(document);
        return document;
    }
}