package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.entity.gen.EmployeeTempAddExtractGen;

public class EmployeeTempAddExtract extends EmployeeTempAddExtractGen implements IPostAssignExtract
{
    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }
}