package ru.tandemservice.moveemployee.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.moveemployee.entity.catalog.gen.EmployeeExtractTypeGen;

public class EmployeeExtractType extends EmployeeExtractTypeGen implements IHierarchyItem
{
    public static final String P_TITLE_WITH_PARENT = "titleWithParent";

    public String getTitleWithParent()
    {
        EmployeeExtractType recurentObject = this;
        StringBuilder resultTitle = new StringBuilder();
        while(recurentObject.getParent()!=null)
        {
            resultTitle.insert(0, recurentObject.getTitle());
            resultTitle.insert(0, ". ");
            recurentObject = recurentObject.getParent();
        }
        resultTitle.insert(0, recurentObject.getTitle());
        return resultTitle.toString();

        //old variant with 1 level of recursion
        //return getParent() != null ? getParent().getTitle() + "." + getTitle() : getTitle();
    }

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return (EmployeeExtractType)getParent();
    }
}