/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e17.AddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;

import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 09.09.2011
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<RevocationFromHolidayExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected RevocationFromHolidayExtract createNewInstance()
    {
        return new RevocationFromHolidayExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        model.getExtract().setEmployeeFioDative(CommonExtractUtil.getModifiedFio(model.getExtract().getEmployee().getPerson(), GrammaCase.DATIVE));

        //достаем отпуска сотрудника действующих на момент формирования выписки
        Date currentDate = new Date();
        MQBuilder builder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
        builder.add(MQExpression.greatOrEq("b", EmployeeHoliday.P_FINISH_DATE, currentDate));
        builder.add(MQExpression.lessOrEq("b", EmployeeHoliday.P_START_DATE, currentDate));
        model.setEmployeeHolidayList(builder.<HolidayType>getResultList(getSession()));

        if (model.isEditForm())
        {
            MQBuilder provideDateBuilder = new MQBuilder(ProvideDateToExtractRelation.ENTITY_CLASS, "b");
            provideDateBuilder.add(MQExpression.eq("b", ProvideDateToExtractRelation.L_REVOCATION_FROM_HOLIDAY_EXTRACT, model.getExtract()));
            provideDateBuilder.addOrder("b", ProvideDateToExtractRelation.P_PROVIDE_BEGIN_DAY, OrderDirection.asc);

            List<ProvideDateToExtractRelation> relationList = provideDateBuilder.getResultList(getSession());
            //если у приказа были указаны части отпуска, то ставим галочку в соответствующее поле
            if (!relationList.isEmpty())
                model.getExtract().setProvideUnusedHoliday(true);
            else
                model.getExtract().setProvideUnusedHoliday(false);

            //переносим данные из релейшенов в мапы
            long id = 0;
            for (ProvideDateToExtractRelation relation : relationList)
            {
                IdentifiableWrapper newWrapper = new IdentifiableWrapper(id++, "");
                model.getBeginDayMap().put(newWrapper, relation.getProvideBeginDay());
                model.getEndDayMap().put(newWrapper, relation.getProvideEndDay());
                model.getDurationDayMap().put(newWrapper, relation.getProvideDurationDay());

                model.getPartHolidayWrapperList().add(newWrapper);
            }
        }

        //если частей отпуска нет, то что бы в момент первого отображение была хотя бы одна часть добавляем ее
        if (model.getPartHolidayWrapperList().isEmpty())
        {
            IdentifiableWrapper wrapper = new IdentifiableWrapper(0L, "");
            model.getPartHolidayWrapperList().add(wrapper);
        }
    }

    @Override
    public void prepareDumyDataSource(Model model)
    {
        model.getDummyDataSource().setCountRow(0);
        UniBaseUtils.createPage(model.getDummyDataSource(), new ArrayList<>());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareHolidayDataSource(Model model)
    {
        model.getHolidayDataSource().setCountRow(model.getPartHolidayWrapperList().size());
        UniBaseUtils.createPage(model.getHolidayDataSource(), model.getPartHolidayWrapperList());

        for (ViewWrapper<IdentifiableWrapper> wrapper : ViewWrapper.<IdentifiableWrapper>getPatchedList(model.getHolidayDataSource()))
        {
            if (model.getPartHolidayWrapperList().size() == 1)
                wrapper.setViewProperty("disableDelete", true);
            else
                wrapper.setViewProperty("disableDelete", false);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        //проверяем, что даты Отозвать с и Отозвать по не выходят за рамки отпуска
        if (model.getExtract().getRevocationFromDay() != null && !CommonBaseDateUtil.isBetween(model.getExtract().getRevocationFromDay(), model.getExtract().getEmployeeHoliday().getStartDate(), model.getExtract().getEmployeeHoliday().getFinishDate()))
            errors.add("Дата в поле \"Отозвать с\" должна попадать в период с " +
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getEmployeeHoliday().getStartDate()) + " по " +
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getEmployeeHoliday().getFinishDate()) + ".",
                        "revocationFromDay");
        if (model.getExtract().getRevocationToDay() != null && !CommonBaseDateUtil.isBetween(model.getExtract().getRevocationToDay(), model.getExtract().getEmployeeHoliday().getStartDate(), model.getExtract().getEmployeeHoliday().getFinishDate()))
                errors.add("Дата в поле \"Отозвать по\" должна попадать в период с " +
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getEmployeeHoliday().getStartDate()) + " по " +
                        DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getEmployeeHoliday().getFinishDate()) + ".",
                        "revocationToDay");

        //проверяем, что начальные даты не больше конечных
        if (model.getExtract().getRevocationFromDay() != null && model.getExtract().getRevocationToDay() != null &&
                model.getExtract().getRevocationFromDay().getTime() > model.getExtract().getRevocationToDay().getTime())
                errors.add("Дата начала должна быть меньше даты окончания.", "revocationFromDay", "revocationToDay");
        if (model.getExtract().isProvideUnusedHoliday())
            for (IdentifiableWrapper wrapper : model.getPartHolidayWrapperList())
                if (model.getBeginDayMap().get(wrapper).getTime() > model.getEndDayMap().get(wrapper).getTime())
                    errors.add("Дата начала должна быть меньше даты окончания.", wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");

        //если предоставляется часть неиспользуемого отпуска
        if (model.getExtract().isProvideUnusedHoliday())
        {
            //проверяем, что даты частей отпуска не пересекаются
            for (IdentifiableWrapper firstWrapper : model.getPartHolidayWrapperList())
                for (IdentifiableWrapper secondWrapper : model.getPartHolidayWrapperList())
                {
                    if (firstWrapper.equals(secondWrapper))
                        continue;

                    Date beginFirstWrapper = model.getBeginDayMap().get(firstWrapper);
                    Date endFirstWrapper = model.getEndDayMap().get(firstWrapper);
                    Date beginSecondWrapper = model.getBeginDayMap().get(secondWrapper);
                    Date endSecondWrapper = model.getEndDayMap().get(secondWrapper);

                    if (CommonBaseDateUtil.isBetween(beginFirstWrapper, beginSecondWrapper, endSecondWrapper) || CommonBaseDateUtil.isBetween(endFirstWrapper, beginSecondWrapper, endSecondWrapper))
                        errors.add("Части отпуска не должны пересекаться.", firstWrapper.getId() + "_beginDay", firstWrapper.getId() + "_endDay", secondWrapper.getId() + "_beginDay", secondWrapper.getId() + "_endDay");
                }

            //проверяем, что части отпуска не пересекаются с существующими отпусками сотрудника
            MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
            empHolidayBuilder.add(MQExpression.eq("eh", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
            for (IdentifiableWrapper wrapper : model.getPartHolidayWrapperList())
                for (EmployeeHoliday holiday : empHolidayBuilder.<EmployeeHoliday>getResultList(getSession()))
                {
                    Date beginDate = model.getBeginDayMap().get(wrapper);
                    Date endDate = model.getEndDayMap().get(wrapper);

                    if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                        errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");
                    else {
                        if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                            errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                    wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");
                    }
                }

            //проверяем, что части отпуску не пересекаются с планируемыми отпусками сотрудника
            MQBuilder planedHolidayBuilder = new MQBuilder(VacationScheduleItem.ENTITY_CLASS, "vsi");
            planedHolidayBuilder.add(MQExpression.eq("vsi", VacationScheduleItem.L_EMPLOYEE_POST, model.getEmployeePost()));
            for (IdentifiableWrapper wrapper : model.getPartHolidayWrapperList())
                for (VacationScheduleItem scheduleItem : planedHolidayBuilder.<VacationScheduleItem>getResultList(getSession()))
                {
                    Date beginDate = model.getBeginDayMap().get(wrapper);
                    Date endDate = model.getEndDayMap().get(wrapper);

                    Date startHoliday = scheduleItem.getFactDate() != null ? scheduleItem.getFactDate() : scheduleItem.getPlanDate();
                    Date finishHoliday = null;

                    if (startHoliday == null)
                        continue;

                    //вычесляем дату окончания планируемого отпуска
                    GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
                    EmployeeWorkWeekDuration workWeekDuration = model.getEmployeePost().getWorkWeekDuration();
                    int counter = 0;
                    int duration = scheduleItem.getDaysAmount();

                    date.setTime(startHoliday);

                    if (duration > 0)
                    {
                        do
                        {
                            if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                                counter++;

                            if (counter != duration)
                                date.add(Calendar.DATE, 1);
                        }
                        while (counter < duration);

                        finishHoliday = date.getTime();
                    }

                    if (finishHoliday == null)
                        continue;

                    if (CommonBaseDateUtil.isBetween(beginDate, startHoliday, finishHoliday) || CommonBaseDateUtil.isBetween(endDate, startHoliday, finishHoliday))
                        errors.add("Указанный отпуск пересекается с планируемым отпуском с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startHoliday) + " по " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(finishHoliday) + " из графика отпусков №" + scheduleItem.getVacationSchedule().getNumber() + " за " +
                                scheduleItem.getVacationSchedule().getYear() + " год от " +
                                DateFormatter.DEFAULT_DATE_FORMATTER.format(scheduleItem.getVacationSchedule().getCreateDate()) + ".",
                                wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");
                    else {
                        if (CommonBaseDateUtil.isBetween(startHoliday, beginDate, endDate) || CommonBaseDateUtil.isBetween(finishHoliday, beginDate, endDate))
                            errors.add("Указанный отпуск пересекается с планируемым отпуском с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startHoliday) + " по " +
                                    DateFormatter.DEFAULT_DATE_FORMATTER.format(finishHoliday) + " из графика отпусков №" + scheduleItem.getVacationSchedule().getNumber() + " за " +
                                    scheduleItem.getVacationSchedule().getYear() + " год от " +
                                    DateFormatter.DEFAULT_DATE_FORMATTER.format(scheduleItem.getVacationSchedule().getCreateDate()) + ".",
                                    wrapper.getId() + "_beginDay", wrapper.getId() + "_endDay");
                    }
                }
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());

        MQBuilder builder = new MQBuilder(ProvideDateToExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", ProvideDateToExtractRelation.L_REVOCATION_FROM_HOLIDAY_EXTRACT, model.getExtract()));

        //удаляем старые релейшены
        for (ProvideDateToExtractRelation relation : builder.<ProvideDateToExtractRelation>getResultList(getSession()))
            delete(relation);

        if (!model.getExtract().isPaymentUnusedHoliday())
            model.getExtract().setCompensationUnusedHoliday(null);

        //если предостовляется часть неиспользуемого отпуска, то создаем новые релейшены
        if (model.getExtract().isProvideUnusedHoliday())
            for (IdentifiableWrapper wrapper : model.getPartHolidayWrapperList())
            {
                ProvideDateToExtractRelation relation = new ProvideDateToExtractRelation();
                relation.setRevocationFromHolidayExtract(model.getExtract());
                relation.setProvideBeginDay(model.getBeginDayMap().get(wrapper));
                relation.setProvideEndDay(model.getEndDayMap().get(wrapper));
                relation.setProvideDurationDay(model.getDurationDayMap().get(wrapper));

                save(relation);
            }
    }
}