/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 *         Created on: 30.10.2009
 */
public class CommonListOrderPrint implements ICommonListOrderPrint
{
    public static final String PARAGRAPHS = "PARAGRAPHS";
//    public static final String EMPLOYEE_LIST = "EMPLOYEE_LIST";
//    public static final IEmployeeListParagraphPrintFormatter EMPLOYEE_LIST_FORMATTER = (post, extractNumber) -> {
//        StringBuilder buffer = new StringBuilder();
//        buffer.append("\\par ").append(extractNumber).append(".  ");
//        buffer.append(post.getPerson().getFullFio()).append(" (");
//        return buffer.toString();
//    };

    public static RtfInjectModifier createListOrderInjectModifier(EmployeeListOrder order)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("commitDate", order.getCommitDate() == null ? "          " : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        injectModifier.put("orderNumber", StringUtils.isEmpty(order.getNumber()) ? "          " : order.getNumber());

        TopOrgUnit academy = TopOrgUnit.getInstance(true);
        injectModifier.put("okpo", null != academy.getOkpo() ? academy.getOkpo() : "");
        injectModifier.put("academy", null != academy.getNominativeCaseTitle() ? academy.getNominativeCaseTitle() : academy.getFullTitle());
        injectModifier.put("topOrgUnitFullTitle", academy.getTitle());
	    injectModifier.put("topOrgUnitCityTitle",
	                       (null != academy.getPostalAddress() && null != academy.getPostalAddress().getSettlement()) ? academy.getPostalAddress().getSettlement().getTitle() :
			                       (null != academy.getLegalAddress() && null != academy.getLegalAddress().getSettlement()) ? academy.getLegalAddress().getSettlement().getTitle() :
					                       (null != academy.getAddress() && null != academy.getAddress().getSettlement()) ? academy.getAddress().getSettlement().getTitle() : "");
//        String executor = order.getExecutor();
//        injectModifier.put("executor", "");
//        injectModifier.put("executorPhone", "");
//        if (StringUtils.isNotEmpty(executor))
//        {
//            int phoneStart = executor.indexOf(" т. ");
//            if (phoneStart == -1)
//            {
//                injectModifier.put("executor", executor);
//            } else
//            {
//                injectModifier.put("executor", executor.substring(0, phoneStart));
//                injectModifier.put("executorPhone", executor.substring(phoneStart + 1));
//            }
//        }

        //initOrgUnit(injectModifier, order.getOrgUnit(), "formativeOrgUnit", "");
        injectModifier.put("listBasics", order.getBasicListStr());

        // Данные об исполнителе
        injectModifier.put("executor", "");
        injectModifier.put("executorPhone", "");
        injectModifier.put("executorStr", "");

        String executor = order.getExecutor();
        if (StringUtils.isNotEmpty(executor))
        {
            int phoneStart = executor.indexOf(" т. ");
            if (phoneStart == -1)
            {
                injectModifier.put("executor", executor);
                injectModifier.put("executorPhone", "");
                injectModifier.put("executorStr", "Исполнитель: " + executor);
            } else
            {
                injectModifier.put("executor", executor.substring(0, phoneStart));
                injectModifier.put("executorPhone", executor.substring(phoneStart + 1));
                injectModifier.put("executorStr", "Исполнитель: " + executor.substring(0, phoneStart) + " (" + executor.substring(phoneStart + 1) + ")");
            }
        }


        return injectModifier;
    }

    public static RtfTableModifier createListOrderTableModifier(EmployeeListOrder order)
    {
        return new RtfTableModifier();
    }

    @Override
    public void appendVisas(RtfTableModifier tableModifier, EmployeeListOrder order)
    {
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        List<String[]> visaData = new ArrayList<>();
        List<String[]> primaryVisaData = new ArrayList<>();
        List<String[]> secondaryVisaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());

        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), "", "", "", str.toString()});

            if (visa.getGroupMemberVising().getCode().equals("2"))
                primaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
            else
                secondaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        tableModifier.put("PRIMARY_VISAS", primaryVisaData.toArray(new String[primaryVisaData.size()][]));
        tableModifier.put("SECONDARY_VISAS", secondaryVisaData.toArray(new String[secondaryVisaData.size()][]));
    }

    public static void injectParagraphs(final RtfDocument document, ParagraphEmployeeListOrder order, IEmployeeListParagraphPrintFormatter formatter)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            List<IRtfElement> parList = new ArrayList<>();

            for (IAbstractParagraph paragraph : order.getParagraphList())
            {
                RtfDocument paragraphPart = getParagraphPart(paragraph);

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    @SuppressWarnings("unchecked")
    public static RtfDocument getParagraphPart(IAbstractParagraph paragraph)
    {
        List<ListEmployeeExtract> extractList = paragraph.getExtractList();
        if (extractList.size() == 0)
            throw new ApplicationException("Невозможно распечатать пустой параграф (№" + paragraph.getNumber() + ").");

        // первая выписка из параграфа
        ListEmployeeExtract extract = extractList.get(0);

        byte[] paragraphTemplate = MoveEmployeeDaoFacade.getMoveEmployeeDao().getTemplate(extract.getType(), 1);

        RtfDocument paragraphPart = new RtfReader().read(paragraphTemplate);

        IListParagraphPrintFormCreator printForm = (IListParagraphPrintFormCreator) ApplicationRuntime.getBean("employeeListOrder" + extract.getType().getIndex() + "_orderPrint");

        RtfInjectModifier injectModifier = printForm.createParagraphInjectModifier(paragraph, extract);
        if (injectModifier != null)
            injectModifier.modify(paragraphPart);

        RtfTableModifier injectTableModifier = printForm.createParagraphTableModifier(paragraph, extract);
        if (injectTableModifier!=null)
            injectTableModifier.modify(paragraphPart);

//        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(paragraphPart, EMPLOYEE_LIST);
//        if (searchResult.isFound())
//        {
//            IListParagraphTextFormatter paragraphTextFormatter = (printForm instanceof IListParagraphTextFormatter) ? (IListParagraphTextFormatter) printForm : null;
//            String paragraphText = null != paragraphTextFormatter ? paragraphTextFormatter.createParagraphText(paragraph) : createParagraphText(paragraph, printForm instanceof IStudentListParagraphPrintFormatter ? (IStudentListParagraphPrintFormatter) printForm : formatter);
//            IRtfText text = RtfBean.getElementFactory().createRtfText(paragraphText);
//            text.setRaw(true);
//
//            searchResult.getElementList().set(searchResult.getIndex(), text);
//        }

        return paragraphPart;
    }
}