package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абстрактная выписка из списочного приказа по сотруднику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ListEmployeeExtractGen extends AbstractEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.ListEmployeeExtract";
    public static final String ENTITY_NAME = "listEmployeeExtract";
    public static final int VERSION_HASH = 1545431579;
    private static IEntityMeta ENTITY_META;

    public static final String L_REASON = "reason";
    public static final String L_EMPLOYEE = "employee";

    private EmployeeOrderReasons _reason;     // Причина приказа по кадрам
    private Employee _employee;     // Кадровый ресурс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null.
     */
    @NotNull
    public EmployeeOrderReasons getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина приказа по кадрам. Свойство не может быть null.
     */
    public void setReason(EmployeeOrderReasons reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     */
    @NotNull
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Кадровый ресурс. Свойство не может быть null.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ListEmployeeExtractGen)
        {
            setReason(((ListEmployeeExtract)another).getReason());
            setEmployee(((ListEmployeeExtract)another).getEmployee());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ListEmployeeExtractGen> extends AbstractEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ListEmployeeExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("ListEmployeeExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return obj.getReason();
                case "employee":
                    return obj.getEmployee();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reason":
                    obj.setReason((EmployeeOrderReasons) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                        return true;
                case "employee":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return true;
                case "employee":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return EmployeeOrderReasons.class;
                case "employee":
                    return Employee.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ListEmployeeExtract> _dslPath = new Path<ListEmployeeExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ListEmployeeExtract");
    }
            

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ListEmployeeExtract#getReason()
     */
    public static EmployeeOrderReasons.Path<EmployeeOrderReasons> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ListEmployeeExtract#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    public static class Path<E extends ListEmployeeExtract> extends AbstractEmployeeExtract.Path<E>
    {
        private EmployeeOrderReasons.Path<EmployeeOrderReasons> _reason;
        private Employee.Path<Employee> _employee;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ListEmployeeExtract#getReason()
     */
        public EmployeeOrderReasons.Path<EmployeeOrderReasons> reason()
        {
            if(_reason == null )
                _reason = new EmployeeOrderReasons.Path<EmployeeOrderReasons>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Кадровый ресурс. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ListEmployeeExtract#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

        public Class getEntityClass()
        {
            return ListEmployeeExtract.class;
        }

        public String getEntityName()
        {
            return "listEmployeeExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
