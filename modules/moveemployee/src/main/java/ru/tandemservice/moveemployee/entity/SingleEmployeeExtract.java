package ru.tandemservice.moveemployee.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.gen.SingleEmployeeExtractGen;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.List;

/**
 * Выписка
 */
public abstract class SingleEmployeeExtract extends SingleEmployeeExtractGen
{
    public static final String[] ORDER_TYPE_TITLE_KEY = new String[]{IAbstractExtract.L_TYPE, ICatalogItem.CATALOG_ITEM_TITLE};
    public static final String[] EMPLOYEE_FIO_KEY = new String[]{SingleEmployeeExtract.L_EMPLOYEE, Employee.L_PERSON, Person.P_FULLFIO};
    public static final String[] ORDER_CREATE_DATE_KEY = new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_CREATE_DATE};
    public static final String[] ORDER_COMMIT_DATE_KEY = new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, AbstractEmployeeOrder.P_COMMIT_DATE};
    public static final String[] ORDER_COMMIT_DATE_SYSTEM_KEY = new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_COMMIT_DATE_SYSTEM};
    public static final String[] ORDER_NUMBER_KEY = new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_NUMBER};
    public static final String[] ORDER_STATE_TITLE_KEY = new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.L_STATE, ICatalogItem.CATALOG_ITEM_TITLE};

    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        StringBuilder extractTitle = new StringBuilder();
        extractTitle.append("Выписка «").append(getType().getTitle()).append("»");
        String employeeWord = null != getEmployee() ? (SexCodes.FEMALE.equals(getEmployee().getPerson().getIdentityCard().getSex().getCode()) ? "сотрудница" : "сотрудник") : "сотрудник";

        if (null != getParagraph().getOrder().getNumber())
        {
            extractTitle.append(" из приказа №").append(getParagraph().getOrder().getNumber());
            if (null != getParagraph().getOrder().getCommitDate())
                extractTitle.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getParagraph().getOrder().getCommitDate()));
        }

        if (getEmployeeTitle().length() > 0)
            extractTitle.append(" | ").append(employeeWord).append(" ").append(getEmployeeTitle());

        return extractTitle.toString();
    }

    public String getBasicListStr()
    {
        List<EmpSingleExtractToBasicRelation> list = MoveEmployeeDaoFacade.getMoveEmployeeDao().getSingleExtractBasicsList(this);
        String[] title = new String[list.size()];
        for (int i = 0; i < list.size(); i++)
            title[i] = list.get(i).getBasic().getTitle() + (StringUtils.isEmpty(list.get(i).getComment()) ? "" : " " + list.get(i).getComment());
        return StringUtils.join(title, "; ");
    }

    @Override
    public boolean isNoDelete()
    {
        return !(UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getParagraph().getOrder().getState().getCode()) || UnimoveDefines.CATALOG_ORDER_STATE_REJECTED.equals(getParagraph().getOrder().getState().getCode()));
    }

    @Override
    public boolean isNoEdit()
    {
        return super.isNoEdit() && !UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER.equals(getState().getCode());
    }
}