/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e23.AddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.moveemployee.entity.IncDecCombinationExtract;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 23.12.2011
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<IncDecCombinationExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        if (model.isEditForm())
        {
            MQBuilder builder = new MQBuilder(CombinationPostStaffRateExtractRelation.ENTITY_CLASS, "cpsrr");
            builder.add(MQExpression.eq("cpsrr", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract().s(), model.getExtract()));

            model.setCombinationPostStaffRateItemList(builder.<CombinationPostStaffRateExtractRelation>getResultList(getSession()));

            if (model.getExtract().getCombinationPost() != null)
            {
                MQBuilder combinationBuilder = new MQBuilder(CombinationPostStaffRateItem.ENTITY_CLASS, "cpsri");
                combinationBuilder.add(MQExpression.eq("cpsri", CombinationPostStaffRateItem.combinationPost().s(), model.getExtract().getCombinationPost()));

                model.setSummStaffRateBefore(0d);
                for (CombinationPostStaffRateItem relation : combinationBuilder.<CombinationPostStaffRateItem>getResultList(getSession()))
                    model.setSummStaffRateBefore(model.getSummStaffRateBefore() + relation.getStaffRate());
            }
        }

        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getEmployeePost());
        model.setDisabledContractFields(contract != null);
        if (contract != null)
        {
            model.getExtract().setContractType(contract.getType());
            model.getExtract().setContractNumber(contract.getNumber());
            model.getExtract().setContractDate(contract.getDate());
            model.getExtract().setContractBeginDate(contract.getBeginDate());
            model.getExtract().setContractEndDate(contract.getEndDate());
        }

        model.setContractTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(LabourContractType.ENTITY_CLASS, "lct");
                if (o != null)
                    builder.add(MQExpression.eq("lct", LabourContractType.id().s(), o));
                builder.addOrder("lct", LabourContractType.title().s());

                return new MQListResultBuilder(builder);
            }
        });

        model.setOrgUnitModel(new OrgUnitAutocompleteModel());

        model.setPostModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getOrgUnit() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                MQBuilder builder = new MQBuilder(CombinationPost.ENTITY_CLASS, "cp");
                builder.add(MQExpression.eq("cp", CombinationPost.employeePost().s(), model.getEmployeePost()));
                builder.add(MQExpression.eq("cp", CombinationPost.orgUnit().s(), model.getExtract().getOrgUnit()));
                if (o != null)
                    builder.add(MQExpression.eq("cp", CombinationPost.id().s(), o));
                builder.addOrder("cp", CombinationPost.postBoundedWithQGandQL().title().s());

                return new MQListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                CombinationPost post = (CombinationPost) value;
                return post.getPostBoundedWithQGandQL().getTitle() + " (" + post.getOrgUnit().getTitle() + ") " +
                        " период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(post.getBeginDate()) +
                        (post.getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(post.getEndDate()) : "");
            }
        });

        model.setMissingEmployeePostModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if  (null == model.getExtract().getCombinationPost() || null == model.getExtract().getOrgUnit())
                    return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
                builder.add(MQExpression.eq("ep", EmployeePost.postRelation().postBoundedWithQGandQL().s(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL()));
                builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT, model.getExtract().getOrgUnit()));
                if (model.getEmployeePost() != null)
                    builder.add(MQExpression.notEq("ep", EmployeePost.P_ID, model.getEmployeePost().getId()));
                builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
                builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
                return new ListResult<>(builder.<EmployeePost>getResultList(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EmployeePost post = (EmployeePost) value;
                return post.getExtendedPostTitle();
            }
        });

        model.setRaisingCoefficientListModel(new FullCheckSelectModel(SalaryRaisingCoefficient.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getExtract().getCombinationPost())
                {
                    return ListResult.getEmpty();
                }

                MQBuilder builder = new MQBuilder(SalaryRaisingCoefficient.ENTITY_CLASS, "rc");
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_POST, model.getExtract().getCombinationPost().getPostBoundedWithQGandQL().getPost()));
                builder.add(MQExpression.eq("rc", SalaryRaisingCoefficient.L_QUALIFICATION_LEVEL, model.getExtract().getCombinationPost().getPostBoundedWithQGandQL().getQualificationLevel()));
                builder.add(MQExpression.like("rc", SalaryRaisingCoefficient.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("rc", SalaryRaisingCoefficient.P_RAISING_COEFFICIENT);
                builder.addOrder("rc", SalaryRaisingCoefficient.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setEtksLevelModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EtksLevels.ENTITY_CLASS, "l");
                builder.add(MQExpression.like("l", EtksLevels.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("l", EtksLevels.title().s());
                if (o != null)
                    builder.add(MQExpression.eq("l", EtksLevels.id().s(), o));

                return new MQListResultBuilder(builder);
            }
        });

        model.setFinancingSourceModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getOrgUnit() == null || model.getExtract().getCombinationPost() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());


                if (model.isThereAnyActiveStaffList())
                {
                    StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getExtract().getOrgUnit());

                    MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sli", new String[]{StaffListItem.financingSource().s()});
                    builder.add(MQExpression.eq("sli", StaffListItem.staffList().s(), activeStaffList));
                    builder.add(MQExpression.eq("sli", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL()));
                    builder.add(MQExpression.like("sli", StaffListItem.financingSource().title().s(), CoreStringUtils.escapeLike(filter)));
                    builder.setNeedDistinct(true);
                    if (o != null)
                        builder.add(MQExpression.eq("sli",StaffListItem.financingSource().id().s(), o));

                    return new MQListResultBuilder(builder);
                }
                else
                {
                    MQBuilder builder = new MQBuilder(FinancingSource.ENTITY_CLASS, "fs");
                    builder.add(MQExpression.like("fs", FinancingSource.title().s(), CoreStringUtils.escapeLike(filter)));
                    if (o != null)
                        builder.add(MQExpression.eq("fs", FinancingSource.id().s(), o));

                    return new MQListResultBuilder(builder);
                }
            }
        });

        model.setFinancingSourceItemModel(new CommonSingleSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                FinancingSource financingSource = finSrcMap.get(id);

                if (model.getExtract().getOrgUnit() == null || model.getExtract().getCombinationPost() == null || financingSource == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());


                if (model.isThereAnyActiveStaffList())
                {
                    StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getExtract().getOrgUnit());

                    MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sl", new String[] {StaffListItem.L_FINANCING_SOURCE_ITEM});
                    builder.add(MQExpression.eq("sl", StaffListItem.staffList().s(), staffList));
                    builder.add(MQExpression.eq("sl", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL()));
                    builder.add(MQExpression.eq("sl", StaffListItem.financingSource().s(), financingSource));
                    builder.add(MQExpression.isNotNull("sl", StaffListItem.financingSourceItem().s()));
                    builder.add(MQExpression.like("sl", StaffListItem.financingSourceItem().title().s(), CoreStringUtils.escapeLike(filter)));
                    if (o != null)
                        builder.add(MQExpression.eq("sl", StaffListItem.financingSourceItem().id().s(), o));
                    builder.setNeedDistinct(true);

                    return new MQListResultBuilder(builder);
                }
                else
                {
                    MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "fsi");
                    builder.add(MQExpression.eq("fsi", FinancingSourceItem.financingSource().id().s(), financingSource.getId()));
                    builder.add(MQExpression.like("fsi", FinancingSourceItem.P_TITLE, CoreStringUtils.escapeLike(filter)));
                    if (o != null)
                        builder.add(MQExpression.eq("fsi", FinancingSourceItem.id().s(), o));

                    return new MQListResultBuilder(builder);
                }
            }
        });

        model.setEmployeeHRModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> objects = findValues("").getObjects();
                List<IdentifiableWrapper> resultList = new ArrayList<>();

                for (IdentifiableWrapper wrapper : objects)
                {
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);
                }

                return resultList;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                String newStaffRateStr = "<Новая ставка> - ";
                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
                final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

                FinancingSource financingSource = finSrcMap.get(id);
                FinancingSourceItem financingSourceItem = finSrcItmMap.get(id);

                if (financingSource == null || model.getExtract().getOrgUnit() == null || model.getExtract().getCombinationPost() == null)
                    return ListResult.getEmpty();

                Set<IdentifiableWrapper> resultList = new HashSet<>();
                List<StaffListAllocationItem> staffListAllocList = new ArrayList<>();

                StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getExtract().getOrgUnit());

                MQBuilder allocBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "a");
                allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.staffListItem().staffList().s(), staffList));
                allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL()));
                allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.financingSource().s(), financingSource));
                allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.financingSourceItem().s(), financingSourceItem));
                allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.employeePost().postStatus().active().s(), false));

                staffListAllocList.addAll(allocBuilder.<StaffListAllocationItem>getResultList(getSession()));

//                if (allocationItemList.size() > 0)
//                    if (allocationItemList.get(0).getStaffListItem().getStaffRate() - allocationItemList.get(0).getStaffListItem().getOccStaffRate() <= 0)
//                        staffListAllocList = new LinkedList<StaffListAllocationItem>();

                if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(model.getExtract().getOrgUnit(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL(), financingSource, financingSourceItem))
                {
                    Double summStaffRate = 0.0d;
                    for (StaffListAllocationItem item : staffListAllocList)
                        summStaffRate += item.getStaffRate();

                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getExtract().getOrgUnit(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL(), financingSource, financingSourceItem);
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate() - summStaffRate;
                    if (diff > 0 && newStaffRateStr.contains(filter))
                        resultList.add(new IdentifiableWrapper(0L, newStaffRateStr + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                if (model.isEditForm())
                {
                    MQBuilder subBuilder = new MQBuilder(CombinationPostStaffRateExtractRelation.ENTITY_CLASS, "srr");
                    subBuilder.add(MQExpression.eq("srr", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract().s(), model.getExtract()));
                    subBuilder.add(MQExpression.eq("srr", CombinationPostStaffRateExtractRelation.financingSource().s(), financingSource));
                    subBuilder.add(MQExpression.eq("srr", CombinationPostStaffRateExtractRelation.financingSourceItem().s(), financingSourceItem));

                    MQBuilder builder = new MQBuilder(CombinationPostStaffRateExtAllocItemRelation.ENTITY_CLASS, "srar");
                    builder.add(MQExpression.in("srar", CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation().s(), subBuilder));

                    for (CombinationPostStaffRateExtAllocItemRelation relation : builder.<CombinationPostStaffRateExtAllocItemRelation>getResultList(getSession()))
                        if (!relation.isHasNewAllocItem() && relation.getChoseStaffListAllocationItem() != null)
                            if (relation.getChoseStaffListAllocationItem().getStaffListItem().getStaffList().getOrgUnit().equals(model.getExtract().getOrgUnit()))
                                if (relation.getChoseStaffListAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().equals(model.getExtract().getCombinationPost().getPostBoundedWithQGandQL()))
                                    resultList.add(new IdentifiableWrapper(relation.getChoseStaffListAllocationItem().getId(), relation.getChoseStaffListAllocationItem().getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(relation.getChoseStaffListAllocationItem().getStaffRate()) + " - " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPostType().getShortTitle() + " " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPostStatus().getShortTitle()));
                }

                if (model.getExtract().getCombinationPost() != null)
                {
                    MQBuilder builder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "slai");
                    builder.add(MQExpression.eq("slai", StaffListAllocationItem.combination().s(), true));
                    builder.add(MQExpression.eq("slai", StaffListAllocationItem.staffListItem().staffList().s(), staffList));
                    builder.add(MQExpression.eq("slai", StaffListAllocationItem.combinationPost().s(), model.getExtract().getCombinationPost()));
                    builder.add(MQExpression.eq("slai", StaffListAllocationItem.financingSource().s(), financingSource));
                    builder.add(MQExpression.eq("slai", StaffListAllocationItem.financingSourceItem().s(), financingSourceItem));

                    for (StaffListAllocationItem item : builder.<StaffListAllocationItem>getResultList(getSession()))
                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                for (StaffListAllocationItem item : staffListAllocList)
                {
                    if (item.getEmployeePost().getPerson().getFullFio().contains(filter))
                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                return new ListResult<>(resultList);
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        actualizeStaffRateItemList(model);

        //final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        //final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());
        //final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        //final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());
        //final IValueMapHolder finSrcItemHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        //final Map<Long, FinancingSourceItem> finSrcItemMap = (null == finSrcItemHolder ? Collections.emptyMap() : finSrcItemHolder.getValueMap());
        final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
        final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());

        //если была доступна колонка Кадровой расстановки, достаем из враперов объекты Штатной расстановки
        final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
        //Мапа, указывающая на то выбрана ли строчка <Новая ставка> в мультиселекте
        Map<Long, Boolean> hasNewStaffRateMap = new HashMap<>();
        if (model.isThereAnyActiveStaffList())
        {
            for (IEntity entity : model.getCombinationPostStaffRateItemList())
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                        allocationItemList.add(item);
                    }

                }

                allocItemsMap.put(entity.getId(), allocationItemList);

                hasNewStaffRateMap.put(entity.getId(), empHRMap.get(entity.getId()).contains(new IdentifiableWrapper(0L, "")));
            }
        }

        if (model.getCombinationPostStaffRateItemList().isEmpty())
            errors.add("Необходимо указать ставку для должности по совмещению.");

        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
            for (CombinationPostStaffRateExtractRelation item2nd : model.getCombinationPostStaffRateItemList())
                if (!item.equals(item2nd))
                    if (item.getFinancingSource().equals(item2nd.getFinancingSource()))
                        if ((item.getFinancingSourceItem() == null && item2nd.getFinancingSourceItem() == null) ||
                                ((item.getFinancingSourceItem() != null && item2nd.getFinancingSourceItem() != null) && item.getFinancingSourceItem().equals(item2nd.getFinancingSourceItem())))
                            errors.add("Набор полей «Источник финансирования», «Источник финансирования (детально)» должен быть уникальным в рамках должности по совмещению.",
                                    "finSrc_id_" + item.getId(), "finSrcItm_id_" + item.getId());

        if (model.isThereAnyActiveStaffList() && !errors.hasErrors())
            for (CombinationPostStaffRateExtractRelation staffRateItem : model.getCombinationPostStaffRateItemList())
            {
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getExtract().getOrgUnit(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL(), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());
                Double diff = 0.0d;
                if (staffListItem != null)
                    diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
                BigDecimal x = new BigDecimal(diff);
                x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
                diff = x.doubleValue();

                for (StaffListAllocationItem allocationItem : UniempDaoFacade.getStaffListDAO().getStaffListItemAllocations(staffListItem))
                    if (allocationItem.getEmployeePost() != null && !allocationItem.getEmployeePost().getPostStatus().isActive() &&
                            !allocItemsMap.get(staffRateItem.getId()).contains(allocationItem))
                        diff -= allocationItem.getStaffRate();

                Double sumStaffRateAllocItems = 0.0d;
                for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
                {
                    sumStaffRateAllocItems += allocItem.getStaffRate();

//                    if (allocItem.getEmployeePost().equals(model.getEmployeePost())
//                            && allocItem.getEmployeePost().getPostStatus().isActive())
//                        diff += allocItem.getStaffRate();
                }

                if ((staffRateItem.getStaffRate() > new BigDecimal(sumStaffRateAllocItems).setScale(2, RoundingMode.HALF_UP).doubleValue() && !hasNewStaffRateMap.get(staffRateItem.getId())) ||
                        (staffRateItem.getStaffRate() > new BigDecimal(diff + sumStaffRateAllocItems).setScale(2, RoundingMode.HALF_UP).doubleValue() && hasNewStaffRateMap.get(staffRateItem.getId())))
                    errors.add("Размер ставки " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() +
                            " превышает суммарную ставку, выбранную в поле «Кадровая расстановка».",
                            "staffRate_id_" + staffRateItem.getId());

                if (staffRateItem.getStaffRate() <= new BigDecimal(sumStaffRateAllocItems).setScale(2, RoundingMode.HALF_UP).doubleValue() && hasNewStaffRateMap.get(staffRateItem.getId()))
                    errors.add("Необходимо точнее указать занимаемые ставки в поле «Кадровая расстановка» для "
                            + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() + ".",
                            "employeeHR_Id_" + staffRateItem.getId());
            }

        Double combinationStaffRate = 0d;
        List<String> staffRateFieldList = new ArrayList<>();
        for (CombinationPostStaffRateExtractRelation staffRateItem : model.getCombinationPostStaffRateItemList())
        {
            combinationStaffRate += staffRateItem.getStaffRate();

            staffRateFieldList.add("staffRate_id_" + staffRateItem.getId());
        }

        Double employeeStaffRate = 0d;
        for (EmployeePostStaffRateItem staffRateItem : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost()))
            employeeStaffRate += staffRateItem.getStaffRate();

        if (combinationStaffRate > employeeStaffRate)
            errors.add("Cтавка должности по совмещению сотрудника превышает суммарный размер ставки основной должности сотрудника.", staffRateFieldList.toArray(new String[staffRateFieldList.size()]));

        if (model.getExtract().getCombinationPost() != null)
            if (combinationStaffRate.equals(model.getSummStaffRateBefore()))
                errors.add("Размер ставки должности по совмещению должен измениться.", staffRateFieldList.toArray(new String[staffRateFieldList.size()]));

        if ((CommonBaseDateUtil.isBefore(model.getExtract().getBeginDate(), model.getExtract().getContractBeginDate())) ||
                (model.getExtract().getContractEndDate() != null && CommonBaseDateUtil.isAfter(model.getExtract().getBeginDate(), model.getExtract().getContractEndDate())))
            errors.add("Период увеличения объема работ должен попадать в период начала - окончания работы сотрудника.", "beginDate", "endDate");
        else if (model.getExtract().getEndDate() != null)
            if (CommonBaseDateUtil.isBefore(model.getExtract().getEndDate(), model.getExtract().getContractBeginDate()) ||
                    (model.getExtract().getContractEndDate() != null && CommonBaseDateUtil.isAfter(model.getExtract().getEndDate(), model.getExtract().getContractEndDate())))
                errors.add("Период увеличения объема работ должен попадать в период начала - окончания работы сотрудника.", "beginDate", "endDate");

        if (model.getExtract().getCombinationPost() != null)
        {
            if (model.getExtract().getCombinationPost().getEndDate() != null)
            {
                if (!CommonBaseDateUtil.isBetween(model.getExtract().getBeginDate(), model.getExtract().getCombinationPost().getBeginDate(), model.getExtract().getCombinationPost().getEndDate())
                        || model.getExtract().getBeginDate().equals(model.getExtract().getCombinationPost().getBeginDate()))
                    errors.add("Дата начала увеличения (уменьшения) работы должна быть позже даты начала и не позже даты окончания работы по выбранной должности.", "beginDate");
            }
            else if (!CommonBaseDateUtil.isAfter(model.getExtract().getBeginDate(), model.getExtract().getCombinationPost().getBeginDate()))
                errors.add("Дата начала увеличения (уменьшения) работы должна быть позднее даты начала работы по выбранной должности.", "beginDate");
        }

        if (model.getExtract().getEndDate() != null)
            if (model.getExtract().getBeginDate().getTime() > model.getExtract().getEndDate().getTime())
                errors.add("Дата начала должна быть меньше даты окончания.", "beginDate", "endDate");

        MQBuilder builder = new MQBuilder(CombinationPost.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", CombinationPost.employeePost().s(), model.getEmployeePost()));
        builder.add(MQExpression.eq("b", CombinationPost.orgUnit().s(), model.getExtract().getOrgUnit()));
        builder.add(MQExpression.eq("b", CombinationPost.postBoundedWithQGandQL().s(), model.getExtract().getCombinationPost().getPostBoundedWithQGandQL()));
        if (model.getExtract().getCombinationPost() != null)
            builder.add(MQExpression.notEq("b", CombinationPost.id().s(), model.getExtract().getCombinationPost().getId()));

        Date beginDate = model.getExtract().getBeginDate();
        Date endDate = model.getExtract().getEndDate();

        for (CombinationPost combinationPost : builder.<CombinationPost>getResultList(getSession()))
        {
            Date begin = combinationPost.getBeginDate();
            Date end = combinationPost.getEndDate();

            if (end != null)
            {
                if (endDate != null)
                {
                    if (CommonBaseDateUtil.isBetween(beginDate, begin, end) || CommonBaseDateUtil.isBetween(endDate, begin, end))
                        errors.add("Даты начала и окончания работы по одинаковой должности по совмещению не должны пересекаться.",
                                "beginDate", "endDate");
                    else {
                        if (CommonBaseDateUtil.isBetween(begin, beginDate, endDate) || CommonBaseDateUtil.isBetween(end, beginDate, endDate))
                            errors.add("Даты начала и окончания работы по одинаковой должности по совмещению не должны пересекаться.",
                                    "beginDate", "endDate");
                    }
                }
                else
                {
                    if (CommonBaseDateUtil.isBefore(beginDate, end) || beginDate.equals(end))
                        errors.add("Даты начала и окончания работы по одинаковой должности по совмещению не должны пересекаться.",
                                "beginDate", "endDate");
                }
            }
            else
            {
                if (endDate != null)
                {
                    if (CommonBaseDateUtil.isAfter(endDate, begin) || endDate.equals(begin))
                        errors.add("Даты начала и окончания работы по одинаковой должности по совмещению не должны пересекаться.",
                                "beginDate", "endDate");
                }
                else
                {
                    errors.add("Даты начала и окончания работы по одинаковой должности по совмещению не должны пересекаться.",
                                "beginDate", "endDate");
                }
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());

        Double combinationStaffRate = 0d;
        for (CombinationPostStaffRateExtractRelation staffRateItem : model.getCombinationPostStaffRateItemList())
            combinationStaffRate += staffRateItem.getStaffRate();

        if (model.getExtract().getCombinationPost() != null)
        {
            if (combinationStaffRate > model.getSummStaffRateBefore())
                model.getExtract().setIncreased(true);
            else
                model.getExtract().setIncreased(false);

            model.getExtract().setStaffRateBefore(model.getSummStaffRateBefore());
        }
        else
            model.getExtract().setIncreased(true);

        model.getExtract().setStaffRateAfter(combinationStaffRate);

        actualizeStaffRateItemList(model);

        //если редактируем выписку, то перед тем как создать новые релейшены ставок - удаляем старые
        if (model.isEditForm())
        {
            MQBuilder subBuilder = new MQBuilder(CombinationPostStaffRateExtractRelation.ENTITY_CLASS, "b");
            subBuilder.add(MQExpression.eq("b", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract().s(), model.getExtract()));

            MQBuilder builder = new MQBuilder(CombinationPostStaffRateExtAllocItemRelation.ENTITY_CLASS, "a");
            builder.add(MQExpression.in("a", CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation().s(), subBuilder));

            for (CombinationPostStaffRateExtAllocItemRelation relation : builder.<CombinationPostStaffRateExtAllocItemRelation>getResultList(getSession()))
                delete(relation);

            for (CombinationPostStaffRateExtractRelation relation : subBuilder.<CombinationPostStaffRateExtractRelation>getResultList(getSession()))
                delete(relation);
        }

        //Мапа, id энтини сечлиста на выбранные в мультиселекте объекты штатной расстановки
        final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
        if (model.isThereAnyActiveStaffList())
        {
            //достаем элементы штатной расстановки, которые выбраны в мультиселектах
            final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
            final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());

            //достаем из враперов объекты Штатной расстановки
            for (Long entityId : ids(model.getCombinationPostStaffRateItemList()))
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entityId))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = IUniBaseDao.instance.get().getNotNull(StaffListAllocationItem.class, wrapper.getId());
                        allocationItemList.add(item);
                    }
                    else
                    {
                        StaffListAllocationItem allocationItem = new StaffListAllocationItem();
                        allocationItem.setId(0L);
                        allocationItemList.add(allocationItem);
                    }
                }
                allocItemsMap.put(entityId, allocationItemList);
            }
        }

        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
        {
            Long id = item.getId();//т.к. после save id поменяется, мы его сохраняем что бы достать из мапы соответствующий ставке набор allocItem

            save(item);

            if (model.isThereAnyActiveStaffList())
                for (StaffListAllocationItem allocationItem : allocItemsMap.get(id))
                {
                    CombinationPostStaffRateExtAllocItemRelation relation = new CombinationPostStaffRateExtAllocItemRelation();

                    if (allocationItem.getId() != 0L)
                    {
                        if (allocationItem.isCombination() && allocationItem.getCombinationPost() != null)
                        {
                            relation.setHasCombinationPost(true);
                            relation.setCombinationPostHistory(allocationItem.getCombinationPost());
                        }
                        else
                            relation.setHasCombinationPost(false);
                        relation.setChoseStaffListAllocationItem(allocationItem);
                        relation.setHasNewAllocItem(false);
                        relation.setEmployeePostHistory(allocationItem.getEmployeePost());
                        relation.setStaffRateHistory(allocationItem.getStaffRate());
                    }
                    else
                    {
                        relation.setHasNewAllocItem(true);
                        relation.setHasCombinationPost(false);
                    }
                    relation.setCombinationPostStaffRateExtractRelation(item);

                    save(relation);
                }
        }
    }

    @Override
    public Map<CombinationPostStaffRateItem, List<StaffListAllocationItem>> getCombinationPostStaffRateAllocMap(Model model)
    {
        Map<CombinationPostStaffRateItem, List<StaffListAllocationItem>> resultMap = new HashMap<>();

        MQBuilder staffRateBuilder = new MQBuilder(CombinationPostStaffRateItem.ENTITY_CLASS, "cpsri");
        staffRateBuilder.add(MQExpression.eq("cpsri", CombinationPostStaffRateItem.combinationPost().s(), model.getExtract().getCombinationPost()));

        List<CombinationPostStaffRateItem> staffRateItemList = staffRateBuilder.getResultList(getSession());

        for (CombinationPostStaffRateItem item : staffRateItemList)
            resultMap.put(item, new ArrayList<>());

        StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getExtract().getOrgUnit());
        MQBuilder allocBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "ai");
        allocBuilder.add(MQExpression.eq("ai", StaffListAllocationItem.staffListItem().staffList().s(), staffList));
        allocBuilder.add(MQExpression.eq("ai", StaffListAllocationItem.combinationPost().s(), model.getExtract().getCombinationPost()));

        List<StaffListAllocationItem> allocationItemList = allocBuilder.getResultList(getSession());

        for (CombinationPostStaffRateItem rateItem : staffRateItemList)
        {
            List<StaffListAllocationItem> tempAllocationItemList = new ArrayList<>();

            for (StaffListAllocationItem allocationItem : allocationItemList)
                if (allocationItem.getCombinationPost().equals(rateItem.getCombinationPost()) &&
                        allocationItem.getFinancingSource().equals(rateItem.getFinancingSource()) &&
                                ((allocationItem.getFinancingSourceItem() == null && rateItem.getFinancingSourceItem() == null) ||
                                allocationItem.getFinancingSourceItem().equals(rateItem.getFinancingSourceItem())))
                    tempAllocationItemList.add(allocationItem);

            resultMap.put(rateItem, tempAllocationItemList);
        }

        return resultMap;
    }

    @Override
    public Map<CombinationPostStaffRateExtractRelation, List<CombinationPostStaffRateExtAllocItemRelation>> getStaffRateAllocItemMap(Model model)
    {
        MQBuilder builder = new MQBuilder(CombinationPostStaffRateExtAllocItemRelation.ENTITY_CLASS, "c");
        builder.add(MQExpression.in("c", CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation().s(), model.getCombinationPostStaffRateItemList()));
        List<CombinationPostStaffRateExtAllocItemRelation> relationList = builder.getResultList(getSession());

        Map<CombinationPostStaffRateExtractRelation, List<CombinationPostStaffRateExtAllocItemRelation>> resultMap = new HashMap<>();
        for (CombinationPostStaffRateExtractRelation relation : model.getCombinationPostStaffRateItemList())
        {
            List<CombinationPostStaffRateExtAllocItemRelation> allocItemRelationList = resultMap.get(relation);
            if (allocItemRelationList == null)
                allocItemRelationList = new ArrayList<>();

            for (CombinationPostStaffRateExtAllocItemRelation allocItemRelation : relationList)
            {
                if (allocItemRelation.getCombinationPostStaffRateExtractRelation().equals(relation))
                    allocItemRelationList.add(allocItemRelation);

                resultMap.put(relation, allocItemRelationList);
            }
        }

        return resultMap;
    }

    @SuppressWarnings("unchecked")
    public void actualizeStaffRateItemList(Model model)
    {
        //достаем введенные знаечения для Ставок и обновляем их в списке
        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

        final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                val = ((Number)value).doubleValue();
            }
            item.setStaffRate(val);
            item.setFinancingSource(finSrcMap.get(item.getId()));
            item.setFinancingSourceItem(finSrcItmMap.get(item.getId()));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareStaffRateDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getCombinationPostStaffRateItemList());
    }

    @Override
    public void prepareEmployeeStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getEmployeeStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getEmployeeStaffRateDataSource(), staffRateItems);
    }

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected IncDecCombinationExtract createNewInstance()
    {
        return new IncDecCombinationExtract();
    }
}