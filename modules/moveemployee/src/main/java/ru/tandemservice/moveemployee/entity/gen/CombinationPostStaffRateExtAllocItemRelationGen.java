package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь ставки должности по совмещению в выписке и должностей ШР
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CombinationPostStaffRateExtAllocItemRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation";
    public static final String ENTITY_NAME = "combinationPostStaffRateExtAllocItemRelation";
    public static final int VERSION_HASH = 1916183307;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMBINATION_POST_STAFF_RATE_EXTRACT_RELATION = "combinationPostStaffRateExtractRelation";
    public static final String L_CHOSE_STAFF_LIST_ALLOCATION_ITEM = "choseStaffListAllocationItem";
    public static final String L_CREATE_STAFF_LIST_ALLOCATION_ITEM = "createStaffListAllocationItem";
    public static final String P_HAS_NEW_ALLOC_ITEM = "hasNewAllocItem";
    public static final String P_HAS_COMBINATION_POST = "hasCombinationPost";
    public static final String P_STAFF_RATE_HISTORY = "staffRateHistory";
    public static final String L_EMPLOYEE_POST_HISTORY = "employeePostHistory";
    public static final String L_COMBINATION_POST_HISTORY = "combinationPostHistory";

    private CombinationPostStaffRateExtractRelation _combinationPostStaffRateExtractRelation;     // Связь выписки и ставки должности по совмещению
    private StaffListAllocationItem _choseStaffListAllocationItem;     // Выбранная ставка штатной расстановки
    private StaffListAllocationItem _createStaffListAllocationItem;     // Созданная ставка штатной расстановки
    private boolean _hasNewAllocItem;     // Выбрана ли Новая ставка
    private boolean _hasCombinationPost;     // Является ставкой должности по совмещению
    private Double _staffRateHistory;     // Кол-во штатных единиц из выбранной ставки штатной расстановки
    private EmployeePost _employeePostHistory;     // Сотрудник на которого ссылалась выбранная ставка штатной расстановки
    private CombinationPost _combinationPostHistory;     // Должность по совмещению на которую ссылалась выбранная ставка штатной расстановки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Связь выписки и ставки должности по совмещению. Свойство не может быть null.
     */
    @NotNull
    public CombinationPostStaffRateExtractRelation getCombinationPostStaffRateExtractRelation()
    {
        return _combinationPostStaffRateExtractRelation;
    }

    /**
     * @param combinationPostStaffRateExtractRelation Связь выписки и ставки должности по совмещению. Свойство не может быть null.
     */
    public void setCombinationPostStaffRateExtractRelation(CombinationPostStaffRateExtractRelation combinationPostStaffRateExtractRelation)
    {
        dirty(_combinationPostStaffRateExtractRelation, combinationPostStaffRateExtractRelation);
        _combinationPostStaffRateExtractRelation = combinationPostStaffRateExtractRelation;
    }

    /**
     * @return Выбранная ставка штатной расстановки.
     */
    public StaffListAllocationItem getChoseStaffListAllocationItem()
    {
        return _choseStaffListAllocationItem;
    }

    /**
     * @param choseStaffListAllocationItem Выбранная ставка штатной расстановки.
     */
    public void setChoseStaffListAllocationItem(StaffListAllocationItem choseStaffListAllocationItem)
    {
        dirty(_choseStaffListAllocationItem, choseStaffListAllocationItem);
        _choseStaffListAllocationItem = choseStaffListAllocationItem;
    }

    /**
     * @return Созданная ставка штатной расстановки.
     */
    public StaffListAllocationItem getCreateStaffListAllocationItem()
    {
        return _createStaffListAllocationItem;
    }

    /**
     * @param createStaffListAllocationItem Созданная ставка штатной расстановки.
     */
    public void setCreateStaffListAllocationItem(StaffListAllocationItem createStaffListAllocationItem)
    {
        dirty(_createStaffListAllocationItem, createStaffListAllocationItem);
        _createStaffListAllocationItem = createStaffListAllocationItem;
    }

    /**
     * @return Выбрана ли Новая ставка. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasNewAllocItem()
    {
        return _hasNewAllocItem;
    }

    /**
     * @param hasNewAllocItem Выбрана ли Новая ставка. Свойство не может быть null.
     */
    public void setHasNewAllocItem(boolean hasNewAllocItem)
    {
        dirty(_hasNewAllocItem, hasNewAllocItem);
        _hasNewAllocItem = hasNewAllocItem;
    }

    /**
     * @return Является ставкой должности по совмещению. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasCombinationPost()
    {
        return _hasCombinationPost;
    }

    /**
     * @param hasCombinationPost Является ставкой должности по совмещению. Свойство не может быть null.
     */
    public void setHasCombinationPost(boolean hasCombinationPost)
    {
        dirty(_hasCombinationPost, hasCombinationPost);
        _hasCombinationPost = hasCombinationPost;
    }

    /**
     * @return Кол-во штатных единиц из выбранной ставки штатной расстановки.
     */
    public Double getStaffRateHistory()
    {
        return _staffRateHistory;
    }

    /**
     * @param staffRateHistory Кол-во штатных единиц из выбранной ставки штатной расстановки.
     */
    public void setStaffRateHistory(Double staffRateHistory)
    {
        dirty(_staffRateHistory, staffRateHistory);
        _staffRateHistory = staffRateHistory;
    }

    /**
     * @return Сотрудник на которого ссылалась выбранная ставка штатной расстановки.
     */
    public EmployeePost getEmployeePostHistory()
    {
        return _employeePostHistory;
    }

    /**
     * @param employeePostHistory Сотрудник на которого ссылалась выбранная ставка штатной расстановки.
     */
    public void setEmployeePostHistory(EmployeePost employeePostHistory)
    {
        dirty(_employeePostHistory, employeePostHistory);
        _employeePostHistory = employeePostHistory;
    }

    /**
     * @return Должность по совмещению на которую ссылалась выбранная ставка штатной расстановки.
     */
    public CombinationPost getCombinationPostHistory()
    {
        return _combinationPostHistory;
    }

    /**
     * @param combinationPostHistory Должность по совмещению на которую ссылалась выбранная ставка штатной расстановки.
     */
    public void setCombinationPostHistory(CombinationPost combinationPostHistory)
    {
        dirty(_combinationPostHistory, combinationPostHistory);
        _combinationPostHistory = combinationPostHistory;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CombinationPostStaffRateExtAllocItemRelationGen)
        {
            setCombinationPostStaffRateExtractRelation(((CombinationPostStaffRateExtAllocItemRelation)another).getCombinationPostStaffRateExtractRelation());
            setChoseStaffListAllocationItem(((CombinationPostStaffRateExtAllocItemRelation)another).getChoseStaffListAllocationItem());
            setCreateStaffListAllocationItem(((CombinationPostStaffRateExtAllocItemRelation)another).getCreateStaffListAllocationItem());
            setHasNewAllocItem(((CombinationPostStaffRateExtAllocItemRelation)another).isHasNewAllocItem());
            setHasCombinationPost(((CombinationPostStaffRateExtAllocItemRelation)another).isHasCombinationPost());
            setStaffRateHistory(((CombinationPostStaffRateExtAllocItemRelation)another).getStaffRateHistory());
            setEmployeePostHistory(((CombinationPostStaffRateExtAllocItemRelation)another).getEmployeePostHistory());
            setCombinationPostHistory(((CombinationPostStaffRateExtAllocItemRelation)another).getCombinationPostHistory());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CombinationPostStaffRateExtAllocItemRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CombinationPostStaffRateExtAllocItemRelation.class;
        }

        public T newInstance()
        {
            return (T) new CombinationPostStaffRateExtAllocItemRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "combinationPostStaffRateExtractRelation":
                    return obj.getCombinationPostStaffRateExtractRelation();
                case "choseStaffListAllocationItem":
                    return obj.getChoseStaffListAllocationItem();
                case "createStaffListAllocationItem":
                    return obj.getCreateStaffListAllocationItem();
                case "hasNewAllocItem":
                    return obj.isHasNewAllocItem();
                case "hasCombinationPost":
                    return obj.isHasCombinationPost();
                case "staffRateHistory":
                    return obj.getStaffRateHistory();
                case "employeePostHistory":
                    return obj.getEmployeePostHistory();
                case "combinationPostHistory":
                    return obj.getCombinationPostHistory();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "combinationPostStaffRateExtractRelation":
                    obj.setCombinationPostStaffRateExtractRelation((CombinationPostStaffRateExtractRelation) value);
                    return;
                case "choseStaffListAllocationItem":
                    obj.setChoseStaffListAllocationItem((StaffListAllocationItem) value);
                    return;
                case "createStaffListAllocationItem":
                    obj.setCreateStaffListAllocationItem((StaffListAllocationItem) value);
                    return;
                case "hasNewAllocItem":
                    obj.setHasNewAllocItem((Boolean) value);
                    return;
                case "hasCombinationPost":
                    obj.setHasCombinationPost((Boolean) value);
                    return;
                case "staffRateHistory":
                    obj.setStaffRateHistory((Double) value);
                    return;
                case "employeePostHistory":
                    obj.setEmployeePostHistory((EmployeePost) value);
                    return;
                case "combinationPostHistory":
                    obj.setCombinationPostHistory((CombinationPost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "combinationPostStaffRateExtractRelation":
                        return true;
                case "choseStaffListAllocationItem":
                        return true;
                case "createStaffListAllocationItem":
                        return true;
                case "hasNewAllocItem":
                        return true;
                case "hasCombinationPost":
                        return true;
                case "staffRateHistory":
                        return true;
                case "employeePostHistory":
                        return true;
                case "combinationPostHistory":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "combinationPostStaffRateExtractRelation":
                    return true;
                case "choseStaffListAllocationItem":
                    return true;
                case "createStaffListAllocationItem":
                    return true;
                case "hasNewAllocItem":
                    return true;
                case "hasCombinationPost":
                    return true;
                case "staffRateHistory":
                    return true;
                case "employeePostHistory":
                    return true;
                case "combinationPostHistory":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "combinationPostStaffRateExtractRelation":
                    return CombinationPostStaffRateExtractRelation.class;
                case "choseStaffListAllocationItem":
                    return StaffListAllocationItem.class;
                case "createStaffListAllocationItem":
                    return StaffListAllocationItem.class;
                case "hasNewAllocItem":
                    return Boolean.class;
                case "hasCombinationPost":
                    return Boolean.class;
                case "staffRateHistory":
                    return Double.class;
                case "employeePostHistory":
                    return EmployeePost.class;
                case "combinationPostHistory":
                    return CombinationPost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CombinationPostStaffRateExtAllocItemRelation> _dslPath = new Path<CombinationPostStaffRateExtAllocItemRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CombinationPostStaffRateExtAllocItemRelation");
    }
            

    /**
     * @return Связь выписки и ставки должности по совмещению. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getCombinationPostStaffRateExtractRelation()
     */
    public static CombinationPostStaffRateExtractRelation.Path<CombinationPostStaffRateExtractRelation> combinationPostStaffRateExtractRelation()
    {
        return _dslPath.combinationPostStaffRateExtractRelation();
    }

    /**
     * @return Выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getChoseStaffListAllocationItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> choseStaffListAllocationItem()
    {
        return _dslPath.choseStaffListAllocationItem();
    }

    /**
     * @return Созданная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getCreateStaffListAllocationItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> createStaffListAllocationItem()
    {
        return _dslPath.createStaffListAllocationItem();
    }

    /**
     * @return Выбрана ли Новая ставка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#isHasNewAllocItem()
     */
    public static PropertyPath<Boolean> hasNewAllocItem()
    {
        return _dslPath.hasNewAllocItem();
    }

    /**
     * @return Является ставкой должности по совмещению. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#isHasCombinationPost()
     */
    public static PropertyPath<Boolean> hasCombinationPost()
    {
        return _dslPath.hasCombinationPost();
    }

    /**
     * @return Кол-во штатных единиц из выбранной ставки штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getStaffRateHistory()
     */
    public static PropertyPath<Double> staffRateHistory()
    {
        return _dslPath.staffRateHistory();
    }

    /**
     * @return Сотрудник на которого ссылалась выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getEmployeePostHistory()
     */
    public static EmployeePost.Path<EmployeePost> employeePostHistory()
    {
        return _dslPath.employeePostHistory();
    }

    /**
     * @return Должность по совмещению на которую ссылалась выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getCombinationPostHistory()
     */
    public static CombinationPost.Path<CombinationPost> combinationPostHistory()
    {
        return _dslPath.combinationPostHistory();
    }

    public static class Path<E extends CombinationPostStaffRateExtAllocItemRelation> extends EntityPath<E>
    {
        private CombinationPostStaffRateExtractRelation.Path<CombinationPostStaffRateExtractRelation> _combinationPostStaffRateExtractRelation;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _choseStaffListAllocationItem;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _createStaffListAllocationItem;
        private PropertyPath<Boolean> _hasNewAllocItem;
        private PropertyPath<Boolean> _hasCombinationPost;
        private PropertyPath<Double> _staffRateHistory;
        private EmployeePost.Path<EmployeePost> _employeePostHistory;
        private CombinationPost.Path<CombinationPost> _combinationPostHistory;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Связь выписки и ставки должности по совмещению. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getCombinationPostStaffRateExtractRelation()
     */
        public CombinationPostStaffRateExtractRelation.Path<CombinationPostStaffRateExtractRelation> combinationPostStaffRateExtractRelation()
        {
            if(_combinationPostStaffRateExtractRelation == null )
                _combinationPostStaffRateExtractRelation = new CombinationPostStaffRateExtractRelation.Path<CombinationPostStaffRateExtractRelation>(L_COMBINATION_POST_STAFF_RATE_EXTRACT_RELATION, this);
            return _combinationPostStaffRateExtractRelation;
        }

    /**
     * @return Выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getChoseStaffListAllocationItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> choseStaffListAllocationItem()
        {
            if(_choseStaffListAllocationItem == null )
                _choseStaffListAllocationItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_CHOSE_STAFF_LIST_ALLOCATION_ITEM, this);
            return _choseStaffListAllocationItem;
        }

    /**
     * @return Созданная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getCreateStaffListAllocationItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> createStaffListAllocationItem()
        {
            if(_createStaffListAllocationItem == null )
                _createStaffListAllocationItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_CREATE_STAFF_LIST_ALLOCATION_ITEM, this);
            return _createStaffListAllocationItem;
        }

    /**
     * @return Выбрана ли Новая ставка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#isHasNewAllocItem()
     */
        public PropertyPath<Boolean> hasNewAllocItem()
        {
            if(_hasNewAllocItem == null )
                _hasNewAllocItem = new PropertyPath<Boolean>(CombinationPostStaffRateExtAllocItemRelationGen.P_HAS_NEW_ALLOC_ITEM, this);
            return _hasNewAllocItem;
        }

    /**
     * @return Является ставкой должности по совмещению. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#isHasCombinationPost()
     */
        public PropertyPath<Boolean> hasCombinationPost()
        {
            if(_hasCombinationPost == null )
                _hasCombinationPost = new PropertyPath<Boolean>(CombinationPostStaffRateExtAllocItemRelationGen.P_HAS_COMBINATION_POST, this);
            return _hasCombinationPost;
        }

    /**
     * @return Кол-во штатных единиц из выбранной ставки штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getStaffRateHistory()
     */
        public PropertyPath<Double> staffRateHistory()
        {
            if(_staffRateHistory == null )
                _staffRateHistory = new PropertyPath<Double>(CombinationPostStaffRateExtAllocItemRelationGen.P_STAFF_RATE_HISTORY, this);
            return _staffRateHistory;
        }

    /**
     * @return Сотрудник на которого ссылалась выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getEmployeePostHistory()
     */
        public EmployeePost.Path<EmployeePost> employeePostHistory()
        {
            if(_employeePostHistory == null )
                _employeePostHistory = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST_HISTORY, this);
            return _employeePostHistory;
        }

    /**
     * @return Должность по совмещению на которую ссылалась выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation#getCombinationPostHistory()
     */
        public CombinationPost.Path<CombinationPost> combinationPostHistory()
        {
            if(_combinationPostHistory == null )
                _combinationPostHistory = new CombinationPost.Path<CombinationPost>(L_COMBINATION_POST_HISTORY, this);
            return _combinationPostHistory;
        }

        public Class getEntityClass()
        {
            return CombinationPostStaffRateExtAllocItemRelation.class;
        }

        public String getEntityName()
        {
            return "combinationPostStaffRateExtAllocItemRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
