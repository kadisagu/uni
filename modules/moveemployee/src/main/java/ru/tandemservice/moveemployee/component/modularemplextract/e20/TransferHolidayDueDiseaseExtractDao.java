/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e20;

import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.util.UniempUtil;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 06.12.2011
 */
public class TransferHolidayDueDiseaseExtractDao extends UniBaseDao implements IExtractComponentDao<TransferHolidayDueDiseaseExtract>
{
    public void doCommit(TransferHolidayDueDiseaseExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        EmployeeHoliday employeeHoliday = extract.getEmployeeHoliday();
        if (extract.getTransferDaysAmount() == employeeHoliday.getDuration())
        {
            if (extract.getDateToTransfer() != null)
            {
                extract.setDeleteHoliday(false);
                extract.setHolidayStartOld(employeeHoliday.getStartDate());
                extract.setHolidayFinishOld(employeeHoliday.getFinishDate());
                extract.setHolidayExtractOld(employeeHoliday.getHolidayExtract());
                extract.setHolidayExtractNumberOld(employeeHoliday.getHolidayExtractNumber());
                extract.setHolidayExtractDateOld(employeeHoliday.getHolidayExtractDate());

                employeeHoliday.setStartDate(extract.getDateToTransfer());
                employeeHoliday.setFinishDate(UniempUtil.getEndDate(extract.getEntity().getWorkWeekDuration(), extract.getTransferDaysAmount(), extract.getDateToTransfer()));
                employeeHoliday.setHolidayExtract(extract);
                employeeHoliday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                employeeHoliday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

                update(extract);
                update(employeeHoliday);
            }
            else
            {
                extract.setDeleteHoliday(true);
                extract.setHolidayStartOld(employeeHoliday.getStartDate());
                extract.setHolidayFinishOld(employeeHoliday.getFinishDate());
                extract.setHolidayDurationOld(employeeHoliday.getDuration());
                extract.setHolidayBeginOld(employeeHoliday.getBeginDate());
                extract.setHolidayEndOld(employeeHoliday.getEndDate());
                extract.setHolidayTitleOld(employeeHoliday.getTitle());
                extract.setHolidayTypeOld(employeeHoliday.getHolidayType());
                extract.setHolidayExtractOld(employeeHoliday.getHolidayExtract());
                extract.setHolidayExtractNumberOld(employeeHoliday.getHolidayExtractNumber());
                extract.setHolidayExtractDateOld(employeeHoliday.getHolidayExtractDate());

                delete(employeeHoliday);
                update(extract);
            }
        }
        else if (extract.getTransferDaysAmount() < employeeHoliday.getDuration())
        {
            extract.setDeleteHoliday(false);
            extract.setHolidayFinishOld(employeeHoliday.getFinishDate());
            extract.setHolidayDurationOld(employeeHoliday.getDuration());
            extract.setHolidayExtractOld(employeeHoliday.getHolidayExtract());
            extract.setHolidayExtractNumberOld(employeeHoliday.getHolidayExtractNumber());
            extract.setHolidayExtractDateOld(employeeHoliday.getHolidayExtractDate());

            employeeHoliday.setFinishDate(UniempUtil.getEndDate(extract.getEntity().getWorkWeekDuration(), extract.getTransferDaysAmount() * -1, employeeHoliday.getFinishDate()));
            employeeHoliday.setDuration(employeeHoliday.getDuration() - extract.getTransferDaysAmount());
            employeeHoliday.setHolidayExtract(extract);
            employeeHoliday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
            employeeHoliday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

            if (extract.getDateToTransfer() != null)
            {
                EmployeeHoliday holiday = new EmployeeHoliday();
                holiday.setEmployeePost(extract.getEntity());
                holiday.setHolidayType(employeeHoliday.getHolidayType());
                holiday.setTitle(employeeHoliday.getTitle());
                holiday.setBeginDate(employeeHoliday.getBeginDate());
                holiday.setEndDate(employeeHoliday.getEndDate());
                holiday.setStartDate(extract.getDateToTransfer());
                holiday.setFinishDate(UniempUtil.getEndDate(extract.getEntity().getWorkWeekDuration(), extract.getTransferDaysAmount(), extract.getDateToTransfer()));
                holiday.setDuration(extract.getTransferDaysAmount());
                holiday.setHolidayExtract(extract);
                holiday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                holiday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

                extract.setCreateHoliday(holiday);

                save(holiday);
            }

            update(employeeHoliday);
            update(extract);
        }
    }

    public void doRollback(TransferHolidayDueDiseaseExtract extract, Map parameters)
    {
        if (extract.getDeleteHoliday())
        {
            EmployeeHoliday holiday = new EmployeeHoliday();
            holiday.setEmployeePost(extract.getEntity());
            holiday.setHolidayType(extract.getHolidayTypeOld());
            holiday.setTitle(extract.getHolidayTitleOld());
            holiday.setBeginDate(extract.getHolidayBeginOld());
            holiday.setEndDate(extract.getHolidayEndOld());
            holiday.setStartDate(extract.getHolidayStartOld());
            holiday.setFinishDate(extract.getHolidayFinishOld());
            holiday.setDuration(extract.getHolidayDurationOld());
            holiday.setHolidayExtract(extract.getHolidayExtractOld());
            holiday.setHolidayExtractNumber(extract.getHolidayExtractNumberOld());
            holiday.setHolidayExtractDate(extract.getHolidayExtractDateOld());

            extract.setDeleteHoliday(null);
            extract.setHolidayStartOld(null);
            extract.setHolidayFinishOld(null);
            extract.setHolidayDurationOld(null);
            extract.setHolidayBeginOld(null);
            extract.setHolidayEndOld(null);
            extract.setHolidayTitleOld(null);
            extract.setHolidayTypeOld(null);
            extract.setHolidayExtractOld(null);
            extract.setHolidayExtractNumberOld(null);
            extract.setHolidayExtractDateOld(null);

            if (extract.getEmployeeHoliday() == null)
                extract.setEmployeeHoliday(holiday);

            save(holiday);
            update(extract);
        }
        else
        {
            EmployeeHoliday holiday = extract.getEmployeeHoliday();
            if (extract.getHolidayStartOld() != null)
            {
                holiday.setStartDate(extract.getHolidayStartOld());
                extract.setHolidayStartOld(null);
            }
            if (extract.getHolidayFinishOld() != null)
            {
                holiday.setFinishDate(extract.getHolidayFinishOld());
                extract.setHolidayFinishOld(null);
            }
            if (extract.getHolidayDurationOld() != null)
            {
                holiday.setDuration(extract.getHolidayDurationOld());
                extract.setHolidayDurationOld(null);
            }
            if (extract.getHolidayExtractOld() != null)
            {
                holiday.setHolidayExtract(extract.getHolidayExtractOld());
                extract.setHolidayExtractOld(null);
            }
            else
            {
                holiday.setHolidayExtract(null);
            }

            if (extract.getHolidayExtractDateOld() != null)
            {
                holiday.setHolidayExtractDate(extract.getHolidayExtractDateOld());
                extract.setHolidayExtractDateOld(null);
            }
            if (extract.getHolidayExtractNumberOld() != null)
            {
                holiday.setHolidayExtractNumber(extract.getHolidayExtractNumberOld());
                extract.setHolidayExtractNumberOld(null);
            }

            if (extract.getCreateHoliday() != null)
            {
                EmployeeHoliday createHoliday = extract.getCreateHoliday();
                extract.setCreateHoliday(null);
                delete(createHoliday);
            }
        }
    }
}