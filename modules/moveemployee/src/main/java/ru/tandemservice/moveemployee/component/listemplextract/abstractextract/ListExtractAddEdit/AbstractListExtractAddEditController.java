/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
public abstract class AbstractListExtractAddEditController<T extends ListEmployeeExtract, IDAO extends IAbstractListExtractAddEditDAO<T, Model>, Model extends AbstractListExtractAddEditModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onChangeBasics(IBusinessComponent component)
    {
//        Model model = getModel(component);
//
//        EmployeeLabourContract contract = getDao().get(EmployeeLabourContract.class, EmployeeLabourContract.employeePost().s(), model.getEmployeePost());
//
//        if (contract == null)
//            return;
//
//        for (EmployeeOrderBasics basic : model.getSelectedBasicList())
//        {
//            if (basic.isLaborContract() && model.getCurrentBasicMap().get(basic.getId()) == null)
//            {
//                StringBuilder basicBuilder = new StringBuilder("№").append(contract.getNumber()).append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getDate()));
//                model.getCurrentBasicMap().put(basic.getId(), basicBuilder.toString());
//            }
//        }
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        getDao().validate(model, errors);
        if (errors.hasErrors()) return;

        getDao().update(model);
        deactivate(component);

        if (model.getExtractId() == null)
        {
            // форма добавления
            int extractIndex = model.getExtract().getType().getIndex();
            activateInRoot(component, new ComponentActivator(MoveEmployeeUtils.getListExtractPubComponent(extractIndex), new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, model.getExtract().getId())));
        }
    }

    public void onChangeReason(IBusinessComponent component)
    {

    }
}