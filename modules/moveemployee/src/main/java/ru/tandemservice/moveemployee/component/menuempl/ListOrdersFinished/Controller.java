/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.menuempl.ListOrdersFinished;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractOrder;

/**
 * @author dseleznev
 * Created on: 30.10.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "EmplListOrdersFinished.filter"));

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<IAbstractOrder> dataSource = UniBaseUtils.createDataSource(component, getDao());
//        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", IAbstractOrder.P_CREATE_DATE);
//        linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
//        linkColumn.setResolver(new DefaultPublisherLinkResolver()
//        {
//            @Override
//            public String getComponentName(IEntity entity)
//            {
//                EmployeeExtractType type = (EmployeeExtractType) entity.getProperty(EmployeeListOrder.L_TYPE);
//                int typeIndex = type.getIndex();
//                return MoveEmployeeUtils.getListOrderPubComponent(typeIndex);
//            }
//        });
//        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Дата формирования", IAbstractOrder.P_CREATE_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Дата приказа", AbstractEmployeeOrder.P_COMMIT_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", IAbstractOrder.P_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn(IAbstractOrder.P_COMMIT_DATE_SYSTEM, "Дата проведения", IAbstractOrder.P_COMMIT_DATE_SYSTEM, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        //dataSource.addColumn(new SimpleColumn("Подразделение", Model.ORG_UNI_FULL_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип приказа", new String[]{EmployeeListOrder.L_TYPE, EmployeeExtractType.P_TITLE}).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во параграфов", AbstractEmployeeOrder.P_COUNT_PARAGRAPH).setClickable(false).setOrderable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("print_menuList_listEmployeeOrdersFinished"));

        dataSource.setOrder(IAbstractOrder.P_COMMIT_DATE_SYSTEM, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        DataSettingsFacade.saveSettings(getModel(component).getSettings());
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        EmployeeListOrder order = getDao().get((Long) component.getListenerParameter());

        if (order instanceof ParagraphEmployeeListOrder)
            activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_PARAGRAPH_LIST_ORDER_PRINT, new ParametersMap()
                    .add("orderId", component.getListenerParameter())
            ));
        else
            activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_LIST_ORDER_PRINT, new ParametersMap()
                    .add("orderId", component.getListenerParameter())
            ));
    }
}