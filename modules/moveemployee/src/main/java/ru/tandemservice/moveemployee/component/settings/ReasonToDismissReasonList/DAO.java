/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.settings.ReasonToDismissReasonList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author dseleznev
 * Created on: 22.09.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        Map<Long, EmployeeReasonToDismissReasonRel> relsMap = new HashMap<Long, EmployeeReasonToDismissReasonRel>();
        List<EmployeeReasonToDismissReasonRel> relsList = getSession().createCriteria(EmployeeReasonToDismissReasonRel.class).list();
        for (EmployeeReasonToDismissReasonRel rel : relsList) relsMap.put(rel.getEmployeeOrderReason().getId(), rel);

        MQBuilder builder = new MQBuilder(EmployeeOrderReasons.ENTITY_CLASS, "r");
        builder.addOrder("r", EmployeeOrderReasons.P_TITLE);
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            if (relsMap.containsKey(wrapper.getEntity().getId()))
            {
                wrapper.setViewProperty(Model.P_DISMISS_REASON, relsMap.get(wrapper.getEntity().getId()).getEmployeeDismissReason().getTitle());
                wrapper.setViewProperty(Model.P_DELETE_DISABLED, false);
            }
            else
            {
                wrapper.setViewProperty(Model.P_DISMISS_REASON, "");
                wrapper.setViewProperty(Model.P_DELETE_DISABLED, true);
            }
        }
    }

    @Override
    public void deleteRelation(Long reasonId)
    {
        EmployeeOrderReasons reason = get(EmployeeOrderReasons.class, reasonId);

        MQBuilder builder = new MQBuilder(EmployeeReasonToDismissReasonRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", EmployeeReasonToDismissReasonRel.L_EMPLOYEE_ORDER_REASON, reason));
        List<EmployeeReasonToDismissReasonRel> list = builder.getResultList(getSession());
        for (EmployeeReasonToDismissReasonRel rel : list) delete(rel);
    }
}