/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractAddEdit.SingleEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */

public abstract class CommonSingleEmployeeExtractAddEditDAO<T extends SingleEmployeeExtract, Model extends CommonSingleEmployeeExtractAddEditModel<T>> extends SingleEmployeeExtractAddEditDAO<T, Model>
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setExecutorListModel(new OrderExecutorSelectModel());
        model.setReasonList(getFirstList(EmployeeReasonToTypeRel.class, model.getExtractType()));
        model.setBasicListModel(new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult<EmployeeReasonToBasicRel> findValues(String filter)
            {
                if (model.getExtract() == null) return ListResult.getEmpty();

                Criteria c = getSession().createCriteria(EmployeeReasonToBasicRel.class, "r");
                c.createAlias("r." + IEntityRelation.L_SECOND, "s");
                c.add(Restrictions.eq("r." + IEntityRelation.L_FIRST, model.getExtract().getReason()));
                c.setProjection(Projections.property("r." + IEntityRelation.L_SECOND));
                c.add(Restrictions.like("s." + EmployeeOrderBasics.P_TITLE, CoreStringUtils.escapeLike(filter)).ignoreCase());
                c.addOrder(Order.asc(("s." + EmployeeOrderBasics.P_TITLE)));
                return new ListResult(c.list());
            }
        });

        if (model.isEditForm())
        {
            List<EmpSingleExtractToBasicRelation> relationList = getList(EmpSingleExtractToBasicRelation.class, EmpSingleExtractToBasicRelation.L_EXTRACT, model.getExtract());

            model.getSelectedBasicList().clear();
            for (EmpSingleExtractToBasicRelation relation : relationList)
                model.getSelectedBasicList().add(relation.getBasic());
            model.setSelectedBasicList(model.getSelectedBasicList());

            model.getCurrentBasicMap().clear();
            for (EmpSingleExtractToBasicRelation relation : relationList)
            {
                model.getCurrentBasicMap().put(relation.getBasic().getId(), relation.getComment());
            }
        }
        else
        {
            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            if (principalContext instanceof EmployeePost)
                model.setExecutor((EmployeePost) principalContext);
        }

        model.setTextParagraph(model.getExtract().getTextParagraph());
    }

    @Override
    public void update(Model model)
    {
        String executor = OrderExecutorSelectModel.getExecutor(model.getExecutor());
        if (executor != null) model.getOrder().setExecutor(executor);

        super.update(model);
        model.getExtract().setTextParagraph(model.getTextParagraph());
        updateBasicList(model);
    }

    private void updateBasicList(Model model)
    {
        if (model.isEditForm())
        {
            for (EmpSingleExtractToBasicRelation relation : getList(EmpSingleExtractToBasicRelation.class, EmpSingleExtractToBasicRelation.L_EXTRACT, model.getExtract()))
                delete(relation);
            getSession().flush();
        }

        for (EmployeeOrderBasics basic : model.getSelectedBasicList())
        {
            EmpSingleExtractToBasicRelation relation = new EmpSingleExtractToBasicRelation();
            relation.setBasic(basic);
            relation.setExtract(model.getExtract());
            relation.setComment(model.getCurrentBasicMap().get(basic.getId()));
            save(relation);
        }
    }

    public void prepareTextParagraph(Model model)
    {
        MQBuilder builder = new MQBuilder(EmployeeReasonToTextParagraphRel.ENTITY_CLASS, "b", new String[] {EmployeeReasonToTextParagraphRel.P_TEXT_PARAGRAPH});
        builder.add(MQExpression.eq("b", EmployeeReasonToTextParagraphRel.L_EMPLOYEE_ORDER_REASON, model.getExtract().getReason()));
        model.setTextParagraph((String) builder.uniqueResult(getSession()));
    }
}