/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e15.AddEdit;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendar;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendarHoliday;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendarHolidayDuration;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.Holiday;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeWorkWeekDurationCodes;
import org.tandemframework.shared.organization.base.util.IHolidayDuration;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 24.03.2011
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<ProlongationAnnualHolidayExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected ProlongationAnnualHolidayExtract createNewInstance()
    {
        return new ProlongationAnnualHolidayExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        //ставка
        if (model.getExtract().getEntity() != null)
        {

            model.setEmployeePost(model.getExtract().getEntity());
        }

        //список отпусков. требуются Ежегодные отпуска данного сотрудника
        EmployeePost employeePost;
        if (model.getExtract().getEntity() != null)
        {
            employeePost = model.getExtract().getEntity();
        }
        else
            employeePost = model.getEmployeePost();

        MQBuilder employeeAnnualHoliday = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eah");
        employeeAnnualHoliday.add(MQExpression.eq("eah", EmployeeHoliday.L_EMPLOYEE_POST, employeePost));
        employeeAnnualHoliday.add(MQExpression.eq("eah",  EmployeeHoliday.holidayType().code().s(), UniempDefines.HOLIDAY_TYPE_ANNUAL));
        employeeAnnualHoliday.addOrder("eah", EmployeeHoliday.P_START_DATE, OrderDirection.desc);
        model.setHolidayList(employeeAnnualHoliday.<EmployeeHoliday>getResultList(getSession()));

        //продлить с
        GregorianCalendar nextDate = (GregorianCalendar)GregorianCalendar.getInstance();
        if (model.getExtract().getHoliday() != null)
        {
            nextDate.setTime(model.getExtract().getHoliday().getFinishDate());
            nextDate.add(Calendar.DATE, 1);
            model.getExtract().setProlongationFrom(nextDate.getTime());
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());

        model.getExtract().setHolidayEndOld(model.getExtract().getHoliday().getFinishDate());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if ((model.getExtract().getProlongationFrom() != null && model.getExtract().getProlongationTo() != null) &&
                (model.getExtract().getProlongationFrom().getTime() > model.getExtract().getProlongationTo().getTime()))
        {
            errors.add("Дата начала продления не может быть больше даты окончания продления.", "prolongationFrom", "prolongationTo");
        }

        if ((model.getExtract().getHolidayBegin() != null && model.getExtract().getHolidayEnd() != null) &&
                (model.getExtract().getHolidayBegin().getTime() > model.getExtract().getHolidayEnd().getTime()))
        {
            errors.add("Дата начала отпуска не может быть больше даты окончания.", "holidayBegin", "holidayEnd");
        }

        MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
        empHolidayBuilder.add(MQExpression.eq("eh", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
        for (EmployeeHoliday holiday : empHolidayBuilder.<EmployeeHoliday>getResultList(getSession()))
        {
            Date beginDate = model.getExtract().getProlongationFrom();
            Date endDate = model.getExtract().getProlongationTo();

            if (beginDate == null || endDate == null)
                return;

            if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                        "prolongationFrom", "prolongationTo");
            else {
                if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                    errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                            "prolongationFrom", "prolongationTo");
            }
        }
    }

    @Override
    public boolean isIndustrialCalendarHoliday(EmployeeWorkWeekDuration weekDuration, Date date)
    {
        GregorianCalendar dayDate = (GregorianCalendar)GregorianCalendar.getInstance();

        dayDate.setTime(date);

        MQBuilder calendarBuilder = new MQBuilder(IndustrialCalendar.ENTITY_CLASS, "ic");
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.P_YEAR, dayDate.get(Calendar.YEAR)));
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.L_WEEK_DURATION, weekDuration));
        IndustrialCalendar calendar = (IndustrialCalendar)calendarBuilder.uniqueResult(getSession());
        if (null == calendar)
            return false;

        MQBuilder builder = new MQBuilder(IndustrialCalendarHoliday.ENTITY_CLASS, "ich");
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_CALENDAR, calendar));
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_HOLIDAY + "." + Holiday.P_PREHOLIDAY, Boolean.FALSE));
        List<IHolidayDuration> holidays = builder.getResultList(getSession());

        builder = new MQBuilder(IndustrialCalendarHolidayDuration.ENTITY_CLASS, "ichd");
        builder.add(MQExpression.in("ichd", IndustrialCalendarHolidayDuration.L_HOLIDAY, holidays));
        builder.add(MQExpression.greatOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        builder.add(MQExpression.lessOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        holidays.addAll(builder.<IHolidayDuration>getResultList(getSession()));

        for(IHolidayDuration holiday : holidays)
        {
            if (holiday.getMonth() != (dayDate.get(Calendar.MONTH) + 1))
                continue;

            int begin = holiday.getBeginDay();
            int end = null == holiday.getEndDay() ? begin : holiday.getEndDay();

            if (holiday.getMonth() == dayDate.get(Calendar.MONTH) + 1 &&
                    dayDate.get(Calendar.DAY_OF_MONTH) <= end &&
                    dayDate.get(Calendar.DAY_OF_MONTH) >= begin)
                return true;
        }
        return false;
    }

    @Override
    public Integer getEmployeeHolidayDuration(EmployeeWorkWeekDuration weekDuration, Date tmpBeginDate, Date tmpEndDate)
    {
        if (weekDuration == null)
            return null;

        Date veryTmpDate = CommonBaseDateUtil.getMinDate(tmpBeginDate, tmpEndDate);
        tmpEndDate = CommonBaseDateUtil.getMaxDate(tmpBeginDate, tmpEndDate);
        tmpBeginDate = veryTmpDate;

        Integer resultDuration = CommonBaseDateUtil.getDifferenceBetweenDatesInDays(tmpBeginDate, tmpEndDate);
        if (null == resultDuration)
            return null;
        if (!EmployeeWorkWeekDurationCodes.WEEK_5.equals(weekDuration.getCode())
                && !EmployeeWorkWeekDurationCodes.WEEK_6.equals(weekDuration.getCode()))
        {
            return resultDuration;
        }
        GregorianCalendar beginDate = (GregorianCalendar)GregorianCalendar.getInstance();
        GregorianCalendar globalEndDate = (GregorianCalendar)GregorianCalendar.getInstance();
        beginDate.setTime(tmpBeginDate);
        globalEndDate.setTime(tmpEndDate);
        GregorianCalendar endDate = (GregorianCalendar)GregorianCalendar.getInstance();
        do
        {
            if (beginDate.get(Calendar.YEAR) == globalEndDate.get(Calendar.YEAR))
            {
                endDate = globalEndDate;
            } else
            {
                endDate.set(Calendar.YEAR, beginDate.get(Calendar.YEAR));
                endDate.set(Calendar.DAY_OF_YEAR, endDate.getActualMaximum(Calendar.DAY_OF_YEAR));
            }

            MQBuilder calendarBuilder = new MQBuilder(IndustrialCalendar.ENTITY_CLASS, "ic");
            calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.P_YEAR, beginDate.get(Calendar.YEAR)));
            calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.L_WEEK_DURATION, weekDuration));
            IndustrialCalendar calendar = (IndustrialCalendar)calendarBuilder.uniqueResult(getSession());
            if (null == calendar)
                return resultDuration;

            MQBuilder builder = new MQBuilder(IndustrialCalendarHoliday.ENTITY_CLASS, "ich");
            builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_CALENDAR, calendar));
            builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_HOLIDAY + "." + Holiday.P_PREHOLIDAY, Boolean.FALSE));
            List<IHolidayDuration> holidays = builder.getResultList(getSession());

            builder = new MQBuilder(IndustrialCalendarHolidayDuration.ENTITY_CLASS, "ichd");
            builder.add(MQExpression.in("ichd", IndustrialCalendarHolidayDuration.L_HOLIDAY, holidays));
            builder.add(MQExpression.greatOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                    beginDate.get(Calendar.MONTH) + 1));
            builder.add(MQExpression.lessOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                    endDate.get(Calendar.MONTH) + 1));
            holidays.addAll(builder.<IHolidayDuration>getResultList(getSession()));


            for(IHolidayDuration holiday : holidays)
            {
                if (holiday.getMonth() < (beginDate.get(Calendar.MONTH) + 1) || holiday.getMonth() > (endDate.get(Calendar.MONTH) + 1))
                    continue;

                int begin = holiday.getBeginDay();

                if (holiday.getMonth() == beginDate.get(Calendar.MONTH) + 1 && beginDate.get(Calendar.DAY_OF_MONTH) > begin)
                    continue;

                int end = null == holiday.getEndDay() ? begin : holiday.getEndDay();
                if (holiday.getMonth() == endDate.get(Calendar.MONTH) + 1)
                    end = Math.min(endDate.get(Calendar.DAY_OF_MONTH), end);

                if (end - begin + 1 < 0)
                    continue;
                resultDuration -= (end - begin + 1);
            }

            beginDate.set(beginDate.get(Calendar.YEAR) + 1, Calendar.JANUARY, 1);
        } while(beginDate.get(Calendar.YEAR) <= globalEndDate.get(Calendar.YEAR));
        return resultDuration;
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }
}