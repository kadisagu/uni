/* $Id$ */
package ru.tandemservice.moveemployee.base.ext.Depersonalization.objects;

import com.google.common.collect.ImmutableList;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.application.IModuleMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.DepersonalizationUtils;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.IDepersonalizationObject;
import ru.tandemservice.moveemployee.entity.*;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 14.11.2014
 */
public class SomeEmployeeExtractDO implements IDepersonalizationObject
{
    public static final String NAME = SomeEmployeeExtractDO.class.getSimpleName();

    @Override
    public String getTitle()
    {
        return "Данные по удостоверениям личности и ФИО из выписок.";
    }

    @Override
    public Collection<Class<? extends IEntity>> getEntityClasses()
    {
        final ImmutableList.Builder<Class<? extends IEntity>> builder = ImmutableList.builder();
        return builder
                .add(EmployeeSurnameChangeExtract.class)
                .add(EmployeeAddExtract.class)
                .add(EmployeeTempAddExtract.class)
                .add(EmployeePostAddExtract.class)
                .add(EmployeePostPPSAddExtract.class)
                .add(RevocationFromHolidayExtract.class)
                .add(ReturnFromChildCareHolidayExtract.class)
                .add(ContractTerminationExtract.class)
                .add(DismissalExtract.class)
                .add(VoluntaryTerminationSExtract.class)
                .build();
    }

    @Override
    public String getSettingsName()
    {
        return NAME;
    }

    @Override
    public Collection<String> getDescription()
    {
        return ImmutableList.of(
                "Все ФИО заменяются на " + REPLACEMENT_FIO + ".",
                "Все ФИО родственников заменяются на " + REPLACEMENT_FIO2 + ".",
                "Серия УЛ заменяется на " + REPLACEMENT_SERIA + ".",
                "Номер УЛ заменяется на " + REPLACEMENT_NUMBER + ".",
                "Место и дата выдачи УЛ удаляются."
        );
    }

    @Override
    public IModuleMeta getModule()
    {
        return DepersonalizationUtils.getModuleMeta(EmployeeSurnameChangeExtract.class);
    }

    @Override
    public void apply(Session session)
    {
        CommonDAO.executeAndClear(new DQLUpdateBuilder(EmployeeSurnameChangeExtract.class)
                .set(EmployeeSurnameChangeExtract.P_LAST_NAME, value(REPLACEMENT_LAST_NAME))
                .set(EmployeeSurnameChangeExtract.P_FIRST_NAME, value(REPLACEMENT_FIRST_NAME))
                .set(EmployeeSurnameChangeExtract.P_MIDDLE_NAME, value(REPLACEMENT_MIDDLE_NAME))
                .set(EmployeeSurnameChangeExtract.P_IC_SERIA, value(REPLACEMENT_SERIA))
                .set(EmployeeSurnameChangeExtract.P_IC_NUMBER, value(REPLACEMENT_NUMBER))
                .set(EmployeeSurnameChangeExtract.P_IC_ISSUANCE_DATE, nul(PropertyType.DATE))
                .set(EmployeeSurnameChangeExtract.P_IC_ISSUANCE_PLACE, nul(PropertyType.STRING)),
                session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(EmployeeAddExtract.class).set(EmployeeAddExtract.P_EMPLOYEE_FIO_MODIFIED1, value(REPLACEMENT_FIO)), session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(EmployeeTempAddExtract.class).set(EmployeeTempAddExtract.P_EMPLOYEE_FIO_MODIFIED1, value(REPLACEMENT_FIO)), session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(EmployeePostAddExtract.class).set(EmployeePostAddExtract.P_EMPLOYEE_FIO_MODIFIED1, value(REPLACEMENT_FIO)), session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(EmployeePostPPSAddExtract.class).set(EmployeePostPPSAddExtract.P_EMPLOYEE_FIO_MODIFIED1, value(REPLACEMENT_FIO)), session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(RevocationFromHolidayExtract.class).set(RevocationFromHolidayExtract.P_EMPLOYEE_FIO_DATIVE, value(REPLACEMENT_FIO_DATIVE)), session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(ReturnFromChildCareHolidayExtract.class).set(ReturnFromChildCareHolidayExtract.P_DATIVE_FIO, value(REPLACEMENT_FIO_DATIVE)), session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(ContractTerminationExtract.class).set(ContractTerminationExtract.P_RELATIVE_FIO, value(REPLACEMENT_FIO2)), session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(DismissalExtract.class).set(DismissalExtract.P_RELATIVE_FIO, value(REPLACEMENT_FIO2)), session);

        CommonDAO.executeAndClear(new DQLUpdateBuilder(VoluntaryTerminationSExtract.class).set(VoluntaryTerminationSExtract.P_RELATIVE_FIO, value(REPLACEMENT_FIO2)), session);
    }
}