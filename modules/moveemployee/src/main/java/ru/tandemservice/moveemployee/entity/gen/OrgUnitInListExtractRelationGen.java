package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.OrgUnitInListExtractRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выбранных подразделений в списочном параграфе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitInListExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.OrgUnitInListExtractRelation";
    public static final String ENTITY_NAME = "orgUnitInListExtractRelation";
    public static final int VERSION_HASH = 421609048;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_LIST_PARAGRAPH = "employeeListParagraph";
    public static final String L_ORG_UNIT = "orgUnit";

    private EmployeeListParagraph _employeeListParagraph;     // Параграф списочного приказа по сотруднику
    private OrgUnit _orgUnit;     // Подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Параграф списочного приказа по сотруднику. Свойство не может быть null.
     */
    @NotNull
    public EmployeeListParagraph getEmployeeListParagraph()
    {
        return _employeeListParagraph;
    }

    /**
     * @param employeeListParagraph Параграф списочного приказа по сотруднику. Свойство не может быть null.
     */
    public void setEmployeeListParagraph(EmployeeListParagraph employeeListParagraph)
    {
        dirty(_employeeListParagraph, employeeListParagraph);
        _employeeListParagraph = employeeListParagraph;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitInListExtractRelationGen)
        {
            setEmployeeListParagraph(((OrgUnitInListExtractRelation)another).getEmployeeListParagraph());
            setOrgUnit(((OrgUnitInListExtractRelation)another).getOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitInListExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitInListExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitInListExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeListParagraph":
                    return obj.getEmployeeListParagraph();
                case "orgUnit":
                    return obj.getOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeListParagraph":
                    obj.setEmployeeListParagraph((EmployeeListParagraph) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeListParagraph":
                        return true;
                case "orgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeListParagraph":
                    return true;
                case "orgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeListParagraph":
                    return EmployeeListParagraph.class;
                case "orgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitInListExtractRelation> _dslPath = new Path<OrgUnitInListExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitInListExtractRelation");
    }
            

    /**
     * @return Параграф списочного приказа по сотруднику. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.OrgUnitInListExtractRelation#getEmployeeListParagraph()
     */
    public static EmployeeListParagraph.Path<EmployeeListParagraph> employeeListParagraph()
    {
        return _dslPath.employeeListParagraph();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.OrgUnitInListExtractRelation#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    public static class Path<E extends OrgUnitInListExtractRelation> extends EntityPath<E>
    {
        private EmployeeListParagraph.Path<EmployeeListParagraph> _employeeListParagraph;
        private OrgUnit.Path<OrgUnit> _orgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Параграф списочного приказа по сотруднику. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.OrgUnitInListExtractRelation#getEmployeeListParagraph()
     */
        public EmployeeListParagraph.Path<EmployeeListParagraph> employeeListParagraph()
        {
            if(_employeeListParagraph == null )
                _employeeListParagraph = new EmployeeListParagraph.Path<EmployeeListParagraph>(L_EMPLOYEE_LIST_PARAGRAPH, this);
            return _employeeListParagraph;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.OrgUnitInListExtractRelation#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

        public Class getEntityClass()
        {
            return OrgUnitInListExtractRelation.class;
        }

        public String getEntityName()
        {
            return "orgUnitInListExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
