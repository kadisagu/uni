/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e10;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.IDeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 14.01.2009
 */
public class EmployeeSurnameChangeExtractPrint implements IPrintFormCreator<EmployeeSurnameChangeExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeSurnameChangeExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        IDeclinationDao declinationDao = PersonManager.instance().declinationDao();
        IdentityCard icard = extract.getEmployee().getPerson().getIdentityCard();
        boolean sexIsMale = icard.getSex().isMale();

        if (!icard.getLastName().equals(extract.getLastName()))
        {
            modifier.put("namePart", "фамилию");
            modifier.put("name_New", extract.getLastName());
            modifier.put("name_N_New", declinationDao.getDeclinationLastName(extract.getLastName(), GrammaCase.NOMINATIVE, sexIsMale));
            modifier.put("name_G_New", declinationDao.getDeclinationLastName(extract.getLastName(), GrammaCase.GENITIVE, sexIsMale));
            modifier.put("name_D_New", declinationDao.getDeclinationLastName(extract.getLastName(), GrammaCase.DATIVE, sexIsMale));
            modifier.put("name_A_New", declinationDao.getDeclinationLastName(extract.getLastName(), GrammaCase.ACCUSATIVE, sexIsMale));
            modifier.put("name_I_New", declinationDao.getDeclinationLastName(extract.getLastName(), GrammaCase.INSTRUMENTAL, sexIsMale));
            modifier.put("name_P_New", declinationDao.getDeclinationLastName(extract.getLastName(), GrammaCase.PREPOSITIONAL, sexIsMale));
        }
        else if (!icard.getFirstName().equals(extract.getFirstName()))
        {
            modifier.put("namePart", "имя");
            modifier.put("name_New", extract.getFirstName());
            modifier.put("name_N_New", declinationDao.getDeclinationFirstName(extract.getFirstName(), GrammaCase.NOMINATIVE, sexIsMale));
            modifier.put("name_G_New", declinationDao.getDeclinationFirstName(extract.getFirstName(), GrammaCase.GENITIVE, sexIsMale));
            modifier.put("name_D_New", declinationDao.getDeclinationFirstName(extract.getFirstName(), GrammaCase.DATIVE, sexIsMale));
            modifier.put("name_A_New", declinationDao.getDeclinationFirstName(extract.getFirstName(), GrammaCase.ACCUSATIVE, sexIsMale));
            modifier.put("name_I_New", declinationDao.getDeclinationFirstName(extract.getFirstName(), GrammaCase.INSTRUMENTAL, sexIsMale));
            modifier.put("name_P_New", declinationDao.getDeclinationFirstName(extract.getFirstName(), GrammaCase.PREPOSITIONAL, sexIsMale));
        }
        else
        {
            modifier.put("namePart", "отчество");
            modifier.put("name_New", extract.getMiddleName());
            modifier.put("name_N_New", declinationDao.getDeclinationMiddleName(extract.getMiddleName(), GrammaCase.NOMINATIVE, sexIsMale));
            modifier.put("name_G_New", declinationDao.getDeclinationMiddleName(extract.getMiddleName(), GrammaCase.GENITIVE, sexIsMale));
            modifier.put("name_D_New", declinationDao.getDeclinationMiddleName(extract.getMiddleName(), GrammaCase.DATIVE, sexIsMale));
            modifier.put("name_A_New", declinationDao.getDeclinationMiddleName(extract.getMiddleName(), GrammaCase.ACCUSATIVE, sexIsMale));
            modifier.put("name_I_New", declinationDao.getDeclinationMiddleName(extract.getMiddleName(), GrammaCase.INSTRUMENTAL, sexIsMale));
            modifier.put("name_P_New", declinationDao.getDeclinationMiddleName(extract.getMiddleName(), GrammaCase.PREPOSITIONAL, sexIsMale));
        }

        StringBuilder fioStr = new StringBuilder(extract.getLastName());
        fioStr.append(" ").append(extract.getFirstName());
        if (null != extract.getMiddleName())
            fioStr.append(" ").append(extract.getMiddleName());

        modifier.put("fioNew", fioStr.toString());

        IdentityCard identityCard = extract.getEntity().getPerson().getIdentityCard();
        identityCard.getSex().isMale();

//        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.ACCUSATIVE, isMaleSex).toUpperCase());
//        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), GrammaCase.ACCUSATIVE, isMaleSex));
//        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
//            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), GrammaCase.ACCUSATIVE, isMaleSex));
//        modifier.put("fio", str.toString());

        if (extract.getLastName() != null && !(extract.getLastName().equals(identityCard.getLastName())))
        {
            modifier.put("lastName", " фамилии ");
            modifier.put("lastNameNew_N", extract.getLastName());
        }
        else
        {
            modifier.put("lastName", "");
            modifier.put("lastNameNew_N", "");
        }

        if (extract.getFirstName() != null && !(extract.getFirstName().equals(identityCard.getFirstName())))
        {
            String s = "";
            if (extract.getLastName() != null && !(extract.getLastName().equals(identityCard.getLastName())))
                s = ",";
            modifier.put("firstName", s + " имени ");
            modifier.put("firstNameNew_N", extract.getFirstName());
        }
        else
        {
            modifier.put("firstName", "");
            modifier.put("firstNameNew_N", "");
        }

        if (extract.getMiddleName() != null && !(extract.getMiddleName().equals(identityCard.getMiddleName())))
        {
            String s = "";
            if ((extract.getFirstName() != null && !(extract.getFirstName().equals(identityCard.getFirstName()))) ||
                    (extract.getLastName() != null && !(extract.getLastName().equals(identityCard.getLastName()))))
                s = ",";
            modifier.put("middleName", s + " отчеству ");
            modifier.put("middleNameNew_N", extract.getMiddleName());
        }
        else
        {
            modifier.put("middleName", "");
            modifier.put("middleNameNew_N", "");
        }


        modifier.modify(document);

        RtfTableModifier visaModifier = CommonExtractPrint.createModularExtractTableModifier(extract);
        visaModifier.modify(document);

        return document;
    }
}