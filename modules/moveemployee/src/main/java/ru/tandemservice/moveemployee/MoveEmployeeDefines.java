/**
 * $Id$
 */
package ru.tandemservice.moveemployee;

/**
 * @author dseleznev
 * Created on: 07.11.2008
 */
public interface MoveEmployeeDefines
{
    // Catalogs
    String CATALOG_EMPLOYEE_EXTRACT_TYPE = "1";

    String EMPLOYEE_EXTRACT_TYPE_MODULAR_ORDER_CODE = "1";
    String MODULAR_EMPLOYEE_ADD_EXTRACT_TYPE_CODE = "1.1";
    String MODULAR_EMPLOYEE_TEMP_ADD_EXTRACT_TYPE_CODE = "1.2";

    String EMPLOYEE_EXTRACT_TYPE_LIST_ORDER_CODE = "2";

    String EMPLOYEE_EXTRACT_TYPE_INDIVIDUAL_ORDER_CODE = "3";
    String INDIVIDUAL_EMPLOYEE_ADD_EXTRACT_TYPE_CODE = "3.1";
    String INDIVIDUAL_EMPLOYEE_TEMP_ADD_EXTRACT_TYPE_CODE = "3.2";

    // Text Codes (for StudentExtractTextRelation and for MovestudentTemplate)
    int EXTRACT_TEXT_CODE = 1;          // код для текста выписки
    int EXTRACT_IN_ORDER_TEXT_CODE = 2; // код для текста выписки для текста приказа

    String PAYMENT_PRINT_FORMAT_SETTINGS_PREFIX = "PaymentPringFormatSettings_";
    String PAYMENT_PRINT_FORMAT_SEPARATOR_SETTING = "separator";

    // extract sorters
    String MANUAL_SORT = "employeeModularOrderParagraphsSorter1";         // "Ручная нумерация"
}
