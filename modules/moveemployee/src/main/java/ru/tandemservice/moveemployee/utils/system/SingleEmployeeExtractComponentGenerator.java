/**
 * $Id$
 */
package ru.tandemservice.moveemployee.utils.system;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.uni.util.system.UniSystemUtils;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public class SingleEmployeeExtractComponentGenerator
{
    private static String _tandemUniPath = UniSystemUtils.getModulesDir().getAbsolutePath();
    private static String _templatePath = _tandemUniPath + "/moveemployee/src/main/java/ru/tandemservice/moveemployee/utils/system/singletemplates";

    public static void main(String[] args) throws Exception
    {
        for (String index : args)
        {
            int extractIndex = Integer.parseInt(index);
            String[] extractData = SingleExtractData.EXTRACT_LIST[extractIndex - 1];
            String entityName = extractData[0];

            createExtractComponent(entityName, extractIndex);
        }
    }

    private static void createExtractComponent(String entityName, int extractIndex) throws Exception
    {
        String extractSimpleClass = StringUtils.capitalize(entityName);

        String componentPath = _tandemUniPath + "/moveemployee/src/main/java/ru/tandemservice/moveemployee/component/singleemplextract/e" + extractIndex;

        UniSystemUtils.initVelocity();

        VelocityContext context = new VelocityContext();
        context.put("entityName", entityName);
        context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        context.put("entityClassName", extractSimpleClass);
        context.put("extractIndex", extractIndex);

        UniSystemUtils.createComponentByTemplate(componentPath + "/AddEdit", _templatePath + "/AddEdit", context, false);

        UniSystemUtils.createComponentByTemplate(componentPath + "/Pub", _templatePath + "/Pub", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Dao.java", _templatePath + "/extractDao.vm", context, false);
        
        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Print.java", _templatePath + "/extractPrint.vm", context, false);
    }
}