/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e0.ListOrderAddEdit;

import ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderAddEdit.AbstractListOrderAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;

/**
 * @author ListExtractComponentGenerator
 * @since 07.05.2009
 */
public class DAO extends AbstractListOrderAddEditDAO<EmployeeListOrder, Model> implements IDAO
{
    @Override
    protected EmployeeListOrder createNewInstance(String formationEntity)
    {
        if ("ParagraphEmployeeListOrder".equals(formationEntity))
            return new ParagraphEmployeeListOrder();
        return new EmployeeListOrder();
    }
}