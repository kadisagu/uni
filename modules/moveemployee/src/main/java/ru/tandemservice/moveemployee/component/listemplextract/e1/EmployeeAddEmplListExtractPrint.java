/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e1;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.ui.formatters.EmployeeCodeFormatter;
import ru.tandemservice.moveemployee.component.listemplextract.CommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.ICommonListOrderPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeAddEmplListExtract;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ListExtractComponentGenerator
 * @since 07.05.2009
 */
public class EmployeeAddEmplListExtractPrint implements IPrintFormCreator<EmployeeListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);

        ICommonListOrderPrint commonListOrderPrint = (ICommonListOrderPrint) ApplicationRuntime.getBean("employeeListOrder_orderPrint");
        commonListOrderPrint.appendVisas(tableModifier, order);

        tableModifier.put("T", getPreparedOrderData(order));
        RtfRowIntercepterBase rowIntercepterBase = getRowIntercepterBase();
        if (rowIntercepterBase != null)
            tableModifier.put("T", rowIntercepterBase);
        tableModifier.modify(document);

        return document;
    }

    protected String[][] getPreparedOrderData(EmployeeListOrder order)
    {
        List<EmployeeAddEmplListExtract> extractList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractList(order);
        if(null == extractList || extractList.isEmpty()) return new String[][]{};

        List<String[]> resultList = new ArrayList<String[]>();

        for(EmployeeAddEmplListExtract extract : extractList)
        {
            String[] line = new String[11];
            Employee employee = extract.getEmployee();
            line[0] = employee.getPerson().getFullFio();
            line[1] = new EmployeeCodeFormatter().format(employee.getEmployeeCode());
            line[2] = null != extract.getOrgUnit().getNominativeCaseTitle() ? extract.getOrgUnit().getNominativeCaseTitle() : extract.getOrgUnit().getFullTitle();
            line[3] = extract.getPostBoundedWithQGandQL().getTitle();
            line[4] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(extract.getSalary()); // TODO: payments
            line[5] = extract.getLabourContractNumber();
            line[6] = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLabourContractDate());
            line[7] = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate());
            line[8] = null == extract.getEndDate() ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate());
            line[9] = 0 != extract.getTrialPeriod() ? String.valueOf(extract.getTrialPeriod()) : "-";
            resultList.add(line);
        }

        return resultList.toArray(new String[extractList.size()][]);
    }

    protected RtfRowIntercepterBase getRowIntercepterBase()
    {
        return null;
    }
}