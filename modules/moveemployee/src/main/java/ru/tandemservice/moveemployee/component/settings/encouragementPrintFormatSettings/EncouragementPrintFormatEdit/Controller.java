/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.encouragementPrintFormatSettings.EncouragementPrintFormatEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * Create by ashaburov
 * Date 29.09.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }
}
