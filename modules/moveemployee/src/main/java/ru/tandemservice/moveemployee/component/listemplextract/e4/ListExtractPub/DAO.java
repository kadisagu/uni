/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e4.ListExtractPub;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract;
import ru.tandemservice.uni.util.NumberConvertingUtil;

/**
 * @author ListExtractComponentGenerator
 * @since 04.10.2011
 */
public class DAO extends AbstractListExtractPubDAO<EmployeeHolidayEmplListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        EmployeeHolidayEmplListExtract extract = model.getExtract();

        if (extract.getPeriodBeginDate() != null)
            model.setPeriodOfSrt("с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPeriodBeginDate()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getPeriodEndDate()));
        else
            model.setPeriodOfSrt("");

        if (extract.getMainHolidayBeginDate() != null && extract.getMainHolidayEndDate() != null && extract.getMainHolidayDuration() != null)
            model.setAnnualHolidayStr("с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getMainHolidayBeginDate()) +
                    " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getMainHolidayEndDate()) + " (" +
                    NumberConvertingUtil.getCalendarDaysWithName(extract.getMainHolidayDuration()) + ")");
        else
            model.setAnnualHolidayStr("");

        if (extract.getSecondHolidayBeginDate() != null && extract.getSecondHolidayEndDate() != null && extract.getSecondHolidayDuration() != null)
            model.setSecondHilidayStr("с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getSecondHolidayBeginDate()) +
                    " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getSecondHolidayEndDate()) + " (" +
                    NumberConvertingUtil.getCalendarDaysWithName(extract.getSecondHolidayDuration()) + ")");
        else
            model.setSecondHilidayStr("");

        model.setHolidayStr("с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()) +
                " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()) + " (" +
                NumberConvertingUtil.getCalendarDaysWithName(extract.getDuration()) + ")");
    }
}
