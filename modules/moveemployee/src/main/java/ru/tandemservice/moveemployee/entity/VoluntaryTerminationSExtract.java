package ru.tandemservice.moveemployee.entity;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.moveemployee.entity.gen.VoluntaryTerminationSExtractGen;

/**
 * Выписка из индивидуального приказа по кадровому составу. Об увольнении
 */
public class VoluntaryTerminationSExtract extends VoluntaryTerminationSExtractGen
{
    public String getrelativeStr()
    {
        StringBuilder relative = new StringBuilder();
        if(null != getRelativeType()) relative.append(getRelativeType().getTitle());
        if(null != getRelativeFio()) relative.append(relative.length() > 0 ? ", " : "").append(getRelativeFio());
        if(null != getIdentityCard()) relative.append(relative.length() > 0 ? ", " : "").append(getIdentityCard());
        return relative.toString();
    }

    public String getHolidayPeriodStr()
    {
        if (null != getHolidayPeriodBegin() && null != getHolidayPeriodEnd())
            return DateFormatter.DEFAULT_DATE_FORMATTER.format(getHolidayPeriodBegin()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getHolidayPeriodEnd());
        else return null;
    }
}