/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e6;

import org.hibernate.Session;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.listemplextract.IOrderParagraphListCustomizer;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class ChangeFinancingSourceEmplListExtractCustomizer implements IOrderParagraphListCustomizer
{
    @Override
    public void customizeParagraphList(DynamicListDataSource dataSource, IBusinessComponent component)
    {
        //TODO описать колонки
    }

    @Override
    public void wrap(DynamicListDataSource dataSource, Session session)
    {
        //Body of implemented method
    }
}