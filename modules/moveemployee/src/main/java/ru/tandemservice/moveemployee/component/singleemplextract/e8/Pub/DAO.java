/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e8.Pub;

import java.util.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract;
import ru.tandemservice.moveemployee.entity.EncouragementToExtractRelation;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 30.09.2011
 */
public class DAO extends SingleEmployeeExtractPubDAO<EmployeeEncouragementSExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        MQBuilder builder = new MQBuilder(EncouragementToExtractRelation.ENTITY_CLASS, "b", new String[]{EncouragementToExtractRelation.L_ENCOURAGEMENT_TYPE});
        builder.add(MQExpression.eq("b", EncouragementToExtractRelation.L_EMPLOYEE_ENCOURAGEMENT_S_EXTRACT, model.getExtract()));

        String result = "";
        List<EncouragementType> encouragementTypeList = builder.<EncouragementType>getResultList(getSession());
        for (EncouragementType encouragementType : encouragementTypeList)
        {
            result += encouragementType.getTitle() + (encouragementTypeList.indexOf(encouragementType) == encouragementTypeList.size() - 1 ? "" : ",") +
                    (encouragementTypeList.indexOf(encouragementType) == encouragementTypeList.size() - 1 ? "" : "<br/>");
        }

        model.setEncouragementTypeStr(result);
    }
}