/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.component.commons.MoveEmployeeColumns;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * @author dseleznev
 *         Created on: 11.11.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);

        model.setSettings(UniBaseUtils.getDataSettings(component, "EmployeeModularOrderPub.filter"));

        model.setAddExtractDisable(getDao().isAddExtractsDisabled(model));
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<IAbstractExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Дата формирования", IAbstractExtract.P_CREATE_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("ФИО сотрудника", ModularEmployeeExtract.EMPLOYEE_FIO_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Тип приказа", new String[]{IAbstractExtract.L_TYPE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("№ пункта приказа", null, "onClickChangeNumber").setTextKey(IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.P_NUMBER).setDisplayHeader(true).setWidth(5).setPermissionKey(model.getSecModel().getPermission("changeParagraphNumber")));
        dataSource.addColumn(new ActionColumn("Вверх", CommonBaseDefine.ICO_UP, "onClickParagraphUp").setPermissionKey(model.getSecModel().getPermission("changeParagraphNumber")));
        dataSource.addColumn(new ActionColumn("Вниз", CommonBaseDefine.ICO_DOWN, "onClickParagraphDown").setPermissionKey(model.getSecModel().getPermission("changeParagraphNumber")));
        dataSource.addColumn(MoveEmployeeColumns.getExtractPrintColumn(this, model.getSecModel().getPermission("printExtract")));
        dataSource.addColumn(new ActionColumn("Исключить", ActionColumn.DELETE, "onClickExtractDelete", "Исключить «{0}» из приказа?", IAbstractExtract.P_TITLE).setPermissionKey(model.getSecModel().getPermission("deleteExtract")).setDisabledProperty(AbstractEmployeeExtract.P_NO_EXCLUDE));

        model.setDataSource(dataSource);
    }

    // Event Listeners

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendToCoordination(getModel(component), null);
        else
            getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
    }

    public void onClickReject(IBusinessComponent component)
    {
        getDao().doReject(getModel(component));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_MODULAR_ORDER_EDIT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickDeleteOrderWithExtracts(IBusinessComponent component)
    {
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(getModel(component).getOrder());
        deactivate(component);
    }

    public void onClickAddExtract(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_MODULAR_ORDER_ADD_EXTRACTS, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public final synchronized void onClickCommit(IBusinessComponent component)
    {
        getDao().doCommit(getModel(component));
    }

    public final synchronized void onClickRollback(IBusinessComponent component)
    {
        getDao().doRollback(getModel(component));
    }

    public void onClickDeleteOrder(IBusinessComponent component)
    {
        MoveDaoFacade.getMoveDao().deleteOrder(getModel(component).getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
        deactivate(component);
    }

    public void onClickPrintOrder(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_MODULAR_ORDER_PRINT, new ParametersMap()
        .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        DataSettingsFacade.saveSettings(getModel(component).getSettings());
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickParagraphUp(IBusinessComponent component)
    {
        getDao().doParagraphUp(component.<Long>getListenerParameter());
    }

    public void onClickParagraphDown(IBusinessComponent component)
    {
        getDao().doParagraphDown(component.<Long>getListenerParameter());
    }

    public void onClickChangeNumber(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderChangeParagraphNumber", new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }

    public void onClickExtractDelete(IBusinessComponent component)
    {
        getDao().deleteExtract(getModel(component), (Long) component.getListenerParameter());
        onRefreshComponent(component);
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.MODULAR_EMPLOYEE_EXTRACT_PRINT, new ParametersMap()
        .add("extractId", component.getListenerParameter())
        ));
    }

    public void onClickSetExecutor(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.STUDENT_ORDER_SET_EXECUTOR, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }
}