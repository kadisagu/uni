/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e7;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.OrgUnitRankComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.listemplextract.CommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.ICommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.IListParagraphPrintFormCreator;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.*;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class PayForHolidayDayEmplListExtractPrint implements IPrintFormCreator<EmployeeListOrder>, IListParagraphPrintFormCreator<PayForHolidayDayEmplListExtract>
{
    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph paragraph, PayForHolidayDayEmplListExtract firstExtract)
    {
        RtfInjectModifier modifier = CommonListOrderPrint.createListOrderInjectModifier((EmployeeListOrder) paragraph.getOrder());
        CommonExtractPrintUtil.injectCommonExtractData(modifier, firstExtract);

        modifier.put("legalHolidayDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getHolidayDate()));
        modifier.put("listBasics", firstExtract.getBasicListStr());

        return modifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(final IAbstractParagraph paragraph, PayForHolidayDayEmplListExtract firstExtract)
    {
        final RtfTableModifier modifier = new RtfTableModifier();

        final List<Integer> indexList = new ArrayList<>();

        modifier.put("T", getPreparedOrderData(paragraph, indexList));

        modifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                int cnt = rowIndex + 1;
                if (indexList.contains(cnt))
                {
                    cell.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.QC));
                    RtfString rtfString = new RtfString();
                    rtfString.boldBegin();
                    rtfString.append(value);
                    rtfString.boldEnd();
                    return rtfString.toList();
                }
                else
                    return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int cnt = 0;
                for (RtfRow row : newRowList)
                {
                    if (indexList.contains(cnt))
                        RtfUtil.unitAllCell(row, 0);

                    cnt++;
                }
            }
        });

        return modifier;
    }

    @SuppressWarnings("unchecked")
    public String[][] getPreparedOrderData(IAbstractParagraph paragraph, List<Integer> indexList)
    {
        List<PayForHolidayDayEmplListExtract> extractList = paragraph.getExtractList();
        if(null == extractList || extractList.isEmpty()) return new String[][]{};

        Map<OrgUnit, List<PayForHolidayDayEmplListExtract>> orgUnitExtractMap = new HashMap<>();
        List<OrgUnit> orgUnitList = new ArrayList<>();
        for (PayForHolidayDayEmplListExtract extract : extractList)
        {
            if (!orgUnitList.contains(extract.getEntity().getOrgUnit()))
                orgUnitList.add(extract.getEntity().getOrgUnit());

            List<PayForHolidayDayEmplListExtract> extList = orgUnitExtractMap.get(extract.getEntity().getOrgUnit());
            if (extList == null)
                extList = new ArrayList<>();
            extList.add(extract);
            orgUnitExtractMap.put(extract.getEntity().getOrgUnit(), extList);
        }

        Collections.sort(orgUnitList, OrgUnitRankComparator.INSTANCE);

        List<String[]> resultList = new ArrayList<>();

        int i = 1;
        for (OrgUnit orgUnit : orgUnitList)
        {
            List<PayForHolidayDayEmplListExtract> innerExtractList = new ArrayList<>(orgUnitExtractMap.get(orgUnit));

            innerExtractList.sort(ListEmployeeExtract.FULL_FIO_AND_ID_COMPARATOR);

            String[] orgUnitLine = new String[1];

            orgUnitLine[0] = CommonExtractPrintUtil.getOrgUnitPrintingTitle(orgUnit, CommonExtractPrintUtil.NOMINATIVE_CASE, true, false);

            resultList.add(orgUnitLine);
            indexList.add(i);
            i++;

            int nmbr = 1;
            for (PayForHolidayDayEmplListExtract extract : innerExtractList)
            {
                String[] line = new String[7];

                line[0] = String.valueOf(nmbr);
                line[1] = CommonExtractUtil.getModifiedFio(extract.getEntity().getEmployee().getPerson(), GrammaCase.DATIVE);
                line[2] = CommonExtractPrintUtil.getPostPrintingTitle(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.DATIVE_CASE, true);
                line[3] = extract.getWorkTime();
                line[4] = String.valueOf(extract.getTimeAmount());
                line[5] = String.valueOf(extract.getNightTimeAmount());
                line[6] = extract.getWorkType();

                resultList.add(line);
                nmbr++;
                i++;
            }
        }

        return resultList.toArray(new String[][]{});
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);

        ICommonListOrderPrint commonListOrderPrint = (ICommonListOrderPrint) ApplicationRuntime.getBean("employeeListOrder_orderPrint");
        commonListOrderPrint.appendVisas(tableModifier, order);

        tableModifier.modify(document);

        return document;
    }
}
