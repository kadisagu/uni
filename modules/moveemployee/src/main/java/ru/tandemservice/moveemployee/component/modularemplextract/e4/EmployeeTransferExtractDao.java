/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e4;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeeTransferExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class EmployeeTransferExtractDao extends UniBaseDao implements IExtractComponentDao<EmployeeTransferExtract>
{
    @Override
    public void doCommit(EmployeeTransferExtract extract, Map parameters)
    {
        extract.setOldEmployeePostStatus(extract.getEmployeePost().getPostStatus());
        extract.getEmployeePost().setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_TRANSFERED));
        extract.getEmployeePost().setDismissalDate(extract.getBeginDate());
        update(extract.getEmployeePost());

        EmployeePost post = CommonExtractCommitUtil.createOrAssignEmployeePost(getSession(), extract);

        List<EmployeePostStaffRateItem> staffRateItemList = CommonExtractCommitUtil.createEmployeePostStaffRateItems(extract, post);

        EmployeeLabourContract contract = CommonExtractCommitUtil.createOrAssignLabourContract(extract, post);

        EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), post, staffRateItemList);

        EmploymentHistoryItemInner empHistItemOld = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEmployeePost(), UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEmployeePost()));

        List<StaffListAllocationItem> allocItemList = createStaffListAllocationItem(extract, post);

        validate(extract, post, staffRateItemList);

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        empHistItemOld.setCurrent(false);
        empHistItemOld.setDismissalDate(extract.getBeginDate());

        getSession().saveOrUpdate(post);

        MoveEmployeeDaoFacade.getMoveEmployeeDao().updateStaffListAllocItemChanges(extract, post, allocItemList);

        for (EmployeePostStaffRateItem oldStaffRateItem : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(post))
            delete(oldStaffRateItem);
        getSession().flush();
        for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
            save(staffRateItem);

        getSession().saveOrUpdate(contract);

        getSession().saveOrUpdate(empHistItem);

        getSession().saveOrUpdate(empHistItemOld);
        
        if(null != extract.getCollateralAgreementNumber() && null != extract.getTransferDate())
        {
            ContractCollateralAgreement agreement = new ContractCollateralAgreement();
            
            if(null != contract.getId())
            {
                List<ContractCollateralAgreement> agrmntsList = UniempDaoFacade.getUniempDAO().getContractCollateralAgreementsList(contract);
                for(ContractCollateralAgreement agrmnt : agrmntsList)
                {
                    if(agrmnt.getNumber().equals(extract.getCollateralAgreementNumber()) && agrmnt.getDate().equals(extract.getTransferDate()))
                    {
                        agreement = agrmnt;
                        break;
                    }
                }
            }
            
            if (null == agreement.getId())
            {
                agreement.setContract(contract);
                agreement.setDate(extract.getTransferDate());
                agreement.setNumber(extract.getCollateralAgreementNumber());
                agreement.setDescription("Доп соглашение к трудовому договору № " + contract.getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getBeginDate()) + "г.");
            }
            getSession().saveOrUpdate(agreement); // TODO: поиск уже созданного соглашения
        }

        // удаляем выплаты у создаваемого сотрудника, если он уже создан, в случае если приказ откатывался
        if (post.getId() != null)
        {
            for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
                bonus.setEntity(null);

            for (EmployeePayment payment : UniempDaoFacade.getUniempDAO().getEmployeePostPaymentList(post))
                delete(payment);

            getSession().flush();
        }

        Double salary = 0d;
        if (extract.getRaisingCoefficient() != null)
            salary = extract.getRaisingCoefficient().getRecommendedSalary();
        else if (extract.getEntity() != null && extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary() != null)
            salary = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary();
        else if (extract.getPostBoundedWithQGandQL().getSalary() != null)
            salary = extract.getPostBoundedWithQGandQL().getSalary();
        List<EmployeePayment> paymentList = CommonExtractCommitUtil.createOrAssignEmployeePaymentsList(getSession(), extract, post, salary, staffRateItemList);

        CommonExtractCommitUtil.validateEmployeePayment(extract.getEntity(), paymentList, getSession());

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;

        for (EmployeePayment payment : paymentList)
            getSession().saveOrUpdate(payment);

        if (null == extract.getEntity())
        {
            extract.setEntity(post);
            update(extract);
        }
    }

    @Override
    public void doRollback(EmployeeTransferExtract extract, Map parameters)
    {
        extract.getEmployeePost().setPostStatus(extract.getOldEmployeePostStatus());
        extract.getEmployeePost().setDismissalDate(null);
        update(extract.getEmployeePost());
        
        extract.getEntity().setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_POSSIBLE));
        update(extract.getEntity());

        EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEntity(), UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity()));
        if (null != empHistItem.getId()) delete(empHistItem);
        
        EmploymentHistoryItemInner empHistItemOld = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEmployeePost(), UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEmployeePost()));
        if(null != empHistItemOld.getId())
        {
            empHistItemOld.setDismissalDate(null);
            empHistItemOld.setCurrent(true);
            update(empHistItemOld);
        }

        MoveEmployeeDaoFacade.getMoveEmployeeDao().rollbackStaffListAllocItemChanges(extract);
    }

    /*выносим в отдельные методы для возможности переопределения действий в проектом слое*/

    protected List<StaffListAllocationItem> createStaffListAllocationItem(EmployeeTransferExtract extract, EmployeePost post)
    {
        return CommonExtractCommitUtil.createStaffListAllocationItem(extract, post);
    }

    protected void validate(EmployeeTransferExtract extract, EmployeePost post, List<EmployeePostStaffRateItem> staffRateItemList)
    {
        CommonExtractCommitUtil.validateStaffRates(extract, post, staffRateItemList);

        CommonExtractCommitUtil.validateAllocationItem(extract, post.getOrgUnit(), post.getPostRelation().getPostBoundedWithQGandQL());
    }
}