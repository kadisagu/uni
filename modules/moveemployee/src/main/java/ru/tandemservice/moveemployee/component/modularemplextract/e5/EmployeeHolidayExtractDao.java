/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.MainRow;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.Model;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.Row;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayToEmpHldyExtractRelation;
import ru.tandemservice.moveemployee.entity.HolidayInEmpHldyExtractRelation;
import ru.tandemservice.moveemployee.entity.VacationScheduleItemToEmployeeExtractRelation;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 21.11.2008
 */
public class EmployeeHolidayExtractDao extends UniBaseDao implements IExtractComponentDao<EmployeeHolidayExtract>
{
    @Override
    public void doCommit(EmployeeHolidayExtract extract, Map parameters)
    {
        Set<Long> isItemUsed = new HashSet<>();
        String basic = null;
        Date date = null;
        if (extract.getVacationScheduleItem() != null)
        {
            basic = extract.getVacationScheduleItem().getPostponeBasic();
            date = extract.getVacationScheduleItem().getPostponeDate();
        }

        Map<Row, Date> beginPeriodMap = new HashMap<>();
        Map<Row, Date> endPeriodMap = new HashMap<>();
        Map<Row, Date> startHolidayMap = new HashMap<>();
        Map<Row, Date> finishHolidayMap = new HashMap<>();
        Map<Row, Integer> durationHolidayMap = new HashMap<>();

        List<HolidayInEmpHldyExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getHolidayInEmpHldyExtractRelation(extract);

        //восстанавливаем список отпусков
        List<MainRow> mainRowList = new ArrayList<>();
        for (HolidayInEmpHldyExtractRelation relation : relationList)
        {
            MainRow mainRow = new MainRow(relation.getMainRowId(), null, null);
            if (!mainRowList.contains(mainRow))
            {
                List<Row> rowList = new ArrayList<>();
                Row row = new Row(relation.getRowId(), mainRow, relation.getRowType());

                beginPeriodMap.put(row, relation.getBeginPeriod());
                endPeriodMap.put(row, relation.getEndPeriod());
                startHolidayMap.put(row, relation.getStartHoliday());
                finishHolidayMap.put(row, relation.getFinishHoliday());
                durationHolidayMap.put(row, relation.getDurationHoliday());

                rowList.add(row);
                mainRow.setRowList(rowList);
                mainRow.setType(relation.getRowType());
                mainRowList.add(mainRow);
            }
            else
            {
                MainRow mainRow1 = mainRowList.get(mainRowList.indexOf(mainRow));
                mainRow1.setType(relation.getRowType());
                Row row = new Row(relation.getRowId(), mainRow1, relation.getRowType());

                beginPeriodMap.put(row, relation.getBeginPeriod());
                endPeriodMap.put(row, relation.getEndPeriod());
                startHolidayMap.put(row, relation.getStartHoliday());
                finishHolidayMap.put(row, relation.getFinishHoliday());
                durationHolidayMap.put(row, relation.getDurationHoliday());

                mainRow1.getRowList().add(row);
            }
        }

        //создаем отпуска
        //если указан График отпусков, то для каждой новой строки отпуска создаем соответствующюю строку в Графике отпусков
        for (MainRow mainRow : mainRowList)
        {
            if (mainRow.getType().equals(Model.FULL_HOLIDAY))
            {
                EmployeeHoliday holiday = new EmployeeHoliday();
                holiday.setEmployeePost(extract.getEntity());
                holiday.setHolidayType(extract.getHolidayType());
                holiday.setTitle(extract.getHolidayType().getTitle());
                holiday.setBeginDate(beginPeriodMap.get(mainRow.getRowList().get(0)));
                holiday.setEndDate(endPeriodMap.get(mainRow.getRowList().get(0)));
                holiday.setStartDate(startHolidayMap.get(mainRow.getRowList().get(0)));
                holiday.setFinishDate(finishHolidayMap.get(mainRow.getRowList().get(0)));
                holiday.setDuration(durationHolidayMap.get(mainRow.getRowList().get(0)));
                holiday.setHolidayExtract(extract);
                holiday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                holiday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());
                save(holiday);

                EmployeeHolidayToEmpHldyExtractRelation relation = new EmployeeHolidayToEmpHldyExtractRelation();
                relation.setEmployeeHoliday(holiday);
                relation.setEmployeeHolidayExtract(extract);
                save(relation);

                //если указан График отпусков, то создаем соответствующюю отпуску строку в Графике отпусков
                if (extract.getVacationScheduleItem() != null)
                {
                    StringBuilder basicBuilder = new StringBuilder().
                            append("Приказ №").
                            append(extract.getParagraph().getOrder().getNumber()).
                            append(" от ").
                            append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));

                    VacationScheduleItem item = extract.getVacationScheduleItem();

                    Date postponeDate;
                    String postponeBasic;
                    if (item.getPlanDate() != null && !item.getPlanDate().equals(startHolidayMap.get(mainRow.getRowList().get(0))))
                    {
                        postponeDate = date != null ? date : extract.getParagraph().getOrder().getCommitDate();
                        postponeBasic = basic != null ? basic + "; " + basicBuilder.toString() : basicBuilder.toString();
                    }
                    else
                    {
                        postponeDate = date;
                        postponeBasic = basic;
                    }

                    VacationScheduleItem scheduleItem = createVacationScheduleItem(
                            isItemUsed,
                            extract,
                            extract.getVacationSchedule(),
                            extract.getEntity(),
                            durationHolidayMap.get(mainRow.getRowList().get(0)),
                            item.getPlanDate(),
                            startHolidayMap.get(mainRow.getRowList().get(0)),
                            postponeDate,
                            postponeBasic,
                            item.getComment());

                    if (scheduleItem.getId() == null)
                    {
                        saveOrUpdate(scheduleItem);

                        VacationScheduleItemToEmployeeExtractRelation vsItemRelation = new VacationScheduleItemToEmployeeExtractRelation();
                        vsItemRelation.setAbstractEmployeeExtract(extract);
                        vsItemRelation.setVacationScheduleItem(scheduleItem);

                        save(vsItemRelation);
                    }
                    else
                        saveOrUpdate(scheduleItem);



                }

            }
            else if (mainRow.getType().equals(Model.PERIODS_OF_HOLIDAY))
            {
                Date start = null;
                Date finish = null;
                for (Row row : mainRow.getRowList())
                {
                    if (startHolidayMap.get(row) != null)
                        start = startHolidayMap.get(row);
                    if (finishHolidayMap.get(row) != null)
                        finish = finishHolidayMap.get(row);
                }

                int i = 0;
                for (Row row : mainRow.getRowList())
                {
                    EmployeeHoliday holiday = new EmployeeHoliday();
                    holiday.setEmployeePost(extract.getEntity());
                    holiday.setHolidayType(extract.getHolidayType());
                    holiday.setTitle(extract.getHolidayType().getTitle());
                    holiday.setBeginDate(beginPeriodMap.get(row));
                    holiday.setEndDate(endPeriodMap.get(row));
                    holiday.setStartDate(start);
                    GregorianCalendar finishDate = (GregorianCalendar)GregorianCalendar.getInstance();
                    finishDate.setTime(getFinishDate(start, durationHolidayMap.get(row), extract.getEntity().getWorkWeekDuration()));
                    if (i == mainRow.getRowList().size() - 1)
                        holiday.setFinishDate(finish);
                    else
                        holiday.setFinishDate(finishDate.getTime());
                    holiday.setDuration(durationHolidayMap.get(row));
                    holiday.setHolidayExtract(extract);
                    holiday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                    holiday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

                    save(holiday);

                    EmployeeHolidayToEmpHldyExtractRelation relation = new EmployeeHolidayToEmpHldyExtractRelation();
                    relation.setEmployeeHoliday(holiday);
                    relation.setEmployeeHolidayExtract(extract);
                    save(relation);

                    //если указан График отпусков, то создаем соответствующюю отпуску строку в Графике отпусков
                    if (extract.getVacationScheduleItem() != null)
                    {
                        StringBuilder basicBuilder = new StringBuilder().
                                append("Приказ №").
                                append(extract.getParagraph().getOrder().getNumber()).
                                append(" от ").
                                append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));

                        VacationScheduleItem item = extract.getVacationScheduleItem();

                        Date postponeDate;
                        String postponeBasic;
                        if (item.getPlanDate() != null && !item.getPlanDate().equals(startHolidayMap.get(mainRow.getRowList().get(0))))
                        {
                            postponeDate = date != null ? date : extract.getParagraph().getOrder().getCommitDate();
                            postponeBasic = basic != null ? basic + "; " + basicBuilder.toString() : basicBuilder.toString();
                        }
                        else
                        {
                            postponeDate = date;
                            postponeBasic = basic;
                        }

                        VacationScheduleItem scheduleItem = createVacationScheduleItem(
                                isItemUsed,
                                extract,
                                extract.getVacationSchedule(),
                                extract.getEntity(),
                                durationHolidayMap.get(row),
                                item.getPlanDate(),
                                start,
                                postponeDate,
                                postponeBasic,
                                item.getComment());

                        if (scheduleItem.getId() == null)
                        {
                            saveOrUpdate(scheduleItem);

                            VacationScheduleItemToEmployeeExtractRelation vsItemRelation = new VacationScheduleItemToEmployeeExtractRelation();
                            vsItemRelation.setAbstractEmployeeExtract(extract);
                            vsItemRelation.setVacationScheduleItem(scheduleItem);

                            save(vsItemRelation);
                        }
                        else
                            saveOrUpdate(scheduleItem);



                    }

                    i++;
                    finishDate.add(Calendar.DATE, 1);
                    start = finishDate.getTime();
                }
            }
            else if (mainRow.getType().equals(Model.HOLIDAYS_OF_PERIOD))
            {
                Date begin = null;
                Date end = null;
                for (Row row : mainRow.getRowList())
                {
                    if (beginPeriodMap.get(row) != null)
                        begin = beginPeriodMap.get(row);
                    if (endPeriodMap.get(row) != null)
                        end = endPeriodMap.get(row);
                }

                for (Row row : mainRow.getRowList())
                {
                    EmployeeHoliday holiday = new EmployeeHoliday();
                    holiday.setEmployeePost(extract.getEntity());
                    holiday.setHolidayType(extract.getHolidayType());
                    holiday.setTitle(extract.getHolidayType().getTitle());
                    holiday.setBeginDate(begin);
                    holiday.setEndDate(end);
                    holiday.setStartDate(startHolidayMap.get(row));
                    holiday.setFinishDate(finishHolidayMap.get(row));
                    holiday.setDuration(durationHolidayMap.get(row));
                    holiday.setHolidayExtract(extract);
                    holiday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                    holiday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());
                    save(holiday);

                    EmployeeHolidayToEmpHldyExtractRelation relation = new EmployeeHolidayToEmpHldyExtractRelation();
                    relation.setEmployeeHoliday(holiday);
                    relation.setEmployeeHolidayExtract(extract);
                    save(relation);

                    //если указан График отпусков, то создаем соответствующюю отпуску строку в Графике отпусков
                    if (extract.getVacationScheduleItem() != null)
                    {
                        StringBuilder basicBuilder = new StringBuilder().
                                append("Приказ №").
                                append(extract.getParagraph().getOrder().getNumber()).
                                append(" от ").
                                append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));

                        VacationScheduleItem item = extract.getVacationScheduleItem();

                        Date postponeDate;
                        String postponeBasic;
                        if (item.getPlanDate() != null && !item.getPlanDate().equals(startHolidayMap.get(mainRow.getRowList().get(0))))
                        {
                            postponeDate = date != null ? date : extract.getParagraph().getOrder().getCommitDate();
                            postponeBasic = basic != null ? basic + "; " + basicBuilder.toString() : basicBuilder.toString();
                        }
                        else
                        {
                            postponeDate = date;
                            postponeBasic = basic;
                        }

                        VacationScheduleItem scheduleItem = createVacationScheduleItem(
                                isItemUsed,
                                extract,
                                extract.getVacationSchedule(),
                                extract.getEntity(),
                                durationHolidayMap.get(row),
                                item.getPlanDate(),
                                startHolidayMap.get(row),
                                postponeDate,
                                postponeBasic,
                                item.getComment());

                        if (scheduleItem.getId() == null)
                        {
                            saveOrUpdate(scheduleItem);

                            VacationScheduleItemToEmployeeExtractRelation vsItemRelation = new VacationScheduleItemToEmployeeExtractRelation();
                            vsItemRelation.setAbstractEmployeeExtract(extract);
                            vsItemRelation.setVacationScheduleItem(scheduleItem);

                            save(vsItemRelation);
                        }
                        else
                            saveOrUpdate(scheduleItem);
                    }
                }
            }
        }

        //делаем пометку в выписке, что мы "удалили" старую строку в Графике отпусков
        if (extract.getVacationScheduleItem() != null && !isItemUsed.isEmpty())
            extract.setDeleteVacationScheduleItem(true);
        else
            extract.setDeleteVacationScheduleItem(false);

        extendCommit(extract, mainRowList, beginPeriodMap, endPeriodMap, startHolidayMap, finishHolidayMap, durationHolidayMap);

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        extract.setOldEmployeePostStatus(extract.getEntity().getPostStatus());
        extract.getEntity().setPostStatus(extract.getEmployeePostStatus());

        update(extract);
        update(extract.getEntity());

    }

    @Override
    public void doRollback(EmployeeHolidayExtract extract, Map parameters)
    {
        extract.getEntity().setPostStatus(extract.getOldEmployeePostStatus());
        update(extract.getEntity());

        MQBuilder builder = new MQBuilder(EmployeeHolidayToEmpHldyExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", EmployeeHolidayToEmpHldyExtractRelation.L_EMPLOYEE_HOLIDAY_EXTRACT, extract));

        for (EmployeeHolidayToEmpHldyExtractRelation relation : builder.<EmployeeHolidayToEmpHldyExtractRelation>getResultList(getSession()))
        {
            EmployeeHoliday holiday = relation.getEmployeeHoliday();
            delete(relation);
            delete(holiday);
        }

        DQLSelectBuilder vsItemBuilder = new DQLSelectBuilder().fromEntity(VacationScheduleItemToEmployeeExtractRelation.class, "vs").column("vs");
        vsItemBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItemToEmployeeExtractRelation.abstractEmployeeExtract().fromAlias("vs")), extract));

        for (VacationScheduleItemToEmployeeExtractRelation relation : vsItemBuilder.createStatement(getSession()).<VacationScheduleItemToEmployeeExtractRelation>list())
        {
            VacationScheduleItem item = relation.getVacationScheduleItem();
            delete(relation);
            delete(item);
        }

        if (extract.getDeleteVacationScheduleItem() != null && extract.getDeleteVacationScheduleItem())
        {
            VacationScheduleItem item = extract.getVacationScheduleItem();

            item.setDaysAmount(extract.getOldScheduleItemDaysAmount());
            item.setPlanDate(extract.getOldScheduleItemPlanDate());
            item.setFactDate(extract.getOldScheduleItemFactDate());
            item.setPostponeDate(extract.getOldScheduleItemPostponeDate());
            item.setPostponeBasic(extract.getOldScheduleItemPostponeBasic());
            item.setComment(extract.getOldScheduleItemComment());

            saveOrUpdate(item);
        }

        update(extract);
    }

    protected Date getFinishDate(Date startHoliday, Integer durationHoliday, EmployeeWorkWeekDuration workWeekDuration)
    {
        if (startHoliday == null || durationHoliday == null)
            throw new NullPointerException();

        GregorianCalendar date = (GregorianCalendar)GregorianCalendar.getInstance();
        int counter = 0;
        int duration = durationHoliday;

        date.setTime(startHoliday);

        if (duration > 0)
        {
            do
            {
//                if (!UniempDaoFacade.getUniempDAO().isIndustrialCalendarHolidayDay(workWeekDuration, date.getTime()))
                    counter++;

                if (counter != duration)
                    date.add(Calendar.DATE, 1);
            }
            while (counter < duration);
        }

        return  date.getTime();
    }

    public VacationScheduleItem createVacationScheduleItem(Set<Long> isItemUsed, EmployeeHolidayExtract extract, VacationSchedule vacationSchedule, EmployeePost employeePost, int daysAmount, Date planDate, Date factDate, Date postponeDate, String postponeBasic, String comment)
    {
        VacationScheduleItem item;
        if (!isItemUsed.contains(extract.getVacationScheduleItem().getId()))
        {
            item = extract.getVacationScheduleItem();

            extract.setOldScheduleItemDaysAmount(item.getDaysAmount());
            extract.setOldScheduleItemPlanDate(item.getPlanDate());
            extract.setOldScheduleItemFactDate(item.getFactDate());
            extract.setOldScheduleItemPostponeDate(item.getPostponeDate());
            extract.setOldScheduleItemPostponeBasic(item.getPostponeBasic());
            extract.setOldScheduleItemComment(item.getComment());

            isItemUsed.add(item.getId());
        }
        else
            item = new VacationScheduleItem();

        item.setVacationSchedule(vacationSchedule);
        item.setEmployeePost(employeePost);
        item.setDaysAmount(daysAmount);
        item.setPlanDate(planDate);
        item.setFactDate(factDate);
        item.setPostponeDate(postponeDate);
        item.setPostponeBasic(postponeBasic);
        item.setComment(comment);

        return item;
    }

    protected void extendCommit(EmployeeHolidayExtract extract, List<MainRow> mainRowList, Map<Row, Date> beginPeriodMap, Map<Row, Date> endPeriodMap, Map<Row, Date> startHolidayMap, Map<Row, Date> finishHolidayMap, Map<Row, Integer> durationHolidayMap)
    {

    }
}