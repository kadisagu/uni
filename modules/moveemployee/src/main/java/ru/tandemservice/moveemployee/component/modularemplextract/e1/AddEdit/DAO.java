/**
* $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e1.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeAddExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 14.11.2008
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<EmployeeAddExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected EmployeeAddExtract createNewInstance()
    {
        return new EmployeeAddExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        CommonEmployeeExtractUtil.createPostBoundedModel(model);
        model.setPostRelationListModel(CommonEmployeeExtractUtil.getPostRelationListModel(model.getExtract()));
//        CommonEmployeeExtractUtil.createExtendedPaymentsBonusModel(model);

        if (model.isEditForm())
        {
            List<FinancingSourceDetails> financingSourceDetailsList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(model.getExtract());
            model.setStaffRateItemList(financingSourceDetailsList);
            CommonEmployeeExtractUtil.prepareColumnsStaffRateSearchList(financingSourceDetailsList, model.getStaffRateDataSource());

            //поднимаем релейшены долей ставок и выбранных ставок ШР
            model.setDetailsToAllocItemList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(financingSourceDetailsList));

            //определяем какие источники финансирования показывать
            CommonEmployeeExtractUtil.fillFinSrcModel(model, model.getExtract().isFreelance(), model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL());

//            CommonEmployeeExtractUtil.onRefreshPaymentCases(model);

            //подготавливаем блок с выплатами
            preparePaymentBlock(model);
        }

        model.setFinancingSourceItemsMultiListModel(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "e");
                builder.add(MQExpression.like("e", FinancingSourceItem.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("e", FinancingSourceItem.title().s());
                return builder;
            }
        });

        if (model.isAddForm())
        {
            model.getExtract().setPostType(getCatalogItem(PostType.class, UniDefines.POST_TYPE_MAIN_JOB));
            IdentityCard identityCard = null != model.getEmployeePost().getId() ? model.getEmployeePost().getPerson().getIdentityCard() : model.getEmployee().getPerson().getIdentityCard();
            model.getExtract().setEmployeeFioModified1(PersonManager.instance().declinationDao().getCalculatedFIODeclination(identityCard, InflectorVariantCodes.RU_INSTRUMENTAL));
        }
        else
        {
            MQBuilder builder = new MQBuilder(FinancingSourceDetails.ENTITY_CLASS, "fsd", new String[] {FinancingSourceDetails.financingSourceItem().s()});
            builder.add(MQExpression.eq("fsd", FinancingSourceDetails.extract().s(), model.getExtract()));
            model.setSelectedFinSrcItemsList(builder.<FinancingSourceItem>getResultList(getSession()));
        }

        model.setFinancingSourceItemModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                for (IEntity item : findValues("").getObjects())
                    if (item.getId().equals(primaryKey))
                        return item;

                return null;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<FinancingSourceItem> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                FinancingSource financingSource = finSrcMap.get(id);
                if (model.isThereAnyActiveStaffList())
                {
                    return new ListResult<>(UniempDaoFacade.getStaffListDAO().getFinancingSourcesItemStaffListItems(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), financingSource, filter));
                }
                else
                {
	                List<FinancingSourceItem> list = new ArrayList<>();
	                if (financingSource != null) list = UniempDaoFacade.getUniempDAO().getFinSrcItm(financingSource.getId(), filter);
	                return new ListResult<>(list);
                }
            }
        });

        model.setEmployeeHRModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> resultList = new ArrayList<>();

                for (IdentifiableWrapper wrapper : findValues("").getObjects())
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);

                return resultList;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
                final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();
                OrgUnit orgUnit = model.getExtract().getOrgUnit();
                PostBoundedWithQGandQL post = model.getExtract().getPostBoundedWithQGandQL();

                if (finSrcMap.get(id) == null || orgUnit == null || post == null)
                    return ListResult.getEmpty();

                String NEW_STAFF_RATE_STR = "<Новая ставка> - ";

                List<IdentifiableWrapper> resultList= new ArrayList<>();

                List<StaffListAllocationItem> staffListAllocList = UniempDaoFacade.getStaffListDAO().getOccupiedStaffListAllocationItem(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id));

                if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id)))
                {
                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id));
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
                    for (StaffListAllocationItem item : staffListAllocList)
                        if (!item.getEmployeePost().getPostStatus().isActive())
                            diff -= item.getStaffRate();
                    if (diff > 0 && NEW_STAFF_RATE_STR.contains(filter))
                        resultList.add(new IdentifiableWrapper(0L, NEW_STAFF_RATE_STR + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                for (StaffListAllocationItem item : staffListAllocList)
                {
                    if (item.getEmployeePost().getPerson().getFullFio().contains(filter))
                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                return new ListResult<>(resultList);
            }
        });

        model.setPaymentModel(CommonEmployeeExtractUtil.getPaymentModelForPaymentBlock());
        model.setFinSrcPaymentModel(CommonEmployeeExtractUtil.getFinSrcModelForPaymentBlock());
        model.setFinSrcItemPaymentModel(CommonEmployeeExtractUtil.getFinSrcItemModelForPaymentBlock(model.getPaymentDataSource()));
    }

    protected void preparePaymentBlock(Model model)
    {
        DQLSelectBuilder payBuilder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "b").column("b");
        payBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeBonus.extract().fromAlias("b")), model.getExtract()));

        model.setPaymentList(payBuilder.createStatement(getSession()).<EmployeeBonus>list());

        List<StaffListPostPayment> paymentList = CommonEmployeeExtractUtil.checkEnabledAddStaffListPaymentButton(model.getExtract().getPostBoundedWithQGandQL(), model.getExtract().getOrgUnit(), model.getPaymentList(), model.getStaffRateItemList());

        if (!paymentList.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);
    }

    @Override
    public void preparePaymentDataSource(Model model)
    {
        CommonEmployeeExtractUtil.preparePaymentDataSourceForPaymentBlock(model.getPaymentList(), model.getPaymentDataSource());
    }

//    @Override
//    public void prepareListDataSource(Model model)
//    {
//        CommonEmployeeExtractUtil.fillPaymentsBonusDataSource(model);
//    }

    @Override
    public void update(Model model)
    {
        super.update(model);

//        CommonEmployeeExtractUtil.updateBonuses(model, getSession());

        CommonEmployeeExtractUtil.updateStaffRateList(model, model.isEditForm(), getSession());

        //PAYMENTS
        //если редактируем приказ, то удаляем созданные ранее выплаты в выписке
        if (model.isEditForm())
        {
            for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getSavedPaymentsFromPaymentBlockDQLBuilder(model.getExtract()).createStatement(getSession()).<EmployeeBonus>list())
                delete(bonus);
        }

        //сохраняем выплаты
        for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getPaymentsForSaveFromPaymentBlock(model.getPaymentList()))
            save(bonus);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateItemList());

        CommonEmployeeExtractUtil.validateStaffRate(model.getStaffRateDataSource(), model.getStaffRateItemList(), model.getExtract().getPostType().getCode(), errors);

        validateFinSrcDetToAllocItem(model, errors);

        CommonEmployeeExtractUtil.validateForPaymentBlock(model.getPaymentList(), errors);

        // TODO Auto-generated method stub
        /*ErrorCollector errCollector = component.getUserContext().getErrorCollector();
        EmployeePostStaffRatesRel rel = new EmployeePostStaffRatesRel();
        rel.setBudgetStaffRate(model.getExtract().getBudgetStaffRate());
        rel.setOffBudgetStaffRate(model.getExtract().getOffBudgetStaffRate());
        model.getEmployeePost().setPostType(model.getExtract().getPostType());*/
        // TODO: take old staffRate into account
        //UniempDaoFacade.getUniempDAO().validateStaffRates(errCollector, model.getEmployeePost().getEmployee(), model.getEmployeePost(), rel, 0d, 0d, true);
    }

    //выносим проверку в метод для переопределения в проекте
    protected void validateFinSrcDetToAllocItem(Model model, ErrorCollector errors)
    {
        CommonEmployeeExtractUtil.validateFinSrcDetToAllocItem(model, model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), errors);
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getStaffRateItemList());

        if (model.isNeedUpdateDataSource())
        {
            CommonEmployeeExtractUtil.prepareColumnsStaffRateSearchList(model.getStaffRateItemList(), model.getStaffRateDataSource());
            model.setNeedUpdateDataSource(false);
        }
    }
}