/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractPub;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
public class AbstractListExtractPubDAO<T extends ListEmployeeExtract, Model extends AbstractListExtractPubModel<T>> extends UniDao<Model> implements IAbstractListExtractPubDAO<T, Model>
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(Model model)
    {
        model.setExtract((T)getNotNull(model.getExtractId()));
        model.setSecModel(new CommonPostfixPermissionModel("listEmployeeExtract"));
        if (model.getExtract().getParagraph().getOrder().getNumber() == null)
        {
            model.setNumberEmpty(true);
        } else
        {
            model.setNumberEmpty(false);
        }
    }
}