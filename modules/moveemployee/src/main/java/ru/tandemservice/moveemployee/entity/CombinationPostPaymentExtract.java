package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.IContractAndAssigmentExtract;
import ru.tandemservice.moveemployee.entity.gen.CombinationPostPaymentExtractGen;

/**
 * Выписка из сборного приказа по кадровому составу. Установление доплат за работу по совмещению
 */
public class CombinationPostPaymentExtract extends CombinationPostPaymentExtractGen implements IContractAndAssigmentExtract
{
}