package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract;
import ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь ставки должности и выписки списочного приказа «Об изменении средств оплаты труда»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffRateToFinSrcChangeListExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation";
    public static final String ENTITY_NAME = "staffRateToFinSrcChangeListExtractRelation";
    public static final int VERSION_HASH = 462140080;
    private static IEntityMeta ENTITY_META;

    public static final String L_LIST_EXTRACT = "listExtract";
    public static final String L_EMPLOYEE_POST_STAFF_RATE_ITEM = "employeePostStaffRateItem";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";
    public static final String L_STAFF_LIST_ALLOCATION_ITEM = "staffListAllocationItem";
    public static final String P_STAFF_RATE = "staffRate";

    private ChangeFinancingSourceEmplListExtract _listExtract;     // Выписка из списочного приказа по кадровому составу. Об изменении средств оплаты труда
    private EmployeePostStaffRateItem _employeePostStaffRateItem;     // Ставка сотрудника (связь ставки и сотрудника)
    private FinancingSource _financingSource;     // Источник финансирования (настройка названий)
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (настройка названий, детально)
    private StaffListAllocationItem _staffListAllocationItem;     // Должность штатной расстановки
    private double _staffRate;     // Ставка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из списочного приказа по кадровому составу. Об изменении средств оплаты труда. Свойство не может быть null.
     */
    @NotNull
    public ChangeFinancingSourceEmplListExtract getListExtract()
    {
        return _listExtract;
    }

    /**
     * @param listExtract Выписка из списочного приказа по кадровому составу. Об изменении средств оплаты труда. Свойство не может быть null.
     */
    public void setListExtract(ChangeFinancingSourceEmplListExtract listExtract)
    {
        dirty(_listExtract, listExtract);
        _listExtract = listExtract;
    }

    /**
     * @return Ставка сотрудника (связь ставки и сотрудника).
     */
    public EmployeePostStaffRateItem getEmployeePostStaffRateItem()
    {
        return _employeePostStaffRateItem;
    }

    /**
     * @param employeePostStaffRateItem Ставка сотрудника (связь ставки и сотрудника).
     */
    public void setEmployeePostStaffRateItem(EmployeePostStaffRateItem employeePostStaffRateItem)
    {
        dirty(_employeePostStaffRateItem, employeePostStaffRateItem);
        _employeePostStaffRateItem = employeePostStaffRateItem;
    }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     */
    @NotNull
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования (настройка названий). Свойство не может быть null.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (настройка названий, детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    /**
     * @return Должность штатной расстановки.
     */
    public StaffListAllocationItem getStaffListAllocationItem()
    {
        return _staffListAllocationItem;
    }

    /**
     * @param staffListAllocationItem Должность штатной расстановки.
     */
    public void setStaffListAllocationItem(StaffListAllocationItem staffListAllocationItem)
    {
        dirty(_staffListAllocationItem, staffListAllocationItem);
        _staffListAllocationItem = staffListAllocationItem;
    }

    /**
     * @return Ставка. Свойство не может быть null.
     */
    @NotNull
    public double getStaffRate()
    {
        return _staffRate;
    }

    /**
     * @param staffRate Ставка. Свойство не может быть null.
     */
    public void setStaffRate(double staffRate)
    {
        dirty(_staffRate, staffRate);
        _staffRate = staffRate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StaffRateToFinSrcChangeListExtractRelationGen)
        {
            setListExtract(((StaffRateToFinSrcChangeListExtractRelation)another).getListExtract());
            setEmployeePostStaffRateItem(((StaffRateToFinSrcChangeListExtractRelation)another).getEmployeePostStaffRateItem());
            setFinancingSource(((StaffRateToFinSrcChangeListExtractRelation)another).getFinancingSource());
            setFinancingSourceItem(((StaffRateToFinSrcChangeListExtractRelation)another).getFinancingSourceItem());
            setStaffListAllocationItem(((StaffRateToFinSrcChangeListExtractRelation)another).getStaffListAllocationItem());
            setStaffRate(((StaffRateToFinSrcChangeListExtractRelation)another).getStaffRate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffRateToFinSrcChangeListExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffRateToFinSrcChangeListExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new StaffRateToFinSrcChangeListExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "listExtract":
                    return obj.getListExtract();
                case "employeePostStaffRateItem":
                    return obj.getEmployeePostStaffRateItem();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
                case "staffListAllocationItem":
                    return obj.getStaffListAllocationItem();
                case "staffRate":
                    return obj.getStaffRate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "listExtract":
                    obj.setListExtract((ChangeFinancingSourceEmplListExtract) value);
                    return;
                case "employeePostStaffRateItem":
                    obj.setEmployeePostStaffRateItem((EmployeePostStaffRateItem) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
                case "staffListAllocationItem":
                    obj.setStaffListAllocationItem((StaffListAllocationItem) value);
                    return;
                case "staffRate":
                    obj.setStaffRate((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "listExtract":
                        return true;
                case "employeePostStaffRateItem":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
                case "staffListAllocationItem":
                        return true;
                case "staffRate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "listExtract":
                    return true;
                case "employeePostStaffRateItem":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
                case "staffListAllocationItem":
                    return true;
                case "staffRate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "listExtract":
                    return ChangeFinancingSourceEmplListExtract.class;
                case "employeePostStaffRateItem":
                    return EmployeePostStaffRateItem.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
                case "staffListAllocationItem":
                    return StaffListAllocationItem.class;
                case "staffRate":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffRateToFinSrcChangeListExtractRelation> _dslPath = new Path<StaffRateToFinSrcChangeListExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffRateToFinSrcChangeListExtractRelation");
    }
            

    /**
     * @return Выписка из списочного приказа по кадровому составу. Об изменении средств оплаты труда. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getListExtract()
     */
    public static ChangeFinancingSourceEmplListExtract.Path<ChangeFinancingSourceEmplListExtract> listExtract()
    {
        return _dslPath.listExtract();
    }

    /**
     * @return Ставка сотрудника (связь ставки и сотрудника).
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getEmployeePostStaffRateItem()
     */
    public static EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem> employeePostStaffRateItem()
    {
        return _dslPath.employeePostStaffRateItem();
    }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    /**
     * @return Должность штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getStaffListAllocationItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> staffListAllocationItem()
    {
        return _dslPath.staffListAllocationItem();
    }

    /**
     * @return Ставка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getStaffRate()
     */
    public static PropertyPath<Double> staffRate()
    {
        return _dslPath.staffRate();
    }

    public static class Path<E extends StaffRateToFinSrcChangeListExtractRelation> extends EntityPath<E>
    {
        private ChangeFinancingSourceEmplListExtract.Path<ChangeFinancingSourceEmplListExtract> _listExtract;
        private EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem> _employeePostStaffRateItem;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _staffListAllocationItem;
        private PropertyPath<Double> _staffRate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из списочного приказа по кадровому составу. Об изменении средств оплаты труда. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getListExtract()
     */
        public ChangeFinancingSourceEmplListExtract.Path<ChangeFinancingSourceEmplListExtract> listExtract()
        {
            if(_listExtract == null )
                _listExtract = new ChangeFinancingSourceEmplListExtract.Path<ChangeFinancingSourceEmplListExtract>(L_LIST_EXTRACT, this);
            return _listExtract;
        }

    /**
     * @return Ставка сотрудника (связь ставки и сотрудника).
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getEmployeePostStaffRateItem()
     */
        public EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem> employeePostStaffRateItem()
        {
            if(_employeePostStaffRateItem == null )
                _employeePostStaffRateItem = new EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem>(L_EMPLOYEE_POST_STAFF_RATE_ITEM, this);
            return _employeePostStaffRateItem;
        }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

    /**
     * @return Должность штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getStaffListAllocationItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> staffListAllocationItem()
        {
            if(_staffListAllocationItem == null )
                _staffListAllocationItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_STAFF_LIST_ALLOCATION_ITEM, this);
            return _staffListAllocationItem;
        }

    /**
     * @return Ставка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.StaffRateToFinSrcChangeListExtractRelation#getStaffRate()
     */
        public PropertyPath<Double> staffRate()
        {
            if(_staffRate == null )
                _staffRate = new PropertyPath<Double>(StaffRateToFinSrcChangeListExtractRelationGen.P_STAFF_RATE, this);
            return _staffRate;
        }

        public Class getEntityClass()
        {
            return StaffRateToFinSrcChangeListExtractRelation.class;
        }

        public String getEntityName()
        {
            return "staffRateToFinSrcChangeListExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
