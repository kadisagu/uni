/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e16;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniemp.UniempDefines;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 08.04.2011
 */
public class ReturnFromChildCareHolidayExtractPrint implements IPrintFormCreator<ReturnFromChildCareHolidayExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ReturnFromChildCareHolidayExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        IdentityCard identityCard = null != extract.getEntity().getId() ? extract.getEntity().getPerson().getIdentityCard() : extract.getEntity().getEmployee().getPerson().getIdentityCard();
        boolean isMaleSex = identityCard.getSex().isMale();
        if (isMaleSex)
        {
            modifier.put("returnSex", "вышедшим");
            modifier.put("beginSex", "приступившим");
        }
        else
        {
            modifier.put("returnSex", "вышедшей");
            modifier.put("beginSex", "приступившей");
        }
        Double sumRate = 0.0d;
        for (FinancingSourceDetails details : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract))
            sumRate += details.getStaffRate();
	    Double tariff = extract.getEntity().getSalary();
	    sumRate*= tariff == null ? 0 : tariff;

        modifier.put("salary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(sumRate));

        modifier.put("returnDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getReturnDate()));

        if (extract.getCompetitionType() != null)
            if (UniDefines.COMPETITION_ASSIGNMENT_TYPE_ELECTION_PASSED.equals(extract.getCompetitionType().getCode()))
                modifier.put("competition", " до избрания на выборах");
            else
                modifier.put("competition", " до избрания по конкурсу");
        else if (extract.getEndDate() != null)
            modifier.put("competition", " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));
        else
            modifier.put("competition", "");

        if (extract.getSavePayment() != null && extract.getSavePayment())
        {
            String secondPart, secondPartFio;
            String secondPartSave = "2. СОХРАНИТЬ";

            StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.DATIVE, isMaleSex).toUpperCase());
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), GrammaCase.DATIVE, isMaleSex));
            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
                str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), GrammaCase.DATIVE, isMaleSex));
            modifier.put("Fio_Dative", str.toString());

            secondPartFio = extract.getDativeFio() + ", " +
                    (extract.getEntity().getPostRelation() != null ? CommonExtractPrintUtil.getPostPrintingTitle(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.DATIVE_CASE, true) : "") + " " +
                    (extract.getEntity().getOrgUnit() != null ? CommonExtractPrintUtil.getOrgUnitPrintingTitle(extract.getEntity().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, false, true) : "") +
                    " выплату пособия по уходу за ребенком до 1,5 лет с " +
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStartDate()) +
                    (extract.getFinishDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getFinishDate()) : "");


            secondPart = ", " +
                    (extract.getEntity().getPostRelation() != null ? CommonExtractPrintUtil.getPostPrintingTitle(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.DATIVE_CASE, true) : "") + " " +
                    (extract.getEntity().getOrgUnit() != null ? CommonExtractPrintUtil.getOrgUnitPrintingTitle(extract.getEntity().getOrgUnit(), CommonExtractPrintUtil.GENITIVE_CASE, false, true) : "") +
                    " выплату пособия по уходу за ребенком до 1,5 лет с " +
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStartDate()) +
                    (extract.getFinishDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getFinishDate()) : "");

            if (extract.getPayment().getFinancingSource() != null)
                if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(extract.getPayment().getFinancingSource().getCode()))
                {
                    secondPart = secondPart + " за счет средств федерального бюджета";
                    secondPartFio = secondPartFio + " за счет средств федерального бюджета";
                }
                else
                {
                    secondPart = secondPart + " за счет средств от приносящей доход деятельности";
                    secondPartFio = secondPartFio + " за счет средств от приносящей доход деятельности";
                }

            secondPart = secondPart + ".";
            secondPartFio = secondPartFio + ".";

            modifier.put("savePayment",secondPart);
            modifier.put("secondPartSave", secondPartSave);
            modifier.put("savePaymentFio", secondPartFio);
        }
        else
        {
            List<String> fieldList = new ArrayList<>(4);
            fieldList.add("savePayment");
            fieldList.add("secondPartSave");
            fieldList.add("Fio_Dative");
            fieldList.add("savePaymentFio");
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, fieldList, true, true);

        }


//        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.ACCUSATIVE, isMaleSex).toUpperCase());
//        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), GrammaCase.ACCUSATIVE, isMaleSex));
//        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
//            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), GrammaCase.ACCUSATIVE, isMaleSex));
//        modifier.put("Fio", str.toString());

        modifier.modify(document);
        return document;
    }
}