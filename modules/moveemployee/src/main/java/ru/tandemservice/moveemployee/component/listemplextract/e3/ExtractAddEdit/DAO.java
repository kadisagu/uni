/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e3.ExtractAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit.AbstractListExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel;
import ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;

/**
 * @author ListExtractComponentGenerator
 * @since 27.04.2011
 */
public class DAO extends AbstractListExtractAddEditDAO<EmploymentDismissalEmplListExtract, Model> implements IDAO
{
    @Override
    protected EmploymentDismissalEmplListExtract createNewExtractInstance(Model model)
    {
        return new EmploymentDismissalEmplListExtract();
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setEmployeeFioModified(model.getExtract().getEmployeePost().getPerson().getFullFio());
        model.getExtract().setEmployee(model.getExtract().getEmployeePost().getEmployee());

        super.update(model);
    }

    @Override
    public EmployeeDismissReasons getEmployeeDismissalReason(EmployeeOrderReasons reason)
    {
        MQBuilder builder = new MQBuilder(EmployeeReasonToDismissReasonRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", EmployeeReasonToDismissReasonRel.L_EMPLOYEE_ORDER_REASON, reason));
        EmployeeReasonToDismissReasonRel rel = (EmployeeReasonToDismissReasonRel)builder.uniqueResult(getSession());

        if (rel != null)
            return rel.getEmployeeDismissReason();

        return null;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setOrderReasonUpdates("dismissReason, lawItem");
    }
}