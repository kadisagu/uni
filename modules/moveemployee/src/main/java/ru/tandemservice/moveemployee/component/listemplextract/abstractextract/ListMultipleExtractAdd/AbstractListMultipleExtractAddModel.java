/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListMultipleExtractAdd;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
@Input({
        @Bind(key = AbstractListMultipleExtractAddModel.PARAMETER_ORDER_ID, binding = "orderId"),
        @Bind(key = AbstractListMultipleExtractAddModel.PARAMETER_EXTRACT_TYPE_ID, binding = "extractType.id")
})
public abstract class AbstractListMultipleExtractAddModel<T extends ListEmployeeExtract>
{
    public static final String PARAMETER_ORDER_ID = "orderId";
    public static final String PARAMETER_EXTRACT_TYPE_ID = "extractTypeId";

    private Long _orderId;
    private EmployeeListOrder _order;
    private EmployeeListParagraph _paragraph;
    private EmployeeExtractType _extractType = new EmployeeExtractType();
    private T _extract;
    private DynamicListDataSource<Employee> _dataSource;
    private String _searchListSettingsKey;
    private List<IEntity> _selectedItemList;

    private List<EmployeeListParagraph> _addedParagraphs;


    // Getters & Setters

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public EmployeeListOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeListOrder order)
    {
        this._order = order;
    }

    public EmployeeListParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EmployeeListParagraph paragraph)
    {
        this._paragraph = paragraph;
    }

    public EmployeeExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(EmployeeExtractType extractType)
    {
        _extractType = extractType;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        this._extract = extract;
    }

    public DynamicListDataSource<Employee> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Employee> dataSource)
    {
        _dataSource = dataSource;
    }

    public String getSearchListSettingsKey()
    {
        return _searchListSettingsKey;
    }

    public void setSearchListSettingsKey(String searchListSettingsKey)
    {
        _searchListSettingsKey = searchListSettingsKey;
    }

    public List<IEntity> getSelectedItemList()
    {
        return _selectedItemList;
    }

    public void setSelectedItemList(List<IEntity> selectedItemList)
    {
        _selectedItemList = selectedItemList;
    }

    public List<EmployeeListParagraph> getAddedParagraphs()
    {
        return _addedParagraphs;
    }

    public void setAddedParagraphs(List<EmployeeListParagraph> addedParagraphs)
    {
        this._addedParagraphs = addedParagraphs;
    }
}