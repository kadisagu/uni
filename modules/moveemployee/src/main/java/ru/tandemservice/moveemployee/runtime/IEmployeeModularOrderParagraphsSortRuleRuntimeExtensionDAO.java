/**
 *$Id$
 */
package ru.tandemservice.moveemployee.runtime;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public interface IEmployeeModularOrderParagraphsSortRuleRuntimeExtensionDAO
{
    public void createRuleList();
}