/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e8.ParagraphAddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class DAO extends AbstractListParagraphAddEditDAO<PayForHolidayDayProvideHoldayEmplListExtract, Model> implements IDAO
{
    protected static OrderDescriptionRegistry orderRegistry = new OrderDescriptionRegistry("b");

    @Override
    protected PayForHolidayDayProvideHoldayEmplListExtract createNewExtractInstance(Model model)
    {
        return new PayForHolidayDayProvideHoldayEmplListExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
        {
            MQBuilder orgUnitBuilder = new MQBuilder(OrgUnitInListExtractRelation.ENTITY_CLASS, "b");
            orgUnitBuilder.add(MQExpression.eq("b", OrgUnitInListExtractRelation.employeeListParagraph().s(), model.getParagraph()));

            MQBuilder employeeTypeBuilder = new MQBuilder(EmployeeTypeInListExtractRelation.ENTITY_CLASS, "b");
            employeeTypeBuilder.add(MQExpression.eq("b", EmployeeTypeInListExtractRelation.employeeListParagraph().s(), model.getParagraph()));

            MQBuilder postBuilder = new MQBuilder(PostInListExtractRelation.ENTITY_CLASS, "b");
            postBuilder.add(MQExpression.eq("b", PostInListExtractRelation.employeeListParagraph().s(), model.getParagraph()));

            List<OrgUnit> orgUnitList = new ArrayList<>();
            List<EmployeeType> employeeTypeList = new ArrayList<>();
            List<PostBoundedWithQGandQL> postList = new ArrayList<>();

            for (EmployeeTypeInListExtractRelation relation : employeeTypeBuilder.<EmployeeTypeInListExtractRelation>getResultList(getSession()))
            {
                employeeTypeList.add(relation.getEmployeeType());
                delete(relation);
            }

            for (PostInListExtractRelation relation : postBuilder.<PostInListExtractRelation>getResultList(getSession()))
            {
                postList.add(relation.getPostBoundedWithQGandQL());
                delete(relation);
            }

            for (OrgUnitInListExtractRelation relation : orgUnitBuilder.<OrgUnitInListExtractRelation>getResultList(getSession()))
            {
                orgUnitList.add(relation.getOrgUnit());
                delete(relation);
            }

            model.setOrgUnitList(orgUnitList);
            model.setEmployeeTypeList(employeeTypeList);
            model.setPostList(postList);

            model.setChildOrgUnit(((PayForHolidayDayProvideHoldayEmplListExtract)model.getParagraph().getFirstExtract()).isChildOrgUnit());

            model.setSelectEmployee(new ArrayList<>());

            for (PayForHolidayDayProvideHoldayEmplListExtract extract : (List<PayForHolidayDayProvideHoldayEmplListExtract>) model.getParagraph().getExtractList())
            {
                model.getSelectEmployee().add(extract.getEntity());
                model.getEmployeeIdHolidayDayMap().put(extract.getEntity().getId(), extract.getHolidayDate());
            }

            PayForHolidayDayProvideHoldayEmplListExtract extract = (PayForHolidayDayProvideHoldayEmplListExtract) model.getParagraph().getFirstExtract();

            model.setChildOrgUnit(extract.isChildOrgUnit());

            model.setTempEmployeeList(model.getSelectEmployee());
        }

        model.setOrgUnitModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", OrgUnit.archival(), false));
                builder.add(MQExpression.like("b", OrgUnit.title(), CoreStringUtils.escapeLike(filter)));
                if (set != null)
                    builder.add(MQExpression.in("b", OrgUnit.id(), set));
                builder.addOrder("b", OrgUnit.title());

                return new MQListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnit)value).getTitleWithType();
            }
        });

        model.setEmployeeTypeModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                MQBuilder builder = new MQBuilder(EmployeeType.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", EmployeeType.title(), CoreStringUtils.escapeLike(filter)));
                if (set != null)
                    builder.add(MQExpression.in("b", EmployeeType.id(), set));
                builder.addOrder("b", EmployeeType.title());

                return new MQListResultBuilder(builder);
            }
        });

        model.setPostModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, "b");
                if (model.getEmployeeTypeList() != null && !model.getEmployeeTypeList().isEmpty())
                builder.add(MQExpression.in("b", PostBoundedWithQGandQL.post().employeeType(), model.getEmployeeTypeList()));
                builder.add(MQExpression.like("b", PostBoundedWithQGandQL.title(), CoreStringUtils.escapeLike(filter)));
                if (set != null)
                    builder.add(MQExpression.in("b", PostBoundedWithQGandQL.id(), set));
                builder.addOrder("b", PostBoundedWithQGandQL.title());

                return new MQListResultBuilder(builder);
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareEmployeeDataSource(Model model)
    {
        if (model.getTempEmployeeList() == null)
            model.setTempEmployeeList(model.getEmployeePostList());

        List<OrgUnit> orgUnitList = new ArrayList<>();
        if (model.getOrgUnitList() != null)
            orgUnitList.addAll(model.getOrgUnitList());
        if (model.getChildOrgUnit())
            for (OrgUnit orgUnit : model.getOrgUnitList())
                orgUnitList.addAll(OrgUnitManager.instance().dao().getChildOrgUnits(orgUnit, true, false));

        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "b");
        builder.add(MQExpression.in("b", EmployeePost.orgUnit().s(), orgUnitList));
        if (model.getEmployeeTypeList() != null && !model.getEmployeeTypeList().isEmpty())
            builder.add(MQExpression.in("b", EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType(), model.getEmployeeTypeList()));
        if (model.getPostList() != null && !model.getPostList().isEmpty())
            builder.add(MQExpression.in("b", EmployeePost.postRelation().postBoundedWithQGandQL(), model.getPostList()));
        builder.add(MQExpression.eq("b", EmployeePost.postStatus().active().s(), Boolean.TRUE));

        orderRegistry.applyOrder(builder, model.getEmployeeDataSource().getEntityOrder());

        model.setEmployeePostList(builder.<EmployeePost>getResultList(getSession(), (int) model.getEmployeeDataSource().getActualStartRow(), (int) model.getEmployeeDataSource().getCountRow()));

        UniBaseUtils.createPage(model.getEmployeeDataSource(), builder, getSession());

        final IValueMapHolder holidayDayHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("holidayDay");
        final Map<Long, Date> holidayDayMap = (null == holidayDayHolder ? Collections.emptyMap() : holidayDayHolder.getValueMap());

        for (EmployeePost employeePost : model.getEmployeeDataSource().getEntityList())
        {
            Date holidayDay = holidayDayMap.get(employeePost.getId());
            Date timeAmountSecond = model.getEmployeeIdHolidayDayMap().get(employeePost.getId());
            holidayDayMap.put(employeePost.getId(), holidayDay != null ? holidayDay : timeAmountSecond);
        }

        model.setTempEmployeeList(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getEmployeeDataSource().getSelectedEntities().isEmpty())
            errors.add("Необходимо выбрать хотя бы одного сотрудника.");

        final IValueMapHolder holidayDayHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("holidayDay");
        final Map<Long, Date> holidayDayMap = (null == holidayDayHolder ? Collections.emptyMap() : holidayDayHolder.getValueMap());

        for (EmployeePost employeePost : model.getEmployeeDataSource().getSelectedEntities())
        {
            if (model.isEditForm())
            {
                if (!model.getSelectEmployee().contains(employeePost))
                    if (isEmployeePostHasNotFinishedExtracts(employeePost, model.getOrder().getType()))
                        errors.add("Нельзя добавить параграф, так как у сотрудника '" + employeePost.getPerson().getFio() + "' уже есть непроведенные выписки.");
            }
            else
            if (isEmployeePostHasNotFinishedExtracts(employeePost, model.getOrder().getType()))
                errors.add("Нельзя добавить параграф, так как у сотрудника '" + employeePost.getPerson().getFio() + "' уже есть непроведенные выписки.");


            if (holidayDayMap.get(employeePost.getId()) == null)
                errors.add("Поле «Нерабочий праздничный день» обязательно для заполнения.", "holidayDayId_" + employeePost.getId());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        model.setToSaveExtractList(new ArrayList<>());

        ExtractStates extractStates = getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER);

        final IValueMapHolder holidayDayHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("holidayDay");
        final Map<Long, Date> holidayDayMap = (null == holidayDayHolder ? Collections.emptyMap() : holidayDayHolder.getValueMap());

        for (EmployeePost employeePost : model.getEmployeeDataSource().getSelectedEntities())
        {
            PayForHolidayDayProvideHoldayEmplListExtract extract = new PayForHolidayDayProvideHoldayEmplListExtract();
            extract.setEntity(employeePost);
            extract.setEmployee(employeePost.getEmployee());
            extract.setParagraph(model.getParagraph());
            extract.setState(extractStates);
            extract.setEmployeeFioModified(employeePost.getPerson().getFio());
            extract.setType(model.getExtractType());
            extract.setCreateDate(model.getCreateDate());
            extract.setReason(model.getReason());
            //данные данной выписки
            extract.setChildOrgUnit(model.getChildOrgUnit());
            extract.setHolidayDate(holidayDayMap.get(employeePost.getId()));

            model.getToSaveExtractList().add(extract);
        }

        saveOrUpdate(model.getParagraph());
        for (OrgUnit orgUnit : model.getOrgUnitList())
        {
            OrgUnitInListExtractRelation relation = new OrgUnitInListExtractRelation();
            relation.setOrgUnit(orgUnit);
            relation.setEmployeeListParagraph(model.getParagraph());
            save(relation);
        }

        if (model.getEmployeeTypeList() != null && !model.getEmployeeTypeList().isEmpty())
        for (EmployeeType type : model.getEmployeeTypeList())
        {
            EmployeeTypeInListExtractRelation relation = new EmployeeTypeInListExtractRelation();
            relation.setEmployeeType(type);
            relation.setEmployeeListParagraph(model.getParagraph());
            save(relation);
        }

        if (model.getPostList() != null && !model.getPostList().isEmpty())
        for (PostBoundedWithQGandQL post : model.getPostList())
        {
            PostInListExtractRelation relation = new PostInListExtractRelation();
            relation.setPostBoundedWithQGandQL(post);
            relation.setEmployeeListParagraph(model.getParagraph());
            save(relation);
        }

        super.update(model);
    }

    //вынесено в отдельный метод, т.к. общий методне подходит
    //для данного типа выписки необходимо вычитать выписки с "аналогичным типом"
    protected boolean isEmployeePostHasNotFinishedExtracts(EmployeePost post, EmployeeExtractType type)
    {
        MQBuilder subBuilder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "b", new String[]{AbstractEmployeeExtract.id().s()});
        subBuilder.add(MQExpression.eq("b", AbstractEmployeeExtract.state().code(), UnimoveDefines.CATALOG_EXTRACT_STATE_REJECTED));
        subBuilder.add(MQExpression.eq("b", AbstractEmployeeExtract.paragraph().order().state().code(), UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

        MQBuilder sub2Builder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "s", new String[]{AbstractEmployeeExtract.id().s()});
        sub2Builder.add(MQExpression.eq("s", AbstractEmployeeExtract.type().parent(), type));
        sub2Builder.add(MQExpression.eq("s", AbstractEmployeeExtract.entity(), post));

        MQBuilder builder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", AbstractEmployeeExtract.entity(), post));
        builder.add(MQExpression.notEq("e", AbstractEmployeeExtract.state().code(), UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        builder.add(MQExpression.notIn("e", AbstractEmployeeExtract.id(), subBuilder));
        builder.add(MQExpression.notIn("e", AbstractEmployeeExtract.id(), sub2Builder));
        return builder.getResultCount(getSession()) > 0;
    }
}
