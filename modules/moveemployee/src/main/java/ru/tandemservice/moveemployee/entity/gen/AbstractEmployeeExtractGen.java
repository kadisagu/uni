package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimv.IAbstractDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка по кадрам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AbstractEmployeeExtractGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract";
    public static final String ENTITY_NAME = "abstractEmployeeExtract";
    public static final int VERSION_HASH = 729308063;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_COMMITTED = "committed";
    public static final String P_NUMBER = "number";
    public static final String P_EMPLOYEE_FIO_MODIFIED = "employeeFioModified";
    public static final String L_ENTITY = "entity";
    public static final String L_STATE = "state";
    public static final String L_TYPE = "type";
    public static final String L_PARAGRAPH = "paragraph";
    public static final String P_TITLE_ORDER = "titleOrder";

    private Date _createDate;     // Дата формирования
    private boolean _committed;     // Проведена
    private Integer _number;     // Номер выписки в параграфе
    private String _employeeFioModified;     // ФИО сотрудника в падеже
    private EmployeePost _entity;     // Сотрудник
    private ExtractStates _state;     // Состояние выписки
    private EmployeeExtractType _type;     // Тип выписки по кадрам
    private AbstractEmployeeParagraph _paragraph;     // Параграф приказа по кадрам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Проведена. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommitted()
    {
        return _committed;
    }

    /**
     * @param committed Проведена. Свойство не может быть null.
     */
    public void setCommitted(boolean committed)
    {
        dirty(_committed, committed);
        _committed = committed;
    }

    /**
     * @return Номер выписки в параграфе.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер выписки в параграфе.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return ФИО сотрудника в падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEmployeeFioModified()
    {
        return _employeeFioModified;
    }

    /**
     * @param employeeFioModified ФИО сотрудника в падеже. Свойство не может быть null.
     */
    public void setEmployeeFioModified(String employeeFioModified)
    {
        dirty(_employeeFioModified, employeeFioModified);
        _employeeFioModified = employeeFioModified;
    }

    /**
     * @return Сотрудник.
     */
    public EmployeePost getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Сотрудник.
     */
    public void setEntity(EmployeePost entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     */
    @NotNull
    public ExtractStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние выписки. Свойство не может быть null.
     */
    public void setState(ExtractStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Тип выписки по кадрам. Свойство не может быть null.
     */
    @NotNull
    public EmployeeExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип выписки по кадрам. Свойство не может быть null.
     */
    public void setType(EmployeeExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Параграф приказа по кадрам.
     */
    public AbstractEmployeeParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Параграф приказа по кадрам.
     */
    public void setParagraph(AbstractEmployeeParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AbstractEmployeeExtractGen)
        {
            setCreateDate(((AbstractEmployeeExtract)another).getCreateDate());
            setCommitted(((AbstractEmployeeExtract)another).isCommitted());
            setNumber(((AbstractEmployeeExtract)another).getNumber());
            setEmployeeFioModified(((AbstractEmployeeExtract)another).getEmployeeFioModified());
            setEntity(((AbstractEmployeeExtract)another).getEntity());
            setState(((AbstractEmployeeExtract)another).getState());
            setType(((AbstractEmployeeExtract)another).getType());
            setParagraph(((AbstractEmployeeExtract)another).getParagraph());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AbstractEmployeeExtractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AbstractEmployeeExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("AbstractEmployeeExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "committed":
                    return obj.isCommitted();
                case "number":
                    return obj.getNumber();
                case "employeeFioModified":
                    return obj.getEmployeeFioModified();
                case "entity":
                    return obj.getEntity();
                case "state":
                    return obj.getState();
                case "type":
                    return obj.getType();
                case "paragraph":
                    return obj.getParagraph();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "committed":
                    obj.setCommitted((Boolean) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "employeeFioModified":
                    obj.setEmployeeFioModified((String) value);
                    return;
                case "entity":
                    obj.setEntity((EmployeePost) value);
                    return;
                case "state":
                    obj.setState((ExtractStates) value);
                    return;
                case "type":
                    obj.setType((EmployeeExtractType) value);
                    return;
                case "paragraph":
                    obj.setParagraph((AbstractEmployeeParagraph) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "committed":
                        return true;
                case "number":
                        return true;
                case "employeeFioModified":
                        return true;
                case "entity":
                        return true;
                case "state":
                        return true;
                case "type":
                        return true;
                case "paragraph":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "committed":
                    return true;
                case "number":
                    return true;
                case "employeeFioModified":
                    return true;
                case "entity":
                    return true;
                case "state":
                    return true;
                case "type":
                    return true;
                case "paragraph":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "committed":
                    return Boolean.class;
                case "number":
                    return Integer.class;
                case "employeeFioModified":
                    return String.class;
                case "entity":
                    return EmployeePost.class;
                case "state":
                    return ExtractStates.class;
                case "type":
                    return EmployeeExtractType.class;
                case "paragraph":
                    return AbstractEmployeeParagraph.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AbstractEmployeeExtract> _dslPath = new Path<AbstractEmployeeExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AbstractEmployeeExtract");
    }
            

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#isCommitted()
     */
    public static PropertyPath<Boolean> committed()
    {
        return _dslPath.committed();
    }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return ФИО сотрудника в падеже. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getEmployeeFioModified()
     */
    public static PropertyPath<String> employeeFioModified()
    {
        return _dslPath.employeeFioModified();
    }

    /**
     * @return Сотрудник.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getEntity()
     */
    public static EmployeePost.Path<EmployeePost> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getState()
     */
    public static ExtractStates.Path<ExtractStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Тип выписки по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getType()
     */
    public static EmployeeExtractType.Path<EmployeeExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Параграф приказа по кадрам.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getParagraph()
     */
    public static AbstractEmployeeParagraph.Path<AbstractEmployeeParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getTitleOrder()
     */
    public static SupportedPropertyPath<String> titleOrder()
    {
        return _dslPath.titleOrder();
    }

    public static class Path<E extends AbstractEmployeeExtract> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<Boolean> _committed;
        private PropertyPath<Integer> _number;
        private PropertyPath<String> _employeeFioModified;
        private EmployeePost.Path<EmployeePost> _entity;
        private ExtractStates.Path<ExtractStates> _state;
        private EmployeeExtractType.Path<EmployeeExtractType> _type;
        private AbstractEmployeeParagraph.Path<AbstractEmployeeParagraph> _paragraph;
        private SupportedPropertyPath<String> _titleOrder;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(AbstractEmployeeExtractGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#isCommitted()
     */
        public PropertyPath<Boolean> committed()
        {
            if(_committed == null )
                _committed = new PropertyPath<Boolean>(AbstractEmployeeExtractGen.P_COMMITTED, this);
            return _committed;
        }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(AbstractEmployeeExtractGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return ФИО сотрудника в падеже. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getEmployeeFioModified()
     */
        public PropertyPath<String> employeeFioModified()
        {
            if(_employeeFioModified == null )
                _employeeFioModified = new PropertyPath<String>(AbstractEmployeeExtractGen.P_EMPLOYEE_FIO_MODIFIED, this);
            return _employeeFioModified;
        }

    /**
     * @return Сотрудник.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getEntity()
     */
        public EmployeePost.Path<EmployeePost> entity()
        {
            if(_entity == null )
                _entity = new EmployeePost.Path<EmployeePost>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getState()
     */
        public ExtractStates.Path<ExtractStates> state()
        {
            if(_state == null )
                _state = new ExtractStates.Path<ExtractStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Тип выписки по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getType()
     */
        public EmployeeExtractType.Path<EmployeeExtractType> type()
        {
            if(_type == null )
                _type = new EmployeeExtractType.Path<EmployeeExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Параграф приказа по кадрам.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getParagraph()
     */
        public AbstractEmployeeParagraph.Path<AbstractEmployeeParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new AbstractEmployeeParagraph.Path<AbstractEmployeeParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract#getTitleOrder()
     */
        public SupportedPropertyPath<String> titleOrder()
        {
            if(_titleOrder == null )
                _titleOrder = new SupportedPropertyPath<String>(AbstractEmployeeExtractGen.P_TITLE_ORDER, this);
            return _titleOrder;
        }

        public Class getEntityClass()
        {
            return AbstractEmployeeExtract.class;
        }

        public String getEntityName()
        {
            return "abstractEmployeeExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleOrder();
}
