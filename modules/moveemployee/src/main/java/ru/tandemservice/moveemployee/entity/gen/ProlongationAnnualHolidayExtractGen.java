package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О продлении ежегодного отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ProlongationAnnualHolidayExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract";
    public static final String ENTITY_NAME = "prolongationAnnualHolidayExtract";
    public static final int VERSION_HASH = 131866699;
    private static IEntityMeta ENTITY_META;

    public static final String L_HOLIDAY = "holiday";
    public static final String P_PROLONGATION_FROM = "prolongationFrom";
    public static final String P_PROLONGATION_TO = "prolongationTo";
    public static final String P_AMOUNT_DAYS_PROLONGATION = "amountDaysProlongation";
    public static final String P_HOLIDAY_BEGIN = "holidayBegin";
    public static final String P_HOLIDAY_END = "holidayEnd";
    public static final String P_HOLIDAY_DURATION = "holidayDuration";
    public static final String P_HOLIDAY_END_OLD = "holidayEndOld";
    public static final String P_SECOND_JOB = "secondJob";

    private EmployeeHoliday _holiday;     // Отпуск
    private Date _prolongationFrom;     // Продлить с
    private Date _prolongationTo;     // Продлить по
    private int _amountDaysProlongation;     // Количество дней продления
    private Date _holidayBegin;     // Дата начала отпуска
    private Date _holidayEnd;     // Дата окончания отпуска
    private Integer _holidayDuration;     // Длительность отпуска
    private Date _holidayEndOld;     // Дата окончания отпуска до проведения
    private Boolean _secondJob;     // Продлить отпуск по работе по совместительству в тот же срок

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отпуск. Свойство не может быть null.
     */
    @NotNull
    public EmployeeHoliday getHoliday()
    {
        return _holiday;
    }

    /**
     * @param holiday Отпуск. Свойство не может быть null.
     */
    public void setHoliday(EmployeeHoliday holiday)
    {
        dirty(_holiday, holiday);
        _holiday = holiday;
    }

    /**
     * @return Продлить с. Свойство не может быть null.
     */
    @NotNull
    public Date getProlongationFrom()
    {
        return _prolongationFrom;
    }

    /**
     * @param prolongationFrom Продлить с. Свойство не может быть null.
     */
    public void setProlongationFrom(Date prolongationFrom)
    {
        dirty(_prolongationFrom, prolongationFrom);
        _prolongationFrom = prolongationFrom;
    }

    /**
     * @return Продлить по. Свойство не может быть null.
     */
    @NotNull
    public Date getProlongationTo()
    {
        return _prolongationTo;
    }

    /**
     * @param prolongationTo Продлить по. Свойство не может быть null.
     */
    public void setProlongationTo(Date prolongationTo)
    {
        dirty(_prolongationTo, prolongationTo);
        _prolongationTo = prolongationTo;
    }

    /**
     * @return Количество дней продления. Свойство не может быть null.
     */
    @NotNull
    public int getAmountDaysProlongation()
    {
        return _amountDaysProlongation;
    }

    /**
     * @param amountDaysProlongation Количество дней продления. Свойство не может быть null.
     */
    public void setAmountDaysProlongation(int amountDaysProlongation)
    {
        dirty(_amountDaysProlongation, amountDaysProlongation);
        _amountDaysProlongation = amountDaysProlongation;
    }

    /**
     * @return Дата начала отпуска.
     */
    public Date getHolidayBegin()
    {
        return _holidayBegin;
    }

    /**
     * @param holidayBegin Дата начала отпуска.
     */
    public void setHolidayBegin(Date holidayBegin)
    {
        dirty(_holidayBegin, holidayBegin);
        _holidayBegin = holidayBegin;
    }

    /**
     * @return Дата окончания отпуска.
     */
    public Date getHolidayEnd()
    {
        return _holidayEnd;
    }

    /**
     * @param holidayEnd Дата окончания отпуска.
     */
    public void setHolidayEnd(Date holidayEnd)
    {
        dirty(_holidayEnd, holidayEnd);
        _holidayEnd = holidayEnd;
    }

    /**
     * @return Длительность отпуска.
     */
    public Integer getHolidayDuration()
    {
        return _holidayDuration;
    }

    /**
     * @param holidayDuration Длительность отпуска.
     */
    public void setHolidayDuration(Integer holidayDuration)
    {
        dirty(_holidayDuration, holidayDuration);
        _holidayDuration = holidayDuration;
    }

    /**
     * @return Дата окончания отпуска до проведения.
     */
    public Date getHolidayEndOld()
    {
        return _holidayEndOld;
    }

    /**
     * @param holidayEndOld Дата окончания отпуска до проведения.
     */
    public void setHolidayEndOld(Date holidayEndOld)
    {
        dirty(_holidayEndOld, holidayEndOld);
        _holidayEndOld = holidayEndOld;
    }

    /**
     * @return Продлить отпуск по работе по совместительству в тот же срок.
     */
    public Boolean getSecondJob()
    {
        return _secondJob;
    }

    /**
     * @param secondJob Продлить отпуск по работе по совместительству в тот же срок.
     */
    public void setSecondJob(Boolean secondJob)
    {
        dirty(_secondJob, secondJob);
        _secondJob = secondJob;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ProlongationAnnualHolidayExtractGen)
        {
            setHoliday(((ProlongationAnnualHolidayExtract)another).getHoliday());
            setProlongationFrom(((ProlongationAnnualHolidayExtract)another).getProlongationFrom());
            setProlongationTo(((ProlongationAnnualHolidayExtract)another).getProlongationTo());
            setAmountDaysProlongation(((ProlongationAnnualHolidayExtract)another).getAmountDaysProlongation());
            setHolidayBegin(((ProlongationAnnualHolidayExtract)another).getHolidayBegin());
            setHolidayEnd(((ProlongationAnnualHolidayExtract)another).getHolidayEnd());
            setHolidayDuration(((ProlongationAnnualHolidayExtract)another).getHolidayDuration());
            setHolidayEndOld(((ProlongationAnnualHolidayExtract)another).getHolidayEndOld());
            setSecondJob(((ProlongationAnnualHolidayExtract)another).getSecondJob());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ProlongationAnnualHolidayExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ProlongationAnnualHolidayExtract.class;
        }

        public T newInstance()
        {
            return (T) new ProlongationAnnualHolidayExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "holiday":
                    return obj.getHoliday();
                case "prolongationFrom":
                    return obj.getProlongationFrom();
                case "prolongationTo":
                    return obj.getProlongationTo();
                case "amountDaysProlongation":
                    return obj.getAmountDaysProlongation();
                case "holidayBegin":
                    return obj.getHolidayBegin();
                case "holidayEnd":
                    return obj.getHolidayEnd();
                case "holidayDuration":
                    return obj.getHolidayDuration();
                case "holidayEndOld":
                    return obj.getHolidayEndOld();
                case "secondJob":
                    return obj.getSecondJob();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "holiday":
                    obj.setHoliday((EmployeeHoliday) value);
                    return;
                case "prolongationFrom":
                    obj.setProlongationFrom((Date) value);
                    return;
                case "prolongationTo":
                    obj.setProlongationTo((Date) value);
                    return;
                case "amountDaysProlongation":
                    obj.setAmountDaysProlongation((Integer) value);
                    return;
                case "holidayBegin":
                    obj.setHolidayBegin((Date) value);
                    return;
                case "holidayEnd":
                    obj.setHolidayEnd((Date) value);
                    return;
                case "holidayDuration":
                    obj.setHolidayDuration((Integer) value);
                    return;
                case "holidayEndOld":
                    obj.setHolidayEndOld((Date) value);
                    return;
                case "secondJob":
                    obj.setSecondJob((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "holiday":
                        return true;
                case "prolongationFrom":
                        return true;
                case "prolongationTo":
                        return true;
                case "amountDaysProlongation":
                        return true;
                case "holidayBegin":
                        return true;
                case "holidayEnd":
                        return true;
                case "holidayDuration":
                        return true;
                case "holidayEndOld":
                        return true;
                case "secondJob":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "holiday":
                    return true;
                case "prolongationFrom":
                    return true;
                case "prolongationTo":
                    return true;
                case "amountDaysProlongation":
                    return true;
                case "holidayBegin":
                    return true;
                case "holidayEnd":
                    return true;
                case "holidayDuration":
                    return true;
                case "holidayEndOld":
                    return true;
                case "secondJob":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "holiday":
                    return EmployeeHoliday.class;
                case "prolongationFrom":
                    return Date.class;
                case "prolongationTo":
                    return Date.class;
                case "amountDaysProlongation":
                    return Integer.class;
                case "holidayBegin":
                    return Date.class;
                case "holidayEnd":
                    return Date.class;
                case "holidayDuration":
                    return Integer.class;
                case "holidayEndOld":
                    return Date.class;
                case "secondJob":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ProlongationAnnualHolidayExtract> _dslPath = new Path<ProlongationAnnualHolidayExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ProlongationAnnualHolidayExtract");
    }
            

    /**
     * @return Отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> holiday()
    {
        return _dslPath.holiday();
    }

    /**
     * @return Продлить с. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getProlongationFrom()
     */
    public static PropertyPath<Date> prolongationFrom()
    {
        return _dslPath.prolongationFrom();
    }

    /**
     * @return Продлить по. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getProlongationTo()
     */
    public static PropertyPath<Date> prolongationTo()
    {
        return _dslPath.prolongationTo();
    }

    /**
     * @return Количество дней продления. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getAmountDaysProlongation()
     */
    public static PropertyPath<Integer> amountDaysProlongation()
    {
        return _dslPath.amountDaysProlongation();
    }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHolidayBegin()
     */
    public static PropertyPath<Date> holidayBegin()
    {
        return _dslPath.holidayBegin();
    }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHolidayEnd()
     */
    public static PropertyPath<Date> holidayEnd()
    {
        return _dslPath.holidayEnd();
    }

    /**
     * @return Длительность отпуска.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHolidayDuration()
     */
    public static PropertyPath<Integer> holidayDuration()
    {
        return _dslPath.holidayDuration();
    }

    /**
     * @return Дата окончания отпуска до проведения.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHolidayEndOld()
     */
    public static PropertyPath<Date> holidayEndOld()
    {
        return _dslPath.holidayEndOld();
    }

    /**
     * @return Продлить отпуск по работе по совместительству в тот же срок.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getSecondJob()
     */
    public static PropertyPath<Boolean> secondJob()
    {
        return _dslPath.secondJob();
    }

    public static class Path<E extends ProlongationAnnualHolidayExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeeHoliday.Path<EmployeeHoliday> _holiday;
        private PropertyPath<Date> _prolongationFrom;
        private PropertyPath<Date> _prolongationTo;
        private PropertyPath<Integer> _amountDaysProlongation;
        private PropertyPath<Date> _holidayBegin;
        private PropertyPath<Date> _holidayEnd;
        private PropertyPath<Integer> _holidayDuration;
        private PropertyPath<Date> _holidayEndOld;
        private PropertyPath<Boolean> _secondJob;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> holiday()
        {
            if(_holiday == null )
                _holiday = new EmployeeHoliday.Path<EmployeeHoliday>(L_HOLIDAY, this);
            return _holiday;
        }

    /**
     * @return Продлить с. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getProlongationFrom()
     */
        public PropertyPath<Date> prolongationFrom()
        {
            if(_prolongationFrom == null )
                _prolongationFrom = new PropertyPath<Date>(ProlongationAnnualHolidayExtractGen.P_PROLONGATION_FROM, this);
            return _prolongationFrom;
        }

    /**
     * @return Продлить по. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getProlongationTo()
     */
        public PropertyPath<Date> prolongationTo()
        {
            if(_prolongationTo == null )
                _prolongationTo = new PropertyPath<Date>(ProlongationAnnualHolidayExtractGen.P_PROLONGATION_TO, this);
            return _prolongationTo;
        }

    /**
     * @return Количество дней продления. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getAmountDaysProlongation()
     */
        public PropertyPath<Integer> amountDaysProlongation()
        {
            if(_amountDaysProlongation == null )
                _amountDaysProlongation = new PropertyPath<Integer>(ProlongationAnnualHolidayExtractGen.P_AMOUNT_DAYS_PROLONGATION, this);
            return _amountDaysProlongation;
        }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHolidayBegin()
     */
        public PropertyPath<Date> holidayBegin()
        {
            if(_holidayBegin == null )
                _holidayBegin = new PropertyPath<Date>(ProlongationAnnualHolidayExtractGen.P_HOLIDAY_BEGIN, this);
            return _holidayBegin;
        }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHolidayEnd()
     */
        public PropertyPath<Date> holidayEnd()
        {
            if(_holidayEnd == null )
                _holidayEnd = new PropertyPath<Date>(ProlongationAnnualHolidayExtractGen.P_HOLIDAY_END, this);
            return _holidayEnd;
        }

    /**
     * @return Длительность отпуска.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHolidayDuration()
     */
        public PropertyPath<Integer> holidayDuration()
        {
            if(_holidayDuration == null )
                _holidayDuration = new PropertyPath<Integer>(ProlongationAnnualHolidayExtractGen.P_HOLIDAY_DURATION, this);
            return _holidayDuration;
        }

    /**
     * @return Дата окончания отпуска до проведения.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getHolidayEndOld()
     */
        public PropertyPath<Date> holidayEndOld()
        {
            if(_holidayEndOld == null )
                _holidayEndOld = new PropertyPath<Date>(ProlongationAnnualHolidayExtractGen.P_HOLIDAY_END_OLD, this);
            return _holidayEndOld;
        }

    /**
     * @return Продлить отпуск по работе по совместительству в тот же срок.
     * @see ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract#getSecondJob()
     */
        public PropertyPath<Boolean> secondJob()
        {
            if(_secondJob == null )
                _secondJob = new PropertyPath<Boolean>(ProlongationAnnualHolidayExtractGen.P_SECOND_JOB, this);
            return _secondJob;
        }

        public Class getEntityClass()
        {
            return ProlongationAnnualHolidayExtract.class;
        }

        public String getEntityName()
        {
            return "prolongationAnnualHolidayExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
