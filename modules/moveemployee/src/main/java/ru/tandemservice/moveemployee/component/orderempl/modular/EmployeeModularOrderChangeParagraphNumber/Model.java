/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderChangeParagraphNumber;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;

/**
 * @author Alexander Zhebko
 * @since 27.03.2013
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")
} )
public class Model
{
    private Long _id;
    private ModularEmployeeExtract _extract;
    private int _newNumber;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public ModularEmployeeExtract getExtract()
    {
        return _extract;
    }

    public void setExtract(ModularEmployeeExtract extract)
    {
        _extract = extract;
    }

    public int getNewNumber()
    {
        return _newNumber;
    }

    public void setNewNumber(int newNumber)
    {
        _newNumber = newNumber;
    }
}