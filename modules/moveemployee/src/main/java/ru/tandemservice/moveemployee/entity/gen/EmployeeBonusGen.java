package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выплата, назначенная сотруднику в рамках выписки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeBonusGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeBonus";
    public static final String ENTITY_NAME = "employeeBonus";
    public static final int VERSION_HASH = 1322823093;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_ENTITY = "entity";
    public static final String L_PAYMENT = "payment";
    public static final String P_VALUE = "value";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";
    public static final String P_ASSIGN_DATE = "assignDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_PRIORITY = "priority";
    public static final String P_DEPEND_ON_SUMM_STAFF_RATE = "dependOnSummStaffRate";

    private AbstractEmployeeExtract _extract;     // Выписка по кадрам
    private EmployeePayment _entity;     // Выплата сотрудника, созданная по выписке
    private Payment _payment;     // Выплата
    private double _value;     // Сумма выплаты
    private FinancingSource _financingSource;     // Источник выплаты
    private FinancingSourceItem _financingSourceItem;     // Источник выплаты (детально)
    private Date _assignDate;     // Дата назначения
    private Date _beginDate;     // Назначена с даты
    private Date _endDate;     // Назначена по дату
    private int _priority;     // Приоритет
    private boolean _dependOnSummStaffRate;     // Учитывать при расчете Суммы выплаты суммарную ставку

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка по кадрам. Свойство не может быть null.
     */
    @NotNull
    public AbstractEmployeeExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка по кадрам. Свойство не может быть null.
     */
    public void setExtract(AbstractEmployeeExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Выплата сотрудника, созданная по выписке.
     */
    public EmployeePayment getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Выплата сотрудника, созданная по выписке.
     */
    public void setEntity(EmployeePayment entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Выплата. Свойство не может быть null.
     */
    @NotNull
    public Payment getPayment()
    {
        return _payment;
    }

    /**
     * @param payment Выплата. Свойство не может быть null.
     */
    public void setPayment(Payment payment)
    {
        dirty(_payment, payment);
        _payment = payment;
    }

    /**
     * @return Сумма выплаты. Свойство не может быть null.
     */
    @NotNull
    public double getValue()
    {
        return _value;
    }

    /**
     * @param value Сумма выплаты. Свойство не может быть null.
     */
    public void setValue(double value)
    {
        dirty(_value, value);
        _value = value;
    }

    /**
     * @return Источник выплаты.
     */
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник выплаты.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник выплаты (детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник выплаты (детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    /**
     * @return Дата назначения. Свойство не может быть null.
     */
    @NotNull
    public Date getAssignDate()
    {
        return _assignDate;
    }

    /**
     * @param assignDate Дата назначения. Свойство не может быть null.
     */
    public void setAssignDate(Date assignDate)
    {
        dirty(_assignDate, assignDate);
        _assignDate = assignDate;
    }

    /**
     * @return Назначена с даты. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Назначена с даты. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Назначена по дату.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Назначена по дату.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Учитывать при расчете Суммы выплаты суммарную ставку. Свойство не может быть null.
     */
    @NotNull
    public boolean isDependOnSummStaffRate()
    {
        return _dependOnSummStaffRate;
    }

    /**
     * @param dependOnSummStaffRate Учитывать при расчете Суммы выплаты суммарную ставку. Свойство не может быть null.
     */
    public void setDependOnSummStaffRate(boolean dependOnSummStaffRate)
    {
        dirty(_dependOnSummStaffRate, dependOnSummStaffRate);
        _dependOnSummStaffRate = dependOnSummStaffRate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeBonusGen)
        {
            setExtract(((EmployeeBonus)another).getExtract());
            setEntity(((EmployeeBonus)another).getEntity());
            setPayment(((EmployeeBonus)another).getPayment());
            setValue(((EmployeeBonus)another).getValue());
            setFinancingSource(((EmployeeBonus)another).getFinancingSource());
            setFinancingSourceItem(((EmployeeBonus)another).getFinancingSourceItem());
            setAssignDate(((EmployeeBonus)another).getAssignDate());
            setBeginDate(((EmployeeBonus)another).getBeginDate());
            setEndDate(((EmployeeBonus)another).getEndDate());
            setPriority(((EmployeeBonus)another).getPriority());
            setDependOnSummStaffRate(((EmployeeBonus)another).isDependOnSummStaffRate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeBonusGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeBonus.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeBonus();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "entity":
                    return obj.getEntity();
                case "payment":
                    return obj.getPayment();
                case "value":
                    return obj.getValue();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
                case "assignDate":
                    return obj.getAssignDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "priority":
                    return obj.getPriority();
                case "dependOnSummStaffRate":
                    return obj.isDependOnSummStaffRate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractEmployeeExtract) value);
                    return;
                case "entity":
                    obj.setEntity((EmployeePayment) value);
                    return;
                case "payment":
                    obj.setPayment((Payment) value);
                    return;
                case "value":
                    obj.setValue((Double) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
                case "assignDate":
                    obj.setAssignDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "dependOnSummStaffRate":
                    obj.setDependOnSummStaffRate((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "entity":
                        return true;
                case "payment":
                        return true;
                case "value":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
                case "assignDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "priority":
                        return true;
                case "dependOnSummStaffRate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "entity":
                    return true;
                case "payment":
                    return true;
                case "value":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
                case "assignDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "priority":
                    return true;
                case "dependOnSummStaffRate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return AbstractEmployeeExtract.class;
                case "entity":
                    return EmployeePayment.class;
                case "payment":
                    return Payment.class;
                case "value":
                    return Double.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
                case "assignDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "priority":
                    return Integer.class;
                case "dependOnSummStaffRate":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeBonus> _dslPath = new Path<EmployeeBonus>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeBonus");
    }
            

    /**
     * @return Выписка по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getExtract()
     */
    public static AbstractEmployeeExtract.Path<AbstractEmployeeExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Выплата сотрудника, созданная по выписке.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getEntity()
     */
    public static EmployeePayment.Path<EmployeePayment> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getPayment()
     */
    public static Payment.Path<Payment> payment()
    {
        return _dslPath.payment();
    }

    /**
     * @return Сумма выплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getValue()
     */
    public static PropertyPath<Double> value()
    {
        return _dslPath.value();
    }

    /**
     * @return Источник выплаты.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник выплаты (детально).
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    /**
     * @return Дата назначения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getAssignDate()
     */
    public static PropertyPath<Date> assignDate()
    {
        return _dslPath.assignDate();
    }

    /**
     * @return Назначена с даты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Назначена по дату.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Учитывать при расчете Суммы выплаты суммарную ставку. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#isDependOnSummStaffRate()
     */
    public static PropertyPath<Boolean> dependOnSummStaffRate()
    {
        return _dslPath.dependOnSummStaffRate();
    }

    public static class Path<E extends EmployeeBonus> extends EntityPath<E>
    {
        private AbstractEmployeeExtract.Path<AbstractEmployeeExtract> _extract;
        private EmployeePayment.Path<EmployeePayment> _entity;
        private Payment.Path<Payment> _payment;
        private PropertyPath<Double> _value;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;
        private PropertyPath<Date> _assignDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Boolean> _dependOnSummStaffRate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getExtract()
     */
        public AbstractEmployeeExtract.Path<AbstractEmployeeExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractEmployeeExtract.Path<AbstractEmployeeExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Выплата сотрудника, созданная по выписке.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getEntity()
     */
        public EmployeePayment.Path<EmployeePayment> entity()
        {
            if(_entity == null )
                _entity = new EmployeePayment.Path<EmployeePayment>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Выплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getPayment()
     */
        public Payment.Path<Payment> payment()
        {
            if(_payment == null )
                _payment = new Payment.Path<Payment>(L_PAYMENT, this);
            return _payment;
        }

    /**
     * @return Сумма выплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getValue()
     */
        public PropertyPath<Double> value()
        {
            if(_value == null )
                _value = new PropertyPath<Double>(EmployeeBonusGen.P_VALUE, this);
            return _value;
        }

    /**
     * @return Источник выплаты.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник выплаты (детально).
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

    /**
     * @return Дата назначения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getAssignDate()
     */
        public PropertyPath<Date> assignDate()
        {
            if(_assignDate == null )
                _assignDate = new PropertyPath<Date>(EmployeeBonusGen.P_ASSIGN_DATE, this);
            return _assignDate;
        }

    /**
     * @return Назначена с даты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeBonusGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Назначена по дату.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeBonusGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(EmployeeBonusGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Учитывать при расчете Суммы выплаты суммарную ставку. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeBonus#isDependOnSummStaffRate()
     */
        public PropertyPath<Boolean> dependOnSummStaffRate()
        {
            if(_dependOnSummStaffRate == null )
                _dependOnSummStaffRate = new PropertyPath<Boolean>(EmployeeBonusGen.P_DEPEND_ON_SUMM_STAFF_RATE, this);
            return _dependOnSummStaffRate;
        }

        public Class getEntityClass()
        {
            return EmployeeBonus.class;
        }

        public String getEntityName()
        {
            return "employeeBonus";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
