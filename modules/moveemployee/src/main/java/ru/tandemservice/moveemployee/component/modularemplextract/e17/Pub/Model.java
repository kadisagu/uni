/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e17.Pub;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 09.09.2011
 */
public class Model extends ModularEmployeeExtractPubModel<RevocationFromHolidayExtract>
{
    String revocationPeriodStr; //строка Период отзыва
    String _providePeriodsStr; //строка Частей отпуска

    //Getters & Setters

    public String getProvidePeriodsStr()
    {
        return _providePeriodsStr;
    }

    public void setProvidePeriodsStr(String providePeriodsStr)
    {
        _providePeriodsStr = providePeriodsStr;
    }

    public String getRevocationPeriodStr()
    {
        return revocationPeriodStr;
    }

    public void setRevocationPeriodStr(String revocationPeriodStr)
    {
        this.revocationPeriodStr = revocationPeriodStr;
    }
}