package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь причин приказов с текстовыми параграфами
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeReasonToTextParagraphRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel";
    public static final String ENTITY_NAME = "employeeReasonToTextParagraphRel";
    public static final int VERSION_HASH = 1932621544;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_ORDER_REASON = "employeeOrderReason";
    public static final String P_TEXT_PARAGRAPH = "textParagraph";

    private EmployeeOrderReasons _employeeOrderReason;     // Причина приказа по кадрам
    private String _textParagraph;     // Текстовый параграф

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EmployeeOrderReasons getEmployeeOrderReason()
    {
        return _employeeOrderReason;
    }

    /**
     * @param employeeOrderReason Причина приказа по кадрам. Свойство не может быть null и должно быть уникальным.
     */
    public void setEmployeeOrderReason(EmployeeOrderReasons employeeOrderReason)
    {
        dirty(_employeeOrderReason, employeeOrderReason);
        _employeeOrderReason = employeeOrderReason;
    }

    /**
     * @return Текстовый параграф.
     */
    public String getTextParagraph()
    {
        return _textParagraph;
    }

    /**
     * @param textParagraph Текстовый параграф.
     */
    public void setTextParagraph(String textParagraph)
    {
        dirty(_textParagraph, textParagraph);
        _textParagraph = textParagraph;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeReasonToTextParagraphRelGen)
        {
            setEmployeeOrderReason(((EmployeeReasonToTextParagraphRel)another).getEmployeeOrderReason());
            setTextParagraph(((EmployeeReasonToTextParagraphRel)another).getTextParagraph());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeReasonToTextParagraphRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeReasonToTextParagraphRel.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeReasonToTextParagraphRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeOrderReason":
                    return obj.getEmployeeOrderReason();
                case "textParagraph":
                    return obj.getTextParagraph();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeOrderReason":
                    obj.setEmployeeOrderReason((EmployeeOrderReasons) value);
                    return;
                case "textParagraph":
                    obj.setTextParagraph((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeOrderReason":
                        return true;
                case "textParagraph":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeOrderReason":
                    return true;
                case "textParagraph":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeOrderReason":
                    return EmployeeOrderReasons.class;
                case "textParagraph":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeReasonToTextParagraphRel> _dslPath = new Path<EmployeeReasonToTextParagraphRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeReasonToTextParagraphRel");
    }
            

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel#getEmployeeOrderReason()
     */
    public static EmployeeOrderReasons.Path<EmployeeOrderReasons> employeeOrderReason()
    {
        return _dslPath.employeeOrderReason();
    }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel#getTextParagraph()
     */
    public static PropertyPath<String> textParagraph()
    {
        return _dslPath.textParagraph();
    }

    public static class Path<E extends EmployeeReasonToTextParagraphRel> extends EntityPath<E>
    {
        private EmployeeOrderReasons.Path<EmployeeOrderReasons> _employeeOrderReason;
        private PropertyPath<String> _textParagraph;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel#getEmployeeOrderReason()
     */
        public EmployeeOrderReasons.Path<EmployeeOrderReasons> employeeOrderReason()
        {
            if(_employeeOrderReason == null )
                _employeeOrderReason = new EmployeeOrderReasons.Path<EmployeeOrderReasons>(L_EMPLOYEE_ORDER_REASON, this);
            return _employeeOrderReason;
        }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel#getTextParagraph()
     */
        public PropertyPath<String> textParagraph()
        {
            if(_textParagraph == null )
                _textParagraph = new PropertyPath<String>(EmployeeReasonToTextParagraphRelGen.P_TEXT_PARAGRAPH, this);
            return _textParagraph;
        }

        public Class getEntityClass()
        {
            return EmployeeReasonToTextParagraphRel.class;
        }

        public String getEntityName()
        {
            return "employeeReasonToTextParagraphRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
