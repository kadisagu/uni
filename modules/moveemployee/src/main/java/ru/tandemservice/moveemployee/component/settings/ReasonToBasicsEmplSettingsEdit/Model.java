/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.settings.ReasonToBasicsEmplSettingsEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;

/**
 * @author alikhanov
 * @since 17.08.2010
 */
@Input({@Bind(key = "employeeOrderReasonsId")})
public class Model
{
    private Long _employeeOrderReasonsId;
    private EmployeeOrderReasons _reason;

    private IMultiSelectModel _employeeBasicsListModel;
    private List<EmployeeOrderBasics> _selectedBasicsList;

    public Long getEmployeeOrderReasonsId()
    {
        return _employeeOrderReasonsId;
    }

    public void setEmployeeOrderReasonsId(Long employeeOrderReasonsId)
    {
        _employeeOrderReasonsId = employeeOrderReasonsId;
    }

    public EmployeeOrderReasons getReason()
    {
        return _reason;
    }

    public void setReason(EmployeeOrderReasons reason)
    {
        _reason = reason;
    }

    public IMultiSelectModel getEmployeeBasicsListModel()
    {
        return _employeeBasicsListModel;
    }

    public void setEmployeeBasicsListModel(IMultiSelectModel employeeBasicsListModel)
    {
        _employeeBasicsListModel = employeeBasicsListModel;
    }

    public List<EmployeeOrderBasics> getSelectedBasicsList()
    {
        return _selectedBasicsList;
    }

    public void setSelectedBasicsList(List<EmployeeOrderBasics> selectedBasicsList)
    {
        _selectedBasicsList = selectedBasicsList;
    }
}
