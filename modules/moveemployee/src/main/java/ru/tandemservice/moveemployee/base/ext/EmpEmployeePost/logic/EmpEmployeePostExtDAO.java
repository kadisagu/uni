/**
 *$Id$
 */
package ru.tandemservice.moveemployee.base.ext.EmpEmployeePost.logic;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.uniemp.base.bo.EmpEmployeePost.logic.EmpEmployeePostDAO;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemFake;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Create by ashaburov
 * Date 31.05.12
 */
public class EmpEmployeePostExtDAO extends EmpEmployeePostDAO
{
    @Override
    public RtfInjectModifier preparePersonCardModifier(EmployeePost employeePost, Date printDate)
    {
        RtfInjectModifier modifier = super.preparePersonCardModifier(employeePost, printDate);

        String emptyString = "";

        // приказ об увольнении
        // поднимаем последний приказ "об увольнении"
        DQLSelectBuilder extractBuilder = new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, "b").column("b");
        extractBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(AbstractEmployeeExtract.entity().fromAlias("b")), employeePost));
        extractBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(AbstractEmployeeExtract.committed().fromAlias("b")), true));
        IDQLExpression modular = DQLExpressions.instanceOf("b", DismissalExtract.class);
        IDQLExpression single = DQLExpressions.instanceOf("b", VoluntaryTerminationSExtract.class);
        IDQLExpression list = DQLExpressions.instanceOf("b", EmploymentDismissalEmplListExtract.class);
        extractBuilder.where(DQLExpressions.or(modular, single, list));
        extractBuilder.order(DQLExpressions.property(AbstractEmployeeExtract.paragraph().order().commitDate().fromAlias("b")), OrderDirection.desc);

        List<AbstractEmployeeExtract> extractList = extractBuilder.createStatement(getSession()).list();

        if (!extractList.isEmpty())
        {
            AbstractEmployeeExtract extract = extractList.get(0);
            if (extract instanceof DismissalExtract)
                modifier.put("lawArticleitem", ((DismissalExtract) extract).getDismissalReason().getLawItem());
            else if (extract instanceof VoluntaryTerminationSExtract)
                modifier.put("lawArticleitem", ((VoluntaryTerminationSExtract) extract).getDismissReason().getLawItem());
            else if (extract instanceof EmploymentDismissalEmplListExtract)
                modifier.put("lawArticleitem", ((EmploymentDismissalEmplListExtract) extract).getEmployeeDismissReasons().getLawItem());

            modifier.put("disOrderNumber", extract.getParagraph().getOrder().getNumber());
            modifier.put("disOrderDay", String.valueOf(CoreDateUtils.getDayOfMonth(extract.getParagraph().getOrder().getCommitDate())));
            modifier.put("disOrderMonthStr", RussianDateFormatUtils.getMonthName(extract.getParagraph().getOrder().getCommitDate(), false));
            modifier.put("disOrderYr", String.valueOf(CoreDateUtils.getYear(extract.getParagraph().getOrder().getCommitDate())).substring(2, 4));
        }
        else
        {
            modifier.put("lawArticleitem", emptyString);
            modifier.put("disOrderNumber", emptyString);
            modifier.put("disOrderDay", emptyString);
            modifier.put("disOrderMonthStr", emptyString);
            modifier.put("disOrderYr", emptyString);
        }

        return modifier;
    }

    @Override
    public RtfTableModifier preparePersonCardTableModifier(EmployeePost employeePost, Date printDate)
    {
        RtfTableModifier tableModifier = super.preparePersonCardTableModifier(employeePost, printDate);

        DateFormatter dateFormatter = DateFormatter.DEFAULT_DATE_FORMATTER;
        DoubleFormatter doubleFormatter = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS;
        String emptyString = "";

        // Данные истории должностей сотрудника
        // переопределяем метку, т.к. в uniemp нет доступа к Выпискам
        DQLSelectBuilder empHistBuilder = new DQLSelectBuilder().fromEntity(EmploymentHistoryItemBase.class, "b").column("b");
        empHistBuilder.where(DQLExpressions.not(DQLExpressions.instanceOf("b", EmploymentHistoryItemFake.class)));
        empHistBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmploymentHistoryItemBase.employee().fromAlias("b")), employeePost.getEmployee()));
        empHistBuilder.order(DQLExpressions.property(EmploymentHistoryItemBase.assignDate().fromAlias("b")), OrderDirection.desc);

        List<EmploymentHistoryItemBase> empHistoryList = empHistBuilder.createStatement(getSession()).list();

        List<String[]> empHistoryLineList = new ArrayList<>();

        for (EmploymentHistoryItemBase item : empHistoryList)
        {
            String salary = emptyString;
            if (item.getExtractDate() != null && item.getExtractNumber() != null)
            {
                // поднимаем выписки приказов "о назначении" на сотрудника, которым должны были назначить сотрудника на данную должность
                DQLSelectBuilder extractBuilder = new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, "b").column("b");
                extractBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(AbstractEmployeeExtract.entity().employee().fromAlias("b")), employeePost.getEmployee()));
                extractBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(AbstractEmployeeExtract.committed().fromAlias("b")), true));
                extractBuilder.where(DQLExpressions.inDay(AbstractEmployeeExtract.paragraph().order().commitDate().fromAlias("b"), item.getExtractDate()));
                extractBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(AbstractEmployeeExtract.paragraph().order().number().fromAlias("b")), item.getExtractNumber()));
                IDQLExpression instanceOf1 = DQLExpressions.instanceOf("b", EmployeeAddEmplListExtract.class);
                IDQLExpression instanceOf2 = DQLExpressions.instanceOf("b", EmployeeAddExtract.class);
                IDQLExpression instanceOf3 = DQLExpressions.instanceOf("b", EmployeeAddSExtract.class);
                IDQLExpression instanceOf4 = DQLExpressions.instanceOf("b", EmployeePostAddExtract.class);
                IDQLExpression instanceOf5 = DQLExpressions.instanceOf("b", EmployeePostAddSExtract.class);
                IDQLExpression instanceOf6 = DQLExpressions.instanceOf("b", EmployeePostPPSAddExtract.class);
                IDQLExpression instanceOf7 = DQLExpressions.instanceOf("b", EmployeeTempAddExtract.class);
                IDQLExpression instanceOf8 = DQLExpressions.instanceOf("b", EmployeeTempAddSExtract.class);
                IDQLExpression instanceOf9 = DQLExpressions.instanceOf("b", EmployeeTransferEmplListExtract.class);
                IDQLExpression instanceOf10 = DQLExpressions.instanceOf("b", EmployeeTransferExtract.class);
                IDQLExpression instanceOf11 = DQLExpressions.instanceOf("b", EmployeeTransferSExtract.class);
                extractBuilder.where(DQLExpressions.or(instanceOf1, instanceOf2, instanceOf3, instanceOf4, instanceOf5, instanceOf6, instanceOf7, instanceOf8, instanceOf9, instanceOf10, instanceOf11));
                extractBuilder.order(DQLExpressions.property(AbstractEmployeeExtract.paragraph().order().commitDate().fromAlias("b")), OrderDirection.desc);

                IPostAssignExtract extract = extractBuilder.createStatement(getSession()).uniqueResult();
                salary = extract != null ? doubleFormatter.format(extract.getSalary()) : emptyString;
            }

            String[] line = new String[6];

            line[0] = dateFormatter.format(item.getAssignDate());
            line[1] = item.getOrgUnitTitle();
            line[2] = item.getPostTitle();
            line[3] = salary;
            line[4] = item.getBasics();
            line[5] = emptyString;

            empHistoryLineList.add(line);
        }

        while (empHistoryLineList.size() < 3)
        {
            String[] line = new String[]{"", "", ""};
            empHistoryLineList.add(line);
        }

        tableModifier.put("T2", empHistoryLineList.toArray(new String[empHistoryLineList.size()][6]));

        return tableModifier;
    }
}
