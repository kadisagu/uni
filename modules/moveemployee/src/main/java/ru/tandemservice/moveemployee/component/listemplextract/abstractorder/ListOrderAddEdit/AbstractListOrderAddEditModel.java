/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderAddEdit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
@Input( { @Bind(key = AbstractListOrderAddEditModel.ORDER_ID, binding = "orderId"), @Bind(key = AbstractListOrderAddEditModel.ORDER_TYPE_ID, binding = "orderTypeId") })
public abstract class AbstractListOrderAddEditModel<T extends EmployeeListOrder>
{
    public static final String ORDER_ID = "orderId";
    public static final String ORDER_TYPE_ID = "orderTypeId";

    private T _order;
    private Long _orderId;
    private Long _orderTypeId;
    private ISelectModel _reasonListModel;
    private ISelectModel _basicListModel;
    private List<EmployeeOrderBasics> _basicList = new ArrayList<EmployeeOrderBasics>();
    private String _fieldsPage;
    private String _commentsPage;

    private EmployeeOrderBasics _currentBasic;
    private Map<Long, String> _currentBasicMap = new HashMap<Long, String>();

    public boolean isNeedComments()
    {
        for (EmployeeOrderBasics basic : _basicList)
            if (basic.isCommentable()) return true;
        return false;
    }

    public String getCurrentBasicTitle()
    {
        return _currentBasicMap.get(_currentBasic.getId());
    }

    public void setCurrentBasicTitle(String title)
    {
        _currentBasicMap.put(_currentBasic.getId(), title);
    }

    public String getCurrentBasicId()
    {
        return "basicId_" + _currentBasic.getId();
    }


    // Getters & Setters

    public T getOrder()
    {
        return _order;
    }

    public void setOrder(T order)
    {
        _order = order;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public Long getOrderTypeId()
    {
        return _orderTypeId;
    }

    public void setOrderTypeId(Long orderTypeId)
    {
        _orderTypeId = orderTypeId;
    }

    public ISelectModel getReasonListModel()
    {
        return _reasonListModel;
    }

    public void setReasonListModel(ISelectModel reasonListModel)
    {
        _reasonListModel = reasonListModel;
    }

    public ISelectModel getBasicListModel()
    {
        return _basicListModel;
    }

    public void setBasicListModel(ISelectModel basicListModel)
    {
        _basicListModel = basicListModel;
    }

    public List<EmployeeOrderBasics> getBasicList()
    {
        return _basicList;
    }

    public void setBasicList(List<EmployeeOrderBasics> basicList)
    {
        _basicList = basicList;
    }

    public String getFieldsPage()
    {
        return _fieldsPage;
    }

    public void setFieldsPage(String fieldsPage)
    {
        _fieldsPage = fieldsPage;
    }

    public String getCommentsPage()
    {
        return _commentsPage;
    }

    public void setCommentsPage(String commentsPage)
    {
        this._commentsPage = commentsPage;
    }

    public EmployeeOrderBasics getCurrentBasic()
    {
        return _currentBasic;
    }

    public void setCurrentBasic(EmployeeOrderBasics currentBasic)
    {
        _currentBasic = currentBasic;
    }

    public Map<Long, String> getCurrentBasicMap()
    {
        return _currentBasicMap;
    }

    public void setCurrentBasicMap(Map<Long, String> currentBasicMap)
    {
        _currentBasicMap = currentBasicMap;
    }
}