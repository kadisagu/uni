package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь ставок в приказе и элементов штатной расстановки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FinancingSourceDetailsToAllocItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem";
    public static final String ENTITY_NAME = "financingSourceDetailsToAllocItem";
    public static final int VERSION_HASH = 1103658322;
    private static IEntityMeta ENTITY_META;

    public static final String L_FINANCING_SOURCE_DETAILS = "financingSourceDetails";
    public static final String L_CHOSE_STAFF_LIST_ALLOCATION_ITEM = "choseStaffListAllocationItem";
    public static final String L_CREATE_STAFF_LIST_ALLOCATION_ITEM = "createStaffListAllocationItem";
    public static final String P_HAS_NEW_ALLOC_ITEM = "hasNewAllocItem";
    public static final String P_HAS_BEEN_SELECTED = "hasBeenSelected";
    public static final String P_STAFF_RATE_HISTORY = "staffRateHistory";
    public static final String L_EMPLOYEE_POST_HISTORY = "employeePostHistory";

    private FinancingSourceDetails _financingSourceDetails;     // Элемент разбивки ставки сотрудника по источникам финансирования
    private StaffListAllocationItem _choseStaffListAllocationItem;     // Выбранная ставка штатной расстановки
    private StaffListAllocationItem _createStaffListAllocationItem;     // Созданная ставка штатной расстановки
    private boolean _hasNewAllocItem;     // Выбрана ли новая ставка
    private boolean _hasBeenSelected;     // Ставка ШР уже выбрана в предыдущей выписке списочного приказа
    private Double _staffRateHistory;     // Кол-во штатных единиц из выбранной ставки штатной расстановки
    private EmployeePost _employeePostHistory;     // Сотрудник на которого ссылалась выбранная ставка штатной расстановки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент разбивки ставки сотрудника по источникам финансирования. Свойство не может быть null.
     */
    @NotNull
    public FinancingSourceDetails getFinancingSourceDetails()
    {
        return _financingSourceDetails;
    }

    /**
     * @param financingSourceDetails Элемент разбивки ставки сотрудника по источникам финансирования. Свойство не может быть null.
     */
    public void setFinancingSourceDetails(FinancingSourceDetails financingSourceDetails)
    {
        dirty(_financingSourceDetails, financingSourceDetails);
        _financingSourceDetails = financingSourceDetails;
    }

    /**
     * @return Выбранная ставка штатной расстановки.
     */
    public StaffListAllocationItem getChoseStaffListAllocationItem()
    {
        return _choseStaffListAllocationItem;
    }

    /**
     * @param choseStaffListAllocationItem Выбранная ставка штатной расстановки.
     */
    public void setChoseStaffListAllocationItem(StaffListAllocationItem choseStaffListAllocationItem)
    {
        dirty(_choseStaffListAllocationItem, choseStaffListAllocationItem);
        _choseStaffListAllocationItem = choseStaffListAllocationItem;
    }

    /**
     * @return Созданная ставка штатной расстановки.
     */
    public StaffListAllocationItem getCreateStaffListAllocationItem()
    {
        return _createStaffListAllocationItem;
    }

    /**
     * @param createStaffListAllocationItem Созданная ставка штатной расстановки.
     */
    public void setCreateStaffListAllocationItem(StaffListAllocationItem createStaffListAllocationItem)
    {
        dirty(_createStaffListAllocationItem, createStaffListAllocationItem);
        _createStaffListAllocationItem = createStaffListAllocationItem;
    }

    /**
     * @return Выбрана ли новая ставка. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasNewAllocItem()
    {
        return _hasNewAllocItem;
    }

    /**
     * @param hasNewAllocItem Выбрана ли новая ставка. Свойство не может быть null.
     */
    public void setHasNewAllocItem(boolean hasNewAllocItem)
    {
        dirty(_hasNewAllocItem, hasNewAllocItem);
        _hasNewAllocItem = hasNewAllocItem;
    }

    /**
     * @return Ставка ШР уже выбрана в предыдущей выписке списочного приказа. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasBeenSelected()
    {
        return _hasBeenSelected;
    }

    /**
     * @param hasBeenSelected Ставка ШР уже выбрана в предыдущей выписке списочного приказа. Свойство не может быть null.
     */
    public void setHasBeenSelected(boolean hasBeenSelected)
    {
        dirty(_hasBeenSelected, hasBeenSelected);
        _hasBeenSelected = hasBeenSelected;
    }

    /**
     * @return Кол-во штатных единиц из выбранной ставки штатной расстановки.
     */
    public Double getStaffRateHistory()
    {
        return _staffRateHistory;
    }

    /**
     * @param staffRateHistory Кол-во штатных единиц из выбранной ставки штатной расстановки.
     */
    public void setStaffRateHistory(Double staffRateHistory)
    {
        dirty(_staffRateHistory, staffRateHistory);
        _staffRateHistory = staffRateHistory;
    }

    /**
     * @return Сотрудник на которого ссылалась выбранная ставка штатной расстановки.
     */
    public EmployeePost getEmployeePostHistory()
    {
        return _employeePostHistory;
    }

    /**
     * @param employeePostHistory Сотрудник на которого ссылалась выбранная ставка штатной расстановки.
     */
    public void setEmployeePostHistory(EmployeePost employeePostHistory)
    {
        dirty(_employeePostHistory, employeePostHistory);
        _employeePostHistory = employeePostHistory;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FinancingSourceDetailsToAllocItemGen)
        {
            setFinancingSourceDetails(((FinancingSourceDetailsToAllocItem)another).getFinancingSourceDetails());
            setChoseStaffListAllocationItem(((FinancingSourceDetailsToAllocItem)another).getChoseStaffListAllocationItem());
            setCreateStaffListAllocationItem(((FinancingSourceDetailsToAllocItem)another).getCreateStaffListAllocationItem());
            setHasNewAllocItem(((FinancingSourceDetailsToAllocItem)another).isHasNewAllocItem());
            setHasBeenSelected(((FinancingSourceDetailsToAllocItem)another).isHasBeenSelected());
            setStaffRateHistory(((FinancingSourceDetailsToAllocItem)another).getStaffRateHistory());
            setEmployeePostHistory(((FinancingSourceDetailsToAllocItem)another).getEmployeePostHistory());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FinancingSourceDetailsToAllocItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FinancingSourceDetailsToAllocItem.class;
        }

        public T newInstance()
        {
            return (T) new FinancingSourceDetailsToAllocItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "financingSourceDetails":
                    return obj.getFinancingSourceDetails();
                case "choseStaffListAllocationItem":
                    return obj.getChoseStaffListAllocationItem();
                case "createStaffListAllocationItem":
                    return obj.getCreateStaffListAllocationItem();
                case "hasNewAllocItem":
                    return obj.isHasNewAllocItem();
                case "hasBeenSelected":
                    return obj.isHasBeenSelected();
                case "staffRateHistory":
                    return obj.getStaffRateHistory();
                case "employeePostHistory":
                    return obj.getEmployeePostHistory();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "financingSourceDetails":
                    obj.setFinancingSourceDetails((FinancingSourceDetails) value);
                    return;
                case "choseStaffListAllocationItem":
                    obj.setChoseStaffListAllocationItem((StaffListAllocationItem) value);
                    return;
                case "createStaffListAllocationItem":
                    obj.setCreateStaffListAllocationItem((StaffListAllocationItem) value);
                    return;
                case "hasNewAllocItem":
                    obj.setHasNewAllocItem((Boolean) value);
                    return;
                case "hasBeenSelected":
                    obj.setHasBeenSelected((Boolean) value);
                    return;
                case "staffRateHistory":
                    obj.setStaffRateHistory((Double) value);
                    return;
                case "employeePostHistory":
                    obj.setEmployeePostHistory((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "financingSourceDetails":
                        return true;
                case "choseStaffListAllocationItem":
                        return true;
                case "createStaffListAllocationItem":
                        return true;
                case "hasNewAllocItem":
                        return true;
                case "hasBeenSelected":
                        return true;
                case "staffRateHistory":
                        return true;
                case "employeePostHistory":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "financingSourceDetails":
                    return true;
                case "choseStaffListAllocationItem":
                    return true;
                case "createStaffListAllocationItem":
                    return true;
                case "hasNewAllocItem":
                    return true;
                case "hasBeenSelected":
                    return true;
                case "staffRateHistory":
                    return true;
                case "employeePostHistory":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "financingSourceDetails":
                    return FinancingSourceDetails.class;
                case "choseStaffListAllocationItem":
                    return StaffListAllocationItem.class;
                case "createStaffListAllocationItem":
                    return StaffListAllocationItem.class;
                case "hasNewAllocItem":
                    return Boolean.class;
                case "hasBeenSelected":
                    return Boolean.class;
                case "staffRateHistory":
                    return Double.class;
                case "employeePostHistory":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FinancingSourceDetailsToAllocItem> _dslPath = new Path<FinancingSourceDetailsToAllocItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FinancingSourceDetailsToAllocItem");
    }
            

    /**
     * @return Элемент разбивки ставки сотрудника по источникам финансирования. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getFinancingSourceDetails()
     */
    public static FinancingSourceDetails.Path<FinancingSourceDetails> financingSourceDetails()
    {
        return _dslPath.financingSourceDetails();
    }

    /**
     * @return Выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getChoseStaffListAllocationItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> choseStaffListAllocationItem()
    {
        return _dslPath.choseStaffListAllocationItem();
    }

    /**
     * @return Созданная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getCreateStaffListAllocationItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> createStaffListAllocationItem()
    {
        return _dslPath.createStaffListAllocationItem();
    }

    /**
     * @return Выбрана ли новая ставка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#isHasNewAllocItem()
     */
    public static PropertyPath<Boolean> hasNewAllocItem()
    {
        return _dslPath.hasNewAllocItem();
    }

    /**
     * @return Ставка ШР уже выбрана в предыдущей выписке списочного приказа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#isHasBeenSelected()
     */
    public static PropertyPath<Boolean> hasBeenSelected()
    {
        return _dslPath.hasBeenSelected();
    }

    /**
     * @return Кол-во штатных единиц из выбранной ставки штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getStaffRateHistory()
     */
    public static PropertyPath<Double> staffRateHistory()
    {
        return _dslPath.staffRateHistory();
    }

    /**
     * @return Сотрудник на которого ссылалась выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getEmployeePostHistory()
     */
    public static EmployeePost.Path<EmployeePost> employeePostHistory()
    {
        return _dslPath.employeePostHistory();
    }

    public static class Path<E extends FinancingSourceDetailsToAllocItem> extends EntityPath<E>
    {
        private FinancingSourceDetails.Path<FinancingSourceDetails> _financingSourceDetails;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _choseStaffListAllocationItem;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _createStaffListAllocationItem;
        private PropertyPath<Boolean> _hasNewAllocItem;
        private PropertyPath<Boolean> _hasBeenSelected;
        private PropertyPath<Double> _staffRateHistory;
        private EmployeePost.Path<EmployeePost> _employeePostHistory;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент разбивки ставки сотрудника по источникам финансирования. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getFinancingSourceDetails()
     */
        public FinancingSourceDetails.Path<FinancingSourceDetails> financingSourceDetails()
        {
            if(_financingSourceDetails == null )
                _financingSourceDetails = new FinancingSourceDetails.Path<FinancingSourceDetails>(L_FINANCING_SOURCE_DETAILS, this);
            return _financingSourceDetails;
        }

    /**
     * @return Выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getChoseStaffListAllocationItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> choseStaffListAllocationItem()
        {
            if(_choseStaffListAllocationItem == null )
                _choseStaffListAllocationItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_CHOSE_STAFF_LIST_ALLOCATION_ITEM, this);
            return _choseStaffListAllocationItem;
        }

    /**
     * @return Созданная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getCreateStaffListAllocationItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> createStaffListAllocationItem()
        {
            if(_createStaffListAllocationItem == null )
                _createStaffListAllocationItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_CREATE_STAFF_LIST_ALLOCATION_ITEM, this);
            return _createStaffListAllocationItem;
        }

    /**
     * @return Выбрана ли новая ставка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#isHasNewAllocItem()
     */
        public PropertyPath<Boolean> hasNewAllocItem()
        {
            if(_hasNewAllocItem == null )
                _hasNewAllocItem = new PropertyPath<Boolean>(FinancingSourceDetailsToAllocItemGen.P_HAS_NEW_ALLOC_ITEM, this);
            return _hasNewAllocItem;
        }

    /**
     * @return Ставка ШР уже выбрана в предыдущей выписке списочного приказа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#isHasBeenSelected()
     */
        public PropertyPath<Boolean> hasBeenSelected()
        {
            if(_hasBeenSelected == null )
                _hasBeenSelected = new PropertyPath<Boolean>(FinancingSourceDetailsToAllocItemGen.P_HAS_BEEN_SELECTED, this);
            return _hasBeenSelected;
        }

    /**
     * @return Кол-во штатных единиц из выбранной ставки штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getStaffRateHistory()
     */
        public PropertyPath<Double> staffRateHistory()
        {
            if(_staffRateHistory == null )
                _staffRateHistory = new PropertyPath<Double>(FinancingSourceDetailsToAllocItemGen.P_STAFF_RATE_HISTORY, this);
            return _staffRateHistory;
        }

    /**
     * @return Сотрудник на которого ссылалась выбранная ставка штатной расстановки.
     * @see ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem#getEmployeePostHistory()
     */
        public EmployeePost.Path<EmployeePost> employeePostHistory()
        {
            if(_employeePostHistory == null )
                _employeePostHistory = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST_HISTORY, this);
            return _employeePostHistory;
        }

        public Class getEntityClass()
        {
            return FinancingSourceDetailsToAllocItem.class;
        }

        public String getEntityName()
        {
            return "financingSourceDetailsToAllocItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
