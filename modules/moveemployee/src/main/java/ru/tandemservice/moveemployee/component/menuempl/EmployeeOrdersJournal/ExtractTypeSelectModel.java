package ru.tandemservice.moveemployee.component.menuempl.EmployeeOrdersJournal;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.list.IViewSelectValueStyle;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


class ExtractTypeSelectModel extends BaseSelectModel implements IMultiSelectModel, IHierarchicalSelectModel
{

    private final static String MAIN_ALIAS = "extractType";
    private Model _model;

    ExtractTypeSelectModel(Model model)
    {
        super(EmployeeExtractType.P_TITLE);
        _model = model;
    }

    private DQLSelectBuilder createBuilder(String filter, Set primaryKeys)
    {
        DQLSelectBuilder extractBuilder = new DQLSelectBuilder().fromEntity(EmployeeExtractType.class, MAIN_ALIAS)
                .where(eq(property(MAIN_ALIAS, EmployeeExtractType.active()), value(true)));

        if (CollectionUtils.isNotEmpty(primaryKeys)) {
                extractBuilder.where(in(property(MAIN_ALIAS, EmployeeExtractType.id()), primaryKeys));
        }
        if (StringUtils.isNotEmpty(filter))
            extractBuilder.where(or(
                    likeUpper(property(MAIN_ALIAS, EmployeeExtractType.title()), value(CoreStringUtils.escapeLike(filter))),
                    isNull(property(MAIN_ALIAS, EmployeeExtractType.parent())))
            );

        List<EmployeeExtractType> orderCategories = _model.getSettings().get("orderCategory");
        if (CollectionUtils.isNotEmpty(orderCategories))
        {
            extractBuilder.where(or(
                    in(property(MAIN_ALIAS, EmployeeExtractType.parent()), orderCategories),
                    in(property(MAIN_ALIAS, EmployeeExtractType.parent().parent()), orderCategories)
            ));
            extractBuilder.where(or(exists(AbstractEmployeeExtract.class, AbstractEmployeeExtract.type().s(), property(MAIN_ALIAS),
                    AbstractEmployeeExtract.state().code().s(), ExtractStatesCodes.FINISHED)));
        }
        else {
            extractBuilder.where(or(
                    exists(AbstractEmployeeExtract.class, AbstractEmployeeExtract.type().s(), property(MAIN_ALIAS),
                            AbstractEmployeeExtract.state().code().s(), ExtractStatesCodes.FINISHED),
                    isNull(property(MAIN_ALIAS, EmployeeExtractType.parent()))
            ));
        }

        return extractBuilder;
    }

    @Override
    public ListResult findValues(String s)
    {
        List<EmployeeExtractType> extractTypeList = UniDaoFacade.getCoreDao().getList(createBuilder(s, null));

        List<EmployeeExtractType> orderCategories = _model.getSettings().get("orderCategory");
        if (CollectionUtils.isNotEmpty(orderCategories))
            extractTypeList.addAll(orderCategories);
        Collections.sort(extractTypeList, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);

        return new ListResult<>(extractTypeList);
    }

    @Override
    public IViewSelectValueStyle getValueStyle(Object value)
    {
        EmployeeExtractType type = (EmployeeExtractType) value;

        if (type.getParent() != null) {
            return super.getValueStyle(value);
        }
        // Задизаблены рутовые и неактивные типы
        DefaultSelectValueStyle valueStyle = new DefaultSelectValueStyle();
        valueStyle.setDisabled(true);
        valueStyle.setRowStyle("background-color:#D2D2D2");
        return valueStyle;
    }


    @Override
    public int getLevel(Object value)
    {
        EmployeeExtractType item = (EmployeeExtractType) value;

        int level = 0;
        while (item.getParent() != null)
        {
            item = item.getParent();
            level++;
        }
        return level;
    }

    @Override
    public List getValues(Set primaryKeys)
    {
        return UniDaoFacade.getCoreDao().getList(createBuilder(null, primaryKeys));
    }

}