/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeModularOrderParagraphsSortRuleSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public class Model
{
    DynamicListDataSource<EmployeeModularOrderParagraphsSortRule> _dataSource;

    public DynamicListDataSource<EmployeeModularOrderParagraphsSortRule> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<EmployeeModularOrderParagraphsSortRule> dataSource)
    {
        _dataSource = dataSource;
    }
}