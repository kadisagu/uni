/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.list.EmployeeParagraphListOrderPrint;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeOrderTextRelation;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author ashaburov
 * Created on: 11.10.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder(getNotNull(ParagraphEmployeeListOrder.class, model.getOrder().getId()));

        String stateCode = model.getOrder().getState().getCode();

        // если приказ на согласовании, согласован или проведен, то печатная форма должна быть сохранена
        if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode))
        {
            // приказ проведен => печатная форма сохранена
            Criteria c = getSession().createCriteria(EmployeeOrderTextRelation.class);
            c.add(Restrictions.eq(EmployeeOrderTextRelation.L_ORDER, model.getOrder()));
            EmployeeOrderTextRelation rel = (EmployeeOrderTextRelation) c.uniqueResult();
			// В процессе деперсонализации БД связь могла быть удалена
            if (rel != null)
				model.setData(rel.getText());
			else
				prepareDefaultTemplate(model);
        }
        else
		{
            prepareDefaultTemplate(model);
        }
    }

	private void prepareDefaultTemplate(Model model)
	{
		model.setData(MoveEmployeeDaoFacade.getMoveEmployeeDao().getTemplate(model.getOrder().getType(), 1));
	}
}