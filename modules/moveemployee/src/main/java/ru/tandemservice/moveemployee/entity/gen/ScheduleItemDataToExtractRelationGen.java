package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь данных из удаляемых планируемых отпусков в выписке «О переносе ежегодного отпуска»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScheduleItemDataToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation";
    public static final String ENTITY_NAME = "scheduleItemDataToExtractRelation";
    public static final int VERSION_HASH = -1662054674;
    private static IEntityMeta ENTITY_META;

    public static final String L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT = "transferAnnualHolidayExtract";
    public static final String P_COMMENT = "comment";
    public static final String P_DAYS_AMOUNT = "daysAmount";
    public static final String P_FACT_DATE = "factDate";
    public static final String P_PLAN_DATE = "planDate";
    public static final String P_POSTPONE_BASIC = "postponeBasic";
    public static final String P_POSTPONE_DATE = "postponeDate";
    public static final String L_VACATION_SCHEDULE = "vacationSchedule";

    private TransferAnnualHolidayExtract _transferAnnualHolidayExtract;     // Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска
    private String _comment;     // Примечание
    private Integer _daysAmount;     // Количество календарных дней отпуска
    private Date _factDate;     // Фактическая дата отпуска
    private Date _planDate;     // Планируемая дата отпуска
    private String _postponeBasic;     // Основание для переноса отпуска
    private Date _postponeDate;     // Предполагаемая дата переноса отпуска
    private VacationSchedule _vacationSchedule;     // График отпусков

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     */
    @NotNull
    public TransferAnnualHolidayExtract getTransferAnnualHolidayExtract()
    {
        return _transferAnnualHolidayExtract;
    }

    /**
     * @param transferAnnualHolidayExtract Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     */
    public void setTransferAnnualHolidayExtract(TransferAnnualHolidayExtract transferAnnualHolidayExtract)
    {
        dirty(_transferAnnualHolidayExtract, transferAnnualHolidayExtract);
        _transferAnnualHolidayExtract = transferAnnualHolidayExtract;
    }

    /**
     * @return Примечание.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Примечание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Количество календарных дней отпуска.
     */
    public Integer getDaysAmount()
    {
        return _daysAmount;
    }

    /**
     * @param daysAmount Количество календарных дней отпуска.
     */
    public void setDaysAmount(Integer daysAmount)
    {
        dirty(_daysAmount, daysAmount);
        _daysAmount = daysAmount;
    }

    /**
     * @return Фактическая дата отпуска.
     */
    public Date getFactDate()
    {
        return _factDate;
    }

    /**
     * @param factDate Фактическая дата отпуска.
     */
    public void setFactDate(Date factDate)
    {
        dirty(_factDate, factDate);
        _factDate = factDate;
    }

    /**
     * @return Планируемая дата отпуска.
     */
    public Date getPlanDate()
    {
        return _planDate;
    }

    /**
     * @param planDate Планируемая дата отпуска.
     */
    public void setPlanDate(Date planDate)
    {
        dirty(_planDate, planDate);
        _planDate = planDate;
    }

    /**
     * @return Основание для переноса отпуска.
     */
    @Length(max=255)
    public String getPostponeBasic()
    {
        return _postponeBasic;
    }

    /**
     * @param postponeBasic Основание для переноса отпуска.
     */
    public void setPostponeBasic(String postponeBasic)
    {
        dirty(_postponeBasic, postponeBasic);
        _postponeBasic = postponeBasic;
    }

    /**
     * @return Предполагаемая дата переноса отпуска.
     */
    public Date getPostponeDate()
    {
        return _postponeDate;
    }

    /**
     * @param postponeDate Предполагаемая дата переноса отпуска.
     */
    public void setPostponeDate(Date postponeDate)
    {
        dirty(_postponeDate, postponeDate);
        _postponeDate = postponeDate;
    }

    /**
     * @return График отпусков.
     */
    public VacationSchedule getVacationSchedule()
    {
        return _vacationSchedule;
    }

    /**
     * @param vacationSchedule График отпусков.
     */
    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        dirty(_vacationSchedule, vacationSchedule);
        _vacationSchedule = vacationSchedule;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScheduleItemDataToExtractRelationGen)
        {
            setTransferAnnualHolidayExtract(((ScheduleItemDataToExtractRelation)another).getTransferAnnualHolidayExtract());
            setComment(((ScheduleItemDataToExtractRelation)another).getComment());
            setDaysAmount(((ScheduleItemDataToExtractRelation)another).getDaysAmount());
            setFactDate(((ScheduleItemDataToExtractRelation)another).getFactDate());
            setPlanDate(((ScheduleItemDataToExtractRelation)another).getPlanDate());
            setPostponeBasic(((ScheduleItemDataToExtractRelation)another).getPostponeBasic());
            setPostponeDate(((ScheduleItemDataToExtractRelation)another).getPostponeDate());
            setVacationSchedule(((ScheduleItemDataToExtractRelation)another).getVacationSchedule());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScheduleItemDataToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScheduleItemDataToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new ScheduleItemDataToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "transferAnnualHolidayExtract":
                    return obj.getTransferAnnualHolidayExtract();
                case "comment":
                    return obj.getComment();
                case "daysAmount":
                    return obj.getDaysAmount();
                case "factDate":
                    return obj.getFactDate();
                case "planDate":
                    return obj.getPlanDate();
                case "postponeBasic":
                    return obj.getPostponeBasic();
                case "postponeDate":
                    return obj.getPostponeDate();
                case "vacationSchedule":
                    return obj.getVacationSchedule();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "transferAnnualHolidayExtract":
                    obj.setTransferAnnualHolidayExtract((TransferAnnualHolidayExtract) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "daysAmount":
                    obj.setDaysAmount((Integer) value);
                    return;
                case "factDate":
                    obj.setFactDate((Date) value);
                    return;
                case "planDate":
                    obj.setPlanDate((Date) value);
                    return;
                case "postponeBasic":
                    obj.setPostponeBasic((String) value);
                    return;
                case "postponeDate":
                    obj.setPostponeDate((Date) value);
                    return;
                case "vacationSchedule":
                    obj.setVacationSchedule((VacationSchedule) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "transferAnnualHolidayExtract":
                        return true;
                case "comment":
                        return true;
                case "daysAmount":
                        return true;
                case "factDate":
                        return true;
                case "planDate":
                        return true;
                case "postponeBasic":
                        return true;
                case "postponeDate":
                        return true;
                case "vacationSchedule":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "transferAnnualHolidayExtract":
                    return true;
                case "comment":
                    return true;
                case "daysAmount":
                    return true;
                case "factDate":
                    return true;
                case "planDate":
                    return true;
                case "postponeBasic":
                    return true;
                case "postponeDate":
                    return true;
                case "vacationSchedule":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "transferAnnualHolidayExtract":
                    return TransferAnnualHolidayExtract.class;
                case "comment":
                    return String.class;
                case "daysAmount":
                    return Integer.class;
                case "factDate":
                    return Date.class;
                case "planDate":
                    return Date.class;
                case "postponeBasic":
                    return String.class;
                case "postponeDate":
                    return Date.class;
                case "vacationSchedule":
                    return VacationSchedule.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScheduleItemDataToExtractRelation> _dslPath = new Path<ScheduleItemDataToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScheduleItemDataToExtractRelation");
    }
            

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getTransferAnnualHolidayExtract()
     */
    public static TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> transferAnnualHolidayExtract()
    {
        return _dslPath.transferAnnualHolidayExtract();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Количество календарных дней отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getDaysAmount()
     */
    public static PropertyPath<Integer> daysAmount()
    {
        return _dslPath.daysAmount();
    }

    /**
     * @return Фактическая дата отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getFactDate()
     */
    public static PropertyPath<Date> factDate()
    {
        return _dslPath.factDate();
    }

    /**
     * @return Планируемая дата отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getPlanDate()
     */
    public static PropertyPath<Date> planDate()
    {
        return _dslPath.planDate();
    }

    /**
     * @return Основание для переноса отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getPostponeBasic()
     */
    public static PropertyPath<String> postponeBasic()
    {
        return _dslPath.postponeBasic();
    }

    /**
     * @return Предполагаемая дата переноса отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getPostponeDate()
     */
    public static PropertyPath<Date> postponeDate()
    {
        return _dslPath.postponeDate();
    }

    /**
     * @return График отпусков.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getVacationSchedule()
     */
    public static VacationSchedule.Path<VacationSchedule> vacationSchedule()
    {
        return _dslPath.vacationSchedule();
    }

    public static class Path<E extends ScheduleItemDataToExtractRelation> extends EntityPath<E>
    {
        private TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> _transferAnnualHolidayExtract;
        private PropertyPath<String> _comment;
        private PropertyPath<Integer> _daysAmount;
        private PropertyPath<Date> _factDate;
        private PropertyPath<Date> _planDate;
        private PropertyPath<String> _postponeBasic;
        private PropertyPath<Date> _postponeDate;
        private VacationSchedule.Path<VacationSchedule> _vacationSchedule;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getTransferAnnualHolidayExtract()
     */
        public TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract> transferAnnualHolidayExtract()
        {
            if(_transferAnnualHolidayExtract == null )
                _transferAnnualHolidayExtract = new TransferAnnualHolidayExtract.Path<TransferAnnualHolidayExtract>(L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, this);
            return _transferAnnualHolidayExtract;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(ScheduleItemDataToExtractRelationGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Количество календарных дней отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getDaysAmount()
     */
        public PropertyPath<Integer> daysAmount()
        {
            if(_daysAmount == null )
                _daysAmount = new PropertyPath<Integer>(ScheduleItemDataToExtractRelationGen.P_DAYS_AMOUNT, this);
            return _daysAmount;
        }

    /**
     * @return Фактическая дата отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getFactDate()
     */
        public PropertyPath<Date> factDate()
        {
            if(_factDate == null )
                _factDate = new PropertyPath<Date>(ScheduleItemDataToExtractRelationGen.P_FACT_DATE, this);
            return _factDate;
        }

    /**
     * @return Планируемая дата отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getPlanDate()
     */
        public PropertyPath<Date> planDate()
        {
            if(_planDate == null )
                _planDate = new PropertyPath<Date>(ScheduleItemDataToExtractRelationGen.P_PLAN_DATE, this);
            return _planDate;
        }

    /**
     * @return Основание для переноса отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getPostponeBasic()
     */
        public PropertyPath<String> postponeBasic()
        {
            if(_postponeBasic == null )
                _postponeBasic = new PropertyPath<String>(ScheduleItemDataToExtractRelationGen.P_POSTPONE_BASIC, this);
            return _postponeBasic;
        }

    /**
     * @return Предполагаемая дата переноса отпуска.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getPostponeDate()
     */
        public PropertyPath<Date> postponeDate()
        {
            if(_postponeDate == null )
                _postponeDate = new PropertyPath<Date>(ScheduleItemDataToExtractRelationGen.P_POSTPONE_DATE, this);
            return _postponeDate;
        }

    /**
     * @return График отпусков.
     * @see ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation#getVacationSchedule()
     */
        public VacationSchedule.Path<VacationSchedule> vacationSchedule()
        {
            if(_vacationSchedule == null )
                _vacationSchedule = new VacationSchedule.Path<VacationSchedule>(L_VACATION_SCHEDULE, this);
            return _vacationSchedule;
        }

        public Class getEntityClass()
        {
            return ScheduleItemDataToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "scheduleItemDataToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
