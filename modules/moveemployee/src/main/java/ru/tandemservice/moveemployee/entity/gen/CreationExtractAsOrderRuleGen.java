package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Правила формирования сборных приказов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CreationExtractAsOrderRuleGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule";
    public static final String ENTITY_NAME = "creationExtractAsOrderRule";
    public static final int VERSION_HASH = 1056897129;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_EXTRACT_TYPE = "employeeExtractType";
    public static final String P_EXTRACT_AS_ORDER = "extractAsOrder";
    public static final String P_EXTRACT_INDIVIDUAL = "extractIndividual";

    private EmployeeExtractType _employeeExtractType;     // Тип выписки по кадрам
    private boolean _extractAsOrder;     // Выписка формируется совместно с приказом
    private boolean _extractIndividual;     // Приказ формируется индивидуально

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип выписки по кадрам. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EmployeeExtractType getEmployeeExtractType()
    {
        return _employeeExtractType;
    }

    /**
     * @param employeeExtractType Тип выписки по кадрам. Свойство не может быть null и должно быть уникальным.
     */
    public void setEmployeeExtractType(EmployeeExtractType employeeExtractType)
    {
        dirty(_employeeExtractType, employeeExtractType);
        _employeeExtractType = employeeExtractType;
    }

    /**
     * @return Выписка формируется совместно с приказом. Свойство не может быть null.
     */
    @NotNull
    public boolean isExtractAsOrder()
    {
        return _extractAsOrder;
    }

    /**
     * @param extractAsOrder Выписка формируется совместно с приказом. Свойство не может быть null.
     */
    public void setExtractAsOrder(boolean extractAsOrder)
    {
        dirty(_extractAsOrder, extractAsOrder);
        _extractAsOrder = extractAsOrder;
    }

    /**
     * @return Приказ формируется индивидуально. Свойство не может быть null.
     */
    @NotNull
    public boolean isExtractIndividual()
    {
        return _extractIndividual;
    }

    /**
     * @param extractIndividual Приказ формируется индивидуально. Свойство не может быть null.
     */
    public void setExtractIndividual(boolean extractIndividual)
    {
        dirty(_extractIndividual, extractIndividual);
        _extractIndividual = extractIndividual;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CreationExtractAsOrderRuleGen)
        {
            setEmployeeExtractType(((CreationExtractAsOrderRule)another).getEmployeeExtractType());
            setExtractAsOrder(((CreationExtractAsOrderRule)another).isExtractAsOrder());
            setExtractIndividual(((CreationExtractAsOrderRule)another).isExtractIndividual());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CreationExtractAsOrderRuleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CreationExtractAsOrderRule.class;
        }

        public T newInstance()
        {
            return (T) new CreationExtractAsOrderRule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeExtractType":
                    return obj.getEmployeeExtractType();
                case "extractAsOrder":
                    return obj.isExtractAsOrder();
                case "extractIndividual":
                    return obj.isExtractIndividual();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeExtractType":
                    obj.setEmployeeExtractType((EmployeeExtractType) value);
                    return;
                case "extractAsOrder":
                    obj.setExtractAsOrder((Boolean) value);
                    return;
                case "extractIndividual":
                    obj.setExtractIndividual((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeExtractType":
                        return true;
                case "extractAsOrder":
                        return true;
                case "extractIndividual":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeExtractType":
                    return true;
                case "extractAsOrder":
                    return true;
                case "extractIndividual":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeExtractType":
                    return EmployeeExtractType.class;
                case "extractAsOrder":
                    return Boolean.class;
                case "extractIndividual":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CreationExtractAsOrderRule> _dslPath = new Path<CreationExtractAsOrderRule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CreationExtractAsOrderRule");
    }
            

    /**
     * @return Тип выписки по кадрам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule#getEmployeeExtractType()
     */
    public static EmployeeExtractType.Path<EmployeeExtractType> employeeExtractType()
    {
        return _dslPath.employeeExtractType();
    }

    /**
     * @return Выписка формируется совместно с приказом. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule#isExtractAsOrder()
     */
    public static PropertyPath<Boolean> extractAsOrder()
    {
        return _dslPath.extractAsOrder();
    }

    /**
     * @return Приказ формируется индивидуально. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule#isExtractIndividual()
     */
    public static PropertyPath<Boolean> extractIndividual()
    {
        return _dslPath.extractIndividual();
    }

    public static class Path<E extends CreationExtractAsOrderRule> extends EntityPath<E>
    {
        private EmployeeExtractType.Path<EmployeeExtractType> _employeeExtractType;
        private PropertyPath<Boolean> _extractAsOrder;
        private PropertyPath<Boolean> _extractIndividual;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип выписки по кадрам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule#getEmployeeExtractType()
     */
        public EmployeeExtractType.Path<EmployeeExtractType> employeeExtractType()
        {
            if(_employeeExtractType == null )
                _employeeExtractType = new EmployeeExtractType.Path<EmployeeExtractType>(L_EMPLOYEE_EXTRACT_TYPE, this);
            return _employeeExtractType;
        }

    /**
     * @return Выписка формируется совместно с приказом. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule#isExtractAsOrder()
     */
        public PropertyPath<Boolean> extractAsOrder()
        {
            if(_extractAsOrder == null )
                _extractAsOrder = new PropertyPath<Boolean>(CreationExtractAsOrderRuleGen.P_EXTRACT_AS_ORDER, this);
            return _extractAsOrder;
        }

    /**
     * @return Приказ формируется индивидуально. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule#isExtractIndividual()
     */
        public PropertyPath<Boolean> extractIndividual()
        {
            if(_extractIndividual == null )
                _extractIndividual = new PropertyPath<Boolean>(CreationExtractAsOrderRuleGen.P_EXTRACT_INDIVIDUAL, this);
            return _extractIndividual;
        }

        public Class getEntityClass()
        {
            return CreationExtractAsOrderRule.class;
        }

        public String getEntityName()
        {
            return "creationExtractAsOrderRule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
