/**
 *:$
 */
package ru.tandemservice.moveemployee.component.settings.CreationExtractAsOrderSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;

import ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule;

/**
 * Create by: Shaburov
 * Date: 28.03.11
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().updateForNewExtracts();
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<CreationExtractAsOrderRule> dataSource = new DynamicListDataSource<CreationExtractAsOrderRule>(component, this);
        dataSource.addColumn(new SimpleColumn("Название", CreationExtractAsOrderRule.employeeExtractType().title().s()).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Выписка формируется совместно с приказом", CreationExtractAsOrderRule.extractAsOrder().s()).setListener("onClickExtractAsOrder"));
        dataSource.addColumn(new ToggleColumn("Приказ формируется индивидуально", CreationExtractAsOrderRule.extractIndividual().s()).setListener("onClickExtractIndividual"));

        getModel(component).setDataSource(dataSource);
    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickExtractAsOrder(IBusinessComponent component)
    {
        getDao().updateExtractAsOrder((Long) component.getListenerParameter());
    }

    public void onClickExtractIndividual(IBusinessComponent component)
    {
        getDao().updateExtractIndividual((Long) component.getListenerParameter());
    }
}
