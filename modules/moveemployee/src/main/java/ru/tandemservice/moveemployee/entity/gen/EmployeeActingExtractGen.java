package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.EmployeeActingExtract;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeeActingItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Исполнение обязанностей временно отсутствующего работника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeActingExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeActingExtract";
    public static final String ENTITY_NAME = "employeeActingExtract";
    public static final int VERSION_HASH = -2134021732;
    private static IEntityMeta ENTITY_META;

    public static final String L_MISSING_EMPLOYEE_POST = "missingEmployeePost";
    public static final String L_MISSING_POST = "missingPost";
    public static final String L_MISSING_ORG_UNIT = "missingOrgUnit";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_SAVE_MAIN_JOB = "saveMainJob";
    public static final String L_CREATE_EMPLOYEE_ACTING_ITEM = "createEmployeeActingItem";

    private EmployeePost _missingEmployeePost;     // ФИО отсутствующего сотрудника
    private PostBoundedWithQGandQL _missingPost;     // Должность
    private OrgUnit _missingOrgUnit;     // Подразделение
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private boolean _saveMainJob;     // Без освобождения от основной работы
    private EmployeeActingItem _createEmployeeActingItem;     // Созданный Факт исполнения обязанностей при проведении приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ФИО отсутствующего сотрудника. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getMissingEmployeePost()
    {
        return _missingEmployeePost;
    }

    /**
     * @param missingEmployeePost ФИО отсутствующего сотрудника. Свойство не может быть null.
     */
    public void setMissingEmployeePost(EmployeePost missingEmployeePost)
    {
        dirty(_missingEmployeePost, missingEmployeePost);
        _missingEmployeePost = missingEmployeePost;
    }

    /**
     * @return Должность. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getMissingPost()
    {
        return _missingPost;
    }

    /**
     * @param missingPost Должность. Свойство не может быть null.
     */
    public void setMissingPost(PostBoundedWithQGandQL missingPost)
    {
        dirty(_missingPost, missingPost);
        _missingPost = missingPost;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getMissingOrgUnit()
    {
        return _missingOrgUnit;
    }

    /**
     * @param missingOrgUnit Подразделение. Свойство не может быть null.
     */
    public void setMissingOrgUnit(OrgUnit missingOrgUnit)
    {
        dirty(_missingOrgUnit, missingOrgUnit);
        _missingOrgUnit = missingOrgUnit;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Без освобождения от основной работы. Свойство не может быть null.
     */
    @NotNull
    public boolean isSaveMainJob()
    {
        return _saveMainJob;
    }

    /**
     * @param saveMainJob Без освобождения от основной работы. Свойство не может быть null.
     */
    public void setSaveMainJob(boolean saveMainJob)
    {
        dirty(_saveMainJob, saveMainJob);
        _saveMainJob = saveMainJob;
    }

    /**
     * @return Созданный Факт исполнения обязанностей при проведении приказа.
     */
    public EmployeeActingItem getCreateEmployeeActingItem()
    {
        return _createEmployeeActingItem;
    }

    /**
     * @param createEmployeeActingItem Созданный Факт исполнения обязанностей при проведении приказа.
     */
    public void setCreateEmployeeActingItem(EmployeeActingItem createEmployeeActingItem)
    {
        dirty(_createEmployeeActingItem, createEmployeeActingItem);
        _createEmployeeActingItem = createEmployeeActingItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeActingExtractGen)
        {
            setMissingEmployeePost(((EmployeeActingExtract)another).getMissingEmployeePost());
            setMissingPost(((EmployeeActingExtract)another).getMissingPost());
            setMissingOrgUnit(((EmployeeActingExtract)another).getMissingOrgUnit());
            setBeginDate(((EmployeeActingExtract)another).getBeginDate());
            setEndDate(((EmployeeActingExtract)another).getEndDate());
            setSaveMainJob(((EmployeeActingExtract)another).isSaveMainJob());
            setCreateEmployeeActingItem(((EmployeeActingExtract)another).getCreateEmployeeActingItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeActingExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeActingExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeActingExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "missingEmployeePost":
                    return obj.getMissingEmployeePost();
                case "missingPost":
                    return obj.getMissingPost();
                case "missingOrgUnit":
                    return obj.getMissingOrgUnit();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "saveMainJob":
                    return obj.isSaveMainJob();
                case "createEmployeeActingItem":
                    return obj.getCreateEmployeeActingItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "missingEmployeePost":
                    obj.setMissingEmployeePost((EmployeePost) value);
                    return;
                case "missingPost":
                    obj.setMissingPost((PostBoundedWithQGandQL) value);
                    return;
                case "missingOrgUnit":
                    obj.setMissingOrgUnit((OrgUnit) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "saveMainJob":
                    obj.setSaveMainJob((Boolean) value);
                    return;
                case "createEmployeeActingItem":
                    obj.setCreateEmployeeActingItem((EmployeeActingItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "missingEmployeePost":
                        return true;
                case "missingPost":
                        return true;
                case "missingOrgUnit":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "saveMainJob":
                        return true;
                case "createEmployeeActingItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "missingEmployeePost":
                    return true;
                case "missingPost":
                    return true;
                case "missingOrgUnit":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "saveMainJob":
                    return true;
                case "createEmployeeActingItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "missingEmployeePost":
                    return EmployeePost.class;
                case "missingPost":
                    return PostBoundedWithQGandQL.class;
                case "missingOrgUnit":
                    return OrgUnit.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "saveMainJob":
                    return Boolean.class;
                case "createEmployeeActingItem":
                    return EmployeeActingItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeActingExtract> _dslPath = new Path<EmployeeActingExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeActingExtract");
    }
            

    /**
     * @return ФИО отсутствующего сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getMissingEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> missingEmployeePost()
    {
        return _dslPath.missingEmployeePost();
    }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getMissingPost()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> missingPost()
    {
        return _dslPath.missingPost();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getMissingOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> missingOrgUnit()
    {
        return _dslPath.missingOrgUnit();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Без освобождения от основной работы. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#isSaveMainJob()
     */
    public static PropertyPath<Boolean> saveMainJob()
    {
        return _dslPath.saveMainJob();
    }

    /**
     * @return Созданный Факт исполнения обязанностей при проведении приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getCreateEmployeeActingItem()
     */
    public static EmployeeActingItem.Path<EmployeeActingItem> createEmployeeActingItem()
    {
        return _dslPath.createEmployeeActingItem();
    }

    public static class Path<E extends EmployeeActingExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _missingEmployeePost;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _missingPost;
        private OrgUnit.Path<OrgUnit> _missingOrgUnit;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _saveMainJob;
        private EmployeeActingItem.Path<EmployeeActingItem> _createEmployeeActingItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ФИО отсутствующего сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getMissingEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> missingEmployeePost()
        {
            if(_missingEmployeePost == null )
                _missingEmployeePost = new EmployeePost.Path<EmployeePost>(L_MISSING_EMPLOYEE_POST, this);
            return _missingEmployeePost;
        }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getMissingPost()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> missingPost()
        {
            if(_missingPost == null )
                _missingPost = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_MISSING_POST, this);
            return _missingPost;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getMissingOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> missingOrgUnit()
        {
            if(_missingOrgUnit == null )
                _missingOrgUnit = new OrgUnit.Path<OrgUnit>(L_MISSING_ORG_UNIT, this);
            return _missingOrgUnit;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeActingExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeActingExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Без освобождения от основной работы. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#isSaveMainJob()
     */
        public PropertyPath<Boolean> saveMainJob()
        {
            if(_saveMainJob == null )
                _saveMainJob = new PropertyPath<Boolean>(EmployeeActingExtractGen.P_SAVE_MAIN_JOB, this);
            return _saveMainJob;
        }

    /**
     * @return Созданный Факт исполнения обязанностей при проведении приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeActingExtract#getCreateEmployeeActingItem()
     */
        public EmployeeActingItem.Path<EmployeeActingItem> createEmployeeActingItem()
        {
            if(_createEmployeeActingItem == null )
                _createEmployeeActingItem = new EmployeeActingItem.Path<EmployeeActingItem>(L_CREATE_EMPLOYEE_ACTING_ITEM, this);
            return _createEmployeeActingItem;
        }

        public Class getEntityClass()
        {
            return EmployeeActingExtract.class;
        }

        public String getEntityName()
        {
            return "employeeActingExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
