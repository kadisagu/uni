/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.sec.entity.Admin;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

/**
 * @author dseleznev
 *         Created on: 20.11.2008
 */
public abstract class ModularEmployeeExtractPubController<T extends ModularEmployeeExtract, IDAO extends IModularEmployeeExtractPubDAO<T, Model>, Model extends ModularEmployeeExtractPubModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setDebugMode(Boolean.parseBoolean(ApplicationRuntime.getProperty("uni.god.mode")) && null == model.getExtract().getParagraph());

        model.setAttributesPage(getClass().getPackage().getName() + ".Attributes");
        model.setAdditionalAttrsPage(getClass().getPackage().getName() + ".AdditionalAttrs");

        if (model.getExtractIndividual())
        {
            AbstractEmployeeOrder order = model.getExtract().getParagraph().getOrder();
            model.setOrderCanBeEdited(CoreServices.securityService().check(order, component.getUserContext().getPrincipalContext(), "editCommitDate_employeeSingleOrder") || CoreServices.securityService().check(order, component.getUserContext().getPrincipalContext(), "editOrderNumber_employeeSingleOrder"));
            model.setTabLable("Сборный приказ");
            model.setStickerValue(model.getExtract().getParagraph().getOrder().getTitle());
            model.setCommonDataInfoTableCaption("Общие данные сборного приказа");
            model.setInfoPropertyTitleStateExt("Состояние");
            model.setInfoPropertyValueStateExt(model.getExtract().getParagraph().getOrder().getState().getTitle());
        }
        else
        {
            model.setTabLable("Проект сборного приказа");
            model.setStickerValue(model.getExtract().getTitle());
            model.setCommonDataInfoTableCaption("Общие данные выписки из сборного приказа");
            model.setInfoPropertyTitleStateExt("Состояние выписки");
            model.setInfoPropertyValueStateExt(model.getExtract().getState().getTitle());
        }
    }

    public void onClickRejectIndividualExt(IBusinessComponent component)
    {
        getDao().doRejectIndividualExt(getModel(component));
    }

    public void onClickSetExecutor(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.STUDENT_ORDER_SET_EXECUTOR, new ParametersMap()
                .add("orderId", getModel(component).getExtract().getParagraph().getOrder().getId())
        ));
    }

    public void onClickEditOrder(IBusinessComponent component)
    {
        Long orderId = getModel(component).getExtract().getParagraph().getOrder().getId();
        component.createChildRegion("extractTabPanel", new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_MODULAR_ORDER_EDIT, new ParametersMap().add("orderId", orderId)));
    }

    public void onClickCommitIndividualExt(IBusinessComponent component)
    {
        try
        {
            getDao().doCommitIndividualExt(getModel(component));
        }
        catch (RuntimeException e)
        {
            component.getDesktop().setRefreshScheduled(true);
        }
    }

    public void onClickCommit(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(model, errors);
        if (errors.hasErrors()) return;

        MoveDaoFacade.getMoveDao().doCommitExtract(model.getExtract(), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
    }

    public void onClickRollback(IBusinessComponent component)
    {
        Model model = getModel(component);
        MoveDaoFacade.getMoveDao().doRollbackExtract(model.getExtract(), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE), null);
    }

    public void onClickRollbackIndividualExt(IBusinessComponent component)
    {
        Model model = getModel(component);
        MoveDaoFacade.getMoveDao().doRollbackOrder(model.getExtract().getParagraph().getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER), null);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        Long extractId = getModel(component).getExtractId();
        int extractIndex = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractIndex(extractId);
        component.createChildRegion("extractTabPanel", new ComponentActivator(MoveEmployeeUtils.getModularExtractAddEditComponent(extractIndex), new ParametersMap().add("extractId", extractId)));
    }

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendToCoordination(getModel(component), null);
        else
            getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    public void onClickSendToCoordinationIndividualExt(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendToCoordination(getModel(component), null);
        else
            getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
    }

    public void onClickSendToFormativeIndividualExt(IBusinessComponent component)
    {
        getDao().doSendToFormativeIndividualExt(getModel(component));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        // TODO: conditions for deletion ablility must be implemented
        Model model = getModel(component);

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        getDao().validateStateOrder(model.getExtract(), errorCollector);

        if (errorCollector.hasErrors())
            return;

        MoveDaoFacade.getMoveDao().deleteExtractWithParagraph(model.getExtract());
        deactivate(component);
    }

    public void onClickDeleteIndividualExt(IBusinessComponent component)
    {
        // TODO: conditions for deletion ablility must be implemented
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(getModel(component).getExtract().getParagraph().getOrder());
        deactivate(component);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.MODULAR_EMPLOYEE_EXTRACT_PRINT, new ParametersMap().add("extractId", getModel(component).getExtractId())));
    }

    public void onClickPrintIndividualExt(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_MODULAR_ORDER_PRINT, new ParametersMap()
                .add("orderId", getModel(component).getExtract().getParagraph().getOrder().getId())
        ));
    }

}