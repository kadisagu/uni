/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.menuempl.ListOrdersFinished;

import java.util.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;

/**
 * @author dseleznev
 * Created on: 30.10.2009
 */
@SuppressWarnings({"unchecked", "unused"})
public class Model
{
    private ISelectModel _orgUnitListModel;
    private List<EmployeeExtractType> _orderTypeList;
    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;

    // Getters & Setters

    public List<EmployeeExtractType> getOrderTypeList()
    {
        return _orderTypeList;
    }

    public void setOrderTypeList(List<EmployeeExtractType> orderTypeList)
    {
        _orderTypeList = orderTypeList;
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }
}