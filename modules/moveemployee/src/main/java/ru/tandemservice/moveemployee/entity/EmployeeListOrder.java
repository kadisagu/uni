package ru.tandemservice.moveemployee.entity;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.moveemployee.entity.gen.EmployeeListOrderGen;
import ru.tandemservice.uni.dao.UniDaoFacade;

/**
 * Списочный приказ по сотрудникам
 */
public class EmployeeListOrder extends EmployeeListOrderGen
{
    public String getEditableTitle()
    {
        return "«" + getType().getTitle() + "»" + (getNumber() == null ? "" : " №" + getNumber()) + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate());
    }
    
    @Override
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        return "Списочный приказ «" + getType().getTitle() + "»" + (getNumber() == null ? "" : " №" + getNumber()) +(getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
    }
    
    public String getBasicListStr()
    {
        List<EmplListOrderToBasicRelation> list = UniDaoFacade.getCoreDao().getList(EmplListOrderToBasicRelation.class, EmplListOrderToBasicRelation.L_ORDER, this);
        String[] title = new String[list.size()];
        for (int i = 0; i < list.size(); i++)
            title[i] = list.get(i).getBasic().getTitle() + (StringUtils.isEmpty(list.get(i).getComment()) ? "" : " " + list.get(i).getComment());
        return StringUtils.join(title, ", ");
    }
}