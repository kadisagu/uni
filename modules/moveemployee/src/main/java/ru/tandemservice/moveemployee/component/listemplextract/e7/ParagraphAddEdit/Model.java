/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e7.ParagraphAddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract;

import java.util.*;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class Model extends AbstractListParagraphAddEditModel<PayForHolidayDayEmplListExtract>
{
    private DynamicListDataSource<EmployeePost> _employeeDataSource;

    private IMultiSelectModel _orgUnitModel;
    private IMultiSelectModel _employeeTypeModel;
    private IMultiSelectModel _postModel;
    private ISingleSelectModel _woorkTypeModel;

    private List<OrgUnit> _orgUnitList;
    private List<EmployeeType> _employeeTypeList;
    private List<PostBoundedWithQGandQL> _postList;

    private List<EmployeePost> _employeePostList;
    private List<EmployeePost> _selectEmployee;
    private List<EmployeePost> _tempEmployeeList = new ArrayList<>();
    private Boolean _childOrgUnit = false;
    private Date _holidayDate;

    private Map<Long, String> _employeeIdWorkTimeMap = new HashMap<>();
    private Map<Long, Double> _employeeIdTimeAmountMap = new HashMap<>();
    private Map<Long, Double> _employeeIdNightTimeAmountMap = new HashMap<>();
    private Map<Long, IdentifiableWrapper> _employeeIdWorkTypeMap = new HashMap<>();

    //Calculate methods

    public String getWorkTimeId()
    {
        return "workTimeId_" + getEmployeeDataSource().getCurrentEntity().getId();
    }

    public String getTimeAmountId()
    {
        return "timeAmountId_" + getEmployeeDataSource().getCurrentEntity().getId();
    }

    public String getNightTimeId()
    {
        return "nightTimeId_" + getEmployeeDataSource().getCurrentEntity().getId();
    }

    public String getWorkTypeId()
    {
        return "workTypeId_" + getEmployeeDataSource().getCurrentEntity().getId();
    }

    //Getters & Setters

    public Date getHolidayDate()
    {
        return _holidayDate;
    }

    public void setHolidayDate(Date holidayDate)
    {
        _holidayDate = holidayDate;
    }

    public Map<Long, String> getEmployeeIdWorkTimeMap()
    {
        return _employeeIdWorkTimeMap;
    }

    public void setEmployeeIdWorkTimeMap(Map<Long, String> employeeIdWorkTimeMap)
    {
        _employeeIdWorkTimeMap = employeeIdWorkTimeMap;
    }

    public Map<Long, Double> getEmployeeIdTimeAmountMap()
    {
        return _employeeIdTimeAmountMap;
    }

    public void setEmployeeIdTimeAmountMap(Map<Long, Double> employeeIdTimeAmountMap)
    {
        _employeeIdTimeAmountMap = employeeIdTimeAmountMap;
    }

    public Map<Long, Double> getEmployeeIdNightTimeAmountMap()
    {
        return _employeeIdNightTimeAmountMap;
    }

    public void setEmployeeIdNightTimeAmountMap(Map<Long, Double> employeeIdNightTimeAmountMap)
    {
        _employeeIdNightTimeAmountMap = employeeIdNightTimeAmountMap;
    }

    public Map<Long, IdentifiableWrapper> getEmployeeIdWorkTypeMap()
    {
        return _employeeIdWorkTypeMap;
    }

    public void setEmployeeIdWorkTypeMap(Map<Long, IdentifiableWrapper> employeeIdWorkTypeMap)
    {
        _employeeIdWorkTypeMap = employeeIdWorkTypeMap;
    }

    public List<EmployeePost> getEmployeePostList()
    {
        return _employeePostList;
    }

    public void setEmployeePostList(List<EmployeePost> employeePostList)
    {
        _employeePostList = employeePostList;
    }

    public Boolean getChildOrgUnit()
    {
        return _childOrgUnit;
    }

    public void setChildOrgUnit(Boolean childOrgUnit)
    {
        _childOrgUnit = childOrgUnit;
    }

    public List<EmployeePost> getTempEmployeeList()
    {
        return _tempEmployeeList;
    }

    public void setTempEmployeeList(List<EmployeePost> tempEmployeeList)
    {
        _tempEmployeeList = tempEmployeeList;
    }

    public ISingleSelectModel getWoorkTypeModel()
    {
        return _woorkTypeModel;
    }

    public void setWoorkTypeModel(ISingleSelectModel woorkTypeModel)
    {
        _woorkTypeModel = woorkTypeModel;
    }

    public List<EmployeePost> getSelectEmployee()
    {
        return _selectEmployee;
    }

    public void setSelectEmployee(List<EmployeePost> selectEmployee)
    {
        _selectEmployee = selectEmployee;
    }

    public List<OrgUnit> getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public List<EmployeeType> getEmployeeTypeList()
    {
        return _employeeTypeList;
    }

    public void setEmployeeTypeList(List<EmployeeType> employeeTypeList)
    {
        _employeeTypeList = employeeTypeList;
    }

    public List<PostBoundedWithQGandQL> getPostList()
    {
        return _postList;
    }

    public void setPostList(List<PostBoundedWithQGandQL> postList)
    {
        _postList = postList;
    }

    public IMultiSelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(IMultiSelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public IMultiSelectModel getEmployeeTypeModel()
    {
        return _employeeTypeModel;
    }

    public void setEmployeeTypeModel(IMultiSelectModel employeeTypeModel)
    {
        _employeeTypeModel = employeeTypeModel;
    }

    public IMultiSelectModel getPostModel()
    {
        return _postModel;
    }

    public void setPostModel(IMultiSelectModel postModel)
    {
        _postModel = postModel;
    }

    public DynamicListDataSource<EmployeePost> getEmployeeDataSource()
    {
        return _employeeDataSource;
    }

    public void setEmployeeDataSource(DynamicListDataSource<EmployeePost> employeeDataSource)
    {
        _employeeDataSource = employeeDataSource;
    }
}
