/**
 *$Id$
 */
package ru.tandemservice.moveemployee.base.bo.MoveemployeeContract.ui.AddDismissalContractExtract;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.datasource.column.BlockDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.IHeaderColumnBuilder;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.HolidayDuration;
import ru.tandemservice.moveemployee.base.bo.MoveemployeeContract.MoveemployeeContractManager;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

/**
 * Create by ashaburov
 * Date 15.05.12
 */
@Input({
        @Bind(key = MoveemployeeContractAddDismissalContractExtractUI.CONTRACT_ID_LIST, binding = MoveemployeeContractAddDismissalContractExtractUI.CONTRACT_ID_LIST)
})
public class MoveemployeeContractAddDismissalContractExtractUI extends UIPresenter
{
    //input fields name

    public static final String CONTRACT_ID_LIST = "contractIdList";


    //property name

    public static final String EXTRACT_REASON = "extractReason";
    public static final String EXTRACT_LIST = "extractList";


    //fields

    private List<Long> _contractIdList;//список id ТД, приходящих с предыдущего БК
    private List<EmploymentDismissalEmplListExtract> _extractList;//списко выписок возможные для создания на форме
    private EmployeeListOrder _order;//создаваемый списочный приказ
    private EmployeeOrderReasons _extractReason;//причина выписок
    private List<EmployeeOrderBasics> _basicList;//основания выписок
    private Date _compensationDateFrom;//Компенсация за неиспользованный отпуск за период с
    private Date _compensationDateTo;//Компенсация за неиспользованный отпуск за период по
    private EmployeeDismissReasons _dismissalReason;//причина увольнения в выписке
    private Map<EmployeePost, Map<String, String>> _basicNoticeMap = new HashMap<>();//мапа примечаний к основаниям по каждой выписке для каждого основания


    //Methods

    @Override
    public void onComponentRefresh()
    {
        //подготавливаем приказ
        EmployeeExtractType orderType = DataAccessServices.dao().getNotNull(EmployeeExtractType.class, EmployeeExtractType.code(), "2.3");
        _order = new EmployeeListOrder();
        _order.setCreateDate(new Date());
        _order.setType(orderType);

        //подготавливаем список ТД, полученых с предыдущего БК
        List<EmployeeLabourContract> contractList = new ArrayList<>();
        for (Long id : _contractIdList)
            contractList.add(DataAccessServices.dao().get(EmployeeLabourContract.class, id));

        //производим действия аналогичные действиям на форме доб./ред. выписки спис. приказа Об увольнении
        //создаем и подготавливаем список выписок и параграф для каждой выписки
        long id = -1;
        _extractList = new ArrayList<>();
        ExtractStates extractState = DataAccessServices.dao().get(ExtractStates.class, ExtractStates.code(), UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER);
        EmployeeExtractType extractType = DataAccessServices.dao().getNotNull(EmployeeExtractType.class, EmployeeExtractType.code(), "2.3.1");
        for (EmployeeLabourContract contract : contractList)
        {
            EmployeeListParagraph paragraph = new EmployeeListParagraph();
            paragraph.setOrder(_order);

            EmploymentDismissalEmplListExtract extract = new EmploymentDismissalEmplListExtract();
            extract.setParagraph(paragraph);
            extract.setEmployeePost(contract.getEmployeePost());
            extract.setEmployee(contract.getEmployeePost().getEmployee());
            extract.setEmployeeFioModified(contract.getEmployeePost().getPerson().getFullFio());
            extract.setState(extractState);
            extract.setType(extractType);
            extract.setDismissalDate(contract.getEndDate());

            extract.setCreateDate(new Date());

            extract.setId(id--);
            _extractList.add(extract);
        }

        //сортируем и нумеруем выписки
        int number = 1;
        _extractList.sort(ListEmployeeExtract.FULL_FIO_AND_ID_COMPARATOR);
        for (EmploymentDismissalEmplListExtract extract : _extractList)
        {
            extract.setNumber(number);
            extract.getParagraph().setNumber(number++);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(MoveemployeeContractAddDismissalContractExtract.BASICS_DS))
        {
            dataSource.put(MoveemployeeContractAddDismissalContractExtractUI.EXTRACT_REASON, _extractReason);
        }
        else if (dataSource.getName().endsWith(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS))
        {
            dataSource.put(MoveemployeeContractAddDismissalContractExtractUI.EXTRACT_LIST, _extractList);
        }

    }


    //Listeners

    public void onChangeReason()
    {
        if (_extractReason == null)
            return;

        //поднимаем причину увольнения связаную с причиной приказа
        EmployeeReasonToDismissReasonRel reasonRel = DataAccessServices.dao().get(EmployeeReasonToDismissReasonRel.class, EmployeeReasonToDismissReasonRel.employeeOrderReason(), _extractReason);

        _dismissalReason = reasonRel != null ? reasonRel.getEmployeeDismissReason() : null;
    }

    public void onChangeBasic()
    {
        //берем датаСорс сечлиста
        BaseSearchListDataSource dataSource = getConfig().getDataSource(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS);

        IHeaderColumnBuilder header = MoveemployeeContractManager.headerColumn("basicNotice");

        dataSource.doCleanupDataSource();
        dataSource.addColumn(header);

        for (EmployeeOrderBasics basic : _basicList)
        {
            //если необходимо примечание, то создаем для него колонку
            if (basic.isCommentable())
            {
                BlockDSColumn column = MoveemployeeContractManager.blockColumn(basic.getCode(), "basicNotice").create();
                column.setLabel(basic.getTitle());

                header.addSubColumn(column);

                //если примечание это трудовой договор
                if (basic.isLaborContract())
                {
                    //то автозаполняем примечания для каждой выписки
                    for (EmploymentDismissalEmplListExtract extract : _extractList)
                    {
                        //если только основание еще не заполненно
                        if (_basicNoticeMap.get(extract.getEmployeePost()) == null || _basicNoticeMap.get(extract.getEmployeePost()).get(column.getName()) == null)
                        {
                            EmployeeLabourContract contract = DataAccessServices.dao().get(EmployeeLabourContract.class, EmployeeLabourContract.employeePost(), extract.getEmployeePost());

                            if (contract != null)
                            {
                                Map<String, String> map = _basicNoticeMap.get(extract.getEmployeePost());
                                if (map == null)
                                    map = new HashMap<>();

                                map.put(column.getName(), "№" + contract.getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(contract.getDate()));
                                _basicNoticeMap.put(extract.getEmployeePost(), map);
                            }
                        }
                    }
                }
            }
        }

        dataSource.doPrepareDataSource();
    }

    public void onChangeCompensationDate()
    {
        /*
        алгоритм рассчитывает компенсацию в днях

        1) поднимаются отпуска, периоды которых пересекаются или полностью в указанном периоде
        2.1) у отпусков период которых полностью попал в период дат берется вся длительность
        2.2) у отпусков период которых частично попал в период дат длительность берется пропорционально части попавшего периода
        3) по указанному периоду вычисляется сколько положено дней ежегодного отпуска сотруднику
        4) вычисляется разница между положенными и взятым отпуском
         */

        if (_compensationDateFrom == null || _compensationDateTo == null)
            return;

        for (EmploymentDismissalEmplListExtract extract : _extractList)
        {
            if (extract.getCompensatingForDay()  != 0d)
                continue;

            // 1.
            List<EmployeeHoliday> holidayList = MoveemployeeContractManager.instance().dao().getEmployeeHolidayList(extract.getEmployeePost(), _compensationDateFrom, _compensationDateTo);

            // 2.
            Double usedHoliday = 0d;
            for (EmployeeHoliday holiday : holidayList)
            {
                // 2.1
                if (CommonBaseDateUtil.isBetween(holiday.getBeginDate(), _compensationDateFrom, _compensationDateTo) && CommonBaseDateUtil.isBetween(holiday.getEndDate(), _compensationDateFrom, _compensationDateTo))
                {
                    usedHoliday += holiday.getDuration();
                }
                // 2.2
                else {
                    if (CommonBaseDateUtil.isBetween(holiday.getBeginDate(), _compensationDateFrom, _compensationDateTo))
                    {
                        double include = CommonBaseDateUtil.getBetweenPeriod(holiday.getBeginDate(), _compensationDateTo, Calendar.DAY_OF_YEAR);
                        double period = CommonBaseDateUtil.getBetweenPeriod(holiday.getBeginDate(), holiday.getEndDate(), Calendar.DAY_OF_YEAR);

                        Double percent = (include * 100) / period;
                        double used = holiday.getDuration() * (percent.longValue() * 0.01);

                        usedHoliday += used;
                    }
                    // 2.2
                    else {
                        if (CommonBaseDateUtil.isBetween(holiday.getEndDate(), _compensationDateFrom, _compensationDateTo)) {
                            double include = CommonBaseDateUtil.getBetweenPeriod(_compensationDateFrom, holiday.getEndDate(), Calendar.DAY_OF_YEAR);
                            double period = CommonBaseDateUtil.getBetweenPeriod(holiday.getBeginDate(), holiday.getEndDate(), Calendar.DAY_OF_YEAR);

                            Double percent = (include * 100) / period;
                            double used = holiday.getDuration() * (percent.longValue() * 0.01);

                            usedHoliday += used;
                        }
                    }
                }
            }

            // 3.
            double period = CommonBaseDateUtil.getBetweenPeriod(_compensationDateFrom, _compensationDateTo, Calendar.DAY_OF_YEAR);

            int holidayDuration = 28;
            HolidayDuration employeeHolidayDuration = extract.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getEmployeeType().getHolidayDuration();
            if (null != employeeHolidayDuration)
                holidayDuration = employeeHolidayDuration.getDaysAmount();

            double fullHoliday = holidayDuration * (period / 365d);

            // 4.
            double difference = fullHoliday - usedHoliday;

            if (difference > 0)
                extract.setCompensatingForDay(difference);
            else
                extract.setCompensatingForDay(0d);
        }
    }

    @SuppressWarnings("unchecked")
    public ErrorCollector validate()
    {
        ErrorCollector errorCollector = getUserContext().getErrorCollector();

        BaseSearchListDataSource dataSource = getConfig().getDataSource(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS);

        List<IEntity> selectedEntities = dataSource.getLegacyDataSource().getSelectedEntities();

        if (selectedEntities.isEmpty())
            errorCollector.add("Необходимо выбрать хотя бы одного сотрудника.");

        //првоерием что у сотрудников нет непроведенных выписок
        for (IEntity entity : selectedEntities)
        {
            EmploymentDismissalEmplListExtract extract = (EmploymentDismissalEmplListExtract) entity;

            if (MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeePostHasNotFinishedExtracts(extract.getEmployeePost()))
                errorCollector.add("Нельзя добавить параграф, так как у сотрудника '" + extract.getEmployeePost().getPerson().getFio() + "' уже есть непроведенные выписки.");

            if (extract.getDismissalDate() == null)
                errorCollector.add("Поле «Дата увольнения» обязательно для заполнения.", "dismissalDateId_" + extract.getId());
        }

        return errorCollector;
    }

    public void onClickApply()
    {
        //делаем проверки
        ErrorCollector errorCollector = validate();

        if (errorCollector.hasErrors())
            return;

        Collection<IEntity> selectedEntities = ((CheckboxColumn) ((BaseSearchListDataSource) getConfig().getDataSource(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS)).getLegacyDataSource().getColumn("checkbox")).getSelectedObjects();

        List<EmploymentDismissalEmplListExtract> extractToSaveList = new ArrayList<>();
        for (IEntity entity : selectedEntities)
                extractToSaveList.add((EmploymentDismissalEmplListExtract) entity);

        //сетим в выписки причину приказа и причину увольнения
        for (EmploymentDismissalEmplListExtract extract : extractToSaveList)
        {
            extract.setReason(_extractReason);
            extract.setEmployeeDismissReasons(_dismissalReason);
        }

        MoveemployeeContractManager.instance().dao().doSave(_order, extractToSaveList);

        MoveemployeeContractManager.instance().dao().updateBasicList(extractToSaveList, _basicList, _basicNoticeMap);

        deactivate();
        getActivationBuilder().asDesktopRoot("ru.tandemservice.moveemployee.component.listemplextract.e0.ListOrderPub").parameter(UIPresenter.PUBLISHER_ID, _order.getId()).activate();
    }


    //Calculate getters & setters

    public String getBasicNotice()
    {
        BaseSearchListDataSource dataSource = getConfig().getDataSource(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS);
        EmploymentDismissalEmplListExtract current = dataSource.getCurrent();
        AbstractColumn currentColumn = ((AbstractListDataSource) dataSource.getResult()).getCurrentColumn();

        Map<String, String> map = _basicNoticeMap.get(current.getEmployeePost());
        if (map == null)
            return null;
        else
            return map.get(currentColumn.getName());
    }

    public void setBasicNotice(String text)
    {
        BaseSearchListDataSource dataSource = getConfig().getDataSource(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS);
        EmploymentDismissalEmplListExtract current = dataSource.getCurrent();
        AbstractColumn currentColumn = ((AbstractListDataSource) dataSource.getResult()).getCurrentColumn();

        Map<String, String> map = _basicNoticeMap.get(current.getEmployeePost());
        if (map == null)
            map = new HashMap<>();

        map.put(currentColumn.getName(), text);
        _basicNoticeMap.put(current.getEmployeePost(), map);
    }

    public String getDismissalDateId()
    {
        return "dismissalDateId_" + getConfig().getDataSource(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS).getCurrentId();
    }

    public String getNotFulfilDayId()
    {
        return "notFulfilDayId_" + getConfig().getDataSource(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS).getCurrentId();
    }

    public String getCompensatingForDayId()
    {
        return "compensatingForDayId_" + getConfig().getDataSource(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS).getCurrentId();
    }

    public String getBasicNoticeId()
    {
        BaseSearchListDataSource dataSource = getConfig().getDataSource(MoveemployeeContractAddDismissalContractExtract.EMPLOYEE_DS);
        AbstractColumn currentColumn = ((AbstractListDataSource) dataSource.getResult()).getCurrentColumn();
        return "basicNoticeId_" + dataSource.getCurrentId() + "_" + currentColumn.getName();
    }


    //Getters & Setters

    public List<Long> getContractIdList()
    {
        return _contractIdList;
    }

    public void setContractIdList(List<Long> contractIdList)
    {
        _contractIdList = contractIdList;
    }

    public EmployeeListOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeListOrder order)
    {
        _order = order;
    }

    public EmployeeOrderReasons getExtractReason()
    {
        return _extractReason;
    }

    public void setExtractReason(EmployeeOrderReasons extractReason)
    {
        _extractReason = extractReason;
    }

    public List<EmployeeOrderBasics> getBasicList()
    {
        return _basicList;
    }

    public void setBasicList(List<EmployeeOrderBasics> basicList)
    {
        _basicList = basicList;
    }

    public Date getCompensationDateFrom()
    {
        return _compensationDateFrom;
    }

    public void setCompensationDateFrom(Date compensationDateFrom)
    {
        _compensationDateFrom = compensationDateFrom;
    }

    public Date getCompensationDateTo()
    {
        return _compensationDateTo;
    }

    public void setCompensationDateTo(Date compensationDateTo)
    {
        _compensationDateTo = compensationDateTo;
    }

    public EmployeeDismissReasons getDismissalReason()
    {
        return _dismissalReason;
    }

    public void setDismissalReason(EmployeeDismissReasons dismissalReason)
    {
        _dismissalReason = dismissalReason;
    }

    public List<EmploymentDismissalEmplListExtract> getExtractList()
    {
        return _extractList;
    }

    public void setExtractList(List<EmploymentDismissalEmplListExtract> extractList)
    {
        _extractList = extractList;
    }
}
