/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.ICommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;

import java.util.Date;

/**
 * @author dseleznev
 * Created on: 21.11.2008
 */
public interface IDAO extends ICommonModularEmployeeExtractAddEditDAO<EmployeeHolidayExtract, Model>
{
    /**
     * Является ли date праздником в производственном календаре
     * @param weekDuration - продолжительность рабочей неделе
     * @param date - проверяемая дата
     * @return Возвращает <b><tt>true</b></tt>, если <tt>date</tt> является праздником в производственном календаре,<p>
     * иначе <b><tt>false</b></tt>
     */
    public boolean isIndustrialCalendarHoliday(EmployeeWorkWeekDuration weekDuration, Date date);

    public void prepareStaffRateDataSource(Model model);

    public void prepareHolidayDataSource(Model model);

    /**
     * @return true, если у Подразделения Сотрудника есть График отпускв и этот Сотрудник включен в него,<p>
     * иначе false
     */
    public boolean hasVacationSchedule(EmployeePost employeePost);
}