/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.utils;

import org.tandemframework.core.runtime.ApplicationRuntime;

import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.listemplextract.IOrderParagraphListCustomizer;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * @author dseleznev
 * Created on: 13.11.2008
 */
public class MoveEmployeeUtils
{
    public static String getModularExtractAddEditComponent(int extractIndex)
    {
        return "ru.tandemservice.moveemployee.component.modularemplextract.e" + extractIndex + ".AddEdit";
    }

    public static String getModularExtractAddEditComponent(Long extractId)
    {
        IAbstractExtract extract = UniDaoFacade.getCoreDao().get(extractId);
        int extractIndex = ((EmployeeExtractType)extract.getType()).getIndex();
        return getModularExtractAddEditComponent(extractIndex);
    }

    public static String getSingleExtractAddEditComponent(int extractIndex)
    {
        return "ru.tandemservice.moveemployee.component.singleemplextract.e" + extractIndex + ".AddEdit";
    }

    public static String getSingleExtractAddEditComponent(Long extractId)
    {
        IAbstractExtract extract = UniDaoFacade.getCoreDao().get(extractId);
        int extractIndex = ((EmployeeExtractType)extract.getType()).getIndex();
        return getSingleExtractAddEditComponent(extractIndex);
    }

    public static String getExtractAddEditComponent(EmployeeExtractType extractType)
    {
        if(null != extractType.getParent())
        {
            if(MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_MODULAR_ORDER_CODE.equals(extractType.getParent().getCode()))
                return getModularExtractAddEditComponent(extractType.getIndex());
            else if(MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_INDIVIDUAL_ORDER_CODE.equals(extractType.getParent().getCode()))
                return getSingleExtractAddEditComponent(extractType.getIndex());
        }
        throw new IllegalStateException(); // Unsupported order type
    }

    public static String getListOrderAddEditComponent(int orderTypeIndex)
    {
        return "ru.tandemservice.moveemployee.component.listemplextract.e" + orderTypeIndex + ".ListOrderAddEdit";
    }

    public static String getListOrderPubComponent(int orderTypeIndex)
    {
        return "ru.tandemservice.moveemployee.component.listemplextract.e" + orderTypeIndex + ".ListOrderPub";
    }

    public static String getListMultipleExtractAddComponent(int extractIndex)
    {
        return "ru.tandemservice.moveemployee.component.listemplextract.e" + extractIndex + ".MultipleExtractAdd";
    }

    public static String getListExtractAddEditComponent(int extractIndex)
    {
        return "ru.tandemservice.moveemployee.component.listemplextract.e" + extractIndex + ".ExtractAddEdit";
    }

    public static String getListExtractPubComponent(int extractIndex)
    {
        return "ru.tandemservice.moveemployee.component.listemplextract.e" + extractIndex + ".ListExtractPub";
    }

    public static String getModularOrderPubComponent()
    {
        return "ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderPub";
    }

    public static String getSingleOrderPubComponent(int extractIndex)
    {
        return "ru.tandemservice.moveemployee.component.singleemplextract.e" + extractIndex + ".Pub";
    }

    @SuppressWarnings("unchecked")
    public static IOrderParagraphListCustomizer getListOrderParagraphListCustomizer(int orderTypeIndex)
    {
        IOrderParagraphListCustomizer customizer = (IOrderParagraphListCustomizer) ApplicationRuntime.getBean("extractIndex" + orderTypeIndex + "_customizer");

        return customizer;
    }

    public static String getModularEmployeeExtractPubComponent(int extractIndex)
    {
        return "ru.tandemservice.moveemployee.component.modularemplextract.e" + extractIndex + ".Pub";
    }

    public static String getListParagraphPubComponent(EmployeeExtractType extractType)
    {
        return "ru.tandemservice.moveemployee.component.listemplextract.e" + extractType.getIndex() + ".ListParagraphPub";
    }

    public static String getListParagraphAddEditComponent(int extractIndex)
    {
        return "ru.tandemservice.moveemployee.component.listemplextract.e" + extractIndex + ".ParagraphAddEdit";
    }
}