/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderPrint;

import org.tandemframework.core.component.State;

import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;

/**
 * @author dseleznev
 * Created on: 20.01.2009
 */
@State(keys = "orderId", bindings = "orderId")
public class Model
{
    private Long _orderId;
    private EmployeeModularOrder _order;
    private byte[] _data;
    private String[][] _visaData;

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public EmployeeModularOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeModularOrder order)
    {
        _order = order;
    }

    public byte[] getData()
    {
        return _data;
    }

    public void setData(byte[] data)
    {
        _data = data;
    }

    public String[][] getVisaData()
    {
        return _visaData;
    }

    public void setVisaData(String[][] visaData)
    {
        _visaData = visaData;
    }
}
