/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

/**
 * @author ashaburov
 * Created on: 11.10.2011
 */
public class AbstractListParagraphPubController<T extends ListEmployeeExtract, Model extends AbstractListParagraphPubModel<T>, IDAO extends IAbstractListParagraphPubDAO<T, Model>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
        
        model.setAttributesPage(getClass().getPackage().getName() + ".Attributes");
        model.setFormTitle(getFormTitle(model));

        model.setSearchListSettingsKey("EmployeeListParagraph." + model.getParagraph().getOrder().getType().getId() + ".");

        prepareListDataSource(component);
    }

    @SuppressWarnings("unchecked")
    public void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getExtractsDataSource() != null)
            return;

        DynamicListDataSource<T> dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareListDataSource(model);
        });

        prepareColumn(dataSource, model);
        dataSource.addColumn(new ActionColumn("Удалить выписку", ActionColumn.DELETE, "onClickExtractDelete", "Исключить сотрудника {0} из параграфа?", ListEmployeeExtract.employee().person().fio().s())
                                     .setPermissionKey(model.getSecModel().getPermission("delete"))
                                     .setDisableHandler(entity -> !OrderStatesCodes.FORMING.equals(entity.getProperty(ListEmployeeExtract.paragraph().order().state().code().s()))));

        model.setExtractsDataSource(dataSource);
    }

    public void prepareColumn(DynamicListDataSource dataSource, Model model)
    {
        //необходимо переопределить метод и описать дополнительные колонки или заменить на другие
        PublisherLinkColumn fioColumn = new PublisherLinkColumn("ФИО", ListEmployeeExtract.entity().person().identityCard().fullFio().s());
        //int extractIndex = model.getParagraph().getFirstExtract().getType().getIndex();
        fioColumn.setOrderable(false);
        fioColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                ListEmployeeExtract o = getDao().get(entity.getId());

                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, o.getEntity().getId());
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return EmployeePostView.class.getSimpleName();
            }
        });
        dataSource.addColumn(fioColumn);
        dataSource.addColumn(new SimpleColumn("Табельный номер", ListEmployeeExtract.employee().employeeCode().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Должность", ListEmployeeExtract.entity().postRelation().postBoundedWithQGandQL().fullTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Подразделение", ListEmployeeExtract.entity().orgUnit().titleWithType().s()).setClickable(false).setOrderable(false));
    }

    public void onClickExtractDelete(IBusinessComponent component)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().deleteListExtract(component.<Long>getListenerParameter());
        getModel(component).getExtractsDataSource().refresh();
    }

    public void onClickDelete(IBusinessComponent component)
    {
        MoveDaoFacade.getMoveDao().deleteParagraph(getModel(component).getParagraph(), null);
        deactivate(component);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        Model model = getModel(component);

        int extractIndex = model.getParagraph().getFirstExtract().getType().getIndex();
        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListParagraphAddEditComponent(extractIndex), new ParametersMap()
                .add(AbstractListParagraphAddEditModel.PARAMETER_ORDER_ID, model.getParagraph().getOrder().getId())
                .add(AbstractListParagraphAddEditModel.PARAMETER_EXTRACT_TYPE_ID, model.getParagraph().getFirstExtract().getType().getId())
                .add(AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_ID, model.getParagraph().getId())));
    }

    private String getFormTitle(Model model)
    {
        StringBuilder title = new StringBuilder();
        title.append("Параграф №");
        title.append(model.getParagraph().getNumber());
        title.append(" списочного приказа");

        if (StringUtils.isNotEmpty(model.getParagraph().getOrder().getNumber()))
            title.append(" №").append(model.getParagraph().getOrder().getNumber());
        if (model.getParagraph().getOrder().getCommitDate() != null)
            title.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getParagraph().getOrder().getCommitDate()));
        return title.toString();
    }
}