package ru.tandemservice.moveemployee.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Причины увольнения сотрудников
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeDismissReasonsGen extends EntityBase
 implements INaturalIdentifiable<EmployeeDismissReasonsGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons";
    public static final String ENTITY_NAME = "employeeDismissReasons";
    public static final int VERSION_HASH = -1286885630;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_LAW_ITEM = "lawItem";
    public static final String P_SALARY_DELEGATING = "salaryDelegating";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _lawItem;     // Статья закона о труде
    private boolean _salaryDelegating;     // Предусматривает выдачу заработной платы родственникам сотрудника
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Статья закона о труде. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLawItem()
    {
        return _lawItem;
    }

    /**
     * @param lawItem Статья закона о труде. Свойство не может быть null.
     */
    public void setLawItem(String lawItem)
    {
        dirty(_lawItem, lawItem);
        _lawItem = lawItem;
    }

    /**
     * @return Предусматривает выдачу заработной платы родственникам сотрудника. Свойство не может быть null.
     */
    @NotNull
    public boolean isSalaryDelegating()
    {
        return _salaryDelegating;
    }

    /**
     * @param salaryDelegating Предусматривает выдачу заработной платы родственникам сотрудника. Свойство не может быть null.
     */
    public void setSalaryDelegating(boolean salaryDelegating)
    {
        dirty(_salaryDelegating, salaryDelegating);
        _salaryDelegating = salaryDelegating;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeDismissReasonsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EmployeeDismissReasons)another).getCode());
            }
            setLawItem(((EmployeeDismissReasons)another).getLawItem());
            setSalaryDelegating(((EmployeeDismissReasons)another).isSalaryDelegating());
            setTitle(((EmployeeDismissReasons)another).getTitle());
        }
    }

    public INaturalId<EmployeeDismissReasonsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EmployeeDismissReasonsGen>
    {
        private static final String PROXY_NAME = "EmployeeDismissReasonsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EmployeeDismissReasonsGen.NaturalId) ) return false;

            EmployeeDismissReasonsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeDismissReasonsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeDismissReasons.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeDismissReasons();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "lawItem":
                    return obj.getLawItem();
                case "salaryDelegating":
                    return obj.isSalaryDelegating();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "lawItem":
                    obj.setLawItem((String) value);
                    return;
                case "salaryDelegating":
                    obj.setSalaryDelegating((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "lawItem":
                        return true;
                case "salaryDelegating":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "lawItem":
                    return true;
                case "salaryDelegating":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "lawItem":
                    return String.class;
                case "salaryDelegating":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeDismissReasons> _dslPath = new Path<EmployeeDismissReasons>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeDismissReasons");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Статья закона о труде. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons#getLawItem()
     */
    public static PropertyPath<String> lawItem()
    {
        return _dslPath.lawItem();
    }

    /**
     * @return Предусматривает выдачу заработной платы родственникам сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons#isSalaryDelegating()
     */
    public static PropertyPath<Boolean> salaryDelegating()
    {
        return _dslPath.salaryDelegating();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EmployeeDismissReasons> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _lawItem;
        private PropertyPath<Boolean> _salaryDelegating;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EmployeeDismissReasonsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Статья закона о труде. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons#getLawItem()
     */
        public PropertyPath<String> lawItem()
        {
            if(_lawItem == null )
                _lawItem = new PropertyPath<String>(EmployeeDismissReasonsGen.P_LAW_ITEM, this);
            return _lawItem;
        }

    /**
     * @return Предусматривает выдачу заработной платы родственникам сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons#isSalaryDelegating()
     */
        public PropertyPath<Boolean> salaryDelegating()
        {
            if(_salaryDelegating == null )
                _salaryDelegating = new PropertyPath<Boolean>(EmployeeDismissReasonsGen.P_SALARY_DELEGATING, this);
            return _salaryDelegating;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EmployeeDismissReasonsGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EmployeeDismissReasons.class;
        }

        public String getEntityName()
        {
            return "employeeDismissReasons";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
