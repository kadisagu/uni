/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e4;

import org.hibernate.Session;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.moveemployee.component.listemplextract.IOrderParagraphListCustomizer;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract;
import ru.tandemservice.uniemp.UniempDefines;

/**
 * @author ListExtractComponentGenerator
 * @since 04.10.2011
 */
public class EmployeeHolidayEmplListExtractCustomizer implements IOrderParagraphListCustomizer
{
    public static final String MAIN_HOLIDAY = "mainHoliday";
    public static final String SECOND_HOLIDAY = "secondHoliday";

    @Override
    public void customizeParagraphList(DynamicListDataSource dataSource, IBusinessComponent component)
    {
        dataSource.addColumn(new SimpleColumn("Подразделение", EmployeeHolidayEmplListExtract.employeePost().orgUnit().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Должность", EmployeeHolidayEmplListExtract.employeePost().postRelation().postBoundedWithQGandQL().title().s()).setClickable(false).setOrderable(false));
        HeadColumn headColumn = new HeadColumn("holidayHeadColumn", "Отпуск");
        headColumn.setHeaderAlign("center");
        headColumn.addColumn(new SimpleColumn("Ежегодный отпуск, календарных дней", MAIN_HOLIDAY).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Дополнительный отпуск, календарных дней", SECOND_HOLIDAY).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Дата начала периода", EmployeeHolidayEmplListExtract.periodBeginDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Дата окончания периода", EmployeeHolidayEmplListExtract.periodEndDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Всего календарных дней", EmployeeHolidayEmplListExtract.duration().s()).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Дата начала отпуска", EmployeeHolidayEmplListExtract.beginDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Дата окончания отпуска", EmployeeHolidayEmplListExtract.endDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(headColumn);
    }

    @Override
    public void wrap(DynamicListDataSource dataSource, Session session)
    {
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
        {
            if (!((EmployeeHolidayEmplListExtract) wrapper.getEntity()).getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL))
                wrapper.setViewProperty(SECOND_HOLIDAY, ((EmployeeHolidayEmplListExtract) wrapper.getEntity()).getHolidayType().getTitle() +
                        ", " + ((EmployeeHolidayEmplListExtract) wrapper.getEntity()).getSecondHolidayDuration());
            else
                wrapper.setViewProperty(SECOND_HOLIDAY, "");

            if (((EmployeeHolidayEmplListExtract) wrapper.getEntity()).getMainHolidayDuration() != null)
                wrapper.setViewProperty(MAIN_HOLIDAY, ((EmployeeHolidayEmplListExtract) wrapper.getEntity()).getMainHolidayDuration());
            else
                wrapper.setViewProperty(MAIN_HOLIDAY, "");
        }
    }
}