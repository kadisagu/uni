/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e10.AddEdit;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Required;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<EmployeeSurnameChangeExtract>
{
    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;

    private List<IdentityCardType> _identityCardTypeList;

    //Getters & Setters

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public List<IdentityCardType> getIdentityCardTypeList()
    {
        return _identityCardTypeList;
    }

    public void setIdentityCardTypeList(List<IdentityCardType> identityCardTypeList)
    {
        this._identityCardTypeList = identityCardTypeList;
    }

    public List<BaseValidator> getMiddleNameValidators ()
    {
        List<BaseValidator> validatorList = new ArrayList<>();
        if (getExtract().getIcCardType() != null)
        {
            validatorList = getExtract().getIcCardType().getMiddleNameValidators();
        }
        return validatorList;
    }

    public List<BaseValidator> getLastNameValidators ()
    {
        if (getExtract().getIcCardType() != null)
        {
            return getExtract().getIcCardType().getLastNameValidators();
        }
        else
        {
            List<BaseValidator> validatorList = new ArrayList<>();
            validatorList.add(new Required());
            return validatorList;
        }
    }

    public List<BaseValidator> getFirstNameValidators ()
    {
        if (getExtract().getIcCardType() != null)
        {
            return getExtract().getIcCardType().getFirstNameValidators();
        }
        else
        {
            List<BaseValidator> validatorList = new ArrayList<>();
            validatorList.add(new Required());
            return validatorList;
        }
    }
}