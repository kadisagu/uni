package ru.tandemservice.moveemployee.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основание приказа по кадрам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeOrderBasicsGen extends EntityBase
 implements INaturalIdentifiable<EmployeeOrderBasicsGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics";
    public static final String ENTITY_NAME = "employeeOrderBasics";
    public static final int VERSION_HASH = 1430422665;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_COMMENTABLE = "commentable";
    public static final String P_HELP = "help";
    public static final String P_LABOR_CONTRACT = "laborContract";
    public static final String P_AGREEMENT_LABOR_CONTRACT = "agreementLaborContract";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _commentable;     // Комментируемо
    private String _help;     // Разъяснение
    private boolean _laborContract;     // Трудовой договор
    private boolean _agreementLaborContract;     // Доп. соглашение к ТД
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Комментируемо. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommentable()
    {
        return _commentable;
    }

    /**
     * @param commentable Комментируемо. Свойство не может быть null.
     */
    public void setCommentable(boolean commentable)
    {
        dirty(_commentable, commentable);
        _commentable = commentable;
    }

    /**
     * @return Разъяснение.
     */
    public String getHelp()
    {
        return _help;
    }

    /**
     * @param help Разъяснение.
     */
    public void setHelp(String help)
    {
        dirty(_help, help);
        _help = help;
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     */
    @NotNull
    public boolean isLaborContract()
    {
        return _laborContract;
    }

    /**
     * @param laborContract Трудовой договор. Свойство не может быть null.
     */
    public void setLaborContract(boolean laborContract)
    {
        dirty(_laborContract, laborContract);
        _laborContract = laborContract;
    }

    /**
     * @return Доп. соглашение к ТД. Свойство не может быть null.
     */
    @NotNull
    public boolean isAgreementLaborContract()
    {
        return _agreementLaborContract;
    }

    /**
     * @param agreementLaborContract Доп. соглашение к ТД. Свойство не может быть null.
     */
    public void setAgreementLaborContract(boolean agreementLaborContract)
    {
        dirty(_agreementLaborContract, agreementLaborContract);
        _agreementLaborContract = agreementLaborContract;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeOrderBasicsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EmployeeOrderBasics)another).getCode());
            }
            setCommentable(((EmployeeOrderBasics)another).isCommentable());
            setHelp(((EmployeeOrderBasics)another).getHelp());
            setLaborContract(((EmployeeOrderBasics)another).isLaborContract());
            setAgreementLaborContract(((EmployeeOrderBasics)another).isAgreementLaborContract());
            setTitle(((EmployeeOrderBasics)another).getTitle());
        }
    }

    public INaturalId<EmployeeOrderBasicsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EmployeeOrderBasicsGen>
    {
        private static final String PROXY_NAME = "EmployeeOrderBasicsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EmployeeOrderBasicsGen.NaturalId) ) return false;

            EmployeeOrderBasicsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeOrderBasicsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeOrderBasics.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeOrderBasics();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "commentable":
                    return obj.isCommentable();
                case "help":
                    return obj.getHelp();
                case "laborContract":
                    return obj.isLaborContract();
                case "agreementLaborContract":
                    return obj.isAgreementLaborContract();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "commentable":
                    obj.setCommentable((Boolean) value);
                    return;
                case "help":
                    obj.setHelp((String) value);
                    return;
                case "laborContract":
                    obj.setLaborContract((Boolean) value);
                    return;
                case "agreementLaborContract":
                    obj.setAgreementLaborContract((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "commentable":
                        return true;
                case "help":
                        return true;
                case "laborContract":
                        return true;
                case "agreementLaborContract":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "commentable":
                    return true;
                case "help":
                    return true;
                case "laborContract":
                    return true;
                case "agreementLaborContract":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "commentable":
                    return Boolean.class;
                case "help":
                    return String.class;
                case "laborContract":
                    return Boolean.class;
                case "agreementLaborContract":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeOrderBasics> _dslPath = new Path<EmployeeOrderBasics>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeOrderBasics");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Комментируемо. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#isCommentable()
     */
    public static PropertyPath<Boolean> commentable()
    {
        return _dslPath.commentable();
    }

    /**
     * @return Разъяснение.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#getHelp()
     */
    public static PropertyPath<String> help()
    {
        return _dslPath.help();
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#isLaborContract()
     */
    public static PropertyPath<Boolean> laborContract()
    {
        return _dslPath.laborContract();
    }

    /**
     * @return Доп. соглашение к ТД. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#isAgreementLaborContract()
     */
    public static PropertyPath<Boolean> agreementLaborContract()
    {
        return _dslPath.agreementLaborContract();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EmployeeOrderBasics> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _commentable;
        private PropertyPath<String> _help;
        private PropertyPath<Boolean> _laborContract;
        private PropertyPath<Boolean> _agreementLaborContract;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EmployeeOrderBasicsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Комментируемо. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#isCommentable()
     */
        public PropertyPath<Boolean> commentable()
        {
            if(_commentable == null )
                _commentable = new PropertyPath<Boolean>(EmployeeOrderBasicsGen.P_COMMENTABLE, this);
            return _commentable;
        }

    /**
     * @return Разъяснение.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#getHelp()
     */
        public PropertyPath<String> help()
        {
            if(_help == null )
                _help = new PropertyPath<String>(EmployeeOrderBasicsGen.P_HELP, this);
            return _help;
        }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#isLaborContract()
     */
        public PropertyPath<Boolean> laborContract()
        {
            if(_laborContract == null )
                _laborContract = new PropertyPath<Boolean>(EmployeeOrderBasicsGen.P_LABOR_CONTRACT, this);
            return _laborContract;
        }

    /**
     * @return Доп. соглашение к ТД. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#isAgreementLaborContract()
     */
        public PropertyPath<Boolean> agreementLaborContract()
        {
            if(_agreementLaborContract == null )
                _agreementLaborContract = new PropertyPath<Boolean>(EmployeeOrderBasicsGen.P_AGREEMENT_LABOR_CONTRACT, this);
            return _agreementLaborContract;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EmployeeOrderBasicsGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EmployeeOrderBasics.class;
        }

        public String getEntityName()
        {
            return "employeeOrderBasics";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
