/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;
import ru.tandemservice.moveemployee.entity.EmployeeModularParagraph;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author dseleznev
 * Created on: 13.11.2008
 */
public abstract class ModularEmployeeExtractAddEditDAO<T extends ModularEmployeeExtract, Model extends ModularEmployeeExtractAddEditModel<T>> extends UniDao<Model> implements IModularEmployeeExtractAddEditDAO<T, Model>
{
    @Override
    public void prepare(final Model model)
    {
        model.setAddForm(model.getExtractId() == null);
        model.setEditForm(!model.isAddForm());

        if (null != model.getEmployeeId())
            model.setEmployee(getNotNull(Employee.class, model.getEmployeeId()));

        if (null != model.getEmployeePost().getId())
            model.setEmployeePost(getNotNull(EmployeePost.class, model.getEmployeePost().getId()));

        if (null != model.getExtract())
            if(null != model.getEmployee())
                model.getExtract().setEmployee(model.getEmployee());
            else
                model.getExtract().setEmployee(model.getEmployeePost().getEmployee());

        // Making decision whereas the extract is being creating (Employee, or EmployeePost)
        // and filling up the list of extract types according to this decision.
        if (null != model.getEmployeePost().getId())
            model.setExtractTypeList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeePostExtractTypeList());
        else
            model.setExtractTypeList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeeExtractTypeList());

        //если выписка создается совместно с приказом, то выбираем только те приказы,
        //которые активные и указаны в настройке Правила формирования сборных приказов
        if (model.getExtractAsOrder() != null && model.getExtractAsOrder())
        {
            if (null != model.getEmployeePost().getId())
                model.setExtractTypeList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeePostExtractAsOrderTypeList());
            else
                model.setExtractTypeList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeeExtractAsOrderTypeList());

            model.setOrderList(new BaseSingleSelectModel()
            {
                @Override
                public ListResult<IdentifiableWrapper> findValues(String filter)
                {
                    //список приказов в состоянии Формируются и в которых нет выписок формирующихся индивидуально
                    List<EmployeeExtractType> employeeExtractIndividualTypeList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeePostExtractIndividualTypeList();
                    employeeExtractIndividualTypeList.addAll(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeeExtractIndividualTypeList());

                    MQBuilder subBuilder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "mee", new String[] {ModularEmployeeExtract.paragraph().order().id().s()});
                    subBuilder.add(MQExpression.in("mee", ModularEmployeeExtract.type().s(), employeeExtractIndividualTypeList));

                    MQBuilder builder = new MQBuilder(EmployeeModularOrder.ENTITY_CLASS, "emo");
                    builder.add(MQExpression.eq("emo", EmployeeModularOrder.state().code().s(), UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
                    builder.add(MQExpression.notIn("emo", EmployeeModularOrder.id().s(), subBuilder));
                    builder.addDescOrder("emo", EmployeeModularOrder.createDate().s());


                    if(StringUtils.isEmpty(filter))
                    {
                        List<IdentifiableWrapper> wrapperList = new ArrayList<>();
                        wrapperList.add(new IdentifiableWrapper(ModularEmployeeExtractAddEditModel.NEW_MODULAR_ORDER_ID, "--Новый приказ--"));

                        for(EmployeeModularOrder order : builder.<EmployeeModularOrder>getResultList(getSession()))
                        {
                            wrapperList.add(new IdentifiableWrapper(order.getId(), order.getTitleDate()));
                        }

                        return new ListResult<>(wrapperList);

                    }
                    else
                    {
                        String[] filterParts = StringUtils.split(filter, " ");
                        int i = 0;
                        for (String str : filterParts)
                        {
                            //поиск осуществляется для первых двух частей строки
                            i++;
                            try
                            {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                                Calendar dateFirst = GregorianCalendar.getInstance();
                                dateFirst.setTime(dateFormat.parse(str));
                                Calendar dateSecond = GregorianCalendar.getInstance();
                                dateSecond.setTime(dateFormat.parse(str));
                                dateSecond.set(Calendar.HOUR_OF_DAY, 23);
                                dateSecond.set(Calendar.MINUTE, 59);

                                //если в строке поиска введена дата,
                                //то проверяем ее на соответствие дате формирования или дате создания приказа,
                                //причем соответствие дате создания проверяем как вхождение в интервал, т.к. дата создания включает в себя время
                                AbstractExpression expr1 = MQExpression.eq("emo", EmployeeModularOrder.commitDate().s(), dateFirst.getTime());
                                AbstractExpression expr2 = MQExpression.greatOrEq("emo", EmployeeModularOrder.createDate().s(), dateFirst.getTime());
                                AbstractExpression expr3 = MQExpression.lessOrEq("emo", EmployeeModularOrder.createDate().s(), dateSecond.getTime());
                                builder.add(MQExpression.or(expr1, MQExpression.and(expr2, expr3)));
                            }
                            catch (Exception e)
                            {
                                try
                                {
                                    //если в строке поиска введен номер приказа/часть номера,
                                    //то проверяем его на вхождение в номера приказов
                                    builder.add(MQExpression.like("emo", EmployeeModularOrder.number().s(), CoreStringUtils.escapeLike(str)));
                                }
                                catch (Exception ee)
                                {
                                    continue;
                                }
                            }

                            //поиск осуществляется для первых двух частей строки
                            if (i == 2)
                                break;
                        }
                        List<IdentifiableWrapper> wrapperList = new ArrayList<>();
                        wrapperList.add(new IdentifiableWrapper(ModularEmployeeExtractAddEditModel.NEW_MODULAR_ORDER_ID, "--Новый приказ--"));

                        for(EmployeeModularOrder order : builder.<EmployeeModularOrder>getResultList(getSession()))
                        {
                            wrapperList.add(new IdentifiableWrapper(order.getId(), order.getTitleDate()));
                        }

                        return new ListResult<>(wrapperList);
                    }
                }

                @Override
                public String getLabelFor(Object value, int columnIndex)
                {
                    if (value instanceof IdentifiableWrapper)
                        return ((IdentifiableWrapper)value).getTitle();
                    return "";
                }

                @Override
                public Object getValue(Object primaryKey)
                {
                    for (IEntity entity : findValues("").getObjects())
                        if (entity.getId().equals(primaryKey))
                            return entity;

                    return null;
                }
            });
        }

        if (null != model.getExtractTypeId())
            model.setExtractType(get(EmployeeExtractType.class, model.getExtractTypeId()));

        if (model.isEditForm())
        {
            model.setExtract((T)get(model.getExtractId()));
            model.setExtractType(model.getExtract().getType());
        }
        else
            model.setExtract(createNewInstance());

        if (model.getExtract() != null)
        {
            //TODO: syncPreCommitData(model);
            if (model.isAddForm())
            {
                model.getExtract().setCreateDate(new Date());
                model.getExtract().setEmployee(model.getEmployee());
                model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE));

                GrammaCase rusCase = getEmployeeTitleCase();
                if (rusCase != null)
                {
                    IdentityCard identityCard = null != model.getEmployeePost().getId() ? model.getEmployeePost().getPerson().getIdentityCard() : model.getEmployee().getPerson().getIdentityCard();
                    boolean isMaleSex = identityCard.getSex().isMale();

                    StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex));
                    str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
                    if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
                        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));
                    model.getExtract().setEmployeeFioModified(str.toString());
                }
                else
                    model.getExtract().setEmployeeFioModified(model.getExtract().getEntity().getPerson().getFullFio());
            }
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        // TODO: We can`t use such validation scheme because we can create extract for employee and employeePost objects simultaneously
        if (model.isAddForm() && null != model.getEmployeePost().getId() && !MoveEmployeeDaoFacade.getMoveEmployeeDao().isMoveAccessible(model.getEmployeePost()))
            throw new ApplicationException("Нельзя добавить выписку, так как у сотрудника уже есть непроведенные выписки.");
    }

    @Override
    public void update(Model model)
    {
        // TODO: syncPreCommitData(model);
        if (model.isAddForm())
            model.getExtract().setType(model.getExtractType());

        //если выписка создается совместно с приказом,
        //то создаем новый параграф и производим необходимые соответствующие действия
        if (model.getExtractAsOrder() != null && model.getExtractAsOrder())
        {
            EmployeeModularParagraph paragraph = new EmployeeModularParagraph();

            model.getOrder().setCommitDate(model.getCommitDate());
            model.getOrder().setNumber(model.getNumber());

            paragraph.setNumber(model.getOrder().getParagraphCount() + 1);
            paragraph.setOrder(model.getOrder());

            model.getExtract().setParagraph(paragraph);
            // TODO DEV-7291
            //model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));

            if (model.getOrder().getParagraphsSortRule() == null)
                model.getOrder().setParagraphsSortRule(get(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_ACTIVE, Boolean.TRUE));

            getSession().saveOrUpdate(model.getOrder());
            getSession().saveOrUpdate(paragraph);
        }

        getSession().saveOrUpdate(model.getExtract());
    }

    protected abstract GrammaCase getEmployeeTitleCase();

    protected abstract T createNewInstance();

    @Override
    public String getName()
    {
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();

        if (principalContext == null || principalContext.isAdmin())
            return null;

        return principalContext.getFio();
    }
}