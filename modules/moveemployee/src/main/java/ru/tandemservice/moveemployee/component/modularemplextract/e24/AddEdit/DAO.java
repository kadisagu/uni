/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e24.AddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.RemovalCombinationExtract;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 29.12.2011
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<RemovalCombinationExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.GENITIVE;
    }

    @Override
    protected RemovalCombinationExtract createNewInstance()
    {
        return new RemovalCombinationExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        if (model.isEditForm())
        {
            MQBuilder builder = new MQBuilder(CombinationPostStaffRateItem.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", CombinationPostStaffRateItem.combinationPost().s(), model.getExtract().getCombinationPost()));

            model.setCombinationPostStaffRateItemList(builder.<CombinationPostStaffRateItem>getResultList(getSession()));
        }

        model.setCombinationPostModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(CombinationPost.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", CombinationPost.employeePost().s(), model.getEmployeePost()));
                if (o != null)
                    builder.add(MQExpression.eq("b", CombinationPost.id().s(), o));
                builder.addOrder("b", CombinationPost.postBoundedWithQGandQL().title().s());

                return new MQListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                CombinationPost post = (CombinationPost) value;
                return post.getPostBoundedWithQGandQL().getTitle() + " (" + post.getOrgUnit().getTitle() + ") " +
                        " - " + post.getCombinationPostType().getShortTitle() +
                        "; период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(post.getBeginDate()) +
                        (post.getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(post.getEndDate()) : "");
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (!model.getExtract().getRemovalDate().after(model.getExtract().getCombinationPost().getBeginDate()))
            errors.add("Дата снятия доплаты должна быть позже чем дата начала работ в совмещаемой должности.", "removalDate");
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());
    }

    @Override
    public void prepareEmployeeStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getEmployeeStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getEmployeeStaffRateDataSource(), staffRateItems);
    }

    @Override
    public void prepareCombinationStaffRateDataSource(Model model)
    {
        model.getCombinationStaffRateDataSource().setCountRow(model.getCombinationPostStaffRateItemList().size());
        UniBaseUtils.createPage(model.getCombinationStaffRateDataSource(), model.getCombinationPostStaffRateItemList());
    }
}