package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EncouragementToPrintFormatRelation;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь поощрений и наград с их форматами печати
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EncouragementToPrintFormatRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EncouragementToPrintFormatRelation";
    public static final String ENTITY_NAME = "encouragementToPrintFormatRelation";
    public static final int VERSION_HASH = -830445301;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENCOURAGEMENT_TYPE = "encouragementType";
    public static final String P_PRINT_FORMAT = "printFormat";

    private EncouragementType _encouragementType;     // Тип поощрения/награды
    private String _printFormat;     // Формат вывода

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип поощрения/награды. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EncouragementType getEncouragementType()
    {
        return _encouragementType;
    }

    /**
     * @param encouragementType Тип поощрения/награды. Свойство не может быть null и должно быть уникальным.
     */
    public void setEncouragementType(EncouragementType encouragementType)
    {
        dirty(_encouragementType, encouragementType);
        _encouragementType = encouragementType;
    }

    /**
     * @return Формат вывода.
     */
    @Length(max=255)
    public String getPrintFormat()
    {
        return _printFormat;
    }

    /**
     * @param printFormat Формат вывода.
     */
    public void setPrintFormat(String printFormat)
    {
        dirty(_printFormat, printFormat);
        _printFormat = printFormat;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EncouragementToPrintFormatRelationGen)
        {
            setEncouragementType(((EncouragementToPrintFormatRelation)another).getEncouragementType());
            setPrintFormat(((EncouragementToPrintFormatRelation)another).getPrintFormat());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EncouragementToPrintFormatRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EncouragementToPrintFormatRelation.class;
        }

        public T newInstance()
        {
            return (T) new EncouragementToPrintFormatRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "encouragementType":
                    return obj.getEncouragementType();
                case "printFormat":
                    return obj.getPrintFormat();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "encouragementType":
                    obj.setEncouragementType((EncouragementType) value);
                    return;
                case "printFormat":
                    obj.setPrintFormat((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "encouragementType":
                        return true;
                case "printFormat":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "encouragementType":
                    return true;
                case "printFormat":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "encouragementType":
                    return EncouragementType.class;
                case "printFormat":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EncouragementToPrintFormatRelation> _dslPath = new Path<EncouragementToPrintFormatRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EncouragementToPrintFormatRelation");
    }
            

    /**
     * @return Тип поощрения/награды. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EncouragementToPrintFormatRelation#getEncouragementType()
     */
    public static EncouragementType.Path<EncouragementType> encouragementType()
    {
        return _dslPath.encouragementType();
    }

    /**
     * @return Формат вывода.
     * @see ru.tandemservice.moveemployee.entity.EncouragementToPrintFormatRelation#getPrintFormat()
     */
    public static PropertyPath<String> printFormat()
    {
        return _dslPath.printFormat();
    }

    public static class Path<E extends EncouragementToPrintFormatRelation> extends EntityPath<E>
    {
        private EncouragementType.Path<EncouragementType> _encouragementType;
        private PropertyPath<String> _printFormat;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип поощрения/награды. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EncouragementToPrintFormatRelation#getEncouragementType()
     */
        public EncouragementType.Path<EncouragementType> encouragementType()
        {
            if(_encouragementType == null )
                _encouragementType = new EncouragementType.Path<EncouragementType>(L_ENCOURAGEMENT_TYPE, this);
            return _encouragementType;
        }

    /**
     * @return Формат вывода.
     * @see ru.tandemservice.moveemployee.entity.EncouragementToPrintFormatRelation#getPrintFormat()
     */
        public PropertyPath<String> printFormat()
        {
            if(_printFormat == null )
                _printFormat = new PropertyPath<String>(EncouragementToPrintFormatRelationGen.P_PRINT_FORMAT, this);
            return _printFormat;
        }

        public Class getEntityClass()
        {
            return EncouragementToPrintFormatRelation.class;
        }

        public String getEntityName()
        {
            return "encouragementToPrintFormatRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
