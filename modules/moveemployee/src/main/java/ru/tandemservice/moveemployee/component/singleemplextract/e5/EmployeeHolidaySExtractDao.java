/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e5;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Date;
import java.util.Map;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 23.09.2009
 */
public class EmployeeHolidaySExtractDao extends UniBaseDao implements IExtractComponentDao<EmployeeHolidaySExtract>
{
    @Override
    public void doCommit(EmployeeHolidaySExtract extract, Map parameters)
    {
        extract.setOldEmployeePostStatus(extract.getEntity().getPostStatus());
        extract.getEntity().setPostStatus(extract.getNewEmployeePostStatus());

        Date periodBeginDate = extract.getFirstPeriodBeginDate();
        Date periodEndDate = getEndPeriod(extract);

        EmployeeHoliday holiday1 = createOrAssignEmployeeHoliday(extract.getEmployeeFirstHoliday(), extract.getEntity(), periodBeginDate, periodEndDate, getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL), extract.getMainHolidayBeginDate(), extract.getMainHolidayEndDate(), extract.getMainHolidayDuration(), extract.getChildNumber());
        if (holiday1 != null)
        {
            holiday1.setHolidayExtract(extract);
            holiday1.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
            holiday1.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());
        }

        periodBeginDate = extract.getFirstPeriodBeginDate();
        periodEndDate = null != extract.getSecondPeriodEndDate() ? extract.getSecondPeriodEndDate() : extract.getFirstPeriodEndDate();

        EmployeeHoliday holiday2 = createOrAssignEmployeeHoliday(extract.getEmployeeSecondHoliday(), extract.getEntity(), periodBeginDate, periodEndDate, extract.getHolidayType(), extract.getSecondHolidayBeginDate(), extract.getSecondHolidayEndDate(), extract.getSecondHolidayDuration(), extract.getChildNumber());
        if (holiday2 != null)
        {
            holiday2.setHolidayExtract(extract);
            holiday2.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
            holiday2.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());
        }

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        if(null != extract.getEmployeeFirstHoliday() && null != extract.getEmployeeFirstHoliday().getId() && null == holiday1)
        {
            EmployeeHoliday holidayToDel = extract.getEmployeeFirstHoliday();
            extract.setEmployeeFirstHoliday(null);
            delete(holidayToDel);
        }
        
        if(null != extract.getEmployeeSecondHoliday() && null != extract.getEmployeeSecondHoliday().getId() && null == holiday2)
        {
            EmployeeHoliday holidayToDel = extract.getEmployeeSecondHoliday();
            extract.setEmployeeSecondHoliday(null);
            delete(holidayToDel);
        }
        
        if(null != holiday1)
        {
            getSession().saveOrUpdate(holiday1);
            extract.setEmployeeFirstHoliday(holiday1);

            if (extract.getVacationScheduleItem() != null)
            {
                extract.setOldScheduleItemFactDate(extract.getVacationScheduleItem().getFactDate());
                extract.getVacationScheduleItem().setFactDate(holiday1.getStartDate());
                if (extract.getVacationScheduleItem().getPlanDate() != null && !extract.getVacationScheduleItem().getPlanDate().equals(holiday1.getStartDate()))
                {
                    StringBuilder basicBuilder = new StringBuilder().
                            append("Приказ №").
                            append(extract.getParagraph().getOrder().getNumber()).
                            append(" от ").
                            append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));
                    if (extract.getVacationScheduleItem().getPostponeBasic() != null)
                    {
                        extract.setOldScheduleItemPostponeBasic(extract.getVacationScheduleItem().getPostponeBasic());
                        extract.getVacationScheduleItem().setPostponeBasic(extract.getVacationScheduleItem().getPostponeBasic() + "; " + basicBuilder.toString());
                    }
                    else
                    {
                        extract.setOldScheduleItemPostponeBasic(extract.getVacationScheduleItem().getPostponeBasic());
                        extract.setOldScheduleItemPostponeDate(extract.getVacationScheduleItem().getPostponeDate());
                        extract.getVacationScheduleItem().setPostponeBasic(basicBuilder.toString());
                        extract.getVacationScheduleItem().setPostponeDate(extract.getParagraph().getOrder().getCommitDate());
                    }
                }
            }
        }
        
        if(null != holiday2)
        {
            getSession().saveOrUpdate(holiday2);
            extract.setEmployeeSecondHoliday(holiday2);
        }
        
        update(extract);
    }

    @Override
    public void doRollback(EmployeeHolidaySExtract extract, Map parameters)
    {
        if(null != extract.getEmployeeFirstHoliday())
        {
            EmployeeHoliday holidayToDel = extract.getEmployeeFirstHoliday();
            extract.setEmployeeFirstHoliday(null);
            delete(holidayToDel);

            if (extract.getVacationScheduleItem() != null)
            {
                extract.getVacationScheduleItem().setFactDate(extract.getOldScheduleItemFactDate());
                extract.setOldScheduleItemFactDate(null);

                if (extract.getOldScheduleItemPostponeBasic() == null)
                {
                    extract.getVacationScheduleItem().setPostponeDate(extract.getOldScheduleItemPostponeDate());
                    extract.setOldScheduleItemPostponeDate(null);
                }

                extract.getVacationScheduleItem().setPostponeBasic(extract.getOldScheduleItemPostponeBasic());
                extract.setOldScheduleItemPostponeBasic(null);
            }
        }
        
        if(null != extract.getEmployeeSecondHoliday())
        {
            EmployeeHoliday holidayToDel = extract.getEmployeeSecondHoliday();
            extract.setEmployeeSecondHoliday(null);
            delete(holidayToDel);
        }
        
        extract.getEntity().setPostStatus(extract.getOldEmployeePostStatus());
        update(extract);
    }
    
    protected EmployeeHoliday createOrAssignEmployeeHoliday(EmployeeHoliday holiday, EmployeePost post, Date periodBeginDate, Date periodEndDate, HolidayType type, Date startDate, Date finishDate, Integer duration, Integer childNumber)
    {
        if(null == duration || null == startDate) return null; //We Have To dell this Holiday

        if(null == holiday) holiday = new EmployeeHoliday();
        
        holiday.setHolidayType(type);
        holiday.setEmployeePost(post);
        holiday.setBeginDate(periodBeginDate);
        holiday.setEndDate(periodEndDate);
        holiday.setStartDate(startDate);
        holiday.setFinishDate(finishDate);
        holiday.setDuration(duration);
        holiday.setChildNumber(childNumber);

        holiday.setTitle(type.getTitle() + " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startDate) + " на " + CommonBaseDateUtil.getDaysCountWithName(duration));
        
        return holiday;
    }

    protected Date getEndPeriod(EmployeeHolidaySExtract extract)
    {
        return null != extract.getSecondPeriodEndDate() ? extract.getSecondPeriodEndDate() : extract.getFirstPeriodEndDate();
    }
}