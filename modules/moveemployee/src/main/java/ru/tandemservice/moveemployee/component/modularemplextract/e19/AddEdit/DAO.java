/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e19.AddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeActingExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.11.2011
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<EmployeeActingExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());
        else
            model.getExtract().setEntity(model.getEmployeePost());

        model.setMissingEmployeePostModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", EmployeePost.employee().archival().s(), Boolean.FALSE));
                String[] filters = filter.trim().split(" ");
                if(filters.length > 0)
                {
                    builder.add(MQExpression.like("b", EmployeePost.person().identityCard().lastName().s(),
                            CoreStringUtils.escapeLike(filters[0])));
                    if(filters.length >= 2)
                    {
                        builder.add(MQExpression.like("b", EmployeePost.person().identityCard().firstName().s(),
                            CoreStringUtils.escapeLike(filters[1])));
                    }
                }
                if (o != null)
                    builder.add(MQExpression.eq("b", EmployeePost.id().s(), o));
                builder.addOrder("b", EmployeePost.person().identityCard().lastName().s());
                builder.addOrder("b", EmployeePost.person().identityCard().firstName().s());
                builder.addOrder("b", EmployeePost.person().identityCard().middleName().s());

                return new MQListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((EmployeePost) value).getFullTitle();
            }
        });

        model.setMissingOrgUnitModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                OrgUnit orgUnit = get(OrgUnit.class, (Long) primaryKey);
                if (findValues("").getObjects().contains(orgUnit))
                    return orgUnit;
                return null;
            }

            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", OrgUnit.archival().s(), Boolean.FALSE));
                builder.add(MQExpression.like("b", OrgUnit.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("b", OrgUnit.title().s());

                return new ListResult<>(builder.<OrgUnit>getResultList(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnit) value).getTitleWithType();
            }
        });

        model.setMissingPostModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", PostBoundedWithQGandQL.title().s(), CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", PostBoundedWithQGandQL.id().s(), o));
                builder.addOrder("b", PostBoundedWithQGandQL.title().s());

                return new MQListResultBuilder(builder);
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (model.getExtract().getBeginDate().getTime() > model.getExtract().getEndDate().getTime())
            errors.add("Дата начала должна быть меньше даты окончания.", "beginDate", "endDate");
    }

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected EmployeeActingExtract createNewInstance()
    {
        return new EmployeeActingExtract();
    }
}