/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.commons;

import org.hibernate.Session;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;

/**
 * @author dseleznev
 * Created on: 30.10.2009
 */
public interface IMoveEmployeeMQBuilder
{
    // filter names ordered by name


    String COMMIT_DATE_FROM = "commitDateFrom";                      // Дата приказа с
    String COMMIT_DATE_TO = "commitDateTo";                          // Дата приказа по
    String COMMIT_DATE_SYSTEM_FROM = "commitDateSystemFrom";         // Дата проведения приказа с
    String COMMIT_DATE_SYSTEM_TO = "commitDateSystemTo";             // Дата проведения приказа по
    String CREATE_DATE_FROM = "createDateFrom";                      // Дата формирования приказа (выписки) с
    String CREATE_DATE_TO = "createDateTo";                          // Дата формирования приказа (выписки) по

    String COMMIT_DATE = "commitDate";                 // Дата приказа
    String COMMIT_DATE_SYSTEM = "commitDateSystem";    // Дата проведения приказа
    String COMPENSATION_TYPE = "compensationType";     // Вид возмещения затрат
    String CREATE_DATE = "createDate";                 // Дата формирования приказа (выписки)
    String LAST_NAME = "lastName";                     // Фамилия сотрудника
    String NUMBER = "number";                          // № приказа
    String STATE = "state";                            // Состояние приказа (выписки)
    String TYPE = "type";                              // Тип приказа (выписки)

    // builder alias ordered by name

    String EXTRACT_ALIAS = "_extract";
    String IDENTITY_CARD_ALIAS = "_idCard";
    String ORDER_ALIAS = "_order";
    String PARAGRAPH_ALIAS = "_paragraph";
    String STATE_ALIAS = "_state";
    String EMPLOYEE_ALIAS = "_employee";

    // MQBuilder delegate method

    MQBuilder getMQBuilder();

    // order filters

    void applyOrderCommitDateFromTo();

    void applyOrderCommitDateSystemFromTo();

    void applyOrderCreateDateFromTo();

    void applyListOrderType();

    void applyOrderCommitDate();

    void applyOrderCommitDateSystem();

    void applyOrderCreateDate();

    void applyOrderNumber();

    void applyOrderState();

    // extract filters ordered by name

    void applyExtractCreateDate();

    void applyExtractLastName();

    void applyExtractState();

    void applyExtractType();

    void applyListExtractType();

    // create page

    @SuppressWarnings("unchecked")
    void createPage(Session session, OrderDescriptionRegistry orderDescriptionRegistry, DynamicListDataSource dataSource);
}