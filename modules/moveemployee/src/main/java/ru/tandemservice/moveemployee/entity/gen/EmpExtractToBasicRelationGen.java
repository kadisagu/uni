package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmpExtractToBasicRelation;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки с основанием. В выписке есть список оснований. (сборный приказ по кадрам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmpExtractToBasicRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmpExtractToBasicRelation";
    public static final String ENTITY_NAME = "empExtractToBasicRelation";
    public static final int VERSION_HASH = -1510881803;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_BASIC = "basic";
    public static final String P_COMMENT = "comment";

    private ModularEmployeeExtract _extract;     // Выписка (сборный приказ по кадрам)
    private EmployeeOrderBasics _basic;     // Основание выписки
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка (сборный приказ по кадрам).
     */
    public ModularEmployeeExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка (сборный приказ по кадрам).
     */
    public void setExtract(ModularEmployeeExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Основание выписки.
     */
    public EmployeeOrderBasics getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание выписки.
     */
    public void setBasic(EmployeeOrderBasics basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmpExtractToBasicRelationGen)
        {
            setExtract(((EmpExtractToBasicRelation)another).getExtract());
            setBasic(((EmpExtractToBasicRelation)another).getBasic());
            setComment(((EmpExtractToBasicRelation)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmpExtractToBasicRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmpExtractToBasicRelation.class;
        }

        public T newInstance()
        {
            return (T) new EmpExtractToBasicRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "basic":
                    return obj.getBasic();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((ModularEmployeeExtract) value);
                    return;
                case "basic":
                    obj.setBasic((EmployeeOrderBasics) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "basic":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "basic":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return ModularEmployeeExtract.class;
                case "basic":
                    return EmployeeOrderBasics.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmpExtractToBasicRelation> _dslPath = new Path<EmpExtractToBasicRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmpExtractToBasicRelation");
    }
            

    /**
     * @return Выписка (сборный приказ по кадрам).
     * @see ru.tandemservice.moveemployee.entity.EmpExtractToBasicRelation#getExtract()
     */
    public static ModularEmployeeExtract.Path<ModularEmployeeExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Основание выписки.
     * @see ru.tandemservice.moveemployee.entity.EmpExtractToBasicRelation#getBasic()
     */
    public static EmployeeOrderBasics.Path<EmployeeOrderBasics> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.moveemployee.entity.EmpExtractToBasicRelation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends EmpExtractToBasicRelation> extends EntityPath<E>
    {
        private ModularEmployeeExtract.Path<ModularEmployeeExtract> _extract;
        private EmployeeOrderBasics.Path<EmployeeOrderBasics> _basic;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка (сборный приказ по кадрам).
     * @see ru.tandemservice.moveemployee.entity.EmpExtractToBasicRelation#getExtract()
     */
        public ModularEmployeeExtract.Path<ModularEmployeeExtract> extract()
        {
            if(_extract == null )
                _extract = new ModularEmployeeExtract.Path<ModularEmployeeExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Основание выписки.
     * @see ru.tandemservice.moveemployee.entity.EmpExtractToBasicRelation#getBasic()
     */
        public EmployeeOrderBasics.Path<EmployeeOrderBasics> basic()
        {
            if(_basic == null )
                _basic = new EmployeeOrderBasics.Path<EmployeeOrderBasics>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.moveemployee.entity.EmpExtractToBasicRelation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EmpExtractToBasicRelationGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return EmpExtractToBasicRelation.class;
        }

        public String getEntityName()
        {
            return "empExtractToBasicRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
