/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;

import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author dseleznev
 * Created on: 20.01.2009
 */
public class EmployeeModularOrderPrint implements IPrintFormCreator<EmployeeModularOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeModularOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier modifier = CommonExtractPrint.createModularOrderInjectModifier(order);
        modifier.modify(document);

        RtfTableModifier tableModifier = CommonExtractPrint.createModularOrderTableModifier(order);
        tableModifier.modify(document);

        injectExtracts(document, order);

        return document;
    }

    @SuppressWarnings({ "unchecked", "null" })
    protected void injectExtracts(final RtfDocument document, EmployeeModularOrder order)
    {
        // 1. ищем ключевое слово EXTRACTS
        List<IRtfElement> elementList = null;
        int index = 0;
        boolean found = false;
        for (IRtfElement element : document.getElementList())
        {
            if (element instanceof RtfParagraph)
            {
                index = 0;
                elementList = ((RtfParagraph) element).getElementList();
                while (index < elementList.size() && !(elementList.get(index) instanceof RtfField && "EXTRACTS".equals(((RtfField) elementList.get(index)).getFieldName())))
                    index++;
                if (index < elementList.size())
                {
                    found = true;
                    break;
                }
            }
        }

        // 2. Если нашли, то вместо него вставляем все выписки
        if (found)
        {
            List<IRtfElement> parList = new ArrayList<IRtfElement>();

            for (IAbstractParagraph<IAbstractOrder> paragraph : order.getParagraphList())
            {
                for (IAbstractExtract extract : paragraph.getExtractList())
                {
                    // создаем выписку
                    byte[] extractTemplate = MoveEmployeeDaoFacade.getMoveEmployeeDao().getTemplate((EmployeeExtractType) extract.getType(), MoveEmployeeDefines.EXTRACT_IN_ORDER_TEXT_CODE);

                    String printName = EntityRuntime.getMeta(extract).getName() + "_extractPrint";
                    IPrintFormCreator componentPrint = (IPrintFormCreator) ApplicationRuntime.getBean(printName);
                    RtfDocument extractDocument = componentPrint.createPrintForm(extractTemplate, extract);

                    // подготавливаем ее для вставки в приказ
                    RtfUtil.modifySourceList(document.getHeader(), extractDocument.getHeader(), extractDocument.getElementList());

                    // дальше добавляем выписку
                    parList.addAll(extractDocument.getElementList());

                    // в конец добавляем перевод строки
                    parList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                }
            }

            // полученный список вставляем вместо ключевого слова
            elementList.remove(index);
            elementList.addAll(index, parList);
        }
    }
}
