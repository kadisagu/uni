/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.utils.system;

/**
 * Create by ashaburov
 * Date 27.04.2011
 */
public class ListExtractData
{
    //список всех entity выписок из списочных приказов
    public static final String[][] EXTRACT_LIST = new String[][]
            {
                    /* 1 */ {"employeeAddEmplListExtract", "О приеме сотрудника", "-"},
                    /* 2 */ {"employeeTransferEmplListExtract", "О переводе", "-"},
                    /* 3 */ {"employmentDismissalEmplListExtract", "Об увольнении работников", "-"},
                    /* 4 */ {"employeeHolidayEmplListExtract", "О предоставлении отпуска работникам", "-"},
                    /* 5 */ {"employeePaymentEmplListExtract", "О премировании работников", "-"}
            };
}