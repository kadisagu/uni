/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphPub;

import java.util.List;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;

/**
 * @author ashaburov
 * Created on: 11.10.2011
 */
public class AbstractListParagraphPubDAO<T extends ListEmployeeExtract, Model extends AbstractListParagraphPubModel<T>> extends UniDao<Model> implements IAbstractListParagraphPubDAO<T, Model>
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(Model model)
    {
        model.setParagraph(getNotNull(EmployeeListParagraph.class, model.getParagraphId()));
        model.setExtractList((List<T>) model.getParagraph().getExtractList());
        model.setSecModel(new CommonPostfixPermissionModel("listEmployeeExtract"));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getExtractsDataSource(), (List<T>) model.getParagraph().getExtractList());
    }
}