/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e6.ParagraphAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class Model extends AbstractListParagraphAddEditModel<ChangeFinancingSourceEmplListExtract>
{
    private ISingleSelectModel _financingSourceModel;
    private ISingleSelectModel _financingSourceItemModel;
    private ISingleSelectModel _orgUnitModel;
    private IMultiSelectModel _staffRateModel;

    private DynamicListDataSource _employeeDataSource;

    private Boolean _childOrgUnit;

    private Map<Long, String> _staffRateMap;

    private List<EmployeePost> _selectEmployeeList;
    private List<OrgUnit> _orgUnitList;
    private List<EmployeePost> _employeePostList;

    private FinancingSource _financingSource;
    private FinancingSourceItem _financingSourceItem;
    private OrgUnit _orgUnit;
    private Date _changeDate;

    private Map<Long, List<EmployeePostStaffRateItem>> _valueMap;

    //Getters & Setters

    public String getStaffRateMSId()
    {
        return "staffRateMSId_" + getEmployeeDataSource().getCurrentEntity().getId();
    }

    public Map<Long, List<EmployeePostStaffRateItem>> getValueMap()
    {
        return _valueMap;
    }

    public void setValueMap(Map<Long, List<EmployeePostStaffRateItem>> valueMap)
    {
        _valueMap = valueMap;
    }

    public Date getChangeDate()
    {
        return _changeDate;
    }

    public void setChangeDate(Date changeDate)
    {
        _changeDate = changeDate;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    public void setFinancingSource(FinancingSource financingSource)
    {
        _financingSource = financingSource;
    }

    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        _financingSourceItem = financingSourceItem;
    }

    public List<EmployeePost> getEmployeePostList()
    {
        return _employeePostList;
    }

    public void setEmployeePostList(List<EmployeePost> employeePostList)
    {
        _employeePostList = employeePostList;
    }

    public List<EmployeePost> getSelectEmployeeList()
    {
        return _selectEmployeeList;
    }

    public void setSelectEmployeeList(List<EmployeePost> selectEmployeeList)
    {
        _selectEmployeeList = selectEmployeeList;
    }

    public List<OrgUnit> getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }

    public ISingleSelectModel getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(ISingleSelectModel financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public ISingleSelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISingleSelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public IMultiSelectModel getStaffRateModel()
    {
        return _staffRateModel;
    }

    public void setStaffRateModel(IMultiSelectModel staffRateModel)
    {
        _staffRateModel = staffRateModel;
    }

    public DynamicListDataSource getEmployeeDataSource()
    {
        return _employeeDataSource;
    }

    public void setEmployeeDataSource(DynamicListDataSource employeeDataSource)
    {
        _employeeDataSource = employeeDataSource;
    }

    public Boolean getChildOrgUnit()
    {
        return _childOrgUnit;
    }

    public void setChildOrgUnit(Boolean childOrgUnit)
    {
        _childOrgUnit = childOrgUnit;
    }

    public Map<Long, String> getStaffRateMap()
    {
        return _staffRateMap;
    }

    public void setStaffRateMap(Map<Long, String> staffRateMap)
    {
        _staffRateMap = staffRateMap;
    }
}
