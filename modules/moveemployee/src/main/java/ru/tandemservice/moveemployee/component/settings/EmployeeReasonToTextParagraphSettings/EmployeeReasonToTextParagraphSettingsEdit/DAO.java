/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeReasonToTextParagraphSettings.EmployeeReasonToTextParagraphSettingsEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.uni.dao.UniDao;

/**
 * Create by: ashaburov
 * Date: 19.04.11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReason(get(EmployeeOrderReasons.class, model.getEmployeeOrderReasonId()));

        MQBuilder builder = new MQBuilder(EmployeeReasonToTextParagraphRel.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", EmployeeReasonToTextParagraphRel.L_EMPLOYEE_ORDER_REASON, model.getReason()));
        model.setSetting( (EmployeeReasonToTextParagraphRel) builder.uniqueResult(getSession()));

        if (model.getSetting() != null)
            model.setTextParagraph(model.getSetting().getTextParagraph());
    }

    @Override
    public void update(Model model)
    {
        if (model.getTextParagraph() == null && model.getSetting() != null)
            delete(model.getSetting());
        else if (model.getTextParagraph() != null && model.getSetting() != null)
        {
            model.getSetting().setTextParagraph(model.getTextParagraph());
            update(model.getSetting());
        }
        else if (model.getTextParagraph() != null)
        {
            EmployeeReasonToTextParagraphRel rel = new EmployeeReasonToTextParagraphRel();
            rel.setTextParagraph(model.getTextParagraph());
            rel.setEmployeeOrderReason(model.getReason());
            save(rel);
        }
    }
}
