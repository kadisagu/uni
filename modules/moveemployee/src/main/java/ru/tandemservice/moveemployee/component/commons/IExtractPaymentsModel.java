/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 02.06.2010
 * 
 * Данный интерфейс описывает набор методов, необходимых для работы базовых механизмов 
 * добавления выплат в выписке по сотруднику.
 * 
 * Используется для выписок о приеме на работу, назначении на должность и назначении выплат сотрудникам.
 */
public interface IExtractPaymentsModel<T extends AbstractEmployeeExtract> extends ICommonEmployeeExtractAddEditModel<T>, IPaymentAddModel<T>
{
    /** Геттер для списка отображаемых(добавленных) долей ставки в сечлисте Ставка. */
    List<FinancingSourceDetails> getStaffRateItemList();
    /** Сеттер для списка отображаемых(добавленных) долей ставки в сечлисте Ставка. */
    void setStaffRateItemList(List<FinancingSourceDetails> staffRateItemList);

    // МЕТОДЫ ДЛЯ БЛОКА ДОБАВЛЕНИЯ ВЫПЛАТЫ


    // МЕТОДЫ ДЛЯ ДОБАВЛЕНИЯ ВЫПЛАТ В РЕЖИМЕ "СОГЛАСНО ШТАТНОМУ РАСПИСАНИЮ"

    /**
     * Геттер для списка выплат в режиме добавления выплат "согласно штатному расписанию" 
     */
    List<EmployeeBonus> getStaffListPaymentsList();

    /**
     * Сеттер для списка выплат в режиме добавления выплат "согласно штатному расписанию" 
     */
    void setStaffListPaymentsList(List<EmployeeBonus> staffListPaymentsList);


    
    // ВСПОМОГАТЕЛЬНЫЕ МЕТОДЫ


    /**
     * Геттер для переменной, отвечающей за видимость кнопки добавления выплат, согласно штатному расписанию
     */
    public boolean isAddStaffListPaymentsButtonVisible();

    /**
     * Сеттер для переменной, отвечающей за видимость кнопки добавления выплат, согласно штатному расписанию
     */
    public void setAddStaffListPaymentsButtonVisible(boolean addStaffListPaymentsButtonVisible);

    /**
     * Геттер для переменной, отвечающей за видимость блока ручного добавления выплат
     */
    public boolean isAddPaymentBlockVisible();

    /**
     * Сеттер для переменной, отвечающей за видимость блока ручного добавления выплат
     */
    public void setAddPaymentBlockVisible(boolean addPaymentBlockVisible);
    

    /**
     * Геттер для переменной, отвечающей за видимость блока добавления выплат согласно штатному расписанию
     */
    public boolean isAddStaffListPaymentsBlockVisible();

    /**
     * Сеттер для переменной, отвечающей за видимость блока добавления выплат согласно штатному расписанию
     */
    public void setAddStaffListPaymentsBlockVisible(boolean addStaffListPaymentsBlockVisible);


    /**
     * Геттер для текущего бонуса, используемого для построения блока дат при добавлении выплат по ШР
     */
    EmployeeBonus getCurrentStaffListPayment();

    /**
     * Сеттер для текущего бонуса, используемого для построения блока дат при добавлении выплат по ШР
     */
    void setCurrentStaffListPayment(EmployeeBonus currentStaffListPayment);
}