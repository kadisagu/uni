/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e7.ParagraphAddEdit;

import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<PayForHolidayDayEmplListExtract, Model>
{
    public void prepareEmployeeDataSource(Model model);
}
