/**
 *$Id$
 */
package ru.tandemservice.moveemployee.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnValidateEmployeePostExt;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

/**
 * Create by ashaburov
 * Date 28.04.12
 */
public class ValidateExt implements IOnValidateEmployeePostExt
{
    @Override
    public void onValidateExt(IUIAddon addon)
    {
        EmployeePost employeePost = ((EmployeePostAddEditUI) addon.getPresenter()).getEmployeePost();

        int count = 0;
        if (null == employeePost.getOrderNumber())
            count++;
        if (null == employeePost.getOrderDate())
            count++;
        if (1 == count)
            ContextLocal.getErrorCollector().add("Поля «Номер приказа» и «Дата проведения приказа» обязательны для заполнения одновременно.", "orderNumber", "orderDate");
    }
}
