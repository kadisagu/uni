/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatCasesEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;

/**
 * @author esych
 *         Created on: 17.01.2011
 */
@Input(keys = "Id", bindings = "entityId")
public class Model
{
    private Long _entityId;
    private IEntity _entity;
    private String _entityTitle;
    private boolean _hasPlurals;

    public Long getEntityId()
    {
        return _entityId;
    }

    public void setEntityId(Long value)
    {
        _entityId = value;
    }

    public IEntity getEntity()
    {
        return _entity;
    }

    public void setEntity(IEntity value)
    {
        _entity = value;
    }

    public void setEntityTitle(String value)
    {
        _entityTitle = value;
    }

    public String getEntityTitle()
    {
        return _entityTitle;
    }

    public boolean isHasPlurals()
    {
        return _hasPlurals;
    }

    public void setHasPlurals(boolean value)
    {
        _hasPlurals = value;
    }
}
