/**
 *:$
 */
package ru.tandemservice.moveemployee.component.settings.CreationExtractAsOrderSettings;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.CreationExtractAsOrderRule;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by: Shaburov
 * Date: 28.03.11
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder extractAsOrderBuilder = new MQBuilder(CreationExtractAsOrderRule.ENTITY_CLASS, "eo");
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.employeeExtractType().active().s(), true));
        extractAsOrderBuilder.addOrder("eo", CreationExtractAsOrderRule.employeeExtractType().title().s());
        List<CreationExtractAsOrderRule> ruleList = extractAsOrderBuilder.getResultList(getSession());

        model.getDataSource().setCountRow(ruleList.size());
        UniBaseUtils.createPage(model.getDataSource(), ruleList);
    }

    /**
     * проверяем, что для всех существующих выписок по сборным приказам есть даная настройка,<p>
     * если нет, то добавляем соответствующие записи в базу
     */
    @Override
    public void updateForNewExtracts()
    {
        List<EmployeeExtractType> employeeExtractTypeList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeeExtractTypeList();
        employeeExtractTypeList.addAll(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeePostExtractTypeList());

        MQBuilder extractAsOrderBuilder = new MQBuilder(CreationExtractAsOrderRule.ENTITY_CLASS, "eo");
        List<CreationExtractAsOrderRule> extractAsOrderBuilderList = extractAsOrderBuilder.getResultList(getSession());

        List<EmployeeExtractType> extractAsOrderTypeList = new ArrayList<>();
        for (CreationExtractAsOrderRule extractRule : extractAsOrderBuilderList)
            extractAsOrderTypeList.add(extractRule.getEmployeeExtractType());

        for (EmployeeExtractType extractType : employeeExtractTypeList)
        {
            if (!extractAsOrderTypeList.contains(extractType))
            {
                extractAsOrderTypeList.add(extractType);

                CreationExtractAsOrderRule newRule = new CreationExtractAsOrderRule();
                newRule.setEmployeeExtractType(extractType);
                newRule.setExtractAsOrder(false);
                newRule.setExtractIndividual(false);
                save(newRule);
            }
        }
    }

    @Override
    public void updateExtractAsOrder(long id)
    {
        CreationExtractAsOrderRule extractAsOrderRule = get(CreationExtractAsOrderRule.class, id);

        if (extractAsOrderRule != null)
        {
            extractAsOrderRule.setExtractAsOrder(!extractAsOrderRule.isExtractAsOrder());
            update(extractAsOrderRule);
        }
    }

    @Override
    public void updateExtractIndividual(long id)
    {
        CreationExtractAsOrderRule extractAsOrderRule = get(CreationExtractAsOrderRule.class, id);

        if (extractAsOrderRule != null)
        {
            extractAsOrderRule.setExtractIndividual(!extractAsOrderRule.isExtractIndividual());
            update(extractAsOrderRule);
        }
    }
}
