/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.moveemployee.base.ext.Employee.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.View.EmployeeView;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class EmployeeViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EmployeeView _employeeView;
    
    public static final String EMPLOYEE_ORDER_TAB = "employeeOrderTab";
    
    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_employeeView.employeePubTabPanelExtPoint())
                .addTab(componentTab(EMPLOYEE_ORDER_TAB, "ru.tandemservice.moveemployee.component.employee.EmployeeOrdersPub").permissionKey("viewTabEmployeeOrders").parameters("mvel:['" + ISecureRoleContext.SECURE_ROLE_CONTEXT + "':presenter.secureRoleContext]"))
                .create();
    }
}
