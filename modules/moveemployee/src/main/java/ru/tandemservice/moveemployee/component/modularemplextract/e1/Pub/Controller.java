/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e1.Pub;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubController;
import ru.tandemservice.moveemployee.entity.EmployeeAddExtract;

/**
 * @author dseleznev
 * Created on: 20.11.2008
 */
public class Controller extends ModularEmployeeExtractPubController<EmployeeAddExtract, IDAO, Model>
{
}