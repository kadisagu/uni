package ru.tandemservice.moveemployee.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.moveemployee.entity.gen.ModularEmployeeExtractGen;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.List;

public abstract class ModularEmployeeExtract extends ModularEmployeeExtractGen
{
    public static final String[] ORDER_TYPE_TITLE_KEY = new String[]{IAbstractExtract.L_TYPE, ICatalogItem.CATALOG_ITEM_TITLE};
    public static final String[] EMPLOYEE_FIO_KEY = new String[]{L_EMPLOYEE, Employee.L_PERSON, Person.P_FULLFIO};
    public static final String[] ORDER_CREATE_DATE_KEY = new String[]{IAbstractExtract.L_PARAGRAPH, IAbstractParagraph.L_ORDER, IAbstractOrder.P_CREATE_DATE};

    public static final String P_TITLE = "title";

    @Override
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        String employeeWord = null != getEmployee() ? (SexCodes.FEMALE.equals(getEmployee().getPerson().getIdentityCard().getSex().getCode()) ? "сотрудница" : "сотрудник") : "сотрудник";
        if (getParagraph() == null)
            return "Проект приказа «" + getType().getTitle() + "» | " + employeeWord + " " + getEmployeeTitle();
        else
            return "Выписка «" + getType().getTitle() + "» №" + getParagraph().getNumber() + " из приказа №" + getParagraph().getOrder().getNumber() + " | " + employeeWord + " " + getEmployeeTitle();
    }

    public String getBasicListStr()
    {
        List<EmpExtractToBasicRelation> list = UniDaoFacade.getCoreDao().getList(EmpExtractToBasicRelation.class, EmpExtractToBasicRelation.L_EXTRACT, this);
        String[] title = new String[list.size()];
        for (int i = 0; i < list.size(); i++)
            title[i] = list.get(i).getBasic().getTitle() + (StringUtils.isEmpty(list.get(i).getComment()) ? "" : " " + list.get(i).getComment());
        return StringUtils.join(title, ", ");
    }

    @Override
    public String getEmployeeTitle()
    {
        return getEmployee().getPerson().getFullFio();
    }

}