/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e1.ExtractAddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.moveemployee.component.commons.IEmployeePostModel;
import ru.tandemservice.moveemployee.component.commons.IPaymentsBonusModel;
import ru.tandemservice.moveemployee.component.commons.IStaffRateListModel;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit.AbstractListExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeAddEmplListExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 06.05.2009
 */
public class Model extends AbstractListExtractAddEditModel<EmployeeAddEmplListExtract> implements IEmployeePostModel<EmployeeAddEmplListExtract>, IPaymentsBonusModel<EmployeeAddEmplListExtract>, IStaffRateListModel<EmployeeAddEmplListExtract>
{
    private final String _paymentsPage = Model.class.getPackage().getName() + ".PaymentBlock";

    private List<FinancingSourceDetails> _oldStaffRateItemList = new ArrayList<>();
    private List<Double> _oldStaffRateList = new ArrayList<>();
    private IMultiSelectModel _employeeHRModel;
    private List<FinancingSourceDetailsToAllocItem> _detailsToAllocItemList = new ArrayList<>();
    private boolean _thereAnyActiveStaffList = false;
    private boolean _needUpdateDataSource = true;

    private DynamicListDataSource<FinancingSourceDetails> _staffRateDataSource;
    private List<FinancingSourceDetails> _staffRateItemList = new ArrayList<>();
    private ISingleSelectModel _financingSourceItemModel;
    private List<FinancingSource> _financingSourceModel;

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Location getLocation()
        {
            return null;  //Body of implemented method
        }
    };

    private List<PostType> _postTypesList;
    private List<CompetitionAssignmentType> _competitionTypeList;
    private ISelectModel _orgUnitListModel;
    private ISelectModel _postRelationListModel;
    private ISelectModel _raisingCoefficientListModel;

    private boolean _budgetStaffRateVisible = true;
    private boolean _offBudgetStaffRateVisible = true;
    
    private List<EtksLevels> _etksLevelsList;
    private List<HSelectOption> _paymentTypesList;
    private List<FinancingSource> _financingSourcesList;
    private ISelectModel _financingSourceItemsListModel;
    private List<LabourContractType> _labourContractTypesList;
    
    private List<EmployeeWeekWorkLoad> _weekWorkLoadsList;
    private List<EmployeeWorkWeekDuration> _workWeekDurationsList;

    private Integer _currentBonusNumber;
    private List<Integer> _bonusNumbers = new ArrayList<>();
    private Map<Integer, EmployeeBonus> _bonusMap = new HashMap<>();
    private Map<Integer, ISelectModel> _paymentListsMap = new HashMap<>();
    
    private List<EmployeeBonus> _bonusesToDel = new ArrayList<>();
    
    private boolean _postPPS;

    //payments fields
    private DynamicListDataSource<EmployeeBonus> _paymentDataSource;
    private ISingleSelectModel _paymentModel;
    private ISingleSelectModel _finSrcPaymentModel;
    private ISingleSelectModel _finSrcItemPaymentModel;
    private List<EmployeeBonus> _paymentList = new ArrayList<>();
    private boolean _addStaffListPaymentsButtonVisible = false;

    //Calculate

    public String getAssignDateId()
    {
        return "assignDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getBeginDateId()
    {
        return "beginDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getEndDateId()
    {
        return "endDate_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getPaymentId()
    {
        return "payment_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getAmountId()
    {
        return "amount_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcId()
    {
        return "finSrc_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItemId()
    {
        return "finSrcItem_Id_" + getPaymentDataSource().getCurrentEntity().getId();
    }
    //end payments fields

    //Getters & Setters

    public String getPaymentsPage()
    {
        return _paymentsPage;
    }

    public DynamicListDataSource<EmployeeBonus> getPaymentDataSource()
    {
        return _paymentDataSource;
    }

    public void setPaymentDataSource(DynamicListDataSource<EmployeeBonus> paymentDataSource)
    {
        _paymentDataSource = paymentDataSource;
    }

    public ISingleSelectModel getPaymentModel()
    {
        return _paymentModel;
    }

    public void setPaymentModel(ISingleSelectModel paymentModel)
    {
        _paymentModel = paymentModel;
    }

    public ISingleSelectModel getFinSrcPaymentModel()
    {
        return _finSrcPaymentModel;
    }

    public void setFinSrcPaymentModel(ISingleSelectModel finSrcPaymentModel)
    {
        _finSrcPaymentModel = finSrcPaymentModel;
    }

    public ISingleSelectModel getFinSrcItemPaymentModel()
    {
        return _finSrcItemPaymentModel;
    }

    public void setFinSrcItemPaymentModel(ISingleSelectModel finSrcItemPaymentModel)
    {
        _finSrcItemPaymentModel = finSrcItemPaymentModel;
    }

    public List<EmployeeBonus> getPaymentList()
    {
        return _paymentList;
    }

    public void setPaymentList(List<EmployeeBonus> paymentList)
    {
        _paymentList = paymentList;
    }

    public boolean isAddStaffListPaymentsButtonVisible()
    {
        return _addStaffListPaymentsButtonVisible;
    }

    public void setAddStaffListPaymentsButtonVisible(boolean addStaffListPaymentsButtonVisible)
    {
        _addStaffListPaymentsButtonVisible = addStaffListPaymentsButtonVisible;
    }

    public List<FinancingSourceDetails> getOldStaffRateItemList()
    {
        return _oldStaffRateItemList;
    }

    public void setOldStaffRateItemList(List<FinancingSourceDetails> oldStaffRateItemList)
    {
        _oldStaffRateItemList = oldStaffRateItemList;
    }

    public List<Double> getOldStaffRateList()
    {
        return _oldStaffRateList;
    }

    public void setOldStaffRateList(List<Double> oldStaffRateList)
    {
        _oldStaffRateList = oldStaffRateList;
    }

    @Override
    public List<FinancingSourceDetailsToAllocItem> getDetailsToAllocItemList()
    {
        return _detailsToAllocItemList;
    }

    @Override
    public void setDetailsToAllocItemList(List<FinancingSourceDetailsToAllocItem> detailsToAllocItemList)
    {
        _detailsToAllocItemList = detailsToAllocItemList;
    }

    @Override
    public boolean isThereAnyActiveStaffList()
    {
        return _thereAnyActiveStaffList;
    }

    @Override
    public void setThereAnyActiveStaffList(boolean thereAnyActiveStaffList)
    {
        _thereAnyActiveStaffList = thereAnyActiveStaffList;
    }

    @Override
    public boolean isNeedUpdateDataSource()
    {
        return _needUpdateDataSource;
    }

    @Override
    public void setNeedUpdateDataSource(boolean needUpdateDataSource)
    {
        _needUpdateDataSource = needUpdateDataSource;
    }

    public String getStaffRateColumnId()
    {
        return "staffRate_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcColumnId()
    {
        return "finSrc_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItmColumnId()
    {
        return "finSrcItm_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getEmployeeHRId()
    {
        return "employeeHR_Id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public IMultiSelectModel getEmployeeHRModel()
    {
        return _employeeHRModel;
    }

    public void setEmployeeHRModel(IMultiSelectModel employeeHRModel)
    {
        _employeeHRModel = employeeHRModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    @Override
    public DynamicListDataSource<FinancingSourceDetails> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    @Override
    public void setStaffRateDataSource(DynamicListDataSource<FinancingSourceDetails> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    @Override
    public List<FinancingSourceDetails> getStaffRateItemList()
    {
        return _staffRateItemList;
    }

    @Override
    public void setStaffRateItemList(List<FinancingSourceDetails> staffRateItemList)
    {
        _staffRateItemList = staffRateItemList;
    }

    @Override
    public List<FinancingSource> getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    @Override
    public void setFinancingSourceModel(List<FinancingSource> financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    @Override
    public List<PostType> getPostTypesList()
    {
        return _postTypesList;
    }

    @Override
    public void setPostTypesList(List<PostType> postTypesList)
    {
        this._postTypesList = postTypesList;
    }

    @Override
    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    @Override
    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        this._orgUnitListModel = orgUnitListModel;
    }

    @Override
    public ISelectModel getPostRelationListModel()
    {
        return _postRelationListModel;
    }

    @Override
    public void setPostRelationListModel(ISelectModel postRelationListModel)
    {
        this._postRelationListModel = postRelationListModel;
    }

    @Override
    public ISelectModel getRaisingCoefficientListModel()
    {
        return _raisingCoefficientListModel;
    }

    @Override
    public void setRaisingCoefficientListModel(ISelectModel raisingCoefficientListModel)
    {
        this._raisingCoefficientListModel = raisingCoefficientListModel;
    }

    public boolean isBudgetStaffRateVisible()
    {
        return _budgetStaffRateVisible;
    }

    public void setBudgetStaffRateVisible(boolean budgetStaffRateVisible)
    {
        this._budgetStaffRateVisible = budgetStaffRateVisible;
    }

    public boolean isOffBudgetStaffRateVisible()
    {
        return _offBudgetStaffRateVisible;
    }

    public void setOffBudgetStaffRateVisible(boolean offBudgetStaffRateVisible)
    {
        this._offBudgetStaffRateVisible = offBudgetStaffRateVisible;
    }

    @Override
    public List<EtksLevels> getEtksLevelsList()
    {
        return _etksLevelsList;
    }

    @Override
    public void setEtksLevelsList(List<EtksLevels> etksLevelsList)
    {
        this._etksLevelsList = etksLevelsList;
    }

    @Override
    public List<HSelectOption> getPaymentTypesList()
    {
        return _paymentTypesList;
    }

    @Override
    public void setPaymentTypesList(List<HSelectOption> paymentTypesList)
    {
        this._paymentTypesList = paymentTypesList;
    }

    @Override
    public List<FinancingSource> getFinancingSourcesList()
    {
        return _financingSourcesList;
    }

    @Override
    public void setFinancingSourcesList(List<FinancingSource> financingSourcesList)
    {
        this._financingSourcesList = financingSourcesList;
    }

    @Override
    public ISelectModel getFinancingSourceItemsListModel()
    {
        return _financingSourceItemsListModel;
    }

    @Override
    public void setFinancingSourceItemsListModel(ISelectModel financingSourceItemsListModel)
    {
        this._financingSourceItemsListModel = financingSourceItemsListModel;
    }

    @Override
    public List<LabourContractType> getLabourContractTypesList()
    {
        return _labourContractTypesList;
    }

    @Override
    public void setLabourContractTypesList(List<LabourContractType> labourContractTypesList)
    {
        this._labourContractTypesList = labourContractTypesList;
    }

    @Override
    public List<EmployeeWeekWorkLoad> getWeekWorkLoadsList()
    {
        return _weekWorkLoadsList;
    }

    @Override
    public void setWeekWorkLoadsList(List<EmployeeWeekWorkLoad> weekWorkLoadsList)
    {
        this._weekWorkLoadsList = weekWorkLoadsList;
    }

    @Override
    public List<EmployeeWorkWeekDuration> getWorkWeekDurationsList()
    {
        return _workWeekDurationsList;
    }

    @Override
    public void setWorkWeekDurationsList(List<EmployeeWorkWeekDuration> workWeekDurationsList)
    {
        this._workWeekDurationsList = workWeekDurationsList;
    }

    @Override
    public Integer getCurrentBonusNumber()
    {
        return _currentBonusNumber;
    }

    @Override
    public void setCurrentBonusNumber(Integer currentBonusNumber)
    {
        this._currentBonusNumber = currentBonusNumber;
    }

    @Override
    public List<Integer> getBonusNumbers()
    {
        return _bonusNumbers;
    }

    @Override
    public void setBonusNumbers(List<Integer> bonusNumbers)
    {
        this._bonusNumbers = bonusNumbers;
    }

    @Override
    public Map<Integer, EmployeeBonus> getBonusMap()
    {
        return _bonusMap;
    }

    @Override
    public void setBonusMap(Map<Integer, EmployeeBonus> bonusMap)
    {
        this._bonusMap = bonusMap;
    }

    @Override
    public Map<Integer, ISelectModel> getPaymentListsMap()
    {
        return _paymentListsMap;
    }

    @Override
    public void setPaymentListsMap(Map<Integer, ISelectModel> paymentListsMap)
    {
        this._paymentListsMap = paymentListsMap;
    }

    @Override
    public EmployeeBonus getCurrentEmployeeBonus()
    {
        return getBonusMap().get(getCurrentBonusNumber());
    }

    @Override
    public ISelectModel getCurrentPaymentsList()
    {
        return getPaymentListsMap().get(getCurrentBonusNumber());
    }

    @Override
    public String getCurrentBonusId()
    {
        return "payment" + getCurrentBonusNumber();
    }

    @Override
    public List<EmployeeBonus> getBonusesToDel()
    {
        return _bonusesToDel;
    }

    @Override
    public void setBonusesToDel(List<EmployeeBonus> bonusesToDel)
    {
        this._bonusesToDel = bonusesToDel;
    }

    public boolean isPostPPS()
    {
        return _postPPS;
    }

    public void setPostPPS(boolean postPPS)
    {
        this._postPPS = postPPS;
    }

    @Override
    public List<CompetitionAssignmentType> getCompetitionTypeList()
    {
        return _competitionTypeList;
    }

    @Override
    public void setCompetitionTypeList(List<CompetitionAssignmentType> competitionTypeList)
    {
        _competitionTypeList = competitionTypeList;
    }
}