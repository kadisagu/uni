/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.menuempl.SingleOrdersFormation;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "SingleEmplOrdersFormation.filter"));

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<SingleEmployeeExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Дата формирования", SingleEmployeeExtract.ORDER_CREATE_DATE_KEY, DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Тип приказа", SingleEmployeeExtract.ORDER_TYPE_TITLE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ФИО сотрудника", SingleEmployeeExtract.EMPLOYEE_FIO_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата приказа", SingleEmployeeExtract.ORDER_COMMIT_DATE_KEY, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", SingleEmployeeExtract.ORDER_NUMBER_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", SingleEmployeeExtract.ORDER_STATE_TITLE_KEY).setClickable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("print_menuList_singleEmployeeOrdersFormation"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("edit_menuList_singleEmployeeOrdersFormation").setDisabledProperty(SingleEmployeeExtract.P_NO_EDIT_ORDER));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить «{0}»?", IAbstractOrder.P_TITLE).setPermissionKey("delete_menuList_singleEmployeeOrdersFormation").setDisabledProperty(SingleEmployeeExtract.P_NO_DELETE));
        dataSource.setOrder(IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_CREATE_DATE, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        DataSettingsFacade.saveSettings(getModel(component).getSettings());
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.MODULAR_EMPLOYEE_EXTRACT_PRINT, new ParametersMap().add("extractId", component.getListenerParameter())));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        Long extractId = (Long)component.getListenerParameter();
        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getSingleExtractAddEditComponent(extractId), new ParametersMap().add("extractId", extractId)));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        IAbstractOrder order = ((SingleEmployeeExtract)UniDaoFacade.getCoreDao().get((Long)component.getListenerParameter())).getParagraph().getOrder();
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(order);
    }
}