/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.singleemplextract.e5.AddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendar;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendarHoliday;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendarHolidayDuration;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.Holiday;
import org.tandemframework.shared.employeebase.catalog.entity.HolidayDuration;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.organization.base.util.IHolidayDuration;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeHolidaySExtract;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.*;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 23.09.2009
 */
public class    DAO extends CommonSingleEmployeeExtractAddEditDAO<EmployeeHolidaySExtract, Model> implements IDAO
{
    protected static final String[] HOLIDAY_TYPES_FOR_PERIOD = new String[]
                                                                        {
        UniempDefines.HOLIDAY_TYPE_ANNUAL,
        UniempDefines.HOLIDAY_TYPE_HARMFUL_CONDITIONS,
        UniempDefines.HOLIDAY_TYPE_SOME_AREAS_OF_PUBLIC_CULTURE,
        UniempDefines.HOLIDAY_TYPE_NON_NORMED_WORK_DAY,
        UniempDefines.HOLIDAY_TYPE_EDGE_NORTH,
        UniempDefines.HOLIDAY_TYPE_WORKING_AT_POLLUTED_REGION,
        UniempDefines.HOLIDAY_TYPE_CHERNOBYL_LIQUIDATION,
        UniempDefines.HOLIDAY_TYPE_STATE_EMPLOYEE
                                                                        };

    @Override
    protected EmployeeHolidaySExtract createNewInstance()
    {
        return new EmployeeHolidaySExtract();
    }

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        List<EmployeePostStatus> postStatusesList = new ArrayList<>();
        postStatusesList.add(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        postStatusesList.add(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR));
        postStatusesList.add(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY));
        postStatusesList.add(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE));
        model.setPostStatusesList(postStatusesList);

        Map<String, EmployeePostStatus> postStatusesMap = new HashMap<>();
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE));
        model.setPostStatusesMap(postStatusesMap);

        model.setHolidayTypesListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(HolidayType.ENTITY_CLASS, "ht");
                builder.add(MQExpression.like("ht", HolidayType.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("ht", HolidayType.P_USER_CODE);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(HolidayType.class, (Long)primaryKey);
            }
        });

        model.setVacationScheduleModel(new CommonSingleSelectModel(VacationSchedule.P_TITLE_WITH_YEAR_AND_CREATE_DATE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (o != null)
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(VacationSchedule.class, "s").column("s");
                    subBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationSchedule.id().fromAlias("s")), o));
                    return new DQLListResultBuilder(subBuilder);
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "si").column(DQLExpressions.property(VacationScheduleItem.vacationSchedule().fromAlias("si")));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("si")), model.getEmployeePost()));
                builder.where(DQLExpressions.ge(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("si")), DQLExpressions.value(Calendar.getInstance().get(Calendar.YEAR))));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().state().code().fromAlias("si")), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
                builder.order(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("si")), OrderDirection.desc);
                builder.predicate(DQLPredicateType.distinct);

                return new DQLListResultBuilder(builder);
            }
        });

        model.setVacationScheduleItemModel(new CommonSingleSelectModel(VacationScheduleItem.P_PLANED_VACATION_TITLE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getVacationSchedule() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().fromAlias("i")), model.getExtract().getVacationSchedule()));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), model.getEmployeePost()));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.id().fromAlias("i")), o));
                builder.order(DQLExpressions.property(VacationScheduleItem.planDate().fromAlias("i")), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }
        });

        if (model.isAddForm())
        {
            model.getExtract().setEntity(model.getEmployeePost());
        }
        else
        {
            model.setEmployeePost(model.getExtract().getEntity());
        }

        //нужно ли отображать поля связанные с Графиком отпусков
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), model.getEmployeePost()));
        builder.where(DQLExpressions.ge(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("i")), DQLExpressions.value(Calendar.getInstance().get(Calendar.YEAR))));
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().state().code().fromAlias("i")), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
        model.setHasVacationSchedule(!builder.createStatement(getSession()).list().isEmpty());
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setOldEmployeePostStatus(model.getEmployeePost().getPostStatus());
        correctValuesBeforeSaving(model);
        super.update(model);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        EmployeeHolidaySExtract extract = model.getExtract();
        HolidayType holidayType = extract.getHolidayType();

        if (extract.getVacationSchedule() != null && extract.getVacationScheduleItem() == null)
            errors.add("Поле «Планируемый отпуск» обязательно для заполнения, если указан график отпусков.", "vacationScheduleItem", "vacationSchedule");

        boolean periodRequired = false;
        for (String code : HOLIDAY_TYPES_FOR_PERIOD)
            if (code.equals(holidayType.getCode()))
                periodRequired = true;

        if (periodRequired && (null == extract.getFirstPeriodBeginDate() || null == extract.getFirstPeriodEndDate()))
            errors.add("Необходимо указать период, за который предоставляется отпуск.", "firstPeriodBeginDate", "firstPeriodEndDate");

        if (null != extract.getFirstPeriodBeginDate() && null != extract.getFirstPeriodEndDate() && extract.getFirstPeriodBeginDate().getTime() > extract.getFirstPeriodEndDate().getTime())
            errors.add("Дата начала периода не может быть больше даты его окончания.", "firstPeriodBeginDate", "firstPeriodEndDate", "firstPeriodBeginDateReq", "firstPeriodEndDateReq");

        if (null != extract.getSecondPeriodBeginDate() && null != extract.getSecondPeriodEndDate() && extract.getSecondPeriodBeginDate().getTime() > extract.getSecondPeriodEndDate().getTime())
            errors.add("Дата начала периода не может быть больше даты его окончания.", "secondPeriodBeginDate", "secondPeriodEndDate");

        if (null != extract.getFirstPeriodBeginDate() && null != extract.getSecondPeriodBeginDate() && extract.getFirstPeriodBeginDate().getTime() >= extract.getSecondPeriodBeginDate().getTime())
            errors.add("Дата начала второго периода должна быть больше даты начала первого.", "firstPeriodBeginDate", "secondPeriodBeginDate", "firstPeriodBeginDateReq");

        if (null != extract.getFirstPeriodEndDate() && null != extract.getSecondPeriodEndDate() && extract.getFirstPeriodEndDate().getTime() >= extract.getSecondPeriodEndDate().getTime())
            errors.add("Дата окончания второго периода должна быть больше даты окончания первого.", "firstPeriodEndDate", "secondPeriodEndDate", "firstPeriodEndDateReq");

        if (null != extract.getFirstPeriodEndDate() && null != extract.getSecondPeriodBeginDate() && extract.getFirstPeriodEndDate().getTime() >= extract.getSecondPeriodBeginDate().getTime())
            errors.add("Периоды не могут пересекаться.", "firstPeriodBeginDate", "firstPeriodEndDate", "secondPeriodBeginDate", "secondPeriodEndDate", "firstPeriodBeginDateReq", "firstPeriodEndDateReq");

        if ((null == extract.getMainHolidayBeginDate() || null == extract.getMainHolidayEndDate()) && (null == extract.getSecondHolidayBeginDate() || null == extract.getSecondHolidayEndDate()))
            errors.add("Необходимо указать период отпуска.", "mainHolidayBeginDate", "mainHolidayEndDate", "secondHolidayBeginDate", "secondHolidayEndDate");

        if (null != extract.getMainHolidayBeginDate() && null != extract.getMainHolidayEndDate() && extract.getMainHolidayBeginDate().after(extract.getMainHolidayEndDate()))
            errors.add("Дата окончания ежегодного отпуска должна быть не раньше даты его начала.", "mainHolidayBeginDate", "mainHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq");

        if (null != extract.getSecondHolidayBeginDate() && null != extract.getSecondHolidayEndDate() && extract.getSecondHolidayBeginDate().getTime() > extract.getSecondHolidayEndDate().getTime())
            errors.add("Дата начала отпуска должна быть меньше даты его окончания", "secondHolidayBeginDate", "secondHolidayEndDate");

        if (null != extract.getMainHolidayBeginDate() && null != extract.getMainHolidayEndDate() && extract.getSecondHolidayBeginDate() != null && extract.getSecondHolidayEndDate() != null) {
            if (CommonBaseDateUtil.isBetween(extract.getMainHolidayBeginDate(), extract.getSecondHolidayBeginDate(), extract.getSecondHolidayEndDate()) || CommonBaseDateUtil.isBetween(extract.getMainHolidayEndDate(), extract.getSecondHolidayBeginDate(), extract.getSecondHolidayEndDate()))
                errors.add("Периоды ежегодного и дополнительного отпусков не могут пересекаться.", "mainHolidayBeginDate", "mainHolidayEndDate", "secondHolidayBeginDate", "secondHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq");
            else {
                if (CommonBaseDateUtil.isBetween(extract.getSecondHolidayBeginDate(), extract.getMainHolidayBeginDate(), extract.getMainHolidayEndDate()) || CommonBaseDateUtil.isBetween(extract.getSecondHolidayEndDate(), extract.getMainHolidayBeginDate(), extract.getMainHolidayEndDate()))
                    errors.add("Периоды ежегодного и дополнительного отпусков не могут пересекаться.", "mainHolidayBeginDate", "mainHolidayEndDate", "secondHolidayBeginDate", "secondHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq");
            }
        }

//        if (null != extract.getFirstPeriodEndDate() && null != extract.getSecondPeriodBeginDate() && extract.getSecondPeriodBeginDate().getTime() > extract.getFirstPeriodEndDate().getTime()
//                && CommonBaseDateUtil.getDifferenceBetweenDatesInDays(extract.getFirstPeriodEndDate(), extract.getSecondPeriodBeginDate()) != 2)
//            errors.add("Период, за который предоставляется отпуска не должен прерываться", "firstPeriodBeginDate", "firstPeriodEndDate", "secondPeriodBeginDate", "secondPeriodEndDate", "firstPeriodBeginDateReq", "firstPeriodEndDateReq");

        if (extract.getMainSecondHolidayEndDate() == null)//заглушка для ННГАСУ. там эта проверка не актуальна
        {
            if (null != extract.getMainHolidayEndDate() && null != extract.getSecondHolidayBeginDate() && extract.getSecondHolidayBeginDate().getTime() > extract.getMainHolidayEndDate().getTime()
                    && CommonBaseDateUtil.getDifferenceBetweenDatesInDays(extract.getMainHolidayEndDate(), extract.getSecondHolidayBeginDate()) != 2)
                errors.add("Период отпуска по одному приказу не должен прерываться.", "mainHolidayEndDate", "secondHolidayBeginDate", "mainHolidayEndDateReq");
        }

        if (model.getExtract().getMainHolidayBeginDate() != null && model.getExtract().getMainHolidayEndDate() != null
                && (model.getExtract().getFirstPeriodBeginDate() == null || model.getExtract().getFirstPeriodEndDate() == null))
            errors.add("При указании Ежегодного отпуска период отпуска обязателен для заполнения.", "firstPeriodBeginDate", "firstPeriodEndDate");

        HolidayDuration maxDuration = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getEmployeeType().getHolidayDuration();
        if (null != maxDuration)
        {
            if ((null == extract.getSecondPeriodBeginDate() || null == extract.getSecondHolidayEndDate()))
            {
                if (null != extract.getMainHolidayDuration() && extract.getMainHolidayDuration() > maxDuration.getDaysAmount())
                    errors.add("Ежегодный отпуск сотрудника не должен превышать " + CommonBaseDateUtil.getDaysCountWithName(maxDuration.getDaysAmount()) + ".", "mainHolidayDuration");
            }
            else
            {
                if (null != extract.getMainHolidayDuration() && extract.getMainHolidayDuration() > maxDuration.getDaysAmount() * 2)
                    errors.add("Ежегодный отпуск сотрудника за два периода не должен превышать " + CommonBaseDateUtil.getDaysCountWithName(maxDuration.getDaysAmount() * 2) + ".", "mainHolidayDuration");
            }
        }

        if (model.isNeedSecondHoliday() &&
                (model.getExtract().getMainHolidayBeginDate() != null ||
                        model.getExtract().getMainHolidayEndDate() != null ||
                        model.getExtract().getMainHolidayDuration() != null) &&
                        (model.getExtract().getMainHolidayBeginDate() == null ||
                                model.getExtract().getMainHolidayEndDate() == null ||
                                model.getExtract().getMainHolidayDuration() == null))
            errors.add("Все поля о ежегодном отпуске должны быть заполнены, либо оставаться пустыми.", "mainHolidayBeginDate", "mainHolidayEndDate", "mainHolidayDuration");

        //проверяем, что части отпуска не пересекаются с существующими отпусками сотрудника
        String[][] fields = new String[][]{{"mainHolidayBeginDate", "mainHolidayEndDate", "mainHolidayBeginDateReq", "mainHolidayEndDateReq"},{"secondHolidayBeginDate", "secondHolidayEndDate"} };
        Date[] beginHolidays = new Date[]{extract.getMainHolidayBeginDate(), extract.getSecondHolidayBeginDate()};
        Date[] endHolidays = new Date[]{extract.getMainHolidayEndDate(), extract.getSecondHolidayEndDate()};
        MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
        empHolidayBuilder.add(MQExpression.eq("eh", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
        for (int i = 0; i < 2; i++)
            for (EmployeeHoliday holiday : empHolidayBuilder.<EmployeeHoliday>getResultList(getSession()))
            {
                Date beginDate = beginHolidays[i];
                Date endDate = endHolidays[i];

                if (beginDate == null || endDate == null)
                    break;

                if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                    errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                            fields[i]);
                else {
                    if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                        errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                fields[i]);
                }
            }

        //если выбран график отпусков
        //проверяем пересечение планируемых отпусков внутри выбранного Графика отпусков
        if (extract.getVacationScheduleItem() != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
            builder.where(DQLExpressions.ne(DQLExpressions.property(VacationScheduleItem.id().fromAlias("i")), DQLExpressions.value(extract.getVacationScheduleItem().getId())));
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), model.getEmployeePost()));
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().fromAlias("i")), extract.getVacationSchedule()));

            Date beginDate = extract.getMainHolidayBeginDate();
            Date endDate = extract.getMainHolidayEndDate();

            for (VacationScheduleItem item : builder.createStatement(getSession()).<VacationScheduleItem>list())
            {
                Date beginItemDate = item.getFactDate() != null ? item.getFactDate() : item.getPlanDate();
                Date endItemDate = UniempUtil.getEndDate(model.getEmployeePost().getWorkWeekDuration(), item.getDaysAmount(), beginItemDate);

                if (beginItemDate == null || endItemDate == null)
                    continue;

                StringBuilder error = new StringBuilder("Предоставляемый отпуск пересекается с планируемым отпуском с ").
                        append(DateFormatter.DEFAULT_DATE_FORMATTER.format(beginItemDate)).
                        append(" на ").
                        append(NumberConvertingUtil.getCalendarDaysWithName(item.getDaysAmount())).
                        append(" из графика отпусков на ").
                        append(item.getVacationSchedule().getYear()).
                        append(" год от ").
                        append(DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getVacationSchedule().getCreateDate())).
                        append(".");

                if (CommonBaseDateUtil.isBetween(beginDate, beginItemDate, endItemDate) || CommonBaseDateUtil.isBetween(endDate, beginItemDate, endItemDate))
                    errors.add(error.toString(), "mainHolidayBeginDateReq", "mainHolidayEndDateReq", "mainHolidayDurationReq");
                else {
                    if (CommonBaseDateUtil.isBetween(beginItemDate, beginDate, endDate) || CommonBaseDateUtil.isBetween(endItemDate, beginDate, endDate))
                        errors.add(error.toString(), "mainHolidayBeginDateReq", "mainHolidayEndDateReq", "mainHolidayDurationReq");
                }
            }
        }
    }

    private void correctValuesBeforeSaving(Model model)
    {
        EmployeeHolidaySExtract extract = model.getExtract();

        if (null == extract.getMainHolidayBeginDate() || null == extract.getMainHolidayEndDate())
        {
            extract.setMainHolidayBeginDate(null);
            extract.setMainHolidayEndDate(null);
            extract.setMainHolidayDuration(null);
        }

        if (null == extract.getSecondHolidayBeginDate() || null == extract.getSecondHolidayEndDate())
        {
            extract.setSecondHolidayBeginDate(null);
            extract.setSecondHolidayEndDate(null);
            extract.setSecondHolidayDuration(null);
        }
    }

    //является ли date праздником в производственном календаре
    @Override
    public boolean isIndustrialCalendarHoliday(EmployeeWorkWeekDuration weekDuration, Date date)
    {
        GregorianCalendar dayDate = (GregorianCalendar)GregorianCalendar.getInstance();

        dayDate.setTime(date);

        MQBuilder calendarBuilder = new MQBuilder(IndustrialCalendar.ENTITY_CLASS, "ic");
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.P_YEAR, dayDate.get(Calendar.YEAR)));
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.L_WEEK_DURATION, weekDuration));
        IndustrialCalendar calendar = (IndustrialCalendar)calendarBuilder.uniqueResult(getSession());
        if (null == calendar)
            return false;

        MQBuilder builder = new MQBuilder(IndustrialCalendarHoliday.ENTITY_CLASS, "ich");
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_CALENDAR, calendar));
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_HOLIDAY + "." + Holiday.P_PREHOLIDAY, Boolean.FALSE));
        List<IHolidayDuration> holidays = builder.getResultList(getSession());

        builder = new MQBuilder(IndustrialCalendarHolidayDuration.ENTITY_CLASS, "ichd");
        builder.add(MQExpression.in("ichd", IndustrialCalendarHolidayDuration.L_HOLIDAY, holidays));
        builder.add(MQExpression.greatOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        builder.add(MQExpression.lessOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        holidays.addAll(builder.<IHolidayDuration>getResultList(getSession()));

        for(IHolidayDuration holiday : holidays)
        {
            if (holiday.getMonth() != (dayDate.get(Calendar.MONTH) + 1))
                continue;

            int begin = holiday.getBeginDay();
            int end = null == holiday.getEndDay() ? begin : holiday.getEndDay();

            if (holiday.getMonth() == dayDate.get(Calendar.MONTH) + 1 &&
                    dayDate.get(Calendar.DAY_OF_MONTH) <= end &&
                    dayDate.get(Calendar.DAY_OF_MONTH) >= begin)
                return true;
        }
        return false;
    }


    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }
}