package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.IChoseRowTransferAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.gen.IChoseRowTransferAnnualHolidayExtractGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О переносе ежегодного отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferAnnualHolidayExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract";
    public static final String ENTITY_NAME = "transferAnnualHolidayExtract";
    public static final int VERSION_HASH = -548177622;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_HOLIDAY = "employeeHoliday";
    public static final String P_BEGIN_HOLIDAY_DATE = "beginHolidayDate";
    public static final String P_END_HOLIDAY_DATE = "endHolidayDate";
    public static final String P_HOLIDAY_TITLE = "holidayTitle";
    public static final String P_OPTIONAL_CONDITION = "optionalCondition";
    public static final String P_PERIOD_HOLIDAY_STR = "periodHolidayStr";
    public static final String P_HOLIDAY_STR = "holidayStr";
    public static final String P_FACT_HOLIDAY_STR = "factHolidayStr";
    public static final String P_SHOW_FACT_HOLIDAY = "showFactHoliday";
    public static final String P_SHOW_HOLIDAY = "showHoliday";

    private IChoseRowTransferAnnualHolidayExtract _employeeHoliday;     // Отпуск за период
    private Date _beginHolidayDate;     // Дата начала периода
    private Date _endHolidayDate;     // Дата окончания периода
    private String _holidayTitle;     // Название отпуска
    private String _optionalCondition;     // Произвольная строка условий отпуска
    private String _periodHolidayStr;     // Отпуск за период (карточка)
    private String _holidayStr;     // Отпуск (карточка)
    private String _factHolidayStr;     // Предоставляемый отпуск (карточка)
    private Boolean _showFactHoliday;     // Выбраны ли отпуска (карточка)
    private Boolean _showHoliday;     // Указаны ли даты переноса (карточка)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отпуск за период.
     */
    public IChoseRowTransferAnnualHolidayExtract getEmployeeHoliday()
    {
        return _employeeHoliday;
    }

    /**
     * @param employeeHoliday Отпуск за период.
     */
    public void setEmployeeHoliday(IChoseRowTransferAnnualHolidayExtract employeeHoliday)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && employeeHoliday!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IChoseRowTransferAnnualHolidayExtract.class);
            IEntityMeta actual =  employeeHoliday instanceof IEntity ? EntityRuntime.getMeta((IEntity) employeeHoliday) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_employeeHoliday, employeeHoliday);
        _employeeHoliday = employeeHoliday;
    }

    /**
     * @return Дата начала периода.
     */
    public Date getBeginHolidayDate()
    {
        return _beginHolidayDate;
    }

    /**
     * @param beginHolidayDate Дата начала периода.
     */
    public void setBeginHolidayDate(Date beginHolidayDate)
    {
        dirty(_beginHolidayDate, beginHolidayDate);
        _beginHolidayDate = beginHolidayDate;
    }

    /**
     * @return Дата окончания периода.
     */
    public Date getEndHolidayDate()
    {
        return _endHolidayDate;
    }

    /**
     * @param endHolidayDate Дата окончания периода.
     */
    public void setEndHolidayDate(Date endHolidayDate)
    {
        dirty(_endHolidayDate, endHolidayDate);
        _endHolidayDate = endHolidayDate;
    }

    /**
     * @return Название отпуска.
     */
    @Length(max=255)
    public String getHolidayTitle()
    {
        return _holidayTitle;
    }

    /**
     * @param holidayTitle Название отпуска.
     */
    public void setHolidayTitle(String holidayTitle)
    {
        dirty(_holidayTitle, holidayTitle);
        _holidayTitle = holidayTitle;
    }

    /**
     * @return Произвольная строка условий отпуска.
     */
    public String getOptionalCondition()
    {
        return _optionalCondition;
    }

    /**
     * @param optionalCondition Произвольная строка условий отпуска.
     */
    public void setOptionalCondition(String optionalCondition)
    {
        dirty(_optionalCondition, optionalCondition);
        _optionalCondition = optionalCondition;
    }

    /**
     * @return Отпуск за период (карточка).
     */
    @Length(max=255)
    public String getPeriodHolidayStr()
    {
        return _periodHolidayStr;
    }

    /**
     * @param periodHolidayStr Отпуск за период (карточка).
     */
    public void setPeriodHolidayStr(String periodHolidayStr)
    {
        dirty(_periodHolidayStr, periodHolidayStr);
        _periodHolidayStr = periodHolidayStr;
    }

    /**
     * @return Отпуск (карточка).
     */
    @Length(max=255)
    public String getHolidayStr()
    {
        return _holidayStr;
    }

    /**
     * @param holidayStr Отпуск (карточка).
     */
    public void setHolidayStr(String holidayStr)
    {
        dirty(_holidayStr, holidayStr);
        _holidayStr = holidayStr;
    }

    /**
     * @return Предоставляемый отпуск (карточка).
     */
    @Length(max=255)
    public String getFactHolidayStr()
    {
        return _factHolidayStr;
    }

    /**
     * @param factHolidayStr Предоставляемый отпуск (карточка).
     */
    public void setFactHolidayStr(String factHolidayStr)
    {
        dirty(_factHolidayStr, factHolidayStr);
        _factHolidayStr = factHolidayStr;
    }

    /**
     * @return Выбраны ли отпуска (карточка).
     */
    public Boolean getShowFactHoliday()
    {
        return _showFactHoliday;
    }

    /**
     * @param showFactHoliday Выбраны ли отпуска (карточка).
     */
    public void setShowFactHoliday(Boolean showFactHoliday)
    {
        dirty(_showFactHoliday, showFactHoliday);
        _showFactHoliday = showFactHoliday;
    }

    /**
     * @return Указаны ли даты переноса (карточка).
     */
    public Boolean getShowHoliday()
    {
        return _showHoliday;
    }

    /**
     * @param showHoliday Указаны ли даты переноса (карточка).
     */
    public void setShowHoliday(Boolean showHoliday)
    {
        dirty(_showHoliday, showHoliday);
        _showHoliday = showHoliday;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransferAnnualHolidayExtractGen)
        {
            setEmployeeHoliday(((TransferAnnualHolidayExtract)another).getEmployeeHoliday());
            setBeginHolidayDate(((TransferAnnualHolidayExtract)another).getBeginHolidayDate());
            setEndHolidayDate(((TransferAnnualHolidayExtract)another).getEndHolidayDate());
            setHolidayTitle(((TransferAnnualHolidayExtract)another).getHolidayTitle());
            setOptionalCondition(((TransferAnnualHolidayExtract)another).getOptionalCondition());
            setPeriodHolidayStr(((TransferAnnualHolidayExtract)another).getPeriodHolidayStr());
            setHolidayStr(((TransferAnnualHolidayExtract)another).getHolidayStr());
            setFactHolidayStr(((TransferAnnualHolidayExtract)another).getFactHolidayStr());
            setShowFactHoliday(((TransferAnnualHolidayExtract)another).getShowFactHoliday());
            setShowHoliday(((TransferAnnualHolidayExtract)another).getShowHoliday());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferAnnualHolidayExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferAnnualHolidayExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransferAnnualHolidayExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    return obj.getEmployeeHoliday();
                case "beginHolidayDate":
                    return obj.getBeginHolidayDate();
                case "endHolidayDate":
                    return obj.getEndHolidayDate();
                case "holidayTitle":
                    return obj.getHolidayTitle();
                case "optionalCondition":
                    return obj.getOptionalCondition();
                case "periodHolidayStr":
                    return obj.getPeriodHolidayStr();
                case "holidayStr":
                    return obj.getHolidayStr();
                case "factHolidayStr":
                    return obj.getFactHolidayStr();
                case "showFactHoliday":
                    return obj.getShowFactHoliday();
                case "showHoliday":
                    return obj.getShowHoliday();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    obj.setEmployeeHoliday((IChoseRowTransferAnnualHolidayExtract) value);
                    return;
                case "beginHolidayDate":
                    obj.setBeginHolidayDate((Date) value);
                    return;
                case "endHolidayDate":
                    obj.setEndHolidayDate((Date) value);
                    return;
                case "holidayTitle":
                    obj.setHolidayTitle((String) value);
                    return;
                case "optionalCondition":
                    obj.setOptionalCondition((String) value);
                    return;
                case "periodHolidayStr":
                    obj.setPeriodHolidayStr((String) value);
                    return;
                case "holidayStr":
                    obj.setHolidayStr((String) value);
                    return;
                case "factHolidayStr":
                    obj.setFactHolidayStr((String) value);
                    return;
                case "showFactHoliday":
                    obj.setShowFactHoliday((Boolean) value);
                    return;
                case "showHoliday":
                    obj.setShowHoliday((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                        return true;
                case "beginHolidayDate":
                        return true;
                case "endHolidayDate":
                        return true;
                case "holidayTitle":
                        return true;
                case "optionalCondition":
                        return true;
                case "periodHolidayStr":
                        return true;
                case "holidayStr":
                        return true;
                case "factHolidayStr":
                        return true;
                case "showFactHoliday":
                        return true;
                case "showHoliday":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    return true;
                case "beginHolidayDate":
                    return true;
                case "endHolidayDate":
                    return true;
                case "holidayTitle":
                    return true;
                case "optionalCondition":
                    return true;
                case "periodHolidayStr":
                    return true;
                case "holidayStr":
                    return true;
                case "factHolidayStr":
                    return true;
                case "showFactHoliday":
                    return true;
                case "showHoliday":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    return IChoseRowTransferAnnualHolidayExtract.class;
                case "beginHolidayDate":
                    return Date.class;
                case "endHolidayDate":
                    return Date.class;
                case "holidayTitle":
                    return String.class;
                case "optionalCondition":
                    return String.class;
                case "periodHolidayStr":
                    return String.class;
                case "holidayStr":
                    return String.class;
                case "factHolidayStr":
                    return String.class;
                case "showFactHoliday":
                    return Boolean.class;
                case "showHoliday":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferAnnualHolidayExtract> _dslPath = new Path<TransferAnnualHolidayExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferAnnualHolidayExtract");
    }
            

    /**
     * @return Отпуск за период.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getEmployeeHoliday()
     */
    public static IChoseRowTransferAnnualHolidayExtractGen.Path<IChoseRowTransferAnnualHolidayExtract> employeeHoliday()
    {
        return _dslPath.employeeHoliday();
    }

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getBeginHolidayDate()
     */
    public static PropertyPath<Date> beginHolidayDate()
    {
        return _dslPath.beginHolidayDate();
    }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getEndHolidayDate()
     */
    public static PropertyPath<Date> endHolidayDate()
    {
        return _dslPath.endHolidayDate();
    }

    /**
     * @return Название отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getHolidayTitle()
     */
    public static PropertyPath<String> holidayTitle()
    {
        return _dslPath.holidayTitle();
    }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getOptionalCondition()
     */
    public static PropertyPath<String> optionalCondition()
    {
        return _dslPath.optionalCondition();
    }

    /**
     * @return Отпуск за период (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getPeriodHolidayStr()
     */
    public static PropertyPath<String> periodHolidayStr()
    {
        return _dslPath.periodHolidayStr();
    }

    /**
     * @return Отпуск (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getHolidayStr()
     */
    public static PropertyPath<String> holidayStr()
    {
        return _dslPath.holidayStr();
    }

    /**
     * @return Предоставляемый отпуск (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getFactHolidayStr()
     */
    public static PropertyPath<String> factHolidayStr()
    {
        return _dslPath.factHolidayStr();
    }

    /**
     * @return Выбраны ли отпуска (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getShowFactHoliday()
     */
    public static PropertyPath<Boolean> showFactHoliday()
    {
        return _dslPath.showFactHoliday();
    }

    /**
     * @return Указаны ли даты переноса (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getShowHoliday()
     */
    public static PropertyPath<Boolean> showHoliday()
    {
        return _dslPath.showHoliday();
    }

    public static class Path<E extends TransferAnnualHolidayExtract> extends ModularEmployeeExtract.Path<E>
    {
        private IChoseRowTransferAnnualHolidayExtractGen.Path<IChoseRowTransferAnnualHolidayExtract> _employeeHoliday;
        private PropertyPath<Date> _beginHolidayDate;
        private PropertyPath<Date> _endHolidayDate;
        private PropertyPath<String> _holidayTitle;
        private PropertyPath<String> _optionalCondition;
        private PropertyPath<String> _periodHolidayStr;
        private PropertyPath<String> _holidayStr;
        private PropertyPath<String> _factHolidayStr;
        private PropertyPath<Boolean> _showFactHoliday;
        private PropertyPath<Boolean> _showHoliday;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отпуск за период.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getEmployeeHoliday()
     */
        public IChoseRowTransferAnnualHolidayExtractGen.Path<IChoseRowTransferAnnualHolidayExtract> employeeHoliday()
        {
            if(_employeeHoliday == null )
                _employeeHoliday = new IChoseRowTransferAnnualHolidayExtractGen.Path<IChoseRowTransferAnnualHolidayExtract>(L_EMPLOYEE_HOLIDAY, this);
            return _employeeHoliday;
        }

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getBeginHolidayDate()
     */
        public PropertyPath<Date> beginHolidayDate()
        {
            if(_beginHolidayDate == null )
                _beginHolidayDate = new PropertyPath<Date>(TransferAnnualHolidayExtractGen.P_BEGIN_HOLIDAY_DATE, this);
            return _beginHolidayDate;
        }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getEndHolidayDate()
     */
        public PropertyPath<Date> endHolidayDate()
        {
            if(_endHolidayDate == null )
                _endHolidayDate = new PropertyPath<Date>(TransferAnnualHolidayExtractGen.P_END_HOLIDAY_DATE, this);
            return _endHolidayDate;
        }

    /**
     * @return Название отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getHolidayTitle()
     */
        public PropertyPath<String> holidayTitle()
        {
            if(_holidayTitle == null )
                _holidayTitle = new PropertyPath<String>(TransferAnnualHolidayExtractGen.P_HOLIDAY_TITLE, this);
            return _holidayTitle;
        }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getOptionalCondition()
     */
        public PropertyPath<String> optionalCondition()
        {
            if(_optionalCondition == null )
                _optionalCondition = new PropertyPath<String>(TransferAnnualHolidayExtractGen.P_OPTIONAL_CONDITION, this);
            return _optionalCondition;
        }

    /**
     * @return Отпуск за период (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getPeriodHolidayStr()
     */
        public PropertyPath<String> periodHolidayStr()
        {
            if(_periodHolidayStr == null )
                _periodHolidayStr = new PropertyPath<String>(TransferAnnualHolidayExtractGen.P_PERIOD_HOLIDAY_STR, this);
            return _periodHolidayStr;
        }

    /**
     * @return Отпуск (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getHolidayStr()
     */
        public PropertyPath<String> holidayStr()
        {
            if(_holidayStr == null )
                _holidayStr = new PropertyPath<String>(TransferAnnualHolidayExtractGen.P_HOLIDAY_STR, this);
            return _holidayStr;
        }

    /**
     * @return Предоставляемый отпуск (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getFactHolidayStr()
     */
        public PropertyPath<String> factHolidayStr()
        {
            if(_factHolidayStr == null )
                _factHolidayStr = new PropertyPath<String>(TransferAnnualHolidayExtractGen.P_FACT_HOLIDAY_STR, this);
            return _factHolidayStr;
        }

    /**
     * @return Выбраны ли отпуска (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getShowFactHoliday()
     */
        public PropertyPath<Boolean> showFactHoliday()
        {
            if(_showFactHoliday == null )
                _showFactHoliday = new PropertyPath<Boolean>(TransferAnnualHolidayExtractGen.P_SHOW_FACT_HOLIDAY, this);
            return _showFactHoliday;
        }

    /**
     * @return Указаны ли даты переноса (карточка).
     * @see ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract#getShowHoliday()
     */
        public PropertyPath<Boolean> showHoliday()
        {
            if(_showHoliday == null )
                _showHoliday = new PropertyPath<Boolean>(TransferAnnualHolidayExtractGen.P_SHOW_HOLIDAY, this);
            return _showHoliday;
        }

        public Class getEntityClass()
        {
            return TransferAnnualHolidayExtract.class;
        }

        public String getEntityName()
        {
            return "transferAnnualHolidayExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
