/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e10.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<EmployeeSurnameChangeExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        prepareStaffRateDataSource(component);
    }

    public void onRefresh(IBusinessComponent component)
    {
        EmployeeSurnameChangeExtract extract = getModel(component).getExtract();
        IdentityCardType cardType = extract.getIcCardType();
        if (null != cardType && cardType.isSeriaRequired() && isNeedChange(extract.getIcSeria(), cardType.getMaxSeriaLength()))
        {
            extract.setIcSeria(getDefaultValue(cardType.getMaxSeriaLength()));
        }
        if (null != cardType && cardType.isNumberRequired() && isNeedChange(extract.getIcNumber(), cardType.getMaxNumberLength()))
        {
            extract.setIcNumber(getDefaultValue(cardType.getMaxNumberLength()));
        }
        if (null == cardType && StringUtils.containsOnly(extract.getIcNumber(), "0"))
        {
            extract.setIcNumber(null);
        }
        if (null != cardType && !cardType.isShowSeria())
        {
            extract.setIcSeria(null);
        }
    }

    private static boolean isNeedChange(String value, int maxLength)
    {
        return StringUtils.isEmpty(value) || StringUtils.containsOnly(value, "0") || value.length() > maxLength;
    }

    private static String getDefaultValue(int maxLength)
    {
        return String.format("%" + maxLength + "s", "").replaceAll(" ", "0");
    }

    private void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getStaffRateDataSource() != null)
            return;


        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setStaffRateDataSource(dataSource);
    }
}