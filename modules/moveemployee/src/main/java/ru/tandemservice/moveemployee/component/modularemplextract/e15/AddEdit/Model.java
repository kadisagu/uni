/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e15.AddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 24.03.2011
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<ProlongationAnnualHolidayExtract>
{
    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;
    private List<EmployeeHoliday> _holidayList;

    //Getters & Setters

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public List<EmployeeHoliday> getHolidayList()
    {
        return _holidayList;
    }

    public void setHolidayList(List<EmployeeHoliday> holidayList)
    {
        _holidayList = holidayList;
    }
}