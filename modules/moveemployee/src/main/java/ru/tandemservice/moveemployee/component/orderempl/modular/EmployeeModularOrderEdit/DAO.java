/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.moveemployee.dao.ModularOrder.IEmployeeModularOrderParagraphsSortDAO;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.IAbstractOrder;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 11.11.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder(get(EmployeeModularOrder.class, model.getOrder().getId()));
        model.setParagraphsSortRuleList(getList(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_DAO_NAME));
    }
    
    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        String orderNumber = model.getOrder().getNumber();
        if (orderNumber != null)
        {
            orderNumber = orderNumber.trim();
            List<AbstractEmployeeOrder> orderList = getList(AbstractEmployeeOrder.class, IAbstractOrder.P_NUMBER, (Object) orderNumber);
            if ((orderList.size() > 1) || (orderList.size() == 1 && !orderList.get(0).equals(model.getOrder())))
            {
                errors.add("Номер приказа должен быть уникальным в рамках всех типов приказов по кадровому составу", "number");
            }
        }
    }

    @Override
    public void update(Model model)
    {
        update(model.getOrder());

        IEmployeeModularOrderParagraphsSortDAO sorter = (IEmployeeModularOrderParagraphsSortDAO) ApplicationRuntime.getBean(model.getOrder().getParagraphsSortRule().getDaoName());
        sorter.sortExtracts(model.getOrder());
    }
}