/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e19;

import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeActingExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.entity.employee.EmployeeActingItem;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.11.2011
 */
public class EmployeeActingExtractDao extends UniBaseDao implements IExtractComponentDao<EmployeeActingExtract>
{
    public void doCommit(EmployeeActingExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        EmployeeActingItem actingItem = new EmployeeActingItem();
        actingItem.setEmployeePost(extract.getEntity());
        actingItem.setMissingEmployeePost(extract.getMissingEmployeePost());
        actingItem.setOrgUnit(extract.getMissingOrgUnit());
        actingItem.setPostBoundedWithQGandQL(extract.getMissingPost());
        actingItem.setBeginDate(extract.getBeginDate());
        actingItem.setEndDate(extract.getEndDate());

        save(actingItem);

        extract.setCreateEmployeeActingItem(actingItem);

        update(extract);
    }

    public void doRollback(EmployeeActingExtract extract, Map parameters)
    {
        if (extract.getCreateEmployeeActingItem() != null)
            delete(extract.getCreateEmployeeActingItem());
    }
}