/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e14;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.entity.ContractTerminationExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 18.03.2011
 */
public class ContractTerminationExtractPrint implements IPrintFormCreator<ContractTerminationExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ContractTerminationExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        //совместительство
        if ( ( UniDefines.POST_TYPE_SECOND_JOB.equals(extract.getEntity().getPostType().getCode())) ||
             ( UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(extract.getEntity().getPostType().getCode())) ||
             ( UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(extract.getEntity().getPostType().getCode())) )
        {
            Double sumRate = 0.0d;
            List<EmployeePostStaffRateItem> employeePostStaffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());
            for (EmployeePostStaffRateItem item : employeePostStaffRateItemList)
                sumRate += item.getStaffRate();

            modifier.put("secondJob", ", совместителем на " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(sumRate) + " ставки");
        }
        else
            modifier.put("secondJob", "");

        //доп. условия
        if (extract.getAdditionalConditions() != null)
        {
            modifier.put("additionalConditions", ", " + extract.getAdditionalConditions());
        }
        else
            modifier.put("additionalConditions", "");

        //дата уволньнения
        modifier.put("dismissalDate", RussianDateFormatUtils.getDayString(extract.getDismissalDate(), true) + " " +
            RussianDateFormatUtils.getMonthName(extract.getDismissalDate(), false) + " " +
            RussianDateFormatUtils.getYearString(extract.getDismissalDate(), false) + " г.");

        //причина
        modifier.put("dismissalReason", extract.getDismissalReason().getTitle());

        //статья увольнения
        modifier.put("lawItem", extract.getDismissalReason().getLawItem());

        //отпуск не положен
        if (extract.isHolidayNotSupposed())
            modifier.put("holidayNotSupposed", " Отпуск не положен.");
        else
            modifier.put("holidayNotSupposed", "");

        //удержание
        if ( (extract.getNotFulfil() != null) && !extract.isHolidayNotSupposed())
        {
            String relative = "";

            if (extract.getDismissalReason().isSalaryDelegating())
            {
                relative = " с родственников ";
            }

            modifier.put("notFulFil", " Удержать" + relative + "за неотработанные дни отпуска " + NumberConvertingUtil.getCalendarDaysWithName(extract.getNotFulfil())+ ".");
        }
        else
            modifier.put("notFulFil", "");

        //компенсация
        if (extract.getCompensatingFor() != null)
        {
            String relative = "";

            if (extract.getDismissalReason().isSalaryDelegating())
            {
                relative = " родственникам ";
            }

            modifier.put("compensatingFor", " Выплатить" + relative + "компенсацию за "+ NumberConvertingUtil.getCalendarDaysWithName(extract.getCompensatingFor())+ ".");
        }
        else
            modifier.put("compensatingFor", "");

        //источник финансирования
        if (extract.getFinancingSource() != null)
        {
            String financingSource ;
            if ( UniempDefines.FINANCING_SOURCE_BUDGET.equals(extract.getFinancingSource().getCode()) )
                financingSource = " федерального бюджета.";
            else
                financingSource = " от приносящей доход деятельности.";

            modifier.put("financingSource", " Выплата за счет средств " + financingSource);
        }
        else
            modifier.put("financingSource", "");

        modifier.modify(document);
        return document;
    }
}