/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author dseleznev
 * Created on: 11.11.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
/*
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("e");
    
    static
    {
        _orderSettings.setOrders(ModularEmployeeExtract.EMPLOYEE_FIO_KEY, new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME));
    }
*/

    @Override
    public void prepare(Model model)
    {
        model.setOrder(getNotNull(EmployeeModularOrder.class, model.getOrder().getId()));
        model.setSecModel(new CommonPostfixPermissionModel("employeeModularOrder"));//employee modular order sec postfix
        model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getOrder()));

        // init filters
        model.setExtractTypeList(HierarchyUtil.listHierarchyNodesWithParents(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularExtractTypeList(), CommonCatalogUtil.CATALOG_CODE_COMPARATOR, false));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Date extractCreationDate = model.getSettings().get("extractCreateDate");
        String employeeLastName = model.getSettings().get("lastName");
        EmployeeExtractType extractType = model.getSettings().get("extractType");
        
        MQBuilder builder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "e");
        builder.addJoinFetch("e", IAbstractExtract.L_PARAGRAPH, "pa");
        builder.addJoinFetch("e", ModularEmployeeExtract.L_EMPLOYEE, "em");
        builder.addJoinFetch("em", Employee.L_PERSON, "p");
        builder.addJoinFetch("p", Person.L_IDENTITY_CARD, "idCard");
        builder.add(MQExpression.eq("pa", IAbstractParagraph.L_ORDER, model.getOrder()));
        
        if (null != extractCreationDate)
            builder.add(UniMQExpression.eqDate("e", IAbstractExtract.P_CREATE_DATE, extractCreationDate));
        if (null != employeeLastName)
            builder.add(MQExpression.like("e", ModularEmployeeExtract.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, "%" + employeeLastName));
        if (null != extractType)
            builder.add(MQExpression.eq("e", IAbstractExtract.L_TYPE, extractType));
        
//        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        builder.addOrder("pa", IAbstractParagraph.P_NUMBER);

        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
    
    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(model.getOrder().getNumber()))
            throw new ApplicationException("Нельзя отправить приказ на согласование без номера.");

        if (model.getOrder().getCommitDate() == null)
            throw new ApplicationException("Нельзя отправить приказ на согласование без даты приказа.");

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveModularOrderText(model.getOrder());

        // Всем выпискам поставить состояние "В приказах"
        new DQLUpdateBuilder(AbstractEmployeeExtract.class)
                .set(IAbstractExtract.L_STATE, value(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER)))
                .where(eq(property(AbstractEmployeeExtract.paragraph().order()), value(model.getOrder())))
                .createStatement(getSession()).execute();

        ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
        sendToCoordinationService.init(model.getOrder(), initiator);
        sendToCoordinationService.execute();
    }

    @Override
    public void doSendToFormative(Model model)
    {
        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на формирование, так как он на согласовании.");

        //2. надо сменить состояние на формируется
        model.getOrder().setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        update(model.getOrder());
    }

    @Override
    public void doReject(Model model)
    {
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        } else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(model.getOrder(), UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(model.getOrder());
            touchService.execute();
        }
    }

    @Override
    public void doCommit(Model model)
    {
        if (model.getOrder().getNumber() == null)
            throw new ApplicationException("Нельзя проводить приказ без номера.");

        if (model.getOrder().getCommitDate() == null)
            throw new ApplicationException("Нельзя проводить приказ без даты приказа.");

        //save print form
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveModularOrderText(model.getOrder());

        getSession().flush();//т.к. в следующем методе MoveDaoFacade.getMoveDao().doCommitOrder() будет вызван getSession.clear(), то делаем getSession().flush(), что бы сохраненная выше в методе MoveEmployeeDaoFacade.getMoveEmployeeDao().saveModularOrderText() печатная форма для проведенного приказа не удалилась.

        MoveDaoFacade.getMoveDao().doCommitOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
    }

    @Override
    public void doRollback(Model model)
    {
        MoveDaoFacade.getMoveDao().doRollbackOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER), null);
    }

    @Override
    public void deleteExtract(Model model, Long extractId)
    {
        ModularEmployeeExtract extract = getNotNull(extractId);
        getSession().refresh(model.getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");
        MoveDaoFacade.getMoveDao().deleteParagraph(extract.getParagraph(), getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
    }
    
    @Override
    public void deleteOrder(Model model)
    {
        getSession().refresh(model.getOrder());
        if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(model.getOrder().getState().getCode()))
            MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(model.getOrder());
        else
            MoveDaoFacade.getMoveDao().deleteOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
    }

    @Override
    public void deleteOrderWithExtracts(Model model)
    {
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(model.getOrder());
    }

    @Override
    public boolean isAddExtractsDisabled(Model model)
    {
        List<EmployeeExtractType> employeeExtractIndividualTypeList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeePostExtractIndividualTypeList();
        employeeExtractIndividualTypeList.addAll(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularEmployeeExtractIndividualTypeList());

        model.getDataSource().refresh();
        for (IAbstractExtract extractType : model.getDataSource().getEntityList())
        {
            if (employeeExtractIndividualTypeList.contains(extractType.getType()))
                return true;
        }


        return false;
    }

    @Override
    public void doParagraphUp(Long extractId)
    {
        EmployeeModularParagraph paragraph1 = (EmployeeModularParagraph) get(ModularEmployeeExtract.class, extractId).getParagraph();
        int number = paragraph1.getNumber();

        if (number == 1) return;

        EmployeeModularParagraph paragraph2 = new DQLSelectBuilder()
                .fromEntity(EmployeeModularParagraph.class, "p")
                .where(eq(property(EmployeeModularParagraph.order().fromAlias("p")), value(paragraph1.getOrder())))
                .where(eq(property(EmployeeModularParagraph.number().fromAlias("p")), value(number - 1)))
                .createStatement(getSession())
                .uniqueResult();

        paragraph1.setNumber(-1);
        update(paragraph1);
        getSession().flush();

        paragraph2.setNumber(number);
        update(paragraph2);
        getSession().flush();

        paragraph1.setNumber(number - 1);
        update(paragraph1);
        getSession().flush();

        // Меняем на "Ручную нумерацию", упорядочивать не требуется
        EmployeeModularOrder order = (EmployeeModularOrder) paragraph1.getOrder();
        order.setParagraphsSortRule(get(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_DAO_NAME, MoveEmployeeDefines.MANUAL_SORT));

        update(order);
    }

    @Override
    public void doParagraphDown(Long extractId)
    {
        EmployeeModularParagraph paragraph1 = (EmployeeModularParagraph) get(ModularEmployeeExtract.class, extractId).getParagraph();
        int number = paragraph1.getNumber();

        if (paragraph1.getOrder().getParagraphCount() == number) return;

        EmployeeModularParagraph paragraph2 = new DQLSelectBuilder()
                .fromEntity(EmployeeModularParagraph.class, "p")
                .where(eq(property(EmployeeModularParagraph.order().fromAlias("p")), value(paragraph1.getOrder())))
                .where(eq(property(EmployeeModularParagraph.number().fromAlias("p")), value(number + 1)))
                .createStatement(getSession())
                .uniqueResult();

        paragraph1.setNumber(-1);
        update(paragraph1);
        getSession().flush();

        paragraph2.setNumber(number);
        update(paragraph2);
        getSession().flush();

        paragraph1.setNumber(number + 1);
        update(paragraph1);
        getSession().flush();

        // Меняем на "Ручную нумерацию", упорядочивать не требуется
        EmployeeModularOrder order = (EmployeeModularOrder) paragraph1.getOrder();
        order.setParagraphsSortRule(get(EmployeeModularOrderParagraphsSortRule.class, EmployeeModularOrderParagraphsSortRule.P_DAO_NAME, MoveEmployeeDefines.MANUAL_SORT));

        update(order);
    }
}