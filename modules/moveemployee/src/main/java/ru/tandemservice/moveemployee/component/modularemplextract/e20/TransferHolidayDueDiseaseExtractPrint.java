/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e20;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract;
import ru.tandemservice.uni.util.NumberConvertingUtil;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 06.12.2011
 */
public class TransferHolidayDueDiseaseExtractPrint implements IPrintFormCreator<TransferHolidayDueDiseaseExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, TransferHolidayDueDiseaseExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        StringBuilder modifierBuilder = new StringBuilder();
        modifierBuilder.append("с ");
        modifierBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStartHistory()));
        modifierBuilder.append(" по ");
        modifierBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getFinishHistory()));
        if (extract.getBeginHistory() != null && extract.getEndHistory() != null)
        {
            modifierBuilder.append(" за период с ");
            modifierBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginHistory()));
            modifierBuilder.append(" по ");
            modifierBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndHistory()));
        }
        modifier.put("employeeHolidayStr", modifierBuilder.toString());

        modifierBuilder = new StringBuilder();
        modifierBuilder.append(NumberConvertingUtil.getCalendarDaysWithName(extract.getTransferDaysAmount()));
        modifier.put("daysAmountCarryingOver", modifierBuilder.toString());

        modifierBuilder = new StringBuilder();
        if (extract.getDateToTransfer() != null)
        {
            modifierBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDateToTransfer()));
            modifierBuilder.append(" г");
        }
        else if (extract.getTransferToAnotherDay() != null && extract.getTransferToAnotherDay())
        {
            modifierBuilder.append("другой срок");
        }
        modifier.put("dateCarryingOver", modifierBuilder.toString());

        modifier.modify(document);
        return document;
    }
}