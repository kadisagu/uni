/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeModularOrderParagraphsSortRuleSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareDataSource(component);
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<EmployeeModularOrderParagraphsSortRule> dataSource = new DynamicListDataSource<>(component, this);

        dataSource.addColumn(new SimpleColumn("Название", EmployeeModularOrderParagraphsSortRule.P_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Описание", EmployeeModularOrderParagraphsSortRule.P_DESCRIPTION, RawFormatter.INSTANCE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Активное", EmployeeModularOrderParagraphsSortRule.P_ACTIVE).setListener("onClickActive").setDisabledProperty(EmployeeModularOrderParagraphsSortRule.P_ACTIVE));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.moveemployee.component.settings.EmployeeModularOrderParagraphsSortRuleEdit", new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, component.getListenerParameter())));
    }

    public void onClickActive(IBusinessComponent component)
    {
        getDao().setActive(component.<Long>getListenerParameter());
        component.refresh();
    }
}