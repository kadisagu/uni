/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e8;

import ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class PayForHolidayDayProvideHoldayEmplListExtractDao extends UniBaseDao implements IExtractComponentDao<PayForHolidayDayProvideHoldayEmplListExtract>
{
    @Override
    public void doCommit(PayForHolidayDayProvideHoldayEmplListExtract extract, Map parameters)
    {

    }

    @Override
    public void doRollback(PayForHolidayDayProvideHoldayEmplListExtract extract, Map parameters)
    {

    }
}
