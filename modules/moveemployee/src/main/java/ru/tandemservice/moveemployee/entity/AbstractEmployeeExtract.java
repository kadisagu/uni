package ru.tandemservice.moveemployee.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.gen.AbstractEmployeeExtractGen;
import ru.tandemservice.moveemployee.entity.gen.AbstractEmployeeOrderGen;
import ru.tandemservice.moveemployee.entity.gen.AbstractEmployeeParagraphGen;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

public abstract class AbstractEmployeeExtract extends AbstractEmployeeExtractGen implements IAbstractExtract<EmployeePost, AbstractEmployeeParagraph>
{
    public static final String P_NO_EDIT = "noEdit";       // нельзя редактировать

    /**
     * Нельзя редактировать, используется в списках индивидуальных приказов, где выписка используется как приказ.<p>
     * Проверяется статус приказа, к которому относится выписка. */
    public static final String P_NO_EDIT_ORDER = "noEditOrder";

    public static final String P_NO_DELETE = "noDelete";   // нельзя удалять
    
    public static final String P_NO_EXCLUDE = "noExclude"; // нельзя исплючать из приказа

    /**
     * @return Приказ № <номер приказа> от <дата формирования> г.
     */
    @Override
    @EntityDSLSupport(parts = {AbstractEmployeeExtractGen.L_PARAGRAPH, AbstractEmployeeParagraphGen.L_ORDER + "." + AbstractEmployeeOrderGen.P_NUMBER,  AbstractEmployeeParagraphGen.L_ORDER + "." + AbstractEmployeeOrderGen.P_COMMIT_DATE})
    public String getTitleOrder()
    {
        return "Приказ № " + getParagraph().getOrder().getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getParagraph().getOrder().getCreateDate()) + " г.";
    }
    
    @Override
    public String getTitle()
    {
        String employeeWord = null != getEntity() ? (SexCodes.FEMALE.equals(getEntity().getPerson().getIdentityCard().getSex().getCode()) ? "сотрудница" : "сотрудник") : "сотрудник";
        if (getParagraph() == null)
            return "Проект приказа «" + getType().getTitle() + "» | " + employeeWord + " " + getEmployeeTitle();
        else
            return "Выписка «" + getType().getTitle() + "» №" + getParagraph().getNumber() + " из приказа №" + getParagraph().getOrder().getNumber() + " | " + employeeWord + " " + getEmployeeTitle();
    }
    
    public String getEmployeeTitle()
    {
        return (null != getEntity() && null != getEntity().getPerson()) ? getEntity().getPerson().getFullFio() : "";
    }
    
    public boolean isNoEdit()
    {
        //нельзя редактировать выписки у сотрудников если они не в состоянии формирования
        return !UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE.equals(getState().getCode());
    }

    public boolean isNoEditOrder()
    {
        return !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getParagraph().getOrder().getState().getCode());
    }

    public boolean isNoDelete()
    {
        //нельзя удалять выписки, которые нельзя исключать из приказа
        return isNoExclude();
    }

    public boolean isNoExclude()
    {
        //нельзя исключать выписки из приказа не в состоянии формирования
        //другими словами: можно исключать выписки не из приказа и можно исключать выписки из формирующегося приказа
        return !(getParagraph() == null || UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getParagraph().getOrder().getState().getCode()));
    }
    
    @Override
    public void setState(ICatalogItem state)
    {
        setState((ExtractStates) state);
    }

    @Override
    public void setType(ICatalogItem type)
    {
        setType((EmployeeExtractType) type);
    }
}