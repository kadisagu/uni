/* $Id$ */
package ru.tandemservice.moveemployee.component.listemplextract.e0.ParagraphAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickNext(IBusinessComponent component)
    {
        Model model = getModel(component);

        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListParagraphAddEditComponent(model.getExtractType().getIndex()), new ParametersMap()
                .add(AbstractListParagraphAddEditModel.PARAMETER_ORDER_ID, model.getOrder().getId())
                .add(AbstractListParagraphAddEditModel.PARAMETER_EXTRACT_TYPE_ID, model.getExtractType().getId())
                .add(AbstractListParagraphAddEditModel.PARAMETER_PARAGRAPH_ID, null)));
    }
}
