package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из списочного приказа по кадровому составу. Об изменении средств оплаты труда
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeFinancingSourceEmplListExtractGen extends ListEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract";
    public static final String ENTITY_NAME = "changeFinancingSourceEmplListExtract";
    public static final int VERSION_HASH = 448430774;
    private static IEntityMeta ENTITY_META;

    public static final String P_CHANGE_DATE = "changeDate";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_CHILD_ORG_UNIT = "childOrgUnit";
    public static final String L_STAFF_LIST_ALLOCATION_ITEM = "staffListAllocationItem";
    public static final String L_EMPLOYEE_POST_STAFF_RATE_ITEM = "employeePostStaffRateItem";

    private Date _changeDate;     // Дата смены оплаты труда
    private FinancingSource _financingSource;     // Источник финансирования (настройка названий)
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (настройка названий, детально)
    private OrgUnit _orgUnit;     // Подразделение
    private boolean _childOrgUnit;     // Учитывать дочерние подразделения
    private StaffListAllocationItem _staffListAllocationItem;     // Созданная должность ШР при проведении выписки
    private EmployeePostStaffRateItem _employeePostStaffRateItem;     // Созданная ставка сотрудника при проведении выписки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата смены оплаты труда. Свойство не может быть null.
     */
    @NotNull
    public Date getChangeDate()
    {
        return _changeDate;
    }

    /**
     * @param changeDate Дата смены оплаты труда. Свойство не может быть null.
     */
    public void setChangeDate(Date changeDate)
    {
        dirty(_changeDate, changeDate);
        _changeDate = changeDate;
    }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     */
    @NotNull
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования (настройка названий). Свойство не может быть null.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (настройка названий, детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Учитывать дочерние подразделения. Свойство не может быть null.
     */
    @NotNull
    public boolean isChildOrgUnit()
    {
        return _childOrgUnit;
    }

    /**
     * @param childOrgUnit Учитывать дочерние подразделения. Свойство не может быть null.
     */
    public void setChildOrgUnit(boolean childOrgUnit)
    {
        dirty(_childOrgUnit, childOrgUnit);
        _childOrgUnit = childOrgUnit;
    }

    /**
     * @return Созданная должность ШР при проведении выписки.
     */
    public StaffListAllocationItem getStaffListAllocationItem()
    {
        return _staffListAllocationItem;
    }

    /**
     * @param staffListAllocationItem Созданная должность ШР при проведении выписки.
     */
    public void setStaffListAllocationItem(StaffListAllocationItem staffListAllocationItem)
    {
        dirty(_staffListAllocationItem, staffListAllocationItem);
        _staffListAllocationItem = staffListAllocationItem;
    }

    /**
     * @return Созданная ставка сотрудника при проведении выписки.
     */
    public EmployeePostStaffRateItem getEmployeePostStaffRateItem()
    {
        return _employeePostStaffRateItem;
    }

    /**
     * @param employeePostStaffRateItem Созданная ставка сотрудника при проведении выписки.
     */
    public void setEmployeePostStaffRateItem(EmployeePostStaffRateItem employeePostStaffRateItem)
    {
        dirty(_employeePostStaffRateItem, employeePostStaffRateItem);
        _employeePostStaffRateItem = employeePostStaffRateItem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangeFinancingSourceEmplListExtractGen)
        {
            setChangeDate(((ChangeFinancingSourceEmplListExtract)another).getChangeDate());
            setFinancingSource(((ChangeFinancingSourceEmplListExtract)another).getFinancingSource());
            setFinancingSourceItem(((ChangeFinancingSourceEmplListExtract)another).getFinancingSourceItem());
            setOrgUnit(((ChangeFinancingSourceEmplListExtract)another).getOrgUnit());
            setChildOrgUnit(((ChangeFinancingSourceEmplListExtract)another).isChildOrgUnit());
            setStaffListAllocationItem(((ChangeFinancingSourceEmplListExtract)another).getStaffListAllocationItem());
            setEmployeePostStaffRateItem(((ChangeFinancingSourceEmplListExtract)another).getEmployeePostStaffRateItem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeFinancingSourceEmplListExtractGen> extends ListEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeFinancingSourceEmplListExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangeFinancingSourceEmplListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "changeDate":
                    return obj.getChangeDate();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "childOrgUnit":
                    return obj.isChildOrgUnit();
                case "staffListAllocationItem":
                    return obj.getStaffListAllocationItem();
                case "employeePostStaffRateItem":
                    return obj.getEmployeePostStaffRateItem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "changeDate":
                    obj.setChangeDate((Date) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "childOrgUnit":
                    obj.setChildOrgUnit((Boolean) value);
                    return;
                case "staffListAllocationItem":
                    obj.setStaffListAllocationItem((StaffListAllocationItem) value);
                    return;
                case "employeePostStaffRateItem":
                    obj.setEmployeePostStaffRateItem((EmployeePostStaffRateItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "changeDate":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
                case "orgUnit":
                        return true;
                case "childOrgUnit":
                        return true;
                case "staffListAllocationItem":
                        return true;
                case "employeePostStaffRateItem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "changeDate":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
                case "orgUnit":
                    return true;
                case "childOrgUnit":
                    return true;
                case "staffListAllocationItem":
                    return true;
                case "employeePostStaffRateItem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "changeDate":
                    return Date.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "childOrgUnit":
                    return Boolean.class;
                case "staffListAllocationItem":
                    return StaffListAllocationItem.class;
                case "employeePostStaffRateItem":
                    return EmployeePostStaffRateItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeFinancingSourceEmplListExtract> _dslPath = new Path<ChangeFinancingSourceEmplListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeFinancingSourceEmplListExtract");
    }
            

    /**
     * @return Дата смены оплаты труда. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getChangeDate()
     */
    public static PropertyPath<Date> changeDate()
    {
        return _dslPath.changeDate();
    }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Учитывать дочерние подразделения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#isChildOrgUnit()
     */
    public static PropertyPath<Boolean> childOrgUnit()
    {
        return _dslPath.childOrgUnit();
    }

    /**
     * @return Созданная должность ШР при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getStaffListAllocationItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> staffListAllocationItem()
    {
        return _dslPath.staffListAllocationItem();
    }

    /**
     * @return Созданная ставка сотрудника при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getEmployeePostStaffRateItem()
     */
    public static EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem> employeePostStaffRateItem()
    {
        return _dslPath.employeePostStaffRateItem();
    }

    public static class Path<E extends ChangeFinancingSourceEmplListExtract> extends ListEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _changeDate;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Boolean> _childOrgUnit;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _staffListAllocationItem;
        private EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem> _employeePostStaffRateItem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата смены оплаты труда. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getChangeDate()
     */
        public PropertyPath<Date> changeDate()
        {
            if(_changeDate == null )
                _changeDate = new PropertyPath<Date>(ChangeFinancingSourceEmplListExtractGen.P_CHANGE_DATE, this);
            return _changeDate;
        }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Учитывать дочерние подразделения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#isChildOrgUnit()
     */
        public PropertyPath<Boolean> childOrgUnit()
        {
            if(_childOrgUnit == null )
                _childOrgUnit = new PropertyPath<Boolean>(ChangeFinancingSourceEmplListExtractGen.P_CHILD_ORG_UNIT, this);
            return _childOrgUnit;
        }

    /**
     * @return Созданная должность ШР при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getStaffListAllocationItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> staffListAllocationItem()
        {
            if(_staffListAllocationItem == null )
                _staffListAllocationItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_STAFF_LIST_ALLOCATION_ITEM, this);
            return _staffListAllocationItem;
        }

    /**
     * @return Созданная ставка сотрудника при проведении выписки.
     * @see ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract#getEmployeePostStaffRateItem()
     */
        public EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem> employeePostStaffRateItem()
        {
            if(_employeePostStaffRateItem == null )
                _employeePostStaffRateItem = new EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem>(L_EMPLOYEE_POST_STAFF_RATE_ITEM, this);
            return _employeePostStaffRateItem;
        }

        public Class getEntityClass()
        {
            return ChangeFinancingSourceEmplListExtract.class;
        }

        public String getEntityName()
        {
            return "changeFinancingSourceEmplListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
