package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IPostAssignExtract;
import ru.tandemservice.moveemployee.entity.gen.EmployeeAddExtractGen;

public class EmployeeAddExtract extends EmployeeAddExtractGen implements IPostAssignExtract
{
    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }
}