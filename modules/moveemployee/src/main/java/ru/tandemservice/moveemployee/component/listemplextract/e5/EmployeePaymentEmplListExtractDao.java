/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e5;

import ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class EmployeePaymentEmplListExtractDao extends UniBaseDao implements IExtractComponentDao<EmployeePaymentEmplListExtract>
{
    @Override
    public void doCommit(EmployeePaymentEmplListExtract extract, Map parameters)
    {
        EmployeePayment employeePayment = new EmployeePayment();
        employeePayment.setAmount(extract.getAmountEmployee());
        employeePayment.setAssignDate(extract.getAssignmentDate());
        if (extract.getPayment().isOneTimePayment())
            employeePayment.setBeginDate(extract.getAssignmentDate());
        else
            employeePayment.setBeginDate(extract.getAssignmentBeginDate());
        employeePayment.setEndDate(extract.getAssignmentEndDate());
        employeePayment.setEmployeePost(extract.getEntity());
        employeePayment.setFinancingSource(extract.getFinancingSource());
        employeePayment.setFinancingSourceItem(extract.getFinancingSourceItem());
        employeePayment.setPayment(extract.getPayment());
        employeePayment.setPaymentValue(extract.getAmountPayment());

        save(employeePayment);

        extract.setCreateEmployeePayment(employeePayment);

        update(extract);
    }

    @Override
    public void doRollback(EmployeePaymentEmplListExtract extract, Map parameters)
    {
        if (extract.getCreateEmployeePayment() != null)
            delete(extract.getCreateEmployeePayment());
    }
}
