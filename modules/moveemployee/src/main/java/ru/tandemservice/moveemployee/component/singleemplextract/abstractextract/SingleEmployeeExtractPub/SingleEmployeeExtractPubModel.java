/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;

import java.util.Map;

/**
 * @author dseleznev
 *         Created on: 27.05.2009
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "extractId"),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public abstract class SingleEmployeeExtractPubModel<T extends SingleEmployeeExtract> implements IVisaOwnerModel
{
    private Long _extractId;
    private T _extract;

    private boolean _debugMode;
    private String _selectedTab;
    private String _attributesPage;
    private Long _visingStatus;
    private boolean _orderCanBeEdited;
    private CommonPostfixPermissionModel _secModel;
    private Map<String, Object> _visaListParameters = ParametersMap.createWith("visaOwnerModel", this);

    // IVisaOwnerModel

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    @Override
    public IAbstractDocument getDocument()
    {
        return _extract.getParagraph().getOrder();
    }

    @Override
    public boolean isVisaListModificable()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(_extract.getParagraph().getOrder().getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        return "employeeSingleOrder";
    }

    // Getters & Setters

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        this._extractId = extractId;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        this._extract = extract;
    }

    public boolean isDebugMode()
    {
        return _debugMode;
    }

    public void setDebugMode(boolean debugMode)
    {
        _debugMode = debugMode;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public String getAttributesPage()
    {
        return _attributesPage;
    }

    public void setAttributesPage(String attributesPage)
    {
        _attributesPage = attributesPage;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(Long visingStatus)
    {
        this._visingStatus = visingStatus;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this._secModel = secModel;
    }

    public boolean isOrderCanBeEdited()
    {
        return _orderCanBeEdited;
    }

    public void setOrderCanBeEdited(boolean orderCanBeEdited)
    {
        this._orderCanBeEdited = orderCanBeEdited;
    }
}