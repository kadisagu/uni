/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e4;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.moveemployee.entity.EmployeeTransferSExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 18.09.2009
 */
public class EmployeeTransferSExtractPrint implements IPrintFormCreator<EmployeeTransferSExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeTransferSExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder)extract.getParagraph().getOrder());
        List<FinancingSourceDetails> staffRateDetails = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);

        List<String> fieldsToRemoveParBefore = new ArrayList<String>();
        List<String> fieldsToRemoveParAfter = new ArrayList<String>();
        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);

        if(extract.isTemporaryTransfer()) modifier.put("transferType", "временно");
        else modifier.put("transferType", "постоянно");
        modifier.put("transferAddConditions", "");

        CommonExtractPrintUtil.injectOrgUnit(modifier, extract.getEmployeePost().getOrgUnit(), "Old");

        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity() != null ? extract.getEntity() : extract.getEmployeePost());
        StringBuilder postBuilder = new StringBuilder(CommonExtractPrintUtil.getPostPrintingTitle(extract.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.NOMINATIVE_CASE, true));

        Double staffRateSumm = 0.0d;
        for (EmployeePostStaffRateItem item : staffRateItems)
            staffRateSumm +=item.getStaffRate();
        if (staffRateItems.size() != 0 && staffRateSumm != 1)
            postBuilder.append(" (").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateSumm)).append(" ставки)");
        if (extract.getEmployeePost().isHourlyPaid())
            postBuilder.append(" на условиях почасовой оплаты");
        else
        {
            String postTypeCode = extract.getPostType().getCode();
            if (UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(postTypeCode))
                postBuilder.append(" на условиях внутривузовского совместительства");
            else if (UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(postTypeCode))
                postBuilder.append(" на условиях внешнего совместительства");
        }
        modifier.put("postOld", postBuilder.toString());

        if (null != extract.getEmployeePost().getCompetitionType())
            modifier.put("byCompetitionResultsOld", StringUtils.uncapitalize(extract.getEmployeePost().getCompetitionType().getTitle()));
        else
            fieldsToRemoveParBefore.add("byCompetitionResultsOld");

        CommonExtractPrintUtil.injectPostType(modifier, extract.getPostType());
        CommonExtractPrintUtil.injectPost(modifier, extract.getPostBoundedWithQGandQL(), null);
        CommonExtractPrintUtil.injectStaffRates(modifier, staffRateDetails);

        Double summStaffRate = 0.0d;
        for (FinancingSourceDetails details : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract))
            summStaffRate += details.getStaffRate();

        CommonExtractPrintUtil.injectWorkConditions(modifier, extract.getWeekWorkLoad(), extract.getWorkWeekDuration(), summStaffRate);

        if (null != extract.getCompetitionType())
            modifier.put("byCompetitionResults", StringUtils.uncapitalize(extract.getCompetitionType().getTitle()));
        else
            fieldsToRemoveParBefore.add("byCompetitionResults");

        boolean pps = EmployeeTypeCodes.EDU_STAFF.equals(extract.getPostBoundedWithQGandQL().getPost().getEmployeeType().getCode());
        CommonExtractPrintUtil.injectUrgpuLikeConditions(document, modifier, extract, staffRateDetails, pps);

        if(null != extract.getOptionalCondition())
            modifier.put("addConditions", extract.getOptionalCondition());
        else
            fieldsToRemoveParAfter.add("addConditions");
        //TODO: если внутривузовское совместительство, то "На условиях внутривузовского совместительства"
        //TODO: если внешнее совместительство, то "На условиях внешнего совместительства"
        /*TODO: на период отпуска по уходу за ребенком до 3-х лет Горбуновой О.В.
                на период нахождения Мининой М.В. в отпуске по уходу за ребенком до достижения им возраста 3 – х лет */
        //TODO: на сезонную работу
        //TODO: с неполным рабочим днем – 5 часов (с 8.00 до 13.00)
        //TODO: срочный трудовой договор на основании медицинского заключения от 11.01.2005  №13
        //TODO: с полной материальной ответственностью
        //TODO: в период – с 04.09.2006 по 28.05.2007 с сокращенной продолжительностью рабочего времени (36 часов в неделю) и с 29.05.2007с нормальной продолжительностью рабочего времени

        UniRtfUtil.removeParagraphsByNamesFromAnywhere(document, fieldsToRemoveParAfter, false, true);
        UniRtfUtil.removeParagraphsByNamesFromAnywhere(document, fieldsToRemoveParBefore, true, true);

        // hourly paid
        if(extract.isHourlyPaid())
        {
            modifier.put("salary", "часовая");
            modifier.put("salaryRub", "часовая");
            modifier.put("salaryKop", "");
        }

        modifier.put("payments", CommonExtractUtil.getBonusListStrRtf(extract));

        // Labour contract
        {
            EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEmployeePost());
            if (null != contract && contract.getNumber().equals(extract.getLabourContractNumber()) && contract.getBeginDate().equals(extract.getLabourContractDate()))
            {
                modifier.put("lcDay", RussianDateFormatUtils.getDayString(extract.getLabourContractDate(), true));
                modifier.put("lcMonthStr", RussianDateFormatUtils.getMonthName(extract.getLabourContractDate(), false));
                modifier.put("lcYear", RussianDateFormatUtils.getYearString(extract.getLabourContractDate(), true));
                modifier.put("labourContractNumber", (String)extract.getLabourContractNumber());

                StringBuilder builder = new StringBuilder(extract.getBasicListStr());
                /*if(null != extract.getCollateralAgreementNumber() || null != extract.getTransferDate())
                {
                    builder.append(builder.length() > 0 ? "; " : "");
                    builder.append("дополнительное соглашение к трудовому договору от ");
                    builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLabourContractDate())).append(" г.");
                }*/
                modifier.put("anotherDocument", builder.toString());

                StringBuilder addBasics = new StringBuilder();
                if (StringUtils.isEmpty(modifier.getStringValue("listBasics")))
                    addBasics.append(", ");
                addBasics.append("дополнительное соглашение");
                if (null != extract.getCollateralAgreementNumber())
                    addBasics.append(" № ").append(extract.getCollateralAgreementNumber());
                if (null != extract.getTransferDate())
                    addBasics.append(" от ");
                modifier.put("addBasicsList", addBasics.toString());
            }
            else
            {
                modifier.put("lcDay", "");
                modifier.put("lcMonthStr", "");
                modifier.put("lcYear", "");
                modifier.put("labourContractNumber", "");

                StringBuilder builder = new StringBuilder(extract.getBasicListStr());
                /*builder.append(builder.length() > 0 ? "; " : "");
                builder.append("трудовой договор от ");
                builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLabourContractDate())).append(" г.");
                builder.append(null != extract.getCompetitionType() ? ", заключенный " + StringUtils.uncapitalize(extract.getCompetitionType().getTitle()) : "");*/
                modifier.put("anotherDocument", builder.toString());
                modifier.put("addBasicsList", "");
            }
        }

        if (extract.getNoMore() != null && extract.getNoMore())
            modifier.put("less", "не более ");
        else
            modifier.put("less", "");

        if (extract.getHoursAmount() != null)
            modifier.put("hoursAmountWithHoursWord", extract.getHoursAmount().toString());
        else
            modifier.put("hoursAmountWithHoursWord", "");



        RtfTableModifier tableModifier = new RtfTableModifier();
        List<EmployeeBonus> bonusesList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract);

        List<String[]> stimulatingPaymentsList = new ArrayList<String[]>();
        List<String[]> compensativePaymentsList = new ArrayList<String[]>();
        List<String[]> stimulatingPayments = new ArrayList<String[]>();
        List<String[]> compensativePayments = new ArrayList<String[]>();

        replaceEmptyValuesWithSpaces(modifier, "minSalary", 8);
        replaceEmptyValuesWithSpaces(modifier, "raisingCoefficient", 8);
        replaceEmptyValuesWithSpaces(modifier, "raisingCoefficientRub", 8);
        replaceEmptyValuesWithSpaces(modifier, "personalRaisingCoefficient", 8);
        replaceEmptyValuesWithSpaces(modifier, "personalRaisingCoefficientRub", 8);

        List<String[]> result = new ArrayList<String[]>();
        for(EmployeeBonus bonus : bonusesList)
        {
            String[] bonusArr = new String[2];
            bonusArr[0] = bonus.getPayment().getTitle();
            bonusArr[1] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()) + " " + bonus.getPayment().getPaymentUnit().getShortTitle();
            result.add(bonusArr);

            PaymentType parentPaymentType = bonus.getPayment().getType();
            while(null != parentPaymentType.getParent()) parentPaymentType = parentPaymentType.getParent();

            String[] altBonusArr = new String[] {"", "- " + bonus.getPayment().getTitle() + "   " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()) + " " + bonus.getPayment().getPaymentUnit().getShortTitle()};
            if(UniempDefines.PAYMENT_TYPE_STLIMULATION.equals(parentPaymentType.getCode()))
                stimulatingPaymentsList.add(altBonusArr);
            else
                compensativePaymentsList.add(altBonusArr);
        }

        for(EmployeeBonus bonus : bonusesList)
        {
            String[] bonusArr = new String[2];
            bonusArr[0] = bonus.getPayment().getTitle();
            bonusArr[1] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()) + " " + bonus.getPayment().getPaymentUnit().getShortTitle();
            result.add(bonusArr);

            PaymentType parentPaymentType = bonus.getPayment().getType();
            while(null != parentPaymentType.getParent()) parentPaymentType = parentPaymentType.getParent();

            String[] altBonusArr = new String[] { bonus.getPayment().getTitle(),"   " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()) + " " + bonus.getPayment().getPaymentUnit().getShortTitle()};
            if(UniempDefines.PAYMENT_TYPE_STLIMULATION.equals(parentPaymentType.getCode()))
                stimulatingPayments.add(altBonusArr);
            else
                compensativePayments.add(altBonusArr);
        }

        tableModifier.put("T", result.toArray(new String[][]{}));
        if(stimulatingPaymentsList.isEmpty()) stimulatingPaymentsList.add(new String[] {"", ""});
        if(compensativePaymentsList.isEmpty()) compensativePaymentsList.add(new String[] {"", ""});
        if(stimulatingPayments.isEmpty()) stimulatingPaymentsList.add(new String[] {"", ""});
        if(compensativePayments.isEmpty()) compensativePaymentsList.add(new String[] {"", ""});
        tableModifier.put("STIMULATING_PAYMENTS_ALT", stimulatingPaymentsList.toArray(new String[][]{}));
        tableModifier.put("COMPENSATIVE_PAYMENTS_ALT", compensativePaymentsList.toArray(new String[][]{}));
        tableModifier.put("STIMULATING_PAYMENTS", stimulatingPayments.toArray(new String[][]{}));
        tableModifier.put("COMPENSATIVE_PAYMENTS", compensativePayments.toArray(new String[][]{}));
        tableModifier.modify(document);

        modifier.modify(document);

        return document;
    }
    
    private void replaceEmptyValuesWithSpaces(RtfInjectModifier modifier, String key, int spacesCount)
    {
        StringBuilder spacesString = new StringBuilder();
        for(int i = 0; i < spacesCount; i++) spacesString.append(" "); 
        if(StringUtils.isEmpty(modifier.getStringValue(key))) 
            modifier.put(key, spacesString.toString());
    }
}