/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSettings;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings;
import ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author esych
 * Created on: 17.01.2011
 */
@SuppressWarnings("unchecked")
public class DAO extends UniDao<Model> implements IDAO
{
    private static String DOCUMENT_TYPE_COMPOSITE_TITLE = "documentTypeCompositeTitle";

    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("rf");

    static
    {
        _orderSettings.setOrders(DOCUMENT_TYPE_COMPOSITE_TITLE,
            new OrderDescription(PaymentStringRepresentationFormat.documentType().title()));
        _orderSettings.setOrders(PaymentStringRepresentationFormat.paymentType().title(),
            new OrderDescription(PaymentStringRepresentationFormat.paymentType().title()));
        _orderSettings.setOrders(PaymentStringRepresentationFormat.payment().title(),
            new OrderDescription(PaymentStringRepresentationFormat.payment().title()));
    }

    public static String getSeparator()
    {
        return (String) DataSettingsFacade.getSettings("general",
            MoveEmployeeDefines.PAYMENT_PRINT_FORMAT_SETTINGS_PREFIX).get(MoveEmployeeDefines.PAYMENT_PRINT_FORMAT_SEPARATOR_SETTING);
    }

    @Override
    public void prepare(Model model, IBusinessComponent component)
    {
        model.setSeparator("«" + getSeparator() + "»");

        if (null != model.getPaymentDataSource())
            return;

        createPaymentDataSource(model, component);
        createFinancingSourceDataSource(model, component);
        createFinancingSourceItemDataSource(model, component);
        createPaymentUnitDataSource(model, component);

        createStringReprFormatDataSource(model, component);
    }

    protected void createPaymentDataSource(final Model model, IBusinessComponent component)
    {
        DynamicListDataSource<Payment> ds = new DynamicListDataSource<>(component, component1 -> {
            createPage(Payment.class, Payment.P_TITLE, model.getPaymentDataSource());
        });

        addColumns(ds, "Выплата", Payment.P_TITLE, false);

        model.setPaymentDataSource(ds);
    }

    protected void createFinancingSourceDataSource(final Model model, IBusinessComponent component)
    {
        DynamicListDataSource<FinancingSource> ds = new DynamicListDataSource<>(component, component1 -> {
            createPage(FinancingSource.class, FinancingSource.P_TITLE, model.getFinancingSourceDataSource());
        });

        addColumns(ds, "Источник финансирования", FinancingSource.P_TITLE, false);

        model.setFinancingSourceDataSource(ds);
    }

    protected void createFinancingSourceItemDataSource(final Model model, IBusinessComponent component)
    {
        DynamicListDataSource<FinancingSourceItem> ds = new DynamicListDataSource<>(component, component1 -> {
            createPage(FinancingSourceItem.class, FinancingSourceItem.P_TITLE, model.getFinancingSourceItemDataSource());
        });

        addColumns(ds, "Источник финансирования (детально)", FinancingSourceItem.P_TITLE, false);

        model.setFinancingSourceItemDataSource(ds);
    }

    protected void createPaymentUnitDataSource(final Model model, IBusinessComponent component)
    {
        DynamicListDataSource<PaymentUnit> ds = new DynamicListDataSource<>(component, component1 -> {
            createPage(PaymentUnit.class, PaymentUnit.P_TITLE, model.getPaymentUnitDataSource());
        });

        addColumns(ds, "Формат ввода выплаты", PaymentUnit.P_TITLE, true);

        model.setPaymentUnitDataSource(ds);
    }

    protected void createPage(Class clss, String sSortProperty, DynamicListDataSource ds)
    {
        List list = getSession().createCriteria(clss).addOrder(getOrder(ds, sSortProperty)).list();
        ds.setTotalSize(list.size());
        ds.setCountRow(list.size());
        ds.createPage(list);
    }

    protected Order getOrder(DynamicListDataSource ds, String sProperty)
    {
        return ds.getEntityOrder().getDirection() == OrderDirection.asc ? Order.asc(sProperty) : Order.desc(sProperty);
    }

    protected void addColumns(DynamicListDataSource ds, String sEntityColTitle, String sTitlePath, boolean addPlurals)
    {
        ds.addColumn(new SimpleColumn(sEntityColTitle, sTitlePath).setClickable(false).setOrderable(true));
        ds.addColumn(new SimpleColumn("Именительный падеж", ITitledWithCases.P_NOMINATIVE_CASE_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Родительный падеж", ITitledWithCases.P_GENITIVE_CASE_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Дательный падеж", ITitledWithCases.P_DATIVE_CASE_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Винительный падеж", ITitledWithCases.P_ACCUSATIVE_CASE_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Творительный падеж", ITitledWithCases.P_INSTRUMENTAL_CASE_TITLE).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Предложный падеж", ITitledWithCases.P_PREPOSITIONAL_CASE_TITLE).setClickable(false).setOrderable(false));
        if (addPlurals)
        {
            ds.addColumn(new SimpleColumn("Именительный падеж мн.ч.", ITitledWithPluralCases.P_PLURAL_NOMINATIVE_CASE_TITLE).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Родительный падеж мн.ч.", ITitledWithPluralCases.P_PLURAL_GENITIVE_CASE_TITLE).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Дательный падеж мн.ч.", ITitledWithPluralCases.P_PLURAL_DATIVE_CASE_TITLE).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Винительный падеж мн.ч.", ITitledWithPluralCases.P_PLURAL_ACCUSATIVE_CASE_TITLE).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Творительный падеж мн.ч.", ITitledWithPluralCases.P_PLURAL_INSTRUMENTAL_CASE_TITLE).setClickable(false).setOrderable(false));
            ds.addColumn(new SimpleColumn("Предложный падеж мн.ч.", ITitledWithPluralCases.P_PLURAL_PREPOSITIONAL_CASE_TITLE).setClickable(false).setOrderable(false));
        }
        ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditCases"));
    }

    protected void createStringReprFormatDataSource(final Model model, IBusinessComponent component)
    {
        DynamicListDataSource<ViewWrapper<PaymentStringRepresentationFormat>> ds =
                new DynamicListDataSource<>(component, component1 -> {
                    MQBuilder builder = new MQBuilder(PaymentStringRepresentationFormat.ENTITY_CLASS, "rf");
                    _orderSettings.applyOrder(builder, model.getStringReprFormatDataSource().getEntityOrder());
                    List<PaymentStringRepresentationFormat> list = builder.getResultList(getSession());

                    List<ViewWrapper<PaymentStringRepresentationFormat>> wrappers = new ArrayList();
                    for (PaymentStringRepresentationFormat format : list)
                    {
                        ViewWrapper<PaymentStringRepresentationFormat> wrapper
                                = new ViewWrapper<>(format);

                        String sTypeTitle = "";
                        PaymentStringFormatDocumentSettings type = format.getDocumentType();
                        while (null != type)
                        {
                            sTypeTitle = type.getTitle() + (sTypeTitle.isEmpty() ? "" : " / ") + sTypeTitle;
                            type = type.getParent();
                        }

                        wrapper.setViewProperty(DOCUMENT_TYPE_COMPOSITE_TITLE, sTypeTitle);
                        wrappers.add(wrapper);
                    }

                    model.getStringReprFormatDataSource().setTotalSize(wrappers.size());
                    model.getStringReprFormatDataSource().setCountRow(wrappers.size());
                    model.getStringReprFormatDataSource().createPage(wrappers);
                });

        ds.addColumn(new SimpleColumn("Тип документа", DOCUMENT_TYPE_COMPOSITE_TITLE).setClickable(false));
        ds.addColumn(new SimpleColumn("Тип выплаты", PaymentStringRepresentationFormat.paymentType().title()).setClickable(false));
        ds.addColumn(new SimpleColumn("Выплата", PaymentStringRepresentationFormat.payment().title()).setClickable(false));
        ds.addColumn(new SimpleColumn("Формат", PaymentStringRepresentationFormat.formatString()).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Пример", PaymentStringRepresentationFormat.exampleString()).setClickable(false).setOrderable(false));

        ds.addColumn(new ToggleColumn("Использовать", PaymentStringRepresentationFormat.active())
        .setListener("onClickToggleStringFormatActive"));

        ds.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditStringFormat"));
        ds.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteStringFormat",
            "Удалить формат вывода выплат?"));

        model.setStringReprFormatDataSource(ds);
    }

    @Override
    public void deleteStringFormat(Long lID)
    {
        delete(lID);
    }

    @Override
    public void updateToggleStringFormatActive(Model model, Long lID)
    {
        PaymentStringRepresentationFormat toggledFormat = get(lID);

        if (!toggledFormat.isActive())
            deactivateIdenticalStringFormats(toggledFormat, getList(PaymentStringRepresentationFormat.class),
                getSession());

        toggledFormat.setActive(!toggledFormat.isActive());
        update(toggledFormat);
    }

    public static void deactivateIdenticalStringFormats(PaymentStringRepresentationFormat activatedFormat,
        List<PaymentStringRepresentationFormat> formats, Session session)
    {
        for(PaymentStringRepresentationFormat format : formats)
        {
            if (format.isActive() && format != activatedFormat
                    && format.getDocumentType() == activatedFormat.getDocumentType()
                    && format.getPaymentType() == activatedFormat.getPaymentType()
                    && format.getPayment() == activatedFormat.getPayment())
            {
                format.setActive(false);
                session.update(format);
            }
        }
    }
}
