/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5.Pub;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.MainRow;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.Row;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;
import ru.tandemservice.moveemployee.entity.HolidayInEmpHldyExtractRelation;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 03.12.2008
 */
public class DAO extends ModularEmployeeExtractPubDAO<EmployeeHolidayExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setStaffRateStr(CommonEmployeeExtractUtil.getExtractFinancingSourceDetailsString(model.getExtract(), getSession(), false, null));

        if (model.getExtract().getVacationScheduleItem() != null)
        {
            model.setVacationScheduleStr(model.getExtract().getVacationSchedule().getSimpleTitleWithYear());
            model.setVacationScheduleItemStr(model.getExtract().getVacationScheduleItem().getPlanedVacationTitle());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareHolidayDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(HolidayInEmpHldyExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", HolidayInEmpHldyExtractRelation.L_EMPLOYEE_HOLIDAY_EXTRACT, model.getExtract()));
        builder.addOrder("b", HolidayInEmpHldyExtractRelation.P_BEGIN_PERIOD);
        builder.addOrder("b", HolidayInEmpHldyExtractRelation.P_START_HOLIDAY);

        List<HolidayInEmpHldyExtractRelation> relationList = builder.getResultList(getSession());

        //восстанавливаем список отпусков
        List<MainRow> mainRowList = new ArrayList<>();
        for (HolidayInEmpHldyExtractRelation relation : relationList)
        {
            MainRow mainRow = new MainRow(relation.getMainRowId(), null, null);
            if (!mainRowList.contains(mainRow))
            {
                List<Row> rowList = new ArrayList<>();
                Row row = new Row(relation.getRowId(), mainRow, relation.getRowType());

                model.getBeginPeriodMap().put(row, relation.getBeginPeriod());
                model.getEndPeriodMap().put(row, relation.getEndPeriod());
                model.getStartHolidayMap().put(row, relation.getStartHoliday());
                model.getFinishHolidayMap().put(row, relation.getFinishHoliday());
                model.getDurationHolidayMap().put(row, relation.getDurationHoliday());

                rowList.add(row);
                mainRow.setRowList(rowList);
                mainRow.setType(relation.getRowType());
                mainRowList.add(mainRow);
            }
            else
            {
                MainRow mainRow1 = mainRowList.get(mainRowList.indexOf(mainRow));
                mainRow1.setType(relation.getRowType());
                Row row = new Row(relation.getRowId(), mainRow1, relation.getRowType());

                model.getBeginPeriodMap().put(row, relation.getBeginPeriod());
                model.getEndPeriodMap().put(row, relation.getEndPeriod());
                model.getStartHolidayMap().put(row, relation.getStartHoliday());
                model.getFinishHolidayMap().put(row, relation.getFinishHoliday());
                model.getDurationHolidayMap().put(row, relation.getDurationHoliday());

                mainRow1.getRowList().add(row);
            }
        }

        model.getHolidayDataSource().setCountRow(mainRowList.size());

        Map<IdentifiableWrapper, MainRow> wrapperMainRowMap = new HashMap<>();
        for (MainRow mainRow : mainRowList)
        {
            IdentifiableWrapper wrapper = new IdentifiableWrapper(mainRow.getId(), "");
            wrapperMainRowMap.put(wrapper, mainRow);
        }

        List<IdentifiableWrapper> wrapperList = new ArrayList<>();
        for (Map.Entry<IdentifiableWrapper, MainRow> entry : wrapperMainRowMap.entrySet())
            wrapperList.add(entry.getKey());

        UniBaseUtils.createPage(model.getHolidayDataSource(), wrapperList);

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getHolidayDataSource()))
        {
            List<IdentifiableWrapper> periodList = new ArrayList<>();
            List<IdentifiableWrapper> holidayList = new ArrayList<>();
            long i = 0;
            List<Row> rowList = wrapperMainRowMap.get((IdentifiableWrapper) wrapper.getEntity()).getRowList();
            for (Row row : rowList)
            {
                i++;
                if (model.getBeginPeriodMap().get(row) != null && model.getEndPeriodMap().get(row) != null)
                    periodList.add(new IdentifiableWrapper(i, DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getBeginPeriodMap().get(row)) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEndPeriodMap().get(row))));

                if (model.getStartHolidayMap().get(row) != null && model.getFinishHolidayMap().get(row) != null)
                    holidayList.add(new IdentifiableWrapper(i, DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getStartHolidayMap().get(row)) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFinishHolidayMap().get(row)) + ", " + NumberConvertingUtil.getCalendarDaysWithName(model.getDurationHolidayMap().get(row))));
                else
                    holidayList.add(new IdentifiableWrapper(i, NumberConvertingUtil.getCalendarDaysWithName(model.getDurationHolidayMap().get(row))));
            }
            wrapper.setViewProperty(Model.PERIOD, periodList);
            wrapper.setViewProperty(Model.HOLIDAY, holidayList);
        }
    }
}