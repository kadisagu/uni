/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.ui.formatters.EmployeeCodeFormatter;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit.AbstractListExtractAddEditModel;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

/**
 * @author dseleznev
 *         Created on: 28.10.2009
 */
@SuppressWarnings("unchecked")
public abstract class AbstractListOrderPubController<T extends EmployeeListOrder, IDAO extends IAbstractListOrderPubDAO<T, Model>, Model extends AbstractListOrderPubModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setActionsPage("ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderPub.ListOrderPubActions");
        model.setParamsPage("ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderPub.ListOrderPubParams");

        getDao().prepare(model);

        // настройка действует в рамках типа приказа
        model.setSearchListSettingsKey("EmployeeListOrder." + model.getOrder().getType().getCode() + ".");

        prepareListDataSource(component);
    }

    private void rebuildListDataSource(IBusinessComponent component)
    {
        getModel(component).setDataSource(null);
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ListEmployeeExtract> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Таб. №", new String[]{ListEmployeeExtract.L_EMPLOYEE, Employee.P_EMPLOYEE_CODE}, new EmployeeCodeFormatter()).setClickable(false).setOrderable(false));
        AbstractColumn linkColumn = new PublisherLinkColumn("ФИО", new String[]{ListEmployeeExtract.L_EMPLOYEE, Employee.L_PERSON, Person.P_FULLFIO}).setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public String getComponentName(IEntity entity)
            {
                return MoveEmployeeUtils.getListExtractPubComponent(((EmployeeExtractType) entity.getProperty(IAbstractExtract.L_TYPE)).getIndex());
            }
        }).setOrderable(false);
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Пол", new String[]{ListEmployeeExtract.L_EMPLOYEE, Employee.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.L_SEX, Sex.P_SHORT_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата рождения", new String[]{ListEmployeeExtract.L_EMPLOYEE, Employee.L_PERSON, Person.L_IDENTITY_CARD, IdentityCard.P_BIRTH_DATE}, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));

        prepareListDataSource(dataSource, component);

        if (!model.getOrder().isReadonly())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditExtract").setPermissionKey(model.getSecModel().getPermission("editExtract")).setDisabledProperty(ListEmployeeExtract.P_NO_EDIT));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteExtract", "Исключить сотрудника из параграфа «{0}»?", new Object[]{new String[]{IAbstractExtract.L_ENTITY, Employee.L_PERSON, Person.P_FULLFIO}}).setPermissionKey(model.getSecModel().getPermission("deleteExtract")));
        }

        model.setDataSource(dataSource);
    }

    // extension point

    protected abstract void prepareListDataSource(DynamicListDataSource dataSource, IBusinessComponent component);

    // Event Listeners

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendToCoordination(getModel(component), null);
        else
            getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
        rebuildListDataSource(component);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        T order = getModel(component).getOrder();
        int orderTypeIndex = order.getType().getIndex();

        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListOrderAddEditComponent(orderTypeIndex), new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())));
    }

    public void onClickAddExtract(IBusinessComponent component)
    {
        Model model = getModel(component);
        EmployeeExtractType extractType = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractTypeByOrderType(model.getOrder().getType());
        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListExtractAddEditComponent(extractType.getIndex()), new ParametersMap()
                .add(AbstractListExtractAddEditModel.PARAMETER_ORDER_ID, model.getOrderId()).add(AbstractListExtractAddEditModel.PARAMETER_EXTRACT_TYPE_ID, extractType.getId()).add(AbstractListExtractAddEditModel.PARAMETER_EXTRACT_ID, null)));
    }

    public void onClickAddMultipleExtract(IBusinessComponent component)
    { //TODO
        /*T order = getModel(component).getOrder();
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_LIST_MULTIPLE_EXTRACT_ADD, new ParametersMap()
                .add("orderId", order.getId())
        ));*/
    }

    public void onClickEditExtract(IBusinessComponent component)
    {
        ListEmployeeExtract extract = getDao().getNotNull(ListEmployeeExtract.class, (Long) component.getListenerParameter());
        int extractIndex = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractTypeByOrderType(getModel(component).getOrder().getType()).getIndex();//extract.getType().getIndex();
        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getListExtractAddEditComponent(extractIndex), new ParametersMap().add("extractId", extract.getId())));
    }

    public void onClickReject(IBusinessComponent component)
    {
        getDao().doReject(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickCommit(IBusinessComponent component)
    {
        getDao().doCommit(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickRollback(IBusinessComponent component)
    {
        getDao().doRollback(getModel(component));
        rebuildListDataSource(component);
    }

    public void onClickDeleteOrder(IBusinessComponent component)
    {
        deactivate(component);
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(getModel(component).getOrder());
    }

    public void onClickPrintOrder(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_LIST_ORDER_PRINT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())));
    }

    public void onClickDeleteExtract(IBusinessComponent component)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().deleteListExtract((Long) component.getListenerParameter());
        rebuildListDataSource(component);
    }
}