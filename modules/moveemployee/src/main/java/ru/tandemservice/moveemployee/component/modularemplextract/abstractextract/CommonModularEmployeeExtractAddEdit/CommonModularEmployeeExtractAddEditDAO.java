/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractAddEdit.ModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmpExtractToBasicRelation;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToTextParagraphRel;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToTypeRel;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;

/**
 * @author dseleznev
 * Created on: 13.11.2008
 */
public abstract class CommonModularEmployeeExtractAddEditDAO<T extends ModularEmployeeExtract, Model extends CommonModularEmployeeExtractAddEditModel<T>> extends ModularEmployeeExtractAddEditDAO<T, Model>
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setReasonList(getFirstList(EmployeeReasonToTypeRel.class, model.getExtractType()));
        model.setBasicListModel(new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getExtract() == null)
                    return ListResult.getEmpty();

                Criteria c = getSession().createCriteria(EmployeeReasonToBasicRel.class, "r");
                c.createAlias("r." + IEntityRelation.L_SECOND, "s");
                c.add(Restrictions.eq("r." + IEntityRelation.L_FIRST, model.getExtract().getReason()));
                c.setProjection(Projections.property("r." + IEntityRelation.L_SECOND));
                c.add(Restrictions.like("s." + EmployeeOrderBasics.P_TITLE, CoreStringUtils.escapeLike(filter)).ignoreCase());
                c.addOrder(Order.asc(("s." + EmployeeOrderBasics.P_TITLE)));
                return new ListResult(c.list());
            }
        });

        if (model.isEditForm())
        {
            List<EmpExtractToBasicRelation> relationList = getList(EmpExtractToBasicRelation.class, EmpExtractToBasicRelation.L_EXTRACT, model.getExtract());

            model.getSelectedBasicList().clear();
            for (EmpExtractToBasicRelation relation : relationList)
                model.getSelectedBasicList().add(relation.getBasic());
            model.setSelectedBasicList(model.getSelectedBasicList());

            model.getCurrentBasicMap().clear();
            for (EmpExtractToBasicRelation relation : relationList)
            {
                model.getCurrentBasicMap().put(relation.getBasic().getId(), relation.getComment());
                model.getDefaultBasicMap().put(relation.getBasic().getId(), relation.getComment());
            }
        }

        model.setTextParagraph(model.getExtract().getTextParagraph());
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        model.getExtract().setTextParagraph(model.getTextParagraph());
        updateBasicList(model);
    }

    private void updateBasicList(Model model)
    {
        if (model.isEditForm())
        {
            for (EmpExtractToBasicRelation relation : getList(EmpExtractToBasicRelation.class, EmpExtractToBasicRelation.L_EXTRACT, model.getExtract()))
                delete(relation);
            getSession().flush();
        }

        for (EmployeeOrderBasics basic : model.getSelectedBasicList())
        {
            EmpExtractToBasicRelation relation = new EmpExtractToBasicRelation();
            relation.setBasic(basic);
            relation.setExtract(model.getExtract());
            relation.setComment(model.getCurrentBasicMap().get(basic.getId()));
            save(relation);
        }
    }

    public void prepareTextParagraph(Model model)
    {
        MQBuilder builder = new MQBuilder(EmployeeReasonToTextParagraphRel.ENTITY_CLASS, "b", new String[] {EmployeeReasonToTextParagraphRel.P_TEXT_PARAGRAPH});
        builder.add(MQExpression.eq("b", EmployeeReasonToTextParagraphRel.L_EMPLOYEE_ORDER_REASON, model.getExtract().getReason()));
        model.setTextParagraph((String) builder.uniqueResult(getSession()));
    }

}