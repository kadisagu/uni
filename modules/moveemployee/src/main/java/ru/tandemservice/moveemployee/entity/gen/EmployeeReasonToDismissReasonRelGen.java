package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь причины и причины увольнения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeReasonToDismissReasonRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel";
    public static final String ENTITY_NAME = "employeeReasonToDismissReasonRel";
    public static final int VERSION_HASH = 2083865185;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_ORDER_REASON = "employeeOrderReason";
    public static final String L_EMPLOYEE_DISMISS_REASON = "employeeDismissReason";

    private EmployeeOrderReasons _employeeOrderReason;     // Причина приказа по кадрам
    private EmployeeDismissReasons _employeeDismissReason;     // Причины увольнения сотрудников

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EmployeeOrderReasons getEmployeeOrderReason()
    {
        return _employeeOrderReason;
    }

    /**
     * @param employeeOrderReason Причина приказа по кадрам. Свойство не может быть null и должно быть уникальным.
     */
    public void setEmployeeOrderReason(EmployeeOrderReasons employeeOrderReason)
    {
        dirty(_employeeOrderReason, employeeOrderReason);
        _employeeOrderReason = employeeOrderReason;
    }

    /**
     * @return Причины увольнения сотрудников. Свойство не может быть null.
     */
    @NotNull
    public EmployeeDismissReasons getEmployeeDismissReason()
    {
        return _employeeDismissReason;
    }

    /**
     * @param employeeDismissReason Причины увольнения сотрудников. Свойство не может быть null.
     */
    public void setEmployeeDismissReason(EmployeeDismissReasons employeeDismissReason)
    {
        dirty(_employeeDismissReason, employeeDismissReason);
        _employeeDismissReason = employeeDismissReason;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeReasonToDismissReasonRelGen)
        {
            setEmployeeOrderReason(((EmployeeReasonToDismissReasonRel)another).getEmployeeOrderReason());
            setEmployeeDismissReason(((EmployeeReasonToDismissReasonRel)another).getEmployeeDismissReason());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeReasonToDismissReasonRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeReasonToDismissReasonRel.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeReasonToDismissReasonRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeOrderReason":
                    return obj.getEmployeeOrderReason();
                case "employeeDismissReason":
                    return obj.getEmployeeDismissReason();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeOrderReason":
                    obj.setEmployeeOrderReason((EmployeeOrderReasons) value);
                    return;
                case "employeeDismissReason":
                    obj.setEmployeeDismissReason((EmployeeDismissReasons) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeOrderReason":
                        return true;
                case "employeeDismissReason":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeOrderReason":
                    return true;
                case "employeeDismissReason":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeOrderReason":
                    return EmployeeOrderReasons.class;
                case "employeeDismissReason":
                    return EmployeeDismissReasons.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeReasonToDismissReasonRel> _dslPath = new Path<EmployeeReasonToDismissReasonRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeReasonToDismissReasonRel");
    }
            

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel#getEmployeeOrderReason()
     */
    public static EmployeeOrderReasons.Path<EmployeeOrderReasons> employeeOrderReason()
    {
        return _dslPath.employeeOrderReason();
    }

    /**
     * @return Причины увольнения сотрудников. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel#getEmployeeDismissReason()
     */
    public static EmployeeDismissReasons.Path<EmployeeDismissReasons> employeeDismissReason()
    {
        return _dslPath.employeeDismissReason();
    }

    public static class Path<E extends EmployeeReasonToDismissReasonRel> extends EntityPath<E>
    {
        private EmployeeOrderReasons.Path<EmployeeOrderReasons> _employeeOrderReason;
        private EmployeeDismissReasons.Path<EmployeeDismissReasons> _employeeDismissReason;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel#getEmployeeOrderReason()
     */
        public EmployeeOrderReasons.Path<EmployeeOrderReasons> employeeOrderReason()
        {
            if(_employeeOrderReason == null )
                _employeeOrderReason = new EmployeeOrderReasons.Path<EmployeeOrderReasons>(L_EMPLOYEE_ORDER_REASON, this);
            return _employeeOrderReason;
        }

    /**
     * @return Причины увольнения сотрудников. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToDismissReasonRel#getEmployeeDismissReason()
     */
        public EmployeeDismissReasons.Path<EmployeeDismissReasons> employeeDismissReason()
        {
            if(_employeeDismissReason == null )
                _employeeDismissReason = new EmployeeDismissReasons.Path<EmployeeDismissReasons>(L_EMPLOYEE_DISMISS_REASON, this);
            return _employeeDismissReason;
        }

        public Class getEntityClass()
        {
            return EmployeeReasonToDismissReasonRel.class;
        }

        public String getEntityName()
        {
            return "employeeReasonToDismissReasonRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
