package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.entity.gen.CombinationPostStaffRateExtractRelationGen;

import java.math.BigDecimal;

/**
 * Связь выписки и ставки должности по совмещению
 */
public class CombinationPostStaffRateExtractRelation extends CombinationPostStaffRateExtractRelationGen
{
    public void setStaffRate(double value)
    {
        setStaffRateInteger((int) (value * 10000));
    }

    public double getStaffRate()
    {
        BigDecimal x = new java.math.BigDecimal(getStaffRateInteger() * 0.0001);
        x = x.setScale(3, BigDecimal.ROUND_HALF_UP);

        return x.doubleValue();
    }
}