/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e23.AddEdit;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.ICommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.moveemployee.entity.IncDecCombinationExtract;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;

import java.util.List;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 23.12.2011
 */
public interface IDAO extends ICommonModularEmployeeExtractAddEditDAO<IncDecCombinationExtract, Model>
{
    public void prepareStaffRateDataSource(Model model);

    public void prepareEmployeeStaffRateDataSource(Model model);

    public Map<CombinationPostStaffRateItem, List<StaffListAllocationItem>> getCombinationPostStaffRateAllocMap(Model model);

    public Map<CombinationPostStaffRateExtractRelation, List<CombinationPostStaffRateExtAllocItemRelation>> getStaffRateAllocItemMap(Model model);
}