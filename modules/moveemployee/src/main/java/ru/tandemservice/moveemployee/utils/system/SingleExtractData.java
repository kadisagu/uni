/**
 * $Id$
 */
package ru.tandemservice.moveemployee.utils.system;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public class SingleExtractData
{
    //список всех entity выписок из индивидуального приказа
    public static final String[][] EXTRACT_LIST = new String[][]{
        /* 1*/{"employeeAddSExtract", "О приеме сотрудника"},
        /* 2*/{"employeeTempAddSExtract", "О приеме сотрудника на время отсутствия другого"},
        /* 3*/{"employeePostAddSExtract", "О назначении на должность"},
        /* 4*/{"employeeTransferSExtract", "О переводе"},
        /* 5*/{"employeeHolidaySExtract", "Об отпуске"},
        /* 6*/{"voluntaryTerminationSExtract", "Об увольнении"},
        /* 7*/{"employeeAddPaymentsSExtract", "Об установлении доплаты"},
        /* 8*/{"employeeEncouragementSExtract", "О поощрении работника"}
    };
}