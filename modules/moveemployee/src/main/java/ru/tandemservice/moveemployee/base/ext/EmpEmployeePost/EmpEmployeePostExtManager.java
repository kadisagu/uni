/**
 *$Id$
 */
package ru.tandemservice.moveemployee.base.ext.EmpEmployeePost;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.moveemployee.base.ext.EmpEmployeePost.logic.EmpEmployeePostExtDAO;
import ru.tandemservice.uniemp.base.bo.EmpEmployeePost.logic.IEmpEmployeePostDAO;

/**
 * Create by ashaburov
 * Date 31.05.12
 */
@Configuration
public class EmpEmployeePostExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEmpEmployeePostDAO employeePostDAO()
    {
        return new EmpEmployeePostExtDAO();
    }
}
