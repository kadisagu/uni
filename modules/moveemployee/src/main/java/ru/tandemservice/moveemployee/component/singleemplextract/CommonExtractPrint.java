/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.singleemplextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.commons.IEmployeeOrgUnitExtract;
import ru.tandemservice.moveemployee.component.commons.IEmployeePostExtract;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.*;

/**
 * @author dseleznev
 *         Created on: 27.05.2009
 */
public class CommonExtractPrint implements ICommonExtractPrint
{
    public static RtfDocument createModularExtract(byte[] template, SingleEmployeeExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createSingleExtractInjectModifier(extract);
        modifier.modify(document);
        return document;
    }

    public static RtfInjectModifier createSingleExtractInjectModifier(SingleEmployeeExtract extract)
    {
        IEntityMeta meta = EntityRuntime.getMeta(extract.getId());

        final RtfInjectModifier modifier = new RtfInjectModifier();
        CommonExtractPrintUtil.injectCommonExtractData(modifier, extract);

        // reason
        modifier.put("reason", extract.getReason().getPrintTitle() == null ? extract.getReason().getTitle() : extract.getReason().getPrintTitle());

        // listBasics
        modifier.put("listBasics", extract.getBasicListStr());

        // fio
        String fio = extract.getEmployeeFioModified();
        modifier.put("fio", fio);

        int indexOfSpace = fio.indexOf(" ");
        String fioCapitalized = indexOfSpace <= 0 ? fio.toUpperCase() : (fio.substring(0, indexOfSpace).toUpperCase() + fio.substring(indexOfSpace));
        modifier.put("fioCapitalized", fioCapitalized);

        CommonExtractPrintUtil.injectCommonEmployeeData(modifier, extract.getEmployee());

        // OrgUnit title
        OrgUnit orgUnit = null;
        if (extract instanceof IEmployeeOrgUnitExtract)
            orgUnit = ((IEmployeeOrgUnitExtract) extract).getOrgUnit();
        else if (null != extract.getEntity())
            orgUnit = extract.getEntity().getOrgUnit();

        CommonExtractPrintUtil.injectOrgUnit(modifier, orgUnit, null);

        // post title
        PostBoundedWithQGandQL post = null;
        if (extract instanceof IEmployeePostExtract)
            post = ((IEmployeePostExtract) extract).getPostBoundedWithQGandQL();
        else if (null != extract.getEntity())
            post = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL();

        CommonExtractPrintUtil.injectPost(modifier, post, null);


        // SPECIAL CODES //

        // beginDate
        if (meta.getProperty("beginDate") != null)
            modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) extract.getProperty("beginDate")));

        // endDate
        if (meta.getProperty("endDate") != null)
        {
            if (null != extract.getProperty("endDate"))
                modifier.put("endDate", DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) extract.getProperty("endDate")));
            else
                modifier.put("endDate", "-");
        }

        // etksLevel
        if (meta.getProperty("etksLevel") != null)
            modifier.put("etksLevel", (String) ((IEntity) extract.getProperty("etksLevels")).getProperty("title"));
        else
            modifier.put("etksLevel", "__");

        Double sumStaffRate = null;
        if (meta.getProperty("budgetStaffRate") != null && meta.getProperty("offBudgetStaffRate") != null)
        {
            Double budgetStaffRate = (Double) extract.getProperty("budgetStaffRate");
            Double offBudgetStaffRate = (Double) extract.getProperty("offBudgetStaffRate");
            sumStaffRate = (null != budgetStaffRate ? budgetStaffRate : 0d) + (null != offBudgetStaffRate ? offBudgetStaffRate : 0d);
        }

        // salary
        if (meta.getProperty("salary") != null)
        {
            double salary = (Double) extract.getProperty("salary");
            modifier.put("salary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(salary) + " рублей");

            long rub = (long) salary;
            long kop = Math.round((salary - rub) * 100);
            modifier.put("salaryRub", String.valueOf(rub));
            modifier.put("salaryKop", (kop < 10 ? "0" : "") + String.valueOf(kop));

            if (null != post && null != post.getProfQualificationGroup().getMinSalary())
            {
                modifier.put("minSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(post.getProfQualificationGroup().getMinSalary()));
                modifier.put("minSalaryRub", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(post.getProfQualificationGroup().getMinSalary()) + " рублей");

                if(null != sumStaffRate)
                {
                    modifier.put("minSalarySRProp", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(post.getProfQualificationGroup().getMinSalary() * sumStaffRate));
                    modifier.put("minSalaryRubSRProp", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(post.getProfQualificationGroup().getMinSalary() * sumStaffRate) + " рублей");
                }
            } else
            {
                modifier.put("minSalary", String.valueOf(rub));
                modifier.put("minSalaryRub", String.valueOf(rub) + " рублей");
                modifier.put("minSalarySRProp", String.valueOf(rub));
                modifier.put("minSalaryRubSRProp", String.valueOf(rub) + " рублей");
            }
        }

        // raisingCoefficient
        if (meta.getProperty("raisingCoefficient") != null && null != extract.getProperty("raisingCoefficient"))
        {
            SalaryRaisingCoefficient raisingCoefficient = (SalaryRaisingCoefficient) extract.getProperty("raisingCoefficient");
            if (null != raisingCoefficient.getProfQualificationGroup().getMinSalary())
            {
                modifier.put("minSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(raisingCoefficient.getProfQualificationGroup().getMinSalary()));
                modifier.put("minSalaryRub", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(raisingCoefficient.getProfQualificationGroup().getMinSalary()) + " рублей");

                if(null != sumStaffRate)
                {
                    modifier.put("minSalarySRProp", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(raisingCoefficient.getProfQualificationGroup().getMinSalary() * sumStaffRate));
                    modifier.put("minSalaryRubSRProp", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(raisingCoefficient.getProfQualificationGroup().getMinSalary() * sumStaffRate) + " рублей");
                }
            }
            modifier.put("raisingCoefficient", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(raisingCoefficient.getRaisingCoefficient()));
            modifier.put("raisingCoefficientRub", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(raisingCoefficient.getRecommendedSalary()));
            modifier.put("raisingCoefficientCurrency", "рублей");

            if(null != sumStaffRate)
            {
                modifier.put("raisingCoefficientRubSRProp", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(raisingCoefficient.getRecommendedSalary() * sumStaffRate));
            }

        } else
        {
            modifier.put("raisingCoefficient", "");
            modifier.put("raisingCoefficientRub", "");
            modifier.put("raisingCoefficientRubSRProp", "");
            modifier.put("raisingCoefficientCurrency", "");
        }

        modifier.put("personalRaisingCoefficient", "");
        modifier.put("personalRaisingCoefficientRub", "");
        modifier.put("personalRaisingCoefficientRubSRProp", "");
        modifier.put("personalRaisingCoefficientCurrency", "");

        // labour contract number
        if (meta.getProperty("labourContractNumber") != null)
            modifier.put("labourContractNumber", (String) extract.getProperty("labourContractNumber"));

        String bonusList = CommonExtractUtil.getFormattedBonusListStrRtf(extract);

        int size = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract).size();
        String preamble =  bonusList.isEmpty() ? "" : ((1 == size ? "установить выплату " : 1 < size ? "установить выплаты " : ""));

        modifier.put("bonusListFormatted", bonusList.isEmpty() ? CommonExtractUtil.getBonusListStrRtf(extract) : bonusList);
        modifier.put("bonusListFormatted_lower", preamble + (bonusList.isEmpty() ? CommonExtractUtil.getBonusListStrRtf(extract) : bonusList));

        if (extract.getTextParagraph() != null)
            modifier.put("textParagraph", extract.getTextParagraph());
        else
            modifier.put("textParagraph", "");

        return modifier;
    }

    @Override
    @SuppressWarnings("deprecation")
    public void appendVisas(RtfDocument document, EmployeeSingleExtractOrder order)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        List<String[]> visaData = new ArrayList<>();
        List<String[]> primaryVisaData = new ArrayList<>();
        List<String[]> primaryVisaDataPSND = new ArrayList<>();
        List<String[]> secondaryVisaData = new ArrayList<>();
        List<String[]> secondaryVisaDataPSND = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());
        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), "", "", "", str.toString()});

            if (visa.getGroupMemberVising().getCode().equals("2"))
            {
                primaryVisaData.add(new String[]{"Руководитель организации", visa.getPossibleVisa().getTitle(), "", "", "", str.toString()});
                primaryVisaDataPSND.add(new String[]{visa.getPossibleVisa().getTitle(), "", str.toString(), ""});
            } else
            {
                secondaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
                secondaryVisaDataPSND.add(new String[]{visa.getPossibleVisa().getTitle(), "", str.toString(), ""});
            }
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        if (primaryVisaData.isEmpty()) UniRtfUtil.removeTableByName(document, "PRIMARY_VISAS", true, false);
        else tableModifier.put("PRIMARY_VISAS", primaryVisaData.toArray(new String[primaryVisaData.size()][]));
        if (primaryVisaDataPSND.isEmpty()) UniRtfUtil.removeTableByName(document, "PRIMARY_VISAS_PSND", true, false);
        else
            tableModifier.put("PRIMARY_VISAS_PSND", primaryVisaDataPSND.toArray(new String[primaryVisaDataPSND.size()][]));
        if (secondaryVisaData.isEmpty()) UniRtfUtil.removeTableByName(document, "SECONDARY_VISAS", true, false);
        else tableModifier.put("SECONDARY_VISAS", secondaryVisaData.toArray(new String[secondaryVisaData.size()][]));
        if (secondaryVisaDataPSND.isEmpty())
            UniRtfUtil.removeTableByName(document, "SECONDARY_VISAS_PSND", true, false);
        else
            tableModifier.put("SECONDARY_VISAS_PSND", secondaryVisaDataPSND.toArray(new String[secondaryVisaDataPSND.size()][]));
        tableModifier.modify(document);
    }
}