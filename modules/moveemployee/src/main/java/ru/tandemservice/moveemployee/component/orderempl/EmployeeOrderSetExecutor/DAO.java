/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.EmployeeOrderSetExecutor;

import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;

/**
 * @author dseleznev
 * Created on: 23.10.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder((AbstractEmployeeOrder) getNotNull(model.getOrderId()));
        model.setEmployeePostModel(new OrderExecutorSelectModel());
    }

    @Override
    public void update(Model model)
    {
        model.getOrder().setExecutor(OrderExecutorSelectModel.getExecutor(model.getEmployeePost()));
        update(model.getOrder());
    }
}