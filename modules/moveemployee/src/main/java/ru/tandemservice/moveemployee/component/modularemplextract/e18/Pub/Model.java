/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e18.Pub;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 15.09.2011
 */
public class Model extends ModularEmployeeExtractPubModel<TransferAnnualHolidayExtract>
{
    private String _periodHolidayStr;
    private String _holidayStr;
    private String _factHolidayStr;
    private Boolean _showHoliday;
    private Boolean _showFactHoliday;

    //Getters & Setters

    public Boolean getShowHoliday()
    {
        return _showHoliday;
    }

    public void setShowHoliday(Boolean showHoliday)
    {
        _showHoliday = showHoliday;
    }

    public Boolean getShowFactHoliday()
    {
        return _showFactHoliday;
    }

    public void setShowFactHoliday(Boolean showFactHoliday)
    {
        _showFactHoliday = showFactHoliday;
    }

    public String getPeriodHolidayStr()
    {
        return _periodHolidayStr;
    }

    public void setPeriodHolidayStr(String periodHolidayStr)
    {
        _periodHolidayStr = periodHolidayStr;
    }

    public String getHolidayStr()
    {
        return _holidayStr;
    }

    public void setHolidayStr(String holidayStr)
    {
        _holidayStr = holidayStr;
    }

    public String getFactHolidayStr()
    {
        return _factHolidayStr;
    }

    public void setFactHolidayStr(String factHolidayStr)
    {
        _factHolidayStr = factHolidayStr;
    }
}