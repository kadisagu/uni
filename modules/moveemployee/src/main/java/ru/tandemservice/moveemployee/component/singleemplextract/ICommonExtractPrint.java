/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.singleemplextract;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;

/**
 * Create by ashaburov
 * Date 23.01.12
 */
public interface ICommonExtractPrint
{
    void appendVisas(RtfDocument document, EmployeeSingleExtractOrder order);
}
