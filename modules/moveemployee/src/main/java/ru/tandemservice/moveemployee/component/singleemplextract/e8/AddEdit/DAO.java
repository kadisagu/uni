/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e8.AddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract;
import ru.tandemservice.moveemployee.entity.EncouragementToExtractRelation;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;

import java.util.ArrayList;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 30.09.2011
 */
public class DAO extends CommonSingleEmployeeExtractAddEditDAO<EmployeeEncouragementSExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.getExtract().getEntity() != null && model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        model.setEncouragementTypeModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EncouragementType.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", EncouragementType.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("b", EncouragementType.P_TITLE, OrderDirection.asc);

                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setFinancingSourceModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(FinancingSource.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", FinancingSource.P_TITLE, CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", FinancingSource.P_ID, o));

                return new MQListResultBuilder(builder);
            }
        });

        model.setFinancingSourceItemModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getFinancingSource() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", FinancingSourceItem.L_FINANCING_SOURCE, model.getExtract().getFinancingSource()));
                builder.add(MQExpression.like("b", FinancingSourceItem.P_TITLE, CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", FinancingSourceItem.P_ID, o));

                return new MQListResultBuilder(builder);
            }
        });

        if (model.isEditForm())
        {
            //если редактируем выписку, то достаем релейшены на выбранные Типы поощрений и "выбираем" их
            MQBuilder builder = new MQBuilder(EncouragementToExtractRelation.ENTITY_CLASS, "b", new String[]{EncouragementToExtractRelation.L_ENCOURAGEMENT_TYPE});
            builder.add(MQExpression.eq("b", EncouragementToExtractRelation.L_EMPLOYEE_ENCOURAGEMENT_S_EXTRACT, model.getExtract()));

            model.setEncouragementTypeList(builder.<EncouragementType>getResultList(getSession()));
        }
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());

        if (model.isEditForm())
        {
            MQBuilder builder = new MQBuilder(EncouragementToExtractRelation.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", EncouragementToExtractRelation.L_EMPLOYEE_ENCOURAGEMENT_S_EXTRACT, model.getExtract()));

            for (EncouragementToExtractRelation relation : builder.<EncouragementToExtractRelation>getResultList(getSession()))
                delete(relation);
        }

        for (EncouragementType encouragement : model.getEncouragementTypeList())
        {
            EncouragementToExtractRelation relation = new EncouragementToExtractRelation();
            relation.setEmployeeEncouragementSExtract(model.getExtract());
            relation.setEncouragementType(encouragement);

            save(relation);
        }
    }

    @Override
    protected EmployeeEncouragementSExtract createNewInstance()
    {
        return new EmployeeEncouragementSExtract();
    }
    
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }
}