/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e5;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.MainRow;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.Model;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.Row;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;
import ru.tandemservice.moveemployee.entity.HolidayInEmpHldyExtractRelation;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 14.01.2009
 */
public class EmployeeHolidayExtractPrint implements IPrintFormCreator<EmployeeHolidayExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeHolidayExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        
        modifier.put("employeeNumber", extract.getEntity().getEmployee().getEmployeeCode());

        Map<Row, Date> beginPeriodMap = new HashMap<>();
        Map<Row, Date> endPeriodMap = new HashMap<>();
        Map<Row, Date> startHolidayMap = new HashMap<>();
        Map<Row, Date> finishHolidayMap = new HashMap<>();
        Map<Row, Integer> durationHolidayMap = new HashMap<>();

        List<HolidayInEmpHldyExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getHolidayInEmpHldyExtractRelation(extract);

        //восстанавливаем список отпусков
        List<MainRow> mainRowList = new ArrayList<>();
        for (HolidayInEmpHldyExtractRelation relation : relationList)
        {
            MainRow mainRow = new MainRow(relation.getMainRowId(), null, null);
            if (!mainRowList.contains(mainRow))
            {
                List<Row> rowList = new ArrayList<>();
                Row row = new Row(relation.getRowId(), mainRow, relation.getRowType());

                beginPeriodMap.put(row, relation.getBeginPeriod());
                endPeriodMap.put(row, relation.getEndPeriod());
                startHolidayMap.put(row, relation.getStartHoliday());
                finishHolidayMap.put(row, relation.getFinishHoliday());
                durationHolidayMap.put(row, relation.getDurationHoliday());

                rowList.add(row);
                mainRow.setRowList(rowList);
                mainRow.setType(relation.getRowType());
                mainRowList.add(mainRow);
            }
            else
            {
                MainRow mainRow1 = mainRowList.get(mainRowList.indexOf(mainRow));
                mainRow1.setType(relation.getRowType());
                Row row = new Row(relation.getRowId(), mainRow1, relation.getRowType());

                beginPeriodMap.put(row, relation.getBeginPeriod());
                endPeriodMap.put(row, relation.getEndPeriod());
                startHolidayMap.put(row, relation.getStartHoliday());
                finishHolidayMap.put(row, relation.getFinishHoliday());
                durationHolidayMap.put(row, relation.getDurationHoliday());

                mainRow1.getRowList().add(row);
            }
        }

        String staffRate = extract.getEntity().getEmployee().getPerson().isMale() ? " работающему" : " работающей";
        Double rate = 0d;
        for (EmployeePostStaffRateItem staffRateItem : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity()))
            rate += staffRateItem.getStaffRate();
        if (rate != 1)
        {
            staffRate += " на " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(rate) + " ставки, ";
            modifier.put("staffRate", staffRate);
        }
        else
            modifier.put("staffRate", " ");

        modifier.put("holidayType", extract.getHolidayType().getTitle());

        String holidayStr = "";
        for (MainRow mainRow : mainRowList)
        {
            if (mainRow.getType().equals(Model.FULL_HOLIDAY))
            {
                holidayStr += "на " + NumberConvertingUtil.getCalendarDaysWithName(durationHolidayMap.get(mainRow.getRowList().get(0))) +
                    " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startHolidayMap.get(mainRow.getRowList().get(0))) +
                    " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(finishHolidayMap.get(mainRow.getRowList().get(0))) +
                    (beginPeriodMap.get(mainRow.getRowList().get(0)) != null ?
                            " за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(beginPeriodMap.get(mainRow.getRowList().get(0))) +
                            " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(endPeriodMap.get(mainRow.getRowList().get(0)))
                            : "");
            }
            else if (mainRow.getType().equals(Model.HOLIDAYS_OF_PERIOD))
            {
                String holiday = "";
                String period = "";
                for (Row row : mainRow.getRowList())
                {
                    holiday += "на " + NumberConvertingUtil.getCalendarDaysWithName(durationHolidayMap.get(row)) +
                        " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startHolidayMap.get(row)) +
                        " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(finishHolidayMap.get(row));

                    if (mainRow.getRowList().indexOf(row) + 1 == mainRow.getRowList().size() - 1)
                        holiday += " и ";
                    else if (mainRow.getRowList().indexOf(row) < mainRow.getRowList().size() - 1)
                        holiday += ", ";
//                    else
//                        holiday += ", ";

                    if (beginPeriodMap.get(row) != null)
                        period += " за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(beginPeriodMap.get(row)) +
                            " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(endPeriodMap.get(row));
                }

                holidayStr += holiday + period;
            }
            else if (mainRow.getType().equals(Model.PERIODS_OF_HOLIDAY))
            {
                String holiday = "";
                String period = "";
                Integer totalDur = 0;
                for (Row row : mainRow.getRowList())
                    totalDur += durationHolidayMap.get(row) != null ? durationHolidayMap.get(row) : 0;

                for (Row row : mainRow.getRowList())
                {
                    if (startHolidayMap.get(row) != null)
                        holiday += "на " + NumberConvertingUtil.getCalendarDaysWithName(totalDur) +
                                " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(startHolidayMap.get(row)) +
                                " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(finishHolidayMap.get(row)) + ", из них ";

                    period += NumberConvertingUtil.getCalendarDaysWithName(durationHolidayMap.get(row)) +
                        " за период с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(beginPeriodMap.get(row)) +
                        " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(endPeriodMap.get(row));

                    if (mainRow.getRowList().indexOf(row) + 1 == mainRow.getRowList().size() - 1)
                        period += " и ";
                    else if (mainRow.getRowList().indexOf(row) < mainRow.getRowList().size() - 1)
                        period += ", ";
//                    else
//                        period += ", ";
                }

                holidayStr += holiday + period;
            }


            if (mainRowList.indexOf(mainRow) == mainRowList.size() - 1)
                holidayStr += ".";
            else
                holidayStr += ", ";
        }

        modifier.put("holidayStr", holidayStr);

        modifier.put("conditions", extract.getOptionalCondition());

        injectCustomModifier(extract, modifier);

        modifier.modify(document);
        return document;
    }

    protected void injectCustomModifier(EmployeeHolidayExtract extract, RtfInjectModifier modifier)
    {

    }
}