package ru.tandemservice.moveemployee.entity.catalog;

import java.util.List;

import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.catalog.gen.EmployeeOrderReasonsGen;

public class EmployeeOrderReasons extends EmployeeOrderReasonsGen
{
    public static final String P_BASICS = "basics";
    public static final String P_DOCUMENT_TYPES = "documentTypes";

    public List<EmployeeOrderBasics> getBasics()
    {
        return MoveEmployeeDaoFacade.getMoveEmployeeDao().getReasonToBasicsList(this);
    }

    public List<EmployeeExtractType> getDocumentTypes()
    {
        return MoveEmployeeDaoFacade.getMoveEmployeeDao().getReasonToDocumentTypeList(this);
    }
}