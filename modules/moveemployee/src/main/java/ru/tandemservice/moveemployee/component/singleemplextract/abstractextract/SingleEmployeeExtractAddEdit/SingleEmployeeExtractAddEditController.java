/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public abstract class SingleEmployeeExtractAddEditController<T extends SingleEmployeeExtract, IDAO extends ISingleEmployeeExtractAddEditDAO<T, Model>, Model extends SingleEmployeeExtractAddEditModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public final void onChangeExtractType(IBusinessComponent component)
    {
        SingleEmployeeExtractAddEditModel<T> model = getModel(component);
        component.createDefaultChildRegion(new ComponentActivator(MoveEmployeeUtils.getExtractAddEditComponent(model.getExtractType()), new ParametersMap().add("employeePostId", model.getEmployeePost().getId()).add("extractTypeId", model.getExtractType().getId())));
    }

    public final synchronized void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(model, errors);
        if (errors.hasErrors()) return;

        getDao().update(model);

        if (model.isAddForm())
        {
            deactivate(component, 2);//назад на 2-е страницы
            activateInRoot(component, new PublisherActivator(model.getExtract()));
        }
        else
            deactivate(component);
    }
    
    public void onClickDeactivate(IBusinessComponent component)
    {
        if (getModel(component).isAddForm())
            deactivate(component, 2);
        else
            deactivate(component);
    }
}