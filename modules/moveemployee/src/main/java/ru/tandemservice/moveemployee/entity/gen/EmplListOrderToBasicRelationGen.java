package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь списочного приказа с основанием. В списочном приказе есть список оснований (приказы по кадрам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmplListOrderToBasicRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation";
    public static final String ENTITY_NAME = "emplListOrderToBasicRelation";
    public static final int VERSION_HASH = -1828572170;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_BASIC = "basic";
    public static final String P_COMMENT = "comment";

    private EmployeeListOrder _order;     // Списочный приказ
    private EmployeeOrderBasics _basic;     // Основание списочного приказа
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Списочный приказ.
     */
    public EmployeeListOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Списочный приказ.
     */
    public void setOrder(EmployeeListOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Основание списочного приказа.
     */
    public EmployeeOrderBasics getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание списочного приказа.
     */
    public void setBasic(EmployeeOrderBasics basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmplListOrderToBasicRelationGen)
        {
            setOrder(((EmplListOrderToBasicRelation)another).getOrder());
            setBasic(((EmplListOrderToBasicRelation)another).getBasic());
            setComment(((EmplListOrderToBasicRelation)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmplListOrderToBasicRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmplListOrderToBasicRelation.class;
        }

        public T newInstance()
        {
            return (T) new EmplListOrderToBasicRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "basic":
                    return obj.getBasic();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((EmployeeListOrder) value);
                    return;
                case "basic":
                    obj.setBasic((EmployeeOrderBasics) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "basic":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "basic":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return EmployeeListOrder.class;
                case "basic":
                    return EmployeeOrderBasics.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmplListOrderToBasicRelation> _dslPath = new Path<EmplListOrderToBasicRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmplListOrderToBasicRelation");
    }
            

    /**
     * @return Списочный приказ.
     * @see ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation#getOrder()
     */
    public static EmployeeListOrder.Path<EmployeeListOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Основание списочного приказа.
     * @see ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation#getBasic()
     */
    public static EmployeeOrderBasics.Path<EmployeeOrderBasics> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends EmplListOrderToBasicRelation> extends EntityPath<E>
    {
        private EmployeeListOrder.Path<EmployeeListOrder> _order;
        private EmployeeOrderBasics.Path<EmployeeOrderBasics> _basic;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Списочный приказ.
     * @see ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation#getOrder()
     */
        public EmployeeListOrder.Path<EmployeeListOrder> order()
        {
            if(_order == null )
                _order = new EmployeeListOrder.Path<EmployeeListOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Основание списочного приказа.
     * @see ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation#getBasic()
     */
        public EmployeeOrderBasics.Path<EmployeeOrderBasics> basic()
        {
            if(_basic == null )
                _basic = new EmployeeOrderBasics.Path<EmployeeOrderBasics>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.moveemployee.entity.EmplListOrderToBasicRelation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(EmplListOrderToBasicRelationGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return EmplListOrderToBasicRelation.class;
        }

        public String getEntityName()
        {
            return "emplListOrderToBasicRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
