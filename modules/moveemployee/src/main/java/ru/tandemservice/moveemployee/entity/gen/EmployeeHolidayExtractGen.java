package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Об отпуске
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeHolidayExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract";
    public static final String ENTITY_NAME = "employeeHolidayExtract";
    public static final int VERSION_HASH = 106858601;
    private static IEntityMeta ENTITY_META;

    public static final String L_HOLIDAY_TYPE = "holidayType";
    public static final String L_VACATION_SCHEDULE = "vacationSchedule";
    public static final String L_VACATION_SCHEDULE_ITEM = "vacationScheduleItem";
    public static final String P_DELETE_VACATION_SCHEDULE_ITEM = "deleteVacationScheduleItem";
    public static final String P_OLD_SCHEDULE_ITEM_DAYS_AMOUNT = "oldScheduleItemDaysAmount";
    public static final String P_OLD_SCHEDULE_ITEM_PLAN_DATE = "oldScheduleItemPlanDate";
    public static final String P_OLD_SCHEDULE_ITEM_FACT_DATE = "oldScheduleItemFactDate";
    public static final String P_OLD_SCHEDULE_ITEM_POSTPONE_DATE = "oldScheduleItemPostponeDate";
    public static final String P_OLD_SCHEDULE_ITEM_POSTPONE_BASIC = "oldScheduleItemPostponeBasic";
    public static final String P_OLD_SCHEDULE_ITEM_COMMENT = "oldScheduleItemComment";
    public static final String L_OLD_EMPLOYEE_POST_STATUS = "oldEmployeePostStatus";
    public static final String L_EMPLOYEE_POST_STATUS = "employeePostStatus";
    public static final String P_OPTIONAL_CONDITION = "optionalCondition";
    public static final String P_TOTAL_DURATION = "totalDuration";
    public static final String P_SECOND_JOB = "secondJob";

    private HolidayType _holidayType;     // Вид отпуска
    private VacationSchedule _vacationSchedule;     // График отпусков
    private VacationScheduleItem _vacationScheduleItem;     // Строка графика отпусков
    private Boolean _deleteVacationScheduleItem;     // Старая строка в Графике отпусков сотрудника удалена
    private Integer _oldScheduleItemDaysAmount;     // Количество календарных дней отпуска в строке Графика отпусков сотрудника до проведения приказа
    private Date _oldScheduleItemPlanDate;     // Планируемая дата отпуска в строке Графика отпусков сотрудника до проведения приказа
    private Date _oldScheduleItemFactDate;     // Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа
    private Date _oldScheduleItemPostponeDate;     // Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа
    private String _oldScheduleItemPostponeBasic;     // Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа
    private String _oldScheduleItemComment;     // Примечание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа
    private EmployeePostStatus _oldEmployeePostStatus;     // Статус на должности до ухода в отпуск
    private EmployeePostStatus _employeePostStatus;     // Статус на должности после ухода в отпуск
    private String _optionalCondition;     // Произвольная строка условий отпуска
    private Integer _totalDuration;     // Итоговая длительность
    private Boolean _secondJob;     // Предоставить отпуск по работе по совместительству в тот же срок

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Вид отпуска.
     */
    public HolidayType getHolidayType()
    {
        return _holidayType;
    }

    /**
     * @param holidayType Вид отпуска.
     */
    public void setHolidayType(HolidayType holidayType)
    {
        dirty(_holidayType, holidayType);
        _holidayType = holidayType;
    }

    /**
     * @return График отпусков.
     */
    public VacationSchedule getVacationSchedule()
    {
        return _vacationSchedule;
    }

    /**
     * @param vacationSchedule График отпусков.
     */
    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        dirty(_vacationSchedule, vacationSchedule);
        _vacationSchedule = vacationSchedule;
    }

    /**
     * @return Строка графика отпусков.
     */
    public VacationScheduleItem getVacationScheduleItem()
    {
        return _vacationScheduleItem;
    }

    /**
     * @param vacationScheduleItem Строка графика отпусков.
     */
    public void setVacationScheduleItem(VacationScheduleItem vacationScheduleItem)
    {
        dirty(_vacationScheduleItem, vacationScheduleItem);
        _vacationScheduleItem = vacationScheduleItem;
    }

    /**
     * @return Старая строка в Графике отпусков сотрудника удалена.
     */
    public Boolean getDeleteVacationScheduleItem()
    {
        return _deleteVacationScheduleItem;
    }

    /**
     * @param deleteVacationScheduleItem Старая строка в Графике отпусков сотрудника удалена.
     */
    public void setDeleteVacationScheduleItem(Boolean deleteVacationScheduleItem)
    {
        dirty(_deleteVacationScheduleItem, deleteVacationScheduleItem);
        _deleteVacationScheduleItem = deleteVacationScheduleItem;
    }

    /**
     * @return Количество календарных дней отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public Integer getOldScheduleItemDaysAmount()
    {
        return _oldScheduleItemDaysAmount;
    }

    /**
     * @param oldScheduleItemDaysAmount Количество календарных дней отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemDaysAmount(Integer oldScheduleItemDaysAmount)
    {
        dirty(_oldScheduleItemDaysAmount, oldScheduleItemDaysAmount);
        _oldScheduleItemDaysAmount = oldScheduleItemDaysAmount;
    }

    /**
     * @return Планируемая дата отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public Date getOldScheduleItemPlanDate()
    {
        return _oldScheduleItemPlanDate;
    }

    /**
     * @param oldScheduleItemPlanDate Планируемая дата отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemPlanDate(Date oldScheduleItemPlanDate)
    {
        dirty(_oldScheduleItemPlanDate, oldScheduleItemPlanDate);
        _oldScheduleItemPlanDate = oldScheduleItemPlanDate;
    }

    /**
     * @return Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа.
     */
    public Date getOldScheduleItemFactDate()
    {
        return _oldScheduleItemFactDate;
    }

    /**
     * @param oldScheduleItemFactDate Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemFactDate(Date oldScheduleItemFactDate)
    {
        dirty(_oldScheduleItemFactDate, oldScheduleItemFactDate);
        _oldScheduleItemFactDate = oldScheduleItemFactDate;
    }

    /**
     * @return Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public Date getOldScheduleItemPostponeDate()
    {
        return _oldScheduleItemPostponeDate;
    }

    /**
     * @param oldScheduleItemPostponeDate Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemPostponeDate(Date oldScheduleItemPostponeDate)
    {
        dirty(_oldScheduleItemPostponeDate, oldScheduleItemPostponeDate);
        _oldScheduleItemPostponeDate = oldScheduleItemPostponeDate;
    }

    /**
     * @return Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    @Length(max=255)
    public String getOldScheduleItemPostponeBasic()
    {
        return _oldScheduleItemPostponeBasic;
    }

    /**
     * @param oldScheduleItemPostponeBasic Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemPostponeBasic(String oldScheduleItemPostponeBasic)
    {
        dirty(_oldScheduleItemPostponeBasic, oldScheduleItemPostponeBasic);
        _oldScheduleItemPostponeBasic = oldScheduleItemPostponeBasic;
    }

    /**
     * @return Примечание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    @Length(max=255)
    public String getOldScheduleItemComment()
    {
        return _oldScheduleItemComment;
    }

    /**
     * @param oldScheduleItemComment Примечание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemComment(String oldScheduleItemComment)
    {
        dirty(_oldScheduleItemComment, oldScheduleItemComment);
        _oldScheduleItemComment = oldScheduleItemComment;
    }

    /**
     * @return Статус на должности до ухода в отпуск. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getOldEmployeePostStatus()
    {
        return _oldEmployeePostStatus;
    }

    /**
     * @param oldEmployeePostStatus Статус на должности до ухода в отпуск. Свойство не может быть null.
     */
    public void setOldEmployeePostStatus(EmployeePostStatus oldEmployeePostStatus)
    {
        dirty(_oldEmployeePostStatus, oldEmployeePostStatus);
        _oldEmployeePostStatus = oldEmployeePostStatus;
    }

    /**
     * @return Статус на должности после ухода в отпуск. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getEmployeePostStatus()
    {
        return _employeePostStatus;
    }

    /**
     * @param employeePostStatus Статус на должности после ухода в отпуск. Свойство не может быть null.
     */
    public void setEmployeePostStatus(EmployeePostStatus employeePostStatus)
    {
        dirty(_employeePostStatus, employeePostStatus);
        _employeePostStatus = employeePostStatus;
    }

    /**
     * @return Произвольная строка условий отпуска.
     */
    public String getOptionalCondition()
    {
        return _optionalCondition;
    }

    /**
     * @param optionalCondition Произвольная строка условий отпуска.
     */
    public void setOptionalCondition(String optionalCondition)
    {
        dirty(_optionalCondition, optionalCondition);
        _optionalCondition = optionalCondition;
    }

    /**
     * @return Итоговая длительность.
     */
    public Integer getTotalDuration()
    {
        return _totalDuration;
    }

    /**
     * @param totalDuration Итоговая длительность.
     */
    public void setTotalDuration(Integer totalDuration)
    {
        dirty(_totalDuration, totalDuration);
        _totalDuration = totalDuration;
    }

    /**
     * @return Предоставить отпуск по работе по совместительству в тот же срок.
     */
    public Boolean getSecondJob()
    {
        return _secondJob;
    }

    /**
     * @param secondJob Предоставить отпуск по работе по совместительству в тот же срок.
     */
    public void setSecondJob(Boolean secondJob)
    {
        dirty(_secondJob, secondJob);
        _secondJob = secondJob;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeHolidayExtractGen)
        {
            setHolidayType(((EmployeeHolidayExtract)another).getHolidayType());
            setVacationSchedule(((EmployeeHolidayExtract)another).getVacationSchedule());
            setVacationScheduleItem(((EmployeeHolidayExtract)another).getVacationScheduleItem());
            setDeleteVacationScheduleItem(((EmployeeHolidayExtract)another).getDeleteVacationScheduleItem());
            setOldScheduleItemDaysAmount(((EmployeeHolidayExtract)another).getOldScheduleItemDaysAmount());
            setOldScheduleItemPlanDate(((EmployeeHolidayExtract)another).getOldScheduleItemPlanDate());
            setOldScheduleItemFactDate(((EmployeeHolidayExtract)another).getOldScheduleItemFactDate());
            setOldScheduleItemPostponeDate(((EmployeeHolidayExtract)another).getOldScheduleItemPostponeDate());
            setOldScheduleItemPostponeBasic(((EmployeeHolidayExtract)another).getOldScheduleItemPostponeBasic());
            setOldScheduleItemComment(((EmployeeHolidayExtract)another).getOldScheduleItemComment());
            setOldEmployeePostStatus(((EmployeeHolidayExtract)another).getOldEmployeePostStatus());
            setEmployeePostStatus(((EmployeeHolidayExtract)another).getEmployeePostStatus());
            setOptionalCondition(((EmployeeHolidayExtract)another).getOptionalCondition());
            setTotalDuration(((EmployeeHolidayExtract)another).getTotalDuration());
            setSecondJob(((EmployeeHolidayExtract)another).getSecondJob());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeHolidayExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeHolidayExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeHolidayExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "holidayType":
                    return obj.getHolidayType();
                case "vacationSchedule":
                    return obj.getVacationSchedule();
                case "vacationScheduleItem":
                    return obj.getVacationScheduleItem();
                case "deleteVacationScheduleItem":
                    return obj.getDeleteVacationScheduleItem();
                case "oldScheduleItemDaysAmount":
                    return obj.getOldScheduleItemDaysAmount();
                case "oldScheduleItemPlanDate":
                    return obj.getOldScheduleItemPlanDate();
                case "oldScheduleItemFactDate":
                    return obj.getOldScheduleItemFactDate();
                case "oldScheduleItemPostponeDate":
                    return obj.getOldScheduleItemPostponeDate();
                case "oldScheduleItemPostponeBasic":
                    return obj.getOldScheduleItemPostponeBasic();
                case "oldScheduleItemComment":
                    return obj.getOldScheduleItemComment();
                case "oldEmployeePostStatus":
                    return obj.getOldEmployeePostStatus();
                case "employeePostStatus":
                    return obj.getEmployeePostStatus();
                case "optionalCondition":
                    return obj.getOptionalCondition();
                case "totalDuration":
                    return obj.getTotalDuration();
                case "secondJob":
                    return obj.getSecondJob();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "holidayType":
                    obj.setHolidayType((HolidayType) value);
                    return;
                case "vacationSchedule":
                    obj.setVacationSchedule((VacationSchedule) value);
                    return;
                case "vacationScheduleItem":
                    obj.setVacationScheduleItem((VacationScheduleItem) value);
                    return;
                case "deleteVacationScheduleItem":
                    obj.setDeleteVacationScheduleItem((Boolean) value);
                    return;
                case "oldScheduleItemDaysAmount":
                    obj.setOldScheduleItemDaysAmount((Integer) value);
                    return;
                case "oldScheduleItemPlanDate":
                    obj.setOldScheduleItemPlanDate((Date) value);
                    return;
                case "oldScheduleItemFactDate":
                    obj.setOldScheduleItemFactDate((Date) value);
                    return;
                case "oldScheduleItemPostponeDate":
                    obj.setOldScheduleItemPostponeDate((Date) value);
                    return;
                case "oldScheduleItemPostponeBasic":
                    obj.setOldScheduleItemPostponeBasic((String) value);
                    return;
                case "oldScheduleItemComment":
                    obj.setOldScheduleItemComment((String) value);
                    return;
                case "oldEmployeePostStatus":
                    obj.setOldEmployeePostStatus((EmployeePostStatus) value);
                    return;
                case "employeePostStatus":
                    obj.setEmployeePostStatus((EmployeePostStatus) value);
                    return;
                case "optionalCondition":
                    obj.setOptionalCondition((String) value);
                    return;
                case "totalDuration":
                    obj.setTotalDuration((Integer) value);
                    return;
                case "secondJob":
                    obj.setSecondJob((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "holidayType":
                        return true;
                case "vacationSchedule":
                        return true;
                case "vacationScheduleItem":
                        return true;
                case "deleteVacationScheduleItem":
                        return true;
                case "oldScheduleItemDaysAmount":
                        return true;
                case "oldScheduleItemPlanDate":
                        return true;
                case "oldScheduleItemFactDate":
                        return true;
                case "oldScheduleItemPostponeDate":
                        return true;
                case "oldScheduleItemPostponeBasic":
                        return true;
                case "oldScheduleItemComment":
                        return true;
                case "oldEmployeePostStatus":
                        return true;
                case "employeePostStatus":
                        return true;
                case "optionalCondition":
                        return true;
                case "totalDuration":
                        return true;
                case "secondJob":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "holidayType":
                    return true;
                case "vacationSchedule":
                    return true;
                case "vacationScheduleItem":
                    return true;
                case "deleteVacationScheduleItem":
                    return true;
                case "oldScheduleItemDaysAmount":
                    return true;
                case "oldScheduleItemPlanDate":
                    return true;
                case "oldScheduleItemFactDate":
                    return true;
                case "oldScheduleItemPostponeDate":
                    return true;
                case "oldScheduleItemPostponeBasic":
                    return true;
                case "oldScheduleItemComment":
                    return true;
                case "oldEmployeePostStatus":
                    return true;
                case "employeePostStatus":
                    return true;
                case "optionalCondition":
                    return true;
                case "totalDuration":
                    return true;
                case "secondJob":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "holidayType":
                    return HolidayType.class;
                case "vacationSchedule":
                    return VacationSchedule.class;
                case "vacationScheduleItem":
                    return VacationScheduleItem.class;
                case "deleteVacationScheduleItem":
                    return Boolean.class;
                case "oldScheduleItemDaysAmount":
                    return Integer.class;
                case "oldScheduleItemPlanDate":
                    return Date.class;
                case "oldScheduleItemFactDate":
                    return Date.class;
                case "oldScheduleItemPostponeDate":
                    return Date.class;
                case "oldScheduleItemPostponeBasic":
                    return String.class;
                case "oldScheduleItemComment":
                    return String.class;
                case "oldEmployeePostStatus":
                    return EmployeePostStatus.class;
                case "employeePostStatus":
                    return EmployeePostStatus.class;
                case "optionalCondition":
                    return String.class;
                case "totalDuration":
                    return Integer.class;
                case "secondJob":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeHolidayExtract> _dslPath = new Path<EmployeeHolidayExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeHolidayExtract");
    }
            

    /**
     * @return Вид отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getHolidayType()
     */
    public static HolidayType.Path<HolidayType> holidayType()
    {
        return _dslPath.holidayType();
    }

    /**
     * @return График отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getVacationSchedule()
     */
    public static VacationSchedule.Path<VacationSchedule> vacationSchedule()
    {
        return _dslPath.vacationSchedule();
    }

    /**
     * @return Строка графика отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getVacationScheduleItem()
     */
    public static VacationScheduleItem.Path<VacationScheduleItem> vacationScheduleItem()
    {
        return _dslPath.vacationScheduleItem();
    }

    /**
     * @return Старая строка в Графике отпусков сотрудника удалена.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getDeleteVacationScheduleItem()
     */
    public static PropertyPath<Boolean> deleteVacationScheduleItem()
    {
        return _dslPath.deleteVacationScheduleItem();
    }

    /**
     * @return Количество календарных дней отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemDaysAmount()
     */
    public static PropertyPath<Integer> oldScheduleItemDaysAmount()
    {
        return _dslPath.oldScheduleItemDaysAmount();
    }

    /**
     * @return Планируемая дата отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemPlanDate()
     */
    public static PropertyPath<Date> oldScheduleItemPlanDate()
    {
        return _dslPath.oldScheduleItemPlanDate();
    }

    /**
     * @return Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemFactDate()
     */
    public static PropertyPath<Date> oldScheduleItemFactDate()
    {
        return _dslPath.oldScheduleItemFactDate();
    }

    /**
     * @return Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemPostponeDate()
     */
    public static PropertyPath<Date> oldScheduleItemPostponeDate()
    {
        return _dslPath.oldScheduleItemPostponeDate();
    }

    /**
     * @return Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemPostponeBasic()
     */
    public static PropertyPath<String> oldScheduleItemPostponeBasic()
    {
        return _dslPath.oldScheduleItemPostponeBasic();
    }

    /**
     * @return Примечание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemComment()
     */
    public static PropertyPath<String> oldScheduleItemComment()
    {
        return _dslPath.oldScheduleItemComment();
    }

    /**
     * @return Статус на должности до ухода в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
    {
        return _dslPath.oldEmployeePostStatus();
    }

    /**
     * @return Статус на должности после ухода в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> employeePostStatus()
    {
        return _dslPath.employeePostStatus();
    }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOptionalCondition()
     */
    public static PropertyPath<String> optionalCondition()
    {
        return _dslPath.optionalCondition();
    }

    /**
     * @return Итоговая длительность.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getTotalDuration()
     */
    public static PropertyPath<Integer> totalDuration()
    {
        return _dslPath.totalDuration();
    }

    /**
     * @return Предоставить отпуск по работе по совместительству в тот же срок.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getSecondJob()
     */
    public static PropertyPath<Boolean> secondJob()
    {
        return _dslPath.secondJob();
    }

    public static class Path<E extends EmployeeHolidayExtract> extends ModularEmployeeExtract.Path<E>
    {
        private HolidayType.Path<HolidayType> _holidayType;
        private VacationSchedule.Path<VacationSchedule> _vacationSchedule;
        private VacationScheduleItem.Path<VacationScheduleItem> _vacationScheduleItem;
        private PropertyPath<Boolean> _deleteVacationScheduleItem;
        private PropertyPath<Integer> _oldScheduleItemDaysAmount;
        private PropertyPath<Date> _oldScheduleItemPlanDate;
        private PropertyPath<Date> _oldScheduleItemFactDate;
        private PropertyPath<Date> _oldScheduleItemPostponeDate;
        private PropertyPath<String> _oldScheduleItemPostponeBasic;
        private PropertyPath<String> _oldScheduleItemComment;
        private EmployeePostStatus.Path<EmployeePostStatus> _oldEmployeePostStatus;
        private EmployeePostStatus.Path<EmployeePostStatus> _employeePostStatus;
        private PropertyPath<String> _optionalCondition;
        private PropertyPath<Integer> _totalDuration;
        private PropertyPath<Boolean> _secondJob;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Вид отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getHolidayType()
     */
        public HolidayType.Path<HolidayType> holidayType()
        {
            if(_holidayType == null )
                _holidayType = new HolidayType.Path<HolidayType>(L_HOLIDAY_TYPE, this);
            return _holidayType;
        }

    /**
     * @return График отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getVacationSchedule()
     */
        public VacationSchedule.Path<VacationSchedule> vacationSchedule()
        {
            if(_vacationSchedule == null )
                _vacationSchedule = new VacationSchedule.Path<VacationSchedule>(L_VACATION_SCHEDULE, this);
            return _vacationSchedule;
        }

    /**
     * @return Строка графика отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getVacationScheduleItem()
     */
        public VacationScheduleItem.Path<VacationScheduleItem> vacationScheduleItem()
        {
            if(_vacationScheduleItem == null )
                _vacationScheduleItem = new VacationScheduleItem.Path<VacationScheduleItem>(L_VACATION_SCHEDULE_ITEM, this);
            return _vacationScheduleItem;
        }

    /**
     * @return Старая строка в Графике отпусков сотрудника удалена.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getDeleteVacationScheduleItem()
     */
        public PropertyPath<Boolean> deleteVacationScheduleItem()
        {
            if(_deleteVacationScheduleItem == null )
                _deleteVacationScheduleItem = new PropertyPath<Boolean>(EmployeeHolidayExtractGen.P_DELETE_VACATION_SCHEDULE_ITEM, this);
            return _deleteVacationScheduleItem;
        }

    /**
     * @return Количество календарных дней отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemDaysAmount()
     */
        public PropertyPath<Integer> oldScheduleItemDaysAmount()
        {
            if(_oldScheduleItemDaysAmount == null )
                _oldScheduleItemDaysAmount = new PropertyPath<Integer>(EmployeeHolidayExtractGen.P_OLD_SCHEDULE_ITEM_DAYS_AMOUNT, this);
            return _oldScheduleItemDaysAmount;
        }

    /**
     * @return Планируемая дата отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemPlanDate()
     */
        public PropertyPath<Date> oldScheduleItemPlanDate()
        {
            if(_oldScheduleItemPlanDate == null )
                _oldScheduleItemPlanDate = new PropertyPath<Date>(EmployeeHolidayExtractGen.P_OLD_SCHEDULE_ITEM_PLAN_DATE, this);
            return _oldScheduleItemPlanDate;
        }

    /**
     * @return Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemFactDate()
     */
        public PropertyPath<Date> oldScheduleItemFactDate()
        {
            if(_oldScheduleItemFactDate == null )
                _oldScheduleItemFactDate = new PropertyPath<Date>(EmployeeHolidayExtractGen.P_OLD_SCHEDULE_ITEM_FACT_DATE, this);
            return _oldScheduleItemFactDate;
        }

    /**
     * @return Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemPostponeDate()
     */
        public PropertyPath<Date> oldScheduleItemPostponeDate()
        {
            if(_oldScheduleItemPostponeDate == null )
                _oldScheduleItemPostponeDate = new PropertyPath<Date>(EmployeeHolidayExtractGen.P_OLD_SCHEDULE_ITEM_POSTPONE_DATE, this);
            return _oldScheduleItemPostponeDate;
        }

    /**
     * @return Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemPostponeBasic()
     */
        public PropertyPath<String> oldScheduleItemPostponeBasic()
        {
            if(_oldScheduleItemPostponeBasic == null )
                _oldScheduleItemPostponeBasic = new PropertyPath<String>(EmployeeHolidayExtractGen.P_OLD_SCHEDULE_ITEM_POSTPONE_BASIC, this);
            return _oldScheduleItemPostponeBasic;
        }

    /**
     * @return Примечание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldScheduleItemComment()
     */
        public PropertyPath<String> oldScheduleItemComment()
        {
            if(_oldScheduleItemComment == null )
                _oldScheduleItemComment = new PropertyPath<String>(EmployeeHolidayExtractGen.P_OLD_SCHEDULE_ITEM_COMMENT, this);
            return _oldScheduleItemComment;
        }

    /**
     * @return Статус на должности до ухода в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOldEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
        {
            if(_oldEmployeePostStatus == null )
                _oldEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_OLD_EMPLOYEE_POST_STATUS, this);
            return _oldEmployeePostStatus;
        }

    /**
     * @return Статус на должности после ухода в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> employeePostStatus()
        {
            if(_employeePostStatus == null )
                _employeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_EMPLOYEE_POST_STATUS, this);
            return _employeePostStatus;
        }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getOptionalCondition()
     */
        public PropertyPath<String> optionalCondition()
        {
            if(_optionalCondition == null )
                _optionalCondition = new PropertyPath<String>(EmployeeHolidayExtractGen.P_OPTIONAL_CONDITION, this);
            return _optionalCondition;
        }

    /**
     * @return Итоговая длительность.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getTotalDuration()
     */
        public PropertyPath<Integer> totalDuration()
        {
            if(_totalDuration == null )
                _totalDuration = new PropertyPath<Integer>(EmployeeHolidayExtractGen.P_TOTAL_DURATION, this);
            return _totalDuration;
        }

    /**
     * @return Предоставить отпуск по работе по совместительству в тот же срок.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract#getSecondJob()
     */
        public PropertyPath<Boolean> secondJob()
        {
            if(_secondJob == null )
                _secondJob = new PropertyPath<Boolean>(EmployeeHolidayExtractGen.P_SECOND_JOB, this);
            return _secondJob;
        }

        public Class getEntityClass()
        {
            return EmployeeHolidayExtract.class;
        }

        public String getEntityName()
        {
            return "employeeHolidayExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
