/* $Id$ */
package ru.tandemservice.moveemployee.component.commons;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;

import java.util.Date;

/**
 * @author Ekaterina Zvereva
 * @since 13.09.2016
 */

/**
 * Интерфейс для работы с выписками, которые создают либо изменяют Трудовой договор и дополнительной соглашение к нему
 */
public interface IContractAndAssigmentExtract
{
    EmployeePost getEntity();

    Date getContractBeginDate();

    Date getContractEndDate();

    LabourContractType getContractType();

    String getContractNumber();

    Date getContractDate();

    void setCreateEmployeeContract(EmployeeLabourContract labourContract);

    Date getContractAddAgreementDate();

    String getContractAddAgreementNumber();

    void setCreateCollateralAgreement(ContractCollateralAgreement agreement);
}
