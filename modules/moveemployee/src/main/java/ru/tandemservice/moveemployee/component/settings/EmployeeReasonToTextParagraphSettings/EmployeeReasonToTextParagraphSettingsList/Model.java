/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeReasonToTextParagraphSettings.EmployeeReasonToTextParagraphSettingsList;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * Create by: ashaburov
 * Date: 18.04.11
 */
public class Model
{
    public static String P_TEXT_PARAGRAPH = "textParagraph";

    private DynamicListDataSource _dataSource;


    //Getters & Setters

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }
}
