/* $Id$ */
package ru.tandemservice.moveemployee.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 06.03.2015
 */
public class MS_moveemployee_2x7x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.16"),
                new ScriptDependency("org.tandemframework.shared", "1.7.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Map<String, Long> orderStateCode2IdMap = MigrationUtils.getCatalogCode2IdMap(tool, "orderstates_t");
        Map<String, Long> extractStateCode2IdMap = MigrationUtils.getCatalogCode2IdMap(tool, "extractstates_t");

        Long orderStateFinishedId = orderStateCode2IdMap.get("5");
        Long extractFinishedStateId = extractStateCode2IdMap.get("6");
        Long extractRejectedStateId = extractStateCode2IdMap.get("4");
        Long extractInOrderStateId = extractStateCode2IdMap.get("5");

        // Необходимо исправить состояния выписок, которые по какой-то причине не соответствуют их положению в приказах

        // 1. Если выписка в проведенном приказе, её состояние может быть только "Проведено" и "Отклонено".
        // Если состояние какое-то другое, выставляем "Проведено".
        String sql = "update abstractemployeeextract_t set state_id=? where id in (" +
                "select e.id from abstractemployeeextract_t e " +
                "join abstractemployeeparagraph_t p on e.paragraph_id=p.id " +
                "join abstractemployeeorder_t o on p.order_id=o.id " +
                "where o.state_id=? and not (e.state_id=? or e.state_id=?)" +
                ")";
        tool.executeUpdate(sql, extractFinishedStateId,
                           orderStateFinishedId, extractFinishedStateId, extractRejectedStateId);

        // 2. Если приказ не проведен, то все выписки в нем должны быть в состоянии "в приказах".
        sql = "update abstractemployeeextract_t set state_id=? where id in (" +
                "select e.id from abstractemployeeextract_t e " +
                "join abstractemployeeparagraph_t p on e.paragraph_id=p.id " +
                "join abstractemployeeorder_t o on p.order_id=o.id " +
                "where o.state_id<>? and e.state_id<>?" +
                ")";
        tool.executeUpdate(sql, extractInOrderStateId,
                           orderStateFinishedId, extractInOrderStateId);
    }
}