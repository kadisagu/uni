/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.sec.entity.Admin;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.utils.MoveEmployeeUtils;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

/**
 * @author dseleznev
 *         Created on: 27.05.2009
 */
public abstract class SingleEmployeeExtractPubController<T extends SingleEmployeeExtract, IDAO extends ISingleEmployeeExtractPubDAO<T, Model>, Model extends SingleEmployeeExtractPubModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setDebugMode(Boolean.parseBoolean(ApplicationRuntime.getProperty("uni.god.mode")) && null == model.getExtract().getParagraph());

        model.setAttributesPage(getClass().getPackage().getName() + ".Attributes");

        AbstractEmployeeOrder order = model.getExtract().getParagraph().getOrder();

        model.setOrderCanBeEdited(CoreServices.securityService().check(order, component.getUserContext().getPrincipalContext(), "editCommitDate_employeeSingleOrder") || CoreServices.securityService().check(order, component.getUserContext().getPrincipalContext(), "editOrderNumber_employeeSingleOrder"));
    }

    public void onClickSetExecutor(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.STUDENT_ORDER_SET_EXECUTOR, new ParametersMap()
                .add("orderId", getModel(component).getExtract().getParagraph().getOrder().getId())
        ));
    }

    public void onClickCommit(IBusinessComponent component)
    {
        try
        {
            getDao().doCommit(getModel(component));
        }
        catch (RuntimeException e)
        {
            component.getDesktop().setRefreshScheduled(true);
            throw e;
        }
    }

    public void onClickRollback(IBusinessComponent component)
    {
        getDao().doRollback(getModel(component));
    }

    public void onClickReject(IBusinessComponent component)
    {
        getDao().doReject(getModel(component));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        Long extractId = getModel(component).getExtractId();
        component.createChildRegion("extractTabPanel", new ComponentActivator(MoveEmployeeUtils.getSingleExtractAddEditComponent(extractId), new ParametersMap().add("extractId", extractId)));
    }

    public void onClickEditOrder(IBusinessComponent component)
    {
        Long orderId = getModel(component).getExtract().getParagraph().getOrder().getId();
        component.createChildRegion("extractTabPanel", new ComponentActivator(IMoveEmployeeComponents.EMPLOYEE_SINGLE_ORDER_EDIT, new ParametersMap().add("orderId", orderId)));
    }

    public void onClickSendToCoordination(IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable) && !(principalContext instanceof Admin && Debug.isEnabled()))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        if (principalContext instanceof Admin)
            getDao().doSendToCoordination(getModel(component), null);
        else
            getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    public void onClickSendToFormative(IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        // TODO: conditions for deletion ablility must be implemented
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(getModel(component).getExtract().getParagraph().getOrder());
        deactivate(component);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.MODULAR_EMPLOYEE_EXTRACT_PRINT, new ParametersMap().add("extractId", getModel(component).getExtractId())));
    }
}