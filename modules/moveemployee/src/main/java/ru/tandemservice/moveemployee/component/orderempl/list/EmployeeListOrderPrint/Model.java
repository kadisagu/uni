/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.list.EmployeeListOrderPrint;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.moveemployee.entity.EmployeeListOrder;

/**
 * @author dseleznev
 * Created on: 29.10.2009
 */
@Input({
        @Bind(key = "orderId", binding = "order.id")
})
public class Model
{
    private EmployeeListOrder _order = new EmployeeListOrder();
    private byte[] _data;

    public EmployeeListOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeListOrder order)
    {
        _order = order;
    }

    public byte[] getData()
    {
        return _data;
    }

    public void setData(byte[] data)
    {
        _data = data;
    }
}