/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderAddExtracts;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.moveemployee.dao.ModularOrder.IEmployeeModularOrderParagraphsSortDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularParagraph;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.Date;

/**
 * @author dseleznev
 *         Created on: 11.11.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("e");
    
    static
    {
        _orderSettings.setOrders(ModularEmployeeExtract.EMPLOYEE_FIO_KEY, new OrderDescription("idCard", IdentityCard.P_LAST_NAME), new OrderDescription("idCard", IdentityCard.P_FIRST_NAME), new OrderDescription("idCard", IdentityCard.P_MIDDLE_NAME));
    }
    
    @Override
    public void prepare(Model model)
    {
        model.setOrder(get(EmployeeModularOrder.class, model.getOrder().getId()));
        model.setExtractTypeList(HierarchyUtil.listHierarchyNodesWithParents(MoveEmployeeDaoFacade.getMoveEmployeeDao().getModularExtractTypeList(), CommonCatalogUtil.CATALOG_CODE_COMPARATOR, false));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        Date extractCreationDate = model.getSettings().get("extractCreateDate");
        String employeeLastName = model.getSettings().get("lastName");
        EmployeeExtractType extractType = model.getSettings().get("extractType");

        MQBuilder builder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "e");
        builder.addJoinFetch("e", ModularEmployeeExtract.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD, "idCard");
        builder.add(MQExpression.eq("e", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED));
        

        if (null != extractCreationDate)
            builder.add(UniMQExpression.eqDate("e", IAbstractExtract.P_CREATE_DATE, extractCreationDate));
        if (null != employeeLastName)
            builder.add(MQExpression.like("e", ModularEmployeeExtract.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, "%" + employeeLastName));
        if (null != extractType)
            builder.add(MQExpression.eq("e", IAbstractExtract.L_TYPE, extractType));

        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        getSession().refresh(model.getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");

        int paragraphNumber = model.getOrder().getParagraphCount() + 1;
        for (IEntity entity : ((CheckboxColumn) model.getDataSource().getColumn("checkbox")).getSelectedObjects())
        {
            IAbstractExtract extract = (IAbstractExtract) entity;
            if (extract.getParagraph() != null)
                throw new ApplicationException(extract.getTitle() + " уже добавлена в приказ №" + extract.getParagraph().getOrder().getNumber()+".");

            if (!UnimoveDefines.CATALOG_EXTRACT_STATE_ACCEPTED.equals(extract.getState().getCode()))
                throw new ApplicationException(extract.getTitle() + " не согласован.");

            if (extract.isCommitted())
                throw new ApplicationException("Выписка " + extract.getTitle() + " уже проведена.");

            // Create paragraph
            EmployeeModularParagraph paragraph = new EmployeeModularParagraph();
            paragraph.setOrder(model.getOrder());
            paragraph.setNumber(paragraphNumber++);
            save(paragraph);

            // Update extract
            extract.setNumber(1);
            extract.setParagraph(paragraph);
            extract.setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
            update(extract);
        }

        IEmployeeModularOrderParagraphsSortDAO sorter = (IEmployeeModularOrderParagraphsSortDAO) ApplicationRuntime.getBean(model.getOrder().getParagraphsSortRule().getDaoName());
        sorter.sortExtracts(model.getOrder());
    }
}