/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import java.util.List;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;

/**
 * @author dseleznev
 * Created on: 21.01.2009
 */
public interface ICommonEmployeeExtractAddEditModel<T extends AbstractEmployeeExtract>
{
    T getExtract();
    
    void setExtract(T extract);

    IMultiSelectModel getBasicListModel();

    void setBasicListModel(IMultiSelectModel basicListModel);

    List<EmployeeOrderBasics> getSelectedBasicList();

    void setSelectedBasicList(List<EmployeeOrderBasics> selectedBasicList);

    boolean isAddForm();
}