/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e1.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.EmployeeAddExtract;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 14.11.2008
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<EmployeeAddExtract, IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareStaffRateDataSource(component);
//        preparePaymentsListDataSource(component);
        preparePaymentDataSource(component);
        super.onRefreshComponent(component);
        hasShowEmployeeHRColumn(component);

        checkEnabledAddStaffListPaymentButton(component);

//        Model model = getModel(component);

//        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateItemList());
//        CommonEmployeeExtractUtil.onRefreshPaymentCases(model);
    }

    public void onClickFreelance(IBusinessComponent component)
    {
        onChangePost(component);

        Model model = getModel(component);

        model.setPostRelationListModel(CommonEmployeeExtractUtil.getPostRelationListModel(model.getExtract()));
    }

    @Override
    public void onChangeBasics(IBusinessComponent component)
    {
        Model model = getModel(component);

        Date date = model.getExtract().getLabourContractDate();
        String number = model.getExtract().getLabourContractNumber();

        CommonEmployeeExtractUtil.fillExtractBasics(model.getSelectedBasicList(), model.getCurrentBasicMap(), date, number, null, null);
    }

    public void onChangeStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);
        CommonEmployeeExtractUtil.onChangePost(model.getExtract(), model.getStaffRateDataSource(), model.getStaffRateItemList());
//        CommonEmployeeExtractUtil.onRefreshPaymentCases(model);
    }

    @SuppressWarnings("unchecked")
    public void onChangePost(IBusinessComponent component)
    {
        Model model = getModel(component);
        CommonEmployeeExtractUtil.onChangePost(model.getExtract(), model.getStaffRateDataSource(), model.getStaffRateItemList());
//        CommonEmployeeExtractUtil.onRefreshPaymentCases(model);

        //првоеряем необходимо ли отображать
        CommonEmployeeExtractUtil.hasNeedShowEmployeeHRColumn(model, model.getExtract().isFreelance(), model.getExtract().getOrgUnit());
        prepareStaffRateDataSource(component);
        CommonEmployeeExtractUtil.fillFinSrcModel(model, model.getExtract().isFreelance(), model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL());

        //зануляем ист.фин.(дет) и Кадр.расст.
        final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());
        final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
        final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? new HashMap<>() : empHRHolder.getValueMap());
        for (FinancingSourceDetails item : model.getStaffRateItemList())
        {
            finSrcItmMap.put(item.getId(), null);
            empHRMap.put(item.getId(), null);
        }
    }

//    public void onPaymentChange(IBusinessComponent component)
//    {
//        Model model = getModel(component);
//
//        Double multiplier = CommonEmployeeExtractUtil.getSummStaffRateExtract(model.getStaffRateDataSource(), model.getStaffRateItemList());
//
//        CommonEmployeeExtractUtil.onPaymentChange(getModel(component), multiplier);
//    }

    @Override
    public void updateListDataSource(IBusinessComponent component)
    {
        getDao().prepareListDataSource(getModel(component));
    }

//    public void preparePaymentsListDataSource(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.createPaymentsBonusDataSource(getModel(component), component, this);
//    }

//    public void onClickAddStaffListBonuses(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickAddStaffListBonuses(getModel(component));
//    }

//    public void onClickPaymentUp(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickPaymentUpOrDown(getModel(component), (Long)component.getListenerParameter(), true);
//    }

//    public void onClickPaymentDown(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickPaymentUpOrDown(getModel(component), (Long)component.getListenerParameter(), false);
//    }

//    public void onClickAddBonus(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickAddBonusExtended(getModel(component)); //getDao().addPaymentAutocompleteModel(getModel(component));
//    }

//    public void onClickDeleteBonus(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickRemoveBonusExtended(getModel(component), (Long)component.getListenerParameter());
//    }

//    public void onClickCallAddSinglePaymentForm(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickCallAddSinglePaymentForm(getModel(component));
//    }

//    public void onClickCallAddStaffListPaymentsForm(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickCallAddStaffListPaymentsForm(getModel(component));
//    }

//    public void onClickCancelAddBonus(IBusinessComponent component)
//    {
//        CommonEmployeeExtractUtil.onClickCancelAddBonus(getModel(component));
//    }

    public void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (!model.isNeedUpdateDataSource() && model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<FinancingSourceDetails> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        }, 5);

        dataSource.addColumn(new BlockColumn("staffRate", "Ставка"));
        dataSource.addColumn(new BlockColumn("financingSource", "Источник финансирования"));
        dataSource.addColumn(new BlockColumn("financingSourceItem", "Источник финансирования (детально)"));
        if (model.isThereAnyActiveStaffList())
            dataSource.addColumn(new BlockColumn("employeeHR", "Кадровая расстановка"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem");
        actionColumn.setDisableSecondSubmit(false);
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setStaffRateDataSource(dataSource);
    }

    public void onChangeFinSrc(IBusinessComponent component)
    {
        Model model = getModel(component);

//        Long itemId = component.getListenerParameter() != null ?
//                (Long) component.getListenerParameter() :
//                model.getStaffRateDataSource().getCurrentEntity().getId();

        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateItemList());

//        CommonEmployeeExtractUtil.onRefreshPaymentCases(model);

        checkEnabledAddStaffListPaymentButton(component);
    }

    public void onChangeFinSrcItm(IBusinessComponent component)
    {
        Model model = getModel(component);

        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateItemList());

        checkEnabledAddStaffListPaymentButton(component);
    }

    public void onClickAddStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        FinancingSourceDetails item = new FinancingSourceDetails();

        short discriminator = EntityRuntime.getMeta(EmployeePostStaffRateItem.class).getEntityCode();
        long id = EntityIDGenerator.generateNewId(discriminator);

        item.setId(id);
        item.setExtract(model.getExtract());

        //добавляем новую не заполненную ставку в список ставок
        model.getStaffRateItemList().add(item);

        getDao().prepareStaffRateDataSource(model);
    }

    public void onClickDeleteItem(IBusinessComponent component)
    {
        Model model = getModel(component);

        FinancingSourceDetails item =(FinancingSourceDetails) component.getListenerParameter();

        model.getStaffRateItemList().remove(item);

        getDao().prepareStaffRateDataSource(model);

        onChangeStaffRate(component);
    }

//    public void prepareEmployeeHR(IBusinessComponent component)
//    {
//        Model model = getModel(component);
//
//        CommonEmployeeExtractUtil.prepareEmployeeHRModel(model, model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL());
//    }

    /**
     * Определяем необходимо ли показывать колонку Кадровая расстановка в сечлисте Ставка,<p>
     * если колонка показывается, то обновляем DataSource и заполняем модели мультиселектов в колонке
     */
    public void hasShowEmployeeHRColumn(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (!model.isEditForm())
            return;

        if (CommonEmployeeExtractUtil.hasNeedShowEmployeeHRColumn(model, model.getExtract().isFreelance(), model.getExtract().getOrgUnit()))
        {
            prepareStaffRateDataSource(component);
//            prepareEmployeeHR(component);
            CommonEmployeeExtractUtil.fillEmpHRColumn(model, model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL());
        }
    }


    //PAYMENTS METHODS

    public void preparePaymentDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getPaymentDataSource() != null)
            return;

        DynamicListDataSource<EmployeeBonus> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().preparePaymentDataSource(model);
        }, 5);

        CommonEmployeeExtractUtil.preparePaymentDataSourceForPaymentBlock(dataSource);

        model.setPaymentDataSource(dataSource);
    }

    public void onClickAddSinglePayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus bonus = CommonEmployeeExtractUtil.onClickAddSinglePayment(model.getPaymentList(), model.getExtract());

        model.getPaymentDataSource().refresh();
    }

    public void onClickAddStaffListPayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        List<StaffListPostPayment> staffListPaymentsList = checkEnabledAddStaffListPaymentButton(component);

        List<EmployeeBonus> employeeBonusList = CommonEmployeeExtractUtil.onClickAddStaffListPayment(model.getPaymentList(), staffListPaymentsList, model.getExtract());

        model.setAddStaffListPaymentsButtonVisible(false);

        model.getPaymentDataSource().refresh();
    }

    public void onChangePayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus bonus = component.getListenerParameter();

        bonus.setValueProxy(CommonEmployeeExtractUtil.getPaymentValueForPaymentBlock(bonus.getPayment(), model.getExtract().getPostBoundedWithQGandQL()));

        checkEnabledAddStaffListPaymentButton(component);
    }

    public void onUpPayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus currentBonus = component.<EmployeeBonus>getListenerParameter();

        CommonEmployeeExtractUtil.onUpPayment(currentBonus, model.getPaymentList());

        model.getPaymentDataSource().refresh();
    }

    public void onDownPayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus currentBonus = component.<EmployeeBonus>getListenerParameter();

        CommonEmployeeExtractUtil.onDownPayment(currentBonus, model.getPaymentList());

        model.getPaymentDataSource().refresh();
    }

    public void onDeletePayment(IBusinessComponent component)
    {
        Model model = getModel(component);
        EmployeeBonus currentBonus = component.<EmployeeBonus>getListenerParameter();

        CommonEmployeeExtractUtil.onDeletePayment(currentBonus, model.getPaymentList());

        checkEnabledAddStaffListPaymentButton(component);

        model.getPaymentDataSource().refresh();
    }

    /**
     * Проверяет и выстовляет доступность кнопки "Добавить выплаты, согласно штатному расписанию".
     * @return Возвращает доступные для добавления выплаты ШР, согласно ШР.
     */
    protected List<StaffListPostPayment> checkEnabledAddStaffListPaymentButton(IBusinessComponent component)
    {
        Model model = getModel(component);

        List<StaffListPostPayment> paymentList = CommonEmployeeExtractUtil.checkEnabledAddStaffListPaymentButton(model.getExtract().getPostBoundedWithQGandQL(), model.getExtract().getOrgUnit(), model.getPaymentList(), model.getStaffRateItemList());

        if (!paymentList.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);

        return paymentList;
    }
}