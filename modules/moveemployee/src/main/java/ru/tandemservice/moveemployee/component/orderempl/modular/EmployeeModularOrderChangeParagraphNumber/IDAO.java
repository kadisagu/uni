/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderChangeParagraphNumber;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 27.03.2013
 */
public interface IDAO extends IUniDao<Model>
{
}