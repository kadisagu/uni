package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.entity.gen.ReturnFromChildCareHolidayExtractGen;

/**
 * Выписка из сборного приказа по кадровому составу. О выходе из отпуска по уходу за ребенком
 */
public class ReturnFromChildCareHolidayExtract extends ReturnFromChildCareHolidayExtractGen
{
}