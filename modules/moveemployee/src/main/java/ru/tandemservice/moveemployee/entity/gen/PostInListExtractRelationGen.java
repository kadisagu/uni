package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import ru.tandemservice.moveemployee.entity.PostInListExtractRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выбранных должностей в списочном параграфе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PostInListExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.PostInListExtractRelation";
    public static final String ENTITY_NAME = "postInListExtractRelation";
    public static final int VERSION_HASH = 1185019089;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_LIST_PARAGRAPH = "employeeListParagraph";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";

    private EmployeeListParagraph _employeeListParagraph;     // Параграф списочного приказа по сотруднику
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность/профессия отнесенная к ПКГ и КУ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Параграф списочного приказа по сотруднику. Свойство не может быть null.
     */
    @NotNull
    public EmployeeListParagraph getEmployeeListParagraph()
    {
        return _employeeListParagraph;
    }

    /**
     * @param employeeListParagraph Параграф списочного приказа по сотруднику. Свойство не может быть null.
     */
    public void setEmployeeListParagraph(EmployeeListParagraph employeeListParagraph)
    {
        dirty(_employeeListParagraph, employeeListParagraph);
        _employeeListParagraph = employeeListParagraph;
    }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PostInListExtractRelationGen)
        {
            setEmployeeListParagraph(((PostInListExtractRelation)another).getEmployeeListParagraph());
            setPostBoundedWithQGandQL(((PostInListExtractRelation)another).getPostBoundedWithQGandQL());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PostInListExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PostInListExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new PostInListExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeListParagraph":
                    return obj.getEmployeeListParagraph();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeListParagraph":
                    obj.setEmployeeListParagraph((EmployeeListParagraph) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeListParagraph":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeListParagraph":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeListParagraph":
                    return EmployeeListParagraph.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PostInListExtractRelation> _dslPath = new Path<PostInListExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PostInListExtractRelation");
    }
            

    /**
     * @return Параграф списочного приказа по сотруднику. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PostInListExtractRelation#getEmployeeListParagraph()
     */
    public static EmployeeListParagraph.Path<EmployeeListParagraph> employeeListParagraph()
    {
        return _dslPath.employeeListParagraph();
    }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PostInListExtractRelation#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    public static class Path<E extends PostInListExtractRelation> extends EntityPath<E>
    {
        private EmployeeListParagraph.Path<EmployeeListParagraph> _employeeListParagraph;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Параграф списочного приказа по сотруднику. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PostInListExtractRelation#getEmployeeListParagraph()
     */
        public EmployeeListParagraph.Path<EmployeeListParagraph> employeeListParagraph()
        {
            if(_employeeListParagraph == null )
                _employeeListParagraph = new EmployeeListParagraph.Path<EmployeeListParagraph>(L_EMPLOYEE_LIST_PARAGRAPH, this);
            return _employeeListParagraph;
        }

    /**
     * @return Должность/профессия отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PostInListExtractRelation#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

        public Class getEntityClass()
        {
            return PostInListExtractRelation.class;
        }

        public String getEntityName()
        {
            return "postInListExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
