/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.singleemplextract.e6.AddEdit;

import java.util.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.VoluntaryTerminationSExtract;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 25.09.2009
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<VoluntaryTerminationSExtract>
{
    private boolean _needRelativeData = false;

    private DynamicListDataSource<EmployeePostStaffRateItem> _staffRateDataSource;
    private List<RelationDegree> _relativeTypesList;

    //Getters & Setters

    public DynamicListDataSource<EmployeePostStaffRateItem> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public boolean isNeedRelativeData()
    {
        return _needRelativeData;
    }

    public void setNeedRelativeData(boolean needRelativeData)
    {
        this._needRelativeData = needRelativeData;
    }

    public List<RelationDegree> getRelativeTypesList()
    {
        return _relativeTypesList;
    }

    public void setRelativeTypesList(List<RelationDegree> relativeTypesList)
    {
        this._relativeTypesList = relativeTypesList;
    }
}