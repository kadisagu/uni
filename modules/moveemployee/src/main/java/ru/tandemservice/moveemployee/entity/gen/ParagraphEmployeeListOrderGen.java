package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочный приказ по сотрудникам. Формируемый параграфами
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ParagraphEmployeeListOrderGen extends EmployeeListOrder
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder";
    public static final String ENTITY_NAME = "paragraphEmployeeListOrder";
    public static final int VERSION_HASH = 834539022;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ParagraphEmployeeListOrderGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ParagraphEmployeeListOrderGen> extends EmployeeListOrder.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ParagraphEmployeeListOrder.class;
        }

        public T newInstance()
        {
            return (T) new ParagraphEmployeeListOrder();
        }
    }
    private static final Path<ParagraphEmployeeListOrder> _dslPath = new Path<ParagraphEmployeeListOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ParagraphEmployeeListOrder");
    }
            

    public static class Path<E extends ParagraphEmployeeListOrder> extends EmployeeListOrder.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return ParagraphEmployeeListOrder.class;
        }

        public String getEntityName()
        {
            return "paragraphEmployeeListOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
