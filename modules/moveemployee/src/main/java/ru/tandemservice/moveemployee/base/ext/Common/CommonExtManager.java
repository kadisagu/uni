/* $Id:$ */
package ru.tandemservice.moveemployee.base.ext.Common;

import org.apache.commons.collections15.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.ModuleStatusReportBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.util.IModuleStatusReporter;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.report.EmployeeVPO1Report;
import ru.tandemservice.uniemp.entity.report.QuantityEmpByDepReport;
import ru.tandemservice.uniemp.entity.report.StaffListAllocationReport;
import ru.tandemservice.uniemp.entity.report.StaffListReport;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;
import ru.tandemservice.unimv.entity.visa.VisaHistoryItem;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author oleyba
 * @since 2/11/14
 */
@Configuration
public class CommonExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private CommonManager _commonManager;

    @Bean
    public ItemListExtension<IModuleStatusReporter> moduleStatusExtPoint()
    {
        return itemListExtension(_commonManager.moduleStatusExtPoint())
                .add("moveemployee", () -> {

                    IUniBaseDao dao = IUniBaseDao.instance.get();
                    List<String> result = new ArrayList<>();
                    String alias = "a";

                    final List<Object[]> finishedExtracts = dao.getList(new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, alias)
                            .where(eq(property(alias, AbstractEmployeeExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                            .column(property(alias, AbstractEmployeeExtract.type().title()))
                            .column(DQLFunctions.year(property(alias, AbstractEmployeeExtract.createDate())), "p")
                            .column(DQLFunctions.count(property(alias, AbstractEmployeeExtract.id())))
                            .group(property(alias, AbstractEmployeeExtract.type().title()))
                            .group(DQLFunctions.year(property(alias, AbstractEmployeeExtract.createDate())))
                            .order(property(alias, AbstractEmployeeExtract.type().title()))
                            .order(property("p")));

                    final List<Object[]> acceptedExtracts = dao.getList(new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, alias)
                            .where(eq(property(alias, AbstractEmployeeExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                            .column(property(alias, AbstractEmployeeExtract.type().title()))
                            .column(DQLFunctions.year(property(alias, AbstractEmployeeExtract.createDate())), "p")
                            .column(DQLFunctions.count(property(alias, AbstractEmployeeExtract.id())))
                            .group(property(alias, AbstractEmployeeExtract.type().title()))
                            .group(DQLFunctions.year(property(alias, AbstractEmployeeExtract.createDate())))
                            .order(property(alias, AbstractEmployeeExtract.type().title()))
                            .order(property("p")));


                    final List<Object[]> finishedExtractsByOrgUnits = dao.getList(new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, alias)
                            .where(eq(property(alias, AbstractEmployeeExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                            .column(property(alias, AbstractEmployeeExtract.entity().orgUnit().title()))
                            .column(DQLFunctions.year(property(alias, AbstractEmployeeExtract.createDate())), "p")
                            .column(DQLFunctions.count(property(alias, AbstractEmployeeExtract.id())))
                            .group(property(alias, AbstractEmployeeExtract.entity().orgUnit().title()))
                            .group(DQLFunctions.year(property(alias, AbstractEmployeeExtract.createDate())))
                            .order(property(alias, AbstractEmployeeExtract.entity().orgUnit().title()))
                            .order(property("p")));

                    result.add("Число проведенных выписок:");
                    result.addAll(CollectionUtils.collect(finishedExtracts, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER));
                    result.add("Число согласованных выписок:");
                    result.addAll(CollectionUtils.collect(acceptedExtracts, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER));
                    result.add("Число пользовательских шаблонов в справочнике: " + dao.getCount(new DQLSelectBuilder().fromEntity(MoveemployeeTemplate.class, alias)
                                    .where(isNotNull(property(alias, MoveemployeeTemplate.document())))
                    ));
                    result.add("Число проведенных выписок по подразделению:");
                    result.addAll(CollectionUtils.collect(finishedExtractsByOrgUnits, ModuleStatusReportBuilder.QUERY_RESULT_TRANSFORMER));
                    result.add("Используется визирование (число выписок с визами): " + dao.getCount(new DQLSelectBuilder().fromEntity(AbstractEmployeeExtract.class, alias)
                                    .joinEntity(alias, DQLJoinType.inner, VisaHistoryItem.class, "b", eq(property(alias, AbstractEmployeeExtract.id()), property("b", VisaHistoryItem.document().id())))
                                    .column(property(alias, AbstractEmployeeExtract.id()))
                                    .distinct()
                    ));
                    result.add("Число сохраненных отчетов:");
                    result.add(" • отчет «ВПО-1 - Сведения о персонале учреждения» - " + dao.getCount(EmployeeVPO1Report.class));
                    result.add(" • отчет «Численность персонала по подразделениям» - " + dao.getCount(QuantityEmpByDepReport.class));
                    result.add(" • отчет «Штатная расстановка» - " + dao.getCount(StaffListAllocationReport.class));
                    result.add(" • отчет «Штатное расписание» - " + dao.getCount(StaffListReport.class));
                    result.add("Число ШР: " + dao.getCount(StaffList.class));

                    return result;
                })
                .create();
    }
}
