package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph;
import ru.tandemservice.moveemployee.entity.EmployeeListParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф списочного приказа по сотруднику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeListParagraphGen extends AbstractEmployeeParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeListParagraph";
    public static final String ENTITY_NAME = "employeeListParagraph";
    public static final int VERSION_HASH = 281335418;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeListParagraphGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeListParagraphGen> extends AbstractEmployeeParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeListParagraph.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeListParagraph();
        }
    }
    private static final Path<EmployeeListParagraph> _dslPath = new Path<EmployeeListParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeListParagraph");
    }
            

    public static class Path<E extends EmployeeListParagraph> extends AbstractEmployeeParagraph.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EmployeeListParagraph.class;
        }

        public String getEntityName()
        {
            return "employeeListParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
