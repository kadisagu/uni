package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeOrderTextRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь приказа и текста приказа (приказы по кадрам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeOrderTextRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeOrderTextRelation";
    public static final String ENTITY_NAME = "employeeOrderTextRelation";
    public static final int VERSION_HASH = -287497102;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_TEXT = "text";

    private AbstractEmployeeOrder _order;     // Приказ по кадрам
    private byte[] _text;     // Сохраненная печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ по кадрам. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AbstractEmployeeOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ по кадрам. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrder(AbstractEmployeeOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     */
    @NotNull
    public byte[] getText()
    {
        return _text;
    }

    /**
     * @param text Сохраненная печатная форма. Свойство не может быть null.
     */
    public void setText(byte[] text)
    {
        dirty(_text, text);
        _text = text;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeOrderTextRelationGen)
        {
            setOrder(((EmployeeOrderTextRelation)another).getOrder());
            setText(((EmployeeOrderTextRelation)another).getText());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeOrderTextRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeOrderTextRelation.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeOrderTextRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "text":
                    return obj.getText();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((AbstractEmployeeOrder) value);
                    return;
                case "text":
                    obj.setText((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "text":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "text":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return AbstractEmployeeOrder.class;
                case "text":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeOrderTextRelation> _dslPath = new Path<EmployeeOrderTextRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeOrderTextRelation");
    }
            

    /**
     * @return Приказ по кадрам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EmployeeOrderTextRelation#getOrder()
     */
    public static AbstractEmployeeOrder.Path<AbstractEmployeeOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeOrderTextRelation#getText()
     */
    public static PropertyPath<byte[]> text()
    {
        return _dslPath.text();
    }

    public static class Path<E extends EmployeeOrderTextRelation> extends EntityPath<E>
    {
        private AbstractEmployeeOrder.Path<AbstractEmployeeOrder> _order;
        private PropertyPath<byte[]> _text;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ по кадрам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.moveemployee.entity.EmployeeOrderTextRelation#getOrder()
     */
        public AbstractEmployeeOrder.Path<AbstractEmployeeOrder> order()
        {
            if(_order == null )
                _order = new AbstractEmployeeOrder.Path<AbstractEmployeeOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeOrderTextRelation#getText()
     */
        public PropertyPath<byte[]> text()
        {
            if(_text == null )
                _text = new PropertyPath<byte[]>(EmployeeOrderTextRelationGen.P_TEXT, this);
            return _text;
        }

        public Class getEntityClass()
        {
            return EmployeeOrderTextRelation.class;
        }

        public String getEntityName()
        {
            return "employeeOrderTextRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
