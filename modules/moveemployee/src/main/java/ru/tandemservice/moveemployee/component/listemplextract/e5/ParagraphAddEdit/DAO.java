/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e5.ParagraphAddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeePaymentEmplListExtract;
import ru.tandemservice.moveemployee.entity.OrgUnitInListExtractRelation;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.FinancingSourceItemSingleSelectModel;
import ru.tandemservice.uniemp.dao.FinancingSourceSingleSelectModel;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.util.UniempUtil;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class DAO extends AbstractListParagraphAddEditDAO<EmployeePaymentEmplListExtract, Model> implements IDAO
{
    protected static OrderDescriptionRegistry orderRegistry = new OrderDescriptionRegistry("b");

    @Override
    @SuppressWarnings("unchecked")
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setPaymentModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(Payment.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", Payment.active().s(), Boolean.TRUE));
                builder.add(MQExpression.like("b", Payment.title().s(), CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", Payment.id().s(), o));

                return new MQListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((Payment) value).getFullTitle();
            }
        });

        model.setFinancingSourceModel(new FinancingSourceSingleSelectModel());
        model.setFinancingSourceItemModel(new FinancingSourceItemSingleSelectModel(model.getFinancingSource()));

        model.setOrgUnitModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<OrgUnit> resultList = new ArrayList<>();
                for (OrgUnit entity : findValues("").getObjects())
                    if (primaryKeys.contains(entity.getId()))
                        resultList.add(entity);

                return resultList;
            }

            @Override
            public ListResult<OrgUnit> findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "b");
                builder.add(MQExpression.eq("b", OrgUnit.archival().s(), Boolean.FALSE));
                builder.add(MQExpression.like("b", OrgUnit.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("b", OrgUnit.orgUnitType().title().s(), OrderDirection.asc);
                builder.addOrder("b", OrgUnit.title().s(), OrderDirection.asc);

                return new ListResult<>(builder.<OrgUnit>getResultList(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnit) value).getTitleWithType();
            }
        });

        if (model.isEditForm())
        {
            MQBuilder builder = new MQBuilder(OrgUnitInListExtractRelation.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", OrgUnitInListExtractRelation.employeeListParagraph().s(), model.getParagraph()));

            List<OrgUnit> orgUnitList = new ArrayList<>();

            for (OrgUnitInListExtractRelation relation : builder.<OrgUnitInListExtractRelation>getResultList(getSession()))
            {
                orgUnitList.add(relation.getOrgUnit());
                delete(relation);
            }

            model.setOrgUnitList(orgUnitList);

            model.setChildOrgUnit(((EmployeePaymentEmplListExtract)model.getParagraph().getFirstExtract()).getChildOrgUnit());

            model.setSelectEmployee(new ArrayList<>());
            model.setEmployeeIdAmountMap(new HashMap<>());
            for (EmployeePaymentEmplListExtract extract : (List<EmployeePaymentEmplListExtract>) model.getParagraph().getExtractList())
            {
                model.getSelectEmployee().add(extract.getEntity());
                model.getEmployeeIdAmountMap().put(extract.getEntity().getId(), extract.getAmountEmployee());
            }

            EmployeePaymentEmplListExtract extract = (EmployeePaymentEmplListExtract) model.getParagraph().getFirstExtract();
            model.setAssignmentDate(extract.getAssignmentDate());
            model.setAssignmentBeginDate(extract.getAssignmentBeginDate());
            model.setAssignmentEndDate(extract.getAssignmentEndDate());
            model.setPayment(extract.getPayment());
            model.setAmount(extract.getAmountPayment());
            model.setFinancingSource(extract.getFinancingSource());
            model.setFinancingSourceItem(extract.getFinancingSourceItem());

            model.setTempEmployeeList(model.getSelectEmployee());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        final IValueMapHolder amountHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("amount");
        final Map<Long, Double> amountMap = (null == amountHolder ? Collections.emptyMap() : amountHolder.getValueMap());

        for (EmployeePost employeePost : model.getEmployeeDataSource().getSelectedEntities())
        {
            if (amountMap.get(employeePost.getId()) == null)
                errors.add("Поле «Сумма выплаты, руб» обязательно для заполнения.", employeePost.getId() + "_amount");
        }

        if (model.getEmployeeDataSource().getSelectedEntities().isEmpty())
            errors.add("Необходимо выбрать хотя бы одного сотрудника.");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        model.setToSaveExtractList(new ArrayList<>());

        ExtractStates extractStates = getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER);

        final IValueMapHolder amountHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("amount");
        final Map<Long, Double> amountMap = (null == amountHolder ? Collections.emptyMap() : amountHolder.getValueMap());

        for (EmployeePost employeePost : model.getEmployeeDataSource().getSelectedEntities())
        {
            if (model.isEditForm())
            {
                if (!model.getSelectEmployee().contains(employeePost))
                    if (MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeePostHasNotFinishedExtracts(employeePost))
                        throw new ApplicationException("Нельзя добавить параграф, так как у сотрудника '" + employeePost.getPerson().getFio() + "' уже есть непроведенные выписки.");
            }
            else
            if (MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeePostHasNotFinishedExtracts(employeePost))
                throw new ApplicationException("Нельзя добавить параграф, так как у сотрудника '" + employeePost.getPerson().getFio() + "' уже есть непроведенные выписки.");

            EmployeePaymentEmplListExtract extract = new EmployeePaymentEmplListExtract();
            extract.setEntity(employeePost);
            extract.setEmployee(employeePost.getEmployee());
            extract.setParagraph(model.getParagraph());
            extract.setState(extractStates);
            extract.setEmployeeFioModified(employeePost.getPerson().getFio());
            extract.setType(model.getExtractType());
            extract.setAssignmentDate(model.getAssignmentDate());
            if (model.getPayment().isOneTimePayment())
            {
                extract.setAssignmentBeginDate(null);
                extract.setAssignmentEndDate(null);
            }
            else
            {
                extract.setAssignmentBeginDate(model.getAssignmentBeginDate());
                extract.setAssignmentEndDate(model.getAssignmentEndDate());
            }
            extract.setPayment(model.getPayment());
            extract.setAmountEmployee(getAmount(amountMap.get(employeePost.getId())));
            extract.setAmountPayment(model.getAmount());
            extract.setFinancingSource(model.getFinancingSource());
            extract.setFinancingSourceItem(model.getFinancingSourceItem());
            extract.setChildOrgUnit(model.getChildOrgUnit());
            extract.setCreateDate(model.getCreateDate());
            extract.setReason(model.getReason());

            model.getToSaveExtractList().add(extract);
        }

        saveOrUpdate(model.getParagraph());
        for (OrgUnit orgUnit : model.getOrgUnitList())
        {
            OrgUnitInListExtractRelation relation = new OrgUnitInListExtractRelation();
            relation.setOrgUnit(orgUnit);
            relation.setEmployeeListParagraph(model.getParagraph());
            save(relation);
        }

        super.update(model);
    }

    private Double getAmount(Object amount)
    {
        if (amount instanceof Long)
            return ((Long) amount).doubleValue();
        else
            return (Double) amount;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareEmployeeDataSource(Model model)
    {
        if (model.getTempEmployeeList() == null)
            model.setTempEmployeeList(model.getEmployeePostList());

        List<OrgUnit> orgUnitList = new ArrayList<>();
        if (model.getOrgUnitList() != null)
            orgUnitList.addAll(model.getOrgUnitList());
        if (model.getChildOrgUnit() != null && model.getChildOrgUnit())
            for (OrgUnit orgUnit : model.getOrgUnitList())
                orgUnitList.addAll(OrgUnitManager.instance().dao().getChildOrgUnits(orgUnit, true, false));

        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "b");
        builder.add(MQExpression.in("b", EmployeePost.orgUnit().s(), orgUnitList));
        builder.add(MQExpression.eq("b", EmployeePost.postStatus().active().s(), Boolean.TRUE));

        orderRegistry.applyOrder(builder, model.getEmployeeDataSource().getEntityOrder());

        model.setEmployeePostList(builder.<EmployeePost>getResultList(getSession(), (int) model.getEmployeeDataSource().getActualStartRow(), (int) model.getEmployeeDataSource().getCountRow()));

        UniBaseUtils.createPage(model.getEmployeeDataSource(), builder, getSession());

        if (model.getPayment() == null)
            return;

        final IValueMapHolder amountHolder = (IValueMapHolder) model.getEmployeeDataSource().getColumn("amount");
        final Map<Long, Double> amountMap = (null == amountHolder ? Collections.emptyMap() : amountHolder.getValueMap());

        for (EmployeePost employeePost : model.getEmployeeDataSource().getEntityList())
        {
            if (model.getTempEmployeeList() != null && model.getTempEmployeeList().contains(employeePost))
            {
                if (model.getSelectEmployee() != null && model.getSelectEmployee().contains(employeePost))
                {
                    Double amount = getAmount(amountMap.get(employeePost.getId()));
                    amountMap.put(employeePost.getId(), amount != null ? amount : model.getEmployeeIdAmountMap().get(employeePost.getId()));
                }
                continue;
            }

            Double salary = employeePost.getSalary() != null ? employeePost.getSalary() : 0d;

            Double amount = null;
            if (model.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_RUBLES))
            {
                amount = UniempDaoFacade.getUniempDAO().getPaymentBaseValueForPost(model.getPayment(), employeePost.getPostRelation().getPostBoundedWithQGandQL());
                if (amount == 0)
                    amount = null;
            }
            else if (model.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_BASE_PERCENT))
                amount = model.getAmount() != null ? (model.getAmount() * salary) / 100 : null;
            else if (model.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_COEFFICIENT))
                amount = model.getAmount() != null ? model.getAmount() * salary : null;
            else if (model.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_FULL_PERCENT))
            {
                List<Payment> includePaymentList = UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(model.getPayment());
                amount = model.getAmount() != null ? UniempUtil.getPaymentOnFOTAmount(employeePost, includePaymentList) * model.getAmount() / 100 : null;
            }

            amountMap.put(employeePost.getId(), amount != null ? amount : model.getAmount());
        }

        model.setTempEmployeeList(null);
    }

    @Override
    protected EmployeePaymentEmplListExtract createNewExtractInstance(Model model)
    {
        return new EmployeePaymentEmplListExtract();
    }
}