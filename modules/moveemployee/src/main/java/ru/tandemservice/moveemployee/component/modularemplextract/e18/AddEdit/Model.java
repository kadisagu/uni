/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e18.AddEdit;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 15.09.2011
 */
@SuppressWarnings("unchecked")
public class Model extends CommonModularEmployeeExtractAddEditModel<TransferAnnualHolidayExtract>
{
    List<IdentifiableWrapper> _holidayList; //Сорс для селекта Отпуск за период
    IdentifiableWrapper _choseHoliday; //Выбранный объект в селекте Отпуск за период
    Boolean _showHoliday = true; //Значение определяющее будут отображаться Части отпуска или формы для создания нового планируемого отпуска
    IMultiSelectModel _holidayPartModel; //Модель для мультиСелекта Отпуск, содержит Отпуска или Планируемые отпуска сотрудника
    List<IdentifiableWrapper> _holidayPartList; //Список объектов, выбранных в мультиселекте Отпуск

    /*Поля для частей нового планируемого отпуска, раздел Отменяемый отпуск. Используются в случае если в автоКомплитСелекте Отпуск выбранно -Новый период-*/
    DynamicListDataSource _holidayDataSource; // ДатаСорс для сечЛиста нового планируемого отпуска
    List<IdentifiableWrapper> _partHolidayList = new ArrayList<IdentifiableWrapper>(); //Список содержащий части нового планируемого отпуска
    Map<IdentifiableWrapper, Date> _beginHolidayDayMap = new HashMap<IdentifiableWrapper, Date>(); //Мапа содержащая даты начала частей нового планируемого отпуска
    Map<IdentifiableWrapper, Date> _endHolidayDayMap = new HashMap<IdentifiableWrapper, Date>(); //Мапа содержащая даты окончания частей нового планируемого отпуска
    Map<IdentifiableWrapper, Integer> _durationHolidayDayMap = new HashMap<IdentifiableWrapper, Integer>(); //Мапа содержащая длительности частей нового планируемого отпуска

    /*Поля для частей фактического Предоставляемого отпуска*/
    DynamicListDataSource<IdentifiableWrapper> _factHolidayDataSource; //ДатаСорс для сечЛиста Предостовляемого отпуска
    List<IdentifiableWrapper> _partFactHolidayList = new ArrayList<IdentifiableWrapper>(); //Список содержащий части Предоставляемого отпуска
    Map<IdentifiableWrapper, Date> _beginFactHolidayDayMap = new HashMap<IdentifiableWrapper, Date>(); //Мапа содержащая даты начала частей Предоставляемого отпуска
    Map<IdentifiableWrapper, Date> _endFactHolidayDayMap = new HashMap<IdentifiableWrapper, Date>(); //Мапа содержащая даты окончания частей Предоставляемого отпуска
    Map<IdentifiableWrapper, Integer> _durationFactHolidayDayMap = new HashMap<IdentifiableWrapper, Integer>(); //Мапа содержащая длительности частей Предоставляемого отпуска

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add_item.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Location getLocation()
        {
            return null;  //Body of implemented method
        }
    };

    //Getters & Setters

    public String getBeginHolidayDayId()
    {
        return getHolidayDataSource().getCurrentEntity().getId() + "_beginDayId";
    }

    public String getEndHolidayDayId()
    {
        return getHolidayDataSource().getCurrentEntity().getId() + "_endDayId";
    }

    public String getDurationHolidayDayId()
    {
        return getHolidayDataSource().getCurrentEntity().getId() + "_durationDayId";
    }

    public Date getBeginHolidayDay()
    {
        return getBeginHolidayDayMap().get(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity());
    }

    public void setBeginHolidayDay(Date beginDay)
    {
        getBeginHolidayDayMap().put(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity(), beginDay);
    }

    public Date getEndHolidayDay()
    {
        return getEndHolidayDayMap().get(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity());
    }

    public void setEndHolidayDay(Date endDay)
    {
        getEndHolidayDayMap().put(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity(), endDay);
    }

    public Integer getDurationHolidayDay()
    {
        return getDurationHolidayDayMap().get(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity());
    }

    public void setDurationHolidayDay(Integer durationDay)
    {
        getDurationHolidayDayMap().put(((ViewWrapper<IdentifiableWrapper>) getHolidayDataSource().getCurrentEntity()).getEntity(), durationDay);
    }

    public String getBeginFactHolidayDayId()
    {
        return getFactHolidayDataSource().getCurrentEntity().getId() + "_beginDay";
    }

    public String getEndFactHolidayDayId()
    {
        return getFactHolidayDataSource().getCurrentEntity().getId() + "_endDay";
    }

    public String getDurationFactHolidayDayId()
    {
        return getFactHolidayDataSource().getCurrentEntity().getId() + "_durationDay";
    }

    public Date getBeginFactHolidayDay()
    {
        return getBeginFactHolidayDayMap().get(getFactHolidayDataSource().getCurrentEntity());
    }

    public void setBeginFactHolidayDay(Date beginDay)
    {
        getBeginFactHolidayDayMap().put(getFactHolidayDataSource().getCurrentEntity(), beginDay);
    }

    public Date getEndFactHolidayDay()
    {
        return getEndFactHolidayDayMap().get(getFactHolidayDataSource().getCurrentEntity());
    }

    public void setEndFactHolidayDay(Date endDay)
    {
        getEndFactHolidayDayMap().put(getFactHolidayDataSource().getCurrentEntity(), endDay);
    }

    public Integer getDurationFactHolidayDay()
    {
        return getDurationFactHolidayDayMap().get(getFactHolidayDataSource().getCurrentEntity());
    }

    public void setDurationFactHolidayDay(Integer durationDay)
    {
        getDurationFactHolidayDayMap().put(getFactHolidayDataSource().getCurrentEntity(), durationDay);
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    public DynamicListDataSource getHolidayDataSource()
    {
        return _holidayDataSource;
    }

    public void setHolidayDataSource(DynamicListDataSource holidayDataSource)
    {
        _holidayDataSource = holidayDataSource;
    }

    public DynamicListDataSource<IdentifiableWrapper> getFactHolidayDataSource()
    {
        return _factHolidayDataSource;
    }

    public void setFactHolidayDataSource(DynamicListDataSource<IdentifiableWrapper> factHolidayDataSource)
    {
        _factHolidayDataSource = factHolidayDataSource;
    }

    public IdentifiableWrapper getChoseHoliday()
    {
        return _choseHoliday;
    }

    public void setChoseHoliday(IdentifiableWrapper choseHoliday)
    {
        _choseHoliday = choseHoliday;
    }

    public List<IdentifiableWrapper> getHolidayList()
    {
        return _holidayList;
    }

    public void setHolidayList(List<IdentifiableWrapper> holidayList)
    {
        _holidayList = holidayList;
    }

    public Boolean getShowHoliday()
    {
        return _showHoliday;
    }

    public void setShowHoliday(Boolean showHoliday)
    {
        _showHoliday = showHoliday;
    }

    public IMultiSelectModel getHolidayPartModel()
    {
        return _holidayPartModel;
    }

    public void setHolidayPartModel(IMultiSelectModel holidayPartModel)
    {
        _holidayPartModel = holidayPartModel;
    }

    public List<IdentifiableWrapper> getHolidayPartList()
    {
        return _holidayPartList;
    }

    public void setHolidayPartList(List<IdentifiableWrapper> holidayPartList)
    {
        _holidayPartList = holidayPartList;
    }

    public List<IdentifiableWrapper> getPartHolidayList()
    {
        return _partHolidayList;
    }

    public void setPartHolidayList(List<IdentifiableWrapper> partHolidayList)
    {
        _partHolidayList = partHolidayList;
    }

    public Map<IdentifiableWrapper, Date> getBeginHolidayDayMap()
    {
        return _beginHolidayDayMap;
    }

    public void setBeginHolidayDayMap(Map<IdentifiableWrapper, Date> beginHolidayDayMap)
    {
        _beginHolidayDayMap = beginHolidayDayMap;
    }

    public Map<IdentifiableWrapper, Date> getEndHolidayDayMap()
    {
        return _endHolidayDayMap;
    }

    public void setEndHolidayDayMap(Map<IdentifiableWrapper, Date> endHolidayDayMap)
    {
        _endHolidayDayMap = endHolidayDayMap;
    }

    public Map<IdentifiableWrapper, Integer> getDurationHolidayDayMap()
    {
        return _durationHolidayDayMap;
    }

    public void setDurationHolidayDayMap(Map<IdentifiableWrapper, Integer> durationHolidayDayMap)
    {
        _durationHolidayDayMap = durationHolidayDayMap;
    }

    public List<IdentifiableWrapper> getPartFactHolidayList()
    {
        return _partFactHolidayList;
    }

    public void setPartFactHolidayList(List<IdentifiableWrapper> partFactHolidayList)
    {
        _partFactHolidayList = partFactHolidayList;
    }

    public Map<IdentifiableWrapper, Date> getBeginFactHolidayDayMap()
    {
        return _beginFactHolidayDayMap;
    }

    public void setBeginFactHolidayDayMap(Map<IdentifiableWrapper, Date> beginFactHolidayDayMap)
    {
        _beginFactHolidayDayMap = beginFactHolidayDayMap;
    }

    public Map<IdentifiableWrapper, Date> getEndFactHolidayDayMap()
    {
        return _endFactHolidayDayMap;
    }

    public void setEndFactHolidayDayMap(Map<IdentifiableWrapper, Date> endFactHolidayDayMap)
    {
        _endFactHolidayDayMap = endFactHolidayDayMap;
    }

    public Map<IdentifiableWrapper, Integer> getDurationFactHolidayDayMap()
    {
        return _durationFactHolidayDayMap;
    }

    public void setDurationFactHolidayDayMap(Map<IdentifiableWrapper, Integer> durationFactHolidayDayMap)
    {
        _durationFactHolidayDayMap = durationFactHolidayDayMap;
    }
}