package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь причины и основания (приказы по кадрам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeReasonToBasicRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel";
    public static final String ENTITY_NAME = "employeeReasonToBasicRel";
    public static final int VERSION_HASH = -2125772434;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIRST = "first";
    public static final String L_SECOND = "second";

    private EmployeeOrderReasons _first;     // Причина приказа по кадрам
    private EmployeeOrderBasics _second;     // Основание приказа по кадрам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null.
     */
    @NotNull
    public EmployeeOrderReasons getFirst()
    {
        return _first;
    }

    /**
     * @param first Причина приказа по кадрам. Свойство не может быть null.
     */
    public void setFirst(EmployeeOrderReasons first)
    {
        dirty(_first, first);
        _first = first;
    }

    /**
     * @return Основание приказа по кадрам. Свойство не может быть null.
     */
    @NotNull
    public EmployeeOrderBasics getSecond()
    {
        return _second;
    }

    /**
     * @param second Основание приказа по кадрам. Свойство не может быть null.
     */
    public void setSecond(EmployeeOrderBasics second)
    {
        dirty(_second, second);
        _second = second;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EmployeeReasonToBasicRelGen)
        {
            setFirst(((EmployeeReasonToBasicRel)another).getFirst());
            setSecond(((EmployeeReasonToBasicRel)another).getSecond());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeReasonToBasicRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeReasonToBasicRel.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeReasonToBasicRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "first":
                    return obj.getFirst();
                case "second":
                    return obj.getSecond();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "first":
                    obj.setFirst((EmployeeOrderReasons) value);
                    return;
                case "second":
                    obj.setSecond((EmployeeOrderBasics) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "first":
                        return true;
                case "second":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "first":
                    return true;
                case "second":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "first":
                    return EmployeeOrderReasons.class;
                case "second":
                    return EmployeeOrderBasics.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeReasonToBasicRel> _dslPath = new Path<EmployeeReasonToBasicRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeReasonToBasicRel");
    }
            

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel#getFirst()
     */
    public static EmployeeOrderReasons.Path<EmployeeOrderReasons> first()
    {
        return _dslPath.first();
    }

    /**
     * @return Основание приказа по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel#getSecond()
     */
    public static EmployeeOrderBasics.Path<EmployeeOrderBasics> second()
    {
        return _dslPath.second();
    }

    public static class Path<E extends EmployeeReasonToBasicRel> extends EntityPath<E>
    {
        private EmployeeOrderReasons.Path<EmployeeOrderReasons> _first;
        private EmployeeOrderBasics.Path<EmployeeOrderBasics> _second;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина приказа по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel#getFirst()
     */
        public EmployeeOrderReasons.Path<EmployeeOrderReasons> first()
        {
            if(_first == null )
                _first = new EmployeeOrderReasons.Path<EmployeeOrderReasons>(L_FIRST, this);
            return _first;
        }

    /**
     * @return Основание приказа по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeReasonToBasicRel#getSecond()
     */
        public EmployeeOrderBasics.Path<EmployeeOrderBasics> second()
        {
            if(_second == null )
                _second = new EmployeeOrderBasics.Path<EmployeeOrderBasics>(L_SECOND, this);
            return _second;
        }

        public Class getEntityClass()
        {
            return EmployeeReasonToBasicRel.class;
        }

        public String getEntityName()
        {
            return "employeeReasonToBasicRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
