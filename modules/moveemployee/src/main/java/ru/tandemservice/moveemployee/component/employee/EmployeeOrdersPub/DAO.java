/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.employee.EmployeeOrdersPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author dseleznev
 *         Created on: 07.11.2008
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setExtractCantBeAdded(MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeeHasFormativeExtracts(model.getEmployee()));
        model.setSextractCantBeAdded(MoveEmployeeDaoFacade.getMoveEmployeeDao().isEmployeeHasFormativeExtracts(model.getEmployee()));
    }

    @Override
    public void prepareCustomDataSource(Model model, int index)
    {
        switch (index)
        {
            case IDAO.MODULAR_PROJECTS_DATASOUCE_IDX:
                prepareModularProjectsDataSource(model);
                break;
            case IDAO.MODULAR_EXTRACTS_DATASOUCE_IDX:
                prepareModularExtractsDataSource(model);
                break;
            case IDAO.SINGLE_PROJECTS_DATASOUCE_IDX:
                prepareSingleProjectsDataSource(model);
                break;
            case IDAO.SINGLE_EXTRACTS_DATASOUCE_IDX:
                prepareSingleExtractsDataSource(model);
                break;
            case IDAO.LIST_ORDERS_DATASOUCE_IDX:
                prepareListOrdersDataSource(model);
                break;
            default:
                throw new IllegalStateException();  // Unsupported list type
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareModularProjectsDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", ModularEmployeeExtract.L_EMPLOYEE, model.getEmployee()));
        builder.add(MQExpression.isNull("e", IAbstractExtract.L_PARAGRAPH));
        new OrderDescriptionRegistry("e").applyOrder(builder, model.getModularProjectsModel().getDataSource().getEntityOrder());
        //builder.addOrder("e", IAbstractExtract.P_CREATE_DATE);
        UniBaseUtils.createPage(model.getModularProjectsModel().getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareModularExtractsDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", ModularEmployeeExtract.L_EMPLOYEE, model.getEmployee()));
        builder.add(MQExpression.isNotNull("e", IAbstractExtract.L_PARAGRAPH));
        builder.addOrder("e", IAbstractExtract.P_CREATE_DATE);
        UniBaseUtils.createPage(model.getModularExtractsModel().getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareSingleProjectsDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(SingleEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", SingleEmployeeExtract.L_EMPLOYEE, model.getEmployee()));
        builder.add(MQExpression.in("e", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        new OrderDescriptionRegistry("e").applyOrder(builder, model.getModularProjectsModel().getDataSource().getEntityOrder());
        //builder.addOrder("e", IAbstractExtract.P_CREATE_DATE);
        UniBaseUtils.createPage(model.getSingleProjectsModel().getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareSingleExtractsDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(SingleEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", SingleEmployeeExtract.L_EMPLOYEE, model.getEmployee()));
        builder.add(MQExpression.notIn("e", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        builder.addOrder("e", IAbstractExtract.P_CREATE_DATE);
        UniBaseUtils.createPage(model.getSingleExtractsModel().getDataSource(), builder, getSession());

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getSingleExtractsModel().getDataSource()))
        {
            wrapper.setViewProperty(IDAO.P_PARAGRAPH_NUMBER, "");
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListOrdersDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(ListEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", ListEmployeeExtract.L_EMPLOYEE, model.getEmployee()));
        builder.addOrder("e", IAbstractExtract.P_CREATE_DATE);
        UniBaseUtils.createPage(model.getListOrdersModel().getDataSource(), builder, getSession());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareOrdersDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "e");
        builder.addJoin("e", IAbstractExtract.L_STATE, "state");
        builder.add(MQExpression.eq("state", ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        builder.add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + EmployeePost.L_EMPLOYEE, model.getEmployee()));
        //builder.addOrder("e", IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);

        new OrderDescriptionRegistry("e").applyOrder(builder, model.getOrdersModel().getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getOrdersModel().getDataSource(), builder, getSession());

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getOrdersModel().getDataSource()))
        {
            if (wrapper.getEntity() instanceof ModularEmployeeExtract)
            {
                IAbstractParagraph paragraph = ((IAbstractExtract) wrapper.getEntity()).getParagraph();

                wrapper.setViewProperty(IDAO.P_PARAGRAPH_NUMBER, "");
                wrapper.setViewProperty(IDAO.P_EXTRACT_NUMBER, paragraph == null ? "" : paragraph.getNumber());
            } else
            {
                IAbstractExtract extract = (IAbstractExtract) wrapper.getEntity();
                wrapper.setViewProperty(IDAO.P_EXTRACT_NUMBER, extract == null ? "" : (null != extract.getNumber() ? extract.getNumber() : ""));

                if (wrapper.getEntity() instanceof SingleEmployeeExtract)
                    wrapper.setViewProperty(IDAO.P_PARAGRAPH_NUMBER, "");
                else
                {
                    IAbstractParagraph paragraph = extract == null ? null : extract.getParagraph();
                    wrapper.setViewProperty(IDAO.P_PARAGRAPH_NUMBER, paragraph == null ? "" : paragraph.getNumber());
                }
            }
        }
    }

}