/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e26.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.MainRow;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.Row;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 17.02.2012
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<ChangeHolidayExtract, Model> implements IDAO
{
    private static final List<String> EMPLOYEE_POST_STATUSES = new ArrayList<>(Arrays.asList(
            EmployeePostStatusCodes.STATUS_LEAVE_REGULAR,
            EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR,
            EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY,
            EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE
    ));

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected ChangeHolidayExtract createNewInstance()
    {
        return new ChangeHolidayExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (null == model.getExtract().getEntity() && null != model.getEmployeePost())
            model.getExtract().setEntity(model.getEmployeePost());
        else if (null == model.getEmployeePost() || null == model.getEmployeePost().getId())
            model.setEmployeePost(model.getExtract().getEntity());

        Map<String, EmployeePostStatus> postStatusesMap = new HashMap<>();
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE));
        model.setPostStatusesMap(postStatusesMap);

        model.setVacationScheduleModel(new CommonSingleSelectModel(VacationSchedule.P_TITLE_WITH_YEAR_AND_CREATE_DATE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (o != null)
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(VacationSchedule.class, "s").column("s");
                    subBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationSchedule.id().fromAlias("s")), o));
                    return new DQLListResultBuilder(subBuilder);
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "si").column(DQLExpressions.property(VacationScheduleItem.vacationSchedule().fromAlias("si")));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("si")), model.getEmployeePost()));
                builder.where(DQLExpressions.ge(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("si")), DQLExpressions.value(Calendar.getInstance().get(Calendar.YEAR))));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().state().code().fromAlias("si")), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
                builder.order(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("si")), OrderDirection.desc);
                builder.predicate(DQLPredicateType.distinct);

                return new DQLListResultBuilder(builder);
            }
        });

        model.setVacationScheduleItemModel(new CommonSingleSelectModel(VacationScheduleItem.P_PLANED_VACATION_TITLE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getVacationSchedule() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().fromAlias("i")), model.getExtract().getVacationSchedule()));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), model.getEmployeePost()));
                if (model.getExtract().getChangedHolidayExtract() != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.id().fromAlias("i")), model.getExtract().getChangedHolidayExtract().getVacationScheduleItem().getId()));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.id().fromAlias("i")), o));
                builder.order(DQLExpressions.property(VacationScheduleItem.planDate().fromAlias("i")), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }
        });

        model.setChangedHolidayExtractModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                // берем приказы, которые уже были изменены
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(ChangeHolidayExtract.class, "c").column(DQLExpressions.property(ChangeHolidayExtract.changedHolidayExtract().id().fromAlias("c")));
                if (model.isEditForm())
                    subBuilder.where(DQLExpressions.ne(DQLExpressions.property(ChangeHolidayExtract.id().fromAlias("c")), DQLExpressions.value(model.getExtract().getId())));
                subBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(ChangeHolidayExtract.entity().fromAlias("c")), model.getEmployeePost()));
//                subBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(ChangeHolidayExtract.committed().fromAlias("c")), true));
                List<Long> excludeExtractIdList = subBuilder.createStatement(getSession()).list();

                //берем id приказов из оснований отпусков
                DQLSelectBuilder hBuilder = new DQLSelectBuilder().fromEntity(EmployeeHoliday.class, "h").column(DQLExpressions.property(EmployeeHoliday.holidayExtract().id().fromAlias("h")));
                hBuilder.where(DQLExpressions.isNotNull(DQLExpressions.property(EmployeeHoliday.holidayExtract().fromAlias("h"))));
                hBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeHoliday.employeePost().fromAlias("h")), model.getEmployeePost()));
                List<Long> includeExtractIdList = hBuilder.createStatement(getSession()).list();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeHolidayExtract.class, "h").column("h");

                if (!StringUtils.isEmpty(filter))
                {
                    String[] filterSplit = filter.split(" ");
                    for (String filt : filterSplit)
                    {
                        try
                        {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                            Calendar dateFirst = GregorianCalendar.getInstance();
                            dateFirst.setTime(dateFormat.parse(filt));
                            Calendar dateSecond = GregorianCalendar.getInstance();
                            dateSecond.setTime(dateFormat.parse(filt));
                            dateSecond.set(Calendar.HOUR_OF_DAY, 23);
                            dateSecond.set(Calendar.MINUTE, 59);

                            //если в строке поиска введена дата,
                            //то проверяем ее на соответствие дате формирования или дате создания приказа,
                            //причем соответствие дате создания проверяем как вхождение в интервал, т.к. дата создания включает в себя время
                            IDQLExpression eq = DQLExpressions.eqValue(DQLExpressions.property(EmployeeHolidayExtract.paragraph().order().commitDate().fromAlias("h")), dateFirst.getTime());
                            IDQLExpression greatOrEq = DQLExpressions.ge(DQLExpressions.property(EmployeeHolidayExtract.paragraph().order().createDate().fromAlias("h")), DQLExpressions.valueTimestamp(dateFirst.getTime()));
                            IDQLExpression lessOrEq = DQLExpressions.le(DQLExpressions.property(EmployeeHolidayExtract.createDate().fromAlias("h")), DQLExpressions.valueTimestamp(dateSecond.getTime()));

                            builder.where(DQLExpressions.or(eq, DQLExpressions.and(greatOrEq, lessOrEq)));
                        }
                        catch (ParseException e)
                        {
                            builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EmployeeHolidayExtract.paragraph().order().number().fromAlias("h"))), DQLExpressions.value(CoreStringUtils.escapeLike(filt))));
                        }
                    }
                }


                builder.where(DQLExpressions.eq(DQLExpressions.property("h", EmployeeHolidayExtract.committed()), DQLExpressions.value(true)));
                builder.where(DQLExpressions.eq(DQLExpressions.property("h", EmployeeHolidayExtract.entity()), DQLExpressions.value(model.getEmployeePost())));
                if (!excludeExtractIdList.isEmpty())//если есть приказы об отпуске, которые уже были изменены, то их вычитаем из выборки
                    builder.where(DQLExpressions.notIn(DQLExpressions.property("h", EmployeeHolidayExtract.id()), excludeExtractIdList));
                //берем только те выписки, которые есть в основаниях приказах, таким образом в выборку попадут только те приказы об отпуске, отпуска которых не изменялись другими приказами
                builder.where(DQLExpressions.in(DQLExpressions.property("h", EmployeeHolidayExtract.id()), includeExtractIdList));
                if (o != null)
                    builder.where(DQLExpressions.eq(DQLExpressions.property("h", EmployeeHolidayExtract.id()), DQLExpressions.commonValue(o)));
                builder.order(DQLExpressions.property("h", EmployeeHolidayExtract.paragraph().order().commitDate()), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EmployeeHolidayExtract extract = (EmployeeHolidayExtract) value;

                StringBuilder resultBuilder = new StringBuilder();
                resultBuilder.append("Приказ №");
                resultBuilder.append(extract.getParagraph().getOrder().getNumber());
                resultBuilder.append(" от ");
                resultBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));
                resultBuilder.append(" ");
                resultBuilder.append(extract.getType().getTitle());

                return resultBuilder.toString();
            }
        });

        model.setHolidayTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(HolidayType.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", HolidayType.P_TITLE, CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", HolidayType.P_ID, o));

                return new SimpleListResultBuilder<>(builder.getResultList(getSession()));
            }
        });

        model.setEmployeePostStatusModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EmployeePostStatus.ENTITY_CLASS, "b");
                builder.add(MQExpression.in("b", EmployeePostStatus.P_CODE, EMPLOYEE_POST_STATUSES));
                if (o != null)
                    builder.add(MQExpression.eq("b", EmployeePostStatus.P_ID, o));

                return new MQListResultBuilder(builder);
            }
        });

        //нужно ли отображать поля связанные с Графиком отпусков
        DQLSelectBuilder vsBuilder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
        vsBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), model.getEmployeePost()));
        vsBuilder.where(DQLExpressions.ge(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("i")), DQLExpressions.value(Calendar.getInstance().get(Calendar.YEAR))));
        vsBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().state().code().fromAlias("i")), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
        model.setShowVacationSchedule(!vsBuilder.createStatement(getSession()).list().isEmpty());

        if (model.isEditForm())
        {
            if (model.getExtract().getVacationScheduleItem() != null || model.getExtract().getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL))
                model.setShowVacationSchedule(true);

            MQBuilder builder = new MQBuilder(HolidayInEmployeeExtractRelation.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", HolidayInEmployeeExtractRelation.L_EMPLOYEE_HOLIDAY_EXTRACT, model.getExtract()));
            builder.addOrder("b", HolidayInEmployeeExtractRelation.P_BEGIN_PERIOD);
            builder.addOrder("b", HolidayInEmployeeExtractRelation.P_START_HOLIDAY);

            List<HolidayInEmployeeExtractRelation> relationList = builder.getResultList(getSession());

            //восстанавливаем список отпусков
            List<MainRow> mainRowList = new ArrayList<>();
            model.setHolidayRelationList(new ArrayList<>());
            for (HolidayInEmployeeExtractRelation relation : relationList)
            {
                MainRow mainRow = new MainRow(relation.getMainRowId(), null, null);
                if (!mainRowList.contains(mainRow))
                {
                    List<Row> rowList = new ArrayList<>();
                    Row row = new Row(relation.getRowId(), mainRow, relation.getRowType());

                    model.getBeginPeriodMap().put(row, relation.getBeginPeriod());
                    model.getEndPeriodMap().put(row, relation.getEndPeriod());
                    model.getStartHolidayMap().put(row, relation.getStartHoliday());
                    model.getFinishHolidayMap().put(row, relation.getFinishHoliday());
                    model.getDurationHolidayMap().put(row, relation.getDurationHoliday());

                    rowList.add(row);
                    mainRow.setRowList(rowList);
                    mainRow.setType(relation.getRowType());
                    mainRowList.add(mainRow);
                }
                else
                {
                    MainRow mainRow1 = mainRowList.get(mainRowList.indexOf(mainRow));
                    mainRow1.setType(relation.getRowType());
                    Row row = new Row(relation.getRowId(), mainRow1, relation.getRowType());

                    model.getBeginPeriodMap().put(row, relation.getBeginPeriod());
                    model.getEndPeriodMap().put(row, relation.getEndPeriod());
                    model.getStartHolidayMap().put(row, relation.getStartHoliday());
                    model.getFinishHolidayMap().put(row, relation.getFinishHoliday());
                    model.getDurationHolidayMap().put(row, relation.getDurationHoliday());

                    mainRow1.getRowList().add(row);
                }

                model.getHolidayRelationList().add(relation);
            }

            model.setMainRowList(mainRowList);

            if (model.getExtract().getChangedHolidayExtract().getVacationScheduleItem() != null)
            {
                model.setVacationScheduleStr(model.getExtract().getChangedHolidayExtract().getVacationSchedule().getSimpleTitleWithYear());
                model.setVacationScheduleItemStr(model.getExtract().getChangedHolidayExtract().getVacationScheduleItem().getPlanedVacationTitle());
            }
        }

        if (model.getMainRowList().isEmpty())
        {
            List<Row> rowList = new ArrayList<>();
            MainRow mainRow = new MainRow(MainRow.getNewMainRowId(model.getMainRowList()), rowList, Model.FULL_HOLIDAY);
            rowList.add(new Row(Row.getNewRowId(model.getMainRowList()), mainRow, Model.FULL_HOLIDAY));
            model.getMainRowList().add(mainRow);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        //проверяем, что изменилось значение в блоке «Данные проекта приказа» хотя бы одного из полей
        changedExtractValidate(model, errors);

        //производим стандартные проверки приказа об откуске
        if (model.getExtract().getEntity().getPerson().getIdentityCard().getSex().isMale() && model.getExtract().getHolidayType().getCode().equals(3))
            errors.add("Отпуск по беременности не может быть назначен для сотрудника мужского пола.");

        for (MainRow mainRow : model.getMainRowList())
            for (Row row : mainRow.getRowList())
            {
                Date beginPeriod = model.getBeginPeriodMap().get(row);
                Date endPeriod = model.getEndPeriodMap().get(row);
                Date startHoliday = model.getStartHolidayMap().get(row);
                Date finishHoliday = model.getFinishHolidayMap().get(row);

                //если назначается отпуск за несколько периодов, то поля периода обязательны
                if (row.getType().equals(Model.PERIODS_OF_HOLIDAY) || row.getMainRow().getType().equals(Model.PERIODS_OF_HOLIDAY))
                    if (beginPeriod == null || endPeriod == null)
                        errors.add("Поля периода отпуска обязательны для заполнения.", row.getId() + "_beginPeriod", row.getId() + "_endPeriod");

                //проверяем, что Дата начала отпуска не больше Даты окончания отпуска
                if (startHoliday != null && finishHoliday != null && startHoliday.getTime() > finishHoliday.getTime())
                    errors.add("Дата окончания отпуска должна быть больше даты начала.", row.getId() + "_startHoliday", row.getId() + "_finishHoliday");

                //проверяем корректность заполненности Дат периода
                if (model.getExtract().getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL) &&
                        !row.getType().equals(Model.HOLIDAYS_OF_PERIOD))
                {
                    if (beginPeriod == null || endPeriod == null)
                        errors.add("Поля периода отпуска обязательны для заполнения.", row.getId() + "_beginPeriod", row.getId() + "_endPeriod");
                }
                else if ((beginPeriod == null && endPeriod != null) || (beginPeriod != null && endPeriod == null))
                    errors.add("Поля периода должны быть заполнены, либо оставаться пустыми.", row.getId() + "_beginPeriod", row.getId() + "_endPeriod");

                //проверяем, что Дата начала периода не больше Даны окончания
                if (beginPeriod != null && endPeriod != null && beginPeriod.getTime() > endPeriod.getTime())
                    errors.add("Дата окончания периода должна быть больше даты начала.", row.getId() + "_beginPeriod", row.getId() + "_endPeriod");

                //проверяем, что Даты отпусков не пересекаются
                for (MainRow mainRow2 : model.getMainRowList())
                    for (Row row2 : mainRow2.getRowList())
                    {
                        if (row.equals(row2))
                            continue;

                        Date startFirst = model.getStartHolidayMap().get(row);
                        Date finishFirst = model.getFinishHolidayMap().get(row);
                        Date startSecond = model.getStartHolidayMap().get(row2);
                        Date finishSecond = model.getFinishHolidayMap().get(row2);

                        if (startFirst == null || finishFirst == null || startSecond == null || finishSecond == null)
                            continue;

                        if (CommonBaseDateUtil.isBetween(startFirst, startSecond, finishSecond) || CommonBaseDateUtil.isBetween(finishFirst, startSecond, finishSecond))
                            errors.add("Указанные отпуска пересекаются.",
                                       row.getId() + "_startHoliday", row.getId() + "_finishHoliday", row2.getId() + "_startHoliday", row2.getId() + "_finishHoliday");
                        else
                        {
                            if (CommonBaseDateUtil.isBetween(startSecond, startFirst, finishFirst) || CommonBaseDateUtil.isBetween(finishSecond, startFirst, finishFirst))
                                errors.add("Указанные отпуска пересекаются.",
                                           row.getId() + "_startHoliday", row.getId() + "_finishHoliday", row2.getId() + "_startHoliday", row2.getId() + "_finishHoliday");
                        }
                    }

                //проверяем, что Даты периода отпусков не пересекаются
                for (MainRow mainRow2 : model.getMainRowList())
                    for (Row row2 : mainRow.getRowList())
                    {
                        if (row.equals(row2))
                            continue;

                        Date startFirst = model.getBeginPeriodMap().get(row);
                        Date finishFirst = model.getEndPeriodMap().get(row);
                        Date startSecond = model.getBeginPeriodMap().get(row2);
                        Date finishSecond = model.getEndPeriodMap().get(row2);

                        if (startFirst == null || finishFirst == null || startSecond == null || finishSecond == null)
                            continue;

                        if (CommonBaseDateUtil.isBetween(startFirst, startSecond, finishSecond) || CommonBaseDateUtil.isBetween(finishFirst, startSecond, finishSecond))
                            errors.add("Указанные периоды пересекаются.",
                                       row.getId() + "_beginPeriod", row.getId() + "_endPeriod", row2.getId() + "_beginPeriod", row2.getId() + "_endPeriod");
                        else
                        {
                            if (CommonBaseDateUtil.isBetween(startSecond, startFirst, finishFirst) || CommonBaseDateUtil.isBetween(finishSecond, startFirst, finishFirst))
                                errors.add("Указанные периоды пересекаются.",
                                           row.getId() + "_beginPeriod", row.getId() + "_endPeriod", row2.getId() + "_beginPeriod", row2.getId() + "_endPeriod");
                        }
                    }
            }

        for (MainRow mainRow : model.getMainRowList())
        {
            if (mainRow.getType().equals(Model.PERIODS_OF_HOLIDAY))
            {
                Integer summDur = 0;
                Date start = null;
                Date finish = null;
                String[] fields = new String[mainRow.getRowList().size()];
                int i = 0;
                for (Row row : mainRow.getRowList())
                {
                    if (model.getStartHolidayMap().get(row) != null)
                        start = model.getStartHolidayMap().get(row);
                    if (model.getFinishHolidayMap().get(row) != null)
                        finish = model.getFinishHolidayMap().get(row);

                    summDur += model.getDurationHolidayMap().get(row) != null ? model.getDurationHolidayMap().get(row) : 0;

                    fields[i++] = row.getId() + "_durationHoliday";
                }

                Long diff = finish.getTime() - start.getTime();
                diff = ((((diff / 1000) / 60) / 60) / 24) + 1;
                if (summDur > diff.intValue())
                    errors.add("Длительность указанная для отпуска с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(start) +
                                       " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(finish) + " превышена.", fields);
            }
        }


        MQBuilder builder = new MQBuilder(EmployeeHolidayToEmpHldyExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", EmployeeHolidayToEmpHldyExtractRelation.L_EMPLOYEE_HOLIDAY_EXTRACT, model.getExtract().getChangedHolidayExtract()));

        List<EmployeeHoliday> excludeHolidayList = new ArrayList<>();
        for (EmployeeHolidayToEmpHldyExtractRelation relation : builder.<EmployeeHolidayToEmpHldyExtractRelation>getResultList(getSession()))
            excludeHolidayList.add(relation.getEmployeeHoliday());

        MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
        empHolidayBuilder.add(MQExpression.eq("eh", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
        empHolidayBuilder.add(MQExpression.notIn("eh", EmployeeHoliday.id(), ids(excludeHolidayList)));
        for (EmployeeHoliday holiday : empHolidayBuilder.<EmployeeHoliday>getResultList(getSession()))
            for (MainRow mainRow : model.getMainRowList())
                for (Row row : mainRow.getRowList())
                {
                    Date beginDate = model.getStartHolidayMap().get(row);
                    Date endDate = model.getFinishHolidayMap().get(row);

                    if (beginDate == null || endDate == null)
                        continue;

                    if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                        errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                   row.getId() + "_startHoliday", row.getId() + "_finishHoliday");
                    else
                    {
                        if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                            errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                       row.getId() + "_startHoliday", row.getId() + "_finishHoliday");
                    }
                }

        if (model.getExtract().getVacationSchedule() != null && model.getExtract().getVacationScheduleItem() == null)
            errors.add("Поле «Планируемый отпуск» обязательно для заполнения, если указан график отпусков.", "vacationScheduleItem", "vacationSchedule");

        //если выбран график отпусков
        //проверяем пересечение планируемых отпусков внутри выбранного Графика отпусков
        if (model.getExtract().getVacationScheduleItem() != null)
        {
            DQLSelectBuilder vsItemBuilder = new DQLSelectBuilder().fromEntity(VacationScheduleItemToEmployeeExtractRelation.class, "vs").column(DQLExpressions.property(VacationScheduleItemToEmployeeExtractRelation.vacationScheduleItem().id().fromAlias("vs")));
            vsItemBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItemToEmployeeExtractRelation.abstractEmployeeExtract().fromAlias("vs")), model.getExtract().getChangedHolidayExtract()));

            List<Long> vsItemIdList = vsItemBuilder.createStatement(getSession()).list();

            DQLSelectBuilder vsBuilder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
            vsBuilder.where(DQLExpressions.ne(DQLExpressions.property(VacationScheduleItem.id().fromAlias("i")), DQLExpressions.value(model.getExtract().getVacationScheduleItem().getId())));
            vsBuilder.where(DQLExpressions.notIn(DQLExpressions.property(VacationScheduleItem.id().fromAlias("i")), vsItemIdList));
            vsBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), model.getEmployeePost()));
            vsBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().fromAlias("i")), model.getExtract().getVacationSchedule()));

            for (VacationScheduleItem item : vsBuilder.createStatement(getSession()).<VacationScheduleItem>list())
                for (MainRow mainRow : model.getMainRowList())
                    for (Row row : mainRow.getRowList())
                    {
                        Date beginDate = model.getStartHolidayMap().get(row);
                        Date endDate = model.getFinishHolidayMap().get(row);

                        Date beginItemDate = item.getFactDate() != null ? item.getFactDate() : item.getPlanDate();
                        Date endItemDate = UniempUtil.getEndDate(model.getEmployeePost().getWorkWeekDuration(), item.getDaysAmount(), beginItemDate);

                        if (beginItemDate == null || endItemDate == null || beginDate == null || endDate == null)
                            continue;

                        StringBuilder error = new StringBuilder("Предоставляемый отпуск пересекается с планируемым отпуском с ").
                                append(DateFormatter.DEFAULT_DATE_FORMATTER.format(beginItemDate)).
                                append(" на ").
                                append(NumberConvertingUtil.getCalendarDaysWithName(item.getDaysAmount())).
                                append(" из графика отпусков на ").
                                append(item.getVacationSchedule().getYear()).
                                append(" год от ").
                                append(DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getVacationSchedule().getCreateDate()));
                        error.append(".");

                        if (CommonBaseDateUtil.isBetween(beginDate, beginItemDate, endItemDate) || CommonBaseDateUtil.isBetween(endDate, beginItemDate, endItemDate))
                            errors.add(error.toString(), row.getId() + "_startHoliday", row.getId() + "_finishHoliday");
                        else
                        {
                            if (CommonBaseDateUtil.isBetween(beginItemDate, beginDate, endDate) || CommonBaseDateUtil.isBetween(endItemDate, beginDate, endDate))
                                errors.add(error.toString(), row.getId() + "_startHoliday", row.getId() + "_finishHoliday");
                        }
                    }
        }
    }

    public void changedExtractValidate(Model model, ErrorCollector errors)
    {
        ChangeHolidayExtract extract = model.getExtract();
        EmployeeHolidayExtract changedExtract = model.getExtract().getChangedHolidayExtract();

        List<Object> extractDataList = new ArrayList<>();
        List<Object> changedExtractDataList = new ArrayList<>();

        extractDataList.add(extract.getHolidayType());
        extractDataList.add(extract.getEmployeePostStatus());
        extractDataList.add(extract.getOptionalCondition());
        extractDataList.add(extract.getTotalDuration());
        extractDataList.add(extract.getVacationSchedule());
        extractDataList.add(extract.getVacationScheduleItem());

        changedExtractDataList.add(changedExtract.getHolidayType());
        changedExtractDataList.add(changedExtract.getEmployeePostStatus());
        changedExtractDataList.add(changedExtract.getOptionalCondition());
        changedExtractDataList.add(changedExtract.getTotalDuration());
        changedExtractDataList.add(changedExtract.getVacationSchedule());
        changedExtractDataList.add(changedExtract.getVacationScheduleItem());

        if (extractDataList.equals(changedExtractDataList))
        {
            model.setItemList(new ArrayList<>());
            for (MainRow mainRow : model.getMainRowList())
                for (Row row : mainRow.getRowList())
                    model.getItemList().add(new Item(model.getBeginPeriodMap().get(row), model.getEndPeriodMap().get(row), model.getStartHolidayMap().get(row), model.getFinishHolidayMap().get(row), model.getDurationHolidayMap().get(row)));

            Collections.sort(model.getItemList(), Item.COMPARATOR);

            if (model.getItemList().equals(model.getChangedItemList()))
                errors.add("Ни одно из значений полей приказа №" + extract.getChangedHolidayExtract().getParagraph().getOrder().getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getChangedHolidayExtract().getParagraph().getOrder().getCommitDate()) + " не было изменено.");
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setOldEmployeePostStatus(model.getExtract().getEntity().getPostStatus());

        super.update(model);

        if (model.isEditForm())
            for (HolidayInEmployeeExtractRelation relation : model.getHolidayRelationList())
                delete(relation);

        for (MainRow mainRow : model.getMainRowList())
            for (Row row : mainRow.getRowList())
            {
                HolidayInEmployeeExtractRelation relation = new HolidayInEmployeeExtractRelation();
                relation.setEmployeeHolidayExtract(model.getExtract());
                relation.setMainRowId(mainRow.getId());
                relation.setRowId(row.getId());
                relation.setRowType(row.getType());

                relation.setBeginPeriod(model.getBeginPeriodMap().get(row));
                relation.setEndPeriod(model.getEndPeriodMap().get(row));
                relation.setStartHoliday(model.getStartHolidayMap().get(row));
                relation.setFinishHoliday(model.getFinishHolidayMap().get(row));
                relation.setDurationHoliday(model.getDurationHolidayMap().get(row));

                save(relation);
            }
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }

    @Override
    public void prepareHolidayDataSource(Model model)
    {
        model.getHolidayDataSource().setCountRow(model.getMainRowList().size());
        UniBaseUtils.createPage(model.getHolidayDataSource(), model.getMainRowList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareHolidayDataSourceChanged(Model model)
    {
        if (model.isEditForm())
            getHolidayDataSourceChangedData(model);

        model.getHolidayDataSourceChanged().setCountRow(model.getMainRowList().size());

        Map<IdentifiableWrapper, MainRow> wrapperMainRowMap = new HashMap<>();
        for (MainRow mainRow : model.getMainRowChangedList())
        {
            IdentifiableWrapper wrapper = new IdentifiableWrapper(mainRow.getId(), "");
            wrapperMainRowMap.put(wrapper, mainRow);
        }

        List<IdentifiableWrapper> wrapperList = new ArrayList<>();
        for (Map.Entry<IdentifiableWrapper, MainRow> entry : wrapperMainRowMap.entrySet())
            wrapperList.add(entry.getKey());

        UniBaseUtils.createPage(model.getHolidayDataSourceChanged(), wrapperList);

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getHolidayDataSourceChanged()))
        {
            List<IdentifiableWrapper> periodList = new ArrayList<>();
            List<IdentifiableWrapper> holidayList = new ArrayList<>();
            long i = 0;
            List<Row> rowList = wrapperMainRowMap.get((IdentifiableWrapper) wrapper.getEntity()).getRowList();
            for (Row row : rowList)
            {
                i++;
                if (model.getBeginPeriodChangedMap().get(row) != null && model.getEndPeriodChangedMap().get(row) != null)
                    periodList.add(new IdentifiableWrapper(i, DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getBeginPeriodChangedMap().get(row)) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEndPeriodChangedMap().get(row))));

                if (model.getStartHolidayChangedMap().get(row) != null && model.getFinishHolidayChangedMap().get(row) != null)
                    holidayList.add(new IdentifiableWrapper(i, DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getStartHolidayChangedMap().get(row)) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFinishHolidayChangedMap().get(row)) + ", " + NumberConvertingUtil.getCalendarDaysWithName(model.getDurationHolidayChangedMap().get(row))));
                else
                    holidayList.add(new IdentifiableWrapper(i, NumberConvertingUtil.getCalendarDaysWithName(model.getDurationHolidayChangedMap().get(row))));
            }
            wrapper.setViewProperty(Model.PERIOD, periodList);
            wrapper.setViewProperty(Model.HOLIDAY, holidayList);
        }
    }

    public void getHolidayDataSourceChangedData(Model model)
    {
        MQBuilder builder = new MQBuilder(HolidayInEmpHldyExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", HolidayInEmpHldyExtractRelation.L_EMPLOYEE_HOLIDAY_EXTRACT, model.getExtract().getChangedHolidayExtract()));
        builder.addOrder("b", HolidayInEmpHldyExtractRelation.P_BEGIN_PERIOD);
        builder.addOrder("b", HolidayInEmpHldyExtractRelation.P_START_HOLIDAY);

        List<HolidayInEmpHldyExtractRelation> relationList = builder.getResultList(getSession());

        //восстанавливаем список отпусков
        List<MainRow> mainRowList = new ArrayList<>();
        Map<Row, Date> beginPeriodMap = new HashMap<>();
        Map<Row, Date> endPeriodMap = new HashMap<>();
        Map<Row, Date> startHolidayMap = new HashMap<>();
        Map<Row, Date> finishHolidayMap = new HashMap<>();
        Map<Row, Integer> durationHolidayMap = new HashMap<>();
        for (HolidayInEmpHldyExtractRelation relation : relationList)
        {
            MainRow mainRow = new MainRow(relation.getMainRowId(), null, null);
            if (!mainRowList.contains(mainRow))
            {
                List<Row> rowList = new ArrayList<>();
                Row row = new Row(relation.getRowId(), mainRow, relation.getRowType());

                beginPeriodMap.put(row, relation.getBeginPeriod());
                endPeriodMap.put(row, relation.getEndPeriod());
                startHolidayMap.put(row, relation.getStartHoliday());
                finishHolidayMap.put(row, relation.getFinishHoliday());
                durationHolidayMap.put(row, relation.getDurationHoliday());

                rowList.add(row);
                mainRow.setRowList(rowList);
                mainRow.setType(relation.getRowType());
                mainRowList.add(mainRow);
            }
            else
            {
                MainRow mainRow1 = mainRowList.get(mainRowList.indexOf(mainRow));
                mainRow1.setType(relation.getRowType());
                Row row = new Row(relation.getRowId(), mainRow1, relation.getRowType());

                beginPeriodMap.put(row, relation.getBeginPeriod());
                endPeriodMap.put(row, relation.getEndPeriod());
                startHolidayMap.put(row, relation.getStartHoliday());
                finishHolidayMap.put(row, relation.getFinishHoliday());
                durationHolidayMap.put(row, relation.getDurationHoliday());

                mainRow1.getRowList().add(row);
            }
        }

        model.setChangedItemList(new ArrayList<>());
        for (MainRow mainRow : mainRowList)
            for (Row row : mainRow.getRowList())
                model.getChangedItemList().add(new Item(beginPeriodMap.get(row), endPeriodMap.get(row), startHolidayMap.get(row), finishHolidayMap.get(row), durationHolidayMap.get(row)));

        Collections.sort(model.getChangedItemList(), Item.COMPARATOR);

        model.setMainRowChangedList(mainRowList);
        model.setBeginPeriodChangedMap(beginPeriodMap);
        model.setEndPeriodChangedMap(endPeriodMap);
        model.setStartHolidayChangedMap(startHolidayMap);
        model.setFinishHolidayChangedMap(finishHolidayMap);
        model.setDurationHolidayChangedMap(durationHolidayMap);
    }

    @Override
    public boolean hasVacationSchedule(EmployeePost employeePost)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), employeePost));
        builder.where(DQLExpressions.ge(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("i")), DQLExpressions.value(Calendar.getInstance().get(Calendar.YEAR))));
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().state().code().fromAlias("i")), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
        return !builder.createStatement(getSession()).list().isEmpty();
    }
}