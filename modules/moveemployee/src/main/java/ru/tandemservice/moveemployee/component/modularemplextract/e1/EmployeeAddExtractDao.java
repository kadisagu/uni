/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e1;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeAddExtract;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 20.11.2008
 */
public class EmployeeAddExtractDao extends UniDao implements IExtractComponentDao<EmployeeAddExtract>
{
    @Override
    public void doCommit(EmployeeAddExtract extract, Map parameters)
    {
        EmployeePost post = CommonExtractCommitUtil.createOrAssignEmployeePost(getSession(), extract);

        List<EmployeePostStaffRateItem> staffRateItemList = CommonExtractCommitUtil.createEmployeePostStaffRateItems(extract, post);

        EmployeeLabourContract contract = CommonExtractCommitUtil.createOrAssignLabourContract(extract, post);

        EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), post, staffRateItemList);
        // TODO: employment periods

        List<StaffListAllocationItem> allocItemList = createStaffListAllocationItem(extract, post);

        validate(extract, post, staffRateItemList);

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        getSession().saveOrUpdate(post);

        MoveEmployeeDaoFacade.getMoveEmployeeDao().updateStaffListAllocItemChanges(extract, post, allocItemList);

        for (EmployeePostStaffRateItem oldStaffRateItem : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(post))
            delete(oldStaffRateItem);
        getSession().flush();
        for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
            save(staffRateItem);

        getSession().saveOrUpdate(contract);

        getSession().saveOrUpdate(empHistItem);

        Double salary = 0d;
        if (extract.getRaisingCoefficient() != null)
            salary = extract.getRaisingCoefficient().getRecommendedSalary();
        else if (extract.getEntity() != null && extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary() != null)
            salary = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary();
        else if (extract.getPostBoundedWithQGandQL().getSalary() != null)
            salary = extract.getPostBoundedWithQGandQL().getSalary();

        List<EmployeePayment> paymentList = CommonExtractCommitUtil.createOrAssignEmployeePaymentsList(getSession(), extract, post, salary, staffRateItemList);

        CommonExtractCommitUtil.validateEmployeePayment(extract.getEntity(), paymentList, getSession());

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;

        for (EmployeePayment payment : paymentList)
            getSession().saveOrUpdate(payment);

        if (null == extract.getEntity())
        {
            extract.setEntity(post);
            update(extract);
        }
    }

    @Override
    public void doRollback(EmployeeAddExtract extract, Map parameters)
    {
        extract.getEntity().setPostStatus(getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_POSSIBLE));
        update(extract.getEntity());

        EmploymentHistoryItemInner empHistItem = CommonExtractCommitUtil.createOrAssignEmployementHistoryItem(extract, getSession(), extract.getEntity(), UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity()));
        if (null != empHistItem.getId()) delete(empHistItem);

        MoveEmployeeDaoFacade.getMoveEmployeeDao().rollbackStaffListAllocItemChanges(extract);
    }

    /*выносим в отдельные методы для возможности переопределения действий в проектом слое*/

    protected List<StaffListAllocationItem> createStaffListAllocationItem(AbstractEmployeeExtract extract, EmployeePost post)
    {
        return CommonExtractCommitUtil.createStaffListAllocationItem(extract, post);
    }

    protected void validate(EmployeeAddExtract extract, EmployeePost post, List<EmployeePostStaffRateItem> staffRateItemList)
    {
        CommonExtractCommitUtil.validateStaffRates(extract, post, staffRateItemList);

        CommonExtractCommitUtil.validateAllocationItem(extract, post.getOrgUnit(), post.getPostRelation().getPostBoundedWithQGandQL());
    }
}