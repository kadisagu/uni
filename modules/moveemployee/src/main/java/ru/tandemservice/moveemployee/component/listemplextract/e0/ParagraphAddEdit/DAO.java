/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e0.ParagraphAddEdit;

import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setOrder((ParagraphEmployeeListOrder) getNotNull(model.getOrderId()));

        model.setExtractTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EmployeeExtractType.ENTITY_CLASS, "et");
                builder.add(MQExpression.eq("et", EmployeeExtractType.L_PARENT, model.getOrder().getType()));
                builder.addOrder("et", EmployeeExtractType.title());
                if (o != null)
                    builder.add(MQExpression.eq("et", EmployeeExtractType.id(), o));

                return new MQListResultBuilder(builder);
            }
        });


    }
}
