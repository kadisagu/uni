/**
 *$Id:$
 */
package ru.tandemservice.moveemployee.base.bo.MoveEmployeeSettingsPaymentStringFormatDocument.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.moveemployee.base.bo.MoveEmployeeSettingsPaymentStringFormatDocument.MoveEmployeeSettingsPaymentStringFormatDocumentManager;
import ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings;

/**
 * @author Alexander Shaburov
 * @since 19.10.12
 */
public class MoveEmployeeSettingsPaymentStringFormatDocumentListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        MoveEmployeeSettingsPaymentStringFormatDocumentManager.instance().dao().doUpdateSettingsEntity();
    }

    public void onChangeActive()
    {
        MoveEmployeeSettingsPaymentStringFormatDocumentManager.instance().dao().doChangeActive(DataAccessServices.dao().<PaymentStringFormatDocumentSettings>get(getListenerParameterAsLong()));
    }
}
