package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из списочного приказа по кадровому составу. О предоставлении отпуска работникам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeHolidayEmplListExtractGen extends ListEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract";
    public static final String ENTITY_NAME = "employeeHolidayEmplListExtract";
    public static final int VERSION_HASH = -1557311142;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_HOLIDAY_TYPE = "holidayType";
    public static final String P_PERIOD_BEGIN_DATE = "periodBeginDate";
    public static final String P_PERIOD_END_DATE = "periodEndDate";
    public static final String P_MAIN_HOLIDAY_BEGIN_DATE = "mainHolidayBeginDate";
    public static final String P_MAIN_HOLIDAY_END_DATE = "mainHolidayEndDate";
    public static final String P_MAIN_HOLIDAY_DURATION = "mainHolidayDuration";
    public static final String P_SECOND_HOLIDAY_BEGIN_DATE = "secondHolidayBeginDate";
    public static final String P_SECOND_HOLIDAY_END_DATE = "secondHolidayEndDate";
    public static final String P_SECOND_HOLIDAY_DURATION = "secondHolidayDuration";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_DURATION = "duration";
    public static final String L_EMPLOYEE_FIRST_HOLIDAY = "employeeFirstHoliday";
    public static final String L_EMPLOYEE_SECOND_HOLIDAY = "employeeSecondHoliday";
    public static final String P_OPTIONAL_CONDITION = "optionalCondition";
    public static final String L_VACATION_SCHEDULE = "vacationSchedule";
    public static final String L_VACATION_SCHEDULE_ITEM = "vacationScheduleItem";
    public static final String P_OLD_SCHEDULE_ITEM_FACT_DATE = "oldScheduleItemFactDate";
    public static final String P_OLD_SCHEDULE_ITEM_POSTPONE_DATE = "oldScheduleItemPostponeDate";
    public static final String P_OLD_SCHEDULE_ITEM_POSTPONE_BASIC = "oldScheduleItemPostponeBasic";
    public static final String L_EMPLOYEE_POST_STATUS_NEW = "employeePostStatusNew";
    public static final String L_EMPLOYEE_POST_STATUS_OLD = "employeePostStatusOld";

    private EmployeePost _employeePost;     // Сотрудник
    private HolidayType _holidayType;     // Вид отпуска
    private Date _periodBeginDate;     // Дата начала периода
    private Date _periodEndDate;     // Дата окончания периода
    private Date _mainHolidayBeginDate;     // Дата начала основного отпуска
    private Date _mainHolidayEndDate;     // Дата окончания основного отпуска
    private Integer _mainHolidayDuration;     // Длительность основного отпуска, календарных дней
    private Date _secondHolidayBeginDate;     // Дата начала дополнительного отпуска
    private Date _secondHolidayEndDate;     // Дата окончания дополнительного отпуска
    private Integer _secondHolidayDuration;     // Длительность дополнительного отпуска, календарных дней
    private Date _beginDate;     // Дата начала отпуска
    private Date _endDate;     // Дата окончания отпуска
    private int _duration;     // Длительность отпуска, календарных дней
    private EmployeeHoliday _employeeFirstHoliday;     // Отпуск сотрудника
    private EmployeeHoliday _employeeSecondHoliday;     // Отпуск сотрудника
    private String _optionalCondition;     // Произвольная строка условий отпуска
    private VacationSchedule _vacationSchedule;     // График отпусков
    private VacationScheduleItem _vacationScheduleItem;     // Строка графика отпусков
    private Date _oldScheduleItemFactDate;     // Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа
    private Date _oldScheduleItemPostponeDate;     // Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа
    private String _oldScheduleItemPostponeBasic;     // Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа
    private EmployeePostStatus _employeePostStatusNew;     // Статус на должности при уходе в отпуск
    private EmployeePostStatus _employeePostStatusOld;     // Статус на должности до ухода в отпуск

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Вид отпуска. Свойство не может быть null.
     */
    @NotNull
    public HolidayType getHolidayType()
    {
        return _holidayType;
    }

    /**
     * @param holidayType Вид отпуска. Свойство не может быть null.
     */
    public void setHolidayType(HolidayType holidayType)
    {
        dirty(_holidayType, holidayType);
        _holidayType = holidayType;
    }

    /**
     * @return Дата начала периода.
     */
    public Date getPeriodBeginDate()
    {
        return _periodBeginDate;
    }

    /**
     * @param periodBeginDate Дата начала периода.
     */
    public void setPeriodBeginDate(Date periodBeginDate)
    {
        dirty(_periodBeginDate, periodBeginDate);
        _periodBeginDate = periodBeginDate;
    }

    /**
     * @return Дата окончания периода.
     */
    public Date getPeriodEndDate()
    {
        return _periodEndDate;
    }

    /**
     * @param periodEndDate Дата окончания периода.
     */
    public void setPeriodEndDate(Date periodEndDate)
    {
        dirty(_periodEndDate, periodEndDate);
        _periodEndDate = periodEndDate;
    }

    /**
     * @return Дата начала основного отпуска.
     */
    public Date getMainHolidayBeginDate()
    {
        return _mainHolidayBeginDate;
    }

    /**
     * @param mainHolidayBeginDate Дата начала основного отпуска.
     */
    public void setMainHolidayBeginDate(Date mainHolidayBeginDate)
    {
        dirty(_mainHolidayBeginDate, mainHolidayBeginDate);
        _mainHolidayBeginDate = mainHolidayBeginDate;
    }

    /**
     * @return Дата окончания основного отпуска.
     */
    public Date getMainHolidayEndDate()
    {
        return _mainHolidayEndDate;
    }

    /**
     * @param mainHolidayEndDate Дата окончания основного отпуска.
     */
    public void setMainHolidayEndDate(Date mainHolidayEndDate)
    {
        dirty(_mainHolidayEndDate, mainHolidayEndDate);
        _mainHolidayEndDate = mainHolidayEndDate;
    }

    /**
     * @return Длительность основного отпуска, календарных дней.
     */
    public Integer getMainHolidayDuration()
    {
        return _mainHolidayDuration;
    }

    /**
     * @param mainHolidayDuration Длительность основного отпуска, календарных дней.
     */
    public void setMainHolidayDuration(Integer mainHolidayDuration)
    {
        dirty(_mainHolidayDuration, mainHolidayDuration);
        _mainHolidayDuration = mainHolidayDuration;
    }

    /**
     * @return Дата начала дополнительного отпуска.
     */
    public Date getSecondHolidayBeginDate()
    {
        return _secondHolidayBeginDate;
    }

    /**
     * @param secondHolidayBeginDate Дата начала дополнительного отпуска.
     */
    public void setSecondHolidayBeginDate(Date secondHolidayBeginDate)
    {
        dirty(_secondHolidayBeginDate, secondHolidayBeginDate);
        _secondHolidayBeginDate = secondHolidayBeginDate;
    }

    /**
     * @return Дата окончания дополнительного отпуска.
     */
    public Date getSecondHolidayEndDate()
    {
        return _secondHolidayEndDate;
    }

    /**
     * @param secondHolidayEndDate Дата окончания дополнительного отпуска.
     */
    public void setSecondHolidayEndDate(Date secondHolidayEndDate)
    {
        dirty(_secondHolidayEndDate, secondHolidayEndDate);
        _secondHolidayEndDate = secondHolidayEndDate;
    }

    /**
     * @return Длительность дополнительного отпуска, календарных дней.
     */
    public Integer getSecondHolidayDuration()
    {
        return _secondHolidayDuration;
    }

    /**
     * @param secondHolidayDuration Длительность дополнительного отпуска, календарных дней.
     */
    public void setSecondHolidayDuration(Integer secondHolidayDuration)
    {
        dirty(_secondHolidayDuration, secondHolidayDuration);
        _secondHolidayDuration = secondHolidayDuration;
    }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала отпуска. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания отпуска. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Длительность отпуска, календарных дней. Свойство не может быть null.
     */
    @NotNull
    public int getDuration()
    {
        return _duration;
    }

    /**
     * @param duration Длительность отпуска, календарных дней. Свойство не может быть null.
     */
    public void setDuration(int duration)
    {
        dirty(_duration, duration);
        _duration = duration;
    }

    /**
     * @return Отпуск сотрудника.
     */
    public EmployeeHoliday getEmployeeFirstHoliday()
    {
        return _employeeFirstHoliday;
    }

    /**
     * @param employeeFirstHoliday Отпуск сотрудника.
     */
    public void setEmployeeFirstHoliday(EmployeeHoliday employeeFirstHoliday)
    {
        dirty(_employeeFirstHoliday, employeeFirstHoliday);
        _employeeFirstHoliday = employeeFirstHoliday;
    }

    /**
     * @return Отпуск сотрудника.
     */
    public EmployeeHoliday getEmployeeSecondHoliday()
    {
        return _employeeSecondHoliday;
    }

    /**
     * @param employeeSecondHoliday Отпуск сотрудника.
     */
    public void setEmployeeSecondHoliday(EmployeeHoliday employeeSecondHoliday)
    {
        dirty(_employeeSecondHoliday, employeeSecondHoliday);
        _employeeSecondHoliday = employeeSecondHoliday;
    }

    /**
     * @return Произвольная строка условий отпуска.
     */
    public String getOptionalCondition()
    {
        return _optionalCondition;
    }

    /**
     * @param optionalCondition Произвольная строка условий отпуска.
     */
    public void setOptionalCondition(String optionalCondition)
    {
        dirty(_optionalCondition, optionalCondition);
        _optionalCondition = optionalCondition;
    }

    /**
     * @return График отпусков.
     */
    public VacationSchedule getVacationSchedule()
    {
        return _vacationSchedule;
    }

    /**
     * @param vacationSchedule График отпусков.
     */
    public void setVacationSchedule(VacationSchedule vacationSchedule)
    {
        dirty(_vacationSchedule, vacationSchedule);
        _vacationSchedule = vacationSchedule;
    }

    /**
     * @return Строка графика отпусков.
     */
    public VacationScheduleItem getVacationScheduleItem()
    {
        return _vacationScheduleItem;
    }

    /**
     * @param vacationScheduleItem Строка графика отпусков.
     */
    public void setVacationScheduleItem(VacationScheduleItem vacationScheduleItem)
    {
        dirty(_vacationScheduleItem, vacationScheduleItem);
        _vacationScheduleItem = vacationScheduleItem;
    }

    /**
     * @return Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа.
     */
    public Date getOldScheduleItemFactDate()
    {
        return _oldScheduleItemFactDate;
    }

    /**
     * @param oldScheduleItemFactDate Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemFactDate(Date oldScheduleItemFactDate)
    {
        dirty(_oldScheduleItemFactDate, oldScheduleItemFactDate);
        _oldScheduleItemFactDate = oldScheduleItemFactDate;
    }

    /**
     * @return Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public Date getOldScheduleItemPostponeDate()
    {
        return _oldScheduleItemPostponeDate;
    }

    /**
     * @param oldScheduleItemPostponeDate Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemPostponeDate(Date oldScheduleItemPostponeDate)
    {
        dirty(_oldScheduleItemPostponeDate, oldScheduleItemPostponeDate);
        _oldScheduleItemPostponeDate = oldScheduleItemPostponeDate;
    }

    /**
     * @return Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    @Length(max=255)
    public String getOldScheduleItemPostponeBasic()
    {
        return _oldScheduleItemPostponeBasic;
    }

    /**
     * @param oldScheduleItemPostponeBasic Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     */
    public void setOldScheduleItemPostponeBasic(String oldScheduleItemPostponeBasic)
    {
        dirty(_oldScheduleItemPostponeBasic, oldScheduleItemPostponeBasic);
        _oldScheduleItemPostponeBasic = oldScheduleItemPostponeBasic;
    }

    /**
     * @return Статус на должности при уходе в отпуск. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getEmployeePostStatusNew()
    {
        return _employeePostStatusNew;
    }

    /**
     * @param employeePostStatusNew Статус на должности при уходе в отпуск. Свойство не может быть null.
     */
    public void setEmployeePostStatusNew(EmployeePostStatus employeePostStatusNew)
    {
        dirty(_employeePostStatusNew, employeePostStatusNew);
        _employeePostStatusNew = employeePostStatusNew;
    }

    /**
     * @return Статус на должности до ухода в отпуск. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getEmployeePostStatusOld()
    {
        return _employeePostStatusOld;
    }

    /**
     * @param employeePostStatusOld Статус на должности до ухода в отпуск. Свойство не может быть null.
     */
    public void setEmployeePostStatusOld(EmployeePostStatus employeePostStatusOld)
    {
        dirty(_employeePostStatusOld, employeePostStatusOld);
        _employeePostStatusOld = employeePostStatusOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeHolidayEmplListExtractGen)
        {
            setEmployeePost(((EmployeeHolidayEmplListExtract)another).getEmployeePost());
            setHolidayType(((EmployeeHolidayEmplListExtract)another).getHolidayType());
            setPeriodBeginDate(((EmployeeHolidayEmplListExtract)another).getPeriodBeginDate());
            setPeriodEndDate(((EmployeeHolidayEmplListExtract)another).getPeriodEndDate());
            setMainHolidayBeginDate(((EmployeeHolidayEmplListExtract)another).getMainHolidayBeginDate());
            setMainHolidayEndDate(((EmployeeHolidayEmplListExtract)another).getMainHolidayEndDate());
            setMainHolidayDuration(((EmployeeHolidayEmplListExtract)another).getMainHolidayDuration());
            setSecondHolidayBeginDate(((EmployeeHolidayEmplListExtract)another).getSecondHolidayBeginDate());
            setSecondHolidayEndDate(((EmployeeHolidayEmplListExtract)another).getSecondHolidayEndDate());
            setSecondHolidayDuration(((EmployeeHolidayEmplListExtract)another).getSecondHolidayDuration());
            setBeginDate(((EmployeeHolidayEmplListExtract)another).getBeginDate());
            setEndDate(((EmployeeHolidayEmplListExtract)another).getEndDate());
            setDuration(((EmployeeHolidayEmplListExtract)another).getDuration());
            setEmployeeFirstHoliday(((EmployeeHolidayEmplListExtract)another).getEmployeeFirstHoliday());
            setEmployeeSecondHoliday(((EmployeeHolidayEmplListExtract)another).getEmployeeSecondHoliday());
            setOptionalCondition(((EmployeeHolidayEmplListExtract)another).getOptionalCondition());
            setVacationSchedule(((EmployeeHolidayEmplListExtract)another).getVacationSchedule());
            setVacationScheduleItem(((EmployeeHolidayEmplListExtract)another).getVacationScheduleItem());
            setOldScheduleItemFactDate(((EmployeeHolidayEmplListExtract)another).getOldScheduleItemFactDate());
            setOldScheduleItemPostponeDate(((EmployeeHolidayEmplListExtract)another).getOldScheduleItemPostponeDate());
            setOldScheduleItemPostponeBasic(((EmployeeHolidayEmplListExtract)another).getOldScheduleItemPostponeBasic());
            setEmployeePostStatusNew(((EmployeeHolidayEmplListExtract)another).getEmployeePostStatusNew());
            setEmployeePostStatusOld(((EmployeeHolidayEmplListExtract)another).getEmployeePostStatusOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeHolidayEmplListExtractGen> extends ListEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeHolidayEmplListExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeHolidayEmplListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "holidayType":
                    return obj.getHolidayType();
                case "periodBeginDate":
                    return obj.getPeriodBeginDate();
                case "periodEndDate":
                    return obj.getPeriodEndDate();
                case "mainHolidayBeginDate":
                    return obj.getMainHolidayBeginDate();
                case "mainHolidayEndDate":
                    return obj.getMainHolidayEndDate();
                case "mainHolidayDuration":
                    return obj.getMainHolidayDuration();
                case "secondHolidayBeginDate":
                    return obj.getSecondHolidayBeginDate();
                case "secondHolidayEndDate":
                    return obj.getSecondHolidayEndDate();
                case "secondHolidayDuration":
                    return obj.getSecondHolidayDuration();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "duration":
                    return obj.getDuration();
                case "employeeFirstHoliday":
                    return obj.getEmployeeFirstHoliday();
                case "employeeSecondHoliday":
                    return obj.getEmployeeSecondHoliday();
                case "optionalCondition":
                    return obj.getOptionalCondition();
                case "vacationSchedule":
                    return obj.getVacationSchedule();
                case "vacationScheduleItem":
                    return obj.getVacationScheduleItem();
                case "oldScheduleItemFactDate":
                    return obj.getOldScheduleItemFactDate();
                case "oldScheduleItemPostponeDate":
                    return obj.getOldScheduleItemPostponeDate();
                case "oldScheduleItemPostponeBasic":
                    return obj.getOldScheduleItemPostponeBasic();
                case "employeePostStatusNew":
                    return obj.getEmployeePostStatusNew();
                case "employeePostStatusOld":
                    return obj.getEmployeePostStatusOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "holidayType":
                    obj.setHolidayType((HolidayType) value);
                    return;
                case "periodBeginDate":
                    obj.setPeriodBeginDate((Date) value);
                    return;
                case "periodEndDate":
                    obj.setPeriodEndDate((Date) value);
                    return;
                case "mainHolidayBeginDate":
                    obj.setMainHolidayBeginDate((Date) value);
                    return;
                case "mainHolidayEndDate":
                    obj.setMainHolidayEndDate((Date) value);
                    return;
                case "mainHolidayDuration":
                    obj.setMainHolidayDuration((Integer) value);
                    return;
                case "secondHolidayBeginDate":
                    obj.setSecondHolidayBeginDate((Date) value);
                    return;
                case "secondHolidayEndDate":
                    obj.setSecondHolidayEndDate((Date) value);
                    return;
                case "secondHolidayDuration":
                    obj.setSecondHolidayDuration((Integer) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "duration":
                    obj.setDuration((Integer) value);
                    return;
                case "employeeFirstHoliday":
                    obj.setEmployeeFirstHoliday((EmployeeHoliday) value);
                    return;
                case "employeeSecondHoliday":
                    obj.setEmployeeSecondHoliday((EmployeeHoliday) value);
                    return;
                case "optionalCondition":
                    obj.setOptionalCondition((String) value);
                    return;
                case "vacationSchedule":
                    obj.setVacationSchedule((VacationSchedule) value);
                    return;
                case "vacationScheduleItem":
                    obj.setVacationScheduleItem((VacationScheduleItem) value);
                    return;
                case "oldScheduleItemFactDate":
                    obj.setOldScheduleItemFactDate((Date) value);
                    return;
                case "oldScheduleItemPostponeDate":
                    obj.setOldScheduleItemPostponeDate((Date) value);
                    return;
                case "oldScheduleItemPostponeBasic":
                    obj.setOldScheduleItemPostponeBasic((String) value);
                    return;
                case "employeePostStatusNew":
                    obj.setEmployeePostStatusNew((EmployeePostStatus) value);
                    return;
                case "employeePostStatusOld":
                    obj.setEmployeePostStatusOld((EmployeePostStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "holidayType":
                        return true;
                case "periodBeginDate":
                        return true;
                case "periodEndDate":
                        return true;
                case "mainHolidayBeginDate":
                        return true;
                case "mainHolidayEndDate":
                        return true;
                case "mainHolidayDuration":
                        return true;
                case "secondHolidayBeginDate":
                        return true;
                case "secondHolidayEndDate":
                        return true;
                case "secondHolidayDuration":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "duration":
                        return true;
                case "employeeFirstHoliday":
                        return true;
                case "employeeSecondHoliday":
                        return true;
                case "optionalCondition":
                        return true;
                case "vacationSchedule":
                        return true;
                case "vacationScheduleItem":
                        return true;
                case "oldScheduleItemFactDate":
                        return true;
                case "oldScheduleItemPostponeDate":
                        return true;
                case "oldScheduleItemPostponeBasic":
                        return true;
                case "employeePostStatusNew":
                        return true;
                case "employeePostStatusOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "holidayType":
                    return true;
                case "periodBeginDate":
                    return true;
                case "periodEndDate":
                    return true;
                case "mainHolidayBeginDate":
                    return true;
                case "mainHolidayEndDate":
                    return true;
                case "mainHolidayDuration":
                    return true;
                case "secondHolidayBeginDate":
                    return true;
                case "secondHolidayEndDate":
                    return true;
                case "secondHolidayDuration":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "duration":
                    return true;
                case "employeeFirstHoliday":
                    return true;
                case "employeeSecondHoliday":
                    return true;
                case "optionalCondition":
                    return true;
                case "vacationSchedule":
                    return true;
                case "vacationScheduleItem":
                    return true;
                case "oldScheduleItemFactDate":
                    return true;
                case "oldScheduleItemPostponeDate":
                    return true;
                case "oldScheduleItemPostponeBasic":
                    return true;
                case "employeePostStatusNew":
                    return true;
                case "employeePostStatusOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "holidayType":
                    return HolidayType.class;
                case "periodBeginDate":
                    return Date.class;
                case "periodEndDate":
                    return Date.class;
                case "mainHolidayBeginDate":
                    return Date.class;
                case "mainHolidayEndDate":
                    return Date.class;
                case "mainHolidayDuration":
                    return Integer.class;
                case "secondHolidayBeginDate":
                    return Date.class;
                case "secondHolidayEndDate":
                    return Date.class;
                case "secondHolidayDuration":
                    return Integer.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "duration":
                    return Integer.class;
                case "employeeFirstHoliday":
                    return EmployeeHoliday.class;
                case "employeeSecondHoliday":
                    return EmployeeHoliday.class;
                case "optionalCondition":
                    return String.class;
                case "vacationSchedule":
                    return VacationSchedule.class;
                case "vacationScheduleItem":
                    return VacationScheduleItem.class;
                case "oldScheduleItemFactDate":
                    return Date.class;
                case "oldScheduleItemPostponeDate":
                    return Date.class;
                case "oldScheduleItemPostponeBasic":
                    return String.class;
                case "employeePostStatusNew":
                    return EmployeePostStatus.class;
                case "employeePostStatusOld":
                    return EmployeePostStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeHolidayEmplListExtract> _dslPath = new Path<EmployeeHolidayEmplListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeHolidayEmplListExtract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Вид отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getHolidayType()
     */
    public static HolidayType.Path<HolidayType> holidayType()
    {
        return _dslPath.holidayType();
    }

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getPeriodBeginDate()
     */
    public static PropertyPath<Date> periodBeginDate()
    {
        return _dslPath.periodBeginDate();
    }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getPeriodEndDate()
     */
    public static PropertyPath<Date> periodEndDate()
    {
        return _dslPath.periodEndDate();
    }

    /**
     * @return Дата начала основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getMainHolidayBeginDate()
     */
    public static PropertyPath<Date> mainHolidayBeginDate()
    {
        return _dslPath.mainHolidayBeginDate();
    }

    /**
     * @return Дата окончания основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getMainHolidayEndDate()
     */
    public static PropertyPath<Date> mainHolidayEndDate()
    {
        return _dslPath.mainHolidayEndDate();
    }

    /**
     * @return Длительность основного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getMainHolidayDuration()
     */
    public static PropertyPath<Integer> mainHolidayDuration()
    {
        return _dslPath.mainHolidayDuration();
    }

    /**
     * @return Дата начала дополнительного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getSecondHolidayBeginDate()
     */
    public static PropertyPath<Date> secondHolidayBeginDate()
    {
        return _dslPath.secondHolidayBeginDate();
    }

    /**
     * @return Дата окончания дополнительного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getSecondHolidayEndDate()
     */
    public static PropertyPath<Date> secondHolidayEndDate()
    {
        return _dslPath.secondHolidayEndDate();
    }

    /**
     * @return Длительность дополнительного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getSecondHolidayDuration()
     */
    public static PropertyPath<Integer> secondHolidayDuration()
    {
        return _dslPath.secondHolidayDuration();
    }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Длительность отпуска, календарных дней. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getDuration()
     */
    public static PropertyPath<Integer> duration()
    {
        return _dslPath.duration();
    }

    /**
     * @return Отпуск сотрудника.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeeFirstHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> employeeFirstHoliday()
    {
        return _dslPath.employeeFirstHoliday();
    }

    /**
     * @return Отпуск сотрудника.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeeSecondHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> employeeSecondHoliday()
    {
        return _dslPath.employeeSecondHoliday();
    }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getOptionalCondition()
     */
    public static PropertyPath<String> optionalCondition()
    {
        return _dslPath.optionalCondition();
    }

    /**
     * @return График отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getVacationSchedule()
     */
    public static VacationSchedule.Path<VacationSchedule> vacationSchedule()
    {
        return _dslPath.vacationSchedule();
    }

    /**
     * @return Строка графика отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getVacationScheduleItem()
     */
    public static VacationScheduleItem.Path<VacationScheduleItem> vacationScheduleItem()
    {
        return _dslPath.vacationScheduleItem();
    }

    /**
     * @return Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getOldScheduleItemFactDate()
     */
    public static PropertyPath<Date> oldScheduleItemFactDate()
    {
        return _dslPath.oldScheduleItemFactDate();
    }

    /**
     * @return Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getOldScheduleItemPostponeDate()
     */
    public static PropertyPath<Date> oldScheduleItemPostponeDate()
    {
        return _dslPath.oldScheduleItemPostponeDate();
    }

    /**
     * @return Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getOldScheduleItemPostponeBasic()
     */
    public static PropertyPath<String> oldScheduleItemPostponeBasic()
    {
        return _dslPath.oldScheduleItemPostponeBasic();
    }

    /**
     * @return Статус на должности при уходе в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeePostStatusNew()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusNew()
    {
        return _dslPath.employeePostStatusNew();
    }

    /**
     * @return Статус на должности до ухода в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeePostStatusOld()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusOld()
    {
        return _dslPath.employeePostStatusOld();
    }

    public static class Path<E extends EmployeeHolidayEmplListExtract> extends ListEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private HolidayType.Path<HolidayType> _holidayType;
        private PropertyPath<Date> _periodBeginDate;
        private PropertyPath<Date> _periodEndDate;
        private PropertyPath<Date> _mainHolidayBeginDate;
        private PropertyPath<Date> _mainHolidayEndDate;
        private PropertyPath<Integer> _mainHolidayDuration;
        private PropertyPath<Date> _secondHolidayBeginDate;
        private PropertyPath<Date> _secondHolidayEndDate;
        private PropertyPath<Integer> _secondHolidayDuration;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Integer> _duration;
        private EmployeeHoliday.Path<EmployeeHoliday> _employeeFirstHoliday;
        private EmployeeHoliday.Path<EmployeeHoliday> _employeeSecondHoliday;
        private PropertyPath<String> _optionalCondition;
        private VacationSchedule.Path<VacationSchedule> _vacationSchedule;
        private VacationScheduleItem.Path<VacationScheduleItem> _vacationScheduleItem;
        private PropertyPath<Date> _oldScheduleItemFactDate;
        private PropertyPath<Date> _oldScheduleItemPostponeDate;
        private PropertyPath<String> _oldScheduleItemPostponeBasic;
        private EmployeePostStatus.Path<EmployeePostStatus> _employeePostStatusNew;
        private EmployeePostStatus.Path<EmployeePostStatus> _employeePostStatusOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Вид отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getHolidayType()
     */
        public HolidayType.Path<HolidayType> holidayType()
        {
            if(_holidayType == null )
                _holidayType = new HolidayType.Path<HolidayType>(L_HOLIDAY_TYPE, this);
            return _holidayType;
        }

    /**
     * @return Дата начала периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getPeriodBeginDate()
     */
        public PropertyPath<Date> periodBeginDate()
        {
            if(_periodBeginDate == null )
                _periodBeginDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_PERIOD_BEGIN_DATE, this);
            return _periodBeginDate;
        }

    /**
     * @return Дата окончания периода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getPeriodEndDate()
     */
        public PropertyPath<Date> periodEndDate()
        {
            if(_periodEndDate == null )
                _periodEndDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_PERIOD_END_DATE, this);
            return _periodEndDate;
        }

    /**
     * @return Дата начала основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getMainHolidayBeginDate()
     */
        public PropertyPath<Date> mainHolidayBeginDate()
        {
            if(_mainHolidayBeginDate == null )
                _mainHolidayBeginDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_MAIN_HOLIDAY_BEGIN_DATE, this);
            return _mainHolidayBeginDate;
        }

    /**
     * @return Дата окончания основного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getMainHolidayEndDate()
     */
        public PropertyPath<Date> mainHolidayEndDate()
        {
            if(_mainHolidayEndDate == null )
                _mainHolidayEndDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_MAIN_HOLIDAY_END_DATE, this);
            return _mainHolidayEndDate;
        }

    /**
     * @return Длительность основного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getMainHolidayDuration()
     */
        public PropertyPath<Integer> mainHolidayDuration()
        {
            if(_mainHolidayDuration == null )
                _mainHolidayDuration = new PropertyPath<Integer>(EmployeeHolidayEmplListExtractGen.P_MAIN_HOLIDAY_DURATION, this);
            return _mainHolidayDuration;
        }

    /**
     * @return Дата начала дополнительного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getSecondHolidayBeginDate()
     */
        public PropertyPath<Date> secondHolidayBeginDate()
        {
            if(_secondHolidayBeginDate == null )
                _secondHolidayBeginDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_SECOND_HOLIDAY_BEGIN_DATE, this);
            return _secondHolidayBeginDate;
        }

    /**
     * @return Дата окончания дополнительного отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getSecondHolidayEndDate()
     */
        public PropertyPath<Date> secondHolidayEndDate()
        {
            if(_secondHolidayEndDate == null )
                _secondHolidayEndDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_SECOND_HOLIDAY_END_DATE, this);
            return _secondHolidayEndDate;
        }

    /**
     * @return Длительность дополнительного отпуска, календарных дней.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getSecondHolidayDuration()
     */
        public PropertyPath<Integer> secondHolidayDuration()
        {
            if(_secondHolidayDuration == null )
                _secondHolidayDuration = new PropertyPath<Integer>(EmployeeHolidayEmplListExtractGen.P_SECOND_HOLIDAY_DURATION, this);
            return _secondHolidayDuration;
        }

    /**
     * @return Дата начала отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Длительность отпуска, календарных дней. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getDuration()
     */
        public PropertyPath<Integer> duration()
        {
            if(_duration == null )
                _duration = new PropertyPath<Integer>(EmployeeHolidayEmplListExtractGen.P_DURATION, this);
            return _duration;
        }

    /**
     * @return Отпуск сотрудника.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeeFirstHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> employeeFirstHoliday()
        {
            if(_employeeFirstHoliday == null )
                _employeeFirstHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_EMPLOYEE_FIRST_HOLIDAY, this);
            return _employeeFirstHoliday;
        }

    /**
     * @return Отпуск сотрудника.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeeSecondHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> employeeSecondHoliday()
        {
            if(_employeeSecondHoliday == null )
                _employeeSecondHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_EMPLOYEE_SECOND_HOLIDAY, this);
            return _employeeSecondHoliday;
        }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getOptionalCondition()
     */
        public PropertyPath<String> optionalCondition()
        {
            if(_optionalCondition == null )
                _optionalCondition = new PropertyPath<String>(EmployeeHolidayEmplListExtractGen.P_OPTIONAL_CONDITION, this);
            return _optionalCondition;
        }

    /**
     * @return График отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getVacationSchedule()
     */
        public VacationSchedule.Path<VacationSchedule> vacationSchedule()
        {
            if(_vacationSchedule == null )
                _vacationSchedule = new VacationSchedule.Path<VacationSchedule>(L_VACATION_SCHEDULE, this);
            return _vacationSchedule;
        }

    /**
     * @return Строка графика отпусков.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getVacationScheduleItem()
     */
        public VacationScheduleItem.Path<VacationScheduleItem> vacationScheduleItem()
        {
            if(_vacationScheduleItem == null )
                _vacationScheduleItem = new VacationScheduleItem.Path<VacationScheduleItem>(L_VACATION_SCHEDULE_ITEM, this);
            return _vacationScheduleItem;
        }

    /**
     * @return Фактическая дата ухода в отпуск в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getOldScheduleItemFactDate()
     */
        public PropertyPath<Date> oldScheduleItemFactDate()
        {
            if(_oldScheduleItemFactDate == null )
                _oldScheduleItemFactDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_OLD_SCHEDULE_ITEM_FACT_DATE, this);
            return _oldScheduleItemFactDate;
        }

    /**
     * @return Предполагаемая дата переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getOldScheduleItemPostponeDate()
     */
        public PropertyPath<Date> oldScheduleItemPostponeDate()
        {
            if(_oldScheduleItemPostponeDate == null )
                _oldScheduleItemPostponeDate = new PropertyPath<Date>(EmployeeHolidayEmplListExtractGen.P_OLD_SCHEDULE_ITEM_POSTPONE_DATE, this);
            return _oldScheduleItemPostponeDate;
        }

    /**
     * @return Основание для переноса отпуска в строке Графика отпусков сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getOldScheduleItemPostponeBasic()
     */
        public PropertyPath<String> oldScheduleItemPostponeBasic()
        {
            if(_oldScheduleItemPostponeBasic == null )
                _oldScheduleItemPostponeBasic = new PropertyPath<String>(EmployeeHolidayEmplListExtractGen.P_OLD_SCHEDULE_ITEM_POSTPONE_BASIC, this);
            return _oldScheduleItemPostponeBasic;
        }

    /**
     * @return Статус на должности при уходе в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeePostStatusNew()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusNew()
        {
            if(_employeePostStatusNew == null )
                _employeePostStatusNew = new EmployeePostStatus.Path<EmployeePostStatus>(L_EMPLOYEE_POST_STATUS_NEW, this);
            return _employeePostStatusNew;
        }

    /**
     * @return Статус на должности до ухода в отпуск. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract#getEmployeePostStatusOld()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusOld()
        {
            if(_employeePostStatusOld == null )
                _employeePostStatusOld = new EmployeePostStatus.Path<EmployeePostStatus>(L_EMPLOYEE_POST_STATUS_OLD, this);
            return _employeePostStatusOld;
        }

        public Class getEntityClass()
        {
            return EmployeeHolidayEmplListExtract.class;
        }

        public String getEntityName()
        {
            return "employeeHolidayEmplListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
