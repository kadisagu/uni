/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e25;

import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.RepealExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.01.2012
 */
public class RepealExtractDao extends UniBaseDao implements IExtractComponentDao<RepealExtract>
{
    public void doCommit(RepealExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        MoveDaoFacade.getMoveDao().doRollbackExtract(extract.getAbstractEmployeeExtract(), getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_REJECTED), parameters);
    }

    public void doRollback(RepealExtract extract, Map parameters)
    {
        MoveDaoFacade.getMoveDao().doCommitExtract(extract.getAbstractEmployeeExtract(), getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), parameters);
    }
}