/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e25;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.entity.RepealExtract;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.01.2012
 */
public class RepealExtractPrint implements IPrintFormCreator<RepealExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RepealExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        StringBuilder revOrderBuilder = new StringBuilder();
        if (extract.getAbstractEmployeeExtract() instanceof SingleEmployeeExtract)
            revOrderBuilder.append("Приказ");
        else
            revOrderBuilder.append("Пункт приказа");
        modifier.put("revOrder", revOrderBuilder.toString());

        modifier.put("revocationOrderNumber", extract.getAbstractEmployeeExtract().getParagraph().getOrder().getNumber());

        modifier.put("revocationCommitDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getAbstractEmployeeExtract().getParagraph().getOrder().getCommitDate()));

        modifier.put("revocationOrderType", "\"" + extract.getAbstractEmployeeExtract().getType().getTitle() + "\"");


        StringBuilder postConditionsBuilder = new StringBuilder();

        List<EmployeePostStaffRateItem> staffRateItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());
        Double staffRate = 0d;
        for (EmployeePostStaffRateItem item :staffRateItemList)
            staffRate += item.getStaffRate();

        if (staffRate < 1)
        {
            if (extract.getEntity().getPerson().isMale())
                postConditionsBuilder.append(" работающему ");
            else
                postConditionsBuilder.append(" работающей ");

            if (extract.getEntity().getPostType().getCode().equals(UniDefines.POST_TYPE_SECOND_JOB_INNER) || extract.getEntity().getPostType().getCode().equals(UniDefines.POST_TYPE_SECOND_JOB_OUTER))
                postConditionsBuilder.append("по совместительству ");

            postConditionsBuilder.append("на ");

            BigDecimal x = new java.math.BigDecimal(staffRate);
            x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
            staffRate = x.doubleValue();

            postConditionsBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate));

            postConditionsBuilder.append(" ставки,");
        }

        modifier.put("postConditions", postConditionsBuilder.toString());

        modifier.modify(document);
        return document;
    }
}