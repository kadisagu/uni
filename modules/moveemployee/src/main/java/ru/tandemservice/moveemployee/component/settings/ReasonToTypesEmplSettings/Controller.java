/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.settings.ReasonToTypesEmplSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.uni.component.settings.abstractRelation.AbstractRelationList.AbstractRelationListController;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 26.01.2009
 */
public class Controller extends AbstractRelationListController<IDAO, Model>
{
    @Override
    protected String getRelationPubComponent()
    {
        return IMoveEmployeeComponents.REASON_TO_TYPES_SETTINGS_PUB;
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        prepareListDataSource(component);
    }

    @SuppressWarnings("unchecked")
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());
        PublisherLinkColumn column = new PublisherLinkColumn("Причина", "title");
        IPublisherLinkResolver resolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                Map<String, Object> params = new HashMap<>();
                params.put("firstId", entity.getId());
                return params;
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return getRelationPubComponent();
            }
        };
        column.setResolver(resolver);
        dataSource.addColumn(column);
        dataSource.addColumn(new MultiValuesColumn("Типы документов", EmployeeOrderReasons.P_DOCUMENT_TYPES)
                                     .setFormatter(new RowCollectionFormatter(source -> ((EmployeeExtractType) source).getTitleWithParent()))
                                     .setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.REASON_TO_TYPES_SETTINGS_EDIT, new ParametersMap().add("employeeOrderReasonsId", component.getListenerParameter())));
    }
}