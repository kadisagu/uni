/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e27.Pub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract;

import java.util.Date;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 06.03.2012
 */
public class Model extends ModularEmployeeExtractPubModel<ScienceDegreePaymentExtract>
{
    private String _beforeDataPage = getClass().getPackage().getName() + ".BeforeDataPage";

    private String _labourContractStr;

    private String _staffRateStr;
    private String _emplStaffRateStr;

    private String _labourContractType;
    private String _labourContractNumber;
    private Date _labourContractDate;
    private Date _labourContractBeginDate;
    private Date _labourContractEndDate;

    private DynamicListDataSource _paymentDataSource;

    //Getters & Setters

    public String getLabourContractStr()
    {
        return _labourContractStr;
    }

    public void setLabourContractStr(String labourContractStr)
    {
        _labourContractStr = labourContractStr;
    }

    public DynamicListDataSource getPaymentDataSource()
    {
        return _paymentDataSource;
    }

    public void setPaymentDataSource(DynamicListDataSource paymentDataSource)
    {
        _paymentDataSource = paymentDataSource;
    }

    public String getBeforeDataPage()
    {
        return _beforeDataPage;
    }

    public void setBeforeDataPage(String beforeDataPage)
    {
        _beforeDataPage = beforeDataPage;
    }

    public String getLabourContractType()
    {
        return _labourContractType;
    }

    public void setLabourContractType(String labourContractType)
    {
        _labourContractType = labourContractType;
    }

    public String getLabourContractNumber()
    {
        return _labourContractNumber;
    }

    public void setLabourContractNumber(String labourContractNumber)
    {
        _labourContractNumber = labourContractNumber;
    }

    public Date getLabourContractDate()
    {
        return _labourContractDate;
    }

    public void setLabourContractDate(Date labourContractDate)
    {
        _labourContractDate = labourContractDate;
    }

    public Date getLabourContractBeginDate()
    {
        return _labourContractBeginDate;
    }

    public void setLabourContractBeginDate(Date labourContractBeginDate)
    {
        _labourContractBeginDate = labourContractBeginDate;
    }

    public Date getLabourContractEndDate()
    {
        return _labourContractEndDate;
    }

    public void setLabourContractEndDate(Date labourContractEndDate)
    {
        _labourContractEndDate = labourContractEndDate;
    }

    public String getStaffRateStr()
    {
        return _staffRateStr;
    }

    public void setStaffRateStr(String staffRateStr)
    {
        _staffRateStr = staffRateStr;
    }

    public String getEmplStaffRateStr()
    {
        return _emplStaffRateStr;
    }

    public void setEmplStaffRateStr(String emplStaffRateStr)
    {
        _emplStaffRateStr = emplStaffRateStr;
    }
}