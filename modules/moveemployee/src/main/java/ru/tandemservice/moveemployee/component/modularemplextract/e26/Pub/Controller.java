/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e26.Pub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.RowCollectionFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubController;
import ru.tandemservice.moveemployee.entity.ChangeHolidayExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 17.02.2012
 */
public class Controller extends ModularEmployeeExtractPubController<ChangeHolidayExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareHolidayDataSource(component);
    }

    public void prepareHolidayDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getHolidayDataSource() != null)
            return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
                getDao().prepareHolidayDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Отпуск за период", Model.PERIOD).setFormatter(RowCollectionFormatter.INSTANCE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Отпуск", Model.HOLIDAY).setFormatter(RowCollectionFormatter.INSTANCE).setClickable(false).setOrderable(false));

        model.setHolidayDataSource(dataSource);
    }
}