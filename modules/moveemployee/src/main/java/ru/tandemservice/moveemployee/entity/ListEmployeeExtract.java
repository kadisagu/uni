package ru.tandemservice.moveemployee.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.gen.ListEmployeeExtractGen;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.Comparator;
import java.util.List;

/**
 * Абстрактная выписка из списочного приказа по сотруднику
 */
public abstract class ListEmployeeExtract extends ListEmployeeExtractGen
{
    public static final Comparator<ListEmployeeExtract> FULL_FIO_AND_ID_COMPARATOR = CommonCollator.comparing(e -> e.getEmployee().getFullFio(), true);

    @Override
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        String employeeWord = null != getEntity() ? (SexCodes.FEMALE.equals(getEntity().getPerson().getIdentityCard().getSex().getCode()) ? "сотрудница" : "сотрудник") : "сотрудник";
        if (getParagraph() == null)
            return "Проект приказа «" + getType().getTitle() + "» | " + employeeWord + " " + getEmployeeTitle();
        else
            return "Выписка «" + getType().getTitle() + "» №" + getNumber() +(getParagraph().getOrder().getNumber() == null? "": " из приказа №" + getParagraph().getOrder().getNumber()) + (null == getEntity() ? "": " | " + employeeWord + " " + getEmployeeTitle());
    }
    
    public String getBasicListStr()
    {
        List<EmpListExtractToBasicRelation> list = MoveEmployeeDaoFacade.getMoveEmployeeDao().getListExtractBasicsList(this);
        String[] title = new String[list.size()];
        for (int i = 0; i < list.size(); i++)
            title[i] = list.get(i).getBasic().getTitle() + (StringUtils.isEmpty(list.get(i).getComment()) ? "" : " " + list.get(i).getComment());
        return StringUtils.join(title, "; ");
    }

    @Override
    public boolean isNoEdit()
    {
        //нельзя редактировать выписки у сотрудников если они не в состоянии формирования
        return !UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE.equals(getState().getCode()) && !UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER.equals(getState().getCode());
    }
}