/**
 *$Id:$
 */
package ru.tandemservice.moveemployee.base.bo.MoveEmployeeSettingsPaymentStringFormatDocument.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings;

/**
 * @author Alexander Shaburov
 * @since 19.10.12
 */
public interface IPaymentStringFormatDocumentSettingsDAO extends INeedPersistenceSupport
{
    /**
     * Обновляет элементы настройки в соответствии с Типами выписок по кадрам.
     * Если есть тип выписки для которой нет элемента настройки, то он создается.
     */
    void doUpdateSettingsEntity();

    /**
     * Вкл./выкл. использования типа документа.
     * При вкл. у дочернего включается у всех родителей.
     * @param entity элемент настройки
     */
    void doChangeActive(PaymentStringFormatDocumentSettings entity);
}
