/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e0.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractAddEdit.ModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;

/**
 * @author dseleznev
 * Created on: 14.11.2008
 */
public class DAO extends ModularEmployeeExtractAddEditDAO<ModularEmployeeExtract, Model> implements IDAO
{
    @Override
    protected ModularEmployeeExtract createNewInstance()
    {
        return null;
    }
    
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return null;
    }
}