/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatCasesEdit;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.runtime.EntityRuntime;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniemp.entity.catalog.ITitledWithCases;
import ru.tandemservice.uniemp.entity.catalog.ITitledWithPluralCases;

/**
 * @author esych
 * Created on: 17.01.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntity(get(model.getEntityId()));
        model.setEntityTitle(EntityRuntime.getMeta(model.getEntityId()).getTitle());
        if (null == ((ITitledWithCases)model.getEntity()).getNominativeCaseTitle())
            ((ITitledWithCases)model.getEntity()).setNominativeCaseTitle(((ITitled)model.getEntity()).getTitle());
        model.setHasPlurals(model.getEntity() instanceof ITitledWithPluralCases);
    }

    @Override
    public void update(Model model)
    {
        update(model.getEntity());
    }
}
