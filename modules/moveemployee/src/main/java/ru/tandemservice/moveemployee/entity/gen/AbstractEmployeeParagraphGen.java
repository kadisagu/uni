package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа по кадрам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AbstractEmployeeParagraphGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph";
    public static final String ENTITY_NAME = "abstractEmployeeParagraph";
    public static final int VERSION_HASH = 143610967;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String L_ORDER = "order";

    private int _number;     // Номер параграфа в приказе
    private AbstractEmployeeOrder _order;     // Приказ по кадрам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер параграфа в приказе. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер параграфа в приказе. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Приказ по кадрам. Свойство не может быть null.
     */
    @NotNull
    public AbstractEmployeeOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ по кадрам. Свойство не может быть null.
     */
    public void setOrder(AbstractEmployeeOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AbstractEmployeeParagraphGen)
        {
            setNumber(((AbstractEmployeeParagraph)another).getNumber());
            setOrder(((AbstractEmployeeParagraph)another).getOrder());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AbstractEmployeeParagraphGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AbstractEmployeeParagraph.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("AbstractEmployeeParagraph is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "order":
                    return obj.getOrder();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "order":
                    obj.setOrder((AbstractEmployeeOrder) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "order":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "order":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return Integer.class;
                case "order":
                    return AbstractEmployeeOrder.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AbstractEmployeeParagraph> _dslPath = new Path<AbstractEmployeeParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AbstractEmployeeParagraph");
    }
            

    /**
     * @return Номер параграфа в приказе. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Приказ по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph#getOrder()
     */
    public static AbstractEmployeeOrder.Path<AbstractEmployeeOrder> order()
    {
        return _dslPath.order();
    }

    public static class Path<E extends AbstractEmployeeParagraph> extends EntityPath<E>
    {
        private PropertyPath<Integer> _number;
        private AbstractEmployeeOrder.Path<AbstractEmployeeOrder> _order;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер параграфа в приказе. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(AbstractEmployeeParagraphGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Приказ по кадрам. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph#getOrder()
     */
        public AbstractEmployeeOrder.Path<AbstractEmployeeOrder> order()
        {
            if(_order == null )
                _order = new AbstractEmployeeOrder.Path<AbstractEmployeeOrder>(L_ORDER, this);
            return _order;
        }

        public Class getEntityClass()
        {
            return AbstractEmployeeParagraph.class;
        }

        public String getEntityName()
        {
            return "abstractEmployeeParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
