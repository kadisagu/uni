package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings;
import ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Формат строкового представления выплаты в приказах
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PaymentStringRepresentationFormatGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat";
    public static final String ENTITY_NAME = "paymentStringRepresentationFormat";
    public static final int VERSION_HASH = -1281668088;
    private static IEntityMeta ENTITY_META;

    public static final String L_DOCUMENT_TYPE = "documentType";
    public static final String L_PAYMENT_TYPE = "paymentType";
    public static final String L_PAYMENT = "payment";
    public static final String P_FORMAT_STRING = "formatString";
    public static final String P_EXAMPLE_STRING = "exampleString";
    public static final String P_ACTIVE = "active";

    private PaymentStringFormatDocumentSettings _documentType;     // Используемые типы документов в настройке Форматы печати выплат сотрудникам
    private PaymentType _paymentType;     // Тип выплат
    private Payment _payment;     // Выплата
    private String _formatString;     // Строка формата вывода выплаты
    private String _exampleString;     // Пример вывода выплаты
    private boolean _active;     // Используется или нет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Используемые типы документов в настройке Форматы печати выплат сотрудникам.
     */
    public PaymentStringFormatDocumentSettings getDocumentType()
    {
        return _documentType;
    }

    /**
     * @param documentType Используемые типы документов в настройке Форматы печати выплат сотрудникам.
     */
    public void setDocumentType(PaymentStringFormatDocumentSettings documentType)
    {
        dirty(_documentType, documentType);
        _documentType = documentType;
    }

    /**
     * @return Тип выплат.
     */
    public PaymentType getPaymentType()
    {
        return _paymentType;
    }

    /**
     * @param paymentType Тип выплат.
     */
    public void setPaymentType(PaymentType paymentType)
    {
        dirty(_paymentType, paymentType);
        _paymentType = paymentType;
    }

    /**
     * @return Выплата.
     */
    public Payment getPayment()
    {
        return _payment;
    }

    /**
     * @param payment Выплата.
     */
    public void setPayment(Payment payment)
    {
        dirty(_payment, payment);
        _payment = payment;
    }

    /**
     * @return Строка формата вывода выплаты. Свойство не может быть null.
     */
    @NotNull
    public String getFormatString()
    {
        return _formatString;
    }

    /**
     * @param formatString Строка формата вывода выплаты. Свойство не может быть null.
     */
    public void setFormatString(String formatString)
    {
        dirty(_formatString, formatString);
        _formatString = formatString;
    }

    /**
     * @return Пример вывода выплаты. Свойство не может быть null.
     */
    @NotNull
    public String getExampleString()
    {
        return _exampleString;
    }

    /**
     * @param exampleString Пример вывода выплаты. Свойство не может быть null.
     */
    public void setExampleString(String exampleString)
    {
        dirty(_exampleString, exampleString);
        _exampleString = exampleString;
    }

    /**
     * @return Используется или нет. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Используется или нет. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PaymentStringRepresentationFormatGen)
        {
            setDocumentType(((PaymentStringRepresentationFormat)another).getDocumentType());
            setPaymentType(((PaymentStringRepresentationFormat)another).getPaymentType());
            setPayment(((PaymentStringRepresentationFormat)another).getPayment());
            setFormatString(((PaymentStringRepresentationFormat)another).getFormatString());
            setExampleString(((PaymentStringRepresentationFormat)another).getExampleString());
            setActive(((PaymentStringRepresentationFormat)another).isActive());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PaymentStringRepresentationFormatGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PaymentStringRepresentationFormat.class;
        }

        public T newInstance()
        {
            return (T) new PaymentStringRepresentationFormat();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "documentType":
                    return obj.getDocumentType();
                case "paymentType":
                    return obj.getPaymentType();
                case "payment":
                    return obj.getPayment();
                case "formatString":
                    return obj.getFormatString();
                case "exampleString":
                    return obj.getExampleString();
                case "active":
                    return obj.isActive();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "documentType":
                    obj.setDocumentType((PaymentStringFormatDocumentSettings) value);
                    return;
                case "paymentType":
                    obj.setPaymentType((PaymentType) value);
                    return;
                case "payment":
                    obj.setPayment((Payment) value);
                    return;
                case "formatString":
                    obj.setFormatString((String) value);
                    return;
                case "exampleString":
                    obj.setExampleString((String) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "documentType":
                        return true;
                case "paymentType":
                        return true;
                case "payment":
                        return true;
                case "formatString":
                        return true;
                case "exampleString":
                        return true;
                case "active":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "documentType":
                    return true;
                case "paymentType":
                    return true;
                case "payment":
                    return true;
                case "formatString":
                    return true;
                case "exampleString":
                    return true;
                case "active":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "documentType":
                    return PaymentStringFormatDocumentSettings.class;
                case "paymentType":
                    return PaymentType.class;
                case "payment":
                    return Payment.class;
                case "formatString":
                    return String.class;
                case "exampleString":
                    return String.class;
                case "active":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PaymentStringRepresentationFormat> _dslPath = new Path<PaymentStringRepresentationFormat>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PaymentStringRepresentationFormat");
    }
            

    /**
     * @return Используемые типы документов в настройке Форматы печати выплат сотрудникам.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getDocumentType()
     */
    public static PaymentStringFormatDocumentSettings.Path<PaymentStringFormatDocumentSettings> documentType()
    {
        return _dslPath.documentType();
    }

    /**
     * @return Тип выплат.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getPaymentType()
     */
    public static PaymentType.Path<PaymentType> paymentType()
    {
        return _dslPath.paymentType();
    }

    /**
     * @return Выплата.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getPayment()
     */
    public static Payment.Path<Payment> payment()
    {
        return _dslPath.payment();
    }

    /**
     * @return Строка формата вывода выплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getFormatString()
     */
    public static PropertyPath<String> formatString()
    {
        return _dslPath.formatString();
    }

    /**
     * @return Пример вывода выплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getExampleString()
     */
    public static PropertyPath<String> exampleString()
    {
        return _dslPath.exampleString();
    }

    /**
     * @return Используется или нет. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    public static class Path<E extends PaymentStringRepresentationFormat> extends EntityPath<E>
    {
        private PaymentStringFormatDocumentSettings.Path<PaymentStringFormatDocumentSettings> _documentType;
        private PaymentType.Path<PaymentType> _paymentType;
        private Payment.Path<Payment> _payment;
        private PropertyPath<String> _formatString;
        private PropertyPath<String> _exampleString;
        private PropertyPath<Boolean> _active;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Используемые типы документов в настройке Форматы печати выплат сотрудникам.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getDocumentType()
     */
        public PaymentStringFormatDocumentSettings.Path<PaymentStringFormatDocumentSettings> documentType()
        {
            if(_documentType == null )
                _documentType = new PaymentStringFormatDocumentSettings.Path<PaymentStringFormatDocumentSettings>(L_DOCUMENT_TYPE, this);
            return _documentType;
        }

    /**
     * @return Тип выплат.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getPaymentType()
     */
        public PaymentType.Path<PaymentType> paymentType()
        {
            if(_paymentType == null )
                _paymentType = new PaymentType.Path<PaymentType>(L_PAYMENT_TYPE, this);
            return _paymentType;
        }

    /**
     * @return Выплата.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getPayment()
     */
        public Payment.Path<Payment> payment()
        {
            if(_payment == null )
                _payment = new Payment.Path<Payment>(L_PAYMENT, this);
            return _payment;
        }

    /**
     * @return Строка формата вывода выплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getFormatString()
     */
        public PropertyPath<String> formatString()
        {
            if(_formatString == null )
                _formatString = new PropertyPath<String>(PaymentStringRepresentationFormatGen.P_FORMAT_STRING, this);
            return _formatString;
        }

    /**
     * @return Пример вывода выплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#getExampleString()
     */
        public PropertyPath<String> exampleString()
        {
            if(_exampleString == null )
                _exampleString = new PropertyPath<String>(PaymentStringRepresentationFormatGen.P_EXAMPLE_STRING, this);
            return _exampleString;
        }

    /**
     * @return Используется или нет. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(PaymentStringRepresentationFormatGen.P_ACTIVE, this);
            return _active;
        }

        public Class getEntityClass()
        {
            return PaymentStringRepresentationFormat.class;
        }

        public String getEntityName()
        {
            return "paymentStringRepresentationFormat";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
