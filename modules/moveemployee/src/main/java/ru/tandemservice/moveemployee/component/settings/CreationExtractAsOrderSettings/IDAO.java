/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.settings.CreationExtractAsOrderSettings;


import ru.tandemservice.uni.dao.IUniDao;

/**
 * Create by: Shaburov
 * Date: 28.03.11
 */
public interface IDAO extends IUniDao<Model>
{
    public void updateExtractAsOrder(long id);
    public void updateExtractIndividual(long id);

    /**
     * проверяем, что для всех существующих выписок по сборным приказам есть даная настройка,<p>
     * если нет, то добавляем соответствующие записи в базу
     */
    public void updateForNewExtracts();
}
