package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.entity.gen.PaymentAssigningExtractGen;

public class PaymentAssigningExtract extends PaymentAssigningExtractGen
{
    public String getBonusListStr()
    {
        return CommonExtractUtil.getBonusListStr(this);
    }
}