package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь предоставляемых частей отпуска и выписки «Об отзыве из отпуска»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ProvideDateToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation";
    public static final String ENTITY_NAME = "provideDateToExtractRelation";
    public static final int VERSION_HASH = 2006204245;
    private static IEntityMeta ENTITY_META;

    public static final String L_REVOCATION_FROM_HOLIDAY_EXTRACT = "revocationFromHolidayExtract";
    public static final String P_PROVIDE_BEGIN_DAY = "provideBeginDay";
    public static final String P_PROVIDE_END_DAY = "provideEndDay";
    public static final String P_PROVIDE_DURATION_DAY = "provideDurationDay";

    private RevocationFromHolidayExtract _revocationFromHolidayExtract;     // Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска
    private Date _provideBeginDay;     // Дата начала
    private Date _provideEndDay;     // Дата окончания
    private int _provideDurationDay;     // Длительность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска. Свойство не может быть null.
     */
    @NotNull
    public RevocationFromHolidayExtract getRevocationFromHolidayExtract()
    {
        return _revocationFromHolidayExtract;
    }

    /**
     * @param revocationFromHolidayExtract Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска. Свойство не может быть null.
     */
    public void setRevocationFromHolidayExtract(RevocationFromHolidayExtract revocationFromHolidayExtract)
    {
        dirty(_revocationFromHolidayExtract, revocationFromHolidayExtract);
        _revocationFromHolidayExtract = revocationFromHolidayExtract;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getProvideBeginDay()
    {
        return _provideBeginDay;
    }

    /**
     * @param provideBeginDay Дата начала. Свойство не может быть null.
     */
    public void setProvideBeginDay(Date provideBeginDay)
    {
        dirty(_provideBeginDay, provideBeginDay);
        _provideBeginDay = provideBeginDay;
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     */
    @NotNull
    public Date getProvideEndDay()
    {
        return _provideEndDay;
    }

    /**
     * @param provideEndDay Дата окончания. Свойство не может быть null.
     */
    public void setProvideEndDay(Date provideEndDay)
    {
        dirty(_provideEndDay, provideEndDay);
        _provideEndDay = provideEndDay;
    }

    /**
     * @return Длительность. Свойство не может быть null.
     */
    @NotNull
    public int getProvideDurationDay()
    {
        return _provideDurationDay;
    }

    /**
     * @param provideDurationDay Длительность. Свойство не может быть null.
     */
    public void setProvideDurationDay(int provideDurationDay)
    {
        dirty(_provideDurationDay, provideDurationDay);
        _provideDurationDay = provideDurationDay;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ProvideDateToExtractRelationGen)
        {
            setRevocationFromHolidayExtract(((ProvideDateToExtractRelation)another).getRevocationFromHolidayExtract());
            setProvideBeginDay(((ProvideDateToExtractRelation)another).getProvideBeginDay());
            setProvideEndDay(((ProvideDateToExtractRelation)another).getProvideEndDay());
            setProvideDurationDay(((ProvideDateToExtractRelation)another).getProvideDurationDay());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ProvideDateToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ProvideDateToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new ProvideDateToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "revocationFromHolidayExtract":
                    return obj.getRevocationFromHolidayExtract();
                case "provideBeginDay":
                    return obj.getProvideBeginDay();
                case "provideEndDay":
                    return obj.getProvideEndDay();
                case "provideDurationDay":
                    return obj.getProvideDurationDay();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "revocationFromHolidayExtract":
                    obj.setRevocationFromHolidayExtract((RevocationFromHolidayExtract) value);
                    return;
                case "provideBeginDay":
                    obj.setProvideBeginDay((Date) value);
                    return;
                case "provideEndDay":
                    obj.setProvideEndDay((Date) value);
                    return;
                case "provideDurationDay":
                    obj.setProvideDurationDay((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "revocationFromHolidayExtract":
                        return true;
                case "provideBeginDay":
                        return true;
                case "provideEndDay":
                        return true;
                case "provideDurationDay":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "revocationFromHolidayExtract":
                    return true;
                case "provideBeginDay":
                    return true;
                case "provideEndDay":
                    return true;
                case "provideDurationDay":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "revocationFromHolidayExtract":
                    return RevocationFromHolidayExtract.class;
                case "provideBeginDay":
                    return Date.class;
                case "provideEndDay":
                    return Date.class;
                case "provideDurationDay":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ProvideDateToExtractRelation> _dslPath = new Path<ProvideDateToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ProvideDateToExtractRelation");
    }
            

    /**
     * @return Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation#getRevocationFromHolidayExtract()
     */
    public static RevocationFromHolidayExtract.Path<RevocationFromHolidayExtract> revocationFromHolidayExtract()
    {
        return _dslPath.revocationFromHolidayExtract();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation#getProvideBeginDay()
     */
    public static PropertyPath<Date> provideBeginDay()
    {
        return _dslPath.provideBeginDay();
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation#getProvideEndDay()
     */
    public static PropertyPath<Date> provideEndDay()
    {
        return _dslPath.provideEndDay();
    }

    /**
     * @return Длительность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation#getProvideDurationDay()
     */
    public static PropertyPath<Integer> provideDurationDay()
    {
        return _dslPath.provideDurationDay();
    }

    public static class Path<E extends ProvideDateToExtractRelation> extends EntityPath<E>
    {
        private RevocationFromHolidayExtract.Path<RevocationFromHolidayExtract> _revocationFromHolidayExtract;
        private PropertyPath<Date> _provideBeginDay;
        private PropertyPath<Date> _provideEndDay;
        private PropertyPath<Integer> _provideDurationDay;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation#getRevocationFromHolidayExtract()
     */
        public RevocationFromHolidayExtract.Path<RevocationFromHolidayExtract> revocationFromHolidayExtract()
        {
            if(_revocationFromHolidayExtract == null )
                _revocationFromHolidayExtract = new RevocationFromHolidayExtract.Path<RevocationFromHolidayExtract>(L_REVOCATION_FROM_HOLIDAY_EXTRACT, this);
            return _revocationFromHolidayExtract;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation#getProvideBeginDay()
     */
        public PropertyPath<Date> provideBeginDay()
        {
            if(_provideBeginDay == null )
                _provideBeginDay = new PropertyPath<Date>(ProvideDateToExtractRelationGen.P_PROVIDE_BEGIN_DAY, this);
            return _provideBeginDay;
        }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation#getProvideEndDay()
     */
        public PropertyPath<Date> provideEndDay()
        {
            if(_provideEndDay == null )
                _provideEndDay = new PropertyPath<Date>(ProvideDateToExtractRelationGen.P_PROVIDE_END_DAY, this);
            return _provideEndDay;
        }

    /**
     * @return Длительность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ProvideDateToExtractRelation#getProvideDurationDay()
     */
        public PropertyPath<Integer> provideDurationDay()
        {
            if(_provideDurationDay == null )
                _provideDurationDay = new PropertyPath<Integer>(ProvideDateToExtractRelationGen.P_PROVIDE_DURATION_DAY, this);
            return _provideDurationDay;
        }

        public Class getEntityClass()
        {
            return ProvideDateToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "provideDateToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
