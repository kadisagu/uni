/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e12;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeePostPPSAddExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uniemp.UniempDefines;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 15.09.2010
 */
public class EmployeePostPPSAddExtractPrint implements IPrintFormCreator<EmployeePostPPSAddExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeePostPPSAddExtract extract)
    {
        boolean maleSex = extract.getEmployee().getPerson().getIdentityCard().getSex().isMale();
        
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        modifier.put("fioI", extract.getEmployeeFioModified1());
        CommonExtractPrintUtil.injectTrialPeriods(modifier, null);

        String postTypeCode = extract.getPostType().getCode();
        if(UniDefines.POST_TYPE_SECOND_JOB.equals(postTypeCode) || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(postTypeCode) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(postTypeCode))
            modifier.put("secondJob", " по совместительству");
        else
            modifier.put("secondJob", "");

        if(null != extract.getCompetitionType())
        {
            String passedSexCase = maleSex ? "прошедшего" : "прошедшую";
            String compTypeCode = extract.getCompetitionType().getCode();
            modifier.put("competitionStr", " " + StringUtils.uncapitalize(extract.getCompetitionType().getTitle()) + ",");
            if(UniDefines.COMPETITION_ASSIGNMENT_TYPE_BEFORE_COMPETITION.equals(compTypeCode))
            {
                modifier.put("competitionTypeCase", " до прохождения по конкурсу");
                modifier.put("competitionTypeCase_prePass", " до прохождения по конкурсу");
                modifier.put("competitionTypeCase_pass", "");
            }
            else if(UniDefines.COMPETITION_ASSIGNMENT_TYPE_BEFORE_ELECTION.equals(compTypeCode))
            {
                modifier.put("competitionTypeCase", " до прохождения на выборах");
                modifier.put("competitionTypeCase_prePass", " до прохождения выборов");
                modifier.put("competitionTypeCase_pass", "");
            }
            else if(UniDefines.COMPETITION_ASSIGNMENT_TYPE_BY_COMPETITION_RESULTS.equals(compTypeCode))
            {
                modifier.put("competitionTypeCase", ", как " + passedSexCase + " конкурсный отбор,");
                modifier.put("competitionTypeCase_pass", ", " + passedSexCase + " конкурсный отбор,");
                modifier.put("competitionTypeCase_prePass", "");
            }
            else if(UniDefines.COMPETITION_ASSIGNMENT_TYPE_ELECTION_PASSED.equals(compTypeCode))
            {
                modifier.put("competitionTypeCase", ", как " + passedSexCase + " на выборах,");
                modifier.put("competitionTypeCase_pass", ", " + passedSexCase + " на выборах,");
                modifier.put("competitionTypeCase_prePass", "");
            }
        }
        else
        {
            modifier.put("competitionStr", "");
            modifier.put("competitionTypeCase", "");
            modifier.put("competitionTypeCase_pass", "");
            modifier.put("competitionTypeCase_prePass", "");
        }

        //алгоритм определяет какой источник финансирования указан у долей ставки в выписке
        //TODO НЕОБХОДИМ РЕФАКТОРИНГ КОГДА ПОЯВИТСЯ НОВЫЙ ИСТОЧНИК ФИНАНСИРОВАНИЯ
        Boolean budget = null;
        for (FinancingSourceDetails details : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract))
        {
            boolean currentFinSrc = details.getFinancingSource().getCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET);

            if (budget != null && !budget.equals(currentFinSrc))
            {
                budget = null;
                break;
            }

            if (budget == null & currentFinSrc)
                budget = true;

            if (budget == null && !currentFinSrc)
                budget = false;
        }

        if (budget == null)
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, null, "");
        }
        else if (budget)
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, true, "");
        }
        else
        {
            CommonExtractPrintUtil.injectFinancingSourceCases(modifier, false, "");
        }

        modifier.modify(document);

        RtfTableModifier visaModifier = CommonExtractPrint.createModularExtractTableModifier(extract);
        visaModifier.modify(document);

        return document;
    }
}