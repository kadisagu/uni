package ru.tandemservice.moveemployee.entity;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.moveemployee.entity.gen.AbstractEmployeeOrderGen;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;

import java.util.List;

public abstract class AbstractEmployeeOrder extends AbstractEmployeeOrderGen implements IAbstractOrder
{
    public static final String P_PARAGRAPH_LIST = "paragraphList";
    public static final String P_READONLY = "readonly";
    public static final String P_EXTRACT_COUNT = "countExtract";
    public static final String P_COUNT_PARAGRAPH = "countParagraph"; // view-property
    public static final String P_NO_DELETE = "noDelete";   // нельзя удалять

    @Override
    @SuppressWarnings({"unchecked"})
    public List<? extends IAbstractParagraph> getParagraphList()
    {
        return UniDaoFacade.getCoreDao().getList(AbstractEmployeeParagraph.class, IAbstractParagraph.L_ORDER, this, IAbstractParagraph.P_NUMBER);
    }

    @Override
    public int getParagraphCount()
    {
        return UniDaoFacade.getCoreDao().getCount(AbstractEmployeeParagraph.class, IAbstractParagraph.L_ORDER, this);
    }

    public boolean isReadonly()
    {
        return !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getState().getCode());
    }

    public boolean isNoDelete()
    {
        return !(UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getState().getCode()) || UnimoveDefines.CATALOG_ORDER_STATE_REJECTED.equals(getState().getCode()));
    }

    @Override
    public void setState(ICatalogItem state)
    {
        super.setState((OrderStates)state);
    }

    public String getCreateDateFormatted()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getCreateDate());
    }
}