package ru.tandemservice.moveemployee.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.moveemployee.entity.gen.EmployeeSurnameChangeExtractGen;

public class EmployeeSurnameChangeExtract extends EmployeeSurnameChangeExtractGen
{
    public String getIdentityCardFullTitle()
    {
        StringBuilder str = new StringBuilder();
        if (getIcCardType() != null)
            str.append(getIcCardType().getTitle()).append(": ");
        if (getIcSeria() != null)
            str.append(getIcSeria()).append(" ");
        if (getIcNumber() != null)
            str.append(getIcNumber()).append(" ");
        if (StringUtils.isNotEmpty(getIcIssuancePlace()))
            str.append("Выдан: ").append(getIcIssuancePlace()).append(" ");
        if (getIcIssuanceDate() != null)
            str.append("Дата выдачи: ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getIcIssuanceDate()));
        return str.toString();
    }
}