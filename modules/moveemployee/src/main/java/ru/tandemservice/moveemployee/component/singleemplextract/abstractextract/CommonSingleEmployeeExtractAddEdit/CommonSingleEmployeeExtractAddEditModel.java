/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.moveemployee.component.commons.ICommonEmployeeSingleExtractAddEditModel;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractAddEdit.SingleEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
public abstract class CommonSingleEmployeeExtractAddEditModel<T extends SingleEmployeeExtract> extends SingleEmployeeExtractAddEditModel<T> implements ICommonEmployeeSingleExtractAddEditModel<T>
{
    private String _textParagraph;

    private IMultiSelectModel _basicListModel;
    
    private List<EmployeeOrderReasons> _reasonList;

    private List<EmployeeOrderBasics> _selectedBasicList = new ArrayList<>();
    
    private boolean _disableCommitDate;
    private boolean _disableOrderNumber;
    
    private static final String _commonPage = CommonSingleEmployeeExtractAddEditModel.class.getPackage().getName() + ".CommonSingleEmployeeExtractAddEdit";
    private static final String _commentsPage = CommonSingleEmployeeExtractAddEditModel.class.getPackage().getName() + ".BasicCommentAddEdit";
    
    private EmployeeOrderBasics _currentBasic;
    private Map<Long, String> _currentBasicMap = new HashMap<>();

    private String _orderReasonUpdates = "basics, comments, textParagraph";

    private ISelectModel _executorListModel;
    private EmployeePost _executor;

    public String getCommonPage()
    {
        return _commonPage;
    }
    
    public String getCommentsPage()
    {
        return _commentsPage;
    }
    
    public boolean isNeedComments()
    {
        for (EmployeeOrderBasics basic : _selectedBasicList)
            if (basic.isCommentable()) return true;
        return false;
    }
    
    public String getCurrentBasicTitle()
    {
        return _currentBasicMap.get(_currentBasic.getId());
    }

    public void setCurrentBasicTitle(String title)
    {
        _currentBasicMap.put(_currentBasic.getId(), title);
    }

    public String getCurrentBasicId()
    {
        return "basicId_" + _currentBasic.getId();
    }

    // Getters & Setters

    public String getTextParagraph()
    {
        return _textParagraph;
    }

    public void setTextParagraph(String textParagraph)
    {
        _textParagraph = textParagraph;
    }

    @Override
    public IMultiSelectModel getBasicListModel()
    {
        return _basicListModel;
    }

    @Override
    public void setBasicListModel(IMultiSelectModel basicListModel)
    {
        this._basicListModel = basicListModel;
    }

    public List<EmployeeOrderReasons> getReasonList()
    {
        return _reasonList;
    }

    public void setReasonList(List<EmployeeOrderReasons> reasonList)
    {
        this._reasonList = reasonList;
    }

    @Override
    public List<EmployeeOrderBasics> getSelectedBasicList()
    {
        return _selectedBasicList;
    }

    @Override
    public void setSelectedBasicList(List<EmployeeOrderBasics> selectedBasicList)
    {
        this._selectedBasicList = selectedBasicList;
    }

    public boolean isDisableCommitDate()
    {
        return _disableCommitDate;
    }

    public void setDisableCommitDate(boolean disableCommitDate)
    {
        this._disableCommitDate = disableCommitDate;
    }

    public boolean isDisableOrderNumber()
    {
        return _disableOrderNumber;
    }

    public void setDisableOrderNumber(boolean disableOrderNumber)
    {
        this._disableOrderNumber = disableOrderNumber;
    }

    public EmployeeOrderBasics getCurrentBasic()
    {
        return _currentBasic;
    }

    public void setCurrentBasic(EmployeeOrderBasics currentBasic)
    {
        this._currentBasic = currentBasic;
    }

    public Map<Long, String> getCurrentBasicMap()
    {
        return _currentBasicMap;
    }

    public void setCurrentBasicMap(Map<Long, String> currentBasicMap)
    {
        this._currentBasicMap = currentBasicMap;
    }

    public String getOrderReasonUpdates()
    {
        return _orderReasonUpdates;
    }

    public void setOrderReasonUpdates(String orderReasonUpdates)
    {
        this._orderReasonUpdates = orderReasonUpdates;
    }

    public ISelectModel getExecutorListModel()
    {
        return _executorListModel;
    }

    public void setExecutorListModel(ISelectModel executorListModel)
    {
        this._executorListModel = executorListModel;
    }

    public EmployeePost getExecutor()
    {
        return _executor;
    }

    public void setExecutor(EmployeePost executor)
    {
        this._executor = executor;
    }

    public String getExecutorStr()
    {
        return null != getOrder().getExecutor() ? getOrder().getExecutor() : OrderExecutorSelectModel.getExecutor(getExecutor());
    }
}