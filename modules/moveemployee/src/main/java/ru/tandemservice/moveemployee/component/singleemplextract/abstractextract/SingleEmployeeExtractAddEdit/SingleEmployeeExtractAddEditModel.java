/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractAddEdit;

import java.util.List;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractParagraph;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;

/**
 * @author dseleznev
 * Created on: 27.05.2009
 */
@Input( {
    @Bind(key = "employeeId", binding = "employeeId"),
    @Bind(key = "employeePostId", binding = "employeePost.id"), 
    @Bind(key = "extractTypeId", binding = "extractTypeId"), 
    @Bind(key = "extractId", binding = "extractId") 
})
public abstract class SingleEmployeeExtractAddEditModel<T extends SingleEmployeeExtract>
{
    private Long _extractId;
    private T _extract;
    private EmployeeSingleExtractOrder _order;
    private EmployeeSingleExtractParagraph _paragraph;
    private boolean _addForm;
    private boolean _editForm;
    private Long _employeeId;
    private Employee _employee;
    private EmployeePost _employeePost = new EmployeePost();
    
    private Long _extractTypeId;
    private EmployeeExtractType _extractType;
    private List<EmployeeExtractType> _extractTypeList;

    // Getters & Setters

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        _extract = extract;
    }

    public EmployeeSingleExtractOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeSingleExtractOrder order)
    {
        this._order = order;
    }

    public EmployeeSingleExtractParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EmployeeSingleExtractParagraph paragraph)
    {
        this._paragraph = paragraph;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        this._employeePost = employeePost;
    }

    public Long getExtractTypeId()
    {
        return _extractTypeId;
    }

    public void setExtractTypeId(Long extractTypeId)
    {
        this._extractTypeId = extractTypeId;
    }

    public EmployeeExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(EmployeeExtractType extractType)
    {
        this._extractType = extractType;
    }

    public List<EmployeeExtractType> getExtractTypeList()
    {
        return _extractTypeList;
    }

    public void setExtractTypeList(List<EmployeeExtractType> extractTypeList)
    {
        this._extractTypeList = extractTypeList;
    }

    public Long getEmployeeId()
    {
        return _employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        this._employeeId = employeeId;
    }

    public Employee getEmployee()
    {
        return _employee;
    }

    public void setEmployee(Employee employee)
    {
        this._employee = employee;
    }

}