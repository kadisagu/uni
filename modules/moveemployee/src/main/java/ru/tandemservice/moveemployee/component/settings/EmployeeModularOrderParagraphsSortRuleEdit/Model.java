/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.EmployeeModularOrderParagraphsSortRuleEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "rule.id")
} )
public class Model
{
    private EmployeeModularOrderParagraphsSortRule _rule = new EmployeeModularOrderParagraphsSortRule();

    public EmployeeModularOrderParagraphsSortRule getRule()
    {
        return _rule;
    }

    public void setRule(EmployeeModularOrderParagraphsSortRule rule)
    {
        _rule = rule;
    }
}