/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderPrint;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 20.01.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    @SuppressWarnings("unchecked")
    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        Model model = getModel(component);

        deactivate(component);

        StringBuilder orderFileNameBuilder = new StringBuilder("ModularOrder");
        if (null != model.getOrder().getCommitDate())
            orderFileNameBuilder.append("_").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getOrder().getCommitDate()).replaceAll("\\.", "_"));
        if (null != model.getOrder().getNumber())
            orderFileNameBuilder.append("_").append(CoreStringUtils.transliterate(model.getOrder().getNumber()));
        orderFileNameBuilder.append(".rtf");

        if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(model.getOrder().getState().getCode()))
        {
            // выписка уже проведена и печатная форма сохранена
            return new CommonBaseRenderer().rtf().document(model.getData()).fileName(getSuitableFileName(orderFileNameBuilder.toString()));
        } else
        {
            IPrintFormCreator<EmployeeModularOrder> formCreator = (IPrintFormCreator) ApplicationRuntime.getBean("employeeModularOrder_orderPrint");
            final RtfDocument document = formCreator.createPrintForm(model.getData(), model.getOrder());
            return new CommonBaseRenderer().rtf().document(document).fileName(getSuitableFileName(orderFileNameBuilder.toString()));
        }
    }

    private String getSuitableFileName(String srcFileName)
    {
        /*
         * Возвращает более-менее приемлемое имя для файла.
         * Делается транслит + выбрасываются лишние символы.
         */
        final List<Character> FILENAME_SUITABLE_CHARS_TABLE = ImmutableList.of('.', '-', '_', '(', ')');
        StringBuilder suitableFileName = new StringBuilder();
        String tmpFileName = CoreStringUtils.transliterate(srcFileName);

        char liter;
        for (int i = 0; i < tmpFileName.length(); ++i)
        {
            liter = tmpFileName.charAt(i);
            if (Character.isLetter(liter) || Character.isDigit(liter) || FILENAME_SUITABLE_CHARS_TABLE.contains(liter))
                suitableFileName.append(liter);
        }
        return suitableFileName.toString();
    }
}
