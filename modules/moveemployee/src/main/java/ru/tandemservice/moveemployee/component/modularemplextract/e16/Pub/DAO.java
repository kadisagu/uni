/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e16.Pub;

import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 08.04.2011
 */
public class DAO extends ModularEmployeeExtractPubDAO<ReturnFromChildCareHolidayExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setStaffRateStr(CommonEmployeeExtractUtil.getExtractFinancingSourceDetailsString(model.getExtract(), getSession(), true, null));
    }
}