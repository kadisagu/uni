/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWeekWorkLoad;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.ui.formatters.PensionPolicyFormatter;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.catalog.PaymentUnit;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 22.10.2010
 */
public class CommonExtractPrintUtil
{
    public static final int NOMINATIVE_CASE = 0;
    public static final int GENITIVE_CASE = 1;
    public static final int DATIVE_CASE = 2;
    public static final int ACCUSATIVE_CASE = 3;
    public static final int INSTRUMENTAL_CASE = 4;
    public static final int PREPOSITIONAL_CASE = 5;

    private static final String EMPTY_VALUE = "";
    private static final String SPACE_SIGN = "_";
    private static final String[] SIGNS_FOR_DIFFERENT_VARIATIONS = new String[] { ",", ";" };
    private static final String[] CASE_POSTFIXES_ARRAY = new String[] { "_N", "_G", "_D", "_A", "_I", "_P" };
    private static final String[] SEX_PREP_MALE_CASES_ARRAY = new String[] { "он", "его", "ему", "его", "им", "нем" };
    private static final String[] SEX_PREP_FEMALE_CASES_ARRAY = new String[] { "она", "ее", "ей", "ее", "ей", "ней" };
    private static final String[] EMPLOYEE_MALE_CASES_ARRAY = new String[] { "сотрудник", "сотрудника", "сотруднику", "сотрудника", "сотрудником", "сотруднике" };
    private static final String[] EMPLOYEE_FEMALE_CASES_ARRAY = new String[] { "сотрудница", "сотрудницы", "сотруднице", "сотрудницу", "сотрудницей", "сотруднице" };
    private static final String[] EMPLOYEE_MALE_ALT_CASES_ARRAY = new String[] { "работник", "работника", "работнику", "работника", "работником", "работнике" };
    private static final String[] EMPLOYEE_FEMALE_ALT_CASES_ARRAY = new String[] { "работница", "работницы", "работнице", "работницу", "работницей", "работнице" };

    /**
     * Добавляет в модифаер данные о выписке, её типе, датах создания и проведения,
     * а так же её номер в приказе, плюс дату и номер приказа.
     */
    public static void injectCommonExtractData(RtfInjectModifier modifier, AbstractEmployeeExtract extract)
    {
        // Общие данные о печатном документе
        Date currentTime = new Date();
        modifier.put("docPrintingTime", new SimpleDateFormat("HH:mm").format(currentTime));
        modifier.put("docPrintingDate", new SimpleDateFormat("dd.MM.yyyy").format(currentTime));
        modifier.put("docPrintingTimeSec", new SimpleDateFormat("HH:mm:ss").format(currentTime));
        modifier.put("docPrintingDateTime", new SimpleDateFormat("dd.MM.yyyy HH:mm").format(currentTime));
        modifier.put("docPrintingDateTimeSec", new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(currentTime));
        modifier.put("docPrintingDateMonthStr", new SimpleDateFormat("dd MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(currentTime));
        modifier.put("docPrintingDateTimeMonthStr", new SimpleDateFormat("dd MMMMM yyyy HH:mm", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(currentTime));
        modifier.put("docPrintingDateTimeSecMonthStr", new SimpleDateFormat("dd MMMMM yyyy HH:mm:ss", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(currentTime));

        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        if (principalContext instanceof EmployeePost)
        {
            EmployeePost docPrintingPost = (EmployeePost) principalContext;
            modifier.put("docPrintingEmployeeFio", docPrintingPost.getPerson().getFio());
            modifier.put("docPrintingEmployeeFullFio", docPrintingPost.getPerson().getFullFio());
            injectDifferentVariations(modifier, "docPrintingEmployeePhone", docPrintingPost.getPhone());
        }

        // Тип выписки
        modifier.put("extractType", extract.getParagraph() == null ? "проект приказа" : "выписка");

        // Дата формирования выписки
        modifier.put("createDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getCreateDate()));

        // Дата приказа (формальная)
        Date commitDate;
        if (extract.getParagraph() == null || (commitDate = extract.getParagraph().getOrder().getCommitDate()) == null)
            modifier.put("commitDate", "");
        else
            modifier.put("commitDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(commitDate));

        // Номер приказа
        modifier.put("orderNumber", extract.getParagraph() == null ? "" : extract.getParagraph().getOrder().getNumber());

        // Номер выписки в приказе (актуально только для выписок из сборных приказов)
        modifier.put("extractNumber", extract.getParagraph() == null ? "" : Integer.toString(extract.getParagraph().getNumber()));

        // Полное название образовательного учреждения для шапки
        TopOrgUnit academy = TopOrgUnit.getInstance(true);
        modifier.put("academy", getOrgUnitPrintingTitle(academy, NOMINATIVE_CASE, false, false));
        modifier.put("Academy", getOrgUnitPrintingTitle(academy, NOMINATIVE_CASE, true, false));
        modifier.put("academy_N", getOrgUnitPrintingTitle(academy, NOMINATIVE_CASE, false, false));
        modifier.put("Academy_N", getOrgUnitPrintingTitle(academy, NOMINATIVE_CASE, true, false));
        modifier.put("academy_G", getOrgUnitPrintingTitle(academy, GENITIVE_CASE, false, false));
        modifier.put("Academy_G", getOrgUnitPrintingTitle(academy, GENITIVE_CASE, true, false));
        modifier.put("academy_D", getOrgUnitPrintingTitle(academy, DATIVE_CASE, false, false));
        modifier.put("Academy_D", getOrgUnitPrintingTitle(academy, DATIVE_CASE, true, false));
        modifier.put("academy_A", getOrgUnitPrintingTitle(academy, ACCUSATIVE_CASE, false, false));
        modifier.put("Academy_A", getOrgUnitPrintingTitle(academy, ACCUSATIVE_CASE, true, false));
        modifier.put("academy_I", getOrgUnitPrintingTitle(academy, INSTRUMENTAL_CASE, false, false));
        modifier.put("Academy_I", getOrgUnitPrintingTitle(academy, INSTRUMENTAL_CASE, true, false));
        modifier.put("academy_P", getOrgUnitPrintingTitle(academy, PREPOSITIONAL_CASE, false, false));
        modifier.put("Academy_P", getOrgUnitPrintingTitle(academy, PREPOSITIONAL_CASE, true, false));
        modifier.put("organizationPrintTitle", getOrgUnitPrintingTitle(academy, NOMINATIVE_CASE, false, false));
        modifier.put("OrganizationPrintTitle", getOrgUnitPrintingTitle(academy, NOMINATIVE_CASE, true, false));
        modifier.put("academyShortTitle", null != academy.getShortTitle() ? academy.getShortTitle() : "");

        // Код ОКПО образовательного учредления
        modifier.put("okpo", academy.getOkpo());

        // Данные об исполнителе
        String executor = null!= extract.getParagraph() ? extract.getParagraph().getOrder().getExecutor() : null;
        modifier.put("executor", "");
        modifier.put("executorPhone", "");
        modifier.put("executorStr", "");
        if (executor != null && StringUtils.isNotEmpty(executor))
        {
            int phoneStart = executor.indexOf(" т. ");
            if (phoneStart == -1)
            {
                modifier.put("executor", executor);
                modifier.put("executorPhone", "");
                modifier.put("executorStr", "Исполнитель: " + executor);
            } else
            {
                modifier.put("executor", executor.substring(0, phoneStart));
                modifier.put("executorPhone", executor.substring(phoneStart + 1));
                modifier.put("executorStr", "Исполнитель: " + executor.substring(0, phoneStart) + " (" + executor.substring(phoneStart + 1) + ")");
            }
        }

        // TODO: hard code
        // Данные о ученой степени и ученом звании кадрового ресурса
        Employee employee = null;
        if (extract instanceof ModularEmployeeExtract) employee = ((ModularEmployeeExtract)extract).getEmployee();
        else if (extract instanceof SingleEmployeeExtract) employee = ((SingleEmployeeExtract)extract).getEmployee();

        if (null != employee)
        {
            PersonAcademicDegree degree = UniempDaoFacade.getUniempDAO().getEmployeeUpperAcademicDegree(employee);
            String degreeShortTitle = null != degree ? degree.getAcademicDegree().getShortTitle() : null;
            injectDifferentVariations(modifier, "academicDegreeShort", degreeShortTitle);

            PersonAcademicStatus status = UniempDaoFacade.getUniempDAO().getEmployeeUpperAcademicStatus(employee);
            String statusShortTitle = null != status ? status.getAcademicStatus().getShortTitle() : null;
            injectDifferentVariations(modifier, "academicStatusShort", statusShortTitle);
        }
    }

    public static void injectCommonEmployeeData(RtfInjectModifier modifier, Employee employee)
    {
        IdentityCard identityCard = employee.getPerson().getIdentityCard();

        // Employee birth date
        modifier.put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate()));

        // Employee policy number
        modifier.put("policyNumber", new PensionPolicyFormatter().format(employee.getPerson().getSnilsNumber()));
        modifier.put("policyNumberAlt", null != employee.getPerson().getSnilsNumber() ? new PensionPolicyFormatter().format(employee.getPerson().getSnilsNumber()) : "анкета");

        // Employee number
        modifier.put("employeeNumber", employee.getEmployeeCode());

        // Fio with all the declinations
        injectFio(modifier, identityCard);

        // Employee prepositions and additional words
        injectSexParts(modifier, identityCard.getSex());
    }

    public static void injectFio(RtfInjectModifier modifier, IdentityCard identityCard)
    {
        int indexOfSpace;
        Map<InflectorVariant, String> fioDeclinations = PersonManager.instance().declinationDao().getCalculatedFIODeclinations(identityCard);

        for (InflectorVariant variant : DeclinationDao.INFLECTOR_VARIANT_LIST)
        {
            String postfix;
            if (InflectorVariantCodes.RU_NOMINATIVE.equals(variant.getCode())) postfix = "N";
            else if (InflectorVariantCodes.RU_GENITIVE.equals(variant.getCode())) postfix = "G";
            else if (InflectorVariantCodes.RU_DATIVE.equals(variant.getCode())) postfix = "D";
            else if (InflectorVariantCodes.RU_ACCUSATIVE.equals(variant.getCode())) postfix = "A";
            else if (InflectorVariantCodes.RU_INSTRUMENTAL.equals(variant.getCode())) postfix = "I";
            else if (InflectorVariantCodes.RU_PREPOSITION.equals(variant.getCode())) postfix = "P";
            else
                break;

            String fioDeclinated = fioDeclinations.get(variant);
            modifier.put("fio_" + postfix, fioDeclinated);

            indexOfSpace = fioDeclinated.indexOf(" ");
            modifier.put("Fio_" + postfix, indexOfSpace <= 0 ? fioDeclinated.toUpperCase() : (fioDeclinated.substring(0, indexOfSpace).toUpperCase() + fioDeclinated.substring(indexOfSpace)));
            modifier.put("FIO_" + postfix, fioDeclinated.toUpperCase());

            String[] fioArr = StringUtils.split(fioDeclinated, " ");
            String fioInitials = fioArr[0] + " " + fioArr[1].charAt(0) + "." + (fioArr.length > 2 ? (fioArr[2].charAt(0) + ".") : "");
            modifier.put("fio_" + postfix + "_initials", fioInitials);

            //indexOfSpace = fioDeclinated.indexOf(" ");
            modifier.put("Fio_" + postfix + "_initials", fioInitials.toUpperCase());
            modifier.put("FIO_" + postfix + "_initials", fioInitials.toUpperCase());
        }
    }

    public static void injectSexParts(RtfInjectModifier modifier, Sex sex)
    {
        if (null == sex) return;

        boolean maleSex = sex.isMale();
        for (int i = 0; i < CASE_POSTFIXES_ARRAY.length; i++)
        {
            injectCapitalizedUncapitalizedValuePair(modifier, "sexPrep" + CASE_POSTFIXES_ARRAY[i], maleSex ? SEX_PREP_MALE_CASES_ARRAY[i] : SEX_PREP_FEMALE_CASES_ARRAY[i]);
            injectCapitalizedUncapitalizedValuePair(modifier, "employee" + CASE_POSTFIXES_ARRAY[i], maleSex ? EMPLOYEE_MALE_CASES_ARRAY[i] : EMPLOYEE_FEMALE_CASES_ARRAY[i]);
            injectCapitalizedUncapitalizedValuePair(modifier, "employeeAlt" + CASE_POSTFIXES_ARRAY[i], maleSex ? EMPLOYEE_MALE_ALT_CASES_ARRAY[i] : EMPLOYEE_FEMALE_ALT_CASES_ARRAY[i]);
        }
    }

    public static void injectPost(RtfInjectModifier modifier, PostBoundedWithQGandQL post, String postfix)
    {
        String realPostfix = (null == postfix || 0 == postfix.length()) ? "" : ("_" + postfix);
        modifier.put("post" + realPostfix, getPostPrintingTitle(post, NOMINATIVE_CASE, true));
        modifier.put("Post" + realPostfix, getPostPrintingTitle(post, NOMINATIVE_CASE, false));
        modifier.put("post_N" + realPostfix, getPostPrintingTitle(post, NOMINATIVE_CASE, true));
        modifier.put("Post_N" + realPostfix, getPostPrintingTitle(post, NOMINATIVE_CASE, false));
        modifier.put("post_G" + realPostfix, getPostPrintingTitle(post, GENITIVE_CASE, true));
        modifier.put("Post_G" + realPostfix, getPostPrintingTitle(post, GENITIVE_CASE, false));
        modifier.put("post_D" + realPostfix, getPostPrintingTitle(post, DATIVE_CASE, true));
        modifier.put("Post_D" + realPostfix, getPostPrintingTitle(post, DATIVE_CASE, false));
        modifier.put("post_A" + realPostfix, getPostPrintingTitle(post, ACCUSATIVE_CASE, true));
        modifier.put("Post_A" + realPostfix, getPostPrintingTitle(post, ACCUSATIVE_CASE, false));
        modifier.put("post_I" + realPostfix, getPostPrintingTitle(post, INSTRUMENTAL_CASE, true));
        modifier.put("Post_I" + realPostfix, getPostPrintingTitle(post, INSTRUMENTAL_CASE, false));
        modifier.put("post_P" + realPostfix, getPostPrintingTitle(post, PREPOSITIONAL_CASE, true));
        modifier.put("Post_P" + realPostfix, getPostPrintingTitle(post, PREPOSITIONAL_CASE, false));
        modifier.put("qualificationLevelShortTitle" + realPostfix, post.getQualificationLevel().getShortTitle());
        modifier.put("profQualificationGroup" + realPostfix, post.getProfQualificationGroup().getTitle());
    }

    public static void injectOrgUnit(RtfInjectModifier modifier, OrgUnit orgUnit, String postfix)
    {
        String realPostfix = (null == postfix || 0 == postfix.length()) ? "" : ("_" + postfix);
        modifier.put("orgUnit" + realPostfix, getOrgUnitPrintingTitle(orgUnit, NOMINATIVE_CASE, false, true));
        modifier.put("OrgUnit" + realPostfix, getOrgUnitPrintingTitle(orgUnit, NOMINATIVE_CASE, true, false));
        modifier.put("orgUnit_N" + realPostfix, getOrgUnitPrintingTitle(orgUnit, NOMINATIVE_CASE, false, true));
        modifier.put("OrgUnit_N" + realPostfix, getOrgUnitPrintingTitle(orgUnit, NOMINATIVE_CASE, true, false));
        modifier.put("orgUnit_G" + realPostfix, getOrgUnitPrintingTitle(orgUnit, GENITIVE_CASE, false, true));
        modifier.put("OrgUnit_G" + realPostfix, getOrgUnitPrintingTitle(orgUnit, GENITIVE_CASE, true, false));
        modifier.put("orgUnit_D" + realPostfix, getOrgUnitPrintingTitle(orgUnit, DATIVE_CASE, false, true));
        modifier.put("OrgUnit_D" + realPostfix, getOrgUnitPrintingTitle(orgUnit, DATIVE_CASE, true, false));
        modifier.put("orgUnit_A" + realPostfix, getOrgUnitPrintingTitle(orgUnit, ACCUSATIVE_CASE, false, true));
        modifier.put("OrgUnit_A" + realPostfix, getOrgUnitPrintingTitle(orgUnit, ACCUSATIVE_CASE, true, false));
        modifier.put("orgUnit_I" + realPostfix, getOrgUnitPrintingTitle(orgUnit, INSTRUMENTAL_CASE, false, true));
        modifier.put("OrgUnit_I" + realPostfix, getOrgUnitPrintingTitle(orgUnit, INSTRUMENTAL_CASE, true, false));
        modifier.put("orgUnit_P" + realPostfix, getOrgUnitPrintingTitle(orgUnit, PREPOSITIONAL_CASE, false, true));
        modifier.put("OrgUnit_P" + realPostfix, getOrgUnitPrintingTitle(orgUnit, PREPOSITIONAL_CASE, true, false));

        if (null != orgUnit)
        {
            if (OrgUnitTypeCodes.CATHEDRA.equals(orgUnit.getOrgUnitType().getCode()) || OrgUnitTypeCodes.FACULTY.equals(orgUnit.getOrgUnitType().getCode()))
            {
                modifier.put("orgUnitPrep", "на");
                modifier.put("orgUnitPrep_IN", "на");
                modifier.put("orgUnitPrep_OUT", "с");
            }
            else
            {
                modifier.put("orgUnitPrep", "в");
                modifier.put("orgUnitPrep_IN", "в");
                modifier.put("orgUnitPrep_OUT", "из");
            }
        }
    }

    public static void injectPostType(RtfInjectModifier modifier, PostType postType)
    {
        if (UniDefines.POST_TYPE_SECOND_JOB.equals(postType.getCode())
                || UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(postType.getCode())
                || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(postType.getCode()))
            injectCapitalizedUncapitalizedValuePair(modifier, "postType", "По совместительству");
        else if (UniDefines.POST_TYPE_MAIN_JOB.equals(postType.getCode()))
            injectCapitalizedUncapitalizedValuePair(modifier, "postType", "Основная работа");
        else
            injectCapitalizedUncapitalizedValuePair(modifier, "postType", postType.getTitle());

        injectCapitalizedUncapitalizedValuePair(modifier, "postTypeExtended", postType.getTitle());
    }

    public static void injectStaffRates(RtfInjectModifier modifier, List<FinancingSourceDetails> staffRateDetails)
    {
        double budgetStaffRate = 0d;
        double offBudgetStaffRate = 0d;

        for (FinancingSourceDetails details : staffRateDetails)
        {
            if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(details.getFinancingSource().getCode()))
                budgetStaffRate += details.getStaffRate();
            else
                offBudgetStaffRate += details.getStaffRate();
        }
        injectDifferentVariations(modifier, "forSumStaffRateNRV", "");

        if (budgetStaffRate + offBudgetStaffRate == 1)
        {
            modifier.put(",proportionalToStaffRate", "");
            CommonExtractPrintUtil.injectCapitalizedUncapitalizedValuePair(modifier, "fullStaffRate", "Полная ставка");
            CommonExtractPrintUtil.injectCapitalizedUncapitalizedValuePair(modifier, "fullStaffRate_A", "на полную ставку" + (budgetStaffRate > 0 && offBudgetStaffRate > 0 ? ", " : ""));
            modifier.put("forSumStaffRateCaseWithRateWord", "");
            modifier.put("sumStaffRateFullCase", "");
            modifier.put("staffRatePrep", "");
        }
        else
        {
            modifier.put(",proportionalToStaffRate", ", пропорционально занимаемой ставке");
            CommonExtractPrintUtil.injectCapitalizedUncapitalizedValuePair(modifier, "fullStaffRate", "");
            CommonExtractPrintUtil.injectCapitalizedUncapitalizedValuePair(modifier, "fullStaffRate_G", "");
            modifier.put("sumStaffRateFullCase", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(budgetStaffRate + offBudgetStaffRate));
            modifier.put("forSumStaffRateCaseWithRateWord", " (" + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(budgetStaffRate + offBudgetStaffRate) + " ставки)");
            modifier.put("staffRatePrep", "на ");

            injectDifferentVariations(modifier, "forSumStaffRateNRV", "на " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(budgetStaffRate + offBudgetStaffRate) + " НРВ");
        }

        modifier.put("sumStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(budgetStaffRate + offBudgetStaffRate));

        if (budgetStaffRate == 1)
        {
            modifier.put("budgetStaffRate", "");
            modifier.put("offBudgetStaffRate", "");
            injectDifferentVariations(modifier, "financingSourceAlt", "из г/б средств");
            modifier.put("financingSource", " с оплатой из средств федерального бюджета");
            modifier.put("budgetFinancingSourceCase", " с оплатой из средств федерального бюджета");
            modifier.put("budgetOffBudgetPartsSeparator", "");
            modifier.put("offBudgetFinancingSourceCase", "");
        }
        else if (offBudgetStaffRate == 1)
        {
            modifier.put("budgetStaffRate", "");
            modifier.put("offBudgetStaffRate", "");
            modifier.put("budgetFinancingSourceCase", "");
            injectDifferentVariations(modifier, "financingSourceAlt", "из внебюджетных средств");
            modifier.put("offBudgetFinancingSourceCase", " с оплатой из внебюджетных средств");
            modifier.put("financingSource", " с оплатой из внебюджетных средств");
            modifier.put("budgetOffBudgetPartsSeparator", "");
        }
        else if (budgetStaffRate > 0 && offBudgetStaffRate == 0)
        {
            modifier.put("offBudgetStaffRate", "");
            modifier.put("budgetStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(budgetStaffRate) + " ставки");
            modifier.put("financingSource", "с оплатой из средств федерального бюджета");
            modifier.put("budgetFinancingSourceCase", " с оплатой из средств федерального бюджета");
            injectDifferentVariations(modifier, "financingSourceAlt", "из г/б средств");
            modifier.put("budgetOffBudgetPartsSeparator", "");
            modifier.put("offBudgetFinancingSourceCase", "");
        }
        else if (budgetStaffRate == 0 && offBudgetStaffRate > 0)
        {
            modifier.put("budgetStaffRate", "");
            injectDifferentVariations(modifier, "financingSourceAlt", "из внебюджетных средств");
            modifier.put("offBudgetStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(offBudgetStaffRate) + " ставки");
            modifier.put("financingSource", "с оплатой из внебюджетных средств");
            modifier.put("offBudgetFinancingSourceCase", " с оплатой из внебюджетных средств");
            modifier.put("budgetOffBudgetPartsSeparator", "");
            modifier.put("budgetFinancingSourceCase", "");
        }
        else
        {
            modifier.put("budgetStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(budgetStaffRate) + " ставки");
            modifier.put("offBudgetStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(offBudgetStaffRate) + " ставки");
            modifier.put("budgetFinancingSourceCase", " с оплатой из средств федерального бюджета");
            modifier.put("offBudgetFinancingSourceCase", " с оплатой из внебюджетных средств");
            injectDifferentVariations(modifier, "financingSourceAlt", "");
            modifier.put("budgetOffBudgetPartsSeparator", ", ");
            modifier.put("financingSource", "");
        }
    }

    public static void injectUrgpuLikeConditions(RtfDocument document, RtfInjectModifier modifier, AbstractEmployeeExtract extract, List<FinancingSourceDetails> staffRateDetails, boolean addFullStaffRate)
    {
        double sumStaffRate = 0d;
        double budgetStaffRate = 0d;
        double offBudgetStaffRate = 0d;
        for (FinancingSourceDetails details : staffRateDetails)
        {
            sumStaffRate += details.getStaffRate();
            if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(details.getFinancingSource().getCode()))
                budgetStaffRate += details.getStaffRate();
            else
                offBudgetStaffRate += details.getStaffRate();
        }

        FinancingSource budget = UniDaoFacade.getCoreDao().get(FinancingSource.class, FinancingSource.P_CODE, UniempDefines.FINANCING_SOURCE_BUDGET);
        FinancingSource offBudget = UniDaoFacade.getCoreDao().get(FinancingSource.class, FinancingSource.P_CODE, UniempDefines.FINANCING_SOURCE_OFF_BUDGET);

        String budgetTitle = budget.getGenitiveCaseTitle() == null ? budget.getTitle() : budget.getGenitiveCaseTitle();
        String offBudgetTitle = offBudget.getGenitiveCaseTitle() == null ? offBudget.getTitle() : offBudget.getGenitiveCaseTitle();

        StringBuilder conditions = new StringBuilder();
        List<String> paragraphsToRemove = new ArrayList<>();
        FinancingSourceDetails onlyFinancingSource = staffRateDetails.size() == 1 ? staffRateDetails.get(0) : null;

        if (null != onlyFinancingSource && 1 == onlyFinancingSource.getStaffRate() && UniempDefines.FINANCING_SOURCE_BUDGET.equals(onlyFinancingSource.getFinancingSource().getCode()))
        {
            conditions.append("полная ставка с оплатой из ");
            conditions.append(budgetTitle);
            conditions.append(MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getStaffRateDetailsString(extract, true));
            paragraphsToRemove.add("sumStaffRateCondition");
        }
        else if (null != onlyFinancingSource && 1 == onlyFinancingSource.getStaffRate() && !UniempDefines.FINANCING_SOURCE_BUDGET.equals(onlyFinancingSource.getFinancingSource().getCode()))
        {
            conditions.append("полная ставка с оплатой из ");
            conditions.append(offBudgetTitle);
            conditions.append(MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getStaffRateDetailsString(extract, false));
            paragraphsToRemove.add("sumStaffRateCondition");
        }
        else
        {
            if (budgetStaffRate > 0 && offBudgetStaffRate > 0 && addFullStaffRate)
            {
                if(sumStaffRate == 1) modifier.put("sumStaffRateCondition", "полная ставка ");
                else modifier.put("sumStaffRateCondition", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(sumStaffRate) + " ставки");
            }
            else
            {
                paragraphsToRemove.add("sumStaffRateCondition");
            }

            if (budgetStaffRate > 0)
            {
                conditions.append(" ");
                conditions.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(budgetStaffRate));
                conditions.append(" ставки с оплатой из ");
                conditions.append(budgetTitle);
                conditions.append(MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getStaffRateDetailsString(extract, true));
                conditions.append(offBudgetStaffRate > 0 ? "," : "");
            }

            if (offBudgetStaffRate > 0)
            {
                conditions.append(" ");
                conditions.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(offBudgetStaffRate));
                conditions.append(" ставки с оплатой из ");
                conditions.append(offBudgetTitle);
                conditions.append(MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getStaffRateDetailsString(extract, false));
            }
        }

        UniRtfUtil.removeParagraphsByNamesFromAnywhere(document, paragraphsToRemove, false, true);
        modifier.put("conditions", conditions.toString());
    }

    public static void injectWorkConditions(RtfInjectModifier modifier, EmployeeWeekWorkLoad weekWorkLoad, EmployeeWorkWeekDuration workWeekDuration, Double sumStaffRate)
    {
        if (null != workWeekDuration)
        {
            String prefix;
            String shortPrefix;
            switch (workWeekDuration.getDaysAmount())
            {
            case 1: prefix = "одно"; shortPrefix = "1-но"; break;
            case 2: prefix = "двух"; shortPrefix = "2-х"; break;
            case 3: prefix = "трех"; shortPrefix = "3-х"; break;
            case 4: prefix = "четырех"; shortPrefix = "4-х"; break;
            case 5: prefix = "пяти"; shortPrefix = "5-ти"; break;
            case 6: prefix = "шести"; shortPrefix = "6-ти"; break;
            case 7: prefix = "семи"; shortPrefix = "7-ми"; break;
            default: prefix = "скольки?-"; shortPrefix = "скольки?-";
            }

            modifier.put("workWeekDuration", prefix + "дневная");
            modifier.put("workWeekDuration_D", prefix + "дневной");
            modifier.put("workWeekDurationShort", shortPrefix + " дневная");
            modifier.put("workWeekDurationShort_D", shortPrefix + " дневной");
        }

        if (null != weekWorkLoad)
        {
            modifier.put("weekWorkLoad", weekWorkLoad.getTitle());
            modifier.put("weekWorkLoadShort", DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(weekWorkLoad.getHoursAmount()));
            modifier.put("weekWorkLoadShortRated", DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(weekWorkLoad.getHoursAmount() * (null != sumStaffRate ? sumStaffRate : 1d)));
        }
    }

    public static void injectTrialPeriods(RtfInjectModifier modifier, Integer trialPeriod)
    {
        if(null == trialPeriod || 0 >= trialPeriod)
        {
            injectDifferentVariations(modifier, "trialPeriod", "");
            injectDifferentVariations(modifier, "trialPeriodAlt", "");
        }
        else
        {
            StringBuilder trialBuilder = new StringBuilder(" с испытательным сроком ").append(trialPeriod);
            StringBuilder trialAltBuilder = new StringBuilder(" с испытанием на срок ").append(trialPeriod);

            switch(trialPeriod)
            {
            case 1:
                injectDifferentVariations(modifier, "trialPeriod", trialBuilder.append(" месяц").toString());
                injectDifferentVariations(modifier, "trialPeriodAlt", trialAltBuilder.append(" месяц").toString());
                break;
            case 2:
            case 3:
            case 4:
                injectDifferentVariations(modifier, "trialPeriod", trialBuilder.append(" месяца").toString());
                injectDifferentVariations(modifier, "trialPeriodAlt", trialAltBuilder.append(" месяца").toString());
                break;
            default:
                injectDifferentVariations(modifier, "trialPeriod", trialBuilder.append(" месяцев").toString());
                injectDifferentVariations(modifier, "trialPeriodAlt", trialAltBuilder.append(" месяцев").toString());
            }
        }
    }

    public static void injectFinancingSourceCases(RtfInjectModifier modifier, Boolean isBudget, String postfix)
    {
        if(null == isBudget)
        {
            modifier.put("financingSource" + postfix, "____________________________________________");
            modifier.put("financingSource_N" + postfix, "____________________________________________");
            modifier.put("financingSource_G" + postfix, "____________________________________________");
            modifier.put("financingSource_D" + postfix, "____________________________________________");
            modifier.put("financingSource_A" + postfix, "____________________________________________");
            modifier.put("financingSource_I" + postfix, "____________________________________________");
            modifier.put("financingSource_P" + postfix, "____________________________________________");
        }
        else if(isBudget)
        {
            modifier.put("financingSource" + postfix, "средства федерального бюджета");
            modifier.put("financingSource_N" + postfix, "средства федерального бюджета");
            modifier.put("financingSource_G" + postfix, "средств федерального бюджета");
            modifier.put("financingSource_D" + postfix, "средствам федерального бюджета");
            modifier.put("financingSource_A" + postfix, "средства федерального бюджета");
            modifier.put("financingSource_I" + postfix, "средствами федерального бюджета");
            modifier.put("financingSource_P" + postfix, "средствах федерального бюджета");
        }
        else
        {
            modifier.put("financingSource" + postfix, "средства от приносящей доход деятельности");
            modifier.put("financingSource_N" + postfix, "средства от приносящей доход деятельности");
            modifier.put("financingSource_G" + postfix, "средств от приносящей доход деятельности");
            modifier.put("financingSource_D" + postfix, "средствам от приносящей доход деятельности");
            modifier.put("financingSource_A" + postfix, "средства от приносящей доход деятельности");
            modifier.put("financingSource_I" + postfix, "средствами от приносящей доход деятельности");
            modifier.put("financingSource_P" + postfix, "средствах от приносящей доход деятельности");
        }
    }

    public static void injectRsvpuPayments(RtfInjectModifier modifier, AbstractEmployeeExtract extract)
    {
        StringBuilder result = new StringBuilder();
        List<EmployeeBonus> fullMonthBonus = new ArrayList<>();
        List<EmployeeBonus> bonusesList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract);

        for (EmployeeBonus bonus : bonusesList)
        {
            Payment payment = bonus.getPayment();
            PaymentType paymentType = payment.getType();
            PaymentUnit paymentUnit = payment.getPaymentUnit();

            if (UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(paymentUnit.getCode()))
            {
                fullMonthBonus.add(bonus);
                continue;
            }

            result.append(", ");

            if (UniempDefines.PAYMENT_TYPE_COMPENSATION.equals(paymentType.getCode()))
                result.append("компенсационной выплатой ");
            else if (UniempDefines.PAYMENT_TYPE_COMPENSATION_ADD_PAYMENT.equals(paymentType.getCode()))
                result.append("компепнсационной доплатой ");
            else if (UniempDefines.PAYMENT_TYPE_COMPENSATION_INCREMENT.equals(paymentType.getCode()))
                result.append("компенсационной надбавкой ");
            else if (UniempDefines.PAYMENT_TYPE_STLIMULATION.equals(paymentType.getCode()))
                result.append("стимулирующей выплатой ");
            else if (UniempDefines.PAYMENT_TYPE_STLIMULATION_ADD_PAYMENT.equals(paymentType.getCode()))
                result.append("стимулирующей доплатой ");
            else if (UniempDefines.PAYMENT_TYPE_STLIMULATION_INCREMENT.equals(paymentType.getCode()))
                result.append("стимулирующей надбавкой ");
            else if (UniempDefines.PAYMENT_TYPE_STLIMULATION_BONUS.equals(paymentType.getCode()))
                result.append("премией ");
            else if (UniempDefines.PAYMENT_TYPE_STLIMULATION.equals(paymentType.getCode()))
                result.append("поощрительной выплатой ");
            else
                result.append("выплатой ");

            if (UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(paymentUnit.getCode()) || UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(paymentUnit.getCode()))
            {
                result.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()));
                if (UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(paymentUnit.getCode()))
                    result.append(" ").append(paymentUnit.getShortTitle());
                result.append(" ");
            }

            result.append(StringUtils.uncapitalize(bonus.getPayment().getTitle()));

            if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(paymentUnit.getCode()))
            {
                result.append(" ");
                result.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()));
                result.append(" руб.");
            }

            if (null != bonus.getFinancingSource())
            {
                if (UniempDefines.FINANCING_SOURCE_BUDGET.equals(bonus.getFinancingSourceCode()))
                    result.append(" из г/б средств");
                else if (UniempDefines.FINANCING_SOURCE_OFF_BUDGET.equals(bonus.getFinancingSourceCode()))
                    result.append(" из внебюджетных средств");

                if (null != bonus.getFinancingSourceItem())
                {
                    if(null == bonus.getFinancingSourceItem().getOrgUnit())
                        result.append(bonus.getFinancingSourceItem().getTitle());
                    else if (bonus.getFinancingSourceItem().getOrgUnit().isTop())
                        result.append(" университета");
                    else
                        result.append(" ").append(getOrgUnitPrintingTitle(bonus.getFinancingSourceItem().getOrgUnit(), GENITIVE_CASE, false, true));
                }
            }
        }

        modifier.put("_paymentsRsvpu", result.toString());

        if(!fullMonthBonus.isEmpty())
        {
            StringBuilder compensationBuilder = new StringBuilder();
            for(EmployeeBonus bonus : fullMonthBonus)
            {
                if(compensationBuilder.length() == 0) compensationBuilder.append(" Компенсационная выплата: ");
                else compensationBuilder.append(", ");

                compensationBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()));
                compensationBuilder.append("% ").append(StringUtils.uncapitalize(bonus.getPayment().getTitle()));
            }
            if(compensationBuilder.length() > 0) compensationBuilder.append(".");
            modifier.put("_compensationPaymentRsvpu", compensationBuilder.toString());
        }
        else
            modifier.put("_compensationPaymentRsvpu", "");
    }

    public static String getOrgUnitPrintingTitle(OrgUnit orgUnit, int caseNumber, boolean capitalize, boolean uncapitalize)
    {
        if(null == orgUnit) return "";

        String orgUnitTitle;
        String divisionWord = "подразделение";
        switch (caseNumber)
        {
            case NOMINATIVE_CASE: orgUnitTitle = orgUnit.getNominativeCaseTitle(); divisionWord = "подразделение"; break;
            case GENITIVE_CASE: orgUnitTitle = orgUnit.getGenitiveCaseTitle(); divisionWord = "подразделения"; break;
            case DATIVE_CASE: orgUnitTitle =  orgUnit.getDativeCaseTitle(); divisionWord = "подразделению"; break;
            case ACCUSATIVE_CASE: orgUnitTitle =  orgUnit.getAccusativeCaseTitle(); divisionWord = "подразделение"; break;
            case INSTRUMENTAL_CASE: orgUnitTitle =  orgUnit.getFullTitle(); divisionWord = "подразделением"; break;
            case PREPOSITIONAL_CASE: orgUnitTitle =  orgUnit.getPrepositionalCaseTitle(); divisionWord = "подразделении"; break;
            default: orgUnitTitle = orgUnit.getFullTitle();
        }

        return null != orgUnitTitle ? (capitalize ? StringUtils.capitalize(orgUnitTitle) : (uncapitalize ? StringUtils.uncapitalize(orgUnitTitle) : orgUnitTitle)) : (divisionWord + " «" + StringUtils.uncapitalize(orgUnit.getFullTitle()) + "»");
    }

    public static String getPostPrintingTitle(PostBoundedWithQGandQL post, int caseNumber, boolean uncapitalize)
    {
        if(null == post) return "";

        String postTitle;
        switch (caseNumber)
        {
            case NOMINATIVE_CASE: postTitle = post.getPost().getNominativeCaseTitle(); break;
            case GENITIVE_CASE: postTitle = post.getPost().getGenitiveCaseTitle(); break;
            case DATIVE_CASE: postTitle =  post.getPost().getDativeCaseTitle(); break;
            case ACCUSATIVE_CASE: postTitle =  post.getPost().getAccusativeCaseTitle(); break;
            case INSTRUMENTAL_CASE: postTitle =  post.getPost().getInstrumentalCaseTitle(); break;
            case PREPOSITIONAL_CASE: postTitle =  post.getPost().getPrepositionalCaseTitle(); break;
            default: postTitle = post.getPost().getTitle();
        }

        return null != postTitle ? (uncapitalize ? StringUtils.uncapitalize(postTitle) : StringUtils.capitalize(postTitle)) : ("«" + StringUtils.uncapitalize(post.getPost().getTitle()) + "»");
    }

    public static void injectDifferentVariations(RtfInjectModifier modifier, String keyBase, String value)
    {

        if (!StringUtils.isEmpty(value))
        {
            modifier.put(keyBase, value);
            modifier.put(keyBase + SPACE_SIGN, value + " ");
            modifier.put(SPACE_SIGN + keyBase, " " + value);
            modifier.put(SPACE_SIGN + keyBase + SPACE_SIGN, " " + value + " ");

            for (String sign : SIGNS_FOR_DIFFERENT_VARIATIONS)
            {
                modifier.put(keyBase + sign, value + sign);
                modifier.put(sign + keyBase, sign + " " + value);
                modifier.put(sign + keyBase + sign, sign + " " + value + sign);
            }
        }
        else
        {
            modifier.put(keyBase, EMPTY_VALUE);
            modifier.put(keyBase + SPACE_SIGN, EMPTY_VALUE);
            modifier.put(SPACE_SIGN + keyBase, EMPTY_VALUE);
            modifier.put(SPACE_SIGN + keyBase + SPACE_SIGN, EMPTY_VALUE);

            for (String sign : SIGNS_FOR_DIFFERENT_VARIATIONS)
            {
                modifier.put(keyBase + sign, EMPTY_VALUE);
                modifier.put(sign + keyBase, EMPTY_VALUE);
                modifier.put(sign + keyBase + sign, EMPTY_VALUE);
            }
        }
    }

    public static void injectCapitalizedUncapitalizedValuePair(RtfInjectModifier modifier, String key, String value)
    {
        if (StringUtils.isEmpty(key)) return;
        modifier.put(StringUtils.capitalize(key), StringUtils.isEmpty(value) ? "" : StringUtils.capitalize(value));
        modifier.put(StringUtils.uncapitalize(key), StringUtils.isEmpty(value) ? "" : StringUtils.uncapitalize(value));
    }
}