/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e15;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract;
import ru.tandemservice.uni.util.NumberConvertingUtil;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 24.03.2011
 */
public class ProlongationAnnualHolidayExtractPrint implements IPrintFormCreator<ProlongationAnnualHolidayExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ProlongationAnnualHolidayExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        //тип выписки
        modifier.put("orderType", extract.getType().getTitle());

        //период
        if (extract.getHoliday().getBeginDate() != null && extract.getHoliday().getEndDate() != null)
        {
            modifier.put("period" ," за период " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getHoliday().getBeginDate()) + " по "  +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getHoliday().getEndDate()));
        }
        else
            modifier.put("period", "");

        //кол-во дней периода продления
        modifier.put("amountDaysProlongation", NumberConvertingUtil.getCalendarDaysWithName(extract.getAmountDaysProlongation()));

        //начало и конец периода продления
        modifier.put("prolongationFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getProlongationFrom()));
        modifier.put("prolongationTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getProlongationTo()));

//        //ФАМИЛИЯ имя отчество
//        IdentityCard identityCard = null != extract.getEntity().getId() ? extract.getEntity().getPerson().getIdentityCard() : extract.getEntity().getEmployee().getPerson().getIdentityCard();
//        boolean isMaleSex = identityCard.getSex().isMale();
//
//        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.DATIVE, isMaleSex).toUpperCase());
//        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), GrammaCase.DATIVE, isMaleSex));
//        if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
//            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), GrammaCase.DATIVE, isMaleSex));
//        modifier.put("Fio", str.toString());

        RtfTableModifier visaModifier = CommonExtractPrint.createModularExtractTableModifier(extract);
        visaModifier.modify(document);

        modifier.modify(document);
        return document;
    }
}