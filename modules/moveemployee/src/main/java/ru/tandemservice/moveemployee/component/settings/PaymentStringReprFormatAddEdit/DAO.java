/* $Id$ */
package ru.tandemservice.moveemployee.component.settings.PaymentStringReprFormatAddEdit;

import org.hibernate.FlushMode;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.PaymentStringFormatDocumentSettings;
import ru.tandemservice.moveemployee.entity.PaymentStringRepresentationFormat;
import ru.tandemservice.moveemployee.entity.catalog.PaymentPrintFormatLabel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author esych
 * Created on: 17.01.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    protected Model _model;

    @Override
    public void prepare(Model model, IBusinessComponent component)
    {
        _model = model;

        if (null != model.getFormat().getId())
            model.setFormat((PaymentStringRepresentationFormat)get(model.getFormat().getId()));

        List<PaymentStringFormatDocumentSettings> documentSettingsList = new DQLSelectBuilder().fromEntity(PaymentStringFormatDocumentSettings.class, "b")
                .where(eq(property(PaymentStringFormatDocumentSettings.active().fromAlias("b")), value(true)))
                .createStatement(getSession()).list();
        model.setExtractTypeList(HierarchyUtil.listHierarchyNodesWithParents(documentSettingsList, true));
        model.setPaymentTypeList(HierarchyUtil.listHierarchyNodesWithParents(getList(PaymentType.class), true));
        model.setPaymentModel(new PaymentModel());

        if (null == model.getLabelDataSource())
            createLabelDataSource(model, component);
    }

    protected void createLabelDataSource(final Model model, IBusinessComponent component)
    {
        DynamicListDataSource<PaymentPrintFormatLabel> ds =
            new DynamicListDataSource<>(component, component1 -> {
                MQBuilder builder = new MQBuilder(PaymentPrintFormatLabel.ENTITY_CLASS, "fl");
                builder.addOrder("fl", PaymentPrintFormatLabel.code());
                model.getLabelDataSource().setTotalSize(builder.getResultCount(getSession()));
                model.getLabelDataSource().createPage(builder.<PaymentPrintFormatLabel>getResultList(getSession(),
                    (int)model.getLabelDataSource().getStartRow(), (int)model.getLabelDataSource().getCountRow()));
            });

        ActionColumn col = new ActionColumn("Копировать", ActionColumn.ADD, "onClickCopyLabel");
        col.setDisplayHeader(true);
        ds.addColumn(col);
        ds.addColumn(new SimpleColumn("Метка", PaymentPrintFormatLabel.label()).setClickable(false).setOrderable(false));
        ds.addColumn(new SimpleColumn("Описание", PaymentPrintFormatLabel.title()).setClickable(false).setOrderable(false));

        model.setLabelDataSource(ds);
    }

    @Override
    public void copyLabelToFormatString(Model model, Long lLabelID)
    {
        model.getFormat().setFormatString((null == model.getFormat().getFormatString() ?
            "" : model.getFormat().getFormatString()) + ((PaymentPrintFormatLabel)get(lLabelID)).getLabel());
    }

    @Override
    public void update(Model model)
    {
        if (model.getFormat().isActive())
            ru.tandemservice.moveemployee.component.settings.PaymentPrintFormatSettings.DAO
                .deactivateIdenticalStringFormats(model.getFormat(), getList(PaymentStringRepresentationFormat.class),
                    getSession());

        getSession().saveOrUpdate(model.getFormat());
    }

    protected class PaymentModel extends UniQueryFullCheckSelectModel
    {
        @Override
        protected MQBuilder query(String alias, String filter)
        {
            MQBuilder builder = new MQBuilder(Payment.ENTITY_CLASS, alias);
            if (null != filter && !filter.isEmpty())
                builder.add(MQExpression.like(alias, Payment.title(), CoreStringUtils.escapeLike(filter)));
            if (null != _model.getFormat().getPaymentType())
                builder.add(MQExpression.eq(alias, Payment.L_TYPE, _model.getFormat().getPaymentType()));
            builder.addOrder(alias, Payment.title());
            return builder;
        }
    }

    @Override
    public void setFormatStringExample(Model model)
    {
        getSession().setFlushMode(FlushMode.MANUAL);

        if (null == model.getFormat().getFormatString() || model.getFormat().getFormatString().isEmpty())
            return;

        MQBuilder builder = new MQBuilder(EmployeeBonus.ENTITY_CLASS, "eb");
        if (null != model.getFormat().getPaymentType())
            builder.add(MQExpression.eq("eb",
                EmployeeBonus.payment().type().id(), model.getFormat().getPaymentType().getId()));
        if (null != model.getFormat().getPayment())
            builder.add(MQExpression.eq("eb",
                EmployeeBonus.payment().id(), model.getFormat().getPayment().getId()));

        MQBuilder builder2 = new MQBuilder(builder);

        builder.add(MQExpression.isNotNull("eb", EmployeeBonus.extract().entity()));

        MQBuilder builder3 = new MQBuilder(builder);

        if (null != model.getFormat().getDocumentType())
            builder.add(MQExpression.eq("eb",
                EmployeeBonus.extract().type().code(), model.getFormat().getDocumentType().getCode()));
        EmployeeBonus bonus = (EmployeeBonus)builder.first(getSession());

        if (null == bonus)
            bonus = (EmployeeBonus)builder3.first(getSession());

        if (null == bonus)
            bonus = (EmployeeBonus)builder2.first(getSession());

        if (null == bonus)
            bonus = (EmployeeBonus)new MQBuilder(EmployeeBonus.ENTITY_CLASS, "eb").first(getSession());

        model.getFormat().setExampleString(
            CommonExtractUtil.expandPaymentFormatString(model.getFormat().getFormatString(), bonus, true));
    }
}