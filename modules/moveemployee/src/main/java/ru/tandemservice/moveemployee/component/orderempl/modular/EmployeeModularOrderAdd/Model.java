/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderAdd;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.moveemployee.component.commons.ExtractListModel;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrderParagraphsSortRule;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 10.11.2008
 */
public class Model extends ExtractListModel
{
    private EmployeeModularOrder _order = new EmployeeModularOrder();
    private boolean _disableCommitDate;
    private boolean _disableOrderNumber;
    private ISelectModel _employeePostModel;
    private EmployeePost _employeePost;
    private List<EmployeeModularOrderParagraphsSortRule> _paragraphsSortRuleList = new ArrayList<>();

    public EmployeeModularOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeModularOrder order)
    {
        _order = order;
    }

    public boolean isDisableCommitDate()
    {
        return _disableCommitDate;
    }

    public void setDisableCommitDate(boolean disableCommitDate)
    {
        this._disableCommitDate = disableCommitDate;
    }

    public boolean isDisableOrderNumber()
    {
        return _disableOrderNumber;
    }

    public void setDisableOrderNumber(boolean disableOrderNumber)
    {
        this._disableOrderNumber = disableOrderNumber;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        this._employeePostModel = employeePostModel;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        this._employeePost = employeePost;
    }

    public List<EmployeeModularOrderParagraphsSortRule> getParagraphsSortRuleList()
    {
        return _paragraphsSortRuleList;
    }

    public void setParagraphsSortRuleList(List<EmployeeModularOrderParagraphsSortRule> paragraphsSortRuleList)
    {
        _paragraphsSortRuleList = paragraphsSortRuleList;
    }
}