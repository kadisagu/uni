/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.e10.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeSurnameChangeExtract;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;

import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<EmployeeSurnameChangeExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected EmployeeSurnameChangeExtract createNewInstance()
    {
        return new EmployeeSurnameChangeExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        
        if(null == model.getExtract().getEntity() && null != model.getEmployeePost())
            model.getExtract().setEntity(model.getEmployeePost());
        
        model.setIdentityCardTypeList(getCatalogItemListOrderByCode(IdentityCardType.class));
        
        if(null != model.getExtract().getEntity())
        {
            model.setEmployeePost(model.getExtract().getEntity());
        }

        if (model.isAddForm())
        {
            model.getExtract().setLastName(model.getEmployee().getPerson().getIdentityCard().getLastName());
            model.getExtract().setFirstName(model.getEmployee().getPerson().getIdentityCard().getFirstName());
            model.getExtract().setMiddleName(model.getEmployee().getPerson().getIdentityCard().getMiddleName());
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setIdentityCardOld(model.getExtract().getEmployee().getPerson().getIdentityCard());
        super.update(model);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        //прверяем, что новые ФИО отличаются от текущего
        IdentityCard identityCard = null != model.getEmployeePost().getId() ? model.getEmployeePost().getPerson().getIdentityCard() : model.getEmployee().getPerson().getIdentityCard();
        if (model.getExtract().getLastName().equals(identityCard.getLastName()) &&
                model.getExtract().getFirstName().equals(identityCard.getFirstName()) &&
                ((model.getExtract().getMiddleName() == null && identityCard.getMiddleName() == null) || (model.getExtract().getMiddleName() != null && model.getExtract().getMiddleName().equals(identityCard.getMiddleName()))))
            errors.add("Новые фамилия, имя или отчество должны отличаться от текущих.", "lastName", "firstName", "middleName");
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }
}