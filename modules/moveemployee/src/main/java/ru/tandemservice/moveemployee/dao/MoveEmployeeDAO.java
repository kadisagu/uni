/**
 * $Id$
 */
package ru.tandemservice.moveemployee.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.IContractAndAssigmentExtract;
import ru.tandemservice.moveemployee.component.listemplextract.CommonListOrderPrint;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 07.11.2008
 */
@SuppressWarnings("unchecked")
public class MoveEmployeeDAO extends UniBaseDao implements IMoveEmployeeDAO
{
    private static final Object[] EMPLOYEE_ONLY_MODULAR_EXTRACT_TYPES = new Object[]
    {
        MoveEmployeeDefines.MODULAR_EMPLOYEE_ADD_EXTRACT_TYPE_CODE,
        MoveEmployeeDefines.MODULAR_EMPLOYEE_TEMP_ADD_EXTRACT_TYPE_CODE
    };

    private static final Object[] EMPLOYEE_ONLY_SINGLE_EXTRACT_TYPES = new Object[]
    {
        MoveEmployeeDefines.INDIVIDUAL_EMPLOYEE_ADD_EXTRACT_TYPE_CODE,
        MoveEmployeeDefines.INDIVIDUAL_EMPLOYEE_TEMP_ADD_EXTRACT_TYPE_CODE
    };

    @Override
    public List<EmployeePayment> getRemovePaymentsListExtracts(PaymentAssigningExtract extract)
    {
        MQBuilder builder = new MQBuilder(RemoveEmployeePaymentToExtractRelation.ENTITY_CLASS, "b", new String[]{RemoveEmployeePaymentToExtractRelation.employeePayment().s()});
        builder.add(MQExpression.eq("b", RemoveEmployeePaymentToExtractRelation.paymentAssigningExtract().id().s(), extract.getId()));

        return builder.getResultList(getSession());
    }

    @Override
    public boolean isExtractIndividual(ModularEmployeeExtract extract)
    {
        List<EmployeeExtractType> employeeExtractIndividualTypeList = getModularEmployeePostExtractIndividualTypeList();
        employeeExtractIndividualTypeList.addAll(getModularEmployeeExtractIndividualTypeList());

        return employeeExtractIndividualTypeList.contains(extract.getType());
    }

    @Override
    public ModularEmployeeExtract getOrdersModularEmployeeExtract(EmployeeModularOrder order)
    {
        if (order.getParagraphCount() == 1)
        {
            MQBuilder builder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "meeo");
            builder.add(MQExpression.eq("meeo", ModularEmployeeExtract.paragraph().order().s(), order));

            List<ModularEmployeeExtract> extractList = builder.getResultList(getSession());

            return extractList.get(0) != null ? extractList.get(0) : null;
        }
        else
            return null;
    }

    @Override
    public boolean isIndividualExtractOrder(EmployeeModularOrder order)
    {
        if (order.getParagraphCount() == 1)
        {
            List<EmployeeExtractType> employeeExtractIndividualTypeList = getModularEmployeePostExtractIndividualTypeList();
            employeeExtractIndividualTypeList.addAll(getModularEmployeeExtractIndividualTypeList());

            MQBuilder subBuilder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "meei", new String[] {ModularEmployeeExtract.type().s()});
            subBuilder.add(MQExpression.in("meei", ModularEmployeeExtract.type().s(), employeeExtractIndividualTypeList));

            MQBuilder builder = new MQBuilder(ModularEmployeeExtract.ENTITY_CLASS, "omee");
            builder.add(MQExpression.eq("omee", ModularEmployeeExtract.paragraph().order().s(), order));
            builder.add(MQExpression.in("omee", ModularEmployeeExtract.type().s(), subBuilder));

            return builder.getResultList(getSession()).size() != 0;
        }
        return false;
    }

    @Override
    public List<EmployeeExtractType> getModularEmployeeExtractIndividualTypeList()
    {
        MQBuilder extractAsOrderBuilder = new MQBuilder(CreationExtractAsOrderRule.ENTITY_CLASS, "eo");
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.employeeExtractType().active().s(), true));
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.extractIndividual().s(), true));
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.employeeExtractType().formForEmployee().s(), true));

        List<CreationExtractAsOrderRule> creationExtractAsOrderRuleList = extractAsOrderBuilder.getResultList(getSession());
        List<EmployeeExtractType> employeeExtractTypeList = new ArrayList<>();
        for (CreationExtractAsOrderRule extractRule : creationExtractAsOrderRuleList)
            employeeExtractTypeList.add(extractRule.getEmployeeExtractType());

        return employeeExtractTypeList;
    }

    @Override
    public List<EmployeeExtractType> getModularEmployeePostExtractIndividualTypeList()
    {
        MQBuilder extractAsOrderBuilder = new MQBuilder(CreationExtractAsOrderRule.ENTITY_CLASS, "eo");
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.employeeExtractType().active().s(), true));
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.extractIndividual().s(), true));
        extractAsOrderBuilder.add(MQExpression.notEq("eo", CreationExtractAsOrderRule.employeeExtractType().code().s(),
                MoveEmployeeDefines.MODULAR_EMPLOYEE_ADD_EXTRACT_TYPE_CODE));
        extractAsOrderBuilder.add(MQExpression.notEq("eo", CreationExtractAsOrderRule.employeeExtractType().code().s(),
                MoveEmployeeDefines.MODULAR_EMPLOYEE_TEMP_ADD_EXTRACT_TYPE_CODE));

        List<CreationExtractAsOrderRule> creationExtractAsOrderRuleList = extractAsOrderBuilder.getResultList(getSession());
        List<EmployeeExtractType> employeeExtractTypeList = new ArrayList<>();
        for (CreationExtractAsOrderRule extractRule : creationExtractAsOrderRuleList)
            employeeExtractTypeList.add(extractRule.getEmployeeExtractType());

        return employeeExtractTypeList;
    }

    @Override
    public List<EmployeeExtractType> getModularEmployeePostExtractAsOrderTypeList()
    {
        MQBuilder extractAsOrderBuilder = new MQBuilder(CreationExtractAsOrderRule.ENTITY_CLASS, "eo");
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.employeeExtractType().active().s(), true));
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.extractAsOrder().s(), true));
        extractAsOrderBuilder.add(MQExpression.notEq("eo", CreationExtractAsOrderRule.employeeExtractType().code().s(),
                MoveEmployeeDefines.MODULAR_EMPLOYEE_ADD_EXTRACT_TYPE_CODE));
        extractAsOrderBuilder.add(MQExpression.notEq("eo", CreationExtractAsOrderRule.employeeExtractType().code().s(),
                MoveEmployeeDefines.MODULAR_EMPLOYEE_TEMP_ADD_EXTRACT_TYPE_CODE));
        extractAsOrderBuilder.addOrder("eo", CreationExtractAsOrderRule.employeeExtractType().title(), OrderDirection.asc);

        List<CreationExtractAsOrderRule> creationExtractAsOrderRuleList = extractAsOrderBuilder.getResultList(getSession());
        List<EmployeeExtractType> employeeExtractTypeList = new ArrayList<>();
        for (CreationExtractAsOrderRule extractRule : creationExtractAsOrderRuleList)
            employeeExtractTypeList.add(extractRule.getEmployeeExtractType());

        return employeeExtractTypeList;
    }

    @Override
    public List<EmployeeExtractType> getModularEmployeeExtractAsOrderTypeList()
    {
        MQBuilder extractAsOrderBuilder = new MQBuilder(CreationExtractAsOrderRule.ENTITY_CLASS, "eo");
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.employeeExtractType().active().s(), true));
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.extractAsOrder().s(), true));
        extractAsOrderBuilder.add(MQExpression.eq("eo", CreationExtractAsOrderRule.employeeExtractType().formForEmployee().s(), true));
        extractAsOrderBuilder.addOrder("eo", CreationExtractAsOrderRule.employeeExtractType().title(), OrderDirection.asc);

        List<CreationExtractAsOrderRule> creationExtractAsOrderRuleList = extractAsOrderBuilder.getResultList(getSession());
        List<EmployeeExtractType> employeeExtractTypeList = new ArrayList<>();
        for (CreationExtractAsOrderRule extractRule : creationExtractAsOrderRuleList)
            employeeExtractTypeList.add(extractRule.getEmployeeExtractType());

        return employeeExtractTypeList;
    }

    @Override
    public List<EmployeeExtractType> getModularEmployeePostExtractTypeList()
    {
        Criteria c = getSession().createCriteria(EmployeeExtractType.class, "e");
        c.createAlias("e." + EmployeeExtractType.L_PARENT, "p");
        c.add(Restrictions.eq("e." + EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
        c.add(Restrictions.eq("p." + ICatalogItem.CATALOG_ITEM_CODE, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_MODULAR_ORDER_CODE));
        c.add(Restrictions.not(Restrictions.in("e." + ICatalogItem.CATALOG_ITEM_CODE, EMPLOYEE_ONLY_MODULAR_EXTRACT_TYPES)));
        c.addOrder(Order.asc(EmployeeExtractType.P_TITLE));
        return c.list();
    }

    @Override
    public List<EmployeeExtractType> getModularEmployeeExtractTypeList()
    {
        Criteria c = getSession().createCriteria(EmployeeExtractType.class, "e");
        c.createAlias("e." + EmployeeExtractType.L_PARENT, "p");
        c.add(Restrictions.eq("e." + EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
        c.add(Restrictions.eq("p." + ICatalogItem.CATALOG_ITEM_CODE, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_MODULAR_ORDER_CODE));
        c.add(Restrictions.eq("e." + EmployeeExtractType.P_FORM_FOR_EMPLOYEE, Boolean.TRUE));
        c.addOrder(Order.asc(EmployeeExtractType.P_TITLE));
        return c.list();
    }

    @Override
    public List<EmployeeExtractType> getSingleEmployeePostExtractTypeList()
    {
        Criteria c = getSession().createCriteria(EmployeeExtractType.class, "e");
        c.createAlias("e." + EmployeeExtractType.L_PARENT, "p");
        c.add(Restrictions.eq("e." + EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
        c.add(Restrictions.eq("p." + ICatalogItem.CATALOG_ITEM_CODE, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_INDIVIDUAL_ORDER_CODE));
        c.add(Restrictions.not(Restrictions.in("e." + ICatalogItem.CATALOG_ITEM_CODE, EMPLOYEE_ONLY_SINGLE_EXTRACT_TYPES)));
        c.addOrder(Order.asc(EmployeeExtractType.P_TITLE));
        return c.list();
    }

    @Override
    public List<EmployeeExtractType> getSingleEmployeeExtractTypeList()
    {
        Criteria c = getSession().createCriteria(EmployeeExtractType.class, "e");
        c.createAlias("e." + EmployeeExtractType.L_PARENT, "p");
        c.add(Restrictions.eq("e." + EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
        c.add(Restrictions.eq("p." + ICatalogItem.CATALOG_ITEM_CODE, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_INDIVIDUAL_ORDER_CODE));
        c.add(Restrictions.eq("e." + EmployeeExtractType.P_FORM_FOR_EMPLOYEE, Boolean.TRUE));
        c.addOrder(Order.asc(EmployeeExtractType.P_TITLE));
        return c.list();
    }

    @Override
    public List<EmployeeExtractType> getSingleExtractTypeList()
    {
        Criteria c = getSession().createCriteria(EmployeeExtractType.class, "e");
        c.createAlias("e." + EmployeeExtractType.L_PARENT, "p");
        c.add(Restrictions.eq("e." + EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
        c.add(Restrictions.eq("p." + ICatalogItem.CATALOG_ITEM_CODE, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_INDIVIDUAL_ORDER_CODE));
        c.addOrder(Order.asc(EmployeeExtractType.P_TITLE));
        return c.list();
    }

    @Override
    public List<EmployeeExtractType> getModularExtractTypeList()
    {
        Criteria c = getSession().createCriteria(EmployeeExtractType.class, "e");
        c.createAlias("e." + EmployeeExtractType.L_PARENT, "p");
        c.add(Restrictions.eq("p." + ICatalogItem.CATALOG_ITEM_CODE, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_MODULAR_ORDER_CODE));
        c.add(Restrictions.eq("e." + EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
        return c.list();
    }

    @Override
    public boolean isEmployeeHasFormativeExtracts(EmployeePost post)
    {
        MQBuilder builder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", IAbstractExtract.L_ENTITY, post));
        builder.add(MQExpression.in("e", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        return builder.getResultCount(getSession()) > 0;
    }

    @Override
    public boolean isEmployeePostHasNotFinishedExtracts(EmployeePost post)
    {
        MQBuilder subBuilder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "b", new String[]{AbstractEmployeeExtract.id().s()});
        subBuilder.add(MQExpression.eq("b", AbstractEmployeeExtract.state().code(), UnimoveDefines.CATALOG_EXTRACT_STATE_REJECTED));
        subBuilder.add(MQExpression.eq("b", AbstractEmployeeExtract.paragraph().order().state().code(), UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

        MQBuilder employeePostAddSubBuilder = new MQBuilder(EmployeePostAddExtract.ENTITY_CLASS, "epe", new String[]{EmployeePostAddExtract.P_ID});
        employeePostAddSubBuilder.add(MQExpression.eq("epe", EmployeePostAddExtract.L_EMPLOYEE_POST, post));

        MQBuilder employeeTransferSubBuilder = new MQBuilder(EmployeeTransferExtract.ENTITY_CLASS, "ete", new String[]{EmployeeTransferExtract.P_ID});
        employeeTransferSubBuilder.add(MQExpression.eq("ete", EmployeeTransferExtract.L_EMPLOYEE_POST, post));

        MQBuilder sciDegPaySubBuilder = new MQBuilder(ScienceDegreePaymentExtract.ENTITY_CLASS, "dpe", new String[]{ScienceDegreePaymentExtract.P_ID});
        sciDegPaySubBuilder.add(MQExpression.isNotNull("dpe", ScienceDegreePaymentExtract.employeePostNew()));
        sciDegPaySubBuilder.add(MQExpression.eq("dpe", ScienceDegreePaymentExtract.employeePostNew(), post));

        MQBuilder sciStatPaySubBuilder = new MQBuilder(ScienceStatusPaymentExtract.ENTITY_CLASS, "spe", new String[]{ScienceStatusPaymentExtract.P_ID});
        sciStatPaySubBuilder.add(MQExpression.isNotNull("spe", ScienceStatusPaymentExtract.employeePostNew()));
        sciStatPaySubBuilder.add(MQExpression.eq("spe", ScienceStatusPaymentExtract.employeePostNew(), post));

        AbstractExpression expr2 = MQExpression.in("e", AbstractEmployeeExtract.P_ID, employeePostAddSubBuilder);
        AbstractExpression expr3 = MQExpression.in("e", AbstractEmployeeExtract.P_ID, employeeTransferSubBuilder);
        AbstractExpression expr4 = MQExpression.in("e", AbstractEmployeeExtract.P_ID, sciDegPaySubBuilder);
        AbstractExpression expr5 = MQExpression.in("e", AbstractEmployeeExtract.P_ID, sciStatPaySubBuilder);

        MQBuilder builder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "e");
        AbstractExpression eqPost = MQExpression.eq("e", AbstractEmployeeExtract.entity(), post);
        builder.add(MQExpression.or(eqPost, expr2, expr3, expr4, expr5));
        builder.add(MQExpression.notEq("e", AbstractEmployeeExtract.state().code(), UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        builder.add(MQExpression.notIn("e", AbstractEmployeeExtract.id(), subBuilder));
        return builder.getResultCount(getSession()) > 0;
    }

    @Override
    public boolean isEmployeeHasFormativeExtracts(Employee employee)
    {
        MQBuilder builder1 = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "e");
        builder1.add(MQExpression.in("e", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        builder1.add(MQExpression.eq("e", ModularEmployeeExtract.L_EMPLOYEE, employee));

        MQBuilder builder2 = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "e");
        builder2.add(MQExpression.in("e", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER));
        builder2.add(MQExpression.eq("e", IAbstractExtract.L_ENTITY + "." + EmployeePost.L_EMPLOYEE, employee));

        return builder1.getResultCount(getSession()) > 0 || builder2.getResultCount(getSession()) > 0;
    }

    @Override
    public Integer getExtractIndex(Long extractId)
    {
        return ((EmployeeExtractType)((IAbstractExtract)get(extractId)).getType()).getIndex();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<EmployeeBonus> getExtractEmployeeBonusesList(AbstractEmployeeExtract extract)
    {
        return getSession().createCriteria(EmployeeBonus.class).add(Restrictions.eq(EmployeeBonus.L_EXTRACT, extract)).addOrder(Order.asc(EmployeeBonus.P_PRIORITY)).list();
    }

    @Override
    public boolean isMoveAccessible(EmployeePost employeePost)
    {
        // у сотрудника не должно быть выписок в непроведенном состоянии
        // откаченные выписки в проведенном состоянии разрешены
        MQBuilder subBuilder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "b", new String[]{AbstractEmployeeExtract.id().s()});
        subBuilder.add(MQExpression.eq("b", AbstractEmployeeExtract.state().code(), UnimoveDefines.CATALOG_EXTRACT_STATE_REJECTED));
        subBuilder.add(MQExpression.eq("b", AbstractEmployeeExtract.paragraph().order().state().code(), UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

        MQBuilder builder = new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", AbstractEmployeeExtract.entity(), employeePost));
        builder.add(MQExpression.notEq("e", AbstractEmployeeExtract.state().code(), UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED));
        builder.add(MQExpression.notIn("e", AbstractEmployeeExtract.id(), subBuilder));
        return !(builder.getResultCount(getSession()) > 0);
    }

    @Override
    public byte[] getTemplate(EmployeeExtractType extractType, int templateIndex)
    {
        Criteria c = getSession().createCriteria(MoveemployeeTemplate.class);
        c.add(Restrictions.eq(MoveemployeeTemplate.L_TYPE, extractType));
        c.add(Restrictions.eq(MoveemployeeTemplate.P_INDEX, templateIndex));
        return ((MoveemployeeTemplate) c.uniqueResult()).getContent();
    }

    @Override
    public void saveExtractText(AbstractEmployeeExtract extract, int textCode)
    {
        //1. проверяем существование печатной формы
        Criteria c = getSession().createCriteria(EmployeeExtractTextRelation.class);
        c.add(Restrictions.eq(EmployeeExtractTextRelation.L_EXTRACT, extract));
        c.add(Restrictions.eq(EmployeeExtractTextRelation.P_CODE, textCode));
        EmployeeExtractTextRelation rel = (EmployeeExtractTextRelation) c.uniqueResult();
        if (rel == null)
        {
            //2. если нету - создаем
            rel = new EmployeeExtractTextRelation();
            rel.setExtract(extract);
            rel.setCode(textCode);
        }
        final byte[] template = getTemplate(extract.getType(), textCode);
        String printName = EntityRuntime.getMeta(extract).getName() + "_extractPrint";
        IPrintFormCreator componentPrint = (IPrintFormCreator) ApplicationRuntime.getBean(printName);
        RtfDocument document = componentPrint.createPrintForm(template, extract);
        rel.setText(RtfUtil.toByteArray(document));
        getSession().saveOrUpdate(rel);
    }

    @Override
    public void saveModularOrderText(EmployeeModularOrder order)
    {
        //1. проверяем существование печатной формы
        Criteria c = getSession().createCriteria(EmployeeOrderTextRelation.class);
        c.add(Restrictions.eq(EmployeeOrderTextRelation.L_ORDER, order));
        EmployeeOrderTextRelation rel = (EmployeeOrderTextRelation) c.uniqueResult();
        if (rel == null)
        {
            //2. если нету - создаем
            rel = new EmployeeOrderTextRelation();
            rel.setOrder(order);
        }
        EmployeeExtractType modularOrderType = get(EmployeeExtractType.class, EmployeeExtractType.P_CODE, MoveEmployeeDefines.EMPLOYEE_EXTRACT_TYPE_MODULAR_ORDER_CODE);
        final byte[] template = get(MoveemployeeTemplate.class, MoveemployeeTemplate.L_TYPE, modularOrderType).getContent();
        IPrintFormCreator<EmployeeModularOrder> formCreator = (IPrintFormCreator) ApplicationRuntime.getBean("employeeModularOrder_orderPrint");
        final RtfDocument document = formCreator.createPrintForm(template, order);
        rel.setText(RtfUtil.toByteArray(document));
        getSession().saveOrUpdate(rel);
    }

    @Override
    public void saveListOrderText(EmployeeListOrder order)
    {
        //1. проверяем существование печатной формы
        Criteria c = getSession().createCriteria(EmployeeOrderTextRelation.class);
        c.add(Restrictions.eq(EmployeeOrderTextRelation.L_ORDER, order));
        EmployeeOrderTextRelation rel = (EmployeeOrderTextRelation) c.uniqueResult();
        if (rel == null)
        {
            //2. если нету - создаем
            rel = new EmployeeOrderTextRelation();
            rel.setOrder(order);
        }

        EmployeeListParagraph paragraph = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFirstParagraph(order);
        if(null == paragraph) throw new ApplicationException("Невозможно отправить на печать пустой приказ.");
        ListEmployeeExtract extract = paragraph.getFirstExtract();
        if(null == extract) throw new ApplicationException("Невозможно отправить на печать пустой приказ.");

        final byte[] template = getTemplate(order.getType(), 1);
        IPrintFormCreator<EmployeeListOrder> formCreator = (IPrintFormCreator) ApplicationRuntime.getBean("employeeListOrder" + extract.getType().getIndex() + "_orderPrint");
        final RtfDocument document = formCreator.createPrintForm(template, order);
        rel.setText(RtfUtil.toByteArray(document));
        getSession().saveOrUpdate(rel);
    }

    @Override
    public void saveListOrderText(ParagraphEmployeeListOrder order)
    {
        //1. проверяем существование печатной формы
        Criteria c = getSession().createCriteria(EmployeeOrderTextRelation.class);
        c.add(Restrictions.eq(EmployeeOrderTextRelation.L_ORDER, order));
        EmployeeOrderTextRelation rel = (EmployeeOrderTextRelation) c.uniqueResult();
        if (rel == null)
        {
            //2. если нету - создаем
            rel = new EmployeeOrderTextRelation();
            rel.setOrder(order);
        }

        EmployeeListParagraph paragraph = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFirstParagraph(order);
        if(null == paragraph) throw new ApplicationException("Невозможно отправить на печать пустой приказ.");
        ListEmployeeExtract extract = paragraph.getFirstExtract();
        if(null == extract) throw new ApplicationException("Невозможно отправить на печать пустой приказ.");

        final byte[] template = getTemplate(order.getType(), 1);

        IPrintFormCreator printForm;
        if (ApplicationRuntime.containsBean("employeeListOrder_" + extract.getType().getParent().getCode() + "_print"))
            printForm = (IPrintFormCreator) ApplicationRuntime.getBean("employeeListOrder_" + extract.getType().getParent().getCode() + "_print");
        else
            printForm = (IPrintFormCreator) ApplicationRuntime.getBean("employeeListOrder" + extract.getType().getIndex() + "_orderPrint");

        RtfDocument document = printForm.createPrintForm(template, order);

//        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
//        AbstractEmployeeExtract firstExtract = (AbstractEmployeeExtract) order.getParagraphList().get(0).getExtractList().get(0);
//        printForm.modifyOrderTemplate(injectModifier, order, firstExtract);
//        injectModifier.modify(document);
//
//        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
//        tableModifier.modify(document);

        CommonListOrderPrint.injectParagraphs(document, order, null);

        rel.setText(RtfUtil.toByteArray(document));
        getSession().saveOrUpdate(rel);
    }

    @Override
    public void deleteListExtract(Long extractId)
    {
        ListEmployeeExtract extract = getNotNull(ListEmployeeExtract.class, extractId);
        EmployeeListParagraph paragraph = (EmployeeListParagraph) extract.getParagraph();
        getSession().refresh(paragraph.getOrder());

        if (paragraph.getOrder().isReadonly())
            throw new ApplicationException("Приказ не может быть изменен.");

        if (paragraph.getExtractCount() == 1)
            MoveDaoFacade.getMoveDao().deleteParagraph(extract.getParagraph(), null);
        else
            MoveDaoFacade.getMoveDao().deleteExtractFromParagraph(extract);

        // Перенумеруем все выписки, так чтобы сортировка была по ФИО
        List<ListEmployeeExtract> extractList = getList(ListEmployeeExtract.class, IAbstractExtract.L_PARAGRAPH, paragraph);
        extractList.sort(ListEmployeeExtract.FULL_FIO_AND_ID_COMPARATOR);

        // нумеруем выписки с единицы
        int counter = 1;
        for (ListEmployeeExtract listEmployeeExtract : extractList)
        {
            listEmployeeExtract.setNumber(counter++);
            update(listEmployeeExtract);
        }
    }

    @Override
    public List<EmpExtractToBasicRelation> getExtractBasicsList(ModularEmployeeExtract extract)
    {
        Criteria crit = getSession().createCriteria(EmpExtractToBasicRelation.class, "rel");
        crit.createAlias("rel." + EmpExtractToBasicRelation.L_BASIC, "b");
        crit.add(Restrictions.eq(EmpExtractToBasicRelation.L_EXTRACT, extract));
        crit.addOrder(Order.asc("b." + EmployeeOrderBasics.P_TITLE));
        return crit.list();
    }

    @Override
    public List<EmpSingleExtractToBasicRelation> getSingleExtractBasicsList(SingleEmployeeExtract extract)
    {
        Criteria crit = getSession().createCriteria(EmpSingleExtractToBasicRelation.class, "rel");
        crit.createAlias("rel." + EmpSingleExtractToBasicRelation.L_BASIC, "b");
        crit.add(Restrictions.eq(EmpSingleExtractToBasicRelation.L_EXTRACT, extract));
        crit.addOrder(Order.asc("b." + EmployeeOrderBasics.P_TITLE));
        return crit.list();
    }

    @Override
    public SingleEmployeeExtract getExtractBySingleExtractOrder(EmployeeSingleExtractOrder order)
    {
        MQBuilder builder = new MQBuilder(SingleEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER, order));
        return (SingleEmployeeExtract) builder.uniqueResult(getSession());
    }

    @Override
    public List<EmpListExtractToBasicRelation> getListExtractBasicsList(ListEmployeeExtract extract)
    {
        Criteria crit = getSession().createCriteria(EmpListExtractToBasicRelation.class, "rel");
        crit.createAlias("rel." + EmpListExtractToBasicRelation.L_BASIC, "b");
        crit.add(Restrictions.eq(EmpListExtractToBasicRelation.L_EXTRACT, extract));
        crit.addOrder(Order.asc("b." + EmployeeOrderBasics.P_TITLE));
        return crit.list();
    }

    @Override
    public EmployeeListParagraph getFirstParagraph(EmployeeListOrder order)
    {
        MQBuilder builder = new MQBuilder(EmployeeListParagraph.ENTITY_CLASS, "p");
        builder.add(MQExpression.eq("p", IAbstractParagraph.L_ORDER, order));

        int resultCount = new Long(builder.getResultCount(getSession())).intValue();

        if(resultCount == 0) return null;
        else if (resultCount == 1) return (EmployeeListParagraph) builder.uniqueResult(getSession());
        else return (EmployeeListParagraph) builder.getResultList(getSession(), 0, 1).get(0);
    }

    @Override
    public <T extends ListEmployeeExtract> List<T> getExtractList(EmployeeListOrder order)
    {
        MQBuilder builder = new MQBuilder(ListEmployeeExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", IAbstractExtract.L_PARAGRAPH
                + "." + IAbstractParagraph.L_ORDER, order));
        return builder.getResultList(getSession());
    }

    @Override
    public EmployeeExtractType getExtractTypeByOrderType(EmployeeExtractType orderType)
    {
        MQBuilder builder = new MQBuilder(EmployeeExtractType.ENTITY_CLASS, "et");
        builder.add(MQExpression.eq("et", EmployeeExtractType.L_PARENT, orderType));
        List<EmployeeExtractType> list = builder.getResultList(getSession());

        if(list.isEmpty()) throw new IllegalStateException(); // There is no any child extract types for given order type
        if(list.size() > 1) throw new IllegalStateException(); // There are more than one child extract types for given order type
        return list.get(0);
    }

    @Override
    public List<EmployeeExtractType> getExtractTypeListByOrderType(EmployeeExtractType orderType)
    {
        MQBuilder builder = new MQBuilder(EmployeeExtractType.ENTITY_CLASS, "et");
        builder.add(MQExpression.eq("et", EmployeeExtractType.L_PARENT, orderType));
        List<EmployeeExtractType> list = builder.getResultList(getSession());

        if(list.isEmpty()) throw new IllegalStateException();
        return list;
    }

    @Override
    public List<EmployeeOrderBasics> getReasonToBasicsList(final EmployeeOrderReasons reason)
    {
        final MQBuilder builder = new MQBuilder(EmployeeReasonToBasicRel.ENTITY_CLASS, "rel", new String[] {IEntityRelation.L_SECOND});
        builder.add(MQExpression.eq("rel", IEntityRelation.L_FIRST, reason));
        builder.addOrder("rel", IEntityRelation.L_SECOND + "." + EmployeeOrderBasics.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public List<EmployeeExtractType> getReasonToDocumentTypeList(final EmployeeOrderReasons reason)
    {
        final MQBuilder builder = new MQBuilder(EmployeeReasonToTypeRel.ENTITY_CLASS, "rel", new String[] {IEntityRelation.L_SECOND});
        builder.add(MQExpression.eq("rel", IEntityRelation.L_FIRST, reason));
        builder.addOrder("rel", IEntityRelation.L_SECOND + "." + EmployeeExtractType.parent().title().s());
        builder.addOrder("rel", IEntityRelation.L_SECOND + "." + EmployeeExtractType.title().s());
        return builder.getResultList(getSession());
    }

    @Override
    public String getPaymentFormatString(String documentCode, Payment payment)
    {
        List<PaymentStringFormatDocumentSettings> documentTypeList = new ArrayList();
        PaymentStringFormatDocumentSettings documentSettings = getByNaturalId(new PaymentStringFormatDocumentSettings.NaturalId(documentCode));

        if (documentSettings != null)
        {
            do
                documentTypeList.add(0, documentSettings);
            while (null != (documentSettings = documentSettings.getParent()));
        }

        List<PaymentType> paymentTypes = new ArrayList();
        PaymentType paymentType = payment.getType();
        do
            paymentTypes.add(0, paymentType);
        while(null != (paymentType = paymentType.getParent()));

        MQBuilder builder = new MQBuilder(PaymentStringRepresentationFormat.ENTITY_CLASS, "rf");
        builder.add(MQExpression.eq("rf", PaymentStringRepresentationFormat.active(), Boolean.TRUE));

        builder.addLeftJoin("rf", PaymentStringRepresentationFormat.documentType(), "et");
        builder.add(MQExpression.or(
            MQExpression.isNull("rf", PaymentStringRepresentationFormat.documentType()),
            MQExpression.in("rf", PaymentStringRepresentationFormat.documentType(), documentTypeList)));

        builder.addLeftJoin("rf", PaymentStringRepresentationFormat.paymentType(), "pt");
        builder.add(MQExpression.or(
            MQExpression.isNull("rf", PaymentStringRepresentationFormat.paymentType()),
            MQExpression.in("rf", PaymentStringRepresentationFormat.paymentType(), paymentTypes)));

        builder.addLeftJoin("rf", PaymentStringRepresentationFormat.payment(), "p");
        builder.add(MQExpression.or(
            MQExpression.isNull("rf", PaymentStringRepresentationFormat.payment()),
            MQExpression.eq("rf", PaymentStringRepresentationFormat.payment(), payment)));

        List<PaymentStringRepresentationFormat> formats = builder.getResultList(getSession());
        PaymentStringRepresentationFormat bestFormat = null;
        int iBestWeight = -1;
        for(PaymentStringRepresentationFormat format : formats)
        {
            int iWeight = getFormatWeight(format, documentTypeList, paymentTypes, payment);
            if (iWeight > iBestWeight)
            {
                iBestWeight = iWeight;
                bestFormat = format;
            }
        }

        return null == bestFormat ? null : bestFormat.getFormatString();
    }

    protected static int getFormatWeight(PaymentStringRepresentationFormat format, List<PaymentStringFormatDocumentSettings> extractTypes,
        List<PaymentType> paymentTypes, Payment payment)
    {
        int iTotalWeight = 0;
        int iCurrentStepWeight = 1;

        for(PaymentStringFormatDocumentSettings extractType : extractTypes)
        {
            if (null != format.getDocumentType() && format.getDocumentType() == extractType)
                iTotalWeight += iCurrentStepWeight;
            iCurrentStepWeight *= 2;
        }

        for(PaymentType paymentType : paymentTypes)
        {
            if (null != format.getPaymentType() && format.getPaymentType() == paymentType)
                iTotalWeight += iCurrentStepWeight;
            iCurrentStepWeight *= 2;
        }

        if (null != format.getPayment() && format.getPayment() == payment)
            iTotalWeight += iCurrentStepWeight;

        return iTotalWeight;
    }

    @Override
    public List<FinancingSourceDetails> getExtractFinancingSourceDetails(AbstractEmployeeExtract extract)
    {
        MQBuilder builder = new MQBuilder(FinancingSourceDetails.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", FinancingSourceDetails.L_EXTRACT, extract));

        return builder.getResultList(getSession());
    }

    @Override
    public List<FinancingSourceDetailsToAllocItem> getFinSrcDetToAllocItemRelation(FinancingSourceDetails financingSourceDetails)
    {
        MQBuilder builder = new MQBuilder(FinancingSourceDetailsToAllocItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", FinancingSourceDetailsToAllocItem.financingSourceDetails().s(), financingSourceDetails));

        return builder.getResultList(getSession());
    }

    @Override
    public List<FinancingSourceDetailsToAllocItem> getFinSrcDetToAllocItemRelation(List<FinancingSourceDetails> financingSourceDetailsList)
    {
        MQBuilder builder = new MQBuilder(FinancingSourceDetailsToAllocItem.ENTITY_CLASS, "b");
        builder.add(MQExpression.in("b", FinancingSourceDetailsToAllocItem.financingSourceDetails().s(), financingSourceDetailsList));

        return builder.getResultList(getSession());
    }

    @Override
    public List<ProvideDateToExtractRelation> getProvideHolidayFromRevocationHolidayExtract(RevocationFromHolidayExtract extract)
    {
        MQBuilder builder = new MQBuilder(ProvideDateToExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", ProvideDateToExtractRelation.L_REVOCATION_FROM_HOLIDAY_EXTRACT, extract));

        return builder.getResultList(getSession());
    }

    @Override
    public List<PeriodPartToExtractRelation> getPeriodPartFromExtractRelation(TransferAnnualHolidayExtract extract)
    {
        MQBuilder builder = new MQBuilder(PeriodPartToExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", PeriodPartToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, extract));

        return builder.getResultList(getSession());
    }

    @Override
    public List<TransferHolidayDateToExtractRelation> getTransferHolidayDateFromExtractRelation(TransferAnnualHolidayExtract extract)
    {
        MQBuilder builder = new MQBuilder(TransferHolidayDateToExtractRelation.ENTITY_CLASS, "bb");
        builder.add(MQExpression.eq("bb", TransferHolidayDateToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, extract));

        return builder.getResultList(getSession());
    }

    @Override
    public List<NewVacationScheduleToExtractRelation> getNewVacationScheduleFromExtractRelation(TransferAnnualHolidayExtract extract)
    {
        MQBuilder builder = new MQBuilder(NewVacationScheduleToExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", NewVacationScheduleToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, extract));

        return builder.getResultList(getSession());
    }

    @Override
    public List<EncouragementType> getEncouragementTypeFromExtractList(EmployeeEncouragementSExtract extract)
    {
        MQBuilder builder = new MQBuilder(EncouragementToExtractRelation.ENTITY_CLASS, "b", new String[]{EncouragementToExtractRelation.L_ENCOURAGEMENT_TYPE});
        builder.add(MQExpression.eq("b", EncouragementToExtractRelation.L_EMPLOYEE_ENCOURAGEMENT_S_EXTRACT, extract));

        return builder.getResultList(getSession());
    }

    @Override
    public Map<EncouragementType, String> getEncouragementTypePrintFormat(List<EncouragementType> list)
    {
        MQBuilder builder = new MQBuilder(EncouragementToPrintFormatRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.in("b", EncouragementToPrintFormatRelation.L_ENCOURAGEMENT_TYPE, list));
        builder.addOrder("b", EncouragementToPrintFormatRelation.encouragementType().title().s(), OrderDirection.asc);

        Map<EncouragementType, String> relationMap = new LinkedHashMap();
        for (EncouragementToPrintFormatRelation relation : builder.<EncouragementToPrintFormatRelation>getResultList(getSession()))
            relationMap.put(relation.getEncouragementType(), relation.getPrintFormat());

        Map<EncouragementType, String> resultMap = new LinkedHashMap<>();
        for (EncouragementType type : list)
            resultMap.put(type, relationMap.get(type));

        return resultMap;
    }

    @Override
    public List<HolidayInEmpHldyExtractRelation> getHolidayInEmpHldyExtractRelation(EmployeeHolidayExtract extract)
    {
        MQBuilder builder = new MQBuilder(HolidayInEmpHldyExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", HolidayInEmpHldyExtractRelation.L_EMPLOYEE_HOLIDAY_EXTRACT, extract));
        builder.addOrder("b", HolidayInEmpHldyExtractRelation.P_BEGIN_PERIOD);
        builder.addOrder("b", HolidayInEmpHldyExtractRelation.P_START_HOLIDAY);

        return builder.getResultList(getSession());
    }

    @Override
    public List<HolidayInEmployeeExtractRelation> getHolidayInEmployeeExtractRelation(AbstractEmployeeExtract extract)
    {
        MQBuilder builder = new MQBuilder(HolidayInEmployeeExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", HolidayInEmployeeExtractRelation.L_EMPLOYEE_HOLIDAY_EXTRACT, extract));
        builder.addOrder("b", HolidayInEmployeeExtractRelation.P_BEGIN_PERIOD);
        builder.addOrder("b", HolidayInEmployeeExtractRelation.P_START_HOLIDAY);

        return builder.getResultList(getSession());
    }

    @Override
    public List<CombinationPostStaffRateExtractRelation> getCombinationPostStaffRateExtractRelationList(AbstractEmployeeExtract extract)
    {
        MQBuilder builder = new MQBuilder(CombinationPostStaffRateExtractRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract().s(), extract));

        return builder.<CombinationPostStaffRateExtractRelation>getResultList(getSession());
    }

    @Override
    public List<CombinationPostStaffRateExtAllocItemRelation> getCombinationPostStaffRateExtAllocItemRelationList(CombinationPostStaffRateExtractRelation relation)
    {
        MQBuilder builder = new MQBuilder(CombinationPostStaffRateExtAllocItemRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation().s(), relation));

        return builder.<CombinationPostStaffRateExtAllocItemRelation>getResultList(getSession());
    }

    @Override
    public List<EmployeePayment> getCommitExtractActualEmployeePaymentList(AbstractEmployeeExtract extract, List<Payment> paymentList, Date beginDate)
    {
        if (beginDate == null)
            return Collections.emptyList();

        DQLSelectBuilder ebBuilder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "eb").column(DQLExpressions.property(EmployeeBonus.entity().id().fromAlias("eb")));
        ebBuilder.where(DQLExpressions.isNotNull(DQLExpressions.property(EmployeeBonus.entity().fromAlias("eb"))));
        ebBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeBonus.extract().fromAlias("eb")), extract));

        List<Long> excludeEmployeePaymentIdList = ebBuilder.createStatement(getSession()).list();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePayment.class, "b").column("b");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePayment.employeePost().fromAlias("b")), extract.getEntity()));
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePayment.payment().oneTimePayment().fromAlias("b")), false));
        builder.where(DQLExpressions.le(DQLExpressions.property(EmployeePayment.beginDate().fromAlias("b")), DQLExpressions.valueDate(beginDate)));
        IDQLExpression ge = DQLExpressions.ge(DQLExpressions.property(EmployeePayment.endDate().fromAlias("b")), DQLExpressions.valueDate(beginDate));
        IDQLExpression isNull = DQLExpressions.isNull(DQLExpressions.property(EmployeePayment.endDate().fromAlias("b")));
        builder.where(DQLExpressions.or(isNull, ge));

        if (!excludeEmployeePaymentIdList.isEmpty())
            builder.where(DQLExpressions.notIn(DQLExpressions.property(EmployeePayment.id().fromAlias("b")), excludeEmployeePaymentIdList));

        if (paymentList != null && !paymentList.isEmpty())
            builder.where(DQLExpressions.in(DQLExpressions.property(EmployeePayment.payment().fromAlias("b")), paymentList));

        return builder.createStatement(getSession()).list();
    }

    public void updateStaffListAllocItemChanges(AbstractEmployeeExtract extract, EmployeePost post, List<StaffListAllocationItem> allocItemList)
    {
        List<FinancingSourceDetails> staffRateItems = getExtractFinancingSourceDetails(extract);
        List<FinancingSourceDetailsToAllocItem> relList = getFinSrcDetToAllocItemRelation(staffRateItems);
        if (staffRateItems.size() != 0 && relList.size() != 0)
        {
            for (FinancingSourceDetailsToAllocItem rel : relList)
                if (!rel.isHasNewAllocItem())
                {
                    if (rel.getChoseStaffListAllocationItem() != null)
                        delete(rel.getChoseStaffListAllocationItem());
                    rel.setChoseStaffListAllocationItem(null);
                    update(rel);
                }

            for (StaffListAllocationItem oldAllocationItem : UniempDaoFacade.getUniempDAO().getEmployeePostAllocationItemList(post))
                delete(oldAllocationItem);

            for ( StaffListAllocationItem allocationItem : allocItemList)
            {
                save(allocationItem);
                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());
            }
        }
    }


    public void rollbackStaffListAllocItemChanges(AbstractEmployeeExtract extract)
    {
        List<FinancingSourceDetails> staffRateItems = getExtractFinancingSourceDetails(extract);
        List<FinancingSourceDetailsToAllocItem> relList = getFinSrcDetToAllocItemRelation(staffRateItems);

        if (staffRateItems.size() != 0 && relList.size() != 0)
        {
            for (StaffListAllocationItem oldAllocationItem : UniempDaoFacade.getUniempDAO().getEmployeePostAllocationItemList(extract.getEntity()))
                delete(oldAllocationItem);

            for (FinancingSourceDetailsToAllocItem relation : relList)
            {
                if (!relation.isHasNewAllocItem())
                {
                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(extract.getEntity().getOrgUnit(), extract.getEntity().getPostRelation().getPostBoundedWithQGandQL(), relation.getFinancingSourceDetails().getFinancingSource(), relation.getFinancingSourceDetails().getFinancingSourceItem());

                    StaffListAllocationItem allocationItem = new StaffListAllocationItem();

                    allocationItem.setStaffRate(relation.getStaffRateHistory());
                    allocationItem.setFinancingSource(relation.getFinancingSourceDetails().getFinancingSource());
                    allocationItem.setFinancingSourceItem(relation.getFinancingSourceDetails().getFinancingSourceItem());
                    allocationItem.setStaffListItem(staffListItem);
                    allocationItem.setEmployeePost(relation.getEmployeePostHistory());
                    if (allocationItem.getEmployeePost() != null)
                        allocationItem.setRaisingCoefficient(relation.getEmployeePostHistory().getRaisingCoefficient());
                    if (allocationItem.getRaisingCoefficient() != null)
                        allocationItem.setMonthBaseSalaryFund(allocationItem.getRaisingCoefficient().getRecommendedSalary() * allocationItem.getStaffRate());
                    else if (allocationItem.getStaffListItem() != null)
                        allocationItem.setMonthBaseSalaryFund(allocationItem.getStaffListItem().getSalary() * allocationItem.getStaffRate());

                    save(allocationItem);
                    StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());

                    if (relation.getChoseStaffListAllocationItem() == null)
                        relation.setChoseStaffListAllocationItem(allocationItem);

                    update(relation);
                }

            }
        }
    }


    public void createOrUpdateContractAndAgreement(IContractAndAssigmentExtract extract, String comment)
    {
        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());

        ContractCollateralAgreement agreement = null;

        if (contract != null)
        {
            for (ContractCollateralAgreement collateralAgreement : UniempDaoFacade.getUniempDAO().getContractCollateralAgreementsList(contract))
                if (collateralAgreement.getNumber().equals(extract.getContractAddAgreementNumber()) && collateralAgreement.getDate().equals(extract.getContractAddAgreementDate()))
                    agreement = collateralAgreement;
        }

        if (contract == null)
        {
            EmployeeLabourContract labourContract = new EmployeeLabourContract();
            labourContract.setDate(extract.getContractDate());
            labourContract.setBeginDate(extract.getContractBeginDate());
            labourContract.setEndDate(extract.getContractEndDate());
            labourContract.setType(extract.getContractType());
            labourContract.setNumber(extract.getContractNumber());
            labourContract.setEmployeePost(extract.getEntity());

            extract.setCreateEmployeeContract(labourContract);

            contract = labourContract;

            save(labourContract);
        }

        if (agreement == null)
        {
            agreement = new ContractCollateralAgreement();

            agreement.setContract(contract);
            agreement.setDate(extract.getContractAddAgreementDate());
            agreement.setNumber(extract.getContractAddAgreementNumber());
            agreement.setDescription(comment);

            extract.setCreateCollateralAgreement(agreement);

            save(agreement);
        }
    }
}
