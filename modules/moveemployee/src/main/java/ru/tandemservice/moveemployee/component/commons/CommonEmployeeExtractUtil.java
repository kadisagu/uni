/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.hibernate.Session;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * @author dseleznev
 *         Created on: 02.06.2010
 */
@SuppressWarnings("deprecation")
public class CommonEmployeeExtractUtil
{
    /**
     * Минимальное количество элементов, отображаемых в списке выплат
     */
    private static final int FIXED_MIN_SEARCHLIST_ELEMENTS_NUMBER = 5;

    /**
     * Иднентификатор поля "Доля ставки Бюджет"
     */
    public static final String BUDGET_STAFF_RATE_FIELD_ID = "budgetStaffRateEdt";

    /**
     * Иднентификатор поля "Доля ставки Вне бюджета"
     */
    public static final String OFF_BUDGET_STAFF_RATE_FIELD_ID = "offBudgetStaffRateEdt";

    /**
     * Префикс иднентификатора для поля в таблице детализированных долей ставок по источникам финансирования
     */
    public static final String FINANCING_SOURCE_ITEM_DETAIL_FIELD_ID_PREFIX = "finSrcItem_id_";

    /**
     * Компаратор, выстраивающий элементы штатной расстановки в соответствии с их совпадением
     * по выплатам с требуемыми
     */
    public static final Comparator<EmployeeBonus> PRIORITY_COMPARATOR = (o1, o2) -> o1.getPriority() - o2.getPriority();

    /**
     * Подготавливает данные в модели компонента добавления выписки о приеме (назначении) сотрудника на должность.
     *
     * @param model - модель, включающая в себя набор методов, описанных в интерфейсе IEmployeePostModel;
     */
    public static void createPostBoundedModel(final IEmployeePostModel<? extends IPostAssignExtract> model)
    {
        final IUniBaseDao coreDao = UniDaoFacade.getCoreDao();
        model.setOrgUnitListModel(new OrgUnitAutocompleteModel());
        model.setPostTypesList(coreDao.getCatalogItemList(PostType.class));
        model.setEtksLevelsList(coreDao.getCatalogItemListOrderByCode(EtksLevels.class));
        model.setCompetitionTypeList(coreDao.getCatalogItemList(CompetitionAssignmentType.class));
        model.setWeekWorkLoadsList(coreDao.getCatalogItemListOrderByCode(EmployeeWeekWorkLoad.class));
        model.setWorkWeekDurationsList(coreDao.getCatalogItemListOrderByCode(EmployeeWorkWeekDuration.class));
        model.setLabourContractTypesList(coreDao.getCatalogItemList(LabourContractType.class));

        model.setPostRelationListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getExtract().getOrgUnit() == null)
                {
                    return ListResult.getEmpty();
                }

                int maxCount = UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnitCount(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), filter, null, null);
                List<PostBoundedWithQGandQL> resultList = new ArrayList<>();
                for (OrgUnitTypePostRelation postRelation : UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnit(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), filter, null, null, 50))
                    resultList.add(postRelation.getPostBoundedWithQGandQL());
                return new ListResult<>(resultList, maxCount);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                PostBoundedWithQGandQL entity = coreDao.get(PostBoundedWithQGandQL.class, (Long) primaryKey);
                if (null == entity || null == model.getExtract().getOrgUnit())
                    return null;

                if (findValues(entity.getTitle()).getObjects().contains(entity))
                    return entity;

                if (model.getExtract() instanceof IEmployeePostExtract)
                    model.getExtract().setPostBoundedWithQGandQL(null);

                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((PostBoundedWithQGandQL) value).getFullTitleWithSalary();
            }
        });

        model.setRaisingCoefficientListModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (null == model.getExtract().getPostBoundedWithQGandQL())
                {
                    return ListResult.getEmpty();
                }

                return new ListResult<>(MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getRaisingCoefficientsList(model.getExtract().getPostBoundedWithQGandQL(), filter));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = coreDao.get(SalaryRaisingCoefficient.class, (Long) primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                model.getExtract().setRaisingCoefficient(null);
                model.getExtract().setSalary(0d);
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((SalaryRaisingCoefficient) value).getFullTitle();
            }
        });

        if (model.isAddForm())
        {
            model.getExtract().setWeekWorkLoad(EmployeeManager.instance().dao().getDefaultWeekWorkLoad());
            model.getExtract().setWorkWeekDuration(EmployeeManager.instance().dao().getDefaultWorkWeekDuration());
        }
    }

    /**
     * Возвращает список элементов детализации, заполняет данные по долям ставок
     */
    @SuppressWarnings("unchecked")
    public static List<FinancingSourceItem> getFinancingSourceDetailsItemsList(AbstractEmployeeExtract extract, IValueMapHolder staffRatesValueMapHolder)
    {
        List<FinancingSourceItem> result = new ArrayList<>();
        for (FinancingSourceDetails item : UniDaoFacade.getCoreDao().getList(FinancingSourceDetails.class, FinancingSourceDetails.extract().s(), extract))
        {
            result.add(item.getFinancingSourceItem());
            staffRatesValueMapHolder.getValueMap().put(item.getFinancingSourceItem().getId(), item.getStaffRate());
        }
        return result;
    }

    /**
     * Обработчик события смены должности или подразделения
     */
    @SuppressWarnings("unchecked")
    public static void onChangePost(IPostAssignExtract extract, DynamicListDataSource<FinancingSourceDetails> dataSource, List<FinancingSourceDetails> detailsList)
    {
        if (null == extract.getOrgUnit())
        {
            extract.setPostBoundedWithQGandQL(null);
        }

        if (null != extract.getPostBoundedWithQGandQL() && (null != extract.getRaisingCoefficient() || null != extract.getPostBoundedWithQGandQL().getSalary()))
        {
            final IValueMapHolder staffRateHolder = (IValueMapHolder) dataSource.getColumn("staffRate");
            final Map<Long, Object> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

            Double multiplier = 0.0d;
            for (FinancingSourceDetails staffRate : detailsList)
            {
                Double val = 0.0d;
                Object value = staffRateMap.get(staffRate.getId());
                if (null != value)
                {
                    if (value instanceof Long)
                        val = ((Long) value).doubleValue();
                    else
                        val = (Double) value;
                }


                multiplier += val;
            }

            PostBoundedWithQGandQL postBounded = extract.getPostBoundedWithQGandQL();
            SalaryRaisingCoefficient coefficient = extract.getRaisingCoefficient();
            Double salary = Math.round((null != coefficient ? coefficient.getRecommendedSalary() : (null != postBounded.getSalary() ? postBounded.getSalary() : 0d)) * multiplier * 100) / 100d;

            extract.setSalary(salary);
            extract.setEtksLevels(postBounded.getEtksLevels());
        }
        // TODO: удалить все добавленные выплаты ?
        //CommonExtractUtil.onStaffRateForPaymentChange(model);
    }

    /**
     * Производит валидацию введенных долей ставок при разбивке по источникам финансирования детально
     */
//    public static void validateFinancingSourceItemsForStaffRates(ErrorCollector errors, IStaffRatesExtract extract, IValueMapHolder staffRateItemsValueHolder, List<FinancingSourceItem> selectedFinSrcItemsList)
//    {
//        if (selectedFinSrcItemsList.isEmpty() || null == staffRateItemsValueHolder) return;
//
//        double sumBudgetStaffRate = 0d;
//        double sumOffBudgetStaffRate = 0d;
//        List<Long> budgetSrcIdsList = new ArrayList<Long>();
//        List<Long> offBudgetSrcIdsList = new ArrayList<Long>();
//
//        for (FinancingSourceItem item : selectedFinSrcItemsList)
//        {
//            Object preValue = staffRateItemsValueHolder.getValueMap().get(item.getId());
//            Double value = (preValue instanceof Double) ? (Double) preValue : ((Long) preValue).doubleValue();
//
//            if (null == item.getFinancingSource() || !UniempDefines.FINANCING_SOURCE_BUDGET.equals(item.getFinancingSource().getCode()))
//            {
//                sumOffBudgetStaffRate += value;
//                offBudgetSrcIdsList.add(item.getId());
//            }
//            else
//            {
//                sumBudgetStaffRate += value;
//                budgetSrcIdsList.add(item.getId());
//            }
//        }
//
//        if (budgetSrcIdsList.size() > 0 && sumBudgetStaffRate != extract.getBudgetStaffRate())
//        {
//            List<String> errFieldIdsList = new ArrayList<String>();
//            errFieldIdsList.add(BUDGET_STAFF_RATE_FIELD_ID);
//            for (Long errField : budgetSrcIdsList) errFieldIdsList.add(FINANCING_SOURCE_ITEM_DETAIL_FIELD_ID_PREFIX + errField);
//            errors.add("Сумма всех долей ставок по бюджету должна быть равной доле ставки по бюджету.", errFieldIdsList.toArray(new String[] {}));
//        }
//
//        if (offBudgetSrcIdsList.size() > 0 && sumOffBudgetStaffRate != extract.getOffBudgetStaffRate())
//        {
//            List<String> errFieldIdsList = new ArrayList<String>();
//            errFieldIdsList.add(OFF_BUDGET_STAFF_RATE_FIELD_ID);
//            for (Long errField : offBudgetSrcIdsList) errFieldIdsList.add(FINANCING_SOURCE_ITEM_DETAIL_FIELD_ID_PREFIX + errField);
//            errors.add("Сумма всех долей ставок вне бюджета должна быть равной доле ставки вне бюджета.", errFieldIdsList.toArray(new String[] {}));
//        }
//    }

    /**
     * Инициализирует переменные и списки, относящиеся к единым механизмам добавления выплат в приказах
     */
    //@SuppressWarnings("unchecked")
    @Deprecated
    public static void createExtendedPaymentsBonusModel(IExtractPaymentsModel<? extends IPostAssignExtract> model)
    {
        IUniBaseDao coreDao = UniDaoFacade.getCoreDao();

        /*model.setPaymentAddingCasesList(Arrays.asList(
                new IdentifiableWrapper(IExtractPaymentsModel.PAYMENT_ADDING_CASE_ADD_SINGLE_PAYMENT, "Вручную"),
                new IdentifiableWrapper(IExtractPaymentsModel.PAYMENT_ADDING_CASE_ADD_STAFF_LIST_PAYMENTS, "Согласно штатному расписанию")
                ));*/
        //model.setPaymentAddingCase(model.getPaymentAddingCasesList().get(1));

        model.setNewEmployeeBonus(new EmployeeBonus());
        model.setStaffListPaymentsList(new ArrayList<>());

        model.setPaymentListModel(getPaymentAutocompleteModel(model));
        model.setFinancingSourcesList(coreDao.getCatalogItemList(FinancingSource.class));

        model.setFinancingSourceItemsListModel(new BaseSingleSelectModel(FinancingSourceItem.P_FULL_TITLE)
        {
            @Override
            public ListResult<FinancingSourceItem> findValues(String filter)
            {
                return MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getFinancinSourceItemsListResult(filter);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return UniDaoFacade.getCoreDao().get(FinancingSourceItem.class, (Long) primaryKey);
            }
        });

        model.setAlreadyAddedPayments(new ArrayList<>());
        if (!model.isAddForm())
        {
            model.setBonusesList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(model.getExtract()));
            for (EmployeeBonus bonus : model.getBonusesList())
            {
                model.getAlreadyAddedPayments().add(bonus.getPayment());
            }
        }

        onRefreshPaymentCases(model);
        //model.setAddStaffListPaymentsBlockVisible(model.getStaffListPaymentsList().size() > 0);
    }

    /**
     * Инициализирует переменные и списки, относящиеся к единым механизмам добавления выплат в приказах
     */
    @Deprecated
    public static void createPaymentsBonusModel(IPaymentAddModel<? extends AbstractEmployeeExtract> model)
    {
        IUniBaseDao coreDao = UniDaoFacade.getCoreDao();

        model.setNewEmployeeBonus(new EmployeeBonus());

        model.setPaymentListModel(getPaymentAutocompleteModel(model));
        model.setFinancingSourcesList(coreDao.getCatalogItemList(FinancingSource.class));
        model.setFinancingSourceItemsListModel(new BaseSingleSelectModel(FinancingSourceItem.P_FULL_TITLE)
        {
            @Override
            public ListResult<FinancingSourceItem> findValues(String filter)
            {
                return MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getFinancinSourceItemsListResult(filter);
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return UniDaoFacade.getCoreDao().get(FinancingSourceItem.class, (Long) primaryKey);
            }
        });

        model.setAlreadyAddedPayments(new ArrayList<>());
        if (!model.isAddForm())
        {
            model.setBonusesList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(model.getExtract()));
            for (EmployeeBonus bonus : model.getBonusesList())
            {
                model.getAlreadyAddedPayments().add(bonus.getPayment());
            }
        }
    }

    @Deprecated
    public static void createPaymentsBonusDataSource(IPaymentAddModel<? extends AbstractEmployeeExtract> model, IBusinessComponent component, IListDataSourceDelegate listDataSourceDelegate)
    {
        if (null != model.getBonusesDataSource()) return;

        DynamicListDataSource<EmployeeBonus> dataSource = new DynamicListDataSource<>(component, listDataSourceDelegate);
        dataSource.addColumn(new SimpleColumn("Дата назначения", EmployeeBonus.assignDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Назначена с даты", EmployeeBonus.beginDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Назначена по дату", EmployeeBonus.endDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Наименование", EmployeeBonus.payment().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Размер", EmployeeBonus.value().s(), DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формат ввода", EmployeeBonus.payment().paymentUnit().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeeBonus.financingSource().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeeBonus.financingSourceItem().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Вверх", CommonBaseDefine.ICO_UP, "onClickPaymentUp"));
        dataSource.addColumn(new ActionColumn("Вниз", CommonBaseDefine.ICO_DOWN, "onClickPaymentDown"));
        dataSource.addColumn(new ActionColumn("Исключить", ActionColumn.DELETE, "onClickDeleteBonus", "Исключить из списка выплату «{0}»?", new Object[]{new String[]{EmployeeBonus.payment().s(), Payment.title().s()}}));
        model.setBonusesDataSource(dataSource);
    }

    /**
     * Заполняет список с выплатами
     */
    @Deprecated
    public static void fillPaymentsBonusDataSource(IPaymentAddModel<? extends AbstractEmployeeExtract> model)
    {
        DynamicListDataSource<EmployeeBonus> dataSource = model.getBonusesDataSource();
        dataSource.setCountRow(model.getBonusesList().size() < FIXED_MIN_SEARCHLIST_ELEMENTS_NUMBER ? FIXED_MIN_SEARCHLIST_ELEMENTS_NUMBER : model.getBonusesList().size());
        dataSource.setTotalSize(model.getBonusesList().size());
        dataSource.createPage(model.getBonusesList());
    }

    /**
     * Возвращает селект-модель для селекта с перечнем выплат
     */
    @Deprecated
    public static ISelectModel getPaymentAutocompleteModel(final IExtractPaymentsModel<? extends AbstractEmployeeExtract> model)
    {
        return new FullCheckSelectModel(Payment.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getAvailablePaymentsList(model.getAlreadyAddedPayments(), filter));
            }
        };
    }

    /**
     * Возвращает селект-модель для селекта с перечнем выплат
     */
    @Deprecated
    public static ISelectModel getPaymentAutocompleteModel(final IPaymentAddModel<? extends AbstractEmployeeExtract> model)
    {
        return new FullCheckSelectModel(Payment.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(MoveEmployeeDaoFacade.getMoveEmployeeUtilDao().getAvailablePaymentsList(model.getAlreadyAddedPayments(), filter));
            }
        };
    }

    /**
     * Обработчик кнопки добавления одиночной выплаты
     */
    @Deprecated
    public static void onClickCallAddSinglePaymentForm(IExtractPaymentsModel<? extends IPostAssignExtract> model)
    {
        model.setAddPaymentBlockVisible(true);
        model.setAddStaffListPaymentsBlockVisible(false);
        onRefreshPaymentCases(model);
    }

    /**
     * Обработчик кнопки отмены на формах добавления выплат
     */
    @Deprecated
    public static void onClickCancelAddBonus(IExtractPaymentsModel<? extends IPostAssignExtract> model)
    {
        model.setAddPaymentBlockVisible(false);
        model.setAddStaffListPaymentsBlockVisible(false);
        onRefreshPaymentCases(model);
    }

    /**
     * Обработчик кнопки добавления выплат, согласно штатному расписанию
     */
    @Deprecated
    public static void onClickCallAddStaffListPaymentsForm(IExtractPaymentsModel<? extends IPostAssignExtract> model)
    {
        model.setAddPaymentBlockVisible(false);
        model.setAddStaffListPaymentsBlockVisible(true);
        onRefreshPaymentCases(model);
    }

    /**
     * Обработчик события на смену типа добавления выплат сотруднику приказом
     */
    @Deprecated
    @SuppressWarnings("unchecked")
    public static void onRefreshPaymentCases(IExtractPaymentsModel<? extends IPostAssignExtract> model)
    {
//        Map<FinancingSource, FinancingSourceItem> finSrcMap = new LinkedHashMap<FinancingSource, FinancingSourceItem>();
//        for (FinancingSourceDetails details : model.getStaffRateItemList())
//            finSrcMap.put(details.getFinancingSource(), details.getFinancingSourceItem());

        Set<Payment> staffListPaymentsSet = new HashSet<>();
        if (null != model.getExtract().getOrgUnit() && null != model.getExtract().getPostBoundedWithQGandQL())
        {
            List<StaffListPostPayment> paymentsList = UniempDaoFacade.getStaffListDAO().getStaffListPostPaymentsList(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL());
            if (null != paymentsList)
            {
                for (StaffListPostPayment paym : paymentsList)
                {
                    staffListPaymentsSet.add(paym.getPayment());
                }
            }
        }

        staffListPaymentsSet.removeAll(model.getAlreadyAddedPayments());
        if (!staffListPaymentsSet.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);

        if (model.isAddStaffListPaymentsBlockVisible())
        {
            if (null != model.getExtract().getOrgUnit() && null != model.getExtract().getPostBoundedWithQGandQL())
            {
                List<EmployeeBonus> staffListBonusesList = new ArrayList<>();
                List<StaffListPostPayment> staffListPaymentsList = UniempDaoFacade.getStaffListDAO().getStaffListPostPaymentsList(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL());

                long id = -1;
                for (StaffListPostPayment payment : staffListPaymentsList)
                {
//                    if (!model.getAlreadyAddedPayments().contains(payment.getPayment()))
                    {
                        EmployeeBonus bonus = new EmployeeBonus();
                        bonus.setPayment(payment.getPayment());
                        bonus.setAssignDate(model.getExtract().getBeginDate());
                        bonus.setFinancingSource(payment.getFinancingSource());
                        bonus.setValue(payment.getAmount());
                        bonus.setExtract(model.getExtract());
                        bonus.setId(id--);

//                        if (!model.getStaffListPaymentsList().contains(bonus))
                            staffListBonusesList.add(bonus);
//                        else
//                            staffListBonusesList.add(model.getStaffListPaymentsList().get(model.getStaffListPaymentsList().indexOf(bonus)));
                    }
                }

                model.setStaffListPaymentsList(staffListBonusesList);
            }

            model.setAddStaffListPaymentsBlockVisible(!model.getStaffListPaymentsList().isEmpty());
        }
    }

    /**
     * Обработчик события на изменения выплаты при добавлении выплат вручную
     */
    @Deprecated
    public static void onPaymentChange(final IPaymentAddModel<? extends AbstractEmployeeExtract> model, Double summStaffRate)
    {
        if (summStaffRate == null)
            summStaffRate = 1d;

        EmployeeBonus bonus = model.getNewEmployeeBonus();
        if (null != bonus.getPayment())
        {
            double paymentValue = 0d;
            if (model.getExtract() instanceof IPostAssignExtract)
            {
                IPostAssignExtract extract = (IPostAssignExtract) model.getExtract();
                PostBoundedWithQGandQL postBounded = extract.getPostBoundedWithQGandQL();
                paymentValue = UniempDaoFacade.getUniempDAO().getPaymentBaseValueForPost(bonus.getPayment(), postBounded);
            }

            if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(bonus.getPayment().getPaymentUnit().getCode()))
                bonus.setValue(paymentValue * summStaffRate);
            else
                bonus.setValue(paymentValue);

            if (bonus.getPayment().isFixedValue())
                bonus.setValueProxy(bonus.getPayment().getValue());

            bonus.setFinancingSource(bonus.getPayment().getFinancingSource());
        } else
        {
            bonus.setValue(0d);
            bonus.setValueProxy(0d);
            bonus.setFinancingSource(null);
        }
    }

    /**
     * Отвечает за видимость поля "Источник финансирования (детально)" на формах добавления/редактирования выписок
     */
    public static boolean isFinancingOrgUnitVisible(IPaymentAddModel<? extends AbstractEmployeeExtract> model)
    {
        return true;
        /*if (null != model.getNewEmployeeBonus().getFinancingSource() && UniempDefines.FINANCING_SOURCE_OFF_BUDGET.equals(model.getNewEmployeeBonus().getFinancingSource().getCode()))
            return true;
        return false;*/
    }

    /**
     * Обработчик события на удаление выплаты из списка добавленных выплат в приказе
     */
    @Deprecated
    public static void onClickRemoveBonus(IPaymentAddModel<? extends AbstractEmployeeExtract> model, long bonusId)
    {
        int priority = 1;
        EmployeeBonus bonusToRemove = null;
        for (EmployeeBonus bonus : model.getBonusesList())
        {
            if (bonus.getId() == bonusId) bonusToRemove = bonus;
            else bonus.setPriority(priority++);
        }

        if (null != bonusToRemove && bonusToRemove.getId() > 0)
        {
            model.getBonusesToDel().add(bonusToRemove);
            model.getAlreadyAddedPayments().remove(bonusToRemove.getPayment());
        }
        model.getBonusesList().remove(bonusToRemove);
    }

    /**
     * Обработчик события на удаление выплаты из списка добавленных выплат в приказе
     */
    @Deprecated
    public static void onClickRemoveBonusExtended(IExtractPaymentsModel<? extends IPostAssignExtract> model, long bonusId)
    {
        onClickRemoveBonus(model, bonusId);
        onRefreshPaymentCases(model);
    }

    /**
     * Обработчик события смены порядка следования выплат в списке выплат приказа
     */
    @Deprecated
    public static void onClickPaymentUpOrDown(IPaymentAddModel<? extends AbstractEmployeeExtract> model, long bonusId, boolean up)
    {
        EmployeeBonus bonusToMove;
        EmployeeBonus bonusToMoveInstead;

        for (EmployeeBonus bonus : model.getBonusesList())
        {
            if (bonus.getId() == bonusId)
            {
                bonusToMove = bonus;
                int bonusIndex = model.getBonusesList().indexOf(bonus);
                bonusToMoveInstead = up ? (bonusIndex > 0 ? model.getBonusesList().get(bonusIndex - 1) : null) : (bonusIndex < model.getBonusesList().size() - 1 ? model.getBonusesList().get(bonusIndex + 1) : null);
                if (null != bonusToMoveInstead)
                {
                    int indexBuffer = bonusToMove.getPriority();
                    bonusToMove.setPriority(bonusToMoveInstead.getPriority());
                    bonusToMoveInstead.setPriority(indexBuffer);
                    break;
                }
            }
        }

        Collections.sort(model.getBonusesList(), PRIORITY_COMPARATOR);
    }

    /**
     * Обработчик кнопки добавления выплат согласно штатного расписания
     */
    @Deprecated
    public static void onClickAddStaffListBonuses(IExtractPaymentsModel<? extends IPostAssignExtract> model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        for (EmployeeBonus bonus : model.getStaffListPaymentsList())
        {
            if (bonus.getPayment().isOneTimePayment())
            {
                bonus.setBeginDate(null);
                bonus.setEndDate(null);
            }

            if (bonus.isIncludeBonus())
            {
                if (null == bonus.getAssignDate())
                    errCollector.add("Поле «Дата назначения» обязательно для заполнения.", getGeneratedFieldId("staffListPaymentAssignDate", bonus.getId()));

                if (!bonus.getPayment().isOneTimePayment() && null == bonus.getBeginDate())
                    errCollector.add("Поле «Назначена с даты» обязательно для заполнения.", getGeneratedFieldId("staffListPaymentBeginDate", bonus.getId()));

                if (null != bonus.getBeginDate() && null != bonus.getEndDate() && bonus.getBeginDate().getTime() > bonus.getEndDate().getTime())
                    errCollector.add("Дата начала выплаты должна быть раньше даты её окончания.", getGeneratedFieldId("staffListPaymentBeginDate", bonus.getId()), getGeneratedFieldId("staffListPaymentEndDate", bonus.getId()));
            }
        }

        long id = 0L;
        int priority = 0;
        for (EmployeeBonus bonus : model.getBonusesList())
        {
            if (bonus.getId() < id) id = bonus.getId();
            if (bonus.getPriority() > priority) priority = bonus.getPriority();
        }

        id--;
        priority++;

        if (!errCollector.hasErrors())
        {
            for (EmployeeBonus bonus : model.getStaffListPaymentsList())
            {
                if (bonus.isIncludeBonus())
                {
                    bonus.setId(id--);
                    bonus.setPriority(priority++);
                    model.getBonusesList().add(bonus);
//                    model.getAlreadyAddedPayments().add(bonus.getPayment());
                }
            }
            model.getStaffListPaymentsList().clear();
            model.setAddStaffListPaymentsBlockVisible(false);
            onRefreshPaymentCases(model);
        }
    }

    /**
     * Обработчик кнопки добавления выплаты вручную
     */
    @Deprecated
    public static void onClickAddBonus(IPaymentAddModel<? extends AbstractEmployeeExtract> model)
    {
        EmployeeBonus bonus = model.getNewEmployeeBonus();
        validateSinglePaymentBlock(bonus);

        if (!UserContext.getInstance().getErrorCollector().hasErrors())
        {
            if (bonus.getPayment().isOneTimePayment())
            {
                bonus.setBeginDate(null);
                bonus.setEndDate(null);
            }

            long id = -1;
            int maxPriority = 0;
            for (EmployeeBonus addedBonus : model.getBonusesList())
            {
                if (addedBonus.getId() < id) id = addedBonus.getId();
                if (addedBonus.getPriority() > maxPriority) maxPriority = addedBonus.getPriority();
            }

            bonus.setId(--id);
            bonus.setPriority(++maxPriority);
            model.getBonusesList().add(bonus);
            model.setNewEmployeeBonus(new EmployeeBonus());
        }
    }

    /**
     * Обработчик кнопки добавления выплаты вручную
     */
    @Deprecated
    public static void onClickAddBonusExtended(IExtractPaymentsModel<? extends IPostAssignExtract> model)
    {
        onClickAddBonus(model);

        if (!UserContext.getInstance().getErrorCollector().hasErrors())
        {
            model.setAddStaffListPaymentsBlockVisible(false);
            model.setAddPaymentBlockVisible(false);
            //model.setPaymentAddingCase(null);
            onRefreshPaymentCases(model);
        }
    }

    /**
     * Производит валидацию введенных данных при ручном добавлении выплат.
     */
    @Deprecated
    private static void validateSinglePaymentBlock(EmployeeBonus bonus)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        if (bonus.getFinancingSource() == null)
            errCollector.add("Поле «Источник финансирования» обязательно для заполнения.", "financingSourcesList");

        if (null == bonus.getPayment())
            errCollector.add("Поле «Выплата» обязательно для заполнения.", "payment");

        if (null == bonus.getAssignDate())
            errCollector.add("Поле «Дата назначения» обязательно для заполнения.", "paymentAssignDate");

        if ((null == bonus.getPayment() || !bonus.getPayment().isOneTimePayment()) && null == bonus.getBeginDate())
            errCollector.add("Поле «Дата начала» обязательно для заполнения.", "paymentBeginDate");

        if (null != bonus.getBeginDate() && null != bonus.getEndDate() && bonus.getBeginDate().getTime() > bonus.getEndDate().getTime())
            errCollector.add("Дата начала выплаты должна быть раньше даты её окончания.", "paymentBeginDate", "paymentEndDate");
    }

    /**
     * Сохраняет / обновляет детализацию дробления ставок по источникам финансирования
     */
    public static void updateFinancingSourceItemsStaffRates(AbstractEmployeeExtract extract, List<FinancingSourceItem> selectedFinancingSourceItems, IValueMapHolder staffRatesValueHolder, Session session)
    {
        for (FinancingSourceDetails item : UniDaoFacade.getCoreDao().getList(FinancingSourceDetails.class, FinancingSourceDetails.extract().s(), extract))
        {
            session.delete(item);
        }

        for (FinancingSourceItem item : selectedFinancingSourceItems)
        {
            Object preValue = staffRatesValueHolder.getValueMap().get(item.getId());
            Double value = (preValue instanceof Double) ? (Double) preValue : ((Long) preValue).doubleValue();

            FinancingSourceDetails details = new FinancingSourceDetails();
            details.setExtract(extract);
            details.setFinancingSourceItem(item);
            details.setStaffRate(value);
            session.save(details);
        }
    }

    /**
     * Сохраняет добавленные в приказе выплаты
     */
    @Deprecated
    public static void updateBonuses(IPaymentAddModel<? extends AbstractEmployeeExtract> model, Session session)
    {
        for (EmployeeBonus bonus : model.getBonusesToDel())
            UniDaoFacade.getCoreDao().delete(bonus);

        int priority = 0;
        for (EmployeeBonus bonus : model.getBonusesList())
        {
            if (bonus.getPayment().isOneTimePayment())
            {
                bonus.setEndDate(null);
                bonus.setBeginDate(bonus.getAssignDate());
            }

            if (0 > bonus.getId())
                bonus.setId(null);

            bonus.setPriority(++priority);
            bonus.setExtract(model.getExtract());
            session.saveOrUpdate(bonus);
        }
    }

    /**
     * Генерирует ID поля по заданным строковому префиксу и идентификатору
     */
    public static String getGeneratedFieldId(String prefix, long id)
    {
        return prefix + id;
    }

    /**
     * Возвращает строковое представление ставки (бюджет, или вне бюджета)
     * с детализацией источников финансирования по долям ставок.
     */
//    public static String getStaffRateDetailsString(IStaffRatesExtract extract, boolean budget, boolean addSumRate)
//    {
//        StringBuilder budgetStaffRateStr = new StringBuilder();
//        StringBuilder offBudgetStaffRateStr = new StringBuilder();
//        List<FinancingSourceDetails> details = UniDaoFacade.getCoreDao().getList(FinancingSourceDetails.class, FinancingSourceDetails.extract().s(), extract);
//
//        for (FinancingSourceDetails item : details)
//        {
//            if (null == item.getFinancingSourceItem().getFinancingSource() || !UniempDefines.FINANCING_SOURCE_BUDGET.equals(item.getFinancingSourceItem().getFinancingSource().getCode()))
//            {
//                offBudgetStaffRateStr.append(offBudgetStaffRateStr.length() == 0 ? " - (" : ", ");
//                offBudgetStaffRateStr.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()));
//                offBudgetStaffRateStr.append(" - ").append(item.getFinancingSourceItem().getTitle());
//            }
//            else
//            {
//                budgetStaffRateStr.append(budgetStaffRateStr.length() == 0 ? " - (" : ", ");
//                budgetStaffRateStr.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()));
//                budgetStaffRateStr.append(" - ").append(item.getFinancingSourceItem().getTitle());
//            }
//        }
//
//        if (budgetStaffRateStr.length() > 0) budgetStaffRateStr.append(")");
//        if (offBudgetStaffRateStr.length() > 0) offBudgetStaffRateStr.append(")");
//
//        if (budget) return (addSumRate ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getBudgetStaffRate()) : "") + budgetStaffRateStr.toString();
//        else return (addSumRate ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getOffBudgetStaffRate()) : "") + offBudgetStaffRateStr.toString();
//    }

    /**
     * Проводит валидацию ставок в выписке по сотруднику.
     */
    @SuppressWarnings("unchecked")
    public static void validateStaffRate(DynamicListDataSource dataSource, List<FinancingSourceDetails> detailsList, String postTypeCode, ErrorCollector errors)
    {
        final IValueMapHolder staffRateHolder = (IValueMapHolder) dataSource.getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? java.util.Collections.emptyMap() : staffRateHolder.getValueMap());

        final IValueMapHolder finSrcHolder = (IValueMapHolder) dataSource.getColumn("financingSource");
        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? java.util.Collections.emptyMap() : finSrcHolder.getValueMap());

        if (detailsList.isEmpty())
            errors.add("Ставка обязательна для заполнения.");

        double staffRateSumm = 0.0d;
        for (FinancingSourceDetails item : detailsList)
        {
            if (staffRateMap.get(item.getId()) == null)
                errors.add("Поле «Ставка» обязательно для заполнения.", "staffRate_id_" + item.getId());

            if (staffRateMap.get(item.getId()) != null && item.getStaffRate() == 0.0d)
                errors.add("Поле «Ставка» не может равняться нулю.", "staffRate_id_" + item.getId());

            if (finSrcMap.get(item.getId()) == null)
                errors.add("Поле «Источник финансирования» обязательно для заполнения.", "finSrc_id_" + item.getId());

            for (FinancingSourceDetails item2nd : detailsList)
                if (!item.equals(item2nd))
                    if (item.getFinancingSource() != null && item.getFinancingSource().equals(item2nd.getFinancingSource()))
                        if ((item.getFinancingSourceItem() == null && item2nd.getFinancingSourceItem() == null) ||
                                ((item.getFinancingSourceItem() != null && item2nd.getFinancingSourceItem() != null) && item.getFinancingSourceItem().equals(item2nd.getFinancingSourceItem())))
                            errors.add("Набор полей «Источник финансирования», «Источник финансирования (детально)» должен быть уникальным в рамках сотрудника.",
                                    "finSrc_id_" + item.getId(), "finSrcItm_id_" + item.getId());

            IDataSettings settings = DataSettingsFacade.getSettings("general", UniempDefines.STAFF_RATE_STEP_SETTINGS_PREFIX);
            Double minStaffRate = null != settings.get(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME) ? (Double) settings.get(UniempDefines.STAFF_RATE_STEP_POST_SETING_NAME) : 0d;

            Double reminder = Math.round((item.getStaffRate() % minStaffRate) * 100) / 100d;
            if (reminder != 0 && !minStaffRate.equals(reminder))
                errors.add("Доля ставки должна быть кратна " + minStaffRate + ".", "staffRate_id_" + item.getId());

            staffRateSumm += item.getStaffRate();
        }

        for (FinancingSourceDetails item : detailsList)
        {
            if (staffRateSumm > 1)
                errors.add("Суммарная доля ставки не должна превышать 1", "staffRate_id_" + item.getId());

            if (staffRateSumm > 0.5 && (UniDefines.POST_TYPE_SECOND_JOB_INNER.equals(postTypeCode) || UniDefines.POST_TYPE_SECOND_JOB_OUTER.equals(postTypeCode)))
                errors.add("Суммарная доля ставки по совместительству не может быть больше 0,5.", "staffRate_id_" + item.getId());
        }
    }

    /**
     * Кладет значения из списка в сечлист.<p>
     * (Создает и кладет мапы в <tt>ValueMap</tt> колонок <tt>BlockColumn</tt>)
     */
    @SuppressWarnings("unchecked")
    public static void prepareColumnsStaffRateSearchList(List<FinancingSourceDetails> staffRateItems, DynamicListDataSource dataSource)
    {
//        for (FinancingSourceDetails item : staffRateItems)
//        {
//            OrgUnit orgUnit = null;
//            PostBoundedWithQGandQL post = null;
//            if (EntityRuntime.getMeta(item.getExtract()).getProperty("orgUnit") != null)
//                orgUnit = (OrgUnit) item.getExtract().getProperty("orgUnit");
//            if (EntityRuntime.getMeta(item.getExtract()).getProperty("postBoundedWithQGandQL") != null)
//                post = (PostBoundedWithQGandQL) item.getExtract().getProperty("postBoundedWithQGandQL");
//            financingSourceItemModelMap.put(item.getId(), new FinSrcItmSingleSelectModel(orgUnit, post, item.getFinancingSource(), hasActiveStaffList));
//        }

        Map<Long, Double> staffRateMap = new HashMap<>();
        for (FinancingSourceDetails item : staffRateItems)
            staffRateMap.put(item.getId(), item.getStaffRate());
        ((BlockColumn) dataSource.getColumn(0)).setValueMap(staffRateMap);

        Map<Long, FinancingSource> finSrcMap = new HashMap<>();
        for (FinancingSourceDetails item : staffRateItems)
            finSrcMap.put(item.getId(), item.getFinancingSource());
        ((BlockColumn) dataSource.getColumn(1)).setValueMap(finSrcMap);

        Map<Long, FinancingSourceItem> finSrcItmMap = new HashMap<>();
        for (FinancingSourceDetails item : staffRateItems)
            finSrcItmMap.put(item.getId(), item.getFinancingSourceItem());
        ((BlockColumn) dataSource.getColumn(2)).setValueMap(finSrcItmMap);
    }

    /**
     * Обновляет значения в списке <tt>staffRateItems</tt> из соответствующих <tt>ValueMap</tt> колонок сеч листа
     */
    @SuppressWarnings("unchecked")
    public static void prepareStaffRateList(DynamicListDataSource dataSource, List<FinancingSourceDetails> staffRateItems)
    {
        final IValueMapHolder staffRateHolder = (IValueMapHolder) dataSource.getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        final IValueMapHolder finSrcHolder = (IValueMapHolder) dataSource.getColumn("financingSource");
        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

        final IValueMapHolder finSrcItmHolder = (IValueMapHolder) dataSource.getColumn("financingSourceItem");
        final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

        for (FinancingSourceDetails item : staffRateItems)
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                val = ((Number) value).doubleValue();
            }
            item.setStaffRate(val);
            item.setFinancingSource(finSrcMap.get(item.getId()));
            item.setFinancingSourceItem(finSrcItmMap.get(item.getId()));
        }
    }

    /**
     * @param add          - указывает на то откуда надо брать данные по ставкам.<p>
     *                     Если <tt>true</tt>, то данные берутся из выписки, т.е. поднимаеются ставки, которые созданы в выписке.<p>
     *                     Если <tt>false</tt>, то данные берутся из сотрудника, т.е. поднимаются ставки относительно сотрудника на которого создана выписка, указанная первым параметром.
     * @param employeePost - если в <tt>add</tt> лежит <tt>false</tt> и в поле <tt>entity</tt> выписки лежит <tt>null</tt>, то сотрудник будет браться из этого поля <tt>employeePost</tt>
     * @return Возвращает строку с данными по ставкам для отображения на карточке выписки
     */
    public static String getExtractFinancingSourceDetailsString(AbstractEmployeeExtract extract, Session session, boolean add, EmployeePost employeePost)
    {
        if (add)
        {
            List<FinancingSourceDetails> detailsList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);

            String staffRateStr = "";
            Double staffRateSumm = 0.0d;
            String resultStaffRateStr;
            for (FinancingSourceDetails details : detailsList)
            {
                staffRateSumm += details.getStaffRate();

                staffRateStr += DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(details.getStaffRate()) + " - " +
                        details.getFinancingSource().getTitle() +
                        (details.getFinancingSourceItem() != null ? " (" + details.getFinancingSourceItem().getTitle() + ")" : "") + "<br/>";
            }

            resultStaffRateStr = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateSumm) + "<br/>" + staffRateStr;

            return resultStaffRateStr;
        } else
        {
            List<EmployeePostStaffRateItem> detailsList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity() != null ? extract.getEntity() : employeePost);

            String staffRateStr = "";
            Double staffRateSumm = 0.0d;
            String resultStaffRateStr;
            for (EmployeePostStaffRateItem item : detailsList)
            {
                staffRateSumm += item.getStaffRate();

                staffRateStr += DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " +
                        item.getFinancingSource().getTitle() +
                        (item.getFinancingSourceItem() != null ? " (" + item.getFinancingSourceItem().getTitle() + ")" : "") + "<br/>";
            }

            resultStaffRateStr = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateSumm) + "<br/>" + staffRateStr;

            return resultStaffRateStr;
        }
    }

    /**
     * Заполняет мапу с source'ми селекта Источник финансирования.<p>
     * Обнуляет выбранные значения в полях Источник финансирования (детально) и Кадровая расстановка.
     */
    public static void fillFinSrcModel(IStaffRateListModel model, boolean freelance, OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        if (orgUnit != null && postBoundedWithQGandQL != null)
            if (UniempDaoFacade.getStaffListDAO().getActiveStaffList(orgUnit) != null && !freelance)
                model.setFinancingSourceModel(UniempDaoFacade.getStaffListDAO().getFinancingSourcesStaffListItems(orgUnit, postBoundedWithQGandQL));
            else
                model.setFinancingSourceModel(UniDaoFacade.getCoreDao().getCatalogItemList(FinancingSource.class));
    }

    /**
     * Заполняет мапу с source'ми селекта Источник финансирования (детально).
     * Обнуляем выбранные значения в полях Источник финансирования (детально) и Кадровая расстановка.
     */
    @Deprecated
    public static void fillFinSrcItmModel(/*Long itemId, IStaffRateListModel model, OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL*/)
    {
//        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
//        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? java.util.Collections.emptyMap() : finSrcHolder.getValueMap());
//
//
//        if (finSrcMap.get(itemId) != null)
//        {
//            ((BlockColumn) model.getStaffRateDataSource().getColumn("financingSourceItem")).getValueMap().put(itemId, null);
//
//            if (model.isThereAnyActiveStaffList())
//                ((BlockColumn) model.getStaffRateDataSource().getColumn("employeeHR")).getValueMap().put(itemId, null);
//            if (orgUnit != null && postBoundedWithQGandQL != null)
//                model.getFinancingSourceItemModelMap().put(itemId, new FinSrcItmSingleSelectModel(orgUnit, postBoundedWithQGandQL, finSrcMap.get(itemId), model.isThereAnyActiveStaffList()));
//        }
    }

    /**
     * Заполняет мапу с model'ми мультиселекта Кадровая расстановка.
     */
    @Deprecated
    public static void prepareEmployeeHRModel(/*IStaffRateListModel<? extends AbstractEmployeeExtract> model, OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL*/)
    {
//        if (!model.isThereAnyActiveStaffList())
//            return;
//
//        for (IEntity entity : model.getStaffRateItemList())
//        {
//            model.getEmployeeHRModelMap().put(entity.getId(), new IndividualizedBaseMultiSelectModel(entity.getId(), model.getStaffRateDataSource(), postBoundedWithQGandQL, orgUnit));
//        }
    }

    /**
     * Определяет необходима ли колонка Кадровая расстановка при редактировании выписки.<p>
     * Производит соответствующие действия в модели.<p>
     *
     * @return Возвращает <tt>true</tt>, если колонка Кадровая расстановка необходима, иначе <tt>false</tt>.
     */
    public static boolean hasNeedShowEmployeeHRColumn(IStaffRateListModel model, boolean freelance, OrgUnit orgUnit)
    {
        if (orgUnit != null &&
                UniempDaoFacade.getStaffListDAO().getActiveStaffList(orgUnit) != null && !freelance)
        {
            if (!model.isThereAnyActiveStaffList())
            {
                model.setThereAnyActiveStaffList(true);
                model.setNeedUpdateDataSource(true);
            }

            return true;
        }
        else
        {
            if (model.isThereAnyActiveStaffList())
            {
                model.setThereAnyActiveStaffList(false);
                model.setNeedUpdateDataSource(true);
            }

            return false;
        }
    }

    /**
     * Заполняет колонку Кадровая расстановка.
     */
    @SuppressWarnings("unchecked")
    public static void fillEmpHRColumn(IStaffRateListModel<? extends AbstractEmployeeExtract> model, OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        Map<Long, List<IdentifiableWrapper>> idWrapperMap = new HashMap<>();
        for (FinancingSourceDetails rateItem : model.getStaffRateItemList())
        {
            List<IdentifiableWrapper> wrapperList = new ArrayList<>();

            for (FinancingSourceDetailsToAllocItem relation : model.getDetailsToAllocItemList())
            {
                if (relation.getFinancingSourceDetails().equals(rateItem))
                    if (!relation.isHasNewAllocItem() && relation.getChoseStaffListAllocationItem() != null && relation.getChoseStaffListAllocationItem().getEmployeePost() != null)
                        wrapperList.add(new IdentifiableWrapper(relation.getChoseStaffListAllocationItem().getId(), relation.getChoseStaffListAllocationItem().getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(relation.getChoseStaffListAllocationItem().getStaffRate()) + " - " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPostType().getShortTitle() + " " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPostStatus().getShortTitle()));
                    else if (relation.isHasNewAllocItem())
                    {
                        StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(orgUnit, postBoundedWithQGandQL, rateItem.getFinancingSource(), rateItem.getFinancingSourceItem());
                        Double diff = 0.0d;
                        if (staffListItem != null)
                            diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();

                        List<StaffListAllocationItem> staffListAllocList = UniempDaoFacade.getStaffListDAO().getOccupiedStaffListAllocationItem(orgUnit, postBoundedWithQGandQL, rateItem.getFinancingSource(), rateItem.getFinancingSourceItem());
                        for (StaffListAllocationItem allocationItem : staffListAllocList)
                            if (!allocationItem.getEmployeePost().getPostStatus().isActive())
                                diff -= allocationItem.getStaffRate();

                        wrapperList.add(new IdentifiableWrapper(0L, "<Новая ставка> - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                    }
            }

            idWrapperMap.put(rateItem.getId(), wrapperList);
        }
        if (model.getDetailsToAllocItemList().size() > 0)
            ((BlockColumn) model.getStaffRateDataSource().getColumn("employeeHR")).setValueMap(idWrapperMap);
    }

    /**
     * Производит сохранение FinancingSourceDetails и FinancingSourceDetailsToAllocItem при сохранении выписки.
     */
    @SuppressWarnings("unchecked")
    public static void updateStaffRateList(IStaffRateListModel<? extends AbstractEmployeeExtract> model, boolean isEditForm, Session session)
    {
        final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
        final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? java.util.Collections.emptyMap() : empHRHolder.getValueMap());
        //достаем из враперов объекты Штатной расстановки
        final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
        new HashMap<Long, Boolean>();
        //если была доступна колонка Кадровой расстановки, то заполняем мапы
        if (model.isThereAnyActiveStaffList())
        {
            for (IEntity entity : model.getStaffRateItemList())
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                        allocationItemList.add(item);
                    } else
                    {
                        StaffListAllocationItem allocationItem = new StaffListAllocationItem();
                        allocationItem.setId(0L);
                        allocationItemList.add(allocationItem);
                    }
                }

                allocItemsMap.put(entity.getId(), allocationItemList);
            }
        }

        //если редактируем приказ, то удаляем созданные ранее релейшены и доли ставки, что бы не возникло ошибок уникальности набору полей
        if (isEditForm)
        {
            List<FinancingSourceDetails> staffRateItems = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(model.getExtract());
            List<FinancingSourceDetailsToAllocItem> relList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(staffRateItems);

            for (FinancingSourceDetailsToAllocItem rel : relList)
                session.delete(rel);

            for (FinancingSourceDetails item : staffRateItems)
                session.delete(item);

            session.flush();
        }

        prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateItemList());

        boolean isListExtract = model.getExtract() instanceof ListEmployeeExtract;
        List<StaffListAllocationItem> choseAllocationItemList = new ArrayList<>();
        //если выписка списочного приказа, то необходимо проверить не выбраны ли аналогичные ставки ШР в предыдущих выписках
        if (isListExtract)
        {
            MQBuilder builder = new MQBuilder(FinancingSourceDetailsToAllocItem.ENTITY_CLASS, "b", new String[]{FinancingSourceDetailsToAllocItem.L_CHOSE_STAFF_LIST_ALLOCATION_ITEM});
            builder.add(MQExpression.eq("b", FinancingSourceDetailsToAllocItem.financingSourceDetails().extract().paragraph().order().s(), model.getExtract().getParagraph().getOrder()));
            if (isEditForm)
                builder.add(MQExpression.notEq("b", FinancingSourceDetailsToAllocItem.financingSourceDetails().extract().s(), model.getExtract()));

            choseAllocationItemList.addAll(builder.<StaffListAllocationItem>getResultList(session));
        }

        for (FinancingSourceDetails item : model.getStaffRateItemList())
        {
            Long id = item.getId();//т.к. после save id поменяется, мы его сохраняем что бы достать из мапы соответствующий ставке набор allocItem

            session.save(item);

            if (model.isThereAnyActiveStaffList())
                for (StaffListAllocationItem allocationItem : allocItemsMap.get(id))
                {
                    FinancingSourceDetailsToAllocItem relation = new FinancingSourceDetailsToAllocItem();
                    if (allocationItem.getId() != 0L)
                    {
                        relation.setChoseStaffListAllocationItem(allocationItem);
                        relation.setHasNewAllocItem(false);
                        relation.setEmployeePostHistory(allocationItem.getEmployeePost());
                        relation.setStaffRateHistory(allocationItem.getStaffRate());
                    } else
                        relation.setHasNewAllocItem(true);
                    relation.setHasBeenSelected(false);
                    //если выписка из списочного приказа и ставка ШР выбрана в предыдущей выписке, то простовляем у релейшена метку Уже выбранности,
                    //что бы при откате не восстанавливать одиннаковые выписки
                    if (isListExtract && choseAllocationItemList.contains(allocationItem))
                        relation.setHasBeenSelected(true);
                    relation.setFinancingSourceDetails(item);

                    session.save(relation);
                }
        }
    }

    @SuppressWarnings("unchecked")
    public static void validateFinSrcDetToAllocItem(IStaffRateListModel<? extends AbstractEmployeeExtract> model, OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL, ErrorCollector errors)
    {
        //если была доступна колонка Кадровой расстановки, то выполняем соответствующие проверки
        if (model.isThereAnyActiveStaffList() && !errors.hasErrors())
        {
            final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
            final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? java.util.Collections.emptyMap() : empHRHolder.getValueMap());
            //достаем из враперов объекты Штатной расстановки
            final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
            //Мапа, указывающая на то выбрана ли строчка <Новая ставка> в мультиселекте
            Map<Long, Boolean> hasNewStaffRateMap = new HashMap<>();

            for (IEntity entity : model.getStaffRateItemList())
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                        allocationItemList.add(item);
                    }
                }

                allocItemsMap.put(entity.getId(), allocationItemList);

                hasNewStaffRateMap.put(entity.getId(), empHRMap.get(entity.getId()).contains(new IdentifiableWrapper(0L, "")));
            }


            for (FinancingSourceDetails details : model.getStaffRateItemList())
            {
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(orgUnit, postBoundedWithQGandQL, details.getFinancingSource(), details.getFinancingSourceItem());
                BigDecimal diff = new BigDecimal(0);
                if (staffListItem != null)
                    diff = new BigDecimal(staffListItem.getStaffRate()).add(new BigDecimal(staffListItem.getOccStaffRate()).negate()).setScale(2, RoundingMode.HALF_UP);

                List<StaffListAllocationItem> staffListAllocList = UniempDaoFacade.getStaffListDAO().getOccupiedStaffListAllocationItem(orgUnit, postBoundedWithQGandQL, details.getFinancingSource(), details.getFinancingSourceItem());
                for (StaffListAllocationItem allocItem : staffListAllocList)
                    if (!allocItem.getEmployeePost().getPostStatus().isActive())
                        diff = diff.add(new BigDecimal(allocItem.getStaffRate()).negate()).setScale(2, RoundingMode.HALF_UP);

                BigDecimal sumStaffRateAllocItems = new BigDecimal(0);
                for (StaffListAllocationItem allocItem : allocItemsMap.get(details.getId()))
                    sumStaffRateAllocItems = sumStaffRateAllocItems.add(new BigDecimal(allocItem.getStaffRate())).setScale(2, RoundingMode.HALF_UP);

                if ((details.getStaffRate() > sumStaffRateAllocItems.doubleValue() && !hasNewStaffRateMap.get(details.getId())) ||
                        (details.getStaffRate() > diff.add(sumStaffRateAllocItems).doubleValue()  && hasNewStaffRateMap.get(details.getId())))
                    errors.add("Размер ставки " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(details.getStaffRate()) + " " + details.getFinancingSource().getTitle() +
                            " превышает суммарную ставку, выбранную в поле «Кадровая расстановка».",
                            "staffRate_id_" + details.getId());

                if (details.getStaffRate() <= sumStaffRateAllocItems.doubleValue() && hasNewStaffRateMap.get(details.getId()))
                    errors.add("Необходимо точнее указать занимаемые ставки в поле «Кадровая расстановка» для "
                            + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(details.getStaffRate()) + " " + details.getFinancingSource().getTitle() + ".",
                            "employeeHR_Id_" + details.getId());
            }
        }
    }

    /**
     * Вспомогательный метод для подсчета суммарной ставки в выписке.
     *
     * @return Возвращает сумму долей ставок, если ставок не введено, то возвращает 0.
     */
    @SuppressWarnings("unchecked")
    public static double getSummStaffRateExtract(DynamicListDataSource<FinancingSourceDetails> dataSource, List<FinancingSourceDetails> financingSourceDetailsList)
    {
        final IValueMapHolder staffRateHolder = (IValueMapHolder) dataSource.getColumn("staffRate");
        final Map<Long, Object> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double multiplier = 0.0d;
        for (FinancingSourceDetails staffRate : financingSourceDetailsList)
        {
            Double val = 0.0d;
            Object value = staffRateMap.get(staffRate.getId());
            if (null != value)
            {
                if (value instanceof Long)
                    val = ((Long) value).doubleValue();
                else
                    val = (Double) value;
            }

            multiplier += val;
        }

        return multiplier;
    }

    /**
     * @return Создает и возвращает Выплату созданную в рамках выписки.
     */
    public static EmployeeBonus onClickAddSinglePayment(List<EmployeeBonus> employeeBonusList, AbstractEmployeeExtract extract)
    {
        EmployeeBonus item = new EmployeeBonus();

        int priority = 0;
        for (EmployeeBonus bonus : employeeBonusList)
            if (bonus.getPriority() > priority)
                priority = bonus.getPriority();
        priority++;

        short discriminator = EntityRuntime.getMeta(EmployeeBonus.class).getEntityCode();
        long id = EntityIDGenerator.generateNewId(discriminator);

        item.setId(id);
        item.setExtract(extract);

        item.setPriority(priority);

        employeeBonusList.add(item);

        return item;
    }

    public static List<EmployeeBonus> onClickAddStaffListPayment(List<EmployeeBonus> employeeBonusList, List<StaffListPostPayment> staffListPaymentsSet, AbstractEmployeeExtract extract)
    {
        int priority = 0;
        for (EmployeeBonus bonus : employeeBonusList)
            if (bonus.getPriority() > priority)
                priority = bonus.getPriority();
        priority++;

        List<EmployeeBonus> resultList = new ArrayList<>();

        for (StaffListPostPayment payment : staffListPaymentsSet)
        {
            //если уже добавленна выплата с таким же типом, размером, ИФ, ИФ(Д), то считаем, что эти выплаты с разных должностей ШР,
            //тогда вторую такую же выплату не добавляем и ставим у предыдущей признак, что она расчитывается на суммарную ставку
            boolean isContinue = false;//метка, означающая, что "аналогичная" выплата уже добавленна и нужно пропустить добавление текущей
            for (EmployeeBonus bonus : resultList)
            {
                if (bonus.getPayment().equals(payment.getPayment()))
                    if (bonus.getFinancingSource().equals(payment.getFinancingSource()))
                        if ((bonus.getFinancingSourceItem() == null && payment.getFinancingSourceItem() == null) || (bonus.getFinancingSourceItem() != null && bonus.getFinancingSourceItem().equals(payment.getFinancingSourceItem())))
                            if (bonus.getValue() == payment.getAmount())
                            {
                                bonus.setDependOnSummStaffRate(true);
                                isContinue = true;
                            }
                            else//уже добавлена выплата у которой совпадает все, кроме Значения, тогда не понятно из какой выплаты брать Значение, следовательно оставляем его пустым
                            {
                                bonus.setDependOnSummStaffRate(true);
                                bonus.setValue(0);
                                isContinue = true;
                            }



            }

            if (isContinue)
                continue;

            EmployeeBonus item = new EmployeeBonus();

            short discriminator = EntityRuntime.getMeta(EmployeeBonus.class).getEntityCode();
            long id = EntityIDGenerator.generateNewId(discriminator);

            item.setId(id);
            item.setExtract(extract);

            item.setPriority(priority++);

            item.setPayment(payment.getPayment());
            item.setValueProxy(payment.getAmount());
            item.setFinancingSource(payment.getFinancingSource());
            item.setFinancingSourceItem(payment.getFinancingSourceItem());

            employeeBonusList.add(item);

            resultList.add(item);
        }

        return resultList;
    }

    /**
     * Переставляет выплату вверх по приоритету по приоритету в списке.
     */
    public static void onUpPayment(EmployeeBonus currentBonus, List<EmployeeBonus> employeeBonusList)
    {
        if (currentBonus == null)
            return;

        int index;
        if (employeeBonusList.indexOf(currentBonus) == 0)
            return;
        else
            index = employeeBonusList.indexOf(currentBonus) - 1;

        EmployeeBonus beforeBonus = employeeBonusList.get(index);

        beforeBonus.setPriority(beforeBonus.getPriority() + 1);
        currentBonus.setPriority(currentBonus.getPriority() - 1);
    }

    /**
     * Переставляет выплату вниз по приоритету в списке.
     */
    public static void onDownPayment(EmployeeBonus currentBonus, List<EmployeeBonus> employeeBonusList)
    {
        if (currentBonus == null)
            return;

        int index;
        if (employeeBonusList.indexOf(currentBonus) + 1 == employeeBonusList.size())
            return;
        else
            index = employeeBonusList.indexOf(currentBonus) + 1;

        EmployeeBonus afterBonus = employeeBonusList.get(index);

        afterBonus.setPriority(afterBonus.getPriority() - 1);
        currentBonus.setPriority(currentBonus.getPriority() + 1);
    }

    /**
     * Удаляет выплату из списка.
     */
    public static void onDeletePayment(EmployeeBonus currentBonus, List<EmployeeBonus> employeeBonusList)
    {
        if (currentBonus == null)
            return;

        employeeBonusList.remove(currentBonus);

        int priority = 1;
        for (EmployeeBonus bonus : employeeBonusList)
            bonus.setPriority(priority++);
    }

    /**
     * Проверяет есть ли еще доступные выплаты в ШР.
     * Выплаты считаются одинаковыми, если у них совпадают: тип, размер, иф, иф(д).
     * @param post - должность, выплаты которой проверяются в ШР.
     * @param orgUnit - подразделение, ШР которого рассматривается.
     * @param employeeBonusList - список выплат, относительно которого проверяется все ли выплаты по ШР добавлены.
     * @param financingSourceDetailsList - список долей ставки сотрудника
     * @return Возвращает доступные для добавления выплаты ШР, согласно ШР.
     */
    public static List<StaffListPostPayment> checkEnabledAddStaffListPaymentButton(PostBoundedWithQGandQL post, OrgUnit orgUnit, List<EmployeeBonus> employeeBonusList, List financingSourceDetailsList)
    {
        List<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>> finSrcPairList = null;
        if (financingSourceDetailsList != null)
        {
            finSrcPairList = new ArrayList<>();
            for (Object item : financingSourceDetailsList)
                if (item instanceof FinancingSourceDetails)
                    finSrcPairList.add(new CoreCollectionUtils.Pair<>(((FinancingSourceDetails) item).getFinancingSource(), ((FinancingSourceDetails) item).getFinancingSourceItem()));
                else if (item instanceof EmployeePostStaffRateItem)
                    finSrcPairList.add(new CoreCollectionUtils.Pair<>(((EmployeePostStaffRateItem) item).getFinancingSource(), ((EmployeePostStaffRateItem) item).getFinancingSourceItem()));
                else
                    throw new IllegalArgumentException("'financingSourceDetailsList' list items type must be FinancingSourceDetails or EmployeePostStaffRateItem.");
        }

        List<StaffListPostPayment> staffListPostPaymentsList = UniempDaoFacade.getStaffListDAO().getStaffListPostPaymentsList(orgUnit, post, finSrcPairList);

        List<StaffListPostPayment> paymentsList = new ArrayList<>();
        List<StaffListPostPayment> resultPaymentsList = new ArrayList<>();

        if (staffListPostPaymentsList != null)
        {
            paymentsList.addAll(staffListPostPaymentsList);
            resultPaymentsList.addAll(staffListPostPaymentsList);
        }

        List<StaffListPostPayment> excludePaymentList = new ArrayList<>();
        for (EmployeeBonus bonus : employeeBonusList)
            for (StaffListPostPayment payment : paymentsList)
                if (payment.getPayment().equals(bonus.getPayment()))
                    if (payment.getFinancingSource().equals(bonus.getFinancingSource()))
                        if ((payment.getFinancingSourceItem() == null && bonus.getFinancingSourceItem() == null) || (payment.getFinancingSourceItem() != null && payment.getFinancingSourceItem().equals(bonus.getFinancingSourceItem())))
                            if (bonus.isDependOnSummStaffRate())
                            {
                                if (!excludePaymentList.contains(payment))
                                    excludePaymentList.add(payment);
                            }
                            else
                            {
                                if (payment.getAmount() == bonus.getValue())
                                    if (!excludePaymentList.contains(payment))
                                        excludePaymentList.add(payment);
                            }

        for (StaffListPostPayment payment : excludePaymentList)
            resultPaymentsList.remove(payment);

        return resultPaymentsList;
    }

    /**
     * @return Возвращает модель для селекта Выплаты в блоке выплат выписок.
     */
    public static ISingleSelectModel getPaymentModelForPaymentBlock()
    {
        return new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Payment.class, "b").column("b");
                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(Payment.title().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(Payment.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(Payment.title().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }
        };
    }

    /**
     * @return Возвращает модель для селекта Источник финансирования в блоке выплат выписок.
     */
    public static ISingleSelectModel getFinSrcModelForPaymentBlock()
    {
        return new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSource.class, "b").column("b");
                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(FinancingSource.title().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSource.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(FinancingSource.title().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }
        };
    }

    /**
     * @return Возвращает модель для селекта Источник финансирования (детально) в блоке выплат выписок.
     */
    public static ISingleSelectModel getFinSrcItemModelForPaymentBlock(final DynamicListDataSource dataSource)
    {
        return new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                EmployeeBonus currentEntity = (EmployeeBonus) dataSource.getCurrentEntity();

                FinancingSource financingSource = currentEntity.getFinancingSource();

                if (financingSource == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSourceItem.class, "b").column("b");
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSourceItem.financingSource().fromAlias("b")), financingSource));
                builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(FinancingSourceItem.title().fromAlias("b"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(FinancingSourceItem.id().fromAlias("b")), o));
                builder.order(DQLExpressions.property(FinancingSourceItem.title().fromAlias("b")));

                return new DQLListResultBuilder(builder);
            }
        };
    }

    /**
     * Подготовливает датаСорс для списка выплат в блоке выплат.
     * Заполняет колонки и их содержимое.
     * @return Датасорс с заполненными колонками.
     */
    public static DynamicListDataSource preparePaymentDataSourceForPaymentBlock(DynamicListDataSource dataSource)
    {
        IListenerParametersResolver parametersResolver = (entity, valueEntity) -> entity;

        dataSource.addColumn(new BlockColumn("assignDate", "Дата назначения"));
        dataSource.addColumn(new BlockColumn("beginDate", "Назначена с даты"));
        dataSource.addColumn(new BlockColumn("endDate", "Назначена по дату"));
        dataSource.addColumn(new BlockColumn("payment", "Выплата"));
        dataSource.addColumn(new BlockColumn("amount", "Размер выплаты"));
        dataSource.addColumn(new SimpleColumn("Формат ввода", EmployeeBonus.payment().paymentUnit().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn("finSrc", "Источник финансирования"));
        dataSource.addColumn(new BlockColumn("finSrcItem", "Источник финансирования (детально)"));
        ActionColumn upColumn = new ActionColumn("Вверх", "up", "onUpPayment");
        upColumn.setParametersResolver(parametersResolver);
        dataSource.addColumn(upColumn);
        ActionColumn downColumn = new ActionColumn("Вниз", "down", "onDownPayment");
        downColumn.setParametersResolver(parametersResolver);
        dataSource.addColumn(downColumn);
        ActionColumn delColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onDeletePayment");
        delColumn.setParametersResolver(parametersResolver);
        dataSource.addColumn(delColumn);

        return dataSource;
    }

    /**
     * Подготавливает датаСорс для списка выплат в блоке выплат выписок.
     * Сортирует и выставляет количество строк в списке.
     */
    @SuppressWarnings("unchecked")
    public static void preparePaymentDataSourceForPaymentBlock(List<EmployeeBonus> paymentList, DynamicListDataSource paymentDataSource)
    {
        java.util.Collections.sort(paymentList, CommonBaseUtil.PRIORITY_SIMPLE_SAFE_COMPARATOR);
        paymentDataSource.setCountRow(paymentList.size() < 5 ? 5 : paymentList.size());
        UniBaseUtils.createPage(paymentDataSource, paymentList);
    }

    /**
     * Метод вызывается при сохранении редактируемой выписки.
     * Создаеся DQLSelectBuilder поднимающий Выплаты, созданные в рамках выписки..
     * @param extract выписка у которой удаляем созданные в ней выплаты
     * @return DQLSelectBuilder
     */
    public static DQLSelectBuilder getSavedPaymentsFromPaymentBlockDQLBuilder(AbstractEmployeeExtract extract)
    {
        DQLSelectBuilder payBuilder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "b").column("b");
        payBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeBonus.extract().fromAlias("b")), extract));

        return payBuilder;
    }

    /**
     * Вызывается при сохранени выписки.
     * Подготавливает выплаты, простовляя дату начала и окончания для разовых выплат.
     * @return Возвращает выплаты, созданные в рамках выписки.
     */
    public static List<EmployeeBonus> getPaymentsForSaveFromPaymentBlock(List<EmployeeBonus> employeeBonusList)
    {
        for (EmployeeBonus bonus : employeeBonusList)
        {
            if (bonus.getPayment().isOneTimePayment())
            {
                bonus.setBeginDate(bonus.getAssignDate());
                bonus.setEndDate(null);
            }
        }

        return employeeBonusList;
    }

    /**
     * Подготавливает размер выплаты.<p/>
     * Учитывается настройка Базовые значения выплат для должностей.
     * @param payment выплата по справочнику
     * @param post должность по ПКГ и КУ, относительно которой будет подниматся базовое значение выплаты
     * @return Размер выплаты, если <tt>payment == null</tt>, то возвращается <tt>null</tt>.
     */
    public static Double getPaymentValueForPaymentBlock(Payment payment, PostBoundedWithQGandQL post)
    {
        Double paymentValue = null;
        if (payment != null)
        {
            double basePaymentValue = UniempDaoFacade.getUniempDAO().getPaymentBaseValueForPost(payment, post);
            paymentValue = basePaymentValue != 0d ? basePaymentValue : null;
            if (paymentValue == null)
                paymentValue = payment.getValue();
        }

        return paymentValue;
    }

    /**
     * Проводит валидацию блока выплат в выписке.
     */
    public static void validateForPaymentBlock(List<EmployeeBonus> employeeBonusList, ErrorCollector errorCollector)
    {
        for (EmployeeBonus bonus : employeeBonusList)
        {
            if (bonus.getEndDate() != null && bonus.getBeginDate().getTime() > bonus.getEndDate().getTime())
                errorCollector.add("Дата начала должна быть меньше даты окончания.", "beginDate_Id_" + bonus.getId(), "endDate_Id_" + bonus.getId());

            for (EmployeeBonus bonus2 : employeeBonusList)
            {
                if (bonus.equals(bonus2))
                    continue;

                if (bonus.getPayment().equals(bonus2.getPayment()))
                    if (bonus.getFinancingSource().equals(bonus2.getFinancingSource()))
                        if ((bonus.getFinancingSourceItem() == null && bonus2.getFinancingSourceItem() == null) || (bonus.getFinancingSourceItem() != null && bonus.getFinancingSourceItem().equals(bonus2.getFinancingSourceItem())))
                            errorCollector.add("Список «Выплаты» не может содержать одинаковые выплаты с одинаковыми источниками финансирования.",
                                        "payment_Id_" + bonus.getId(), "payment_Id_" + bonus2.getId(), "finSrc_Id_" + bonus.getId(), "finSrc_Id_" + bonus2.getId(), "finSrcItem_Id_" + bonus.getId(), "finSrcItem_Id_" + bonus2.getId());
            }
        }
    }

    /**
     * Заполняет примечания к основаниям в выписках.<p/>
     * Если основание помечено как ТД или как Доп. соглашение к ТД, и если оно еще не заполнено, то заполняет его подстовляя указаную дату и номер.
     * @param basicsList список выбранных оснований
     * @param basicMap мапа примечаний к основаниям
     * @param contractDate дата ТД
     * @param contractNumber номер ТД
     * @param agreementDate дата доп. соглашения к ТД
     * @param agreementNumber номер доп. соглашения к ТД
     */
    public static void fillExtractBasics(List<EmployeeOrderBasics> basicsList, Map<Long, String> basicMap, Date contractDate, String contractNumber, Date agreementDate, String agreementNumber)
    {
        for (EmployeeOrderBasics basic : basicsList)
        {
            if (basic.isLaborContract() && basicMap.get(basic.getId()) == null && contractDate != null && contractNumber != null)
            {
                basicMap.put(basic.getId(), "№" + contractNumber + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(contractDate));
            }
            else if (basic.isAgreementLaborContract() && basicMap.get(basic.getId()) == null && agreementDate != null && agreementNumber != null)
            {
                basicMap.put(basic.getId(), "№" + agreementNumber + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(agreementDate));
            }
        }
    }

    /**
     * Подготавоивает сингл-селект модель Должностей по ПКГ и КУ.<p/>
     * Если <code>freelance == true</code>, то модель заполняется относительно Должностей в типе подразделения,<p/>
     * иначе в соответствии с ШР, если оно есть и активно.
     * @param extract выписка прказа по кадрам, реализующая <code>IEmployeePostExtract<code/> и <code>IEmployeeOrgUnitExtract<code/>
     * @return Модель Должностей по ПКГ и КУ
     */
    public static ISingleSelectModel getPostRelationListModel(final IPostAssignExtract extract)
    {
        return new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (extract.getOrgUnit() == null)
                {
                    return ListResult.getEmpty();
                }

                if (extract.isFreelance())
                {
                    List<PostBoundedWithQGandQL> resultList = new ArrayList<>();
                    for (OrgUnitTypePostRelation postRelation : UniempDaoFacade.getUniempDAO().getPostRelationList(extract.getOrgUnit(), extract.getPostBoundedWithQGandQL(), filter, 50))
                        resultList.add(postRelation.getPostBoundedWithQGandQL());
                    return new ListResult<>(resultList);
                }
                else
                {
                    int maxCount = UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnitCount(extract.getOrgUnit(), extract.getPostBoundedWithQGandQL(), filter, null, null);
                    List<PostBoundedWithQGandQL> resultList = new ArrayList<>();
                    for (OrgUnitTypePostRelation postRelation : UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnit(extract.getOrgUnit(), extract.getPostBoundedWithQGandQL(), filter, null, null, 50))
                        resultList.add(postRelation.getPostBoundedWithQGandQL());
                    return new ListResult<>(resultList, maxCount);
                }
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                PostBoundedWithQGandQL entity = UniDaoFacade.getCoreDao().get(PostBoundedWithQGandQL.class, (Long) primaryKey);
                if (null == entity || null == extract.getOrgUnit())
                    return null;

                if (findValues(entity.getTitle()).getObjects().contains(entity))
                    return entity;

                extract.setPostBoundedWithQGandQL(null);

                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((PostBoundedWithQGandQL) value).getFullTitleWithSalary();
            }
        };
    }
}