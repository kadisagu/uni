/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.commons;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWeekWorkLoad;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeWeekWorkLoadCodes;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeWorkWeekDurationCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.Payment;
import ru.tandemservice.uniemp.entity.employee.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author dseleznev
 *         Created on: 16.09.2009
 *         <p/>
 *         Содержит набор общих методов для обновления сотрудников и связанных с ними объектов
 *         при проведении и откате приказов по кадровому составу.
 */
public class CommonExtractCommitUtil
{
    /**
     * Возвращает должность, подходящую под выбранные параметры
     */
    public static OrgUnitTypePostRelation getPostRelation(Session session, OrgUnit orgUnit, PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        MQBuilder builder = new MQBuilder(OrgUnitPostRelation.ENTITY_CLASS, "oup");
        builder.add(MQExpression.eq("oup", OrgUnitPostRelation.L_ORG_UNIT, orgUnit));
        builder.add(MQExpression.eq("oup", OrgUnitPostRelation.L_ORG_UNIT_TYPE_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, postBoundedWithQGandQL));
        List<OrgUnitPostRelation> postList = builder.getResultList(session);

        if (!postList.isEmpty()) return postList.get(0).getOrgUnitTypePostRelation();

        @SuppressWarnings("unchecked")
        List<OrgUnitTypePostRelation> postRelList = session.createCriteria(OrgUnitTypePostRelation.class).add(Restrictions.eq(OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L, postBoundedWithQGandQL)).list();
        if (!postRelList.isEmpty())
            return postRelList.get(0);

        UserContext.getInstance().getErrorCollector().add("Должность «" + postBoundedWithQGandQL.getPost().getTitle() + "» не может быть создана на подразделении «" + orgUnit.getFullTitle() + "».", "postRelation");
        return null;
    }

    /**
     * Создает для КР новую должность, либо обновляет уже созданную при проведении выписки.
     */
    public static EmployeePost createOrAssignEmployeePost(Session session, IPostAssignExtract extract)
    {
        IUniBaseDao dao = UniDaoFacade.getCoreDao();

        EmployeePost post = null != extract.getEntity() ? extract.getEntity() : new EmployeePost();
        OrgUnitTypePostRelation postRelation = getPostRelation(session, post.getOrgUnit(), extract.getPostBoundedWithQGandQL());

        if (UserContext.getInstance().getErrorCollector().hasErrors()) return null;

        post.setEmployee(extract.getEmployee());
        post.setOrgUnit(extract.getOrgUnit());
        post.setPostRelation(postRelation);
        post.setFreelance(extract.isFreelance());
        post.setPostDate(extract.getBeginDate());
        post.setDismissalDate(extract.getEndDate());
        post.setEtksLevels(extract.getEtksLevels());
        post.setPostType(extract.getPostType());
        post.setPostStatus(dao.getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_ACTIVE));
        post.setRaisingCoefficient(extract.getRaisingCoefficient());
        post.setSalary(extract.getSalary());
        EmployeeWeekWorkLoad weeWorkLoad = extract.getWeekWorkLoad();
        if (null == weeWorkLoad) weeWorkLoad = (EmployeeWeekWorkLoad) session.createCriteria(EmployeeWeekWorkLoad.class).add(Restrictions.eq(EmployeeWeekWorkLoad.P_DFLT, Boolean.TRUE)).uniqueResult();
        if (null == weeWorkLoad) weeWorkLoad = dao.getCatalogItem(EmployeeWeekWorkLoad.class, EmployeeWeekWorkLoadCodes.TITLE_40);
        EmployeeWorkWeekDuration weekDuration = extract.getWorkWeekDuration();
        if (null == weekDuration) weekDuration = (EmployeeWorkWeekDuration) session.createCriteria(EmployeeWorkWeekDuration.class).add(Restrictions.eq(EmployeeWorkWeekDuration.P_DFLT, Boolean.TRUE)).uniqueResult();
        if (null == weekDuration) weekDuration = dao.getCatalogItem(EmployeeWorkWeekDuration.class, EmployeeWorkWeekDurationCodes.WEEK_5);
        post.setWeekWorkLoad(weeWorkLoad);
        post.setWorkWeekDuration(weekDuration);
        post.setCompetitionType(extract.getCompetitionType());

        if (UniDefines.POST_TYPE_MAIN_JOB.equals(extract.getPostType().getCode()))
            post.setMainJob(true);
        else
            post.setMainJob(false);

        if (null != extract.getParagraph() && null != extract.getParagraph().getOrder())
        {
            if (null != extract.getParagraph().getOrder().getCommitDate())
                post.setOrderDate(extract.getParagraph().getOrder().getCommitDate());

            if (null != extract.getParagraph().getOrder().getNumber())
                post.setOrderNumber(extract.getParagraph().getOrder().getNumber());
        }

        try
        {
            if (null != extract.getProperty("hourlyPaid"))
                post.setHourlyPaid((Boolean) extract.getProperty("hourlyPaid"));
        }
        catch (Exception e)
        { /* do nothing. Great thanks to the framework development group. I suppose, since this moment we should process with try-catch blocks every potentially weak place like this... My greetings, dear colleagues! */ }

        return post;
    }

    /**
     * Создает ставки (релейшоны) сотрудника при проведении выписки.
     */
    public static List<EmployeePostStaffRateItem> createEmployeePostStaffRateItems(AbstractEmployeeExtract extract, EmployeePost post)
    {
        List<EmployeePostStaffRateItem> resultList = new ArrayList<>();

        List<FinancingSourceDetails> detailsList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
        //создаем новые ставки по релейшенам из приказа
        for (FinancingSourceDetails details : detailsList)
        {
            EmployeePostStaffRateItem staffRateItem = new EmployeePostStaffRateItem();
            staffRateItem.setEmployeePost(post);
            staffRateItem.setFinancingSource(details.getFinancingSource());
            staffRateItem.setFinancingSourceItem(details.getFinancingSourceItem());
            staffRateItem.setStaffRate(details.getStaffRate());

            resultList.add(staffRateItem);
        }

        return resultList;
    }

    /**
     * Создает новые ставки Штатной расстановки сотруднику при проведении выписки.
     */
    public static List<StaffListAllocationItem> createStaffListAllocationItem(AbstractEmployeeExtract extract, EmployeePost post)
    {
        List<FinancingSourceDetails> staffRateItems = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
        List<FinancingSourceDetailsToAllocItem> relList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(staffRateItems);

        if (relList.size() == 0 || staffRateItems.size() == 0)
            return Collections.emptyList();

        List<StaffListAllocationItem> resultList = new ArrayList<>();

        for (FinancingSourceDetails details : staffRateItems)
        {
            StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(post.getOrgUnit(), post.getPostRelation().getPostBoundedWithQGandQL(), details.getFinancingSource(), details.getFinancingSourceItem());

            StaffListAllocationItem staffListAllocationItem = new StaffListAllocationItem();

            staffListAllocationItem.setStaffRate(details.getStaffRate());
            staffListAllocationItem.setFinancingSource(details.getFinancingSource());
            staffListAllocationItem.setFinancingSourceItem(details.getFinancingSourceItem());
            staffListAllocationItem.setStaffListItem(staffListItem);
            staffListAllocationItem.setEmployeePost(post);
            staffListAllocationItem.setRaisingCoefficient(post.getRaisingCoefficient());
            if (staffListAllocationItem.getRaisingCoefficient() != null)
                staffListAllocationItem.setMonthBaseSalaryFund(staffListAllocationItem.getRaisingCoefficient().getRecommendedSalary() * staffListAllocationItem.getStaffRate());
            else if (staffListAllocationItem.getStaffListItem() != null)
                staffListAllocationItem.setMonthBaseSalaryFund(staffListAllocationItem.getStaffListItem().getSalary() * staffListAllocationItem.getStaffRate());

            resultList.add(staffListAllocationItem);
        }

        return resultList;
    }

    /**
     * Создает новый договор сотруднику, или обновляет уже созданный при проведении выписки.
     */
    public static EmployeeLabourContract createOrAssignLabourContract(ILabourContractExtract extract, EmployeePost post)
    {
        EmployeeLabourContract contract = null != extract.getEntity() ? UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(post) : new EmployeeLabourContract();
        contract.setDate(extract.getLabourContractDate());
        contract.setBeginDate(extract.getBeginDate());
        contract.setEndDate(extract.getEndDate());
        contract.setType(extract.getLabourContractType());
        contract.setNumber(extract.getLabourContractNumber());
        contract.setEmployeePost(post);
        return contract;
    }

    /**
     * Создает сотруднику запись в истории должностей, или обновляет уже созданную при проведении выписки.
     */
    public static EmploymentHistoryItemInner createOrAssignEmployementHistoryItem(AbstractEmployeeExtract extract, Session session, EmployeePost post, List<EmployeePostStaffRateItem> staffRateItemList)
    {
        Criteria crit = session.createCriteria(EmploymentHistoryItemInner.class);
        crit.add(Restrictions.eq(EmploymentHistoryItemInner.L_EMPLOYEE, post.getEmployee()));
        crit.add(Restrictions.eq(EmploymentHistoryItemInner.L_ORG_UNIT, post.getOrgUnit()));
        crit.add(Restrictions.eq(EmploymentHistoryItemInner.L_POST_BOUNDED_WITH_Q_GAND_Q_L, post.getPostRelation().getPostBoundedWithQGandQL()));
        crit.add(Restrictions.eq(EmploymentHistoryItemInner.L_POST_TYPE, post.getPostType()));
        crit.add(Restrictions.eq(EmploymentHistoryItemInner.P_ASSIGN_DATE, post.getPostDate()));

        if (null != staffRateItemList && staffRateItemList.size() > 0)
        {
            Double sumStaffRate = 0.0d;
            for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
                sumStaffRate += staffRateItem.getStaffRate();

            crit.add(Restrictions.eq(EmploymentHistoryItemInner.P_STAFF_RATE, sumStaffRate));
        }

        @SuppressWarnings("unchecked")
        List<EmploymentHistoryItemInner> list = crit.list();

        EmploymentHistoryItemInner empHistItem = list.isEmpty() ? new EmploymentHistoryItemInner() : list.get(0);
        empHistItem.setEmployee(post.getEmployee());
        empHistItem.setOrgUnit(post.getOrgUnit());
        empHistItem.setPostBoundedWithQGandQL(post.getPostRelation().getPostBoundedWithQGandQL());
        empHistItem.setPostType(post.getPostType());
        empHistItem.setAssignDate(post.getPostDate());
        empHistItem.setCurrent(true);
        if (empHistItem.getId() == null)
        {
            empHistItem.setExtractDate(extract.getParagraph().getOrder().getCommitDate());
            empHistItem.setExtractNumber(extract.getParagraph().getOrder().getNumber());
            empHistItem.setExtract(extract);
        }

        if (null != staffRateItemList && staffRateItemList.size() > 0)
        {
            Double sumStaffRate = 0.0d;
            for (EmployeePostStaffRateItem staffRateItem : staffRateItemList)
                sumStaffRate += staffRateItem.getStaffRate();

            empHistItem.setStaffRate(sumStaffRate);
        }

        return empHistItem;
    }

    /**
     * Создает новые выплаты сотруднику, или обновляет уже созданные при проведении выписки.
     */
    @SuppressWarnings("deprecation")
    @Deprecated
    public static List<EmployeePayment> createOrAssignEmployeePaymentsList(Session session, AbstractEmployeeExtract extract, EmployeePost post, double salary/*, StaffListAllocationItem allocItem*/)
    {
        double sumPayments = 0d;
        List<EmployeePayment> paymentsList = new ArrayList<>();
        List<EmployeeBonus> totalBonusesList = new ArrayList<>();
        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
        {
            if (UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                totalBonusesList.add(bonus);
            else
            {
                EmployeePayment payment = createOrAssignSingleEmployeePayment(session, bonus, post, salary, 0d);
                sumPayments += payment.getAmount();
                paymentsList.add(payment);
            }
        }

        for (EmployeeBonus bonus : totalBonusesList)
        {
            paymentsList.add(createOrAssignSingleEmployeePayment(session, bonus, post, salary, sumPayments));
        }

        return paymentsList;
    }

    /**
     * Создает новую выплату сотруднику, или обновляет уже созданную при проведении выписки
     * для указанной выплаты для выписки, а так же обновляет выплату выписки, подставляя ссылку
     * на созданную выплату сотрудника.
     */
    @Deprecated
    private static EmployeePayment createOrAssignSingleEmployeePayment(Session session, EmployeeBonus bonus, EmployeePost post, double salary, double sumPayments)
    {
        EmployeePayment payment = null != bonus.getEntity() ? bonus.getEntity() : new EmployeePayment();

        payment.setEmployeePost(post);
        payment.setAssignDate(bonus.getAssignDate());
        payment.setBeginDate(bonus.getBeginDate());
        payment.setEndDate(bonus.getEndDate());
        payment.setPayment(bonus.getPayment());
        payment.setFinancingSource(bonus.getFinancingSource());
        payment.setFinancingSourceItem(bonus.getFinancingSourceItem());
        payment.setPaymentValue(bonus.getValue());

        if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(bonus.getPayment().getPaymentUnit().getCode()))
            payment.setAmount(bonus.getValue());
        else
        {
            if (UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                payment.setAmount(Math.round(salary * bonus.getValue() * 100) / 100d);
            else if (UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                payment.setAmount(Math.round(salary * bonus.getValue()) / 100d);
            else
                payment.setAmount(Math.round((salary + sumPayments) * bonus.getValue()) / 100d);
        }

        bonus.setEntity(payment);
        session.update(bonus);

        return payment;
    }

    /**
     * Создает новую выплату сотруднику, или обновляет уже созданную при проведении выписки
     * для указанной выплаты для выписки, а так же обновляет выплату выписки, подставляя ссылку
     * на созданную выплату сотрудника.
     *
     * @param bonus       выплата, созданная в рамках выписки
     * @param post        сотрудник, для которой создаем выплату
     * @param salary      базовый оклад должности сотрудника
     * @param sumPayments сумма выплат сотрудника
     * @param staffRate   актуальная для данной выплаты доля ставки
     * @return Подготовленную выплату сотрудника.
     */
    private static EmployeePayment createOrAssignSingleEmployeePayment(Session session, EmployeeBonus bonus, EmployeePost post, double salary, double sumPayments, double staffRate, double summStaffRate)
    {
        EmployeePayment payment = null != bonus.getEntity() ? bonus.getEntity() : new EmployeePayment();

        payment.setEmployeePost(post);
        payment.setAssignDate(bonus.getAssignDate());
        payment.setBeginDate(bonus.getBeginDate());
        payment.setEndDate(bonus.getEndDate());
        payment.setPayment(bonus.getPayment());
        payment.setFinancingSource(bonus.getFinancingSource());
        payment.setFinancingSourceItem(bonus.getFinancingSourceItem());
        payment.setPaymentValue(bonus.getValue());

        staffRate = bonus.getPayment().isDoesntDependOnStaffRate() ? 1d : (bonus.isDependOnSummStaffRate() ? summStaffRate : staffRate);
        salary = salary * staffRate;

        if (UniempDefines.PAYMENT_UNIT_RUBLES.equals(bonus.getPayment().getPaymentUnit().getCode()))
            payment.setAmount(bonus.getValue() * staffRate);
        else
        {
            if (UniempDefines.PAYMENT_UNIT_COEFFICIENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                payment.setAmount(Math.round(salary * bonus.getValue() * 100) / 100d);
            else if (UniempDefines.PAYMENT_UNIT_BASE_PERCENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                payment.setAmount(Math.round(salary * bonus.getValue()) / 100d);
            else
                payment.setAmount(Math.round((salary + sumPayments) * bonus.getValue()) / 100d);
        }

        bonus.setEntity(payment);
        session.update(bonus);

        return payment;
    }

    /**
     * Создает новые выплаты сотруднику, или обновляет уже созданные при проведении выписки.<p/>
     * При расчете выплат на МФОТ учитывается настройка "Порядок начисления выплат на ФОТ с надбавками". Учитываются только выплаты с одинаковыми ИФ.
     *
     * @param extract           выписка, которая проводится
     * @param post              сотрудник, для которого создаются выплаты
     * @param salary            базовый оклад сотрудника
     * @param staffRateItemList доли ставки сотрудника
     * @return Список выплат сотрудника, созданных в рамках выписки.
     */
    public static List<EmployeePayment> createOrAssignEmployeePaymentsList(Session session, AbstractEmployeeExtract extract, EmployeePost post, double salary, List<EmployeePostStaffRateItem> staffRateItemList)
    {
        if (staffRateItemList.isEmpty())
            throw new ApplicationException("Не указано ни одной доли ставки.");

        List<EmployeeBonus> employeeBonusList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract);

        //выясняем кол-во долей ставки и суммарную ставку
        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> staffRateMap = null;
        Double staffRate = null;
        Double sumStaffRate = 0d;
        for (EmployeePostStaffRateItem item : staffRateItemList)
            sumStaffRate += item.getStaffRate();
        if (staffRateItemList.size() > 1)
        {
            staffRateMap = new LinkedHashMap<>();
            for (EmployeePostStaffRateItem item : staffRateItemList)
                staffRateMap.put(new CoreCollectionUtils.Pair<>(item.getFinancingSource(), item.getFinancingSourceItem()), item.getStaffRate());
        }
        else
        {
            staffRate = staffRateItemList.get(0).getStaffRate();
        }

        //расчитываем выплаты
        //если формат вывода "Процент на МФОТ", то откладываем ее, что бы вычеслить позже
        Map<EmployeeBonus, Double> bonusAmountMap = new LinkedHashMap<>();//сохраняем суммы выплат
        List<EmployeePayment> paymentsList = new ArrayList<>();
        List<EmployeeBonus> totalBonusesList = new ArrayList<>();
        List<EmployeePayment> resultPaymentList = new ArrayList<>();
        for (EmployeeBonus bonus : employeeBonusList)
        {
            if (UniempDefines.PAYMENT_UNIT_FULL_PERCENT.equals(bonus.getPayment().getPaymentUnit().getCode()))
                totalBonusesList.add(bonus);
            else
            {
                CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcPairKey = new CoreCollectionUtils.Pair<>(bonus.getFinancingSource(), bonus.getFinancingSourceItem());
                double rate = staffRate != null ? staffRate : (staffRateMap.get(finSrcPairKey) != null ? staffRateMap.get(finSrcPairKey) : 0d);
                EmployeePayment payment = createOrAssignSingleEmployeePayment(session, bonus, post, salary, 0d, rate, sumStaffRate);
                paymentsList.add(payment);
                bonusAmountMap.put(bonus, payment.getAmount());
                resultPaymentList.add(payment);
            }
        }

        for (EmployeePayment payment : paymentsList)
            session.saveOrUpdate(payment);

        //расчитывае выплаты на МФОТ
        for (EmployeeBonus bonus : totalBonusesList)
        {
            CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem> finSrcPairKey = new CoreCollectionUtils.Pair<>(bonus.getFinancingSource(), bonus.getFinancingSourceItem());
            double rate = staffRate != null ? staffRate : (staffRateMap.get(finSrcPairKey) != null ? staffRateMap.get(finSrcPairKey) : 0d);
            List<Payment> dependPaymentList = UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(bonus.getPayment());//поднимаем выплаты на которые расчитывается текущая выплата(по настройке "Порядок начисления выплат на ФОТ с надбавками")
            if (dependPaymentList.isEmpty())//если не заполнена настройка, то расчитываем исходя из текущей суммы выплат
            {
                double paymentSumm = 0d;

                //если у сотрудника есть выплаты, то включаем их в расчет
                List<EmployeePayment> employeePostPaymentList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getCommitExtractActualEmployeePaymentList(extract, dependPaymentList, bonus.getBeginDate());
                for (EmployeePayment payment : employeePostPaymentList)
                {
                    //разовые выплаты не учитываем
                    if (payment.getPayment().isOneTimePayment())
                        continue;

                    //если период выплаты не актуален, то не учитываем
                    if (payment.getBeginDate().getTime() > bonus.getBeginDate().getTime() || (payment.getEndDate() != null && payment.getEndDate().getTime() < bonus.getBeginDate().getTime()))
                        continue;

                    CoreCollectionUtils.Pair paymentKey = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());

                    //если ист.фин. не совпадет, то не учитываем
                    if (!paymentKey.equals(finSrcPairKey))
                        continue;

                    paymentSumm += payment.getAmount();
                }

                //смотри среди добавленных в выписке выплат
                for (EmployeePayment payment : paymentsList)
                {
                    //разовые выплаты не учитываем
                    if (payment.getPayment().isOneTimePayment())
                        continue;

                    //если период выплаты не актуален, то не учитываем
                    if (payment.getBeginDate().getTime() > bonus.getBeginDate().getTime() || (payment.getEndDate() != null && payment.getEndDate().getTime() < bonus.getBeginDate().getTime()))
                        continue;

                    CoreCollectionUtils.Pair innerKey = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());

                    //если ист.фин. не совпадет, то не учитываем
                    if (!innerKey.equals(finSrcPairKey))
                        continue;

                    paymentSumm += payment.getAmount();
                }

                EmployeePayment payment = createOrAssignSingleEmployeePayment(session, bonus, post, salary, paymentSumm, rate, sumStaffRate);
                resultPaymentList.add(payment);
                session.saveOrUpdate(payment);
                bonusAmountMap.put(bonus, payment.getAmount());
            }
            else//если настройка для выплаты заполнена
            {
                List<EmployeeBonus> innerList = new ArrayList<>(employeeBonusList);
                innerList.remove(bonus);

                double paymentSumm = 0d;

                //если у сотрудника есть выплаты, то включаем их в расчет
                List<EmployeePayment> employeePostPaymentList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getCommitExtractActualEmployeePaymentList(extract, dependPaymentList, bonus.getBeginDate());
                for (EmployeePayment payment : employeePostPaymentList)
                {
                    //разовые выплаты не учитываем
                    if (payment.getPayment().isOneTimePayment())
                        continue;

                    //если выплаты нет в настройке, то не учитываем
                    if (!dependPaymentList.contains(payment.getPayment()))
                        continue;

                    //если период выплаты не актуален, то не учитываем
                    if (payment.getBeginDate().getTime() > bonus.getBeginDate().getTime() || (payment.getEndDate() != null && payment.getEndDate().getTime() < bonus.getBeginDate().getTime()))
                        continue;

                    CoreCollectionUtils.Pair paymentKey = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());

                    //если ист.фин. не совпадет, то не учитываем
                    if (!paymentKey.equals(finSrcPairKey))
                        continue;

                    paymentSumm += payment.getAmount();
                }

                //бежим по текущим выплатам и берем те, на которые расчитывается выплата исходя из настройки
                //если выплата не на МФОТ, то просто берем ее сумму, иначе расчитываем ее
                for (EmployeeBonus innerBonus : innerList)
                {
                    //разовые выплаты не учитываем
                    if (innerBonus.getPayment().isOneTimePayment())
                        continue;

                    //если выплаты нет в настройке, то не учитываем
                    if (!dependPaymentList.contains(innerBonus.getPayment()))
                        continue;

                    //если период выплаты не актуален, то не учитываем
                    if (innerBonus.getBeginDate().getTime() > bonus.getBeginDate().getTime() || (innerBonus.getEndDate() != null && innerBonus.getEndDate().getTime() < bonus.getBeginDate().getTime()))
                        continue;

                    CoreCollectionUtils.Pair innerKey = new CoreCollectionUtils.Pair<>(innerBonus.getFinancingSource(), innerBonus.getFinancingSourceItem());

                    //если ист.фин. не совпадет, то не учитываем
                    if (!innerKey.equals(finSrcPairKey))
                        continue;

                    double amount;
                    if (!innerBonus.getPayment().getPaymentUnit().getCode().equals(UniempDefines.PAYMENT_UNIT_FULL_PERCENT))
                        amount = bonusAmountMap.get(innerBonus);
                    else
                        amount = bonusAmountMap.get(innerBonus) != null ? bonusAmountMap.get(innerBonus) :
                                getPaymentOnFOTAmount(innerBonus, employeeBonusList, paymentsList, salary, rate, sumStaffRate);
                    paymentSumm += amount;
                }

                //получили сумму выплат, на которые расчитывается выплата по настройке
                EmployeePayment payment = createOrAssignSingleEmployeePayment(session, bonus, post, salary, paymentSumm, rate, sumStaffRate);//вычесляем сумму выплаты
                resultPaymentList.add(payment);
                session.saveOrUpdate(payment);
                bonusAmountMap.put(bonus, payment.getAmount());
            }
        }

        return resultPaymentList;
    }

    /**
     * Используется при проведении выписок. Расчитывается сумма выплаты, созданной в рамках выписки, фрмат вывода у которй "Процент от месячного фонда оплаты труда".<p/>
     * Учитывается настройка "Порядок начисления выплат на ФОТ с надбавками". Если для выплаты в настройке ничего не указано, то выплата расчитывается просто исходя из суммы выплат ставки и оклада.
     * Если настройка заполненна, то указанные в настройке выплаты берутся из списка bonusList(если они там есть) и исходя из них расчитывается сумма выплат.<p/>
     * Учитывается метка "Учитывать при расчете Суммы выплаты суммарную ставку" в выплате.
     *
     * @param fullPercentPayment выплата, для которой высчитывается сумма
     * @param bonusList          выплаты, исходя из которых будут производиться расчеты
     * @param baseSalary         базовый оклад, без учета ставки
     * @param staffRate          ставка, исходя из которой расчитывается выплата
     * @param summStaffRate      суммарная ставка
     * @return Сумма выплаты врублях.
     */
    public static double getPaymentOnFOTAmount(EmployeeBonus fullPercentPayment, List<EmployeeBonus> bonusList, List<EmployeePayment> employeePaymentList, double baseSalary, double staffRate, double summStaffRate)
    {
        Double resultAmount = 0d;
        List<Payment> includePaymentList = UniempDaoFacade.getUniempDAO().getPaymentOnFOTToPaymentsList(fullPercentPayment.getPayment());//поднимаем выплаты на которые расчитывается текущая выплата(по настройке "Порядок начисления выплат на ФОТ с надбавками")
        CoreCollectionUtils.Pair finSrcPairKey = new CoreCollectionUtils.Pair<>(fullPercentPayment.getFinancingSource(), fullPercentPayment.getFinancingSourceItem());
        if (includePaymentList.isEmpty())//если не заполнена настройка, то расчитываем исходя из текущей суммы выплат
        {
            double summPayments = 0d;

            for (EmployeePayment payment : employeePaymentList)
            {
                //разовые выплаты не учитываем
                if (payment.getPayment().isOneTimePayment())
                    continue;

                //если период выплаты не актуален, то не учитываем
                if (payment.getBeginDate().getTime() > fullPercentPayment.getBeginDate().getTime() || (payment.getEndDate() != null && payment.getEndDate().getTime() < fullPercentPayment.getBeginDate().getTime()))
                    continue;

                CoreCollectionUtils.Pair innerKey = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());

                //если ист.фин. не совпадет, то не учитываем
                if (!innerKey.equals(finSrcPairKey))
                    continue;

                summPayments += payment.getAmount();
            }

            return Math.round((baseSalary * staffRate + summPayments) * fullPercentPayment.getValue()) / 100d;
        }

        bonusList.remove(fullPercentPayment);

        //если настройка заполненна, то в списке назначенных выплат ищем указанные в настройке выплаты и расчитываем их сумму
        //если указана выплата на МФОТ, то вызываем рекурсию, бесконечной рекурсии не может быть, т.к. в настройке есть проверка что бы выплат ыне ссылались циклично друг на друга
        for (EmployeeBonus payment : bonusList)
        {
            //разовые выплаты не учитываем
            if (payment.getPayment().isOneTimePayment())
                continue;

            //если период выплаты не актуален, то не учитываем
            if (payment.getBeginDate().getTime() > fullPercentPayment.getBeginDate().getTime() || (payment.getEndDate() != null && payment.getEndDate().getTime() < fullPercentPayment.getBeginDate().getTime()))
                continue;

            CoreCollectionUtils.Pair innerKey = new CoreCollectionUtils.Pair<>(payment.getFinancingSource(), payment.getFinancingSourceItem());

            //если ист.фин. не совпадет, то не учитываем
            if (!innerKey.equals(finSrcPairKey))
                continue;

            //если выплаты нет в настройке, то не учитываем
            if (!includePaymentList.contains(payment.getPayment()))
                continue;

            double rate = payment.getPayment().isDoesntDependOnStaffRate() ? 1d : (payment.isDependOnSummStaffRate() ? summStaffRate : staffRate);
            double salary = baseSalary * rate;

            double paymentValue = payment.getValue();

            switch (payment.getPayment().getPaymentUnit().getCode())
            {
                case UniempDefines.PAYMENT_UNIT_RUBLES:
                    resultAmount += paymentValue * rate;
                    break;
                case UniempDefines.PAYMENT_UNIT_COEFFICIENT:
                    resultAmount += paymentValue * salary;
                    break;
                case UniempDefines.PAYMENT_UNIT_BASE_PERCENT:
                    resultAmount += (paymentValue * salary) / 100;
                    break;
                case UniempDefines.PAYMENT_UNIT_FULL_PERCENT:
                    List<EmployeeBonus> innerBonusList = new ArrayList<>(bonusList);
                    innerBonusList.remove(payment);
                    resultAmount += paymentValue * getPaymentOnFOTAmount(payment, innerBonusList, employeePaymentList, baseSalary, rate, summStaffRate) / 100;
                    break;
            }
        }

        double rate = fullPercentPayment.getPayment().isDoesntDependOnStaffRate() ? 1d : (fullPercentPayment.isDependOnSummStaffRate() ? summStaffRate : staffRate);
        double salary = baseSalary * rate;

        return (resultAmount + salary) * fullPercentPayment.getValue() / 100d;
    }

    /**
     * Производит проверку назначаемой должности на соответствие свободным ставкам указанной должности в момент проведения приказа.
     */
    public static void validateStaffRates(IPostAssignExtract extract, EmployeePost post, List<EmployeePostStaffRateItem> staffRateItemList)
    {
        UniDaoFacade.getCoreDao().getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_POSSIBLE);

        if (null != extract.getEntity()) // Should be refactored, when entity field type is changing
        {
            UniempDaoFacade.getUniempDAO().validateStaffRates(UserContext.getInstance().getErrorCollector(), extract.getEmployee(), extract.getEntity(), staffRateItemList, false, false);
        }
        else
            UniempDaoFacade.getUniempDAO().validateStaffRates(UserContext.getInstance().getErrorCollector(), extract.getEmployee(), post, staffRateItemList, true, false);
    }

    /**
     * Производит проверку ставок ШР в момент проведения приказа.
     */
    public static void validateAllocationItem(AbstractEmployeeExtract extract, OrgUnit orgUnit, PostBoundedWithQGandQL post)
    {
        List<FinancingSourceDetails> staffRateItems = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
        List<FinancingSourceDetailsToAllocItem> relList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(staffRateItems);

        if (staffRateItems.size() == 0 || relList.size() == 0)
            return;

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        for (FinancingSourceDetails details : staffRateItems)
        {
            Double diff = 0.0d;
            StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(orgUnit, post, details.getFinancingSource(), details.getFinancingSourceItem());
            if (staffListItem != null)
                diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
            BigDecimal x = new java.math.BigDecimal(diff);
            x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
            diff = x.doubleValue();

            if (details.getStaffRate() > diff)
                errorCollector.add("Невозможно провести приказ, так как превышено количество штатных единиц на подразделении " + '"' + orgUnit.getTitleWithType() + '"' + " по должности " + '"' + post.getFullTitle() + '"' + " - " + details.getFinancingSource().getTitle() + (details.getFinancingSourceItem() != null ? " (" + details.getFinancingSourceItem().getTitle() + ")" : "") + ".");
        }

        for (FinancingSourceDetailsToAllocItem relation : relList)
        {
            if (!relation.isHasNewAllocItem())
                if (relation.getChoseStaffListAllocationItem() != null)
                {
                    if (relation.getChoseStaffListAllocationItem().getEmployeePost() == null)
                        errorCollector.add("Невозможно провести приказ, поле «Кадровая расстановка» обязательно для заполнения.");
                    else if (relation.getChoseStaffListAllocationItem().getEmployeePost().getPostStatus().isActive())
                        errorCollector.add("Невозможно провести приказ, поскольку нельзя занять ставку активного сотрудника: " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPerson().getFio() + " на должности " + '"' + post.getFullTitle() + '"' + " подразделения " + '"' + orgUnit.getTitleWithType() + '"' + ".");
                }
        }
    }

    public static List<Payment> getStaffListAllocPaymentsForStaffListItem(IPostAssignExtract extract)
    {
        Set<Payment> paymentsSet = new HashSet<>();

        List<StaffListPostPayment> paymentsList = UniempDaoFacade.getStaffListDAO().getStaffListPostPaymentsList(extract.getOrgUnit(), extract.getPostBoundedWithQGandQL());
        if (null != paymentsList)
        {
            for (StaffListPaymentBase staffListPayment : paymentsList)
            {
                paymentsSet.add(staffListPayment.getPayment());
            }
        }

        List<Payment> result = new ArrayList<>();
        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList((AbstractEmployeeExtract) extract))
        {
            if (!paymentsSet.contains(bonus.getPayment())) result.add(bonus.getPayment());
        }

        return result;
    }

    /**
     * Проверяет валидность ШР при проведении приказа о совмещении.
     */
    public static void validateCombinationPostAllocItems(AbstractEmployeeExtract extract, OrgUnit orgUnit, PostBoundedWithQGandQL post, CombinationPost combinationPost)
    {
        List<CombinationPostStaffRateExtractRelation> staffRateRelationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getCombinationPostStaffRateExtractRelationList(extract);
        List<CombinationPostStaffRateExtAllocItemRelation> allocItemRelationList = new ArrayList<>();
        for (CombinationPostStaffRateExtractRelation relation : staffRateRelationList)
            allocItemRelationList.addAll(MoveEmployeeDaoFacade.getMoveEmployeeDao().getCombinationPostStaffRateExtAllocItemRelationList(relation));

        if (staffRateRelationList.isEmpty() || allocItemRelationList.isEmpty())
            return;

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        Map<CombinationPostStaffRateExtractRelation, List<CombinationPostStaffRateExtAllocItemRelation>> staffRateAllocItemMap = new HashMap<>();
        for (CombinationPostStaffRateExtAllocItemRelation relation : allocItemRelationList)
        {
            if (!relation.isHasNewAllocItem() && relation.getChoseStaffListAllocationItem() != null)
            {
                List<CombinationPostStaffRateExtAllocItemRelation> allocItemList = staffRateAllocItemMap.get(relation.getCombinationPostStaffRateExtractRelation());
                if (allocItemList == null)
                    allocItemList = new ArrayList<>();
                allocItemList.add(relation);
                staffRateAllocItemMap.put(relation.getCombinationPostStaffRateExtractRelation(), allocItemList);
            }
        }

        for (CombinationPostStaffRateExtractRelation relation : staffRateRelationList)
        {
            Double diff = 0.0d;
            StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(orgUnit, post, relation.getFinancingSource(), relation.getFinancingSourceItem());
            if (staffListItem != null)
                diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
            BigDecimal x = new java.math.BigDecimal(diff);
            x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
            diff = x.doubleValue();

            if (combinationPost != null && staffRateAllocItemMap.get(relation) != null)
                for (CombinationPostStaffRateExtAllocItemRelation allocItemRelation : staffRateAllocItemMap.get(relation))
                {
                    if (allocItemRelation.isHasCombinationPost())
                        if (allocItemRelation.getChoseStaffListAllocationItem().getEmployeePost() != null && allocItemRelation.getChoseStaffListAllocationItem().getEmployeePost().getPostStatus().isActive())
                            if (allocItemRelation.getChoseStaffListAllocationItem().getCombinationPost().equals(combinationPost))
                                diff += allocItemRelation.getChoseStaffListAllocationItem().getStaffRate();
                }

            if (relation.getStaffRate() > diff)
                errorCollector.add("Невозможно провести приказ, так как превышено количество штатных единиц на подразделении " + '"' + orgUnit.getTitleWithType() + '"' + " по должности " + '"' + post.getFullTitle() + '"' + " - " + relation.getFinancingSource().getTitle() + (relation.getFinancingSourceItem() != null ? " (" + relation.getFinancingSourceItem().getTitle() + ")" : "") + ".");
        }

        for (CombinationPostStaffRateExtAllocItemRelation relation : allocItemRelationList)
        {
            if (!relation.isHasNewAllocItem() && !relation.isHasCombinationPost())
                if (relation.getChoseStaffListAllocationItem() != null && relation.getChoseStaffListAllocationItem().getCombinationPost() == null)
                    if (relation.getChoseStaffListAllocationItem().getEmployeePost().getPostStatus().isActive())
                        errorCollector.add("Невозможно провести приказ, поскольку нельзя занять ставку активного сотрудника: " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPerson().getFio() + " на должности " + '"' + post.getFullTitle() + '"' + " подразделения " + '"' + orgUnit.getTitleWithType() + '"' + ".");
        }
    }

    /**
     * Производит проверку выплат сотрудника в момент проведения выписки.
     * 1. проверяется пересечение периодов выплат с одинаковым типом, иф, иф(д)
     */
    public static void validateEmployeePayment(EmployeePost employeePost, List<EmployeePayment> paymentList, Session session)
    {
        if (employeePost == null || paymentList == null)
            return;

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePayment.class, "b").column("b");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeePayment.employeePost().fromAlias("b")), employeePost));
        if (!paymentList.isEmpty())
            builder.where(DQLExpressions.notIn(DQLExpressions.property(EmployeePayment.id().fromAlias("b")), UniBaseDao.ids(paymentList)));

        List<EmployeePayment> employeePaymentList = builder.createStatement(session).list();

        for (EmployeePayment payment : paymentList)
            for (EmployeePayment employeePayment : employeePaymentList)
            {
                if (employeePayment.getPayment().equals(payment.getPayment()))
                    if (employeePayment.getFinancingSource().equals(payment.getFinancingSource()))
                        if ((employeePayment.getFinancingSourceItem() == null && payment.getFinancingSourceItem() == null) || (employeePayment.getFinancingSourceItem() != null && employeePayment.getFinancingSourceItem().equals(payment.getFinancingSourceItem())))
                        {
                            Date begin1 = employeePayment.getBeginDate();
                            Date end1 = employeePayment.getEndDate();
                            Date begin2 = payment.getBeginDate();
                            Date end2 = payment.getEndDate();

                            StringBuilder errorBuilder = new StringBuilder("Период выплаты ").append(payment.getTitle()).append(" ").append(payment.getFinancingSource().getTitle()).append((payment.getFinancingSourceItem() != null ? " " + payment.getFinancingSourceItem().getTitle() : "")).append("  пересекается с периодом такой же выплаты в списке «Выплаты» на карточке сотрудника.");

                            if (end1 == null && end2 == null)
                                errorCollector.add(errorBuilder.toString());
                            else if (end1 == null)
                            {
                                if (begin1.getTime() <= end2.getTime())
                                    errorCollector.add(errorBuilder.toString());
                            }
                            else if (end2 == null)
                            {
                                if (begin2.getTime() <= end1.getTime())
                                    errorCollector.add(errorBuilder.toString());
                            }
                            else
                            {
                                if (CommonBaseDateUtil.isBetween(begin1, begin2, end2) || CommonBaseDateUtil.isBetween(end1, begin2, end2))
                                    errorCollector.add(errorBuilder.toString());
                                else
                                {
                                    if (CommonBaseDateUtil.isBetween(begin2, begin1, end1) || CommonBaseDateUtil.isBetween(end2, begin1, end1))
                                        errorCollector.add(errorBuilder.toString());
                                }
                            }
                        }
            }
    }
}