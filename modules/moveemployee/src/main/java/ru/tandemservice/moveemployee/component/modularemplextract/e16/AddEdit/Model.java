/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e16.AddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.commons.IStaffRateListModel;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 08.04.2011
 */
public class Model extends CommonModularEmployeeExtractAddEditModel<ReturnFromChildCareHolidayExtract> implements IStaffRateListModel<ReturnFromChildCareHolidayExtract>
{
    private List<FinancingSourceDetails> _oldStaffRateItemList = new ArrayList<>();
    private List<Double> _oldStaffRateList = new ArrayList<>();
    private IMultiSelectModel _employeeHRModel;
    private List<FinancingSourceDetailsToAllocItem> _detailsToAllocItemList = new ArrayList<>();
    private boolean _thereAnyActiveStaffList = false;
    private boolean _needUpdateDataSource = true;

    private DynamicListDataSource<FinancingSourceDetails> _staffRateDataSource;
    private List<FinancingSourceDetails> _staffRateItemList = new ArrayList<>();
    private ISingleSelectModel _financingSourceItemModel;
    private List<FinancingSource> _financingSourceModel;

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;  //Body of implemented method
        }

        @Override
        public Location getLocation()
        {
            return null;  //Body of implemented method
        }
    };

    private List<CompetitionAssignmentType> _assignmentTypesList;
    private List<HolidayType> _holidayTypeList;
    private List<EmployeePayment> _paymentList;

    private boolean _savePaymentDisable;


    // Getters & Setters

    public String getStaffRateColumnId()
    {
        return "staffRate_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcColumnId()
    {
        return "finSrc_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItmColumnId()
    {
        return "finSrcItm_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getEmployeeHRId()
    {
        return "employeeHR_Id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public IMultiSelectModel getEmployeeHRModel()
    {
        return _employeeHRModel;
    }

    public void setEmployeeHRModel(IMultiSelectModel employeeHRModel)
    {
        _employeeHRModel = employeeHRModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public List<FinancingSourceDetails> getOldStaffRateItemList()
    {
        return _oldStaffRateItemList;
    }

    public void setOldStaffRateItemList(List<FinancingSourceDetails> oldStaffRateItemList)
    {
        _oldStaffRateItemList = oldStaffRateItemList;
    }

    public List<Double> getOldStaffRateList()
    {
        return _oldStaffRateList;
    }

    public void setOldStaffRateList(List<Double> oldStaffRateList)
    {
        _oldStaffRateList = oldStaffRateList;
    }

    @Override
    public List<FinancingSourceDetailsToAllocItem> getDetailsToAllocItemList()
    {
        return _detailsToAllocItemList;
    }

    @Override
    public void setDetailsToAllocItemList(List<FinancingSourceDetailsToAllocItem> detailsToAllocItemList)
    {
        _detailsToAllocItemList = detailsToAllocItemList;
    }

    @Override
    public boolean isThereAnyActiveStaffList()
    {
        return _thereAnyActiveStaffList;
    }

    @Override
    public void setThereAnyActiveStaffList(boolean thereAnyActiveStaffList)
    {
        _thereAnyActiveStaffList = thereAnyActiveStaffList;
    }

    @Override
    public boolean isNeedUpdateDataSource()
    {
        return _needUpdateDataSource;
    }

    @Override
    public void setNeedUpdateDataSource(boolean needUpdateDataSource)
    {
        _needUpdateDataSource = needUpdateDataSource;
    }

    @Override
    public DynamicListDataSource<FinancingSourceDetails> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    @Override
    public void setStaffRateDataSource(DynamicListDataSource<FinancingSourceDetails> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    @Override
    public List<FinancingSourceDetails> getStaffRateItemList()
    {
        return _staffRateItemList;
    }

    @Override
    public void setStaffRateItemList(List<FinancingSourceDetails> staffRateItemList)
    {
        _staffRateItemList = staffRateItemList;
    }

    @Override
    public List<FinancingSource> getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    @Override
    public void setFinancingSourceModel(List<FinancingSource> financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    public List<EmployeePayment> getPaymentList()
    {
        return _paymentList;
    }

    public void setPaymentList(List<EmployeePayment> paymentList)
    {
        _paymentList = paymentList;
    }

    public boolean isSavePaymentDisable()
    {
        return _savePaymentDisable;
    }

    public void setSavePaymentDisable(boolean savePaymentDisable)
    {
        _savePaymentDisable = savePaymentDisable;
    }

    public List<HolidayType> getHolidayTypeList()
    {
        return _holidayTypeList;
    }

    public void setHolidayTypeList(List<HolidayType> holidayTypeList)
    {
        _holidayTypeList = holidayTypeList;
    }

    public List<CompetitionAssignmentType> getAssignmentTypesList()
    {
        return _assignmentTypesList;
    }

    public void setAssignmentTypesList(List<CompetitionAssignmentType> assignmentTypesList)
    {
        _assignmentTypesList = assignmentTypesList;
    }
}