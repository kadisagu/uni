/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;

/**
 * @author dseleznev
 * Created on: 28.10.2009
 */
public abstract class AbstractListOrderAddEditController<T extends EmployeeListOrder, IDAO extends IAbstractListOrderAddEditDAO<T, Model>, Model extends AbstractListOrderAddEditModel<T>> extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        model.setFieldsPage("ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderAddEdit.ListOrderAddEditFields");
        model.setCommentsPage("ru.tandemservice.moveemployee.component.listemplextract.abstractorder.ListOrderAddEdit.BasicCommentAddEdit");

        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().update(model);

        if (model.getOrderId() == null)
        {
            // форма добавления
            deactivate(component, 2); //назад на 2-е страницы
//            int orderTypeIndex = model.getOrder().getType().getIndex();
//            activateInRoot(component, new ComponentActivator(MoveEmployeeUtils.getListOrderPubComponent(orderTypeIndex), new ParametersMap()
//                        .add(PublisherActivator.PUBLISHER_ID_KEY, model.getOrder().getId())));
            activateInRoot(component, new PublisherActivator(model.getOrder()));
        }
        else
            deactivate(component);
    }

    public void onClickDeactivate(IBusinessComponent component)
    {
        if (getModel(component).getOrderId() == null)
            deactivate(component, 2); // форма добавления
        else
            deactivate(component);
    }
}