/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import ru.tandemservice.moveemployee.component.commons.ICommonEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractAddEdit.ModularEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;

/**
 * @author dseleznev
 * Created on: 13.11.2008
 */
public abstract class CommonModularEmployeeExtractAddEditModel<T extends ModularEmployeeExtract> extends ModularEmployeeExtractAddEditModel<T> implements ICommonEmployeeExtractAddEditModel<T>
{
    private String _textParagraph;

    private IMultiSelectModel _basicListModel;
    
    private List<EmployeeOrderReasons> _reasonList;

    private List<EmployeeOrderBasics> _selectedBasicList = new ArrayList<EmployeeOrderBasics>();

    private static final String _commonPage = CommonModularEmployeeExtractAddEditModel.class.getPackage().getName() + ".CommonModularEmployeeExtractAddEdit";
    private static final String _commentsPage = CommonModularEmployeeExtractAddEditModel.class.getPackage().getName() + ".BasicCommentAddEdit";

    private EmployeeOrderBasics _currentBasic;
    private Map<Long, String> _currentBasicMap = new HashMap<Long, String>();
    private Map<Long, String> _defaultBasicMap = new HashMap<Long, String>();
    
    private String _orderReasonUpdates = "basics, comments, textParagraph";


    // Getters & Setters

    public String getTextParagraph()
    {
        return _textParagraph;
    }

    public void setTextParagraph(String textParagraph)
    {
        _textParagraph = textParagraph;
    }

    public String getCommonPage()
    {
        return _commonPage;
    }

    public String getCommentsPage()
    {
        return _commentsPage;
    }

    public boolean isNeedComments()
    {
        for (EmployeeOrderBasics basic : _selectedBasicList)
            if (basic.isCommentable()) return true;
        return false;
    }
    
    public String getCurrentBasicTitle()
    {
        return _currentBasicMap.get(_currentBasic.getId());
    }

    public void setCurrentBasicTitle(String title)
    {
        _currentBasicMap.put(_currentBasic.getId(), title);
    }

    public String getCurrentBasicId()
    {
        return "basicId_" + _currentBasic.getId();
    }

    @Override
    public IMultiSelectModel getBasicListModel()
    {
        return _basicListModel;
    }

    @Override
    public void setBasicListModel(IMultiSelectModel basicListModel)
    {
        this._basicListModel = basicListModel;
    }

    public List<EmployeeOrderReasons> getReasonList()
    {
        return _reasonList;
    }

    public void setReasonList(List<EmployeeOrderReasons> reasonList)
    {
        this._reasonList = reasonList;
    }

    @Override
    public List<EmployeeOrderBasics> getSelectedBasicList()
    {
        return _selectedBasicList;
    }

    @Override
    public void setSelectedBasicList(List<EmployeeOrderBasics> selectedBasicList)
    {
        this._selectedBasicList = selectedBasicList;
    }

    public Map<Long, String> getCurrentBasicMap()
    {
        return _currentBasicMap;
    }

    public void setCurrentBasicMap(Map<Long, String> currentBasicMap)
    {
        this._currentBasicMap = currentBasicMap;
    }

    public Map<Long, String> getDefaultBasicMap()
    {
        return _defaultBasicMap;
    }

    public void setDefaultBasicMap(Map<Long, String> defaultBasicMap)
    {
        this._defaultBasicMap = defaultBasicMap;
    }

    public String getOrderReasonUpdates()
    {
        return _orderReasonUpdates;
    }

    public void setOrderReasonUpdates(String orderReasonUpdates)
    {
        this._orderReasonUpdates = orderReasonUpdates;
    }

    public EmployeeOrderBasics getCurrentBasic()
    {
        return _currentBasic;
    }

    public void setCurrentBasic(EmployeeOrderBasics currentBasic)
    {
        this._currentBasic = currentBasic;
    }
}