/**
 *$Id$
 */
package ru.tandemservice.moveemployee.base.bo.MoveemployeeContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.moveemployee.base.bo.MoveemployeeContract.logic.ContractDao;
import ru.tandemservice.moveemployee.base.bo.MoveemployeeContract.logic.IContractDao;

/**
 * Create by ashaburov
 * Date 15.05.12
 */
@Configuration
public class MoveemployeeContractManager extends BusinessObjectManager
{
    public static MoveemployeeContractManager instance()
    {
        return instance(MoveemployeeContractManager.class);
    }

    @Bean
    public IContractDao dao()
    {
        return new ContractDao();
    }
}
