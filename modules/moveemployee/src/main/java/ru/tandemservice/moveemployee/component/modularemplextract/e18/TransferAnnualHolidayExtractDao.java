/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e18;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.HolidayDataToExtractRelation;
import ru.tandemservice.moveemployee.entity.HolidayToTransferAnnualHolidayExtRelation;
import ru.tandemservice.moveemployee.entity.NewPartInTransferAnnualHolidayExtract;
import ru.tandemservice.moveemployee.entity.PeriodPartToExtractRelation;
import ru.tandemservice.moveemployee.entity.ScheduleItemDataToExtractRelation;
import ru.tandemservice.moveemployee.entity.TransferAnnualHolidayExtract;
import ru.tandemservice.moveemployee.entity.TransferHolidayDateToExtractRelation;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 15.09.2011
 */
public class TransferAnnualHolidayExtractDao extends UniBaseDao implements IExtractComponentDao<TransferAnnualHolidayExtract>
{
    @Override
    public void doCommit(TransferAnnualHolidayExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        List<TransferHolidayDateToExtractRelation> transferHolidayList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getTransferHolidayDateFromExtractRelation(extract);

        //если заполнены поля Предоставляемый отпуск
        if (!transferHolidayList.isEmpty())
        {
            //если выбран Ежегодный отпуск,
            if (extract.getEmployeeHoliday() instanceof EmployeeHoliday)
            {
                List<EmployeeHoliday> toCreateHolidayList = new ArrayList<EmployeeHoliday>();
                List<EmployeeHoliday> toDeleteHolidayList = new ArrayList<EmployeeHoliday>();
                List<PeriodPartToExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getPeriodPartFromExtractRelation(extract);

                //сохраняем исходные значения отпусков
                for (PeriodPartToExtractRelation relation : relationList)
                {
                    EmployeeHoliday holiday = (EmployeeHoliday) relation.getPart();

                    HolidayDataToExtractRelation newRelation = new HolidayDataToExtractRelation();
                    newRelation.setTransferAnnualHolidayExtract(extract);
                    newRelation.setHolidayTitle(holiday.getTitle());
                    newRelation.setHolidayType(holiday.getHolidayType());
                    newRelation.setHolidayBegin(holiday.getBeginDate());
                    newRelation.setHolidayEnd(holiday.getEndDate());
                    newRelation.setHolidayStart(holiday.getStartDate());
                    newRelation.setHolidayFinish(holiday.getFinishDate());
                    newRelation.setHolidayDuration(holiday.getDuration());
                    newRelation.setHolidayExtract(holiday.getHolidayExtract());
                    newRelation.setHolidayExtractNumber(holiday.getHolidayExtractNumber());
                    newRelation.setHolidayExtractDate(holiday.getHolidayExtractDate());

                    save(newRelation);

                    delete(relation);

                    toDeleteHolidayList.add(holiday);
                }

                //создаем новые отпуска с тем же периодом, если указано несколько частей отпуска
                EmployeeHoliday holiday = (EmployeeHoliday) relationList.get(0).getPart();
                for (TransferHolidayDateToExtractRelation transferHolidayDate : transferHolidayList)
                {
                    EmployeeHoliday newHoliday = new EmployeeHoliday();
                    newHoliday.setEmployeePost(extract.getEntity());
                    newHoliday.setTitle(holiday.getTitle());
                    newHoliday.setHolidayType(holiday.getHolidayType());
                    newHoliday.setBeginDate(holiday.getBeginDate());
                    newHoliday.setEndDate(holiday.getEndDate());
                    newHoliday.setStartDate(transferHolidayDate.getBeginDay());
                    newHoliday.setFinishDate(transferHolidayDate.getEndDay());
                    newHoliday.setDuration(transferHolidayDate.getDurationDay());
                    newHoliday.setHolidayExtract(extract);
                    newHoliday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                    newHoliday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

                    toCreateHolidayList.add(newHoliday);
                }

                //выбранные отпуска удаляем
                for (EmployeeHoliday employeeHoliday : toDeleteHolidayList)
                    delete(employeeHoliday);
                for (EmployeeHoliday employeeHoliday : toCreateHolidayList)
                {
                    HolidayToTransferAnnualHolidayExtRelation relation = new HolidayToTransferAnnualHolidayExtRelation();
                    relation.setNewObject(employeeHoliday);
                    relation.setTransferAnnualHolidayExtract(extract);

                    save(employeeHoliday);

                    save(relation);
                }

            }
            //если выбран График отпусков
            else if (extract.getEmployeeHoliday() instanceof VacationScheduleItem)
            {
                List<VacationScheduleItem> toCreateScheduleItemList = new ArrayList<VacationScheduleItem>();
                List<VacationScheduleItem> toDeleteScheduleItemList = new ArrayList<VacationScheduleItem>();
                List<PeriodPartToExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getPeriodPartFromExtractRelation(extract);

                //сохраняем данные выбранных графиков отпусков
                for (PeriodPartToExtractRelation relation : relationList)
                {
                    VacationScheduleItem scheduleItem = (VacationScheduleItem) relation.getPart();

                    ScheduleItemDataToExtractRelation newRelation = new ScheduleItemDataToExtractRelation();
                    newRelation.setTransferAnnualHolidayExtract(extract);
                    newRelation.setVacationSchedule(scheduleItem.getVacationSchedule());
                    newRelation.setComment(scheduleItem.getComment());
                    newRelation.setPlanDate(scheduleItem.getPlanDate());
                    newRelation.setFactDate(scheduleItem.getFactDate());
                    newRelation.setDaysAmount(scheduleItem.getDaysAmount());
                    newRelation.setPostponeDate(scheduleItem.getPostponeDate());
                    newRelation.setPostponeBasic(scheduleItem.getPostponeBasic());

                    toDeleteScheduleItemList.add(scheduleItem);

                    save(newRelation);

                    delete(relation);
                }

                //создаем новые графики отпусков, указывая дату и причину переноса
                VacationScheduleItem scheduleItem = (VacationScheduleItem) relationList.get(0).getPart();
                for (TransferHolidayDateToExtractRelation transferHolidayDate : transferHolidayList)
                {
                    VacationScheduleItem newScheduleItem = new VacationScheduleItem();
                    newScheduleItem.setEmployeePost(extract.getEntity());
                    newScheduleItem.setVacationSchedule(scheduleItem.getVacationSchedule());
                    newScheduleItem.setComment(scheduleItem.getComment());
                    newScheduleItem.setPlanDate(scheduleItem.getPlanDate());
                    newScheduleItem.setFactDate(transferHolidayDate.getBeginDay());
                    newScheduleItem.setDaysAmount(transferHolidayDate.getDurationDay());
                    newScheduleItem.setPostponeDate(extract.getCreateDate());
                    newScheduleItem.setPostponeBasic("Приказ №" + extract.getParagraph().getOrder().getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getCreateDate()));

                    toCreateScheduleItemList.add(newScheduleItem);
                }

                //выбранные отпуска удаляем
                for (VacationScheduleItem vacationScheduleItem : toDeleteScheduleItemList)
                    delete(vacationScheduleItem);
                for (VacationScheduleItem vacationScheduleItem : toCreateScheduleItemList)
                {
                    HolidayToTransferAnnualHolidayExtRelation relation = new HolidayToTransferAnnualHolidayExtRelation();
                    relation.setNewObject(vacationScheduleItem);
                    relation.setTransferAnnualHolidayExtract(extract);

                    save(vacationScheduleItem);

                    save(relation);
                }

            }
            //если указан -Новый период-
            else if (extract.getEmployeeHoliday() instanceof NewPartInTransferAnnualHolidayExtract)
            {
                //создаем новые отпуска с указанным периодом и на перенесенные сроки
                for (TransferHolidayDateToExtractRelation transferHolidayDate : transferHolidayList)
                {
                    EmployeeHoliday newHoliday = new EmployeeHoliday();
                    newHoliday.setEmployeePost(extract.getEntity());
                    newHoliday.setTitle(extract.getHolidayTitle());
                    newHoliday.setHolidayType(getCatalogItem(HolidayType.class, UniempDefines.HOLIDAY_TYPE_ANNUAL));
                    newHoliday.setBeginDate(extract.getBeginHolidayDate());
                    newHoliday.setEndDate(extract.getEndHolidayDate());
                    newHoliday.setStartDate(transferHolidayDate.getBeginDay());
                    newHoliday.setFinishDate(transferHolidayDate.getEndDay());
                    newHoliday.setDuration(transferHolidayDate.getDurationDay());
                    newHoliday.setHolidayExtract(extract);
                    newHoliday.setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());
                    newHoliday.setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());

                    HolidayToTransferAnnualHolidayExtRelation relation = new HolidayToTransferAnnualHolidayExtRelation();
                    relation.setNewObject(newHoliday);
                    relation.setTransferAnnualHolidayExtract(extract);

                    save(newHoliday);

                    save(relation);
                }
            }
        }
        //если даты Предостовляемого отпуска не указаны
        else
        {
            //если выбран Ежегодный отпуск
            if (extract.getEmployeeHoliday() instanceof EmployeeHoliday)
            {
                List<PeriodPartToExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getPeriodPartFromExtractRelation(extract);

                //сохраняем даттые отпуска и удаляем его
                for (PeriodPartToExtractRelation relation : relationList)
                {
                    EmployeeHoliday holiday = (EmployeeHoliday) relation.getPart();

                    HolidayDataToExtractRelation newRelation = new HolidayDataToExtractRelation();
                    newRelation.setTransferAnnualHolidayExtract(extract);
                    newRelation.setHolidayStart(holiday.getStartDate());
                    newRelation.setHolidayFinish(holiday.getFinishDate());
                    newRelation.setHolidayDuration(holiday.getDuration());
                    newRelation.setHolidayBegin(holiday.getBeginDate());
                    newRelation.setHolidayEnd(holiday.getEndDate());
                    newRelation.setHolidayTitle(holiday.getTitle());
                    newRelation.setHolidayType(holiday.getHolidayType());
                    newRelation.setHolidayExtract(holiday.getHolidayExtract());
                    newRelation.setHolidayExtractNumber(holiday.getHolidayExtractNumber());
                    newRelation.setHolidayExtractDate(holiday.getHolidayExtractDate());

                    save(newRelation);

                    delete(relation);

                    delete(holiday);
                }
            }
        }
    }

    @Override
    public void doRollback(TransferAnnualHolidayExtract extract, Map parameters)
    {
        MQBuilder holidayBuilder = new MQBuilder(HolidayDataToExtractRelation.ENTITY_CLASS, "b");
        holidayBuilder.add(MQExpression.eq("b", HolidayDataToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, extract));
        List<HolidayDataToExtractRelation> deleteHolidayList = holidayBuilder.getResultList(getSession());

        MQBuilder scheduleItemBuilder = new MQBuilder(ScheduleItemDataToExtractRelation.ENTITY_CLASS, "b");
        scheduleItemBuilder.add(MQExpression.eq("b", ScheduleItemDataToExtractRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, extract));
        List<ScheduleItemDataToExtractRelation> deleteScheduleItemList = scheduleItemBuilder.getResultList(getSession());

        MQBuilder builder = new MQBuilder(HolidayToTransferAnnualHolidayExtRelation.ENTITY_CLASS, "b");
        builder.add(MQExpression.eq("b", HolidayToTransferAnnualHolidayExtRelation.L_TRANSFER_ANNUAL_HOLIDAY_EXTRACT, extract));
        List<HolidayToTransferAnnualHolidayExtRelation> createHolidayList = builder.getResultList(getSession());

        if (!(extract.getEmployeeHoliday() instanceof NewPartInTransferAnnualHolidayExtract))
        {
            if (!deleteHolidayList.isEmpty())
            {
                for (HolidayToTransferAnnualHolidayExtRelation relation : createHolidayList)
                {
                    EmployeeHoliday employeeHoliday = (EmployeeHoliday) relation.getNewObject();
                    delete(relation);
                    delete(employeeHoliday);
                }

                for (HolidayDataToExtractRelation relation : deleteHolidayList)
                {
                    EmployeeHoliday oldHoliday = new EmployeeHoliday();
                    oldHoliday.setEmployeePost(extract.getEntity());
                    oldHoliday.setTitle(relation.getHolidayTitle());
                    oldHoliday.setHolidayType(relation.getHolidayType());
                    oldHoliday.setBeginDate(relation.getHolidayBegin());
                    oldHoliday.setEndDate(relation.getHolidayEnd());
                    oldHoliday.setStartDate(relation.getHolidayStart());
                    oldHoliday.setFinishDate(relation.getHolidayFinish());
                    oldHoliday.setDuration(relation.getHolidayDuration());
                    oldHoliday.setHolidayExtract(relation.getHolidayExtract());
                    oldHoliday.setHolidayExtractNumber(relation.getHolidayExtractNumber());
                    oldHoliday.setHolidayExtractDate(relation.getHolidayExtractDate());

                    extract.setEmployeeHoliday(oldHoliday);

                    PeriodPartToExtractRelation newRelation = new PeriodPartToExtractRelation();
                    newRelation.setTransferAnnualHolidayExtract(extract);
                    newRelation.setPart(oldHoliday);

                    save(oldHoliday);
                    save(newRelation);
                    delete(relation);
                }
                update(extract);
            }
            else if (!deleteScheduleItemList.isEmpty())
            {
                for (HolidayToTransferAnnualHolidayExtRelation relation : createHolidayList)
                {
                    VacationScheduleItem scheduleItem = (VacationScheduleItem) relation.getNewObject();
                    delete(relation);
                    delete(scheduleItem);
                }

                for (ScheduleItemDataToExtractRelation relation : deleteScheduleItemList)
                {
                    VacationScheduleItem oldScheduleItem = new VacationScheduleItem();
                    oldScheduleItem.setEmployeePost(extract.getEntity());
                    oldScheduleItem.setVacationSchedule(relation.getVacationSchedule());
                    oldScheduleItem.setComment(relation.getComment());
                    oldScheduleItem.setPostponeBasic(relation.getPostponeBasic());
                    oldScheduleItem.setPostponeDate(relation.getPostponeDate());
                    oldScheduleItem.setPlanDate(relation.getPlanDate());
                    oldScheduleItem.setFactDate(relation.getFactDate());
                    oldScheduleItem.setDaysAmount(relation.getDaysAmount());

                    save(oldScheduleItem);
                    delete(relation);

                    extract.setEmployeeHoliday(oldScheduleItem);
                    update(extract);

                    PeriodPartToExtractRelation newRelation = new PeriodPartToExtractRelation();
                    newRelation.setTransferAnnualHolidayExtract(extract);
                    newRelation.setPart(oldScheduleItem);

                    save(newRelation);
                }
            }
        }
        else
        {
            for (HolidayToTransferAnnualHolidayExtRelation relation : createHolidayList)
            {
                EmployeeHoliday employeeHoliday = (EmployeeHoliday) relation.getNewObject();
                delete(relation);
                delete(employeeHoliday);
            }
        }

    }
}