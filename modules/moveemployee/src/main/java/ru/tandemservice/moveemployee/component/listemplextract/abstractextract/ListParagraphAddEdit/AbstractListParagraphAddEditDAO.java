/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListParagraphAddEdit;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.dao.IEntityRelation;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author ashaburov
 * Created on: 11.10.2011
 */
public abstract class AbstractListParagraphAddEditDAO<T extends ListEmployeeExtract, Model extends AbstractListParagraphAddEditModel<T>> extends UniDao<Model> implements IAbstractListParagraphAddEditDAO<T, Model>
{
    private static OrderDescriptionRegistry orderRegistry = new OrderDescriptionRegistry("p");

    static
    {
        orderRegistry.setOrders(Employee.EMPLOYEE_FIO, new OrderDescription("p", Employee.EMPLOYEE_LAST_NAME), new OrderDescription("p", Employee.EMPLOYEE_FIRST_NAME), new OrderDescription("p", Employee.EMPLOYEE_MIDDLE_NAME));
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public void prepare(final Model model)
    {
        if (model.getParagraphId() != null)
        {
            // форма редактирования
            model.setAddForm(false);
            model.setParagraph(getNotNull(EmployeeListParagraph.class, model.getParagraphId()));
            model.setOrder((EmployeeListOrder) model.getParagraph().getOrder());
            model.setExtractType(getNotNull(EmployeeExtractType.class, model.getExtractTypeId()));
            model.setExtractList((List<T>) model.getParagraph().getExtractList());
            model.setReason(model.getExtractList().get(0).getReason());
        }
        else
        {
            // форма создания
            model.setAddForm(true);
            model.setExtractType(getNotNull(EmployeeExtractType.class, model.getExtractTypeId()));

            if (null != model.getOrderId())
                model.setOrder(getNotNull(EmployeeListOrder.class, model.getOrderId()));

            model.setParagraph(new EmployeeListParagraph());
            model.getParagraph().setOrder(model.getOrder());
            model.getParagraph().setNumber(model.getOrder().getParagraphCount() + 1);
        }

        model.setEditForm(!model.isAddForm());

        model.setReasonList(getFirstList(EmployeeReasonToTypeRel.class, model.getOrder().getType()));

        model.setBasicListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getReason() == null)
                    return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(EmployeeReasonToBasicRel.ENTITY_CLASS, "r", new String[] { IEntityRelation.L_SECOND });
                builder.add(MQExpression.eq("r", IEntityRelation.L_FIRST, model.getReason()));
                builder.add(MQExpression.like("r", IEntityRelation.L_SECOND + "." + EmployeeOrderBasics.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("r", IEntityRelation.L_SECOND + "." + EmployeeOrderBasics.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        if (model.isEditForm())
        {
            List<EmpListExtractToBasicRelation> relationList = getList(EmpListExtractToBasicRelation.class, EmpListExtractToBasicRelation.L_EXTRACT, model.getParagraph().getFirstExtract());

            model.getSelectedBasicList().clear();
            for (EmpListExtractToBasicRelation relation : relationList)
                model.getSelectedBasicList().add(relation.getBasic());
            model.setSelectedBasicList(model.getSelectedBasicList());

            model.getCurrentBasicMap().clear();
            for (EmpListExtractToBasicRelation relation : relationList)
            {
                model.getCurrentBasicMap().put(relation.getBasic().getId(), relation.getComment());
            }
        }

        model.setEmployeeListModel(new BaseSingleSelectModel(Employee.P_FULL_FIO_WITH_BIRTH_YEAR)
        {
            @Override
            public ListResult findValues(String filter)
            {
                String[] fio = StringUtils.split(filter);
                MQBuilder builder = new MQBuilder(Employee.ENTITY_CLASS, "e");
                builder.add(MQExpression.eq("e", Employee.P_ARCHIVAL, Boolean.FALSE));
                if (fio.length > 0) builder.add(MQExpression.like("e", Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike(fio[0])));
                if (fio.length > 1) builder.add(MQExpression.like("e", Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME, CoreStringUtils.escapeLike(fio[1])));
                if (fio.length > 2) builder.add(MQExpression.like("e", Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME, CoreStringUtils.escapeLike(fio[2])));
                builder.add(MQExpression.notExists(new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "ex")
                .addDomain("ep", EmployeePost.ENTITY_CLASS)
                .add(MQExpression.eqProperty("ex", IAbstractExtract.L_ENTITY, "ep", EmployeePost.id().s()))
                .add(MQExpression.eqProperty("ep", EmployeePost.L_EMPLOYEE, "e", Employee.id().s()))
                .add(MQExpression.notEq("ex", IAbstractExtract.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED))));
                builder.addOrder("e", Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
                builder.addOrder("e", Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
                //builder.addOrder("e", Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." +  IdentityCard.P_LAST_NAME);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = UniDaoFacade.getCoreDao().get(Employee.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                return null;
            }
        });

        model.setEmployeePostListModel(new BaseSingleSelectModel(EmployeePost.P_EXTRA_INFORMATIVE_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                String[] fio = StringUtils.split(filter);
                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "p");
                builder.add(MQExpression.eq("p", EmployeePost.L_EMPLOYEE + "." + Employee.P_ARCHIVAL, Boolean.FALSE));
                if (fio.length > 0) builder.add(MQExpression.like("p", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike(fio[0])));
                if (fio.length > 1) builder.add(MQExpression.like("p", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME, CoreStringUtils.escapeLike(fio[1])));
                if (fio.length > 2) builder.add(MQExpression.like("p", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_MIDDLE_NAME, CoreStringUtils.escapeLike(fio[2])));
                builder.add(MQExpression.eq("p", EmployeePost.L_ORG_UNIT + "." + OrgUnit.P_ARCHIVAL, Boolean.FALSE));
                builder.add(MQExpression.notExists(new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "ex")
                .add(MQExpression.eqProperty("ex", AbstractEmployeeExtract.entity().id().s(), "p", EmployeePost.id().s()))
                .add(MQExpression.notEq("ex", AbstractEmployeeExtract.id().s(), model.getExtractList() != null ? CommonBaseEntityUtil.getIdList(model.getExtractList()) : new ArrayList<>()))
                .add(MQExpression.notEq("ex", AbstractEmployeeExtract.state().code().s(), UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED))));
                builder.addOrder("p", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
                builder.addOrder("p", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
                builder.addOrder("p", EmployeePost.L_POST_RELATION + "." + OrgUnitTypePostRelation.L_POST_BOUNDED_WITH_Q_GAND_Q_L + "." + PostBoundedWithQGandQL.P_TITLE);
                builder.addOrder("p", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_PRIORITY);
                builder.addOrder("p", EmployeePost.L_ORG_UNIT + "." + OrgUnit.L_ORG_UNIT_TYPE + "." + OrgUnitType.P_TITLE);
                builder.addOrder("p", EmployeePost.L_ORG_UNIT + "." + OrgUnit.P_SHORT_TITLE);
                //builder.addOrder("e", Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." +  IdentityCard.P_LAST_NAME);
                return new ListResult<>(builder.getResultList(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = UniDaoFacade.getCoreDao().get(EmployeePost.class, (Long)primaryKey);
                if (findValues("").getObjects().contains(entity)) return entity;
                return null;
            }
        });

        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        // V A L I D A T E
        final Session session = getSession();

        session.refresh(model.getParagraph().getOrder());
        if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(model.getParagraph().getOrder().getState().getCode()))
            throw new ApplicationException("Нельзя изменять приказ. Изменение приказа возможно только в состоянии «Формируется».");

        //удаляем выписки у параграфа
        for (IAbstractExtract extract : model.getParagraph().getExtractList())
            delete(extract);

        session.saveOrUpdate(model.getParagraph());

        for (T extract : model.getToSaveExtractList())
        {
            session.saveOrUpdate(extract);
            updateBasicList(model.isEditForm(), extract, model.getSelectedBasicList(), model.getCurrentBasicMap());
        }


        List<EmployeeListParagraph> paragraphList = getList(EmployeeListParagraph.class, IAbstractParagraph.L_ORDER,model.getOrder());
        List<ListEmployeeExtract> extractList = new ArrayList<>();
        for (EmployeeListParagraph item : paragraphList)
        {
            extractList.addAll((List<ListEmployeeExtract>)item.getExtractList());
        }
        extractList.sort(ListEmployeeExtract.FULL_FIO_AND_ID_COMPARATOR);

        // нумеруем выписки с единицы
        int counter = 1;
        for (ListEmployeeExtract extract : extractList)
        {
            extract.setNumber(counter++);
            update(extract);
        }
    }

    // конструктор выписки
    protected abstract T createNewExtractInstance(Model model);

    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    /*    // TODO: doesn't need this any more
        private void syncPreCommitData(Model model)
        {
            T extract = model.getExtractBuffer();
            Employee employee = extract.getEmployee();
            EmployeePost employeePost = extract.getEntity();

            /*extract.setStudentTitleStr(employee.getPerson().getFullFio());
            extract.setStudentStatusStr(employee.getStatus().getTitle());
            extract.setPersonalNumberStr(Integer.toString(employee.getPersonalNumber()));
            extract.setCourseStr(employee.getCourse().getTitle());/
        }*/

    private void updateBasicList(boolean isEditForm, ListEmployeeExtract extract, List<EmployeeOrderBasics> selectedBasicList, Map<Long, String> currentBasicMap)
    {
        if (isEditForm)
        {
            for (EmpListExtractToBasicRelation relation : getList(EmpListExtractToBasicRelation.class, EmpListExtractToBasicRelation.L_EXTRACT, extract))
                delete(relation);
            getSession().flush();
        }

        for (EmployeeOrderBasics basic : selectedBasicList)
        {
            EmpListExtractToBasicRelation relation = new EmpListExtractToBasicRelation();
            relation.setBasic(basic);
            relation.setExtract(extract);
            relation.setComment(currentBasicMap.get(basic.getId()));
            save(relation);
        }
    }
}