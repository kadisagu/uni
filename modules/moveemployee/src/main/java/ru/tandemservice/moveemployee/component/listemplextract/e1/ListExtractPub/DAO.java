/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e1.ListExtractPub;

import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeAddEmplListExtract;

/**
 * @author ListExtractComponentGenerator
 * @since 12.05.2009
 */
public class DAO extends AbstractListExtractPubDAO<EmployeeAddEmplListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStaffRateStr(CommonEmployeeExtractUtil.getExtractFinancingSourceDetailsString(model.getExtract(), getSession(), true, null));
        model.setEmployeePPS(EmployeeTypeCodes.EDU_STAFF.equals(model.getExtract().getPostBoundedWithQGandQL().getPost().getEmployeeType().getCode()));
    }
}