/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e25.Pub;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.entity.RepealExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.01.2012
 */
public class Model extends ModularEmployeeExtractPubModel<RepealExtract>
{
    String _extractStr;

    //Getters & Setters

    public String getExtractStr()
    {
        return _extractStr;
    }

    public void setExtractStr(String extractStr)
    {
        _extractStr = extractStr;
    }
}