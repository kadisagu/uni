package ru.tandemservice.moveemployee.entity;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.component.commons.ITransferEmployeePostExtract;
import ru.tandemservice.moveemployee.entity.gen.*;

/**
 * Выписка из сборного приказа по кадровому составу. Об установлении доплат в связи с присуждением ученой степени
 */
public class ScienceDegreePaymentExtract extends ScienceDegreePaymentExtractGen implements ITransferEmployeePostExtract
{
    @Override
    public EmployeePost getEmployeePostOld()
    {
        return getEntity();
    }
}