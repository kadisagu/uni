/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.catalog.moveemployeeTemplate.MoveemployeeTemplatePub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogPub.DefaultPrintCatalogPubDAO;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author AutoGenerator
 * Created on 13.01.2009
 */
public class DAO extends DefaultPrintCatalogPubDAO<MoveemployeeTemplate, Model> implements IDAO
{
    @Override
    protected void prepareListItemDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(model.getItemClass().getName(), "ci");

        applyFilters(model, builder);
        
        builder.add(MQExpression.eq("ci", MoveemployeeTemplate.L_TYPE + "." + EmployeeExtractType.P_ACTIVE, Boolean.TRUE));
        getOrderDescriptionRegistry().applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}