package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. Об установлении работнику доплат
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeAddPaymentsSExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract";
    public static final String ENTITY_NAME = "employeeAddPaymentsSExtract";
    public static final int VERSION_HASH = 1525546066;
    private static IEntityMeta ENTITY_META;

    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String L_RAISING_COEFFICIENT_OLD = "raisingCoefficientOld";
    public static final String P_SALARY = "salary";
    public static final String P_SALARY_OLD = "salaryOld";
    public static final String L_LABOUR_CONTRACT_TYPE = "labourContractType";
    public static final String P_LABOUR_CONTRACT_NUMBER = "labourContractNumber";
    public static final String P_LABOUR_CONTRACT_DATE = "labourContractDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_COLLATERAL_AGREEMENT_NUMBER = "collateralAgreementNumber";
    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String P_HOURS_AMOUNT = "hoursAmount";
    public static final String L_LABOUR_CONTRACT = "labourContract";
    public static final String L_COLLATERAL_AGREEMENT = "collateralAgreement";

    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private SalaryRaisingCoefficient _raisingCoefficientOld;     // Повышающий коэффициент на момент проведения приказа
    private double _salary;     // Сумма оплаты
    private double _salaryOld;     // Сумма оплаты на момент проведения приказа
    private LabourContractType _labourContractType;     // Тип трудового договора
    private String _labourContractNumber;     // Номер трудового договора
    private Date _labourContractDate;     // Дата трудового договора
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private String _collateralAgreementNumber;     // Номер доп. соглашения
    private Date _transferDate;     // Дата доп. соглашения
    private Integer _hoursAmount;     // Учебная нагрузка в течении учебного года
    private EmployeeLabourContract _labourContract;     // Трудовой договор, созданный по выписке
    private ContractCollateralAgreement _collateralAgreement;     // Дополнительное соглашение к трудовому договору, созданное по выписке

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Повышающий коэффициент на момент проведения приказа.
     */
    public SalaryRaisingCoefficient getRaisingCoefficientOld()
    {
        return _raisingCoefficientOld;
    }

    /**
     * @param raisingCoefficientOld Повышающий коэффициент на момент проведения приказа.
     */
    public void setRaisingCoefficientOld(SalaryRaisingCoefficient raisingCoefficientOld)
    {
        dirty(_raisingCoefficientOld, raisingCoefficientOld);
        _raisingCoefficientOld = raisingCoefficientOld;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Сумма оплаты на момент проведения приказа. Свойство не может быть null.
     */
    @NotNull
    public double getSalaryOld()
    {
        return _salaryOld;
    }

    /**
     * @param salaryOld Сумма оплаты на момент проведения приказа. Свойство не может быть null.
     */
    public void setSalaryOld(double salaryOld)
    {
        dirty(_salaryOld, salaryOld);
        _salaryOld = salaryOld;
    }

    /**
     * @return Тип трудового договора.
     */
    public LabourContractType getLabourContractType()
    {
        return _labourContractType;
    }

    /**
     * @param labourContractType Тип трудового договора.
     */
    public void setLabourContractType(LabourContractType labourContractType)
    {
        dirty(_labourContractType, labourContractType);
        _labourContractType = labourContractType;
    }

    /**
     * @return Номер трудового договора.
     */
    @Length(max=255)
    public String getLabourContractNumber()
    {
        return _labourContractNumber;
    }

    /**
     * @param labourContractNumber Номер трудового договора.
     */
    public void setLabourContractNumber(String labourContractNumber)
    {
        dirty(_labourContractNumber, labourContractNumber);
        _labourContractNumber = labourContractNumber;
    }

    /**
     * @return Дата трудового договора.
     */
    public Date getLabourContractDate()
    {
        return _labourContractDate;
    }

    /**
     * @param labourContractDate Дата трудового договора.
     */
    public void setLabourContractDate(Date labourContractDate)
    {
        dirty(_labourContractDate, labourContractDate);
        _labourContractDate = labourContractDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Номер доп. соглашения.
     */
    @Length(max=255)
    public String getCollateralAgreementNumber()
    {
        return _collateralAgreementNumber;
    }

    /**
     * @param collateralAgreementNumber Номер доп. соглашения.
     */
    public void setCollateralAgreementNumber(String collateralAgreementNumber)
    {
        dirty(_collateralAgreementNumber, collateralAgreementNumber);
        _collateralAgreementNumber = collateralAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения.
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата доп. соглашения.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Учебная нагрузка в течении учебного года.
     */
    public Integer getHoursAmount()
    {
        return _hoursAmount;
    }

    /**
     * @param hoursAmount Учебная нагрузка в течении учебного года.
     */
    public void setHoursAmount(Integer hoursAmount)
    {
        dirty(_hoursAmount, hoursAmount);
        _hoursAmount = hoursAmount;
    }

    /**
     * @return Трудовой договор, созданный по выписке.
     */
    public EmployeeLabourContract getLabourContract()
    {
        return _labourContract;
    }

    /**
     * @param labourContract Трудовой договор, созданный по выписке.
     */
    public void setLabourContract(EmployeeLabourContract labourContract)
    {
        dirty(_labourContract, labourContract);
        _labourContract = labourContract;
    }

    /**
     * @return Дополнительное соглашение к трудовому договору, созданное по выписке.
     */
    public ContractCollateralAgreement getCollateralAgreement()
    {
        return _collateralAgreement;
    }

    /**
     * @param collateralAgreement Дополнительное соглашение к трудовому договору, созданное по выписке.
     */
    public void setCollateralAgreement(ContractCollateralAgreement collateralAgreement)
    {
        dirty(_collateralAgreement, collateralAgreement);
        _collateralAgreement = collateralAgreement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeAddPaymentsSExtractGen)
        {
            setRaisingCoefficient(((EmployeeAddPaymentsSExtract)another).getRaisingCoefficient());
            setRaisingCoefficientOld(((EmployeeAddPaymentsSExtract)another).getRaisingCoefficientOld());
            setSalary(((EmployeeAddPaymentsSExtract)another).getSalary());
            setSalaryOld(((EmployeeAddPaymentsSExtract)another).getSalaryOld());
            setLabourContractType(((EmployeeAddPaymentsSExtract)another).getLabourContractType());
            setLabourContractNumber(((EmployeeAddPaymentsSExtract)another).getLabourContractNumber());
            setLabourContractDate(((EmployeeAddPaymentsSExtract)another).getLabourContractDate());
            setBeginDate(((EmployeeAddPaymentsSExtract)another).getBeginDate());
            setEndDate(((EmployeeAddPaymentsSExtract)another).getEndDate());
            setCollateralAgreementNumber(((EmployeeAddPaymentsSExtract)another).getCollateralAgreementNumber());
            setTransferDate(((EmployeeAddPaymentsSExtract)another).getTransferDate());
            setHoursAmount(((EmployeeAddPaymentsSExtract)another).getHoursAmount());
            setLabourContract(((EmployeeAddPaymentsSExtract)another).getLabourContract());
            setCollateralAgreement(((EmployeeAddPaymentsSExtract)another).getCollateralAgreement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeAddPaymentsSExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeAddPaymentsSExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeAddPaymentsSExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "raisingCoefficientOld":
                    return obj.getRaisingCoefficientOld();
                case "salary":
                    return obj.getSalary();
                case "salaryOld":
                    return obj.getSalaryOld();
                case "labourContractType":
                    return obj.getLabourContractType();
                case "labourContractNumber":
                    return obj.getLabourContractNumber();
                case "labourContractDate":
                    return obj.getLabourContractDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "collateralAgreementNumber":
                    return obj.getCollateralAgreementNumber();
                case "transferDate":
                    return obj.getTransferDate();
                case "hoursAmount":
                    return obj.getHoursAmount();
                case "labourContract":
                    return obj.getLabourContract();
                case "collateralAgreement":
                    return obj.getCollateralAgreement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "raisingCoefficientOld":
                    obj.setRaisingCoefficientOld((SalaryRaisingCoefficient) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "salaryOld":
                    obj.setSalaryOld((Double) value);
                    return;
                case "labourContractType":
                    obj.setLabourContractType((LabourContractType) value);
                    return;
                case "labourContractNumber":
                    obj.setLabourContractNumber((String) value);
                    return;
                case "labourContractDate":
                    obj.setLabourContractDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "collateralAgreementNumber":
                    obj.setCollateralAgreementNumber((String) value);
                    return;
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "hoursAmount":
                    obj.setHoursAmount((Integer) value);
                    return;
                case "labourContract":
                    obj.setLabourContract((EmployeeLabourContract) value);
                    return;
                case "collateralAgreement":
                    obj.setCollateralAgreement((ContractCollateralAgreement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "raisingCoefficient":
                        return true;
                case "raisingCoefficientOld":
                        return true;
                case "salary":
                        return true;
                case "salaryOld":
                        return true;
                case "labourContractType":
                        return true;
                case "labourContractNumber":
                        return true;
                case "labourContractDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "collateralAgreementNumber":
                        return true;
                case "transferDate":
                        return true;
                case "hoursAmount":
                        return true;
                case "labourContract":
                        return true;
                case "collateralAgreement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "raisingCoefficient":
                    return true;
                case "raisingCoefficientOld":
                    return true;
                case "salary":
                    return true;
                case "salaryOld":
                    return true;
                case "labourContractType":
                    return true;
                case "labourContractNumber":
                    return true;
                case "labourContractDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "collateralAgreementNumber":
                    return true;
                case "transferDate":
                    return true;
                case "hoursAmount":
                    return true;
                case "labourContract":
                    return true;
                case "collateralAgreement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "raisingCoefficientOld":
                    return SalaryRaisingCoefficient.class;
                case "salary":
                    return Double.class;
                case "salaryOld":
                    return Double.class;
                case "labourContractType":
                    return LabourContractType.class;
                case "labourContractNumber":
                    return String.class;
                case "labourContractDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "collateralAgreementNumber":
                    return String.class;
                case "transferDate":
                    return Date.class;
                case "hoursAmount":
                    return Integer.class;
                case "labourContract":
                    return EmployeeLabourContract.class;
                case "collateralAgreement":
                    return ContractCollateralAgreement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeAddPaymentsSExtract> _dslPath = new Path<EmployeeAddPaymentsSExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeAddPaymentsSExtract");
    }
            

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Повышающий коэффициент на момент проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getRaisingCoefficientOld()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficientOld()
    {
        return _dslPath.raisingCoefficientOld();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Сумма оплаты на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getSalaryOld()
     */
    public static PropertyPath<Double> salaryOld()
    {
        return _dslPath.salaryOld();
    }

    /**
     * @return Тип трудового договора.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getLabourContractType()
     */
    public static LabourContractType.Path<LabourContractType> labourContractType()
    {
        return _dslPath.labourContractType();
    }

    /**
     * @return Номер трудового договора.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getLabourContractNumber()
     */
    public static PropertyPath<String> labourContractNumber()
    {
        return _dslPath.labourContractNumber();
    }

    /**
     * @return Дата трудового договора.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getLabourContractDate()
     */
    public static PropertyPath<Date> labourContractDate()
    {
        return _dslPath.labourContractDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Номер доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getCollateralAgreementNumber()
     */
    public static PropertyPath<String> collateralAgreementNumber()
    {
        return _dslPath.collateralAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Учебная нагрузка в течении учебного года.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getHoursAmount()
     */
    public static PropertyPath<Integer> hoursAmount()
    {
        return _dslPath.hoursAmount();
    }

    /**
     * @return Трудовой договор, созданный по выписке.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getLabourContract()
     */
    public static EmployeeLabourContract.Path<EmployeeLabourContract> labourContract()
    {
        return _dslPath.labourContract();
    }

    /**
     * @return Дополнительное соглашение к трудовому договору, созданное по выписке.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getCollateralAgreement()
     */
    public static ContractCollateralAgreement.Path<ContractCollateralAgreement> collateralAgreement()
    {
        return _dslPath.collateralAgreement();
    }

    public static class Path<E extends EmployeeAddPaymentsSExtract> extends SingleEmployeeExtract.Path<E>
    {
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficientOld;
        private PropertyPath<Double> _salary;
        private PropertyPath<Double> _salaryOld;
        private LabourContractType.Path<LabourContractType> _labourContractType;
        private PropertyPath<String> _labourContractNumber;
        private PropertyPath<Date> _labourContractDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _collateralAgreementNumber;
        private PropertyPath<Date> _transferDate;
        private PropertyPath<Integer> _hoursAmount;
        private EmployeeLabourContract.Path<EmployeeLabourContract> _labourContract;
        private ContractCollateralAgreement.Path<ContractCollateralAgreement> _collateralAgreement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Повышающий коэффициент на момент проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getRaisingCoefficientOld()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficientOld()
        {
            if(_raisingCoefficientOld == null )
                _raisingCoefficientOld = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT_OLD, this);
            return _raisingCoefficientOld;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(EmployeeAddPaymentsSExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Сумма оплаты на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getSalaryOld()
     */
        public PropertyPath<Double> salaryOld()
        {
            if(_salaryOld == null )
                _salaryOld = new PropertyPath<Double>(EmployeeAddPaymentsSExtractGen.P_SALARY_OLD, this);
            return _salaryOld;
        }

    /**
     * @return Тип трудового договора.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getLabourContractType()
     */
        public LabourContractType.Path<LabourContractType> labourContractType()
        {
            if(_labourContractType == null )
                _labourContractType = new LabourContractType.Path<LabourContractType>(L_LABOUR_CONTRACT_TYPE, this);
            return _labourContractType;
        }

    /**
     * @return Номер трудового договора.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getLabourContractNumber()
     */
        public PropertyPath<String> labourContractNumber()
        {
            if(_labourContractNumber == null )
                _labourContractNumber = new PropertyPath<String>(EmployeeAddPaymentsSExtractGen.P_LABOUR_CONTRACT_NUMBER, this);
            return _labourContractNumber;
        }

    /**
     * @return Дата трудового договора.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getLabourContractDate()
     */
        public PropertyPath<Date> labourContractDate()
        {
            if(_labourContractDate == null )
                _labourContractDate = new PropertyPath<Date>(EmployeeAddPaymentsSExtractGen.P_LABOUR_CONTRACT_DATE, this);
            return _labourContractDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeAddPaymentsSExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeAddPaymentsSExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Номер доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getCollateralAgreementNumber()
     */
        public PropertyPath<String> collateralAgreementNumber()
        {
            if(_collateralAgreementNumber == null )
                _collateralAgreementNumber = new PropertyPath<String>(EmployeeAddPaymentsSExtractGen.P_COLLATERAL_AGREEMENT_NUMBER, this);
            return _collateralAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(EmployeeAddPaymentsSExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Учебная нагрузка в течении учебного года.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getHoursAmount()
     */
        public PropertyPath<Integer> hoursAmount()
        {
            if(_hoursAmount == null )
                _hoursAmount = new PropertyPath<Integer>(EmployeeAddPaymentsSExtractGen.P_HOURS_AMOUNT, this);
            return _hoursAmount;
        }

    /**
     * @return Трудовой договор, созданный по выписке.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getLabourContract()
     */
        public EmployeeLabourContract.Path<EmployeeLabourContract> labourContract()
        {
            if(_labourContract == null )
                _labourContract = new EmployeeLabourContract.Path<EmployeeLabourContract>(L_LABOUR_CONTRACT, this);
            return _labourContract;
        }

    /**
     * @return Дополнительное соглашение к трудовому договору, созданное по выписке.
     * @see ru.tandemservice.moveemployee.entity.EmployeeAddPaymentsSExtract#getCollateralAgreement()
     */
        public ContractCollateralAgreement.Path<ContractCollateralAgreement> collateralAgreement()
        {
            if(_collateralAgreement == null )
                _collateralAgreement = new ContractCollateralAgreement.Path<ContractCollateralAgreement>(L_COLLATERAL_AGREEMENT, this);
            return _collateralAgreement;
        }

        public Class getEntityClass()
        {
            return EmployeeAddPaymentsSExtract.class;
        }

        public String getEntityName()
        {
            return "employeeAddPaymentsSExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
