/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e27.AddEdit;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.ICommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.moveemployee.entity.ScienceDegreePaymentExtract;

import java.util.List;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 06.03.2012
 */
public interface IDAO extends ICommonModularEmployeeExtractAddEditDAO<ScienceDegreePaymentExtract, Model>
{
    /**
     * @return Возвращает мапу Ставок Сотрудника, созданных в рамках выписки, на элементы Штатной расстановки, выбранных для каждй ставки.
     */
    Map<FinancingSourceDetails, List<FinancingSourceDetailsToAllocItem>> getStaffRateAllocItemMap(Model model);

    void prepareStaffRateDataSource(Model model);

    void prepareEmployeeStaffRateDataSource(Model model);

    void preparePaymentDataSource(Model model);

}