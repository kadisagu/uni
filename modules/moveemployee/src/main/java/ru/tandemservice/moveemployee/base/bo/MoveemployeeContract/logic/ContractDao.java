/**
 *$Id$
 */
package ru.tandemservice.moveemployee.base.bo.MoveemployeeContract.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.entity.EmpListExtractToBasicRelation;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderBasics;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Create by ashaburov
 * Date 16.05.12
 */
public class ContractDao extends CommonDAO implements IContractDao
{
    @Override
    public List<EmployeeHoliday> getEmployeeHolidayList(EmployeePost post, Date begin, Date end)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeHoliday.class, "b").column("b");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeHoliday.employeePost().fromAlias("b")), post));
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeHoliday.holidayType().code().fromAlias("b")), UniempDefines.HOLIDAY_TYPE_ANNUAL));

        IDQLExpression beginGe = DQLExpressions.ge(DQLExpressions.property(EmployeeHoliday.beginDate().fromAlias("b")), DQLExpressions.valueDate(begin));
        IDQLExpression beginLe = DQLExpressions.le(DQLExpressions.property(EmployeeHoliday.beginDate().fromAlias("b")), DQLExpressions.valueDate(end));
        IDQLExpression endGe = DQLExpressions.ge(DQLExpressions.property(EmployeeHoliday.endDate().fromAlias("b")), DQLExpressions.valueDate(begin));
        IDQLExpression endLe = DQLExpressions.le(DQLExpressions.property(EmployeeHoliday.endDate().fromAlias("b")), DQLExpressions.valueDate(end));
        IDQLExpression beginAnd = DQLExpressions.and(beginGe, beginLe);
        IDQLExpression endAnd = DQLExpressions.and(endGe, endLe);

        builder.where(DQLExpressions.or(beginAnd, endAnd));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public void doSave(EmployeeListOrder order, List<EmploymentDismissalEmplListExtract> extractList)
    {
        save(order);

        for (EmploymentDismissalEmplListExtract extract : extractList)
        {
            save(extract);
            save(extract.getParagraph());
        }
    }

    @Override
    public void updateBasicList(List<EmploymentDismissalEmplListExtract> extractList, List<EmployeeOrderBasics> basicsList, Map<EmployeePost, Map<String, String>> basicNoticeMap)
    {
        for (EmploymentDismissalEmplListExtract extract : extractList)
        {
            for (EmployeeOrderBasics basic : basicsList)
            {
                String basicNotice = basicNoticeMap.get(extract.getEmployeePost()) != null ? basicNoticeMap.get(extract.getEmployeePost()).get(basic.getCode()) : null;

                EmpListExtractToBasicRelation relation = new EmpListExtractToBasicRelation();
                relation.setBasic(basic);
                relation.setExtract(extract);
                relation.setComment(basicNotice);
                save(relation);
            }
        }
    }
}
