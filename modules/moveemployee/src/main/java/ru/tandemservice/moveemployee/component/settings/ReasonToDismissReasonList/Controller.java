/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.settings.ReasonToDismissReasonList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;

/**
 * @author dseleznev
 * Created on: 22.09.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        prepareDataSource(context);
    }

    @Override
    public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareListDataSource(getModel(context));
    }

    private void prepareDataSource(IBusinessComponent context)
    {
        if (getModel(context).getDataSource() != null) return;
        DynamicListDataSource<EmployeeOrderReasons> dataSource = new DynamicListDataSource<EmployeeOrderReasons>(context, this);
        dataSource.addColumn(new SimpleColumn("Причина приказа", EmployeeOrderReasons.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Причина увольнения", Model.P_DISMISS_REASON).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete").setDisabledProperty(Model.P_DELETE_DISABLED));
        getModel(context).setDataSource(dataSource);
    }
    
    public void onClickEdit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveEmployeeComponents.ORDER_REASON_TO_DISMISS_ORDER_REL_ADD_EDIT, new ParametersMap().add("employeeOrderReasonId", component.getListenerParameter())));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRelation((Long)component.getListenerParameter());
    }
}