package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из списочного приказа по кадровому составу. Предоставить день отдыха за работу в нерабочий день
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PayForHolidayDayProvideHoldayEmplListExtractGen extends ListEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract";
    public static final String ENTITY_NAME = "payForHolidayDayProvideHoldayEmplListExtract";
    public static final int VERSION_HASH = -2087805945;
    private static IEntityMeta ENTITY_META;

    public static final String P_HOLIDAY_DATE = "holidayDate";
    public static final String P_CHILD_ORG_UNIT = "childOrgUnit";

    private Date _holidayDate;     // Дата нерабочего/праздничного дня
    private boolean _childOrgUnit;     // Учитывать дочерние подразделения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата нерабочего/праздничного дня. Свойство не может быть null.
     */
    @NotNull
    public Date getHolidayDate()
    {
        return _holidayDate;
    }

    /**
     * @param holidayDate Дата нерабочего/праздничного дня. Свойство не может быть null.
     */
    public void setHolidayDate(Date holidayDate)
    {
        dirty(_holidayDate, holidayDate);
        _holidayDate = holidayDate;
    }

    /**
     * @return Учитывать дочерние подразделения. Свойство не может быть null.
     */
    @NotNull
    public boolean isChildOrgUnit()
    {
        return _childOrgUnit;
    }

    /**
     * @param childOrgUnit Учитывать дочерние подразделения. Свойство не может быть null.
     */
    public void setChildOrgUnit(boolean childOrgUnit)
    {
        dirty(_childOrgUnit, childOrgUnit);
        _childOrgUnit = childOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PayForHolidayDayProvideHoldayEmplListExtractGen)
        {
            setHolidayDate(((PayForHolidayDayProvideHoldayEmplListExtract)another).getHolidayDate());
            setChildOrgUnit(((PayForHolidayDayProvideHoldayEmplListExtract)another).isChildOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PayForHolidayDayProvideHoldayEmplListExtractGen> extends ListEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PayForHolidayDayProvideHoldayEmplListExtract.class;
        }

        public T newInstance()
        {
            return (T) new PayForHolidayDayProvideHoldayEmplListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "holidayDate":
                    return obj.getHolidayDate();
                case "childOrgUnit":
                    return obj.isChildOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "holidayDate":
                    obj.setHolidayDate((Date) value);
                    return;
                case "childOrgUnit":
                    obj.setChildOrgUnit((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "holidayDate":
                        return true;
                case "childOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "holidayDate":
                    return true;
                case "childOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "holidayDate":
                    return Date.class;
                case "childOrgUnit":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PayForHolidayDayProvideHoldayEmplListExtract> _dslPath = new Path<PayForHolidayDayProvideHoldayEmplListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PayForHolidayDayProvideHoldayEmplListExtract");
    }
            

    /**
     * @return Дата нерабочего/праздничного дня. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract#getHolidayDate()
     */
    public static PropertyPath<Date> holidayDate()
    {
        return _dslPath.holidayDate();
    }

    /**
     * @return Учитывать дочерние подразделения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract#isChildOrgUnit()
     */
    public static PropertyPath<Boolean> childOrgUnit()
    {
        return _dslPath.childOrgUnit();
    }

    public static class Path<E extends PayForHolidayDayProvideHoldayEmplListExtract> extends ListEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _holidayDate;
        private PropertyPath<Boolean> _childOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата нерабочего/праздничного дня. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract#getHolidayDate()
     */
        public PropertyPath<Date> holidayDate()
        {
            if(_holidayDate == null )
                _holidayDate = new PropertyPath<Date>(PayForHolidayDayProvideHoldayEmplListExtractGen.P_HOLIDAY_DATE, this);
            return _holidayDate;
        }

    /**
     * @return Учитывать дочерние подразделения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.PayForHolidayDayProvideHoldayEmplListExtract#isChildOrgUnit()
     */
        public PropertyPath<Boolean> childOrgUnit()
        {
            if(_childOrgUnit == null )
                _childOrgUnit = new PropertyPath<Boolean>(PayForHolidayDayProvideHoldayEmplListExtractGen.P_CHILD_ORG_UNIT, this);
            return _childOrgUnit;
        }

        public Class getEntityClass()
        {
            return PayForHolidayDayProvideHoldayEmplListExtract.class;
        }

        public String getEntityName()
        {
            return "payForHolidayDayProvideHoldayEmplListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
