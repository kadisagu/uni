/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e23;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.entity.IncDecCombinationExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 23.12.2011
 */
public class IncDecCombinationExtractPrint implements IPrintFormCreator<IncDecCombinationExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, IncDecCombinationExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("changeSize", extract.isIncreased() ? "увеличить" : "уменьшить");

        StringBuilder secondEmploymentPeriod = new StringBuilder();
        secondEmploymentPeriod.append("c ");
        secondEmploymentPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        if (extract.getEndDate() != null)
        {
            secondEmploymentPeriod.append(" по ");
            secondEmploymentPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));
        }
        modifier.put("secondEmploymentPeriod", secondEmploymentPeriod.toString());

        CommonExtractPrintUtil.injectPost(modifier, extract.getCombinationPost().getPostBoundedWithQGandQL(), "combination");

        CommonExtractPrintUtil.injectOrgUnit(modifier, extract.getOrgUnit(), "combination");

        if (extract.getMissingEmployeePost() != null && extract.isIncreased())
        {
            StringBuilder absEmployer = new StringBuilder(" на время отсутствия ");
            String fioDeclinated = CommonExtractUtil.getModifiedFioInitials(extract.getMissingEmployeePost().getPerson().getIdentityCard(), GrammaCase.GENITIVE);
            absEmployer.append(fioDeclinated);
            modifier.put("absEmployer",absEmployer.toString());
        }
        else
            modifier.put("absEmployer", "");

        StringBuilder procStaffRate = new StringBuilder();
        Double staffRateBefore = extract.getStaffRateBefore() * 100;
        Double staffRateAfter = extract.getStaffRateAfter() * 100;
        procStaffRate.append("c ");
        procStaffRate.append(staffRateBefore.longValue());
        procStaffRate.append("% до ");
        procStaffRate.append(staffRateAfter.longValue());
        procStaffRate.append("%");
        modifier.put("procStaffRate", procStaffRate.toString());

        if (extract.getSalaryRaisingCoefficient() != null)
            modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getSalaryRaisingCoefficient().getRecommendedSalary()));
        else
            modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getCombinationPost().getPostBoundedWithQGandQL().getSalary()));

        extendModifier(extract, modifier);
        modifier.modify(document);

        return document;
    }

    protected void extendModifier(IncDecCombinationExtract extract, RtfInjectModifier modifier)
    {

    }
}