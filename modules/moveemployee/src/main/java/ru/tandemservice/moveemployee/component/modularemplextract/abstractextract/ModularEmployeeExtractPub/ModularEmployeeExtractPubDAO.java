/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

/**
 * @author dseleznev
 * Created on: 20.11.2008
 */
public abstract class ModularEmployeeExtractPubDAO<T extends ModularEmployeeExtract, Model extends ModularEmployeeExtractPubModel<T>> extends UniDao<Model> implements IModularEmployeeExtractPubDAO<T, Model>
{
    @Override
    @SuppressWarnings("unchecked")
    public void prepare(Model model)
    {
        model.setExtract((T)getNotNull(model.getExtractId()));
        if (model.getExtractIndividual())
        {
            model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getExtract().getParagraph().getOrder()));
            model.setSecModel(new CommonPostfixPermissionModel("employeeModularOrder"));
        }
        else
        {
            model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getExtract()));
            model.setSecModel(new CommonPostfixPermissionModel("modularEmployeeExtract"));
        }
    }

    @Override
    public void doRejectIndividualExt(Model model)
    {
        IAbstractDocument document = model.getExtract().getParagraph().getOrder();
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(document);
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        }
        else
        {
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(document, UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(document);
            touchService.execute();
        }
    }

    @Override
    public void doCommitIndividualExt(Model model)
    {
        AbstractEmployeeOrder order = model.getExtract().getParagraph().getOrder();

        if (order.getNumber() == null)
            throw new ApplicationException("Нельзя проводить приказ без номера.");

        if (order.getCommitDate() == null)
            throw new ApplicationException("Нельзя проводить приказ без даты приказа.");

        try
        {
            MoveEmployeeDaoFacade.getMoveEmployeeDao().saveModularOrderText((EmployeeModularOrder) order);

            getSession().flush();//т.к. в следующем методе MoveDaoFacade.getMoveDao().doCommitOrder() будет вызван getSession.clear(), то делаем getSession().flush(), что бы сохраненная выше в методе MoveEmployeeDaoFacade.getMoveEmployeeDao().saveModularOrderText() печатная форма для проведенного приказа не удалилась.

            MoveDaoFacade.getMoveDao().doCommitOrder(order, UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
        }
        catch (ApplicationException e)
        {
            order.setCommitDateSystem(null);
            throw e;
        }
    }

    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
        if (model.getExtract().getParagraph() != null)
            throw new ApplicationException("Нельзя отправить выписку на согласование, т.к. она уже включена в приказ.");

        ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
        sendToCoordinationService.init(model.getExtract(), initiator);
        sendToCoordinationService.execute();
    }

    @Override
    public void doSendToCoordinationIndividualExt(Model model, IPersistentPersonable initiator)
    {
        AbstractEmployeeOrder order = model.getExtract().getParagraph().getOrder();

        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(order.getNumber()))
            throw new ApplicationException("Нельзя отправить приказ на согласование без номера.");

        if (order.getCommitDate() == null)
            throw new ApplicationException("Нельзя отправить приказ на согласование без даты приказа.");

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveModularOrderText((EmployeeModularOrder) order);

        ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
        sendToCoordinationService.init(order, initiator);
        sendToCoordinationService.execute();
    }

    @Override
    public void doSendToFormative(Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        //1. надо проверить, что у выписки сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getExtract());
        if (visaTask != null)
            errCollector.add("Нельзя отправить выписку на формирование, так как она на согласовании.");

        getSession().refresh(model.getExtract());
        if (model.getExtract().getParagraph() != null)
            errCollector.add("Нельзя отправить выписку на формирование, так как она находится в приказе.");

        if (!errCollector.hasErrors())
        {
            //2. надо сменить состояние на формируется
            model.getExtract().setState(getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE));
            update(model.getExtract());
        }
    }

    @Override
    public void doSendToFormativeIndividualExt(Model model)
    {
        AbstractEmployeeOrder order = model.getExtract().getParagraph().getOrder();

        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(order);
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на формирование, так как он на согласовании.");

        //2. надо сменить состояние на формируется
        order.setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        update(order);
    }

    @Override
    public void doRollback(Model model)
    {
        if (!UserContext.getInstance().getErrorCollector().hasErrors())
            MoveDaoFacade.getMoveDao().doRollbackExtract(model.getExtract(), getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FORMATIVE), null);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        getSession().evict(model.getExtract());
        model.setExtract(this.<T>get(model.getExtractId()));
        if(null != model.getExtract().getParagraph())
        {
            errors.add("Данная выписка включена приказ, и не может быть проведена вручную.");
        }
    }

    @Override
    public void validateStateOrder(ModularEmployeeExtract extract, ErrorCollector errors)
    {
        getSession().refresh(extract);
        if (extract.getParagraph() != null)
        {
            getSession().refresh(extract.getParagraph().getOrder());
            if (extract.isNoDelete())
                errors.add("Нельзя удалить выписку, если она в приказе, который в состоянии отличном от состояния «формируется».");
        }
    }
}