/* $Id$ */
package ru.tandemservice.moveemployee.base.ext.Depersonalization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.DepersonalizationManager;
import org.tandemframework.shared.commonbase.base.bo.Depersonalization.logic.*;
import ru.tandemservice.moveemployee.base.ext.Depersonalization.objects.SomeEmployeeExtractDO;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeExtractTextRelation;
import ru.tandemservice.moveemployee.entity.EmployeeOrderTextRelation;
import ru.tandemservice.moveemployee.entity.catalog.MoveemployeeTemplate;

/**
 * @author Nikolay Fedorovskih
 * @since 12.11.2014
 */
@Configuration
public class DepersonalizationExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private DepersonalizationManager _depersonalizationManager;

    @Bean
    public ItemListExtension<IDepersonalizationObject> depersonalizationObjectListExtension()
    {
        return _depersonalizationManager.extensionBuilder()
                .add(_depersonalizationManager.updateBuilder(AbstractEmployeeExtract.class).replaceFio(AbstractEmployeeExtract.P_EMPLOYEE_FIO_MODIFIED))
                .add(new TemplateDocumentResetToDefaultObject(MoveemployeeTemplate.class))
                .add(SomeEmployeeExtractDO.class)
                .add(_depersonalizationManager.multipleDropBuilder()
                        .title("Вспомогательные сущности, содержащие текст приказов и выписок (движение кадрового состава)")
                        .drop(EmployeeOrderTextRelation.class)
                        .drop(EmployeeExtractTextRelation.class)
                        .optional(true)
                        .build())
                .build();
    }
}