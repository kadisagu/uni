/**
* $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendar;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendarHoliday;
import org.tandemframework.shared.employeebase.base.entity.IndustrialCalendarHolidayDuration;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.Holiday;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.organization.base.util.IHolidayDuration;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayExtract;
import ru.tandemservice.moveemployee.entity.HolidayInEmpHldyExtractRelation;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.VacationSchedule;
import ru.tandemservice.uniemp.entity.employee.VacationScheduleItem;
import ru.tandemservice.uniemp.util.UniempUtil;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 21.11.2008
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<EmployeeHolidayExtract, Model> implements IDAO
{
    protected List<String> EMPLOYEE_POST_STATUSES = Arrays.asList(
        EmployeePostStatusCodes.STATUS_LEAVE_REGULAR,
        EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR,
        EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY,
        EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE
    );

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected EmployeeHolidayExtract createNewInstance()
    {
        return new EmployeeHolidayExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        
        if(null == model.getExtract().getEntity() && null != model.getEmployeePost())
            model.getExtract().setEntity(model.getEmployeePost());
        else if(null == model.getEmployeePost() || null == model.getEmployeePost().getId())
            model.setEmployeePost(model.getExtract().getEntity());

        Map<String, EmployeePostStatus> postStatusesMap = new HashMap<>();
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_REGULAR, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_REGULAR));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_IRREGULAR));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_MATERNITY));
        postStatusesMap.put(EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE, getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_LEAVE_CHILD_CARE));
        model.setPostStatusesMap(postStatusesMap);
        
        model.setHolidayTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(HolidayType.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", HolidayType.P_TITLE, CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", HolidayType.P_ID, o));

                return new SimpleListResultBuilder<>(builder.getResultList(getSession()));
            }
        });

        model.setEmployeePostStatusModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(EmployeePostStatus.ENTITY_CLASS, "b");
                builder.add(MQExpression.in("b", EmployeePostStatus.P_CODE, EMPLOYEE_POST_STATUSES));
                if (o != null)
                    builder.add(MQExpression.eq("b", EmployeePostStatus.P_ID, o));

                return new MQListResultBuilder(builder);
            }
        });

        model.setVacationScheduleModel(new CommonSingleSelectModel(VacationSchedule.P_TITLE_WITH_YEAR_AND_CREATE_DATE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (o != null)
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(VacationSchedule.class, "s").column("s");
                    subBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationSchedule.id().fromAlias("s")), o));
                    return new DQLListResultBuilder(subBuilder);
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "si").column(DQLExpressions.property(VacationScheduleItem.vacationSchedule().fromAlias("si")));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("si")), model.getEmployeePost()));
                builder.where(DQLExpressions.ge(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("si")), DQLExpressions.value(Calendar.getInstance().get(Calendar.YEAR))));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().state().code().fromAlias("si")), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
                builder.order(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("si")), OrderDirection.desc);
                builder.predicate(DQLPredicateType.distinct);

                return new DQLListResultBuilder(builder);
            }
        });

        model.setVacationScheduleItemModel(new CommonSingleSelectModel(VacationScheduleItem.P_PLANED_VACATION_TITLE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getVacationSchedule() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().fromAlias("i")), model.getExtract().getVacationSchedule()));
                builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), model.getEmployeePost()));
                if (o != null)
                    builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.id().fromAlias("i")), o));
                builder.order(DQLExpressions.property(VacationScheduleItem.planDate().fromAlias("i")), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }
        });

        if (model.isEditForm())
        {
            if (model.getExtract().getVacationScheduleItem() != null || model.getExtract().getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL))
                model.setShowVacationSchedule(true);

            MQBuilder builder = new MQBuilder(HolidayInEmpHldyExtractRelation.ENTITY_CLASS, "b");
            builder.add(MQExpression.eq("b", HolidayInEmpHldyExtractRelation.L_EMPLOYEE_HOLIDAY_EXTRACT, model.getExtract()));
            builder.addOrder("b", HolidayInEmpHldyExtractRelation.P_BEGIN_PERIOD);
            builder.addOrder("b", HolidayInEmpHldyExtractRelation.P_START_HOLIDAY);

            List<HolidayInEmpHldyExtractRelation> relationList = builder.getResultList(getSession());

            //восстанавливаем список отпусков
            List<MainRow> mainRowList = new ArrayList<>();
            model.setHolidayRelationList(new ArrayList<>());
            for (HolidayInEmpHldyExtractRelation relation : relationList)
            {
                MainRow mainRow = new MainRow(relation.getMainRowId(), null, null);
                if (!mainRowList.contains(mainRow))
                {
                    List<Row> rowList = new ArrayList<>();
                    Row row = new Row(relation.getRowId(), mainRow, relation.getRowType());

                    model.getBeginPeriodMap().put(row, relation.getBeginPeriod());
                    model.getEndPeriodMap().put(row, relation.getEndPeriod());
                    model.getStartHolidayMap().put(row, relation.getStartHoliday());
                    model.getFinishHolidayMap().put(row, relation.getFinishHoliday());
                    model.getDurationHolidayMap().put(row, relation.getDurationHoliday());

                    rowList.add(row);
                    mainRow.setRowList(rowList);
                    mainRow.setType(relation.getRowType());
                    mainRowList.add(mainRow);
                }
                else
                {
                    MainRow mainRow1 = mainRowList.get(mainRowList.indexOf(mainRow));
                    mainRow1.setType(relation.getRowType());
                    Row row = new Row(relation.getRowId(), mainRow1, relation.getRowType());

                    model.getBeginPeriodMap().put(row, relation.getBeginPeriod());
                    model.getEndPeriodMap().put(row, relation.getEndPeriod());
                    model.getStartHolidayMap().put(row, relation.getStartHoliday());
                    model.getFinishHolidayMap().put(row, relation.getFinishHoliday());
                    model.getDurationHolidayMap().put(row, relation.getDurationHoliday());

                    mainRow1.getRowList().add(row);
                }

                model.getHolidayRelationList().add(relation);
            }

            model.setMainRowList(mainRowList);
        }

        if (model.getMainRowList().isEmpty())
        {
            List<Row> rowList = new ArrayList<>();
            MainRow mainRow = new MainRow(MainRow.getNewMainRowId(model.getMainRowList()), rowList, Model.FULL_HOLIDAY);
            rowList.add(new Row(Row.getNewRowId(model.getMainRowList()), mainRow, Model.FULL_HOLIDAY));
            model.getMainRowList().add(mainRow);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getExtract().getEntity().getPerson().getIdentityCard().getSex().isMale() && model.getExtract().getHolidayType().getCode().equals("3"))
            errors.add("Отпуск по беременности не может быть назначен для сотрудника мужского пола.");

        for (MainRow mainRow : model.getMainRowList())
            for (Row row : mainRow.getRowList())
            {
                Date beginPeriod = model.getBeginPeriodMap().get(row);
                Date endPeriod = model.getEndPeriodMap().get(row);
                Date startHoliday = model.getStartHolidayMap().get(row);
                Date finishHoliday = model.getFinishHolidayMap().get(row);

                //если назначается отпуск за несколько периодов, то поля периода обязательны
                if (row.getType().equals(Model.PERIODS_OF_HOLIDAY) || row.getMainRow().getType().equals(Model.PERIODS_OF_HOLIDAY))
                    if (beginPeriod == null || endPeriod == null)
                        errors.add("Поля периода отпуска обязательны для заполнения.", row.getId() +  "_beginPeriod", row.getId() + "_endPeriod");

                //проверяем, что Дата начала отпуска не больше Даты окончания отпуска
                if (startHoliday != null && finishHoliday != null && startHoliday.getTime() > finishHoliday.getTime())
                    errors.add("Дата окончания отпуска должна быть больше даты начала.", row.getId() +  "_startHoliday", row.getId() + "_finishHoliday");

                //проверяем корректность заполненности Дат периода
                if (model.getExtract().getHolidayType().getCode().equals(UniempDefines.HOLIDAY_TYPE_ANNUAL) &&
                        !row.getType().equals(Model.HOLIDAYS_OF_PERIOD))
                {
                    if (beginPeriod == null || endPeriod == null)
                        errors.add("Поля периода отпуска обязательны для заполнения.", row.getId() +  "_beginPeriod", row.getId() + "_endPeriod");
                }
                else if ((beginPeriod == null && endPeriod != null) || (beginPeriod != null && endPeriod == null))
                    errors.add("Поля периода должны быть заполнены, либо оставаться пустыми.", row.getId() +  "_beginPeriod", row.getId() + "_endPeriod");

                //проверяем, что Дата начала периода не больше Даты окончания
                if (beginPeriod != null && endPeriod != null && beginPeriod.getTime() > endPeriod.getTime())
                    errors.add("Дата окончания периода должна быть больше даты начала.", row.getId() +  "_beginPeriod", row.getId() + "_endPeriod");

                //проверяем, что Даты отпусков не пересекаются
                for (MainRow mainRow2 : model.getMainRowList())
                    for (Row row2 : mainRow2.getRowList())
                    {
                        if (row.equals(row2))
                            continue;

                        Date startFirst = model.getStartHolidayMap().get(row);
                        Date finishFirst = model.getFinishHolidayMap().get(row);
                        Date startSecond = model.getStartHolidayMap().get(row2);
                        Date finishSecond = model.getFinishHolidayMap().get(row2);

                        if (startFirst == null || finishFirst == null || startSecond == null || finishSecond == null)
                            continue;

                        if (CommonBaseDateUtil.isBetween(startFirst, startSecond, finishSecond) || CommonBaseDateUtil.isBetween(finishFirst, startSecond, finishSecond))
                            errors.add("Указанные отпуска пересекаются.",
                                    row.getId() +  "_startHoliday", row.getId() + "_finishHoliday", row2.getId() +  "_startHoliday", row2.getId() + "_finishHoliday");
                        else {
                            if (CommonBaseDateUtil.isBetween(startSecond, startFirst, finishFirst) || CommonBaseDateUtil.isBetween(finishSecond, startFirst, finishFirst))
                                errors.add("Указанные отпуска пересекаются.",
                                        row.getId() +  "_startHoliday", row.getId() + "_finishHoliday", row2.getId() +  "_startHoliday", row2.getId() + "_finishHoliday");
                        }
                    }

                //проверяем, что Даты периода отпусков не пересекаются
                for (MainRow mainRow2 : model.getMainRowList())
                    for (Row row2 : mainRow.getRowList())
                    {
                        if (row.equals(row2))
                            continue;

                        Date startFirst = model.getBeginPeriodMap().get(row);
                        Date finishFirst = model.getEndPeriodMap().get(row);
                        Date startSecond = model.getBeginPeriodMap().get(row2);
                        Date finishSecond = model.getEndPeriodMap().get(row2);

                        if (startFirst == null || finishFirst == null || startSecond == null || finishSecond == null)
                            continue;

                        if (CommonBaseDateUtil.isBetween(startFirst, startSecond, finishSecond) || CommonBaseDateUtil.isBetween(finishFirst, startSecond, finishSecond))
                            errors.add("Указанные периоды пересекаются.",
                                    row.getId() +  "_beginPeriod", row.getId() + "_endPeriod", row2.getId() +  "_beginPeriod", row2.getId() + "_endPeriod");
                        else {
                            if (CommonBaseDateUtil.isBetween(startSecond, startFirst, finishFirst) || CommonBaseDateUtil.isBetween(finishSecond, startFirst, finishFirst))
                                errors.add("Указанные периоды пересекаются.",
                                        row.getId() +  "_beginPeriod", row.getId() + "_endPeriod", row2.getId() +  "_beginPeriod", row2.getId() + "_endPeriod");
                        }
                    }
            }

        for (MainRow mainRow : model.getMainRowList())
        {
            if (mainRow.getType().equals(Model.PERIODS_OF_HOLIDAY))
            {
                Integer summDur = 0;
                Date start = null;
                Date finish = null;
                String[] fields = new String[mainRow.getRowList().size()];
                int i = 0;
                for (Row row : mainRow.getRowList())
                {
                    if (model.getStartHolidayMap().get(row) != null)
                        start = model.getStartHolidayMap().get(row);
                    if (model.getFinishHolidayMap().get(row) != null)
                        finish = model.getFinishHolidayMap().get(row);

                    summDur += model.getDurationHolidayMap().get(row) != null ?  model.getDurationHolidayMap().get(row) : 0;

                    fields[i++] = row.getId() + "_durationHoliday";
                }

                Long diff = finish.getTime() - start.getTime();
                diff = ((((diff / 1000) / 60) / 60) / 24) + 1;
                if (summDur > diff.intValue())
                    errors.add("Длительность указанная для отпуска с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(start) +
                            " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(finish) + " превышена.", fields);
            }
        }

        MQBuilder empHolidayBuilder = new MQBuilder(EmployeeHoliday.ENTITY_CLASS, "eh");
        empHolidayBuilder.add(MQExpression.eq("eh", EmployeeHoliday.L_EMPLOYEE_POST, model.getEmployeePost()));
        for (EmployeeHoliday holiday : empHolidayBuilder.<EmployeeHoliday>getResultList(getSession()))
            for (MainRow mainRow : model.getMainRowList())
                for (Row row : mainRow.getRowList())
                {
                    Date beginDate = model.getStartHolidayMap().get(row);
                    Date endDate = model.getFinishHolidayMap().get(row);

                    if (beginDate == null || endDate == null)
                        continue;

                    if (CommonBaseDateUtil.isBetween(beginDate, holiday.getStartDate(), holiday.getFinishDate()) || CommonBaseDateUtil.isBetween(endDate, holiday.getStartDate(), holiday.getFinishDate()))
                        errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                row.getId() +  "_startHoliday", row.getId() + "_finishHoliday");
                    else {
                        if (CommonBaseDateUtil.isBetween(holiday.getStartDate(), beginDate, endDate) || CommonBaseDateUtil.isBetween(holiday.getFinishDate(), beginDate, endDate))
                            errors.add("Указанный отпуск пересекается с отпуском " + holiday.getFormattedNameBeginEndPeriodDateString() + ".",
                                    row.getId() +  "_startHoliday", row.getId() + "_finishHoliday");
                    }
                }

        if (model.getExtract().getVacationSchedule() != null && model.getExtract().getVacationScheduleItem() == null)
            errors.add("Поле «Планируемый отпуск» обязательно для заполнения, если указан график отпусков.", "vacationScheduleItem", "vacationSchedule");

        //если выбран график отпусков
        //проверяем пересечение планируемых отпусков внутри выбранного Графика отпусков
        if (model.getExtract().getVacationScheduleItem() != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
            builder.where(DQLExpressions.ne(DQLExpressions.property(VacationScheduleItem.id().fromAlias("i")), DQLExpressions.value(model.getExtract().getVacationScheduleItem().getId())));
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), model.getEmployeePost()));
            builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().fromAlias("i")), model.getExtract().getVacationSchedule()));

            for (VacationScheduleItem item : builder.createStatement(getSession()).<VacationScheduleItem>list())
                for (MainRow mainRow : model.getMainRowList())
                    for (Row row : mainRow.getRowList())
                    {
                        Date beginDate = model.getStartHolidayMap().get(row);
                        Date endDate = model.getFinishHolidayMap().get(row);

                        Date beginItemDate = item.getFactDate() != null ? item.getFactDate() : item.getPlanDate();
                        Date endItemDate = UniempUtil.getEndDate(model.getEmployeePost().getWorkWeekDuration(), item.getDaysAmount(), beginItemDate);

                        if (beginItemDate == null || endItemDate == null || beginDate == null || endDate == null)
                            continue;

                        StringBuilder error = new StringBuilder("Предоставляемый отпуск пересекается с планируемым отпуском с ").
                                append(DateFormatter.DEFAULT_DATE_FORMATTER.format(beginItemDate)).
                                append(" на ").
                                append(NumberConvertingUtil.getCalendarDaysWithName(item.getDaysAmount())).
                                append(" из графика отпусков на ").
                                append(item.getVacationSchedule().getYear()).
                                append(" год от ").
                                append(DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getVacationSchedule().getCreateDate())).
                                append(".");

                        if (CommonBaseDateUtil.isBetween(beginDate, beginItemDate, endItemDate) || CommonBaseDateUtil.isBetween(endDate, beginItemDate, endItemDate))
                            errors.add(error.toString(), row.getId() +  "_startHoliday", row.getId() + "_finishHoliday");
                        else {
                            if (CommonBaseDateUtil.isBetween(beginItemDate, beginDate, endDate) || CommonBaseDateUtil.isBetween(endItemDate, beginDate, endDate))
                                errors.add(error.toString(), row.getId() +  "_startHoliday", row.getId() + "_finishHoliday");
                        }
                    }
        }
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setOldEmployeePostStatus(model.getExtract().getEntity().getPostStatus());

        super.update(model);

        if (model.isEditForm())
            for (HolidayInEmpHldyExtractRelation relation : model.getHolidayRelationList())
                delete(relation);

        for (MainRow mainRow : model.getMainRowList())
            for (Row row : mainRow.getRowList())
            {
                HolidayInEmpHldyExtractRelation relation = new HolidayInEmpHldyExtractRelation();
                relation.setEmployeeHolidayExtract(model.getExtract());
                relation.setMainRowId(mainRow.getId());
                relation.setRowId(row.getId());
                relation.setRowType(row.getType());

                relation.setBeginPeriod(model.getBeginPeriodMap().get(row));
                relation.setEndPeriod(model.getEndPeriodMap().get(row));
                relation.setStartHoliday(model.getStartHolidayMap().get(row));
                relation.setFinishHoliday(model.getFinishHolidayMap().get(row));
                relation.setDurationHoliday(model.getDurationHolidayMap().get(row));

                save(relation);
            }
    }

    @Override
    public boolean hasVacationSchedule(EmployeePost employeePost)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(VacationScheduleItem.class, "i").column("i");
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.employeePost().fromAlias("i")), employeePost));
        builder.where(DQLExpressions.ge(DQLExpressions.property(VacationScheduleItem.vacationSchedule().year().fromAlias("i")), DQLExpressions.value(Calendar.getInstance().get(Calendar.YEAR))));
        builder.where(DQLExpressions.eqValue(DQLExpressions.property(VacationScheduleItem.vacationSchedule().state().code().fromAlias("i")), UniempDefines.STAFF_LIST_STATUS_ACCEPTED));
        return !builder.createStatement(getSession()).list().isEmpty();
    }

    //является ли date праздником в производственном календаре
    @Override
    public boolean isIndustrialCalendarHoliday(EmployeeWorkWeekDuration weekDuration, Date date)
    {
        GregorianCalendar dayDate = (GregorianCalendar)GregorianCalendar.getInstance();

        dayDate.setTime(date);

        MQBuilder calendarBuilder = new MQBuilder(IndustrialCalendar.ENTITY_CLASS, "ic");
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.P_YEAR, dayDate.get(Calendar.YEAR)));
        calendarBuilder.add(MQExpression.eq("ic", IndustrialCalendar.L_WEEK_DURATION, weekDuration));
        IndustrialCalendar calendar = (IndustrialCalendar)calendarBuilder.uniqueResult(getSession());
        if (null == calendar)
            return false;

        MQBuilder builder = new MQBuilder(IndustrialCalendarHoliday.ENTITY_CLASS, "ich");
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_CALENDAR, calendar));
        builder.add(MQExpression.eq("ich", IndustrialCalendarHoliday.L_HOLIDAY + "." + Holiday.P_PREHOLIDAY, Boolean.FALSE));
        List<IHolidayDuration> holidays = builder.getResultList(getSession());

        builder = new MQBuilder(IndustrialCalendarHolidayDuration.ENTITY_CLASS, "ichd");
        builder.add(MQExpression.in("ichd", IndustrialCalendarHolidayDuration.L_HOLIDAY, holidays));
        builder.add(MQExpression.greatOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        builder.add(MQExpression.lessOrEq("ichd", IndustrialCalendarHoliday.P_MONTH,
                dayDate.get(Calendar.MONTH) + 1));
        holidays.addAll(builder.<IHolidayDuration>getResultList(getSession()));

        for(IHolidayDuration holiday : holidays)
        {
            if (holiday.getMonth() != (dayDate.get(Calendar.MONTH) + 1))
                continue;

            int begin = holiday.getBeginDay();
            int end = null == holiday.getEndDay() ? begin : holiday.getEndDay();

            if (holiday.getMonth() == dayDate.get(Calendar.MONTH) + 1 &&
                    dayDate.get(Calendar.DAY_OF_MONTH) <= end &&
                    dayDate.get(Calendar.DAY_OF_MONTH) >= begin)
                return true;
        }
        return false;
    }


    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getStaffRateDataSource(), staffRateItems);
    }

    @Override
    public void prepareHolidayDataSource(Model model)
    {
        model.getHolidayDataSource().setCountRow(model.getMainRowList().size());
        UniBaseUtils.createPage(model.getHolidayDataSource(), model.getMainRowList());
    }
}