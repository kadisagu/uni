/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e19.Pub;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeActingExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.11.2011
 */
public class DAO extends ModularEmployeeExtractPubDAO<EmployeeActingExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setMissingDataStr(model.getExtract().getMissingPost().getTitle() + ", " +
                model.getExtract().getMissingOrgUnit().getTitle());

        model.setActingDateStr("с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getBeginDate()) + " по " +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getEndDate()));
    }
}