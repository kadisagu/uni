/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.menuempl.SingleOrdersFinished;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author dseleznev
 *         Created on: 27.05.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("e");

    static
    {
        _orderSettings.setOrders(SingleEmployeeExtract.EMPLOYEE_FIO_KEY, new OrderDescription("i", IdentityCard.P_LAST_NAME), new OrderDescription("i", IdentityCard.P_FIRST_NAME), new OrderDescription("i", IdentityCard.P_MIDDLE_NAME));
    }

    @Override
    public void prepare(Model model)
    {
        model.setOrderTypesList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getSingleExtractTypeList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        // init datasource
        MQBuilder builder = new MQBuilder(SingleEmployeeExtract.ENTITY_CLASS, "e");
        builder.addJoin("e", SingleEmployeeExtract.L_EMPLOYEE, "em");
        builder.addJoin("em", Employee.L_PERSON, "p");
        builder.addJoin("p", Person.L_IDENTITY_CARD, "i");

        builder.add(MQExpression.eq("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.L_STATE + "." + ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

        // order filters
        Date commitSystemDate = (Date) model.getSettings().get("commitDateSystem");
        Date commitDate = (Date) model.getSettings().get("commitDate");
        Date createDate = (Date) model.getSettings().get("createDate");
        String number = (String) model.getSettings().get("number");
        EmployeeExtractType extractType = (EmployeeExtractType) model.getSettings().get("orderType");
        String lastName = (String) model.getSettings().get("employeeLastName");
        String firstName = (String) model.getSettings().get("employeeFirstName");

        // apply filters
        if (createDate != null)
            builder.add(UniMQExpression.eqDate("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_CREATE_DATE, createDate));
        if (commitDate != null)
            builder.add(UniMQExpression.eqDate("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + EmployeeSingleExtractOrder.P_COMMIT_DATE, commitDate));
        if (commitSystemDate != null)
            builder.add(UniMQExpression.eqDate("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_COMMIT_DATE_SYSTEM, commitSystemDate));
        if (StringUtils.isNotEmpty(number))
            builder.add(MQExpression.like("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + "." + IAbstractOrder.P_NUMBER, CoreStringUtils.escapeLike(number)));
        if (null != extractType)
            builder.add(MQExpression.eq("e", IAbstractExtract.L_TYPE, extractType));
        if (StringUtils.isNotEmpty(lastName))
            builder.add(MQExpression.like("i", IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike("!" + lastName)));
        if (StringUtils.isNotEmpty(firstName))
            builder.add(MQExpression.like("i", IdentityCard.P_FIRST_NAME, CoreStringUtils.escapeLike("!" + firstName)));

        _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }
}