/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e6;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.listemplextract.CommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.ICommonListOrderPrint;
import ru.tandemservice.moveemployee.component.listemplextract.IListParagraphPrintFormCreator;
import ru.tandemservice.moveemployee.entity.ChangeFinancingSourceEmplListExtract;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 07.10.2011
 */
public class ChangeFinancingSourceEmplListExtractPrint implements IPrintFormCreator<EmployeeListOrder>, IListParagraphPrintFormCreator<ChangeFinancingSourceEmplListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);

        ICommonListOrderPrint commonListOrderPrint = (ICommonListOrderPrint) ApplicationRuntime.getBean("employeeListOrder_orderPrint");
        commonListOrderPrint.appendVisas(tableModifier, order);

        tableModifier.modify(document);

        return document;
    }

    @SuppressWarnings("unchecked")
    public String[][] getPreparedOrderData(IAbstractParagraph paragraph)
    {
        List<ChangeFinancingSourceEmplListExtract> extractList = paragraph.getExtractList();
        if(null == extractList || extractList.isEmpty()) return new String[][]{};

        Map<OrgUnit, List<EmployeePost>> orgUnitPostMap = new HashMap<>();
        for (ChangeFinancingSourceEmplListExtract extract : extractList)
        {
            List<EmployeePost> postList = orgUnitPostMap.get(extract.getEntity().getOrgUnit());
            if (postList == null)
                postList = new ArrayList<>();
            postList.add(extract.getEntity());
            orgUnitPostMap.put(extract.getEntity().getOrgUnit(), postList);
        }

        List<String[]> resultList = new ArrayList<>();

        int i = 1;
        for (Map.Entry<OrgUnit, List<EmployeePost>> entry : orgUnitPostMap.entrySet())
        {
            String[] orgUnitLine = new String[1];

            orgUnitLine[0] = entry.getKey().getTitle();

            resultList.add(orgUnitLine);

            for (EmployeePost employeePost : entry.getValue())
            {
                String[] line = new String[1];

                StringBuilder lineBuilder = new StringBuilder();

                lineBuilder.append(employeePost.getEmployee().getEmployeeCode());
                lineBuilder.append(" ");
                lineBuilder.append(CommonExtractUtil.getModifiedFio(employeePost.getEmployee().getPerson(), GrammaCase.DATIVE));
                lineBuilder.append(", ");
                lineBuilder.append(CommonExtractPrintUtil.getPostPrintingTitle(employeePost.getPostRelation().getPostBoundedWithQGandQL(), CommonExtractPrintUtil.DATIVE_CASE, true));

                line[0] = lineBuilder.toString();

                resultList.add(line);
            }

            if (i < orgUnitPostMap.size())
                resultList.add(new String[]{""});
            i++;
        }

        return resultList.toArray(new String[][]{});
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph paragraph, ChangeFinancingSourceEmplListExtract firstExtract)
    {
        RtfInjectModifier modifier = CommonListOrderPrint.createListOrderInjectModifier((EmployeeListOrder) paragraph.getOrder());
        CommonExtractPrintUtil.injectCommonExtractData(modifier, firstExtract);

        modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(firstExtract.getChangeDate()));

        StringBuilder financeSourcesBuilder = new StringBuilder();
        if (firstExtract.getFinancingSource().getGenitiveCaseTitle() != null)
            financeSourcesBuilder.append(firstExtract.getFinancingSource().getGenitiveCaseTitle());
        else
            financeSourcesBuilder.append(firstExtract.getFinancingSource().getTitle());

        if (firstExtract.getFinancingSourceItem() != null)
        {
            financeSourcesBuilder.append(" ");
            if (firstExtract.getFinancingSourceItem().getGenitiveCaseTitle() != null)
                financeSourcesBuilder.append(firstExtract.getFinancingSourceItem().getGenitiveCaseTitle());
            else
                financeSourcesBuilder.append(firstExtract.getFinancingSourceItem().getTitle());
        }
        modifier.put("financeSource_G", financeSourcesBuilder.toString());

        modifier.put("listBasics", firstExtract.getBasicListStr());

        return modifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph paragraph, ChangeFinancingSourceEmplListExtract firstExtract)
    {
        RtfTableModifier modifier = new RtfTableModifier();

        modifier.put("EMPLOYERS", getPreparedOrderData(paragraph));

        return modifier;
    }
}
