/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e2;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.moveemployee.entity.EmployeeTempAddSExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.catalog.PaymentType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 14.09.2009
 */
public class EmployeeTempAddSExtractPrint implements IPrintFormCreator<EmployeeTempAddSExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeTempAddSExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder)extract.getParagraph().getOrder());
        List<FinancingSourceDetails> staffRateDetails = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract);
        
        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        CommonExtractPrintUtil.injectPost(modifier, extract.getPostBoundedWithQGandQL(), null);
        
        List<String> fieldsToRemoveParBefore = new ArrayList<String>();
        List<String> fieldsToRemoveParAfter = new ArrayList<String>();
        if(null != extract.getCompetitionType())
            modifier.put("byCompetitionResults", StringUtils.uncapitalize(extract.getCompetitionType().getTitle()));
        else
            fieldsToRemoveParBefore.add("byCompetitionResults");

        // PostType
        CommonExtractPrintUtil.injectPostType(modifier, extract.getPostType());
        CommonExtractPrintUtil.injectStaffRates(modifier, staffRateDetails);

        Double summStaffRate = 0.0d;
        for (FinancingSourceDetails details : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(extract))
            summStaffRate += details.getStaffRate();

        CommonExtractPrintUtil.injectWorkConditions(modifier, extract.getWeekWorkLoad(), extract.getWorkWeekDuration(), summStaffRate);

        boolean pps = EmployeeTypeCodes.EDU_STAFF.equals(extract.getPostBoundedWithQGandQL().getPost().getEmployeeType().getCode());
        CommonExtractPrintUtil.injectUrgpuLikeConditions(document, modifier, extract, staffRateDetails, pps);

        if(null != extract.getOptionalCondition())
            modifier.put("addConditions", extract.getOptionalCondition());
        else
            fieldsToRemoveParAfter.add("addConditions");

        if (null != extract.getMainWork())
        {
            OrgUnit mainWorkOrgUnit = extract.getMainWork().getOrgUnit();
            Post post = extract.getMainWork().getPostRelation().getPostBoundedWithQGandQL().getPost();

            StringBuilder mainWork = new StringBuilder();
            mainWork.append(null != mainWorkOrgUnit.getNominativeCaseTitle() ? mainWorkOrgUnit.getNominativeCaseTitle() : mainWorkOrgUnit.getFullTitle());
            mainWork.append(", ").append(StringUtils.uncapitalize(null != post.getNominativeCaseTitle() ? post.getNominativeCaseTitle() : post.getTitle()));
            modifier.put("mainWork", mainWork.toString());
        }
        else
            fieldsToRemoveParBefore.add("mainWork");
        //TODO: если внутривузовское совместительство, то "На условиях внутривузовского совместительства"
        //TODO: если внешнее совместительство, то "На условиях внешнего совместительства"
        /*TODO: на период отпуска по уходу за ребенком до 3-х лет Горбуновой О.В.
                на период нахождения Мининой М.В. в отпуске по уходу за ребенком до достижения им возраста 3 – х лет */
        //TODO: на сезонную работу
        //TODO: с неполным рабочим днем – 5 часов (с 8.00 до 13.00)
        //TODO: срочный трудовой договор на основании медицинского заключения от 11.01.2005  №13
        //TODO: с полной материальной ответственностью
        //TODO: в период – с 04.09.2006 по 28.05.2007 с сокращенной продолжительностью рабочего времени (36 часов в неделю) и с 29.05.2007с нормальной продолжительностью рабочего времени

        /*if(null != extract.getHoursAmount())
        {
            modifier.put("hoursAmountWithHoursWord", String.valueOf(extract.getHoursAmount()) + " часов");
        }
        else*/
        {
            modifier.put("hoursAmountWithHoursWord", "");
        }

        UniRtfUtil.removeParagraphsByNamesFromAnywhere(document, fieldsToRemoveParAfter, false, true);
        UniRtfUtil.removeParagraphsByNamesFromAnywhere(document, fieldsToRemoveParBefore, true, true);

        // hourly paid
        if(extract.isHourlyPaid())
        {
            modifier.put("salary", "часовая");
            modifier.put("salaryRub", "часовая");
            modifier.put("salaryKop", "");
        }
        
        modifier.put("payments", CommonExtractUtil.getBonusListStrRtf(extract));
        
        // trial period
        modifier.put("trialPeriod", String.valueOf(0 != extract.getTrialPeriod() ? extract.getTrialPeriod() : "не устанавливается"));
        
        // Labour contract
        {
            modifier.put("lcDay", RussianDateFormatUtils.getDayString(extract.getLabourContractDate(), true));
            modifier.put("lcMonthStr", RussianDateFormatUtils.getMonthName(extract.getLabourContractDate(), false));
            modifier.put("lcYear", RussianDateFormatUtils.getYearString(extract.getLabourContractDate(), true));
        }
        
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<EmployeeBonus> bonusesList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract);

        List<String[]> stimulatingPaymentsList = new ArrayList<String[]>();
        List<String[]> compensativePaymentsList = new ArrayList<String[]>();

        List<String[]> result = new ArrayList<String[]>();
        for(EmployeeBonus bonus : bonusesList)
        {
            String[] bonusArr = new String[5];
            bonusArr[0] = bonus.getPayment().getTitle();
            bonusArr[1] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()) + " " + bonus.getPayment().getPaymentUnit().getShortTitle();
            result.add(bonusArr);

            PaymentType parentPaymentType = bonus.getPayment().getType();
            while(null != parentPaymentType.getParent()) parentPaymentType = parentPaymentType.getParent();

            if(UniempDefines.PAYMENT_TYPE_STLIMULATION.equals(parentPaymentType.getCode()))
                stimulatingPaymentsList.add(bonusArr);
            else
                compensativePaymentsList.add(bonusArr);
        }

        tableModifier.put("T", result.toArray(new String[][]{}));
        if(stimulatingPaymentsList.isEmpty()) stimulatingPaymentsList.add(new String[] {"", ""});
        if(compensativePaymentsList.isEmpty()) compensativePaymentsList.add(new String[] {"", ""});
        tableModifier.put("STIMULATING_PAYMENTS", stimulatingPaymentsList.toArray(new String[][]{}));
        tableModifier.put("COMPENSATIVE_PAYMENTS", compensativePaymentsList.toArray(new String[][]{}));
        tableModifier.modify(document);

        injectModifier(modifier, extract);

        modifier.modify(document);

        return document;
    }

    protected void injectModifier(RtfInjectModifier modifier, EmployeeTempAddSExtract extract)
    {

    }
}