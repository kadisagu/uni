package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.gen.IAbstractDocumentGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Об отзыве из отпуска
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RevocationFromHolidayExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract";
    public static final String ENTITY_NAME = "revocationFromHolidayExtract";
    public static final int VERSION_HASH = 1124060653;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_HOLIDAY = "employeeHoliday";
    public static final String P_EMPLOYEE_FIO_DATIVE = "employeeFioDative";
    public static final String P_REVOCATION_FROM_DAY = "revocationFromDay";
    public static final String P_REVOCATION_TO_DAY = "revocationToDay";
    public static final String P_REVOCATION_DAY_AMOUNT = "revocationDayAmount";
    public static final String P_PROVIDE_UNUSED_HOLIDAY = "provideUnusedHoliday";
    public static final String P_PAYMENT_UNUSED_HOLIDAY = "paymentUnusedHoliday";
    public static final String P_COMPENSATION_UNUSED_HOLIDAY = "compensationUnusedHoliday";
    public static final String P_OPTIONAL_CONDITION = "optionalCondition";
    public static final String P_TRANSFER_UNUSED_HOLIDAY = "transferUnusedHoliday";
    public static final String P_UNUSED_HOLIDAY_AMOUNT = "unusedHolidayAmount";
    public static final String P_HOLIDAY_START_OLD = "holidayStartOld";
    public static final String P_HOLIDAY_FINISH_OLD = "holidayFinishOld";
    public static final String P_HOLIDAY_DURATION_OLD = "holidayDurationOld";
    public static final String P_HOLIDAY_TITLE_OLD = "holidayTitleOld";
    public static final String P_HOLIDAY_BEGIN_OLD = "holidayBeginOld";
    public static final String P_HOLIDAY_END_OLD = "holidayEndOld";
    public static final String L_HOLIDAY_TYPE_OLD = "holidayTypeOld";
    public static final String P_HOLIDAY_DELETE_OLD = "holidayDeleteOld";
    public static final String L_HOLIDAY_EXTRACT_OLD = "holidayExtractOld";
    public static final String P_HOLIDAY_EXTRACT_NUMBER_OLD = "holidayExtractNumberOld";
    public static final String P_HOLIDAY_EXTRACT_DATE_OLD = "holidayExtractDateOld";

    private EmployeeHoliday _employeeHoliday;     // Отпуск за период
    private String _employeeFioDative;     // ФИО сотрудника в дательном падеже
    private Date _revocationFromDay;     // Отозвать с
    private Date _revocationToDay;     // Отозвать по
    private Integer _revocationDayAmount;     // Количество дней
    private boolean _provideUnusedHoliday;     // Предоставить часть неиспользованного отпуска
    private boolean _paymentUnusedHoliday;     // Выплатить компенсацию за неиспользованную часть отпуска
    private Double _compensationUnusedHoliday;     // Компенсация за, дней
    private String _optionalCondition;     // Произвольная строка условий отпуска
    private Boolean _transferUnusedHoliday;     // Перенести часть неиспользованного отпуска на другой срок
    private Double _unusedHolidayAmount;     // Количество дней неиспользованного отпуска
    private Date _holidayStartOld;     // Дата начала выбранного отпуска
    private Date _holidayFinishOld;     // Дата окончания выбранного отпуска
    private Integer _holidayDurationOld;     // Длительность выбранного отпуска
    private String _holidayTitleOld;     // Название выбранного отпуска
    private Date _holidayBeginOld;     // Дата начала периода выбранного отпуска
    private Date _holidayEndOld;     // Дата окончания периода выбранного отпуска
    private HolidayType _holidayTypeOld;     // Вид выбранного отпуска
    private Boolean _holidayDeleteOld;     // Удален ли выбранный отпуск
    private IAbstractDocument _holidayExtractOld;     // Выписка приказа об отпуске выбранного отпуска
    private String _holidayExtractNumberOld;     // Номер приказа об отпуске выбранного отпуска
    private Date _holidayExtractDateOld;     // Дата приказа об отпуске выбранного отпуска

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отпуск за период.
     */
    public EmployeeHoliday getEmployeeHoliday()
    {
        return _employeeHoliday;
    }

    /**
     * @param employeeHoliday Отпуск за период.
     */
    public void setEmployeeHoliday(EmployeeHoliday employeeHoliday)
    {
        dirty(_employeeHoliday, employeeHoliday);
        _employeeHoliday = employeeHoliday;
    }

    /**
     * @return ФИО сотрудника в дательном падеже.
     */
    @Length(max=255)
    public String getEmployeeFioDative()
    {
        return _employeeFioDative;
    }

    /**
     * @param employeeFioDative ФИО сотрудника в дательном падеже.
     */
    public void setEmployeeFioDative(String employeeFioDative)
    {
        dirty(_employeeFioDative, employeeFioDative);
        _employeeFioDative = employeeFioDative;
    }

    /**
     * @return Отозвать с. Свойство не может быть null.
     */
    @NotNull
    public Date getRevocationFromDay()
    {
        return _revocationFromDay;
    }

    /**
     * @param revocationFromDay Отозвать с. Свойство не может быть null.
     */
    public void setRevocationFromDay(Date revocationFromDay)
    {
        dirty(_revocationFromDay, revocationFromDay);
        _revocationFromDay = revocationFromDay;
    }

    /**
     * @return Отозвать по.
     */
    public Date getRevocationToDay()
    {
        return _revocationToDay;
    }

    /**
     * @param revocationToDay Отозвать по.
     */
    public void setRevocationToDay(Date revocationToDay)
    {
        dirty(_revocationToDay, revocationToDay);
        _revocationToDay = revocationToDay;
    }

    /**
     * @return Количество дней.
     */
    public Integer getRevocationDayAmount()
    {
        return _revocationDayAmount;
    }

    /**
     * @param revocationDayAmount Количество дней.
     */
    public void setRevocationDayAmount(Integer revocationDayAmount)
    {
        dirty(_revocationDayAmount, revocationDayAmount);
        _revocationDayAmount = revocationDayAmount;
    }

    /**
     * @return Предоставить часть неиспользованного отпуска. Свойство не может быть null.
     */
    @NotNull
    public boolean isProvideUnusedHoliday()
    {
        return _provideUnusedHoliday;
    }

    /**
     * @param provideUnusedHoliday Предоставить часть неиспользованного отпуска. Свойство не может быть null.
     */
    public void setProvideUnusedHoliday(boolean provideUnusedHoliday)
    {
        dirty(_provideUnusedHoliday, provideUnusedHoliday);
        _provideUnusedHoliday = provideUnusedHoliday;
    }

    /**
     * @return Выплатить компенсацию за неиспользованную часть отпуска. Свойство не может быть null.
     */
    @NotNull
    public boolean isPaymentUnusedHoliday()
    {
        return _paymentUnusedHoliday;
    }

    /**
     * @param paymentUnusedHoliday Выплатить компенсацию за неиспользованную часть отпуска. Свойство не может быть null.
     */
    public void setPaymentUnusedHoliday(boolean paymentUnusedHoliday)
    {
        dirty(_paymentUnusedHoliday, paymentUnusedHoliday);
        _paymentUnusedHoliday = paymentUnusedHoliday;
    }

    /**
     * @return Компенсация за, дней.
     */
    public Double getCompensationUnusedHoliday()
    {
        return _compensationUnusedHoliday;
    }

    /**
     * @param compensationUnusedHoliday Компенсация за, дней.
     */
    public void setCompensationUnusedHoliday(Double compensationUnusedHoliday)
    {
        dirty(_compensationUnusedHoliday, compensationUnusedHoliday);
        _compensationUnusedHoliday = compensationUnusedHoliday;
    }

    /**
     * @return Произвольная строка условий отпуска.
     */
    public String getOptionalCondition()
    {
        return _optionalCondition;
    }

    /**
     * @param optionalCondition Произвольная строка условий отпуска.
     */
    public void setOptionalCondition(String optionalCondition)
    {
        dirty(_optionalCondition, optionalCondition);
        _optionalCondition = optionalCondition;
    }

    /**
     * @return Перенести часть неиспользованного отпуска на другой срок.
     */
    public Boolean getTransferUnusedHoliday()
    {
        return _transferUnusedHoliday;
    }

    /**
     * @param transferUnusedHoliday Перенести часть неиспользованного отпуска на другой срок.
     */
    public void setTransferUnusedHoliday(Boolean transferUnusedHoliday)
    {
        dirty(_transferUnusedHoliday, transferUnusedHoliday);
        _transferUnusedHoliday = transferUnusedHoliday;
    }

    /**
     * @return Количество дней неиспользованного отпуска.
     */
    public Double getUnusedHolidayAmount()
    {
        return _unusedHolidayAmount;
    }

    /**
     * @param unusedHolidayAmount Количество дней неиспользованного отпуска.
     */
    public void setUnusedHolidayAmount(Double unusedHolidayAmount)
    {
        dirty(_unusedHolidayAmount, unusedHolidayAmount);
        _unusedHolidayAmount = unusedHolidayAmount;
    }

    /**
     * @return Дата начала выбранного отпуска.
     */
    public Date getHolidayStartOld()
    {
        return _holidayStartOld;
    }

    /**
     * @param holidayStartOld Дата начала выбранного отпуска.
     */
    public void setHolidayStartOld(Date holidayStartOld)
    {
        dirty(_holidayStartOld, holidayStartOld);
        _holidayStartOld = holidayStartOld;
    }

    /**
     * @return Дата окончания выбранного отпуска.
     */
    public Date getHolidayFinishOld()
    {
        return _holidayFinishOld;
    }

    /**
     * @param holidayFinishOld Дата окончания выбранного отпуска.
     */
    public void setHolidayFinishOld(Date holidayFinishOld)
    {
        dirty(_holidayFinishOld, holidayFinishOld);
        _holidayFinishOld = holidayFinishOld;
    }

    /**
     * @return Длительность выбранного отпуска.
     */
    public Integer getHolidayDurationOld()
    {
        return _holidayDurationOld;
    }

    /**
     * @param holidayDurationOld Длительность выбранного отпуска.
     */
    public void setHolidayDurationOld(Integer holidayDurationOld)
    {
        dirty(_holidayDurationOld, holidayDurationOld);
        _holidayDurationOld = holidayDurationOld;
    }

    /**
     * @return Название выбранного отпуска.
     */
    @Length(max=255)
    public String getHolidayTitleOld()
    {
        return _holidayTitleOld;
    }

    /**
     * @param holidayTitleOld Название выбранного отпуска.
     */
    public void setHolidayTitleOld(String holidayTitleOld)
    {
        dirty(_holidayTitleOld, holidayTitleOld);
        _holidayTitleOld = holidayTitleOld;
    }

    /**
     * @return Дата начала периода выбранного отпуска.
     */
    public Date getHolidayBeginOld()
    {
        return _holidayBeginOld;
    }

    /**
     * @param holidayBeginOld Дата начала периода выбранного отпуска.
     */
    public void setHolidayBeginOld(Date holidayBeginOld)
    {
        dirty(_holidayBeginOld, holidayBeginOld);
        _holidayBeginOld = holidayBeginOld;
    }

    /**
     * @return Дата окончания периода выбранного отпуска.
     */
    public Date getHolidayEndOld()
    {
        return _holidayEndOld;
    }

    /**
     * @param holidayEndOld Дата окончания периода выбранного отпуска.
     */
    public void setHolidayEndOld(Date holidayEndOld)
    {
        dirty(_holidayEndOld, holidayEndOld);
        _holidayEndOld = holidayEndOld;
    }

    /**
     * @return Вид выбранного отпуска.
     */
    public HolidayType getHolidayTypeOld()
    {
        return _holidayTypeOld;
    }

    /**
     * @param holidayTypeOld Вид выбранного отпуска.
     */
    public void setHolidayTypeOld(HolidayType holidayTypeOld)
    {
        dirty(_holidayTypeOld, holidayTypeOld);
        _holidayTypeOld = holidayTypeOld;
    }

    /**
     * @return Удален ли выбранный отпуск.
     */
    public Boolean getHolidayDeleteOld()
    {
        return _holidayDeleteOld;
    }

    /**
     * @param holidayDeleteOld Удален ли выбранный отпуск.
     */
    public void setHolidayDeleteOld(Boolean holidayDeleteOld)
    {
        dirty(_holidayDeleteOld, holidayDeleteOld);
        _holidayDeleteOld = holidayDeleteOld;
    }

    /**
     * @return Выписка приказа об отпуске выбранного отпуска.
     */
    public IAbstractDocument getHolidayExtractOld()
    {
        return _holidayExtractOld;
    }

    /**
     * @param holidayExtractOld Выписка приказа об отпуске выбранного отпуска.
     */
    public void setHolidayExtractOld(IAbstractDocument holidayExtractOld)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && holidayExtractOld!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IAbstractDocument.class);
            IEntityMeta actual =  holidayExtractOld instanceof IEntity ? EntityRuntime.getMeta((IEntity) holidayExtractOld) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_holidayExtractOld, holidayExtractOld);
        _holidayExtractOld = holidayExtractOld;
    }

    /**
     * @return Номер приказа об отпуске выбранного отпуска.
     */
    @Length(max=255)
    public String getHolidayExtractNumberOld()
    {
        return _holidayExtractNumberOld;
    }

    /**
     * @param holidayExtractNumberOld Номер приказа об отпуске выбранного отпуска.
     */
    public void setHolidayExtractNumberOld(String holidayExtractNumberOld)
    {
        dirty(_holidayExtractNumberOld, holidayExtractNumberOld);
        _holidayExtractNumberOld = holidayExtractNumberOld;
    }

    /**
     * @return Дата приказа об отпуске выбранного отпуска.
     */
    public Date getHolidayExtractDateOld()
    {
        return _holidayExtractDateOld;
    }

    /**
     * @param holidayExtractDateOld Дата приказа об отпуске выбранного отпуска.
     */
    public void setHolidayExtractDateOld(Date holidayExtractDateOld)
    {
        dirty(_holidayExtractDateOld, holidayExtractDateOld);
        _holidayExtractDateOld = holidayExtractDateOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RevocationFromHolidayExtractGen)
        {
            setEmployeeHoliday(((RevocationFromHolidayExtract)another).getEmployeeHoliday());
            setEmployeeFioDative(((RevocationFromHolidayExtract)another).getEmployeeFioDative());
            setRevocationFromDay(((RevocationFromHolidayExtract)another).getRevocationFromDay());
            setRevocationToDay(((RevocationFromHolidayExtract)another).getRevocationToDay());
            setRevocationDayAmount(((RevocationFromHolidayExtract)another).getRevocationDayAmount());
            setProvideUnusedHoliday(((RevocationFromHolidayExtract)another).isProvideUnusedHoliday());
            setPaymentUnusedHoliday(((RevocationFromHolidayExtract)another).isPaymentUnusedHoliday());
            setCompensationUnusedHoliday(((RevocationFromHolidayExtract)another).getCompensationUnusedHoliday());
            setOptionalCondition(((RevocationFromHolidayExtract)another).getOptionalCondition());
            setTransferUnusedHoliday(((RevocationFromHolidayExtract)another).getTransferUnusedHoliday());
            setUnusedHolidayAmount(((RevocationFromHolidayExtract)another).getUnusedHolidayAmount());
            setHolidayStartOld(((RevocationFromHolidayExtract)another).getHolidayStartOld());
            setHolidayFinishOld(((RevocationFromHolidayExtract)another).getHolidayFinishOld());
            setHolidayDurationOld(((RevocationFromHolidayExtract)another).getHolidayDurationOld());
            setHolidayTitleOld(((RevocationFromHolidayExtract)another).getHolidayTitleOld());
            setHolidayBeginOld(((RevocationFromHolidayExtract)another).getHolidayBeginOld());
            setHolidayEndOld(((RevocationFromHolidayExtract)another).getHolidayEndOld());
            setHolidayTypeOld(((RevocationFromHolidayExtract)another).getHolidayTypeOld());
            setHolidayDeleteOld(((RevocationFromHolidayExtract)another).getHolidayDeleteOld());
            setHolidayExtractOld(((RevocationFromHolidayExtract)another).getHolidayExtractOld());
            setHolidayExtractNumberOld(((RevocationFromHolidayExtract)another).getHolidayExtractNumberOld());
            setHolidayExtractDateOld(((RevocationFromHolidayExtract)another).getHolidayExtractDateOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RevocationFromHolidayExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RevocationFromHolidayExtract.class;
        }

        public T newInstance()
        {
            return (T) new RevocationFromHolidayExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    return obj.getEmployeeHoliday();
                case "employeeFioDative":
                    return obj.getEmployeeFioDative();
                case "revocationFromDay":
                    return obj.getRevocationFromDay();
                case "revocationToDay":
                    return obj.getRevocationToDay();
                case "revocationDayAmount":
                    return obj.getRevocationDayAmount();
                case "provideUnusedHoliday":
                    return obj.isProvideUnusedHoliday();
                case "paymentUnusedHoliday":
                    return obj.isPaymentUnusedHoliday();
                case "compensationUnusedHoliday":
                    return obj.getCompensationUnusedHoliday();
                case "optionalCondition":
                    return obj.getOptionalCondition();
                case "transferUnusedHoliday":
                    return obj.getTransferUnusedHoliday();
                case "unusedHolidayAmount":
                    return obj.getUnusedHolidayAmount();
                case "holidayStartOld":
                    return obj.getHolidayStartOld();
                case "holidayFinishOld":
                    return obj.getHolidayFinishOld();
                case "holidayDurationOld":
                    return obj.getHolidayDurationOld();
                case "holidayTitleOld":
                    return obj.getHolidayTitleOld();
                case "holidayBeginOld":
                    return obj.getHolidayBeginOld();
                case "holidayEndOld":
                    return obj.getHolidayEndOld();
                case "holidayTypeOld":
                    return obj.getHolidayTypeOld();
                case "holidayDeleteOld":
                    return obj.getHolidayDeleteOld();
                case "holidayExtractOld":
                    return obj.getHolidayExtractOld();
                case "holidayExtractNumberOld":
                    return obj.getHolidayExtractNumberOld();
                case "holidayExtractDateOld":
                    return obj.getHolidayExtractDateOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    obj.setEmployeeHoliday((EmployeeHoliday) value);
                    return;
                case "employeeFioDative":
                    obj.setEmployeeFioDative((String) value);
                    return;
                case "revocationFromDay":
                    obj.setRevocationFromDay((Date) value);
                    return;
                case "revocationToDay":
                    obj.setRevocationToDay((Date) value);
                    return;
                case "revocationDayAmount":
                    obj.setRevocationDayAmount((Integer) value);
                    return;
                case "provideUnusedHoliday":
                    obj.setProvideUnusedHoliday((Boolean) value);
                    return;
                case "paymentUnusedHoliday":
                    obj.setPaymentUnusedHoliday((Boolean) value);
                    return;
                case "compensationUnusedHoliday":
                    obj.setCompensationUnusedHoliday((Double) value);
                    return;
                case "optionalCondition":
                    obj.setOptionalCondition((String) value);
                    return;
                case "transferUnusedHoliday":
                    obj.setTransferUnusedHoliday((Boolean) value);
                    return;
                case "unusedHolidayAmount":
                    obj.setUnusedHolidayAmount((Double) value);
                    return;
                case "holidayStartOld":
                    obj.setHolidayStartOld((Date) value);
                    return;
                case "holidayFinishOld":
                    obj.setHolidayFinishOld((Date) value);
                    return;
                case "holidayDurationOld":
                    obj.setHolidayDurationOld((Integer) value);
                    return;
                case "holidayTitleOld":
                    obj.setHolidayTitleOld((String) value);
                    return;
                case "holidayBeginOld":
                    obj.setHolidayBeginOld((Date) value);
                    return;
                case "holidayEndOld":
                    obj.setHolidayEndOld((Date) value);
                    return;
                case "holidayTypeOld":
                    obj.setHolidayTypeOld((HolidayType) value);
                    return;
                case "holidayDeleteOld":
                    obj.setHolidayDeleteOld((Boolean) value);
                    return;
                case "holidayExtractOld":
                    obj.setHolidayExtractOld((IAbstractDocument) value);
                    return;
                case "holidayExtractNumberOld":
                    obj.setHolidayExtractNumberOld((String) value);
                    return;
                case "holidayExtractDateOld":
                    obj.setHolidayExtractDateOld((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                        return true;
                case "employeeFioDative":
                        return true;
                case "revocationFromDay":
                        return true;
                case "revocationToDay":
                        return true;
                case "revocationDayAmount":
                        return true;
                case "provideUnusedHoliday":
                        return true;
                case "paymentUnusedHoliday":
                        return true;
                case "compensationUnusedHoliday":
                        return true;
                case "optionalCondition":
                        return true;
                case "transferUnusedHoliday":
                        return true;
                case "unusedHolidayAmount":
                        return true;
                case "holidayStartOld":
                        return true;
                case "holidayFinishOld":
                        return true;
                case "holidayDurationOld":
                        return true;
                case "holidayTitleOld":
                        return true;
                case "holidayBeginOld":
                        return true;
                case "holidayEndOld":
                        return true;
                case "holidayTypeOld":
                        return true;
                case "holidayDeleteOld":
                        return true;
                case "holidayExtractOld":
                        return true;
                case "holidayExtractNumberOld":
                        return true;
                case "holidayExtractDateOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    return true;
                case "employeeFioDative":
                    return true;
                case "revocationFromDay":
                    return true;
                case "revocationToDay":
                    return true;
                case "revocationDayAmount":
                    return true;
                case "provideUnusedHoliday":
                    return true;
                case "paymentUnusedHoliday":
                    return true;
                case "compensationUnusedHoliday":
                    return true;
                case "optionalCondition":
                    return true;
                case "transferUnusedHoliday":
                    return true;
                case "unusedHolidayAmount":
                    return true;
                case "holidayStartOld":
                    return true;
                case "holidayFinishOld":
                    return true;
                case "holidayDurationOld":
                    return true;
                case "holidayTitleOld":
                    return true;
                case "holidayBeginOld":
                    return true;
                case "holidayEndOld":
                    return true;
                case "holidayTypeOld":
                    return true;
                case "holidayDeleteOld":
                    return true;
                case "holidayExtractOld":
                    return true;
                case "holidayExtractNumberOld":
                    return true;
                case "holidayExtractDateOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeHoliday":
                    return EmployeeHoliday.class;
                case "employeeFioDative":
                    return String.class;
                case "revocationFromDay":
                    return Date.class;
                case "revocationToDay":
                    return Date.class;
                case "revocationDayAmount":
                    return Integer.class;
                case "provideUnusedHoliday":
                    return Boolean.class;
                case "paymentUnusedHoliday":
                    return Boolean.class;
                case "compensationUnusedHoliday":
                    return Double.class;
                case "optionalCondition":
                    return String.class;
                case "transferUnusedHoliday":
                    return Boolean.class;
                case "unusedHolidayAmount":
                    return Double.class;
                case "holidayStartOld":
                    return Date.class;
                case "holidayFinishOld":
                    return Date.class;
                case "holidayDurationOld":
                    return Integer.class;
                case "holidayTitleOld":
                    return String.class;
                case "holidayBeginOld":
                    return Date.class;
                case "holidayEndOld":
                    return Date.class;
                case "holidayTypeOld":
                    return HolidayType.class;
                case "holidayDeleteOld":
                    return Boolean.class;
                case "holidayExtractOld":
                    return IAbstractDocument.class;
                case "holidayExtractNumberOld":
                    return String.class;
                case "holidayExtractDateOld":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RevocationFromHolidayExtract> _dslPath = new Path<RevocationFromHolidayExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RevocationFromHolidayExtract");
    }
            

    /**
     * @return Отпуск за период.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getEmployeeHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> employeeHoliday()
    {
        return _dslPath.employeeHoliday();
    }

    /**
     * @return ФИО сотрудника в дательном падеже.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getEmployeeFioDative()
     */
    public static PropertyPath<String> employeeFioDative()
    {
        return _dslPath.employeeFioDative();
    }

    /**
     * @return Отозвать с. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getRevocationFromDay()
     */
    public static PropertyPath<Date> revocationFromDay()
    {
        return _dslPath.revocationFromDay();
    }

    /**
     * @return Отозвать по.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getRevocationToDay()
     */
    public static PropertyPath<Date> revocationToDay()
    {
        return _dslPath.revocationToDay();
    }

    /**
     * @return Количество дней.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getRevocationDayAmount()
     */
    public static PropertyPath<Integer> revocationDayAmount()
    {
        return _dslPath.revocationDayAmount();
    }

    /**
     * @return Предоставить часть неиспользованного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#isProvideUnusedHoliday()
     */
    public static PropertyPath<Boolean> provideUnusedHoliday()
    {
        return _dslPath.provideUnusedHoliday();
    }

    /**
     * @return Выплатить компенсацию за неиспользованную часть отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#isPaymentUnusedHoliday()
     */
    public static PropertyPath<Boolean> paymentUnusedHoliday()
    {
        return _dslPath.paymentUnusedHoliday();
    }

    /**
     * @return Компенсация за, дней.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getCompensationUnusedHoliday()
     */
    public static PropertyPath<Double> compensationUnusedHoliday()
    {
        return _dslPath.compensationUnusedHoliday();
    }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getOptionalCondition()
     */
    public static PropertyPath<String> optionalCondition()
    {
        return _dslPath.optionalCondition();
    }

    /**
     * @return Перенести часть неиспользованного отпуска на другой срок.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getTransferUnusedHoliday()
     */
    public static PropertyPath<Boolean> transferUnusedHoliday()
    {
        return _dslPath.transferUnusedHoliday();
    }

    /**
     * @return Количество дней неиспользованного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getUnusedHolidayAmount()
     */
    public static PropertyPath<Double> unusedHolidayAmount()
    {
        return _dslPath.unusedHolidayAmount();
    }

    /**
     * @return Дата начала выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayStartOld()
     */
    public static PropertyPath<Date> holidayStartOld()
    {
        return _dslPath.holidayStartOld();
    }

    /**
     * @return Дата окончания выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayFinishOld()
     */
    public static PropertyPath<Date> holidayFinishOld()
    {
        return _dslPath.holidayFinishOld();
    }

    /**
     * @return Длительность выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayDurationOld()
     */
    public static PropertyPath<Integer> holidayDurationOld()
    {
        return _dslPath.holidayDurationOld();
    }

    /**
     * @return Название выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayTitleOld()
     */
    public static PropertyPath<String> holidayTitleOld()
    {
        return _dslPath.holidayTitleOld();
    }

    /**
     * @return Дата начала периода выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayBeginOld()
     */
    public static PropertyPath<Date> holidayBeginOld()
    {
        return _dslPath.holidayBeginOld();
    }

    /**
     * @return Дата окончания периода выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayEndOld()
     */
    public static PropertyPath<Date> holidayEndOld()
    {
        return _dslPath.holidayEndOld();
    }

    /**
     * @return Вид выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayTypeOld()
     */
    public static HolidayType.Path<HolidayType> holidayTypeOld()
    {
        return _dslPath.holidayTypeOld();
    }

    /**
     * @return Удален ли выбранный отпуск.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayDeleteOld()
     */
    public static PropertyPath<Boolean> holidayDeleteOld()
    {
        return _dslPath.holidayDeleteOld();
    }

    /**
     * @return Выписка приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayExtractOld()
     */
    public static IAbstractDocumentGen.Path<IAbstractDocument> holidayExtractOld()
    {
        return _dslPath.holidayExtractOld();
    }

    /**
     * @return Номер приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayExtractNumberOld()
     */
    public static PropertyPath<String> holidayExtractNumberOld()
    {
        return _dslPath.holidayExtractNumberOld();
    }

    /**
     * @return Дата приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayExtractDateOld()
     */
    public static PropertyPath<Date> holidayExtractDateOld()
    {
        return _dslPath.holidayExtractDateOld();
    }

    public static class Path<E extends RevocationFromHolidayExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeeHoliday.Path<EmployeeHoliday> _employeeHoliday;
        private PropertyPath<String> _employeeFioDative;
        private PropertyPath<Date> _revocationFromDay;
        private PropertyPath<Date> _revocationToDay;
        private PropertyPath<Integer> _revocationDayAmount;
        private PropertyPath<Boolean> _provideUnusedHoliday;
        private PropertyPath<Boolean> _paymentUnusedHoliday;
        private PropertyPath<Double> _compensationUnusedHoliday;
        private PropertyPath<String> _optionalCondition;
        private PropertyPath<Boolean> _transferUnusedHoliday;
        private PropertyPath<Double> _unusedHolidayAmount;
        private PropertyPath<Date> _holidayStartOld;
        private PropertyPath<Date> _holidayFinishOld;
        private PropertyPath<Integer> _holidayDurationOld;
        private PropertyPath<String> _holidayTitleOld;
        private PropertyPath<Date> _holidayBeginOld;
        private PropertyPath<Date> _holidayEndOld;
        private HolidayType.Path<HolidayType> _holidayTypeOld;
        private PropertyPath<Boolean> _holidayDeleteOld;
        private IAbstractDocumentGen.Path<IAbstractDocument> _holidayExtractOld;
        private PropertyPath<String> _holidayExtractNumberOld;
        private PropertyPath<Date> _holidayExtractDateOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отпуск за период.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getEmployeeHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> employeeHoliday()
        {
            if(_employeeHoliday == null )
                _employeeHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_EMPLOYEE_HOLIDAY, this);
            return _employeeHoliday;
        }

    /**
     * @return ФИО сотрудника в дательном падеже.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getEmployeeFioDative()
     */
        public PropertyPath<String> employeeFioDative()
        {
            if(_employeeFioDative == null )
                _employeeFioDative = new PropertyPath<String>(RevocationFromHolidayExtractGen.P_EMPLOYEE_FIO_DATIVE, this);
            return _employeeFioDative;
        }

    /**
     * @return Отозвать с. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getRevocationFromDay()
     */
        public PropertyPath<Date> revocationFromDay()
        {
            if(_revocationFromDay == null )
                _revocationFromDay = new PropertyPath<Date>(RevocationFromHolidayExtractGen.P_REVOCATION_FROM_DAY, this);
            return _revocationFromDay;
        }

    /**
     * @return Отозвать по.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getRevocationToDay()
     */
        public PropertyPath<Date> revocationToDay()
        {
            if(_revocationToDay == null )
                _revocationToDay = new PropertyPath<Date>(RevocationFromHolidayExtractGen.P_REVOCATION_TO_DAY, this);
            return _revocationToDay;
        }

    /**
     * @return Количество дней.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getRevocationDayAmount()
     */
        public PropertyPath<Integer> revocationDayAmount()
        {
            if(_revocationDayAmount == null )
                _revocationDayAmount = new PropertyPath<Integer>(RevocationFromHolidayExtractGen.P_REVOCATION_DAY_AMOUNT, this);
            return _revocationDayAmount;
        }

    /**
     * @return Предоставить часть неиспользованного отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#isProvideUnusedHoliday()
     */
        public PropertyPath<Boolean> provideUnusedHoliday()
        {
            if(_provideUnusedHoliday == null )
                _provideUnusedHoliday = new PropertyPath<Boolean>(RevocationFromHolidayExtractGen.P_PROVIDE_UNUSED_HOLIDAY, this);
            return _provideUnusedHoliday;
        }

    /**
     * @return Выплатить компенсацию за неиспользованную часть отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#isPaymentUnusedHoliday()
     */
        public PropertyPath<Boolean> paymentUnusedHoliday()
        {
            if(_paymentUnusedHoliday == null )
                _paymentUnusedHoliday = new PropertyPath<Boolean>(RevocationFromHolidayExtractGen.P_PAYMENT_UNUSED_HOLIDAY, this);
            return _paymentUnusedHoliday;
        }

    /**
     * @return Компенсация за, дней.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getCompensationUnusedHoliday()
     */
        public PropertyPath<Double> compensationUnusedHoliday()
        {
            if(_compensationUnusedHoliday == null )
                _compensationUnusedHoliday = new PropertyPath<Double>(RevocationFromHolidayExtractGen.P_COMPENSATION_UNUSED_HOLIDAY, this);
            return _compensationUnusedHoliday;
        }

    /**
     * @return Произвольная строка условий отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getOptionalCondition()
     */
        public PropertyPath<String> optionalCondition()
        {
            if(_optionalCondition == null )
                _optionalCondition = new PropertyPath<String>(RevocationFromHolidayExtractGen.P_OPTIONAL_CONDITION, this);
            return _optionalCondition;
        }

    /**
     * @return Перенести часть неиспользованного отпуска на другой срок.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getTransferUnusedHoliday()
     */
        public PropertyPath<Boolean> transferUnusedHoliday()
        {
            if(_transferUnusedHoliday == null )
                _transferUnusedHoliday = new PropertyPath<Boolean>(RevocationFromHolidayExtractGen.P_TRANSFER_UNUSED_HOLIDAY, this);
            return _transferUnusedHoliday;
        }

    /**
     * @return Количество дней неиспользованного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getUnusedHolidayAmount()
     */
        public PropertyPath<Double> unusedHolidayAmount()
        {
            if(_unusedHolidayAmount == null )
                _unusedHolidayAmount = new PropertyPath<Double>(RevocationFromHolidayExtractGen.P_UNUSED_HOLIDAY_AMOUNT, this);
            return _unusedHolidayAmount;
        }

    /**
     * @return Дата начала выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayStartOld()
     */
        public PropertyPath<Date> holidayStartOld()
        {
            if(_holidayStartOld == null )
                _holidayStartOld = new PropertyPath<Date>(RevocationFromHolidayExtractGen.P_HOLIDAY_START_OLD, this);
            return _holidayStartOld;
        }

    /**
     * @return Дата окончания выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayFinishOld()
     */
        public PropertyPath<Date> holidayFinishOld()
        {
            if(_holidayFinishOld == null )
                _holidayFinishOld = new PropertyPath<Date>(RevocationFromHolidayExtractGen.P_HOLIDAY_FINISH_OLD, this);
            return _holidayFinishOld;
        }

    /**
     * @return Длительность выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayDurationOld()
     */
        public PropertyPath<Integer> holidayDurationOld()
        {
            if(_holidayDurationOld == null )
                _holidayDurationOld = new PropertyPath<Integer>(RevocationFromHolidayExtractGen.P_HOLIDAY_DURATION_OLD, this);
            return _holidayDurationOld;
        }

    /**
     * @return Название выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayTitleOld()
     */
        public PropertyPath<String> holidayTitleOld()
        {
            if(_holidayTitleOld == null )
                _holidayTitleOld = new PropertyPath<String>(RevocationFromHolidayExtractGen.P_HOLIDAY_TITLE_OLD, this);
            return _holidayTitleOld;
        }

    /**
     * @return Дата начала периода выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayBeginOld()
     */
        public PropertyPath<Date> holidayBeginOld()
        {
            if(_holidayBeginOld == null )
                _holidayBeginOld = new PropertyPath<Date>(RevocationFromHolidayExtractGen.P_HOLIDAY_BEGIN_OLD, this);
            return _holidayBeginOld;
        }

    /**
     * @return Дата окончания периода выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayEndOld()
     */
        public PropertyPath<Date> holidayEndOld()
        {
            if(_holidayEndOld == null )
                _holidayEndOld = new PropertyPath<Date>(RevocationFromHolidayExtractGen.P_HOLIDAY_END_OLD, this);
            return _holidayEndOld;
        }

    /**
     * @return Вид выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayTypeOld()
     */
        public HolidayType.Path<HolidayType> holidayTypeOld()
        {
            if(_holidayTypeOld == null )
                _holidayTypeOld = new HolidayType.Path<HolidayType>(L_HOLIDAY_TYPE_OLD, this);
            return _holidayTypeOld;
        }

    /**
     * @return Удален ли выбранный отпуск.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayDeleteOld()
     */
        public PropertyPath<Boolean> holidayDeleteOld()
        {
            if(_holidayDeleteOld == null )
                _holidayDeleteOld = new PropertyPath<Boolean>(RevocationFromHolidayExtractGen.P_HOLIDAY_DELETE_OLD, this);
            return _holidayDeleteOld;
        }

    /**
     * @return Выписка приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayExtractOld()
     */
        public IAbstractDocumentGen.Path<IAbstractDocument> holidayExtractOld()
        {
            if(_holidayExtractOld == null )
                _holidayExtractOld = new IAbstractDocumentGen.Path<IAbstractDocument>(L_HOLIDAY_EXTRACT_OLD, this);
            return _holidayExtractOld;
        }

    /**
     * @return Номер приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayExtractNumberOld()
     */
        public PropertyPath<String> holidayExtractNumberOld()
        {
            if(_holidayExtractNumberOld == null )
                _holidayExtractNumberOld = new PropertyPath<String>(RevocationFromHolidayExtractGen.P_HOLIDAY_EXTRACT_NUMBER_OLD, this);
            return _holidayExtractNumberOld;
        }

    /**
     * @return Дата приказа об отпуске выбранного отпуска.
     * @see ru.tandemservice.moveemployee.entity.RevocationFromHolidayExtract#getHolidayExtractDateOld()
     */
        public PropertyPath<Date> holidayExtractDateOld()
        {
            if(_holidayExtractDateOld == null )
                _holidayExtractDateOld = new PropertyPath<Date>(RevocationFromHolidayExtractGen.P_HOLIDAY_EXTRACT_DATE_OLD, this);
            return _holidayExtractDateOld;
        }

        public Class getEntityClass()
        {
            return RevocationFromHolidayExtract.class;
        }

        public String getEntityName()
        {
            return "revocationFromHolidayExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
