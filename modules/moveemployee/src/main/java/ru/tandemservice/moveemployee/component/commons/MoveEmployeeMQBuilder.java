/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.commons;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

/**
 * @author dseleznev
 * Created on: 30.10.2009
 */
public class MoveEmployeeMQBuilder implements IMoveEmployeeMQBuilder
{
    private IDataSettings _settings;
    private MQBuilder _builder;

    // MQBuilder factory methods

    public static <T extends IAbstractOrder> IMoveEmployeeMQBuilder createOrderMQBuilder(Class<T> orderClass, IDataSettings settings)
    {
        MoveEmployeeMQBuilder builder = new MoveEmployeeMQBuilder();
        builder._settings = settings;
        builder._builder = new MQBuilder(orderClass.getName(), ORDER_ALIAS);
        builder._builder.addJoin(ORDER_ALIAS, IAbstractOrder.L_STATE, STATE_ALIAS);
        return builder;
    }

    public static <T extends IAbstractExtract> IMoveEmployeeMQBuilder createExtractMQBuilder(Class<T> extractClass, IDataSettings settings)
    {
        MoveEmployeeMQBuilder builder = new MoveEmployeeMQBuilder();
        builder._settings = settings;
        builder._builder = new MQBuilder(extractClass.getName(), IMoveEmployeeMQBuilder.EXTRACT_ALIAS);
        builder._builder.addJoin(IMoveEmployeeMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_STATE, IMoveEmployeeMQBuilder.STATE_ALIAS);
        builder._builder.addLeftJoin(IMoveEmployeeMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_PARAGRAPH, IMoveEmployeeMQBuilder.PARAGRAPH_ALIAS);
        builder._builder.addLeftJoin(IMoveEmployeeMQBuilder.PARAGRAPH_ALIAS, IAbstractParagraph.L_ORDER, IMoveEmployeeMQBuilder.ORDER_ALIAS);
        // TODO builder._builder.addJoin(IMoveEmployeeMQBuilder.EXTRACT_ALIAS, IAbstractExtract.L_ENTITY, IMoveEmployeeMQBuilder.EMPLOYEE_ALIAS);
        builder._builder.addJoin(IMoveEmployeeMQBuilder.EMPLOYEE_ALIAS, Employee.L_PERSON, "person");
        builder._builder.addJoin("person", Person.L_IDENTITY_CARD, IMoveEmployeeMQBuilder.IDENTITY_CARD_ALIAS);
        return builder;
    }

    // MQBuilder delegate method

    @Override
    public MQBuilder getMQBuilder()
    {
        return _builder;
    }

    // filter applyers ordered by name

    @Override
    public void applyListOrderType()
    {
        EmployeeExtractType type = (EmployeeExtractType) _settings.get(TYPE);
        if (type != null)
            _builder.add(MQExpression.eq(ORDER_ALIAS, EmployeeListOrder.L_TYPE, type));
    }

    @Override
    public void applyOrderCommitDate()
    {
        Date commitDate = (Date) _settings.get(COMMIT_DATE);
        if (commitDate != null)
            _builder.add(UniMQExpression.eqDate(ORDER_ALIAS, AbstractEmployeeOrder.P_COMMIT_DATE, commitDate));
    }

    @Override
    public void applyOrderCommitDateSystem()
    {
        Date commitDateSystem = (Date) _settings.get(COMMIT_DATE_SYSTEM);
        if (commitDateSystem != null)
            _builder.add(UniMQExpression.eqDate(ORDER_ALIAS, IAbstractOrder.P_COMMIT_DATE_SYSTEM, commitDateSystem));
    }

    @Override
    public void applyOrderCreateDate()
    {
        Date createDate = (Date) _settings.get(CREATE_DATE);
        if (createDate != null)
            _builder.add(UniMQExpression.eqDate(ORDER_ALIAS, IAbstractOrder.P_CREATE_DATE, createDate));
    }

    @Override
    public void applyOrderCommitDateFromTo()
    {
        Date commitDateFrom = (Date) _settings.get(COMMIT_DATE_FROM);
        Date commitDateTo = (Date) _settings.get(COMMIT_DATE_TO);

        if (commitDateFrom != null)
            _builder.add(UniMQExpression.greatOrEq(ORDER_ALIAS, AbstractEmployeeOrder.P_COMMIT_DATE, commitDateFrom));

        if (commitDateTo != null)
            _builder.add(UniMQExpression.lessOrEq(ORDER_ALIAS, AbstractEmployeeOrder.P_COMMIT_DATE, commitDateTo));
    }

    @Override
    public void applyOrderCommitDateSystemFromTo()
    {
        Date commitDateSystemFrom = (Date) _settings.get(COMMIT_DATE_SYSTEM_FROM);
        Date commitDateSystemTo = (Date) _settings.get(COMMIT_DATE_SYSTEM_TO);

        if (commitDateSystemFrom != null)
            _builder.add(UniMQExpression.greatOrEq(ORDER_ALIAS, IAbstractOrder.P_COMMIT_DATE_SYSTEM, commitDateSystemFrom));

        if (commitDateSystemTo != null)
            _builder.add(UniMQExpression.lessOrEq(ORDER_ALIAS, IAbstractOrder.P_COMMIT_DATE_SYSTEM, commitDateSystemTo));
    }

    @Override
    public void applyOrderCreateDateFromTo()
    {
        Date createDateFrom = (Date) _settings.get(CREATE_DATE_FROM);
        Date createDateTo = (Date) _settings.get(CREATE_DATE_TO);

        if (createDateFrom != null)
            _builder.add(UniMQExpression.greatOrEq(ORDER_ALIAS, IAbstractOrder.P_CREATE_DATE, createDateFrom));

        if (createDateTo != null)
            _builder.add(UniMQExpression.lessOrEq(ORDER_ALIAS, IAbstractOrder.P_CREATE_DATE, createDateTo));
    }


    @Override
    public void applyOrderNumber()
    {
        String number = (String) _settings.get(NUMBER);
        if (StringUtils.isNotEmpty(number))
            _builder.add(MQExpression.like(ORDER_ALIAS, IAbstractOrder.P_NUMBER, "%" + number));
    }

    @Override
    public void applyOrderState()
    {
        ICatalogItem state = (ICatalogItem) _settings.get(STATE);
        if (state != null)
            _builder.add(MQExpression.eq(ORDER_ALIAS, IAbstractOrder.L_STATE, state));
    }

    // extract filters ordered by name

    @Override
    public void applyExtractCreateDate()
    {
        Date createDate = (Date) _settings.get(CREATE_DATE);
        if (createDate != null)
            _builder.add(UniMQExpression.eqDate(EXTRACT_ALIAS, IAbstractExtract.P_CREATE_DATE, createDate));
    }

    @Override
    public void applyExtractState()
    {
        ExtractStates state = (ExtractStates) _settings.get(STATE);
        if (state != null)
            _builder.add(MQExpression.eq(EXTRACT_ALIAS, IAbstractExtract.L_STATE, state));
    }

    @Override
    public void applyExtractLastName()
    {
        String lastName = (String) _settings.get(LAST_NAME);
        if (StringUtils.isNotEmpty(lastName))
            _builder.add(MQExpression.like(IDENTITY_CARD_ALIAS, IdentityCard.P_LAST_NAME, "%" + lastName));
    }

    @Override
    public void applyExtractType()
    {
        EmployeeExtractType type = (EmployeeExtractType) _settings.get(TYPE);
        if (type != null)
            _builder.add(MQExpression.eq(EXTRACT_ALIAS, IAbstractExtract.L_TYPE, type));
    }

    @Override
    public void applyListExtractType()
    {
        EmployeeExtractType type = (EmployeeExtractType) _settings.get(TYPE);
        if (type != null)
            _builder.add(MQExpression.eq(ORDER_ALIAS, EmployeeListOrder.L_TYPE, type));
    }

    // create page

    @Override
    @SuppressWarnings("unchecked")
    public void createPage(Session session, OrderDescriptionRegistry orderDescriptionRegistry, DynamicListDataSource dataSource)
    {
        orderDescriptionRegistry.applyOrder(_builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, _builder, session);
    }
}