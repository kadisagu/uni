/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e1.AddEdit;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.ICommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeAddExtract;

/**
 * @author dseleznev
 * Created on: 14.11.2008
 */
public interface IDAO extends ICommonModularEmployeeExtractAddEditDAO<EmployeeAddExtract, Model>
{
    public void prepareStaffRateDataSource(Model model);

    void preparePaymentDataSource(Model model);
}