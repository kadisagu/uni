/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e3.ExtractAddEdit;

import org.tandemframework.core.component.IBusinessComponent;

import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractAddEdit.AbstractListExtractAddEditController;
import ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract;

/**
 * @author ListExtractComponentGenerator
 * @since 27.04.2011
 */
public class Controller extends AbstractListExtractAddEditController<EmploymentDismissalEmplListExtract, IDAO, Model>
{
    @Override
    public void onChangeReason(IBusinessComponent component)
    {
        super.onChangeReason(component);

        Model model = getModel(component);

        model.getExtract().setEmployeeDismissReasons(getDao().getEmployeeDismissalReason(model.getExtract().getReason()));
    }
}
