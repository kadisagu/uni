/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e20.AddEdit;

import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.TransferHolidayDueDiseaseExtract;
import org.tandemframework.core.component.IBusinessComponent;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 06.12.2011
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<TransferHolidayDueDiseaseExtract, IDAO, Model>
{
    public void onChangeTransferToAnotherDay(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getExtract().getTransferToAnotherDay() != null && model.getExtract().getTransferToAnotherDay())
            model.getExtract().setDateToTransfer(null);
    }
}