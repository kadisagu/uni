/**
 * $Id$
 */
package ru.tandemservice.moveemployee.component.employee.EmployeePostOrdersPub;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author dseleznev
 * Created on: 07.11.2008
 */
public interface IDAO extends IUniDao<Model>
{
    public static final int MODULAR_PROJECTS_DATASOUCE_IDX = 1;
    public static final int MODULAR_EXTRACTS_DATASOUCE_IDX = 2;
    public static final int SINGLE_PROJECTS_DATASOUCE_IDX = 3;
    public static final int SINGLE_EXTRACTS_DATASOUCE_IDX = 4;
    public static final int LIST_ORDERS_DATASOUCE_IDX = 5;
    
    String P_PARAGRAPH_NUMBER = "paragraphNumber";
    String P_EXTRACT_NUMBER = "extractNumber";
    
    void prepareCustomDataSource(Model model, int index);
    
    void prepareModularProjectsDataSource(Model model);
    
    void prepareModularExtractsDataSource(Model model);
    
    void prepareSingleProjectsDataSource(Model model);
    
    void prepareSingleExtractsDataSource(Model model);
    
    void prepareListOrdersDataSource(Model model);
    
    void prepareOrdersDataSource(Model model);
}