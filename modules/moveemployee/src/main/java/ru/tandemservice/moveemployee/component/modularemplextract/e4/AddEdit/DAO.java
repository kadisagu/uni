/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e4.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeeTransferExtract;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniemp.entity.employee.StaffListItem;
import ru.tandemservice.uniemp.entity.employee.StaffListPostPayment;

import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 26.11.2008
 */
public class DAO extends CommonModularEmployeeExtractAddEditDAO<EmployeeTransferExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected EmployeeTransferExtract createNewInstance()
    {
        return new EmployeeTransferExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        CommonEmployeeExtractUtil.createPostBoundedModel(model);
        model.setPostRelationListModel(CommonEmployeeExtractUtil.getPostRelationListModel(model.getExtract()));
//        CommonEmployeeExtractUtil.createExtendedPaymentsBonusModel(model);

        if (model.isEditForm())
        {
            List<FinancingSourceDetails> financingSourceDetailsList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractFinancingSourceDetails(model.getExtract());
            model.setStaffRateItemList(financingSourceDetailsList);
            CommonEmployeeExtractUtil.prepareColumnsStaffRateSearchList(financingSourceDetailsList, model.getStaffRateDataSource());

            //поднимаем релейшены долей ставок и выбранных ставок ШР
            model.setDetailsToAllocItemList(MoveEmployeeDaoFacade.getMoveEmployeeDao().getFinSrcDetToAllocItemRelation(financingSourceDetailsList));

            //определяем какие источники финансирования показывать
            CommonEmployeeExtractUtil.fillFinSrcModel(model, model.getExtract().isFreelance(), model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL());

            //подготавливаем блок с выплатами
            preparePaymentBlock(model);
        }
        model.setFinancingSourceItemsMultiListModel(new UniQueryFullCheckSelectModel()
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "e");
                builder.add(MQExpression.like("e", FinancingSourceItem.title().s(), CoreStringUtils.escapeLike(filter)));
                builder.addOrder("e", FinancingSourceItem.title().s());
                return builder;
            }
        });

        if (model.isAddForm())
        {
            model.getExtract().setPostType(model.getEmployeePost().getPostType());

            EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getEmployeePost());
            if (null != contract)
            {
                model.getExtract().setLabourContractNumber(contract.getNumber());
                model.getExtract().setLabourContractDate(contract.getBeginDate());
                model.getExtract().setLabourContractType(contract.getType());
            }
            
            model.getExtract().setEmployeePost(model.getEmployeePost());
        }
        else
        {
            MQBuilder builder = new MQBuilder(FinancingSourceDetails.ENTITY_CLASS, "fsd", new String[] {FinancingSourceDetails.financingSourceItem().s()});
            builder.add(MQExpression.eq("fsd", FinancingSourceDetails.extract().s(), model.getExtract()));
            model.setSelectedFinSrcItemsList(builder.<FinancingSourceItem>getResultList(getSession()));

            model.setEmployeePost(model.getExtract().getEmployeePost());
        }

        model.setFinancingSourceItemModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                for (IEntity item : findValues("").getObjects())
                    if (item.getId().equals(primaryKey))
                        return item;

                return null;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<FinancingSourceItem> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                FinancingSource financingSource = finSrcMap.get(id);
                if (model.isThereAnyActiveStaffList())
                {
                    return new ListResult<>(UniempDaoFacade.getStaffListDAO().getFinancingSourcesItemStaffListItems(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), financingSource, filter));
                } else
                    return new ListResult<>(UniempDaoFacade.getUniempDAO().getFinSrcItm(financingSource.getId(), filter));
            }
        });

        model.setEmployeeHRModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> resultList = new ArrayList<>();

                for (IdentifiableWrapper wrapper : findValues("").getObjects())
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);

                return resultList;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
                final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

                long id = model.getStaffRateDataSource().getCurrentEntity().getId();
                OrgUnit orgUnit = model.getExtract().getOrgUnit();
                PostBoundedWithQGandQL post = model.getExtract().getPostBoundedWithQGandQL();

                if (finSrcMap.get(id) == null || orgUnit == null || post == null)
                    return ListResult.getEmpty();

                String NEW_STAFF_RATE_STR = "<Новая ставка> - ";

                List<IdentifiableWrapper> resultList = new ArrayList<>();

                List<StaffListAllocationItem> staffListAllocList = UniempDaoFacade.getStaffListDAO().getOccupiedStaffListAllocationItem(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id));

                if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id)))
                {
                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(orgUnit, post, finSrcMap.get(id), finSrcItmMap.get(id));
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
                    for (StaffListAllocationItem item : staffListAllocList)
                        if (!item.getEmployeePost().getPostStatus().isActive())
                            diff -= item.getStaffRate();
                    if (diff > 0 && NEW_STAFF_RATE_STR.contains(filter))
                        resultList.add(new IdentifiableWrapper(0L, NEW_STAFF_RATE_STR + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                for (StaffListAllocationItem item : staffListAllocList)
                {
                    if (item.getEmployeePost().getPerson().getFullFio().contains(filter))
                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                return new ListResult<>(resultList);
            }
        });

        model.setPaymentModel(CommonEmployeeExtractUtil.getPaymentModelForPaymentBlock());
        model.setFinSrcPaymentModel(CommonEmployeeExtractUtil.getFinSrcModelForPaymentBlock());
        model.setFinSrcItemPaymentModel(CommonEmployeeExtractUtil.getFinSrcItemModelForPaymentBlock(model.getPaymentDataSource()));
    }

    protected void preparePaymentBlock(Model model)
    {
        DQLSelectBuilder payBuilder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "b").column("b");
        payBuilder.where(DQLExpressions.eqValue(DQLExpressions.property(EmployeeBonus.extract().fromAlias("b")), model.getExtract()));

        model.setPaymentList(payBuilder.createStatement(getSession()).<EmployeeBonus>list());

        List<StaffListPostPayment> paymentList = CommonEmployeeExtractUtil.checkEnabledAddStaffListPaymentButton(model.getExtract().getPostBoundedWithQGandQL(), model.getExtract().getOrgUnit(), model.getPaymentList(), model.getStaffRateItemList());

        if (!paymentList.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);
    }

    @Override
    public void preparePaymentDataSource(Model model)
    {
        CommonEmployeeExtractUtil.preparePaymentDataSourceForPaymentBlock(model.getPaymentList(), model.getPaymentDataSource());
    }

//    @Override
//    public void prepareListDataSource(Model model)
//    {
//        CommonEmployeeExtractUtil.fillPaymentsBonusDataSource(model);
//    }

    @Override
    public void prepareFinSrcItemsDataSource(Model model)
    {
        UniBaseUtils.createFullPage(model.getFinSrcItemsDataSource(), model.getSelectedFinSrcItemsList());
        model.setSelectedFinSrcItemsList(CommonEmployeeExtractUtil.getFinancingSourceDetailsItemsList(model.getExtract(), (IValueMapHolder) model.getFinSrcItemsDataSource().getColumn("staffRateItems")));
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateItemList());

        CommonEmployeeExtractUtil.validateStaffRate(model.getStaffRateDataSource(), model.getStaffRateItemList(), model.getExtract().getPostType().getCode(), errors);

        validateFinSrcDetToAllocItem(model, errors);

        CommonEmployeeExtractUtil.validateForPaymentBlock(model.getPaymentList(), errors);
    }

    //выносим проверку в метод для переопределения в проекте
    protected void validateFinSrcDetToAllocItem(Model model, ErrorCollector errors)
    {
        CommonEmployeeExtractUtil.validateFinSrcDetToAllocItem(model, model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), errors);
    }

    @Override
    public void update(Model model)
    {
        if (null != model.getEmployeePost().getId())
        {
            model.getExtract().setEmployeePost(model.getEmployeePost());
            model.getExtract().setOldEmployeePostStatus(model.getEmployeePost().getPostStatus());
        }

        super.update(model);

//        CommonEmployeeExtractUtil.updateBonuses(model, getSession());

        CommonEmployeeExtractUtil.updateStaffRateList(model, model.isEditForm(), getSession());

        //PAYMENTS
        //если редактируем приказ, то удаляем созданные ранее выплаты в выписке
        if (model.isEditForm())
        {
            for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getSavedPaymentsFromPaymentBlockDQLBuilder(model.getExtract()).createStatement(getSession()).<EmployeeBonus>list())
                delete(bonus);
        }

        //сохраняем выплаты
        for (EmployeeBonus bonus : CommonEmployeeExtractUtil.getPaymentsForSaveFromPaymentBlock(model.getPaymentList()))
            save(bonus);
    }

    @Override
    public void prepareStaffRateDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getStaffRateItemList());

        if (model.isNeedUpdateDataSource())
        {
            CommonEmployeeExtractUtil.prepareColumnsStaffRateSearchList(model.getStaffRateItemList(), model.getStaffRateDataSource());
            model.setNeedUpdateDataSource(false);
        }
    }
}