/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e15;

import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.ProlongationAnnualHolidayExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 24.03.2011
 */
public class ProlongationAnnualHolidayExtractDao extends UniBaseDao implements IExtractComponentDao<ProlongationAnnualHolidayExtract>
{
    @Override
    public void doCommit(ProlongationAnnualHolidayExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        extract.getHoliday().setFinishDate(extract.getProlongationTo());
        extract.getHoliday().setHolidayExtract(extract);
        extract.getHoliday().setHolidayExtractDate(extract.getParagraph().getOrder().getCommitDate());
        extract.getHoliday().setHolidayExtractNumber(extract.getParagraph().getOrder().getNumber());

        update(extract.getHoliday());
        update(extract);
    }

    @Override
    public void doRollback(ProlongationAnnualHolidayExtract extract, Map parameters)
    {
        extract.getHoliday().setFinishDate(extract.getHolidayEndOld());

        update(extract.getHoliday());
        update(extract);
    }
}