/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.orderempl.modular.EmployeeModularOrderAdd;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.IMoveEmployeeComponents;
import ru.tandemservice.moveemployee.component.commons.MoveEmployeeColumns;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * @author dseleznev
 * Created on: 10.11.2008
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "EmployeeModularOrderAdd.filter"));

        model.setDisableCommitDate(!CoreServices.securityService().check(model.getOrder(), component.getUserContext().getPrincipalContext(), "editCommitDate_employeeModularOrder"));
        model.setDisableOrderNumber(!CoreServices.securityService().check(model.getOrder(), component.getUserContext().getPrincipalContext(), "editOrderNumber_employeeModularOrder"));

        prepareListDataSource(component);
    }

    @SuppressWarnings("unchecked")
    private void prepareListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn("checkbox"));
        dataSource.addColumn(MoveEmployeeColumns.getCreateDateColumn());
        dataSource.addColumn(MoveEmployeeColumns.getFullFioColumn());
        dataSource.addColumn(MoveEmployeeColumns.getExtractTypeColumn("Тип приказа"));
        dataSource.addColumn(MoveEmployeeColumns.getExtractPrintColumn(this, "printExtractFromModularOrderAddExtracts_employeeModularOrder"));
        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        getModel(component).setDataSource(dataSource);
    }

    public void onClickExtractPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMoveEmployeeComponents.MODULAR_EMPLOYEE_EXTRACT_PRINT, new ParametersMap()
        .add("extractId", component.getListenerParameter())
        ));
    }

    public synchronized final void onClickApply(IBusinessComponent component)
    {
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        Model model = getModel(component);
        getDao().validate(model, errors);
        if (errors.hasErrors()) return;

        getDao().update(model);
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getOrder()));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);
    }
}