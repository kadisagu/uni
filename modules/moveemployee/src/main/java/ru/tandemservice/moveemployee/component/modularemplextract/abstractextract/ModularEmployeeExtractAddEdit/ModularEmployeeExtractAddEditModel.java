/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;
import ru.tandemservice.moveemployee.entity.EmployeeModularParagraph;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;

import java.util.Date;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 13.11.2008
 */
@Input( {
    @Bind(key = "employeeId", binding = "employeeId"),
    @Bind(key = "employeePostId", binding = "employeePost.id"), 
    @Bind(key = "extractTypeId", binding = "extractTypeId"), 
    @Bind(key = "extractId", binding = "extractId"),
    @Bind(key = "extractAsOrder", binding = "extractAsOrder")
})
public abstract class ModularEmployeeExtractAddEditModel<T extends ModularEmployeeExtract>
{
    public static final Long NEW_MODULAR_ORDER_ID = -1L;

    private Long _extractId;
    private T _extract;
    private boolean _addForm;
    private boolean _editForm;
    private Long _employeeId;
    private Employee _employee;
    private EmployeePost _employeePost = new EmployeePost();
    
    private Long _extractTypeId;
    private EmployeeExtractType _extractType;
    private List<EmployeeExtractType> _extractTypeList;

    private Boolean _extractAsOrder;

    private EmployeeModularOrder _order;
    private IdentifiableWrapper _selectOrder;
    private EmployeeModularParagraph _paragraph;
    private ISingleSelectModel _orderList;
    private String _executorStr;
    private String _number;
    private Date _commitDate;

    private boolean _disableOrderNumber;
    private boolean _disableCommitDate;

    // Getters & Setters

    public String getNumber()
    {
        return _number;
    }

    public void setNumber(String number)
    {
        _number = number;
    }

    public Date getCommitDate()
    {
        return _commitDate;
    }

    public void setCommitDate(Date commitDate)
    {
        _commitDate = commitDate;
    }

    public boolean isDisableOrderNumber()
    {
        return _disableOrderNumber;
    }

    public void setDisableOrderNumber(boolean disableOrderNumber)
    {
        _disableOrderNumber = disableOrderNumber;
    }

    public boolean isDisableCommitDate()
    {
        return _disableCommitDate;
    }

    public void setDisableCommitDate(boolean disableCommitDate)
    {
        _disableCommitDate = disableCommitDate;
    }

    public IdentifiableWrapper getSelectOrder()
    {
        return _selectOrder;
    }

    public void setSelectOrder(IdentifiableWrapper selectOrder)
    {
        _selectOrder = selectOrder;
    }

    public ISingleSelectModel getOrderList()
    {
        return _orderList;
    }

    public void setOrderList(ISingleSelectModel orderList)
    {
        _orderList = orderList;
    }

    public String getExecutorStr()
    {
        return _executorStr;
    }

    public void setExecutorStr(String executorStr)
    {
        _executorStr = executorStr;
    }

    public EmployeeModularOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EmployeeModularOrder order)
    {
        _order = order;
    }

    public EmployeeModularParagraph getParagraph()
    {
        return _paragraph;
    }

    public void setParagraph(EmployeeModularParagraph paragraph)
    {
        _paragraph = paragraph;
    }

    public Boolean getExtractAsOrder()
    {
        return _extractAsOrder;
    }

    public void setExtractAsOrder(Boolean extractAsOrder)
    {
        _extractAsOrder = extractAsOrder;
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public T getExtract()
    {
        return _extract;
    }

    public void setExtract(T extract)
    {
        _extract = extract;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        _addForm = addForm;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        _editForm = editForm;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        this._employeePost = employeePost;
    }

    public Long getExtractTypeId()
    {
        return _extractTypeId;
    }

    public void setExtractTypeId(Long extractTypeId)
    {
        this._extractTypeId = extractTypeId;
    }

    public EmployeeExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(EmployeeExtractType extractType)
    {
        this._extractType = extractType;
    }

    public List<EmployeeExtractType> getExtractTypeList()
    {
        return _extractTypeList;
    }

    public void setExtractTypeList(List<EmployeeExtractType> extractTypeList)
    {
        this._extractTypeList = extractTypeList;
    }

    public Long getEmployeeId()
    {
        return _employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        this._employeeId = employeeId;
    }

    public Employee getEmployee()
    {
        return _employee;
    }

    public void setEmployee(Employee employee)
    {
        this._employee = employee;
    }

}