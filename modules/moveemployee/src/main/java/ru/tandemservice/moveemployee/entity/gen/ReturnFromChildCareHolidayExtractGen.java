package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О выходе из отпуска по уходу за ребенком
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ReturnFromChildCareHolidayExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract";
    public static final String ENTITY_NAME = "returnFromChildCareHolidayExtract";
    public static final int VERSION_HASH = -1315126219;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATIVE_FIO = "dativeFio";
    public static final String L_COMPETITION_TYPE = "competitionType";
    public static final String L_HOLIDAY_TYPE = "holidayType";
    public static final String P_RETURN_DATE = "returnDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_SAVE_PAYMENT = "savePayment";
    public static final String L_PAYMENT = "payment";
    public static final String P_START_DATE = "startDate";
    public static final String P_FINISH_DATE = "finishDate";
    public static final String L_STATE_OLD = "stateOld";
    public static final String L_COMPETITION_TYPE_OLD = "competitionTypeOld";
    public static final String P_HOLIDAY_END_OLD = "holidayEndOld";
    public static final String L_CHANGE_HOLIDAY = "changeHoliday";
    public static final String P_FREELANCE = "freelance";

    private String _dativeFio;     // ФИО сотрудника в дательном падеже
    private CompetitionAssignmentType _competitionType;     // Тип конкурсного назначения на должность
    private HolidayType _holidayType;     // Вид отпуска
    private Date _returnDate;     // Дата выхода из отпуска
    private Date _endDate;     // Дата окончания
    private Boolean _savePayment;     // Сохранить сотруднику выплату пособия по уходу за ребенком
    private EmployeePayment _payment;     // Выплата
    private Date _startDate;     // Назначена с даты
    private Date _finishDate;     // Назначена по дату
    private EmployeePostStatus _stateOld;     // Состояние сотрудника до проведения приказа
    private CompetitionAssignmentType _competitionTypeOld;     // Тип конкурсного назначения на должность до проведения приказа
    private Date _holidayEndOld;     // Дата окончания отпуска до проведения приказа
    private EmployeeHoliday _changeHoliday;     // Отпуск по уходу за ребенком, дату окончания которого редактируем
    private boolean _freelance;     // Вне штата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ФИО сотрудника в дательном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDativeFio()
    {
        return _dativeFio;
    }

    /**
     * @param dativeFio ФИО сотрудника в дательном падеже. Свойство не может быть null.
     */
    public void setDativeFio(String dativeFio)
    {
        dirty(_dativeFio, dativeFio);
        _dativeFio = dativeFio;
    }

    /**
     * @return Тип конкурсного назначения на должность.
     */
    public CompetitionAssignmentType getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Тип конкурсного назначения на должность.
     */
    public void setCompetitionType(CompetitionAssignmentType competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Вид отпуска. Свойство не может быть null.
     */
    @NotNull
    public HolidayType getHolidayType()
    {
        return _holidayType;
    }

    /**
     * @param holidayType Вид отпуска. Свойство не может быть null.
     */
    public void setHolidayType(HolidayType holidayType)
    {
        dirty(_holidayType, holidayType);
        _holidayType = holidayType;
    }

    /**
     * @return Дата выхода из отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getReturnDate()
    {
        return _returnDate;
    }

    /**
     * @param returnDate Дата выхода из отпуска. Свойство не может быть null.
     */
    public void setReturnDate(Date returnDate)
    {
        dirty(_returnDate, returnDate);
        _returnDate = returnDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Сохранить сотруднику выплату пособия по уходу за ребенком.
     */
    public Boolean getSavePayment()
    {
        return _savePayment;
    }

    /**
     * @param savePayment Сохранить сотруднику выплату пособия по уходу за ребенком.
     */
    public void setSavePayment(Boolean savePayment)
    {
        dirty(_savePayment, savePayment);
        _savePayment = savePayment;
    }

    /**
     * @return Выплата.
     */
    public EmployeePayment getPayment()
    {
        return _payment;
    }

    /**
     * @param payment Выплата.
     */
    public void setPayment(EmployeePayment payment)
    {
        dirty(_payment, payment);
        _payment = payment;
    }

    /**
     * @return Назначена с даты.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Назначена с даты.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Назначена по дату.
     */
    public Date getFinishDate()
    {
        return _finishDate;
    }

    /**
     * @param finishDate Назначена по дату.
     */
    public void setFinishDate(Date finishDate)
    {
        dirty(_finishDate, finishDate);
        _finishDate = finishDate;
    }

    /**
     * @return Состояние сотрудника до проведения приказа.
     */
    public EmployeePostStatus getStateOld()
    {
        return _stateOld;
    }

    /**
     * @param stateOld Состояние сотрудника до проведения приказа.
     */
    public void setStateOld(EmployeePostStatus stateOld)
    {
        dirty(_stateOld, stateOld);
        _stateOld = stateOld;
    }

    /**
     * @return Тип конкурсного назначения на должность до проведения приказа.
     */
    public CompetitionAssignmentType getCompetitionTypeOld()
    {
        return _competitionTypeOld;
    }

    /**
     * @param competitionTypeOld Тип конкурсного назначения на должность до проведения приказа.
     */
    public void setCompetitionTypeOld(CompetitionAssignmentType competitionTypeOld)
    {
        dirty(_competitionTypeOld, competitionTypeOld);
        _competitionTypeOld = competitionTypeOld;
    }

    /**
     * @return Дата окончания отпуска до проведения приказа.
     */
    public Date getHolidayEndOld()
    {
        return _holidayEndOld;
    }

    /**
     * @param holidayEndOld Дата окончания отпуска до проведения приказа.
     */
    public void setHolidayEndOld(Date holidayEndOld)
    {
        dirty(_holidayEndOld, holidayEndOld);
        _holidayEndOld = holidayEndOld;
    }

    /**
     * @return Отпуск по уходу за ребенком, дату окончания которого редактируем.
     */
    public EmployeeHoliday getChangeHoliday()
    {
        return _changeHoliday;
    }

    /**
     * @param changeHoliday Отпуск по уходу за ребенком, дату окончания которого редактируем.
     */
    public void setChangeHoliday(EmployeeHoliday changeHoliday)
    {
        dirty(_changeHoliday, changeHoliday);
        _changeHoliday = changeHoliday;
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreelance()
    {
        return _freelance;
    }

    /**
     * @param freelance Вне штата. Свойство не может быть null.
     */
    public void setFreelance(boolean freelance)
    {
        dirty(_freelance, freelance);
        _freelance = freelance;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ReturnFromChildCareHolidayExtractGen)
        {
            setDativeFio(((ReturnFromChildCareHolidayExtract)another).getDativeFio());
            setCompetitionType(((ReturnFromChildCareHolidayExtract)another).getCompetitionType());
            setHolidayType(((ReturnFromChildCareHolidayExtract)another).getHolidayType());
            setReturnDate(((ReturnFromChildCareHolidayExtract)another).getReturnDate());
            setEndDate(((ReturnFromChildCareHolidayExtract)another).getEndDate());
            setSavePayment(((ReturnFromChildCareHolidayExtract)another).getSavePayment());
            setPayment(((ReturnFromChildCareHolidayExtract)another).getPayment());
            setStartDate(((ReturnFromChildCareHolidayExtract)another).getStartDate());
            setFinishDate(((ReturnFromChildCareHolidayExtract)another).getFinishDate());
            setStateOld(((ReturnFromChildCareHolidayExtract)another).getStateOld());
            setCompetitionTypeOld(((ReturnFromChildCareHolidayExtract)another).getCompetitionTypeOld());
            setHolidayEndOld(((ReturnFromChildCareHolidayExtract)another).getHolidayEndOld());
            setChangeHoliday(((ReturnFromChildCareHolidayExtract)another).getChangeHoliday());
            setFreelance(((ReturnFromChildCareHolidayExtract)another).isFreelance());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ReturnFromChildCareHolidayExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ReturnFromChildCareHolidayExtract.class;
        }

        public T newInstance()
        {
            return (T) new ReturnFromChildCareHolidayExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "dativeFio":
                    return obj.getDativeFio();
                case "competitionType":
                    return obj.getCompetitionType();
                case "holidayType":
                    return obj.getHolidayType();
                case "returnDate":
                    return obj.getReturnDate();
                case "endDate":
                    return obj.getEndDate();
                case "savePayment":
                    return obj.getSavePayment();
                case "payment":
                    return obj.getPayment();
                case "startDate":
                    return obj.getStartDate();
                case "finishDate":
                    return obj.getFinishDate();
                case "stateOld":
                    return obj.getStateOld();
                case "competitionTypeOld":
                    return obj.getCompetitionTypeOld();
                case "holidayEndOld":
                    return obj.getHolidayEndOld();
                case "changeHoliday":
                    return obj.getChangeHoliday();
                case "freelance":
                    return obj.isFreelance();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "dativeFio":
                    obj.setDativeFio((String) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((CompetitionAssignmentType) value);
                    return;
                case "holidayType":
                    obj.setHolidayType((HolidayType) value);
                    return;
                case "returnDate":
                    obj.setReturnDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "savePayment":
                    obj.setSavePayment((Boolean) value);
                    return;
                case "payment":
                    obj.setPayment((EmployeePayment) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "finishDate":
                    obj.setFinishDate((Date) value);
                    return;
                case "stateOld":
                    obj.setStateOld((EmployeePostStatus) value);
                    return;
                case "competitionTypeOld":
                    obj.setCompetitionTypeOld((CompetitionAssignmentType) value);
                    return;
                case "holidayEndOld":
                    obj.setHolidayEndOld((Date) value);
                    return;
                case "changeHoliday":
                    obj.setChangeHoliday((EmployeeHoliday) value);
                    return;
                case "freelance":
                    obj.setFreelance((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dativeFio":
                        return true;
                case "competitionType":
                        return true;
                case "holidayType":
                        return true;
                case "returnDate":
                        return true;
                case "endDate":
                        return true;
                case "savePayment":
                        return true;
                case "payment":
                        return true;
                case "startDate":
                        return true;
                case "finishDate":
                        return true;
                case "stateOld":
                        return true;
                case "competitionTypeOld":
                        return true;
                case "holidayEndOld":
                        return true;
                case "changeHoliday":
                        return true;
                case "freelance":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "dativeFio":
                    return true;
                case "competitionType":
                    return true;
                case "holidayType":
                    return true;
                case "returnDate":
                    return true;
                case "endDate":
                    return true;
                case "savePayment":
                    return true;
                case "payment":
                    return true;
                case "startDate":
                    return true;
                case "finishDate":
                    return true;
                case "stateOld":
                    return true;
                case "competitionTypeOld":
                    return true;
                case "holidayEndOld":
                    return true;
                case "changeHoliday":
                    return true;
                case "freelance":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "dativeFio":
                    return String.class;
                case "competitionType":
                    return CompetitionAssignmentType.class;
                case "holidayType":
                    return HolidayType.class;
                case "returnDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "savePayment":
                    return Boolean.class;
                case "payment":
                    return EmployeePayment.class;
                case "startDate":
                    return Date.class;
                case "finishDate":
                    return Date.class;
                case "stateOld":
                    return EmployeePostStatus.class;
                case "competitionTypeOld":
                    return CompetitionAssignmentType.class;
                case "holidayEndOld":
                    return Date.class;
                case "changeHoliday":
                    return EmployeeHoliday.class;
                case "freelance":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ReturnFromChildCareHolidayExtract> _dslPath = new Path<ReturnFromChildCareHolidayExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ReturnFromChildCareHolidayExtract");
    }
            

    /**
     * @return ФИО сотрудника в дательном падеже. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getDativeFio()
     */
    public static PropertyPath<String> dativeFio()
    {
        return _dslPath.dativeFio();
    }

    /**
     * @return Тип конкурсного назначения на должность.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getCompetitionType()
     */
    public static CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Вид отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getHolidayType()
     */
    public static HolidayType.Path<HolidayType> holidayType()
    {
        return _dslPath.holidayType();
    }

    /**
     * @return Дата выхода из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getReturnDate()
     */
    public static PropertyPath<Date> returnDate()
    {
        return _dslPath.returnDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Сохранить сотруднику выплату пособия по уходу за ребенком.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getSavePayment()
     */
    public static PropertyPath<Boolean> savePayment()
    {
        return _dslPath.savePayment();
    }

    /**
     * @return Выплата.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getPayment()
     */
    public static EmployeePayment.Path<EmployeePayment> payment()
    {
        return _dslPath.payment();
    }

    /**
     * @return Назначена с даты.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Назначена по дату.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getFinishDate()
     */
    public static PropertyPath<Date> finishDate()
    {
        return _dslPath.finishDate();
    }

    /**
     * @return Состояние сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getStateOld()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> stateOld()
    {
        return _dslPath.stateOld();
    }

    /**
     * @return Тип конкурсного назначения на должность до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getCompetitionTypeOld()
     */
    public static CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionTypeOld()
    {
        return _dslPath.competitionTypeOld();
    }

    /**
     * @return Дата окончания отпуска до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getHolidayEndOld()
     */
    public static PropertyPath<Date> holidayEndOld()
    {
        return _dslPath.holidayEndOld();
    }

    /**
     * @return Отпуск по уходу за ребенком, дату окончания которого редактируем.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getChangeHoliday()
     */
    public static EmployeeHoliday.Path<EmployeeHoliday> changeHoliday()
    {
        return _dslPath.changeHoliday();
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#isFreelance()
     */
    public static PropertyPath<Boolean> freelance()
    {
        return _dslPath.freelance();
    }

    public static class Path<E extends ReturnFromChildCareHolidayExtract> extends ModularEmployeeExtract.Path<E>
    {
        private PropertyPath<String> _dativeFio;
        private CompetitionAssignmentType.Path<CompetitionAssignmentType> _competitionType;
        private HolidayType.Path<HolidayType> _holidayType;
        private PropertyPath<Date> _returnDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Boolean> _savePayment;
        private EmployeePayment.Path<EmployeePayment> _payment;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _finishDate;
        private EmployeePostStatus.Path<EmployeePostStatus> _stateOld;
        private CompetitionAssignmentType.Path<CompetitionAssignmentType> _competitionTypeOld;
        private PropertyPath<Date> _holidayEndOld;
        private EmployeeHoliday.Path<EmployeeHoliday> _changeHoliday;
        private PropertyPath<Boolean> _freelance;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ФИО сотрудника в дательном падеже. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getDativeFio()
     */
        public PropertyPath<String> dativeFio()
        {
            if(_dativeFio == null )
                _dativeFio = new PropertyPath<String>(ReturnFromChildCareHolidayExtractGen.P_DATIVE_FIO, this);
            return _dativeFio;
        }

    /**
     * @return Тип конкурсного назначения на должность.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getCompetitionType()
     */
        public CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new CompetitionAssignmentType.Path<CompetitionAssignmentType>(L_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Вид отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getHolidayType()
     */
        public HolidayType.Path<HolidayType> holidayType()
        {
            if(_holidayType == null )
                _holidayType = new HolidayType.Path<HolidayType>(L_HOLIDAY_TYPE, this);
            return _holidayType;
        }

    /**
     * @return Дата выхода из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getReturnDate()
     */
        public PropertyPath<Date> returnDate()
        {
            if(_returnDate == null )
                _returnDate = new PropertyPath<Date>(ReturnFromChildCareHolidayExtractGen.P_RETURN_DATE, this);
            return _returnDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(ReturnFromChildCareHolidayExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Сохранить сотруднику выплату пособия по уходу за ребенком.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getSavePayment()
     */
        public PropertyPath<Boolean> savePayment()
        {
            if(_savePayment == null )
                _savePayment = new PropertyPath<Boolean>(ReturnFromChildCareHolidayExtractGen.P_SAVE_PAYMENT, this);
            return _savePayment;
        }

    /**
     * @return Выплата.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getPayment()
     */
        public EmployeePayment.Path<EmployeePayment> payment()
        {
            if(_payment == null )
                _payment = new EmployeePayment.Path<EmployeePayment>(L_PAYMENT, this);
            return _payment;
        }

    /**
     * @return Назначена с даты.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(ReturnFromChildCareHolidayExtractGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Назначена по дату.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getFinishDate()
     */
        public PropertyPath<Date> finishDate()
        {
            if(_finishDate == null )
                _finishDate = new PropertyPath<Date>(ReturnFromChildCareHolidayExtractGen.P_FINISH_DATE, this);
            return _finishDate;
        }

    /**
     * @return Состояние сотрудника до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getStateOld()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> stateOld()
        {
            if(_stateOld == null )
                _stateOld = new EmployeePostStatus.Path<EmployeePostStatus>(L_STATE_OLD, this);
            return _stateOld;
        }

    /**
     * @return Тип конкурсного назначения на должность до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getCompetitionTypeOld()
     */
        public CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionTypeOld()
        {
            if(_competitionTypeOld == null )
                _competitionTypeOld = new CompetitionAssignmentType.Path<CompetitionAssignmentType>(L_COMPETITION_TYPE_OLD, this);
            return _competitionTypeOld;
        }

    /**
     * @return Дата окончания отпуска до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getHolidayEndOld()
     */
        public PropertyPath<Date> holidayEndOld()
        {
            if(_holidayEndOld == null )
                _holidayEndOld = new PropertyPath<Date>(ReturnFromChildCareHolidayExtractGen.P_HOLIDAY_END_OLD, this);
            return _holidayEndOld;
        }

    /**
     * @return Отпуск по уходу за ребенком, дату окончания которого редактируем.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#getChangeHoliday()
     */
        public EmployeeHoliday.Path<EmployeeHoliday> changeHoliday()
        {
            if(_changeHoliday == null )
                _changeHoliday = new EmployeeHoliday.Path<EmployeeHoliday>(L_CHANGE_HOLIDAY, this);
            return _changeHoliday;
        }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.ReturnFromChildCareHolidayExtract#isFreelance()
     */
        public PropertyPath<Boolean> freelance()
        {
            if(_freelance == null )
                _freelance = new PropertyPath<Boolean>(ReturnFromChildCareHolidayExtractGen.P_FREELANCE, this);
            return _freelance;
        }

        public Class getEntityClass()
        {
            return ReturnFromChildCareHolidayExtract.class;
        }

        public String getEntityName()
        {
            return "returnFromChildCareHolidayExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
