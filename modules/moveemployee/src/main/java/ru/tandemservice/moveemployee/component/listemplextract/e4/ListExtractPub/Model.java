/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e4.ListExtractPub;

import ru.tandemservice.moveemployee.component.listemplextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.moveemployee.entity.EmployeeHolidayEmplListExtract;

/**
 * @author ListExtractComponentGenerator
 * @since 04.10.2011
 */
public class Model extends AbstractListExtractPubModel<EmployeeHolidayEmplListExtract>
{
    String _periodOfSrt;
    String _annualHolidayStr;
    String _secondHilidayStr;
    String _holidayStr;

    //Getters & Setters

    public String getPeriodOfSrt()
    {
        return _periodOfSrt;
    }

    public void setPeriodOfSrt(String periodOfSrt)
    {
        _periodOfSrt = periodOfSrt;
    }

    public String getAnnualHolidayStr()
    {
        return _annualHolidayStr;
    }

    public void setAnnualHolidayStr(String annualHolidayStr)
    {
        _annualHolidayStr = annualHolidayStr;
    }

    public String getSecondHilidayStr()
    {
        return _secondHilidayStr;
    }

    public void setSecondHilidayStr(String secondHilidayStr)
    {
        _secondHilidayStr = secondHilidayStr;
    }

    public String getHolidayStr()
    {
        return _holidayStr;
    }

    public void setHolidayStr(String holidayStr)
    {
        _holidayStr = holidayStr;
    }
}
