\keep\keepn\qc \b{extractNumber}.\b0
\par
\keep\keepn\qj \b{employeeNumber}.\b0
 {fio_D}, {post_D} {orgUnit_G}, {changeSize} {secondEmploymentPeriod} доплату за работу по совмещению должности {post_G_combination} {orgUnit_G_combination}{absEmployer} в размере {procStaffRate} от {fixedSalary} рублей в месяц.
 \par
 \par
 Основание: {listBasics}.
