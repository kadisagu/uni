\fs28\fi709\qj {textParagraph} \par\fi0\par
\keep\keepn\qj
\b{extractNumber}. \b0
На {Fio}, {post_G} {orgUnit_G}, возложить с {dateFrom} по {dateTo} исполнение обязанностей {absPost_G} {absOrgUnit_G} на время отсутствия {absFio_G}{conditions}.
\par
\par
Основание: {listBasics}\par