/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e22;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.moveemployee.component.commons.CommonExtractPrintUtil;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.CombinationPostPaymentExtract;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 21.12.2011
 */
public class CombinationPostPaymentExtractPrint implements IPrintFormCreator<CombinationPostPaymentExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, CombinationPostPaymentExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        StringBuilder secondEmploymentPeriod = new StringBuilder();
        secondEmploymentPeriod.append("c ");
        secondEmploymentPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        if (extract.getEndDate() != null)
        {
            secondEmploymentPeriod.append(" по ");
            secondEmploymentPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));
        }
        modifier.put("secondEmploymentPeriod", secondEmploymentPeriod.toString());

        CommonExtractPrintUtil.injectPost(modifier, extract.getPostBoundedWithQGandQL(), "combination");

        CommonExtractPrintUtil.injectOrgUnit(modifier, extract.getOrgUnit(), "combination");

        if (extract.getMissingEmployeePost() != null)
        {
            StringBuilder absEmployer = new StringBuilder(" на время отсутствия ");
            String fioDeclinated = CommonExtractUtil.getModifiedFioInitials(extract.getMissingEmployeePost().getPerson().getIdentityCard(), GrammaCase.GENITIVE);
            absEmployer.append(fioDeclinated);
            modifier.put("absEmployer",absEmployer.toString());
        }
        else
            modifier.put("absEmployer", "");

        Double staffRate = 0d;
        for (CombinationPostStaffRateExtractRelation relation : MoveEmployeeDaoFacade.getMoveEmployeeDao().getCombinationPostStaffRateExtractRelationList(extract))
            staffRate += relation.getStaffRate();

        staffRate = staffRate * 100;
        modifier.put("procStaffRate", staffRate.longValue() + "% от");

        if (extract.getSalaryRaisingCoefficient() != null)
            modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getSalaryRaisingCoefficient().getRecommendedSalary()));
        else
            modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getPostBoundedWithQGandQL().getSalary()));

        extendModifier(extract, modifier);

        modifier.modify(document);

        return document;
    }

    protected void extendModifier(CombinationPostPaymentExtract extract, RtfInjectModifier modifier)
    {

    }
}