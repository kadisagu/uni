/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.settings.encouragementPrintFormatSettings.EncouragementPrintFormatList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * Create by ashaburov
 * Date 29.09.11
 */
public interface IDAO extends IUniDao<Model>
{
    @Override
    void prepareListDataSource(Model model);
}
