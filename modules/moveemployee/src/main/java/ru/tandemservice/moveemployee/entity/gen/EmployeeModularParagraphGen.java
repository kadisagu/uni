package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeParagraph;
import ru.tandemservice.moveemployee.entity.EmployeeModularParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф сборного приказа по кадровому составу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeModularParagraphGen extends AbstractEmployeeParagraph
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeModularParagraph";
    public static final String ENTITY_NAME = "employeeModularParagraph";
    public static final int VERSION_HASH = -402987988;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeModularParagraphGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeModularParagraphGen> extends AbstractEmployeeParagraph.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeModularParagraph.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeModularParagraph();
        }
    }
    private static final Path<EmployeeModularParagraph> _dslPath = new Path<EmployeeModularParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeModularParagraph");
    }
            

    public static class Path<E extends EmployeeModularParagraph> extends AbstractEmployeeParagraph.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EmployeeModularParagraph.class;
        }

        public String getEntityName()
        {
            return "employeeModularParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
