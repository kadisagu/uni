/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e28.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.CommonModularEmployeeExtractAddEdit.CommonModularEmployeeExtractAddEditController;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetails;
import ru.tandemservice.moveemployee.entity.FinancingSourceDetailsToAllocItem;
import ru.tandemservice.moveemployee.entity.ScienceStatusPaymentExtract;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.*;

import java.util.*;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 19.03.2012
 */
public class Controller extends CommonModularEmployeeExtractAddEditController<ScienceStatusPaymentExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareStaffRateDataSource(component);
        preparePaymentDataSource(component);
        super.onRefreshComponent(component);

        prepareEmployeeStaffRateDataSource(component);


        checkEnabledAddStaffListPaymentButton(component);

        hasActiveStaffList(component);

        Model model = getModel(component);

        if (model.isEditForm() && isShowPostFields(model))
        {
            fillStaffRateValueMaps(model);
        }
    }

    public void onClickFreelance(IBusinessComponent component)
    {
        hasActiveStaffList(component);
    }

    @Override
    public void onChangeBasics(IBusinessComponent component)
    {
        Model model = getModel(component);

        Date date = model.getExtract().getAddAgreementDate();
        String number = model.getExtract().getAddAgreementNumber();

        CommonEmployeeExtractUtil.fillExtractBasics(model.getSelectedBasicList(), model.getCurrentBasicMap(), null, null, date, number);
    }

    private void prepareEmployeeStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getEmployeeStaffRateDataSource() != null)
            return;


        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareEmployeeStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setEmployeeStaffRateDataSource(dataSource);
    }

    public void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<FinancingSourceDetails> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        }, 5);

        dataSource.addColumn(new BlockColumn("staffRate", "Ставка"));
        dataSource.addColumn(new BlockColumn("financingSource", "Источник финансирования"));
        dataSource.addColumn(new BlockColumn("financingSourceItem", "Источник финансирования (детально)"));
        if (model.isThereAnyActiveStaffList())
            dataSource.addColumn(new BlockColumn("employeeHR", "Кадровая расстановка"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem");
        actionColumn.setDisableSecondSubmit(false);
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setStaffRateDataSource(dataSource);
    }

    public void preparePaymentDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getPaymentDataSource() != null)
            return;

        DynamicListDataSource<EmployeeBonus> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().preparePaymentDataSource(model);
        }, 5);

//        IListenerParametersResolver parametersResolver = new IListenerParametersResolver()
//        {
//            @Override
//            public Object getListenerParameters(IEntity entity, IEntity valueEntity)
//            {
//                return entity;
//            }
//        };

        CommonEmployeeExtractUtil.preparePaymentDataSourceForPaymentBlock(dataSource);

        model.setPaymentDataSource(dataSource);
    }

    /**
     * Добавляет долю ставки(объект в список Ставка).
     */
    public void onClickAddStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        FinancingSourceDetails item = new FinancingSourceDetails();

        short discriminator = EntityRuntime.getMeta(FinancingSourceDetails.class).getEntityCode();
        long id = EntityIDGenerator.generateNewId(discriminator);

        item.setId(id);
        item.setExtract(model.getExtract());

        model.getStaffRateList().add(item);

        model.getStaffRateDataSource().refresh();
    }

    /**
     * Удаляет долю ставки(элемент из списка Ставка).
     */
    public void onClickDeleteItem(IBusinessComponent component)
    {
        Model model = getModel(component);

        FinancingSourceDetails item = (FinancingSourceDetails) component.getListenerParameter();

        model.getStaffRateList().remove(item);

        model.getStaffRateDataSource().refresh();

        onChangeStaffRate(component);
    }

    public void onChangeStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double staffRateSumm = 0d;
        for (FinancingSourceDetails item : model.getStaffRateList())
            staffRateSumm += getValue(staffRateMap.get(item.getId()));

        if (model.getExtract().getRaisingCoefficient() != null)
            model.getExtract().setSalary(model.getExtract().getRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (model.getExtract().getPost() != null && model.getExtract().getPost().getSalary() != null)
            model.getExtract().setSalary(model.getExtract().getPost().getSalary() * staffRateSumm);
    }

    public void onChangeFinSrc(IBusinessComponent component)
    {
        Model model = getModel(component);

        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateList());

        checkEnabledAddStaffListPaymentButton(component);
    }

    public void onChangeFinSrcItm(IBusinessComponent component)
    {
        Model model = getModel(component);

        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateList());

        checkEnabledAddStaffListPaymentButton(component);
    }

    public void onChangeRaisingCoefficient(IBusinessComponent component)
    {
        onChangeStaffRate(component);
    }

    public void onChangePost(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getExtract().getPost() == null)
        {
            model.setStaffRateList(new ArrayList<>());
            return;
        }

        prepareStaffRateList(model);

        CommonEmployeeExtractUtil.prepareStaffRateList(model.getStaffRateDataSource(), model.getStaffRateList());

        checkEnabledAddStaffListPaymentButton(component);

        onChangeStaffRate(component);
    }

    /**
     * Подготавливает и заполняет значениями по умолчанию список Ставка.
     * Заполняется уже имеющимися ставками сотрудника.
     */
    @SuppressWarnings("unchecked")
    protected void prepareStaffRateList(Model model)
    {
        Map<Long, Double> staffRateMap = new HashMap<>();
        Map<Long, FinancingSource> finSrcMap = new HashMap<>();
        Map<Long, FinancingSourceItem> finSrcItemMap = new HashMap<>();
        model.setStaffRateList(new ArrayList<>());
        for (EmployeePostStaffRateItem staffRate : model.getEmployeeStaffRateDataSource().getEntityList())
        {
            FinancingSourceDetails item = new FinancingSourceDetails();

            short discriminator = EntityRuntime.getMeta(FinancingSourceDetails.class).getEntityCode();
            long id = EntityIDGenerator.generateNewId(discriminator);

            item.setId(id);
            item.setExtract(model.getExtract());

            staffRateMap.put(id, staffRate.getStaffRate());
            finSrcMap.put(id, staffRate.getFinancingSource());
            finSrcItemMap.put(id, staffRate.getFinancingSourceItem());

            model.getStaffRateList().add(item);
        }

        ((BlockColumn)model.getStaffRateDataSource().getColumn("staffRate")).setValueMap(staffRateMap);
        ((BlockColumn)model.getStaffRateDataSource().getColumn("financingSource")).setValueMap(finSrcMap);
        ((BlockColumn)model.getStaffRateDataSource().getColumn("financingSourceItem")).setValueMap(finSrcItemMap);

        model.getStaffRateDataSource().refresh();
    }

    public void onClickAddSinglePayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus bonus = CommonEmployeeExtractUtil.onClickAddSinglePayment(model.getPaymentList(), model.getExtract());

        if (model.getExtract().getBeginPayDate() != null)
            bonus.setBeginDate(model.getExtract().getBeginPayDate());
        if (model.getExtract().getEndPayDate() != null)
            bonus.setEndDate(model.getExtract().getEndPayDate());

        model.getPaymentDataSource().refresh();
    }

    public void onClickAddStaffListPayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        List<StaffListPostPayment> staffListPaymentsSet = checkEnabledAddStaffListPaymentButton(component);

        List<EmployeeBonus> employeeBonusList = CommonEmployeeExtractUtil.onClickAddStaffListPayment(model.getPaymentList(), staffListPaymentsSet, model.getExtract());

        for (EmployeeBonus item : employeeBonusList)
        {
            if (model.getExtract().getBeginPayDate() != null)
                item.setBeginDate(model.getExtract().getBeginPayDate());
            if (model.getExtract().getEndPayDate() != null)
                item.setEndDate(model.getExtract().getEndPayDate());
        }

        model.setAddStaffListPaymentsButtonVisible(false);

        model.getPaymentDataSource().refresh();
    }

    public void onChangePayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus bonus = component.getListenerParameter();

        bonus.setValueProxy(CommonEmployeeExtractUtil.getPaymentValueForPaymentBlock(bonus.getPayment(), model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL()));

        checkEnabledAddStaffListPaymentButton(component);
    }

    public void onUpPayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus currentBonus = component.<EmployeeBonus>getListenerParameter();

        CommonEmployeeExtractUtil.onUpPayment(currentBonus, model.getPaymentList());

        model.getPaymentDataSource().refresh();
    }

    public void onDownPayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus currentBonus = component.<EmployeeBonus>getListenerParameter();

        CommonEmployeeExtractUtil.onDownPayment(currentBonus, model.getPaymentList());

        model.getPaymentDataSource().refresh();
    }

    public void onDeletePayment(IBusinessComponent component)
    {
        Model model = getModel(component);

        EmployeeBonus currentBonus = component.<EmployeeBonus>getListenerParameter();

        CommonEmployeeExtractUtil.onDeletePayment(currentBonus, model.getPaymentList());

        checkEnabledAddStaffListPaymentButton(component);

        model.getPaymentDataSource().refresh();
    }

    public void onChangeBeginDate(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getExtract().getBeginDate() == null)
            return;

        model.getExtract().setBeginPayDate(model.getExtract().getBeginDate());
    }

    /**
     * Если есть активное ШР на подразделении сотрудника, то обновляем список Ставки добавляя колонку Кадровая расстановка.
     */
    protected void hasActiveStaffList(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getEmployeePost().getOrgUnit() != null &&
                UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEmployeePost().getOrgUnit()) != null && !model.getExtract().isFreelance())
        {
            if (!model.isThereAnyActiveStaffList())
            {
                model.setThereAnyActiveStaffList(true);
                model.setStaffRateDataSource(null);
                prepareStaffRateDataSource(component);
                prepareEmpHRColumn(component);
            }
        }
        else if (model.isThereAnyActiveStaffList())
        {
            model.setThereAnyActiveStaffList(false);
            model.setStaffRateDataSource(null);
            prepareStaffRateDataSource(component);
        }

    }

    /**
     * Заполняет поля списка Ставок из объектов списка.
     */
    @SuppressWarnings("unchecked")
    protected void fillStaffRateValueMaps(Model model)
    {
        Map<Long, Double> staffRateMap = new HashMap<>();
        for (FinancingSourceDetails item : model.getStaffRateList())
            staffRateMap.put(item.getId(), item.getStaffRate());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(0)).setValueMap(staffRateMap);

        Map<Long, FinancingSource> finSrcMap = new HashMap<>();
        for (FinancingSourceDetails item : model.getStaffRateList())
            finSrcMap.put(item.getId(), item.getFinancingSource());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(1)).setValueMap(finSrcMap);

        Map<Long, FinancingSourceItem> finSrcItmMap = new HashMap<>();
        for (FinancingSourceDetails item : model.getStaffRateList())
            finSrcItmMap.put(item.getId(), item.getFinancingSourceItem());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(2)).setValueMap(finSrcItmMap);
    }

    /**
     * Если в колонке Кадровая расстановка списка Ставки уже были выбраны эл-ты, то заполняем их.
     */
    @SuppressWarnings("unchecked")
    protected void prepareEmpHRColumn(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.isAddForm() || !model.isThereAnyActiveStaffList())
            return;

        StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEmployeePost().getOrgUnit());
        if (isShowPostFields(model) && activeStaffList != null)
        {
            prepareStaffRateDataSource(component);

            fillStaffRateValueMaps(model);
            Map<Long, List<IdentifiableWrapper>> idWrapperMap = new HashMap<>();
            for (FinancingSourceDetails rateItem : model.getStaffRateList())
            {
                List<IdentifiableWrapper> wrapperList = new ArrayList<>();

                Map<FinancingSourceDetails, List<FinancingSourceDetailsToAllocItem>> staffRateAllocItemMap = getDao().getStaffRateAllocItemMap(model);

                List<FinancingSourceDetailsToAllocItem> items = staffRateAllocItemMap.get(rateItem);
                if (items != null)
                    for (FinancingSourceDetailsToAllocItem allocationItem : items)
                        if (!allocationItem.isHasNewAllocItem() && allocationItem.getChoseStaffListAllocationItem() != null)
                            wrapperList.add(new IdentifiableWrapper(allocationItem.getChoseStaffListAllocationItem().getId(), allocationItem.getChoseStaffListAllocationItem().getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allocationItem.getChoseStaffListAllocationItem().getStaffRate()) + " - " + allocationItem.getChoseStaffListAllocationItem().getEmployeePost().getPostType().getShortTitle() +  " " + allocationItem.getChoseStaffListAllocationItem().getEmployeePost().getPostStatus().getShortTitle()));
                        else if (allocationItem.isHasNewAllocItem())
                        {
                            StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getEmployeePost().getOrgUnit(), model.getExtract().getPost(), rateItem.getFinancingSource(), rateItem.getFinancingSourceItem());
                            Double diff = 0.0d;
                            if (staffListItem != null)
                                diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();

                            List<StaffListAllocationItem> staffListAllocList = UniempDaoFacade.getStaffListDAO().getOccupiedStaffListAllocationItem(model.getEmployeePost().getOrgUnit(), model.getExtract().getPost(), rateItem.getFinancingSource(), rateItem.getFinancingSourceItem());
                            for (StaffListAllocationItem item : staffListAllocList)
                                if (!item.getEmployeePost().getPostStatus().isActive())
                                    diff -= item.getStaffRate();

                            if (diff > 0)
                                wrapperList.add(new IdentifiableWrapper(0L, "<Новая ставка> - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                        }

                idWrapperMap.put(rateItem.getId(), wrapperList);
            }
            if (idWrapperMap.size() > 0)
                ((BlockColumn) model.getStaffRateDataSource().getColumn(3)).setValueMap(idWrapperMap);

        }
    }

    /**
     * @return true, если нужно отображать поля связанные с должностью,<p/>
     * иначе false
     */
    protected boolean isShowPostFields(Model model)
    {
        return model.getExtract().getPost() != null && !model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL());
    }

    /**
     * Вспомогательные метод для конвертации принимаемого значения в Double.
     * @param value Long или Double
     * @return Если приходит Long, то ковертируется в Double, если приходит null, то возвращается 0.0d.
     */
    protected double getValue(Object value)
    {
        double val = 0.0d;
        if (null != value)
        {
            if(value instanceof Long)
                val = ((Long)value).doubleValue();
            else
                val = (Double) value;
        }

        return val;
    }

    /**
     * Проверяет и выстовляет доступность кнопки "Добавить выплаты, согласно штатному расписанию".
     * @return Возвращает доступные для добавления выплаты ШР, согласно ШР.
     */
    protected List<StaffListPostPayment> checkEnabledAddStaffListPaymentButton(IBusinessComponent component)
    {
        Model model = getModel(component);

        PostBoundedWithQGandQL post = model.getExtract().getPost() != null ? model.getExtract().getPost() : model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();
        List staffRateList = model.getExtract().getPost() != null && !model.getExtract().getPost().equals(model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL())
                ? model.getStaffRateList() : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());

        List<StaffListPostPayment> paymentList = CommonEmployeeExtractUtil.checkEnabledAddStaffListPaymentButton(post, model.getEmployeePost().getOrgUnit(), model.getPaymentList(), staffRateList);

        if (!paymentList.isEmpty())
            model.setAddStaffListPaymentsButtonVisible(true);
        else
            model.setAddStaffListPaymentsButtonVisible(false);

        return paymentList;
    }
}