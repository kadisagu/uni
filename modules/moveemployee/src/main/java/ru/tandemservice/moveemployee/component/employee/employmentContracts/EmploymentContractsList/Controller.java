/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.employee.employmentContracts.EmploymentContractsList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import ru.tandemservice.moveemployee.base.bo.MoveemployeeContract.ui.AddDismissalContractExtract.MoveemployeeContractAddDismissalContractExtract;
import ru.tandemservice.moveemployee.base.bo.MoveemployeeContract.ui.AddDismissalContractExtract.MoveemployeeContractAddDismissalContractExtractUI;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Create by ashaburov
 * Date 15.05.12
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public void onClickAddExtract(IBusinessComponent component)
    {
        ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsList.Model model = ((ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsList.Model) component.getModel("ru.tandemservice.uniemp.component.employee.employmentContracts.EmploymentContractsList"));
        Collection<IEntity> entityCollection = ((CheckboxColumn) model.getDataSource().getColumn(0)).getSelectedObjects();
        ErrorCollector errorCollector = component.getUserContext().getErrorCollector();

        if (entityCollection.isEmpty())
            errorCollector.add("Необходимо выбрать хотя бы одного сотрудника.");

        if (errorCollector.hasErrors())
            return;

        List<Long> contractIdList = new ArrayList<Long>();
        for (IEntity entity : entityCollection)
            contractIdList.add(entity.getId());

        component.createDefaultChildRegion(new ComponentActivator(MoveemployeeContractAddDismissalContractExtract.class.getSimpleName(), new ParametersMap().
                add(MoveemployeeContractAddDismissalContractExtractUI.CONTRACT_ID_LIST, contractIdList)));
    }
}
