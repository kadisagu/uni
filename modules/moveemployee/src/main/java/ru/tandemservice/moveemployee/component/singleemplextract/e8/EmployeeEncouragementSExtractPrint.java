/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.singleemplextract.e8;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 30.09.2011
 */
public class EmployeeEncouragementSExtractPrint implements IPrintFormCreator<EmployeeEncouragementSExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, EmployeeEncouragementSExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder)extract.getParagraph().getOrder());

        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);

        modifier.put("orderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCreateDate()));
        modifier.put("awardReason", extract.getEncouragementReason());

        PersonAcademicDegree degree = UniempDaoFacade.getUniempDAO().getEmployeeUpperAcademicDegree(extract.getEntity().getEmployee());
        if (degree != null)
            modifier.put("comma", ", ");
        else
            modifier.put("comma", "");

        Map<EncouragementType, String> printFormatMap = MoveEmployeeDaoFacade.getMoveEmployeeDao().getEncouragementTypePrintFormat(MoveEmployeeDaoFacade.getMoveEmployeeDao().getEncouragementTypeFromExtractList(extract));
        String awardTypeStr = "";
        int i = 1;
        for (Map.Entry<EncouragementType, String> entry : printFormatMap.entrySet())
        {
            awardTypeStr += (entry.getValue() != null ? entry.getValue() : entry.getKey().getTitle()) + (i == printFormatMap.entrySet().size() ? "" : ", ");
            i++;
        }
        modifier.put("awardType", awardTypeStr);
        if (extract.getAmount() != null)
        {
        modifier.put("sumStr", NumberSpellingUtil.spellNumberMasculineGender(extract.getAmount().longValue()));

        Double amount = extract.getAmount() - extract.getAmount().longValue() == 0 ? null : extract.getAmount() - extract.getAmount().longValue();
        BigDecimal fract = amount != null ? new BigDecimal(amount) : null;
        if (fract != null)
            fract = fract.setScale(2 ,BigDecimal.ROUND_HALF_UP);
        modifier.put("sumKop", fract != null ? String.valueOf((long) (fract.doubleValue() * 100)) : "00");

        modifier.put("sumRub", String.valueOf(extract.getAmount().longValue()));
        }
        else
        {
            modifier.put("sumStr", "");
            modifier.put("sumKop", "");
            modifier.put("sumRub", "");
        }

        if (extract.getFinancingSource() != null)
        {
            String finSrcStr = "Выплаты производить из " + (extract.getFinancingSource().getGenitiveCaseTitle() != null ? extract.getFinancingSource().getGenitiveCaseTitle() : extract.getFinancingSource().getTitle());
            if (extract.getFinancingSourceItem() != null)
                finSrcStr += " (" + (extract.getFinancingSourceItem().getGenitiveCaseTitle() != null ? extract.getFinancingSourceItem().getGenitiveCaseTitle() : extract.getFinancingSourceItem().getTitle()) + ")";
            finSrcStr += ".";
            modifier.put("financeSourceStr", finSrcStr);
        }
        else
            modifier.put("financeSourceStr", "");

        modifier.modify(document);
        return document;
    }
}