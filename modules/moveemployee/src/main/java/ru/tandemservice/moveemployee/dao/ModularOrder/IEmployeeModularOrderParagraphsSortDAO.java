/**
 *$Id$
 */
package ru.tandemservice.moveemployee.dao.ModularOrder;

import ru.tandemservice.moveemployee.entity.EmployeeModularOrder;

/**
 * @author Alexander Zhebko
 * @since 26.03.2013
 */
public interface IEmployeeModularOrderParagraphsSortDAO
{
    /**
     * Упорядочивает выписки сборного приказа
     * @param order - приказ, выписки которого нужно упорядочить
     */
    public void sortExtracts(EmployeeModularOrder order);
}