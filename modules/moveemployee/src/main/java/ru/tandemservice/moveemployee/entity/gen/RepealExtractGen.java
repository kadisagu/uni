package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.moveemployee.entity.RepealExtract;
import ru.tandemservice.uniemp.entity.catalog.EmployeeExtractGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Отмена приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RepealExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.RepealExtract";
    public static final String ENTITY_NAME = "repealExtract";
    public static final int VERSION_HASH = 283831078;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_EXTRACT_GROUP = "employeeExtractGroup";
    public static final String L_ABSTRACT_EMPLOYEE_EXTRACT = "abstractEmployeeExtract";

    private EmployeeExtractGroup _employeeExtractGroup;     // Группы приказов по движению кадрового состава
    private AbstractEmployeeExtract _abstractEmployeeExtract;     // Отменяемая выписка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группы приказов по движению кадрового состава. Свойство не может быть null.
     */
    @NotNull
    public EmployeeExtractGroup getEmployeeExtractGroup()
    {
        return _employeeExtractGroup;
    }

    /**
     * @param employeeExtractGroup Группы приказов по движению кадрового состава. Свойство не может быть null.
     */
    public void setEmployeeExtractGroup(EmployeeExtractGroup employeeExtractGroup)
    {
        dirty(_employeeExtractGroup, employeeExtractGroup);
        _employeeExtractGroup = employeeExtractGroup;
    }

    /**
     * @return Отменяемая выписка. Свойство не может быть null.
     */
    @NotNull
    public AbstractEmployeeExtract getAbstractEmployeeExtract()
    {
        return _abstractEmployeeExtract;
    }

    /**
     * @param abstractEmployeeExtract Отменяемая выписка. Свойство не может быть null.
     */
    public void setAbstractEmployeeExtract(AbstractEmployeeExtract abstractEmployeeExtract)
    {
        dirty(_abstractEmployeeExtract, abstractEmployeeExtract);
        _abstractEmployeeExtract = abstractEmployeeExtract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RepealExtractGen)
        {
            setEmployeeExtractGroup(((RepealExtract)another).getEmployeeExtractGroup());
            setAbstractEmployeeExtract(((RepealExtract)another).getAbstractEmployeeExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RepealExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RepealExtract.class;
        }

        public T newInstance()
        {
            return (T) new RepealExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeeExtractGroup":
                    return obj.getEmployeeExtractGroup();
                case "abstractEmployeeExtract":
                    return obj.getAbstractEmployeeExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeeExtractGroup":
                    obj.setEmployeeExtractGroup((EmployeeExtractGroup) value);
                    return;
                case "abstractEmployeeExtract":
                    obj.setAbstractEmployeeExtract((AbstractEmployeeExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeExtractGroup":
                        return true;
                case "abstractEmployeeExtract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeExtractGroup":
                    return true;
                case "abstractEmployeeExtract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeExtractGroup":
                    return EmployeeExtractGroup.class;
                case "abstractEmployeeExtract":
                    return AbstractEmployeeExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RepealExtract> _dslPath = new Path<RepealExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RepealExtract");
    }
            

    /**
     * @return Группы приказов по движению кадрового состава. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RepealExtract#getEmployeeExtractGroup()
     */
    public static EmployeeExtractGroup.Path<EmployeeExtractGroup> employeeExtractGroup()
    {
        return _dslPath.employeeExtractGroup();
    }

    /**
     * @return Отменяемая выписка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RepealExtract#getAbstractEmployeeExtract()
     */
    public static AbstractEmployeeExtract.Path<AbstractEmployeeExtract> abstractEmployeeExtract()
    {
        return _dslPath.abstractEmployeeExtract();
    }

    public static class Path<E extends RepealExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeeExtractGroup.Path<EmployeeExtractGroup> _employeeExtractGroup;
        private AbstractEmployeeExtract.Path<AbstractEmployeeExtract> _abstractEmployeeExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группы приказов по движению кадрового состава. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RepealExtract#getEmployeeExtractGroup()
     */
        public EmployeeExtractGroup.Path<EmployeeExtractGroup> employeeExtractGroup()
        {
            if(_employeeExtractGroup == null )
                _employeeExtractGroup = new EmployeeExtractGroup.Path<EmployeeExtractGroup>(L_EMPLOYEE_EXTRACT_GROUP, this);
            return _employeeExtractGroup;
        }

    /**
     * @return Отменяемая выписка. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.RepealExtract#getAbstractEmployeeExtract()
     */
        public AbstractEmployeeExtract.Path<AbstractEmployeeExtract> abstractEmployeeExtract()
        {
            if(_abstractEmployeeExtract == null )
                _abstractEmployeeExtract = new AbstractEmployeeExtract.Path<AbstractEmployeeExtract>(L_ABSTRACT_EMPLOYEE_EXTRACT, this);
            return _abstractEmployeeExtract;
        }

        public Class getEntityClass()
        {
            return RepealExtract.class;
        }

        public String getEntityName()
        {
            return "repealExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
