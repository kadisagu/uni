/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e25.Pub;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.ParagraphEmployeeListOrder;
import ru.tandemservice.moveemployee.entity.RepealExtract;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 11.01.2012
 */
public class DAO extends ModularEmployeeExtractPubDAO<RepealExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        AbstractEmployeeExtract extract = model.getExtract().getAbstractEmployeeExtract();
        StringBuilder extractBuilder = new StringBuilder();
        extractBuilder.append("Приказ №");
        extractBuilder.append(extract.getParagraph().getOrder().getNumber());
        extractBuilder.append(" от ");
        extractBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));
        extractBuilder.append(" «");
        extractBuilder.append(extract.getType().getTitle());
        extractBuilder.append("»");
        if (extract.getParagraph().getOrder() instanceof EmployeeListOrder)
        {
            extractBuilder.append(" пар.");
            extractBuilder.append(extract.getParagraph().getNumber());
            if (extract.getParagraph().getOrder() instanceof ParagraphEmployeeListOrder)
            {
                extractBuilder.append(" пункт ");
                extractBuilder.append(extract.getNumber());
            }
        }
        model.setExtractStr(extractBuilder.toString());
    }
}