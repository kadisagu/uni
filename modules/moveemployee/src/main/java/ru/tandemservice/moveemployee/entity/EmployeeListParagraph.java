package ru.tandemservice.moveemployee.entity;

import java.util.List;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.moveemployee.entity.gen.EmployeeListParagraphGen;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;

/**
 * Параграф списочного приказа по сотруднику
 */
public class EmployeeListParagraph extends EmployeeListParagraphGen
{
    public static final String P_NO_EDIT = "noEdit";       // нельзя редактировать

    public static final String P_TITLE = "title";

    public String getEditableTitle()
    {
        return (getOrder().getNumber() != null ? "№ " + getOrder().getNumber() : "") + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getOrder().getCommitDate());
    }
    
    public String getTitle()
    {
        return "Параграф №" + getNumber();
    }

    public static final String P_FIRST_EXTRACT = "firstExtract";

    public ListEmployeeExtract getFirstExtract()
    {
        List<ListEmployeeExtract> extractList = UniDaoFacade.getCoreDao().getList(ListEmployeeExtract.class, IAbstractExtract.L_PARAGRAPH, this);
        return extractList.size() > 0 ? extractList.get(0) : null;
    }

    public boolean isNoEdit()
    {
        return getOrder().isReadonly();
    }
}