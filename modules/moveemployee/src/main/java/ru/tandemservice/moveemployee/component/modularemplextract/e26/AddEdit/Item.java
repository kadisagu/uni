/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.modularemplextract.e26.AddEdit;

import java.util.Comparator;
import java.util.Date;

/**
 * Create by ashaburov
 * Date 17.02.12
 */
public class Item
{
    public static Comparator<Item> COMPARATOR = new Comparator<Item>()
    {
        @Override
        public int compare(Item o1, Item o2)
        {
            if (o1.getBeginPeriod() != null && o2.getBeginPeriod() != null && o1.getBeginPeriod().getTime() > o2.getBeginPeriod().getTime())
                return 1;
            else if (o1.getBeginPeriod() != null && o2.getBeginPeriod() != null && o1.getBeginPeriod().getTime() < o2.getBeginPeriod().getTime())
                return -1;
            else if (o1.getEndPeriod() != null && o2.getEndPeriod() != null && o1.getEndPeriod().getTime() > o2.getEndPeriod().getTime())
                return 1;
            else if (o1.getEndPeriod() != null && o2.getEndPeriod() != null && o1.getEndPeriod().getTime() < o2.getEndPeriod().getTime())
                return -1;
            else if (o1.getBeginHoliday() != null && o2.getBeginHoliday() != null && o1.getBeginHoliday().getTime() > o2.getBeginHoliday().getTime())
                return 1;
            else if (o1.getBeginHoliday() != null && o2.getBeginHoliday() != null && o1.getBeginHoliday().getTime() < o2.getBeginHoliday().getTime())
                return -1;
            else if (o1.getEndHoliday() != null && o2.getEndHoliday() != null && o1.getEndHoliday().getTime() > o2.getEndHoliday().getTime())
                return 1;
            else if (o1.getEndHoliday() != null && o2.getEndHoliday() != null && o1.getEndHoliday().getTime() < o2.getEndHoliday().getTime())
                return -1;
            else if (o1.getDuration() > o2.getDuration())
                return 1;
            else if (o1.getDuration() < o2.getDuration())
                return -1;
            else
                return 0;
        }
    };

    private Date _beginPeriod;
    private Date _endPeriod;

    private Date _beginHoliday;
    private Date _endHoliday;

    private int _duration;

    //Constructor

    public Item(Date beginPeriod, Date endPeriod, Date beginHoliday, Date endHoliday, int duration)
    {
        _beginPeriod = beginPeriod;
        _endPeriod = endPeriod;
        _beginHoliday = beginHoliday;
        _endHoliday = endHoliday;
        _duration = duration;
    }


    //Methods

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;

        Item o = (Item) obj;

        boolean beginPeriod = false;
        boolean endPeriod = false;
        boolean beginHoliday = false;
        boolean endHoliday = false;
        boolean duration = false;

        if (getBeginPeriod() == null && o.getBeginPeriod() == null)
            beginPeriod = true;
        else if (getBeginPeriod() != null && getBeginPeriod().equals(o.getBeginPeriod()))
            beginPeriod = true;

        if (getEndPeriod() == null && o.getEndPeriod() == null)
            endPeriod = true;
        else if (getEndPeriod() != null && getEndPeriod().equals(o.getEndPeriod()))
            endPeriod = true;

        if (getBeginHoliday() == null && o.getBeginHoliday() == null)
            beginHoliday = true;
        else if (getBeginHoliday() != null && getBeginHoliday().equals(o.getBeginHoliday()))
            beginHoliday = true;

        if (getEndHoliday() == null && o.getEndHoliday() == null)
            endHoliday = true;
        else if (getEndHoliday() != null && getEndHoliday().equals(o.getEndHoliday()))
            endHoliday = true;


        if (getDuration() == o.getDuration())
            duration = true;

        return beginPeriod && endPeriod && beginHoliday && endHoliday && duration;
    }


    //Getters & Setters

    public Date getBeginPeriod()
    {
        return _beginPeriod;
    }

    public void setBeginPeriod(Date beginPeriod)
    {
        _beginPeriod = beginPeriod;
    }

    public Date getEndPeriod()
    {
        return _endPeriod;
    }

    public void setEndPeriod(Date endPeriod)
    {
        _endPeriod = endPeriod;
    }

    public Date getBeginHoliday()
    {
        return _beginHoliday;
    }

    public void setBeginHoliday(Date beginHoliday)
    {
        _beginHoliday = beginHoliday;
    }

    public Date getEndHoliday()
    {
        return _endHoliday;
    }

    public void setEndHoliday(Date endHoliday)
    {
        _endHoliday = endHoliday;
    }

    public int getDuration()
    {
        return _duration;
    }

    public void setDuration(int duration)
    {
        _duration = duration;
    }
}
