/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.listemplextract.e3;

import org.hibernate.Session;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.listemplextract.IOrderParagraphListCustomizer;
import ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract;

/**
 * @author ListExtractComponentGenerator
 * @since 27.04.2011
 */
public class EmploymentDismissalEmplListExtractCustomizer implements IOrderParagraphListCustomizer
{
    @Override
    public void customizeParagraphList(DynamicListDataSource dataSource, IBusinessComponent component)
    {
        dataSource.addColumn(new SimpleColumn("Подразделение", new String[]{EmploymentDismissalEmplListExtract.L_EMPLOYEE_POST, EmployeePost.L_ORG_UNIT, OrgUnit.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Должность", new String[]{EmploymentDismissalEmplListExtract.L_EMPLOYEE_POST, EmployeePost.L_POST_RELATION, OrgUnitTypePostRelation.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата прекращения трудового договора", new String[]{EmploymentDismissalEmplListExtract.P_DISMISSAL_DATE }, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Основание прекращения трудового договора", new String[]{EmploymentDismissalEmplListExtract.P_CONTRACT_TERMINATION_BASIC}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Документ, номер, дата", new String[]{EmploymentDismissalEmplListExtract.P_BASICS_LIST_STR}).setClickable(false).setOrderable(false));
    }

    @Override
    public void wrap(DynamicListDataSource dataSource, Session session)
    {
    }
}