/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.moveemployee.component.menuempl.EmployeeOrdersJournal;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.moveemployee.component.commons.IMoveEmployeeMQBuilder;
import ru.tandemservice.moveemployee.component.commons.MoveEmployeeMQBuilder;
import ru.tandemservice.moveemployee.entity.*;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author alikhanov
 * @since 16.08.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setOrderCategoryModel(new StaticSelectModel("id", "title", getList(new DQLSelectBuilder()
                .fromEntity(EmployeeExtractType.class, "e")
                .order(property("e", EmployeeExtractType.code()))
                .where(isNull(property("e", EmployeeExtractType.parent()))))));
        model.setOrderTypeModel(new ExtractTypeSelectModel(model));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(Model model)
    {
        // init datasource
        IMoveEmployeeMQBuilder builder = MoveEmployeeMQBuilder.createOrderMQBuilder(AbstractEmployeeOrder.class, model.getSettings());

        // apply user defined restrictions
        builder.getMQBuilder().add(MQExpression.eq(IMoveEmployeeMQBuilder.STATE_ALIAS, ICatalogItem.CATALOG_ITEM_CODE, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));

        final List<EmployeeExtractType> orderCategory = model.getSettings().get("orderCategory");
        final List<EmployeeExtractType> extractTypes = model.getSettings().get("orderType");
        // Сортировка по типу выписки уже включает сортировку по категории
        if (CollectionUtils.isNotEmpty(extractTypes))
        {
            builder.getMQBuilder().add(MQExpression.exists(
                    new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "e")
                            .add(MQExpression.and(
                                    MQExpression.in("e", AbstractEmployeeExtract.type(), extractTypes),
                                    MQExpression.eqProperty("e", AbstractEmployeeExtract.paragraph().order().id().s(), IMoveEmployeeMQBuilder.ORDER_ALIAS,  AbstractEmployeeOrder.id().s())))));
        } else if (CollectionUtils.isNotEmpty(orderCategory))
        {
            builder.getMQBuilder().add(MQExpression.exists(new MQBuilder(AbstractEmployeeExtract.ENTITY_CLASS, "e")
                    .add(MQExpression.and(
                            MQExpression.or(
                                    MQExpression.in("e", AbstractEmployeeExtract.type().parent(), orderCategory),
                                    MQExpression.in("e", AbstractEmployeeExtract.type().parent().parent(), orderCategory)),
                            MQExpression.eqProperty("e", AbstractEmployeeExtract.paragraph().order().id().s(), IMoveEmployeeMQBuilder.ORDER_ALIAS,  AbstractEmployeeOrder.id().s()))
                    )
            ));
        }

        // apply filters
        builder.applyOrderCommitDateFromTo();
        builder.applyOrderCreateDateFromTo();
        builder.applyOrderNumber();
        builder.applyOrderCommitDateSystemFromTo();

        // apply order
        new OrderDescriptionRegistry(IMoveEmployeeMQBuilder.ORDER_ALIAS).applyOrder(builder.getMQBuilder(), model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder.getMQBuilder(), getSession());

        // get statictics for view properties
        String hql = "select " + IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER + ".id, count(*) from " + AbstractEmployeeExtract.ENTITY_CLASS + " group by " + IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER;
        Map<Long, Integer> id2count = new HashMap<>();
        for (Object[] row : (List<Object[]>) getSession().createQuery(hql).list())
        {
            Long id = (Long) row[0];
            Number count = (Number) row[1];
            id2count.put(id, count == null ? 0 : count.intValue());
        }

        // inject view properties
        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            Integer value = id2count.get(wrapper.getId());
            wrapper.setViewProperty(AbstractEmployeeOrder.P_EXTRACT_COUNT, value == null ? 0 : value);
            /* if (wrapper.getEntity() instanceof EmployeeSingleExtractOrder)
            {
                wrapper.setViewProperty(IAbstractOrder.P_CREATE_DATE, ((EmployeeSingleExtractOrder) wrapper.getEntity()).getExtract().getCreateDate());
            }        */
            wrapper.setViewProperty("disabledPrint", value == null);
        }
    }

}
