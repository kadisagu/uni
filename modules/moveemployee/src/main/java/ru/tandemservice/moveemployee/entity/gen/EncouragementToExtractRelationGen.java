package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmployeeEncouragementSExtract;
import ru.tandemservice.moveemployee.entity.EncouragementToExtractRelation;
import ru.tandemservice.uniemp.entity.catalog.EncouragementType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выбранных поощрений и наград в выписке «О поощрении работника» и самой выписки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EncouragementToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EncouragementToExtractRelation";
    public static final String ENTITY_NAME = "encouragementToExtractRelation";
    public static final int VERSION_HASH = 1171055366;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_ENCOURAGEMENT_S_EXTRACT = "employeeEncouragementSExtract";
    public static final String L_ENCOURAGEMENT_TYPE = "encouragementType";

    private EmployeeEncouragementSExtract _employeeEncouragementSExtract;     // Выписка из индивидуального приказа по кадровому составу. О поощрении работника
    private EncouragementType _encouragementType;     // Тип поощрения/награды

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из индивидуального приказа по кадровому составу. О поощрении работника. Свойство не может быть null.
     */
    @NotNull
    public EmployeeEncouragementSExtract getEmployeeEncouragementSExtract()
    {
        return _employeeEncouragementSExtract;
    }

    /**
     * @param employeeEncouragementSExtract Выписка из индивидуального приказа по кадровому составу. О поощрении работника. Свойство не может быть null.
     */
    public void setEmployeeEncouragementSExtract(EmployeeEncouragementSExtract employeeEncouragementSExtract)
    {
        dirty(_employeeEncouragementSExtract, employeeEncouragementSExtract);
        _employeeEncouragementSExtract = employeeEncouragementSExtract;
    }

    /**
     * @return Тип поощрения/награды. Свойство не может быть null.
     */
    @NotNull
    public EncouragementType getEncouragementType()
    {
        return _encouragementType;
    }

    /**
     * @param encouragementType Тип поощрения/награды. Свойство не может быть null.
     */
    public void setEncouragementType(EncouragementType encouragementType)
    {
        dirty(_encouragementType, encouragementType);
        _encouragementType = encouragementType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EncouragementToExtractRelationGen)
        {
            setEmployeeEncouragementSExtract(((EncouragementToExtractRelation)another).getEmployeeEncouragementSExtract());
            setEncouragementType(((EncouragementToExtractRelation)another).getEncouragementType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EncouragementToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EncouragementToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new EncouragementToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeEncouragementSExtract":
                    return obj.getEmployeeEncouragementSExtract();
                case "encouragementType":
                    return obj.getEncouragementType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeEncouragementSExtract":
                    obj.setEmployeeEncouragementSExtract((EmployeeEncouragementSExtract) value);
                    return;
                case "encouragementType":
                    obj.setEncouragementType((EncouragementType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeEncouragementSExtract":
                        return true;
                case "encouragementType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeEncouragementSExtract":
                    return true;
                case "encouragementType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeEncouragementSExtract":
                    return EmployeeEncouragementSExtract.class;
                case "encouragementType":
                    return EncouragementType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EncouragementToExtractRelation> _dslPath = new Path<EncouragementToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EncouragementToExtractRelation");
    }
            

    /**
     * @return Выписка из индивидуального приказа по кадровому составу. О поощрении работника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EncouragementToExtractRelation#getEmployeeEncouragementSExtract()
     */
    public static EmployeeEncouragementSExtract.Path<EmployeeEncouragementSExtract> employeeEncouragementSExtract()
    {
        return _dslPath.employeeEncouragementSExtract();
    }

    /**
     * @return Тип поощрения/награды. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EncouragementToExtractRelation#getEncouragementType()
     */
    public static EncouragementType.Path<EncouragementType> encouragementType()
    {
        return _dslPath.encouragementType();
    }

    public static class Path<E extends EncouragementToExtractRelation> extends EntityPath<E>
    {
        private EmployeeEncouragementSExtract.Path<EmployeeEncouragementSExtract> _employeeEncouragementSExtract;
        private EncouragementType.Path<EncouragementType> _encouragementType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из индивидуального приказа по кадровому составу. О поощрении работника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EncouragementToExtractRelation#getEmployeeEncouragementSExtract()
     */
        public EmployeeEncouragementSExtract.Path<EmployeeEncouragementSExtract> employeeEncouragementSExtract()
        {
            if(_employeeEncouragementSExtract == null )
                _employeeEncouragementSExtract = new EmployeeEncouragementSExtract.Path<EmployeeEncouragementSExtract>(L_EMPLOYEE_ENCOURAGEMENT_S_EXTRACT, this);
            return _employeeEncouragementSExtract;
        }

    /**
     * @return Тип поощрения/награды. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EncouragementToExtractRelation#getEncouragementType()
     */
        public EncouragementType.Path<EncouragementType> encouragementType()
        {
            if(_encouragementType == null )
                _encouragementType = new EncouragementType.Path<EncouragementType>(L_ENCOURAGEMENT_TYPE, this);
            return _encouragementType;
        }

        public Class getEntityClass()
        {
            return EncouragementToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "encouragementToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
