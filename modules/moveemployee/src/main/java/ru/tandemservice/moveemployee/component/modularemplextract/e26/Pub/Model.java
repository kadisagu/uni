/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.moveemployee.component.modularemplextract.e26.Pub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.modularemplextract.abstractextract.ModularEmployeeExtractPub.ModularEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.component.modularemplextract.e5.AddEdit.Row;
import ru.tandemservice.moveemployee.entity.ChangeHolidayExtract;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ModularEmployeeExtractComponentGenerator
 * @since 17.02.2012
 */
public class Model extends ModularEmployeeExtractPubModel<ChangeHolidayExtract>
{
    private String _attributesChangedExtractPage = getClass().getPackage().getName() + ".AttributesChangedExtractPage";

    private String _vacationScheduleStr;
    private String _vacationScheduleItemStr;

    public static String PERIOD = "period";
    public static String HOLIDAY = "holiday";

    private String _staffRateStr;

    private DynamicListDataSource _holidayDataSource;

    private Map<Row, Date> _beginPeriodMap = new HashMap<Row, Date>();
    private Map<Row, Date> _endPeriodMap = new HashMap<Row, Date>();
    private Map<Row, Date> _startHolidayMap = new HashMap<Row, Date>();
    private Map<Row, Date> _finishHolidayMap = new HashMap<Row, Date>();
    private Map<Row, Integer> _durationHolidayMap = new HashMap<Row, Integer>();

    //Getters & Setters

    public String getVacationScheduleStr()
    {
        return _vacationScheduleStr;
    }

    public void setVacationScheduleStr(String vacationScheduleStr)
    {
        _vacationScheduleStr = vacationScheduleStr;
    }

    public String getVacationScheduleItemStr()
    {
        return _vacationScheduleItemStr;
    }

    public void setVacationScheduleItemStr(String vacationScheduleItemStr)
    {
        _vacationScheduleItemStr = vacationScheduleItemStr;
    }

    public String getStaffRateStr()
    {
        return _staffRateStr;
    }

    public void setStaffRateStr(String staffRateStr)
    {
        _staffRateStr = staffRateStr;
    }

    public DynamicListDataSource getHolidayDataSource()
    {
        return _holidayDataSource;
    }

    public void setHolidayDataSource(DynamicListDataSource holidayDataSource)
    {
        _holidayDataSource = holidayDataSource;
    }

    public Map<Row, Date> getBeginPeriodMap()
    {
        return _beginPeriodMap;
    }

    public void setBeginPeriodMap(Map<Row, Date> beginPeriodMap)
    {
        _beginPeriodMap = beginPeriodMap;
    }

    public Map<Row, Date> getEndPeriodMap()
    {
        return _endPeriodMap;
    }

    public void setEndPeriodMap(Map<Row, Date> endPeriodMap)
    {
        _endPeriodMap = endPeriodMap;
    }

    public Map<Row, Date> getStartHolidayMap()
    {
        return _startHolidayMap;
    }

    public void setStartHolidayMap(Map<Row, Date> startHolidayMap)
    {
        _startHolidayMap = startHolidayMap;
    }

    public Map<Row, Date> getFinishHolidayMap()
    {
        return _finishHolidayMap;
    }

    public void setFinishHolidayMap(Map<Row, Date> finishHolidayMap)
    {
        _finishHolidayMap = finishHolidayMap;
    }

    public Map<Row, Integer> getDurationHolidayMap()
    {
        return _durationHolidayMap;
    }

    public void setDurationHolidayMap(Map<Row, Integer> durationHolidayMap)
    {
        _durationHolidayMap = durationHolidayMap;
    }

    public String getAttributesChangedExtractPage()
    {
        return _attributesChangedExtractPage;
    }

    public void setAttributesChangedExtractPage(String attributesChangedExtractPage)
    {
        _attributesChangedExtractPage = attributesChangedExtractPage;
    }
}