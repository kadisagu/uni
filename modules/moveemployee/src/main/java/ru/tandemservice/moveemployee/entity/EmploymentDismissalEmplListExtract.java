package ru.tandemservice.moveemployee.entity;

import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.gen.EmploymentDismissalEmplListExtractGen;

import java.util.List;

/**
 * Выписка из списочного приказа по кадровому составу. Об увольнении работников
 */
public class EmploymentDismissalEmplListExtract extends EmploymentDismissalEmplListExtractGen
{
    public static String P_CONTRACT_TERMINATION_BASIC = "contractTerminationBasic";
    public static String P_BASICS_LIST_STR = "basicListStr";

    public String getContractTerminationBasic()
    {
        return getEmployeeDismissReasons().getTitle() + ", " + getEmployeeDismissReasons().getLawItem();
    }

    @Override
    public String getBasicListStr()
    {
        StringBuilder result = new StringBuilder();
        List<EmpListExtractToBasicRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getListExtractBasicsList(this);
        for (EmpListExtractToBasicRelation relation : relationList)
        {
            result.append(relation.getBasic() != null ? relation.getBasic().getTitle() : "");
            result.append((relation.getComment() != null ? " " + relation.getComment() : "") );
            if (relationList.indexOf(relation) < relationList.size() - 1)
                result.append(", ");
        }
        return result.toString();
    }
}