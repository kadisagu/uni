package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeDismissReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из списочного приказа по кадровому составу. Об увольнении работников
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmploymentDismissalEmplListExtractGen extends ListEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract";
    public static final String ENTITY_NAME = "employmentDismissalEmplListExtract";
    public static final int VERSION_HASH = -1240309024;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_EMPLOYEE_DISMISS_REASONS = "employeeDismissReasons";
    public static final String P_DISMISSAL_DATE = "dismissalDate";
    public static final String P_NOT_FULFIL_DAY = "notFulfilDay";
    public static final String P_COMPENSATING_FOR_DAY = "compensatingForDay";
    public static final String L_EMPLOYEE_POST_STATUS_OLD = "employeePostStatusOld";
    public static final String P_DISMISSAL_DATE_OLD = "dismissalDateOld";

    private EmployeePost _employeePost;     // Сотрудник
    private EmployeeDismissReasons _employeeDismissReasons;     // Причина увольнения
    private Date _dismissalDate;     // Дата увольнения
    private double _notFulfilDay;     // Не отработано, дней
    private double _compensatingForDay;     // Компенсация за, дней
    private EmployeePostStatus _employeePostStatusOld;     // Статус на должности до проведения приказа
    private Date _dismissalDateOld;     // Дата увольнения до проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Причина увольнения. Свойство не может быть null.
     */
    @NotNull
    public EmployeeDismissReasons getEmployeeDismissReasons()
    {
        return _employeeDismissReasons;
    }

    /**
     * @param employeeDismissReasons Причина увольнения. Свойство не может быть null.
     */
    public void setEmployeeDismissReasons(EmployeeDismissReasons employeeDismissReasons)
    {
        dirty(_employeeDismissReasons, employeeDismissReasons);
        _employeeDismissReasons = employeeDismissReasons;
    }

    /**
     * @return Дата увольнения. Свойство не может быть null.
     */
    @NotNull
    public Date getDismissalDate()
    {
        return _dismissalDate;
    }

    /**
     * @param dismissalDate Дата увольнения. Свойство не может быть null.
     */
    public void setDismissalDate(Date dismissalDate)
    {
        dirty(_dismissalDate, dismissalDate);
        _dismissalDate = dismissalDate;
    }

    /**
     * @return Не отработано, дней. Свойство не может быть null.
     */
    @NotNull
    public double getNotFulfilDay()
    {
        return _notFulfilDay;
    }

    /**
     * @param notFulfilDay Не отработано, дней. Свойство не может быть null.
     */
    public void setNotFulfilDay(double notFulfilDay)
    {
        dirty(_notFulfilDay, notFulfilDay);
        _notFulfilDay = notFulfilDay;
    }

    /**
     * @return Компенсация за, дней. Свойство не может быть null.
     */
    @NotNull
    public double getCompensatingForDay()
    {
        return _compensatingForDay;
    }

    /**
     * @param compensatingForDay Компенсация за, дней. Свойство не может быть null.
     */
    public void setCompensatingForDay(double compensatingForDay)
    {
        dirty(_compensatingForDay, compensatingForDay);
        _compensatingForDay = compensatingForDay;
    }

    /**
     * @return Статус на должности до проведения приказа.
     */
    public EmployeePostStatus getEmployeePostStatusOld()
    {
        return _employeePostStatusOld;
    }

    /**
     * @param employeePostStatusOld Статус на должности до проведения приказа.
     */
    public void setEmployeePostStatusOld(EmployeePostStatus employeePostStatusOld)
    {
        dirty(_employeePostStatusOld, employeePostStatusOld);
        _employeePostStatusOld = employeePostStatusOld;
    }

    /**
     * @return Дата увольнения до проведения приказа.
     */
    public Date getDismissalDateOld()
    {
        return _dismissalDateOld;
    }

    /**
     * @param dismissalDateOld Дата увольнения до проведения приказа.
     */
    public void setDismissalDateOld(Date dismissalDateOld)
    {
        dirty(_dismissalDateOld, dismissalDateOld);
        _dismissalDateOld = dismissalDateOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmploymentDismissalEmplListExtractGen)
        {
            setEmployeePost(((EmploymentDismissalEmplListExtract)another).getEmployeePost());
            setEmployeeDismissReasons(((EmploymentDismissalEmplListExtract)another).getEmployeeDismissReasons());
            setDismissalDate(((EmploymentDismissalEmplListExtract)another).getDismissalDate());
            setNotFulfilDay(((EmploymentDismissalEmplListExtract)another).getNotFulfilDay());
            setCompensatingForDay(((EmploymentDismissalEmplListExtract)another).getCompensatingForDay());
            setEmployeePostStatusOld(((EmploymentDismissalEmplListExtract)another).getEmployeePostStatusOld());
            setDismissalDateOld(((EmploymentDismissalEmplListExtract)another).getDismissalDateOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmploymentDismissalEmplListExtractGen> extends ListEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmploymentDismissalEmplListExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmploymentDismissalEmplListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "employeeDismissReasons":
                    return obj.getEmployeeDismissReasons();
                case "dismissalDate":
                    return obj.getDismissalDate();
                case "notFulfilDay":
                    return obj.getNotFulfilDay();
                case "compensatingForDay":
                    return obj.getCompensatingForDay();
                case "employeePostStatusOld":
                    return obj.getEmployeePostStatusOld();
                case "dismissalDateOld":
                    return obj.getDismissalDateOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "employeeDismissReasons":
                    obj.setEmployeeDismissReasons((EmployeeDismissReasons) value);
                    return;
                case "dismissalDate":
                    obj.setDismissalDate((Date) value);
                    return;
                case "notFulfilDay":
                    obj.setNotFulfilDay((Double) value);
                    return;
                case "compensatingForDay":
                    obj.setCompensatingForDay((Double) value);
                    return;
                case "employeePostStatusOld":
                    obj.setEmployeePostStatusOld((EmployeePostStatus) value);
                    return;
                case "dismissalDateOld":
                    obj.setDismissalDateOld((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "employeeDismissReasons":
                        return true;
                case "dismissalDate":
                        return true;
                case "notFulfilDay":
                        return true;
                case "compensatingForDay":
                        return true;
                case "employeePostStatusOld":
                        return true;
                case "dismissalDateOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "employeeDismissReasons":
                    return true;
                case "dismissalDate":
                    return true;
                case "notFulfilDay":
                    return true;
                case "compensatingForDay":
                    return true;
                case "employeePostStatusOld":
                    return true;
                case "dismissalDateOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "employeeDismissReasons":
                    return EmployeeDismissReasons.class;
                case "dismissalDate":
                    return Date.class;
                case "notFulfilDay":
                    return Double.class;
                case "compensatingForDay":
                    return Double.class;
                case "employeePostStatusOld":
                    return EmployeePostStatus.class;
                case "dismissalDateOld":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmploymentDismissalEmplListExtract> _dslPath = new Path<EmploymentDismissalEmplListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmploymentDismissalEmplListExtract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Причина увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getEmployeeDismissReasons()
     */
    public static EmployeeDismissReasons.Path<EmployeeDismissReasons> employeeDismissReasons()
    {
        return _dslPath.employeeDismissReasons();
    }

    /**
     * @return Дата увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getDismissalDate()
     */
    public static PropertyPath<Date> dismissalDate()
    {
        return _dslPath.dismissalDate();
    }

    /**
     * @return Не отработано, дней. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getNotFulfilDay()
     */
    public static PropertyPath<Double> notFulfilDay()
    {
        return _dslPath.notFulfilDay();
    }

    /**
     * @return Компенсация за, дней. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getCompensatingForDay()
     */
    public static PropertyPath<Double> compensatingForDay()
    {
        return _dslPath.compensatingForDay();
    }

    /**
     * @return Статус на должности до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getEmployeePostStatusOld()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusOld()
    {
        return _dslPath.employeePostStatusOld();
    }

    /**
     * @return Дата увольнения до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getDismissalDateOld()
     */
    public static PropertyPath<Date> dismissalDateOld()
    {
        return _dslPath.dismissalDateOld();
    }

    public static class Path<E extends EmploymentDismissalEmplListExtract> extends ListEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private EmployeeDismissReasons.Path<EmployeeDismissReasons> _employeeDismissReasons;
        private PropertyPath<Date> _dismissalDate;
        private PropertyPath<Double> _notFulfilDay;
        private PropertyPath<Double> _compensatingForDay;
        private EmployeePostStatus.Path<EmployeePostStatus> _employeePostStatusOld;
        private PropertyPath<Date> _dismissalDateOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Причина увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getEmployeeDismissReasons()
     */
        public EmployeeDismissReasons.Path<EmployeeDismissReasons> employeeDismissReasons()
        {
            if(_employeeDismissReasons == null )
                _employeeDismissReasons = new EmployeeDismissReasons.Path<EmployeeDismissReasons>(L_EMPLOYEE_DISMISS_REASONS, this);
            return _employeeDismissReasons;
        }

    /**
     * @return Дата увольнения. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getDismissalDate()
     */
        public PropertyPath<Date> dismissalDate()
        {
            if(_dismissalDate == null )
                _dismissalDate = new PropertyPath<Date>(EmploymentDismissalEmplListExtractGen.P_DISMISSAL_DATE, this);
            return _dismissalDate;
        }

    /**
     * @return Не отработано, дней. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getNotFulfilDay()
     */
        public PropertyPath<Double> notFulfilDay()
        {
            if(_notFulfilDay == null )
                _notFulfilDay = new PropertyPath<Double>(EmploymentDismissalEmplListExtractGen.P_NOT_FULFIL_DAY, this);
            return _notFulfilDay;
        }

    /**
     * @return Компенсация за, дней. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getCompensatingForDay()
     */
        public PropertyPath<Double> compensatingForDay()
        {
            if(_compensatingForDay == null )
                _compensatingForDay = new PropertyPath<Double>(EmploymentDismissalEmplListExtractGen.P_COMPENSATING_FOR_DAY, this);
            return _compensatingForDay;
        }

    /**
     * @return Статус на должности до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getEmployeePostStatusOld()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> employeePostStatusOld()
        {
            if(_employeePostStatusOld == null )
                _employeePostStatusOld = new EmployeePostStatus.Path<EmployeePostStatus>(L_EMPLOYEE_POST_STATUS_OLD, this);
            return _employeePostStatusOld;
        }

    /**
     * @return Дата увольнения до проведения приказа.
     * @see ru.tandemservice.moveemployee.entity.EmploymentDismissalEmplListExtract#getDismissalDateOld()
     */
        public PropertyPath<Date> dismissalDateOld()
        {
            if(_dismissalDateOld == null )
                _dismissalDateOld = new PropertyPath<Date>(EmploymentDismissalEmplListExtractGen.P_DISMISSAL_DATE_OLD, this);
            return _dismissalDateOld;
        }

        public Class getEntityClass()
        {
            return EmploymentDismissalEmplListExtract.class;
        }

        public String getEntityName()
        {
            return "employmentDismissalEmplListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
