/**
 *$Id$
 */
package ru.tandemservice.moveemployee.component.listemplextract.e7;

import ru.tandemservice.moveemployee.entity.PayForHolidayDayEmplListExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * Create by ashaburov
 * Date 08.02.12
 */
public class PayForHolidayDayEmplListExtractDao extends UniBaseDao implements IExtractComponentDao<PayForHolidayDayEmplListExtract>
{
    @Override
    public void doCommit(PayForHolidayDayEmplListExtract extract, Map parameters)
    {

    }

    @Override
    public void doRollback(PayForHolidayDayEmplListExtract extract, Map parameters)
    {

    }
}
