package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeOrder;
import ru.tandemservice.moveemployee.entity.EmployeeListOrder;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeExtractType;
import ru.tandemservice.moveemployee.entity.catalog.EmployeeOrderReasons;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списочный приказ по сотрудникам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeListOrderGen extends AbstractEmployeeOrder
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeListOrder";
    public static final String ENTITY_NAME = "employeeListOrder";
    public static final int VERSION_HASH = 1951241029;
    private static IEntityMeta ENTITY_META;

    public static final String L_REASON = "reason";
    public static final String L_TYPE = "type";

    private EmployeeOrderReasons _reason;     // Причина списочного приказа
    private EmployeeExtractType _type;     // Тип приказа. Второй уровень иерархии справочника

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причина списочного приказа.
     */
    public EmployeeOrderReasons getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина списочного приказа.
     */
    public void setReason(EmployeeOrderReasons reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Тип приказа. Второй уровень иерархии справочника. Свойство не может быть null.
     */
    @NotNull
    public EmployeeExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип приказа. Второй уровень иерархии справочника. Свойство не может быть null.
     */
    public void setType(EmployeeExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeListOrderGen)
        {
            setReason(((EmployeeListOrder)another).getReason());
            setType(((EmployeeListOrder)another).getType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeListOrderGen> extends AbstractEmployeeOrder.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeListOrder.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeListOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return obj.getReason();
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "reason":
                    obj.setReason((EmployeeOrderReasons) value);
                    return;
                case "type":
                    obj.setType((EmployeeExtractType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                        return true;
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return true;
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "reason":
                    return EmployeeOrderReasons.class;
                case "type":
                    return EmployeeExtractType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeListOrder> _dslPath = new Path<EmployeeListOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeListOrder");
    }
            

    /**
     * @return Причина списочного приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeListOrder#getReason()
     */
    public static EmployeeOrderReasons.Path<EmployeeOrderReasons> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Тип приказа. Второй уровень иерархии справочника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeListOrder#getType()
     */
    public static EmployeeExtractType.Path<EmployeeExtractType> type()
    {
        return _dslPath.type();
    }

    public static class Path<E extends EmployeeListOrder> extends AbstractEmployeeOrder.Path<E>
    {
        private EmployeeOrderReasons.Path<EmployeeOrderReasons> _reason;
        private EmployeeExtractType.Path<EmployeeExtractType> _type;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причина списочного приказа.
     * @see ru.tandemservice.moveemployee.entity.EmployeeListOrder#getReason()
     */
        public EmployeeOrderReasons.Path<EmployeeOrderReasons> reason()
        {
            if(_reason == null )
                _reason = new EmployeeOrderReasons.Path<EmployeeOrderReasons>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Тип приказа. Второй уровень иерархии справочника. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeListOrder#getType()
     */
        public EmployeeExtractType.Path<EmployeeExtractType> type()
        {
            if(_type == null )
                _type = new EmployeeExtractType.Path<EmployeeExtractType>(L_TYPE, this);
            return _type;
        }

        public Class getEntityClass()
        {
            return EmployeeListOrder.class;
        }

        public String getEntityName()
        {
            return "employeeListOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
