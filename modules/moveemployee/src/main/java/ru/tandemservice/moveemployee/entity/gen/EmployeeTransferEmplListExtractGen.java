package ru.tandemservice.moveemployee.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWeekWorkLoad;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract;
import ru.tandemservice.moveemployee.entity.ListEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из списочного приказа по кадровому составу. О переводе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EmployeeTransferEmplListExtractGen extends ListEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract";
    public static final String ENTITY_NAME = "employeeTransferEmplListExtract";
    public static final int VERSION_HASH = -785797251;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_COMPETITION_TYPE = "competitionType";
    public static final String L_POST_TYPE = "postType";
    public static final String L_WEEK_WORK_LOAD = "weekWorkLoad";
    public static final String L_WORK_WEEK_DURATION = "workWeekDuration";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String P_SALARY = "salary";
    public static final String P_HOURLY_PAID = "hourlyPaid";
    public static final String P_OPTIONAL_CONDITION = "optionalCondition";
    public static final String L_LABOUR_CONTRACT_TYPE = "labourContractType";
    public static final String P_LABOUR_CONTRACT_NUMBER = "labourContractNumber";
    public static final String P_LABOUR_CONTRACT_DATE = "labourContractDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_COLLATERAL_AGREEMENT_NUMBER = "collateralAgreementNumber";
    public static final String P_TRANSFER_DATE = "transferDate";
    public static final String L_OLD_EMPLOYEE_POST_STATUS = "oldEmployeePostStatus";
    public static final String P_FREELANCE = "freelance";

    private EmployeePost _employeePost;     // Сотрудник, который переводится на новую должность
    private OrgUnit _orgUnit;     // Принимающее подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private CompetitionAssignmentType _competitionType;     // Типы конкурсного назначения на должность
    private PostType _postType;     // Тип назначения на должность
    private EmployeeWeekWorkLoad _weekWorkLoad;     // Продолжительность рабочего времени
    private EmployeeWorkWeekDuration _workWeekDuration;     // Продолжительность трудовой недели
    private EtksLevels _etksLevels;     // Разряд ЕТКС
    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private double _salary;     // Сумма оплаты
    private boolean _hourlyPaid;     // Почасовая оплата
    private String _optionalCondition;     // Произвольная строка условий перевода
    private LabourContractType _labourContractType;     // Тип трудового договора
    private String _labourContractNumber;     // Номер трудового договора
    private Date _labourContractDate;     // Дата трудового договора
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private String _collateralAgreementNumber;     // Номер доп. соглашения
    private Date _transferDate;     // Дата доп. соглашения
    private EmployeePostStatus _oldEmployeePostStatus;     // Статус на должности до перевода
    private boolean _freelance;     // Вне штата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник, который переводится на новую должность. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник, который переводится на новую должность. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Принимающее подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Типы конкурсного назначения на должность.
     */
    public CompetitionAssignmentType getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Типы конкурсного назначения на должность.
     */
    public void setCompetitionType(CompetitionAssignmentType competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     */
    @NotNull
    public PostType getPostType()
    {
        return _postType;
    }

    /**
     * @param postType Тип назначения на должность. Свойство не может быть null.
     */
    public void setPostType(PostType postType)
    {
        dirty(_postType, postType);
        _postType = postType;
    }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     */
    @NotNull
    public EmployeeWeekWorkLoad getWeekWorkLoad()
    {
        return _weekWorkLoad;
    }

    /**
     * @param weekWorkLoad Продолжительность рабочего времени. Свойство не может быть null.
     */
    public void setWeekWorkLoad(EmployeeWeekWorkLoad weekWorkLoad)
    {
        dirty(_weekWorkLoad, weekWorkLoad);
        _weekWorkLoad = weekWorkLoad;
    }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     */
    @NotNull
    public EmployeeWorkWeekDuration getWorkWeekDuration()
    {
        return _workWeekDuration;
    }

    /**
     * @param workWeekDuration Продолжительность трудовой недели. Свойство не может быть null.
     */
    public void setWorkWeekDuration(EmployeeWorkWeekDuration workWeekDuration)
    {
        dirty(_workWeekDuration, workWeekDuration);
        _workWeekDuration = workWeekDuration;
    }

    /**
     * @return Разряд ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Разряд ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Почасовая оплата. Свойство не может быть null.
     */
    @NotNull
    public boolean isHourlyPaid()
    {
        return _hourlyPaid;
    }

    /**
     * @param hourlyPaid Почасовая оплата. Свойство не может быть null.
     */
    public void setHourlyPaid(boolean hourlyPaid)
    {
        dirty(_hourlyPaid, hourlyPaid);
        _hourlyPaid = hourlyPaid;
    }

    /**
     * @return Произвольная строка условий перевода.
     */
    @Length(max=255)
    public String getOptionalCondition()
    {
        return _optionalCondition;
    }

    /**
     * @param optionalCondition Произвольная строка условий перевода.
     */
    public void setOptionalCondition(String optionalCondition)
    {
        dirty(_optionalCondition, optionalCondition);
        _optionalCondition = optionalCondition;
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     */
    @NotNull
    public LabourContractType getLabourContractType()
    {
        return _labourContractType;
    }

    /**
     * @param labourContractType Тип трудового договора. Свойство не может быть null.
     */
    public void setLabourContractType(LabourContractType labourContractType)
    {
        dirty(_labourContractType, labourContractType);
        _labourContractType = labourContractType;
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLabourContractNumber()
    {
        return _labourContractNumber;
    }

    /**
     * @param labourContractNumber Номер трудового договора. Свойство не может быть null.
     */
    public void setLabourContractNumber(String labourContractNumber)
    {
        dirty(_labourContractNumber, labourContractNumber);
        _labourContractNumber = labourContractNumber;
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     */
    @NotNull
    public Date getLabourContractDate()
    {
        return _labourContractDate;
    }

    /**
     * @param labourContractDate Дата трудового договора. Свойство не может быть null.
     */
    public void setLabourContractDate(Date labourContractDate)
    {
        dirty(_labourContractDate, labourContractDate);
        _labourContractDate = labourContractDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Номер доп. соглашения.
     */
    @Length(max=255)
    public String getCollateralAgreementNumber()
    {
        return _collateralAgreementNumber;
    }

    /**
     * @param collateralAgreementNumber Номер доп. соглашения.
     */
    public void setCollateralAgreementNumber(String collateralAgreementNumber)
    {
        dirty(_collateralAgreementNumber, collateralAgreementNumber);
        _collateralAgreementNumber = collateralAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения.
     */
    public Date getTransferDate()
    {
        return _transferDate;
    }

    /**
     * @param transferDate Дата доп. соглашения.
     */
    public void setTransferDate(Date transferDate)
    {
        dirty(_transferDate, transferDate);
        _transferDate = transferDate;
    }

    /**
     * @return Статус на должности до перевода.
     */
    public EmployeePostStatus getOldEmployeePostStatus()
    {
        return _oldEmployeePostStatus;
    }

    /**
     * @param oldEmployeePostStatus Статус на должности до перевода.
     */
    public void setOldEmployeePostStatus(EmployeePostStatus oldEmployeePostStatus)
    {
        dirty(_oldEmployeePostStatus, oldEmployeePostStatus);
        _oldEmployeePostStatus = oldEmployeePostStatus;
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreelance()
    {
        return _freelance;
    }

    /**
     * @param freelance Вне штата. Свойство не может быть null.
     */
    public void setFreelance(boolean freelance)
    {
        dirty(_freelance, freelance);
        _freelance = freelance;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EmployeeTransferEmplListExtractGen)
        {
            setEmployeePost(((EmployeeTransferEmplListExtract)another).getEmployeePost());
            setOrgUnit(((EmployeeTransferEmplListExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((EmployeeTransferEmplListExtract)another).getPostBoundedWithQGandQL());
            setCompetitionType(((EmployeeTransferEmplListExtract)another).getCompetitionType());
            setPostType(((EmployeeTransferEmplListExtract)another).getPostType());
            setWeekWorkLoad(((EmployeeTransferEmplListExtract)another).getWeekWorkLoad());
            setWorkWeekDuration(((EmployeeTransferEmplListExtract)another).getWorkWeekDuration());
            setEtksLevels(((EmployeeTransferEmplListExtract)another).getEtksLevels());
            setRaisingCoefficient(((EmployeeTransferEmplListExtract)another).getRaisingCoefficient());
            setSalary(((EmployeeTransferEmplListExtract)another).getSalary());
            setHourlyPaid(((EmployeeTransferEmplListExtract)another).isHourlyPaid());
            setOptionalCondition(((EmployeeTransferEmplListExtract)another).getOptionalCondition());
            setLabourContractType(((EmployeeTransferEmplListExtract)another).getLabourContractType());
            setLabourContractNumber(((EmployeeTransferEmplListExtract)another).getLabourContractNumber());
            setLabourContractDate(((EmployeeTransferEmplListExtract)another).getLabourContractDate());
            setBeginDate(((EmployeeTransferEmplListExtract)another).getBeginDate());
            setEndDate(((EmployeeTransferEmplListExtract)another).getEndDate());
            setCollateralAgreementNumber(((EmployeeTransferEmplListExtract)another).getCollateralAgreementNumber());
            setTransferDate(((EmployeeTransferEmplListExtract)another).getTransferDate());
            setOldEmployeePostStatus(((EmployeeTransferEmplListExtract)another).getOldEmployeePostStatus());
            setFreelance(((EmployeeTransferEmplListExtract)another).isFreelance());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EmployeeTransferEmplListExtractGen> extends ListEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EmployeeTransferEmplListExtract.class;
        }

        public T newInstance()
        {
            return (T) new EmployeeTransferEmplListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "competitionType":
                    return obj.getCompetitionType();
                case "postType":
                    return obj.getPostType();
                case "weekWorkLoad":
                    return obj.getWeekWorkLoad();
                case "workWeekDuration":
                    return obj.getWorkWeekDuration();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "salary":
                    return obj.getSalary();
                case "hourlyPaid":
                    return obj.isHourlyPaid();
                case "optionalCondition":
                    return obj.getOptionalCondition();
                case "labourContractType":
                    return obj.getLabourContractType();
                case "labourContractNumber":
                    return obj.getLabourContractNumber();
                case "labourContractDate":
                    return obj.getLabourContractDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "collateralAgreementNumber":
                    return obj.getCollateralAgreementNumber();
                case "transferDate":
                    return obj.getTransferDate();
                case "oldEmployeePostStatus":
                    return obj.getOldEmployeePostStatus();
                case "freelance":
                    return obj.isFreelance();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((CompetitionAssignmentType) value);
                    return;
                case "postType":
                    obj.setPostType((PostType) value);
                    return;
                case "weekWorkLoad":
                    obj.setWeekWorkLoad((EmployeeWeekWorkLoad) value);
                    return;
                case "workWeekDuration":
                    obj.setWorkWeekDuration((EmployeeWorkWeekDuration) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "hourlyPaid":
                    obj.setHourlyPaid((Boolean) value);
                    return;
                case "optionalCondition":
                    obj.setOptionalCondition((String) value);
                    return;
                case "labourContractType":
                    obj.setLabourContractType((LabourContractType) value);
                    return;
                case "labourContractNumber":
                    obj.setLabourContractNumber((String) value);
                    return;
                case "labourContractDate":
                    obj.setLabourContractDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "collateralAgreementNumber":
                    obj.setCollateralAgreementNumber((String) value);
                    return;
                case "transferDate":
                    obj.setTransferDate((Date) value);
                    return;
                case "oldEmployeePostStatus":
                    obj.setOldEmployeePostStatus((EmployeePostStatus) value);
                    return;
                case "freelance":
                    obj.setFreelance((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "competitionType":
                        return true;
                case "postType":
                        return true;
                case "weekWorkLoad":
                        return true;
                case "workWeekDuration":
                        return true;
                case "etksLevels":
                        return true;
                case "raisingCoefficient":
                        return true;
                case "salary":
                        return true;
                case "hourlyPaid":
                        return true;
                case "optionalCondition":
                        return true;
                case "labourContractType":
                        return true;
                case "labourContractNumber":
                        return true;
                case "labourContractDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "collateralAgreementNumber":
                        return true;
                case "transferDate":
                        return true;
                case "oldEmployeePostStatus":
                        return true;
                case "freelance":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "competitionType":
                    return true;
                case "postType":
                    return true;
                case "weekWorkLoad":
                    return true;
                case "workWeekDuration":
                    return true;
                case "etksLevels":
                    return true;
                case "raisingCoefficient":
                    return true;
                case "salary":
                    return true;
                case "hourlyPaid":
                    return true;
                case "optionalCondition":
                    return true;
                case "labourContractType":
                    return true;
                case "labourContractNumber":
                    return true;
                case "labourContractDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "collateralAgreementNumber":
                    return true;
                case "transferDate":
                    return true;
                case "oldEmployeePostStatus":
                    return true;
                case "freelance":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "competitionType":
                    return CompetitionAssignmentType.class;
                case "postType":
                    return PostType.class;
                case "weekWorkLoad":
                    return EmployeeWeekWorkLoad.class;
                case "workWeekDuration":
                    return EmployeeWorkWeekDuration.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "salary":
                    return Double.class;
                case "hourlyPaid":
                    return Boolean.class;
                case "optionalCondition":
                    return String.class;
                case "labourContractType":
                    return LabourContractType.class;
                case "labourContractNumber":
                    return String.class;
                case "labourContractDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "collateralAgreementNumber":
                    return String.class;
                case "transferDate":
                    return Date.class;
                case "oldEmployeePostStatus":
                    return EmployeePostStatus.class;
                case "freelance":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EmployeeTransferEmplListExtract> _dslPath = new Path<EmployeeTransferEmplListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EmployeeTransferEmplListExtract");
    }
            

    /**
     * @return Сотрудник, который переводится на новую должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Типы конкурсного назначения на должность.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getCompetitionType()
     */
    public static CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getPostType()
     */
    public static PostType.Path<PostType> postType()
    {
        return _dslPath.postType();
    }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getWeekWorkLoad()
     */
    public static EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> weekWorkLoad()
    {
        return _dslPath.weekWorkLoad();
    }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getWorkWeekDuration()
     */
    public static EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> workWeekDuration()
    {
        return _dslPath.workWeekDuration();
    }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Почасовая оплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#isHourlyPaid()
     */
    public static PropertyPath<Boolean> hourlyPaid()
    {
        return _dslPath.hourlyPaid();
    }

    /**
     * @return Произвольная строка условий перевода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getOptionalCondition()
     */
    public static PropertyPath<String> optionalCondition()
    {
        return _dslPath.optionalCondition();
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getLabourContractType()
     */
    public static LabourContractType.Path<LabourContractType> labourContractType()
    {
        return _dslPath.labourContractType();
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getLabourContractNumber()
     */
    public static PropertyPath<String> labourContractNumber()
    {
        return _dslPath.labourContractNumber();
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getLabourContractDate()
     */
    public static PropertyPath<Date> labourContractDate()
    {
        return _dslPath.labourContractDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Номер доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getCollateralAgreementNumber()
     */
    public static PropertyPath<String> collateralAgreementNumber()
    {
        return _dslPath.collateralAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getTransferDate()
     */
    public static PropertyPath<Date> transferDate()
    {
        return _dslPath.transferDate();
    }

    /**
     * @return Статус на должности до перевода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getOldEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
    {
        return _dslPath.oldEmployeePostStatus();
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#isFreelance()
     */
    public static PropertyPath<Boolean> freelance()
    {
        return _dslPath.freelance();
    }

    public static class Path<E extends EmployeeTransferEmplListExtract> extends ListEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private CompetitionAssignmentType.Path<CompetitionAssignmentType> _competitionType;
        private PostType.Path<PostType> _postType;
        private EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> _weekWorkLoad;
        private EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> _workWeekDuration;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private PropertyPath<Double> _salary;
        private PropertyPath<Boolean> _hourlyPaid;
        private PropertyPath<String> _optionalCondition;
        private LabourContractType.Path<LabourContractType> _labourContractType;
        private PropertyPath<String> _labourContractNumber;
        private PropertyPath<Date> _labourContractDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _collateralAgreementNumber;
        private PropertyPath<Date> _transferDate;
        private EmployeePostStatus.Path<EmployeePostStatus> _oldEmployeePostStatus;
        private PropertyPath<Boolean> _freelance;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник, который переводится на новую должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Типы конкурсного назначения на должность.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getCompetitionType()
     */
        public CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new CompetitionAssignmentType.Path<CompetitionAssignmentType>(L_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Тип назначения на должность. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getPostType()
     */
        public PostType.Path<PostType> postType()
        {
            if(_postType == null )
                _postType = new PostType.Path<PostType>(L_POST_TYPE, this);
            return _postType;
        }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getWeekWorkLoad()
     */
        public EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> weekWorkLoad()
        {
            if(_weekWorkLoad == null )
                _weekWorkLoad = new EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad>(L_WEEK_WORK_LOAD, this);
            return _weekWorkLoad;
        }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getWorkWeekDuration()
     */
        public EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> workWeekDuration()
        {
            if(_workWeekDuration == null )
                _workWeekDuration = new EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration>(L_WORK_WEEK_DURATION, this);
            return _workWeekDuration;
        }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(EmployeeTransferEmplListExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Почасовая оплата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#isHourlyPaid()
     */
        public PropertyPath<Boolean> hourlyPaid()
        {
            if(_hourlyPaid == null )
                _hourlyPaid = new PropertyPath<Boolean>(EmployeeTransferEmplListExtractGen.P_HOURLY_PAID, this);
            return _hourlyPaid;
        }

    /**
     * @return Произвольная строка условий перевода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getOptionalCondition()
     */
        public PropertyPath<String> optionalCondition()
        {
            if(_optionalCondition == null )
                _optionalCondition = new PropertyPath<String>(EmployeeTransferEmplListExtractGen.P_OPTIONAL_CONDITION, this);
            return _optionalCondition;
        }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getLabourContractType()
     */
        public LabourContractType.Path<LabourContractType> labourContractType()
        {
            if(_labourContractType == null )
                _labourContractType = new LabourContractType.Path<LabourContractType>(L_LABOUR_CONTRACT_TYPE, this);
            return _labourContractType;
        }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getLabourContractNumber()
     */
        public PropertyPath<String> labourContractNumber()
        {
            if(_labourContractNumber == null )
                _labourContractNumber = new PropertyPath<String>(EmployeeTransferEmplListExtractGen.P_LABOUR_CONTRACT_NUMBER, this);
            return _labourContractNumber;
        }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getLabourContractDate()
     */
        public PropertyPath<Date> labourContractDate()
        {
            if(_labourContractDate == null )
                _labourContractDate = new PropertyPath<Date>(EmployeeTransferEmplListExtractGen.P_LABOUR_CONTRACT_DATE, this);
            return _labourContractDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EmployeeTransferEmplListExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(EmployeeTransferEmplListExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Номер доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getCollateralAgreementNumber()
     */
        public PropertyPath<String> collateralAgreementNumber()
        {
            if(_collateralAgreementNumber == null )
                _collateralAgreementNumber = new PropertyPath<String>(EmployeeTransferEmplListExtractGen.P_COLLATERAL_AGREEMENT_NUMBER, this);
            return _collateralAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getTransferDate()
     */
        public PropertyPath<Date> transferDate()
        {
            if(_transferDate == null )
                _transferDate = new PropertyPath<Date>(EmployeeTransferEmplListExtractGen.P_TRANSFER_DATE, this);
            return _transferDate;
        }

    /**
     * @return Статус на должности до перевода.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#getOldEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
        {
            if(_oldEmployeePostStatus == null )
                _oldEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_OLD_EMPLOYEE_POST_STATUS, this);
            return _oldEmployeePostStatus;
        }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.moveemployee.entity.EmployeeTransferEmplListExtract#isFreelance()
     */
        public PropertyPath<Boolean> freelance()
        {
            if(_freelance == null )
                _freelance = new PropertyPath<Boolean>(EmployeeTransferEmplListExtractGen.P_FREELANCE, this);
            return _freelance;
        }

        public Class getEntityClass()
        {
            return EmployeeTransferEmplListExtract.class;
        }

        public String getEntityName()
        {
            return "employeeTransferEmplListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
