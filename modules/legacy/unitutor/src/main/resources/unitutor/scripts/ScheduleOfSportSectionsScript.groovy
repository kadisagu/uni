/* $Id$ */
package unitutor.scripts

import org.apache.commons.collections.CollectionUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.MergeType
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.person.catalog.entity.Sex
import ru.tandemservice.uni.entity.catalog.Course
import ru.tandemservice.uni.util.FilterUtils
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace
import ru.tandemservice.unitutor.entity.SportSection
import ru.tandemservice.unitutor.entity.SportSection2CourseRel
import ru.tandemservice.unitutor.entity.SportSection2SexRel
import ru.tandemservice.unitutor.entity.catalog.codes.SportSectionTypeCodes

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists
import static org.tandemframework.hibsupport.dql.DQLExpressions.property

return new ScheduleOfSportSections(
        session: session,                           //Сессия
        template: template,                         //Шаблон
        educationYearId: educationYearId,           //Учебный год
        formativeOrgUnitIds: formativeOrgUnitIds,   //Формирующее подразделение
        courseIds: courseIds,                       //Курс
        genderId: genderId,                         //Пол
        placeIds: placeIds,                         //Спортивный объект
        withNumbers: withNumbers                    //Выводить количество свободных мест
).print()

/**
 * @author Andrey Andreev
 * @since 26.08.2016
 */
class ScheduleOfSportSections
{
    private Session session;
    private byte[] template;
    private Long educationYearId;
    private List<Long> formativeOrgUnitIds;
    private List<Long> courseIds;
    private Long genderId;
    private List<Long> placeIds;
    private Boolean withNumbers;

    private static final String EDUCATION_YEAR_TITLE = "Учебный год";
    private static final String FORMATIVE_ORG_UNIT_TITLE = "Формирующее подразделение";
    private static final String COURSE_TITLE = "Курс";
    private static final String SEX_TITLE = "Пол";
    private static final String PLACE_TITLE = "Спортивный объект";

    private static final Integer DAY_START = 8;
    private static final Integer DAY_FINISH = 21;
    private static final Integer WEEK_START = 1;
    private static final Integer WEEK_FINISH = 6;

    private Map<Integer, Map<Integer, List<EventOfSchedule>>> _dataMap = new TreeMap<>();
    private Map<String, EventOfSchedule> _eventByKey = new HashMap<>();

    /**
     * Основной метод печати.
     */
    def print()
    {
        def document = buildReport(new RtfReader().read(template))
        String date = DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(new Date())
        return [document: RtfUtil.toByteArray(document), fileName: 'Список заявок на секции(' + date + ').rtf']
    }

    RtfDocument buildReport(RtfDocument document)
    {
        document = printHeaderTable(document);
        document = printTable(document);

        return document
    }

    RtfDocument printHeaderTable(RtfDocument document)
    {
        List<String[]> rows = new ArrayList<>();
        String[] row = new String[2];
        row[0] = EDUCATION_YEAR_TITLE;
        row[1] = ((EducationYear) session.get(EducationYear.class, educationYearId)).title;
        rows.add(row)

        if (CollectionUtils.isNotEmpty(formativeOrgUnitIds))
        {
            row = new String[2];
            row[0] = FORMATIVE_ORG_UNIT_TITLE;

            row[1] = new DQLSelectBuilder()
                    .fromEntity(OrgUnit, "e")
                    .column(property("e", OrgUnit.title()))
                    .where(DQLExpressions.in(property("e", OrgUnit.id()), formativeOrgUnitIds))
                    .createStatement(session).<String> list().join(", ");
            rows.add(row)
        }

        if (CollectionUtils.isNotEmpty(courseIds))
        {
            row = new String[2];
            row[0] = COURSE_TITLE;

            row[1] = new DQLSelectBuilder()
                    .fromEntity(Course, "e")
                    .column(property("e", Course.title()))
                    .where(DQLExpressions.in(property("e", Course.id()), courseIds))
                    .createStatement(session).<String> list().join(", ");
            rows.add(row)
        }

        if (genderId != null)
        {
            row = new String[2];
            row[0] = SEX_TITLE;
            row[1] = ((Sex) session.get(Sex.class, genderId)).title;
            rows.add(row)
        }

        if (CollectionUtils.isNotEmpty(placeIds))
        {
            row = new String[2];
            row[0] = PLACE_TITLE;

            row[1] = new DQLSelectBuilder()
                    .fromEntity(UniplacesPlace, "e")
                    .column(property("e", UniplacesPlace.title()))
                    .where(DQLExpressions.in(property("e", UniplacesPlace.id()), placeIds))
                    .createStatement(session).<String> list().join(", ");
            rows.add(row)
        }

        def tm = new RtfTableModifier()
        tm.put("Theader", rows as String[][])
        tm.modify(document)

        return document;
    }

    RtfDocument printTable(RtfDocument document)
    {
        fillDataMap();

        def tm = new RtfTableModifier()
        tm.put("T1", getTable() as String[][])
        tm.put("T1", new RtfRowIntercepterBase() {

            @Override
            public List<IRtfElement> beforeInject(
                    RtfTable table,
                    RtfRow row,
                    RtfCell cell,
                    int rowIndex,
                    int colIndex,
                    String value)
            {
                if (colIndex == 0)
                {
                    if (value == null)
                        cell.setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    else
                        cell.setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                } else
                {
                    if (value != null)
                    {
                        cell.setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                        return _eventByKey.get(value)?.info?.toList();
                    } else
                        cell.setMergeType(row.getCellList().get(0).getMergeType());
                }

                return null;
            }
        })
        tm.modify(document)

        return document;
    }

    void fillDataMap()
    {
        String alias = "ss";
        DQLSelectBuilder sectionBuilder = new DQLSelectBuilder()
                .fromEntity(SportSection.class, alias)
                .column(property(alias))
                .where(DQLExpressions.eq(property(alias, SportSection.educationYear()), DQLExpressions.value(educationYearId)))
                .column(SportSection.getFreePlacesExpression(alias));

        if (CollectionUtils.isNotEmpty(courseIds))
            sectionBuilder.where(exists(SportSection2CourseRel.class,
                                        SportSection2CourseRel.section().s(), property(alias),
                                        SportSection2CourseRel.course().id().s(), courseIds));
        if (genderId != null)
            sectionBuilder.where(exists(SportSection2SexRel.class,
                                        SportSection2SexRel.section().s(), property(alias),
                                        SportSection2SexRel.sex().s(), genderId));

        FilterUtils.applySelectFilter(sectionBuilder, alias, SportSection.formativeOrgUnit().id().s(), formativeOrgUnitIds);
        FilterUtils.applySelectFilter(sectionBuilder, alias, SportSection.place().id().s(), placeIds);

        List<Object[]> sections = sectionBuilder.createStatement(session).<Object[]> list();

        for (Integer hour = DAY_START; hour <= DAY_FINISH; hour++)
        {
            Map<Integer, List<SportSection>> row = new TreeMap<>();
            for (Integer day = WEEK_START; day <= WEEK_FINISH; day++)
                row.put(day, new ArrayList<SportSection>())
            _dataMap.put(hour, row);
        }

        List<Long> sectionIds = new ArrayList<>();
        for (Object[] o : sections)
            sectionIds.add((o[0] as SportSection).id);
        Map<Long, List<String>> courseBySection = getCoursesMap(sectionIds);
        Map<Long, List<String>> genderBySection = getGenderMap(sectionIds);

        for (Object[] o : sections)
        {
            SportSection section = o[0] as SportSection;
            Long freePlaces = o[1] as Long;
            addEvents(section, freePlaces, courseBySection.get(section.id), genderBySection.get(section.id));
        }
    }

    Map<Long, List<String>> getCoursesMap(List<Long> sectionIds)
    {
        List<Object[]> list = new DQLSelectBuilder()
                .fromEntity(SportSection2CourseRel.class, "rel")
                .column(property("rel", SportSection2CourseRel.section().id()))
                .column(property("rel", SportSection2CourseRel.course().title()))
                .where(DQLExpressions.in(property("rel", SportSection2CourseRel.section().id()), sectionIds))
                .createStatement(session).<Object[]> list();

        Map<Long, List<String>> courseBySection = new HashMap<>();
        for (Object[] o : list)
        {
            Long sectionId = (Long) o[0];
            List<String> courses = courseBySection.get(sectionId);
            if (courses == null)
            {
                courses = new ArrayList<>();
                courseBySection.put(sectionId, courses);
            }
            courses.add((String) o[1]);
        }

        return courseBySection;
    }

    Map<Long, List<String>> getGenderMap(List<Long> sectionIds)
    {
        List<Object[]> list = new DQLSelectBuilder()
                .fromEntity(SportSection2SexRel.class, "rel")
                .column(property("rel", SportSection2SexRel.section().id()))
                .column(property("rel", SportSection2SexRel.sex().shortTitle()))
                .where(DQLExpressions.in(property("rel", SportSection2SexRel.section().id()), sectionIds))
                .createStatement(session).<Object[]> list();

        Map<Long, List<String>> genderBySection = new HashMap<>();
        for (Object[] o : list)
        {
            Long sectionId = (Long) o[0];
            List<String> genders = genderBySection.get(sectionId);
            if (genders == null)
            {
                genders = new ArrayList<>();
                genderBySection.put(sectionId, genders);
            }
            genders.add((String) o[1]);
        }

        return genderBySection;
    }

    List<String[]> getTable()
    {
        List<String[]> rows = new ArrayList<>();

        int eventKey = 0;
        for (Map.Entry<Integer, Map<Integer, List<EventOfSchedule>>> entryByHour : _dataMap.entrySet())
        {
            int eventInHour = 0;
            boolean d0 = true;
            while (d0)
            {
                d0 = false;
                String[] row = new String[WEEK_FINISH - WEEK_START + 2];
                row[0] = eventInHour == 0 ? String.valueOf(entryByHour.key) + ":00" : null;
                int day = 1;
                for (Map.Entry<Integer, List<EventOfSchedule>> entryByDay : entryByHour.value.entrySet())
                {
                    EventOfSchedule section = null;
                    if (eventInHour < entryByDay.value.size())
                        section = entryByDay.value.get(eventInHour);
                    if (section != null)
                    {
                        String key = String.valueOf(eventKey++);
                        row[day] = key;
                        d0 = true;
                        _eventByKey.put(key, section);
                        eventInHour++;
                    } else
                        row[day] = null;
                    day++;
                }

                if (d0 || row[0] != null)
                    rows.add(row);
            }
        }

        return rows;
    }

    void addEvents(SportSection section, Long freePlaces, List<String> courses, List<String> genders)
    {
        addEvent(section.getFirstClassTime(), section.getFirstClassDay(), section, freePlaces, courses, genders);
        addEvent(section.getSecondClassTime(), section.getSecondClassDay(), section, freePlaces, courses, genders);
    }

    void addEvent(
            Date time,
            Integer day,
            SportSection section,
            Long freePlaces,
            List<String> courses,
            List<String> genders)
    {
        if (time == null) return;

        def calendar = Calendar.getInstance();
        calendar.setTime(time);
        def hour = calendar.get(Calendar.HOUR_OF_DAY)

        if (hour < DAY_START || hour > DAY_FINISH) return;
        if (day == null || day < WEEK_START || day > WEEK_FINISH) return;

        _dataMap.get(hour).get(day).add(new EventOfSchedule(section, freePlaces, courses, genders));
    }

    class EventOfSchedule
    {
        final String title;
        final String place;
        final String type;
        final String course;
        final String gender;
        final String freePlaces;
        final String trainer;

        EventOfSchedule(SportSection section, Long freePlaces, List<String> courses, List<String> genders)
        {
            title = section.title;
            place = section.place.title;
            trainer = section.trainer?.fio;
            type = "тип(" + (section.type.code.equals(SportSectionTypeCodes.HIGH_SCHOOL) ? "ВШ" : "вне сетки") + ")";
            course = "Курс: " + courses.join(", ");
            gender = "Пол: " + genders.join(", ");
            this.freePlaces = "Свободно мест: " + String.valueOf(freePlaces);
        }

        RtfString getInfo()
        {

            RtfString string = new RtfString();
            string.boldBegin().append(title).boldEnd();
            if (CollectionUtils.isEmpty(placeIds) || placeIds.size() > 1)
                string.append(IRtfData.PAR).append(place);

            if (trainer != null)
                string.append(",").append(IRtfData.PAR).append(trainer);

            string.append(",").append(IRtfData.PAR).append(IRtfData.I).append(type);

            string.append(",").append(IRtfData.PAR).append(course);

            string.append(", ").append(gender);

            if (withNumbers)
                string.append(",").append(IRtfData.PAR).append(freePlaces);

            return string.append(".").append(IRtfData.I, 0);
        }
    }
}
