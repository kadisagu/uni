package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.ChoiceOrientationSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки выбора направленностей по умолчанию
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChoiceOrientationSettingsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.ChoiceOrientationSettings";
    public static final String ENTITY_NAME = "choiceOrientationSettings";
    public static final int VERSION_HASH = -795856653;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String P_DATE = "date";
    public static final String P_COUNT_PLACES = "countPlaces";

    private EducationYear _educationYear;     // Учебный год
    private Date _date;     // Запись до
    private int _countPlaces;     // Минимальное количество мест

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null и должно быть уникальным.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Запись до. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Запись до. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Минимальное количество мест. Свойство не может быть null.
     */
    @NotNull
    public int getCountPlaces()
    {
        return _countPlaces;
    }

    /**
     * @param countPlaces Минимальное количество мест. Свойство не может быть null.
     */
    public void setCountPlaces(int countPlaces)
    {
        dirty(_countPlaces, countPlaces);
        _countPlaces = countPlaces;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChoiceOrientationSettingsGen)
        {
            setEducationYear(((ChoiceOrientationSettings)another).getEducationYear());
            setDate(((ChoiceOrientationSettings)another).getDate());
            setCountPlaces(((ChoiceOrientationSettings)another).getCountPlaces());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChoiceOrientationSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChoiceOrientationSettings.class;
        }

        public T newInstance()
        {
            return (T) new ChoiceOrientationSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationYear":
                    return obj.getEducationYear();
                case "date":
                    return obj.getDate();
                case "countPlaces":
                    return obj.getCountPlaces();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "countPlaces":
                    obj.setCountPlaces((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationYear":
                        return true;
                case "date":
                        return true;
                case "countPlaces":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationYear":
                    return true;
                case "date":
                    return true;
                case "countPlaces":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationYear":
                    return EducationYear.class;
                case "date":
                    return Date.class;
                case "countPlaces":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChoiceOrientationSettings> _dslPath = new Path<ChoiceOrientationSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChoiceOrientationSettings");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitutor.entity.ChoiceOrientationSettings#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Запись до. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceOrientationSettings#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Минимальное количество мест. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceOrientationSettings#getCountPlaces()
     */
    public static PropertyPath<Integer> countPlaces()
    {
        return _dslPath.countPlaces();
    }

    public static class Path<E extends ChoiceOrientationSettings> extends EntityPath<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private PropertyPath<Date> _date;
        private PropertyPath<Integer> _countPlaces;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitutor.entity.ChoiceOrientationSettings#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Запись до. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceOrientationSettings#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(ChoiceOrientationSettingsGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Минимальное количество мест. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceOrientationSettings#getCountPlaces()
     */
        public PropertyPath<Integer> countPlaces()
        {
            if(_countPlaces == null )
                _countPlaces = new PropertyPath<Integer>(ChoiceOrientationSettingsGen.P_COUNT_PLACES, this);
            return _countPlaces;
        }

        public Class getEntityClass()
        {
            return ChoiceOrientationSettings.class;
        }

        public String getEntityName()
        {
            return "choiceOrientationSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
