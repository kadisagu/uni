package ru.tandemservice.unitutor.component.awp.OrientationList.DateAndPlacesEdit;


import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEntityList(getList(Orientation2EduPlanVersion.class, model.getEntityIds()));
        List<Integer> counts = model.getEntityList().stream()
                                .map(Orientation2EduPlanVersion::getMinCount)
                                .distinct()
                                .collect(Collectors.toList());
        if (!counts.isEmpty() && counts.size() == 1)
            model.setCountPlaces(counts.get(0));
        List<Date> dates = model.getEntityList().stream()
                            .map(Orientation2EduPlanVersion::getDate)
                            .distinct()
                            .collect(Collectors.toList());
        if (!dates.isEmpty() && dates.size() == 1)
            model.setDate(dates.get(0));

    }

    @Override
    public void update(Model model)
    {
        super.update(model);
        for (Orientation2EduPlanVersion rel : model.getEntityList()) {
            if (model.getDate()!= null)
                rel.setDate(model.getDate());
            if (model.getCountPlaces() != null)
                rel.setMinCount(model.getCountPlaces());
            update(rel);
        }
    }
}
