package ru.tandemservice.unitutor.base.bo.Tutor.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;

@Configuration
public class TutorTab extends BusinessComponentManager {

    public static final String AWP_TUTOR_PANEL = "tutorAWPTabPanel";

    public static final String DISCIPLINES_TAB = "optionalDisciplinesList";
    public static final String DISCIPLINES_RECORD_TAB = "optionalDisciplinesSubscribe";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }

    @Bean
    public TabPanelExtPoint tutorTabPanelExtPoint() {

        return tabPanelExtPointBuilder(AWP_TUTOR_PANEL)

                .addTab(componentTab(DISCIPLINES_TAB, "electiveDisciplineList")
                                .parameters("ui:listParam") // заменяю имя параметра
                                .visible("ui:tutor"))


                .addTab(componentTab(DISCIPLINES_RECORD_TAB, "studentSubscribesFromTutorCardTab")
                                .parameters("ui:listParam")
                                .visible("ui:tutor"))
                .create();
    }
}
