//package ru.tandemservice.unitutor.utils;
//
//import org.apache.commons.lang.StringUtils;
//
//import java.util.List;
//
//public class ElectiveMailUtil {
//
//    public final static String ELECTIVE_MAIL_SUBJECT = "Подписка на ДПВ";
//    public final static String TEXT_CONTENT_TYPE = "text/plain";
//    public final static String HTML_CONTENT_TYPE = "text/html";
//
//    /**
//     * Нафея!!!!
//     *
//     * @return
//     */
//    public static String electiveMailBuild(MailWrapperData mailWrapperData) {
//        StringBuilder contentBuilder = new StringBuilder();
//        contentBuilder.append("<html><body>");
//        contentBuilder.append("<p>Добрый день !</p><p>Для изучения в следующем учебном году Вы записаны на следующие дисциплины:</p>");
//        contentBuilder.append("<ul type='disc'>");
//        contentBuilder.append("<li>")
//                .append(mailWrapperData.getEppRegistryElementPart().getRegistryElement().getTitle())
//                .append("<ol type='a'>")
//                .append("<li>Аннотация: ").append(StringUtils.trimToEmpty(mailWrapperData.getAnnotation())).append("</li>")
//                .append("<li>Семестр изучения: ").append(mailWrapperData.getTerm()).append("</li>");
//
//        if (mailWrapperData.getFormOfControl() != null)
//            contentBuilder.append("<li>Форма контроля: ").append(mailWrapperData.getFormOfControl()).append("</li>");
//
//        contentBuilder.append("</ol></li>");
//        contentBuilder.append("</ul>");
//        contentBuilder.append("</body></html>");
//        return contentBuilder.toString();
//    }
//
//    /**
//     * Оповещение при записи из Личного кабинета Сакаи
//     *
//     * @param mailwrapperDataList
//     *
//     * @return
//     */
//    public static String electiveMailBuild(List<MailWrapperData> mailwrapperDataList) {
//        StringBuilder contentBuilder = new StringBuilder();
//        contentBuilder.append("<html><body>");
//        contentBuilder.append("<p>Добрый день !</p><p>Для изучения в следующем учебном году Вы выбрали следующие дисциплины:</p>");
//        contentBuilder.append("<ul type='disc'>");
//
//        for (MailWrapperData mailData : mailwrapperDataList) {
//            contentBuilder.append("<li>")
//                    .append(mailData.getEppRegistryElementPart().getRegistryElement().getTitle())
//                    .append("<ol type='a'>")
//                    .append("<li>Аннотация: ").append(StringUtils.trimToEmpty(mailData.getAnnotation())).append("</li>")
//                    .append("<li>Семестр изучения: ").append(mailData.getTerm()).append("</li>");
//
//            if (mailData.getFormOfControl() != null)
//                contentBuilder.append("<li>Форма контроля: ").append(mailData.getFormOfControl()).append("</li>");
//
//            contentBuilder.append("</ol></li>");
//        }
//
//        contentBuilder.append("</ul>");
//        contentBuilder.append("</body></html>");
//        return contentBuilder.toString();
//    }
//}
