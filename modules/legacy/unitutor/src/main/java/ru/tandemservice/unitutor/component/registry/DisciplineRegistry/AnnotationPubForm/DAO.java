package ru.tandemservice.unitutor.component.registry.DisciplineRegistry.AnnotationPubForm;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {

        EppRegistryElementExt registryElementExt = get(EppRegistryElementExt.class, EppRegistryElementExt.eppRegistryElement().s(), model.getRegistryElementId());

        if (registryElementExt == null)
            registryElementExt = new EppRegistryElementExt(getNotNull(EppRegistryElement.class, model.getRegistryElementId()));

        model.setRegistryElementExt(registryElementExt);
    }

    @Override
    public void saveOrUpdate(IEntity entity) {
        super.saveOrUpdate(entity);
    }
}
