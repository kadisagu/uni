package ru.tandemservice.unitutor.component.registry.RegistryBase;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitutor.component.awp.base.IFilterProperties;

import java.util.List;

public class RegistryBaseModel {

    private IDataSettings settings;
    private DynamicListDataSource<RegistryWrapper> dataSource;

    private ISelectModel educationYearModel;
    private ISelectModel educationYearPartModel;
    private ISelectModel courseModel;
    private ISelectModel disciplineModel;
    private ISelectModel eduGroupModel;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getEducationYearModel() {
        return educationYearModel;
    }

    public void setEducationYearModel(ISelectModel educationYearModel) {
        this.educationYearModel = educationYearModel;
    }

    public ISelectModel getEducationYearPartModel() {
        return educationYearPartModel;
    }

    public void setEducationYearPartModel(ISelectModel educationYearPartModel) {
        this.educationYearPartModel = educationYearPartModel;
    }

    public ISelectModel getEduGroupModel() {
        return eduGroupModel;
    }

    public void setEduGroupModel(ISelectModel eduGroupModel) {
        this.eduGroupModel = eduGroupModel;
    }

    public ISelectModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel) {
        this.courseModel = courseModel;
    }

    public ISelectModel getDisciplineModel() {
        return disciplineModel;
    }

    public void setDisciplineModel(ISelectModel disciplineModel) {
        this.disciplineModel = disciplineModel;
    }

    public DynamicListDataSource<RegistryWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<RegistryWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    ////////////////////////////////////////////
    // значения фильтров
    ////////////////////////////////////////////

    public EducationYear getFilterEducationYear() {
        return getSettings().get(IFilterProperties.EDUCATION_YEAR_FILTER);
    }

    public List<Course> getFilterCourse() {
        return getSettings().get(IFilterProperties.COURSE_FILTER);
    }

    public YearDistributionPart getFilterYearDistributionPart() {
        return getSettings().get(IFilterProperties.EDUCATION_YEAR_PART_FILTER);
    }

    public List<EducationLevelsHighSchool> getFilterEduGroupList() {
        return getSettings().get(IFilterProperties.EDU_GROUP_FILTER);
    }

    public List<EppRegistryElement> getFilterDiscipline() {
        return getSettings().get(IFilterProperties.DISCIPLINE_FILTER);
    }
/*
    @SuppressWarnings("rawtypes")
	public List<String> getFilterDiscipline() {
		List<IdentifiableWrapper> disciplineList = getSettings().get(IFilterProperties.DISCIPLINE_FILTER);

		List<String> disciplineTitleList = null;

		if (disciplineList != null && !disciplineList.isEmpty()) {
			disciplineTitleList = new ArrayList<String>();
			for (IdentifiableWrapper discipline : disciplineList) {
				disciplineTitleList.add(discipline.getTitle());
			}
		}

		return disciplineTitleList;
	}
	*/
}
