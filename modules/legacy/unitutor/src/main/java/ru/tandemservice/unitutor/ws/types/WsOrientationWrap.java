package ru.tandemservice.unitutor.ws.types;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "orientationWrap", namespace = "http://ws.unitutor.tandemservice.ru/")
public class WsOrientationWrap implements Serializable {

    private static final long serialVersionUID = -7110878198357691422L;

    private Long orientationId;

    /**
     * Название направленности
     */
    private String title;

    /**
     * Запись на направленность
     */
    private boolean subscribed;

    /**
     * Источник подписки
     */
    private String source;

    /**
     * Запись до
     */
    private String subscribedToDate;

    public WsOrientationWrap() {
    }

    public WsOrientationWrap(Orientation2EduPlanVersion orientation) {
        this.orientationId = orientation.getOrientation().getId();
        this.title = orientation.getOrientation().getEducationLevelHighSchool().getFullTitle();
        this.subscribedToDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(orientation.getDate());
    }

    public Long getOrientationId() {
        return orientationId;
    }

    public void setOrientationId(Long orientationId) {
        this.orientationId = orientationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public String getSubscribedToDate() {
        return subscribedToDate;
    }

    public void setSubscribedToDate(String subscribedToDate) {
        this.subscribedToDate = subscribedToDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
