package ru.tandemservice.unitutor.component.awp.base.discipline;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitutor.component.awp.base.IFilterProperties;

import java.util.ArrayList;
import java.util.List;

@Input({
        @Bind(key = "orgUnitId", binding = "orgUnitId"),
        @Bind(key = "employeeId", binding = "employeeId"),
        @Bind(key = "employeePostId", binding = "employeePostId")
})
@State({@org.tandemframework.core.component.Bind(key = "selectedTab", binding = "selectedTab")})
public abstract class ModelBase {
    private String selectedTab;
    private IDataSettings settings;
    private Long orgUnitId;
    private Long employeeId;
    private Long employeePostId;


    private OrgUnit orgUnit;
    private Employee employee;
    private EmployeePost employeePost;
    private CommonPostfixPermissionModelBase sec;

    private ISelectModel educationLevelHighSchoolListModel;
    private ISelectModel educationYearListModel;
    private ISelectModel educationYearPartListModel;
    private ISelectModel groupListModel;
    private ISelectModel disciplineListModel;

    public final static Long FREE_YES = 1L;
    public final static Long FREE_NO = 2L;

    public final static List<IdentifiableWrapper> FREE_STATUS_LIST = ImmutableList.of(
            new IdentifiableWrapper(FREE_YES, "Да"),
            new IdentifiableWrapper(FREE_NO, "Нет")
    );

    protected abstract String getSecurityPostfix();

    public String getSelectedTab() {
        return this.selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public CommonPostfixPermissionModelBase getSec() {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModelBase sec) {
        this.sec = sec;
    }

    public Long getEmployeePostId() {
        return employeePostId;
    }

    public void setEmployeePostId(Long employeePostId) {
        this.employeePostId = employeePostId;
    }

    public EmployeePost getEmployeePost() {
        return employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost) {
        this.employeePost = employeePost;
    }

    public ISelectModel getEducationLevelHighSchoolListModel() {
        return educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(
            ISelectModel educationLevelHighSchoolListModel)
    {
        this.educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public ISelectModel getEducationYearListModel() {
        return educationYearListModel;
    }

    public void setEducationYearListModel(ISelectModel educationYearListModel) {
        this.educationYearListModel = educationYearListModel;
    }

    public ISelectModel getEducationYearPartListModel() {
        return educationYearPartListModel;
    }

    public void setEducationYearPartListModel(ISelectModel educationYearPartListModel) {
        this.educationYearPartListModel = educationYearPartListModel;
    }

    public ISelectModel getGroupListModel() {
        return groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel) {
        this.groupListModel = groupListModel;
    }

    public ISelectModel getDisciplineListModel() {
        return disciplineListModel;
    }

    public void setDisciplineListModel(ISelectModel disciplineListModel) {
        this.disciplineListModel = disciplineListModel;
    }


    ///////////////////////////////////////////////////////
    ////// значения фильтров
    ///////////////////////////////////////////////////////

    public EducationYear getFilterEducationYear()
    {
        return getSettings().get(IFilterProperties.EDUCATION_YEAR_FILTER);
    }

    public YearDistributionPart getFilterYearDistributionPart()
    {
        return getSettings().get(IFilterProperties.EDUCATION_YEAR_PART_FILTER);
    }

    public List<EducationLevelsHighSchool> getFilterEducationLevelsHighSchoolList()
    {
        return getSettings().get(IFilterProperties.EDUCATION_LEVEL_HIGH_SCHOOL_FILTER);
    }

    public List<Group> getFilterGroupList()
    {
        return getSettings().get(IFilterProperties.GROUP_FILTER);
    }

    public List<Student> getFilterStudentList()
    {
        return getSettings().get(IFilterProperties.STUDENT_FILTER);
    }


    /**
     * непосредственно к фильтру отношения не имеет
     *
     * @return
     */
    public List<OrgUnit> getFilterOrgUnit()
    {

        if (getEmployee() != null) {
            // указан преподаватель, все фильтры вяжем на преподавателя
            return null;
        }
        List<OrgUnit> retVal = new ArrayList<>();

        // если преводавателя нет и зашли со вкладки кафедры
        if (OrgUnitTypeCodes.CATHEDRA.equals(getOrgUnit().getOrgUnitType().getCode())) {
            OrgUnit parentCathedraOrgUnit = getOrgUnit().getParent();
            if (parentCathedraOrgUnit != null) {
                retVal.add(parentCathedraOrgUnit);
            }
            else {
                retVal = null;
            }
        }
        else {
            // препода нет, только орг юнит
            retVal.add(getOrgUnit());
        }

        return retVal;
    }

    /**
     * По читающему подразделению
     * непосредственно к фильтру отношения не имеет
     *
     * @return
     */
    public List<OrgUnit> getFilterCathedraOrgUnit()
    {

        if (getEmployee() != null) {
            // указан преподаватель, все фильтры вяжем на преподавателя
            return null;
        }
        // если преводавателя нет и зашли со вкладки кафедры
        if (OrgUnitTypeCodes.CATHEDRA.equals(getOrgUnit().getOrgUnitType().getCode())) {
            // препода нет, только орг юнит = кафедра
            List<OrgUnit> retVal = new ArrayList<>();
            retVal.add(getOrgUnit());
            return retVal;
        }

        return null;
    }

    public List<Employee> getFilterEmployee()
    {
        if (getEmployee() != null) {
            List<Employee> retVal = new ArrayList<>();
            retVal.add(getEmployee());
            return retVal;
        }
        else
            return null;
    }

    public List<EppRegistryElement> getFilterDiscipline()
    {
        List<EppRegistryElement> disciplineList = getSettings().get(IFilterProperties.DISCIPLINE_FILTER);

        return disciplineList;

    }

    public Object getSecuredObject()
    {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public boolean isCathedra() {
        if (getOrgUnit() != null)
            return OrgUnitTypeCodes.CATHEDRA.equals(getOrgUnit().getOrgUnitType().getCode());
        return false;
    }

}
