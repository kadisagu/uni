package ru.tandemservice.unitutor.component.awp.OrientationList;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.EppEduPlanVersionExt;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;
import ru.tandemservice.unitutor.entity.Student2Orientation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    private static final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(Orientation2EduPlanVersion.class, "rel");

    private static final String VERSION_ALIAS = "version";
    private static final String VERSION_EXT_ALIAS = "versionExt";
    private static final String REL_ALIAS = "rel";

    @Override
    public void prepare(final Model model) {
        model.getOrgUnitHolder().refresh();

        model.setEduYearModel(new LazySimpleSelectModel<>(EducationYear.class, EducationYear.title().s()));

//TODO DEV-6870
//		model.setParentModel(new DQLFullCheckSelectModel(EducationLevelsHighSchool.fullTitle().s()) {
//			@Override
//			protected DQLSelectBuilder query(String alias, String filter) {
//				DQLSelectBuilder subBuilder = getBuilder(model)
//						.column(Orientation2EduPlanVersion.eduPlanVersion().eduPlan().educationLevelHighSchool().fromAlias(REL_ALIAS).s())
//						;
//				DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, alias)
//						.where(DQLExpressions.in(DQLExpressions.property(alias), subBuilder.buildQuery()));
//
//				FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle(), filter);
//				return builder;
//			}
//		});

        model.setOrientationModel(new DQLFullCheckSelectModel(EducationLevelsHighSchool.fullTitle().s()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(Orientation2EduPlanVersion.orientation().educationLevelHighSchool().fromAlias(REL_ALIAS).s());
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));

                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle(), filter);
                return builder;
            }
        });

        model.setCourseModel(new DQLFullCheckSelectModel(Course.title().s()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(Orientation2EduPlanVersion.course().fromAlias(REL_ALIAS).s());
                return new DQLSelectBuilder().fromEntity(Course.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));
            }
        });

        model.setEduPlanModel(new DQLFullCheckSelectModel(EppEduPlanVersion.fullTitle().s()) {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(Orientation2EduPlanVersion.eduPlanVersion().fromAlias(REL_ALIAS).s());
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));
                FilterUtils.applyLikeFilter(builder, filter, EppEduPlanVersion.number().fromAlias(alias),
                        EppEduPlanVersion.eduPlan().number().fromAlias(alias));
                return builder;
            }
        });

        model.setActualSelectModel(new LazySimpleSelectModel<>(Model.YES_NO_ITEMS));
        model.setFreePlaceSelectModel(new LazySimpleSelectModel<>(Model.YES_NO_ITEMS));
        model.setAllowEntryModel(new LazySimpleSelectModel<>(Model.YES_NO_ITEMS));
    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<Orientation2EduPlanVersion> dataSource = model.getDataSource();

        DQLSelectBuilder builder = getBuilder(model)
                .column(REL_ALIAS);
        order.applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());

        List<Object[]> lst = getBuilder(model)
                .column(REL_ALIAS)
                .column("dsCount.summ")
                .createStatement(getSession()).list();

        Map<Long, Long> map = new HashMap<>();

        for (Object[] obj : lst) {
            Orientation2EduPlanVersion entity = (Orientation2EduPlanVersion) obj[0];
            Long count = (Long) obj[1];
            map.put(entity.getId(), count);
        }

        List<ViewWrapper<Orientation2EduPlanVersion>> wrappers = ViewWrapper.getPatchedList(dataSource);

        for (ViewWrapper<Orientation2EduPlanVersion> wrapper : wrappers) {
            Long count = map.get(wrapper.getEntity().getId());
            wrapper.setViewProperty("recordCount", count == null ? 0 : count.intValue());
        }
    }

    private DQLSelectBuilder getBuilder(Model model) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Orientation2EduPlanVersion.class, REL_ALIAS)
                .where(eqValue(property(Orientation2EduPlanVersion.orientation().formativeOrgUnit().id().fromAlias(REL_ALIAS)), model.getOrgUnitId()))
                .where(eqValue(property(Orientation2EduPlanVersion.educationYear().fromAlias(REL_ALIAS)), model.getEducationYear()))
                .joinPath(DQLJoinType.inner, Orientation2EduPlanVersion.eduPlanVersion().fromAlias(REL_ALIAS), VERSION_ALIAS)
                        //расширение УПв
                .joinEntity(REL_ALIAS, DQLJoinType.left, EppEduPlanVersionExt.class, VERSION_EXT_ALIAS,
                            eq(property(EppEduPlanVersionExt.eduPlanVersion().id().fromAlias(VERSION_EXT_ALIAS)),property(EppEduPlanVersion.id().fromAlias(VERSION_ALIAS))))
                .joinDataSource(REL_ALIAS, DQLJoinType.left, getCountRecord().buildQuery(), "dsCount",
                                and(
                                        eq(property(Orientation2EduPlanVersion.eduPlanVersion().id().fromAlias(REL_ALIAS)),property("dsCount.versionId")),
                                        eq(property(Orientation2EduPlanVersion.orientation().id().fromAlias(REL_ALIAS)),property("dsCount.orientationId")),
                                        eq(property(Orientation2EduPlanVersion.educationYear().id().fromAlias(REL_ALIAS)),property("dsCount.educationYearId"))
                                ));

//		FilterUtils.applySelectFilter(builder, REL_ALIAS, Orientation2EduPlanVersion.eduPlanVersion().eduPlan().educationLevelHighSchool(), model.getParentsEHS());
        FilterUtils.applySelectFilter(builder, REL_ALIAS, Orientation2EduPlanVersion.orientation().educationLevelHighSchool(), model.getOrientationsEHS());
        FilterUtils.applySelectFilter(builder, REL_ALIAS, Orientation2EduPlanVersion.course(), model.getCourse());
        FilterUtils.applySelectFilter(builder, REL_ALIAS, Orientation2EduPlanVersion.eduPlanVersion(), model.getEduPlanVersions());

        if (model.isActual() != null)
            builder.where(eqValue(property(Orientation2EduPlanVersion.actual().fromAlias(REL_ALIAS)), model.isActual()));

        if (model.isAllowEntry() != null)
            builder.where(eqValue(property(Orientation2EduPlanVersion.allowEntry().fromAlias(REL_ALIAS)), model.isAllowEntry()));

        if (model.isFreePlaces() != null) {
            if (model.isFreePlaces())
                builder.where(or(isNull("dsCount.summ"),
                                gt(property(Orientation2EduPlanVersion.minCount().fromAlias(REL_ALIAS)), property("dsCount.summ"))
                        )
                );
            else
                builder.where(and(isNotNull("dsCount.summ"),
                                  le(property(Orientation2EduPlanVersion.minCount().fromAlias(REL_ALIAS)),property("dsCount.summ"))
                ));
        }

        return builder;
    }

    @Override
    public void actualizeRows(Model model) {
        DaoStudentSubscribe.instanse().actualizeOrientations(model.getOrgUnitId(), model.getEducationYear());
    }

    private DQLSelectBuilder getCountRecord() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student2Orientation.class, "r")
                .joinEntity("r", DQLJoinType.inner, EppStudent2EduPlanVersion.class, "st2v",
                            eq(property(Student2Orientation.student().id().fromAlias("r")), property(EppStudent2EduPlanVersion.student().id().fromAlias("st2v"))))
                .column(Student2Orientation.orientation().id().fromAlias("r").s(), "orientationId")
                .column(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("st2v").s(), "versionId")
                .column(Student2Orientation.educationYear().id().fromAlias("r").s(), "educationYearId")
                .column(DQLFunctions.count(Student2Orientation.id().fromAlias("r").s()), "summ")
                .group(Student2Orientation.orientation().id().fromAlias("r").s())
                .group(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("st2v").s())
                .group(Student2Orientation.educationYear().id().fromAlias("r").s());
        return builder;
    }

    @Override
    public void allowEntry(Long id) {
        Orientation2EduPlanVersion rel = get(Orientation2EduPlanVersion.class, id);
        rel.setAllowEntry(!rel.isAllowEntry());
        update(rel);

    }
}
