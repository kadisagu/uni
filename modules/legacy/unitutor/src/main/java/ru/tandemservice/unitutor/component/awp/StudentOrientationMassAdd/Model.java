package ru.tandemservice.unitutor.component.awp.StudentOrientationMassAdd;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.List;

@Input({
        @org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "orgUnitId"),
})
public class Model {

    public final static String EDU_LEVEL_FILTER = "eduLevelFilter";
    public final static String GROUP_FILTER = "groupFilter";
    public final static String EDU_PLAN_FILTER = "eduPlanFilter";

    private Long orgUnitId;

    private ISelectModel orientationSelectModel;
    private EducationOrgUnit orientation;
    private boolean sendMail = true;
    private DynamicListDataSource<Student> dataSource;
    private IDataSettings settings;
    private ISelectModel eduLevelModel;
    private ISelectModel groupModel;
    private ISelectModel eduPlanModel;

    public EppEduPlanVersion getEduPlanVersion() {
        return getSettings().get(EDU_PLAN_FILTER);
    }

    public EducationLevelsHighSchool getEduLevel() {
        return getSettings().get(EDU_LEVEL_FILTER);
    }

    public List<Group> getGroups() {
        return getSettings().get(GROUP_FILTER);
    }

    public String getEduYear() {
        EducationYear educationYear = getEducationYear();
        return "Учебный год: " + (educationYear == null ? "" : educationYear.getTitle());
    }

    public EducationYear getEducationYear() {
        final EducationYear current = EducationYearManager.instance().dao().getCurrent();
        return IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.intValue(), current.getIntValue() + 1);
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public ISelectModel getOrientationSelectModel() {
        return orientationSelectModel;
    }

    public void setOrientationSelectModel(ISelectModel orientationSelectModel) {
        this.orientationSelectModel = orientationSelectModel;
    }

    public EducationOrgUnit getOrientation() {
        return orientation;
    }

    public void setOrientation(EducationOrgUnit orientation) {
        this.orientation = orientation;
    }

    public boolean isSendMail() {
        return sendMail;
    }

    public void setSendMail(boolean sendMail) {
        this.sendMail = sendMail;
    }

    public DynamicListDataSource<Student> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<Student> dataSource) {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getEduLevelModel() {
        return eduLevelModel;
    }

    public void setEduLevelModel(ISelectModel eduLevelModel) {
        this.eduLevelModel = eduLevelModel;
    }

    public ISelectModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel) {
        this.groupModel = groupModel;
    }

    public ISelectModel getEduPlanModel() {
        return eduPlanModel;
    }

    public void setEduPlanModel(ISelectModel eduPlanModel) {
        this.eduPlanModel = eduPlanModel;
    }

}
