package ru.tandemservice.unitutor.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Конфигурация для скриптовой печати модуля «Тьютор»"
 * Имя сущности : tutorScriptItem
 * Файл data.xml : unitutor.data.xml
 */
public interface TutorScriptItemCodes
{
    /** Константа кода (code) элемента : Отчет «Расписание секций» (title) */
    String REPORT_SCHEDULE_OF_SPORT_SECTIONS = "report.scheduleOfSportSections";

    Set<String> CODES = ImmutableSet.of(REPORT_SCHEDULE_OF_SPORT_SECTIONS);
}
