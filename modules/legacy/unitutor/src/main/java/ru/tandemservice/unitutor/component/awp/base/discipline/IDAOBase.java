package ru.tandemservice.unitutor.component.awp.base.discipline;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAOBase<T> extends IUniDao<T> {
    void prepareCommonFilters4Tab(T model);
}
