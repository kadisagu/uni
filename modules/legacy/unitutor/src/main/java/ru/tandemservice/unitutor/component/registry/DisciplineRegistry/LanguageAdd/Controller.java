/* $Id$ */
package ru.tandemservice.unitutor.component.registry.DisciplineRegistry.LanguageAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Ekaterina Zvereva
 * @since 04.02.2016
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().saveOrUpdate(model.getRegistryElementExt());
        deactivate(component);
    }
}