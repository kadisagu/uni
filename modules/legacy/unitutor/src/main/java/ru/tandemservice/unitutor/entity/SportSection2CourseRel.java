package ru.tandemservice.unitutor.entity;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unitutor.entity.gen.*;

/** @see ru.tandemservice.unitutor.entity.gen.SportSection2CourseRelGen */
public class SportSection2CourseRel extends SportSection2CourseRelGen
{

    public SportSection2CourseRel()
    {
    }

    public SportSection2CourseRel(SportSection section, Course course)
    {
        this.setSection(section);
        this.setCourse(course);
    }
}