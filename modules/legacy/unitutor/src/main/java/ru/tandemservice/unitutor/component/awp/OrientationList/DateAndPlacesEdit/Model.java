package ru.tandemservice.unitutor.component.awp.OrientationList.DateAndPlacesEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;


import java.util.Collection;
import java.util.Date;
import java.util.List;

@Input(@Bind(key = "entityIds", binding = "entityIds"))
public class Model {

    private Integer countPlaces;
    private Date date;
    private Collection<Long> _entityIds;
    private List<Orientation2EduPlanVersion> _entityList;

    public Integer getCountPlaces() {
        return countPlaces;
    }

    public void setCountPlaces(Integer countPlaces) {
        this.countPlaces = countPlaces;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Collection<Long> getEntityIds()
    {
        return _entityIds;
    }

    public void setEntityIds(Collection<Long> entityIds)
    {
        _entityIds = entityIds;
    }

    public List<Orientation2EduPlanVersion> getEntityList()
    {
        return _entityList;
    }

    public void setEntityList(List<Orientation2EduPlanVersion> entityList)
    {
        _entityList = entityList;
    }
}
