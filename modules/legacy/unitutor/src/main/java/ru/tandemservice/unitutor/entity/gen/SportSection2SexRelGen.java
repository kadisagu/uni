package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.SportSection2SexRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь Спортивной секции и Пола
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SportSection2SexRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.SportSection2SexRel";
    public static final String ENTITY_NAME = "sportSection2SexRel";
    public static final int VERSION_HASH = 436036530;
    private static IEntityMeta ENTITY_META;

    public static final String L_SECTION = "section";
    public static final String L_SEX = "sex";

    private SportSection _section;     // Спортивная секция
    private Sex _sex;     // Пол

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Спортивная секция. Свойство не может быть null.
     */
    @NotNull
    public SportSection getSection()
    {
        return _section;
    }

    /**
     * @param section Спортивная секция. Свойство не может быть null.
     */
    public void setSection(SportSection section)
    {
        dirty(_section, section);
        _section = section;
    }

    /**
     * @return Пол. Свойство не может быть null.
     */
    @NotNull
    public Sex getSex()
    {
        return _sex;
    }

    /**
     * @param sex Пол. Свойство не может быть null.
     */
    public void setSex(Sex sex)
    {
        dirty(_sex, sex);
        _sex = sex;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SportSection2SexRelGen)
        {
            setSection(((SportSection2SexRel)another).getSection());
            setSex(((SportSection2SexRel)another).getSex());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SportSection2SexRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SportSection2SexRel.class;
        }

        public T newInstance()
        {
            return (T) new SportSection2SexRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "section":
                    return obj.getSection();
                case "sex":
                    return obj.getSex();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "section":
                    obj.setSection((SportSection) value);
                    return;
                case "sex":
                    obj.setSex((Sex) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "section":
                        return true;
                case "sex":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "section":
                    return true;
                case "sex":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "section":
                    return SportSection.class;
                case "sex":
                    return Sex.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SportSection2SexRel> _dslPath = new Path<SportSection2SexRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SportSection2SexRel");
    }
            

    /**
     * @return Спортивная секция. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection2SexRel#getSection()
     */
    public static SportSection.Path<SportSection> section()
    {
        return _dslPath.section();
    }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection2SexRel#getSex()
     */
    public static Sex.Path<Sex> sex()
    {
        return _dslPath.sex();
    }

    public static class Path<E extends SportSection2SexRel> extends EntityPath<E>
    {
        private SportSection.Path<SportSection> _section;
        private Sex.Path<Sex> _sex;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Спортивная секция. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection2SexRel#getSection()
     */
        public SportSection.Path<SportSection> section()
        {
            if(_section == null )
                _section = new SportSection.Path<SportSection>(L_SECTION, this);
            return _section;
        }

    /**
     * @return Пол. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection2SexRel#getSex()
     */
        public Sex.Path<Sex> sex()
        {
            if(_sex == null )
                _sex = new Sex.Path<Sex>(L_SEX, this);
            return _sex;
        }

        public Class getEntityClass()
        {
            return SportSection2SexRel.class;
        }

        public String getEntityName()
        {
            return "sportSection2SexRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
