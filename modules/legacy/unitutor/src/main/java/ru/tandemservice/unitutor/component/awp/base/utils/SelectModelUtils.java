package ru.tandemservice.unitutor.component.awp.base.utils;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.dao.IDaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.concat;
import static org.tandemframework.hibsupport.dql.DQLFunctions.upper;


/**
 * @author vch
 */
public class SelectModelUtils {


    @SuppressWarnings("rawtypes")
    public static DQLSelectBuilder getDisciplineListSelectModel(String alias, EducationYear educationYear, YearDistributionPart yearDistributionPart,
                                         List<EducationLevelsHighSchool> lstEducationLevelsHighSchool, List<Group> lstGroup, List<Employee> lstEmployee,
                                            List<OrgUnit> lstOrgUnit, List<OrgUnit> lstCathedraOrgUnit, String filter)
    {
//        DQLSelectBuilder dqlIn = DqlDisciplineBuilder.DqlInWorkPlanRegistryElementRow(educationYear, yearDistributionPart, lstEducationLevelsHighSchool,
//                lstGroup, null, lstEmployee, lstOrgUnit, lstCathedraOrgUnit, null, null);
//        dqlIn.column(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS).s());

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppRegistryElement.class, alias)
                .order(property(EppRegistryElement.title().fromAlias(alias)));
//                .where(in(property(EppRegistryElement.id().fromAlias(alias)), dqlIn.buildQuery()));
        if (!StringUtils.isEmpty(filter))
        {
            IDQLExpression expression = concat(property(alias, EppRegistryElement.title().s()),
                                                            property(alias, EppRegistryElement.number().s()));

            builder.where(like(upper(expression), value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));
        }

        return builder;
    }


    public static DQLFullCheckSelectModel getElectiveDisciplineListModel(final EducationYear educationYear, final YearDistributionPart yearDistributionPart,
                                     final List<EppEpvGroupImRow> eppEpvGroupImRowList, final List<Employee> lstEmployee, final List<OrgUnit> orgUnitList,
                                        final List<OrgUnit> cathedraOrgUnitLst, final ElectiveDiscipline excludeEd)
    {
        final DQLSelectBuilder dqlIn = DqlDisciplineBuilder.DqlInWorkPlanRegistryElementRow(educationYear, yearDistributionPart, null, null, null,
                lstEmployee, orgUnitList, cathedraOrgUnitLst, null, eppEpvGroupImRowList);
        dqlIn.column(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS).s());


        return new DQLFullCheckSelectModel(ElectiveDiscipline.registryElementPart().title().s())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(ElectiveDiscipline.class, alias)
                        .where(in(property(ElectiveDiscipline.registryElementPart().id().fromAlias(alias)), dqlIn.buildQuery()))
                                // ДПВ
                        .where(eqValue(property(ElectiveDiscipline.type().code().fromAlias(alias)), ElectiveDisciplineTypeCodes.DPV))
                        .order(property(ElectiveDiscipline.registryElementPart().registryElement().title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, ElectiveDiscipline.registryElementPart().registryElement().title(), filter);

                if (educationYear != null)
                    builder.where(eq(property(ElectiveDiscipline.educationYear().id().fromAlias(alias)), value(educationYear.getId())));


                if (yearDistributionPart != null)
                    builder.where(eq(property(ElectiveDiscipline.yearPart().id().fromAlias(alias)), value(yearDistributionPart.getId())));


                if (excludeEd != null)
                    builder.where(ne(property(ElectiveDiscipline.id().fromAlias(alias)), value(excludeEd.getId())));
                return builder;
            }

        };
    }


    @SuppressWarnings({"rawtypes"})
    public static List<IdentifiableWrapper> getEppEpvGroupImRowListSelectModel(ElectiveDiscipline electiveDiscipline, List<Employee> lstEmployee, List<OrgUnit> lstOrgUnit)
    {
        DQLSelectBuilder dql = DqlDisciplineBuilder.getDqlEppEpvGroupImRow(electiveDiscipline.getEducationYear(), electiveDiscipline.getYearPart(), lstEmployee,
                lstOrgUnit, electiveDiscipline.getRegistryElementPart().getRegistryElement());

        List<EppEpvGroupImRow> lst = UniDaoFacade.getCoreDao().getList(dql);

        List<IdentifiableWrapper> groupTitleList = new ArrayList<>();

        IDaoStudentSubscribe idao = DaoStudentSubscribe.instanse();
        for (EppEpvGroupImRow grp : lst) {
            String title = idao.getRowIndex(grp);
            groupTitleList.add(new IdentifiableWrapper(grp.getId(), title));
        }

        return groupTitleList;
    }

}
