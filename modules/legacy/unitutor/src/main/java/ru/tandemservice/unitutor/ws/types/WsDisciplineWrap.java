package ru.tandemservice.unitutor.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "disciplineWrap", namespace = "http://ws.unitutor.tandemservice.ru/")
public class WsDisciplineWrap implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * значение обязательно
     */
    private Long eppWorkPlanRegistryElementRowId;

    /**
     * может быть null
     */
    private Long electiveDisciplineId;

    /**
     * Название группы дисциплин
     */
    private String disciplineGroupTitle;

    /**
     * Если true - строка ошибочна (подробности в warningRow)
     */
    private boolean incorrectRow = false;
    private String warningRow;

    //////////////////////////////////////////////
    //////////// целиком от Вани
    //////////////////////////////////////////////
    private String disciplineTitle;
    private String disciplineNumber;
    private String term;

    private String totalHours;
    private String zet;
    private String formOfControl;
    private String placesToSubscribe;
    private String freePlacesToSubscribe;
    private String subscribedToDate;
    private boolean subscribed;
    /**
     * может быть null
     */
    private Long disciplineGroupId;
    //////////////////////////////////////////////
    //////////// конец куска от Вани
    //////////////////////////////////////////////

    /**
     * Используемя для определения источника записи
     * записали из ЛК или из сакая
     * Ваня говорит - Данилов или Базжина просили
     * Суть - если запись из Тандема, значит в сакае отказаться от такой ДПВ нельзя
     */
    private String sourceCode;

    private String rowIndex;

    /**
     * наличие аннотации у дисциплины
     */
    private boolean annotationExist;

    private String studentId;

    public WsDisciplineWrap() {
        // Пустой конструктор нужен Ивану Анищенко
        // иначе у него что-то падает на стороне сакая
    }

    public WsDisciplineWrap(
            Long eppWorkPlanRegistryElementRowId
            , Long electiveDisciplineId
            , String sourceCode
    )
    {
        this.eppWorkPlanRegistryElementRowId = eppWorkPlanRegistryElementRowId;
        this.electiveDisciplineId = electiveDisciplineId;
        this.sourceCode = sourceCode;

    }

    public Long getEppWorkPlanRegistryElementRowId() {
        return eppWorkPlanRegistryElementRowId;
    }

    public String getDisciplineGroupTitle() {
        return disciplineGroupTitle;
    }

    public void setDisciplineGroupTitle(String disciplineGroupTitle) {
        this.disciplineGroupTitle = disciplineGroupTitle;
    }

    public boolean isIncorrectRow() {
        return incorrectRow;
    }

    public void setIncorrectRow(boolean incorrectRow) {
        this.incorrectRow = incorrectRow;
    }

    public String getWarningRow() {
        return warningRow;
    }

    public void setWarningRow(String warningRow) {
        this.warningRow = warningRow;
    }

    public String getDisciplineTitle() {
        return disciplineTitle;
    }

    public void setDisciplineTitle(String disciplineTitle) {
        this.disciplineTitle = disciplineTitle;
    }

    public String getDisciplineNumber() {
        return disciplineNumber;
    }

    public void setDisciplineNumber(String disciplineNumber) {
        this.disciplineNumber = disciplineNumber;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public void setAnnotationExist(boolean annotationExist) {
        this.annotationExist = annotationExist;
    }

    public boolean isAnnotationExist() {
        return annotationExist;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getZet() {
        return zet;
    }

    public void setZet(String zet) {
        this.zet = zet;
    }

    public String getFormOfControl() {
        return formOfControl;
    }

    public void setFormOfControl(String formOfControl) {
        this.formOfControl = formOfControl;
    }

    public String getPlacesToSubscribe() {
        return placesToSubscribe;
    }

    public void setPlacesToSubscribe(String placesToSubscribe) {
        this.placesToSubscribe = placesToSubscribe;
    }

    public String getFreePlacesToSubscribe() {
        return freePlacesToSubscribe;
    }

    public void setFreePlacesToSubscribe(String freePlacesToSubscribe) {
        this.freePlacesToSubscribe = freePlacesToSubscribe;
    }

    public String getSubscribedToDate() {
        return subscribedToDate;
    }

    public void setSubscribedToDate(String subscribedToDate) {
        this.subscribedToDate = subscribedToDate;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    /**
     * Может быть NULL
     *
     * @return
     */
    public Long getDisciplineGroupId() {
        return disciplineGroupId;
    }

    public void setDisciplineGroupId(Long disciplineGroupId) {
        this.disciplineGroupId = disciplineGroupId;
    }

    public Long getElectiveDisciplineId() {
        return electiveDisciplineId;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }


    public String getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(String rowIndex) {
        this.rowIndex = rowIndex;
    }

    public String getStudentId()
    {
        return studentId;
    }

    public void setStudentId(String studentId)
    {
        this.studentId = studentId;
    }
}
