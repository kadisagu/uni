package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.SportSection2CourseRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь Спортивной секции и Курса
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SportSection2CourseRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.SportSection2CourseRel";
    public static final String ENTITY_NAME = "sportSection2CourseRel";
    public static final int VERSION_HASH = 2126199377;
    private static IEntityMeta ENTITY_META;

    public static final String L_SECTION = "section";
    public static final String L_COURSE = "course";

    private SportSection _section;     // Спортивная секция
    private Course _course;     // Курс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Спортивная секция. Свойство не может быть null.
     */
    @NotNull
    public SportSection getSection()
    {
        return _section;
    }

    /**
     * @param section Спортивная секция. Свойство не может быть null.
     */
    public void setSection(SportSection section)
    {
        dirty(_section, section);
        _section = section;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SportSection2CourseRelGen)
        {
            setSection(((SportSection2CourseRel)another).getSection());
            setCourse(((SportSection2CourseRel)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SportSection2CourseRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SportSection2CourseRel.class;
        }

        public T newInstance()
        {
            return (T) new SportSection2CourseRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "section":
                    return obj.getSection();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "section":
                    obj.setSection((SportSection) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "section":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "section":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "section":
                    return SportSection.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SportSection2CourseRel> _dslPath = new Path<SportSection2CourseRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SportSection2CourseRel");
    }
            

    /**
     * @return Спортивная секция. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection2CourseRel#getSection()
     */
    public static SportSection.Path<SportSection> section()
    {
        return _dslPath.section();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection2CourseRel#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends SportSection2CourseRel> extends EntityPath<E>
    {
        private SportSection.Path<SportSection> _section;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Спортивная секция. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection2CourseRel#getSection()
     */
        public SportSection.Path<SportSection> section()
        {
            if(_section == null )
                _section = new SportSection.Path<SportSection>(L_SECTION, this);
            return _section;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection2CourseRel#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return SportSection2CourseRel.class;
        }

        public String getEntityName()
        {
            return "sportSection2CourseRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
