package ru.tandemservice.unitutor.component.awp.base.discipline;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;

public abstract class ControllerBase<T extends ModelBase> extends AbstractBusinessController<IDAOBase<T>, T> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        T model = component.getModel();
        model.setSettings(component.getSettings());

        getDao().prepare(model);
        prepareDataSource(component);
    }

    protected abstract void prepareDataSource(IBusinessComponent component);

    protected void validateFilters(IBusinessComponent component)
    {
        T model = component.getModel();

        if (model.getSettings().get("educationYearPart") == null)
            throw new ApplicationException("Фильтр \"Часть года\" является обязательным для выбора");

        if (model.getSettings().get("educationYear") == null)
            throw new ApplicationException("Фильтр \"Учебный год\" является обязательным для выбора");
    }


    public void onClickSearch(IBusinessComponent component) {

        validateFilters(component);
        component.saveSettings();

        onRefreshComponent(component);
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.getSettings().clear();
        component.saveSettings();
        onRefreshComponent(component);
    }

}
