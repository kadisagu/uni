package ru.tandemservice.unitutor.dao;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.catalog.ElectiveDisciplineType;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DaoElectiveDiscipline extends UniBaseDao implements IDaoElectiveDiscipline {

    public static IDaoElectiveDiscipline daoElectiveDiscipline;

    public static IDaoElectiveDiscipline instanse() {

        if (daoElectiveDiscipline == null)
            daoElectiveDiscipline = (IDaoElectiveDiscipline) ApplicationRuntime.getBean(IDaoElectiveDiscipline.class.getName());
        return daoElectiveDiscipline;
    }

    @Override
    public void createOrDeleteElectiveDiscipline(EducationYear educationYear)
    {

        // 1 получим части элементов реестра, актуальные на указанный год
        DQLSelectBuilder dqlCheckWP = DqlDisciplineBuilder.getDqlRegistryElementPartCheck(educationYear);
        List<Object[]> lst = getList(dqlCheckWP);

        Map<MultiKey, MultiKey> keyMap = new HashMap<>();

        for (Object[] objs : lst) {
            Long registryElementPartId = (Long) objs[0];
            Long educationYearId = (Long) objs[1];
            Long partId = (Long) objs[2];

            MultiKey key = new MultiKey(new Object[]{
                    registryElementPartId,
                    educationYearId,
                    partId
            });

            keyMap.put(key, key);
        }


        DQLSelectBuilder dqlSumED = new DQLSelectBuilder()
                .fromEntity(ElectiveDiscipline.class, "ed")
                        // ДПВ
                .where(eqValue(property(ElectiveDiscipline.type().code().fromAlias("ed")), ElectiveDisciplineTypeCodes.DPV))

                .where(eq(property(ElectiveDiscipline.educationYear().fromAlias("ed")), value(educationYear)))
                .column(property("ed", ElectiveDiscipline.id()))

                .column(property("ed", ElectiveDiscipline.registryElementPart().id()))
                .column(property("ed", ElectiveDiscipline.educationYear().id()))
                .column(property("ed", ElectiveDiscipline.yearPart().id()))

                .column(property("ed", ElectiveDiscipline.actualRow()))
                .predicate(DQLPredicateType.distinct);

        lst = getList(dqlSumED);

        Map<MultiKey, MultiKey> keyMapExsist = new HashMap<>();

        for (Object[] objs : lst) {
            Long electiveDisciplineId = (Long) objs[0];
            Long registryElementPartId = (Long) objs[1];
            Long educationYearId = (Long) objs[2];
            Long partId = (Long) objs[3];

            boolean actualRow = (Boolean) objs[4];


            MultiKey key = new MultiKey(new Object[]{
                    registryElementPartId,
                    educationYearId,
                    partId
            });

            MultiKey keyFull = new MultiKey(new Object[]{
                    electiveDisciplineId,
                    registryElementPartId,
                    educationYearId,
                    partId,
                    actualRow
            });

            keyMapExsist.put(key, keyFull);
        }


        // 1 - нужно понять, какие записи добавляем, какие исправляем
        // просматриваем от необходимого набора
        List<MultiKey> rowToAdd = new ArrayList<>();
        List<MultiKey> rowToEdit = new ArrayList<>();
        List<MultiKey> rowToRemove = new ArrayList<>();


        for (MultiKey key : keyMap.keySet()) {
            if (!keyMapExsist.containsKey(key))
                // нужно добавить строчку
                rowToAdd.add(key);
            else {
                // может надо редактировать?
                MultiKey keyExsist = keyMapExsist.get(key);
                boolean isActual = (boolean) keyExsist.getKey(4);
                if (!isActual)
                    rowToEdit.add(keyExsist);
            }
        }

        // 2 - смотрим, что у нас лишнего
        for (MultiKey key : keyMapExsist.keySet()) {
            if (!keyMap.containsKey(key))
                rowToRemove.add(keyMapExsist.get(key));
        }


        /////////////////////////////////////
        //// манипуляции с electiveDiscipline
        /////////////////////////////////////


        // создаем новое
        for (MultiKey key : rowToAdd) {
            Long registryElementPartId = (Long) key.getKey(0);
            Long educationYearId = (Long) key.getKey(1);
            Long partId = (Long) key.getKey(2);

            ElectiveDiscipline ed = new ElectiveDiscipline();
            ed.setActualRow(true);
            // ДПВ
            ed.setType(getCatalogItem(ElectiveDisciplineType.class, ElectiveDisciplineTypeCodes.DPV));

            EppRegistryElementPart epp = new EppRegistryElementPart();
            epp.setId(registryElementPartId);
            ed.setRegistryElementPart(epp);

            EducationYear eYear = new EducationYear();
            eYear.setId(educationYearId);
            ed.setEducationYear(eYear);

            YearDistributionPart part = new YearDistributionPart();
            part.setId(partId);
            ed.setYearPart(part);

            saveOrUpdate(ed);
        }

        // редактируем (возврашаеи признак актуальности)
        for (MultiKey key : rowToEdit) {
            Long electiveDisciplineId = (Long) key.getKey(0);

            ElectiveDiscipline ed = get(electiveDisciplineId);
            ed.setActualRow(true);
            saveOrUpdate(ed);

        }


        // пытаемся удалить (нельзя удалять, ставим признак - не актуально)
        for (MultiKey key : rowToRemove) {
            Long electiveDisciplineId = (Long) key.getKey(0);
            ElectiveDiscipline ed = get(electiveDisciplineId);
            //ed.setActualRow(false);
            //saveOrUpdate(ed);

            // если записей на дисциплину нет - удалить
            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(ElectiveDisciplineSubscribe.class, "ed")

                    .where(eq(property(ElectiveDisciplineSubscribe.electiveDiscipline().id().fromAlias("ed")), value(electiveDisciplineId)))
                    .column(ElectiveDisciplineSubscribe.id().fromAlias("ed").s());

            List<Long> lstCheck = getList(dql);

            if (lstCheck == null || lstCheck.isEmpty()) {
                // удалить
                delete(ed);
            }
            else {
                // редактировать
                ed.setActualRow(false);
                saveOrUpdate(ed);
            }
        }
    }

}
