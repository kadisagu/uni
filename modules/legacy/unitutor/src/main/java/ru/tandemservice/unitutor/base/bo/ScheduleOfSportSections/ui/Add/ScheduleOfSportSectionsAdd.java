/* $Id$ */
package ru.tandemservice.unitutor.base.bo.ScheduleOfSportSections.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.logic.DefaultActiveCatalogComboDSHandler;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.SportSectRegistriesManager;

/**
 * @author Andrey Andreev
 * @since 05.09.2016
 */
@Configuration
public class ScheduleOfSportSectionsAdd  extends BusinessComponentManager
{
    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String EDUCATION_YEAR_PARAM = "educationYear";

    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String FORMATIVE_ORG_UNIT_LIST_PARAM = "formativeOrgUnitList";

    public static final String COURSE_DS = "courseDS";
    public static final String COURSE_LIST_PARAM = "courseList";

    public static final String SEX_DS = "sexDS";
    public static final String SEX_PARAM = "sex";

    public static final String PLACE_DS = "placeDS";
    public static final String PLACE_LIST_PARAM = "placeList";

    public static final String WITH_NUMBERS_PARAM = "withNumbers";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(SEX_DS, sexDSHandler()))
                .addDataSource(selectDS(PLACE_DS, SportSectRegistriesManager.instance().sportPlaceDSHandler())
                                       .addColumn("title", "", source -> ((UniplacesPlace) source).getTitleWithLocation()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return Course.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sexDSHandler()
    {
        return new DefaultActiveCatalogComboDSHandler(this.getName(), Sex.class);
    }
}