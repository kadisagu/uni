package ru.tandemservice.unitutor.base.ext.Employee.ui.PostView;


import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class EmployeePostViewUI extends org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI {

    private List<Long> orgUnitIds;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        if (getEmployeePost() != null)
            setOrgUnitIds(OptionalDisciplineUtil.getOrgUnitList4Tutor(getEmployeePost().getEmployee()));
    }

    public List<Long> getOrgUnitIds() {
        return orgUnitIds;
    }

    public void setOrgUnitIds(List<Long> orgUnitIds) {
        this.orgUnitIds = orgUnitIds;
    }

    public boolean isTutor() {
        return  (getOrgUnitIds() != null && getOrgUnitIds().size() > 0);
    }

    public Map<String, Object> getEmployeePostParam() {
        Map<String, Object> params = new Hashtable<String, Object>();
        if (getEmployeePost() != null)
            params.put("employeePostId", getEmployeePost().getId());

        return params;
    }
}
