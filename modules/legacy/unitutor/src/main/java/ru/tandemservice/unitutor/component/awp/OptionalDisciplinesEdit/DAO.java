package ru.tandemservice.unitutor.component.awp.OptionalDisciplinesEdit;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;
import ru.tandemservice.unitutor.entity.catalog.ElectiveDisciplineType;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {

        model.setDisciplinePart(getNotNull(EppRegistryElementPart.class, model.getRegistryElementPartId()));

        model.setEducationYear(getNotNull(EducationYear.class, model.getEducationYearId()));

        model.setYearPart(getNotNull(YearDistributionPart.class, model.getYearDistributionPartId()));

        model.setTotalSize(Model.formatter.format(model.getDisciplinePart().getSize()));
        model.setTotalLabor(Model.formatter.format(model.getDisciplinePart().getLabor()));

        EppRegistryElementExt registryElementExt = get(EppRegistryElementExt.class, EppRegistryElementExt.eppRegistryElement().id().s(), model.getDisciplinePart().getRegistryElement().getId());
        if (registryElementExt == null)
            registryElementExt = new EppRegistryElementExt(model.getDisciplinePart().getRegistryElement());

        model.setRegistryElementExt(registryElementExt);

        if (model.getElectiveDisciplineId() == null) {
            ElectiveDiscipline electiveDiscipline = new ElectiveDiscipline(model.getDisciplinePart(), model.getEducationYear());
            electiveDiscipline.setType(getCatalogItem(ElectiveDisciplineType.class, ElectiveDisciplineTypeCodes.DPV));
            model.setElectiveDiscipline(electiveDiscipline);
        }
        else
            model.setElectiveDiscipline(getNotNull(ElectiveDiscipline.class, model.getElectiveDisciplineId()));

        model.setFormsOfControl(OptionalDisciplineUtil.getFormsOfControlTitles(model.getDisciplinePart().getId()));

        if (model.getElectiveDiscipline().isSetTotalPlacesToSubscribe()) {
            if (model.getElectiveDiscipline().getPlacesCount() != 0)
                model.setPlacesFree(String.valueOf(model.getElectiveDiscipline().getFreePlacesToSubscribe()));
            else model.setPlacesFree("");
            model.setPlacesTotal(String.valueOf(model.getElectiveDiscipline().getPlacesCount()));
        }
        else {
            model.setPlacesTotal("");
            model.setPlacesFree("");
        }
        model.setSubscribed(String.valueOf(model.getElectiveDiscipline().getSubscribersCount()));
    }

    @Override
    public void update(Model model) {
        if (!StringUtils.isEmpty(model.getPlacesTotal()))
            model.getElectiveDiscipline().setPlacesCount(Long.parseLong(model.getPlacesTotal()));

        saveOrUpdate(model.getElectiveDiscipline());
        saveOrUpdate(model.getRegistryElementExt());

    }
}
