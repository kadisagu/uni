/* $Id$ */
package ru.tandemservice.unitutor.ws.types;

import ru.tandemservice.uni.entity.employee.Student;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Ekaterina Zvereva
 * @since 09.11.2015
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "studentWrap", namespace = "http://ws.unitutor.tandemservice.ru/")
public class WsStudentWrap implements Serializable
{
    private static final long serialVersionUID = -7616719337019967700L;

    private Long studentId;

    private String fio;
    private String educationOrgUnit;
    private String group;

    private String course;

    public WsStudentWrap()
    {

    }


    public WsStudentWrap(Student student)
    {
        this.fio = student.getFio();
        this.studentId = student.getId();
        this.educationOrgUnit = student.getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
        this.course = student.getCourse().getTitle();
        this.group = student.getGroup() == null ? "" : student.getGroup().getFullTitle();
    }

    public Long getStudentId()
    {
        return studentId;
    }

    public void setStudentId(Long studentId)
    {
        this.studentId = studentId;
    }

    public String getFio()
    {
        return fio;
    }

    public void setFio(String fio)
    {
        this.fio = fio;
    }

    public String getEducationOrgUnit()
    {
        return educationOrgUnit;
    }

    public void setEducationOrgUnit(String educationOrgUnit)
    {
        this.educationOrgUnit = educationOrgUnit;
    }

    public String getGroup()
    {
        return group;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public String getCourse()
    {
        return course;
    }

    public void setCourse(String course)
    {
        this.course = course;
    }
}