package ru.tandemservice.unitutor.component.awp.StudentSubscribesList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unitutor.base.bo.MassStudentSubscribe.ui.Add.MassStudentSubscribeAdd;
import ru.tandemservice.unitutor.component.awp.base.IFilterProperties;
import ru.tandemservice.unitutor.component.awp.base.discipline.ControllerBase;
import ru.tandemservice.unitutor.dao.print.IElectiveApplicationPrintDAO;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;

import java.util.*;

public class Controller extends ControllerBase<Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);
        Model model = component.getModel();
        if (model.getDataSource() != null)
            model.getDataSource().getColumn("select").setVisible(model.isCanCheck());
    }

    @Override
    protected void prepareDataSource(IBusinessComponent component) {

        final Model model = component.getModel();

        DynamicListDataSource<DataWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new CheckboxColumn("select").setVisible(model.isCanCheck()));

        dataSource.addColumn(new SimpleColumn("Название", ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().registryElement().title()).setClickable(true).setOrderable(true));

        dataSource.addColumn(new PublisherLinkColumn("№ в реестре", ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().registryElement().number())
                                     .setResolver(new IPublisherLinkResolver()
                                     {
                                         @Override
                                         public Object getParameters(IEntity entity)
                                         {
                                             final Map<String, Object> parameters = new HashMap<>();
                                             DataWrapper wrap = (DataWrapper) entity;
                                             parameters.put(PublisherActivator.PUBLISHER_ID_KEY, ((ElectiveDisciplineSubscribe) wrap.getWrapped()).getElectiveDiscipline().getRegistryElementPart().getRegistryElement().getId());
                                             return parameters;
                                         }

                                         @Override
                                         public String getComponentName(IEntity paramIEntity)
                                         {
                                             return null;
                                         }
                                     })
                                     .setClickable(true)
                                     .setOrderable(true));

        if (model.isDpvType())
            dataSource.addColumn(new SimpleColumn("Индекс", DAO.WORK_PLAN_ROW_ALIAS + "." + EppWorkPlanRegistryElementRow.number()).setOrderable(true));

        dataSource.addColumn(new PublisherLinkColumn("ФИО студента", ElectiveDisciplineSubscribe.student().person().identityCard().fullFio())
                                     .setResolver(new IPublisherLinkResolver() {
                                         @Override
                                         public Object getParameters(IEntity entity) {
                                             final Map<String, Object> parameters = new HashMap<>();
                                             DataWrapper wrap = (DataWrapper) entity;
                                             parameters.put(PublisherActivator.PUBLISHER_ID_KEY, ((ElectiveDisciplineSubscribe) wrap.getWrapped()).getStudent().getId());
                                             return parameters;
                                         }

                                         @Override
                                         public String getComponentName(IEntity paramIEntity) {
                                             return null;
                                         }
                                     })
                                     .setWidth("150")
                                     .setClickable(true)
                                     .setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Группа", ElectiveDisciplineSubscribe.student().group().title()));

        dataSource.addColumn(new SimpleColumn("РУП", DAO.WORK_PLAN_ALIAS + "." + EppWorkPlanBase.title()));

        dataSource.addColumn(new BooleanColumn("Актуальность", ElectiveDisciplineSubscribe.electiveDiscipline().actualRow()).setOrderable(false));

        dataSource.addColumn(new DateColumn("Дата и время записи", ElectiveDisciplineSubscribe.subscribeOn()).setOrderable(false).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Источник записи", ElectiveDisciplineSubscribe.source()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Дата утраты актуальности", ElectiveDisciplineSubscribe.removalDate(), new DateFormatter(DateFormatter.PATTERN_WITH_TIME))
                                     .setOrderable(false).setClickable(false));
       

        dataSource.addColumn(new ActionColumn("Переместить", "daemon", "onClickMove")
                                     .setPermissionKey(model.getSec().getPermission("move"))
                                     .setDisableHandler(entity -> !model.isDpvType()));

        model.setDataSource(dataSource);
    }

    public void onClickStudentSubscribesChoose(IBusinessComponent component) {
        Model model = component.getModel();
        model.setCanCheck(true);
        model.getDataSource().getColumn("select").setVisible(model.isCanCheck());
        model.getDataSource().refresh();
    }

    public void onClickStudentSubscribesHideChoose(IBusinessComponent component) {
        Model model = component.getModel();
        model.setCanCheck(false);
        model.getDataSource().getColumn("select").setVisible(model.isCanCheck());
        model.getDataSource().refresh();
    }

    public void onClickMove(IBusinessComponent component)
    {
        // типа редактирование
        // Ванин контрол в сад

        Model model = component.getModel();
        Long id = model.getDataSource().getLastVisitedEntityId();
        ElectiveDisciplineSubscribe subscribe = ((DataWrapper)model.getDataSource().getRecordById(id)).getWrapped();

        ParametersMap map = new ParametersMap();
        map.add("electiveDisciplineSubscribeId", subscribe.getId());
        map.add("electiveDisciplineTypeCode", subscribe.getElectiveDiscipline().getType().getCode());

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(ru.tandemservice.unitutor.component.awp.StudentSubscribesAddEdit.Controller.class.getPackage().getName(), map));

    }


    public void onClickMassStudentSubscribe(IBusinessComponent component)
    {
        Model model = component.getModel();
        ParametersMap params = new ParametersMap();

        EducationYear year = model.getSettings().get(IFilterProperties.EDUCATION_YEAR_FILTER);
        if (year == null)
            throw new ApplicationException("Поле Учебный год обязательно для заполнения");
        params.put("educationYearId", year.getId());

        YearDistributionPart educationYearPart = model.getSettings().get(IFilterProperties.EDUCATION_YEAR_PART_FILTER);
        if (educationYearPart == null)
            throw new ApplicationException("Поле часть года обязательно для заполнения");

        params.put("yearDistributionPartId", educationYearPart.getId());

        params.put("electiveDisciplineTypeCode", model.getFilterElectiveDisciplineType().getCode());

        params.put("orgUnitId", model.getOrgUnitId());

        CAFLegacySupportService.asRegion(MassStudentSubscribeAdd.class.getSimpleName(), UIDefines.DEFAULT_REGION_NAME, (BusinessComponent) component).parameters(params).top().activate();
    }

    public void onClickStudentSubscribesDelete(IBusinessComponent component) {
        Model model = component.getModel();
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать записи для удаления ! ");

        ((IDAO) getDao()).deleteElectiveDisciplineSubscribe(CommonBaseEntityUtil.getIdList(entityList));
        model.getDataSource().refresh();
    }

    public void onClickStudentSubscribesPrint(IBusinessComponent component) {
        Model model = component.getModel();
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать записи для печати заявлений на ДПВ ! ");

        List<ElectiveDisciplineSubscribe> electiveDisciplineSubscribes = new ArrayList<>();

        for (IEntity entity : entityList) {
            ElectiveDisciplineSubscribe electiveDisciplineSubscribe = ((DataWrapper)entity).getWrapped();

            // запись актуальна
            if (electiveDisciplineSubscribe.getRemovalDate() == null)
                electiveDisciplineSubscribes.add(electiveDisciplineSubscribe);
        }

        IElectiveApplicationPrintDAO electiveApplicationPrintDAO = (IElectiveApplicationPrintDAO) ApplicationRuntime.getBean(IElectiveApplicationPrintDAO.class.getName());
        RtfDocument document = electiveApplicationPrintDAO.generateDocument(electiveDisciplineSubscribes);

        byte[] bytes = RtfUtil.toByteArray(document);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("ElectiveApplications.rtf").document(bytes), false);
    }

    public void onClickStudentSubscribeAdd(IBusinessComponent component)
    {
        Model model = component.getModel();
        ParametersMap params = new ParametersMap();

        if (model.getEmployeeId() != null)
            params.put("tutorId", model.getEmployeeId());
        else
            params.put("orgUnitIds", Collections.singletonList(model.getOrgUnitId()));

        EducationYear year = model.getSettings().get(IFilterProperties.EDUCATION_YEAR_FILTER);
        if (year == null)
            throw new ApplicationException("Поле Учебный год обязательно для заполнения");
        params.put("educationYearId", year.getId());

        YearDistributionPart educationYearPart = model.getSettings().get(IFilterProperties.EDUCATION_YEAR_PART_FILTER);
        if (educationYearPart == null)
            throw new ApplicationException("Поле часть года обязательно для заполнения");

        params.put("yearDistributionPartId", educationYearPart.getId());

        params.put("electiveDisciplineTypeCode", model.getFilterElectiveDisciplineType().getCode());

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(ru.tandemservice.unitutor.component.awp.StudentSubscribesAddEdit.Controller.class.getPackage().getName(),
                                                          params));
    }

}
