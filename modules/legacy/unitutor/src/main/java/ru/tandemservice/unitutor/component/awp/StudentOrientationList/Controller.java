package ru.tandemservice.unitutor.component.awp.StudentOrientationList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unitutor.entity.Student2Orientation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);

        if (model.getDataSource() != null) return;

        DynamicListDataSource<Student2Orientation> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Название направленности", Student2Orientation.orientation().educationLevelHighSchool().title()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", Student2Orientation.orientation().educationLevelHighSchool().educationLevel().parentLevel().fullTitleWithRootLevel()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Характеристики обучения", Student2Orientation.orientation().developCombinationTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new PublisherLinkColumn("ФИО студента",
                                                     Student2Orientation.student().person().identityCard().fullFio())
                                     .setResolver(new IPublisherLinkResolver() {
                                         @Override
                                         public Object getParameters(IEntity entity) {
                                             final Map<String, Object> parameters = new HashMap<>();
                                             parameters.put(PublisherActivator.PUBLISHER_ID_KEY, ((Student2Orientation) entity).getStudent().getId());
                                             return parameters;
                                         }

                                         @Override
                                         public String getComponentName(IEntity paramIEntity) {
                                             return null;
                                         }
                                     })
                                     .setClickable(true)
                                     .setOrderable(true));
        dataSource.addColumn(new BooleanColumn("Актуальность", Student2Orientation.actual()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата и время записи", Student2Orientation.date(), DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник записи", Student2Orientation.source()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата утраты актуальности", Student2Orientation.removalDate(), DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false));
        dataSource.addColumn(new ActionColumn("Переместить", "daemon", "onClickMove").setPermissionKey(model.getSec().getPermission("move_studentOrientation_list")));
        model.setDataSource(dataSource);
    }

    public void onClickMove(IBusinessComponent component) {
        Model model = getModel(component);
        ParametersMap map = new ParametersMap();
        map.put("orgUnitId", model.getOrgUnitId());
        map.put("studentId", component.getListenerParameter());
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(ru.tandemservice.unitutor.component.awp.StudentOrientationAdd.Controller.class.getPackage().getName(), map));
    }

    public void onClickAdd(IBusinessComponent component) {
        Model model = getModel(component);
        ParametersMap map = new ParametersMap();
        map.put("orgUnitId", model.getOrgUnitId());
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(ru.tandemservice.unitutor.component.awp.StudentOrientationAdd.Controller.class.getPackage().getName(), map));
    }

    public void onClickDelete(IBusinessComponent component) {
        Model model = getModel(component);
        Collection<IEntity> lst = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();
        if (lst.isEmpty())
            throw new ApplicationException("Не выбраны записи для удаления.");
        getDao().deleteRecords(lst);
        ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects().clear();
        onClickSearch(component);
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = getModel(component);
        component.saveSettings();
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.getSettings().clear();
        getDao().prepare(model);
        onClickSearch(component);
    }

    public void onClickMassAdd(IBusinessComponent component) {
        Model model = getModel(component);
        ParametersMap map = new ParametersMap();
        map.put("orgUnitId", model.getOrgUnitId());
        component.createDefaultChildRegion(
                new ComponentActivator(ru.tandemservice.unitutor.component.awp.StudentOrientationMassAdd.Controller.class.getPackage().getName(), map));
    }
}
