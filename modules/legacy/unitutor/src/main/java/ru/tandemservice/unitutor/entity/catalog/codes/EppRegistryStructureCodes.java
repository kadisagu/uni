package ru.tandemservice.unitutor.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Структура реестра"
 * Имя сущности : eppRegistryStructure
 * Файл data.xml : unitutor.data.xml
 */
public interface EppRegistryStructureCodes
{
    /** Константа кода (code) элемента : Дисциплины (title) */
    String REGISTRY_DISCIPLINE = "eppRegistryDiscipline";
    /** Константа кода (code) элемента : Практики (title) */
    String REGISTRY_PRACTICE = "eppRegistryPractice";
    /** Константа кода (code) элемента : Учебная практика (title) */
    String REGISTRY_PRACTICE_TUTORING = "eppRegistryPractice.1";
    /** Константа кода (code) элемента : Производственная практика (title) */
    String REGISTRY_PRACTICE_PRODUCTION = "eppRegistryPractice.2";
    /** Константа кода (code) элемента : Преддипломная практика (title) */
    String REGISTRY_PRACTICE_PRE_DIPLOMA = "eppRegistryPractice.3";
    /** Константа кода (code) элемента : Прочая практика (title) */
    String REGISTRY_PRACTICE_PRE_OTHER = "eppRegistryPractice.4";
    /** Константа кода (code) элемента : Мероприятия ГИА (title) */
    String REGISTRY_ATTESTATION = "eppRegistryAttestation";
    /** Константа кода (code) элемента : Защита выпускной квалификационной работы (title) */
    String REGISTRY_ATTESTATION_DIPLOMA = "eppRegistryAttestation.1";
    /** Константа кода (code) элемента : Государственный экзамен (title) */
    String REGISTRY_ATTESTATION_EXAM = "eppRegistryAttestation.2";
    /** Константа кода (code) элемента : Общеуниверситетский курс по выбору (title) */
    String OBTSHEUNIVERSITETSKIY_KURS_PO_VYBORU = "eppRegistryDiscipline.tutor.1";
    /** Константа кода (code) элемента : Общеуниверситетский факультатив (title) */
    String OBTSHEUNIVERSITETSKIY_FAKULTATIV = "eppRegistryDiscipline.tutor.2";

    Set<String> CODES = ImmutableSet.of(REGISTRY_DISCIPLINE, REGISTRY_PRACTICE, REGISTRY_PRACTICE_TUTORING, REGISTRY_PRACTICE_PRODUCTION, REGISTRY_PRACTICE_PRE_DIPLOMA, REGISTRY_PRACTICE_PRE_OTHER, REGISTRY_ATTESTATION, REGISTRY_ATTESTATION_DIPLOMA, REGISTRY_ATTESTATION_EXAM, OBTSHEUNIVERSITETSKIY_KURS_PO_VYBORU, OBTSHEUNIVERSITETSKIY_FAKULTATIV);
}
