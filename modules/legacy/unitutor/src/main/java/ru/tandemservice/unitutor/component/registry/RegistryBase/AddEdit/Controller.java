package ru.tandemservice.unitutor.component.registry.RegistryBase.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().update(model);
        deactivate(component);
    }

    @Override
    public void deactivate(IBusinessComponent component) {
        super.deactivate(component);
    }

    public void onChangeEduYear(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().onChangeEduYear(model);
    }

}
