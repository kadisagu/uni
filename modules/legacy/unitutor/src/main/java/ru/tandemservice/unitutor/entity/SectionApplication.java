package ru.tandemservice.unitutor.entity;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitutor.entity.catalog.codes.ApplicationStatus4SportsSectionCodes;
import ru.tandemservice.unitutor.entity.catalog.gen.ApplicationStatus4SportsSectionGen;
import ru.tandemservice.unitutor.entity.gen.*;

import java.util.Date;

/**
 * @see ru.tandemservice.unitutor.entity.gen.SectionApplicationGen
 */
public class SectionApplication extends SectionApplicationGen
{

    public SectionApplication()
    {
    }

    public SectionApplication(boolean fromUni)
    {
        this.setFromUni(fromUni);
        this.setStatus(DataAccessServices.dao().getByNaturalId(new ApplicationStatus4SportsSectionGen.NaturalId(ApplicationStatus4SportsSectionCodes.ACCEPTABLE)));
        this.setRegistrationDate(new Date());
    }

    public SectionApplication(SportSection sportSection, Student student, boolean fromUni)
    {
        this.setSportSection(sportSection);
        this.setStudent(student);
        this.setFromUni(fromUni);
        this.setStatus(DataAccessServices.dao().getByNaturalId(new ApplicationStatus4SportsSectionGen.NaturalId(ApplicationStatus4SportsSectionCodes.ACCEPTABLE)));
        this.setRegistrationDate(new Date());
    }
}