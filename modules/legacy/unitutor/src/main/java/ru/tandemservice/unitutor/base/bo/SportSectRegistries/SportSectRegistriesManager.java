/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SportSectRegistries;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniplaces.entity.catalog.codes.UniplacesPlacePurposeCodes;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.util.DayOfWeekDsHandler;
import ru.tandemservice.unitutor.entity.SportSection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 22.08.2016
 */
@Configuration
public class SportSectRegistriesManager extends BusinessObjectManager
{
    public static SportSectRegistriesManager instance()
    {
        return instance(SportSectRegistriesManager.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> dayOfWeekDSHandler()
    {
        return new DayOfWeekDsHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sportPlaceDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesPlace.class)
                .customize((alias, dql, context, filter) -> dql.where(eq(property(alias, UniplacesPlace.purpose().code()),
                                                                         value(UniplacesPlacePurposeCodes.OBEKT_FIZKULTURY_I_SPORTA))))
                .order(UniplacesPlace.title())
                .filter(UniplacesPlace.title())
                .pageable(true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trainerDSHandler()
    {
        return PpsEntry.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> dql.where(exists(SportSection.class, SportSection.trainer().s(), property(alias))));
    }
}