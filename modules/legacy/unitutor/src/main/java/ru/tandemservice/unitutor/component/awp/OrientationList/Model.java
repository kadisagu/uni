package ru.tandemservice.unitutor.component.awp.OrientationList;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;

import java.util.List;

@Input({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "orgUnitHolder.id")})
@State({@org.tandemframework.core.component.Bind(key = "selectedTab", binding = "selectedTab")})
public class Model {

    public final static String PARENT_FILTER = "parentFilter";
    public final static String ORIENTATION_FILTER = "orientationFilter";
    public final static String COURSE_FILTER = "courseFilter";
    public final static String PLACES_FILTER = "placesFilter";
    public final static String ACTUAL_FILTER = "actualFilter";
    public final static String EDU_YEAR_FILTER = "eduYearFilter";
    public final static String ALLOW_ENTRY_FILTER = "allowEntryFilter";
    public final static String EDU_PLAN_FILTER = "eduPlanFilter";

    public final static Long YES = 1L;
    public final static Long NO = 2L;

    public final static List<IdentifiableWrapper> YES_NO_ITEMS = ImmutableList.of(
            new IdentifiableWrapper(YES, "Да"),
            new IdentifiableWrapper(NO, "Нет")
    );

    private IDataSettings settings;
    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    private String selectedTab;
    private DynamicListDataSource<Orientation2EduPlanVersion> dataSource;
    private ISelectModel parentModel;
    private ISelectModel orientationModel;
    private ISelectModel courseModel;
    private ISelectModel freePlaceSelectModel;
    private ISelectModel actualSelectModel;
    private ISelectModel eduYearModel;
    private ISelectModel allowEntryModel;
    private ISelectModel eduPlanModel;

    public EducationYear getEducationYear() {
        return getSettings().get(EDU_YEAR_FILTER);
    }

    public List<EducationLevelsHighSchool> getParentsEHS() {
        return getSettings().get(PARENT_FILTER);
    }

    public List<EppEduPlanVersion> getEduPlanVersions() {
        return getSettings().get(EDU_PLAN_FILTER);
    }

    public List<EducationLevelsHighSchool> getOrientationsEHS() {
        return getSettings().get(ORIENTATION_FILTER);
    }

    public List<Course> getCourse() {
        return getSettings().get(COURSE_FILTER);
    }

    public Boolean isFreePlaces() {
        if (getSettings().get(PLACES_FILTER) == null)
            return null;
        else
            return ((IdentifiableWrapper) getSettings().get(PLACES_FILTER)).getId().equals(YES);
    }

    public Boolean isActual() {
        if (getSettings().get(ACTUAL_FILTER) == null)
            return null;
        else
            return ((IdentifiableWrapper) getSettings().get(ACTUAL_FILTER)).getId().equals(YES);
    }

    public Boolean isAllowEntry() {
        if (getSettings().get(ALLOW_ENTRY_FILTER) == null)
            return null;
        else
            return ((IdentifiableWrapper) getSettings().get(ALLOW_ENTRY_FILTER)).getId().equals(YES);
    }

    public ISelectModel getFreePlaceSelectModel() {
        return freePlaceSelectModel;
    }

    public void setFreePlaceSelectModel(ISelectModel freePlaceSelectModel) {
        this.freePlaceSelectModel = freePlaceSelectModel;
    }

    public ISelectModel getActualSelectModel() {
        return actualSelectModel;
    }

    public void setActualSelectModel(ISelectModel actualSelectModel) {
        this.actualSelectModel = actualSelectModel;
    }

    public ISelectModel getParentModel() {
        return parentModel;
    }

    public void setParentModel(ISelectModel parentModel) {
        this.parentModel = parentModel;
    }

    public ISelectModel getOrientationModel() {
        return orientationModel;
    }

    public void setOrientationModel(ISelectModel orientationModel) {
        this.orientationModel = orientationModel;
    }

    public ISelectModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel) {
        this.courseModel = courseModel;
    }

    public Object getSecuredObject()
    {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public OrgUnitHolder getOrgUnitHolder()
    {
        return this.orgUnitHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOrgUnitHolder().getValue();
    }

    public Long getOrgUnitId()
    {
        return getOrgUnitHolder().getId();
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOrgUnitHolder().getSecModel();
    }

    public String getSelectedTab()
    {
        return this.selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<Orientation2EduPlanVersion> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<Orientation2EduPlanVersion> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getEduYearModel() {
        return eduYearModel;
    }

    public void setEduYearModel(ISelectModel eduYearModel) {
        this.eduYearModel = eduYearModel;
    }

    public ISelectModel getAllowEntryModel() {
        return allowEntryModel;
    }

    public void setAllowEntryModel(ISelectModel allowEntryModel) {
        this.allowEntryModel = allowEntryModel;
    }

    public ISelectModel getEduPlanModel() {
        return eduPlanModel;
    }

    public void setEduPlanModel(ISelectModel eduPlanModel) {
        this.eduPlanModel = eduPlanModel;
    }
}
