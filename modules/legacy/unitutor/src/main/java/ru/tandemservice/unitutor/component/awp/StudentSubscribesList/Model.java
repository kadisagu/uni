package ru.tandemservice.unitutor.component.awp.StudentSubscribesList;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unitutor.component.awp.base.IFilterProperties;
import ru.tandemservice.unitutor.component.awp.base.discipline.ModelBase;
import ru.tandemservice.unitutor.entity.catalog.ElectiveDisciplineType;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;

import java.util.List;


public class Model extends ModelBase
{

    private final static String SECURITY_POSTFIX = "optionalDisciplineSubscribe_list";
    public final static Long ALL = 1L;
    public final static Long ONLY_RELEVANT = 2L;

    public static final String PROPERTY_SUBSCRIBE = "subscribe";


    private ISelectModel relevantSelectModel;

    private ISelectModel actualElectiveDisciplineModel;

    private ISelectModel studentSelectModel;
    private ISelectModel electiveDisciplineTypeModel;

    private boolean isCanCheck;
    private DynamicListDataSource<DataWrapper> dataSource;

    public final static List<IdentifiableWrapper> RELEVANT_LIST = ImmutableList.of(
            new IdentifiableWrapper(ALL, "Все"),
            new IdentifiableWrapper(ONLY_RELEVANT, "Только актуальные")
    );

    @Override
    protected String getSecurityPostfix() {
        return SECURITY_POSTFIX;
    }

    public ISelectModel getRelevantSelectModel() {
        return relevantSelectModel;
    }

    public void setRelevantSelectModel(ISelectModel relevantSelectModel) {
        this.relevantSelectModel = relevantSelectModel;
    }

    public ISelectModel getStudentSelectModel() {
        return studentSelectModel;
    }

    public void setStudentSelectModel(ISelectModel studentSelectModel) {
        this.studentSelectModel = studentSelectModel;
    }

    public boolean isCanCheck() {
        return isCanCheck;
    }

    public void setCanCheck(boolean isCanCheck) {
        this.isCanCheck = isCanCheck;
    }

    public DynamicListDataSource<DataWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<DataWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getActualElectiveDisciplineModel() {
        return actualElectiveDisciplineModel;
    }

    public void setActualElectiveDisciplineModel(
            ISelectModel actualElectiveDisciplineModel)
    {
        this.actualElectiveDisciplineModel = actualElectiveDisciplineModel;
    }

    public ISelectModel getElectiveDisciplineTypeModel() {
        return electiveDisciplineTypeModel;
    }

    public void setElectiveDisciplineTypeModel(ISelectModel electiveDisciplineTypeModel) {
        this.electiveDisciplineTypeModel = electiveDisciplineTypeModel;
    }

    public ElectiveDisciplineType getFilterElectiveDisciplineType() {
        return getSettings().get(IFilterProperties.ELECTIVE_DISCIPLINE_TYPE_FILTER);
    }

    public boolean isDpvType() {
        return ElectiveDisciplineTypeCodes.DPV.equals(getFilterElectiveDisciplineType().getCode());
    }

}