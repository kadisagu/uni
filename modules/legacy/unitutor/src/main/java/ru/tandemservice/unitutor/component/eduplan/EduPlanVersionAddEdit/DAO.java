package ru.tandemservice.unitutor.component.eduplan.EduPlanVersionAddEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.unitutor.entity.EppEduPlanVersionExt;

public class DAO extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));

        if (null == model.getElement().getTitlePostfix()) {
            model.getElement().setTitlePostfix("" + model.getElement().getEduPlan().getEduStartYear());
        }

        EppEduPlanVersionExt eduPlanVersionExt = get(EppEduPlanVersionExt.class, EppEduPlanVersionExt.eduPlanVersion().s(), model.getElement());
        if (eduPlanVersionExt == null) {
            eduPlanVersionExt = new EppEduPlanVersionExt();
            eduPlanVersionExt.setEduPlanVersion(model.getElement());
            if (isSpecialist(model)) {
                eduPlanVersionExt.setCourse(DevelopGridDAO.getCourseMap().get(2));
            }

            if (isBakalavr(model)) {
                eduPlanVersionExt.setCourse(DevelopGridDAO.getCourseMap().get(3));
            }

        }

        myModel.setEduPlanVersionExt(eduPlanVersionExt);
    }

    @Override
    public void save(ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model model) {
        super.save(model);
        Model myModel = (Model) model;
        getSession().saveOrUpdate(myModel.getEduPlanVersionExt());
    }

    private boolean isBakalavr(ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model model) {
        return model.getElement().getEduPlan().getProgramKind().isProgramBachelorDegree();
//TODO DEV-6870
//		Qualifications qualifications = model.getElement().getEduPlan().getEducationLevelHighSchool().getEducationLevel().getQualification();
//
//		if (qualifications != null){
//			return QualificationsCodes.BAKALAVR.equals(qualifications.getCode());
//		}
//
//		return false;
    }

    private boolean isSpecialist(ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model model) {
        return model.getElement().getEduPlan().getProgramKind().isProgramSpecialistDegree();
//TODO DEV-6870
//		Qualifications qualifications = model.getElement().getEduPlan().getEducationLevelHighSchool().getEducationLevel().getQualification();
//
//		if (qualifications != null){
//			return QualificationsCodes.SPETSIALIST.equals(qualifications.getCode());
//		}
//
//		return false;
    }
}
