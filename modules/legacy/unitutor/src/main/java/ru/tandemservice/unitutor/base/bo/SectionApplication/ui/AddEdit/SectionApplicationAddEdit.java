/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.logic.DefaultActiveCatalogComboDSHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.base.bo.SectionApplication.SectionApplicationManager;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.SportSection2CourseRel;
import ru.tandemservice.unitutor.entity.catalog.codes.ApplicationStatus4SportsSectionCodes;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 24.08.2016
 */
@Configuration
public class SectionApplicationAddEdit extends BusinessComponentManager
{
    public static final String APPLICATION_ID = "applicationId";

    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String EDUCATION_YEAR_PARAM = "educationYear";

    public static final String STUDENT_STATUS_DS = "studentStatusDS";
    public static final String STUDENT_STATUS_PARAM = "studentStatus";
    public static final String SEX_DS = "sexDS";
    public static final String SEX_PARAM = "sex";

    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String FORMATIVE_ORG_UNIT_PARAM = "formativeOrgUnit";
    public static final String PROGRAM_FORM_DS = "programFormDS";
    public static final String PROGRAM_FORM_PARAM = "programForm";

    public static final String EDUCATION_LEVEL_DS = "educationLevelDS";
    public static final String EDUCATION_LEVEL_PARAM = "educationLevel";

    public static final String COURSE_DS = "courseDS";
    public static final String COURSE_PARAM = "course";
    public static final String GROUP_DS = "groupDS";
    public static final String GROUP_PARAM = "group";

    public static final String STUDENT_DS = "studentDS";
    public static final String STUDENT_PARAM = "student";
    public static final String SPORT_SECTION_DS = "sectionDS";
    public static final String SPORT_SECTION_PARAM = "section";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))

                .addDataSource(selectDS(STUDENT_STATUS_DS, StudentCatalogsManager.instance().studentStatusDSHandler()))
                .addDataSource(selectDS(SEX_DS, sexDSHandler()))

                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(PROGRAM_FORM_DS, developFormDS()))

                .addDataSource(selectDS(EDUCATION_LEVEL_DS, educationLevelDS()).addColumn(EducationLevelsHighSchool.fullTitle().s()))

                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(GROUP_DS, SectionApplicationManager.instance().groupDSHandler()))

                .addDataSource(selectDS(STUDENT_DS, studentDSHandler())
                                       .addColumn("ФИО", Student.fullFio().s())
                                       .addColumn("Группа", Student.group().s(), source -> source == null ? "" : "Группа " + ((Group) source).getTitle())
                )
                .addDataSource(selectDS(SPORT_SECTION_DS, sportSectionDSHandler())
                                       .addColumn("Название", SportSection.title().s())
                                       .addColumn("Тип", SportSection.type().title().s())
                                       .addColumn("Уровень", SportSection.physicalFitnessLevel().title().s())
                                       .addColumn("Тренер", SportSection.trainer().fio().s())
                )
                .create();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> dql.where(ge(property(alias, EducationYear.intValue()),
                                                                         value(EducationYear.getCurrentRequired().getIntValue()))));
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return Course.defaultSelectDSHandler(getName()).customize((alias, dql, context, filter) -> dql.where(le(property(alias, Course.intValue()), value(3))));
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sexDSHandler()
    {
        return new DefaultActiveCatalogComboDSHandler(this.getName(), Sex.class);
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developFormDS()
    {
        return EduProgramForm.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelDS()
    {
        return new EntityComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
                .customize((alias, dql, context, filter) -> {
                    FilterUtils.applyLikeFilter(dql, filter, EducationLevelsHighSchool.printTitle().fromAlias(alias));
                    return dql;
                })
                .order(EducationLevelsHighSchool.code())
                .pageable(true);
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {

            SectionApplicationManager.filterStudents4Applications(dql, alias,
                                                                  context.get(EDUCATION_YEAR_PARAM),
                                                                  context.get(COURSE_PARAM),
                                                                  context.get(SEX_PARAM),
                                                                  context.get(FORMATIVE_ORG_UNIT_PARAM),
                                                                  context.get(PROGRAM_FORM_PARAM),
                                                                  context.get(EDUCATION_LEVEL_PARAM),
                                                                  context.get(GROUP_PARAM),
                                                                  context.get(STUDENT_STATUS_PARAM),
                                                                  context.get(APPLICATION_ID));

            FilterUtils.applyLikeFilter(dql, filter, Student.person().identityCard().lastName().fromAlias(alias));

            return dql;
        };

        return new EntityComboDataSourceHandler(getName(), Student.class)
                .customize(customizer)
                .order(Student.person().identityCard().lastName())
                .filter(Student.person().identityCard().lastName())
                .pageable(true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sportSectionDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            EducationYear educationYear = context.get(EDUCATION_YEAR_PARAM);
            dql.where(eq(property(alias, SportSection.educationYear()), value(educationYear)));

            OrgUnit unit = null;
            Sex sex = null;

            //секции на которые нет активны или согласованных заявок у выбранного студента
            Student student = context.get(STUDENT_PARAM);
            if (student != null)
            {
                Long appId = context.get(APPLICATION_ID);
                dql.where(notExistsByExpr(SectionApplication.class, "sa", and(
                        eq(property("sa", SectionApplication.sportSection().s()), property(alias)),
                        eq(property("sa", SectionApplication.student().s()), value(student)),
                        or(eq(property("sa", SectionApplication.status().code().s()), value(ApplicationStatus4SportsSectionCodes.ACCEPTED)),
                           eq(property("sa", SectionApplication.status().code().s()), value(ApplicationStatus4SportsSectionCodes.ACCEPTABLE))),
                        eq(property("sa", SectionApplication.sportSection().educationYear()), value(educationYear)),
                        not(eq(property("sa", SectionApplication.id()), value(appId)))
                )));
                int div = educationYear.getIntValue() - EducationYear.getCurrentRequired().getIntValue();
                dql.where(exists(SportSection2CourseRel.class,
                                 SportSection2CourseRel.section().s(), property(alias),
                                 SportSection2CourseRel.course().intValue().s(), student.getCourse().getIntValue() + div));
                sex = student.getPerson().getIdentityCard().getSex();
                unit = student.getEducationOrgUnit().getFormativeOrgUnit();
            }
            else
            {
                SportSection.addWhereExistsCourseRel(dql, property(alias), context.get(COURSE_PARAM));
                sex = context.get(SEX_PARAM);
                unit = context.get(FORMATIVE_ORG_UNIT_PARAM);
            }

            dql.where(or(eq(property(alias, SportSection.formativeOrgUnit()), value(unit)),
                         isNull(property(alias, SportSection.formativeOrgUnit()))));
            SportSection.addWhereExistsSexRel(dql, property(alias), sex);

            //есть свободные места
            dql.where(gt(SportSection.getFreePlacesExpression(alias), value(0)));

            return dql;
        };

        return new EntityComboDataSourceHandler(getName(), SportSection.class)
                .customize(customizer)
                .order(SportSection.title())
                .filter(SportSection.title())
                .pageable(true);
    }

}