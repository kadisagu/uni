package ru.tandemservice.unitutor.component.awp.DisciplineList;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unitutor.component.awp.base.discipline.DAOBase;
import ru.tandemservice.unitutor.dao.DaoElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;
import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends DAOBase<Model> implements IDAO {

    @Override
    public void prepareCommonFilters4Tab(Model model) {

//        Long startTime = Calendar.getInstance().getTimeInMillis();
//        System.out.println("prepareCommonFilters4Tab.start");
        model.setFreeStatusSelectModel(new LazySimpleSelectModel<>(Model.FREE_STATUS_LIST));
        // запись актуальна, или нет
        model.setActualStatusSelectModel(new LazySimpleSelectModel<>(Model.FREE_STATUS_LIST));

        //модель для УП
        model.setEduPlanListModel(new DQLFullCheckSelectModel()
        {

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlan.class, alias);
                applyOrgUnitFilter(builder, alias, model.getOrgUnit());
                FilterUtils.applySimpleLikeFilter(builder, alias, EppEduPlan.number(), filter);
                return builder;
            }

            protected void applyOrgUnitFilter(DQLSelectBuilder builder, String alias, OrgUnit orgUnit)
            {
                if (orgUnit != null)
                {
                    builder.where(or(
                            eq(property(alias, EppEduPlan.owner()), value(orgUnit)),
                            exists(new DQLSelectBuilder()
                                           .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                                           .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), property(alias)))
                                           .where(eq(property("b", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(orgUnit)))
                                           .buildQuery())

                    ));
                }
            }
        });

        super.prepareCommonFilters4Tab(model);

        // модель для РУПов
        model.setWorkPlanListModel(new DQLFullCheckSelectModel()
        {

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppWorkPlan.class, alias);
                applyOrgUnitFilter(builder, alias, model.getOrgUnit());

                builder.where(eq(property(alias, EppWorkPlan.year().educationYear()), value(model.getFilterEducationYear())))
                        .where(eq(property(alias, EppWorkPlan.cachedGridTerm().part()), value(model.getFilterYearDistributionPart())));
                FilterUtils.applySimpleLikeFilter(builder, alias, EppWorkPlan.number(), filter);
                return builder;
            }

            protected void applyOrgUnitFilter(DQLSelectBuilder builder, String alias, OrgUnit orgUnit)
            {
                if (null != orgUnit)
                {
                    builder.where(or(
                            isNull(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().owner().fromAlias(alias))),
                            eq(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().owner().fromAlias(alias)), value(orgUnit)),
                            exists(new DQLSelectBuilder()
                                           .fromEntity(EppEduPlanVersionSpecializationBlock.class, "sb")
                                           .column(property("sb.id"))
                                           .where(eq(property(EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit().fromAlias("sb")), value(orgUnit)))
                                           .where(eq(property("sb"), property(alias, EppWorkPlan.parent())))
                                           .buildQuery())
                    ));
                }

            }
        });
//        System.out.println("prepareCommonFilters4Tab.finish = " + (Calendar.getInstance().getTimeInMillis() - startTime) /1000);
    }

    private boolean hasOtherFilterValue(Model model)
    {
        return ((model.getFilterEducationLevelsHighSchoolList() != null && !model.getFilterEducationLevelsHighSchoolList().isEmpty())
                        || (model.getFilterGroupList() != null && !model.getFilterGroupList().isEmpty())
                        || (model.getFilterDiscipline() != null && !model.getFilterDiscipline().isEmpty())
                        || (model.getFilterEmployee() != null && !model.getFilterEmployee().isEmpty())
                        || (model.getFilterOrgUnit() != null && !model.getFilterOrgUnit().isEmpty()));

    }


    @Override
    public void prepareListDataSource(Model model)
    {
        final String ELECTIVE_DISCIPLINE_ALIAS = "el";

        // зависит от выбора пользователя
        DQLSelectBuilder dqlIn = DqlDisciplineBuilder.DqlInWorkPlanRegistryElementRow(model.getFilterEducationYear(), model.getFilterYearDistributionPart(),
                model.getFilterEducationLevelsHighSchoolList(), model.getFilterGroupList(), model.getFilterDiscipline(), model.getFilterEmployee(),
                model.isCathedra() ? null : model.getFilterOrgUnit(), model.getFilterCathedraOrgUnit(), null, null);

        dqlIn.column(property(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS)), "regElemId");
        //Базовый РУП, не версия

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ElectiveDiscipline.class, ELECTIVE_DISCIPLINE_ALIAS)
                        // только ДПВ
                .where(eqValue(property(ELECTIVE_DISCIPLINE_ALIAS, ElectiveDiscipline.type().code()), ElectiveDisciplineTypeCodes.DPV))

                        // сколько уже записалось
                .joinDataSource(ELECTIVE_DISCIPLINE_ALIAS,
                                DQLJoinType.left,
                                DqlDisciplineBuilder.getElectiveDisciplineSUMMSubDQL(model.getFilterEducationYear(), ElectiveDisciplineTypeCodes.DPV).buildQuery(),
                                "dqlCount",
                                eq(property(ELECTIVE_DISCIPLINE_ALIAS, ElectiveDiscipline.id()),property("dqlCount.id")))

                        // фильтр (год)
                .where(eq(property(ELECTIVE_DISCIPLINE_ALIAS, ElectiveDiscipline.educationYear()), value(model.getFilterEducationYear())))
                        // часть года
                .where(eq(property(ELECTIVE_DISCIPLINE_ALIAS, ElectiveDiscipline.yearPart()), value(model.getFilterYearDistributionPart())));



        dql.joinPath(DQLJoinType.left, ElectiveDiscipline.registryElementPart().fromAlias(ELECTIVE_DISCIPLINE_ALIAS), "registryElementPart");
        // джойним расширение дисциплины
        dql.joinEntity("registryElementPart", DQLJoinType.left, EppRegistryElementExt.class, "eppREExt"
                , eq(property(EppRegistryElementPart.registryElement().id().fromAlias("registryElementPart")),
                     property(EppRegistryElementExt.eppRegistryElement().id().fromAlias("eppREExt"))));
        //отсекаем ДПВ с указанным языком
        dql.where(isNull(property("eppREExt", EppRegistryElementExt.language())));

        // джойним строку РУП
        dql.joinEntity("registryElementPart", DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, "wprer",
                       eq(property(EppRegistryElementPart.id().fromAlias("registryElementPart")),
                          property(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias("wprer"))));
        // джойним РУП
        dql.joinEntity("wprer", DQLJoinType.inner, EppWorkPlan.class, "eppWorkPlan",
                eq(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias("wprer"), EppWorkPlan.id().fromAlias("eppWorkPlan")))
                .where(eq(property("eppWorkPlan", EppWorkPlan.year().educationYear()), value(model.getFilterEducationYear())))
                .where(eq(property("eppWorkPlan", EppWorkPlan.cachedGridTerm().part()), value(model.getFilterYearDistributionPart())));

        // остальные фильтры
        //TODO
        //странное место. Уточнить, почему фильтр активных строк применяется только при наличии других фильтров
        if (hasOtherFilterValue(model)) {
            if (showUnactualRow(model))
                dql.where(or(
                        in(property(ElectiveDiscipline.registryElementPart().id().fromAlias(ELECTIVE_DISCIPLINE_ALIAS)), dqlIn.buildQuery()),
                        eq(property(ElectiveDiscipline.actualRow().fromAlias(ELECTIVE_DISCIPLINE_ALIAS)), value(Boolean.FALSE))));

            else
                // тут можно потерять неактуальные записи
                dql.where(in(property(ElectiveDiscipline.registryElementPart().id().fromAlias(ELECTIVE_DISCIPLINE_ALIAS)), dqlIn.buildQuery()));
        }

        if (model.getFilterWorkPlan() != null && !model.getFilterWorkPlan().isEmpty())
            dql.where(in(property("eppWorkPlan"), model.getFilterWorkPlan()));

        if (model.getFilterEduPlan()!= null && !model.getFilterEduPlan().isEmpty())
            dql.where(in(property("eppWorkPlan", EppWorkPlan.parent().eduPlanVersion().eduPlan()), model.getFilterEduPlan()));

        // фильр по наличию свободных мест
        if (model.getFilterFreeStatus() != null) {
            // есть свободные места
            if (model.getFilterFreeStatus().equals(Model.FREE_YES))
                dql.where(or(isNull("dqlCount.id"),
                             gt(property(ELECTIVE_DISCIPLINE_ALIAS, ElectiveDiscipline.placesCount()),property("dqlCount" + ".subscribe"))));

            else
                // сводобных мест нет
                dql.where(or(isNull("dqlCount.id"), le(property(ELECTIVE_DISCIPLINE_ALIAS, ElectiveDiscipline.placesCount()),property("dqlCount" + ".subscribe"))));


        }

        if (model.getFilterActualStatus() != null) {
            boolean actualRow = false;
            if (model.getFilterActualStatus().equals(Model.FREE_YES))
                actualRow = true;
            dql.where(eq(property(ELECTIVE_DISCIPLINE_ALIAS, ElectiveDiscipline.actualRow()),value(actualRow)));
        }

        // формируем вывод
        dql.column(ELECTIVE_DISCIPLINE_ALIAS)
                // сколько записалось итого
                .column(property("dqlCount.subscribe"))
                .column(property("eppREExt", EppRegistryElementExt.annotation()))
                .column(property("wprer", EppWorkPlanRegistryElementRow.number()))
                .column(property("eppWorkPlan"))
                .column(property("registryElementPart", EppRegistryElementPart.id()));

        EntityOrder order = model.getDataSource().getEntityOrder();
        String columnName = order.getColumnName();

        columnName = columnName.replace("null.electiveDiscipline.", "");
        columnName = columnName.replace("electiveDiscipline.", "");
        columnName = columnName.replace("placesSubscribed", "subscribe");
        columnName = columnName.replace("eppWPlanTitle", "eppWorkPlan.title");
        columnName = columnName.replace("eppWPRElRowIndex", "wprer.number");

        EntityOrder orderSelect = new EntityOrder(columnName, order.getDirection());

        if (columnName.contains("subscribe")) {
            dql.order("dqlCount.subscribe", orderSelect.getDirection());
        }
        else {
            // применяем сортировки
            DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(ElectiveDiscipline.class, ELECTIVE_DISCIPLINE_ALIAS)
                    .addAdditionalAlias(EppWorkPlanRegistryElementRow.class, "wprer")
                    .addAdditionalAlias(EppWorkPlan.class, "eppWorkPlan");
            orderRegistry.applyOrder(dql, orderSelect);
        }


        DynamicListDataSource<Wrapper> dataSource = model.getDataSource();
        // итого записей (нужно, для UI)
        final Number count = dql.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        dataSource.setTotalSize((null == count ? 0 : count.longValue()));

        // начальная, кол-во записей (читаем из забора)
        long startRow = dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow();
        long countRow = dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow();

        // получим столько записей, сколько нам надо (лишнее не читаем)
        List<Object[]> resultData = dql.createStatement(getSession()).setFirstResult((int) startRow).setMaxResults((int) countRow).list();
        List<Wrapper> wrapperList = new ArrayList<>();

        for (Object[] objs : resultData) {
            ElectiveDiscipline electiveDiscipline = (ElectiveDiscipline) objs[0];
            Long subscribe = 0L;
            if (objs[1] != null)
                subscribe = (Long) objs[1];

            String annotation = "";
            if (objs[2] != null)
                annotation = (String) objs[2];

            String wpRowIndex = "";
            if (objs[3] != null)
                wpRowIndex = (String) objs[3];

            EppWorkPlan workPlan = (EppWorkPlan) objs[4];

            Long regElPartId = (Long) objs[5];

            Wrapper wrap = new Wrapper();
            wrap.setElectiveDiscipline(electiveDiscipline);

            if (regElPartId != null)
                wrap.setFormOfControl(OptionalDisciplineUtil.getFormsOfControlTitles(regElPartId));

            wrap.setPlacesSubscribed(subscribe);
            wrap.setEppWPRElRowIndex(wpRowIndex);
            wrap.setWorkPlan(workPlan);

            Long startPlaces = 0L;
            if (electiveDiscipline.getPlacesCount() != null)
                startPlaces = electiveDiscipline.getPlacesCount();

            wrap.setFreePlacesToSubscribe(startPlaces - subscribe);

            if (!StringUtils.isEmpty(annotation))
                wrap.setContainAnnotation(true);

            wrapperList.add(wrap);
        }


        dataSource.createPage(wrapperList);
    }

    private boolean showUnactualRow(Model model)
    {
         return  (model.getFilterActualStatus() != null && model.getFilterActualStatus().equals(Model.FREE_NO));
    }

    public List<Wrapper> filterByProprity(Collection<IEntity> entityList, String fieldName, boolean isForNullValue)
    {
        List<Wrapper> filteredList = new ArrayList<>();
        for (IEntity entity : entityList) {
            Wrapper wrapper = (Wrapper) entity;
            if (fieldName.equals(ElectiveDiscipline.P_PLACES_COUNT)) {
                if (wrapper.getElectiveDiscipline().getPlacesCount() == null || !isForNullValue)
                    filteredList.add(wrapper);

            }
            else if (fieldName.equals(ElectiveDiscipline.P_SUBSCRIBE_TO_DATE)) {
                if (wrapper.getElectiveDiscipline().getSubscribeToDate() == null || !isForNullValue)
                    filteredList.add(wrapper);
            }
        }

        return filteredList;
    }


    @Override
    public void updateCountPlaces(Model model, List<Wrapper> wrapperList, String countPlaces) {

        for (Wrapper wrapper : wrapperList) {
            ElectiveDiscipline electiveDiscipline = wrapper.getElectiveDiscipline();
            electiveDiscipline.setPlacesCount(Long.parseLong(countPlaces));
            saveOrUpdate(electiveDiscipline);
        }
    }

    @Override
    public void updateSubscribeToDate(Model model, List<Wrapper> wrapperList, Date subscribeToDate) {

        for (Wrapper wrapper : wrapperList) {
            ElectiveDiscipline electiveDiscipline = wrapper.getElectiveDiscipline();
            electiveDiscipline.setSubscribeToDate(subscribeToDate);
            saveOrUpdate(electiveDiscipline);
        }
    }


    public boolean checkRegistryElementPart(EducationYear educationYear)
    {
        // 1 получим сумму id по РП
        DQLSelectBuilder dqlCheckWP = DqlDisciplineBuilder.getDqlRegistryElementPartCheck(educationYear);


        DQLSelectBuilder dqlSum = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPart.class, "rep")
                .joinDataSource("rep", DQLJoinType.inner, dqlCheckWP.buildQuery(), "dqlCheckWP",
                                eq(property(EppRegistryElementPart.id().fromAlias("rep")),property("dqlCheckWP.registryElementPartId")))

                .column(DQLFunctions.cast(DQLFunctions.max(property(EppRegistryElementPart.id().fromAlias("rep"))), PropertyType.STRING), "checkSum")
                .column("dqlCheckWP.educationYearId", "educationYearId")
                .column("dqlCheckWP.partId", "partId")
                .group("dqlCheckWP.educationYearId")
                .group("dqlCheckWP.partId");

        List<Object[]> lst = getList(dqlSum);

        Long totalHk = 0L;

        for (Object[] objs : lst) {
            int hk = objs[0].hashCode();

            Long hKey = Long.parseLong(Integer.toString(hk));
            Long educationYearId = (Long) objs[1];
            Long partId = (Long) objs[2];

            totalHk += hKey;
            totalHk += educationYearId;
            totalHk += partId;
        }

        System.out.println(totalHk);


        DQLSelectBuilder dqlSumED = new DQLSelectBuilder()
                .fromEntity(ElectiveDiscipline.class, "ed")

                // ДПВ
                .where(eqValue(property(ElectiveDiscipline.type().code().fromAlias("ed")), ElectiveDisciplineTypeCodes.DPV))

                .where(eq(property(ElectiveDiscipline.educationYear().fromAlias("ed")), value(educationYear)))
                .where(eq(property(ElectiveDiscipline.actualRow().fromAlias("ed")), value(Boolean.TRUE)))

                // только актуальные записи
                .column(DQLFunctions.cast(DQLFunctions.max(property(ElectiveDiscipline.registryElementPart().id().fromAlias("ed"))), PropertyType.STRING), "checkSum")
                .column(ElectiveDiscipline.educationYear().id().fromAlias("ed").s())
                .column(ElectiveDiscipline.yearPart().id().fromAlias("ed").s())
                .group(property(ElectiveDiscipline.educationYear().id().fromAlias("ed")))
                .group(property(ElectiveDiscipline.yearPart().id().fromAlias("ed")));

        lst = getList(dqlSumED);
        Long totalHkED = 0L;

        for (Object[] objs : lst) {
            int hk = objs[0].hashCode();

            Long hKey = Long.parseLong(Integer.toString(hk));
            Long educationYearId = (Long) objs[1];
            Long partId = (Long) objs[2];

            totalHkED += hKey;
            totalHkED += educationYearId;
            totalHkED += partId;
        }

        return  (totalHkED.equals(totalHk));
    }

    @Transactional
    @Override
    public void fixElectiveDisciplineRow(EducationYear filterEducationYear)
    {
        // исправить (добавить/отредактировать/удалить записи)
        DaoElectiveDiscipline.instanse().createOrDeleteElectiveDiscipline(filterEducationYear);
        getSession().flush();
        getSession().clear();

    }

}
