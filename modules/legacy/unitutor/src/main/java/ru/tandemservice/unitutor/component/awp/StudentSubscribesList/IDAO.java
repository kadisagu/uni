package ru.tandemservice.unitutor.component.awp.StudentSubscribesList;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unitutor.component.awp.base.discipline.IDAOBase;

import java.util.Collection;

public interface IDAO extends IDAOBase<Model>
{

    /**
     * Удаление подписок на дисциплину
     *
     * @param lst
     */
    @Transactional
    public void deleteElectiveDisciplineSubscribe(Collection<Long> lst);
}
