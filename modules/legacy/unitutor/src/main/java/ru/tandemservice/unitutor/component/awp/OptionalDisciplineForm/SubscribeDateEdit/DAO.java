package ru.tandemservice.unitutor.component.awp.OptionalDisciplineForm.SubscribeDateEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void update(Model model)
    {
        super.update(model);
        List<ElectiveDiscipline> electives = getList(ElectiveDiscipline.class, model.getDisciplineIds());
        for (ElectiveDiscipline item : electives)
        {
            if ((model.isForNullOnly() && item.getSubscribeToDate() == null) || !model.isForNullOnly())
            {
                item.setSubscribeToDate(model.getSubscribeToDate());
                saveOrUpdate(item);
            }
        }
    }
}
