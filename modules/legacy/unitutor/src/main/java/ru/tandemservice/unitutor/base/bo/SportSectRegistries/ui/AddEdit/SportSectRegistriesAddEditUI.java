/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.AddEdit;


import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.util.DayOfWeekDsHandler;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.SportSection2CourseRel;
import ru.tandemservice.unitutor.entity.SportSection2SexRel;
import ru.tandemservice.unitutor.entity.catalog.codes.SportSectionTypeCodes;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 23.08.2016
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "sportSectionId"),
        @Bind(key = SportSectRegistriesAddEdit.SOURCE_ID_4_COPY, binding = "sourceId4Copy")})
public class SportSectRegistriesAddEditUI extends UIPresenter
{
    private SportSection _sportSection;
    private Long _sportSectionId;
    private Long _sourceId4Copy;

    @Override
    public void onComponentRefresh()
    {
        ICommonDAO dao = DataAccessServices.dao();
        if (_sportSectionId == null)
        {
            _sportSection = new SportSection();
            _sportSection.setEducationYear(EducationYear.getCurrentRequired());
        }
        else _sportSection = dao.getNotNull(SportSection.class, _sportSectionId);


        if (_sourceId4Copy != null)
        {
            SportSection donor = dao.getNotNull(SportSection.class, _sourceId4Copy);

            _sportSection.setTitle(donor.getTitle());
            _sportSection.setEducationYear(donor.getEducationYear());
            _sportSection.setType(donor.getType());
            _sportSection.setFormativeOrgUnit(donor.getFormativeOrgUnit());
            _sportSection.setPhysicalFitnessLevel(donor.getPhysicalFitnessLevel());
            _sportSection.setHours(donor.getHours());
            _sportSection.setTutorOrgUnit(donor.getTutorOrgUnit());
            _sportSection.setTrainer(donor.getTrainer());
            _sportSection.setPlace(donor.getPlace());
            _sportSection.setFirstClassDay(donor.getFirstClassDay());
            _sportSection.setFirstClassTime(donor.getFirstClassTime());
            _sportSection.setSecondClassDay(donor.getSecondClassDay());
            _sportSection.setSecondClassTime(donor.getSecondClassTime());
        }

        if (_sportSectionId != null || _sourceId4Copy != null)
        {
            if (_sportSection.getFirstClassDay() != null)
                getSettings().set(SportSectRegistriesAddEdit.FIRST_CLASS_DAY_PARAM, DayOfWeekDsHandler.getWrappedRuDayOfWeek(_sportSection.getFirstClassDay()));
            if (_sportSection.getSecondClassDay() != null)
                getSettings().set(SportSectRegistriesAddEdit.SECOND_CLASS_DAY_PARAM, DayOfWeekDsHandler.getWrappedRuDayOfWeek(_sportSection.getSecondClassDay()));


            Long sectionId = _sportSectionId == null ? _sourceId4Copy : _sportSectionId;
            List<Course> courseList = dao.<Course>getList(new DQLSelectBuilder()
                                                                  .fromEntity(Course.class, "c")
                                                                  .column(property("c"))
                                                                  .where(exists(SportSection2CourseRel.class,
                                                                                SportSection2CourseRel.section().id().s(), sectionId,
                                                                                SportSection2CourseRel.course().s(), property("c")))
            );
            getSettings().set(SportSectRegistriesAddEdit.COURSE_LIST_PARAM, courseList);

            List<Sex> sexList = dao.<Sex>getList(new DQLSelectBuilder()
                                                         .fromEntity(Sex.class, "s")
                                                         .column(property("s"))
                                                         .where(exists(SportSection2SexRel.class,
                                                                       SportSection2SexRel.section().id().s(), sectionId,
                                                                       SportSection2SexRel.sex().s(), property("s")))
            );
            getSettings().set(SportSectRegistriesAddEdit.SEX_LIST_PARAM, sexList);
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SportSectRegistriesAddEdit.TUTOR_ORG_UNIT_PARAM, _sportSection.getTutorOrgUnit());
    }

    public boolean isNotHighSchool()
    {
        return _sportSection.getType() == null || !_sportSection.getType().getCode().equals(SportSectionTypeCodes.HIGH_SCHOOL);
    }

    public boolean isAddMode()
    {
        return null == _sportSectionId;
    }


    //Listeners
    public void onClickApply()
    {
        ICommonDAO dao = DataAccessServices.dao();

        DataWrapper firstDay = getSettings().get(SportSectRegistriesAddEdit.FIRST_CLASS_DAY_PARAM);
        _sportSection.setFirstClassDay(firstDay == null ? null : firstDay.<DayOfWeek>getWrapped().getValue());

        DataWrapper secondDay = getSettings().get(SportSectRegistriesAddEdit.SECOND_CLASS_DAY_PARAM);
        _sportSection.setSecondClassDay(secondDay == null ? null : secondDay.<DayOfWeek>getWrapped().getValue());

        dao.saveOrUpdate(_sportSection);


        List<Course> courseList = _uiSettings.<List<Course>>get(SportSectRegistriesAddEdit.COURSE_LIST_PARAM);
        List<SportSection2CourseRel> section2CourseRels = dao.getList(SportSection2CourseRel.class, SportSection2CourseRel.section().s(), _sportSection);

        List<SportSection2CourseRel> uselessSection2CourseRels = new ArrayList<>();
        section2CourseRels.forEach(rel -> {
            if (!courseList.remove(rel.getCourse()))
                uselessSection2CourseRels.add(rel);
        });
        uselessSection2CourseRels.forEach(dao::delete);

        List<SportSection2CourseRel> newSection2CourseRels = new ArrayList<>();
        courseList.forEach(course -> newSection2CourseRels.add(new SportSection2CourseRel(_sportSection, course)));
        newSection2CourseRels.forEach(dao::save);


        List<Sex> sexList = _uiSettings.<List<Sex>>get(SportSectRegistriesAddEdit.SEX_LIST_PARAM);
        List<SportSection2SexRel> sportSection2SexRels = dao.getList(SportSection2SexRel.class, SportSection2SexRel.section().s(), _sportSection);

        List<SportSection2SexRel> uselessSportSection2SexRel = new ArrayList<>();
        sportSection2SexRels.forEach(rel -> {
            if (!sexList.remove(rel.getSex()))
                uselessSportSection2SexRel.add(rel);
        });
        uselessSportSection2SexRel.forEach(dao::delete);

        List<SportSection2SexRel> newSportSection2SexRel = new ArrayList<>();
        sexList.forEach(sex -> newSportSection2SexRel.add(new SportSection2SexRel(_sportSection, sex)));
        newSportSection2SexRel.forEach(dao::save);

        deactivate();
    }

    public void onChangeCourse()
    {
        List<Course> courseList = _uiSettings.<List<Course>>get(SportSectRegistriesAddEdit.COURSE_LIST_PARAM);
        if (CollectionUtils.isEmpty(courseList) || courseList.size() > 1) return;

        Course course = courseList.get(0);

        switch (course.getIntValue())
        {
            case 1:
            case 2:
                _sportSection.setHours(54);
                break;
            case 3:
                _sportSection.setHours(36);
                break;
        }
    }

    public void onChangeSectionType()
    {
        if (_sportSection.getType().getCode().equals(SportSectionTypeCodes.OUTSIDE))
            _sportSection.setFormativeOrgUnit(null);
    }


    //Getters and Setters
    public SportSection getSportSection()
    {
        return _sportSection;
    }

    public void setSportSection(SportSection sportSection)
    {
        _sportSection = sportSection;
    }

    public Long getSportSectionId()
    {
        return _sportSectionId;
    }

    public void setSportSectionId(Long sportSectionId)
    {
        _sportSectionId = sportSectionId;
    }

    public Long getSourceId4Copy()
    {
        return _sourceId4Copy;
    }

    public void setSourceId4Copy(Long sourceId4Copy)
    {
        _sourceId4Copy = sourceId4Copy;
    }
}