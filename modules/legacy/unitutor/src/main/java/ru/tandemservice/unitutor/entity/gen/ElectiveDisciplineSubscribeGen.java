package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись на дисциплину(ДПВ, УДВ, УФ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ElectiveDisciplineSubscribeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe";
    public static final String ENTITY_NAME = "electiveDisciplineSubscribe";
    public static final int VERSION_HASH = 739294364;
    private static IEntityMeta ENTITY_META;

    public static final String L_ELECTIVE_DISCIPLINE = "electiveDiscipline";
    public static final String L_STUDENT = "student";
    public static final String P_SUBSCRIBE_ON = "subscribeOn";
    public static final String P_SEND_MAIL = "sendMail";
    public static final String P_SOURCE = "source";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String P_TRANSACTION_ID = "transactionId";
    public static final String P_SOURCE_CODE = "sourceCode";

    private ElectiveDiscipline _electiveDiscipline;     // Дисциплина(ДПВ, УДВ, УФ)
    private Student _student;     // Студент
    private Date _subscribeOn;     // Дата записи
    private Boolean _sendMail = true;     // Нужно ли отправлять уведомление студенту
    private String _source;     // Источник записи
    private Date _removalDate;     // Дата утраты актуальности
    private Long _transactionId;     // id транзакции

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина(ДПВ, УДВ, УФ). Свойство не может быть null.
     */
    @NotNull
    public ElectiveDiscipline getElectiveDiscipline()
    {
        return _electiveDiscipline;
    }

    /**
     * @param electiveDiscipline Дисциплина(ДПВ, УДВ, УФ). Свойство не может быть null.
     */
    public void setElectiveDiscipline(ElectiveDiscipline electiveDiscipline)
    {
        dirty(_electiveDiscipline, electiveDiscipline);
        _electiveDiscipline = electiveDiscipline;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Дата записи.
     */
    public Date getSubscribeOn()
    {
        return _subscribeOn;
    }

    /**
     * @param subscribeOn Дата записи.
     */
    public void setSubscribeOn(Date subscribeOn)
    {
        dirty(_subscribeOn, subscribeOn);
        _subscribeOn = subscribeOn;
    }

    /**
     * @return Нужно ли отправлять уведомление студенту.
     */
    public Boolean getSendMail()
    {
        return _sendMail;
    }

    /**
     * @param sendMail Нужно ли отправлять уведомление студенту.
     */
    public void setSendMail(Boolean sendMail)
    {
        dirty(_sendMail, sendMail);
        _sendMail = sendMail;
    }

    /**
     * @return Источник записи.
     */
    @Length(max=255)
    public String getSource()
    {
        return _source;
    }

    /**
     * @param source Источник записи.
     */
    public void setSource(String source)
    {
        dirty(_source, source);
        _source = source;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * @return id транзакции.
     */
    public Long getTransactionId()
    {
        return _transactionId;
    }

    /**
     * @param transactionId id транзакции.
     */
    public void setTransactionId(Long transactionId)
    {
        dirty(_transactionId, transactionId);
        _transactionId = transactionId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ElectiveDisciplineSubscribeGen)
        {
            setElectiveDiscipline(((ElectiveDisciplineSubscribe)another).getElectiveDiscipline());
            setStudent(((ElectiveDisciplineSubscribe)another).getStudent());
            setSubscribeOn(((ElectiveDisciplineSubscribe)another).getSubscribeOn());
            setSendMail(((ElectiveDisciplineSubscribe)another).getSendMail());
            setSource(((ElectiveDisciplineSubscribe)another).getSource());
            setRemovalDate(((ElectiveDisciplineSubscribe)another).getRemovalDate());
            setTransactionId(((ElectiveDisciplineSubscribe)another).getTransactionId());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ElectiveDisciplineSubscribeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ElectiveDisciplineSubscribe.class;
        }

        public T newInstance()
        {
            return (T) new ElectiveDisciplineSubscribe();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "electiveDiscipline":
                    return obj.getElectiveDiscipline();
                case "student":
                    return obj.getStudent();
                case "subscribeOn":
                    return obj.getSubscribeOn();
                case "sendMail":
                    return obj.getSendMail();
                case "source":
                    return obj.getSource();
                case "removalDate":
                    return obj.getRemovalDate();
                case "transactionId":
                    return obj.getTransactionId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "electiveDiscipline":
                    obj.setElectiveDiscipline((ElectiveDiscipline) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "subscribeOn":
                    obj.setSubscribeOn((Date) value);
                    return;
                case "sendMail":
                    obj.setSendMail((Boolean) value);
                    return;
                case "source":
                    obj.setSource((String) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "transactionId":
                    obj.setTransactionId((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "electiveDiscipline":
                        return true;
                case "student":
                        return true;
                case "subscribeOn":
                        return true;
                case "sendMail":
                        return true;
                case "source":
                        return true;
                case "removalDate":
                        return true;
                case "transactionId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "electiveDiscipline":
                    return true;
                case "student":
                    return true;
                case "subscribeOn":
                    return true;
                case "sendMail":
                    return true;
                case "source":
                    return true;
                case "removalDate":
                    return true;
                case "transactionId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "electiveDiscipline":
                    return ElectiveDiscipline.class;
                case "student":
                    return Student.class;
                case "subscribeOn":
                    return Date.class;
                case "sendMail":
                    return Boolean.class;
                case "source":
                    return String.class;
                case "removalDate":
                    return Date.class;
                case "transactionId":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ElectiveDisciplineSubscribe> _dslPath = new Path<ElectiveDisciplineSubscribe>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ElectiveDisciplineSubscribe");
    }
            

    /**
     * @return Дисциплина(ДПВ, УДВ, УФ). Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getElectiveDiscipline()
     */
    public static ElectiveDiscipline.Path<ElectiveDiscipline> electiveDiscipline()
    {
        return _dslPath.electiveDiscipline();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Дата записи.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getSubscribeOn()
     */
    public static PropertyPath<Date> subscribeOn()
    {
        return _dslPath.subscribeOn();
    }

    /**
     * @return Нужно ли отправлять уведомление студенту.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getSendMail()
     */
    public static PropertyPath<Boolean> sendMail()
    {
        return _dslPath.sendMail();
    }

    /**
     * @return Источник записи.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getSource()
     */
    public static PropertyPath<String> source()
    {
        return _dslPath.source();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * @return id транзакции.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getTransactionId()
     */
    public static PropertyPath<Long> transactionId()
    {
        return _dslPath.transactionId();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getSourceCode()
     */
    public static SupportedPropertyPath<String> sourceCode()
    {
        return _dslPath.sourceCode();
    }

    public static class Path<E extends ElectiveDisciplineSubscribe> extends EntityPath<E>
    {
        private ElectiveDiscipline.Path<ElectiveDiscipline> _electiveDiscipline;
        private Student.Path<Student> _student;
        private PropertyPath<Date> _subscribeOn;
        private PropertyPath<Boolean> _sendMail;
        private PropertyPath<String> _source;
        private PropertyPath<Date> _removalDate;
        private PropertyPath<Long> _transactionId;
        private SupportedPropertyPath<String> _sourceCode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина(ДПВ, УДВ, УФ). Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getElectiveDiscipline()
     */
        public ElectiveDiscipline.Path<ElectiveDiscipline> electiveDiscipline()
        {
            if(_electiveDiscipline == null )
                _electiveDiscipline = new ElectiveDiscipline.Path<ElectiveDiscipline>(L_ELECTIVE_DISCIPLINE, this);
            return _electiveDiscipline;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Дата записи.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getSubscribeOn()
     */
        public PropertyPath<Date> subscribeOn()
        {
            if(_subscribeOn == null )
                _subscribeOn = new PropertyPath<Date>(ElectiveDisciplineSubscribeGen.P_SUBSCRIBE_ON, this);
            return _subscribeOn;
        }

    /**
     * @return Нужно ли отправлять уведомление студенту.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getSendMail()
     */
        public PropertyPath<Boolean> sendMail()
        {
            if(_sendMail == null )
                _sendMail = new PropertyPath<Boolean>(ElectiveDisciplineSubscribeGen.P_SEND_MAIL, this);
            return _sendMail;
        }

    /**
     * @return Источник записи.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getSource()
     */
        public PropertyPath<String> source()
        {
            if(_source == null )
                _source = new PropertyPath<String>(ElectiveDisciplineSubscribeGen.P_SOURCE, this);
            return _source;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(ElectiveDisciplineSubscribeGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * @return id транзакции.
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getTransactionId()
     */
        public PropertyPath<Long> transactionId()
        {
            if(_transactionId == null )
                _transactionId = new PropertyPath<Long>(ElectiveDisciplineSubscribeGen.P_TRANSACTION_ID, this);
            return _transactionId;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe#getSourceCode()
     */
        public SupportedPropertyPath<String> sourceCode()
        {
            if(_sourceCode == null )
                _sourceCode = new SupportedPropertyPath<String>(ElectiveDisciplineSubscribeGen.P_SOURCE_CODE, this);
            return _sourceCode;
        }

        public Class getEntityClass()
        {
            return ElectiveDisciplineSubscribe.class;
        }

        public String getEntityName()
        {
            return "electiveDisciplineSubscribe";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getSourceCode();
}
