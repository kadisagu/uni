package ru.tandemservice.unitutor.base.bo.Tutor.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@State({
        @org.tandemframework.core.component.Bind(key = "employeePostId", binding = "employeePostId"),
        @org.tandemframework.core.component.Bind(key = "publisherId", binding = "id"),
        @org.tandemframework.core.component.Bind(key = "selectedTab", binding = "selectedTab")})
public class TutorTabUI extends UIPresenter {

    public static final String SELECTED_TAB = "selectedTab";
    private Long id;
    private Long employeePostId;
    private String _selectedTab;
    private Employee employee;
    private List<Long> orgUnitIds;

    @Override
    public void onComponentActivate() {
        super.onComponentActivate();

        IEntity entity = UniDaoFacade.getCoreDao().get(EmployeePost.class, this.getId());
        if (entity != null)
            employee = ((EmployeePost) entity).getEmployee();

        if (employee != null)
            setOrgUnitIds(OptionalDisciplineUtil.getOrgUnitList4Tutor(employee));
    }

    public void setOrgUnitIds(List<Long> orgUnitIds) {
        this.orgUnitIds = orgUnitIds;
    }

    public List<Long> getOrgUnitIds() {
        return orgUnitIds;
    }

    public String getSelectedTab() {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this._selectedTab = selectedTab;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isTutor() {
        return  (getOrgUnitIds() != null && getOrgUnitIds().size() > 0);
    }

    public Long getEmployeePostId() {
        return employeePostId;
    }

    public void setEmployeePostId(Long employeePostId) {
        this.employeePostId = employeePostId;
    }

    public Map<String, Object> getTutorParam() {
        Map<String, Object> params = new Hashtable<String, Object>();
        if (this.employee != null)
            params.put("tutorId", employee.getId());
        params.put("tutorOrgUnitIds", getOrgUnitIds());
        if (getEmployeePostId() != null)
            params.put("employeePostId", getEmployeePostId());
        return params;
    }

    public Map<String, Object> getListParam()
    {
        Map<String, Object> params = new Hashtable<>();

        if (this.employee != null)
            params.put("employeeId", employee.getId());

        if (this.employeePostId != null)
            params.put("employeePostId", employeePostId);

        // orgUnit естественно не передаем
        return params;
    }
}
