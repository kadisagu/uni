/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication.ui.ChangeStatus;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unitutor.entity.catalog.ApplicationStatus4SportsSection;

/**
 * @author Andrey Andreev
 * @since 24.08.2016
 */
@Configuration
public class SectionApplicationChangeStatus extends BusinessComponentManager
{
    public static final String APPLICATION_IDS_PARAM = "applicationIds";
    public static final String STATUS_DS = "statusDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(STATUS_DS, statusDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> statusDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), ApplicationStatus4SportsSection.class);
    }
}