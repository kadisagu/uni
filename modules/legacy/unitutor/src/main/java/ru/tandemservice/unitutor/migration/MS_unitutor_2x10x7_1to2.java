package ru.tandemservice.unitutor.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unitutor_2x10x7_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.7")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sportSection

		// раньше свойство place ссылалось на сущность uniplacesBuilding
		// теперь оно ссылается на сущность uniplacesPlace
        // удаляем все!!!
		{
            tool.executeUpdate("delete from sectionapplication_t");
            tool.executeUpdate("delete from sportsection_t");
		}

        // удалено свойство course
        {
            // удалить колонку
            tool.dropColumn("sportsection_t", "course_id");
        }


        {
            // удалить колонку
            tool.dropColumn("sportsection_t", "sex_id");
        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность sportSection2CourseRel

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("sportsection2courserel_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sportsection2courserel"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("section_id", DBType.LONG).setNullable(false),
                                      new DBColumn("course_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sportSection2CourseRel");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sportSection2SexRel

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("sportsection2sexrel_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sportsection2sexrel"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("section_id", DBType.LONG).setNullable(false),
                                      new DBColumn("sex_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("sportSection2SexRel");

        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность sectionApplication

        // создано обязательное свойство registrationDate
        {
            // создать колонку
            tool.createColumn("sectionapplication_t", new DBColumn("registrationdate_p", DBType.TIMESTAMP));

            // сделать колонку NOT NULL
            tool.setColumnNullable("sectionapplication_t", "registrationdate_p", false);
        }
    }
}