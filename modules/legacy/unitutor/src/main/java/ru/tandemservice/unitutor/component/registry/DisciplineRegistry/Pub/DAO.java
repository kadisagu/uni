package ru.tandemservice.unitutor.component.registry.DisciplineRegistry.Pub;


import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;

public class DAO extends ru.tandemservice.uniepp.component.registry.DisciplineRegistry.Pub.DAO
{


    @Override
    public void prepare(ru.tandemservice.uniepp.component.registry.base.Pub.Model<EppRegistryDiscipline> model)
    {
        super.prepare(model);
        Model modelExt = (Model) model;
        EppRegistryElementExt registryElementExt = get(EppRegistryElementExt.class, EppRegistryElementExt.eppRegistryElement().s(), model.getElement().getId());
        ((Model) model).setRegistryElementExt(registryElementExt);
        if (registryElementExt != null)
        {
            StringBuilder languageString = new StringBuilder().append(registryElementExt.getLanguage() != null ? registryElementExt.getLanguage().getTitle() : "");
            if (registryElementExt.isSecondLanguage())
                languageString.append(" (второй изучаемый)");
            ((Model) model).setLanguageStr(languageString.toString());
        }
    }
}
