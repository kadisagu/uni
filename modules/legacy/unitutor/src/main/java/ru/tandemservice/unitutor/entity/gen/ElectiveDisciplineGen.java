package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.catalog.ElectiveDisciplineType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина (ДПВ, УДВ, УФ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ElectiveDisciplineGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.ElectiveDiscipline";
    public static final String ENTITY_NAME = "electiveDiscipline";
    public static final int VERSION_HASH = 931075581;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT_PART = "registryElementPart";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_YEAR_PART = "yearPart";
    public static final String P_ACTUAL_ROW = "actualRow";
    public static final String L_TYPE = "type";
    public static final String P_SUBSCRIBE_TO_DATE = "subscribeToDate";
    public static final String P_PLACES_COUNT = "placesCount";
    public static final String P_FREE_PLACES_TO_SUBSCRIBE = "freePlacesToSubscribe";
    public static final String P_SET_TOTAL_PLACES_TO_SUBSCRIBE = "setTotalPlacesToSubscribe";
    public static final String P_SUBSCRIBERS_COUNT = "subscribersCount";

    private EppRegistryElementPart _registryElementPart;     // Часть дисциплины из реестра
    private EducationYear _educationYear;     // Учебный год, на который создана дисциплина на основе части дисциплины из реестра
    private YearDistributionPart _yearPart;     // Часть учебного года обучения по данной конкретной части дисциплины
    private boolean _actualRow = true;     // Актуальная строка
    private ElectiveDisciplineType _type;     // Тип дисциплины
    private Date _subscribeToDate;     // Дата записи до для дисциплины
    private Long _placesCount;     // Количество мест для записи на дисциплину

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Часть дисциплины из реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegistryElementPart()
    {
        return _registryElementPart;
    }

    /**
     * @param registryElementPart Часть дисциплины из реестра. Свойство не может быть null.
     */
    public void setRegistryElementPart(EppRegistryElementPart registryElementPart)
    {
        dirty(_registryElementPart, registryElementPart);
        _registryElementPart = registryElementPart;
    }

    /**
     * @return Учебный год, на который создана дисциплина на основе части дисциплины из реестра. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год, на который создана дисциплина на основе части дисциплины из реестра. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть учебного года обучения по данной конкретной части дисциплины. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года обучения по данной конкретной части дисциплины. Свойство не может быть null.
     */
    public void setYearPart(YearDistributionPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Актуальная строка. Свойство не может быть null.
     */
    @NotNull
    public boolean isActualRow()
    {
        return _actualRow;
    }

    /**
     * @param actualRow Актуальная строка. Свойство не может быть null.
     */
    public void setActualRow(boolean actualRow)
    {
        dirty(_actualRow, actualRow);
        _actualRow = actualRow;
    }

    /**
     * @return Тип дисциплины. Свойство не может быть null.
     */
    @NotNull
    public ElectiveDisciplineType getType()
    {
        return _type;
    }

    /**
     * @param type Тип дисциплины. Свойство не может быть null.
     */
    public void setType(ElectiveDisciplineType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Дата записи до для дисциплины.
     */
    public Date getSubscribeToDate()
    {
        return _subscribeToDate;
    }

    /**
     * @param subscribeToDate Дата записи до для дисциплины.
     */
    public void setSubscribeToDate(Date subscribeToDate)
    {
        dirty(_subscribeToDate, subscribeToDate);
        _subscribeToDate = subscribeToDate;
    }

    /**
     * @return Количество мест для записи на дисциплину.
     */
    public Long getPlacesCount()
    {
        return _placesCount;
    }

    /**
     * @param placesCount Количество мест для записи на дисциплину.
     */
    public void setPlacesCount(Long placesCount)
    {
        dirty(_placesCount, placesCount);
        _placesCount = placesCount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ElectiveDisciplineGen)
        {
            setRegistryElementPart(((ElectiveDiscipline)another).getRegistryElementPart());
            setEducationYear(((ElectiveDiscipline)another).getEducationYear());
            setYearPart(((ElectiveDiscipline)another).getYearPart());
            setActualRow(((ElectiveDiscipline)another).isActualRow());
            setType(((ElectiveDiscipline)another).getType());
            setSubscribeToDate(((ElectiveDiscipline)another).getSubscribeToDate());
            setPlacesCount(((ElectiveDiscipline)another).getPlacesCount());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ElectiveDisciplineGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ElectiveDiscipline.class;
        }

        public T newInstance()
        {
            return (T) new ElectiveDiscipline();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryElementPart":
                    return obj.getRegistryElementPart();
                case "educationYear":
                    return obj.getEducationYear();
                case "yearPart":
                    return obj.getYearPart();
                case "actualRow":
                    return obj.isActualRow();
                case "type":
                    return obj.getType();
                case "subscribeToDate":
                    return obj.getSubscribeToDate();
                case "placesCount":
                    return obj.getPlacesCount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryElementPart":
                    obj.setRegistryElementPart((EppRegistryElementPart) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "yearPart":
                    obj.setYearPart((YearDistributionPart) value);
                    return;
                case "actualRow":
                    obj.setActualRow((Boolean) value);
                    return;
                case "type":
                    obj.setType((ElectiveDisciplineType) value);
                    return;
                case "subscribeToDate":
                    obj.setSubscribeToDate((Date) value);
                    return;
                case "placesCount":
                    obj.setPlacesCount((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryElementPart":
                        return true;
                case "educationYear":
                        return true;
                case "yearPart":
                        return true;
                case "actualRow":
                        return true;
                case "type":
                        return true;
                case "subscribeToDate":
                        return true;
                case "placesCount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryElementPart":
                    return true;
                case "educationYear":
                    return true;
                case "yearPart":
                    return true;
                case "actualRow":
                    return true;
                case "type":
                    return true;
                case "subscribeToDate":
                    return true;
                case "placesCount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryElementPart":
                    return EppRegistryElementPart.class;
                case "educationYear":
                    return EducationYear.class;
                case "yearPart":
                    return YearDistributionPart.class;
                case "actualRow":
                    return Boolean.class;
                case "type":
                    return ElectiveDisciplineType.class;
                case "subscribeToDate":
                    return Date.class;
                case "placesCount":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ElectiveDiscipline> _dslPath = new Path<ElectiveDiscipline>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ElectiveDiscipline");
    }
            

    /**
     * @return Часть дисциплины из реестра. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getRegistryElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
    {
        return _dslPath.registryElementPart();
    }

    /**
     * @return Учебный год, на который создана дисциплина на основе части дисциплины из реестра. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть учебного года обучения по данной конкретной части дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getYearPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Актуальная строка. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#isActualRow()
     */
    public static PropertyPath<Boolean> actualRow()
    {
        return _dslPath.actualRow();
    }

    /**
     * @return Тип дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getType()
     */
    public static ElectiveDisciplineType.Path<ElectiveDisciplineType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Дата записи до для дисциплины.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getSubscribeToDate()
     */
    public static PropertyPath<Date> subscribeToDate()
    {
        return _dslPath.subscribeToDate();
    }

    /**
     * @return Количество мест для записи на дисциплину.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getPlacesCount()
     */
    public static PropertyPath<Long> placesCount()
    {
        return _dslPath.placesCount();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getFreePlacesToSubscribe()
     */
    public static SupportedPropertyPath<Long> freePlacesToSubscribe()
    {
        return _dslPath.freePlacesToSubscribe();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#isSetTotalPlacesToSubscribe()
     */
    public static SupportedPropertyPath<Boolean> setTotalPlacesToSubscribe()
    {
        return _dslPath.setTotalPlacesToSubscribe();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getSubscribersCount()
     */
    public static SupportedPropertyPath<Long> subscribersCount()
    {
        return _dslPath.subscribersCount();
    }

    public static class Path<E extends ElectiveDiscipline> extends EntityPath<E>
    {
        private EppRegistryElementPart.Path<EppRegistryElementPart> _registryElementPart;
        private EducationYear.Path<EducationYear> _educationYear;
        private YearDistributionPart.Path<YearDistributionPart> _yearPart;
        private PropertyPath<Boolean> _actualRow;
        private ElectiveDisciplineType.Path<ElectiveDisciplineType> _type;
        private PropertyPath<Date> _subscribeToDate;
        private PropertyPath<Long> _placesCount;
        private SupportedPropertyPath<Long> _freePlacesToSubscribe;
        private SupportedPropertyPath<Boolean> _setTotalPlacesToSubscribe;
        private SupportedPropertyPath<Long> _subscribersCount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Часть дисциплины из реестра. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getRegistryElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> registryElementPart()
        {
            if(_registryElementPart == null )
                _registryElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REGISTRY_ELEMENT_PART, this);
            return _registryElementPart;
        }

    /**
     * @return Учебный год, на который создана дисциплина на основе части дисциплины из реестра. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть учебного года обучения по данной конкретной части дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getYearPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Актуальная строка. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#isActualRow()
     */
        public PropertyPath<Boolean> actualRow()
        {
            if(_actualRow == null )
                _actualRow = new PropertyPath<Boolean>(ElectiveDisciplineGen.P_ACTUAL_ROW, this);
            return _actualRow;
        }

    /**
     * @return Тип дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getType()
     */
        public ElectiveDisciplineType.Path<ElectiveDisciplineType> type()
        {
            if(_type == null )
                _type = new ElectiveDisciplineType.Path<ElectiveDisciplineType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Дата записи до для дисциплины.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getSubscribeToDate()
     */
        public PropertyPath<Date> subscribeToDate()
        {
            if(_subscribeToDate == null )
                _subscribeToDate = new PropertyPath<Date>(ElectiveDisciplineGen.P_SUBSCRIBE_TO_DATE, this);
            return _subscribeToDate;
        }

    /**
     * @return Количество мест для записи на дисциплину.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getPlacesCount()
     */
        public PropertyPath<Long> placesCount()
        {
            if(_placesCount == null )
                _placesCount = new PropertyPath<Long>(ElectiveDisciplineGen.P_PLACES_COUNT, this);
            return _placesCount;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getFreePlacesToSubscribe()
     */
        public SupportedPropertyPath<Long> freePlacesToSubscribe()
        {
            if(_freePlacesToSubscribe == null )
                _freePlacesToSubscribe = new SupportedPropertyPath<Long>(ElectiveDisciplineGen.P_FREE_PLACES_TO_SUBSCRIBE, this);
            return _freePlacesToSubscribe;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#isSetTotalPlacesToSubscribe()
     */
        public SupportedPropertyPath<Boolean> setTotalPlacesToSubscribe()
        {
            if(_setTotalPlacesToSubscribe == null )
                _setTotalPlacesToSubscribe = new SupportedPropertyPath<Boolean>(ElectiveDisciplineGen.P_SET_TOTAL_PLACES_TO_SUBSCRIBE, this);
            return _setTotalPlacesToSubscribe;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline#getSubscribersCount()
     */
        public SupportedPropertyPath<Long> subscribersCount()
        {
            if(_subscribersCount == null )
                _subscribersCount = new SupportedPropertyPath<Long>(ElectiveDisciplineGen.P_SUBSCRIBERS_COUNT, this);
            return _subscribersCount;
        }

        public Class getEntityClass()
        {
            return ElectiveDiscipline.class;
        }

        public String getEntityName()
        {
            return "electiveDiscipline";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Long getFreePlacesToSubscribe();

    public abstract boolean isSetTotalPlacesToSubscribe();

    public abstract Long getSubscribersCount();
}
