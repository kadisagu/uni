/* $Id$ */
package ru.tandemservice.unitutor.component.registry.DisciplineRegistry.LanguageAdd;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;

/**
 * @author Ekaterina Zvereva
 * @since 04.02.2016
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        EppRegistryElementExt registryElementExt = get(EppRegistryElementExt.class, EppRegistryElementExt.eppRegistryElement().s(), model.getRegistryElementId());

        if (registryElementExt == null)
            registryElementExt = new EppRegistryElementExt(getNotNull(EppRegistryElement.class, model.getRegistryElementId()));

        model.setRegistryElementExt(registryElementExt);

        model.setLanguageModel(new LazySimpleSelectModel<>(ForeignLanguage.class));
    }

    @Override
    public void update(Model model)
    {
        saveOrUpdate(model.getRegistryElementExt());
    }
}