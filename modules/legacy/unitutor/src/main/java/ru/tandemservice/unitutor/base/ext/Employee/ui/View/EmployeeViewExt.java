package ru.tandemservice.unitutor.base.ext.Employee.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.View.EmployeeView;
import ru.tandemservice.unitutor.base.bo.Tutor.ui.Tab.TutorTab;

@Configuration
public class EmployeeViewExt extends BusinessComponentExtensionManager {

    @Autowired
    private EmployeeView employeeView;

    @Bean
    public TabPanelExtension employeePubTabPanelExtPoint() {
        return tabPanelExtensionBuilder(
                this.employeeView.employeePubTabPanelExtPoint())
                .addTab(componentTab("tutorTab", TutorTab.class).visible("ui:tutor"))
                .create();
    }

}
