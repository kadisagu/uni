/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication.ui.MassAdd;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.logic.DefaultActiveCatalogComboDSHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitutor.base.bo.SectionApplication.SectionApplicationManager;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.SportSectRegistriesManager;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.Pub.SportSectRegistriesPub;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.SportSection2CourseRel;
import ru.tandemservice.unitutor.entity.SportSection2SexRel;
import ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel;
import ru.tandemservice.unitutor.entity.catalog.SportSectionType;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 29.08.2016
 */
@Configuration
public class SectionApplicationMassAdd extends BusinessComponentManager
{
    public static final String ALL_COMMON_FILTERS_REQUIRED = "allCommonFiltersRequied";

    public static final String STUDENT_LIST_DS = "studentListDS";
    public static final String STUDENT_LIST_CHECKBOX = "checkbox";
    public static final String STUDENT_LIST_FIO = "fio";
    public static final String STUDENT_LIST_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String STUDENT_LIST_COURSE = "course";
    public static final String STUDENT_LIST_STATUS = "status";
    public static final String STUDENT_LIST_EDUCATION_LEVEL = "educationLevel";
    public static final String STUDENT_LIST_PROGRAM_FORM = "programForm";
    public static final String STUDENT_LIST_GROUP = "group";

    public static final String SECTION_LIST_DS = "sectionListDS";
    public static final String SECTION_LIST_RADIO = "radioColumn";
    public static final String SECTION_LIST_TITLE = "titleColumn";
    public static final String SECTION_LIST_SECTION_TYPE = "sectionType";
    public static final String SECTION_LIST_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String SECTION_LIST_PHYSICAL_FITNESS_LEVEL = "physicalFitnessLevel";
    public static final String SECTION_LIST_COURSE = "course";
    public static final String SECTION_LIST_SEX = "sex";
    public static final String SECTION_LIST_PLACE = "place";
    public static final String SECTION_LIST_TRAINER = "trainerColumn";
    public static final String SECTION_LIST_FIRST_CLASS = "firstClass";
    public static final String SECTION_LIST_SECOND_CLASS = "secondClass";
    public static final String SECTION_LIST_FREE_NUMBERS = "freeNumbers";

    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String EDUCATION_YEAR_PARAM = "educationYear";
    public static final String SEX_DS = "sexDS";
    public static final String SEX_PARAM = "sex";
    public static final String COURSE_DS = "courseDS";
    public static final String COURSE_PARAM = "course";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String FORMATIVE_ORG_UNIT_PARAM = "formativeOrgUnit";

    public static final String STUDENT_STATUS_DS = "studentStatusDS";
    public static final String STUDENT_STATUS_PARAM = "studentStatusList";
    public static final String PROGRAM_FORM_DS = "programFormDS";
    public static final String PROGRAM_FORM_PARAM = "programForm";
    public static final String EDUCATION_LEVEL_DS = "educationLevelDS";
    public static final String EDUCATION_LEVEL_PARAM = "educationLevelList";
    public static final String GROUP_DS = "groupDS";
    public static final String GROUP_PARAM = "groupList";

    public static final String SECTION_TYPE_DS = "sectionTypeDS";
    public static final String SECTION_TYPE_PARAM = "sectionType";
    public static final String PHYSICAL_FITNESS_LEVEL_DS = "physicalFitnessLevelDS";
    public static final String PHYSICAL_FITNESS_LEVEL_LIST_PARAM = "physicalFitnessLevelList";
    public static final String PLACE_DS = "placeDS";
    public static final String PLACE_LIST_PARAM = "placeList";
    public static final String DAY_OF_WEEK_DS = "dayOfWeekDS";
    public static final String FIRST_CLASS_DAY_PARAM = "firstClassDay";
    public static final String SECOND_CLASS_DAY_PARAM = "secondClassDay";
    public static final String TRAINER_DS = "trainerDS";
    public static final String TRAINER_LIST_PARAM = "trainerList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()

                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(SEX_DS, sexDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))


                .addDataSource(searchListDS(STUDENT_LIST_DS, studentListDSColumnExtPoint(), studentListDSHandler()))

                .addDataSource(selectDS(STUDENT_STATUS_DS, StudentCatalogsManager.instance().studentStatusDSHandler()))
                .addDataSource(selectDS(PROGRAM_FORM_DS, developFormDS()))
                .addDataSource(selectDS(EDUCATION_LEVEL_DS, educationLevelDS()).addColumn(EducationLevelsHighSchool.fullTitle().s()))
                .addDataSource(selectDS(GROUP_DS, SectionApplicationManager.instance().groupDSHandler()))


                .addDataSource(searchListDS(SECTION_LIST_DS, sectionListDSColumnExtPoint(), sectionListDSHandler()))

                .addDataSource(selectDS(SECTION_TYPE_DS, sectionTypeDSHandler()))
                .addDataSource(selectDS(PHYSICAL_FITNESS_LEVEL_DS, physicalFitnessLevelDSHandler()))
                .addDataSource(selectDS(PLACE_DS, SportSectRegistriesManager.instance().sportPlaceDSHandler())
                                       .addColumn("title", "", source -> ((UniplacesPlace) source).getTitleWithLocation()))
                .addDataSource(selectDS(TRAINER_DS, SportSectRegistriesManager.instance().trainerDSHandler()))
                .addDataSource(selectDS(DAY_OF_WEEK_DS, SportSectRegistriesManager.instance().dayOfWeekDSHandler()))

                .create();
    }


    @Bean
    public ColumnListExtPoint studentListDSColumnExtPoint()
    {
        IPublisherLinkResolver studentResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
            }

            @Override
            public String getComponentName(final IEntity entity)
            {
                return IUniComponents.STUDENT_PUB;
            }
        };

        return columnListExtPointBuilder(STUDENT_LIST_DS)
                .addColumn(checkboxColumn(STUDENT_LIST_CHECKBOX))
                .addColumn(publisherColumn(STUDENT_LIST_FIO, Student.fullFio().s()).publisherLinkResolver(studentResolver).order())
                .addColumn(textColumn(STUDENT_LIST_COURSE, Student.course().title().s()).order())
                .addColumn(textColumn(STUDENT_LIST_STATUS, Student.status().title().s()).order())
                .addColumn(textColumn(STUDENT_LIST_GROUP, Student.group().title().s()).order())
                .addColumn(textColumn(STUDENT_LIST_FORMATIVE_ORG_UNIT, Student.educationOrgUnit().formativeOrgUnit().shortTitle().s()).order())
                .addColumn(textColumn(STUDENT_LIST_PROGRAM_FORM, Student.educationOrgUnit().developForm().programForm().title().s()).order())
                .addColumn(textColumn(STUDENT_LIST_EDUCATION_LEVEL, Student.educationOrgUnit().educationLevelHighSchool().s())
                                   .formatter(source -> ((EducationLevelsHighSchool) source).getPrintTitle()).order())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String alias = "e";
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Student.class, alias)
                        .column(property(alias))

                        .fetchPath(DQLJoinType.left, Student.educationOrgUnit().fromAlias(alias), "edu")
                        .fetchPath(DQLJoinType.left, Student.educationOrgUnit().formativeOrgUnit().fromAlias(alias), "fou")
                        .fetchPath(DQLJoinType.left, Student.educationOrgUnit().educationLevelHighSchool().fromAlias(alias), "lvl")
                        .fetchPath(DQLJoinType.left, Student.course().fromAlias(alias), "cr")
                        .fetchPath(DQLJoinType.left, Student.group().fromAlias(alias), "gr")
                        .joinPath(DQLJoinType.left, Student.group().fromAlias(alias), "g");

                SectionApplicationManager.filterStudents4Applications(builder, alias,
                                                                      context.get(EDUCATION_YEAR_PARAM),
                                                                      context.get(COURSE_PARAM),
                                                                      context.get(SEX_PARAM),
                                                                      context.get(FORMATIVE_ORG_UNIT_PARAM),
                                                                      context.get(PROGRAM_FORM_PARAM),
                                                                      context.get(EDUCATION_LEVEL_PARAM),
                                                                      context.get(GROUP_PARAM),
                                                                      context.get(STUDENT_STATUS_PARAM),
                                                                      null);

                EntityOrder entityOrder = input.getEntityOrder();
                DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(SectionApplication.class, alias);
                orderRegistry
                        .setOrders(Student.educationOrgUnit().formativeOrgUnit().shortTitle(),
                                   new OrderDescription(Student.educationOrgUnit().formativeOrgUnit().shortTitle()))
                        .setOrders(Student.course().title(), new OrderDescription(Student.course().title()))
                        .setOrders(Student.status().title(), new OrderDescription(Student.status().title()))
                        .setOrders(Student.educationOrgUnit().educationLevelHighSchool(), new OrderDescription(Student.educationOrgUnit().educationLevelHighSchool().title()))
                        .setOrders(Student.educationOrgUnit().developForm().programForm().title(),
                                   new OrderDescription(Student.educationOrgUnit().developForm().programForm().title()))
                        .setOrders(Student.group().title(), new OrderDescription("g", Group.title()))
                        .setOrders(Student.fullFio(),
                                   new OrderDescription(Student.person().identityCard().lastName()),
                                   new OrderDescription(Student.person().identityCard().firstName()),
                                   new OrderDescription(Student.person().identityCard().middleName()));
                orderRegistry.applyOrder(builder, entityOrder);

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
            }
        };
    }


    @Bean
    public ColumnListExtPoint sectionListDSColumnExtPoint()
    {
        IPublisherLinkResolver sectionResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
            }

            @Override
            public String getComponentName(final IEntity entity)
            {
                return SportSectRegistriesPub.class.getSimpleName();
            }
        };

        return columnListExtPointBuilder(SECTION_LIST_DS)
                .addColumn(radioColumn(SECTION_LIST_RADIO))
                .addColumn(publisherColumn(SECTION_LIST_TITLE, SportSection.title().s()).publisherLinkResolver(sectionResolver).order())
                .addColumn(textColumn(SECTION_LIST_SECTION_TYPE, SportSection.type().title().s()).order())
                .addColumn(textColumn(SECTION_LIST_FORMATIVE_ORG_UNIT, SportSection.formativeOrgUnit().shortTitle().s()).order())
                .addColumn(textColumn(SECTION_LIST_PHYSICAL_FITNESS_LEVEL, SportSection.physicalFitnessLevel().title().s()).order())
                .addColumn(textColumn(SECTION_LIST_COURSE, SECTION_LIST_COURSE))
                .addColumn(textColumn(SECTION_LIST_SEX, SECTION_LIST_SEX))
                .addColumn(textColumn(SECTION_LIST_PLACE, SportSection.place().s()).formatter(source -> ((UniplacesPlace) source).getTitleWithLocation()).order())
                .addColumn(textColumn(SECTION_LIST_TRAINER, SECTION_LIST_TRAINER).order())
                .addColumn(textColumn(SECTION_LIST_FIRST_CLASS, SECTION_LIST_FIRST_CLASS).order())
                .addColumn(textColumn(SECTION_LIST_SECOND_CLASS, SECTION_LIST_SECOND_CLASS).order())
                .addColumn(textColumn(SECTION_LIST_FREE_NUMBERS, SECTION_LIST_FREE_NUMBERS).order())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sectionListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String alias = "e";
                DQLSelectBuilder builder = getSportSectionListBuilder(alias, context);

                OrderDescription titleOrder = new OrderDescription(alias, SportSection.title(), OrderDirection.asc, false);

                builder.joinPath(DQLJoinType.left, SportSection.formativeOrgUnit().fromAlias(alias), "foe");

                EntityOrder entityOrder = input.getEntityOrder();
                if (entityOrder.getColumnName().equals(SECTION_LIST_FREE_NUMBERS))
                    builder.order(SECTION_LIST_FREE_NUMBERS, entityOrder.getDirection());

                DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(SportSection.class, alias);
                orderRegistry
                        .setOrders(SportSection.title(), new OrderDescription(SportSection.title()))
                        .setOrders(SportSection.type().title(), new OrderDescription(SportSection.type().title()))
                        .setOrders(SportSection.formativeOrgUnit().shortTitle(), new OrderDescription("foe", OrgUnit.shortTitle()), titleOrder)
                        .setOrders(SportSection.physicalFitnessLevel().title(), new OrderDescription(SportSection.physicalFitnessLevel().title()))
                        .setOrders(SportSection.place().title(), new OrderDescription(SportSection.place().title()))

                        .setOrders(SECTION_LIST_FIRST_CLASS,
                                   new OrderDescription(SportSection.firstClassDay()),
                                   new OrderDescription(SportSection.firstClassTime()), titleOrder)

                        .setOrders(SECTION_LIST_SECOND_CLASS,
                                   new OrderDescription(SportSection.secondClassDay()),
                                   new OrderDescription(SportSection.secondClassTime()), titleOrder)

                        .setOrders(SportSection.numbers(), new OrderDescription(SportSection.numbers()));

                builder.joinPath(DQLJoinType.left, SportSection.trainer().fromAlias(alias), "tr")
                        .joinPath(DQLJoinType.left, PpsEntry.person().fromAlias("tr"), "tr1")
                        .joinPath(DQLJoinType.left, Person.identityCard().fromAlias("tr1"), "tr2");
                orderRegistry.setOrders(SECTION_LIST_TRAINER,
                                        new OrderDescription("tr2", IdentityCard.lastName()),
                                        new OrderDescription("tr2", IdentityCard.firstName()),
                                        new OrderDescription("tr2", IdentityCard.middleName()));

                orderRegistry.applyOrder(builder, entityOrder);

                return wrapSportSectionList(DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build());
            }
        };
    }

    protected DQLSelectBuilder getSportSectionListBuilder(String alias, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SportSection.class, alias)
                .column(property(alias))
                .column(SportSection.getFreePlacesExpression(alias), SECTION_LIST_FREE_NUMBERS);

        FilterUtils.applySelectFilter(builder, alias, SportSection.educationYear(), context.get(EDUCATION_YEAR_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.type(), context.get(SECTION_TYPE_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.place(), context.get(PLACE_LIST_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.trainer(), context.get(TRAINER_LIST_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.physicalFitnessLevel(), context.get(PHYSICAL_FITNESS_LEVEL_LIST_PARAM));

        DataWrapper firstDay = context.get(FIRST_CLASS_DAY_PARAM);
        if (firstDay != null)
            FilterUtils.applySelectFilter(builder, alias, SportSection.firstClassDay(), firstDay.<DayOfWeek>getWrapped().getValue());
        DataWrapper secondDay = context.get(SECOND_CLASS_DAY_PARAM);
        if (secondDay != null)
            FilterUtils.applySelectFilter(builder, alias, SportSection.secondClassDay(), secondDay.<DayOfWeek>getWrapped().getValue());

        builder.where(gt(SportSection.getFreePlacesExpression(alias), value(0)));

        SportSection.addWhereExistsCourseRel(builder, property(alias), context.get(COURSE_PARAM));
        SportSection.addWhereExistsSexRel(builder, property(alias), context.get(SEX_PARAM));

        OrgUnit fou = context.get(FORMATIVE_ORG_UNIT_PARAM);
        //Формирующее подразделение как выбрано в фильтре или пустое
        if ((Boolean) context.get(ALL_COMMON_FILTERS_REQUIRED) || (fou != null))
            builder.where(or(eq(property(alias, SportSection.formativeOrgUnit()), value(fou)),
                             isNull(property(alias, SportSection.formativeOrgUnit()))));

        return builder;
    }

    protected DSOutput wrapSportSectionList(DSOutput output)
    {
        ICommonDAO dao = DataAccessServices.dao();
        List<Long> sectionIds = output.getRecordList().stream()
                .map(o -> (SportSection) ((Object[]) o)[0]).map(EntityBase::getId).collect(Collectors.toList());
        Map<Long, StringBuilder> coursesBySection = dao.getList(SportSection2CourseRel.class,
                                                                SportSection2CourseRel.section().id().s(), sectionIds)
                .stream()
                .sorted((o1, o2) -> Integer.compare(o1.getCourse().getIntValue(), o2.getCourse().getIntValue()))
                .collect(Collectors.groupingBy(rel -> rel.getSection().getId(),
                                               Collectors.mapping(rel -> rel,
                                                                  Collector.of(StringBuilder::new, (b, rel) -> {
                                                                      if (b.length() > 0)
                                                                          b.append(", ");
                                                                      b.append(rel.getCourse().getTitle());
                                                                  }, StringBuilder::append)
                                               )));

        Map<Long, StringBuilder> sexBySection = dao.getList(SportSection2SexRel.class,
                                                            SportSection2SexRel.section().id().s(), sectionIds)
                .stream()
                .collect(Collectors.groupingBy(rel -> rel.getSection().getId(),
                                               Collectors.mapping(rel -> rel,
                                                                  Collector.of(StringBuilder::new, (b, rel) -> {
                                                                      if (b.length() > 0)
                                                                          b.append(", ");
                                                                      b.append(rel.getSex().getShortTitle());
                                                                  }, StringBuilder::append)
                                               )));

        CollectionUtils.transform(output.getRecordList(), in -> {
            if (in instanceof DataWrapper)
                return in;

            SportSection section = (SportSection) ((Object[]) in)[0];

            StringBuilder courses = coursesBySection.get(section.getId());
            StringBuilder sexes = sexBySection.get(section.getId());

            DataWrapper wrapper = new DataWrapper(section);
            wrapper.setProperty(SECTION_LIST_FREE_NUMBERS, ((Object[]) in)[1]);
            wrapper.setProperty(SECTION_LIST_FIRST_CLASS, section.getFirstClassInWeek());
            wrapper.setProperty(SECTION_LIST_SECOND_CLASS, section.getSecondClassInWeek());
            wrapper.setProperty(SECTION_LIST_TRAINER, section.getTrainer() == null ? "" : section.getTrainer().getFio());
            wrapper.setProperty(SECTION_LIST_COURSE, courses == null ? "" : courses.toString());
            wrapper.setProperty(SECTION_LIST_SEX, sexes == null ? "" : sexes.toString());

            return wrapper;
        });

        return output;
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> dql.where(ge(property(alias, EducationYear.intValue()),
                                                                         value(EducationYear.getCurrentRequired().getIntValue()))));
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return Course.defaultSelectDSHandler(getName()).customize((alias, dql, context, filter) -> dql.where(le(property(alias, Course.intValue()), value(3))));
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sexDSHandler()
    {
        return new DefaultActiveCatalogComboDSHandler(this.getName(), Sex.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developFormDS()
    {
        return EduProgramForm.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelDS()
    {
        return new EntityComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
                .customize((alias, dql, context, filter) -> {
                    FilterUtils.applyLikeFilter(dql, filter, EducationLevelsHighSchool.printTitle().fromAlias(alias));
                    return dql;
                })
                .order(EducationLevelsHighSchool.code())
                .pageable(true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sectionTypeDSHandler()
    {
        return SportSectionType.defaultSelectDSHandler(this.getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> physicalFitnessLevelDSHandler()
    {
        return PhysicalFitnessLevel.defaultSelectDSHandler(getName());
    }
}