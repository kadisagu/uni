package ru.tandemservice.unitutor.component.awp.StudentSubscribesAddEdit;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.dao.DisciplineWrap;
import ru.tandemservice.unitutor.dao.IDaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;
import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {

        model.setSendMail(true);

        if (model.getTutorId() != null) {
            Employee tutor = get(Employee.class, model.getTutorId());
            model.setTutor(tutor);
        }

        if (model.getElectiveDisciplineSubscribeId() != null) {
            // режим редактирования для ДПВ
            ElectiveDisciplineSubscribe ed = get(model.getElectiveDisciplineSubscribeId());
            model.setElectiveDisciplineSubscribe(ed);

            Student student = ed.getStudent();
            model.setStudent(student);

            model.setYearDistributionPart(ed.getElectiveDiscipline().getYearPart());
            model.setEducationYear(ed.getElectiveDiscipline().getEducationYear());
        }
        else {
            if (model.getEducationYearId() != null)
                model.setEducationYear(get(EducationYear.class, model.getEducationYearId()));

            if (model.getYearDistributionPartId() != null)
                model.setYearDistributionPart(get(YearDistributionPart.class, model.getYearDistributionPartId()));

            if (model.getYearDistributionPartListModel() == null)
                model.setYearDistributionPartListModel(new DQLFullCheckSelectModel("title")
                {
                    @Override
                    protected DQLSelectBuilder query(String alias, String filter)
                    {
                        final DQLSelectBuilder dqlIn = DqlDisciplineBuilder.getDqlInYearDistributionPart(model.getEducationYear());
                        DQLSelectBuilder builder = new DQLSelectBuilder()
                                .fromEntity(YearDistributionPart.class, alias)
                                .where(in(
                                        property(YearDistributionPart.id().fromAlias(alias)),
                                        dqlIn.buildQuery()))
                                .order(property(YearDistributionPart.title().fromAlias(alias)));
                        FilterUtils.applySimpleLikeFilter(builder, alias, YearDistributionPart.title(), filter);
                        return builder;
                    }
                });

            model.setGroupListModel(new DQLFullCheckSelectModel()
            {
                @Override
                protected DQLSelectBuilder query(String alias, String filter)
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, alias);
                    if (model.getOrgUnitIds()!= null && !model.getOrgUnitIds().isEmpty())
                        builder.where(in(property(alias, Group.educationOrgUnit().formativeOrgUnit().id()), model.getOrgUnitIds()));
                    builder.where(eq(property(alias, Group.archival()), value(Boolean.FALSE)));
                    FilterUtils.applySimpleLikeFilter(builder, alias, Group.title(), filter);
                    return builder;
                }
            });

        }


        List<Employee> lstEmployee = model.getFilterEmployeeList();
        List<OrgUnit> orgUnitList = getFilterOrgUnitList(model);
        List<OrgUnit> cathedraOrgUnitList = getFilterCathedraOrgUnit(model);

        if (model.getStudent() == null) {
            model.setStudentSelectModel(new DQLFullCheckSelectModel(Student.titleWithFio())
            {
                @Override
                protected DQLSelectBuilder query(String alias, String filter)
                {
                    DQLSelectBuilder dqlIn = DqlDisciplineBuilder.getDqlInStudent(model.getEducationYear(), model.getYearDistributionPart(),
                                                      null, model.getGroupList(), lstEmployee, orgUnitList, cathedraOrgUnitList);
                    DQLSelectBuilder builder = new DQLSelectBuilder()
                            .fromEntity(Student.class, alias)
                            .where(in(
                                    property(Student.id().fromAlias(alias)),
                                    dqlIn.buildQuery()))
                            .order(property(Student.person().identityCard().fullFio().fromAlias(alias)));
                    FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().fullFio(), filter);
                    return builder;
                }
            });
        }
        else {
            // обновить список выбранных дисциплин
            // для ДПВ
            if (ElectiveDisciplineTypeCodes.DPV.equals(model.getElectiveDisciplineTypeCode())) {
                updateDisciplineListModel(model);
            }
            // ДЛЯ УДВ и УФ
            else {
                updateElectiveRegistryDisciplineModel(model);
            }

        }

        model.setEduYearModel(new LazySimpleSelectModel<>(EducationYear.class, EducationYear.title().s()));
    }

    @Override
    public void prepareListDataSource(Model model) {

        if (model.isDpv()) {
            DynamicListDataSource<DisciplineWrap> dataSource = model.getDataSource();
            List<DisciplineWrap> lst = model.getDataList();
            dataSource.setCountRow(lst.size());
            dataSource.createPage(lst);
        }
        else {
            DynamicListDataSource<ElectiveDiscipline> dataSource = model.getElectiveDataSource();
            List<ElectiveDiscipline> lst = model.getElectiveDataList();
            dataSource.setCountRow(lst.size());
            dataSource.createPage(lst);
        }
    }

    @Override
    public void updateDisciplineListModel(Model model)
    {
        YearDistributionPart filterYearDistributionPart = model.getYearDistributionPart();
        EducationYear year = model.getEducationYear();
        List<DisciplineWrap> lst = getDisciplineWrapList(year, filterYearDistributionPart, model.getStudent(), model);

        Collection<IEntity> collection = new ArrayList<>();

        // проставим чек боксы
        for (DisciplineWrap wrap : lst) {
            if (wrap.getElectiveDisciplineSubscribeId() != null)
                collection.add(wrap);

        }

        ((CheckboxColumn) model.getDataSource().getColumn("select")).setSelectedObjects(collection);
        model.setDataList(lst);
    }

    @Override
    public void updateElectiveRegistryDisciplineModel(Model model)
    {
        YearDistributionPart filterYearDistributionPart = model.getYearDistributionPart();
        EducationYear year = model.getEducationYear();
        Student student = null;
        if (model.getStudent() != null)
            student = model.getStudent();

        List<ElectiveDiscipline> lst = getDisciplineWrapFromRegistryList(year, filterYearDistributionPart, student, model);

        Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> subscribesMap = OptionalDisciplineUtil.getSubscribesMap(model.getStudent(), lst);
        Collection<IEntity> collection = new ArrayList<>();

        // проставим чек боксы
        for (ElectiveDiscipline electiveDiscipline : lst) {
            ElectiveDisciplineSubscribe subscribe = subscribesMap.get(electiveDiscipline);
            if (subscribe != null && subscribe.getRemovalDate() == null)
                collection.add(electiveDiscipline);
        }

        ((CheckboxColumn) model.getElectiveDataSource().getColumn("select")).setSelectedObjects(collection);
        model.setSubscribesMap(subscribesMap);
        model.setElectiveDataList(lst);
    }

    /**
     * УДВ или УФ в зависимости от типа добавляемой дисциплины
     *
     * @return
     */
    private List<ElectiveDiscipline> getDisciplineWrapFromRegistryList(EducationYear educationYear, YearDistributionPart yearDistributionPart, Student student, Model model)
    {
        StringBuilder warning = new StringBuilder();
        List<ElectiveDiscipline> retVal = DaoStudentSubscribe.instanse().getElectiveFromRegistryList(educationYear, yearDistributionPart, student,
                                                                                                        model.getElectiveDisciplineTypeCode(), warning);

        model.setWarning(warning.toString());
        return retVal;
    }

    /**
     * ДПВ
     *
     * @return
     */
    private List<DisciplineWrap> getDisciplineWrapList(EducationYear educationYear, YearDistributionPart yearDistributionPart, Student student, Model model)
    {
        StringBuilder warning = new StringBuilder();
        List<DisciplineWrap> retVal = DaoStudentSubscribe.instanse().getDisciplineWrapList(educationYear, yearDistributionPart,
                                                                   student, warning, false, false, ElectiveDisciplineTypeCodes.DPV);

        model.setWarning(warning.toString());
        return retVal;
    }

    public static String MYTEX = "MYTEX_MVC";

    @Override
    public void saveSubscribe(Model model)
    {
        synchronized (MYTEX) {
            IDaoStudentSubscribe idao = DaoStudentSubscribe.instanse();

            // загрузить все в Map - electiveDiscipline и electiveDisciplineSubscribe
            List<DisciplineWrap> selectedDisciplines = model.getDataSource().getSelectedEntities();
            Student student = model.getStudent();
            List<Long> idsElectiveDiscipline = new ArrayList<>();
            List<ElectiveDiscipline> lstEd = new ArrayList<>();

            for (DisciplineWrap dw : selectedDisciplines) {
                if (dw.getElectiveDiscipline() != null) {
                    idsElectiveDiscipline.add(dw.getElectiveDiscipline().getId());
                    lstEd.add(dw.getElectiveDiscipline());
                }
            }

            boolean sendMail = model.isSendMail();
            idao.saveStudentSubscribeByYearAndPart(student, model.getEducationYear(), model.getYearDistributionPart(), lstEd,
                                                   null, sendMail, false);
        }
    }

    @Override
    public void saveElectiveSubscribe(Model model) {
        synchronized (MYTEX) {

            Student student = model.getStudent();

            int count = DaoStudentSubscribe.instanse().getCountElectiveInWorkPlan(model.getEducationYear(), model.getYearDistributionPart(), student, model.getElectiveDisciplineTypeCode());

            // подписываем
            List<ElectiveDiscipline> selectedDisciplines = model.getElectiveDataSource().getSelectedEntities();

            if (selectedDisciplines.size() > count)
                throw new ApplicationException("Нужно выбрать " + count + " " + getDiscStr(count));

            List<ElectiveDisciplineSubscribe> subscibeList = new ArrayList<>();

            for (ElectiveDiscipline ed : selectedDisciplines) {
                ElectiveDisciplineSubscribe subscribe = model.getSubscribesMap().remove(ed);
                if (subscribe == null)
                {
                    subscribe = new ElectiveDisciplineSubscribe(ed, student, new Date(), null, null);
                    subscibeList.add(subscribe);
                }
                else if (subscribe.getRemovalDate() != null) {
                    subscibeList.add(subscribe);
                }
            }

            DaoStudentSubscribe.instanse().saveElectiveSubscribe(student, subscibeList, false);

            // отписываем
            List<ElectiveDisciplineSubscribe> unsubscribeLst = new ArrayList<>();
            for (Map.Entry<ElectiveDiscipline, ElectiveDisciplineSubscribe> entry : model.getSubscribesMap().entrySet()) {
                    unsubscribeLst.add(entry.getValue());
            }

            DaoStudentSubscribe.instanse().unsubscribeElectiveDisciplines(student, unsubscribeLst, false);
        }
    }

    private String getDiscStr(int count) {
        if (count == 1)
            return "дисциплину";
        else if (count > 1 && count < 5)
            return "дисциплины";
        else
            return "дисциплин";

    }

    private List<OrgUnit> getFilterOrgUnitList(Model model) {
        List<OrgUnit> lst = new ArrayList<>();

        if (model.getOrgUnitIds() != null && !model.getOrgUnitIds().isEmpty()) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou")
                    .where(ne(property("ou", OrgUnit.orgUnitType().code()), value(OrgUnitTypeCodes.CATHEDRA)))
                    .where(in(property("ou.id"), model.getOrgUnitIds()));
            lst = getList(builder);
        }
        return lst;
    }

    private List<OrgUnit> getFilterCathedraOrgUnit(Model model) {
        List<OrgUnit> lst = new ArrayList<>();

        if (model.getOrgUnitIds() != null && !model.getOrgUnitIds().isEmpty()) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou")
                    .where(eq(property("ou", OrgUnit.orgUnitType().code()), value(OrgUnitTypeCodes.CATHEDRA)))
                    .where(in(property("ou.id"), model.getOrgUnitIds()));
            lst = getList(builder);
        }
        return lst;
    }
}
