package ru.tandemservice.unitutor.utils;

import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2WorkPlanGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class TutorBuilderUtils {

    public static final String EPP_STUDENT2WORK_PLAN_ALIAS = "st2wp";
    public static final String EPP_STUDENT2EDU_PLAN_VERSION = "s2epv";
    public static final String EPP_EDU_PLAN_ALIAS = "ep";
    public static final String EPP_EDU_PLAN_VERSION_ALIAS = "epv";
    public static final String EPP_EDU_PLAN_VERSION_BLOCK_ALIAS = "epvBlock";
    public static final String EPP_EPV_GROUP_IM_ROW_ALIAS = "eegir";
    public static final String EPP_EPV_REGISTRY_ROW_ALIAS = "eerr";
    public static final String EPP_REGISTRY_ELEMENT_PART_ALIAS = "erep";
    public static final String EPP_WORK_PLAN_ALIAS = "wp";
    public static final String EPP_WORK_PLAN_VERSION_ALIAS = "wp_version";
    public static final String EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS = "wp_row";
    public static final String ELECTIVE_DISCIPLINE_ALIAS = "ed";
    public static final String ELECTIVE_ALIAS = "e";
    public static final String ELECTIVE_DISCIPLINE_SUBSCRIBE_ALIAS = "eds";
    public static final String DATA_SOURCE_ALIAS = "ds";

    /**
     * Связка записи в РУПе и группы дисциплин по выбору
     *
     * @return
     */
    public static DQLSelectBuilder getOnWorkPlanBuilder() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS)
                .joinEntity(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS, DQLJoinType.inner, EppWorkPlan.class, EPP_WORK_PLAN_ALIAS,
                            eq(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS)),
                               property(EppWorkPlan.id().fromAlias(EPP_WORK_PLAN_ALIAS))))
                .joinEntity(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS, DQLJoinType.inner, EppRegistryElementPart.class, EPP_REGISTRY_ELEMENT_PART_ALIAS,
                            eq(property(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS)),
                               property(EppRegistryElementPart.id().fromAlias(EPP_REGISTRY_ELEMENT_PART_ALIAS))))
                .joinEntity(EPP_REGISTRY_ELEMENT_PART_ALIAS, DQLJoinType.inner, EppEpvRegistryRow.class, EPP_EPV_REGISTRY_ROW_ALIAS,
                            eq(property(EppRegistryElementPart.registryElement().fromAlias(EPP_REGISTRY_ELEMENT_PART_ALIAS)),
                               property(EppEpvRegistryRow.registryElement().fromAlias(EPP_EPV_REGISTRY_ROW_ALIAS))))
                .joinEntity(EPP_EPV_REGISTRY_ROW_ALIAS, DQLJoinType.inner, EppEpvGroupImRow.class, EPP_EPV_GROUP_IM_ROW_ALIAS,
                            eq(property(EppEpvRegistryRow.parent().id().fromAlias(EPP_EPV_REGISTRY_ROW_ALIAS)),
                               property(EppEpvGroupImRow.id().fromAlias(EPP_EPV_GROUP_IM_ROW_ALIAS))));

        return builder;
    }

    /**
     * Связка записи в версии РУПа и группы дисциплин по выбору
     *
     * @return
     */
    public static DQLSelectBuilder getOnWorkPlanVersionBuilder() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS)
                .joinEntity(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS, DQLJoinType.inner, EppWorkPlanVersion.class, EPP_WORK_PLAN_VERSION_ALIAS,
                            eq(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS)),
                               property(EppWorkPlanVersion.id().fromAlias(EPP_WORK_PLAN_VERSION_ALIAS))))
                .joinEntity(EPP_WORK_PLAN_VERSION_ALIAS, DQLJoinType.inner, EppWorkPlan.class, EPP_WORK_PLAN_ALIAS,
                            eq(property(EppWorkPlanVersion.parent().id().fromAlias(EPP_WORK_PLAN_VERSION_ALIAS)),
                               property(EppWorkPlan.id().fromAlias(EPP_WORK_PLAN_ALIAS))))
                .joinEntity(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS, DQLJoinType.inner, EppRegistryElementPart.class, EPP_REGISTRY_ELEMENT_PART_ALIAS,
                            eq(property(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS)),
                               property(EppRegistryElementPart.id().fromAlias(EPP_REGISTRY_ELEMENT_PART_ALIAS))))
                .joinEntity(EPP_REGISTRY_ELEMENT_PART_ALIAS, DQLJoinType.inner, EppEpvRegistryRow.class, EPP_EPV_REGISTRY_ROW_ALIAS,
                            eq(property(EppRegistryElementPart.registryElement().fromAlias(EPP_REGISTRY_ELEMENT_PART_ALIAS)),
                               property(EppEpvRegistryRow.registryElement().fromAlias(EPP_EPV_REGISTRY_ROW_ALIAS))))
                .joinEntity(EPP_EPV_REGISTRY_ROW_ALIAS, DQLJoinType.inner, EppEpvGroupImRow.class, EPP_EPV_GROUP_IM_ROW_ALIAS,
                            eq(property(EppEpvRegistryRow.parent().id().fromAlias(EPP_EPV_REGISTRY_ROW_ALIAS)),
                               property(EppEpvGroupImRow.id().fromAlias(EPP_EPV_GROUP_IM_ROW_ALIAS))));

        return builder;
    }


    /**
     * Привязка сущностей УПа к РУПу для использования в запросе, в частности для фильтрации по орг. юниту
     *
     * @param builder
     *
     * @return
     */
    public static DQLSelectBuilder getEduPlanOnWorkPlanBuilder(DQLSelectBuilder builder) {
        builder.joinEntity(EPP_WORK_PLAN_ALIAS, DQLJoinType.inner, EppEduPlanVersionBlock.class, EPP_EDU_PLAN_VERSION_BLOCK_ALIAS,
                           eq(property(EppWorkPlan.parent().id().fromAlias(EPP_WORK_PLAN_ALIAS)),
                              property(EppEduPlanVersionBlock.id().fromAlias(EPP_EDU_PLAN_VERSION_BLOCK_ALIAS))))
                .joinEntity(EPP_EDU_PLAN_VERSION_BLOCK_ALIAS, DQLJoinType.inner, EppEduPlanVersion.class, EPP_EDU_PLAN_VERSION_ALIAS,
                            eq(property(EppEduPlanVersionBlock.eduPlanVersion().id().fromAlias(EPP_EDU_PLAN_VERSION_BLOCK_ALIAS)),
                               property(EppEduPlanVersion.id().fromAlias(EPP_EDU_PLAN_VERSION_ALIAS))))
                .joinEntity(EPP_EDU_PLAN_VERSION_ALIAS, DQLJoinType.inner, EppEduPlan.class, EPP_EDU_PLAN_ALIAS,
                            eq(property(EppEduPlanVersion.eduPlan().id().fromAlias(EPP_EDU_PLAN_VERSION_ALIAS)),
                               property(EppEduPlan.id().fromAlias(EPP_EDU_PLAN_ALIAS))));

        return builder;
    }

    /**
     * Подзапрос для извлечения id РУПов для студентов, учащихся в указанных группах
     *
     * @param groupList
     *
     * @return
     */
    public static DQLSelectBuilder getWorkPlan2GroupSubDQL(List<Group> groupList) {

        DQLSelectBuilder subDQL = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, EPP_STUDENT2WORK_PLAN_ALIAS)
                .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias(EPP_STUDENT2WORK_PLAN_ALIAS), "wp")
                .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias(EPP_STUDENT2WORK_PLAN_ALIAS), "s2epv")
                .where(in(property(EppStudent2WorkPlan.studentEduPlanVersion().student().group().fromAlias(EPP_STUDENT2WORK_PLAN_ALIAS)), groupList))
                .where(isNull(property(EppStudent2WorkPlanGen.removalDate().fromAlias(EPP_STUDENT2WORK_PLAN_ALIAS))))
                .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
                .column(EppStudent2WorkPlan.workPlan().id().fromAlias(EPP_STUDENT2WORK_PLAN_ALIAS).s())
                .distinct();

        return subDQL;
    }

    /**
     * Подзапрос для извлечения дисциплин, входящих в группу дисциплин по выбору из указанных РУПов
     *
     * @param workPlanList
     *
     * @return
     */
    public static DQLSelectBuilder getDisciplineSubDQL(List<EppWorkPlanBase> workPlanList) {
        DQLSelectBuilder subDQL = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS)
                .where(in(property(EppWorkPlanRegistryElementRow.workPlan().fromAlias(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS)), workPlanList))
                .joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().fromAlias(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS), EPP_REGISTRY_ELEMENT_PART_ALIAS)
                .joinEntity(EPP_REGISTRY_ELEMENT_PART_ALIAS, DQLJoinType.inner, EppEpvRegistryRow.class, EPP_EPV_REGISTRY_ROW_ALIAS,
                            eq(property(EppRegistryElementPart.registryElement().fromAlias(EPP_REGISTRY_ELEMENT_PART_ALIAS)),
                               property(EppEpvRegistryRow.registryElement().fromAlias(EPP_EPV_REGISTRY_ROW_ALIAS))))
                .joinEntity(EPP_EPV_REGISTRY_ROW_ALIAS, DQLJoinType.inner, EppEpvGroupImRow.class, EPP_EPV_GROUP_IM_ROW_ALIAS,
                            eq(property(EppEpvRegistryRow.parent().id().fromAlias(EPP_EPV_REGISTRY_ROW_ALIAS)),
                               property(EppEpvGroupImRow.id().fromAlias(EPP_EPV_GROUP_IM_ROW_ALIAS))))
                .column(EppWorkPlanRegistryElementRow.id().fromAlias(EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS).s())
                .distinct();

        return subDQL;
    }

    /**
     * Агрегированный запрос, вычисляющий количество подписок на ДПВ для
     * повторного использования в качестве DataSource для существующего запроса
     *
     * @return DQLSelectBuilder
     * colums = {
     * ElectiveDiscipline.id AS id,
     * COUNT(ElectiveDisciplineSubscribe) AS subscribe}
     */
    public static DQLSelectBuilder getDisciplinePlacesDSSubDQL() {
        DQLSelectBuilder dataSourceSubDQL = new DQLSelectBuilder()
                .fromEntity(ElectiveDiscipline.class, "disc")
                .joinEntity("disc", DQLJoinType.left, ElectiveDisciplineSubscribe.class, "eds",
                            eq(property(ElectiveDisciplineSubscribe.electiveDiscipline().id().fromAlias("eds")),
                               property(ElectiveDiscipline.id().fromAlias("disc"))))

                .where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias("eds"))))
                .column("disc.id", "id")
                .column(DQLFunctions.count(ElectiveDisciplineSubscribe.id().fromAlias("eds").s()), "subscribe")
                .group(ElectiveDiscipline.id().fromAlias("disc").s());

        return dataSourceSubDQL;
    }

}
