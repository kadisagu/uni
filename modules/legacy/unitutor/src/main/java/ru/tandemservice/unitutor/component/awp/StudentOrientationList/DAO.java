package ru.tandemservice.unitutor.component.awp.StudentOrientationList;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.Student2Orientation;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    private static final String REL_ALIAS = "rel";
    private static final String STUDENT_ALIAS = "st";
    private static final String ORIENTATION_ALIAS = "orientation";

    private static final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(Student2Orientation.class, "rel");

    @Override
    public void prepare(final Model model) {
        model.getOrgUnitHolder().refresh();

        model.setEduYearModel(new LazySimpleSelectModel<>(EducationYear.class, EducationYear.title().s()));

        model.setCourseModel(new DQLFullCheckSelectModel(Course.title()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(Student.course().fromAlias(STUDENT_ALIAS).s())
                        .distinct();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Course.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));
                return builder;
            }
        });
        model.setDevelopFormModel(new DQLFullCheckSelectModel(DevelopForm.title()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(Student.educationOrgUnit().developForm().fromAlias(STUDENT_ALIAS).s())
                        .distinct();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DevelopForm.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));
                return builder;
            }
        });
        model.setEduLevelModel(new DQLFullCheckSelectModel(EducationLevelsHighSchool.fullTitle()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = getBuilder(model)
//                        .joinPath(DQLJoinType.inner, Student.educationOrgUnit().educationLevelHighSchool().fromAlias(STUDENT_ALIAS), "elhs")
                        .column(property(STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().id()))
                                        .distinct();
//                IUniBaseDao.instance.get().getList(subBuilder);
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, alias)
                        .where(in(property(alias + ".id"), subBuilder.buildQuery()));

                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle(), filter);

                return builder;
            }
        });
        model.setGroupModel(new DQLFullCheckSelectModel(Group.title()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(Student.group().fromAlias(STUDENT_ALIAS).s())
                        .distinct();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));

                FilterUtils.applySimpleLikeFilter(builder, alias, Group.title(), filter);
                return builder;
            }
        });
        model.setOrientationModel(new DQLFullCheckSelectModel(EducationLevelsHighSchool.fullTitle()) {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(EducationOrgUnit.educationLevelHighSchool().fromAlias(ORIENTATION_ALIAS).s())
                        .distinct();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));

                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle(), filter);
                return builder;
            }
        });
        model.setStudentModel(new DQLFullCheckSelectModel(Student.person().fullFio()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(STUDENT_ALIAS)
                        .distinct();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));

                FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().fullFio(), filter);
                return builder;
            }
        });
        model.setActualModel(new LazySimpleSelectModel<>(Model.YES_NO_ITEMS));
    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<Student2Orientation> dataSource = model.getDataSource();
        DQLSelectBuilder builder = getBuilder(model)
                .column(REL_ALIAS);

        order.applyOrder(builder, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    private DQLSelectBuilder getBuilder(Model model) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student2Orientation.class, REL_ALIAS)
                .joinPath(DQLJoinType.inner, Student2Orientation.student().fromAlias(REL_ALIAS), STUDENT_ALIAS)
                .joinPath(DQLJoinType.inner, Student2Orientation.orientation().fromAlias(REL_ALIAS), ORIENTATION_ALIAS)
                .where(eqValue(property(Student2Orientation.educationYear().fromAlias(REL_ALIAS)), model.getEducationYear()))
                .where(eqValue(property(Student.educationOrgUnit().formativeOrgUnit().id().fromAlias(STUDENT_ALIAS)), model.getOrgUnitId()));

        applyFilters(builder, model);

        return builder;
    }

    private void applyFilters(DQLSelectBuilder builder, Model model) {
        FilterUtils.applySelectFilter(builder, STUDENT_ALIAS, Student.course(), model.getCourses());
        FilterUtils.applySelectFilter(builder, STUDENT_ALIAS, Student.educationOrgUnit().developForm(), model.getDevelopForms());
        FilterUtils.applySelectFilter(builder, STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool(), model.getEduLevels());
        FilterUtils.applySelectFilter(builder, STUDENT_ALIAS, Student.group(), model.getGroups());
        FilterUtils.applySelectFilter(builder, ORIENTATION_ALIAS, EducationOrgUnit.educationLevelHighSchool(), model.getOrientationsEHS());
        FilterUtils.applySelectFilter(builder, REL_ALIAS, Student2Orientation.student(), model.getStudents());
        if (model.isActual() != null) {
            FilterUtils.applySelectFilter(builder, REL_ALIAS, Student2Orientation.actual(), model.isActual());
        }
    }

    @Override
    public void deleteRecords(Collection<IEntity> list) {
        for (IEntity entity : list) {
            new DQLDeleteBuilder(Student2Orientation.class)
                    .where(eqValue(property(Student2Orientation.id()), entity.getId()))
                    .createStatement(getSession()).execute();
        }
    }
}

