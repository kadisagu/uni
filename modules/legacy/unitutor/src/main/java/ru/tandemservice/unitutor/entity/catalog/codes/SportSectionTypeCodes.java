package ru.tandemservice.unitutor.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип секции"
 * Имя сущности : sportSectionType
 * Файл data.xml : unitutor.data.xml
 */
public interface SportSectionTypeCodes
{
    /** Константа кода (code) элемента : Высшая школа (title) */
    String HIGH_SCHOOL = "tutor.highSchool";
    /** Константа кода (code) элемента : Вне сетки (title) */
    String OUTSIDE = "tutor.outside";

    Set<String> CODES = ImmutableSet.of(HIGH_SCHOOL, OUTSIDE);
}
