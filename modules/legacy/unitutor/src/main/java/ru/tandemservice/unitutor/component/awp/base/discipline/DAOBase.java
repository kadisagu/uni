package ru.tandemservice.unitutor.component.awp.base.discipline;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitutor.component.awp.base.IFilterProperties;
import ru.tandemservice.unitutor.component.awp.base.utils.SelectModelUtils;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;


import java.util.Calendar;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static ru.tandemservice.unitutor.utils.DqlDisciplineBuilder.getInOrgUnitFilter;


public abstract class DAOBase<T extends ModelBase>
        extends UniDao<T> implements IDAOBase<T>
{

    @Override
    public void prepare(T model)
    {
        // готовим модель
        prepareModel(model);
        super.prepare(model);
    }

    protected void prepareModel(T model)
    {
//        System.out.println("prepareModel.start");
//        Long startTime = Calendar.getInstance().getTimeInMillis();
        String postfix = model.getSecurityPostfix();

        // для преподавателя
        if (model.getEmployeeId() != null) {
            Employee employee = get(Employee.class, model.getEmployeeId());
            model.setEmployee(employee);
        }
        if (model.getEmployeePostId() != null) {
            EmployeePost employeePost = get(EmployeePost.class, model.getEmployeePostId());
            model.setEmployeePost(employeePost);

            model.setEmployeeId(employeePost.getEmployee().getId());
            model.setEmployee(employeePost.getEmployee());

            model.setOrgUnit(employeePost.getOrgUnit());

            // сотруднику-тьютору доступно все (если включено право на его текущую роль на подразделении)
            postfix = model.getSecurityPostfix() + "_"
                    + StringUtils.uncapitalize(employeePost.getOrgUnit().getOrgUnitType().getCode());
        }

        if (model.getOrgUnitId() != null) {
            OrgUnit orgUnit = get(OrgUnit.class, model.getOrgUnitId());
            model.setOrgUnit(orgUnit);
            postfix = postfix + "_" + StringUtils.uncapitalize(model.getOrgUnit().getOrgUnitType().getCode());
        }

        model.setSec(new CommonPostfixPermissionModel(postfix));


        // Значение по умолчанию - текущий учебный год из соответствующей настройки системы
        if (model.getSettings().get(IFilterProperties.EDUCATION_YEAR_FILTER) == null) {
            EducationYear currentEducationYear = get(EducationYear.class, EducationYear.current().s(), Boolean.TRUE);
            model.getSettings().set(IFilterProperties.EDUCATION_YEAR_FILTER, currentEducationYear);
        }


        prepareCommonFilters4Tab(model);
//        System.out.println("prepareModel.finish = " + (Calendar.getInstance().getTimeInMillis() - startTime)/1000);
    }


    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public void prepareCommonFilters4Tab(T model) {

        // учебный год
        model.setEducationYearListModel(new LazySimpleSelectModel<>(EducationYear.class, EducationYear.title().s()));

        // часть года
        model.setEducationYearPartListModel(new LazySimpleSelectModel<>(YearDistributionPart.class, YearDistributionPart.title().s()));
//        model.setEducationYearPartListModel(new DQLFullCheckSelectModel("title") {
//            @Override
//            protected DQLSelectBuilder query(String alias, String filter)
//            {
//                final DQLSelectBuilder dqlIn = DqlDisciplineBuilder.getDqlInYearDistributionPart(model.getFilterEducationYear());
//                DQLSelectBuilder builder = new DQLSelectBuilder()
//                        .fromEntity(YearDistributionPart.class, alias)
//                        .where(in(
//                                property(YearDistributionPart.id().fromAlias(alias)),
//                                dqlIn.buildQuery()))
//                        .order(property(YearDistributionPart.title().fromAlias(alias)));
//                FilterUtils.applySimpleLikeFilter(builder, alias, YearDistributionPart.title(), filter);
//                return builder;
//            }
//        });


        model.setEducationLevelHighSchoolListModel(new DQLFullCheckSelectModel("fullTitle") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder subBuilder = getInOrgUnitFilter(ImmutableList.of(model.getOrgUnit()), "eduOrgUnit");
                subBuilder.column(property(EducationOrgUnit.educationLevelHighSchool().id().fromAlias("eduOrgUnit")));

                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EducationLevelsHighSchool.class, alias)
                        .where(in(
                                property(EducationLevelsHighSchool.id().fromAlias(alias)),
                                subBuilder.buildQuery()))
                        .order(property(EducationLevelsHighSchool.code().fromAlias(alias)))
                        .order(property(EducationLevelsHighSchool.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle(), filter);

                return builder;
            }
        });

        // академическая группа
        model.setGroupListModel(new DQLFullCheckSelectModel("title")
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Group.class, alias);

                if (model.getFilterOrgUnit() != null && !model.getFilterOrgUnit().isEmpty())
                    builder.where(in(
                            property(Group.educationOrgUnit().formativeOrgUnit().fromAlias(alias)),
                            model.getFilterOrgUnit()));

                if (model.getFilterEmployee() != null && !model.getFilterEmployee().isEmpty())
                    builder.where(in(
                            property(Group.curator().employee().fromAlias(alias)),
                            model.getFilterEmployee()));

                builder.order(property(Group.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, Group.title(), filter);
                return builder;
            }
        });

        // фильтр по дисциплинам
        model.setDisciplineListModel(new DQLFullCheckSelectModel(EppRegistryElement.P_TITLE_WITH_NUMBER)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return SelectModelUtils.getDisciplineListSelectModel(
                        alias,
                        model.getFilterEducationYear(),
                        model.getFilterYearDistributionPart(),
                        model.getFilterEducationLevelsHighSchoolList(),
                        model.getFilterGroupList(),
                        model.getFilterEmployee(),
                        model.isCathedra() ? null : model.getFilterOrgUnit(),
                        model.getFilterCathedraOrgUnit(),
                        filter

                );
            }

        });

    }
}
