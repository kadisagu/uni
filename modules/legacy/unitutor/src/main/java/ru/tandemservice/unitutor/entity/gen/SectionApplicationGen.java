package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.catalog.ApplicationStatus4SportsSection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Заявка на спортивную секцию
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SectionApplicationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.SectionApplication";
    public static final String ENTITY_NAME = "sectionApplication";
    public static final int VERSION_HASH = 199383188;
    private static IEntityMeta ENTITY_META;

    public static final String L_SPORT_SECTION = "sportSection";
    public static final String L_STUDENT = "student";
    public static final String L_STATUS = "status";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String P_FROM_UNI = "fromUni";

    private SportSection _sportSection;     // Спротивная секция
    private Student _student;     // Студент
    private ApplicationStatus4SportsSection _status;     // Статус
    private Date _registrationDate;     // Дата подачи заявки
    private boolean _fromUni = false;     // Добавленна на стороне Юни

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Спротивная секция. Свойство не может быть null.
     */
    @NotNull
    public SportSection getSportSection()
    {
        return _sportSection;
    }

    /**
     * @param sportSection Спротивная секция. Свойство не может быть null.
     */
    public void setSportSection(SportSection sportSection)
    {
        dirty(_sportSection, sportSection);
        _sportSection = sportSection;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Статус. Свойство не может быть null.
     */
    @NotNull
    public ApplicationStatus4SportsSection getStatus()
    {
        return _status;
    }

    /**
     * @param status Статус. Свойство не может быть null.
     */
    public void setStatus(ApplicationStatus4SportsSection status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return Дата подачи заявки. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата подачи заявки. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Добавленна на стороне Юни. Свойство не может быть null.
     */
    @NotNull
    public boolean isFromUni()
    {
        return _fromUni;
    }

    /**
     * @param fromUni Добавленна на стороне Юни. Свойство не может быть null.
     */
    public void setFromUni(boolean fromUni)
    {
        dirty(_fromUni, fromUni);
        _fromUni = fromUni;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SectionApplicationGen)
        {
            setSportSection(((SectionApplication)another).getSportSection());
            setStudent(((SectionApplication)another).getStudent());
            setStatus(((SectionApplication)another).getStatus());
            setRegistrationDate(((SectionApplication)another).getRegistrationDate());
            setFromUni(((SectionApplication)another).isFromUni());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SectionApplicationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SectionApplication.class;
        }

        public T newInstance()
        {
            return (T) new SectionApplication();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sportSection":
                    return obj.getSportSection();
                case "student":
                    return obj.getStudent();
                case "status":
                    return obj.getStatus();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "fromUni":
                    return obj.isFromUni();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sportSection":
                    obj.setSportSection((SportSection) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "status":
                    obj.setStatus((ApplicationStatus4SportsSection) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "fromUni":
                    obj.setFromUni((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sportSection":
                        return true;
                case "student":
                        return true;
                case "status":
                        return true;
                case "registrationDate":
                        return true;
                case "fromUni":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sportSection":
                    return true;
                case "student":
                    return true;
                case "status":
                    return true;
                case "registrationDate":
                    return true;
                case "fromUni":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sportSection":
                    return SportSection.class;
                case "student":
                    return Student.class;
                case "status":
                    return ApplicationStatus4SportsSection.class;
                case "registrationDate":
                    return Date.class;
                case "fromUni":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SectionApplication> _dslPath = new Path<SectionApplication>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SectionApplication");
    }
            

    /**
     * @return Спротивная секция. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#getSportSection()
     */
    public static SportSection.Path<SportSection> sportSection()
    {
        return _dslPath.sportSection();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#getStatus()
     */
    public static ApplicationStatus4SportsSection.Path<ApplicationStatus4SportsSection> status()
    {
        return _dslPath.status();
    }

    /**
     * @return Дата подачи заявки. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Добавленна на стороне Юни. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#isFromUni()
     */
    public static PropertyPath<Boolean> fromUni()
    {
        return _dslPath.fromUni();
    }

    public static class Path<E extends SectionApplication> extends EntityPath<E>
    {
        private SportSection.Path<SportSection> _sportSection;
        private Student.Path<Student> _student;
        private ApplicationStatus4SportsSection.Path<ApplicationStatus4SportsSection> _status;
        private PropertyPath<Date> _registrationDate;
        private PropertyPath<Boolean> _fromUni;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Спротивная секция. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#getSportSection()
     */
        public SportSection.Path<SportSection> sportSection()
        {
            if(_sportSection == null )
                _sportSection = new SportSection.Path<SportSection>(L_SPORT_SECTION, this);
            return _sportSection;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Статус. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#getStatus()
     */
        public ApplicationStatus4SportsSection.Path<ApplicationStatus4SportsSection> status()
        {
            if(_status == null )
                _status = new ApplicationStatus4SportsSection.Path<ApplicationStatus4SportsSection>(L_STATUS, this);
            return _status;
        }

    /**
     * @return Дата подачи заявки. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(SectionApplicationGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Добавленна на стороне Юни. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SectionApplication#isFromUni()
     */
        public PropertyPath<Boolean> fromUni()
        {
            if(_fromUni == null )
                _fromUni = new PropertyPath<Boolean>(SectionApplicationGen.P_FROM_UNI, this);
            return _fromUni;
        }

        public Class getEntityClass()
        {
            return SectionApplication.class;
        }

        public String getEntityName()
        {
            return "sectionApplication";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
