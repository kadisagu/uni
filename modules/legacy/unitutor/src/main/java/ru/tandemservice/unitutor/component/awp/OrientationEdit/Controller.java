package ru.tandemservice.unitutor.component.awp.OrientationEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().update(model);
        deactivate(component, 2);
    }

}
