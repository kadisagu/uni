package ru.tandemservice.unitutor.dao.print;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;

import java.util.List;

/**
 * Печать заявления на ДПВ
 */
public interface IElectiveApplicationPrintDAO {

    /**
     * Сформировать документ для печати заявления на ДПВ
     *
     * @param student
     * @param disciplineTitle
     *
     * @return
     */
    public RtfDocument generateDocument(Student student, String disciplineTitle);

    /**
     * Многостраничный документ для печати заявления на ДПВ
     *
     * @param electiveSubscribes
     *
     * @return
     */
    public RtfDocument generateDocument(List<ElectiveDisciplineSubscribe> electiveSubscribes);
}
