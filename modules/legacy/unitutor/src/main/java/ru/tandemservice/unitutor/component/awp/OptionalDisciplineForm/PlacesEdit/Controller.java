package ru.tandemservice.unitutor.component.awp.OptionalDisciplineForm.PlacesEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public void onClickApply(IBusinessComponent component) {
        getDao().update(getModel(component));
        deactivate(component);
    }

}
