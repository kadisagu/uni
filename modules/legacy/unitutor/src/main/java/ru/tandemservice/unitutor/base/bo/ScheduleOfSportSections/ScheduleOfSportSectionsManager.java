/* $Id$ */
package ru.tandemservice.unitutor.base.bo.ScheduleOfSportSections;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Andrey Andreev
 * @since 05.09.2016
 */
@Configuration
public class ScheduleOfSportSectionsManager extends BusinessObjectManager
{
    public static ScheduleOfSportSectionsManager instance()
    {
        return instance(ScheduleOfSportSectionsManager.class);
    }
}