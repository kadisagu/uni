package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel;
import ru.tandemservice.unitutor.entity.catalog.SportSectionType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Спортивная секция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SportSectionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.SportSection";
    public static final String ENTITY_NAME = "sportSection";
    public static final int VERSION_HASH = 2040089098;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String P_TITLE = "title";
    public static final String L_TYPE = "type";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_PHYSICAL_FITNESS_LEVEL = "physicalFitnessLevel";
    public static final String P_NUMBERS = "numbers";
    public static final String P_HOURS = "hours";
    public static final String L_TUTOR_ORG_UNIT = "tutorOrgUnit";
    public static final String L_TRAINER = "trainer";
    public static final String L_PLACE = "place";
    public static final String P_FIRST_CLASS_DAY = "firstClassDay";
    public static final String P_FIRST_CLASS_TIME = "firstClassTime";
    public static final String P_SECOND_CLASS_DAY = "secondClassDay";
    public static final String P_SECOND_CLASS_TIME = "secondClassTime";

    private EducationYear _educationYear;     // Учебный год
    private String _title;     // Наименование секции
    private SportSectionType _type;     // Тип секции
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private PhysicalFitnessLevel _physicalFitnessLevel;     // Уровень подготовки
    private int _numbers;     // Количество мест
    private int _hours;     // Количество часов
    private OrgUnit _tutorOrgUnit;     // Кафедра
    private PpsEntry _trainer;     // Тренер
    private UniplacesPlace _place;     // Спортивный объект
    private Integer _firstClassDay;     // Первое занятие в неделю. День недели
    private Date _firstClassTime;     // Первое занятие в неделю. Время дня
    private Integer _secondClassDay;     // Второе занятие в неделю. День недели
    private Date _secondClassTime;     // Второе занятие в неделю. Время дня

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Наименование секции. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование секции. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Тип секции. Свойство не может быть null.
     */
    @NotNull
    public SportSectionType getType()
    {
        return _type;
    }

    /**
     * @param type Тип секции. Свойство не может быть null.
     */
    public void setType(SportSectionType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Формирующее подразделение.
     */
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Уровень подготовки. Свойство не может быть null.
     */
    @NotNull
    public PhysicalFitnessLevel getPhysicalFitnessLevel()
    {
        return _physicalFitnessLevel;
    }

    /**
     * @param physicalFitnessLevel Уровень подготовки. Свойство не может быть null.
     */
    public void setPhysicalFitnessLevel(PhysicalFitnessLevel physicalFitnessLevel)
    {
        dirty(_physicalFitnessLevel, physicalFitnessLevel);
        _physicalFitnessLevel = physicalFitnessLevel;
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     */
    @NotNull
    public int getNumbers()
    {
        return _numbers;
    }

    /**
     * @param numbers Количество мест. Свойство не может быть null.
     */
    public void setNumbers(int numbers)
    {
        dirty(_numbers, numbers);
        _numbers = numbers;
    }

    /**
     * @return Количество часов. Свойство не может быть null.
     */
    @NotNull
    public int getHours()
    {
        return _hours;
    }

    /**
     * @param hours Количество часов. Свойство не может быть null.
     */
    public void setHours(int hours)
    {
        dirty(_hours, hours);
        _hours = hours;
    }

    /**
     * @return Кафедра. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getTutorOrgUnit()
    {
        return _tutorOrgUnit;
    }

    /**
     * @param tutorOrgUnit Кафедра. Свойство не может быть null.
     */
    public void setTutorOrgUnit(OrgUnit tutorOrgUnit)
    {
        dirty(_tutorOrgUnit, tutorOrgUnit);
        _tutorOrgUnit = tutorOrgUnit;
    }

    /**
     * @return Тренер.
     */
    public PpsEntry getTrainer()
    {
        return _trainer;
    }

    /**
     * @param trainer Тренер.
     */
    public void setTrainer(PpsEntry trainer)
    {
        dirty(_trainer, trainer);
        _trainer = trainer;
    }

    /**
     * @return Спортивный объект. Свойство не может быть null.
     */
    @NotNull
    public UniplacesPlace getPlace()
    {
        return _place;
    }

    /**
     * @param place Спортивный объект. Свойство не может быть null.
     */
    public void setPlace(UniplacesPlace place)
    {
        dirty(_place, place);
        _place = place;
    }

    /**
     * @return Первое занятие в неделю. День недели.
     */
    public Integer getFirstClassDay()
    {
        return _firstClassDay;
    }

    /**
     * @param firstClassDay Первое занятие в неделю. День недели.
     */
    public void setFirstClassDay(Integer firstClassDay)
    {
        dirty(_firstClassDay, firstClassDay);
        _firstClassDay = firstClassDay;
    }

    /**
     * @return Первое занятие в неделю. Время дня.
     */
    public Date getFirstClassTime()
    {
        return _firstClassTime;
    }

    /**
     * @param firstClassTime Первое занятие в неделю. Время дня.
     */
    public void setFirstClassTime(Date firstClassTime)
    {
        dirty(_firstClassTime, firstClassTime);
        _firstClassTime = firstClassTime;
    }

    /**
     * @return Второе занятие в неделю. День недели.
     */
    public Integer getSecondClassDay()
    {
        return _secondClassDay;
    }

    /**
     * @param secondClassDay Второе занятие в неделю. День недели.
     */
    public void setSecondClassDay(Integer secondClassDay)
    {
        dirty(_secondClassDay, secondClassDay);
        _secondClassDay = secondClassDay;
    }

    /**
     * @return Второе занятие в неделю. Время дня.
     */
    public Date getSecondClassTime()
    {
        return _secondClassTime;
    }

    /**
     * @param secondClassTime Второе занятие в неделю. Время дня.
     */
    public void setSecondClassTime(Date secondClassTime)
    {
        dirty(_secondClassTime, secondClassTime);
        _secondClassTime = secondClassTime;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SportSectionGen)
        {
            setEducationYear(((SportSection)another).getEducationYear());
            setTitle(((SportSection)another).getTitle());
            setType(((SportSection)another).getType());
            setFormativeOrgUnit(((SportSection)another).getFormativeOrgUnit());
            setPhysicalFitnessLevel(((SportSection)another).getPhysicalFitnessLevel());
            setNumbers(((SportSection)another).getNumbers());
            setHours(((SportSection)another).getHours());
            setTutorOrgUnit(((SportSection)another).getTutorOrgUnit());
            setTrainer(((SportSection)another).getTrainer());
            setPlace(((SportSection)another).getPlace());
            setFirstClassDay(((SportSection)another).getFirstClassDay());
            setFirstClassTime(((SportSection)another).getFirstClassTime());
            setSecondClassDay(((SportSection)another).getSecondClassDay());
            setSecondClassTime(((SportSection)another).getSecondClassTime());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SportSectionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SportSection.class;
        }

        public T newInstance()
        {
            return (T) new SportSection();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationYear":
                    return obj.getEducationYear();
                case "title":
                    return obj.getTitle();
                case "type":
                    return obj.getType();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "physicalFitnessLevel":
                    return obj.getPhysicalFitnessLevel();
                case "numbers":
                    return obj.getNumbers();
                case "hours":
                    return obj.getHours();
                case "tutorOrgUnit":
                    return obj.getTutorOrgUnit();
                case "trainer":
                    return obj.getTrainer();
                case "place":
                    return obj.getPlace();
                case "firstClassDay":
                    return obj.getFirstClassDay();
                case "firstClassTime":
                    return obj.getFirstClassTime();
                case "secondClassDay":
                    return obj.getSecondClassDay();
                case "secondClassTime":
                    return obj.getSecondClassTime();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "type":
                    obj.setType((SportSectionType) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "physicalFitnessLevel":
                    obj.setPhysicalFitnessLevel((PhysicalFitnessLevel) value);
                    return;
                case "numbers":
                    obj.setNumbers((Integer) value);
                    return;
                case "hours":
                    obj.setHours((Integer) value);
                    return;
                case "tutorOrgUnit":
                    obj.setTutorOrgUnit((OrgUnit) value);
                    return;
                case "trainer":
                    obj.setTrainer((PpsEntry) value);
                    return;
                case "place":
                    obj.setPlace((UniplacesPlace) value);
                    return;
                case "firstClassDay":
                    obj.setFirstClassDay((Integer) value);
                    return;
                case "firstClassTime":
                    obj.setFirstClassTime((Date) value);
                    return;
                case "secondClassDay":
                    obj.setSecondClassDay((Integer) value);
                    return;
                case "secondClassTime":
                    obj.setSecondClassTime((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationYear":
                        return true;
                case "title":
                        return true;
                case "type":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "physicalFitnessLevel":
                        return true;
                case "numbers":
                        return true;
                case "hours":
                        return true;
                case "tutorOrgUnit":
                        return true;
                case "trainer":
                        return true;
                case "place":
                        return true;
                case "firstClassDay":
                        return true;
                case "firstClassTime":
                        return true;
                case "secondClassDay":
                        return true;
                case "secondClassTime":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationYear":
                    return true;
                case "title":
                    return true;
                case "type":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "physicalFitnessLevel":
                    return true;
                case "numbers":
                    return true;
                case "hours":
                    return true;
                case "tutorOrgUnit":
                    return true;
                case "trainer":
                    return true;
                case "place":
                    return true;
                case "firstClassDay":
                    return true;
                case "firstClassTime":
                    return true;
                case "secondClassDay":
                    return true;
                case "secondClassTime":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationYear":
                    return EducationYear.class;
                case "title":
                    return String.class;
                case "type":
                    return SportSectionType.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "physicalFitnessLevel":
                    return PhysicalFitnessLevel.class;
                case "numbers":
                    return Integer.class;
                case "hours":
                    return Integer.class;
                case "tutorOrgUnit":
                    return OrgUnit.class;
                case "trainer":
                    return PpsEntry.class;
                case "place":
                    return UniplacesPlace.class;
                case "firstClassDay":
                    return Integer.class;
                case "firstClassTime":
                    return Date.class;
                case "secondClassDay":
                    return Integer.class;
                case "secondClassTime":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SportSection> _dslPath = new Path<SportSection>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SportSection");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Наименование секции. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Тип секции. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getType()
     */
    public static SportSectionType.Path<SportSectionType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unitutor.entity.SportSection#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Уровень подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getPhysicalFitnessLevel()
     */
    public static PhysicalFitnessLevel.Path<PhysicalFitnessLevel> physicalFitnessLevel()
    {
        return _dslPath.physicalFitnessLevel();
    }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getNumbers()
     */
    public static PropertyPath<Integer> numbers()
    {
        return _dslPath.numbers();
    }

    /**
     * @return Количество часов. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getHours()
     */
    public static PropertyPath<Integer> hours()
    {
        return _dslPath.hours();
    }

    /**
     * @return Кафедра. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getTutorOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> tutorOrgUnit()
    {
        return _dslPath.tutorOrgUnit();
    }

    /**
     * @return Тренер.
     * @see ru.tandemservice.unitutor.entity.SportSection#getTrainer()
     */
    public static PpsEntry.Path<PpsEntry> trainer()
    {
        return _dslPath.trainer();
    }

    /**
     * @return Спортивный объект. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getPlace()
     */
    public static UniplacesPlace.Path<UniplacesPlace> place()
    {
        return _dslPath.place();
    }

    /**
     * @return Первое занятие в неделю. День недели.
     * @see ru.tandemservice.unitutor.entity.SportSection#getFirstClassDay()
     */
    public static PropertyPath<Integer> firstClassDay()
    {
        return _dslPath.firstClassDay();
    }

    /**
     * @return Первое занятие в неделю. Время дня.
     * @see ru.tandemservice.unitutor.entity.SportSection#getFirstClassTime()
     */
    public static PropertyPath<Date> firstClassTime()
    {
        return _dslPath.firstClassTime();
    }

    /**
     * @return Второе занятие в неделю. День недели.
     * @see ru.tandemservice.unitutor.entity.SportSection#getSecondClassDay()
     */
    public static PropertyPath<Integer> secondClassDay()
    {
        return _dslPath.secondClassDay();
    }

    /**
     * @return Второе занятие в неделю. Время дня.
     * @see ru.tandemservice.unitutor.entity.SportSection#getSecondClassTime()
     */
    public static PropertyPath<Date> secondClassTime()
    {
        return _dslPath.secondClassTime();
    }

    public static class Path<E extends SportSection> extends EntityPath<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private PropertyPath<String> _title;
        private SportSectionType.Path<SportSectionType> _type;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private PhysicalFitnessLevel.Path<PhysicalFitnessLevel> _physicalFitnessLevel;
        private PropertyPath<Integer> _numbers;
        private PropertyPath<Integer> _hours;
        private OrgUnit.Path<OrgUnit> _tutorOrgUnit;
        private PpsEntry.Path<PpsEntry> _trainer;
        private UniplacesPlace.Path<UniplacesPlace> _place;
        private PropertyPath<Integer> _firstClassDay;
        private PropertyPath<Date> _firstClassTime;
        private PropertyPath<Integer> _secondClassDay;
        private PropertyPath<Date> _secondClassTime;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Наименование секции. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(SportSectionGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Тип секции. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getType()
     */
        public SportSectionType.Path<SportSectionType> type()
        {
            if(_type == null )
                _type = new SportSectionType.Path<SportSectionType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unitutor.entity.SportSection#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Уровень подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getPhysicalFitnessLevel()
     */
        public PhysicalFitnessLevel.Path<PhysicalFitnessLevel> physicalFitnessLevel()
        {
            if(_physicalFitnessLevel == null )
                _physicalFitnessLevel = new PhysicalFitnessLevel.Path<PhysicalFitnessLevel>(L_PHYSICAL_FITNESS_LEVEL, this);
            return _physicalFitnessLevel;
        }

    /**
     * @return Количество мест. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getNumbers()
     */
        public PropertyPath<Integer> numbers()
        {
            if(_numbers == null )
                _numbers = new PropertyPath<Integer>(SportSectionGen.P_NUMBERS, this);
            return _numbers;
        }

    /**
     * @return Количество часов. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getHours()
     */
        public PropertyPath<Integer> hours()
        {
            if(_hours == null )
                _hours = new PropertyPath<Integer>(SportSectionGen.P_HOURS, this);
            return _hours;
        }

    /**
     * @return Кафедра. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getTutorOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> tutorOrgUnit()
        {
            if(_tutorOrgUnit == null )
                _tutorOrgUnit = new OrgUnit.Path<OrgUnit>(L_TUTOR_ORG_UNIT, this);
            return _tutorOrgUnit;
        }

    /**
     * @return Тренер.
     * @see ru.tandemservice.unitutor.entity.SportSection#getTrainer()
     */
        public PpsEntry.Path<PpsEntry> trainer()
        {
            if(_trainer == null )
                _trainer = new PpsEntry.Path<PpsEntry>(L_TRAINER, this);
            return _trainer;
        }

    /**
     * @return Спортивный объект. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.SportSection#getPlace()
     */
        public UniplacesPlace.Path<UniplacesPlace> place()
        {
            if(_place == null )
                _place = new UniplacesPlace.Path<UniplacesPlace>(L_PLACE, this);
            return _place;
        }

    /**
     * @return Первое занятие в неделю. День недели.
     * @see ru.tandemservice.unitutor.entity.SportSection#getFirstClassDay()
     */
        public PropertyPath<Integer> firstClassDay()
        {
            if(_firstClassDay == null )
                _firstClassDay = new PropertyPath<Integer>(SportSectionGen.P_FIRST_CLASS_DAY, this);
            return _firstClassDay;
        }

    /**
     * @return Первое занятие в неделю. Время дня.
     * @see ru.tandemservice.unitutor.entity.SportSection#getFirstClassTime()
     */
        public PropertyPath<Date> firstClassTime()
        {
            if(_firstClassTime == null )
                _firstClassTime = new PropertyPath<Date>(SportSectionGen.P_FIRST_CLASS_TIME, this);
            return _firstClassTime;
        }

    /**
     * @return Второе занятие в неделю. День недели.
     * @see ru.tandemservice.unitutor.entity.SportSection#getSecondClassDay()
     */
        public PropertyPath<Integer> secondClassDay()
        {
            if(_secondClassDay == null )
                _secondClassDay = new PropertyPath<Integer>(SportSectionGen.P_SECOND_CLASS_DAY, this);
            return _secondClassDay;
        }

    /**
     * @return Второе занятие в неделю. Время дня.
     * @see ru.tandemservice.unitutor.entity.SportSection#getSecondClassTime()
     */
        public PropertyPath<Date> secondClassTime()
        {
            if(_secondClassTime == null )
                _secondClassTime = new PropertyPath<Date>(SportSectionGen.P_SECOND_CLASS_TIME, this);
            return _secondClassTime;
        }

        public Class getEntityClass()
        {
            return SportSection.class;
        }

        public String getEntityName()
        {
            return "sportSection";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
