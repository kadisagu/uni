package ru.tandemservice.unitutor.component.awp.StudentOrientationList;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.Student2Orientation;

import java.util.List;

@Input({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "orgUnitHolder.id")})
@State({@org.tandemframework.core.component.Bind(key = "selectedTab", binding = "selectedTab")})
public class Model {

    public final static String ORIENTATION_FILTER = "orientationFilter";
    public final static String COURSE_FILTER = "courseFilter";
    public final static String ACTUAL_FILTER = "actualFilter";
    public final static String DEV_FORM_FILTER = "devFormFilter";
    public final static String EDU_LEVEL_FILTER = "eduLevelFilter";
    public final static String GROUP_FILTER = "groupFilter";
    public final static String STUDENT_FILTER = "studentFilter";
    public final static String EDU_YEAR_FILTER = "eduYearFilter";

    public final static Long YES = 1L;
    public final static Long NO = 2L;

    public final static List<IdentifiableWrapper> YES_NO_ITEMS = ImmutableList.of(
            new IdentifiableWrapper(YES, "Да"),
            new IdentifiableWrapper(NO, "Нет")
    );

    private IDataSettings settings;
    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    private String selectedTab;
    private DynamicListDataSource<Student2Orientation> dataSource;

    private ISelectModel courseModel;
    private ISelectModel developFormModel;
    private ISelectModel eduLevelModel;
    private ISelectModel groupModel;
    private ISelectModel orientationModel;
    private ISelectModel studentModel;
    private ISelectModel actualModel;
    private ISelectModel eduYearModel;

    public EducationYear getEducationYear() {
        return getSettings().get(EDU_YEAR_FILTER);
    }

    public List<Course> getCourses() {
        return getSettings().get(COURSE_FILTER);
    }

    public List<DevelopForm> getDevelopForms() {
        return getSettings().get(DEV_FORM_FILTER);
    }

    public List<EducationLevelsHighSchool> getEduLevels() {
        return getSettings().get(EDU_LEVEL_FILTER);
    }

    public List<Group> getGroups() {
        return getSettings().get(GROUP_FILTER);
    }

    public List<EducationLevelsHighSchool> getOrientationsEHS() {
        return getSettings().get(ORIENTATION_FILTER);
    }

    public List<Student> getStudents() {
        return getSettings().get(STUDENT_FILTER);
    }

    public Boolean isActual() {
        if (getSettings().get(ACTUAL_FILTER) == null)
            return null;
        else
            return ((IdentifiableWrapper) getSettings().get(ACTUAL_FILTER)).getId().equals(YES);
    }

    public Object getSecuredObject()
    {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public OrgUnit getOrgUnit()
    {
        return (OrgUnit) getOrgUnitHolder().getValue();
    }

    public Long getOrgUnitId()
    {
        return getOrgUnitHolder().getId();
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOrgUnitHolder().getSecModel();
    }

    public ISelectModel getEduYearModel() {
        return eduYearModel;
    }

    public void setEduYearModel(ISelectModel eduYearModel) {
        this.eduYearModel = eduYearModel;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public OrgUnitHolder getOrgUnitHolder() {
        return orgUnitHolder;
    }

    public DynamicListDataSource<Student2Orientation> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<Student2Orientation> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel) {
        this.courseModel = courseModel;
    }

    public ISelectModel getDevelopFormModel() {
        return developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel) {
        this.developFormModel = developFormModel;
    }

    public ISelectModel getEduLevelModel() {
        return eduLevelModel;
    }

    public void setEduLevelModel(ISelectModel eduLevelModel) {
        this.eduLevelModel = eduLevelModel;
    }

    public ISelectModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel) {
        this.groupModel = groupModel;
    }

    public ISelectModel getOrientationModel() {
        return orientationModel;
    }

    public void setOrientationModel(ISelectModel orientationModel) {
        this.orientationModel = orientationModel;
    }

    public ISelectModel getStudentModel() {
        return studentModel;
    }

    public void setStudentModel(ISelectModel studentModel) {
        this.studentModel = studentModel;
    }

    public ISelectModel getActualModel() {
        return actualModel;
    }

    public void setActualModel(ISelectModel actualModel) {
        this.actualModel = actualModel;
    }

}

