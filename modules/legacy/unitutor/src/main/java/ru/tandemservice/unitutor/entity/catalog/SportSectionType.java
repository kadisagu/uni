package ru.tandemservice.unitutor.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unitutor.entity.catalog.gen.*;

/** @see ru.tandemservice.unitutor.entity.catalog.gen.SportSectionTypeGen */
public class SportSectionType extends SportSectionTypeGen
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, SportSectionType.class)
                .filter(SportSectionType.title())
                .order(SportSectionType.title());
    }
}