package ru.tandemservice.unitutor.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Статус заявки на спортивную секцию"
 * Имя сущности : applicationStatus4SportsSection
 * Файл data.xml : unitutor.data.xml
 */
public interface ApplicationStatus4SportsSectionCodes
{
    /** Константа кода (code) элемента : На согласовании (title) */
    String ACCEPTABLE = "tutor.appSt.acle";
    /** Константа кода (code) элемента : Согласована (title) */
    String ACCEPTED = "tutor.appSt.aced";
    /** Константа кода (code) элемента : Отклонена (title) */
    String REJECTED = "tutor.appSt.rej";
    /** Константа кода (code) элемента : Отозвана (title) */
    String WITHDRAW = "tutor.appSt.wdr";

    Set<String> CODES = ImmutableSet.of(ACCEPTABLE, ACCEPTED, REJECTED, WITHDRAW);
}
