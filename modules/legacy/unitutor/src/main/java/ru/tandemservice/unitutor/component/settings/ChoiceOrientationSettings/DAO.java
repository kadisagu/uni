package ru.tandemservice.unitutor.component.settings.ChoiceOrientationSettings;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.ChoiceOrientationSettings;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setEduYearModel(new LazySimpleSelectModel<>(EducationYear.class, EducationYear.title().s()));
    }

    @Override
    public void update(Model model) {
        getSession().saveOrUpdate(model.getSettings());

        if (model.isActualize()) {
            DaoStudentSubscribe.instanse().actualizeOrientations(null, model.getEducationYear());
        }
    }

    @Override
    public void onChangeEduYear(Model model) {
        if (model.getEducationYear() != null) {
            ChoiceOrientationSettings settings = get(ChoiceOrientationSettings.class, ChoiceOrientationSettings.educationYear(), model.getEducationYear());
            if (settings == null) {
                settings = new ChoiceOrientationSettings();
                settings.setEducationYear(model.getEducationYear());
            }
            model.setSettings(settings);
        }
    }
}
