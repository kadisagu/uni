package ru.tandemservice.unitutor.utils;

import com.google.common.collect.Lists;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.*;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;
import ru.tandemservice.unitutor.entity.catalog.codes.EppRegistryStructureCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vch
 */
public class DqlDisciplineBuilder {

    public static final String STUDENT_WORK_PLAN_ALIAS = "st2wp";
    public static final String WORK_PLAN_REGISTRY_ROW_ALIAS = "wprr";
    public static final String STUDENT_ALIAS = "s";
    public static final String EPP_EPV_REG_ROW_IN = "eerr";
    public static final String EPP_EPV_GROUP_ROW_IN = "eegr";
    public static final String WORK_PLAN_BASE_ALIAS = "wpb";
    public static final String STUD_EDU_PLAN_VERSION = "s2epv";



    /**
     * на выходе id студентов, для которых актуально что-то выбирать
     */
    public static DQLSelectBuilder getDqlInStudent(EducationYear educationYear, YearDistributionPart yearDistributionPart, List<EducationLevelsHighSchool> lstEducationLevelsHighSchool,
                                   List<Group> lstGroup, List<Employee> lstEmployee, List<OrgUnit> orgUnitLst, List<OrgUnit> cathedraOrgUnitLst)
    {
        DQLSelectBuilder dqlIn = getDqlInEppStudent2WorkPlan(educationYear, yearDistributionPart, lstEducationLevelsHighSchool, lstGroup,
                                                             lstEmployee, orgUnitLst, null);
        dqlIn.joinEntity(WORK_PLAN_BASE_ALIAS, DQLJoinType.left, EppWorkPlanVersion.class, "v",
                                     eq(property(WORK_PLAN_BASE_ALIAS, EppWorkPlanBase.id()), property("v", EppWorkPlanVersion.id())))
                .joinEntity(WORK_PLAN_BASE_ALIAS, DQLJoinType.left, EppWorkPlan.class, "wp", eq(property(WORK_PLAN_BASE_ALIAS, EppWorkPlanBase.id()), property("wp", EppWorkPlan.id())))
                .joinEntity("v", DQLJoinType.left, EppWorkPlan.class, "wpp", eq(property("v", EppWorkPlanVersion.parent().id()), property("wpp", EppWorkPlan.id())));

        // в вывод рабочий план студента (не версия)
        dqlIn.column(DQLFunctions.coalesce(property("wp", EppWorkPlan.id()), property("wpp", EppWorkPlan.id())), "workPlanId");

        // появляется студент
        dqlIn.column(property("s", Student.id()), "studentId");


        /**
         * Колин вариант
         */
        // Собираем УПв студентов
//        DQLSelectBuilder studentBuilder = getStudentsIds(lstEducationLevelsHighSchool, lstGroup, lstEmployee, orgUnitLst);
//        List<Long> studentIds = IUniBaseDao.instance.get().getList(studentBuilder);
//
//        final Set<Long> optOrSelectedRegElements = getOptionalAndSelectedDisciplinesIds(studentIds);
//
//        DQLSelectBuilder dql = getDqlStudent2WorkPlanBuilder(educationYear, yearDistributionPart, cathedraOrgUnitLst, studentIds, optOrSelectedRegElements);
//        dql.column(property(STUDENT_ALIAS, "id"), "studentId");
//        dql.predicate(DQLPredicateType.distinct);

         //отсечем выборку по ДПВ
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, WORK_PLAN_REGISTRY_ROW_ALIAS)
                        // не исключенные строки нагрузки
                .where(isNull(property(EppWorkPlanRegistryElementRow.needRetake().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS))))
                        // только выбираемые дисциплины
                .where(in(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS)),
                          getInEppRegistryElement(true)
                                .column(property(EPP_EPV_REG_ROW_IN, EppEpvRegistryRow.registryElement().id()))
                                .buildQuery()));

                  // по рабочим планам (джойним целиком сущность)
           dql.joinDataSource(WORK_PLAN_REGISTRY_ROW_ALIAS,
                              DQLJoinType.left,
                              dqlIn.buildQuery(),
                              "dqlWPPart",
                              eq(
                                      property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS)),
                                      property("dqlWPPart.workPlanId")));

        // колонка с id студента
        dql.column(property("dqlWPPart.studentId"), "studentId");
        dql.predicate(DQLPredicateType.distinct);
        return dql;
    }

    private static DQLSelectBuilder getStudentsIds(List<EducationLevelsHighSchool> lstEducationLevelsHighSchool, List<Group> lstGroup, List<Employee> lstEmployee, List<OrgUnit> orgUnitLst)
    {
        DQLSelectBuilder studentIdsDQL = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property(STUDENT_ALIAS + ".id"));

        FilterUtils.applySelectFilter(studentIdsDQL, STUDENT_ALIAS, Student.group(), lstGroup);
        FilterUtils.applySelectFilter(studentIdsDQL, STUDENT_ALIAS, Student.group().curator().employee(), lstEmployee);
        if (orgUnitLst != null && !orgUnitLst.isEmpty())
        {
            studentIdsDQL.where(or(
                    in(property(STUDENT_ALIAS, Student.educationOrgUnit().formativeOrgUnit()), orgUnitLst),
                    in(property(STUDENT_ALIAS, Student.educationOrgUnit().territorialOrgUnit()), orgUnitLst),
                    in(property(STUDENT_ALIAS, Student.educationOrgUnit().groupOrgUnit()), orgUnitLst),
                    in(property(STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool().orgUnit()), orgUnitLst)
            ));
        }
        FilterUtils.applySelectFilter(studentIdsDQL, STUDENT_ALIAS, Student.educationOrgUnit().educationLevelHighSchool(), lstEducationLevelsHighSchool);
        return studentIdsDQL;
    }

    private static DQLSelectBuilder getDqlStudent2WorkPlanBuilder(EducationYear educationYear, YearDistributionPart yearDistributionPart, List<OrgUnit> cathedraOrgUnitLst, List<Long> studentIds, Set<Long> optOrSelectedRegElements)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, STUDENT_WORK_PLAN_ALIAS)
                .joinPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().student().fromAlias(STUDENT_WORK_PLAN_ALIAS), STUDENT_ALIAS)
                .joinPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias(STUDENT_WORK_PLAN_ALIAS), WORK_PLAN_BASE_ALIAS)
                .joinEntity(WORK_PLAN_BASE_ALIAS, DQLJoinType.left, EppWorkPlanVersion.class, "v", eq(property(WORK_PLAN_BASE_ALIAS), property("v")))
                .joinEntity(WORK_PLAN_BASE_ALIAS, DQLJoinType.left, EppWorkPlan.class, "wp", eq(property(WORK_PLAN_BASE_ALIAS), property("wp")))
                .joinEntity("v", DQLJoinType.left, EppWorkPlan.class, "wpp", eq(property("v", EppWorkPlanVersion.parent()), property("wpp")))
                .joinEntity("wpp", DQLJoinType.left, EppYearEducationProcess.class, "yearEpp", eq(property("yearEpp"), DQLFunctions.coalesce(property("wp", EppWorkPlan.year()), property("wpp", EppWorkPlan.year()))))
                .joinEntity("wpp", DQLJoinType.left, DevelopGridTerm.class, "gridTerm", eq(property("gridTerm"), DQLFunctions.coalesce(property("wp", EppWorkPlan.cachedGridTerm()), property("wpp", EppWorkPlan.cachedGridTerm()))))
                .joinEntity(WORK_PLAN_BASE_ALIAS, DQLJoinType.inner, EppWorkPlanRegistryElementRow.class, "wpr", eq(property("wp"), property("wpr", EppWorkPlanRegistryElementRow.workPlan())))
                .where(in(property("wpr", EppWorkPlanRegistryElementRow.registryElementPart().registryElement()), optOrSelectedRegElements));

        dql.where(in(property(STUDENT_ALIAS), studentIds));
        dql.where(eq(property("yearEpp", EppYearEducationProcess.educationYear()), value(educationYear)));
        dql.where(isNull(property(STUDENT_WORK_PLAN_ALIAS, EppStudent2WorkPlan.removalDate())));
        dql.where(isNull(property(STUDENT_WORK_PLAN_ALIAS, EppStudent2WorkPlan.studentEduPlanVersion().removalDate())));
        FilterUtils.applySelectFilter(dql, "gridTerm", DevelopGridTerm.part(), yearDistributionPart);
        FilterUtils.applySelectFilter(dql, "wpr", EppWorkPlanRegistryElementRow.registryElementPart().registryElement().owner(), cathedraOrgUnitLst);
        return dql;
    }


    /**
     * Дисциплины по выбору и факультативы для выбранных студентов
     * @param studentIds
     * @return
     */
    private static Set<Long> getOptionalAndSelectedDisciplinesIds(List<Long> studentIds)
    {
        Map<Long, EppStudent2EduPlanVersion> studentEpvs = IEppEduPlanDAO.instance.get().getActiveStudentEduplanVersionRelationMap(studentIds);
        final Set<Long> epvBlockIds = new HashSet<>();
        for (EppStudent2EduPlanVersion rel : studentEpvs.values()) {
           if (rel.getBlock() != null)
               epvBlockIds.add(rel.getBlock().getId());
        }
        final Map<Long, IEppEpvBlockWrapper> blockWrappers = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockDataMap(epvBlockIds, false);
        final Set<Long> optOrSelectedRegElements = new HashSet<>();
        for (Map.Entry<Long, IEppEpvBlockWrapper> entry : blockWrappers.entrySet()) {
            for (IEppEpvRowWrapper rowWrapper : entry.getValue().getRowMap().values()) {
                if ((rowWrapper.isOptionalItem() || rowWrapper.isSelectedItem()) && rowWrapper.getRow() instanceof EppEpvRegistryRow) {
                    EppRegistryElement registryElement = ((EppEpvRegistryRow) rowWrapper.getRow()).getRegistryElement();
                    if (registryElement != null) {
                        optOrSelectedRegElements.add(registryElement.getId());
                    }
                }
            }
        }
        return optOrSelectedRegElements;
    }


    public static DQLSelectBuilder getDqlInYearDistributionPart(EducationYear educationYear)
    {
        DQLSelectBuilder dqlIn = getDqlInEppStudent2WorkPlan(educationYear, null, null, null, null, null, null);
        dqlIn.column(EppStudent2WorkPlan.cachedGridTerm().part().id().fromAlias(STUDENT_WORK_PLAN_ALIAS).s(), "partId")
                .distinct();
        return dqlIn;

    }


    /**
     * Для организации проверки на тему, у нас созданы все части!!!!
     *
     * @param educationYear
     *
     * @return
     */
    public static DQLSelectBuilder getDqlRegistryElementPartCheck(EducationYear educationYear)
    {
        DQLSelectBuilder dqlWPPart = getDqlInEppStudent2WorkPlan(educationYear, null, null, null, null, null, null);

        // в вывод базовая запись рабочего плана
        dqlWPPart.column(EppStudent2WorkPlan.workPlan().id().fromAlias(STUDENT_WORK_PLAN_ALIAS).s(), "workPlanId");
        // id года
        dqlWPPart.column(EppStudent2WorkPlan.cachedEppYear().educationYear().id().fromAlias(STUDENT_WORK_PLAN_ALIAS).s(), "educationYearId");
        // id части года
        dqlWPPart.column(EppStudent2WorkPlan.cachedGridTerm().part().id().fromAlias(STUDENT_WORK_PLAN_ALIAS).s(), "partId");


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, WORK_PLAN_REGISTRY_ROW_ALIAS)
                        // не исключенные строки нагрузки
                .where(isNull(property(EppWorkPlanRegistryElementRow.needRetake().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS))))
                        // только ДПВ
                .where(in(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS))
                        , getInEppRegistryElement(false)
                                .column(property(EPP_EPV_REG_ROW_IN, EppEpvRegistryRow.registryElement().id()))
                                .buildQuery()))

                        // по рабочим планам (джойним целиком сущность)
                .joinDataSource(WORK_PLAN_REGISTRY_ROW_ALIAS,
                                DQLJoinType.inner,
                                dqlWPPart.buildQuery(),
                                "dqlWPPart",
                                eq(
                                        property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS)),
                                        property("dqlWPPart" + ".workPlanId")));

        dql.column(property(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS).s()), "registryElementPartId");
        dql.column("dqlWPPart.educationYearId", "educationYearId");
        dql.column("dqlWPPart.partId", "partId");
        dql.predicate(DQLPredicateType.distinct);

        return dql;

    }


    /**
     * РУПы студентов
     * Применять для формирования подзапроса
     * @return
     */
    public static DQLSelectBuilder getDqlInEppStudent2WorkPlan(EducationYear educationYear, YearDistributionPart yearDistributionPart,
                    List<EducationLevelsHighSchool> lstEducationLevelsHighSchool, List<Group> lstGroup, List<Employee> lstEmployee,
                    List<OrgUnit> orgUnitLst, List<Student> lstStudent)
    {
        //Рабочий учебный план студента
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, STUDENT_WORK_PLAN_ALIAS)
                .joinPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias(STUDENT_WORK_PLAN_ALIAS), WORK_PLAN_BASE_ALIAS)
                .joinPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias(STUDENT_WORK_PLAN_ALIAS), STUD_EDU_PLAN_VERSION)
                .joinPath(DQLJoinType.inner, EppStudent2EduPlanVersion.student().fromAlias(STUD_EDU_PLAN_VERSION), "s");


        // Фильтруем по учебному году
        if (educationYear != null)
            FilterUtils.applySelectFilter(dql, EppStudent2WorkPlan.cachedEppYear().educationYear().fromAlias(STUDENT_WORK_PLAN_ALIAS), educationYear);

        if (yearDistributionPart != null)
            // Фильтруем по части года (значения фильтра для части учебного года взяты из соответсвующего справочника)
            FilterUtils.applySelectFilter(dql, EppStudent2WorkPlan.cachedGridTerm().part().fromAlias(STUDENT_WORK_PLAN_ALIAS), yearDistributionPart);

        // актуальный выбор
        dql.where(isNull(property(EppStudent2WorkPlan.removalDate().fromAlias(STUDENT_WORK_PLAN_ALIAS))));
        dql.where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias(STUD_EDU_PLAN_VERSION))));

        // по статусам (согласовано, формируется, на согласовании)
        List<String> lstState = Lists.newArrayList(EppState.STATE_ACCEPTED, EppState.STATE_FORMATIVE, EppState.STATE_ACCEPTABLE);
        dql.where(in(property(WORK_PLAN_BASE_ALIAS, EppWorkPlanBase.state().code()), lstState));

        // по направлениям подготовки
		if (lstEducationLevelsHighSchool!=null && !lstEducationLevelsHighSchool.isEmpty())
			FilterUtils.applySelectFilter(dql, Student.educationOrgUnit().educationLevelHighSchool().fromAlias("s"), lstEducationLevelsHighSchool);

        // по группам студентов
        if (lstGroup != null && !lstGroup.isEmpty())
            FilterUtils.applySelectFilter(dql, Student.group().fromAlias("s"), lstGroup);

        // фильтр по студентам
        if (lstStudent != null && !lstStudent.isEmpty())
            FilterUtils.applySelectFilter(dql, Student.id().fromAlias("s"), lstStudent);

        // по кураторам
        if (lstEmployee != null && !lstEmployee.isEmpty())
            FilterUtils.applySelectFilter(dql, Student.group().curator().employee().fromAlias("s"), lstEmployee);


        // применяем фильтр по подразделению
        // следует разделять ситуацию с закладкой преподавателя (мы в орг юните или в преподавателе)
        dql = applyOrgUnitFilter(dql, orgUnitLst);

        return dql;
    }


//    public static DQLSelectBuilder getDqlInEppStudent2WorkPlan(EducationYear educationYear, YearDistributionPart yearDistributionPart,
//                                                               List<EducationLevelsHighSchool> lstEducationLevelsHighSchool, List<Group> lstGroup, List<Employee> lstEmployee,
//                                                               List<OrgUnit> orgUnitLst, List<Student> lstStudent)
//    {
//        DQLSelectBuilder studentBuilder = getStudentsIds(lstEducationLevelsHighSchool, lstGroup, lstEmployee, orgUnitLst);
//        if (lstStudent != null && !lstStudent.isEmpty())
//            studentBuilder.where(in(property(STUDENT_ALIAS, Student.id()), lstStudent));
//        List<Long> studentsId = IUniBaseDao.instance.get().getList(studentBuilder);
//        Set<Long> optDiscList = getOptionalAndSelectedDisciplinesIds(studentsId);
//        DQLSelectBuilder dql = getDqlStudent2WorkPlanBuilder(educationYear, yearDistributionPart, null, studentsId, optDiscList);
//        return dql;
//    }


    /**
     * Строки РУПов студентов для ДПВ
     */
    public static DQLSelectBuilder DqlInWorkPlanRegistryElementRow(EducationYear educationYear, YearDistributionPart yearDistributionPart,
                                                                   List<EducationLevelsHighSchool> lstEducationLevelsHighSchool, List<Group> lstGroup, List<EppRegistryElement> lstEppRegistryElement, List<Employee> lstEmployee,
                                                                   List<OrgUnit> orgUnitLst, List<OrgUnit> cathedraOrgUnitLst, List<Student> lstStudent, List<EppEpvGroupImRow> groupDisciplineList)
    {
        //Рабочие учебные планы студентов
        DQLSelectBuilder dqlInWorkPlanBase = getDqlInEppStudent2WorkPlan(educationYear, yearDistributionPart, lstEducationLevelsHighSchool, lstGroup,
                                                                         lstEmployee, orgUnitLst, lstStudent);

        dqlInWorkPlanBase.joinEntity(WORK_PLAN_BASE_ALIAS, DQLJoinType.left, EppWorkPlanVersion.class, "v",
                                     eq(property(WORK_PLAN_BASE_ALIAS, EppWorkPlanBase.id()), property("v", EppWorkPlanVersion.id())))
                .joinEntity(WORK_PLAN_BASE_ALIAS, DQLJoinType.left, EppWorkPlan.class, "wp", eq(property(WORK_PLAN_BASE_ALIAS, EppWorkPlanBase.id()), property("wp", EppWorkPlan.id())))
                .joinEntity("v", DQLJoinType.left, EppWorkPlan.class, "wpp", eq(property("v", EppWorkPlanVersion.parent().id()), property("wpp", EppWorkPlan.id())));

        // в вывод рабочий план студента (не версия)
        dqlInWorkPlanBase.column(DQLFunctions.coalesce(property("wp", EppWorkPlan.id()), property("wpp", EppWorkPlan.id())))
                        .distinct();
        List<Long> workPlanBaseId = DataAccessServices.dao().getList(dqlInWorkPlanBase);

        // версии блоков уп
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, WORK_PLAN_REGISTRY_ROW_ALIAS)

                        // не исключенные строки нагрузки
                .where(isNull(property(EppWorkPlanRegistryElementRow.needRetake().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS))));

        // по рабочим планам
        //TODO FIX ME
        // Так делать нельзя, но иначе очень медленно работает
        if (workPlanBaseId.size() < 2000)
            dql.where(in(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS)), workPlanBaseId));
        else
            dql.where(in(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS)), dqlInWorkPlanBase.buildQuery()));

        // только ДПВ
        dql.where(in(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS)),
                     getInEppRegistryElement(false)
                             .column(property(EPP_EPV_REG_ROW_IN, EppEpvRegistryRow.registryElement().id()))
                             .buildQuery()));

        // по дисциплинам
        if (lstEppRegistryElement != null && !lstEppRegistryElement.isEmpty())
            FilterUtils.applySelectFilter(dql, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias(WORK_PLAN_REGISTRY_ROW_ALIAS), lstEppRegistryElement);

        return dql;
    }

    /**
     * дисциплины по выбору, УДВ, УФ
     * @param isAll true - ДПВ, УДВ, УФ;  false - только ДПВ
     */
    public static DQLSelectBuilder getInEppRegistryElement(boolean isAll)
    {
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, EPP_EPV_REG_ROW_IN)
                        // только то, что касается дисциплин по выбору
                .joinEntity(EPP_EPV_REG_ROW_IN, isAll ? DQLJoinType.left : DQLJoinType.inner, EppEpvGroupImRow.class, EPP_EPV_GROUP_ROW_IN,
                            eq(property(EppEpvRegistryRow.parent().id().fromAlias(EPP_EPV_REG_ROW_IN)), property(EppEpvGroupImRow.id().fromAlias(EPP_EPV_GROUP_ROW_IN))))
                        // дисциплины должны быть обязательно
                .where(isNotNull(property(EppEpvRegistryRow.registryElement().fromAlias(EPP_EPV_REG_ROW_IN))));

        //УДВ и УФ
        if (isAll)
            dqlIn.where(or(isNotNull(property(EPP_EPV_GROUP_ROW_IN)),
                           in(property(EppEpvRegistryRow.registryElementType().code().fromAlias(EPP_EPV_REG_ROW_IN)), Arrays.asList(EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_KURS_PO_VYBORU, EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_FAKULTATIV))
            ));
        else
            //У клиентов в УП встречаются строки УП (элемент реестра) , у которых один элемент реестра, но разные parent
            // для этого случая отсекаем элементы реестра, имеющие тип УДВ и УФ
            dqlIn.where(notIn(property(EPP_EPV_REG_ROW_IN, EppEpvRegistryRow.registryElement().parent().code()),
                              Arrays.asList(EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_KURS_PO_VYBORU, EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_FAKULTATIV)));

        return dqlIn;
    }


    public static DQLSelectBuilder applyOrgUnitFilter(DQLSelectBuilder dql, List<OrgUnit> orgUnitLst)
    {
        if (orgUnitLst != null && !orgUnitLst.isEmpty()) {
            DQLSelectBuilder dqlIn = getInOrgUnitFilter(orgUnitLst, "EducationOrgUnitIN");
            dqlIn.column(EducationOrgUnit.educationLevelHighSchool().id().fromAlias("EducationOrgUnitIN").s());
            dql.where(in(property(EppStudent2WorkPlan.studentEduPlanVersion().student().educationOrgUnit().educationLevelHighSchool().id().fromAlias(STUDENT_WORK_PLAN_ALIAS)), dqlIn.buildQuery()));
        }

        return dql;
    }

    /**
     * alias = EducationOrgUnitIN
     * from EducationOrgUnit
     *
     * @param orgUnitLst
     *
     * @return
     */
    public static DQLSelectBuilder getInOrgUnitFilter(List<OrgUnit> orgUnitLst, String alias)
    {
        if (orgUnitLst != null && !orgUnitLst.isEmpty()) {
            DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                    .fromEntity(EducationOrgUnit.class, alias)
                    .where(or(in(property(EducationOrgUnit.groupOrgUnit().fromAlias(alias)), orgUnitLst),
                              in(property(EducationOrgUnit.operationOrgUnit().fromAlias(alias)), orgUnitLst),
                              in(property(EducationOrgUnit.formativeOrgUnit().fromAlias(alias)), orgUnitLst),
                              in(property(EducationOrgUnit.territorialOrgUnit().fromAlias(alias)), orgUnitLst),
                              in(property(EducationOrgUnit.educationLevelHighSchool().orgUnit().fromAlias(alias)), orgUnitLst)
                    ));

            return dqlIn;
        }
        else
            return null;
    }

    /**
     * подзапрос для определения кол-ва записавшихся
     *
     * @return
     */
    public static DQLSelectBuilder getElectiveDisciplineSUMMSubDQL(EducationYear educationYear, String electiveDisciplineCode)
    {
        DQLSelectBuilder dataSourceSubDQL = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, "eds")

                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().type().code().fromAlias("eds")), electiveDisciplineCode))

                .where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias("eds"))));

        if (educationYear != null)
            dataSourceSubDQL.where(eq(property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().fromAlias("eds")), value(educationYear)));

        dataSourceSubDQL
                .column(ElectiveDisciplineSubscribe.electiveDiscipline().id().fromAlias("eds").s(), "id")
                .column(DQLFunctions.count(ElectiveDisciplineSubscribe.id().fromAlias("eds").s()), "subscribe")

                .group(ElectiveDisciplineSubscribe.electiveDiscipline().id().fromAlias("eds").s());

        return dataSourceSubDQL;
    }

    /**
     * каким группам принадлежит дисциплина
     * Используется только для перемещения записей
     */
    public static DQLSelectBuilder getDqlEppEpvGroupImRow(EducationYear educationYear, YearDistributionPart yearDistributionPart,
                                      List<Employee> lstEmployee, List<OrgUnit> orgUnitList, EppRegistryElement registryElement)
    {

        // версии учебных планов
        DQLSelectBuilder dqlInEduPlanVersion =DqlDisciplineBuilder.getDqlInEppStudent2WorkPlan(educationYear, yearDistributionPart, null, null, lstEmployee, orgUnitList, null);
        dqlInEduPlanVersion.column(property(DqlDisciplineBuilder.STUD_EDU_PLAN_VERSION, EppStudent2EduPlanVersion.eduPlanVersion().id()));

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, EPP_EPV_REG_ROW_IN)

                // только нужные версии уп
                .where(in(property(EppEpvRegistryRow.owner().eduPlanVersion().id().fromAlias(EPP_EPV_REG_ROW_IN)), dqlInEduPlanVersion.buildQuery()))

                // фильтр по дисциплине
                .where(eq(property(EppEpvRegistryRow.registryElement().id().fromAlias(EPP_EPV_REG_ROW_IN)), value(registryElement.getId())))

                // вывод
                .column(EppEpvRegistryRow.parent().id().fromAlias(EPP_EPV_REG_ROW_IN).s());

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppEpvGroupImRow.class, EPP_EPV_GROUP_ROW_IN)
                .where(in(property(EPP_EPV_GROUP_ROW_IN, EppEpvGroupImRow.id()), dqlIn.buildQuery()));

        return dql;
    }


    /**
     * Поиск дисциплин, для которых указан язык
     */
    public static DQLSelectBuilder getEppRegistryElementExt(List<Long> eppRegistryElementIds)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryElementExt.class, "disExt")
                .where(in(property("disExt", EppRegistryElementExt.eppRegistryElement().id()), eppRegistryElementIds))
                .where(isNotNull(property("disExt", EppRegistryElementExt.language())));
        return builder;
    }


}
