package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.Student2Orientation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись студента на направленность
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Student2OrientationGen extends EntityBase
 implements INaturalIdentifiable<Student2OrientationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.Student2Orientation";
    public static final String ENTITY_NAME = "student2Orientation";
    public static final int VERSION_HASH = -114413285;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORIENTATION = "orientation";
    public static final String L_STUDENT = "student";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String P_ACTUAL = "actual";
    public static final String P_DATE = "date";
    public static final String P_SOURCE = "source";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String P_SOURCE_CODE = "sourceCode";

    private EducationOrgUnit _orientation;     // Направленность
    private Student _student;     // Студент
    private EducationYear _educationYear;     // Учебный год
    private boolean _actual;     // Актуальность
    private Date _date;     // Дата записи
    private String _source;     // Источник записи
    private Date _removalDate;     // Дата утраты актуальности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направленность. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getOrientation()
    {
        return _orientation;
    }

    /**
     * @param orientation Направленность. Свойство не может быть null.
     */
    public void setOrientation(EducationOrgUnit orientation)
    {
        dirty(_orientation, orientation);
        _orientation = orientation;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Учебный год.
     */
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Актуальность. Свойство не может быть null.
     */
    @NotNull
    public boolean isActual()
    {
        return _actual;
    }

    /**
     * @param actual Актуальность. Свойство не может быть null.
     */
    public void setActual(boolean actual)
    {
        dirty(_actual, actual);
        _actual = actual;
    }

    /**
     * @return Дата записи. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата записи. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Источник записи. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSource()
    {
        return _source;
    }

    /**
     * @param source Источник записи. Свойство не может быть null.
     */
    public void setSource(String source)
    {
        dirty(_source, source);
        _source = source;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата утраты актуальности.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Student2OrientationGen)
        {
            if (withNaturalIdProperties)
            {
                setOrientation(((Student2Orientation)another).getOrientation());
                setStudent(((Student2Orientation)another).getStudent());
            }
            setEducationYear(((Student2Orientation)another).getEducationYear());
            setActual(((Student2Orientation)another).isActual());
            setDate(((Student2Orientation)another).getDate());
            setSource(((Student2Orientation)another).getSource());
            setRemovalDate(((Student2Orientation)another).getRemovalDate());
        }
    }

    public INaturalId<Student2OrientationGen> getNaturalId()
    {
        return new NaturalId(getOrientation(), getStudent());
    }

    public static class NaturalId extends NaturalIdBase<Student2OrientationGen>
    {
        private static final String PROXY_NAME = "Student2OrientationNaturalProxy";

        private Long _orientation;
        private Long _student;

        public NaturalId()
        {}

        public NaturalId(EducationOrgUnit orientation, Student student)
        {
            _orientation = ((IEntity) orientation).getId();
            _student = ((IEntity) student).getId();
        }

        public Long getOrientation()
        {
            return _orientation;
        }

        public void setOrientation(Long orientation)
        {
            _orientation = orientation;
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof Student2OrientationGen.NaturalId) ) return false;

            Student2OrientationGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrientation(), that.getOrientation()) ) return false;
            if( !equals(getStudent(), that.getStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrientation());
            result = hashCode(result, getStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrientation());
            sb.append("/");
            sb.append(getStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Student2OrientationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Student2Orientation.class;
        }

        public T newInstance()
        {
            return (T) new Student2Orientation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orientation":
                    return obj.getOrientation();
                case "student":
                    return obj.getStudent();
                case "educationYear":
                    return obj.getEducationYear();
                case "actual":
                    return obj.isActual();
                case "date":
                    return obj.getDate();
                case "source":
                    return obj.getSource();
                case "removalDate":
                    return obj.getRemovalDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orientation":
                    obj.setOrientation((EducationOrgUnit) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "actual":
                    obj.setActual((Boolean) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "source":
                    obj.setSource((String) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orientation":
                        return true;
                case "student":
                        return true;
                case "educationYear":
                        return true;
                case "actual":
                        return true;
                case "date":
                        return true;
                case "source":
                        return true;
                case "removalDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orientation":
                    return true;
                case "student":
                    return true;
                case "educationYear":
                    return true;
                case "actual":
                    return true;
                case "date":
                    return true;
                case "source":
                    return true;
                case "removalDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orientation":
                    return EducationOrgUnit.class;
                case "student":
                    return Student.class;
                case "educationYear":
                    return EducationYear.class;
                case "actual":
                    return Boolean.class;
                case "date":
                    return Date.class;
                case "source":
                    return String.class;
                case "removalDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Student2Orientation> _dslPath = new Path<Student2Orientation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Student2Orientation");
    }
            

    /**
     * @return Направленность. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getOrientation()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> orientation()
    {
        return _dslPath.orientation();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Актуальность. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#isActual()
     */
    public static PropertyPath<Boolean> actual()
    {
        return _dslPath.actual();
    }

    /**
     * @return Дата записи. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Источник записи. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getSource()
     */
    public static PropertyPath<String> source()
    {
        return _dslPath.source();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getSourceCode()
     */
    public static SupportedPropertyPath<String> sourceCode()
    {
        return _dslPath.sourceCode();
    }

    public static class Path<E extends Student2Orientation> extends EntityPath<E>
    {
        private EducationOrgUnit.Path<EducationOrgUnit> _orientation;
        private Student.Path<Student> _student;
        private EducationYear.Path<EducationYear> _educationYear;
        private PropertyPath<Boolean> _actual;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _source;
        private PropertyPath<Date> _removalDate;
        private SupportedPropertyPath<String> _sourceCode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направленность. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getOrientation()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> orientation()
        {
            if(_orientation == null )
                _orientation = new EducationOrgUnit.Path<EducationOrgUnit>(L_ORIENTATION, this);
            return _orientation;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Актуальность. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#isActual()
     */
        public PropertyPath<Boolean> actual()
        {
            if(_actual == null )
                _actual = new PropertyPath<Boolean>(Student2OrientationGen.P_ACTUAL, this);
            return _actual;
        }

    /**
     * @return Дата записи. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(Student2OrientationGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Источник записи. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getSource()
     */
        public PropertyPath<String> source()
        {
            if(_source == null )
                _source = new PropertyPath<String>(Student2OrientationGen.P_SOURCE, this);
            return _source;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(Student2OrientationGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unitutor.entity.Student2Orientation#getSourceCode()
     */
        public SupportedPropertyPath<String> sourceCode()
        {
            if(_sourceCode == null )
                _sourceCode = new SupportedPropertyPath<String>(Student2OrientationGen.P_SOURCE_CODE, this);
            return _sourceCode;
        }

        public Class getEntityClass()
        {
            return Student2Orientation.class;
        }

        public String getEntityName()
        {
            return "student2Orientation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getSourceCode();
}
