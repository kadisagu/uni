package ru.tandemservice.unitutor.component.awp.OrientationEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setRel(get(Orientation2EduPlanVersion.class, model.getEntityId()));
    }

    @Override
    public void update(Model model) {
        getSession().saveOrUpdate(model.getRel());
    }
}
