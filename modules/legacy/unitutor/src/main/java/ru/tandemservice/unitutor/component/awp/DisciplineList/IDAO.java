package ru.tandemservice.unitutor.component.awp.DisciplineList;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.component.awp.base.discipline.IDAOBase;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IDAO extends IDAOBase<Model>
{

    /**
     * Массово сохраняем количество мест на ДПВ
     */
    @Transactional
    void updateCountPlaces(Model model, List<Wrapper> wrapperList, String countPlaces);

    /**
     * Массово сохраняем "Дату записи до" для ДПВ
     */
    @Transactional
    void updateSubscribeToDate(Model model, List<Wrapper> wrapperList, Date subscribeToDate);

    /**
     * Фильтруем для массового заполнения
     * (если поле заполнено вручную, то массово сохранять не нужно)
     *
     * @param entityList
     * @param fieldName
     *
     * @return
     */
    List<Wrapper> filterByProprity(Collection<IEntity> entityList, String fieldName, boolean isForNullValue);

    /**
     * Проверить, что перечент ДПВ на учебный год соответствует действительности
     *
     * @param educationYear
     *
     * @return
     */
    boolean checkRegistryElementPart(EducationYear educationYear);

    @Transactional
     void fixElectiveDisciplineRow(EducationYear filterEducationYear);


}
