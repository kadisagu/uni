/*$Id$*/
package ru.tandemservice.unitutor.base.bo.SportSectRegistries.util;

import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * @author Andrey Andreev
 * @since 23.08.2016
 */
public class DayOfWeekDsHandler extends SimpleTitledComboDataSourceHandler
{
    public DayOfWeekDsHandler(String ownerId)
    {
        super(ownerId);
        filtered(true);
        addAll(getOptionList());
    }

    public static List<DataWrapper> getOptionList()
    {
        List<DataWrapper> wrapperList = new ArrayList<>(DayOfWeek.values().length);
        final Locale ru = new Locale("ru");
        for (DayOfWeek dayOfWeek : DayOfWeek.values())
            wrapperList.add(getWrappedDayOfWeek(dayOfWeek, ru));

        return Collections.unmodifiableList(wrapperList);
    }

    public static DataWrapper getWrappedRuDayOfWeek(int value)
    {
        return getWrappedDayOfWeek(DayOfWeek.of(value), new Locale("ru"));
    }

    public static DataWrapper getWrappedDayOfWeek(DayOfWeek dayOfWeek, Locale locale)
    {
        Long id = (long) dayOfWeek.getValue();
        String title = String.valueOf(dayOfWeek.getDisplayName(TextStyle.FULL, locale));
        return new DataWrapper(id, title, dayOfWeek);
    }


    public static ISelectModel getSelectModel()
    {
        return new LazySimpleSelectModel<>(getOptionList());
    }
}
