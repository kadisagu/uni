package ru.tandemservice.unitutor.component.eduplan.EduPlanVersionAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unitutor.entity.EppEduPlanVersionExt;

public class Model extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionAddEdit.Model {

    private ISelectModel courseListModel;
    private EppEduPlanVersionExt eduPlanVersionExt;

    public ISelectModel getCourseListModel() {
        return courseListModel;
    }

    public void setCourseListModel(ISelectModel courseListModel) {
        this.courseListModel = courseListModel;
    }

    public EppEduPlanVersionExt getEduPlanVersionExt() {
        return eduPlanVersionExt;
    }

    public void setEduPlanVersionExt(EppEduPlanVersionExt eduPlanVersionExt) {
        this.eduPlanVersionExt = eduPlanVersionExt;
    }
}
