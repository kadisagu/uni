package ru.tandemservice.unitutor.component.awp.StudentOrientationAdd;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.Student2Orientation;

@Input({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "orgUnitId"),
        @org.tandemframework.core.component.Bind(key = "studentId", binding = "studentId")
})
public class Model {

    private Long orgUnitId;
    private Long studentId;
    private Student2Orientation rel;

    private ISelectModel studentSelectModel;
    private Student student;
    private ISelectModel orientationSelectModel;
    private EducationOrgUnit orientation;
    private boolean sendMail = true;

    public String getEduYear() {
        EducationYear educationYear;
        if (getStudentId() == null) {
            //следующий учебный год
            educationYear = getEducationYear();
        }
        else {
            Student2Orientation rel = IUniBaseDao.instance.get().get(Student2Orientation.class, getStudentId());
            educationYear = rel.getEducationYear();
        }
        return "Учебный год: " + ((educationYear == null) ? "" : educationYear.getTitle());
    }

    public EducationYear getEducationYear() {
        final EducationYear current = EducationYearManager.instance().dao().getCurrent();
        return IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.intValue(), current.getIntValue() + 1);
    }

    public boolean isSendMail() {
        return sendMail;
    }

    public void setSendMail(boolean sendMail) {
        this.sendMail = sendMail;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Student2Orientation getRel() {
        return rel;
    }

    public void setRel(Student2Orientation rel) {
        this.rel = rel;
    }

    public ISelectModel getStudentSelectModel() {
        return studentSelectModel;
    }

    public void setStudentSelectModel(ISelectModel studentSelectModel) {
        this.studentSelectModel = studentSelectModel;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ISelectModel getOrientationSelectModel() {
        return orientationSelectModel;
    }

    public void setOrientationSelectModel(ISelectModel orientationSelectModel) {
        this.orientationSelectModel = orientationSelectModel;
    }

    public EducationOrgUnit getOrientation() {
        return orientation;
    }

    public void setOrientation(EducationOrgUnit orientation) {
        this.orientation = orientation;
    }

    public String getComboTitle() {
        return getStudentId() == null ? "Выбор профиля (специализации)" : "Изменить выбор";
    }

}
