package ru.tandemservice.unitutor.component.awp.OptionalDisciplineForm.PlacesEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import java.util.Collection;

@Input({
        @Bind(key="disciplines", binding = "disciplineIds"),
        @Bind(key = "isForNull", binding = "forNullOnly")
})
public class Model {

    private Collection<Long> _disciplineIds;
    private boolean _forNullOnly;
    private Long _countPlaces;

    public Long getCountPlaces() {
        return _countPlaces;
    }

    public void setCountPlaces(Long countPlaces) {
        this._countPlaces = countPlaces;
    }

    public Collection<Long> getDisciplineIds()
    {
        return _disciplineIds;
    }

    public void setDisciplineIds(Collection<Long> disciplineIds)
    {
        _disciplineIds = disciplineIds;
    }

    public boolean isForNullOnly()
    {
        return _forNullOnly;
    }

    public void setForNullOnly(boolean forNullOnly)
    {
        _forNullOnly = forNullOnly;
    }
}
