package ru.tandemservice.unitutor.component.settings.ChoiceOrientationSettings;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.ChoiceOrientationSettings;

public class Model {

    private ChoiceOrientationSettings settings = new ChoiceOrientationSettings();
    private ISelectModel eduYearModel;
    private EducationYear educationYear;
    private boolean actualize = false;

    public ChoiceOrientationSettings getSettings() {
        return settings;
    }

    public void setSettings(ChoiceOrientationSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getEduYearModel() {
        return eduYearModel;
    }

    public void setEduYearModel(ISelectModel eduYearModel) {
        this.eduYearModel = eduYearModel;
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public boolean isActualize() {
        return actualize;
    }

    public void setActualize(boolean actualize) {
        this.actualize = actualize;
    }

}
