package ru.tandemservice.unitutor.ws.types;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;
import ru.tandemservice.unitutor.utils.UniEppFormatter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.text.SimpleDateFormat;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "electiveWrap", namespace = "http://ws.unitutor.tandemservice.ru/")
public class WsElectiveWrap implements Serializable {

    private static final long serialVersionUID = -7616719337019967700L;

    private Long electiveDisciplineId;

    private String disciplineTitle;
    private String disciplineNumber;
    private String course;
    private String educationLevel;
    private String term;

    private String totalHours;
    private String zet;
    private String formOfControl;
    private String placesToSubscribe;
    private String freePlacesToSubscribe;
    private String subscribedToDate;
    private boolean subscribed;

    private String sourceCode;

    public WsElectiveWrap() {
    }

    public WsElectiveWrap(ElectiveDiscipline electiveDiscipline) {

        UniEppFormatter formatter = new UniEppFormatter();
        EppRegistryElementPart registryElementPart = electiveDiscipline.getRegistryElementPart();

        this.electiveDisciplineId = electiveDiscipline.getId();
        this.disciplineTitle = registryElementPart.getTitle();
        this.disciplineNumber = registryElementPart.getRegistryElement().getNumber();
        if (electiveDiscipline.getCourseStr() != null) {
            this.course = electiveDiscipline.getCourseStr();
        }
        if (electiveDiscipline.getGroupStr() != null) {
            this.educationLevel = electiveDiscipline.getGroupStr();
        }
        this.term = electiveDiscipline.getYearPart().getTitle();
        this.totalHours = formatter.format(registryElementPart.getSize());
        this.zet = formatter.format(registryElementPart.getLabor());
        // форма контроля
        this.formOfControl = OptionalDisciplineUtil.getFormsOfControlTitles(registryElementPart.getId());

        if (electiveDiscipline.getSubscribeToDate() != null) {
            this.subscribedToDate = new SimpleDateFormat("dd.MM.yyyy").format(electiveDiscipline.getSubscribeToDate());
        }

    }

    /**
     * наличие аннотации у дисциплины
     */
    private boolean annotationExist;

    public Long getElectiveDisciplineId() {
        return electiveDisciplineId;
    }

    public void setElectiveDisciplineId(Long electiveDisciplineId) {
        this.electiveDisciplineId = electiveDisciplineId;
    }

    public String getDisciplineTitle() {
        return disciplineTitle;
    }

    public void setDisciplineTitle(String disciplineTitle) {
        this.disciplineTitle = disciplineTitle;
    }

    public String getDisciplineNumber() {
        return disciplineNumber;
    }

    public void setDisciplineNumber(String disciplineNumber) {
        this.disciplineNumber = disciplineNumber;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getZet() {
        return zet;
    }

    public void setZet(String zet) {
        this.zet = zet;
    }

    public String getFormOfControl() {
        return formOfControl;
    }

    public void setFormOfControl(String formOfControl) {
        this.formOfControl = formOfControl;
    }

    public String getPlacesToSubscribe() {
        return placesToSubscribe;
    }

    public void setPlacesToSubscribe(String placesToSubscribe) {
        this.placesToSubscribe = placesToSubscribe;
    }

    public String getFreePlacesToSubscribe() {
        return freePlacesToSubscribe;
    }

    public void setFreePlacesToSubscribe(String freePlacesToSubscribe) {
        this.freePlacesToSubscribe = freePlacesToSubscribe;
    }

    public String getSubscribedToDate() {
        return subscribedToDate;
    }

    public void setSubscribedToDate(String subscribedToDate) {
        this.subscribedToDate = subscribedToDate;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public boolean isAnnotationExist() {
        return annotationExist;
    }

    public void setAnnotationExist(boolean annotationExist) {
        this.annotationExist = annotationExist;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }
}
