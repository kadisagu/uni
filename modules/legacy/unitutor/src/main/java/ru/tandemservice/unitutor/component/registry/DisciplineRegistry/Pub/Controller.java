package ru.tandemservice.unitutor.component.registry.DisciplineRegistry.Pub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.component.registry.DisciplineRegistry.Pub.Model;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

public class Controller extends ru.tandemservice.uniepp.component.registry.DisciplineRegistry.Pub.Controller {


    public void onClickEditAnnotation(IBusinessComponent component) {
        Model model = component.getModel();

        ParametersMap map = new ParametersMap();
        map.add(EppRegistryElement.P_ID, model.getElement().getId());

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(ru.tandemservice.unitutor.component.registry.DisciplineRegistry.AnnotationPubForm.Controller.class.getPackage().getName(), map));
    }


    public void onClickAddLanguage(IBusinessComponent component)
    {
        Model model = component.getModel();

        ParametersMap map = new ParametersMap();
        map.add(EppRegistryElement.P_ID, model.getElement().getId());

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(ru.tandemservice.unitutor.component.registry.DisciplineRegistry.LanguageAdd.Controller.class.getPackage().getName(), map));

    }


    public void onClickRemoveLanguage(IBusinessComponent component)
    {
        ru.tandemservice.unitutor.component.registry.DisciplineRegistry.Pub.Model model = component.getModel();
        model.getRegistryElementExt().setLanguage(null);
        model.getRegistryElementExt().setSecondLanguage(false);

        DataAccessServices.dao().update(model.getRegistryElementExt());
        ((BusinessComponent) component.getParentRegion().getOwner()).refresh();
    }

    @Override
    public void onClickEditElement(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.uniepp.component.registry.DisciplineRegistry.AddEdit",
                                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getElement().getId())));
    }
}
