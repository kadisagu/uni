/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.AddEdit.SportSectRegistriesAddEdit;

/**
 * @author Andrey Andreev
 * @since 22.08.2016
 */
public class SportSectRegistriesListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SportSectRegistriesList.EDUCATION_YEAR_LIST_PARAM, _uiSettings.get(SportSectRegistriesList.EDUCATION_YEAR_LIST_PARAM));
        dataSource.put(SportSectRegistriesList.SECTION_NAME_PARAM, _uiSettings.get(SportSectRegistriesList.SECTION_NAME_PARAM));
        dataSource.put(SportSectRegistriesList.SECTION_TYPE_PARAM, _uiSettings.get(SportSectRegistriesList.SECTION_TYPE_PARAM));
        dataSource.put(SportSectRegistriesList.FORMATIVE_ORG_UNIT_LIST_PARAM, _uiSettings.get(SportSectRegistriesList.FORMATIVE_ORG_UNIT_LIST_PARAM));
        dataSource.put(SportSectRegistriesList.PHYSICAL_FITNESS_LEVEL_LIST_PARAM, _uiSettings.get(SportSectRegistriesList.PHYSICAL_FITNESS_LEVEL_LIST_PARAM));
        dataSource.put(SportSectRegistriesList.EXIST_FREE_PLACES_PARAM, _uiSettings.get(SportSectRegistriesList.EXIST_FREE_PLACES_PARAM));
        dataSource.put(SportSectRegistriesList.COURSE_LIST_PARAM, _uiSettings.get(SportSectRegistriesList.COURSE_LIST_PARAM));
        dataSource.put(SportSectRegistriesList.SEX_LIST_PARAM, _uiSettings.get(SportSectRegistriesList.SEX_LIST_PARAM));
        dataSource.put(SportSectRegistriesList.TUTOR_ORG_UNIT_LIST_PARAM, _uiSettings.get(SportSectRegistriesList.TUTOR_ORG_UNIT_LIST_PARAM));
        dataSource.put(SportSectRegistriesList.PLACE_LIST_PARAM, _uiSettings.get(SportSectRegistriesList.PLACE_LIST_PARAM));
        dataSource.put(SportSectRegistriesList.TRAINER_LIST_PARAM, _uiSettings.get(SportSectRegistriesList.TRAINER_LIST_PARAM));
        dataSource.put(SportSectRegistriesList.FIRST_CLASS_DAY_PARAM, _uiSettings.get(SportSectRegistriesList.FIRST_CLASS_DAY_PARAM));
        dataSource.put(SportSectRegistriesList.SECOND_CLASS_DAY_PARAM, _uiSettings.get(SportSectRegistriesList.SECOND_CLASS_DAY_PARAM));
    }

    //Listeners
    public void onClickAdd()
    {
        _uiActivation.asRegionDialog(SportSectRegistriesAddEdit.class).activate();
    }

    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(SportSectRegistriesAddEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        onComponentRefresh();
    }

    public void onClickSearch()
    {
        getSettings().save();
    }

    public void onClickClear()
    {
        getSettings().clear();
    }
}