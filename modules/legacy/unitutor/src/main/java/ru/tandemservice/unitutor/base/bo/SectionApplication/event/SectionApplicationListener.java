/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication.event;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.catalog.codes.ApplicationStatus4SportsSectionCodes;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Проверяет активные заявки у студента и наличие свободных мест в секции
 *
 * @author Andrey Andreev
 * @since 02.09.2016
 */
public class SectionApplicationListener implements IDSetEventListener
{
    @Override
    public void onEvent(final DSetEvent event)
    {
        checkActiveApplication(event);
        checkFreePlaces(event);
    }

    /**
     * Проверка других активных заявок на секцию у студента
     */
    protected void checkActiveApplication(final DSetEvent event)
    {
        String main = "app";
        String sub = "subapp";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SectionApplication.class, main)
                .column(property(main))
                .where(or(eq(property(main, SectionApplication.status().code()), value(ApplicationStatus4SportsSectionCodes.ACCEPTABLE)),
                          eq(property(main, SectionApplication.status().code()), value(ApplicationStatus4SportsSectionCodes.ACCEPTED))));

        if (event.getMultitude().isSingular())
        {
            SectionApplication newApp = (SectionApplication) event.getMultitude().getSingularEntity();
            if (newApp.getStatus().getCode().equals(ApplicationStatus4SportsSectionCodes.REJECTED)
                    || newApp.getStatus().getCode().equals(ApplicationStatus4SportsSectionCodes.WITHDRAW))
                return;

            builder.where(eq(property(main, SectionApplication.student()), value(newApp.getStudent())))
                    .where(eq(property(main, SectionApplication.sportSection().educationYear()),
                              value(newApp.getSportSection().getEducationYear())))
                    .where(not(eq(property(main, SectionApplication.id()), value(newApp.getId()))));
        }
        else
        {
            builder.joinEntity(main, DQLJoinType.inner, SectionApplication.class, sub, and(
                    in(property(sub, SectionApplication.id()), event.getMultitude().getInExpression()),
                    or(eq(property(sub, SectionApplication.status().code()), value(ApplicationStatus4SportsSectionCodes.ACCEPTABLE)),
                       eq(property(sub, SectionApplication.status().code()), value(ApplicationStatus4SportsSectionCodes.ACCEPTED))),
                    eq(property(main, SectionApplication.student()), property(sub, SectionApplication.student())),
                    eq(property(main, SectionApplication.sportSection().educationYear()), property(sub, SectionApplication.sportSection().educationYear())),
                    not(eq(property(main, SectionApplication.sportSection()), property(sub, SectionApplication.sportSection())))
            ));
        }

        List<SectionApplication> applications = builder.createStatement(event.getContext()).<SectionApplication>list();

        if (CollectionUtils.isNotEmpty(applications))
        {
            SectionApplication application = applications.get(0);
            String message = "Можно иметь только одну активную заявку. "
                    + application.getStudent().getFio() + " имеет активную заявку на секцию «" + application.getSportSection().getTitle() + "».";
            throw new ApplicationException(message, true);
        }
    }

    /**
     * Проверка свободных мест в секции
     */
    protected void checkFreePlaces(final DSetEvent event)
    {
        String main = "ss";
        String sub1 = "sa1";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SportSection.class, main)
                .column(property(main));

        if (event.getMultitude().isSingular())
        {
            SectionApplication newApp = (SectionApplication) event.getMultitude().getSingularEntity();
            if (newApp.getStatus().getCode().equals(ApplicationStatus4SportsSectionCodes.REJECTED)
                    || newApp.getStatus().getCode().equals(ApplicationStatus4SportsSectionCodes.WITHDRAW))
                return;

            builder.where(eq(property(main), value(newApp.getSportSection())));
        }
        else
        {
            builder.joinEntity(main, DQLJoinType.inner, SectionApplication.class, sub1, and(
                    in(property(sub1, SectionApplication.id()), event.getMultitude().getInExpression()),
                    or(eq(property(sub1, SectionApplication.status().code()), value(ApplicationStatus4SportsSectionCodes.ACCEPTABLE)),
                       eq(property(sub1, SectionApplication.status().code()), value(ApplicationStatus4SportsSectionCodes.ACCEPTED))),
                    eq(property(main), property(sub1, SectionApplication.sportSection()))
            ));
        }

        builder.column(SportSection.getBusyPlacesCounterQuery(DQLExpressions.property(main), "sa2"));

        List<Object[]> list = builder.createStatement(event.getContext()).list();

        for (Object[] o : list)
        {
            SportSection section = (SportSection) o[0];
            Long busy = (Long) o[1];

            if (section.getNumbers() - busy < 1)
            {
                String message = "В секции «" + section.getTitle() + "» нет свободных мест.";
                throw new ApplicationException(message, true);
            }
        }
    }
}