package ru.tandemservice.unitutor.component.awp.StudentSubscribesList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.unitutor.component.awp.base.IFilterProperties;
import ru.tandemservice.unitutor.component.awp.base.discipline.DAOBase;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.catalog.ElectiveDisciplineType;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.coalesce;
import static org.tandemframework.hibsupport.dql.DQLFunctions.concat;

public class DAO extends DAOBase<Model> implements IDAO, IFilterProperties
{
    public static final String WORK_PLAN_ROW_ALIAS = "wpra";
    public static final String WORK_PLAN_ALIAS = "workPlan";

    private static final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(ElectiveDisciplineSubscribe.class, Model.PROPERTY_SUBSCRIBE)
            .addAdditionalAlias(EppWorkPlanRegistryElementRow.class, WORK_PLAN_ROW_ALIAS)
            .addAdditionalAlias(EppWorkPlanBase.class, WORK_PLAN_ALIAS);

    @Override
    public void prepare(final Model model) {
        super.prepare(model);
        model.setCanCheck(false);

        // Значение по умолчанию для типа дисциплин - ДПВ
        if (model.getSettings().get(IFilterProperties.ELECTIVE_DISCIPLINE_TYPE_FILTER) == null) {
            ElectiveDisciplineType electiveDisciplineType = getCatalogItem(ElectiveDisciplineType.class, ElectiveDisciplineTypeCodes.DPV);
            model.getSettings().set(IFilterProperties.ELECTIVE_DISCIPLINE_TYPE_FILTER, electiveDisciplineType);
        }


    }

    @Override
    public void prepareCommonFilters4Tab(Model model)
    {
        super.prepareCommonFilters4Tab(model);
        model.setElectiveDisciplineTypeModel(new LazySimpleSelectModel<>(ElectiveDisciplineType.class, ElectiveDisciplineType.shortTitle().s()));

        model.setRelevantSelectModel(new LazySimpleSelectModel<>(Model.RELEVANT_LIST));

        model.setActualElectiveDisciplineModel(new LazySimpleSelectModel<>(Model.FREE_STATUS_LIST));

        model.setStudentSelectModel(
                new DQLFullCheckSelectModel(Student.titleWithFio())
                {
                    @Override
                    protected DQLSelectBuilder query(String alias, String filter)
                    {
                        final DQLSelectBuilder dqlIn = DqlDisciplineBuilder.getDqlInStudent(model.getFilterEducationYear(),
                                                            model.getFilterYearDistributionPart(), model.getFilterEducationLevelsHighSchoolList(),
                                                            model.getFilterGroupList(), model.getFilterEmployee(), model.getFilterOrgUnit(), model.getFilterCathedraOrgUnit());
                        DQLSelectBuilder builder = new DQLSelectBuilder()
                                .fromEntity(Student.class, alias)
                                .where(in(
                                        property(Student.id().fromAlias(alias)),
                                        dqlIn.buildQuery()))
                                .order(property(Student.person().identityCard().fullFio().fromAlias(alias)));
                        FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().fullFio(), filter);
                        return builder;
                    }
                });


    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DQLSelectBuilder dql = getResultDqlData(model);

        dql.column(property(Model.PROPERTY_SUBSCRIBE));
        dql.column(property(WORK_PLAN_ALIAS));

        if (model.isDpvType())
            dql.column(property(WORK_PLAN_ROW_ALIAS));


        DynamicListDataSource<DataWrapper> dataSource = model.getDataSource();

        List<DataWrapper> wrappers = new ArrayList<>();

        final int count = getCount(dql);
        dataSource.setTotalSize(count);
        OrderDirection orderD = model.getDataSource().getEntityOrder().getDirection();
        if ("workPlan.title".equals(model.getDataSource().getEntityOrder().getKeyString()))
        {
            // Сначала сортируем по номеру. Номер версии получается из номера РУПа и номера самой версии через точку
            dql.order(coalesce(property("wp", EppWorkPlan.number()), concat(property("wpp", EppWorkPlan.number()), value("."), property("v", EppWorkPlanVersion.number()))), orderD);
            dql.order(property("term", Term.intValue()), orderD);
            dql.order(property("edYear", EducationYear.intValue()), orderD);
        }
        else
            order.applyOrder(dql, model.getDataSource().getEntityOrder());

        long startRow = dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow();
        long countRow = dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow();

        List<Object[]> resultList = getList(dql, (int) startRow, (int) countRow);

        for(Object[] row : resultList)
        {
            DataWrapper wrap = new DataWrapper(row[0]);

            wrap.setProperty(WORK_PLAN_ALIAS, row[1]);
            wrap.setProperty(WORK_PLAN_ROW_ALIAS, model.isDpvType()? row[2]:null);

            wrappers.add(wrap);
        }

        dataSource.createPage(wrappers);

    }


    protected DQLSelectBuilder filter(Model model, DQLSelectBuilder builder) {

        EducationYear educationYear = model.getSettings().get(EDUCATION_YEAR_FILTER);
        YearDistributionPart yearDistributionPart = model.getSettings().get(EDUCATION_YEAR_PART_FILTER);
        ElectiveDisciplineType electiveDisciplineType = model.getSettings().get(ELECTIVE_DISCIPLINE_TYPE_FILTER);

        // если фильтр по типу дисциплин установлен - то отображаем только указанных тип, иначе - все
        FilterUtils.applySelectFilter(builder, ElectiveDisciplineSubscribe.electiveDiscipline().type().fromAlias(Model.PROPERTY_SUBSCRIBE), electiveDisciplineType);

        // Фильтруем по учебному году и части учебного года (фильтры обязательные)
        builder
                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().fromAlias(Model.PROPERTY_SUBSCRIBE)),
                        educationYear));

        if (yearDistributionPart != null)
            builder.where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().yearPart().fromAlias(Model.PROPERTY_SUBSCRIBE)),
                                  yearDistributionPart));

        // Фильтруем по направлению подготовки
        FilterUtils.applySelectFilter(builder, ElectiveDisciplineSubscribe.student().educationOrgUnit().educationLevelHighSchool().fromAlias(Model.PROPERTY_SUBSCRIBE), model.getSettings().get(EDUCATION_LEVEL_HIGH_SCHOOL_FILTER));

        // Фильтруем по академической группе
        List<Group> groupList = model.getSettings().get(GROUP_FILTER);
        FilterUtils.applySelectFilter(builder, ElectiveDisciplineSubscribe.student().group().fromAlias(Model.PROPERTY_SUBSCRIBE), groupList);

        // по преподаватель
        if (model.getEmployeeId() != null)
            FilterUtils.applySelectFilter(builder, ElectiveDisciplineSubscribe.student().group().curator().employee().id().fromAlias(Model.PROPERTY_SUBSCRIBE), model.getEmployeeId());
		/*
		 * на кафедре нужно показывать записи студентов других институтов
		 * RM#6907
		 */
        if (model.getFilterOrgUnit() != null && !model.isCathedra()) {
            // по орг юнитам (у студента по направлению подготовки)
            DQLSelectBuilder dqlIn = DqlDisciplineBuilder.getInOrgUnitFilter(model.getFilterOrgUnit(), "EducationOrgUnitIN");
            dqlIn.column(property(EducationOrgUnit.id().fromAlias("EducationOrgUnitIN")));

            builder.where(in(property(ElectiveDisciplineSubscribe.student().educationOrgUnit().id().fromAlias(Model.PROPERTY_SUBSCRIBE)),
                    dqlIn.buildQuery()));
        }

        //фильтр по кафедре
        if (model.getFilterCathedraOrgUnit() != null && !model.getFilterCathedraOrgUnit().isEmpty()) {
            FilterUtils.applySelectFilter(builder, ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().registryElement().owner().fromAlias(Model.PROPERTY_SUBSCRIBE), model.getFilterCathedraOrgUnit());
        }

        // По студенту
        FilterUtils.applySelectFilter(builder, ElectiveDisciplineSubscribe.student().fromAlias(Model.PROPERTY_SUBSCRIBE), model.getSettings().get(STUDENT_FILTER));

        // Фильтруем по дисциплинам
        if (model.getFilterDiscipline() != null && !model.getFilterDiscipline().isEmpty())
            FilterUtils.applySelectFilter(builder, ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().registryElement().fromAlias(Model.PROPERTY_SUBSCRIBE), model.getFilterDiscipline());


        // Фильтруем только когда - только актуальные, иначе показываем все
        if (model.getSettings().get(IFilterProperties.RELEVANCE_FILTER) != null) {
            IdentifiableWrapper identifiableWrapper = model.getSettings().get(IFilterProperties.RELEVANCE_FILTER);
            if (identifiableWrapper.getId().equals(Model.ONLY_RELEVANT))
                builder.where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias(Model.PROPERTY_SUBSCRIBE))));
        }

        if (model.getSettings().get(IFilterProperties.ACTUAL_ELECTIVE_DISCIPLINE) != null) {
            IdentifiableWrapper identifiableWrapper = model.getSettings().get(ACTUAL_ELECTIVE_DISCIPLINE);
            boolean isActual = false;
            if (identifiableWrapper.getId().equals(Model.FREE_YES))
                isActual = true;

            builder.where(eq(property(ElectiveDisciplineSubscribe.electiveDiscipline().actualRow().fromAlias(Model.PROPERTY_SUBSCRIBE)), value(isActual)));

        }


        return builder;
    }

    @Override
    public void deleteElectiveDisciplineSubscribe(Collection<Long> lst) {
//        for (IEntity entity : lst) {
            new DQLDeleteBuilder(ElectiveDisciplineSubscribe.class)
                    .where(in(property(ElectiveDisciplineSubscribe.id()),lst))
                    .createStatement(getSession()).execute();
//        }
    }

    protected DQLSelectBuilder getResultDqlData(Model model) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, Model.PROPERTY_SUBSCRIBE)
                .joinPath(DQLJoinType.inner, ElectiveDisciplineSubscribe.electiveDiscipline().fromAlias(Model.PROPERTY_SUBSCRIBE), "ed");

        filter(model, builder);

        EducationYear educationYear = model.getSettings().get(EDUCATION_YEAR_FILTER);
        YearDistributionPart yearDistributionPart = model.getSettings().get(EDUCATION_YEAR_PART_FILTER);

        builder.joinEntity(Model.PROPERTY_SUBSCRIBE, DQLJoinType.inner, EppStudent2EduPlanVersion.class, "st2ep",
                           eq(property(Model.PROPERTY_SUBSCRIBE, ElectiveDisciplineSubscribe.student()), property("st2ep", EppStudent2EduPlanVersion.student())));


        //Джойним РУП
        builder.joinEntity("st2ep", DQLJoinType.inner, EppStudent2WorkPlan.class, "st2wp",
                           eq(property("st2wp", EppStudent2WorkPlan.studentEduPlanVersion()), property("st2ep.id")))
                .where(eq(property("st2wp", EppStudent2WorkPlan.cachedEppYear().educationYear()), value(educationYear)))
                .where(eq(property("st2wp", EppStudent2WorkPlan.cachedGridTerm().part()), value(yearDistributionPart)))
                .where(isNull(property("st2wp", EppStudent2WorkPlan.removalDate())))
                .where(isNull(property("st2ep", EppStudent2EduPlanVersion.removalDate())));

        builder.joinPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("st2wp"), WORK_PLAN_ALIAS)
                .joinEntity(WORK_PLAN_ALIAS, DQLJoinType.left, EppWorkPlanVersion.class, "v", eq(property(WORK_PLAN_ALIAS), property("v")))
                .joinEntity(WORK_PLAN_ALIAS, DQLJoinType.left, EppWorkPlan.class, "wp", eq(property(WORK_PLAN_ALIAS), property("wp")))
                .joinEntity("v", DQLJoinType.left, EppWorkPlan.class, "wpp", eq(property("v", EppWorkPlanVersion.parent()), property("wpp")))
                .joinEntity("wpp", DQLJoinType.left, EppYearEducationProcess.class, "yearEpp", eq(property("yearEpp"), DQLFunctions.coalesce(property("wp", EppWorkPlan.year()), property("wpp", EppWorkPlan.year()))))
                .joinEntity("wpp", DQLJoinType.left, DevelopGridTerm.class, "gridTerm", eq(property("gridTerm"), DQLFunctions.coalesce(property("wp", EppWorkPlan.cachedGridTerm()), property("wpp", EppWorkPlan.cachedGridTerm()))))
                .joinPath(DQLJoinType.inner, DevelopGridTerm.term().fromAlias("gridTerm"), "term")
                .joinPath(DQLJoinType.inner, EppYearEducationProcess.educationYear().fromAlias("yearEpp"), "edYear");


        if (model.isDpvType())
        {
            // джойним строку РУП
            builder.joinEntity("ed", DQLJoinType.left, EppWorkPlanRegistryElementRow.class, WORK_PLAN_ROW_ALIAS,
                           eq(property(ElectiveDiscipline.registryElementPart().id().fromAlias("ed")),
                                  property(WORK_PLAN_ROW_ALIAS, EppWorkPlanRegistryElementRow.registryElementPart().id())))
                    .where(eq(property(WORK_PLAN_ROW_ALIAS, EppWorkPlanRegistryElementRow.workPlan()),
                              property(WORK_PLAN_ALIAS)));
            // не исключенные строки нагрузки
            builder.where(isNull(property(EppWorkPlanRegistryElementRow.needRetake().fromAlias(WORK_PLAN_ROW_ALIAS))));
        }


        return builder;
    }
}
