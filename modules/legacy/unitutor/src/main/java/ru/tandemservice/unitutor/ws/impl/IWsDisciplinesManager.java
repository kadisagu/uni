package ru.tandemservice.unitutor.ws.impl;


import ru.tandemservice.unitutor.ws.types.*;

import java.util.List;

public interface IWsDisciplinesManager {

    public static final String STUDENT_PREF = "st";

    /**
     * Список дисциплин ДПВ студента на следующий учебный год(оба семестра)
     */
    List<WsDisciplineWrap> getWsDisciplinesNextEppYear(String studentId, String educationYearId);

    List<WsEducationYearWrap> getEducationYearList();

    List<WsElectiveWrap> getWsElectiveNextEppYear(String studentParam, String electiveType, String educationYearId);

    /**
     * В параметре idsElectiveDiscipline передаем
     * electiveDisciplineId из WsDisciplineWrap только по выделенным строкам данных
     * (не измененные записи, а выделенные записи)
     *
     * @param studentId
     * @param idsElectiveDiscipline
     * @param needSendMail
     *
     * @return
     */
    String subscribeToElectiveDiscipline(String studentId, List<String> idsElectiveDiscipline, String electiveType, boolean needSendMail, String educationYearId);

    /**
     * Учебный план студента
     *
     * @param studentParam
     *
     * @return
     */
    byte[] getEduPlanVersionData(String studentParam);

    /**
     * Заявление студента на ДПВ
     *
     * @param studentParam
     * @param id
     *
     * @return
     */
    byte[] getStudentApplicationData(String studentParam, String id);

    String getAnnotationData(String idStr);

    /**
     * Направленности
     *
     * @param studentParam
     *
     * @return
     */
    List<WsOrientationWrap> getWsOrientations(String studentParam);

    /**
     * Подписка на направленность
     *
     * @param studentPersonalNumber
     * @param orientationId
     *
     * @return
     */
    String subscribeToOrientation(String studentPersonalNumber, String orientationId);

    /**
     * Список студентов по логину
     */
    List<WsStudentWrap> getStudentListByLogin(String studentParam);

    List<WsWorkPlanInfoWrap> getCountElectiveDisciplines(String studentId, Long eduYearId, String electiveType);

}
