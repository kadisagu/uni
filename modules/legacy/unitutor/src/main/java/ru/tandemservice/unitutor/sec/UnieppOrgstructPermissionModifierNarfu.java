package ru.tandemservice.unitutor.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

public class UnieppOrgstructPermissionModifierNarfu
        implements ISecurityConfigMetaMapModifier
{

    @Override
    // Добавлены права на вкладки "Перечень ДПВ" и "Записи студентов на ДПВ"
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap) {

        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unitutor");
        config.setName("unitutor-orgunit-sec-config");
        config.setTitle("");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, new String[]{"title"})) {

            String code = description.getCode();

            PermissionGroupMeta pgEppTab = PermissionMetaUtil.createPermissionGroup(config, code + "EppPG", "Вкладка «Учебный процесс»");

            // Вкладка "Перечень ДПВ"
            PermissionGroupMeta pgOptionalDisciplineTab = PermissionMetaUtil
                    .createPermissionGroup(pgEppTab, code + "EppOptionalDisciplinePG",
                                           "Вкладка «Перечень ДПВ»");

            PermissionMetaUtil.createPermission(pgOptionalDisciplineTab,
                                                "view_optionalDiscipline_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineTab,
                                                "editPlaces_optionalDiscipline_list_" + code,
                                                "Редактирование мест для ДПВ");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineTab,
                                                "editSubscribeDate_optionalDiscipline_list_" + code,
                                                "Редактирование срока даты записи на ДПВ");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineTab,
                                                "edit_optionalDiscipline_list_" + code,
                                                "Редактирование ДПВ из перечня");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineTab,
                                                "studentSubscribesPrint_optionalDiscipline_list_" + code,
                                                "Печать списка студентов, имеющих подписки на ДПВ");

            PermissionMetaUtil.createPermission(pgOptionalDisciplineTab,
                                                "moveSubscribe_optionalDiscipline_list_" + code,
                                                "Переместить запись студентов на ДПВ");


            // Вкладка "Записи студентов на ДПВ"
            PermissionGroupMeta pgOptionalDisciplineSubscribeTab = PermissionMetaUtil
                    .createPermissionGroup(pgEppTab, code + "EppOptionalDisciplineSubscribePG",
                                           "Вкладка «Записи студентов на ДПВ»");

            PermissionMetaUtil.createPermission(pgOptionalDisciplineSubscribeTab,
                                                "view_optionalDisciplineSubscribe_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineSubscribeTab,
                                                "add_optionalDisciplineSubscribe_list_" + code,
                                                "Добавить запись студента на ДПВ");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineSubscribeTab,
                                                "delete_optionalDisciplineSubscribe_list_" + code,
                                                "Удалить запись");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineSubscribeTab,
                                                "move_optionalDisciplineSubscribe_list_" + code,
                                                "Переподписать студента на новую ДПВ");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineSubscribeTab,
                                                "print_optionalDisciplineSubscribe_list_" + code,
                                                "Печать заявлений студента на ДПВ");
            PermissionMetaUtil.createPermission(pgOptionalDisciplineSubscribeTab,
                                                "massSubscribe_optionalDisciplineSubscribe_list_" + code,
                                                "Массовая запись студентов на ДПВ, УДВ, УФ");


            // Вкладка «Направленности»
            PermissionGroupMeta pgOrientationTab = PermissionMetaUtil
                    .createPermissionGroup(pgEppTab, code + "EppOrientationPG",
                                           "Вкладка «Направленности»");

            PermissionMetaUtil.createPermission(pgOrientationTab,
                                                "view_orientation_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgOrientationTab,
                                                "actualize_orientation_list_" + code,
                                                "Актуализировать перечень профилей");
            PermissionMetaUtil.createPermission(pgOrientationTab,
                                                "editDateAndPlaces_orientation_list_" + code,
                                                "Редактирование даты и количества мест");
            PermissionMetaUtil.createPermission(pgOrientationTab,
                                                "edit_orientation_list_" + code,
                                                "Редактирование направленности из перечня");

            //Вкладка «Записи на направленность»
            PermissionGroupMeta pgStudentOrientationTab = PermissionMetaUtil
                    .createPermissionGroup(pgEppTab, code + "EppStudentOrientationPG",
                                           "Вкладка «Записи на направленность»");

            PermissionMetaUtil.createPermission(pgStudentOrientationTab,
                                                "view_studentOrientation_list_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(pgStudentOrientationTab,
                                                "add_studentOrientation_list_" + code,
                                                "Добавить запись студента");
            PermissionMetaUtil.createPermission(pgStudentOrientationTab,
                                                "mass_add_studentOrientation_list_" + code,
                                                "Массово записать студентов");
            PermissionMetaUtil.createPermission(pgStudentOrientationTab,
                                                "delete_studentOrientation_list_" + code,
                                                "Удалить записи");
            PermissionMetaUtil.createPermission(pgStudentOrientationTab,
                                                "move_studentOrientation_list_" + code,
                                                "Переподписать студента на новую направленность");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
