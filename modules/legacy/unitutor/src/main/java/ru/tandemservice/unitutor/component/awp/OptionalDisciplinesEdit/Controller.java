package ru.tandemservice.unitutor.component.awp.OptionalDisciplinesEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().update(model);
        deactivate(component, 2);
    }

    @Override
    public void deactivate(IBusinessComponent component) {
        super.deactivate(component, 2);
    }
}
