package ru.tandemservice.unitutor.component.registry.UDCRegistry.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.component.registry.RegistryBase.RegistryBaseController;
import ru.tandemservice.unitutor.component.registry.RegistryBase.RegistryWrapper;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.UniEppFormatter;

import java.util.HashMap;
import java.util.Map;

public class Controller extends RegistryBaseController<Model>
{

    @Override
    protected void createColumns(DynamicListDataSource<RegistryWrapper> dataSource, Model model) {
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Название УДВ", "electiveDiscipline." + ElectiveDiscipline.registryElementPart().registryElement().title()).setOrderable(true).setWidth("200"));
        dataSource.addColumn(new PublisherLinkColumn("№ в реестре", "electiveDiscipline." + ElectiveDiscipline.registryElementPart().registryElement().number())
                                     .setResolver(new IPublisherLinkResolver() {
                                         @Override
                                         public Object getParameters(IEntity entity)
                                         {
                                             final Map<String, Object> parameters = new HashMap<String, Object>();
                                             Long id = entity.getId();

                                             ElectiveDiscipline ed = UniDaoFacade.getCoreDao().get(id);

                                             parameters.put(PublisherActivator.PUBLISHER_ID_KEY, ed.getRegistryElementPart().getRegistryElement().getId());
                                             return parameters;
                                         }

                                         @Override
                                         public String getComponentName(IEntity paramIEntity) {
                                             return null;
                                         }
                                     })
                                     .setOrderable(true).setWidth("25"));

        dataSource.addColumn(new SimpleColumn("Курс", "electiveDiscipline." + ElectiveDiscipline.P_COURSE_STR).setOrderable(false).setWidth("25"));
        dataSource.addColumn(new SimpleColumn("УГН", "electiveDiscipline." + ElectiveDiscipline.P_GROUP_STR).setOrderable(false).setWidth("200"));


        dataSource.addColumn(new BooleanColumn("Аннотация", "containAnnotation").setWidth("50").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Часов всего", "electiveDiscipline." + ElectiveDiscipline.registryElementPart().size(), new UniEppFormatter()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("ЗЕТ",
                                              "electiveDiscipline." + ElectiveDiscipline.registryElementPart().labor(), new UniEppFormatter()).setClickable(false).setOrderable(false));


        dataSource.addColumn(new SimpleColumn("Формы контроля", "formOfControl").setWidth("100").setClickable(false).setOrderable(false));

        HeadColumn placesCountHeadColumn = new HeadColumn("placesCount", "Количество мест");
        placesCountHeadColumn.addColumn(new SimpleColumn("Всего", "electiveDiscipline." + ElectiveDiscipline.placesCount()).setOrderable(true));

        placesCountHeadColumn.addColumn(new SimpleColumn("Записано", "placesSubscribed")).setWidth("15").setAlign("center").setClickable(true).setOrderable(false);
        placesCountHeadColumn.addColumn(new SimpleColumn("Свободно", "freePlacesToSubscribe")).setWidth("20").setAlign("center").setClickable(false).setOrderable(false);

        dataSource.addColumn(placesCountHeadColumn.setHeaderAlign("center").setRequired(true));

        dataSource.addColumn(new DateColumn("Запись до", "electiveDiscipline." + ElectiveDiscipline.subscribeToDate()).setOrderable(true));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("editElectiveUDVRegistry"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete").setPermissionKey("deleteElectiveUDVRegistry"));
    }

    public void onClickAddElement(IBusinessComponent component) {
        Model model = getModel(component);
        EducationYear year = model.getFilterEducationYear();
        if (year == null)
            throw new ApplicationException("Поле Учебный год обязательно для заполнения");

        YearDistributionPart educationYearPart = model.getFilterYearDistributionPart();
        if (educationYearPart == null)
            throw new ApplicationException("Поле Часть года обязательно для заполнения");

        ParametersMap paramMap = new ParametersMap()
                .add("electiveDisciplineId", null)
                .add("electiveDisciplineTypeCode", ElectiveDisciplineTypeCodes.UDV)
                .add("educationYearId", year.getId())
                .add("educationYearPartId", educationYearPart.getId());

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(ru.tandemservice.unitutor.component.registry.RegistryBase.AddEdit.Controller.class.getPackage().getName(), paramMap));
    }

    public void onClickEdit(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = model.getDataSource().getLastVisitedEntityId();

        RegistryWrapper wrapper = model.getDataSource().getRecordById(id);
        ParametersMap paramMap = new ParametersMap()
                .add("electiveDisciplineId", wrapper.getElectiveDiscipline().getId())
                .add("electiveDisciplineTypeCode", wrapper.getElectiveDiscipline().getType().getCode());

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(ru.tandemservice.unitutor.component.registry.RegistryBase.AddEdit.Controller.class.getPackage().getName(), paramMap));
    }

    public void onClickDelete(IBusinessComponent component) {
        UniDaoFacade.getCoreDao().delete(component.<Long>getListenerParameter());
    }
}
