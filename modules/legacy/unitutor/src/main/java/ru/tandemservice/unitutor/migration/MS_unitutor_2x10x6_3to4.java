package ru.tandemservice.unitutor.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unitutor_2x10x6_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sportSection

		// изменен тип свойства firstClassTime
		{
			// изменить тип колонки
			tool.changeColumnType("sportsection_t", "firstclasstime_p", DBType.TIMESTAMP);

		}

		// изменен тип свойства secondClassTime
		{
			// изменить тип колонки
			tool.changeColumnType("sportsection_t", "secondclasstime_p", DBType.TIMESTAMP);

		}


    }
}