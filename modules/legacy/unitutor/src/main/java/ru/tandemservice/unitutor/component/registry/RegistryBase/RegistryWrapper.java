package ru.tandemservice.unitutor.component.registry.RegistryBase;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;


public class RegistryWrapper extends EntityBase {

    private ElectiveDiscipline electiveDiscipline;

    private String formOfControl;

    private Long placesSubscribed;

    private Long freePlacesToSubscribe;

    /**
     * метка - содержит ли аннотацию
     */
    private boolean containAnnotation;

    @Override
    public Long getId() {
        return electiveDiscipline.getId();
    }

    public ElectiveDiscipline getElectiveDiscipline() {
        return electiveDiscipline;
    }

    public void setElectiveDiscipline(ElectiveDiscipline electiveDiscipline) {
        this.electiveDiscipline = electiveDiscipline;
    }

    public Long getPlacesSubscribed() {
        return placesSubscribed;
    }

    public void setPlacesSubscribed(Long placesSubscribed) {
        this.placesSubscribed = placesSubscribed;
    }

    public Long getFreePlacesToSubscribe() {
        return freePlacesToSubscribe;
    }

    public void setFreePlacesToSubscribe(Long freePlacesToSubscribe) {
        this.freePlacesToSubscribe = freePlacesToSubscribe;
    }

    public void setContainAnnotation(boolean containAnnotation) {
        this.containAnnotation = containAnnotation;
    }

    public boolean isContainAnnotation() {
        return containAnnotation;
    }

    public String getFormOfControl() {
        return formOfControl;
    }

    public void setFormOfControl(String formOfControl) {
        this.formOfControl = formOfControl;
    }

}