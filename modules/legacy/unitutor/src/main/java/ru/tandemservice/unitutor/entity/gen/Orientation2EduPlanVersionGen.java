package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь направленности и УПв
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class Orientation2EduPlanVersionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion";
    public static final String ENTITY_NAME = "orientation2EduPlanVersion";
    public static final int VERSION_HASH = -940271192;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORIENTATION = "orientation";
    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_COURSE = "course";
    public static final String P_ACTUAL = "actual";
    public static final String P_ALLOW_ENTRY = "allowEntry";
    public static final String P_MIN_COUNT = "minCount";
    public static final String P_DATE = "date";
    public static final String P_ANNOTATION = "annotation";

    private EducationOrgUnit _orientation;     // Напралвенность
    private EppEduPlanVersion _eduPlanVersion;     // УПв
    private EducationYear _educationYear;     // Учебный год
    private Course _course;     // Курс
    private boolean _actual;     // Актуальность
    private boolean _allowEntry = false;     // Разрешить запись
    private int _minCount;     // Минимальное количество мест
    private Date _date;     // Запись до
    private String _annotation;     // Аннотация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Напралвенность. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getOrientation()
    {
        return _orientation;
    }

    /**
     * @param orientation Напралвенность. Свойство не может быть null.
     */
    public void setOrientation(EducationOrgUnit orientation)
    {
        dirty(_orientation, orientation);
        _orientation = orientation;
    }

    /**
     * @return УПв. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion УПв. Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Учебный год.
     */
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Курс.
     */
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Актуальность. Свойство не может быть null.
     */
    @NotNull
    public boolean isActual()
    {
        return _actual;
    }

    /**
     * @param actual Актуальность. Свойство не может быть null.
     */
    public void setActual(boolean actual)
    {
        dirty(_actual, actual);
        _actual = actual;
    }

    /**
     * @return Разрешить запись. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowEntry()
    {
        return _allowEntry;
    }

    /**
     * @param allowEntry Разрешить запись. Свойство не может быть null.
     */
    public void setAllowEntry(boolean allowEntry)
    {
        dirty(_allowEntry, allowEntry);
        _allowEntry = allowEntry;
    }

    /**
     * @return Минимальное количество мест. Свойство не может быть null.
     */
    @NotNull
    public int getMinCount()
    {
        return _minCount;
    }

    /**
     * @param minCount Минимальное количество мест. Свойство не может быть null.
     */
    public void setMinCount(int minCount)
    {
        dirty(_minCount, minCount);
        _minCount = minCount;
    }

    /**
     * @return Запись до. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Запись до. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Аннотация.
     */
    public String getAnnotation()
    {
        return _annotation;
    }

    /**
     * @param annotation Аннотация.
     */
    public void setAnnotation(String annotation)
    {
        dirty(_annotation, annotation);
        _annotation = annotation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof Orientation2EduPlanVersionGen)
        {
            setOrientation(((Orientation2EduPlanVersion)another).getOrientation());
            setEduPlanVersion(((Orientation2EduPlanVersion)another).getEduPlanVersion());
            setEducationYear(((Orientation2EduPlanVersion)another).getEducationYear());
            setCourse(((Orientation2EduPlanVersion)another).getCourse());
            setActual(((Orientation2EduPlanVersion)another).isActual());
            setAllowEntry(((Orientation2EduPlanVersion)another).isAllowEntry());
            setMinCount(((Orientation2EduPlanVersion)another).getMinCount());
            setDate(((Orientation2EduPlanVersion)another).getDate());
            setAnnotation(((Orientation2EduPlanVersion)another).getAnnotation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends Orientation2EduPlanVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) Orientation2EduPlanVersion.class;
        }

        public T newInstance()
        {
            return (T) new Orientation2EduPlanVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orientation":
                    return obj.getOrientation();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "educationYear":
                    return obj.getEducationYear();
                case "course":
                    return obj.getCourse();
                case "actual":
                    return obj.isActual();
                case "allowEntry":
                    return obj.isAllowEntry();
                case "minCount":
                    return obj.getMinCount();
                case "date":
                    return obj.getDate();
                case "annotation":
                    return obj.getAnnotation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orientation":
                    obj.setOrientation((EducationOrgUnit) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "actual":
                    obj.setActual((Boolean) value);
                    return;
                case "allowEntry":
                    obj.setAllowEntry((Boolean) value);
                    return;
                case "minCount":
                    obj.setMinCount((Integer) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "annotation":
                    obj.setAnnotation((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orientation":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "educationYear":
                        return true;
                case "course":
                        return true;
                case "actual":
                        return true;
                case "allowEntry":
                        return true;
                case "minCount":
                        return true;
                case "date":
                        return true;
                case "annotation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orientation":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "educationYear":
                    return true;
                case "course":
                    return true;
                case "actual":
                    return true;
                case "allowEntry":
                    return true;
                case "minCount":
                    return true;
                case "date":
                    return true;
                case "annotation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orientation":
                    return EducationOrgUnit.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "educationYear":
                    return EducationYear.class;
                case "course":
                    return Course.class;
                case "actual":
                    return Boolean.class;
                case "allowEntry":
                    return Boolean.class;
                case "minCount":
                    return Integer.class;
                case "date":
                    return Date.class;
                case "annotation":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<Orientation2EduPlanVersion> _dslPath = new Path<Orientation2EduPlanVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "Orientation2EduPlanVersion");
    }
            

    /**
     * @return Напралвенность. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getOrientation()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> orientation()
    {
        return _dslPath.orientation();
    }

    /**
     * @return УПв. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Актуальность. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#isActual()
     */
    public static PropertyPath<Boolean> actual()
    {
        return _dslPath.actual();
    }

    /**
     * @return Разрешить запись. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#isAllowEntry()
     */
    public static PropertyPath<Boolean> allowEntry()
    {
        return _dslPath.allowEntry();
    }

    /**
     * @return Минимальное количество мест. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getMinCount()
     */
    public static PropertyPath<Integer> minCount()
    {
        return _dslPath.minCount();
    }

    /**
     * @return Запись до. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Аннотация.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getAnnotation()
     */
    public static PropertyPath<String> annotation()
    {
        return _dslPath.annotation();
    }

    public static class Path<E extends Orientation2EduPlanVersion> extends EntityPath<E>
    {
        private EducationOrgUnit.Path<EducationOrgUnit> _orientation;
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private EducationYear.Path<EducationYear> _educationYear;
        private Course.Path<Course> _course;
        private PropertyPath<Boolean> _actual;
        private PropertyPath<Boolean> _allowEntry;
        private PropertyPath<Integer> _minCount;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _annotation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Напралвенность. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getOrientation()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> orientation()
        {
            if(_orientation == null )
                _orientation = new EducationOrgUnit.Path<EducationOrgUnit>(L_ORIENTATION, this);
            return _orientation;
        }

    /**
     * @return УПв. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Актуальность. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#isActual()
     */
        public PropertyPath<Boolean> actual()
        {
            if(_actual == null )
                _actual = new PropertyPath<Boolean>(Orientation2EduPlanVersionGen.P_ACTUAL, this);
            return _actual;
        }

    /**
     * @return Разрешить запись. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#isAllowEntry()
     */
        public PropertyPath<Boolean> allowEntry()
        {
            if(_allowEntry == null )
                _allowEntry = new PropertyPath<Boolean>(Orientation2EduPlanVersionGen.P_ALLOW_ENTRY, this);
            return _allowEntry;
        }

    /**
     * @return Минимальное количество мест. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getMinCount()
     */
        public PropertyPath<Integer> minCount()
        {
            if(_minCount == null )
                _minCount = new PropertyPath<Integer>(Orientation2EduPlanVersionGen.P_MIN_COUNT, this);
            return _minCount;
        }

    /**
     * @return Запись до. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(Orientation2EduPlanVersionGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Аннотация.
     * @see ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion#getAnnotation()
     */
        public PropertyPath<String> annotation()
        {
            if(_annotation == null )
                _annotation = new PropertyPath<String>(Orientation2EduPlanVersionGen.P_ANNOTATION, this);
            return _annotation;
        }

        public Class getEntityClass()
        {
            return Orientation2EduPlanVersion.class;
        }

        public String getEntityName()
        {
            return "orientation2EduPlanVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
