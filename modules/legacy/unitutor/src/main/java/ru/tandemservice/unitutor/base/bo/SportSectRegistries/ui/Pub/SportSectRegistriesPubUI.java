/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.AddEdit.SportSectRegistriesAddEdit;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.SportSection2CourseRel;
import ru.tandemservice.unitutor.entity.SportSection2SexRel;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.exists;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Andreev
 * @since 24.08.2016
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "sectionId", required = true)})
public class SportSectRegistriesPubUI extends UIPresenter
{
    private Long _sectionId;
    private SportSection _sportSection;
    private String course;
    private String sex;

    @Override
    public void onComponentRefresh()
    {
        if (_sectionId == null)
            throw new ApplicationException();

        ICommonDAO dao = DataAccessServices.dao();

        _sportSection = dao.getNotNull(SportSection.class, _sectionId);

        List<String> courseList = dao.<String>getList(new DQLSelectBuilder()
                                                              .fromEntity(Course.class, "c")
                                                              .column(property("c", Course.title()))
                                                              .where(exists(SportSection2CourseRel.class,
                                                                            SportSection2CourseRel.section().s(), _sportSection,
                                                                            SportSection2CourseRel.course().s(), property("c")))
                                                              .order(property("c", Course.intValue()))
        );
        course = CommonBaseStringUtil.joinNotEmpty(courseList, ", ");

        List<String> sexList = dao.<String>getList(new DQLSelectBuilder()
                                                           .fromEntity(Sex.class, "s")
                                                           .column(property("s", Sex.title()))
                                                           .where(exists(SportSection2SexRel.class,
                                                                         SportSection2SexRel.section().s(), _sportSection,
                                                                         SportSection2SexRel.sex().s(), property("s")))
        );

        sex = CommonBaseStringUtil.joinNotEmpty(sexList, ", ");
    }

    //Listeners
    public void onClickEdit()
    {
        _uiActivation.asRegionDialog(SportSectRegistriesAddEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, _sectionId)
                .activate();
    }

    public void onClickCopy()
    {
        _uiActivation.asRegionDialog(SportSectRegistriesAddEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, null)
                .parameter(SportSectRegistriesAddEdit.SOURCE_ID_4_COPY, _sectionId)
                .activate();
    }

    //Getters and Setters
    public Long getSectionId()
    {
        return _sectionId;
    }

    public void setSectionId(Long sectionId)
    {
        _sectionId = sectionId;
    }

    public SportSection getSportSection()
    {
        return _sportSection;
    }

    public void setSportSection(SportSection sportSection)
    {
        _sportSection = sportSection;
    }

    public String getPageTitle()
    {
        return getConfig().getProperty("ui.pageTitle") + " " + _sportSection.getTitleWithEduYear();
    }

    public String getCourse()
    {
        return course;
    }

    public String getSex()
    {
        return sex;
    }
}