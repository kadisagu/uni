/* $Id$ */
package ru.tandemservice.unitutor.ws.impl;

import ru.tandemservice.unitutor.ws.types.WsSportSectionWrap;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 29.08.2016
 */
public interface IWsSportSectionManager
{

    List<WsSportSectionWrap> getWsSportSections(String studentId);

    String updateApplications(String studentParam, String createApplication, String withdrawApplication);
}