package ru.tandemservice.unitutor.component.registry.RegistryBase.AddEdit;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    void onChangeEduYear(Model model);
}
