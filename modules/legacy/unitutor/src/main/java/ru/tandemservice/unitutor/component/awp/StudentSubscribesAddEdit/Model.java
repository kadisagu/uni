package ru.tandemservice.unitutor.component.awp.StudentSubscribesAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.dao.DisciplineWrap;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Input({
        @Bind(key = "orgUnitIds", binding = "orgUnitIds"),
        @Bind(key = "tutorId", binding = "tutorId"),
        @Bind(key = "educationYearId", binding = "educationYearId"),
        @Bind(key = "yearDistributionPartId", binding = "yearDistributionPartId"),
        @Bind(key = "electiveDisciplineSubscribeId", binding = "electiveDisciplineSubscribeId"),
        @Bind(key = "electiveDisciplineTypeCode", binding = "electiveDisciplineTypeCode")
})
public class Model {
    private Student student;
    private ISelectModel studentSelectModel;
    private List<Long> orgUnitIds;
    private Long tutorId;
    private Employee tutor;
    private Long _educationYearId;
    private EducationYear educationYear;
    private Long yearDistributionPartId;
    private YearDistributionPart yearDistributionPart;
    private ISelectModel yearDistributionPartListModel;
    private DynamicListDataSource<DisciplineWrap> dataSource;
    private DynamicListDataSource<ElectiveDiscipline> electiveDataSource;

    private String _formTitle;

    Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> subscribesMap = new HashMap<>();

    private Long electiveDisciplineSubscribeId;
    private ElectiveDisciplineSubscribe electiveDisciplineSubscribe;

    private String electiveDisciplineTypeCode;

    private List<DisciplineWrap> dataList;
    private List<ElectiveDiscipline> electiveDataList;

    private String warning;

    private boolean sendMail;

    private ISelectModel eduYearModel;

    private ISelectModel _groupListModel;

    private List<Group> _groupList;

    public String getFormTitle()
    {
        return _formTitle;
    }

    public void setFormTitle(String formTitle)
    {
        _formTitle = formTitle;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        _groupListModel = groupListModel;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public ISelectModel getEduYearModel() {
        return eduYearModel;
    }

    public Long getEducationYearId()
    {
        return _educationYearId;
    }

    public void setEducationYearId(Long educationYearId)
    {
        _educationYearId = educationYearId;
    }

    public void setEduYearModel(ISelectModel eduYearModel) {
        this.eduYearModel = eduYearModel;
    }

    public DynamicListDataSource<DisciplineWrap> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<DisciplineWrap> dataSource) {
        this.dataSource = dataSource;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ISelectModel getStudentSelectModel() {
        return studentSelectModel;
    }

    public void setStudentSelectModel(ISelectModel studentSelectModel) {
        this.studentSelectModel = studentSelectModel;
    }

    public List<Long> getOrgUnitIds() {
        return orgUnitIds;
    }

    public void setOrgUnitIds(List<Long> orgUnitIds) {
        this.orgUnitIds = orgUnitIds;
    }

    public Long getTutorId() {
        return tutorId;
    }

    public void setTutorId(Long tutorId) {
        this.tutorId = tutorId;
    }

    public Employee getTutor() {
        return tutor;
    }

    public void setTutor(Employee tutor) {
        this.tutor = tutor;
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public List<Employee> getFilterEmployeeList()
    {
        List<Employee> lst = new ArrayList<>();

        if (getTutor() != null)
            lst.add(getTutor());
        return lst;
    }

    public Long getYearDistributionPartId() {
        return yearDistributionPartId;
    }

    public void setYearDistributionPartId(Long yearDistributionPartId) {
        this.yearDistributionPartId = yearDistributionPartId;
    }

    public YearDistributionPart getYearDistributionPart() {
        return yearDistributionPart;
    }

    public void setYearDistributionPart(YearDistributionPart yearDistributionPart) {
        this.yearDistributionPart = yearDistributionPart;
    }

    public ISelectModel getYearDistributionPartListModel() {
        return yearDistributionPartListModel;
    }

    public void setYearDistributionPartListModel(
            ISelectModel yearDistributionPartListModel)
    {
        this.yearDistributionPartListModel = yearDistributionPartListModel;
    }

    public List<DisciplineWrap> getDataList() {
        return dataList;
    }

    public void setDataList(List<DisciplineWrap> dataList) {
        this.dataList = dataList;
    }

    public Long getElectiveDisciplineSubscribeId() {
        return electiveDisciplineSubscribeId;
    }

    public void setElectiveDisciplineSubscribeId(
            Long electiveDisciplineSubscribeId)
    {
        this.electiveDisciplineSubscribeId = electiveDisciplineSubscribeId;
    }

    public ElectiveDisciplineSubscribe getElectiveDisciplineSubscribe() {
        return electiveDisciplineSubscribe;
    }

    public void setElectiveDisciplineSubscribe(ElectiveDisciplineSubscribe electiveDisciplineSubscribe)
    {
        this.electiveDisciplineSubscribe = electiveDisciplineSubscribe;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public boolean hasWarning()
    {
        if (warning != null && warning.length() > 0)
            return true;
        else
            return false;
    }

    public boolean isSendMail() {
        return sendMail;
    }

    public void setSendMail(boolean sendMail) {
        this.sendMail = sendMail;
    }

    public DynamicListDataSource<ElectiveDiscipline> getElectiveDataSource() {
        return electiveDataSource;
    }

    public void setElectiveDataSource(DynamicListDataSource<ElectiveDiscipline> electiveDataSource)
    {
        this.electiveDataSource = electiveDataSource;
    }

    public List<ElectiveDiscipline> getElectiveDataList() {
        return electiveDataList;
    }

    public void setElectiveDataList(List<ElectiveDiscipline> electiveDataList) {
        this.electiveDataList = electiveDataList;
    }

    public String getElectiveDisciplineTypeCode() {
        return electiveDisciplineTypeCode;
    }

    public void setElectiveDisciplineTypeCode(String electiveDisciplineTypeCode) {
        this.electiveDisciplineTypeCode = electiveDisciplineTypeCode;
    }

    public Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> getSubscribesMap() {
        return subscribesMap;
    }

    public void setSubscribesMap(Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> subscribesMap)
    {
        this.subscribesMap = subscribesMap;
    }

    public boolean isDpv() {
        return  (ElectiveDisciplineTypeCodes.DPV.equals(electiveDisciplineTypeCode));
    }
}
