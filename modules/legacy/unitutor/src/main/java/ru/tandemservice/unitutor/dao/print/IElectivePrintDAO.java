package ru.tandemservice.unitutor.dao.print;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;


import java.util.List;

/**
 * Печать по данным Дисциплин по выбору (ДПВ)
 */
public interface IElectivePrintDAO {

    /**
     * Сформировать документ для печати
     *
     * @param electiveDisciplines
     *
     * @return
     */
    public RtfDocument generateDocument(List<ElectiveDiscipline> electiveDisciplines);
}
