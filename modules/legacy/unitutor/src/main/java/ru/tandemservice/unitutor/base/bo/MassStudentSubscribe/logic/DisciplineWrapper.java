/* $Id$ */
package ru.tandemservice.unitutor.base.bo.MassStudentSubscribe.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;

/**
 * @author Ekaterina Zvereva
 * @since 15.11.2015
 */
public class DisciplineWrapper extends DataWrapper
{
    private Long _id;

    private Student _student;
    private ElectiveDiscipline _electiveDiscipline;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public ElectiveDiscipline getElectiveDiscipline()
    {
        return _electiveDiscipline;
    }

    public void setElectiveDiscipline(ElectiveDiscipline electiveDiscipline)
    {
        _electiveDiscipline = electiveDiscipline;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DisciplineWrapper that = (DisciplineWrapper) o;

        if (!_id.equals(that._id)) return false;
        if (!_student.equals(that._student)) return false;
        return _electiveDiscipline.equals(that._electiveDiscipline);

    }

    @Override
    public int hashCode()
    {
        int result = _id.hashCode();
        result = 31 * result + _student.hashCode();
        result = 31 * result + _electiveDiscipline.hashCode();
        return result;
    }
}