package ru.tandemservice.unitutor.component.awp.StudentSubscribesAddEdit;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void updateDisciplineListModel(Model model);

    public void updateElectiveRegistryDisciplineModel(Model model);

    @Transactional
    public void saveSubscribe(Model model);

    @Transactional
    public void saveElectiveSubscribe(Model model);

}
