/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication.ui.MassAdd;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.dao.ICatalogDAO;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.catalog.ApplicationStatus4SportsSection;
import ru.tandemservice.unitutor.entity.catalog.codes.ApplicationStatus4SportsSectionCodes;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;

/**
 * @author Andrey Andreev
 * @since 29.08.2016
 */
@Input({@Bind(key = SectionApplicationMassAdd.ALL_COMMON_FILTERS_REQUIRED, binding = "allCommonFiltersRequired")})
public class SectionApplicationMassAddUI extends UIPresenter
{
    private StringBuilder _message = new StringBuilder();

    public boolean _allCommonFiltersRequired = false;

    @Override
    public void onComponentRefresh()
    {
        _uiSettings.set(SectionApplicationMassAdd.EDUCATION_YEAR_PARAM, EducationYear.getCurrentRequired());
        _message.setLength(0);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SectionApplicationMassAdd.ALL_COMMON_FILTERS_REQUIRED, _allCommonFiltersRequired);

        dataSource.put(SectionApplicationMassAdd.COURSE_PARAM, _uiSettings.get(SectionApplicationMassAdd.COURSE_PARAM));
        dataSource.put(SectionApplicationMassAdd.SEX_PARAM, _uiSettings.get(SectionApplicationMassAdd.SEX_PARAM));
        dataSource.put(SectionApplicationMassAdd.FORMATIVE_ORG_UNIT_PARAM, _uiSettings.get(SectionApplicationMassAdd.FORMATIVE_ORG_UNIT_PARAM));


        dataSource.put(SectionApplicationMassAdd.GROUP_PARAM, _uiSettings.get(SectionApplicationMassAdd.GROUP_PARAM));
        dataSource.put(SectionApplicationMassAdd.STUDENT_STATUS_PARAM, _uiSettings.get(SectionApplicationMassAdd.STUDENT_STATUS_PARAM));
        dataSource.put(SectionApplicationMassAdd.PROGRAM_FORM_PARAM, _uiSettings.get(SectionApplicationMassAdd.PROGRAM_FORM_PARAM));
        dataSource.put(SectionApplicationMassAdd.EDUCATION_LEVEL_PARAM, _uiSettings.get(SectionApplicationMassAdd.EDUCATION_LEVEL_PARAM));


        dataSource.put(SectionApplicationMassAdd.EDUCATION_YEAR_PARAM, _uiSettings.get(SectionApplicationMassAdd.EDUCATION_YEAR_PARAM));
        dataSource.put(SectionApplicationMassAdd.SECTION_TYPE_PARAM, _uiSettings.get(SectionApplicationMassAdd.SECTION_TYPE_PARAM));
        dataSource.put(SectionApplicationMassAdd.PHYSICAL_FITNESS_LEVEL_LIST_PARAM, _uiSettings.get(SectionApplicationMassAdd.PHYSICAL_FITNESS_LEVEL_LIST_PARAM));
        dataSource.put(SectionApplicationMassAdd.PLACE_LIST_PARAM, _uiSettings.get(SectionApplicationMassAdd.PLACE_LIST_PARAM));
        dataSource.put(SectionApplicationMassAdd.FIRST_CLASS_DAY_PARAM, _uiSettings.get(SectionApplicationMassAdd.FIRST_CLASS_DAY_PARAM));
        dataSource.put(SectionApplicationMassAdd.SECOND_CLASS_DAY_PARAM, _uiSettings.get(SectionApplicationMassAdd.SECOND_CLASS_DAY_PARAM));
        dataSource.put(SectionApplicationMassAdd.TRAINER_LIST_PARAM, _uiSettings.get(SectionApplicationMassAdd.TRAINER_LIST_PARAM));
    }


    //Listeners
    public void onClickCreate()
    {
        getSettings().save();
        _message.setLength(0);

        ICommonDAO dao = DataAccessServices.dao();

        BaseSearchListDataSource sectionListDS = _uiConfig.getDataSource(SectionApplicationMassAdd.SECTION_LIST_DS);
        Collection<IEntity> sections = sectionListDS.getOptionColumnSelectedObjects(SectionApplicationMassAdd.SECTION_LIST_RADIO);
        if (CollectionUtils.isEmpty(sections))
            throw new ApplicationException("Не выбрана Спортивная секция");
        SportSection section = ((DataWrapper) sections.iterator().next()).getWrapped();

        BaseSearchListDataSource studentListDS = _uiConfig.getDataSource(SectionApplicationMassAdd.STUDENT_LIST_DS);
        CheckboxColumn studentsCheckColumn = (CheckboxColumn) studentListDS.getLegacyDataSource().getColumn(SectionApplicationMassAdd.STUDENT_LIST_CHECKBOX);

        Collection<IEntity> students4NewApplications = getStudents4NewApplications(studentsCheckColumn.getSelectedObjects(), section);
        students4NewApplications.forEach(student -> dao.save(new SectionApplication(section, (Student) student, true)));

        if(CollectionUtils.isNotEmpty(students4NewApplications))
       _message.append("Добавлены заявки для студентов: ")
               .append(students4NewApplications.stream().map(e -> (Student) e).map(PersonRole::getFio).collect(Collectors.joining(", ")))
               .append(".");

        studentsCheckColumn.reset();
    }


    public void onClickCreateAndAccept()
    {
        getSettings().save();
        _message.setLength(0);

        BaseSearchListDataSource sectionListDS = _uiConfig.getDataSource(SectionApplicationMassAdd.SECTION_LIST_DS);
        Collection<IEntity> sections = sectionListDS.getOptionColumnSelectedObjects(SectionApplicationMassAdd.SECTION_LIST_RADIO);
        if (CollectionUtils.isEmpty(sections))
            throw new ApplicationException("Не выбрана Спортивная секция");
        SportSection section = ((DataWrapper) sections.iterator().next()).getWrapped();

        BaseSearchListDataSource studentListDS = _uiConfig.getDataSource(SectionApplicationMassAdd.STUDENT_LIST_DS);
        CheckboxColumn studentsCheckColumn = (CheckboxColumn) studentListDS.getLegacyDataSource().getColumn(SectionApplicationMassAdd.STUDENT_LIST_CHECKBOX);
        Collection<IEntity> students4NewApplications = getStudents4NewApplications(studentsCheckColumn.getSelectedObjects(), section);

        ICommonDAO dao = DataAccessServices.dao();
        int free = section.getNumbers() - dao.getCount(SportSection.getAcceptedApplications(value(section), "ss"));
        if(free < students4NewApplications.size())
            throw new ApplicationException("В данную секцию возможно записать только " + String.valueOf(free) + " человек из отобранных " + String.valueOf(students4NewApplications.size()) + ".");

        ICatalogDAO catalogDao = CatalogManager.instance().dao();
        if(CollectionUtils.isNotEmpty(students4NewApplications))
        {
            students4NewApplications.forEach(student -> {
                SectionApplication newApplication = new SectionApplication(section, (Student) student, true);
                newApplication.setStatus(catalogDao.getCatalogItem(ApplicationStatus4SportsSection.class, ApplicationStatus4SportsSectionCodes.ACCEPTED));
                dao.save(newApplication);
            });
            _message.append("Добавлены согласованные заявки для студентов: ")
                    .append(students4NewApplications.stream().map(e -> (Student) e).map(PersonRole::getFio).collect(Collectors.joining(", ")))
                    .append(".");
        }

        if (free == students4NewApplications.size())
        {
            List<SectionApplication> otherApplications = dao.getList(
                    new DQLSelectBuilder()
                            .fromEntity(SectionApplication.class, "sa")
                            .column(property("sa"))
                            .where(eq(property("sa", SectionApplication.status().code()), value(ApplicationStatus4SportsSectionCodes.ACCEPTABLE)))
                            .where(eq(property("sa", SectionApplication.sportSection()), value(section)))
            );

            if (!otherApplications.isEmpty())
            {
                ApplicationStatus4SportsSection rejected = catalogDao.getCatalogItem(ApplicationStatus4SportsSection.class, ApplicationStatus4SportsSectionCodes.REJECTED);
                otherApplications.forEach(app ->
                                          {
                                              app.setStatus(rejected);
                                              dao.update(app);
                                          });
            }
        }

        studentsCheckColumn.reset();
    }

    public void onClickClearCommonFilters()
    {
        _uiSettings.set(SectionApplicationMassAdd.COURSE_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.SEX_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.FORMATIVE_ORG_UNIT_PARAM, null);
        _message.setLength(0);
    }

    public void onClickClearStudentFilters()
    {
        _uiSettings.set(SectionApplicationMassAdd.GROUP_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.STUDENT_STATUS_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.PROGRAM_FORM_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.EDUCATION_LEVEL_PARAM, null);
        _message.setLength(0);
    }

    public void onClickClearSectionFilters()
    {
        _uiSettings.set(SectionApplicationMassAdd.SECTION_TYPE_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.PHYSICAL_FITNESS_LEVEL_LIST_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.PLACE_LIST_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.FIRST_CLASS_DAY_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.SECOND_CLASS_DAY_PARAM, null);
        _uiSettings.set(SectionApplicationMassAdd.TRAINER_LIST_PARAM, null);
        _message.setLength(0);
    }


    //Getters and Setters

    public String getMessage()
    {
        return _message.toString();
    }

    public boolean isShowMessage()
    {
        return _message.length() > 0;
    }

    public boolean getAllCommonFiltersRequired()
    {
        return _allCommonFiltersRequired;
    }

    public void setAllCommonFiltersRequired(boolean allCommonFiltersRequired)
    {
        _allCommonFiltersRequired = allCommonFiltersRequired;
    }


    public Collection<IEntity> getStudents4NewApplications(Collection<IEntity> students, SportSection section)
    {
        if (CollectionUtils.isEmpty(students))
            throw new ApplicationException("Нет выбранных студентов");

        List<Student> studentsWithExistApp = DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                        .fromEntity(SectionApplication.class, "sa")
                        .column(property("sa", SectionApplication.student()))
                        .where(in(property("sa", SectionApplication.student()), students))
                        .where(eq(property("sa", SectionApplication.sportSection()), value(section)))
        );

        StringBuilder warning = new StringBuilder()
                .append("Студенты: ")
                .append(studentsWithExistApp.stream().map(PersonRole::getFio).collect(Collectors.joining(", ")))
                .append(" уже умеют заявки на эту секцию. ");

        students.removeAll(studentsWithExistApp);
        if (CollectionUtils.isEmpty(students))
            throw new ApplicationException(warning.toString());

        if(CollectionUtils.isNotEmpty(studentsWithExistApp) )
            _message.append(warning);

        return students;
    }
}