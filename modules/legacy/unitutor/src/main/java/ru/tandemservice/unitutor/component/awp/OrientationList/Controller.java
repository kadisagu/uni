package ru.tandemservice.unitutor.component.awp.OrientationList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;

import java.util.*;
import java.util.stream.Collectors;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareDataSource(component);
    }

    @SuppressWarnings("unchecked")
    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);

        if (model.getDataSource() != null) return;

        DynamicListDataSource<Orientation2EduPlanVersion> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new ToggleColumn("Разрешить запись", Orientation2EduPlanVersion.allowEntry()).setListener("onClickAllowEntry").setDisableHandler(entity -> !((ViewWrapper<Orientation2EduPlanVersion>) entity).getEntity().isActual()));
        dataSource.addColumn(new SimpleColumn("Направленность", Orientation2EduPlanVersion.orientation().educationLevelHighSchool().title()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", Orientation2EduPlanVersion.eduPlanVersion().eduPlan().programKind().title()).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("УП", Orientation2EduPlanVersion.eduPlanVersion().title()).setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }
            @Override
            public Object getParameters(IEntity entity) {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, ((ViewWrapper<Orientation2EduPlanVersion>) entity).getEntity().getEduPlanVersion().getId());
            }
        }).setClickable(true).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Orientation2EduPlanVersion.course().title()).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Аннотация", Orientation2EduPlanVersion.P_HAS_ANNOTATION).setClickable(false));

        HeadColumn placesCountHeadColumn = new HeadColumn("placesCount", "Количество мест");
        placesCountHeadColumn.addColumn(new SimpleColumn("Минимум", Orientation2EduPlanVersion.minCount()).setOrderable(false)).setClickable(false);
        placesCountHeadColumn.addColumn(new SimpleColumn("Записано", "recordCount")).setWidth("15").setAlign("center").setClickable(false).setOrderable(false);
        dataSource.addColumn(placesCountHeadColumn.setHeaderAlign("center").setRequired(true).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Запись до", Orientation2EduPlanVersion.date()).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new BooleanColumn("Актуальность", Orientation2EduPlanVersion.actual()).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSec().getPermission("edit_orientation_list")));

        model.setDataSource(dataSource);
    }

    public void onClickAllowEntry(IBusinessComponent component) {
        getDao().allowEntry(component.getListenerParameter());
    }

    public void onClickEdit(IBusinessComponent component) {
        component.createDefaultChildRegion(
                new ComponentActivator(ru.tandemservice.unitutor.component.awp.OrientationEdit.Controller.class.getPackage().getName(),
                                       new ParametersMap().add("entityId", component.getListenerParameter())));
    }

    public void onClickActual(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().actualizeRows(model);
        onClickSearch(component);
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = getModel(component);
        component.saveSettings();
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.getSettings().clear();
        getDao().prepare(model);
        onClickSearch(component);
    }

    public void onClickEditDateAndPlaces(IBusinessComponent component) {
        Model model = getModel(component);
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList.isEmpty())
            throw new ApplicationException("Выберите записи для редактирования.");

        Collection<Long> entityIds = entityList.stream().map(IEntity::getId).collect(Collectors.toList());

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                ru.tandemservice.unitutor.component.awp.OrientationList.DateAndPlacesEdit.Controller.class.getPackage().getName(),
                new ParametersMap().add("entityIds", entityIds)));
        ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects().clear();
    }

}
