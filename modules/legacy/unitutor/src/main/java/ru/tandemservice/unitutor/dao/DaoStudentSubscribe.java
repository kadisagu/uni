package ru.tandemservice.unitutor.dao;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.gen.EppRegistryElementPartGen;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.unitutor.entity.*;
import ru.tandemservice.unitutor.entity.catalog.ElectiveDisciplineType;
import ru.tandemservice.unitutor.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.entity.gen.ElectiveDisciplineGen;
import ru.tandemservice.unitutor.mailprint.OrientationSendMail;
import ru.tandemservice.unitutor.mailprint.SubcribeBySakai;
import ru.tandemservice.unitutor.mailprint.SubscribeTutor;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;
import ru.tandemservice.unitutor.utils.DqlStudentSubscribeBulder;
import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DaoStudentSubscribe extends UniBaseDao implements IDaoStudentSubscribe {

    /**
     * Константа кода(code) элемента : Общеуниверситетский курс по выбору(title)
     */
    public static String OBTSHEUNIVERSITETSKIY_KURS_PO_VYBORU = EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_KURS_PO_VYBORU;
    /**
     * Константа кода(code) элемента : Общеуниверситетский факультатив(title)
     */
    public static String OBTSHEUNIVERSITETSKIY_FAKULTATIV = EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_FAKULTATIV;

    public static IDaoStudentSubscribe daoStudentSubscribe;

    public static IDaoStudentSubscribe instanse() {

        if (daoStudentSubscribe == null)
            daoStudentSubscribe = (IDaoStudentSubscribe) ApplicationRuntime.getBean(IDaoStudentSubscribe.class.getName());
        return daoStudentSubscribe;
    }


    /**
     * Список ДПВ
     * @return
     */
    @Override
    public List<DisciplineWrap> getDisciplineWrapList(EducationYear educationYear, YearDistributionPart yearDistributionPart, Student student,
                                              StringBuilder warning, boolean generateWarningRow, boolean generateRowIndex, String disciplineTypeCode) {
        if (warning == null)
            warning = new StringBuilder();

        Map<Long, String> mapGroup = new HashMap<>();

        Map<Long, String> mapRowIndex = new HashMap<>();

        if (student == null)
            return new ArrayList<>();

        // сначала все группы дисциплин, к которым имеет отношение студент
        // возможна ситуация - дисциплина в текушем РП, а в группе дисциплин другой версии УП
        DQLSelectBuilder dql = DqlStudentSubscribeBulder.getDqlStudentEppWorkPlanRegistryElementRow(educationYear, yearDistributionPart, student);
        List<EppWorkPlanRegistryElementRow> lst = getList(dql);
        Map<EppRegistryElementPart, DisciplineWrap> mapElectiveDiscipline = new HashMap<>();
        ElectiveDisciplineType disciplineType = getByCode(ElectiveDisciplineType.class, disciplineTypeCode);

        for (EppWorkPlanRegistryElementRow rRow : lst) {
            YearDistributionPart part = yearDistributionPart != null ? yearDistributionPart : rRow.getWorkPlan().getGridTerm().getPart();

            Term term = rRow.getWorkPlan().getGridTerm().getTerm();
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ElectiveDiscipline.class, "el")
                    .where(eq(property("el", ElectiveDiscipline.registryElementPart()), value(rRow.getRegistryElementPart())))
                    .where(eq(property("el", ElectiveDiscipline.educationYear()), value(educationYear)))
                    .where(eq(property("el", ElectiveDiscipline.yearPart()), value(part)))
                    .where(eq(property("el", ElectiveDiscipline.actualRow()), value(true)))
                    .where(eq(property("el", ElectiveDiscipline.type()), value(disciplineType)));

            ElectiveDiscipline ed = builder.createStatement(getSession()).uniqueResult();

            DisciplineWrap wrap = new DisciplineWrap(rRow, ed, term.getIntValue(), "");
            if (!mapElectiveDiscipline.containsKey(rRow.getRegistryElementPart()))
                mapElectiveDiscipline.put(rRow.getRegistryElementPart(), wrap);
        }

        // заполняем - на что подписан
        DQLSelectBuilder dqlSubscribed = DqlStudentSubscribeBulder.getDqlStudentSubscribe(educationYear, yearDistributionPart, student, disciplineTypeCode);
        dqlSubscribed.column(DqlStudentSubscribeBulder.ELECIVE_DISC_SUBSCRIBE_ALIAS);

        //на что в данный момент подписан (ДПВ)
        List<ElectiveDisciplineSubscribe> lstSubscribed = getList(dqlSubscribed);

        for (ElectiveDisciplineSubscribe es : lstSubscribed) {
            EppRegistryElementPart rPart = es.getElectiveDiscipline().getRegistryElementPart();
            DisciplineWrap wrap = mapElectiveDiscipline.get(rPart);
            //если существует подписка на дисциплину и дисциплина соответствует РУП
            if (wrap != null) {
                wrap.setElectiveDisciplineSubscribeId(es.getId());
                wrap.setSourceCode(es.getSourceCode());
            } else {
                // не найдено - это ошибочная ситуация, нужен варнинг
                String msg = "Есть подписка на дисциплину " + es.getElectiveDiscipline().getRegistryElementPart().getTitleWithNumber()
                        + " Однако, это дисциплина не актуальна для УП/РП";
                warning.append(msg);

                // нужно для сакая - показать, что запись левая
                if (generateWarningRow) {

                    int termNumber = es.getElectiveDiscipline().getRegistryElementPart().getNumber();
                    DisciplineWrap wrapWrong = new DisciplineWrap(null, es.getElectiveDiscipline(), termNumber, "");
                    wrapWrong.setEppRegistryElementPart(es.getElectiveDiscipline().getRegistryElementPart());
                    wrapWrong.setIncorrectRow(true);
                    wrapWrong.setWarningRow(msg);
                    wrapWrong.setElectiveDisciplineSubscribeId(es.getId());
                    mapElectiveDiscipline.put(es.getElectiveDiscipline().getRegistryElementPart(), wrapWrong);
                }
            }
        }

        // заполняем по группам дисциплин
        DQLSelectBuilder dqlGroup = DqlStudentSubscribeBulder.getDqlStudentEppEpvGroupImRow(educationYear, yearDistributionPart, student);
        List<EppEpvRegistryRow> lstGrp = getList(dqlGroup);

        for (EppEpvRegistryRow rRow : lstGrp)
        {
            if (rRow.getParent() != null)
            {
                Long rowId = rRow.getId();
                if (!mapRowIndex.containsKey(rowId) && generateRowIndex)
                {
                    String rowIndex = getRowIndex(rRow);
                    mapRowIndex.put(rowId, rowIndex);
                }

                Long parentId = rRow.getParent().getId();

                EppEpvGroupImRow group = null;
                if (rRow.getParent() instanceof EppEpvGroupImRow)
                    group = (EppEpvGroupImRow) rRow.getParent();

                if (group != null)
                {
                    String parentName = rRow.getParent().getTitle();

                    String rowIndex;
                    if (mapGroup.containsKey(group.getId()))
                        rowIndex = mapGroup.get(group.getId());
                    else
                    {
                        rowIndex = getRowIndex(group);
                        mapGroup.put(group.getId(), rowIndex);
                    }
                    if (rowIndex != null)
                        parentName += " (" + rowIndex + ")";

                    EppRegistryElement regElem = rRow.getRegistryElement();
                    List<DisciplineWrap> lstWrap = getDisciplineWrap(regElem, mapElectiveDiscipline);

                    for (DisciplineWrap wrap : lstWrap)
                    {
                        wrap.setParentId(parentId);
                        wrap.setParentName(parentName);
                        wrap.setCountInGroup(group.getSize()); // кол-во дисциплин в группе

                        // допишем индекс строки
                        if (mapRowIndex.containsKey(rowId))
                            wrap.setRowIndex(mapRowIndex.get(rowId));

                    }

                    if (lstWrap.isEmpty())
                    {
                        //нужен варнинг
                        // нельзя определить группу дисциплин
                        throw new ApplicationException("Нельзя определить группу дисциплин");

                    }
                } else {
                    // варнинг нужен
                    // нельзя определить группу дисциплин
                    throw new ApplicationException("Нельзя определить группу дисциплин");
                }
            }
        }

        // на удаление
        List<DisciplineWrap> remove = new ArrayList<>();

        //Список дисциплин, для которых указан язык
        DQLSelectBuilder builder = DqlDisciplineBuilder.getEppRegistryElementExt(mapElectiveDiscipline.keySet().stream().map(e -> e.getRegistryElement().getId()).collect(Collectors.toList()));
        builder.column(property("disExt", EppRegistryElementExt.eppRegistryElement()));
        List<EppRegistryElement> extList = getList(builder);

        // Список групп дисциплин, которые нужно скрыть
        Set<Long> groupsRemove = new HashSet<>();

        for (DisciplineWrap wrap : mapElectiveDiscipline.values())
        {
            if (wrap.getParentId() == null)
            {
                // это очень плохо
                if (wrap.getElectiveDisciplineSubscribeId() == null)
                {
                    // подписки нет, нафиг такое
                    remove.add(wrap);
                }
                else
                {
                    wrap.setIncorrectRow(true);
                    String msg = " для " + wrap.getEppRegistryElementPart().getTitleWithNumber() + " нельзя определить группу дисциплин ";
                    warning.append(msg);

                    wrap.addWarningRow(msg);
                }
            }

            //Убираем группы дисциплин, для которых есть указание языка
            if (extList.contains(wrap.getEppRegistryElementPart().getRegistryElement())
                    || groupsRemove.contains(wrap.getParentId()))
            {
                remove.add(wrap);
                groupsRemove.add(wrap.getParentId());
            }

            if (wrap.getElectiveDiscipline() == null)
            {
                if (wrap.getElectiveDisciplineSubscribeId() == null)
                    // подписки нет, нафиг такое
                    remove.add(wrap);
                else
                {
                    wrap.setIncorrectRow(true);
                    String msg = " для " + wrap.getEppRegistryElementPart().getTitleWithNumber() + " нельзя определить ДПВ (Список ДПВ не актуален)";
                    warning.append(msg);
                    wrap.addWarningRow(msg);
                }
            }
        }

        mapElectiveDiscipline.values().removeAll(remove);
        List<DisciplineWrap> retVal = new ArrayList<>(mapElectiveDiscipline.values());
        // отсортировать

        retVal.sort(Comparator.comparing(DisciplineWrap::getTerm).thenComparing(DisciplineWrap::getTitle));
        return retVal;

    }

    /**
     * Список УДВ или УФ
     */
    @Override
    public List<ElectiveDiscipline> getElectiveFromRegistryList(@NotNull EducationYear educationYear, @NotNull YearDistributionPart yearDistributionPart,
             Student student, String electiveDisciplineTypeCode, StringBuilder warning)
    {

        if (warning == null)
            warning = new StringBuilder();

        List<ElectiveDiscipline> retVal = new ArrayList<>();
        if (student == null)
            return retVal;

        boolean containsDiscipline = getCountElectiveInWorkPlan(educationYear, yearDistributionPart, student, electiveDisciplineTypeCode)>0;


        // Если найдена дисциплина с типом УДВ(УФ) - тогда будем брать значения для соответсвующего года и его части из реестра УДВ(УФ)
        if (containsDiscipline)
        {
                Integer nextCourse = student.getCourse().getIntValue() + 1;

                //определим id УГН
                Long groupId = null;
                EduProgramSubject subject = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
                if (subject != null)
                {
                    if (subject instanceof EduProgramSubject2009)
                        groupId = ((EduProgramSubject2009) subject).getGroup().getId();
                    else if (subject instanceof EduProgramSubject2013)
                        groupId = ((EduProgramSubject2013) subject).getGroup().getId();
                    else if (subject instanceof EduProgramSubjectOkso)
                        groupId = ((EduProgramSubjectOkso) subject).getItem().getGroup().getId();
                }


                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(ElectiveDiscipline.class, "ed")
                        .column("ed");

                if (educationYear != null)
                    builder.where(eqValue(property(ElectiveDiscipline.educationYear().fromAlias("ed")), educationYear));

                if (yearDistributionPart != null)
                    builder.where(eqValue(property(ElectiveDiscipline.yearPart().fromAlias("ed")), yearDistributionPart));

                if (StringUtils.isNotEmpty(electiveDisciplineTypeCode))
                    builder.where(eqValue(property(ElectiveDiscipline.type().code().fromAlias("ed")), electiveDisciplineTypeCode));

                // если при добавлении УДВ и УФ курс не задан, то показывать эти дисциплины для всех студентов.
                // если же указан курс, то только для студентов, у которых этот курс будет следующим RM #6870
                builder.joinEntity("ed", DQLJoinType.left, ElectiveDiscipline2Course.class, "ed2c",
                                   eq(property(ElectiveDiscipline2Course.electiveDiscipline().id().fromAlias("ed2c")),
                                      property(ElectiveDiscipline.id().fromAlias("ed"))));

                builder.joinPath(DQLJoinType.left, ElectiveDiscipline2Course.course().fromAlias("ed2c"), "c")
                        .where(or(isNull("c"),
                                  and(isNotNull("c"), eqValue(property(Course.intValue().fromAlias("c")), nextCourse))));

                if (groupId != null)
                {
                    builder.joinEntity("ed", DQLJoinType.left, ElectiveDiscipline2EduGroup.class, "ed2gr",
                                       eq(property(ElectiveDiscipline2EduGroup.electiveDiscipline().id().fromAlias("ed2gr")),
                                          property(ElectiveDiscipline.id().fromAlias("ed"))));

                    builder.where(or(isNull("ed2gr"),
                                     and(isNotNull("ed2gr"),
                                         eqValue(property(ElectiveDiscipline2EduGroup.groupId().fromAlias("ed2gr")), groupId))));
                }


                retVal.addAll(getList(builder));
        }
        else
            warning.append("Согласно выбранным критериям для студента не найдены дисциплины для записи");

        return retVal;
    }

//    @Override
//    public boolean containElectiveInWorkPlan(EducationYear educationYear, YearDistributionPart yearDistributionPart,
//                                             Student student, String electiveDisciplineTypeCode) {
//        return getCountElectiveInWorkPlan(educationYear, yearDistributionPart, student, electiveDisciplineTypeCode) > 0;
//    }


    /**
     * число дисциплин по выбору (УФ и УДВ) в РУПах студентов
     */
    @Override
    public int getCountElectiveInWorkPlan(EducationYear educationYear,YearDistributionPart yearDistributionPart,
                                          Student student, String electiveDisciplineTypeCode)
    {
        //id активных рабочих планов студента (не версий)
        DQLSelectBuilder dqlInWorkPlanBase = DqlDisciplineBuilder.getDqlInEppStudent2WorkPlan(educationYear, yearDistributionPart, null, null, null, null, Collections.singletonList(student));

        dqlInWorkPlanBase.joinEntity(DqlDisciplineBuilder.WORK_PLAN_BASE_ALIAS, DQLJoinType.left, EppWorkPlanVersion.class, "v",
                                     eq(property(DqlDisciplineBuilder.WORK_PLAN_BASE_ALIAS, EppWorkPlanBase.id()), property("v", EppWorkPlanVersion.id())))
                .joinEntity(DqlDisciplineBuilder.WORK_PLAN_BASE_ALIAS, DQLJoinType.left, EppWorkPlan.class, "wp", eq(property(DqlDisciplineBuilder.WORK_PLAN_BASE_ALIAS, EppWorkPlanBase.id()), property("wp", EppWorkPlan.id())))
                .joinEntity("v", DQLJoinType.left, EppWorkPlan.class, "wpp", eq(property("v", EppWorkPlanVersion.parent().id()), property("wpp", EppWorkPlan.id())));

        dqlInWorkPlanBase.column(DQLFunctions.coalesce(property("wp", EppWorkPlan.id()), property("wpp", EppWorkPlan.id())))
                .distinct();
        List<Long> idsWorkPlan = getList(dqlInWorkPlanBase);

        //Строки РУП
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS)
                        // не исключенные строки нагрузки
                .where(isNull(property(EppWorkPlanRegistryElementRow.needRetake().fromAlias(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS))));
                        // по рабочим планам

        //TODO FIX ME так делать плохо
        // увеличим скорость работы
        if (idsWorkPlan.size() < 2000)
            dql.where(in(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS)), idsWorkPlan));
        else
            dql.where(in(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS)), dqlInWorkPlanBase.buildQuery()));

        String registryStructureCode = "";

        if (ElectiveDisciplineTypeCodes.UDV.equals(electiveDisciplineTypeCode))
            registryStructureCode = OBTSHEUNIVERSITETSKIY_KURS_PO_VYBORU;
        else if (ElectiveDisciplineTypeCodes.UF.equals(electiveDisciplineTypeCode))
            registryStructureCode = OBTSHEUNIVERSITETSKIY_FAKULTATIV;

        // только УПВ или УФ
        dql.where(eqValue(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().parent().code().fromAlias(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS)),
                          registryStructureCode));
        dql.column(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS);

        return getCount(dql);
    }

    /**
     * Получим группу дисциплин (ДПВ)
     */
    private EppEpvGroupImRow getDisciplineGroup(Student student, EducationYear educationYear, YearDistributionPart yearDistributionPart, EppRegistryElementPart registryElementPart) {

        // нужные версии УП
        DQLSelectBuilder dqlInEduPlanVersion = DqlDisciplineBuilder.getDqlInEppStudent2WorkPlan(educationYear, yearDistributionPart, null, null, null, null, Lists.newArrayList(student));
        dqlInEduPlanVersion.column(property(DqlDisciplineBuilder.STUD_EDU_PLAN_VERSION, EppStudent2EduPlanVersion.eduPlanVersion().id()))
                .distinct();
        List<Long> versionIds = getList(dqlInEduPlanVersion);

        // должно обязательно входить в группу
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "EppEpvRegistryRow")

                        // джойн до групп дисциплин
                .joinEntity("EppEpvRegistryRow", DQLJoinType.inner, EppEpvGroupImRow.class, "EppEpvGroupImRowIN",
                            eq(property(EppEpvRegistryRow.parent().id().fromAlias("EppEpvRegistryRow")), property(EppEpvGroupImRow.id().fromAlias("EppEpvGroupImRowIN"))))
                        // версия УП (для конкретного студента версий не м.б. больше 2000, поэтому берем список)
                .where(in(property(EppEpvRegistryRow.owner().eduPlanVersion().id().fromAlias("EppEpvRegistryRow")), versionIds))

                        // дисциплина, входящая в группу
                .where(eq(property(EppEpvRegistryRow.registryElement().id().fromAlias("EppEpvRegistryRow")), value(registryElementPart.getRegistryElement().getId())))
                .column("EppEpvGroupImRowIN");

        List<EppEpvGroupImRow> lst = getList(dql);

        if (lst == null || lst.isEmpty())
            throw new ApplicationException("Для дисциплины " + registryElementPart.getTitle() + " не найдена группа ");

        return lst.get(0);
    }

    @Override
    public String getRowIndex(EppEpvRow row)
    {
        // важно - мап перенести, учитывать, что по одной и той-же версии можем работать
        final IEppEpvRowWrapper rowIndex = IEppEduPlanVersionDataDAO.instance.get().getEpvRowWrapper(row, true);
        return rowIndex.getIndex();
    }

    private List<DisciplineWrap> getDisciplineWrap(EppRegistryElement regElem, Map<EppRegistryElementPart, DisciplineWrap> mapDisciplines)
    {
        List<DisciplineWrap> retVal = new ArrayList<>();

        for (Map.Entry<EppRegistryElementPart, DisciplineWrap> entry : mapDisciplines.entrySet()) {
            EppRegistryElementPart key = entry.getKey();
            if (key.getRegistryElement().equals(regElem))
                retVal.add(entry.getValue());
        }
        return retVal;
    }

    public static final String MYTEX = "MYTEX";
    public static final String UNSUBSCRIBE_MUTEX = "UNSUBSCRIBE_MUTEX";
    public static final String ELECTIVE_MUTEX = "ELECTIVE_MUTEX";


    /**
     * Сохраняет запись студента на УДВ и УФ
     */
    @Override
    public String saveElectiveSubscribe(Student student, List<ElectiveDisciplineSubscribe> listSubscribes, boolean fromSakai)
    {
        synchronized (ELECTIVE_MUTEX) {
            Long transactionId = getTransactionId();
            String source = fromSakai ? "Личный кабинет" : OptionalDisciplineUtil.getCurrentUser();
            List<String> subscribeTo = new ArrayList<>();

            for (ElectiveDisciplineSubscribe subscribe : listSubscribes)
            {
                validateDate(subscribe.getElectiveDiscipline(), true);
                validateCount(subscribe.getElectiveDiscipline(), true);
                subscribe.setRemovalDate(null);
                subscribe.setTransactionId(transactionId);
                subscribe.setSource(source);
                saveOrUpdate(subscribe);
                subscribeTo.add(subscribe.getElectiveDiscipline().getRegistryElementPart().getRegistryElement().getTitle());
            }

            getSession().flush();

            StringBuilder sb = new StringBuilder("");

            if (!subscribeTo.isEmpty())
            {
                sb.append("<p> Вы записались на следующие дисциплины:</p>");
                sb.append("<ul>");
                for (String title : subscribeTo)
                    sb.append("<li>").append(title).append("</li>");

                sb.append("</ul>");
            }

            return sb.toString();
        }
    }

    @Override
    public String unsubscribeElectiveDisciplines(Student student, List<ElectiveDisciplineSubscribe> subscribes, boolean fromSakai)
    {
        synchronized (UNSUBSCRIBE_MUTEX) {
            Long transactionId = getTransactionId();
            List<String> unsubscribeFrom = new ArrayList<>();

            for (ElectiveDisciplineSubscribe subscribe : subscribes)
                unsubscribeFrom.add(usubscribe(subscribe, null, true, transactionId, fromSakai));

            StringBuilder sb = new StringBuilder();

            if (!unsubscribeFrom.isEmpty())
            {
                sb.append("<p>Вы отписались со следующих дисциплин:</p>");
                sb.append("<ul>");
                for (String title : unsubscribeFrom)
                    sb.append("<li>").append(title).append("</li>");
                sb.append("</ul>");
                sb.append("<br>");
            }
            return sb.toString();

        }
    }

    @Override
    @Transactional
    public String saveStudentSubscribe(Student student, EducationYear educationYear, List<Long> idsElectiveDiscipline, String userName, boolean sendMail, boolean checkCountAndDate)
    {
        synchronized (MYTEX)
        {
            Long transactionId = getTransactionId();

            // прежде всего по годам и частям
            Map<EducationYear, List<YearDistributionPart>> mapYPart = new HashMap<>();

            Map<Long, List<ElectiveDiscipline>> mapED = new HashMap<>();

            for (Long id : idsElectiveDiscipline)
            {
                ElectiveDiscipline ed = get(ElectiveDiscipline.class, id);
                if (ed == null)
                    throw new ApplicationException("Не найдена 'Дисциплина по выбору (ДПВ)' для id=" + id);

                EducationYear year = ed.getEducationYear();
                YearDistributionPart part = ed.getYearPart();

                Long keyED = year.getId() + part.getId();
                List<ElectiveDiscipline> lstEd = new ArrayList<>();
                if (mapED.containsKey(keyED))
                    lstEd = mapED.get(keyED);
                else
                    mapED.put(keyED, lstEd);

                if (!lstEd.contains(ed))
                    lstEd.add(ed);

                List<YearDistributionPart> lst = new ArrayList<>();
                if (mapYPart.containsKey(year))
                    lst = mapYPart.get(year);
                else
                    mapYPart.put(year, lst);

                if (!lst.contains(part))
                    lst.add(part);
            }

            // части года по уже записанным дисциплинам (в общий мар)
            List<ElectiveDisciplineSubscribe> edsList = getSubscribedElectiveDiscipline(Lists.newArrayList(student), educationYear, null, false, null);
            for (ElectiveDisciplineSubscribe eds : edsList)
            {
                ElectiveDiscipline ed = eds.getElectiveDiscipline();
                EducationYear year = ed.getEducationYear();
                YearDistributionPart part = ed.getYearPart();

                List<YearDistributionPart> lst = new ArrayList<>();
                if (mapYPart.containsKey(year))
                    lst = mapYPart.get(year);
                else
                    mapYPart.put(year, lst);

                if (!lst.contains(part))
                    lst.add(part);
            }

            String retVal = "";
            // по годам и частям
            for (EducationYear year : mapYPart.keySet())
            {
                List<YearDistributionPart> parts = mapYPart.get(year);
                List<ElectiveDiscipline> disciplines = new ArrayList<>();
                for (YearDistributionPart part : parts)
                {
                    Long keyED = year.getId() + part.getId();
                    disciplines.addAll(mapED.get(keyED));
                }
                    retVal += _saveStudentSubscribeByYearAndPart(student, disciplines, year, null, userName, sendMail, checkCountAndDate, transactionId);

            }

            if (sendMail && retVal.length() > 0)
                // что то было
                sendMail(student, educationYear, transactionId, true);

            return StringUtils.isEmpty(retVal)? "Данные не были изменены." : retVal;
        }
    }

    @Override
    public void sendMail(Student student, EducationYear educationYear, Long transactionId, boolean fromSakai)
    {
        // формируем через БИНЫ
        // Для режима отладки формируем и так и так
        if (fromSakai)
            SubcribeBySakai.Instanse().makeMailMessage(student, transactionId);
        else
            SubscribeTutor.Instanse().makeMailMessage(student, transactionId);

    }

    /**
     *  Подписываем студента на ДПВ
     */
    private String _saveStudentSubscribeByYearAndPart(Student student, List<ElectiveDiscipline> subscribedTo, EducationYear year, YearDistributionPart part,
                    String userName, boolean sendMail, boolean checkCountAndDate, Long transactionId)
    {
        String retVal = "";
        if (subscribedTo == null)
            subscribedTo = new ArrayList<>();

        List<String> subcsibeToLog = new ArrayList<>();
        List<String> unsubscribeToLog = new ArrayList<>();

        // начальное состояние (на что подписан сейчас)
        List<ElectiveDisciplineSubscribe> subscribeNow = getSubscribedElectiveDiscipline(Lists.newArrayList(student), year, part, false, null);

        List<ElectiveDisciplineSubscribe> unsubscribe = new ArrayList<>();

        for (ElectiveDisciplineSubscribe eds : subscribeNow)
        {
            ElectiveDiscipline disc = eds.getElectiveDiscipline();
                if (subscribedTo.contains(disc))
                    // на такое уже подписаны, повторно не подписываем
                    subscribedTo.remove(disc);
                else
                    // такое никто не просил - отписать
                    if (!unsubscribe.contains(eds))
                        unsubscribe.add(eds);
        }

        // сначала отписываем
        for (ElectiveDisciplineSubscribe eds : unsubscribe)
        {
            String strUnsubscribe = usubscribe(eds, null, checkCountAndDate, transactionId, false);
            unsubscribeToLog.add(strUnsubscribe);
        }

        // теперь подписываем
        for (ElectiveDiscipline ed : subscribedTo)
        {
            String rezult = saveStudentSubscribe(student, ed, userName, null, sendMail, checkCountAndDate, transactionId);
            // Для лога
            subcsibeToLog.add(rezult);
        }


        // формируем лог
        if (!subcsibeToLog.isEmpty() || !unsubscribeToLog.isEmpty())
        {
            StringBuilder sb = new StringBuilder();
            sb.append("<p> Учебный год:").append(year.getTitle())
//                    .append(" часть: ").append(part.getTitle())
                    .append("</p>");

            if (!subcsibeToLog.isEmpty())
            {
                sb.append("<p> Вы записались на следующие дисциплины:</p>");
                sb.append("<ul>");
                for (String title : subcsibeToLog)
                    sb.append("<li>").append(title).append("</li>");
                sb.append("</ul>");
            }

            if (!unsubscribeToLog.isEmpty())
            {
                sb.append("<p>Вы отписались со следующих дисциплин:</p>");
                sb.append("<ul>");
                for (String title : unsubscribeToLog)
                    sb.append("<li>").append(title).append("</li>");
                sb.append("</ul>");
            }

            sb.append("<br>");

            retVal = sb.toString();
        }
        return retVal;
    }

    @Override
    public String usubscribe(ElectiveDisciplineSubscribe electiveDisciplineSubscribe, String comment, boolean checkCountAndDate, Long transactionId, boolean fromSakai)
    {
        validateDate(electiveDisciplineSubscribe.getElectiveDiscipline(), checkCountAndDate);

        String retVal = electiveDisciplineSubscribe.getElectiveDiscipline().getRegistryElementPart().getTitle();
        // отписать такое
        electiveDisciplineSubscribe.setRemovalDate(new Date());
        electiveDisciplineSubscribe.setTransactionId(transactionId);

        if (fromSakai)
            electiveDisciplineSubscribe.setSource("Личный кабинет");
        else if (!StringUtils.isEmpty(comment))
        {
            String commentNew = electiveDisciplineSubscribe.getSource();
            if (commentNew == null)
                commentNew = "";
            commentNew += " " + comment;

            electiveDisciplineSubscribe.setSource(commentNew);
         }

        saveOrUpdate(electiveDisciplineSubscribe);
        getSession().flush();
        return retVal;
    }

    private void validateDate(ElectiveDiscipline electiveDiscipline, boolean checkCountAndDate)
    {
        if (checkCountAndDate && electiveDiscipline.getSubscribeToDate() != null)
        {
            Date check = new Date();
            if (check.after(electiveDiscipline.getSubscribeToDate()))
                throw new ApplicationException("Запись на дисциплину " + electiveDiscipline.getRegistryElementPart().getTitle() + " завершена ");
        }
    }

    @Override
    public String saveStudentSubscribe(Student student, ElectiveDiscipline electiveDiscipline, String userName, String comment, boolean sendMail, boolean checkCountAndDate, Long transactionId)
    {
        String retVal = electiveDiscipline.getRegistryElementPart().getTitle();

        String user = userName;
        if (user == null)
            user = OptionalDisciplineUtil.getCurrentUser();

        if (!StringUtils.isEmpty(comment) && !user.contains(comment))
                user += " " + comment;



        // именно тут и валидируем!!!
        // в случаи ошибки взвести исключение
        validateBeforeSave(student, electiveDiscipline.getEducationYear(), electiveDiscipline.getYearPart(), electiveDiscipline, checkCountAndDate);

        ElectiveDisciplineSubscribe subscribe = new DQLSelectBuilder().fromEntity(ElectiveDisciplineSubscribe.class, "el")
                .where(eq(property("el", ElectiveDisciplineSubscribe.student()), value(student)))
                .where(eq(property("el", ElectiveDisciplineSubscribe.electiveDiscipline()), value(electiveDiscipline)))
                .where(isNotNull(property("el", ElectiveDisciplineSubscribe.removalDate())))
                .order(property("el", ElectiveDisciplineSubscribe.subscribeOn()), OrderDirection.desc)
                .top(1)
                .createStatement(getSession())
                .uniqueResult();

        if (subscribe != null)
        {
            subscribe.setRemovalDate(null);
            subscribe.setTransactionId(transactionId);
            subscribe.setSource(user);
            subscribe.setSubscribeOn(Calendar.getInstance().getTime());
        }
        else
            subscribe = new ElectiveDisciplineSubscribe(electiveDiscipline, student, Calendar.getInstance().getTime(), user, transactionId);

        subscribe.setSendMail(sendMail);
        saveOrUpdate(subscribe);

        return retVal;
    }


    private void validateBeforeSave(Student student, EducationYear year, YearDistributionPart part, ElectiveDiscipline ed, boolean checkCountAndDate)
    {

        // проверяем на дату записи
        validateDate(ed, checkCountAndDate);

        validateCount(ed, checkCountAndDate);

        // проверяем на:
        // 1 - количество дисциплин в группе
        // 2 - в одной группе можно выбрать только одну дисциплину за все время
        // иначе: если дисциплина читается в нескольких семестрах, из этой группы всегда нужно выбирать
        // одно и то-же

        // получим группу дисциплин
        EppEpvGroupImRow group = getDisciplineGroup(student, year, part, ed.getRegistryElementPart());

        Long idCheck = ed.getRegistryElementPart().getRegistryElement().getId();

        // на что из группы за все годы были подписки
        // все что было ранее, должно совпадать с eppRegistryElement
        DQLSelectBuilder dql = DqlStudentSubscribeBulder.getDqlElectiveDisciplineSubscribe(null, null, student, group);
        List<Long> lstCheck = getList(dql);

        // проверяем только, еcли в группе 1 ДПВ
        if (lstCheck != null && group.getSize() == 1)
        {
            for (Long registryElementId : lstCheck)
            {
                if (!registryElementId.equals(idCheck))
                {
                    EppRegistryElement er = get(EppRegistryElement.class, registryElementId);
                    throw new ApplicationException("Дисциплину " + ed.getRegistryElementPart().getTitle() + " выбрать нельзя, вами ранее из группы дисциплин уже была выбрана " + er.getTitle());
                }
            }
        }

        // проверка на количество (в рамках части года)
        DQLSelectBuilder dqlCheckCount = DqlStudentSubscribeBulder.getDqlElectiveDisciplineSubscribe(year, part, student, group);
        List<Long> lstCheckCount = getList(dqlCheckCount);

        int size = 0;
        if (lstCheckCount != null && !lstCheckCount.isEmpty())
            size = lstCheckCount.size();

        if (size >= group.getSize())
            throw new ApplicationException("Из группы дисциплин можно выбрать не более " + group.getSize());

    }

    private void validateCount(ElectiveDiscipline ed, boolean checkCountAndDate)
    {
        if (checkCountAndDate && ed.getPlacesCount() != null && ed.getPlacesCount() > 0)
            if (ed.getFreePlacesToSubscribe() < 1)
                throw new ApplicationException("Для дисциплины " + ed.getRegistryElementPart().getTitle() + " нет свободных мест");
    }

    @Override
    public List<ElectiveDisciplineSubscribe> getSubscribedElectiveDiscipline(List<Student> student, EducationYear year, YearDistributionPart part, boolean sort, Long transactionId)
    {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, "eds");
        //только ДПВ
        dql.where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().type().code().fromAlias("eds")), ElectiveDisciplineTypeCodes.DPV));
        // год
        if (year != null)
            dql.where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().id().fromAlias("eds")), year.getId()));

        // часть года
        if (part != null)
            dql.where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().yearPart().id().fromAlias("eds")), part.getId()));

        // студент
        dql.where(in(property(ElectiveDisciplineSubscribe.student().fromAlias("eds")), student));

        // только актуальное
        if (transactionId == null)
            dql.where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias("eds"))));
        else
            dql.where(eqValue(property(ElectiveDisciplineSubscribe.transactionId().fromAlias("eds")), transactionId));

        if (sort) {
            // по году
            dql.order(property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().intValue().fromAlias("eds")), OrderDirection.asc);
            // по части года
            dql.order(property(ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().number().fromAlias("eds")), OrderDirection.asc);
        }
        return getList(dql);
    }

    @Override
    @Transactional
    public String saveStudentSubscribeByYearAndPart(Student student, EducationYear year, YearDistributionPart part, List<ElectiveDiscipline> subscribedTo,
            String userName, boolean sendMail, boolean checkCountAndDate)
    {

        synchronized (MYTEX)
        {
            Long transactionId = getTransactionId();

            String retVal = "";
            retVal += _saveStudentSubscribeByYearAndPart(student, subscribedTo, year, part, userName, sendMail, checkCountAndDate, transactionId);

            if (sendMail && retVal.length() > 0)
                // что то было
                sendMail(student, year, transactionId, false);

            return retVal;
        }
    }

    /**
     * id транзакции для работы с ДПВ
     * Нужно для корректного формирования e-mail писем
     */
    @Override
    public Long getTransactionId()
    {
        return new Date().getTime();
    }

    @Override
    @Transactional
    public String saveStudentOrientationSubscribe(Student student, EducationOrgUnit orientation, Student2Orientation student2Orientation, String source, boolean needSendMail)
    {
        synchronized (MYTEX)
        {
            StringBuilder resultBuilder = new StringBuilder("");
            //следующий учебный год
            EducationYear educationYear = get(EducationYear.class, EducationYear.intValue(), EducationYearManager.instance().dao().getCurrent().getIntValue() + 1);

            if (orientation != null)
            {
                Student2Orientation entity = getSubscribe(student, orientation, educationYear);

                if (entity == null)
                {
                    entity = new Student2Orientation();
                    entity.setStudent(student);
                    entity.setOrientation(orientation);
                }
                entity.setEducationYear(educationYear);
                entity.setDate(new Date());
                entity.setActual(true);
                entity.setRemovalDate(null);
                if (Student2Orientation.TANDEM_SOURCE.equals(source))
                    entity.setSource(PersonSecurityUtil.getExecutor());
                else
                    entity.setSource("Личный кабинет");

                getSession().saveOrUpdate(entity);

                resultBuilder.append("Вы подписались на ").append(orientation.getEducationLevelHighSchool().getFullTitle()).append(". ");

                //отправка уведомления на e-mail
                if (needSendMail)
                    OrientationSendMail.Instanse().makeMailMessage(entity.getStudent(), entity.getOrientation());

            }

            if (student2Orientation != null)
            {
                student2Orientation.setActual(false);
                student2Orientation.setRemovalDate(new Date());
                if (Student2Orientation.TANDEM_SOURCE.equals(source))
                    student2Orientation.setSource(PersonSecurityUtil.getExecutor());
                else
                    student2Orientation.setSource("Личный кабинет");

                getSession().update(student2Orientation);

                resultBuilder.append("Вы отписались от ").append(student2Orientation.getOrientation().getEducationLevelHighSchool().getFullTitle()).append(". ");
            }

            return resultBuilder.toString();
        }
    }

    private Student2Orientation getSubscribe(Student student, EducationOrgUnit eduOrgUnit, EducationYear educationYear)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student2Orientation.class, "s2o")
                .where(eqValue(property(Student2Orientation.student().fromAlias("s2o")), student))
                .where(eqValue(property(Student2Orientation.educationYear().fromAlias("s2o")), educationYear))
                .where(eqValue(property(Student2Orientation.orientation().fromAlias("s2o")), eduOrgUnit));

        return builder.createStatement(getSession()).uniqueResult();
    }

    @Override
    public void actualizeOrientations(Long orgUnitId, EducationYear educationYear)
    {
        ChoiceOrientationSettings settings = get(ChoiceOrientationSettings.class, ChoiceOrientationSettings.educationYear(), educationYear);
        List<Long> actualIds = new ArrayList<>();

        String EDU_ORG_UNIT_ALIAS = "eduOu";
        String EDU_LEVEL_ALIAS = "lvl";
        String EDU_LHS = "eduLHS";


        DQLSelectBuilder eduOuDql = new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, EDU_ORG_UNIT_ALIAS)
                .column(property(EDU_ORG_UNIT_ALIAS))
                .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias(EDU_ORG_UNIT_ALIAS), EDU_LHS)
                .joinPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias(EDU_LHS), EDU_LEVEL_ALIAS)
                .where(isNotNull(property(EducationLevels.eduProgramSpecialization().fromAlias(EDU_LEVEL_ALIAS))));
        if (orgUnitId != null)
            eduOuDql.where(eq(property(EducationOrgUnit.formativeOrgUnit().id().fromAlias(EDU_ORG_UNIT_ALIAS)), value(orgUnitId)));

        List<EducationOrgUnit> orgUnits = getList(eduOuDql);

        for (EducationOrgUnit eduOU : orgUnits)
        {
            DQLSelectBuilder builder = getBlockVersionSelectBuilder(educationYear, eduOU);

            List<Object[]> list = getList(builder);
            for (Object[] obj : list)
            {
                Orientation2EduPlanVersion entity = createOrUpdateOrientation2EduPlanVersion(educationYear, settings, eduOU, obj);
                actualIds.add(entity.getId());
            }
        }


        //направленности, которые в данный момент неактуальны (нет блока версии УП)
        //они все будут не актуальны
        DQLSelectBuilder notActualBuilder = getNotActualBuilder(orgUnitId, educationYear, actualIds);

        List<Orientation2EduPlanVersion> notActualList = getList(notActualBuilder);

        for (Orientation2EduPlanVersion entity : notActualList)
        {
            entity.setActual(false);
            entity.setAllowEntry(false);
            //актуализируем записи студентов на данную направленность
            actualizeStudentRecords(entity);
            update(entity);
        }
    }

    private Orientation2EduPlanVersion createOrUpdateOrientation2EduPlanVersion(EducationYear educationYear, ChoiceOrientationSettings settings, EducationOrgUnit eduOU, Object[] obj)
    {
        EppEduPlanVersion version = (EppEduPlanVersion) obj[0];
        Course course = (Course) obj[1];
        Orientation2EduPlanVersion entity = (Orientation2EduPlanVersion) obj[2];

        if (settings == null && entity == null)
            throw new ApplicationException("Необходимо заполнить настройку выбора направленностей по умолчанию для " + educationYear.getTitle() + " года.");

        if (entity == null)
        {
            entity = new Orientation2EduPlanVersion();
            entity.setOrientation(eduOU);
            entity.setEduPlanVersion(version);
        }

        if (settings != null)
        {
            entity.setMinCount(settings.getCountPlaces());
            entity.setDate(settings.getDate());
        }

        entity.setEducationYear(educationYear);
        entity.setActual(true);
        entity.setCourse(course);

        saveOrUpdate(entity);
        return entity;
    }

    private DQLSelectBuilder getBlockVersionSelectBuilder(EducationYear educationYear, EducationOrgUnit eduOU)
    {
        String BLOCK_ALIAS = "block";
        String VERSION_ALIAS = "version";
        String EDU_PLAN = "eduplan";
        String PLAN_PROF_ALIAS = "planProf";
        String REL_ALIAS = "relation";
        String VERSION_EXT_ALIAS = "versionExt";

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionSpecializationBlock.class, BLOCK_ALIAS)
                .joinPath(DQLJoinType.inner, EppEduPlanVersionSpecializationBlock.eduPlanVersion().fromAlias(BLOCK_ALIAS), VERSION_ALIAS)
                .joinPath(DQLJoinType.inner, EppEduPlanVersion.eduPlan().fromAlias(VERSION_ALIAS), EDU_PLAN)
                .joinEntity(EDU_PLAN, DQLJoinType.inner, EppEduPlanProf.class, PLAN_PROF_ALIAS, eq(property(EDU_PLAN + ".id"), property(PLAN_PROF_ALIAS + ".id")))
                .joinEntity(VERSION_ALIAS, DQLJoinType.left, EppEduPlanVersionExt.class, VERSION_EXT_ALIAS,
                            eq(property(EppEduPlanVersionExt.eduPlanVersion().id().fromAlias(VERSION_EXT_ALIAS)),
                               property(EppEduPlanVersion.id().fromAlias(VERSION_ALIAS))))
                .column(property(VERSION_ALIAS))
                .column(property(EppEduPlanVersionExt.course().fromAlias(VERSION_EXT_ALIAS)), "course")

                .where(eq(property(EppEduPlan.programForm().fromAlias(EDU_PLAN)), value(eduOU.getDevelopForm().getProgramForm())))
                .where(eq(property(EppEduPlan.developCondition().fromAlias(EDU_PLAN)), value(eduOU.getDevelopCondition())))
                .where(eq(property(EppEduPlan.developGrid().developPeriod().fromAlias(EDU_PLAN)), value(eduOU.getDevelopPeriod())))
                .where(eq(property(EppEduPlanProf.programSubject().fromAlias(PLAN_PROF_ALIAS)), value(eduOU.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject())))
                .where(ne(property(EppEduPlan.state().code().fromAlias(EDU_PLAN)), "5")) //УП не в архиве
                .where(ne(property(EppEduPlanVersion.state().code().fromAlias(VERSION_ALIAS)), "5")) //УПв не в архиве
                        //совпадает специализация
                .where(eq(property(EppEduPlanVersionSpecializationBlock.programSpecialization().fromAlias(BLOCK_ALIAS)), value(eduOU.getEducationLevelHighSchool().getEducationLevel().getEduProgramSpecialization())))
                .joinEntity(VERSION_ALIAS, DQLJoinType.left, Orientation2EduPlanVersion.class, REL_ALIAS,
                            and(eq(property(EppEduPlanVersion.id().fromAlias(VERSION_ALIAS)), property(Orientation2EduPlanVersion.eduPlanVersion().id().fromAlias(REL_ALIAS))),
                                eq(property(Orientation2EduPlanVersion.educationYear().fromAlias(REL_ALIAS)), value(educationYear)),
                                eq(property(Orientation2EduPlanVersion.orientation().fromAlias(REL_ALIAS)), value(eduOU))))

                .column(property(REL_ALIAS));

        if (eduOU.getDevelopTech().getProgramTrait() != null)
            builder.where(eq(property(EppEduPlan.programTrait()), value(eduOU.getDevelopTech().getProgramTrait())));
        return builder;
    }

    private void actualizeStudentRecords(Orientation2EduPlanVersion rel)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student2Orientation.class, "r")
                .joinEntity("r", DQLJoinType.inner, EppStudent2EduPlanVersion.class, "st2v",
                        eq(property(Student2Orientation.student().id().fromAlias("r")),property(EppStudent2EduPlanVersion.student().id().fromAlias("st2v")))
                )
                .where(eqValue(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("st2v")), rel.getEduPlanVersion().getId()))
                .where(eqValue(property(Student2Orientation.orientation().id().fromAlias("r")), rel.getOrientation().getId()))
                .where(eqValue(property(Student2Orientation.educationYear().id().fromAlias("r")), rel.getEducationYear().getId()))
                .column("r");
        List<Student2Orientation> lst = getList(builder);

        for (Student2Orientation s : lst)
        {
            s.setActual(false);
            s.setRemovalDate(new Date());
            s.setSource(PersonSecurityUtil.getExecutor());
            getSession().saveOrUpdate(s);
        }
    }


    private DQLSelectBuilder getNotActualBuilder(Long orgUnitId, EducationYear educationYear, List<Long> usedIds)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Orientation2EduPlanVersion.class, "relation")
                .where(eqValue(property(Orientation2EduPlanVersion.orientation().formativeOrgUnit().id().fromAlias("relation")), orgUnitId))
                .where(eqValue(property(Orientation2EduPlanVersion.educationYear().fromAlias("relation")), educationYear));
        if (usedIds != null && !usedIds.isEmpty())
            builder.where(notIn(property(Orientation2EduPlanVersion.id().fromAlias("relation")), usedIds));

        return builder;
    }

}
