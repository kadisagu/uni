package ru.tandemservice.unitutor.entity.catalog;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unitutor.entity.catalog.gen.*;

/**
 * @see ru.tandemservice.unitutor.entity.catalog.gen.PhysicalFitnessLevelGen
 */
public class PhysicalFitnessLevel extends PhysicalFitnessLevelGen implements IDynamicCatalogItem
{

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, PhysicalFitnessLevel.class)
                .titleProperty(PhysicalFitnessLevel.P_TITLE)
                .filter(PhysicalFitnessLevel.title())
                .order(PhysicalFitnessLevel.title());
    }
}