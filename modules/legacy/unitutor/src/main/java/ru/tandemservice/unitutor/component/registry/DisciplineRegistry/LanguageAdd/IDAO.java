/* $Id$ */
package ru.tandemservice.unitutor.component.registry.DisciplineRegistry.LanguageAdd;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Ekaterina Zvereva
 * @since 04.02.2016
 */
public interface IDAO extends IUniDao<Model>
{
}