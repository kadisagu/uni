/* $Id$ */
package ru.tandemservice.unitutor.base.bo.MassStudentSubscribe.logic;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Ekaterina Zvereva
 * @since 15.11.2015
 */
public class GeneratorIds
{
        private static AtomicLong lastId = new AtomicLong(System.currentTimeMillis());

        public static Long generateNewId()
        {
            return lastId.getAndIncrement();
        }
}