package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности «Запись реестра (базовая)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppRegistryElementExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.EppRegistryElementExt";
    public static final String ENTITY_NAME = "eppRegistryElementExt";
    public static final int VERSION_HASH = 378261515;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_REGISTRY_ELEMENT = "eppRegistryElement";
    public static final String P_ANNOTATION = "annotation";
    public static final String L_LANGUAGE = "language";
    public static final String P_SECOND_LANGUAGE = "secondLanguage";

    private EppRegistryElement _eppRegistryElement;     // Дисциплина реестра
    private String _annotation;     // Аннотация (для ДПВ, УДВ, УФ)
    private ForeignLanguage _language;     // Язык
    private boolean _secondLanguage = false;     // Второй изучаемый

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getEppRegistryElement()
    {
        return _eppRegistryElement;
    }

    /**
     * @param eppRegistryElement Дисциплина реестра. Свойство не может быть null.
     */
    public void setEppRegistryElement(EppRegistryElement eppRegistryElement)
    {
        dirty(_eppRegistryElement, eppRegistryElement);
        _eppRegistryElement = eppRegistryElement;
    }

    /**
     * @return Аннотация (для ДПВ, УДВ, УФ).
     */
    @Length(max=1000)
    public String getAnnotation()
    {
        return _annotation;
    }

    /**
     * @param annotation Аннотация (для ДПВ, УДВ, УФ).
     */
    public void setAnnotation(String annotation)
    {
        dirty(_annotation, annotation);
        _annotation = annotation;
    }

    /**
     * @return Язык.
     */
    public ForeignLanguage getLanguage()
    {
        return _language;
    }

    /**
     * @param language Язык.
     */
    public void setLanguage(ForeignLanguage language)
    {
        dirty(_language, language);
        _language = language;
    }

    /**
     * @return Второй изучаемый. Свойство не может быть null.
     */
    @NotNull
    public boolean isSecondLanguage()
    {
        return _secondLanguage;
    }

    /**
     * @param secondLanguage Второй изучаемый. Свойство не может быть null.
     */
    public void setSecondLanguage(boolean secondLanguage)
    {
        dirty(_secondLanguage, secondLanguage);
        _secondLanguage = secondLanguage;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppRegistryElementExtGen)
        {
            setEppRegistryElement(((EppRegistryElementExt)another).getEppRegistryElement());
            setAnnotation(((EppRegistryElementExt)another).getAnnotation());
            setLanguage(((EppRegistryElementExt)another).getLanguage());
            setSecondLanguage(((EppRegistryElementExt)another).isSecondLanguage());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppRegistryElementExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppRegistryElementExt.class;
        }

        public T newInstance()
        {
            return (T) new EppRegistryElementExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eppRegistryElement":
                    return obj.getEppRegistryElement();
                case "annotation":
                    return obj.getAnnotation();
                case "language":
                    return obj.getLanguage();
                case "secondLanguage":
                    return obj.isSecondLanguage();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eppRegistryElement":
                    obj.setEppRegistryElement((EppRegistryElement) value);
                    return;
                case "annotation":
                    obj.setAnnotation((String) value);
                    return;
                case "language":
                    obj.setLanguage((ForeignLanguage) value);
                    return;
                case "secondLanguage":
                    obj.setSecondLanguage((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eppRegistryElement":
                        return true;
                case "annotation":
                        return true;
                case "language":
                        return true;
                case "secondLanguage":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eppRegistryElement":
                    return true;
                case "annotation":
                    return true;
                case "language":
                    return true;
                case "secondLanguage":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eppRegistryElement":
                    return EppRegistryElement.class;
                case "annotation":
                    return String.class;
                case "language":
                    return ForeignLanguage.class;
                case "secondLanguage":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppRegistryElementExt> _dslPath = new Path<EppRegistryElementExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppRegistryElementExt");
    }
            

    /**
     * @return Дисциплина реестра. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.EppRegistryElementExt#getEppRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> eppRegistryElement()
    {
        return _dslPath.eppRegistryElement();
    }

    /**
     * @return Аннотация (для ДПВ, УДВ, УФ).
     * @see ru.tandemservice.unitutor.entity.EppRegistryElementExt#getAnnotation()
     */
    public static PropertyPath<String> annotation()
    {
        return _dslPath.annotation();
    }

    /**
     * @return Язык.
     * @see ru.tandemservice.unitutor.entity.EppRegistryElementExt#getLanguage()
     */
    public static ForeignLanguage.Path<ForeignLanguage> language()
    {
        return _dslPath.language();
    }

    /**
     * @return Второй изучаемый. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.EppRegistryElementExt#isSecondLanguage()
     */
    public static PropertyPath<Boolean> secondLanguage()
    {
        return _dslPath.secondLanguage();
    }

    public static class Path<E extends EppRegistryElementExt> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _eppRegistryElement;
        private PropertyPath<String> _annotation;
        private ForeignLanguage.Path<ForeignLanguage> _language;
        private PropertyPath<Boolean> _secondLanguage;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина реестра. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.EppRegistryElementExt#getEppRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> eppRegistryElement()
        {
            if(_eppRegistryElement == null )
                _eppRegistryElement = new EppRegistryElement.Path<EppRegistryElement>(L_EPP_REGISTRY_ELEMENT, this);
            return _eppRegistryElement;
        }

    /**
     * @return Аннотация (для ДПВ, УДВ, УФ).
     * @see ru.tandemservice.unitutor.entity.EppRegistryElementExt#getAnnotation()
     */
        public PropertyPath<String> annotation()
        {
            if(_annotation == null )
                _annotation = new PropertyPath<String>(EppRegistryElementExtGen.P_ANNOTATION, this);
            return _annotation;
        }

    /**
     * @return Язык.
     * @see ru.tandemservice.unitutor.entity.EppRegistryElementExt#getLanguage()
     */
        public ForeignLanguage.Path<ForeignLanguage> language()
        {
            if(_language == null )
                _language = new ForeignLanguage.Path<ForeignLanguage>(L_LANGUAGE, this);
            return _language;
        }

    /**
     * @return Второй изучаемый. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.EppRegistryElementExt#isSecondLanguage()
     */
        public PropertyPath<Boolean> secondLanguage()
        {
            if(_secondLanguage == null )
                _secondLanguage = new PropertyPath<Boolean>(EppRegistryElementExtGen.P_SECOND_LANGUAGE, this);
            return _secondLanguage;
        }

        public Class getEntityClass()
        {
            return EppRegistryElementExt.class;
        }

        public String getEntityName()
        {
            return "eppRegistryElementExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
