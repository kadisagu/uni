package ru.tandemservice.unitutor.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unitutor.entity.gen.*;

/** @see ru.tandemservice.unitutor.entity.gen.Student2OrientationGen */
public class Student2Orientation extends Student2OrientationGen
{
    public final static String TANDEM_SOURCE = "TANDEM";
    public final static String SAKAI_SOURCE = "SAKAI";

    @EntityDSLSupport
    public String getSourceCode() {
        if (this.getSource().contains("Личный кабинет"))
            return SAKAI_SOURCE;

        return TANDEM_SOURCE;
    }
}