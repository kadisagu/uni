package ru.tandemservice.unitutor.dao;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.entity.Student2Orientation;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * ДАО подписки студента на дисциплины
 *
 * @author vch
 */
public interface IDaoStudentSubscribe {

    String MOVED_ROW = "Перемещено";

    List<DisciplineWrap> getDisciplineWrapList(EducationYear educationYear, YearDistributionPart yearDistributionPart, Student student,
                                               StringBuilder warningLog, boolean generateWarningRow, boolean generateRowIndex, String electiveDisciplineTypeCode);

    List<ElectiveDiscipline> getElectiveFromRegistryList(@NotNull EducationYear educationYear, @NotNull YearDistributionPart yearDistributionPart, Student student,
                                                         String electiveDisciplineTypeCode, StringBuilder warning);

//    boolean containElectiveInWorkPlan(EducationYear educationYear, YearDistributionPart yearDistributionPart,
//                                      Student student, String electiveDisciplineTypeCode);

    /**
     * Количество УДВ/УФ в РУП студента на указанный семестр
     *
     * @param educationYear - учебный год
     * @param yearDistributionPart - часть учебного года
     * @param student - студент
     * @param electiveDisciplineTypeCode - тип дисциплины
     */
    int getCountElectiveInWorkPlan(EducationYear educationYear, YearDistributionPart yearDistributionPart, Student student, String electiveDisciplineTypeCode);

    /**
     * Подписать студента на дисциплины
     *
     * @param student - студент
     * @param idsElectiveDiscipline (id ElectiveDiscipline, на которые хотим подписать)
     */
    @Transactional
    String saveStudentSubscribe(Student student, EducationYear year, List<Long> idsElectiveDiscipline, String userName,
                                       boolean sendMail, boolean checkCountAndDate);

    @Transactional
    String saveElectiveSubscribe(Student student, List<ElectiveDisciplineSubscribe> listSubscribes, boolean fromSakai);

    @Transactional
    String unsubscribeElectiveDisciplines(Student student, List<ElectiveDisciplineSubscribe> subscribes, boolean fromSakai);

    String saveStudentSubscribe(Student student, ElectiveDiscipline electiveDiscipline, String comment,
                                       String userName, boolean sendMail, boolean checkCountAndDate, Long transactionId);

    String usubscribe(ElectiveDisciplineSubscribe electiveDisciplineSubscribe, String comment, boolean checkCountAndDate,
                             Long transactionId, boolean fromSakai);

    /**
     * Для тандема (в случае, если у нас открыть UI с фильтрацией по частям года)
     */
    @Transactional
    String saveStudentSubscribeByYearAndPart(Student student, EducationYear year, YearDistributionPart part,
                                                    List<ElectiveDiscipline> subscribedTo, String userName, boolean sendMail, boolean checkCountAndDate);

    /**
     * на что студент уже подписан
     * Если transactionId указан, то выдаем все, в том числе не актуальные подписки
     */
    List<ElectiveDisciplineSubscribe> getSubscribedElectiveDiscipline(List<Student> student, EducationYear year, YearDistributionPart part,
                                                                             boolean sort, Long transactionId);

    String getRowIndex(EppEpvRow row);

    void sendMail(Student student, EducationYear educationYear, Long transactionId, boolean fromSakai);

    Long getTransactionId();

    /**
     * Подписываем на направленность
     */
    @Transactional
    String saveStudentOrientationSubscribe(Student student, EducationOrgUnit orientation, Student2Orientation student2Orientation, String source, boolean needSendMail);

    /**
     * Актуализируем список направленоостей
     */
    @Transactional
    void actualizeOrientations(Long orgUnitId, EducationYear educationYear);

}
