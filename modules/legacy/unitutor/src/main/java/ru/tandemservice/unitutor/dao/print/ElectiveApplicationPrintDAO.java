package ru.tandemservice.unitutor.dao.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.catalog.TutorTemplateDocument;

import java.util.Calendar;
import java.util.List;

public class ElectiveApplicationPrintDAO extends UniBaseDao implements IElectiveApplicationPrintDAO {

    @Override
    public RtfDocument generateDocument(Student student, String disciplineTitle) {
        return generatePage(getTemplate(), student, disciplineTitle);
    }

    @Override
    public RtfDocument generateDocument(List<ElectiveDisciplineSubscribe> electiveSubscribes) {
        // готовим шаблон для страницы
        RtfDocument template = getTemplate().getClone();
        // готовим документ
        RtfDocument document = template.getClone();
        document.getElementList().clear();

        if (!electiveSubscribes.isEmpty()) {
            boolean hasManyPage = false;

            for (ElectiveDisciplineSubscribe electiveSubscribe : electiveSubscribes) {

                Student student = electiveSubscribe.getStudent();
                String disciplineTitle = electiveSubscribe.getElectiveDiscipline().getRegistryElementPart().getRegistryElement().getTitle();

                if (hasManyPage)
                    document.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                RtfDocument groupPage = generatePage(template, student, disciplineTitle);
                if (groupPage != null)
                    document.getElementList().addAll(groupPage.getElementList());

                hasManyPage = true;
            }
        }
        return document;
    }

    protected RtfDocument generatePage(RtfDocument template, Student student, String disciplineTitle)
    {
        if (student == null)
            return null;

        RtfDocument page = template.getClone();
        RtfInjectModifier im = getInjectModifier(student, disciplineTitle);
        im.modify(page);
        return page;
    }

    private RtfInjectModifier getInjectModifier(Student student, String disciplineTitle) {
        RtfInjectModifier im = new RtfInjectModifier();

        if (student == null)
            return im;

        String male = student.getPerson().isMale() ? "та" : "тки";
        String course = StringUtils.trimToEmpty(student.getCourse().getTitle());
        String group = student.getGroup() != null ? student.getGroup().getTitle() : "";

        String formativeOrgUnit = student.getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle();
        if (StringUtils.isEmpty(formativeOrgUnit))
            formativeOrgUnit = student.getEducationOrgUnit().getFormativeOrgUnit().getTitle();

        String educationLevelsHighSchool = student.getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableShortTitle();

        String developForm = student.getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase();

        String currentDate = DateFormatter.STRING_MONTHS_AND_QUOTES.format(Calendar.getInstance().getTime());

        String compensationType = student.getCompensationType().getShortTitle();

        IdentityCard iCard = student.getPerson().getIdentityCard();
        String lastName = iCard.getLastName();
        String firstName = iCard.getFirstName();
        String middleName = iCard.getMiddleName();
        IdentityCardDeclinability identityCardDeclinability = IUniBaseDao.instance.get().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard(), iCard);
        if (identityCardDeclinability != null)
        {
            if (identityCardDeclinability.isLastNameDeclinable())
                lastName = PersonManager.instance().declinationDao().getDeclinationLastName(lastName, GrammaCase.GENITIVE, student.getPerson().isMale());

            if (identityCardDeclinability.isFirstNameDeclinable())
                firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(firstName, GrammaCase.GENITIVE, student.getPerson().isMale());

            if (identityCardDeclinability.isMiddleNameDeclinable())
                middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(middleName, GrammaCase.GENITIVE, student.getPerson().isMale());

        }

        im.put("male", male);
        im.put("course", course);
        im.put("group", group);
        im.put("formativeOrgUnit", formativeOrgUnit);
        im.put("educationLevelsHighSchool", educationLevelsHighSchool);
        im.put("developForm", developForm);
        im.put("compensationType", compensationType);
        im.put("disciplineTitle", disciplineTitle);
        im.put("lastName", lastName);
        im.put("firstName", firstName);
        im.put("middleName", middleName);
        im.put("currentDate", currentDate);
        return im;
    }

    private RtfDocument getTemplate() {
        ITemplateDocument template = getCatalogItem(TutorTemplateDocument.class, TutorTemplateDocument.ELECTIVE_APPLICATION_TEMPLATE);
        return new RtfReader().read(template.getContent());
    }
}
