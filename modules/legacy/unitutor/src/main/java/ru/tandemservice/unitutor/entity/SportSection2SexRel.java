package ru.tandemservice.unitutor.entity;

import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.unitutor.entity.gen.*;

/** @see ru.tandemservice.unitutor.entity.gen.SportSection2SexRelGen */
public class SportSection2SexRel extends SportSection2SexRelGen
{
    public SportSection2SexRel()
    {
    }

    public SportSection2SexRel(SportSection section, Sex sex)
    {
        this.setSection(section);
        this.setSex(sex);
    }
}