package ru.tandemservice.unitutor.component.awp.DisciplineList;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;

public class Wrapper extends EntityBase {
    private ElectiveDiscipline electiveDiscipline;

    private String formOfControl;

    private Long placesSubscribed;

    private Long freePlacesToSubscribe;

    private String eppWPRElRowIndex;

    private String eppWPlanTitle;

    private EppWorkPlan workPlan;

    /**
     * метка - содержит ли аннотацию
     */
    private boolean containAnnotation;

    @Override
    public Long getId()
    {
        return electiveDiscipline.getId();
    }

    public ElectiveDiscipline getElectiveDiscipline() {
        return electiveDiscipline;
    }

    public void setElectiveDiscipline(ElectiveDiscipline electiveDiscipline) {
        this.electiveDiscipline = electiveDiscipline;
    }

    public String getFormOfControl() {
        return formOfControl;
    }

    public void setFormOfControl(String formOfControl) {
        this.formOfControl = formOfControl;
    }

    public Long getPlacesSubscribed() {
        return placesSubscribed;
    }

    public void setPlacesSubscribed(Long placesSubscribed) {
        this.placesSubscribed = placesSubscribed;
    }

    public Long getFreePlacesToSubscribe() {
        return freePlacesToSubscribe;
    }

    public void setFreePlacesToSubscribe(Long freePlacesToSubscribe) {
        this.freePlacesToSubscribe = freePlacesToSubscribe;
    }

    public void setContainAnnotation(boolean containAnnotation) {
        this.containAnnotation = containAnnotation;
    }

    public boolean isContainAnnotation() {
        return containAnnotation;
    }

    public String getEppWPRElRowIndex() {
        return eppWPRElRowIndex;
    }

    public void setEppWPRElRowIndex(String eppWPRElRowIndex) {
        this.eppWPRElRowIndex = eppWPRElRowIndex;
    }

    public String getEppWPlanTitle() {
        return eppWPlanTitle;
    }

    public void setEppWPlanTitle(String eppWPlanTitle) {
        this.eppWPlanTitle = eppWPlanTitle;
    }

    public EppWorkPlan getWorkPlan() {
        return workPlan;
    }

    public void setWorkPlan(EppWorkPlan workPlan) {
        this.workPlan = workPlan;
        setEppWPlanTitle(this.workPlan.getTitle());
    }
}
