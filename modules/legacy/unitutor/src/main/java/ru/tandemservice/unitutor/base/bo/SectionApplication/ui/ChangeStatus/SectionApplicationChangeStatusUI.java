/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication.ui.ChangeStatus;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.catalog.ApplicationStatus4SportsSection;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 24.08.2016
 */
@Input({@Bind(key = SectionApplicationChangeStatus.APPLICATION_IDS_PARAM, binding = "applicationIds")})
public class SectionApplicationChangeStatusUI extends UIPresenter
{
    private List<Long> _applicationIds;
    private List<SectionApplication> _applications;
    private ApplicationStatus4SportsSection _status;


    @Override
    public void onComponentRefresh()
    {
        _applications = DataAccessServices.dao().getList(SectionApplication.class, _applicationIds);
    }

    //Listeners
    public void onClickApply()
    {
        if (CollectionUtils.isNotEmpty(_applications))
        {
            ICommonDAO dao = DataAccessServices.dao();
            _applications.stream()
                    .filter(application -> !application.getStatus().equals(_status))
                    .forEach(application -> {
                        application.setStatus(_status);
                        dao.saveOrUpdate(application);
                    });
        }

        deactivate();
    }

    //Getters and Setters
    public List<Long> getApplicationIds()
    {
        return _applicationIds;
    }

    public void setApplicationIds(List<Long> applicationIds)
    {
        _applicationIds = applicationIds;
    }

    public ApplicationStatus4SportsSection getStatus()
    {
        return _status;
    }

    public void setStatus(ApplicationStatus4SportsSection status)
    {
        _status = status;
    }
}