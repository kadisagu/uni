package ru.tandemservice.unitutor.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

public interface IDaoElectiveDiscipline {


    /**
     * создать или удалить ДПВ
     * (привести в соотвествие с РП)
     *
     * @param educationYear
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    void createOrDeleteElectiveDiscipline(EducationYear educationYear);
}
