package ru.tandemservice.unitutor.component.awp.StudentOrientationList;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.dao.IUniDao;

import java.util.Collection;

public interface IDAO extends IUniDao<Model> {

    public void deleteRecords(Collection<IEntity> list);
}
