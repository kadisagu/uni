package ru.tandemservice.unitutor.component.registry.UFRegistry.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unitutor.component.registry.RegistryBase.IRegistryBaseDAO;
import ru.tandemservice.unitutor.component.registry.RegistryBase.RegistryBaseDAO;
import ru.tandemservice.unitutor.component.registry.RegistryBase.RegistryWrapper;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;

import java.util.List;

public class DAO extends RegistryBaseDAO<Model> implements IRegistryBaseDAO<Model>
{

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        model.setDisciplineModel(getDisciplineListSelectModel(ElectiveDisciplineTypeCodes.UF));
    }

    @Override
    public void prepareListDataSource(Model model) {

        DQLSelectBuilder dql = getElectiveDisciplineDQL(model, "ElectiveDiscipline", ElectiveDisciplineTypeCodes.UF);
        patchOrder(model, dql, "ElectiveDiscipline");

        DynamicListDataSource<RegistryWrapper> dataSource = model.getDataSource();
        // итого записей (нужно, для UI)
        final Number count = dql.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        dataSource.setTotalSize((null == count ? 0 : count.longValue()));

        // начальная, кол-во записей (читаем из забора)
        long startRow = dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow();
        long countRow = dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow();

        // получим столько записей, сколько нам надо (лишнее не читаем)
        List<Object[]> resultData = dql.createStatement(getSession()).setFirstResult((int) startRow).setMaxResults((int) countRow).list();

        dataSource.createPage(getWrapperList(resultData));
    }
}
