/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.catalog.codes.ApplicationStatus4SportsSectionCodes;

import javax.annotation.Nullable;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 24.08.2016
 */
@Configuration
public class SectionApplicationManager extends BusinessObjectManager
{
    public static SectionApplicationManager instance()
    {
        return instance(SectionApplicationManager.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
                .customize((alias, dql, context, filter) ->
                           {

                               EducationYear year = context.get("educationYear");
                               Course course = context.get("course");
                               if (course != null)
                               {
                                   EducationYear currentYear = EducationYear.getCurrentRequired();
                                   if (currentYear.equals(year))
                                       dql.where(eq(property(alias, Group.course()), value(course)));
                                   else
                                       dql.where(eq(property(alias, Group.course().intValue()),
                                                    value(course.getIntValue() - (year.getIntValue() - currentYear.getIntValue()))));
                               }

                               FilterUtils.applyLikeFilter(dql, filter, Group.title().fromAlias(alias));
                               FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().developForm().programForm(), context.get("programForm"));
                               FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().formativeOrgUnit(), context.get("formativeOrgUnit"));
                               FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().educationLevelHighSchool(), context.get("educationLevel"));

                               return dql.where(eq(property(alias, Group.archival()), value(false)));
                           });
    }


    /**
     * Фильтрует студентов для заявлений на секцию
     * <ul>
     * <li>только активные
     * <li>не имеющие заявление в статусе Согласованно или На согласовании (кроме сurrentApplicationId если он указан)
     * <li>курс студента в выбранном учебном году должен быть до третьего курса и совпадать с course если указан
     * <li>бакалавриат или специалитет
     * <li>по полу если указан
     * <li>по формирующему подразделению если указано
     * <li>по форме обюучения если указана
     * <li>по направлению подготовки если указано
     * <li>по группа если указана
     * <li>по статусу студента если указан
     * </ul>
     *
     * @param builder билдер селекта студентов
     * @param alias алиас
     * @param educationYear Год обучения, обязателен,
     * @param course курс студента в выбранном учебном году
     * @param sex пол
     * @param formativeOrgUnit формирующее подразделение
     * @param programForm сущность или коллекция
     * @param levelsHighSchool сущность или коллекция
     * @param group сущность или коллекция
     * @param studentStatus сущность или коллекция
     * @param currentApplicationId id существующего заявления (для формы редактирования)
     * @return тот же билдер что и на входе
     */
    public static DQLSelectBuilder filterStudents4Applications(DQLSelectBuilder builder, String alias,
                                                               EducationYear educationYear, Course course,
                                                               Object sex, Object formativeOrgUnit, Object programForm, Object levelsHighSchool, Object group, Object studentStatus,
                                                               Long currentApplicationId)
    {
        builder.where(eq(property(alias, Student.archival()), value(false)));
//                .where(eq(property(alias, Student.status().active()), value(true)));

        String exAppAlias = "sa1";
        IDQLExpression existApps = and(
                eq(property(exAppAlias, SectionApplication.student().s()), property(alias)),
                not(eq(property(exAppAlias, SectionApplication.id()), value(currentApplicationId))),

                or(eq(property(exAppAlias, SectionApplication.status().code().s()), value(ApplicationStatus4SportsSectionCodes.ACCEPTED)),
                   eq(property(exAppAlias, SectionApplication.status().code().s()), value(ApplicationStatus4SportsSectionCodes.ACCEPTABLE))),
                eq(property(exAppAlias, SectionApplication.sportSection().educationYear()), value(educationYear))
        );
        builder.where(notExistsByExpr(SectionApplication.class, exAppAlias, existApps));

        EducationYear currentYear = EducationYear.getCurrentRequired();
        int shift = educationYear.getIntValue() - currentYear.getIntValue();
        if (course != null)
            builder.where(eq(property(alias, Student.course().intValue()), value(course.getIntValue() - shift)));
        builder.where(le(property(alias, Student.course().intValue()), value(3 - shift)));
        builder.where(or(
                eq(property(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind().code()),
                   value(EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA)),
                eq(property(alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind().code()),
                   value(EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV))
        ));

        FilterUtils.applySelectFilter(builder, alias, Student.person().identityCard().sex(), sex);
        FilterUtils.applySelectFilter(builder, alias, Student.educationOrgUnit().formativeOrgUnit(), formativeOrgUnit);
        FilterUtils.applySelectFilter(builder, alias, Student.status(), programForm);
        FilterUtils.applySelectFilter(builder, alias, Student.educationOrgUnit().educationLevelHighSchool(), levelsHighSchool);
        FilterUtils.applySelectFilter(builder, alias, Student.educationOrgUnit().developForm().programForm(), studentStatus);
        FilterUtils.applySelectFilter(builder, alias, Student.group(), group);

        return builder;
    }
}