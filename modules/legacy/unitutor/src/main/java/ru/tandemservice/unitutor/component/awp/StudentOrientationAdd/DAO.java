package ru.tandemservice.unitutor.component.awp.StudentOrientationAdd;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;
import ru.tandemservice.unitutor.entity.Student2Orientation;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {

        if (model.getStudentId() == null) {
            //добавление направленности
            model.setSendMail(true);
        }
        else {
            model.setRel(get(Student2Orientation.class, model.getStudentId()));
            model.setSendMail(true);
            model.setStudent(model.getRel().getStudent());
            model.setOrientation(model.getRel().getOrientation());
        }
        model.setStudentSelectModel(new DQLFullCheckSelectModel(Student.fullTitle()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                //те студенты, у которых есть актуальный УПв
                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "rel")
                        .column(EppStudent2EduPlanVersion.student().id().fromAlias("rel").s())
                        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("rel"))))
                        .where(eqValue(property(EppStudent2EduPlanVersion.student().educationOrgUnit().formativeOrgUnit().id().fromAlias("rel")), model.getOrgUnitId()))
                        .where(eqValue(property(EppStudent2EduPlanVersion.student().status().active().fromAlias("rel")), Boolean.TRUE));
                //студента, уже подписаны на напрвленость
                DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder().fromEntity(Student2Orientation.class, "r")
                        .where(eqValue(property(Student2Orientation.student().educationOrgUnit().formativeOrgUnit().id().fromAlias("r")), model.getOrgUnitId()))
                        .where(eqValue(property(Student2Orientation.educationYear().fromAlias("r")), model.getEducationYear()))
                        .column(Student2Orientation.student().id().fromAlias("r").s());

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, alias)
                        .where(in(property(Student.id().fromAlias(alias)), subBuilder.buildQuery()))
                        .where(notIn(property(Student.id().fromAlias(alias)), dqlSelectBuilder.buildQuery()));
                FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().fullFio().s(), filter);
                return builder;
            }
        });

        model.setOrientationSelectModel(new DQLFullCheckSelectModel(EducationOrgUnit.educationLevelHighSchool().fullTitle()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                if (model.getStudent() == null)
                    return null;

                DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Orientation2EduPlanVersion.class, "rel")
                        .column(Orientation2EduPlanVersion.orientation().id().fromAlias("rel").s())
                        .where(eqValue(property(Orientation2EduPlanVersion.actual().fromAlias("rel")), Boolean.TRUE))
                        .where(eqValue(property(Orientation2EduPlanVersion.allowEntry().fromAlias("rel")), Boolean.TRUE))
                        .where(eqValue(property(Orientation2EduPlanVersion.educationYear().fromAlias("rel")), model.getEducationYear()))
                        .joinPath(DQLJoinType.inner, Orientation2EduPlanVersion.orientation().fromAlias("rel"), "eou")
                                //ФУТС должны быть одинаковы
                        .where(eqValue(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), model.getStudent().getEducationOrgUnit().getFormativeOrgUnit()))
                        .where(eqValue(property(EducationOrgUnit.developForm().fromAlias("eou")), model.getStudent().getEducationOrgUnit().getDevelopForm()))
                        .where(eqValue(property(EducationOrgUnit.developCondition().fromAlias("eou")), model.getStudent().getEducationOrgUnit().getDevelopCondition()))
                        .where(eqValue(property(EducationOrgUnit.developPeriod().fromAlias("eou")), model.getStudent().getEducationOrgUnit().getDevelopPeriod()))
                        .where(eqValue(property(EducationOrgUnit.developTech().fromAlias("eou")), model.getStudent().getEducationOrgUnit().getDevelopTech()))
                                //учебные планы должен быть одинаковые
                        .joinEntity("rel", DQLJoinType.inner, EppStudent2EduPlanVersion.class, "st2v",
                                    eq(property(Orientation2EduPlanVersion.eduPlanVersion().id().fromAlias("rel")),
                                       property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("st2v"))))
                                //только актуальные УП
                        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("st2v"))))
                        .where(eqValue(property(EppStudent2EduPlanVersion.student().fromAlias("st2v")), model.getStudent()));

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, alias)
                        .where(in(property(EducationOrgUnit.id().fromAlias(alias)), subBuilder.buildQuery()));
                if (model.getRel() != null)
                    builder.where(ne(property(EducationOrgUnit.id().fromAlias(alias)), model.getRel().getOrientation().getId()));

                FilterUtils.applySimpleLikeFilter(builder, alias, EducationOrgUnit.educationLevelHighSchool().fullTitle().s(), filter);
                return builder;
            }
        });
    }

    @Override
    public void update(Model model) {
        DaoStudentSubscribe.instanse().saveStudentOrientationSubscribe(
                model.getStudent(), model.getOrientation(), model.getRel(), Student2Orientation.TANDEM_SOURCE, model.isSendMail());
    }

}
