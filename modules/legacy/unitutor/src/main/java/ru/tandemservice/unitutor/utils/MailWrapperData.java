package ru.tandemservice.unitutor.utils;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;


/**
 * Класс-обертка для данных, необходимых почтовому уведомлению
 */
public class MailWrapperData {

    private EppRegistryElementPart eppRegistryElementPart;
    private String annotation;
    private String term;
    private String formOfControl;

    public MailWrapperData(EppRegistryElementPart eppRegistryElementPart, String annotation, String term, String formOfControl) {
        this.setEppRegistryElementPart(eppRegistryElementPart);
        this.annotation = annotation;
        this.term = term;
        this.formOfControl = formOfControl;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getFormOfControl() {
        return formOfControl;
    }

    public void setFormOfControl(String formOfControl) {
        this.formOfControl = formOfControl;
    }

    public EppRegistryElementPart getEppRegistryElementPart() {
        return eppRegistryElementPart;
    }

    public void setEppRegistryElementPart(EppRegistryElementPart eppRegistryElementPart) {
        this.eppRegistryElementPart = eppRegistryElementPart;
    }

}
