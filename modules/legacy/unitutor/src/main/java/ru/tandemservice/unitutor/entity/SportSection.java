package ru.tandemservice.unitutor.entity;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unitutor.entity.catalog.codes.ApplicationStatus4SportsSectionCodes;
import ru.tandemservice.unitutor.entity.gen.*;

import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @see ru.tandemservice.unitutor.entity.gen.SportSectionGen
 */
public class SportSection extends SportSectionGen
{

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId)
    {
        return new EntityComboDataSourceHandler(ownerId, SportSection.class)
                .titleProperty(SportSection.title().s())
                .filter(SportSection.title());
    }

    /**
     * @return Название - Год обучения
     */
    public String getTitleWithEduYear()
    {
        return getTitle() + " - " + getEducationYear().getTitle();
    }

    /**
     * @return Первое занятие в неделю в формате "День недели, HH:mm"
     */
    public String getFirstClassInWeek()
    {
        return getDayAndTime(getFirstClassDay(), getFirstClassTime());
    }

    /**
     * @return Второе занятие в неделю в формате "День недели, HH:mm"
     */
    public String getSecondClassInWeek()
    {
        return getDayAndTime(getSecondClassDay(), getSecondClassTime());
    }

    /**
     * @return Время занятия в формате "День недели, HH:mm"
     */
    public static String getDayAndTime(Integer dayOfWeek, Date timeOfDay)
    {
        String day = "";
        if (dayOfWeek != null)
        {
            Locale ru = new Locale("ru");
            day = DayOfWeek.of(dayOfWeek).getDisplayName(TextStyle.FULL, ru);
        }

        String time = "";
        if (timeOfDay != null)
            time = DateFormatter.DATE_FORMATTER_TIME.format(timeOfDay);

        String separator = (dayOfWeek != null && timeOfDay != null) ? ", " : "";
        return day + separator + time;
    }

    /**
     * считает занятые места в секции по количетсву заявок со статусом "Согласована"
     *
     * @param section выражение для спортивной секии для которой будут считаться занятые места
     * @param alias   алиас который будет использовать внутри селекта
     * @return селект с количетсвом занятых мест в секции
     */
    public static IDQLSelectableQuery getBusyPlacesCounterQuery(IDQLExpression section, String alias)
    {
        return getAcceptedApplications(section, alias).buildCountQuery();
    }

    /**
     * Билдер запроса заявок со статусом "Согласована"
     *
     * @param section выражение для спортивной секии для которой будут считаться занятые места
     * @param alias   алиас который будет использовать внутри селекта
     */
    public static DQLSelectBuilder getAcceptedApplications(IDQLExpression section, String alias)
    {
        return new DQLSelectBuilder()
                .fromEntity(SectionApplication.class, alias)
                .column(property(alias))
                .where(eq(property(alias, SectionApplication.status().code()), value(ApplicationStatus4SportsSectionCodes.ACCEPTED)))
                .where(eq(property(alias, SectionApplication.sportSection()), section));
    }


    /**
     * Выражение с числом свободных мест в секции
     */
    public static IDQLExpression getFreePlacesExpression(String alias)
    {
        return minus(property(alias, SportSection.numbers()), getBusyPlacesCounterQuery(property(alias), "sa"));
    }

    /**
     * Добавляет в билдер условие "where exists" для связи Секции с Курсом.
     * Для пустых section или course условие не добавляется
     *
     * @param section SportSection, Массив, Коллекция или DQL выражение
     * @param course  Course, Массив, Коллекция или DQL выражение
     */
    public static DQLSelectBuilder addWhereExistsCourseRel(DQLSelectBuilder builder, Object section, Object course)
    {
        if (isEmpty(section) || isEmpty(course)) return builder;

        return builder.where(exists(SportSection2CourseRel.class,
                                    SportSection2CourseRel.section().s(), section,
                                    SportSection2CourseRel.course().s(), course));
    }

    /**
     * Добавляет в билдер условие "where exists" для связи Секции с Полом.
     * Для пустых section или sex условие не добавляется
     *
     * @param section SportSection, Массив, Коллекция или DQL выражение
     * @param sex     Sex, Массив, Коллекция или DQL выражение.
     */
    public static DQLSelectBuilder addWhereExistsSexRel(DQLSelectBuilder builder, Object section, Object sex)
    {
        if (isEmpty(section) || isEmpty(sex)) return builder;

        return builder.where(exists(SportSection2SexRel.class,
                                    SportSection2SexRel.section().s(), section,
                                    SportSection2SexRel.sex().s(), sex));
    }

    private static boolean isEmpty(Object o)
    {
        return o == null
                || (o instanceof Collection && ((Collection) o).size() == 0)
                || (o instanceof Object[] && ((Object[]) o).length == 0);

    }
}