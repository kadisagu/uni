package ru.tandemservice.unitutor.component.registry.DisciplineRegistry.AnnotationPubForm;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;

@Input({@Bind(key = EppRegistryElement.P_ID, binding = "registryElementId", required = true)})
public class Model {

    private Long registryElementId;
    private EppRegistryElementExt registryElementExt;

    public Long getRegistryElementId() {
        return registryElementId;
    }

    public void setRegistryElementId(Long registryElementId) {
        this.registryElementId = registryElementId;
    }

    public EppRegistryElementExt getRegistryElementExt() {
        return registryElementExt;
    }

    public void setRegistryElementExt(EppRegistryElementExt registryElementExt) {
        this.registryElementExt = registryElementExt;
    }
}
