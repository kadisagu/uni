/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication.ui.List;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.report.ExcelListDataSourcePrinter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.base.bo.SectionApplication.ui.AddEdit.SectionApplicationAddEdit;
import ru.tandemservice.unitutor.base.bo.SectionApplication.ui.ChangeStatus.SectionApplicationChangeStatus;
import ru.tandemservice.unitutor.base.bo.SectionApplication.ui.MassAdd.SectionApplicationMassAdd;
import ru.tandemservice.unitutor.entity.SectionApplication;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 24.08.2016
 */
public class SectionApplicationListUI extends UIPresenter
{
    private EducationYear _educationYear;

    @Override
    public void onComponentRefresh()
    {
        if (_educationYear == null)
            _educationYear = EducationYear.getCurrentRequired();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SectionApplicationList.EDUCATION_YEAR_PARAM, _educationYear);

        dataSource.put(SectionApplicationList.STATUS_LIST_PARAM, _uiSettings.get(SectionApplicationList.STATUS_LIST_PARAM));
        dataSource.put(SectionApplicationList.REGISTRATION_DATE_FROM_PARAM, _uiSettings.get(SectionApplicationList.REGISTRATION_DATE_FROM_PARAM));
        dataSource.put(SectionApplicationList.REGISTRATION_DATE_TO_PARAM, _uiSettings.get(SectionApplicationList.REGISTRATION_DATE_TO_PARAM));

        dataSource.put(SectionApplicationList.SECTION_TYPE_PARAM, _uiSettings.get(SectionApplicationList.SECTION_TYPE_PARAM));
        dataSource.put(SectionApplicationList.PHYSICAL_FITNESS_LEVEL_LIST_PARAM, _uiSettings.get(SectionApplicationList.PHYSICAL_FITNESS_LEVEL_LIST_PARAM));
        dataSource.put(SectionApplicationList.TUTOR_ORG_UNIT_LIST_PARAM, _uiSettings.get(SectionApplicationList.TUTOR_ORG_UNIT_LIST_PARAM));
        dataSource.put(SectionApplicationList.PLACE_LIST_PARAM, _uiSettings.get(SectionApplicationList.PLACE_LIST_PARAM));
        dataSource.put(SectionApplicationList.SECTION_NAME_PARAM, _uiSettings.get(SectionApplicationList.SECTION_NAME_PARAM));
        dataSource.put(SectionApplicationList.TRAINER_LIST_PARAM, _uiSettings.get(SectionApplicationList.TRAINER_LIST_PARAM));
        dataSource.put(SectionApplicationList.FIRST_CLASS_PARAM, _uiSettings.get(SectionApplicationList.FIRST_CLASS_PARAM));
        dataSource.put(SectionApplicationList.SECOND_CLASS_PARAM, _uiSettings.get(SectionApplicationList.SECOND_CLASS_PARAM));

        dataSource.put(SectionApplicationList.FORMATIVE_ORG_UNIT_LIST_PARAM, _uiSettings.get(SectionApplicationList.FORMATIVE_ORG_UNIT_LIST_PARAM));
        dataSource.put(SectionApplicationList.EDUCATION_LEVEL_LIST_PARAM, _uiSettings.get(SectionApplicationList.EDUCATION_LEVEL_LIST_PARAM));
        dataSource.put(SectionApplicationList.PROGRAM_FORM_LIST_PARAM, _uiSettings.get(SectionApplicationList.PROGRAM_FORM_LIST_PARAM));
        dataSource.put(SectionApplicationList.COURSE_LIST_PARAM, _uiSettings.get(SectionApplicationList.COURSE_LIST_PARAM));
        dataSource.put(SectionApplicationList.GROUP_LIST_PARAM, _uiSettings.get(SectionApplicationList.GROUP_LIST_PARAM));
        dataSource.put(SectionApplicationList.STUDENT_LIST_PARAM, _uiSettings.get(SectionApplicationList.STUDENT_LIST_PARAM));
    }

    //Listeners
    public void onClickAddEdit()
    {
        _uiActivation.asRegionDialog(SectionApplicationAddEdit.class).activate();
    }

    public void onClickMassAdd()
    {
        _uiActivation.asDesktopRoot(SectionApplicationMassAdd.class).parameter(SectionApplicationMassAdd.ALL_COMMON_FILTERS_REQUIRED, Boolean.TRUE).activate();
    }

    public void onClickMassAdd2()
    {
        _uiActivation.asDesktopRoot(SectionApplicationMassAdd.class).parameter(SectionApplicationMassAdd.ALL_COMMON_FILTERS_REQUIRED, Boolean.FALSE).activate();
    }

    public void onClickChangeStatus()
    {
        Collection<IEntity> selectedApplications = getSelectedApplications();

        if (CollectionUtils.isEmpty(selectedApplications))
            throw new ApplicationException("Нет выбранных заявок");

        List<Long> ids = selectedApplications.stream().map(IEntity::getId).collect(Collectors.toList());

        _uiActivation.asRegionDialog(SectionApplicationChangeStatus.class)
                .parameter(SectionApplicationChangeStatus.APPLICATION_IDS_PARAM, ids)
                .activate();
    }

    public void onClickEdit()
    {
        _uiActivation.asDesktopRoot(SectionApplicationAddEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getListenerParameterAsLong())
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
        onComponentRefresh();
    }

    public void onClickMassDelete()
    {
        Collection<IEntity> selectedApplications = getSelectedApplications();

        if (CollectionUtils.isEmpty(selectedApplications))
            throw new ApplicationException("Нет выбранных заявок");

        if (!"ok".equals(_uiSupport.getClientParameter()))
        {
            ConfirmInfo confirm = new ConfirmInfo("Удалить заявки?", new ClickButtonAction("massDelete", "ok", true));
            TapSupportUtils.displayConfirm(confirm);
            return;
        }

        ICommonDAO dao = DataAccessServices.dao();
        selectedApplications.forEach(wrapper -> dao.delete(((DataWrapper) wrapper).<SectionApplication>getWrapped()));
    }

    public void onClickRegistrationDate()
    {
        if (!isShowRegistrationDateFilter())
        {
            _uiSettings.set(SectionApplicationList.REGISTRATION_DATE_FROM_PARAM, null);
            _uiSettings.set(SectionApplicationList.REGISTRATION_DATE_TO_PARAM, null);
        }
    }

    public void onClickClassDays()
    {
        if (!isShowClassDaysFilter())
        {
            _uiSettings.set(SectionApplicationList.FIRST_CLASS_PARAM, null);
            _uiSettings.set(SectionApplicationList.SECOND_CLASS_PARAM, null);
        }
    }


    public void onClickSearch()
    {
        getSettings().save();
    }

    public void onClickClear()
    {
        getSettings().clear();
    }

    //Getters and Setters
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    public boolean isShowClassDaysFilter()
    {
        Boolean show = getSettings().get(SectionApplicationList.CLASS_DAYS_FILTER_PARAM);

        return show == null ? false : show;
    }

    public boolean isShowRegistrationDateFilter()
    {
        Boolean show = getSettings().get(SectionApplicationList.REGISTRATION_DATE_FILTER_PARAM);

        return show == null ? false : show;
    }

    public ExcelListDataSourcePrinter getExcelPrinter()
    {
        return new ExcelListDataSourcePrinter(
                getConfig().getProperty("bc.meta.title"),
                ((BaseSearchListDataSource) _uiConfig.getDataSource(SectionApplicationList.SEARCH_LIST_DS)).getLegacyDataSource())
                .setupPrintSelected();
    }

    public Collection<IEntity> getSelectedApplications()
    {
        BaseSearchListDataSource dataSource = _uiConfig.getDataSource(SectionApplicationList.SEARCH_LIST_DS);
        return dataSource.getOptionColumnSelectedObjects(SectionApplicationList.SEARCH_LIST_CHECKBOX);
    }
}