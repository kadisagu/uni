package ru.tandemservice.unitutor.ws.impl;

import com.google.common.collect.Lists;
import com.google.common.primitives.Longs;
import jxl.Workbook;
import jxl.write.WritableWorkbook;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.dao.print.IEppEduPlanPrintDAO;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.dao.DisciplineWrap;
import ru.tandemservice.unitutor.dao.IDaoStudentSubscribe;
import ru.tandemservice.unitutor.dao.print.IElectiveApplicationPrintDAO;
import ru.tandemservice.unitutor.entity.*;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;
import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;
import ru.tandemservice.unitutor.utils.UniEppFormatter;
import ru.tandemservice.unitutor.ws.types.*;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DisciplinesManager extends UniBaseDao implements IWsDisciplinesManager {

    public static IWsDisciplinesManager wsDisciplinesManager;

    public static IWsDisciplinesManager getWsDisciplinesManager() {

        if (wsDisciplinesManager == null)
            wsDisciplinesManager = (IWsDisciplinesManager) ApplicationRuntime.getBean(IWsDisciplinesManager.class.getName());
        return wsDisciplinesManager;
    }


    @Override
    public byte[] getEduPlanVersionData(String studentParam)
    {
        Long studentId = Longs.tryParse(studentParam);
        Student student = (studentId == null)? null : DataAccessServices.dao().get(Student.class, studentId);

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        if (student == null)
            return os.toByteArray();

        try {
            List<Long> epvIdList = OptionalDisciplineUtil.getEduPlanVersion(student);

            WritableWorkbook book = Workbook.createWorkbook(os);

            if (!epvIdList.isEmpty())
                IEppEduPlanPrintDAO.instance.get().printRegistryEduPlanVersion(book, epvIdList, new HashMap<>());

            book.write();
            book.close();
            os.close();

        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        return os.toByteArray();
    }

    protected List<Student> getStudentFromSakai(String studentParam)
    {
        if (studentParam == null || studentParam.isEmpty())
            throw new ApplicationException("Не указан параметр поиска студента");

        List<Student> result;
        if (isLdap()) {
            // ищем по логину
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "st")
                    .where(eq(property("st", Student.principal().login()), value(studentParam)))
                    .where(eq(property("st", Student.archival()), value(false)));
            result = UniDaoFacade.getCoreDao().getList(builder);
        }
        else {
            int personalNumber = 0;
            try {
                // из сакая может прийти "st123"
                personalNumber = Integer.parseInt(studentParam.replace(STUDENT_PREF, ""));
            }
            catch (Exception ex) {
                throw new ApplicationException("studentParam=" + studentParam + " нельзя преобразовать к целому числу, ошибка поиска студента");
            }

            // передан персональный номер
            result = UniDaoFacade.getCoreDao().getList(Student.class, Student.personalNumber(), studentParam);
        }

        if (result.isEmpty())
            throw new ApplicationException("По параметру studentParam = " + studentParam + " студент не найден");

        return result;
    }


    /**
     * Проверка аутентификации по ЛДАП
     */
    private boolean isLdap()
    {
        // смотрим на настройки Сакая uniSakaiSettings_t
        if (EntityRuntime.getMeta("ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings") != null) {
            MQBuilder builder = new MQBuilder("ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings", "e");
            builder.add(MQExpression.eq("e", "stringKey", "unisakai.useLDAP"));
            builder.addSelect("e", new Object[]{"stringValue"});
            List<String> lst = builder.getResultList(getSession());

            if (lst == null || lst.isEmpty()) {
                return false;
            }
            else {
                String val = lst.get(0);
                if (val == null)
                    return false;
                else {
                    val = val.toLowerCase();
                    return  (val.equals("true"));
                }
            }

        }
        else {
            // модуль сакая не подключен (почему-то)
            throw new ApplicationException("Не найдены настройки интеграции с Сакай, возможно модуль сакая не подключен");
        }

    }


    /**
     * Заявление для подписки на дисциплину
     * @param studentId - студент
     * @param disciplineId - идентификатор дисциплины
     *
     * @return - заявление студента
     */
    @Override
    public byte[] getStudentApplicationData(String studentId, String disciplineId)
    {
        Long studId = Longs.tryParse(studentId);
        Student student = get(Student.class, studId);
        Long discId = Longs.tryParse(disciplineId);
        if (discId == null)
            throw new ApplicationException("Не удается определить дисциплину");
        Object discipline = get(discId);
        String disciplineTitle;
        if (discipline instanceof EppWorkPlanRegistryElementRow) {
            disciplineTitle = ((EppWorkPlanRegistryElementRow) discipline).getRegistryElement().getTitle();
        }
        else if (discipline instanceof ElectiveDiscipline) {
            disciplineTitle = ((ElectiveDiscipline) discipline).getRegistryElementPart().getRegistryElement().getTitle();
        }
        else
            throw new ApplicationException("Не удается определить дисциплину");

        IElectiveApplicationPrintDAO electiveApplicationPrintDAO = (IElectiveApplicationPrintDAO) ApplicationRuntime.getBean(IElectiveApplicationPrintDAO.class.getName());
        RtfDocument document = electiveApplicationPrintDAO.generateDocument(student, disciplineTitle);

        return (document == null) ? new byte[0]: RtfUtil.toByteArray(document);
    }


    /**
     * Следующий учебный год
     * @return
     */
    private EducationYear getNextEducationYear()
    {
        EducationYear educationYearCurrent = EducationYear.getCurrentRequired();
//        return educationYearCurrent;

        int nextEducationYear = educationYearCurrent.getIntValue() + 1;
        EducationYear educationYearNext = UniDaoFacade.getCoreDao().get(EducationYear.class, EducationYear.intValue().s(), nextEducationYear);

        if (educationYearNext == null)
            throw new ApplicationException("Не удалось определить следующий учебный год");

        return educationYearNext;
    }


    /**
     *
     * @param studentParam - идентификатор студента
     * @param educationYearId
     * @return
     */
    @Override
    public List<WsDisciplineWrap> getWsDisciplinesNextEppYear(String studentParam, String educationYearId)
    {
        Long studentId = Longs.tryParse(studentParam);
        Student student = (studentId == null)? null : DataAccessServices.dao().get(Student.class, studentId);
        List<WsDisciplineWrap> retVal = new ArrayList<>();
        if (student == null)
            return retVal;

        EducationYear educationYearNext = null;
        if (educationYearId != null) {
            Long eduYearId = Long.parseLong(educationYearId);
            educationYearNext = get(EducationYear.class, eduYearId);
        }
        else
            educationYearNext = getNextEducationYear();

        // массив уже правильно отсортирован
        List<DisciplineWrap> wraps = DaoStudentSubscribe.instanse().getDisciplineWrapList(educationYearNext, null, student, null, true, true, ElectiveDisciplineTypeCodes.DPV);

        Map<Long, Integer> mapSubscribed = getCountSubscribersMap(educationYearNext, ElectiveDisciplineTypeCodes.DPV);

        List<EppWorkPlanRegistryElementRow> registryElementRows = new ArrayList<>();

        for (DisciplineWrap wrap : wraps) {
            WsDisciplineWrap wrapNew = rewrap(wrap, mapSubscribed);
            if (wrapNew != null) {
                retVal.add(wrapNew);
                if (wrap.getEppWorkPlanRegistryElementRow() != null)
                    registryElementRows.add(wrap.getEppWorkPlanRegistryElementRow());
            }

        }

        // достанем аннотации и добавим пометку во wrapNew
        Map<Long, String> annotationMap = OptionalDisciplineUtil.getAnnotationMapByWorkPlanRow(registryElementRows);
        for (WsDisciplineWrap rewrap : retVal) {
            if (annotationMap.containsKey(rewrap.getEppWorkPlanRegistryElementRowId()))
                rewrap.setAnnotationExist(true);
        }

        return retVal;
    }

    @Override
    public List<WsElectiveWrap> getWsElectiveNextEppYear(String studentParam, String electiveType, String educationYearId)
    {

        Long studentId = Longs.tryParse(studentParam);
        Student student = (studentId == null)? null : get(Student.class, studentId);
        if (student == null)
            throw new ApplicationException("Cтудент не найден");


        EducationYear educationYearNext;
        if (StringUtils.isNotEmpty(educationYearId)) {
            Long eduYearId = Longs.tryParse(educationYearId);
            educationYearNext = get(EducationYear.class, eduYearId);
        }
        else
            educationYearNext = getNextEducationYear();
        IDaoStudentSubscribe idao = DaoStudentSubscribe.instanse();

        DQLSelectBuilder builder = DqlDisciplineBuilder.getDqlInYearDistributionPart(educationYearNext);
        List<YearDistributionPart> partsYear = getList(YearDistributionPart.class, getList(builder));
        List<ElectiveDiscipline> electives = new ArrayList<>();

        for (YearDistributionPart part : partsYear)
            electives.addAll(idao.getElectiveFromRegistryList(educationYearNext, part, student, electiveType, null));
        List<WsElectiveWrap> retVal = new ArrayList<>();

        Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> subscribesMap = getElectiveSubscribesMap(educationYearNext, student, electiveType);
        Map<Long, Integer> subscribersCountMap = getCountSubscribersMap(educationYearNext, electiveType);

        for (ElectiveDiscipline electiveDiscipline : electives) {
            WsElectiveWrap wrap = new WsElectiveWrap(electiveDiscipline);

            ElectiveDisciplineSubscribe subscribe = subscribesMap.get(electiveDiscipline);
            // 	подписаны или нет
            if (subscribe != null && subscribe.getRemovalDate() == null) {
                wrap.setSourceCode(subscribe.getSourceCode());
                wrap.setSubscribed(true);
            }
            else {
                wrap.setSubscribed(false);
            }

            wrap.setFreePlacesToSubscribe("");
            wrap.setPlacesToSubscribe("");

            int subscribeCount = 0;
            if (subscribersCountMap.containsKey(electiveDiscipline.getId())) {
                subscribeCount = subscribersCountMap.get(electiveDiscipline.getId());
            }

            if (electiveDiscipline.isSetTotalPlacesToSubscribe()) {
                wrap.setPlacesToSubscribe(String.valueOf(electiveDiscipline.getPlacesCount()));

                if (electiveDiscipline.getPlacesCount() != null && electiveDiscipline.getPlacesCount() != 0) {
                    long freeToSubscribe = electiveDiscipline.getPlacesCount() - subscribeCount;
                    wrap.setFreePlacesToSubscribe(String.valueOf(freeToSubscribe));
                }
            }


            retVal.add(wrap);
        }

        // Аннотация
        Map<Long, String> annotationMap = getAnnotationMapByElective(electives);
        for (WsElectiveWrap rewrap : retVal) {
            if (annotationMap.get(rewrap.getElectiveDisciplineId()) != null)
                rewrap.setAnnotationExist(true);
        }

        return retVal;
    }

    private Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> getElectiveSubscribesMap(EducationYear educationYear, Student student, String electiveType) {
        Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> subscribesMap = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, "eds")
                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().fromAlias("eds")), educationYear))
                .where(eq(property(ElectiveDisciplineSubscribe.student().fromAlias("eds")), value(student)))
                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().type().code().fromAlias("eds")), electiveType))
                .column("eds");

        List<ElectiveDisciplineSubscribe> subscribes = getList(dql);
        for (ElectiveDisciplineSubscribe subscribe : subscribes) {
            subscribesMap.put(subscribe.getElectiveDiscipline(), subscribe);
        }

        return subscribesMap;
    }

    private Map<Long, String> getAnnotationMapByElective(List<ElectiveDiscipline> electiveDisciplines) {

        Map<Long, String> annotationMap = new HashMap<Long, String>();
        Map<Long, Long> electiveToRegistryElementMap = new HashMap<Long, Long>();

        for (ElectiveDiscipline electiveDiscipline : electiveDisciplines) {
            electiveToRegistryElementMap.put(electiveDiscipline.getId(), electiveDiscipline.getRegistryElementPart().getRegistryElement().getId());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementExt.class, "e")
                .where(in(property(EppRegistryElementExt.eppRegistryElement().id().fromAlias("e")), electiveToRegistryElementMap.values()))
                .column("e");

        List<EppRegistryElementExt> registryElementExts = getList(builder);
        Map<Long, String> registryMap = new HashMap<Long, String>();
        for (EppRegistryElementExt ext : registryElementExts) {
            registryMap.put(ext.getEppRegistryElement().getId(), ext.getAnnotation());
        }

        for (Map.Entry<Long, Long> entry : electiveToRegistryElementMap.entrySet()) {
            annotationMap.put(entry.getKey(), registryMap.get(entry.getValue()));
        }

        return annotationMap;
    }

    @Override
    public String getAnnotationData(String idStr) {

        Long id = Long.parseLong(idStr);
        Object obj = get(id);

        if (obj instanceof EppWorkPlanRegistryElementRow) {
            EppWorkPlanRegistryElementRow workPlanRegistryElementRow = (EppWorkPlanRegistryElementRow) obj;
            Map<Long, String> annotationMap = OptionalDisciplineUtil.getAnnotationMapByWorkPlanRow(Collections.singletonList(workPlanRegistryElementRow));
            return StringUtils.trimToEmpty(annotationMap.get(workPlanRegistryElementRow.getId()));
        }
        else if (obj instanceof ElectiveDiscipline) {

            ElectiveDiscipline electiveDiscipline = (ElectiveDiscipline) obj;
            EppRegistryElementExt registryElementExt = get(EppRegistryElementExt.class, EppRegistryElementExt.eppRegistryElement().id().s(),
                                                                   electiveDiscipline.getRegistryElementPart().getRegistryElement().getId());

            if (registryElementExt != null) {
                return StringUtils.trimToEmpty(registryElementExt.getAnnotation());
            }
            else {
                return "";
            }
        }
        else
            throw new ApplicationException("Не удается загрузить аннотацию для дисциплины");
    }

    /**
     * Мап с итоговым кол-вом подписавшихся
     *
     * @param educationYear
     *
     * @return
     */
    private Map<Long, Integer> getCountSubscribersMap(EducationYear educationYear, String electiveType)
    {
        Map<Long, Integer> retVal = new HashMap<>();

        DQLSelectBuilder builder = DqlDisciplineBuilder.getElectiveDisciplineSUMMSubDQL(educationYear, electiveType);

        List<Object[]> lst = getList(builder);

        for (Object[] objs : lst) {
            Long id = (Long) objs[0];
            Integer count = Integer.parseInt(objs[1].toString());

            retVal.put(id, count);
        }
        return retVal;
    }

    /**
     * Перезавернуть для ws
     *
     * @param wrap
     *
     * @return
     */
    private WsDisciplineWrap rewrap(DisciplineWrap wrap, Map<Long, Integer> mapSubscribed)
    {

        if (wrap.getEppRegistryElementPart() == null)
            return null;

        UniEppFormatter formatter = new UniEppFormatter();

        Long electiveDisciplineId = null;

        if (wrap.getElectiveDiscipline() != null)
            electiveDisciplineId = wrap.getElectiveDiscipline().getId();

        Long idWPRRow = null;
        if (wrap.getEppWorkPlanRegistryElementRow() != null)
            idWPRRow = wrap.getEppWorkPlanRegistryElementRow().getId();

        WsDisciplineWrap disciplineWrapper = new WsDisciplineWrap(idWPRRow, electiveDisciplineId, wrap.getSourceCode());

        if (wrap.getRowIndex() == null)
            disciplineWrapper.setRowIndex("");
        else
            disciplineWrapper.setRowIndex(wrap.getRowIndex());

        if (wrap.getParentId() != null)
            disciplineWrapper.setDisciplineGroupTitle(wrap.getParentName());
        else
            disciplineWrapper.setDisciplineGroupTitle("");

        disciplineWrapper.setIncorrectRow(wrap.isIncorrectRow());
        disciplineWrapper.setWarningRow(wrap.getWarningRow());

        disciplineWrapper.setDisciplineTitle(wrap.getEppRegistryElementPart().getTitle());
        disciplineWrapper.setDisciplineNumber(wrap.getEppRegistryElementPart().getRegistryElement().getNumber());
        disciplineWrapper.setTerm(Integer.toString(wrap.getTerm()));

        ElectiveDiscipline electiveDiscipline = wrap.getElectiveDiscipline();

        disciplineWrapper.setTotalHours(formatter.format(wrap.getEppRegistryElementPart().getSize()));
        disciplineWrapper.setZet(formatter.format(wrap.getEppRegistryElementPart().getLabor()));

        // форма контроля
        disciplineWrapper.setFormOfControl(OptionalDisciplineUtil.getFormsOfControlTitles(wrap.getEppRegistryElementPart().getId()));

        // группа дисциплин
        disciplineWrapper.setDisciplineGroupId(wrap.getParentId());

        // подписаны или нет
        disciplineWrapper.setSubscribed(wrap.isSubcribed());

        disciplineWrapper.setSubscribedToDate("");
        disciplineWrapper.setFreePlacesToSubscribe("");
        disciplineWrapper.setPlacesToSubscribe("");


        if (electiveDiscipline != null) {
            int subscribeCount = 0;
            if (mapSubscribed.containsKey(electiveDiscipline.getId()))
                subscribeCount = mapSubscribed.get(electiveDiscipline.getId());

            if (electiveDiscipline.getSubscribeToDate() != null)
                disciplineWrapper.setSubscribedToDate(new SimpleDateFormat("dd.MM.yyyy").format(electiveDiscipline.getSubscribeToDate()));

            if (electiveDiscipline.isSetTotalPlacesToSubscribe()) {
                disciplineWrapper.setPlacesToSubscribe(String.valueOf(electiveDiscipline.getPlacesCount()));

                if (electiveDiscipline.getPlacesCount() != null
                        && electiveDiscipline.getPlacesCount() != 0)
                {
                    long freeToSubscribe = electiveDiscipline.getPlacesCount() - subscribeCount;
                    disciplineWrapper.setFreePlacesToSubscribe(String.valueOf(freeToSubscribe));
                }
            }
        }


        return disciplineWrapper;
    }

    @Override
    public String subscribeToElectiveDiscipline(String studentParam, List<String> idsElectiveDiscipline, String electiveType, boolean needSendMail, String educationYearId)
    {
        Long studentId = Longs.tryParse(studentParam);
        Student student = (studentId == null)? null : get(Student.class, studentId);
        if (student == null)
            throw new ApplicationException("Cтудент не найден");
        EducationYear educationYearNext;
        if (StringUtils.isNotEmpty(educationYearId)) {
            Long eduYearId = Longs.tryParse(educationYearId);
            educationYearNext = get(EducationYear.class, eduYearId);
        }
        else
            educationYearNext = getNextEducationYear();

        IDaoStudentSubscribe idao = DaoStudentSubscribe.instanse();

        List<Long> lst = new ArrayList<>();

        for (String id_string : idsElectiveDiscipline) {
            Long id = Longs.tryParse(id_string);

            if (id != null && !lst.contains(id))
                lst.add(id);
        }

        StringBuilder resultBuilder = new StringBuilder();

        if (ElectiveDisciplineTypeCodes.DPV.equals(electiveType)) {
            resultBuilder.append(idao.saveStudentSubscribe(student, educationYearNext, lst, "Личный кабинет", needSendMail, true));
        }
        else {

            Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> subscribesMap = getElectiveSubscribesMap(educationYearNext, student, electiveType);

            // подписываем
            List<ElectiveDiscipline> selectedDisciplines = getList(ElectiveDiscipline.class, lst, ElectiveDiscipline.id().s());
            List<ElectiveDisciplineSubscribe> lstElDisc = new ArrayList<>();

            for (ElectiveDiscipline ed : selectedDisciplines) {
                ElectiveDisciplineSubscribe subscribe = subscribesMap.remove(ed);
                if (subscribe == null)
                {
                    subscribe = new ElectiveDisciplineSubscribe(ed, student, new Date(), null, null);
                    lstElDisc.add(subscribe);
                }
                else if (subscribe.getRemovalDate() != null) {
                    lstElDisc.add(subscribe);
                }
            }

            resultBuilder.append(idao.saveElectiveSubscribe(student, lstElDisc, true));

            // отписываем
            List<ElectiveDisciplineSubscribe> unsubscribeLst = new ArrayList<>();
            for (Map.Entry<ElectiveDiscipline, ElectiveDisciplineSubscribe> entry : subscribesMap.entrySet()) {
                if (entry.getValue().getRemovalDate() == null)
                    unsubscribeLst.add(entry.getValue());
            }

            resultBuilder.append(idao.unsubscribeElectiveDisciplines(student, unsubscribeLst, true));

        }
        if (resultBuilder.length() == 0)
            resultBuilder.append("Данные не были изменены.");
        return resultBuilder.toString();
    }

    @Override
    public List<WsEducationYearWrap> getEducationYearList() {

        List<WsEducationYearWrap> retVal = new ArrayList<>();

        EducationYear educationYearCurrent = EducationYear.getCurrentRequired();

        int nextEducationYear = educationYearCurrent.getIntValue() + 1;
        EducationYear educationYearNext = get(EducationYear.class, EducationYear.intValue().s(), nextEducationYear);

        if (educationYearNext == null)
            throw new ApplicationException("Не удалось определить следующий учебный год");

        retVal.add(new WsEducationYearWrap(educationYearCurrent));
        retVal.add(new WsEducationYearWrap(educationYearNext));

        return retVal;
    }

    @Override
    public List<WsOrientationWrap> getWsOrientations(String studentParam)
    {

        Long studentId = Longs.tryParse(studentParam);
        Student student = (studentId == null)? null : get(Student.class, studentId);
        if (student == null)
            throw new ApplicationException("Cтудент не найден");

        List<WsOrientationWrap> orientationWrapList = new ArrayList<>();
        List<Orientation2EduPlanVersion> orientationsList = getOrientations(student);

        Map<EducationOrgUnit, Student2Orientation> orientationSubscribeMap = getOrientationSubscribeMap(student);

        for (Orientation2EduPlanVersion orientation : orientationsList) {
            WsOrientationWrap wsOrientationWrap = new WsOrientationWrap(orientation);

            Student2Orientation student2Orientation = orientationSubscribeMap.get(orientation.getOrientation());

            if (student2Orientation != null) {
                wsOrientationWrap.setSubscribed(Boolean.TRUE);
                wsOrientationWrap.setSource(student2Orientation.getSourceCode());
            }

            orientationWrapList.add(wsOrientationWrap);
        }

        return orientationWrapList;
    }

    private List<Orientation2EduPlanVersion> getOrientations(Student student) {

        int orientationCourse = student.getCourse().getIntValue() + 1;
        EducationYear educationYear = IUniBaseDao.instance.get().get(EducationYear.class, EducationYear.intValue(), EducationYearManager.instance().dao().getCurrent().getIntValue() + 1);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Orientation2EduPlanVersion.class, "o2epv")
                .column("o2epv")
                .where(eqValue(property(Orientation2EduPlanVersion.actual().fromAlias("o2epv")), Boolean.TRUE))
                .where(eqValue(property(Orientation2EduPlanVersion.allowEntry().fromAlias("o2epv")), Boolean.TRUE))
                .where(eqValue(property(Orientation2EduPlanVersion.educationYear().fromAlias("o2epv")), educationYear))
                .joinPath(DQLJoinType.inner, Orientation2EduPlanVersion.orientation().fromAlias("o2epv"), "eou")
                        //ФУТС должны быть одинаковы
                .where(eqValue(property(EducationOrgUnit.formativeOrgUnit().fromAlias("eou")), student.getEducationOrgUnit().getFormativeOrgUnit()))
                .where(eqValue(property(EducationOrgUnit.developForm().fromAlias("eou")), student.getEducationOrgUnit().getDevelopForm()))
                .where(eqValue(property(EducationOrgUnit.developCondition().fromAlias("eou")), student.getEducationOrgUnit().getDevelopCondition()))
                .where(eqValue(property(EducationOrgUnit.developPeriod().fromAlias("eou")), student.getEducationOrgUnit().getDevelopPeriod()))
                .where(eqValue(property(EducationOrgUnit.developTech().fromAlias("eou")), student.getEducationOrgUnit().getDevelopTech()))
                        //учебные планы должен быть одинаковые
                .joinEntity("o2epv", DQLJoinType.inner, EppStudent2EduPlanVersion.class, "st2epv",
                            eq(property(Orientation2EduPlanVersion.eduPlanVersion().id().fromAlias("o2epv")),
                               property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("st2epv"))))
                        //только актуальные УП
                .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("st2epv"))))
                .where(eqValue(property(EppStudent2EduPlanVersion.student().fromAlias("st2epv")), student))
                        // с соответсвующим курсом профилизации
                .where(and(
                        isNotNull(Orientation2EduPlanVersion.course().fromAlias("o2epv").s()),
                        eqValue(property(Orientation2EduPlanVersion.course().intValue().fromAlias("o2epv")), orientationCourse)));

        return builder.createStatement(getSession()).list();
    }

    @Override
    public String subscribeToOrientation(String studentIdStr, String orientationId)
    {

        Long studentId = Longs.tryParse(studentIdStr);
        Student student = (studentId == null)? null : DataAccessServices.dao().get(Student.class, studentId);
        if (student == null) {
            throw new ApplicationException("Cтудент не найден");
        }

        Student2Orientation student2Orientation = getActualSubscribe(student);
        // подписываем
        if (StringUtils.isNotEmpty(orientationId))
        {
            EducationOrgUnit orientation = get(EducationOrgUnit.class, Long.valueOf(orientationId));

            if (orientation == null)
                throw new ApplicationException("Направленность не найдена");

            if (student2Orientation != null && student2Orientation.getOrientation() == orientation)
                return "Вы уже подписаны на данную направленность.";

            return DaoStudentSubscribe.instanse().saveStudentOrientationSubscribe(student, orientation,
                                                                                  student2Orientation, Student2Orientation.SAKAI_SOURCE, false);
        }
        else
        // отписываем
            return DaoStudentSubscribe.instanse().saveStudentOrientationSubscribe(student, null,
                                                    student2Orientation, Student2Orientation.SAKAI_SOURCE, false);

    }

    /**
     * Список студентов по логину
     *
     * @param studentParam
     */
    @Override
    public List<WsStudentWrap> getStudentListByLogin(String studentParam)
    {
        List<Student> students = getStudentFromSakai(studentParam);
        List<WsStudentWrap> resultList = new ArrayList<>(students.size());
        for (Student student : students)
        {
            WsStudentWrap wrap = new WsStudentWrap(student);
            resultList.add(wrap);

        }

        return resultList;
    }

    @Override
    public List<WsWorkPlanInfoWrap> getCountElectiveDisciplines(String studentIdStr, Long eduYearId, String electiveType)
    {
        List<WsWorkPlanInfoWrap> wrappers = new ArrayList<>();
        if (eduYearId == null)
            return wrappers;

        Long studentId = Longs.tryParse(studentIdStr);
        Student student = (studentId == null)? null : DataAccessServices.dao().get(Student.class, studentId);
        EducationYear educationYear = DataAccessServices.dao().get(EducationYear.class, eduYearId);
        DQLSelectBuilder builder = DqlDisciplineBuilder.getDqlInYearDistributionPart(educationYear);
        List<YearDistributionPart> partsYear = getList(YearDistributionPart.class, getList(builder));


        IDaoStudentSubscribe idao = DaoStudentSubscribe.instanse();

        for (YearDistributionPart part : partsYear)
        {
            WsWorkPlanInfoWrap wrap = new WsWorkPlanInfoWrap();
            wrap.setElectiveCount(idao.getCountElectiveInWorkPlan(educationYear, part, student, electiveType));
            wrap.setEduYearId(eduYearId);
            wrap.setElectiveDisciplineType(electiveType);
            wrap.setTerm(part.getTitle());
            wrappers.add(wrap);
        }
        return wrappers;
    }

    private Map<EducationOrgUnit, Student2Orientation> getOrientationSubscribeMap(Student student) {
        Map<EducationOrgUnit, Student2Orientation> orientationSubscribeMap = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Student2Orientation.class, "s2o")
                .where(eq(property(Student2Orientation.student().fromAlias("s2o")), value(student)))
                .where(eqValue(property(Student2Orientation.actual().fromAlias("s2o")), Boolean.TRUE))
                .column("s2o");

        List<Student2Orientation> subscribes = dql.createStatement(getSession()).list();

        for (Student2Orientation student2Orientation : subscribes)
            orientationSubscribeMap.put(student2Orientation.getOrientation(), student2Orientation);

        return orientationSubscribeMap;
    }

    private Student2Orientation getActualSubscribe(Student student) {

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student2Orientation.class, "s2o")
                .where(eqValue(property(Student2Orientation.student().fromAlias("s2o")), student))
                .where(eqValue(property(Student2Orientation.actual().fromAlias("s2o")), Boolean.TRUE));

        return builder.createStatement(getSession()).uniqueResult();
    }
}
