package ru.tandemservice.unitutor.component.registry.RegistryBase;

import ru.tandemservice.uni.dao.IUniDao;

public interface IRegistryBaseDAO<T> extends IUniDao<T> {

    void prepareCommonFilters(T model);
}
