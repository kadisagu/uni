package ru.tandemservice.unitutor.mailprint;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimail.mailprint.BaseMailPrint;
import ru.tandemservice.unimail.mailprint.IMailPrint;

import java.util.HashMap;
import java.util.Map;

public class OrientationSendMail extends BaseMailPrint
{

    private static IMailPrint _IMailPrint = null;

    public static IMailPrint Instanse()
    {
        if (_IMailPrint == null)
            _IMailPrint = (IMailPrint) ApplicationRuntime.getBean("orientationSendMail");
        return _IMailPrint;
    }

    @Override
    protected Map<String, Object> getMapParameters(IEntity entity, Object params) {
        Student student = (Student) entity;
        EducationOrgUnit educationOrgUnit = (EducationOrgUnit) params;

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("fullFio", student.getPerson().getIdentityCard().getFullFio());
        map.put("orientation", educationOrgUnit.getEducationLevelHighSchool().getFullTitle());

        return map;
    }

    @Override
    protected String getTemplateCode() {
        return "orientation";
    }

    @Override
    protected Person getPerson(IEntity entity) {
        Student student = (Student) entity;
        return student.getPerson();
    }

}
