package ru.tandemservice.unitutor.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.component.registry.RegistryBase.EduProgramSubjectGroup;
import ru.tandemservice.unitutor.entity.gen.ElectiveDisciplineGen;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Дисциплина по выбору (ДПВ)
 */
public class ElectiveDiscipline extends ElectiveDisciplineGen implements ITitled {
    public static final String P_COURSE_STR = "courseStr";
    public static final String P_GROUP_STR = "groupStr";

    public ElectiveDiscipline() {
    }

    public ElectiveDiscipline(EppRegistryElementPart registryElementPart, EducationYear educationYear) {
        this.setRegistryElementPart(registryElementPart);
        this.setEducationYear(educationYear);
    }

    public ElectiveDiscipline(EppRegistryElementPart registryElementPart, EducationYear educationYear, Long placesCount) {
        this(registryElementPart, educationYear);
        this.setPlacesCount(placesCount);
    }

    public ElectiveDiscipline(EppRegistryElementPart registryElementPart, EducationYear educationYear, Date subscribeToDate) {
        this(registryElementPart, educationYear);
        this.setSubscribeToDate(subscribeToDate);
    }

    /**
     * Количество студентов-подписчиков на ДПВ на год создания данной ДПВ
     * (только актуальные)
     *
     * @return
     */
    @EntityDSLSupport
    public Long getSubscribersCount() {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, "eds")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().id().fromAlias("eds")),
                        DQLExpressions.value(this.getRegistryElementPart().getId())))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().id().fromAlias("eds")),
                        DQLExpressions.value(this.getEducationYear().getId())))
                .where(DQLExpressions.isNull(DQLExpressions.property(ElectiveDisciplineSubscribe.removalDate().fromAlias("eds"))));
        return new Long(IUniBaseDao.instance.get().getCount(builder));
    }

    /**
     * Количество свободных мест для записи на дисциплину по выбору
     *
     * @return
     */
    @EntityDSLSupport
    public Long getFreePlacesToSubscribe() {
        if (!isSetTotalPlacesToSubscribe())
            return null;
        return (this.getPlacesCount() - this.getSubscribersCount());
    }

    /**
     * Задано ли общее количество мест для записи на дисциплину по выбору
     *
     * @return
     */
    @EntityDSLSupport
    public boolean isSetTotalPlacesToSubscribe() {
        if (this.getPlacesCount() == null)
            return false;

        return true;
    }

    public String getTitle()
    {
        String retVal = "ДПВ: ";

        retVal += " учебный год: " + this.getEducationYear().getTitle() + "; ";
        retVal += " часть: " + this.getYearPart().getTitle() + "; ";
        retVal += " дисциплина: " + this.getRegistryElementPart().getTitle() + "; ";

        return retVal;
    }

    public List<Course> getCourseList() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ElectiveDiscipline2Course.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(ElectiveDiscipline2Course.electiveDiscipline().id().fromAlias("rel")), this.getId()))
                .column(ElectiveDiscipline2Course.course().fromAlias("rel").s())
                .order(ElectiveDiscipline2Course.course().title().fromAlias("rel").s());
        return IUniBaseDao.instance.get().getList(builder);
    }

    public String getCourseStr() {
        if (getCourseList() == null)
            return "";

        List<String> lst = new ArrayList<>();

        for (Course course : getCourseList()) {
            lst.add(course.getTitle());
        }

        return StringUtils.join(lst, ", ");
    }

    public List<EduProgramSubjectGroup> getGroupList() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ElectiveDiscipline2EduGroup.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(ElectiveDiscipline2EduGroup.electiveDiscipline().id().fromAlias("rel")), this.getId()))
                .order(ElectiveDiscipline2EduGroup.title().fromAlias("rel").s());
        List<ElectiveDiscipline2EduGroup> lst = IUniBaseDao.instance.get().getList(builder);
        if (lst == null || lst.isEmpty())
            return null;
        List<EduProgramSubjectGroup> groupList = new ArrayList<>();

        for (ElectiveDiscipline2EduGroup rel : lst) {
            groupList.add(new EduProgramSubjectGroup(rel.getGroupId(), rel.getTitle()));
        }
        return groupList;
    }

    public String getGroupStr() {
        if (getGroupList() == null)
            return "";
        List<String> lst = new ArrayList<>();
        for (EduProgramSubjectGroup group : getGroupList()) {
            lst.add(group.getTitle());
        }
        return StringUtils.join(lst, ", ");
    }
}