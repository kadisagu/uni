package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline2EduGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность связь ДПВ - ID УГН
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ElectiveDiscipline2EduGroupGen extends EntityBase
 implements INaturalIdentifiable<ElectiveDiscipline2EduGroupGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.ElectiveDiscipline2EduGroup";
    public static final String ENTITY_NAME = "electiveDiscipline2EduGroup";
    public static final int VERSION_HASH = -1983224642;
    private static IEntityMeta ENTITY_META;

    public static final String L_ELECTIVE_DISCIPLINE = "electiveDiscipline";
    public static final String P_GROUP_ID = "groupId";
    public static final String P_TITLE = "title";

    private ElectiveDiscipline _electiveDiscipline;     // Дисциплина по выбору (ДПВ, УДВ, УФ)
    private long _groupId;     // ID УГН
    private String _title;     // Название УГН

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина по выбору (ДПВ, УДВ, УФ). Свойство не может быть null.
     */
    @NotNull
    public ElectiveDiscipline getElectiveDiscipline()
    {
        return _electiveDiscipline;
    }

    /**
     * @param electiveDiscipline Дисциплина по выбору (ДПВ, УДВ, УФ). Свойство не может быть null.
     */
    public void setElectiveDiscipline(ElectiveDiscipline electiveDiscipline)
    {
        dirty(_electiveDiscipline, electiveDiscipline);
        _electiveDiscipline = electiveDiscipline;
    }

    /**
     * @return ID УГН. Свойство не может быть null.
     */
    @NotNull
    public long getGroupId()
    {
        return _groupId;
    }

    /**
     * @param groupId ID УГН. Свойство не может быть null.
     */
    public void setGroupId(long groupId)
    {
        dirty(_groupId, groupId);
        _groupId = groupId;
    }

    /**
     * @return Название УГН. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название УГН. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ElectiveDiscipline2EduGroupGen)
        {
            if (withNaturalIdProperties)
            {
                setElectiveDiscipline(((ElectiveDiscipline2EduGroup)another).getElectiveDiscipline());
                setGroupId(((ElectiveDiscipline2EduGroup)another).getGroupId());
                setTitle(((ElectiveDiscipline2EduGroup)another).getTitle());
            }
        }
    }

    public INaturalId<ElectiveDiscipline2EduGroupGen> getNaturalId()
    {
        return new NaturalId(getElectiveDiscipline(), getGroupId(), getTitle());
    }

    public static class NaturalId extends NaturalIdBase<ElectiveDiscipline2EduGroupGen>
    {
        private static final String PROXY_NAME = "ElectiveDiscipline2EduGroupNaturalProxy";

        private Long _electiveDiscipline;
        private long _groupId;
        private String _title;

        public NaturalId()
        {}

        public NaturalId(ElectiveDiscipline electiveDiscipline, long groupId, String title)
        {
            _electiveDiscipline = ((IEntity) electiveDiscipline).getId();
            _groupId = groupId;
            _title = title;
        }

        public Long getElectiveDiscipline()
        {
            return _electiveDiscipline;
        }

        public void setElectiveDiscipline(Long electiveDiscipline)
        {
            _electiveDiscipline = electiveDiscipline;
        }

        public long getGroupId()
        {
            return _groupId;
        }

        public void setGroupId(long groupId)
        {
            _groupId = groupId;
        }

        public String getTitle()
        {
            return _title;
        }

        public void setTitle(String title)
        {
            _title = title;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof ElectiveDiscipline2EduGroupGen.NaturalId) ) return false;

            ElectiveDiscipline2EduGroupGen.NaturalId that = (NaturalId) o;

            if( !equals(getElectiveDiscipline(), that.getElectiveDiscipline()) ) return false;
            if( !equals(getGroupId(), that.getGroupId()) ) return false;
            if( !equals(getTitle(), that.getTitle()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getElectiveDiscipline());
            result = hashCode(result, getGroupId());
            result = hashCode(result, getTitle());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getElectiveDiscipline());
            sb.append("/");
            sb.append(getGroupId());
            sb.append("/");
            sb.append(getTitle());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ElectiveDiscipline2EduGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ElectiveDiscipline2EduGroup.class;
        }

        public T newInstance()
        {
            return (T) new ElectiveDiscipline2EduGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "electiveDiscipline":
                    return obj.getElectiveDiscipline();
                case "groupId":
                    return obj.getGroupId();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "electiveDiscipline":
                    obj.setElectiveDiscipline((ElectiveDiscipline) value);
                    return;
                case "groupId":
                    obj.setGroupId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "electiveDiscipline":
                        return true;
                case "groupId":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "electiveDiscipline":
                    return true;
                case "groupId":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "electiveDiscipline":
                    return ElectiveDiscipline.class;
                case "groupId":
                    return Long.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ElectiveDiscipline2EduGroup> _dslPath = new Path<ElectiveDiscipline2EduGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ElectiveDiscipline2EduGroup");
    }
            

    /**
     * @return Дисциплина по выбору (ДПВ, УДВ, УФ). Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2EduGroup#getElectiveDiscipline()
     */
    public static ElectiveDiscipline.Path<ElectiveDiscipline> electiveDiscipline()
    {
        return _dslPath.electiveDiscipline();
    }

    /**
     * @return ID УГН. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2EduGroup#getGroupId()
     */
    public static PropertyPath<Long> groupId()
    {
        return _dslPath.groupId();
    }

    /**
     * @return Название УГН. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2EduGroup#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends ElectiveDiscipline2EduGroup> extends EntityPath<E>
    {
        private ElectiveDiscipline.Path<ElectiveDiscipline> _electiveDiscipline;
        private PropertyPath<Long> _groupId;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина по выбору (ДПВ, УДВ, УФ). Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2EduGroup#getElectiveDiscipline()
     */
        public ElectiveDiscipline.Path<ElectiveDiscipline> electiveDiscipline()
        {
            if(_electiveDiscipline == null )
                _electiveDiscipline = new ElectiveDiscipline.Path<ElectiveDiscipline>(L_ELECTIVE_DISCIPLINE, this);
            return _electiveDiscipline;
        }

    /**
     * @return ID УГН. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2EduGroup#getGroupId()
     */
        public PropertyPath<Long> groupId()
        {
            if(_groupId == null )
                _groupId = new PropertyPath<Long>(ElectiveDiscipline2EduGroupGen.P_GROUP_ID, this);
            return _groupId;
        }

    /**
     * @return Название УГН. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2EduGroup#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ElectiveDiscipline2EduGroupGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return ElectiveDiscipline2EduGroup.class;
        }

        public String getEntityName()
        {
            return "electiveDiscipline2EduGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
