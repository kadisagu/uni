package ru.tandemservice.unitutor.component.awp.OrientationList.DateAndPlacesEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component) {
        getDao().update(getModel(component));
        deactivate(component);
    }
}
