package ru.tandemservice.unitutor.utils;

import org.tandemframework.core.view.formatter.IFormatter;
import ru.tandemservice.uniepp.UniEppUtils;

public class UniEppFormatter implements IFormatter<Long> {

    @Override
    public String format(Long value) {
        return UniEppUtils.formatLoad(UniEppUtils.wrap(value), false);
    }


}
