package ru.tandemservice.unitutor.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unitutor_2x10x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность physicalFitnessLevel

		// создана новая сущность
		if(!tool.tableExists("physicalfitnesslevel_t")){
			// создать таблицу
			DBTable dbt = new DBTable("physicalfitnesslevel_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_physicalfitnesslevel"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
									  new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
									  new DBColumn("description_p", DBType.createVarchar(255)).setNullable(true)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("physicalFitnessLevel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность applicationStatus4SportsSection

		// создана новая сущность
		if(!tool.tableExists("app_status_4_sports_section_t")){
			// создать таблицу
			DBTable dbt = new DBTable("app_status_4_sports_section_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_f4534874"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
									  new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("applicationStatus4SportsSection");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sportSectionType

		// создана новая сущность
		if(!tool.tableExists("sportsectiontype_t")){
			// создать таблицу
			DBTable dbt = new DBTable("sportsectiontype_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sportsectiontype"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
									  new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sportSectionType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность sportSection

		// создана новая сущность
		if(!tool.tableExists("sportsection_t")){
			// создать таблицу
			DBTable dbt = new DBTable("sportsection_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_sportsection"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("educationyear_id", DBType.LONG).setNullable(false),
									  new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
									  new DBColumn("type_id", DBType.LONG).setNullable(false),
									  new DBColumn("formativeorgunit_id", DBType.LONG),
									  new DBColumn("physicalfitnesslevel_id", DBType.LONG).setNullable(false),
									  new DBColumn("course_id", DBType.LONG).setNullable(false),
									  new DBColumn("sex_id", DBType.LONG).setNullable(false),
									  new DBColumn("numbers_p", DBType.INTEGER).setNullable(false),
									  new DBColumn("hours_p", DBType.INTEGER).setNullable(false),
									  new DBColumn("tutororgunit_id", DBType.LONG).setNullable(false),
									  new DBColumn("trainer_id", DBType.LONG),
									  new DBColumn("place_id", DBType.LONG).setNullable(false),
									  new DBColumn("firstclassday_p", DBType.INTEGER),
									  new DBColumn("firstclasstime_p", DBType.TIME),
									  new DBColumn("secondclassday_p", DBType.INTEGER),
									  new DBColumn("secondclasstime_p", DBType.TIME)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("sportSection");
		}
    }
}