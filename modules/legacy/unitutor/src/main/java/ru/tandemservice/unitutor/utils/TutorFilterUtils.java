package ru.tandemservice.unitutor.utils;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class TutorFilterUtils {

    /**
     * Фильтрация по формирующим подразделениям
     *
     * @param builder
     * @param orgUnitIds
     *
     * @return
     */
    public static DQLSelectBuilder applyOrgUnitFilter(DQLSelectBuilder builder, List<Long> orgUnitIds) {

        if (orgUnitIds != null && !orgUnitIds.isEmpty()) {
            // Добавляем недостающие связи
            TutorBuilderUtils.getEduPlanOnWorkPlanBuilder(builder);
            // Подобная проверка использована Тандемом на определение принадлежности РУПа OrgUnit-у
            builder.where(or(in(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().owner().id().fromAlias(TutorBuilderUtils.EPP_WORK_PLAN_ALIAS)), orgUnitIds),
                             in(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().owner().id().fromAlias(TutorBuilderUtils.EPP_WORK_PLAN_ALIAS)), orgUnitIds),
//TODO DEV-6870
//	    		DQLExpressions.in(
//	    				DQLExpressions.property(EppWorkPlan.parent().educationLevelHighSchool().orgUnit().id().fromAlias(TutorBuilderUtils.EPP_WORK_PLAN_ALIAS)),
//	    				orgUnitIds),
                             exists(
                                     new DQLSelectBuilder()
                                             .fromEntity(EducationOrgUnit.class, "eduOU")
                                             .column(property("eduOU.id"))
                                             .where(or(new IDQLExpression[]{
                                                     in(property(EducationOrgUnit.groupOrgUnit().id().fromAlias("eduOU")), orgUnitIds),
                                                     in(property(EducationOrgUnit.operationOrgUnit().id().fromAlias("eduOU")), orgUnitIds),
                                                     in(property(EducationOrgUnit.formativeOrgUnit().id().fromAlias("eduOU")), orgUnitIds),
                                                     in(property(EducationOrgUnit.territorialOrgUnit().id().fromAlias("eduOU")), orgUnitIds),
                                                     in(property(EducationOrgUnit.educationLevelHighSchool().orgUnit().id().fromAlias("eduOU")), orgUnitIds)}))

//TODO DEV-6870
//	    					.where(DQLExpressions.eq(
//	    						DQLExpressions.property(EppEduPlan.developForm().fromAlias(TutorBuilderUtils.EPP_EDU_PLAN_ALIAS)),
//	    							DQLExpressions.property(EducationOrgUnit.developForm().fromAlias("eduOU"))))
                                             .where(eq(property(EppEduPlan.developCondition().fromAlias(TutorBuilderUtils.EPP_EDU_PLAN_ALIAS)),
                                                     property(EducationOrgUnit.developCondition().fromAlias("eduOU"))))
//TODO DEV-6870
//	    					.where(DQLExpressions.eq(
//	    							DQLExpressions.property(EppEduPlan.developTech().fromAlias(TutorBuilderUtils.EPP_EDU_PLAN_ALIAS)),
//	    							DQLExpressions.property(EducationOrgUnit.developTech().fromAlias("eduOU"))))
                                             .where(eq(property(EppEduPlan.developGrid().developPeriod().fromAlias(TutorBuilderUtils.EPP_EDU_PLAN_ALIAS)),
                                                     property(EducationOrgUnit.developPeriod().fromAlias("eduOU"))))
//TODO DEV-6870
//	    					.where(DQLExpressions.eq(
//	    							DQLExpressions.property(EppEduPlanVersionBlock.educationLevelHighSchool().fromAlias(TutorBuilderUtils.EPP_EDU_PLAN_VERSION_BLOCK_ALIAS)),
//	    							DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("eduOU"))))
                                             .buildQuery())));
        }

        return builder;
    }

    /**
     * Фильтрация по академической группе
     *
     * @param builder
     * @param groupList
     *
     * @return
     */
    public static DQLSelectBuilder applyAcademicalGroupFilter(DQLSelectBuilder builder, List<Group> groupList) {

        if (groupList != null && !groupList.isEmpty()) {
            builder.where(in(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(TutorBuilderUtils.EPP_WORK_PLAN_REGISTRY_ELEMENT_ROW_ALIAS)),
                            TutorBuilderUtils.getWorkPlan2GroupSubDQL(groupList).buildQuery()));
        }

        return builder;
    }

    /**
     * Фильтрация по названиям дисциплин
     *
     * @param builder
     * @param disciplineList
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static DQLSelectBuilder applyDisciplineTitleFilter(DQLSelectBuilder builder, List<IdentifiableWrapper> disciplineList) {

        if (disciplineList != null && !disciplineList.isEmpty()) {
            List<String> disciplineTitleList = new ArrayList<String>();
            for (IdentifiableWrapper discipline : disciplineList) {
                disciplineTitleList.add(discipline.getTitle());
            }

            FilterUtils.applySelectFilter(builder, EppEpvRegistryRow.registryElement().title().fromAlias(TutorBuilderUtils.EPP_EPV_REGISTRY_ROW_ALIAS), disciplineTitleList);
        }
        return builder;
    }
}
