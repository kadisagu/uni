package ru.tandemservice.unitutor.component.awp.OrientationList;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;


public interface IDAO extends IUniDao<Model> {

    @Transactional
    void actualizeRows(Model model);

    @Transactional
    void allowEntry(Long id);

}
