package ru.tandemservice.unitutor.component.awp.move.MoveSubcscibe;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;

import java.util.Collection;

public interface IDAO extends IUniDao<Model> {

    void prepareListDataSourceA(Model model);

    void prepareListDataSourceB(Model model);

    void prepareFilters(Model model);

    @Transactional
    void moveSubscribe(Collection<IEntity> entityList, ElectiveDiscipline electiveDiscipline, boolean sendMail);

    @Transactional
    public void undoSubscribe(Collection<IEntity> entityList, ElectiveDiscipline electiveDiscipline, boolean sendMail);

}
