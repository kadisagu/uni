package ru.tandemservice.unitutor.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitutor.entity.gen.ElectiveDisciplineSubscribeGen;

import java.util.Date;

/**
 * Запись на дисциплину по выбору
 */
public class ElectiveDisciplineSubscribe extends ElectiveDisciplineSubscribeGen
{
    public final static String TANDEM_SOURCE = "TANDEM";
    public final static String SAKAI_SOURCE = "SAKAI";

    public ElectiveDisciplineSubscribe() {
    }

    public ElectiveDisciplineSubscribe(
            ElectiveDiscipline electiveDiscipline,
            Student student,
            Date subscribeOn,
            String subscribeSource,
            Long transactionId)
    {
        this.setElectiveDiscipline(electiveDiscipline);
        this.setStudent(student);
        this.setSubscribeOn(subscribeOn);
        this.setSource(subscribeSource);
        this.setTransactionId(transactionId);
    }

    @EntityDSLSupport
    public String getSourceCode() {
        if (this.getSource().contains("Личный кабинет"))
            return SAKAI_SOURCE;

        return TANDEM_SOURCE;
    }
}