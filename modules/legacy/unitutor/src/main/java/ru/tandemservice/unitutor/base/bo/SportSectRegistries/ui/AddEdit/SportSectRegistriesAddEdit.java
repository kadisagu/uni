/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.logic.DefaultActiveCatalogComboDSHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.SportSectRegistriesManager;
import ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel;
import ru.tandemservice.unitutor.entity.catalog.SportSectionType;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 23.08.2016
 */
@Configuration
public class SportSectRegistriesAddEdit extends BusinessComponentManager
{
    public static final String SOURCE_ID_4_COPY = "source4Copy";

    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String SECTION_TYPE_DS = "sectionTypeDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String PHYSICAL_FITNESS_LEVEL_DS = "physicalFitnessLevelDS";

    public static final String COURSE_DS = "courseDS";
    public static final String COURSE_LIST_PARAM = "courseList";

    public static final String SEX_DS = "sexDS";
    public static final String SEX_LIST_PARAM = "sexList";

    public static final String TUTOR_ORG_UNIT_DS = "tutorOrgUnitDS";
    public static final String TUTOR_ORG_UNIT_PARAM = "tutorOrgUnit";

    public static final String TRAINER_DS = "trainerDS";
    public static final String PLACE_DS = "placeDS";

    public static final String DAY_OF_WEEK_DS = "dayOfWeekDS";
    public static final String FIRST_CLASS_DAY_PARAM = "firstClassDay";
    public static final String SECOND_CLASS_DAY_PARAM = "secondClassDay";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(selectDS(SECTION_TYPE_DS, sectionTypeDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(PHYSICAL_FITNESS_LEVEL_DS, physicalFitnessLevelDSHandler()))
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(SEX_DS, sexDSHandler()))
                .addDataSource(selectDS(TUTOR_ORG_UNIT_DS, tutorOrgUnitDSHandler()))
                .addDataSource(selectDS(TRAINER_DS, trainerDSHandler()))
                .addDataSource(selectDS(PLACE_DS, SportSectRegistriesManager.instance().sportPlaceDSHandler())
                                       .addColumn("title", "", source -> ((UniplacesPlace) source).getTitleWithLocation()))
                .addDataSource(selectDS(DAY_OF_WEEK_DS, SportSectRegistriesManager.instance().dayOfWeekDSHandler()))
                .create();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sectionTypeDSHandler()
    {
        return SportSectionType.defaultSelectDSHandler(this.getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> physicalFitnessLevelDSHandler()
    {
        return PhysicalFitnessLevel.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return Course.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sexDSHandler()
    {
        return new DefaultActiveCatalogComboDSHandler(this.getName(), Sex.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler tutorOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
                .customize((alias, dql, context, filter) -> dql.where(exists(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit().s(), property(alias))))
                .order(OrgUnit.title())
                .where(OrgUnit.archival(), false)
                .filter(OrgUnit.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> trainerDSHandler()
    {
        return PpsEntry.defaultSelectDSHandler(getName())
                .customize((alias, dql, context, filter) -> dql.where(eq(property(alias, PpsEntry.orgUnit()), value((OrgUnit) context.get(TUTOR_ORG_UNIT_PARAM)))));
    }
}
