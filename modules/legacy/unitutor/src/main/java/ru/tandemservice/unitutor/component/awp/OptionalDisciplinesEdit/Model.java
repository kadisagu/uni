package ru.tandemservice.unitutor.component.awp.OptionalDisciplinesEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;
import ru.tandemservice.unitutor.utils.UniEppFormatter;

@Input({
        @Bind(key = "registryElementPartId", binding = "registryElementPartId"),
        @Bind(key = "electiveDisciplineId", binding = "electiveDisciplineId"),
        @Bind(key = "educationYearId", binding = "educationYearId"),
        @Bind(key = "yearDistributionPartId", binding = "yearDistributionPartId")
})
public class Model {

    public static final UniEppFormatter formatter = new UniEppFormatter();

    private Long registryElementPartId;
    private Long electiveDisciplineId;
    private Long yearDistributionPartId;
    private Long educationYearId;

    private ElectiveDiscipline electiveDiscipline;
    private EppRegistryElementExt registryElementExt;
    private EducationYear educationYear;
    private YearDistributionPart yearPart;
    private EppRegistryElementPart disciplinePart;

    private String totalSize;
    private String totalLabor;
    private String placesTotal;
    private String placesFree;
    private String subscribed;
    private String formsOfControl;

    public Long getRegistryElementPartId() {
        return registryElementPartId;
    }

    public void setRegistryElementPartId(Long registryElementPartId) {
        this.registryElementPartId = registryElementPartId;
    }

    public Long getElectiveDisciplineId() {
        return electiveDisciplineId;
    }

    public void setElectiveDisciplineId(Long electiveDisciplineId) {
        this.electiveDisciplineId = electiveDisciplineId;
    }

    public ElectiveDiscipline getElectiveDiscipline() {
        return electiveDiscipline;
    }

    public void setElectiveDiscipline(ElectiveDiscipline electiveDiscipline) {
        this.electiveDiscipline = electiveDiscipline;
    }

    public EppRegistryElementPart getDisciplinePart() {
        return disciplinePart;
    }

    public void setDisciplinePart(EppRegistryElementPart disciplinePart) {
        this.disciplinePart = disciplinePart;
    }

    public String getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(String totalSize) {
        this.totalSize = totalSize;
    }

    public String getTotalLabor() {
        return totalLabor;
    }

    public void setTotalLabor(String totalLabor) {
        this.totalLabor = totalLabor;
    }

    public String getPlacesTotal() {
        return placesTotal;
    }

    public void setPlacesTotal(String placesTotal) {
        this.placesTotal = placesTotal;
    }

    public String getPlacesFree() {
        return placesFree;
    }

    public void setPlacesFree(String placesFree) {
        this.placesFree = placesFree;
    }

    public String getFormsOfControl() {
        return formsOfControl;
    }

    public void setFormsOfControl(String formsOfControl) {
        this.formsOfControl = formsOfControl;
    }

    public Long getYearDistributionPartId() {
        return yearDistributionPartId;
    }

    public void setYearDistributionPartId(Long yearDistributionPartId) {
        this.yearDistributionPartId = yearDistributionPartId;
    }

    public Long getEducationYearId() {
        return educationYearId;
    }

    public void setEducationYearId(Long educationYearId) {
        this.educationYearId = educationYearId;
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public YearDistributionPart getYearPart() {
        return yearPart;
    }

    public void setYearPart(YearDistributionPart yearPart) {
        this.yearPart = yearPart;
    }

    public String getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(String subscribed) {
        this.subscribed = subscribed;
    }

    public EppRegistryElementExt getRegistryElementExt() {
        return registryElementExt;
    }

    public void setRegistryElementExt(EppRegistryElementExt registryElementExt) {
        this.registryElementExt = registryElementExt;
    }
}