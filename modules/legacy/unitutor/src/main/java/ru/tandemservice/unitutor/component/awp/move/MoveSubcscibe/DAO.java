package ru.tandemservice.unitutor.component.awp.move.MoveSubcscibe;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.unitutor.component.awp.base.utils.SelectModelUtils;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.dao.IDaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;
import ru.tandemservice.unitutor.utils.TutorBuilderUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        // по умолчанию - посылаем уведомления
        model.setSendMail(true);

        if (model.getEmployeeId() != null) {
            Employee employee = get(Employee.class, model.getEmployeeId());
            model.setEmployee(employee);
        }

        if (model.getElectiveDisciplineId() != null) {
            ElectiveDiscipline electiveDiscipline = get(ElectiveDiscipline.class, model.getElectiveDisciplineId());
            model.setElectiveDiscipline(electiveDiscipline);
        }

        if (model.getOrgUnitId() != null) {
            OrgUnit orgUnit = get(OrgUnit.class, model.getOrgUnitId());
            model.setOrgUnit(orgUnit);
        }
        // сбросить группу
        model.getSettings().set("group", null);
        // сбросить выбор Б
        model.getSettings().set("electiveDisciplineB", null);

        prepareFilters(model);
    }

    @Override
    public void prepareListDataSourceA(Model model)
    {
        ElectiveDiscipline ed = model.getElectiveDiscipline();

        DQLSelectBuilder dql = getDqlListDataSource(model, ed);

        UniBaseUtils.createPage(model.getDataSourceA(), dql, getSession());

        if (model.getDataSourceA().getEntityOrder() != null) {
            Collections.sort(
                    model.getDataSourceA().getEntityList()
                    , new EntityComparator<>(model.getDataSourceA().getEntityOrder()));
        }

    }

    @Override
    public void prepareListDataSourceB(Model model)
    {
        ElectiveDiscipline ed = null;

        if (model.getSettings().get("electiveDisciplineB") != null) {
            List<ElectiveDiscipline> lst = model.getSettings().get("electiveDisciplineB");
            if (lst != null && !lst.isEmpty())
                ed = lst.get(0);
        }

        DQLSelectBuilder dql = getDqlListDataSource(model, ed);

        UniBaseUtils.createPage(model.getDataSourceB(), dql, getSession());

        if (model.getDataSourceB().getEntityOrder() != null) {
            Collections.sort(
                    model.getDataSourceB().getEntityList()
                    , new EntityComparator<>(model.getDataSourceB().getEntityOrder()));
        }
    }

    private DQLSelectBuilder getDqlListDataSource(Model model, ElectiveDiscipline ed)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, TutorBuilderUtils.ELECTIVE_DISCIPLINE_SUBSCRIBE_ALIAS);
        filter(model, ed, dql);
        return dql;
    }

    private DQLSelectBuilder filter(Model model, ElectiveDiscipline ed, DQLSelectBuilder builder)
    {
        if (ed == null) // не может быть пусто, если ed не указали - ничего не выводим
        {
            builder.where(eqValue(property(ElectiveDisciplineSubscribe.id().fromAlias(TutorBuilderUtils.ELECTIVE_DISCIPLINE_SUBSCRIBE_ALIAS)), 1L));
            return builder;
        }
        else {
            // по ДПВ
            builder.where(eqValue(
                    property(ElectiveDisciplineSubscribe.electiveDiscipline().id().fromAlias(TutorBuilderUtils.ELECTIVE_DISCIPLINE_SUBSCRIBE_ALIAS)),
                    ed.getId()));
        }

        // по преподаватель
        if (model.getEmployeeId() != null)
            FilterUtils.applySelectFilter(builder, ElectiveDisciplineSubscribe.student().group().curator().employee().id().fromAlias(TutorBuilderUtils.ELECTIVE_DISCIPLINE_SUBSCRIBE_ALIAS),
                                          model.getEmployeeId());

        // актуальные записи
        builder.where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias(TutorBuilderUtils.ELECTIVE_DISCIPLINE_SUBSCRIBE_ALIAS))));

        // актуальные строки	
        builder.where(eq(property(ElectiveDisciplineSubscribe.electiveDiscipline().actualRow().fromAlias(TutorBuilderUtils.ELECTIVE_DISCIPLINE_SUBSCRIBE_ALIAS)), value(true)));

        return builder;
    }

    @Override
    public void prepareFilters(Model model)
    {
        List<EppEpvGroupImRow> eppEpvGroupImRow = getGroupList(model);
        if (model.getElectiveDiscipline() != null) {

            // фильтр а не нужен как выяснилось
            ElectiveDiscipline ed = model.getElectiveDiscipline();
            model.setElectiveDisciplineASelectModel(SelectModelUtils.getElectiveDisciplineListModel(ed.getEducationYear(), ed.getYearPart(), null, model.getFilterEmployee(),
                                                    model.getFilterOrgUnit(), model.getFilterCathedraOrgUnit(), null));

            model.setGroupListModel(new LazySimpleSelectModel<>(SelectModelUtils.getEppEpvGroupImRowListSelectModel(ed, model.getFilterEmployee(), model.getFilterOrgUnit())));

            // По группе дисуиплин, исключая дисциплину А
            model.setElectiveDisciplineBSelectModel(SelectModelUtils.getElectiveDisciplineListModel(ed.getEducationYear(), ed.getYearPart(), eppEpvGroupImRow,
                                                    model.getFilterEmployee(), model.getFilterOrgUnit(), model.getFilterCathedraOrgUnit(), ed));
        }

    }

    @SuppressWarnings("rawtypes")
    private List<EppEpvGroupImRow> getGroupList(Model model)
    {
        List<EppEpvGroupImRow> retVal = new ArrayList<>();
        if (model.getSettings().get("group") != null) {
            IdentifiableWrapper groupFilter = model.getSettings().get("group");
            EppEpvGroupImRow group = get(EppEpvGroupImRow.class, groupFilter.getId());
            retVal.add(group);
        }
        else {
            if (model.getElectiveDiscipline() != null) {
                ElectiveDiscipline ed = model.getElectiveDiscipline();
                DQLSelectBuilder dql = DqlDisciplineBuilder.getDqlEppEpvGroupImRow(ed.getEducationYear(), ed.getYearPart(), model.getFilterEmployee(),
                        model.getFilterOrgUnit(), ed.getRegistryElementPart().getRegistryElement());

                retVal = getList(dql);
            }
        }
        return retVal;
    }

    @Override
    @Transactional
    public void moveSubscribe(Collection<IEntity> entityList, ElectiveDiscipline electiveDiscipline, boolean sendMail)
    {
        IDaoStudentSubscribe idao = DaoStudentSubscribe.instanse();
        Map<Long, Long> transactionMap = new HashMap<>();

        List<Student> lstStudent = new ArrayList<>();
        EducationYear year = null;

        for (IEntity ent : entityList) {
            if (ent instanceof ElectiveDisciplineSubscribe) {
                // отписать
                ElectiveDisciplineSubscribe electiveDisciplineSubscribe = (ElectiveDisciplineSubscribe) ent;

                // всегда из одной части
                year = electiveDisciplineSubscribe.getElectiveDiscipline().getEducationYear();

                Student student = electiveDisciplineSubscribe.getStudent();

                Long transactionId;
                if (!transactionMap.containsKey(student.getId())) {
                    transactionId = idao.getTransactionId();
                    transactionMap.put(student.getId(), transactionId);
                }
                else
                    transactionId = transactionMap.get(student.getId());

                idao.usubscribe(electiveDisciplineSubscribe, IDaoStudentSubscribe.MOVED_ROW, false, transactionId, false);
                // приписать
                idao.saveStudentSubscribe(student, electiveDiscipline, null, IDaoStudentSubscribe.MOVED_ROW, sendMail, false, transactionId);
                lstStudent.add(student);
            }
        }

        // посылка по почте
        if (!lstStudent.isEmpty() && sendMail) {
            for (Student student : lstStudent) {
                Long transactionId = transactionMap.get(student.getId());
                idao.sendMail(student, year, transactionId, false);
            }
        }
    }

    @Override
    @Transactional
    public void undoSubscribe(Collection<IEntity> entityList, ElectiveDiscipline electiveDiscipline, boolean sendMail)
    {
        moveSubscribe(entityList, electiveDiscipline, sendMail);
    }


}
