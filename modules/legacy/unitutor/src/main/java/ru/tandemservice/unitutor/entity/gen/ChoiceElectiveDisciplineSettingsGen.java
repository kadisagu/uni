package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки выбора дисциплин по умолчанию
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChoiceElectiveDisciplineSettingsGen extends EntityBase
 implements INaturalIdentifiable<ChoiceElectiveDisciplineSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings";
    public static final String ENTITY_NAME = "choiceElectiveDisciplineSettings";
    public static final int VERSION_HASH = 994368642;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_YEAR_PART = "yearPart";
    public static final String P_DATE = "date";
    public static final String P_COUNT_PLACES = "countPlaces";

    private EducationYear _educationYear;     // Учебный год
    private YearDistributionPart _yearPart;     // Часть учебного года
    private Date _date;     // Запись до
    private int _countPlaces;     // Количество мест всего

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года. Свойство не может быть null.
     */
    public void setYearPart(YearDistributionPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Запись до. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Запись до. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Количество мест всего. Свойство не может быть null.
     */
    @NotNull
    public int getCountPlaces()
    {
        return _countPlaces;
    }

    /**
     * @param countPlaces Количество мест всего. Свойство не может быть null.
     */
    public void setCountPlaces(int countPlaces)
    {
        dirty(_countPlaces, countPlaces);
        _countPlaces = countPlaces;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ChoiceElectiveDisciplineSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setEducationYear(((ChoiceElectiveDisciplineSettings)another).getEducationYear());
                setYearPart(((ChoiceElectiveDisciplineSettings)another).getYearPart());
            }
            setDate(((ChoiceElectiveDisciplineSettings)another).getDate());
            setCountPlaces(((ChoiceElectiveDisciplineSettings)another).getCountPlaces());
        }
    }

    public INaturalId<ChoiceElectiveDisciplineSettingsGen> getNaturalId()
    {
        return new NaturalId(getEducationYear(), getYearPart());
    }

    public static class NaturalId extends NaturalIdBase<ChoiceElectiveDisciplineSettingsGen>
    {
        private static final String PROXY_NAME = "ChoiceElectiveDisciplineSettingsNaturalProxy";

        private Long _educationYear;
        private Long _yearPart;

        public NaturalId()
        {}

        public NaturalId(EducationYear educationYear, YearDistributionPart yearPart)
        {
            _educationYear = ((IEntity) educationYear).getId();
            _yearPart = ((IEntity) yearPart).getId();
        }

        public Long getEducationYear()
        {
            return _educationYear;
        }

        public void setEducationYear(Long educationYear)
        {
            _educationYear = educationYear;
        }

        public Long getYearPart()
        {
            return _yearPart;
        }

        public void setYearPart(Long yearPart)
        {
            _yearPart = yearPart;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof ChoiceElectiveDisciplineSettingsGen.NaturalId) ) return false;

            ChoiceElectiveDisciplineSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getEducationYear(), that.getEducationYear()) ) return false;
            if( !equals(getYearPart(), that.getYearPart()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEducationYear());
            result = hashCode(result, getYearPart());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEducationYear());
            sb.append("/");
            sb.append(getYearPart());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChoiceElectiveDisciplineSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChoiceElectiveDisciplineSettings.class;
        }

        public T newInstance()
        {
            return (T) new ChoiceElectiveDisciplineSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "educationYear":
                    return obj.getEducationYear();
                case "yearPart":
                    return obj.getYearPart();
                case "date":
                    return obj.getDate();
                case "countPlaces":
                    return obj.getCountPlaces();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "yearPart":
                    obj.setYearPart((YearDistributionPart) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "countPlaces":
                    obj.setCountPlaces((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "educationYear":
                        return true;
                case "yearPart":
                        return true;
                case "date":
                        return true;
                case "countPlaces":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "educationYear":
                    return true;
                case "yearPart":
                    return true;
                case "date":
                    return true;
                case "countPlaces":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "educationYear":
                    return EducationYear.class;
                case "yearPart":
                    return YearDistributionPart.class;
                case "date":
                    return Date.class;
                case "countPlaces":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChoiceElectiveDisciplineSettings> _dslPath = new Path<ChoiceElectiveDisciplineSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChoiceElectiveDisciplineSettings");
    }
            

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings#getYearPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Запись до. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Количество мест всего. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings#getCountPlaces()
     */
    public static PropertyPath<Integer> countPlaces()
    {
        return _dslPath.countPlaces();
    }

    public static class Path<E extends ChoiceElectiveDisciplineSettings> extends EntityPath<E>
    {
        private EducationYear.Path<EducationYear> _educationYear;
        private YearDistributionPart.Path<YearDistributionPart> _yearPart;
        private PropertyPath<Date> _date;
        private PropertyPath<Integer> _countPlaces;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть учебного года. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings#getYearPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Запись до. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(ChoiceElectiveDisciplineSettingsGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Количество мест всего. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings#getCountPlaces()
     */
        public PropertyPath<Integer> countPlaces()
        {
            if(_countPlaces == null )
                _countPlaces = new PropertyPath<Integer>(ChoiceElectiveDisciplineSettingsGen.P_COUNT_PLACES, this);
            return _countPlaces;
        }

        public Class getEntityClass()
        {
            return ChoiceElectiveDisciplineSettings.class;
        }

        public String getEntityName()
        {
            return "choiceElectiveDisciplineSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
