/* $Id$ */
package ru.tandemservice.unitutor.component.registry.DisciplineRegistry.Pub;

import ru.tandemservice.unitutor.entity.EppRegistryElementExt;

/**
 * @author Ekaterina Zvereva
 * @since 04.02.2016
 */
public class Model extends ru.tandemservice.uniepp.component.registry.DisciplineRegistry.Pub.Model
{
    private EppRegistryElementExt _registryElementExt;
    private String _languageStr;

    public EppRegistryElementExt getRegistryElementExt()
    {
        return _registryElementExt;
    }

    public void setRegistryElementExt(EppRegistryElementExt registryElementExt)
    {
        _registryElementExt = registryElementExt;
    }

    public String getLanguageStr()
    {
        return _languageStr;
    }

    public void setLanguageStr(String languageStr)
    {
        _languageStr = languageStr;
    }
}