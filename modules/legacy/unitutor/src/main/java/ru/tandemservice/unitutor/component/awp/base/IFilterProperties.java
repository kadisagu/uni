package ru.tandemservice.unitutor.component.awp.base;


public interface IFilterProperties {

    String EDUCATION_YEAR_FILTER = "educationYear";
    String EDUCATION_YEAR_PART_FILTER = "educationYearPart";
    String EDUCATION_LEVEL_HIGH_SCHOOL_FILTER = "educationLevelsHighSchool";
    String GROUP_FILTER = "groups";
    String DISCIPLINE_FILTER = "disciplines";
    String PLACES_FILTER = "freeStatus";
    String RELEVANCE_FILTER = "relevance";
    String STUDENT_FILTER = "student";
    String ACTUAL_FILTER = "actualStatus";
    String ACTUAL_ELECTIVE_DISCIPLINE = "actualElectiveDiscipline";

    String EPP_EDUPLAN_FILTER = "eduPlans";
    String EPP_WORKPLAN_FILTER = "workEduPlans";

    String ELECTIVE_DISCIPLINE_TYPE_FILTER = "electiveDisciplineType";
    String COURSE_FILTER = "course";
    String EDU_GROUP_FILTER = "eduGroup";

}

