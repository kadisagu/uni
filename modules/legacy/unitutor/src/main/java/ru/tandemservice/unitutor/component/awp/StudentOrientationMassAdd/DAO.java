package ru.tandemservice.unitutor.component.awp.StudentOrientationMassAdd;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;
import ru.tandemservice.unitutor.entity.Student2Orientation;
import ru.tandemservice.unitutor.entity.EppEduPlanVersionExt;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    private static final String VERSION_ALIAS = "version";
    private static final String STUDENT_ALIAS = "student";
    private static final String VERSION_EXT_ALIAS = "versionExt";
    private static final String REL_ALIAS = "rel";

    @Override
    public void prepare(final Model model) {
        model.setEduLevelModel(new DQLFullCheckSelectModel(EducationLevelsHighSchool.fullTitle()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                //берем НПП, для которых данное подразделение является формирующим
                DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                        .fromEntity(EducationOrgUnit.class, "eou")
                        .where(eq(property(EducationOrgUnit.formativeOrgUnit().id().fromAlias("eou")), value(model.getOrgUnitId())))
                        .distinct()
                        .column(property(EducationOrgUnit.educationLevelHighSchool().id().fromAlias("eou")));

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, alias)
                        .where(in(property(alias), dqlIn.buildQuery()));
                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle(), filter);
                return builder;
            }
        });

        model.setEduPlanModel(new DQLFullCheckSelectModel(EppEduPlanVersion.fullTitle().s())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(Orientation2EduPlanVersion.eduPlanVersion().fromAlias(REL_ALIAS).s());
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersion.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));
                FilterUtils.applyLikeFilter(builder, filter, EppEduPlanVersion.number().fromAlias(alias),
                                            EppEduPlanVersion.eduPlan().number().fromAlias(alias));
                return builder;
            }
        });

        model.setGroupModel(new DQLFullCheckSelectModel(Group.title())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, alias);

                Set<String> kindSet = new HashSet<>();
                for (OrgUnitKind orgUnitKind : UniDaoFacade.getOrgstructDao().getOrgUnitKindList(get(OrgUnit.class, model.getOrgUnitId())))
                {
                    if (orgUnitKind.isAllowGroups())
                        kindSet.add(orgUnitKind.getCode());
                }
                IDQLExpression condition = eq(property(alias, "id"), value(0L));

                if (kindSet.contains("1"))
                    condition = or(condition, eq(property(alias, Group.educationOrgUnit().educationLevelHighSchool().orgUnit().id()), value(model.getOrgUnitId())));

                if (kindSet.contains("2"))
                    condition = or(condition, eq(property(alias, Group.educationOrgUnit().formativeOrgUnit().id()), value(model.getOrgUnitId())));

                if (kindSet.contains("3"))
                    condition = or(condition, eq(property(alias, Group.educationOrgUnit().territorialOrgUnit().id()), value(model.getOrgUnitId())));

                builder.where(condition);

                EducationLevels parentLevel = EducationOrgUnitUtil.getParentLevel(model.getEduLevel());
                if (parentLevel != null)
                {
                    builder.where(or(
                            eq(property(alias, Group.educationOrgUnit().educationLevelHighSchool().educationLevel()), value(parentLevel)),
                            eq(property(alias, Group.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel()), value(parentLevel))));
                }
                else
                    FilterUtils.applySelectFilter(builder, alias, Group.educationOrgUnit().educationLevelHighSchool(), model.getEduLevel());

                FilterUtils.applySimpleLikeFilter(builder, alias, Group.title(), filter);
                builder.order(property(alias, Group.title()));
                return builder;
            }
        });

        model.setOrientationSelectModel(new DQLFullCheckSelectModel(EducationOrgUnit.educationLevelHighSchool().fullTitle())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder subBuilder = getBuilder(model)
                        .column(Orientation2EduPlanVersion.orientation().fromAlias(REL_ALIAS).s());
                FilterUtils.applySelectFilter(subBuilder, REL_ALIAS, Orientation2EduPlanVersion.eduPlanVersion(), model.getEduPlanVersion());
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, alias)
                        .where(in(property(alias), subBuilder.buildQuery()));

                FilterUtils.applySimpleLikeFilter(builder, alias, EducationOrgUnit.educationLevelHighSchool().fullTitle(), filter);

                return builder;
            }
        });
    }

    private DQLSelectBuilder getBuilder(Model model) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Orientation2EduPlanVersion.class, REL_ALIAS)
                .where(eqValue(property(Orientation2EduPlanVersion.orientation().formativeOrgUnit().id().fromAlias(REL_ALIAS)), model.getOrgUnitId()))
                .where(eqValue(property(Orientation2EduPlanVersion.educationYear().fromAlias(REL_ALIAS)), model.getEducationYear()))
                .where(eqValue(property(Orientation2EduPlanVersion.actual().fromAlias(REL_ALIAS)), Boolean.TRUE))
                .where(eqValue(property(Orientation2EduPlanVersion.allowEntry().fromAlias(REL_ALIAS)), Boolean.TRUE))
                .joinPath(DQLJoinType.inner, Orientation2EduPlanVersion.eduPlanVersion().fromAlias(REL_ALIAS), VERSION_ALIAS)
                        //расширение УПв
                .joinEntity(REL_ALIAS, DQLJoinType.left, EppEduPlanVersionExt.class, VERSION_EXT_ALIAS,
                            eq(property(EppEduPlanVersionExt.eduPlanVersion().id().fromAlias(VERSION_EXT_ALIAS)),
                                    property(EppEduPlanVersion.id().fromAlias(VERSION_ALIAS))));

//		FilterUtils.applySelectFilter(builder, REL_ALIAS, Orientation2EduPlanVersion.eduPlanVersion().eduPlan().educationLevelHighSchool(), model.getEduLevel());
//        FilterUtils.applySelectFilter(builder, REL_ALIAS, Orientation2EduPlanVersion.eduPlanVersion(), model.getEduPlanVersion());

        return builder;
    }

    @Override
    public void prepareListDataSource(Model model) {

        DynamicListDataSource<Student> dataSource = model.getDataSource();

        if (model.getEduLevel() == null || model.getEduPlanVersion() == null) {
            dataSource.setTotalSize(0);
            dataSource.createPage(new ArrayList<>());
            return;
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, STUDENT_ALIAS)
                .where(eq(property(Student.educationOrgUnit().formativeOrgUnit().id().fromAlias(STUDENT_ALIAS)), value(model.getOrgUnitId())));
        FilterUtils.applySelectFilter(builder, STUDENT_ALIAS, Student.group(), model.getGroups());
                        //УП студента
        builder.joinEntity(STUDENT_ALIAS, DQLJoinType.inner, EppStudent2EduPlanVersion.class, "st2v",
                    eq(property(EppStudent2EduPlanVersion.student().id().fromAlias("st2v")), property(Student.id().fromAlias(STUDENT_ALIAS)))
        )
                //только актуальные УП
        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("st2v"))))
        .joinPath(DQLJoinType.inner, EppStudent2EduPlanVersion.eduPlanVersion().fromAlias("st2v"), VERSION_ALIAS)
        .joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias(STUDENT_ALIAS), "eou")
        .joinPath(DQLJoinType.inner, EppEduPlanVersion.eduPlan().fromAlias(VERSION_ALIAS), "plan")
                //Расширение УП студента
        .joinEntity(VERSION_ALIAS, DQLJoinType.left, EppEduPlanVersionExt.class, VERSION_EXT_ALIAS,
                    eq(
                            property(EppEduPlanVersionExt.eduPlanVersion().id().fromAlias(VERSION_EXT_ALIAS)),
                            property(EppEduPlanVersion.id().fromAlias(VERSION_ALIAS))))
        .where(eqValue(property(Student.educationOrgUnit().educationLevelHighSchool().fromAlias(STUDENT_ALIAS)), model.getEduLevel()))
        .where(eqValue(property(EppEduPlanVersion.id().fromAlias(VERSION_ALIAS)), model.getEduPlanVersion().getId()))
                //ФУТС студента и УП должны совпадать
        .where(and(
                eq(property(EducationOrgUnit.developForm().programForm().fromAlias("eou")), property(EppEduPlan.programForm().fromAlias("plan"))),
                eq(property(EducationOrgUnit.developCondition().fromAlias("eou")), property(EppEduPlan.developCondition().fromAlias("plan"))),
                eq(property(EducationOrgUnit.developPeriod().fromAlias("eou")), property(EppEduPlan.developGrid().developPeriod().fromAlias("plan"))),
                eqNullSafe(property(EducationOrgUnit.developTech().programTrait().fromAlias("eou")), property(EppEduPlan.programTrait().fromAlias("plan")))
        ))
                //Если в УП курс начала профилизации = 3, то показываем всех студентов 2 курса
        .where(eq(plus(property(Student.course().intValue().fromAlias(STUDENT_ALIAS)), value(1)), property(EppEduPlanVersionExt.course().intValue().fromAlias(VERSION_EXT_ALIAS))))
        .column(STUDENT_ALIAS);



        new DQLOrderDescriptionRegistry(Student.class, STUDENT_ALIAS).applyOrder(builder, dataSource.getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());

        DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Student2Orientation.class, "st2o")
                .where(eqValue(property(Student2Orientation.actual().fromAlias("st2o")), Boolean.TRUE))
                .where(in(property(Student2Orientation.student().fromAlias("st2o")), builder.buildQuery()));

        Map<Long, Student2Orientation> st2Or = new HashMap<>();

        List<Student2Orientation> objs = getList(subBuilder);

        for (Student2Orientation obj : objs) {
            st2Or.put(obj.getStudent().getId(), obj);
        }


        List<ViewWrapper<Student>> patchedList = ViewWrapper.getPatchedList(dataSource);

        for (ViewWrapper<Student> wrapper : patchedList) {
            wrapper.setViewProperty("choosenOrientation", st2Or.get(wrapper.getId()));
        }

    }

    @Override
    public void onMassAdd(Map<Student, Student2Orientation> map, Model model) {
        for (Map.Entry<Student, Student2Orientation> entry : map.entrySet()) {
            Student2Orientation rel = entry.getValue() != null ? (entry.getValue().getOrientation().equals(model.getOrientation()) ? null : entry.getValue()) : null;
            DaoStudentSubscribe.instanse().saveStudentOrientationSubscribe(
                    entry.getKey(), model.getOrientation(), rel, Student2Orientation.TANDEM_SOURCE, model.isSendMail());
        }
    }
}
