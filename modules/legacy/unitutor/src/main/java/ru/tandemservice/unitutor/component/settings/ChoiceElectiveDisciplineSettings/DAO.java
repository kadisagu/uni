package ru.tandemservice.unitutor.component.settings.ChoiceElectiveDisciplineSettings;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setEduYearModel(new LazySimpleSelectModel<>(EducationYear.class, EducationYear.title().s()));
        model.setYearPartModel(new DQLFullCheckSelectModel("title") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                final DQLSelectBuilder dqlIn = DqlDisciplineBuilder.getDqlInYearDistributionPart(model.getEducationYear());
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(YearDistributionPart.class, alias)
                        .where(in(
                                property(YearDistributionPart.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(property(YearDistributionPart.title().fromAlias(alias)));
                FilterUtils.applySimpleLikeFilter(builder, alias, YearDistributionPart.title(), filter);
                return builder;
            }
        });
    }

    @Override
    public void update(Model model) {
        getSession().saveOrUpdate(model.getSettings());
    }

    @Override
    public void onChangeEduYear(Model model) {
        if (model.getEducationYear() != null && model.getYearPart() != null) {

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ChoiceElectiveDisciplineSettings.class, "e")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(ChoiceElectiveDisciplineSettings.educationYear().fromAlias("e")), model.getEducationYear()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(ChoiceElectiveDisciplineSettings.yearPart().fromAlias("e")), model.getYearPart()));

            ChoiceElectiveDisciplineSettings settings = builder.createStatement(getSession()).uniqueResult();
            if (settings == null) {
                settings = new ChoiceElectiveDisciplineSettings();
                settings.setEducationYear(model.getEducationYear());
                settings.setYearPart(model.getYearPart());
            }
            model.setSettings(settings);
        }
    }
}
