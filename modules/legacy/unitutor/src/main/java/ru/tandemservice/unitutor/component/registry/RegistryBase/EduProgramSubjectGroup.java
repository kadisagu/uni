package ru.tandemservice.unitutor.component.registry.RegistryBase;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class EduProgramSubjectGroup extends EntityBase implements ITitled, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8200163592314971247L;
    private String title;

    public EduProgramSubjectGroup(EduProgramSubject2013Group group) {
        setId(group.getId());
        this.title = group.getTitle() + " (перечень направлений подготовки 2013)";
    }

    public EduProgramSubjectGroup(EduProgramSubjectOksoGroup group) {
        setId(group.getId());
        this.title = group.getTitle() + " (перечень направлений подготовки ОКСО и 2009)";
    }

    public EduProgramSubjectGroup(Long id, String title) {
        setId(id);
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static ISelectModel getEduGroupModel(final List<Long> ids) {

        return new FullCheckSelectModel() {
            @Override
            public ListResult<EduProgramSubjectGroup> findValues(String filter) {
                List<EduProgramSubjectGroup> lst = new ArrayList<>();

                lst.addAll(getEduGroupList(EduProgramSubject2013Group.class, ids, filter));
                lst.addAll(getEduGroupList(EduProgramSubjectOksoGroup.class, ids, filter));

                Collections.sort(lst, ITitled.TITLED_COMPARATOR);
                return new ListResult<EduProgramSubjectGroup>(lst);
            }
        };
    }

    public static ISelectModel getEduGroupModel() {

        return new FullCheckSelectModel() {
            @Override
            public ListResult<EduProgramSubjectGroup> findValues(String filter) {
                List<EduProgramSubjectGroup> lst = new ArrayList<>();

                List<EduProgramSubject2013Group> group2013Lst = IUniBaseDao.instance.get().getCatalogItemList(EduProgramSubject2013Group.class);
                for (EduProgramSubject2013Group group : group2013Lst)
                    lst.add(new EduProgramSubjectGroup(group));

                List<EduProgramSubjectOksoGroup> group2009Lst = IUniBaseDao.instance.get().getCatalogItemList(EduProgramSubjectOksoGroup.class);
                for (EduProgramSubjectOksoGroup group : group2009Lst)
                    lst.add(new EduProgramSubjectGroup(group));

                List<EduProgramSubjectGroup> retVal = new ArrayList<>();
                if (!StringUtils.isEmpty(filter)) {
                    for (EduProgramSubjectGroup group : lst)
                        if (group.getTitle().toLowerCase().contains(filter.toLowerCase()))
                            retVal.add(group);
                }
                else
                    retVal.addAll(lst);

                Collections.sort(retVal, ITitled.TITLED_COMPARATOR);
                return new ListResult<EduProgramSubjectGroup>(retVal);
            }
        };
    }

    private static List<EduProgramSubjectGroup> getEduGroupList(final Class<? extends ICatalogItem> clazz, List<Long> ids, final String filter) {
        final List<EduProgramSubjectGroup> retVal = new ArrayList<>();

        BatchUtils.execute(ids, 1000, new BatchUtils.Action<Long>() {
            @Override
            public void execute(Collection<Long> elements) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(clazz, "gr")
                        .where(DQLExpressions.in(DQLExpressions.property("gr.id"), elements));

                List<ICatalogItem> lst = IUniBaseDao.instance.get().getList(builder);

                for (ICatalogItem item : lst) {
                    EduProgramSubjectGroup group;
                    if (clazz.getName().equals(EduProgramSubject2013Group.ENTITY_NAME))
                        group = new EduProgramSubjectGroup((EduProgramSubject2013Group) item);
                    else
                        group = new EduProgramSubjectGroup((EduProgramSubjectOksoGroup) item);

                    if (!StringUtils.isEmpty(filter)) {
                        if (group.getTitle().toLowerCase().contains(filter.toLowerCase()))
                            retVal.add(group);
                    }
                    else
                        retVal.add(group);

                }
            }
        });

        return retVal;
    }

}
