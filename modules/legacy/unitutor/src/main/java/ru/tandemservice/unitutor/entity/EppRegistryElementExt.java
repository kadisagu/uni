package ru.tandemservice.unitutor.entity;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitutor.entity.gen.EppRegistryElementExtGen;

/**
 * Расширение сущности «Запись реестра (базовая)»
 */
public class EppRegistryElementExt extends EppRegistryElementExtGen implements IEntity {

    public EppRegistryElementExt() {
    }

    public EppRegistryElementExt(EppRegistryElement registryElement) {
        this.setEppRegistryElement(registryElement);
    }
}