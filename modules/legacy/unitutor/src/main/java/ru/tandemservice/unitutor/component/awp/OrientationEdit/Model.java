package ru.tandemservice.unitutor.component.awp.OrientationEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unitutor.entity.Orientation2EduPlanVersion;


@Input({
        @Bind(key = "entityId", binding = "entityId")
})
public class Model {

    private Long entityId;
    private Orientation2EduPlanVersion rel;

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Orientation2EduPlanVersion getRel() {
        return rel;
    }

    public void setRel(Orientation2EduPlanVersion rel) {
        this.rel = rel;
    }


}
