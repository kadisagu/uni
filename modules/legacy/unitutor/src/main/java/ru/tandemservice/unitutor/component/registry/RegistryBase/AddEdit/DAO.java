package ru.tandemservice.unitutor.component.registry.RegistryBase.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.component.registry.RegistryBase.EduProgramSubjectGroup;
import ru.tandemservice.unitutor.entity.*;
import ru.tandemservice.unitutor.entity.catalog.ElectiveDisciplineType;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {

        // Режим добавления новой записи
        if (model.getElectiveDisciplineId() == null) {
            model.setEditMode(Boolean.FALSE);
        }
        // Режим редактирования текущей записи
        else {
            model.setEditMode(Boolean.TRUE);
            ElectiveDiscipline electiveDiscipline = getNotNull(ElectiveDiscipline.class, model.getElectiveDisciplineId());
            model.setEppRegistryElementParts(Collections.singletonList(electiveDiscipline.getRegistryElementPart()));
            model.setEducationYear(electiveDiscipline.getEducationYear());
            model.setYearDistributionPart(electiveDiscipline.getYearPart());
            EppRegistryElementExt registryElementExt = get(EppRegistryElementExt.class, EppRegistryElementExt.eppRegistryElement().id().s(), electiveDiscipline.getRegistryElementPart().getRegistryElement().getId());

            if (registryElementExt != null) {
                model.setAnnotation(registryElementExt.getAnnotation());
            }


            model.setCourseList(electiveDiscipline.getCourseList());
            model.setSubscribeToDate(electiveDiscipline.getSubscribeToDate());
            model.setEduGroupList(electiveDiscipline.getGroupList());
            if (electiveDiscipline.isSetTotalPlacesToSubscribe())
                model.setPlacesTotal(electiveDiscipline.getPlacesCount());

        }


        // учебный год
        model.setEducationYearModel(new LazySimpleSelectModel<>(EducationYear.class, EducationYear.title().s()));

        // часть года
        model.setEducationYearPartModel(new DQLFullCheckSelectModel("title") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                final DQLSelectBuilder dqlIn = DqlDisciplineBuilder.getDqlInYearDistributionPart(model.getEducationYear());
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(YearDistributionPart.class, alias)
                        .where(in(
                                property(YearDistributionPart.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(property(YearDistributionPart.title().fromAlias(alias)));
                FilterUtils.applySimpleLikeFilter(builder, alias, YearDistributionPart.title(), filter);
                return builder;
            }
        });

        // курс
        model.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));

        // направления подготовки
        model.setEduGroupModel(EduProgramSubjectGroup.getEduGroupModel());

        // фильтр по дисциплинам
        model.setDisciplineModel(new DQLFullCheckSelectModel(EppRegistryElementPart.titleWithNumber().s()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder()
                        .fromEntity(EppRegistryElementPart.class, alias);

                if (model.getElectiveDisciplineTypeCode().equals(ElectiveDisciplineTypeCodes.UDV))
                    dqlSelectBuilder.where(eqValue(property(EppRegistryElementPart.registryElement().parent().code().fromAlias(alias)), Model.UDV));
                else if (model.getElectiveDisciplineTypeCode().equals(ElectiveDisciplineTypeCodes.UF))
                    dqlSelectBuilder.where(eqValue(property(EppRegistryElementPart.registryElement().parent().code().fromAlias(alias)), Model.UF));

                FilterUtils.applyLikeFilter(dqlSelectBuilder, filter, EppRegistryElementPart.registryElement().number().fromAlias(alias),
                                                    EppRegistryElementPart.registryElement().title().fromAlias(alias));

                return dqlSelectBuilder;
            }
        });

        if (model.getEducationYearId() != null)
            model.setEducationYear(get(EducationYear.class, model.getEducationYearId()));
        if (model.getEducationYearPartId() != null)
            model.setYearDistributionPart(get(YearDistributionPart.class, model.getEducationYearPartId()));
    }

    @Override
    public void update(Model model) {

        List<String> checkResult = checkBeforeSave(model);
        if (!model.isEditMode() && !checkResult.isEmpty()) {
            StringBuilder sb = new StringBuilder()
                    .append(checkResult.size()>1 ? "Дисциплины " : "Дисциплина ")
                    .append(CoreStringUtils.join(checkResult, ","))
                    .append(" с указанными параметрами уже ")
                    .append(checkResult.size()>1 ?"существуют!" : "существует!");
            throw new ApplicationException(sb.toString());
        }

        for (EppRegistryElementPart part : model.getEppRegistryElementParts())
        {
                //в БД для элемента реестра уже может лежать расширение
                //сначала попробуем достать из БД
                EppRegistryElementExt registryElementExt = get(EppRegistryElementExt.class, EppRegistryElementExt.eppRegistryElement().id().s(), part.getRegistryElement().getId());
                //создаем новое
                if (registryElementExt == null)
                    registryElementExt = new EppRegistryElementExt(part.getRegistryElement());
            registryElementExt.setAnnotation(model.getAnnotation());
            getSession().saveOrUpdate(registryElementExt);

            ElectiveDiscipline electiveDiscipline;
            if (model.getElectiveDisciplineId()!= null)
                electiveDiscipline = getNotNull(ElectiveDiscipline.class, model.getElectiveDisciplineId());
            else
            {
                electiveDiscipline = new ElectiveDiscipline(part, model.getEducationYear());
                electiveDiscipline.setYearPart(model.getYearDistributionPart());
                electiveDiscipline.setType(getByCode(ElectiveDisciplineType.class, model.getElectiveDisciplineTypeCode()));
            }

            electiveDiscipline.setPlacesCount(model.getPlacesTotal());
            electiveDiscipline.setSubscribeToDate(model.getSubscribeToDate());
            saveOrUpdate(electiveDiscipline);

            new DQLDeleteBuilder(ElectiveDiscipline2Course.class)
                    .where(eqValue(property(ElectiveDiscipline2Course.electiveDiscipline().id()), electiveDiscipline.getId()))
                    .createStatement(getSession()).execute();

            for (Course course : model.getCourseList()) {
                ElectiveDiscipline2Course rel = new ElectiveDiscipline2Course();
                rel.setElectiveDiscipline(electiveDiscipline);
                rel.setCourse(course);
                getSession().save(rel);
            }

            new DQLDeleteBuilder(ElectiveDiscipline2EduGroup.class)
                    .where(eqValue(property(ElectiveDiscipline2EduGroup.electiveDiscipline().id()), electiveDiscipline.getId()))
                    .createStatement(getSession()).execute();

            for (EduProgramSubjectGroup group : model.getEduGroupList()) {
                ElectiveDiscipline2EduGroup rel = new ElectiveDiscipline2EduGroup();
                rel.setElectiveDiscipline(electiveDiscipline);
                rel.setGroupId(group.getId());
                rel.setTitle(group.getTitle());
                getSession().save(rel);
            }
        }
    }


    private List<String> checkBeforeSave(Model model) {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ElectiveDiscipline.class, "ed")
                .column(property(ElectiveDiscipline.registryElementPart().registryElement().title().fromAlias("ed")))
                .where(eqValue(property(ElectiveDiscipline.type().code().fromAlias("ed")), model.getElectiveDisciplineTypeCode()))
                .where(eqValue(property(ElectiveDiscipline.educationYear().fromAlias("ed")), model.getEducationYear()))
                .where(eqValue(property(ElectiveDiscipline.yearPart().fromAlias("ed")), model.getYearDistributionPart()))
                .where(in(property(ElectiveDiscipline.registryElementPart().fromAlias("ed")), model.getEppRegistryElementParts()));
        return dql.createStatement(getSession()).list();
    }

    @Override
    public void onChangeEduYear(Model model) {
        if (model.getEducationYear() != null && model.getYearDistributionPart() != null) {

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ChoiceElectiveDisciplineSettings.class, "e")
                    .where(eqValue(property(ChoiceElectiveDisciplineSettings.educationYear().fromAlias("e")), model.getEducationYear()))
                    .where(eqValue(property(ChoiceElectiveDisciplineSettings.yearPart().fromAlias("e")), model.getYearDistributionPart()));

            ChoiceElectiveDisciplineSettings settings = builder.createStatement(getSession()).uniqueResult();

            if (settings != null) {
                model.setSubscribeToDate(settings.getDate());
                model.setPlacesTotal((long) settings.getCountPlaces());
            }
            else {
                model.setSubscribeToDate(null);
                model.setPlacesTotal(null);
            }
        }

    }
}
