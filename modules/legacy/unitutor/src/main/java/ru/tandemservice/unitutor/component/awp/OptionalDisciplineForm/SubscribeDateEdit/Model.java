package ru.tandemservice.unitutor.component.awp.OptionalDisciplineForm.SubscribeDateEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;


import java.util.Collection;
import java.util.Date;

@Input({@Bind(key="disciplines", binding = "disciplineIds"),
        @Bind(key = "isForNull", binding = "forNullOnly")})
public class Model {

    private Collection<Long> _disciplineIds;
    private boolean _forNullOnly;
    private Date subscribeToDate;

    public Date getSubscribeToDate() {
        return subscribeToDate;
    }

    public void setSubscribeToDate(Date subscribeToDate) {
        this.subscribeToDate = subscribeToDate;
    }

    public Collection<Long> getDisciplineIds()
    {
        return _disciplineIds;
    }

    public void setDisciplineIds(Collection<Long> disciplineIds)
    {
        _disciplineIds = disciplineIds;
    }

    public boolean isForNullOnly()
    {
        return _forNullOnly;
    }

    public void setForNullOnly(boolean forNullOnly)
    {
        _forNullOnly = forNullOnly;
    }
}
