/* $Id$ */
package ru.tandemservice.unitutor.ws.impl;

import com.google.common.primitives.Longs;
import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.SportSection2CourseRel;
import ru.tandemservice.unitutor.entity.catalog.codes.ApplicationStatus4SportsSectionCodes;
import ru.tandemservice.unitutor.entity.catalog.gen.ApplicationStatus4SportsSectionGen;
import ru.tandemservice.unitutor.ws.types.WsSportSectionWrap;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 29.08.2016
 */
public class SportSectionManager extends UniBaseDao implements IWsSportSectionManager
{
    public static final String RESPONSE_ERROR_STUDENT = "error:student";
    public static final String RESPONSE_ERROR_CREATE = "error:create";
    public static final String RESPONSE_ERROR_WITHDRAW = "error:withdraw";
    public static final String RESPONSE_OK_CREATE = "ok:create";
    public static final String RESPONSE_OK_WITHDRAW = "ok:withdraw";


    @Override
    public List<WsSportSectionWrap> getWsSportSections(String studentParam)
    {
        Long studentId = Longs.tryParse(studentParam);
        Student student = (studentId == null) ? null : get(Student.class, studentId);

        if (student == null)
            return Collections.emptyList();

        EducationYear currentYear = EducationYear.getCurrentRequired();
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SportSection.class, "ss")

                .column(property("ss"))
                .where(ge(property("ss", SportSection.educationYear().intValue()), value(currentYear.getIntValue())))
                .where(or(isNull(property("ss", SportSection.formativeOrgUnit())),
                          eq(property("ss", SportSection.formativeOrgUnit()), value(student.getEducationOrgUnit().getFormativeOrgUnit()))
                ))

                .column(SportSection.getBusyPlacesCounterQuery(DQLExpressions.property("ss"), "sa1"))

                .joinEntity("ss", DQLJoinType.left, SectionApplication.class, "sa2", and(
                        eq(property("sa2", SectionApplication.student()), value(student)),
                        eq(property("sa2", SectionApplication.sportSection()), property("ss"))
                ))
                .column(property("sa2"));

        SportSection.addWhereExistsSexRel(builder, property("ss"), student.getPerson().getIdentityCard().getSex());

        List<Object[]> objects = this.<Object[]>getList(builder);

        List<Long> sectionIds = objects.stream().map(row -> ((SportSection) row[0]).getId()).collect(Collectors.toList());
        Map<Long, List<Integer>> coursesBySection = this.getList(SportSection2CourseRel.class, SportSection2CourseRel.section().id().s(), sectionIds)
                .stream()
                .collect(Collectors.groupingBy(rel -> rel.getSection().getId(),
                                               Collectors.mapping(rel -> rel.getCourse().getIntValue(), Collectors.toList())));

        return objects.stream()
                .filter(row -> {
                    SportSection section = (SportSection) row[0];
                    Long sectionId = section.getId();
                    int divYear = section.getEducationYear().getIntValue() - currentYear.getIntValue();
                    if (divYear == 0)
                        return coursesBySection.get(sectionId) != null &&
                                coursesBySection.get(sectionId).contains(student.getCourse().getIntValue());
                    else
                        return coursesBySection.get(sectionId) != null &&
                                coursesBySection.get(sectionId).contains(student.getCourse().getIntValue() + divYear);
                })
                .map(row -> {
                    SportSection section = (SportSection) row[0];
                    int busy = ((Long) row[1]).intValue();
                    SectionApplication app = (SectionApplication) row[2];
                    return new WsSportSectionWrap(section, section.getNumbers() - busy, app != null, app == null ? "" : app.getStatus().getCode());
                })
                .collect(Collectors.toList());
    }

    @Override
    public String updateApplications(String studentParam, String createApplication, String withdrawApplication)
    {
        Long studentId = Longs.tryParse(studentParam);
        Student student = (studentId == null) ? null : get(Student.class, studentId);

        if (student == null)
            return RESPONSE_ERROR_STUDENT;

        StringBuilder response = new StringBuilder();

        Long sectionId;
        SportSection section;


        sectionId = Longs.tryParse(withdrawApplication);
        if (sectionId != null)
        {
            section = get(SportSection.class, sectionId);
            if (section != null)
            {
                SectionApplication application = getSectionApplication(student, section);
                if (application != null)
                {
                    application.setStatus(getByNaturalId(new ApplicationStatus4SportsSectionGen.NaturalId(ApplicationStatus4SportsSectionCodes.WITHDRAW)));
                    saveOrUpdate(application);
                    addSeparatorIfNeed(response).append(RESPONSE_OK_WITHDRAW);
                }
                else
                    addSeparatorIfNeed(response).append(RESPONSE_ERROR_WITHDRAW);
            }
            else
                addSeparatorIfNeed(response).append(RESPONSE_ERROR_WITHDRAW);
        }

        sectionId = Longs.tryParse(createApplication);
        if (sectionId != null)
        {
            section = get(SportSection.class, sectionId);
            if (section != null)
            {
                SectionApplication application = getSectionApplication(student, section);
                if (application != null)
                    application.setStatus(getByNaturalId(new ApplicationStatus4SportsSectionGen.NaturalId(ApplicationStatus4SportsSectionCodes.ACCEPTABLE)));
                else
                    application = new SectionApplication(section, student, false);
                saveOrUpdate(application);
                if (application.getId() != null)
                    addSeparatorIfNeed(response).append(RESPONSE_OK_CREATE);
                else
                    addSeparatorIfNeed(response).append(RESPONSE_ERROR_CREATE);
            }
            else
                addSeparatorIfNeed(response).append(RESPONSE_ERROR_CREATE);
        }

        return response.toString();
    }

    private static StringBuilder addSeparatorIfNeed(StringBuilder responseBuilder)
    {
        if (responseBuilder.length() > 0)
            responseBuilder.append(";");

        return responseBuilder;
    }

    private SectionApplication getSectionApplication(Student student, SportSection section)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SectionApplication.class, "sa")
                .column(property("sa"))
                .where(eq(property("sa", SectionApplication.sportSection()), value(section)))
                .where(eq(property("sa", SectionApplication.student()), value(student)));
        return builder.createStatement(getSession()).uniqueResult();
    }
}