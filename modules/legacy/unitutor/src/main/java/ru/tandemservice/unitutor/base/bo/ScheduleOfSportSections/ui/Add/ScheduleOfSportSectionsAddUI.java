/* $Id$ */
package ru.tandemservice.unitutor.base.bo.ScheduleOfSportSections.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitutor.entity.catalog.TutorScriptItem;
import ru.tandemservice.unitutor.entity.catalog.codes.TutorScriptItemCodes;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 05.09.2016
 */
public class ScheduleOfSportSectionsAddUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        _uiSettings.set(ScheduleOfSportSectionsAdd.EDUCATION_YEAR_PARAM, EducationYear.getCurrentRequired());
    }

    public void onClickApply()
    {
        final Long educationYearId = ((IEntity) _uiSettings.get(ScheduleOfSportSectionsAdd.EDUCATION_YEAR_PARAM)).getId();
        List<OrgUnit> formativeOrgUnits = _uiSettings.get(ScheduleOfSportSectionsAdd.FORMATIVE_ORG_UNIT_LIST_PARAM);
        final List<Long> formativeOrgUnitIds = formativeOrgUnits.stream().map(IEntity::getId).collect(Collectors.toList());
        List<Course> courses = _uiSettings.get(ScheduleOfSportSectionsAdd.COURSE_LIST_PARAM);
        final List<Long> courseIds = courses.stream().map(IEntity::getId).collect(Collectors.toList());
        Sex sex = _uiSettings.get(ScheduleOfSportSectionsAdd.SEX_PARAM);
        final Long genderId = sex == null ? null : sex.getId();
        List<UniplacesPlace> places = _uiSettings.get(ScheduleOfSportSectionsAdd.PLACE_LIST_PARAM);
        final List<Long> placeIds = places.stream().map(IEntity::getId).collect(Collectors.toList());
        final Boolean withNumbers = _uiSettings.get(ScheduleOfSportSectionsAdd.WITH_NUMBERS_PARAM);

        IUniBaseDao.instance.get().doInTransaction(session -> {
            IScriptItem scriptItem = DataAccessServices.dao().getByCode(TutorScriptItem.class, TutorScriptItemCodes.REPORT_SCHEDULE_OF_SPORT_SECTIONS);
            CommonManager.instance().scriptDao()
                    .getScriptResultAndDownloadIt(scriptItem,
                                     "session", session,
                                     "educationYearId", educationYearId,
                                     "formativeOrgUnitIds", formativeOrgUnitIds,
                                     "courseIds", courseIds,
                                     "genderId", genderId,
                                     "placeIds", placeIds,
                                     "withNumbers", withNumbers
                    );
            return 0L;
        });
    }
}