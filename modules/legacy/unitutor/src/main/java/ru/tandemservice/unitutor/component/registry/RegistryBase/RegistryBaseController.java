package ru.tandemservice.unitutor.component.registry.RegistryBase;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;

public abstract class RegistryBaseController<T extends RegistryBaseModel> extends AbstractBusinessController<IRegistryBaseDAO<T>, T> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        T model = component.getModel();
        model.setSettings(component.getSettings());

        getDao().prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        T model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<RegistryWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());

        createColumns(dataSource, model);
        model.setDataSource(dataSource);
    }

    protected abstract void createColumns(DynamicListDataSource<RegistryWrapper> dataSource, T model);

    protected void validateFilters(IBusinessComponent component) {
        T model = component.getModel();

        if (model.getSettings().get("educationYearPart") == null)
            throw new ApplicationException("Фильтр \"Часть года\" является обязательным для выбора");

        if (model.getSettings().get("educationYear") == null)
            throw new ApplicationException("Фильтр \"Учебный год\" является обязательным для выбора");
    }

    /**
     * листенер смены учебного года
     *
     * @param component
     */
    public void onChangeEducationYear(IBusinessComponent component) {
        onSearchParamsChange(component);
        onRefreshComponent(component);
    }

    public void onClickSearch(IBusinessComponent component) {

        validateFilters(component);
        component.saveSettings();

        onRefreshComponent(component);
    }

    public void onClickClear(IBusinessComponent component) {
        component.getSettings().clear();
        component.saveSettings();
        onRefreshComponent(component);
    }

    public void onSearchParamsChange(IBusinessComponent component) {
        component.saveSettings();
        T model = component.getModel();
        model.setSettings(component.getSettings());
        getDao().prepareCommonFilters(model);
    }
}
