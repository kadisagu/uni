package ru.tandemservice.unitutor.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unitutor_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eppRegistryElementExt

		// создано свойство language
		{
			// создать колонку
			tool.createColumn("eppregistryelementext_t", new DBColumn("language_id", DBType.LONG));

		}

		// создано обязательное свойство secondLanguage
		{
			// создать колонку
			tool.createColumn("eppregistryelementext_t", new DBColumn("secondlanguage_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			tool.executeUpdate("update eppregistryelementext_t set secondlanguage_p=? where secondlanguage_p is null", false);

			// сделать колонку NOT NULL
			tool.setColumnNullable("eppregistryelementext_t", "secondlanguage_p", false);

		}


    }
}