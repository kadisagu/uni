package ru.tandemservice.unitutor.ws;

import com.google.common.primitives.Longs;
import org.apache.axis.encoding.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.interceptor.InInterceptors;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.unitutor.ws.impl.DisciplinesManager;
import ru.tandemservice.unitutor.ws.impl.IWsSportSectionManager;
import ru.tandemservice.unitutor.ws.types.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebService(serviceName = "wsDisciplines")
@InInterceptors(interceptors = {"ru.tandemservice.unitutor.ws.interceptors.WsAuthorizationInterceptor"})
public class WsDisciplines implements IWsDisciplines {

    @WebMethod
    public String getTestMessage() {
        return "Web-service WsDisciplines is acceptable";
    }

    @WebMethod
    @WebResult(name = "disciplineWrap")
    public List<WsDisciplineWrap> getWsDisciplinesNextEppYear(
            @WebParam(name = "studentParam") String studentParam,
            @WebParam(name = "educationYearId") String educationYearId)
            throws Exception
    {
        if (!StringUtils.isEmpty(studentParam)) {
            List<WsDisciplineWrap> disciplineList = DisciplinesManager.getWsDisciplinesManager()
                    .getWsDisciplinesNextEppYear(studentParam, educationYearId);
            return disciplineList;
        }

        return null;
    }

    @Override
    @WebMethod
    public String subscribeToElectiveDiscipline(
            @WebParam(name = "studentParam") String studentParam,
            @WebParam(name = "disciplineSubscribes") String disciplineIds,
            @WebParam(name = "needSendMail") String needSendMail,
            @WebParam(name = "electiveType") String electiveType,
            @WebParam(name = "educationYearId") String educationYearId)
            throws Exception
    {
        String response = "";

        try {
            List<String> ids = new ArrayList<>();
            if (!StringUtils.isEmpty(disciplineIds))
                ids = Arrays.asList(disciplineIds.split(","));

                boolean sendMail = Boolean.parseBoolean(needSendMail);
                response = DisciplinesManager.getWsDisciplinesManager().subscribeToElectiveDiscipline(studentParam, ids, electiveType, sendMail, educationYearId);
        }
        catch (Exception ex) {
            response = "error:" + "Ошибка при попытке записаться на дисциплины " + ex.getMessage();
        }

        return response.length() > 0? response : null;
    }

    @Override
    @WebMethod
    public String getEduPlanVersion(@WebParam(name = "studentParam") String studentParam) throws Exception {
        String eduPlanVersionData = "";
        try {
            if (!StringUtils.isEmpty(studentParam)) {
                byte[] data = DisciplinesManager.getWsDisciplinesManager().getEduPlanVersionData(studentParam);
                eduPlanVersionData = Base64.encode(data);
            }
        }
        catch (Exception e) {
            throw e;
        }
        return eduPlanVersionData;

    }

    @Override
    @WebMethod
    public String getElectiveApplication(String studentParam,
                                         String id) throws Exception
    {
        String electiveApplicationData = "";
        try {
            if (!StringUtils.isEmpty(studentParam) && !StringUtils.isEmpty(id)) {
                byte[] data = DisciplinesManager.getWsDisciplinesManager().getStudentApplicationData(studentParam, id);
                electiveApplicationData = Base64.encode(data);
            }
        }
        catch (Exception e) {
            throw e;
        }
        return electiveApplicationData;
    }

    @Override
    public String getAnnotation(@WebParam(name = "idStr") String idStr) throws Exception {

        String annotation = "";

        if (!StringUtils.isEmpty(idStr) && StringUtils.isNumeric(idStr))
            annotation = DisciplinesManager.getWsDisciplinesManager().getAnnotationData(idStr);

        return annotation;
    }

    @WebMethod
    @WebResult(name = "electiveWrap")
    public List<WsElectiveWrap> getWsElectiveNextEppYear(@WebParam(name = "studentParam") String studentParam,
                                                         @WebParam(name = "electiveType") String electiveType,
                                                         @WebParam(name = "educationYearId") String educationYearId) throws Exception
    {

        if (!StringUtils.isEmpty(studentParam))
            return DisciplinesManager.getWsDisciplinesManager().getWsElectiveNextEppYear(studentParam, electiveType, educationYearId);

        return null;
    }

    /**
     * Список учебных годов
     * @return
     */
    @Override
    public List<WsEducationYearWrap> getEducationYearList() {
        return DisciplinesManager.getWsDisciplinesManager().getEducationYearList();
    }


    /**
     * Направленности
     */
    @Override
    @WebMethod
    @WebResult(name = "orientationWrap")
    public List<WsOrientationWrap> getWsOrientations(@WebParam(name = "studentParam") String studentParam) throws Exception {
        if (!StringUtils.isEmpty(studentParam))
            return DisciplinesManager.getWsDisciplinesManager().getWsOrientations(studentParam);

        return null;
    }


    /**
     * Подписка на направленности
     */
    @Override
    @WebMethod
    public String subscribeToOrientation(
            @WebParam(name = "studentParam") String studentParam,
            @WebParam(name = "orientationSubscribe") String orientationSubscribe) throws Exception
    {
        String response = "";
        try {
            if (!StringUtils.isEmpty(studentParam)) {
                response = DisciplinesManager.getWsDisciplinesManager().subscribeToOrientation(studentParam, orientationSubscribe);
            }
        }
        catch (Exception ex) {
            response = "error:" + "Ошибка при попытке записаться на направленность " + ex.getMessage();
        }

        return response;
    }

    /**
     * Список студентов по логину
     * @throws Exception
     */
    @Override
    @WebMethod
    @WebResult(name = "studentWrap")
    public List<WsStudentWrap> getStudentsList(@WebParam(name = "studentParam") String studentParam) throws Exception
    {
        if (!StringUtils.isEmpty(studentParam))
            return DisciplinesManager.getWsDisciplinesManager().getStudentListByLogin(studentParam);
        return null;
    }


    /**
     * Число дисциплин
     */
    @Override
    @WebMethod
    @WebResult(name = "workPlanWrap")
    public List<WsWorkPlanInfoWrap> getCountElectiveDiscipline(@WebParam(name = "studentParam") String studentParam,
                                                          @WebParam(name = "educationYearId") String educationYearId,
                                                          @WebParam(name = "electiveType") String electiveType) throws Exception
    {
        if (!StringUtils.isEmpty(studentParam))
        {
            Long eduYearId = Longs.tryParse(educationYearId);
            return DisciplinesManager.getWsDisciplinesManager().getCountElectiveDisciplines(studentParam, eduYearId, electiveType);
        }
        return null;
    }

    @Override
    @WebMethod
    @WebResult(name = "sportSectionWrap")
    public List<WsSportSectionWrap> getWsSportSections(@WebParam(name = "studentParam") String studentParam) throws Exception
    {
        if (!StringUtils.isEmpty(studentParam))
        {
            return ((IWsSportSectionManager) ApplicationRuntime
                    .getBean(IWsSportSectionManager.class.getName()))
                    .getWsSportSections(studentParam);
        }

        return null;
    }

    @Override
    @WebMethod
    public String updateApplications(@WebParam(name = "studentParam") String studentParam,
                                                       @WebParam(name = "createApplication") String createApplication,
                                                       @WebParam(name = "withdrawApplication") String withdrawApplication) throws Exception
    {
        if (!StringUtils.isEmpty(studentParam))
        {
            return ((IWsSportSectionManager) ApplicationRuntime
                    .getBean(IWsSportSectionManager.class.getName()))
                    .updateApplications(studentParam, createApplication, withdrawApplication);
        }

        return null;
    }
}
