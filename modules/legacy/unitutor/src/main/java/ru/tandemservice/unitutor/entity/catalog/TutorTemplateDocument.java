package ru.tandemservice.unitutor.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unitutor.entity.catalog.gen.*;

/** @see ru.tandemservice.unitutor.entity.catalog.gen.TutorTemplateDocumentGen */
public class TutorTemplateDocument extends TutorTemplateDocumentGen implements
        ITemplateDocument
{

    /**
     * Печатный шаблон для списка студентов, подписанных на ДПВ
     */
    public final static String ELECTIVE_SUBSCRIBES_TEMPLATE = "electiveSubscribe";

    /**
     * Печатный шаблон заявления студента на ДПВ
     */
    public final static String ELECTIVE_APPLICATION_TEMPLATE = "electiveApplication";

    @Override
    public byte[] getContent() {
        return CommonBaseUtil.getTemplateContent(this);
    }

}