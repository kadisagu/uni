package ru.tandemservice.unitutor.mailprint;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimail.mailprint.IMailPrint;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SubscribeTutor extends SubcribeBySakai {
    private static IMailPrint _IMailPrint = null;

    public static IMailPrint Instanse()
    {
        if (_IMailPrint == null)
            _IMailPrint = (IMailPrint) ApplicationRuntime.getBean("SubscribeTutor");
        return _IMailPrint;
    }


    @Override
    protected String getTemplateCode()
    {
        return "dpvTutor";
    }

    @Override
    protected Map<String, Object> getMapParameters(IEntity entity, Object params) {

        Long transactionId = (Long) params;
        Student student = (Student) entity;

        Map<String, Object> map = super.getMapParameters(entity, params);

        String unsubscibeName = "";
        String subscribeTutorTitle1 = "Преподавателем внесены изменения в Вашу подписку на ДПВ в " + map.get("educationYearTitle");
        String subscribeToDates = "";

        List<ElectiveDisciplineSubscribe> unsubscribed = getElectiveDisciplineAndFillEducationYear(
                student
                , transactionId
                , map
                , false
                , true);

        List<String> lstDate = new ArrayList<>();
        if (unsubscribed != null && !unsubscribed.isEmpty()) {
            for (ElectiveDisciplineSubscribe eds : unsubscribed) {
                if (eds.getElectiveDiscipline().getSubscribeToDate() != null) {
                    String date_str = DateFormatter.DEFAULT_DATE_FORMATTER.format(eds.getElectiveDiscipline().getSubscribeToDate());
                    if (!lstDate.contains(date_str))
                        lstDate.add(date_str);
                }
            }
            subscribeToDates = StringUtils.join(lstDate, ",");

            if (unsubscribed.size() > 1) {
                unsubscibeName = "Вы отправляли заявки на выбор дисциплин:";
                subscribeTutorTitle1 = "В связи с тем, что на данную дисциплину набор не состоялся и в срок до " + subscribeToDates
                        + " Вы не произвели повторный выбор дисциплины с целью формирования рабочего учебного плана на " + map.get("educationYearTitle") + " учебный год ";
            }
            else {
                unsubscibeName = "Вы отправляли заявку на выбор дисциплины";
                subscribeTutorTitle1 = "В связи с тем, что на данные дисциплины набор не состоялся и в сроки до ( " + subscribeToDates + ") "
                        + " Вы не произвели повторный выбор дисциплины с целью формирования рабочего учебного плана на " + map.get("educationYearTitle") + " учебный год ";
            }
        }


        map.put("subscribeToDates", subscribeToDates);
        map.put("subscribeTutorTitle1", subscribeTutorTitle1);


        String[][] unsubcribeInTransaction = getDisciplinetable(unsubscribed);
        map.put("unsubscibeName", unsubscibeName);
        map.put("unsubcribeInTransaction", unsubcribeInTransaction);

        // теперь то, что указал преподаватель
        List<ElectiveDisciplineSubscribe> subscribedTutor = getElectiveDisciplineAndFillEducationYear(student, transactionId, map, true, false);
        String[][] subscribedTutorTable = getDisciplinetable(subscribedTutor);
        String subscribeTutorTitle2 = "";

        if (subscribedTutor != null && !subscribedTutor.isEmpty())
            // преподаватель что-то выбрал
            subscribeTutorTitle2 = "Вам предлагается пройти обучение по дисциплинам";

        map.put("subscribeTutorTitle2", subscribeTutorTitle2);
        map.put("subscribedTutorTable", subscribedTutorTable);

        // преподаватель отменил подписку
        String unsubbcribeTutor = "";
        List<ElectiveDisciplineSubscribe> unsubscribedTutor = getElectiveDisciplineAndFillEducationYear(student, transactionId, map, false, false);
        if (unsubscribedTutor != null && !unsubscribedTutor.isEmpty()) {
            // преподаватель что-то выбрал
            if (unsubscribedTutor.size() > 1)
                unsubbcribeTutor = "преподаватель отменил подписку на дисциплины";
            else
                unsubbcribeTutor = "преподаватель отменил подписку на дисциплину";
        }
        String[][] unsubscribedTutorTable = getDisciplinetable(unsubscribedTutor);
        map.put("unsubscribedTutor", unsubbcribeTutor);
        map.put("unsubscribedTutorTable", unsubscribedTutorTable);


        // таблица актуальных подписок
        List<ElectiveDisciplineSubscribe> lstActual = DaoStudentSubscribe.instanse().getSubscribedElectiveDiscipline(Lists.newArrayList(student), null, null, true, null);
        String[][] subcribeActual = getDisciplinetable(lstActual);
        map.put("subcribeActual", subcribeActual);

        return map;
    }

}
