/* $Id$ */
package ru.tandemservice.unitutor.component.registry.DisciplineRegistry.LanguageAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;

/**
 * @author Ekaterina Zvereva
 * @since 04.02.2016
 */

@Input({@Bind(key = EppRegistryElement.P_ID, binding = "registryElementId", required = true)})
public class Model
{
    private ISelectModel _languageModel;
    private Long _registryElementId;
    private EppRegistryElementExt _registryElementExt;

    public ISelectModel getLanguageModel()
    {
        return _languageModel;
    }

    public void setLanguageModel(ISelectModel languageModel)
    {
        _languageModel = languageModel;
    }

    public Long getRegistryElementId()
    {
        return _registryElementId;
    }

    public void setRegistryElementId(Long registryElementId)
    {
        _registryElementId = registryElementId;
    }

    public EppRegistryElementExt getRegistryElementExt()
    {
        return _registryElementExt;
    }

    public void setRegistryElementExt(EppRegistryElementExt registryElementExt)
    {
        _registryElementExt = registryElementExt;
    }
}