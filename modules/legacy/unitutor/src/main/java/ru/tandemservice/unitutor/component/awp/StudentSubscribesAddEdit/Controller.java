package ru.tandemservice.unitutor.component.awp.StudentSubscribesAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unitutor.dao.DisciplineWrap;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;

import java.util.ArrayList;
import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        Model model = component.getModel();


        if (model.isDpv()) {
            prepareDataSource(component);
        }
        else {
            prepareElectiveRegistryDataSource(component);
        }
        getDao().prepare(model);

        // если выбор студента остался - то просто обновляем список ДПВ
        if (model.getStudent() != null)
            onSelectStudent(component);
    }

    @Override
    public void onRenderComponent(IBusinessComponent component)
    {
        StringBuilder title = new StringBuilder("Добавление записи на ");
        switch (getModel(component).getElectiveDisciplineTypeCode())
        {
            case ElectiveDisciplineTypeCodes.DPV:
                title.append("дисциплину по выбору");
                break;
            case ElectiveDisciplineTypeCodes.UDV:
                title.append("общеуниверситетскую дисциплину по выбору");
                break;
            case ElectiveDisciplineTypeCodes.UF:
                title.append("общеуниверситетский факультатив");
                break;
        }
        ContextLocal.beginPageTitlePart(title.toString());
    }

    protected void prepareDataSource(IBusinessComponent component) {

        final Model model = component.getModel();

        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<DisciplineWrap> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new CheckboxColumn("select").setListener("onCheckDiscipline"));
        dataSource.addColumn(new SimpleColumn("Группа", "parentName").setClickable(false).setOrderable(false).setWidth(200));
        dataSource.addColumn(new SimpleColumn("Название ДПВ", "eppRegistryElementPart.titleWithNumber").setClickable(false).setOrderable(false).setWidth(300));

        model.setDataSource(dataSource);

    }

    protected void prepareElectiveRegistryDataSource(IBusinessComponent component) {

        final Model model = component.getModel();

        if (model.getElectiveDataSource() != null)
            return;

        DynamicListDataSource<ElectiveDiscipline> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Название", ElectiveDiscipline.registryElementPart().titleWithNumber()).setClickable(false).setOrderable(false).setWidth(300));

        model.setElectiveDataSource(dataSource);
    }

    public void onSelectStudent(IBusinessComponent component) {
        Model model = component.getModel();

        if (model.getEducationYear() == null)
            throw new ApplicationException("Выберите учебный год");
        if (model.getYearDistributionPart() == null)
            throw new ApplicationException("Выберите часть года");

        if (model.isDpv()) {
           getDao().updateDisciplineListModel(model);
        }
        else {
            getDao().updateElectiveRegistryDisciplineModel(model);
        }
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();

        if (model.isDpv()) {
            getDao().saveSubscribe(model);
        }
        else {
            getDao().saveElectiveSubscribe(model);
        }
        deactivate(component);
    }

    public void onCheckDiscipline(IBusinessComponent component) {

        Model model = component.getModel();
        // Выбранная ДПВ
        Long id = ((Model) component.getModel()).getDataSource().getLastVisitedEntityId();
        List<IEntity> removeList = new ArrayList<>();
        DisciplineWrap currentDiscipline = ((Model) component.getModel()).getDataSource().getRecordById(id);

        List<Long> lstCheck = new ArrayList<>();

        for (IEntity entity : ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects()) {
            DisciplineWrap row = (DisciplineWrap) entity;

            if (
                    row.getParentId() != null
                            && currentDiscipline.getParentId() != null
                            && row.getParentId().equals(currentDiscipline.getParentId())
                            && !currentDiscipline.getId().equals(((DisciplineWrap) entity).getId()))
            {
                // если в группе >1 дисциплины
                lstCheck.add(row.getId());
                if (lstCheck.size() >= currentDiscipline.getCountInGroup())
                    removeList.add(entity);

            }

            if (row.isIncorrectRow()) {
                // что делать, пока не ясно, но думаю - снять выделение однозначно
                removeList.add(entity);
            }
        }

        if (!removeList.isEmpty())
            ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects().removeAll(removeList);

    }
}
