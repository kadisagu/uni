package ru.tandemservice.unitutor.component.awp.StudentOrientationAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component) {
        getDao().update(component.<Model>getModel());
        deactivate(component);
    }
}
