/* $Id$ */
package ru.tandemservice.unitutor.ws.types;

import ru.tandemservice.unitutor.entity.SportSection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import java.io.Serializable;

/**
 * @author Andrey Andreev
 * @since 29.08.2016
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "sportSectionWrap", namespace = "http://ws.unitutor.tandemservice.ru/")
public class WsSportSectionWrap implements Serializable
{
    private static final long serialVersionUID = 8580930160425515010L;

    private String sportSectionId;

    private String year;
    private String title;
    private String type;
    private String formativeOrgUnit;
    private String firstClassInWeek;
    private String secondClassInWeek;
    private String physicalFitnessLevel;
    private String place;
    private Integer numbers;
    private String trainer;
    private String tutorOrgUnit;
    private Boolean application;
    private String status;

    public WsSportSectionWrap()
    {
    }

    public WsSportSectionWrap(SportSection sportSection, Integer freePlaces, boolean application, String status)
    {
        sportSectionId = String.valueOf(sportSection.getId());

        year = sportSection.getEducationYear().getTitle();
        title = sportSection.getTitle();
        type = sportSection.getType().getTitle();
        formativeOrgUnit = sportSection.getFormativeOrgUnit() == null ? "" : sportSection.getFormativeOrgUnit().getTitle();
        firstClassInWeek = sportSection.getFirstClassInWeek();
        secondClassInWeek = sportSection.getSecondClassInWeek();
        physicalFitnessLevel = sportSection.getPhysicalFitnessLevel().getTitle();
        place = sportSection.getPlace().getTitleWithLocation();
        numbers = freePlaces;
        trainer = sportSection.getTrainer() == null ? "" : sportSection.getTrainer().getFio();
        tutorOrgUnit = sportSection.getTutorOrgUnit().getPrintTitle();
        this.application = application;
        this.status = status;
    }

    public String getFirstClassInWeek()
    {
        return firstClassInWeek;
    }

    public void setFirstClassInWeek(String firstClassInWeek)
    {
        this.firstClassInWeek = firstClassInWeek;
    }

    public String getFormativeOrgUnit()
    {
        return formativeOrgUnit;
    }

    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public Integer getNumbers()
    {
        return numbers;
    }

    public void setNumbers(Integer numbers)
    {
        this.numbers = numbers;
    }

    public String getPhysicalFitnessLevel()
    {
        return physicalFitnessLevel;
    }

    public void setPhysicalFitnessLevel(String physicalFitnessLevel)
    {
        this.physicalFitnessLevel = physicalFitnessLevel;
    }

    public String getPlace()
    {
        return place;
    }

    public void setPlace(String place)
    {
        this.place = place;
    }

    public String getSecondClassInWeek()
    {
        return secondClassInWeek;
    }

    public void setSecondClassInWeek(String secondClassInWeek)
    {
        this.secondClassInWeek = secondClassInWeek;
    }

    public String getSportSectionId()
    {
        return sportSectionId;
    }

    public void setSportSectionId(String sportSectionId)
    {
        this.sportSectionId = sportSectionId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTrainer()
    {
        return trainer;
    }

    public void setTrainer(String trainer)
    {
        this.trainer = trainer;
    }

    public String getTutorOrgUnit()
    {
        return tutorOrgUnit;
    }

    public void setTutorOrgUnit(String tutorOrgUnit)
    {
        this.tutorOrgUnit = tutorOrgUnit;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Boolean isApplication()
    {
        return application;
    }

    public void setApplication(Boolean application)
    {
        this.application = application;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getYear()
    {
        return year;
    }

    public void setYear(String year)
    {
        this.year = year;
    }
}