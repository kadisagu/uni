package ru.tandemservice.unitutor.component.awp.move.MoveSubcscibe;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;

import java.util.ArrayList;
import java.util.List;

@Input({
            @org.tandemframework.core.component.Bind(key = "electiveDisciplineId", binding = "electiveDisciplineId"),
            @org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "orgUnitId"),
            @org.tandemframework.core.component.Bind(key = "employeeId", binding = "employeeId")
})
public class Model {
    private Long electiveDisciplineId;
    private Long orgUnitId;
    private Long employeeId;

    private OrgUnit orgUnit;
    private Employee employee;
    private CommonPostfixPermissionModelBase sec;
    private IDataSettings settings;
    private ElectiveDiscipline electiveDiscipline;
    private DynamicListDataSource<ElectiveDisciplineSubscribe> dataSourceA;
    private DynamicListDataSource<ElectiveDisciplineSubscribe> dataSourceB;

    private ISelectModel electiveDisciplineASelectModel;
    private ISelectModel electiveDisciplineBSelectModel;
    private ISelectModel groupListModel;

    private boolean warning = false;
    private String warningMessage = null;
    private boolean sendMail;


    public Long getElectiveDisciplineId() {
        return electiveDisciplineId;
    }

    public void setElectiveDisciplineId(Long electiveDisciplineId) {
        this.electiveDisciplineId = electiveDisciplineId;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public CommonPostfixPermissionModelBase getSec() {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModelBase sec) {
        this.sec = sec;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ElectiveDiscipline getElectiveDiscipline() {
        return electiveDiscipline;
    }

    public void setElectiveDiscipline(ElectiveDiscipline electiveDiscipline) {
        this.electiveDiscipline = electiveDiscipline;
    }

    public DynamicListDataSource<ElectiveDisciplineSubscribe> getDataSourceA() {
        return dataSourceA;
    }

    public void setDataSourceA(DynamicListDataSource<ElectiveDisciplineSubscribe> dataSourceA) {
        this.dataSourceA = dataSourceA;
    }

    public DynamicListDataSource<ElectiveDisciplineSubscribe> getDataSourceB() {
        return dataSourceB;
    }

    public void setDataSourceB(DynamicListDataSource<ElectiveDisciplineSubscribe> dataSourceB) {
        this.dataSourceB = dataSourceB;
    }

    public ISelectModel getElectiveDisciplineASelectModel() {
        return electiveDisciplineASelectModel;
    }

    public void setElectiveDisciplineASelectModel(ISelectModel electiveDisciplineASelectModel)
    {
        this.electiveDisciplineASelectModel = electiveDisciplineASelectModel;
    }

    public ISelectModel getElectiveDisciplineBSelectModel() {
        return electiveDisciplineBSelectModel;
    }

    public void setElectiveDisciplineBSelectModel(
            ISelectModel electiveDisciplineBSelectModel)
    {
        this.electiveDisciplineBSelectModel = electiveDisciplineBSelectModel;
    }

    public ISelectModel getGroupListModel() {
        return groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel) {
        this.groupListModel = groupListModel;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    /**
     * непосредственно к фильтру отношения не имеет
     */
    public List<OrgUnit> getFilterOrgUnit()
    {

        if (getEmployee() != null) {
            // указан преподаватель, все фильтры вяжем на преподавателя
            return null;
        }
        List<OrgUnit> retVal = new ArrayList<>();

        // если преводавателя нет и зашли со вкладки кафедры
        if (OrgUnitTypeCodes.CATHEDRA.equals(getOrgUnit().getOrgUnitType().getCode())) {
            OrgUnit parentCathedraOrgUnit = getOrgUnit().getParent();
            if (parentCathedraOrgUnit != null) {
                retVal.add(parentCathedraOrgUnit);
            }
            else {
                retVal = null;
            }
        }
        else {
            // препода нет, только орг юнит
            retVal.add(getOrgUnit());
        }

        return retVal;
    }

    /**
     * По читающему подразделению
     * непосредственно к фильтру отношения не имеет
     */
    public List<OrgUnit> getFilterCathedraOrgUnit()
    {

        if (getEmployee() != null) {
            // указан преподаватель, все фильтры вяжем на преподавателя
            return null;
        }
        // если преводавателя нет и зашли со вкладки кафедры
        if (OrgUnitTypeCodes.CATHEDRA.equals(getOrgUnit().getOrgUnitType().getCode())) {
            // препода нет, только орг юнит = кафедра
            List<OrgUnit> retVal = new ArrayList<>();
            retVal.add(getOrgUnit());
            return retVal;
        }

        return null;
    }

    public List<Employee> getFilterEmployee()
    {
        if (getEmployee() != null) {
            List<Employee> retVal = new ArrayList<>();
            retVal.add(getEmployee());
            return retVal;
        }
        else
            return null;
    }


    public ElectiveDiscipline getFilterB_ElectiveDiscipline()
    {
        ElectiveDiscipline retVal = null;

        if (getSettings().get("electiveDisciplineB") != null) {
            List<ElectiveDiscipline> lst = getSettings().get("electiveDisciplineB");
            if (!lst.isEmpty()) {
                retVal = lst.get(0);
            }
        }
        return retVal;
    }

    public boolean isWarning() {
        return warning;
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }

    public boolean isSendMail() {
        return sendMail;
    }

    public void setSendMail(boolean sendMail) {
        this.sendMail = sendMail;
    }
}
