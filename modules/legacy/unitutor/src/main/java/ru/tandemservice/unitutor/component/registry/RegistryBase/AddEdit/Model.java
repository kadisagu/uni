package ru.tandemservice.unitutor.component.registry.RegistryBase.AddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.component.registry.RegistryBase.EduProgramSubjectGroup;
import ru.tandemservice.unitutor.entity.catalog.codes.EppRegistryStructureCodes;

import java.util.Date;
import java.util.List;

@Input({
        @Bind(key = "electiveDisciplineId", binding = "electiveDisciplineId"),
        @Bind(key = "electiveDisciplineTypeCode", binding = "electiveDisciplineTypeCode", required = true),
        @Bind(key = "educationYearId", binding = "educationYearId"),
        @Bind(key = "educationYearPartId", binding = "educationYearPartId")
})
public class Model {

    public static final String UDV = EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_KURS_PO_VYBORU;
    public static final String UF = EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_FAKULTATIV;

    private ISelectModel educationYearModel;
    private ISelectModel educationYearPartModel;
    private ISelectModel eduGroupModel;
    private ISelectModel courseModel;
    private ISelectModel disciplineModel;

    private Long electiveDisciplineId;
    private String electiveDisciplineTypeCode;
    private List<EduProgramSubjectGroup> eduGroupList;

    private Long _educationYearId;
    private Long _educationYearPartId;
    private EducationYear _educationYear;
    private YearDistributionPart _yearDistributionPart;
    private List<EppRegistryElementPart> _eppRegistryElementParts;
    private Date _subscribeToDate;


    private String annotation;
    private Long placesTotal;
    private boolean editMode;

    private List<Course> courseList;

    public List<EduProgramSubjectGroup> getEduGroupList() {
        return eduGroupList;
    }

    public void setEduGroupList(List<EduProgramSubjectGroup> eduGroupList) {
        this.eduGroupList = eduGroupList;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public Long getElectiveDisciplineId() {
        return electiveDisciplineId;
    }

    public void setElectiveDisciplineId(Long electiveDisciplineId) {
        this.electiveDisciplineId = electiveDisciplineId;
    }

    public Long getPlacesTotal() {
        return placesTotal;
    }

    public void setPlacesTotal(Long placesTotal) {
        this.placesTotal = placesTotal;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public ISelectModel getEducationYearModel() {
        return educationYearModel;
    }

    public void setEducationYearModel(ISelectModel educationYearModel) {
        this.educationYearModel = educationYearModel;
    }

    public ISelectModel getEducationYearPartModel() {
        return educationYearPartModel;
    }

    public void setEducationYearPartModel(ISelectModel educationYearPartModel) {
        this.educationYearPartModel = educationYearPartModel;
    }

    public ISelectModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel) {
        this.courseModel = courseModel;
    }

    public ISelectModel getDisciplineModel() {
        return disciplineModel;
    }

    public void setDisciplineModel(ISelectModel disciplineModel) {
        this.disciplineModel = disciplineModel;
    }

    public String getElectiveDisciplineTypeCode() {
        return electiveDisciplineTypeCode;
    }

    public void setElectiveDisciplineTypeCode(String electiveDisciplineTypeCode) {
        this.electiveDisciplineTypeCode = electiveDisciplineTypeCode;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }


    public ISelectModel getEduGroupModel() {
        return eduGroupModel;
    }

    public void setEduGroupModel(ISelectModel eduGroupModel) {
        this.eduGroupModel = eduGroupModel;
    }

    public List<EppRegistryElementPart> getEppRegistryElementParts()
    {
        return _eppRegistryElementParts;
    }

    public void setEppRegistryElementParts(List<EppRegistryElementPart> eppRegistryElementParts)
    {
        _eppRegistryElementParts = eppRegistryElementParts;
    }

    public Long getEducationYearId()
    {
        return _educationYearId;
    }

    public void setEducationYearId(Long educationYearId)
    {
        _educationYearId = educationYearId;
    }

    public Long getEducationYearPartId()
    {
        return _educationYearPartId;
    }

    public void setEducationYearPartId(Long educationYearPartId)
    {
        _educationYearPartId = educationYearPartId;
    }

    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EducationYear educationYear)
    {
        _educationYear = educationYear;
    }

    public YearDistributionPart getYearDistributionPart()
    {
        return _yearDistributionPart;
    }

    public void setYearDistributionPart(YearDistributionPart yearDistributionPart)
    {
        _yearDistributionPart = yearDistributionPart;
    }

    public Date getSubscribeToDate()
    {
        return _subscribeToDate;
    }

    public void setSubscribeToDate(Date subscribeToDate)
    {
        _subscribeToDate = subscribeToDate;
    }
}
