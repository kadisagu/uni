package ru.tandemservice.unitutor.dao;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;

/**
 * обертка для отображения на экране
 * (обертку будем использовать для всего, в том числе и для САКАЙ части)
 * Добавить:
 * 1) EppWorkPlanRegistryElementRow
 * 2) term
 * <p/>
 * Для УДВ и УФ используем урезанный вариант обертки(@author Ivan Anishchenko)
 *
 * @author vch
 */
public class DisciplineWrap extends EntityBase implements IEntity {

    /**
     * id группы дисциплин
     */
    private Long parentId;

    /**
     * название родительской группы
     */
    private String parentName;

    /**
     * количество дисциплин в группе
     */
    private int countInGroup;

    /**
     * часть элемента реестра
     */
    private EppRegistryElementPart eppRegistryElementPart;


    /**
     * начальное состояние подписки
     */
    private Long electiveDisciplineSubscribeId;

    private boolean incorrectRow = false;

    private String sourceCode;

    ///////////////////////////////////////
    /////// добавлено для общей совместимости
    ///////////////////////////////////////
    /**
     * Строка РУП
     */
    private EppWorkPlanRegistryElementRow eppWorkPlanRegistryElementRow;

    /**
     * Дисциплина по выбору (ДПВ)
     */
    private ElectiveDiscipline electiveDiscipline;

    private int term;
    /**
     * предупреждения
     */
    private String warningRow;

    /**
     * индекс строки по УПУ
     * return ((IEppWorkPlanRowWrapper)entity).getNumber();
     */
    private String rowIndex;


    public DisciplineWrap(EppWorkPlanRegistryElementRow eppWorkPlanRegistryElementRow, ElectiveDiscipline electiveDiscipline,
                    int term, String sourseCode)
    {
        this.eppWorkPlanRegistryElementRow = eppWorkPlanRegistryElementRow;
        this.electiveDiscipline = electiveDiscipline;
        if (eppWorkPlanRegistryElementRow != null)
            setEppRegistryElementPart(eppWorkPlanRegistryElementRow.getRegistryElementPart());

        setTerm(term);
        setSourceCode(sourseCode);

    }

    @Override
    public Long getId()
    {
        return eppRegistryElementPart.getId();
    }

    public EppRegistryElementPart getEppRegistryElementPart() {
        return eppRegistryElementPart;
    }

    public void setEppRegistryElementPart(EppRegistryElementPart eppRegistryElementPart) {
        this.eppRegistryElementPart = eppRegistryElementPart;
    }

    public boolean isSubcribed()
    {
        if (electiveDisciplineSubscribeId != null)
            return true;
        else
            return false;
    }


    public int getCountInGroup() {
        return countInGroup;
    }

    public void setCountInGroup(int countInGroup) {
        this.countInGroup = countInGroup;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getTitle()
    {
        String title = "";

        if (getParentName() != null)
            title += "(" + getParentName() + ") ";


        title += getEppRegistryElementPart().getTitle();


        return title;
    }

    public Long getElectiveDisciplineSubscribeId() {
        return electiveDisciplineSubscribeId;
    }

    public void setElectiveDisciplineSubscribeId(Long electiveDisciplineSubscribeId)
    {
        this.electiveDisciplineSubscribeId = electiveDisciplineSubscribeId;
    }

    public boolean isIncorrectRow() {
        return incorrectRow;
    }

    public void setIncorrectRow(boolean incorrectRow) {
        this.incorrectRow = incorrectRow;
    }

    public EppWorkPlanRegistryElementRow getEppWorkPlanRegistryElementRow() {
        return eppWorkPlanRegistryElementRow;
    }

    public void setEppWorkPlanRegistryElementRow(EppWorkPlanRegistryElementRow eppWorkPlanRegistryElementRow)
    {
        this.eppWorkPlanRegistryElementRow = eppWorkPlanRegistryElementRow;
    }

    public ElectiveDiscipline getElectiveDiscipline() {
        return electiveDiscipline;
    }

    public void setElectiveDiscipline(ElectiveDiscipline electiveDiscipline) {
        this.electiveDiscipline = electiveDiscipline;
    }

    public String getWarningRow() {
        return warningRow;
    }

    public void setWarningRow(String warningRow) {
        this.warningRow = warningRow;
    }

    public void addWarningRow(String warning)
    {
        if (warning == null || warning.isEmpty())
            return;

        if (warningRow == null)
            warningRow = "";

        warningRow += " " + warning;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(String rowIndex) {
        this.rowIndex = rowIndex;
    }
}
