package ru.tandemservice.unitutor.component.settings.ChoiceElectiveDisciplineSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onChangeEduYear(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().onChangeEduYear(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().update(model);
        deactivate(component);
    }
}
