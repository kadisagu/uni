/* $Id$ */
package ru.tandemservice.unitutor.base.bo.MassStudentSubscribe.ui.Add;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.dao.DisciplineWrap;
import ru.tandemservice.unitutor.dao.IDaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.entity.gen.ElectiveDisciplineSubscribeGen;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 13.11.2015
 */
@Input({
        @Bind(key = "orgUnitId", binding = "orgUnitId"),
        @Bind(key = "educationYearId", binding = "educationYearId", required = true),
        @Bind(key = "yearDistributionPartId", binding = "yearDistributionPartId", required = true),
        @Bind(key = "electiveDisciplineTypeCode", binding = "electiveDisciplineTypeCode", required = true)
})
public class MassStudentSubscribeAddUI extends UIPresenter
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;
    private String _electiveDisciplineTypeCode;
    private Long _educationYearId;
    private Long _yearDistributionPartId;
    private List<Group> _groups;

    public static String MYTEX = "MYTEX_SUBSCRIBE";

    public List<Group> getGroups()
    {
        return _groups;
    }

    public void setGroups(List<Group> groups)
    {
        _groups = groups;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public String getElectiveDisciplineTypeCode()
    {
        return _electiveDisciplineTypeCode;
    }

    public void setElectiveDisciplineTypeCode(String electiveDisciplineTypeCode)
    {
        _electiveDisciplineTypeCode = electiveDisciplineTypeCode;
    }

    public Long getEducationYearId()
    {
        return _educationYearId;
    }

    public void setEducationYearId(Long educationYearId)
    {
        _educationYearId = educationYearId;
    }

    public Long getYearDistributionPartId()
    {
        return _yearDistributionPartId;
    }

    public void setYearDistributionPartId(Long yearDistributionPartId)
    {
        _yearDistributionPartId = yearDistributionPartId;
    }


    @Override
    public void onComponentRefresh()
    {
        if (_orgUnitId != null)
            _orgUnit = DataAccessServices.dao().getNotNull(OrgUnit.class, _orgUnitId);
        getSettings().set(MassStudentSubscribeAdd.EDU_YEAR, DataAccessServices.dao().getNotNull(EducationYear.class, _educationYearId));
        getSettings().set(MassStudentSubscribeAdd.EDU_YEAR_PART, DataAccessServices.dao().getNotNull(YearDistributionPart.class, _yearDistributionPartId));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (MassStudentSubscribeAdd.GROUP_DS.equals(dataSource.getName()))
            dataSource.put(MassStudentSubscribeAdd.ORG_UNIT, getOrgUnit());
        else if (MassStudentSubscribeAdd.STUDENTS_SUBSCRIBE_DS.equals(dataSource.getName()))
        {
            dataSource.put(MassStudentSubscribeAdd.ORG_UNIT, getOrgUnit());
            dataSource.put(MassStudentSubscribeAdd.EDU_YEAR, getSettings().get(MassStudentSubscribeAdd.EDU_YEAR));
            dataSource.put(MassStudentSubscribeAdd.EDU_YEAR_PART, getSettings().get(MassStudentSubscribeAdd.EDU_YEAR_PART));
            dataSource.put(MassStudentSubscribeAdd.STATES_LIST, getSettings().get(MassStudentSubscribeAdd.STATES_LIST));
            dataSource.put(MassStudentSubscribeAdd.GROUP_LIST, _groups);
            dataSource.put(MassStudentSubscribeAdd.ELECTIVE_DISCIPLINE_CODE, _electiveDisciplineTypeCode);
        }
    }

    public void onSaveSettings()
    {
        DataSettingsFacade.saveSettings(getSettings());
    }

    public boolean isDPV()
    {
        return ElectiveDisciplineTypeCodes.DPV.equals(_electiveDisciplineTypeCode);
    }

    public boolean isNotDPV()
    {
        return !isDPV();
    }


    public void onClearSettings()
    {
        getSettings().clear();
        onSaveSettings();
        getGroups().clear();
    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (MassStudentSubscribeAdd.STUDENTS_SUBSCRIBE_DS.equals(dataSource.getName()))
        {
            final CheckboxColumn check = (CheckboxColumn) ((BaseSearchListDataSource) dataSource).getLegacyDataSource().getColumn("subscribed");
            Set<IEntity> checked = new HashSet<>();

            List<DataWrapper> wrappersList = dataSource.getRecords();
            Set<Student> students = wrappersList.stream().map(e -> (Student) e.getProperty("student")).collect(Collectors.toSet());
            Map<PairKey, DataWrapper> result = wrappersList.stream().collect(
                    Collectors.toMap(object -> new PairKey<Student, ElectiveDiscipline>((Student) object.getProperty("student"), ((DisciplineWrap) object.getProperty("discipline")).getElectiveDiscipline()),
                                     object -> object));

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ElectiveDisciplineSubscribe.class, "el")
                    .where(in(property("el", ElectiveDisciplineSubscribe.student()), students))
                    .where(isNull(property("el", ElectiveDisciplineSubscribe.removalDate())))
                    .where(eq(property("el", ElectiveDisciplineSubscribe.electiveDiscipline().type().code()), value(getElectiveDisciplineTypeCode())));
            List<ElectiveDisciplineSubscribe> listSubscribed = DataAccessServices.dao().getList(builder);

            for (ElectiveDisciplineSubscribe discipline : listSubscribed)
            {
                DataWrapper wrap = result.get(new PairKey<Student, ElectiveDiscipline>(discipline.getStudent(), discipline.getElectiveDiscipline()));
                if (wrap != null)
                    checked.add(wrap);
            }
            if (check != null)
                check.setSelectedObjects(checked);
        }

    }

    public void onClickApply()
    {
            saveSubscribe();
    }


    /**
     * Сохранение ДПВ
     */
    public void saveSubscribe()
    {
        synchronized (MYTEX) {
            IDaoStudentSubscribe idao = DaoStudentSubscribe.instanse();
            List<IEntity> selectedDisciplines = getSelectedEntities();
            Map<Student, Set<DisciplineWrap>> studentToDisciplineWrap = SafeMap.get(HashSet.class);
            selectedDisciplines.stream().map(object -> (DataWrapper) object)
                    .forEach(object -> studentToDisciplineWrap.get(object.getProperty("student")).add(((DisciplineWrap) object.getProperty("discipline"))));

            ErrorCollector errorCollector = validateDisciplineCount(studentToDisciplineWrap);

            if (errorCollector.hasErrors())
                return;

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(ElectiveDisciplineSubscribe.class, "eds")
                    .where(in(property(ElectiveDisciplineSubscribe.student().fromAlias("eds")), studentToDisciplineWrap.keySet()))
                    .where(eq(property("eds", ElectiveDisciplineSubscribe.electiveDiscipline().type().code()), value(getElectiveDisciplineTypeCode())))
                    .column("eds");

            Map<Student, List<ElectiveDisciplineSubscribe>> mapSubscribed = DataAccessServices.dao().getList(dql).stream().map(e -> (ElectiveDisciplineSubscribe) e).collect(Collectors.groupingBy(e -> e.getStudent()));


            for (Student student : studentToDisciplineWrap.keySet())
            {
                List<ElectiveDiscipline> elDisc = studentToDisciplineWrap.get(student).stream().map(DisciplineWrap::getElectiveDiscipline).collect(Collectors.toList());
                if (isDPV())
                    idao.saveStudentSubscribeByYearAndPart(student, getSettings().get(MassStudentSubscribeAdd.EDU_YEAR),
                                                           getSettings().get(MassStudentSubscribeAdd.EDU_YEAR_PART), elDisc, null, false, false);
                else
                {
                    saveElectiveDisciplines(mapSubscribed, student, elDisc);
                }
            }
        }
        deactivate();
    }


    /**
     * Подписка на УДВ и УФ
     */
    private void saveElectiveDisciplines(Map<Student, List<ElectiveDisciplineSubscribe>> mapSubscribed, Student student, List<ElectiveDiscipline> elDisc)
    {
       List<ElectiveDisciplineSubscribe> subscibeList = new ArrayList<>();
        List<ElectiveDisciplineSubscribe> subList = mapSubscribed.get(student);
        Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> studentSubscribe = subList!= null?  subList.stream()
                .collect(Collectors.toMap(ElectiveDisciplineSubscribeGen::getElectiveDiscipline, e -> e)): new HashMap<>();

        for (ElectiveDiscipline ed : elDisc)
        {
            ElectiveDisciplineSubscribe subscribe = studentSubscribe.remove(ed);
            if (subscribe == null)
            {
                subscribe = new ElectiveDisciplineSubscribe(ed, student, new Date(), null, null);
                subscibeList.add(subscribe);
            }
            else if (subscribe.getRemovalDate() != null)
            {
                subscibeList.add(subscribe);
            }
        }

        DaoStudentSubscribe.instanse().saveElectiveSubscribe(student, subscibeList, false);

        // отписываем
        List<ElectiveDisciplineSubscribe> unsubscribeLst = new ArrayList<>();
        for (Map.Entry<ElectiveDiscipline, ElectiveDisciplineSubscribe> entry : studentSubscribe.entrySet())
        {
            unsubscribeLst.add(entry.getValue());
        }

        DaoStudentSubscribe.instanse().unsubscribeElectiveDisciplines(student, unsubscribeLst, false);
    }


    private List<IEntity> getSelectedEntities()
    {
        return ((BaseSearchListDataSource) getConfig()
                .getDataSource(MassStudentSubscribeAdd.STUDENTS_SUBSCRIBE_DS))
                .getLegacyDataSource()
                .getSelectedEntities();

    }


    /**
     * Проверка выбора ДПВ
     * @param studentToDisciplineWrap
     * @return
     */
    public ErrorCollector validateDisciplineCount(Map<Student, Set<DisciplineWrap>> studentToDisciplineWrap)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
            for (Student student : studentToDisciplineWrap.keySet())
            {
                Set<DisciplineWrap> disciplines = studentToDisciplineWrap.get(student);
                if (isDPV())
                {

                    Map<Long, List<DisciplineWrap>> disc2groupMap = SafeMap.get(ArrayList.class);
                    for (DisciplineWrap wrap : disciplines)
                        disc2groupMap.get(wrap.getParentId()).add(wrap);

                    for (Long groupId : disc2groupMap.keySet())
                    {
                        List<DisciplineWrap> wraps = disc2groupMap.get(groupId);
                        if (!wraps.isEmpty() && wraps.size() > wraps.get(0).getCountInGroup())
                            errorCollector.add("Для студента " + student.getFio() + "число выбранных дисциплин в группе "
                                                       + wraps.get(0).getParentName() + " больше допустимого (" + wraps.get(0).getCountInGroup() + ")");
                    }
                }
                else
                {
                    int count = DaoStudentSubscribe.instanse().getCountElectiveInWorkPlan(getSettings().get(MassStudentSubscribeAdd.EDU_YEAR),
                                                                                          getSettings().get(MassStudentSubscribeAdd.EDU_YEAR_PART), student, getElectiveDisciplineTypeCode());
                    if (disciplines.size() > count)
                        errorCollector.add("Для студента " + student.getFio() + " число выбранных дисциплин больше допустимого (" + count + ")");
                }

            }

        return errorCollector;

    }

}