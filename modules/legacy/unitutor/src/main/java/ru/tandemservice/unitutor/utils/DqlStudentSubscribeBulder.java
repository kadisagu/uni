package ru.tandemservice.unitutor.utils;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DqlStudentSubscribeBulder {


    public static String ELECIVE_DISC_SUBSCRIBE_ALIAS = "ElectiveDisciplineSubscribe";

    /**
     * смотрим, на что у нас подписан студент
     * используем для проверок
     * на выходе колонка с id registryElement
     *
     * @param educationYear
     * @param yearDistributionPart
     * @param student
     * @param group
     *
     * @return
     */
    public static DQLSelectBuilder getDqlElectiveDisciplineSubscribe(EducationYear educationYear, YearDistributionPart yearDistributionPart,
            Student student, EppEpvGroupImRow group)
    {
        // подзапрос по дисциплинам из группы
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "EppEpvRegistryRow")
                        // только из группы
                .where(eq(property(EppEpvRegistryRow.parent().id().fromAlias("EppEpvRegistryRow")), value(group.getId())))
                        // дисциплина
                .column(EppEpvRegistryRow.registryElement().id().fromAlias("EppEpvRegistryRow").s());


        // на что подписан
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, ELECIVE_DISC_SUBSCRIBE_ALIAS)

                        // ДПВ
                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().type().code().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS)), ElectiveDisciplineTypeCodes.DPV))

                        // по студенту
                .where(eqValue(
                        property(ElectiveDisciplineSubscribe.student().id().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS)),
                        student.getId()));

        // год
        if (educationYear != null)
            dql.where(eqValue(
                    property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().id().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS)),
                    educationYear.getId()));

        // часть года
        if (yearDistributionPart != null)
            dql.where(eqValue(
                    property(ElectiveDisciplineSubscribe.electiveDiscipline().yearPart().id().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS)),
                    yearDistributionPart.getId()));

        // актуальные записи
        dql.where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS))));

        // все из группы
        dql.where(in(
                property(ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().registryElement().id().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS)),
                dqlIn.buildQuery()))

                .column(ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().registryElement().id().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS).s());

        return dql;
    }


    /**
     * часть первая - что должен выбрать студент (без разделения на группы)
     * Список строк РУП, содержащих ДПВ
     */
    public static DQLSelectBuilder getDqlStudentEppWorkPlanRegistryElementRow(EducationYear educationYear,
                              YearDistributionPart yearDistributionPart, Student student)
    {
        DQLSelectBuilder dqlIn = DqlDisciplineBuilder.DqlInWorkPlanRegistryElementRow(educationYear, yearDistributionPart, null, null,
                                                                                      null, null, null, null, Collections.singletonList(student), null);

        dqlIn.column(property(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS));

        return dqlIn;
    }


    public static DQLSelectBuilder getDqlStudentEppEpvGroupImRow(EducationYear educationYear, YearDistributionPart yearDistributionPart, Student student)
    {

        List<Student> lstStudent = Collections.singletonList(student);

        // версии учебных планов
        DQLSelectBuilder dqlInEduPlanVersion =
                DqlDisciplineBuilder.getDqlInEppStudent2WorkPlan(educationYear, yearDistributionPart, null, null, null, null, lstStudent);

        dqlInEduPlanVersion.column(property(DqlDisciplineBuilder.STUD_EDU_PLAN_VERSION, EppStudent2EduPlanVersion.eduPlanVersion().id()));

        // дисциплины
        DQLSelectBuilder dqlIn = DqlDisciplineBuilder.DqlInWorkPlanRegistryElementRow(educationYear, yearDistributionPart, null, null, null, null, null
                , null, lstStudent, null);
        dqlIn.column(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias(DqlDisciplineBuilder.WORK_PLAN_REGISTRY_ROW_ALIAS).s());


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN)
//                .joinEntity(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN, DQLJoinType.inner, EppEpvGroupImRow.class, DqlDisciplineBuilder.EPP_EPV_GROUP_ROW_IN,
//                            eq(property(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN, EppEpvRegistryRow.parent().id()), property(DqlDisciplineBuilder.EPP_EPV_GROUP_ROW_IN + ".id")))

                        // только нужные версии уп
                .where(in(property(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN, EppEpvRegistryRow.owner().eduPlanVersion().id()), dqlInEduPlanVersion.buildQuery()))

                        // только нужные дисциплины (они точно в группах) (далее берем 1-ого родителя)
                .where(in(property(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN, EppEpvRegistryRow.registryElement().id()), dqlIn.buildQuery()))

                        // вывод
                .column(property(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN));

        return dql;
    }


    /**
     * dql без колонок по ElectiveDisciplineSubscribe - на что подписаны студенты
     * алиас=ELECIVE_DISC_SUBSCRIBE_ALIAS
     *
     * @param educationYear
     * @param yearDistributionPart
     * @param student
     *
     * @return
     */
    public static DQLSelectBuilder getDqlStudentSubscribe(EducationYear educationYear, YearDistributionPart yearDistributionPart,
            Student student, String electiveDisciplineTypeCode)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, ELECIVE_DISC_SUBSCRIBE_ALIAS)

                        // Тип дисциплины
                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().type().code().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS)), electiveDisciplineTypeCode))

                        // по студенту
                .where(eq(property(ElectiveDisciplineSubscribe.student().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS)), value(student)));

        // год

        if (educationYear != null)
            dql.where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS)), educationYear));

        // часть года
        if (yearDistributionPart != null)
            dql.where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().yearPart().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS)), yearDistributionPart));

        // актуальные записи
        dql.where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias(ELECIVE_DISC_SUBSCRIBE_ALIAS))));

        return dql;
    }
}
