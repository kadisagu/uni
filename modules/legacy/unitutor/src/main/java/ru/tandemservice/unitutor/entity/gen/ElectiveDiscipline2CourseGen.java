package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline2Course;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность связь ДПВ - Курс
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ElectiveDiscipline2CourseGen extends EntityBase
 implements INaturalIdentifiable<ElectiveDiscipline2CourseGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.ElectiveDiscipline2Course";
    public static final String ENTITY_NAME = "electiveDiscipline2Course";
    public static final int VERSION_HASH = -775146385;
    private static IEntityMeta ENTITY_META;

    public static final String L_ELECTIVE_DISCIPLINE = "electiveDiscipline";
    public static final String L_COURSE = "course";

    private ElectiveDiscipline _electiveDiscipline;     // Дисциплина по выбору (ДПВ, УДВ, УФ)
    private Course _course;     // курс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина по выбору (ДПВ, УДВ, УФ). Свойство не может быть null.
     */
    @NotNull
    public ElectiveDiscipline getElectiveDiscipline()
    {
        return _electiveDiscipline;
    }

    /**
     * @param electiveDiscipline Дисциплина по выбору (ДПВ, УДВ, УФ). Свойство не может быть null.
     */
    public void setElectiveDiscipline(ElectiveDiscipline electiveDiscipline)
    {
        dirty(_electiveDiscipline, electiveDiscipline);
        _electiveDiscipline = electiveDiscipline;
    }

    /**
     * @return курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ElectiveDiscipline2CourseGen)
        {
            if (withNaturalIdProperties)
            {
                setElectiveDiscipline(((ElectiveDiscipline2Course)another).getElectiveDiscipline());
                setCourse(((ElectiveDiscipline2Course)another).getCourse());
            }
        }
    }

    public INaturalId<ElectiveDiscipline2CourseGen> getNaturalId()
    {
        return new NaturalId(getElectiveDiscipline(), getCourse());
    }

    public static class NaturalId extends NaturalIdBase<ElectiveDiscipline2CourseGen>
    {
        private static final String PROXY_NAME = "ElectiveDiscipline2CourseNaturalProxy";

        private Long _electiveDiscipline;
        private Long _course;

        public NaturalId()
        {}

        public NaturalId(ElectiveDiscipline electiveDiscipline, Course course)
        {
            _electiveDiscipline = ((IEntity) electiveDiscipline).getId();
            _course = ((IEntity) course).getId();
        }

        public Long getElectiveDiscipline()
        {
            return _electiveDiscipline;
        }

        public void setElectiveDiscipline(Long electiveDiscipline)
        {
            _electiveDiscipline = electiveDiscipline;
        }

        public Long getCourse()
        {
            return _course;
        }

        public void setCourse(Long course)
        {
            _course = course;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof ElectiveDiscipline2CourseGen.NaturalId) ) return false;

            ElectiveDiscipline2CourseGen.NaturalId that = (NaturalId) o;

            if( !equals(getElectiveDiscipline(), that.getElectiveDiscipline()) ) return false;
            if( !equals(getCourse(), that.getCourse()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getElectiveDiscipline());
            result = hashCode(result, getCourse());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getElectiveDiscipline());
            sb.append("/");
            sb.append(getCourse());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ElectiveDiscipline2CourseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ElectiveDiscipline2Course.class;
        }

        public T newInstance()
        {
            return (T) new ElectiveDiscipline2Course();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "electiveDiscipline":
                    return obj.getElectiveDiscipline();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "electiveDiscipline":
                    obj.setElectiveDiscipline((ElectiveDiscipline) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "electiveDiscipline":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "electiveDiscipline":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "electiveDiscipline":
                    return ElectiveDiscipline.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ElectiveDiscipline2Course> _dslPath = new Path<ElectiveDiscipline2Course>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ElectiveDiscipline2Course");
    }
            

    /**
     * @return Дисциплина по выбору (ДПВ, УДВ, УФ). Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2Course#getElectiveDiscipline()
     */
    public static ElectiveDiscipline.Path<ElectiveDiscipline> electiveDiscipline()
    {
        return _dslPath.electiveDiscipline();
    }

    /**
     * @return курс. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2Course#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends ElectiveDiscipline2Course> extends EntityPath<E>
    {
        private ElectiveDiscipline.Path<ElectiveDiscipline> _electiveDiscipline;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина по выбору (ДПВ, УДВ, УФ). Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2Course#getElectiveDiscipline()
     */
        public ElectiveDiscipline.Path<ElectiveDiscipline> electiveDiscipline()
        {
            if(_electiveDiscipline == null )
                _electiveDiscipline = new ElectiveDiscipline.Path<ElectiveDiscipline>(L_ELECTIVE_DISCIPLINE, this);
            return _electiveDiscipline;
        }

    /**
     * @return курс. Свойство не может быть null.
     * @see ru.tandemservice.unitutor.entity.ElectiveDiscipline2Course#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return ElectiveDiscipline2Course.class;
        }

        public String getEntityName()
        {
            return "electiveDiscipline2Course";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
