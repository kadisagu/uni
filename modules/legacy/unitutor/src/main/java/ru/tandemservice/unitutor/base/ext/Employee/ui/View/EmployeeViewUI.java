package ru.tandemservice.unitutor.base.ext.Employee.ui.View;


import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;

import java.util.List;

public class EmployeeViewUI extends org.tandemframework.shared.employeebase.base.bo.Employee.ui.View.EmployeeViewUI {

    private List<Long> orgUnitIds;

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        if (getEmployee() != null)
            setOrgUnitIds(OptionalDisciplineUtil.getOrgUnitList4Tutor(getEmployee()));
    }

    public List<Long> getOrgUnitIds() {
        return orgUnitIds;
    }

    public void setOrgUnitIds(List<Long> orgUnitIds) {
        this.orgUnitIds = orgUnitIds;
    }

    public boolean isTutor() {
        return  (getOrgUnitIds() != null && getOrgUnitIds().size() > 0);
    }
}
