package ru.tandemservice.unitutor.component.awp.DisciplineList;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unitutor.component.awp.base.IFilterProperties;
import ru.tandemservice.unitutor.component.awp.base.discipline.ModelBase;

import java.util.Date;
import java.util.List;


public class Model extends ModelBase
{

    private final static String SECURITY_POSTFIX = "optionalDiscipline_list";
    private DynamicListDataSource<Wrapper> dataSource;

    private ISelectModel freeStatusSelectModel;

    private ISelectModel actualStatusSelectModel;

    private ISelectModel _eduPlanListModel;
    private ISelectModel _workPlanListModel;

    private Date lastDate = new Date();


    private boolean validDisciplineArray;

    /**
     * проверка была выполнена
     */
    private boolean hasBeChecked = false;

    private String dpvChekInfo = "";


    @Override
    protected String getSecurityPostfix() {
        return SECURITY_POSTFIX;
    }

    public DynamicListDataSource<Wrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<Wrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getFreeStatusSelectModel() {
        return freeStatusSelectModel;
    }

    public void setFreeStatusSelectModel(ISelectModel freeStatusSelectModel) {
        this.freeStatusSelectModel = freeStatusSelectModel;
    }

    @SuppressWarnings("rawtypes")
    public Long getFilterFreeStatus()
    {
        if (getSettings().get(IFilterProperties.PLACES_FILTER) == null)
            return null;
        else
            return ((IdentifiableWrapper) getSettings().get(IFilterProperties.PLACES_FILTER)).getId();
    }

    @SuppressWarnings("rawtypes")
    public Long getFilterActualStatus()
    {
        if (getSettings().get(IFilterProperties.ACTUAL_FILTER) == null)
            return null;
        else
            return ((IdentifiableWrapper) getSettings().get(IFilterProperties.ACTUAL_FILTER)).getId();
    }

    public boolean isForNullValue()
    {
        boolean nullValue = false;
        if (getSettings().get("nullValue") != null)
            nullValue = getSettings().get("nullValue");

        return nullValue;
    }

    public List<EppEduPlan> getFilterEduPlan()
    {
        return getSettings().get(IFilterProperties.EPP_EDUPLAN_FILTER);
    }
    public List<EppWorkPlan> getFilterWorkPlan()
    {
        return getSettings().get(IFilterProperties.EPP_WORKPLAN_FILTER);
    }

    public boolean isValidDisciplineArray() {
        return validDisciplineArray;
    }

    public void setValidDisciplineArray(boolean validDisciplineArray) {
        this.validDisciplineArray = validDisciplineArray;
    }

    public boolean isHasBeChecked() {
        return hasBeChecked;
    }

    public void setHasBeChecked(boolean hasBeChecked)
    {
        this.hasBeChecked = hasBeChecked;
        lastDate = new Date();

    }

    public Boolean isElaspend()
    {
        // устаревает раз в 3 минуты
        long lastTime = lastDate.getTime() + 1000 * 60 * 3;
        long nowTime = (new Date()).getTime();

        return (nowTime > lastTime);
    }

    public ISelectModel getActualStatusSelectModel() {
        return actualStatusSelectModel;
    }

    public void setActualStatusSelectModel(ISelectModel actualStatusSelectModel) {
        this.actualStatusSelectModel = actualStatusSelectModel;
    }

    public String getDpvChekInfo() {
        return dpvChekInfo;
    }

    public void setDpvChekInfo(String dpvChekInfo) {
        this.dpvChekInfo = dpvChekInfo;
    }

    public ISelectModel getEduPlanListModel()
    {
        return _eduPlanListModel;
    }

    public void setEduPlanListModel(ISelectModel eduPlanListModel)
    {
        _eduPlanListModel = eduPlanListModel;
    }

    public ISelectModel getWorkPlanListModel()
    {
        return _workPlanListModel;
    }

    public void setWorkPlanListModel(ISelectModel workPlanListModel)
    {
        _workPlanListModel = workPlanListModel;
    }
}
