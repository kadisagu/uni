package ru.tandemservice.unitutor.component.awp.StudentOrientationMassAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unitutor.entity.Student2Orientation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<Student> dataSource = UniBaseUtils.createDataSource(component, ((IDAO) getDao()));

        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new PublisherLinkColumn("ФИО студента", Student.person().identityCard().fullFio())
                                     .setResolver(new IPublisherLinkResolver() {
                                         @SuppressWarnings("unchecked")
                                         @Override
                                         public Object getParameters(IEntity entity) {
                                             return ParametersMap.createWith(PublisherActivator.PUBLISHER_ID_KEY, ((ViewWrapper<Student>) entity).getEntity().getId());
                                         }
                                         @Override
                                         public String getComponentName(IEntity paramIEntity) {
                                             return null;
                                         }
                                     })
                                     .setClickable(true)
                                     .setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Выбранная направленность", "choosenOrientation." + Student2Orientation.orientation().educationLevelHighSchool().educationLevel().fullTitleWithRootLevel()).setOrderable(false).setClickable(false));
        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = getModel(component);
        component.saveSettings();
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = getModel(component);
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Выберите студентов.");

        Map<Student, Student2Orientation> map = new HashMap<>();
        for (IEntity entity : entityList) {
            ViewWrapper wr = (ViewWrapper) entity;
            map.put((Student) wr.getEntity(), (Student2Orientation) wr.getViewProperty("choosenOrientation"));
        }

        getDao().onMassAdd(map, model);
        ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects().clear();
        deactivate(component);
    }
}