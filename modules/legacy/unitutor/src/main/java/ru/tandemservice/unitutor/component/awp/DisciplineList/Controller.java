package ru.tandemservice.unitutor.component.awp.DisciplineList;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unitutor.component.awp.base.discipline.ControllerBase;
import ru.tandemservice.unitutor.dao.print.IElectivePrintDAO;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.utils.UniEppFormatter;

import java.util.*;


public class Controller extends ControllerBase<Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        Model model = component.getModel();

        // проверку не запускали
        if (!model.isHasBeChecked() || model.isElaspend()) {
            boolean isValid = ((IDAO) getDao()).checkRegistryElementPart(model.getFilterEducationYear());

            String dpvInfo;

            if (isValid) {
                Date date = new Date();
                dpvInfo = "ДПВ корректны, проверка проведена в " + DateFormatter.DATE_FORMATTER_WITH_TIME.format(date);
            }
            else
                dpvInfo = "ДПВ требует актуализации";

            model.setDpvChekInfo(dpvInfo);

            model.setValidDisciplineArray(isValid);
            model.setHasBeChecked(true);
        }

    }

    /**
     * Запускаем проверку ДПВ
     */
    public void onActualaziRow(IBusinessComponent component)
    {
        Model model = component.getModel();
        // исправить записи на текущий год
        ((IDAO)getDao()).fixElectiveDisciplineRow(model.getFilterEducationYear());
        model.setHasBeChecked(false);
        onClickSearch(component);
    }

    @Override
    public void onClickSearch(IBusinessComponent component)
    {
        validateFilters(component);
        Model model = getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());

        onRefreshComponent(component);
    }

    @Override
    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        IDataSettings settings = model.getSettings();
        settings.clear();
        this.onClickSearch(component);
    }

    /**
     * листенер смены учебного года
     */
    public void onChangeeducationYear(IBusinessComponent component)
    {
        Model model = component.getModel();
        model.setHasBeChecked(false);
    }

    @Override
    protected void prepareDataSource(IBusinessComponent component)
    {
        Model model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<Wrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());

        createColumns(dataSource, model);
        model.setDataSource(dataSource);
    }

    private void createColumns(DynamicListDataSource<Wrapper> dataSource, Model model)
    {
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Название ДПВ", "electiveDiscipline." + ElectiveDiscipline.registryElementPart().registryElement().title()).setOrderable(true).setWidth("200"));
        dataSource.addColumn(new SimpleColumn("Индекс", "eppWPRElRowIndex").setOrderable(true).setWidth("80"));
        dataSource.addColumn(new SimpleColumn("РУП", "eppWPlanTitle").setOrderable(true).setWidth("200"));
        dataSource.addColumn(new PublisherLinkColumn("№ в реестре", "electiveDiscipline." + ElectiveDiscipline.registryElementPart().registryElement().number())
                                     .setResolver(new IPublisherLinkResolver() {
                                         @Override
                                         public Object getParameters(IEntity entity)
                                         {
                                             final Map<String, Object> parameters = new HashMap<>();
                                             Long id = entity.getId();

                                             ElectiveDiscipline ed = UniDaoFacade.getCoreDao().get(id);

                                             parameters.put(PublisherActivator.PUBLISHER_ID_KEY
                                                     , ed.getRegistryElementPart().getRegistryElement().getId());
                                             return parameters;
                                         }

                                         @Override
                                         public String getComponentName(IEntity paramIEntity) {
                                             return null;
                                         }
                                     })
                                     .setOrderable(true).setWidth("25"));


        dataSource.addColumn(new BooleanColumn("Аннотация", "containAnnotation").setWidth("50").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Часов всего", "electiveDiscipline." + ElectiveDiscipline.registryElementPart().size(), new UniEppFormatter()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("ЗЕТ",
                                              "electiveDiscipline." + ElectiveDiscipline.registryElementPart().labor(), new UniEppFormatter()).setClickable(false).setOrderable(false));


        // форму контроля временно исключаем
        dataSource.addColumn(new SimpleColumn("Формы контроля",
                                              "formOfControl").setWidth("100").setClickable(false).setOrderable(false));

        HeadColumn placesCountHeadColumn = new HeadColumn("placesCount", "Количество мест");
        placesCountHeadColumn.addColumn(new SimpleColumn("Всего", "electiveDiscipline." + ElectiveDiscipline.placesCount()).setOrderable(true));

        placesCountHeadColumn.addColumn(new SimpleColumn("Записано", "placesSubscribed")).setWidth("15").setAlign("center").setClickable(true).setOrderable(false);
        placesCountHeadColumn.addColumn(new SimpleColumn("Свободно", "freePlacesToSubscribe")).setWidth("20").setAlign("center").setClickable(false).setOrderable(false);

        dataSource.addColumn(placesCountHeadColumn.setHeaderAlign("center").setRequired(true));

        dataSource.addColumn(new DateColumn("Запись до", "electiveDiscipline." + ElectiveDiscipline.subscribeToDate()).setOrderable(true));

        dataSource.addColumn(new BooleanColumn("Актуальность", "electiveDiscipline." + ElectiveDiscipline.actualRow()).setOrderable(true));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey(model.getSec().getPermission("edit")));

    }

    /**
     * перенес
     */
    public void onClickStudentSubscribesPrint(IBusinessComponent component) {
        Model model = component.getModel();
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать дисциплины для печати списков студентов ! ");

        List<ElectiveDiscipline> disciplines = new ArrayList<>();
        for (IEntity entity : entityList) {
            Wrapper wrapper = (Wrapper) entity;
            // проверяем есть ли сущность ДПВ, если нет, значит и подписок на нее тоже нет
            if (wrapper.getElectiveDiscipline().getId() != null) {
                if (!disciplines.contains(wrapper.getElectiveDiscipline()))
                    disciplines.add(wrapper.getElectiveDiscipline());
            }
        }

        IElectivePrintDAO electivePrintDAO = (IElectivePrintDAO) ApplicationRuntime.getBean(IElectivePrintDAO.class.getName());
        RtfDocument document = electivePrintDAO.generateDocument(disciplines);

        byte[] bytes = RtfUtil.toByteArray(document);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("ElectiveSubscribes.rtf").document(bytes), false);
    }

    public void onClickOptionalDisciplinesPlaceEdit(IBusinessComponent component) {

        component.saveSettings();

        Model model = component.getModel();

        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать дисциплины, чтобы задать значения для поля «Количество мест» ! ");

        List<ElectiveDiscipline> disciplines = new ArrayList<>();
        for (IEntity entity : entityList) {
            Wrapper wrapper = (Wrapper) entity;

            if (!disciplines.contains(wrapper.getElectiveDiscipline()))
                disciplines.add(wrapper.getElectiveDiscipline());
        }


        CAFLegacySupportService.asRegion(ru.tandemservice.unitutor.component.awp.OptionalDisciplineForm.PlacesEdit.Controller.class.getPackage().getName(),
                             UIDefines.DEFAULT_REGION_NAME, (BusinessComponent) component).parameters(new ParametersMap().add("disciplines", disciplines)
                                                                                                                         .add("isForNull", model.isForNullValue()))
                .top().activate();
        clearCheckboxes(component);
    }


    public void onClickSubscribeToDateEdit(IBusinessComponent component) {

        component.saveSettings();
        Model model = component.getModel();

        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать дисциплины, чтобы задать значения для поля «Дата записи» ! ");

        Set<Long> disciplinesId = new HashSet<>();
        for (IEntity entity : entityList) {
            Wrapper wrapper = (Wrapper) entity;
            disciplinesId.add(wrapper.getElectiveDiscipline().getId());
        }


        CAFLegacySupportService.asRegion(ru.tandemservice.unitutor.component.awp.OptionalDisciplineForm.SubscribeDateEdit.Controller.class.getPackage().getName(),
                                         UIDefines.DEFAULT_REGION_NAME, (BusinessComponent) component)
                .parameters(new ParametersMap().add("disciplines", disciplinesId).add("isForNull", model.isForNullValue()))
                .top().activate();
        clearCheckboxes(component);
    }


    public void onClickEdit(IBusinessComponent component) {
        Model model = component.getModel();
        Long id = model.getDataSource().getLastVisitedEntityId();

        Wrapper wrapper = model.getDataSource().getRecordById(id);
        ParametersMap paramMap = new ParametersMap()
                .add("registryElementPartId", wrapper.getElectiveDiscipline().getRegistryElementPart().getId())
                .add("electiveDisciplineId", wrapper.getElectiveDiscipline().getId())
                .add("educationYearId", wrapper.getElectiveDiscipline().getEducationYear().getId())
                .add("yearDistributionPartId", wrapper.getElectiveDiscipline().getYearPart().getId());

        component.createDefaultChildRegion(new ComponentActivator(ru.tandemservice.unitutor.component.awp.OptionalDisciplinesEdit.Controller.class.getPackage().getName(), paramMap));
    }


    private void clearCheckboxes(IBusinessComponent component) {
        Model model = component.getModel();
        ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects().clear();
    }

    public void onClickMoveSubscribe(IBusinessComponent component)
    {
        component.saveSettings();
        Model model = component.getModel();

        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать дисциплину");

        List<ElectiveDiscipline> disciplines = new ArrayList<ElectiveDiscipline>();
        for (IEntity entity : entityList) {
            Wrapper wrapper = (Wrapper) entity;
            if (!disciplines.contains(wrapper.getElectiveDiscipline()))
                disciplines.add(wrapper.getElectiveDiscipline());
        }

        if (disciplines.size() != 1)
            throw new ApplicationException("Необходимо выбрать одну дисциплину");

        ElectiveDiscipline ed = disciplines.get(0);

        ParametersMap map = new ParametersMap();

        map.add("electiveDisciplineId", ed.getId());
        map.add("orgUnitId", model.getOrgUnitId());
        map.add("employeeId", model.getEmployeeId());
        String pkgName = ru.tandemservice.unitutor.component.awp.move.MoveSubcscibe.Model.class.getPackage().getName();

        component.createDefaultChildRegion(new ComponentActivator(pkgName, map));

    }
}
