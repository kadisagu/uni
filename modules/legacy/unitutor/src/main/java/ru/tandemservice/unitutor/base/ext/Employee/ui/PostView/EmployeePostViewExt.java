package ru.tandemservice.unitutor.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;
import ru.tandemservice.unitutor.base.bo.Tutor.ui.Tab.TutorTab;

@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager {

    @Autowired
    private EmployeePostView employeePostView;

    @Bean
    public TabPanelExtension employeePostPubTabPanelExtPoint() {
        return tabPanelExtensionBuilder(
                this.employeePostView.employeePostPubTabPanelExtPoint())
                .addTab(componentTab("tutorTab", TutorTab.class).visible("ui:tutor").parameters("ui:employeePostParam"))
                .create();
    }
}