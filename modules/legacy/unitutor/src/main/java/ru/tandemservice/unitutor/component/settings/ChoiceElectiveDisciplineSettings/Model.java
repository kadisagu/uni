package ru.tandemservice.unitutor.component.settings.ChoiceElectiveDisciplineSettings;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.ChoiceElectiveDisciplineSettings;

public class Model {

    private ChoiceElectiveDisciplineSettings settings = new ChoiceElectiveDisciplineSettings();
    private ISelectModel eduYearModel;
    private EducationYear educationYear;
    private ISelectModel yearPartModel;
    private YearDistributionPart yearPart;

    public ChoiceElectiveDisciplineSettings getSettings() {
        return settings;
    }

    public void setSettings(ChoiceElectiveDisciplineSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getEduYearModel() {
        return eduYearModel;
    }

    public void setEduYearModel(ISelectModel eduYearModel) {
        this.eduYearModel = eduYearModel;
    }

    public EducationYear getEducationYear() {
        return educationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.educationYear = educationYear;
    }

    public ISelectModel getYearPartModel() {
        return yearPartModel;
    }

    public void setYearPartModel(ISelectModel yearPartModel) {
        this.yearPartModel = yearPartModel;
    }

    public YearDistributionPart getYearPart() {
        return yearPart;
    }

    public void setYearPart(YearDistributionPart yearPart) {
        this.yearPart = yearPart;
    }
}
