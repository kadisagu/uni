package ru.tandemservice.unitutor.component.settings.ChoiceElectiveDisciplineSettings;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    void onChangeEduYear(Model model);
}
