package ru.tandemservice.unitutor.ws.types;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "educationYearWrap", namespace = "http://ws.unitutor.tandemservice.ru/")
public class WsEducationYearWrap implements Serializable {

    private static final long serialVersionUID = 8096690561758780008L;

    private Long educationYearId;
    private String title;

    public WsEducationYearWrap() {
    }

    public WsEducationYearWrap(EducationYear educationYear) {
        this.educationYearId = educationYear.getId();
        this.title = educationYear.getTitle();
    }

    public Long getEducationYearId() {
        return educationYearId;
    }

    public void setEducationYearId(Long educationYearId) {
        this.educationYearId = educationYearId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
