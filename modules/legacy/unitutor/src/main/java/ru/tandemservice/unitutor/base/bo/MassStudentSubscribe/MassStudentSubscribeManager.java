/* $Id$ */
package ru.tandemservice.unitutor.base.bo.MassStudentSubscribe;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Ekaterina Zvereva
 * @since 12.11.2015
 */
@Configuration
public class MassStudentSubscribeManager extends BusinessObjectManager
{
    public static MassStudentSubscribeManager instance()
    {
        return instance(MassStudentSubscribeManager.class);
    }

}