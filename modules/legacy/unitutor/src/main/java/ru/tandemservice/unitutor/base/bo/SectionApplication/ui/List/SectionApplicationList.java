/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication.ui.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniplaces.entity.catalog.codes.UniplacesPlacePurposeCodes;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.SportSectRegistriesManager;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.Pub.SportSectRegistriesPub;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.catalog.ApplicationStatus4SportsSection;
import ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel;
import ru.tandemservice.unitutor.entity.catalog.SportSectionType;
import ru.tandemservice.unitutor.entity.catalog.codes.ApplicationStatus4SportsSectionCodes;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.substring;

/**
 * @author Andrey Andreev
 * @since 24.08.2016
 */
@Configuration
public class SectionApplicationList extends BusinessComponentManager
{
    public static final String SEARCH_LIST_DS = "applicationSearchListDS";
    public static final String SEARCH_LIST_CHECKBOX = "checkbox";
    public static final String SEARCH_LIST_FIO = "fio";
    public static final String SEARCH_LIST_SECTION_TITLE = "sectionTitle";
    public static final String SEARCH_LIST_SECTION_TYPE = "sectionType";
    public static final String SEARCH_LIST_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String SEARCH_LIST_FITNESS_LEVEL = "physicalFitnessLevel";
    public static final String SEARCH_LIST_COURSE = "course";
    public static final String SEARCH_LIST_HOURS = "hours";
    public static final String SEARCH_LIST_TUTOR_ORG_UNIT = "tutorOrgUnit";
    public static final String SEARCH_LIST_TRAINER = "trainerColumn";
    public static final String SEARCH_LIST_PLACE = "place";
    public static final String SEARCH_LIST_STATUS = "status";
    public static final String SEARCH_LIST_EDUCATION_LEVEL = "educationLevel";
    public static final String SEARCH_LIST_PROGRAM_FORM = "programForm";
    public static final String SEARCH_LIST_GROUP = "group";
    public static final String SEARCH_LIST_REGISTRATION_DATE = "registrationDate";
    public static final String SEARCH_LIST_FIRST_CLASS = "firstClass";
    public static final String SEARCH_LIST_SECOND_CLASS = "secondClass";
    public static final String SEARCH_LIST_EDITABLE = "editable";

    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String EDUCATION_YEAR_PARAM = "educationYear";

    public static final String SECTION_TYPE_DS = "sectionTypeDS";
    public static final String SECTION_TYPE_PARAM = "sectionType";

    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String FORMATIVE_ORG_UNIT_LIST_PARAM = "formativeOrgUnitList";

    public static final String COURSE_DS = "courseDS";
    public static final String COURSE_LIST_PARAM = "courseList";

    public static final String PHYSICAL_FITNESS_LEVEL_DS = "physicalFitnessLevelDS";
    public static final String PHYSICAL_FITNESS_LEVEL_LIST_PARAM = "physicalFitnessLevelList";

    public static final String TUTOR_ORG_UNIT_DS = "tutorOrgUnitDS";
    public static final String TUTOR_ORG_UNIT_LIST_PARAM = "tutorOrgUnitList";

    public static final String STATUS_DS = "statusDS";
    public static final String STATUS_LIST_PARAM = "statusList";

    public static final String PLACE_DS = "placeDS";
    public static final String PLACE_LIST_PARAM = "placeList";

    public static final String SECTION_NAME_PARAM = "sectionName";

    public static final String GROUP_DS = "groupDS";
    public static final String GROUP_LIST_PARAM = "groupList";

    public static final String STUDENT_DS = "studentDS";
    public static final String STUDENT_LIST_PARAM = "studentList";

    public static final String EDUCATION_LEVEL_DS = "educationLevelDS";
    public static final String EDUCATION_LEVEL_LIST_PARAM = "educationLevelList";

    public static final String PROGRAM_FORM_DS = "programFormDS";
    public static final String PROGRAM_FORM_LIST_PARAM = "programFormList";

    public static final String REGISTRATION_DATE_FILTER_PARAM = "registrationDateFilter";
    public static final String REGISTRATION_DATE_FROM_PARAM = "registrationDateFrom";
    public static final String REGISTRATION_DATE_TO_PARAM = "registrationDateTo";

    public static final String CLASS_DAYS_FILTER_PARAM = "classDaysFilter";
    public static final String DAY_OF_WEEK_DS = "dayOfWeekDS";
    public static final String FIRST_CLASS_PARAM = "firstClassDay";
    public static final String SECOND_CLASS_PARAM = "secondClassDay";

    public static final String TRAINER_DS = "trainerDS";
    public static final String TRAINER_LIST_PARAM = "trainerList";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SEARCH_LIST_DS, applicationSearchListDSColumnExtPoint(), applicationSearchListDSHandler()))

                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(selectDS(STATUS_DS, statusDSHandler()))

                .addDataSource(selectDS(SECTION_TYPE_DS, sectionTypeDSHandler()))
                .addDataSource(selectDS(PHYSICAL_FITNESS_LEVEL_DS, physicalFitnessLevelDSHandler()))
                .addDataSource(selectDS(TUTOR_ORG_UNIT_DS, tutorOrgUnitDSHandler()))
                .addDataSource(selectDS(PLACE_DS, SportSectRegistriesManager.instance().sportPlaceDSHandler())
                                       .addColumn("title", "", source -> ((UniplacesPlace) source).getTitleWithLocation()))
                .addDataSource(selectDS(TRAINER_DS, SportSectRegistriesManager.instance().trainerDSHandler()))
                .addDataSource(selectDS(DAY_OF_WEEK_DS, SportSectRegistriesManager.instance().dayOfWeekDSHandler()))

                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(EDUCATION_LEVEL_DS, educationLevelDS()).addColumn(EducationLevelsHighSchool.fullTitle().s()))
                .addDataSource(selectDS(PROGRAM_FORM_DS, developFormDS()))
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
                .addDataSource(selectDS(STUDENT_DS, studentDSHandler()).addColumn(Student.fullFio().s()))

                .create();
    }


    @Bean
    public ColumnListExtPoint applicationSearchListDSColumnExtPoint()
    {
        IPublisherLinkResolver studentResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY,
                                               ((DataWrapper) entity).<SectionApplication>getWrapped().getStudent().getId());
            }

            @Override
            public String getComponentName(final IEntity entity)
            {
                return IUniComponents.STUDENT_PUB;
            }
        };

        IPublisherLinkResolver sectionResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY,
                                               ((DataWrapper) entity).<SectionApplication>getWrapped().getSportSection().getId());
            }

            @Override
            public String getComponentName(final IEntity entity)
            {
                return SportSectRegistriesPub.class.getSimpleName();
            }
        };

        return columnListExtPointBuilder(SEARCH_LIST_DS)
                .addColumn(checkboxColumn(SEARCH_LIST_CHECKBOX))
                .addColumn(publisherColumn(SEARCH_LIST_FIO, SectionApplication.student().fio().s())
                                   .publisherLinkResolver(studentResolver).order())
                .addColumn(publisherColumn(SEARCH_LIST_SECTION_TITLE, SectionApplication.sportSection().title().s())
                                   .publisherLinkResolver(sectionResolver).order())
                .addColumn(textColumn(SEARCH_LIST_SECTION_TYPE, SectionApplication.sportSection().type().title().s()).order())
                .addColumn(textColumn(SEARCH_LIST_FORMATIVE_ORG_UNIT, SectionApplication.student().educationOrgUnit().formativeOrgUnit().shortTitle().s()).order())
                .addColumn(textColumn(SEARCH_LIST_FITNESS_LEVEL, SectionApplication.sportSection().physicalFitnessLevel().title().s()).order())
                .addColumn(textColumn(SEARCH_LIST_COURSE, SEARCH_LIST_COURSE).order())
                .addColumn(textColumn(SEARCH_LIST_HOURS, SectionApplication.sportSection().hours().s()).order())
                .addColumn(textColumn(SEARCH_LIST_TUTOR_ORG_UNIT, SectionApplication.sportSection().tutorOrgUnit().shortTitle().s()).order())
                .addColumn(textColumn(SEARCH_LIST_TRAINER, SEARCH_LIST_TRAINER).order())
                .addColumn(textColumn(SEARCH_LIST_PLACE, SectionApplication.sportSection().place().s()).formatter(source -> ((UniplacesPlace) source).getTitleWithLocation()).order())
                .addColumn(textColumn(SEARCH_LIST_STATUS, SectionApplication.status().s()).order())
                .addColumn(textColumn(SEARCH_LIST_EDUCATION_LEVEL, SectionApplication.student().educationOrgUnit().educationLevelHighSchool().s())
                                   .formatter(source -> ((EducationLevelsHighSchool) source).getPrintTitle()).order())
                .addColumn(textColumn(SEARCH_LIST_PROGRAM_FORM, SectionApplication.student().educationOrgUnit().developForm().programForm().title().s()).order())
                .addColumn(textColumn(SEARCH_LIST_GROUP, SectionApplication.student().group().title().s()).order())
                .addColumn(textColumn(SEARCH_LIST_REGISTRATION_DATE, SectionApplication.registrationDate().s())
                                   .formatter(source -> DateFormatter.DATE_FORMATTER_WITH_TIME.format((Date) source)).order())
                .addColumn(textColumn(SEARCH_LIST_FIRST_CLASS, SEARCH_LIST_FIRST_CLASS).order())
                .addColumn(textColumn(SEARCH_LIST_SECOND_CLASS, SEARCH_LIST_SECOND_CLASS).order())


                .addColumn(actionColumn("edit", new Icon("edit"), "onClickEdit").permissionKey("editApplication_List")
                                   .disableHandler(entity -> !(((DataWrapper) entity).<Boolean>get(SEARCH_LIST_EDITABLE))))
                .addColumn(actionColumn("delete", new Icon("delete"), "onClickDelete").permissionKey("deleteApplication_List")
                                   .alert(FormattedMessage.with()
                                                  .template("applicationSearchList.deleteApplication.alert")
                                                  .parameter(SectionApplication.student().fio().s())
                                                  .parameter(SectionApplication.sportSection().title().s())
                                                  .create()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> applicationSearchListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String alias = "e";
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(SectionApplication.class, alias)
                        .where(eq(property(alias, SectionApplication.sportSection().educationYear()), value((EducationYear) context.get(EDUCATION_YEAR_PARAM))))
                        .column(property(alias))

                        .fetchPath(DQLJoinType.left, SectionApplication.status().fromAlias(alias), "sts")
                        .fetchPath(DQLJoinType.left, SectionApplication.sportSection().fromAlias(alias), "ss", true)
                        .fetchPath(DQLJoinType.left, SectionApplication.student().course().fromAlias(alias), "cr")
                        .fetchPath(DQLJoinType.left, SectionApplication.student().group().fromAlias(alias), "gr");

                FilterUtils.applyBetweenFilter(builder, alias, SectionApplication.registrationDate().s(),
                                               context.<Date>get(REGISTRATION_DATE_FROM_PARAM), context.<Date>get(REGISTRATION_DATE_TO_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.status(), context.get(STATUS_LIST_PARAM));

                FilterUtils.applySimpleLikeFilter(builder, alias, SectionApplication.sportSection().title(), context.get(SECTION_NAME_PARAM));

                FilterUtils.applySelectFilter(builder, alias, SectionApplication.sportSection().type(), context.get(SECTION_TYPE_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.sportSection().physicalFitnessLevel(), context.get(PHYSICAL_FITNESS_LEVEL_LIST_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.sportSection().tutorOrgUnit(), context.get(TUTOR_ORG_UNIT_LIST_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.sportSection().place(), context.get(PLACE_LIST_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.sportSection().trainer(), context.get(TRAINER_LIST_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.sportSection().firstClassDay(), context.get(FIRST_CLASS_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.sportSection().secondClassDay(), context.get(SECOND_CLASS_PARAM));

                FilterUtils.applySelectFilter(builder, alias, SectionApplication.student().educationOrgUnit().formativeOrgUnit(), context.get(FORMATIVE_ORG_UNIT_LIST_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.student().educationOrgUnit().educationLevelHighSchool(), context.get(EDUCATION_LEVEL_LIST_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.student().educationOrgUnit().developForm().programForm(), context.get(PROGRAM_FORM_LIST_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.student().course(), context.get(COURSE_LIST_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.student().group(), context.get(GROUP_LIST_PARAM));
                FilterUtils.applySelectFilter(builder, alias, SectionApplication.student(), context.get(STUDENT_LIST_PARAM));

                builder.joinPath(DQLJoinType.left, SectionApplication.student().group().fromAlias(alias), "g");

                EntityOrder entityOrder = input.getEntityOrder();
                DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(SectionApplication.class, alias);
                orderRegistry
                        .setOrders(SectionApplication.sportSection().title(), new OrderDescription(SectionApplication.sportSection().title()))
                        .setOrders(SectionApplication.sportSection().type().title(), new OrderDescription(SectionApplication.sportSection().type().title()))
                        .setOrders(SectionApplication.student().educationOrgUnit().formativeOrgUnit().shortTitle(),
                                   new OrderDescription(SectionApplication.student().educationOrgUnit().formativeOrgUnit().shortTitle()))
                        .setOrders(SectionApplication.sportSection().physicalFitnessLevel().title(), new OrderDescription(SectionApplication.sportSection().physicalFitnessLevel().title()))
                        .setOrders(SectionApplication.student().course().title(), new OrderDescription(SectionApplication.student().course().title()))
                        .setOrders(SectionApplication.sportSection().hours(), new OrderDescription(SectionApplication.sportSection().hours()))
                        .setOrders(SectionApplication.sportSection().place().title(), new OrderDescription(SectionApplication.sportSection().place().title()))
                        .setOrders(SectionApplication.student().group().title(), new OrderDescription("g", Group.title()))
                        .setOrders(SectionApplication.registrationDate(), new OrderDescription(SectionApplication.registrationDate()))

                        .setOrders(SEARCH_LIST_FIRST_CLASS,
                                   new OrderDescription(SectionApplication.sportSection().firstClassDay()),
                                   new OrderDescription(SectionApplication.sportSection().firstClassTime()))

                        .setOrders(SEARCH_LIST_SECOND_CLASS,
                                   new OrderDescription(SectionApplication.sportSection().secondClassDay()),
                                   new OrderDescription(SectionApplication.sportSection().secondClassTime()));

                builder.joinPath(DQLJoinType.left, SectionApplication.sportSection().trainer().fromAlias(alias), "tr")
                        .joinPath(DQLJoinType.left, PpsEntry.person().fromAlias("tr"), "tr1")
                        .joinPath(DQLJoinType.left, Person.identityCard().fromAlias("tr1"), "tr2");
                orderRegistry.setOrders(SEARCH_LIST_TRAINER, new OrderDescription("tr2", IdentityCard.lastName()), new OrderDescription("tr2", IdentityCard.firstName()), new OrderDescription("tr2", IdentityCard.middleName()));

                orderRegistry.applyOrder(builder, entityOrder);
                if (entityOrder.getKey().equals(SEARCH_LIST_FIO))
                {
                    builder.joinPath(DQLJoinType.left, SectionApplication.student().person().identityCard().fromAlias(alias), "ic");
                    builder.order(property("ic", IdentityCard.lastName()), entityOrder.getDirection());
                    builder.order(substring(property("ic", IdentityCard.firstName()), 1, 1), entityOrder.getDirection());
                    builder.order(substring(property("ic", IdentityCard.middleName()), 1, 1), entityOrder.getDirection());
                }


                return wrapSportSectionList(DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build());
            }
        };
    }

    protected DSOutput wrapSportSectionList(DSOutput output)
    {
        int currentYear = EducationYear.getCurrentRequired().getIntValue();

        CollectionUtils.transform(output.getRecordList(), in -> {
            if (in instanceof DataWrapper)
                return in;

            SectionApplication application = (SectionApplication) in;
            SportSection section = application.getSportSection();

            DataWrapper wrapper = new DataWrapper(application);
            wrapper.setProperty(SEARCH_LIST_FIRST_CLASS, section.getFirstClassInWeek());
            wrapper.setProperty(SEARCH_LIST_SECOND_CLASS, section.getSecondClassInWeek());
            wrapper.setProperty(SEARCH_LIST_TRAINER, section.getTrainer() == null ? "" : section.getTrainer().getFio());

            wrapper.setProperty(SEARCH_LIST_EDITABLE, application.isFromUni()
                    && !ApplicationStatus4SportsSectionCodes.ACCEPTED.equals(application.getStatus().getCode()));

            int course = application.getStudent().getCourse().getIntValue() + (application.getSportSection().getEducationYear().getIntValue() - currentYear);
            wrapper.setProperty(SEARCH_LIST_COURSE, course);

            return wrapper;
        });

        return output;
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> statusDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), ApplicationStatus4SportsSection.class);
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sectionTypeDSHandler()
    {
        return SportSectionType.defaultSelectDSHandler(this.getName());
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> physicalFitnessLevelDSHandler()
    {
        return PhysicalFitnessLevel.defaultSelectDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler tutorOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
                .customize((alias, dql, context, filter) -> dql.where(exists(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit().s(), property(alias))))
                .order(OrgUnit.title())
                .where(OrgUnit.archival(), false)
                .filter(OrgUnit.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> placeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesPlace.class)
                .where(UniplacesPlace.purpose().code(), UniplacesPlacePurposeCodes.OBEKT_FIZKULTURY_I_SPORTA)
                .order(UniplacesPlace.title())
                .filter(UniplacesPlace.title())
                .pageable(true);
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developFormDS()
    {
        return EduProgramForm.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelDS()
    {
        return new EntityComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
                .customize((alias, dql, context, filter) -> {
                    FilterUtils.applyLikeFilter(dql, filter, EducationLevelsHighSchool.printTitle().fromAlias(alias));
                    return dql;
                })
                .order(EducationLevelsHighSchool.code())
                .pageable(true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return Course.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
                .customize((alias, dql, context, filter) -> {
                    FilterUtils.applyLikeFilter(dql, filter, Group.title().fromAlias(alias));
                    FilterUtils.applySelectFilter(dql, alias, Group.course(), context.get(COURSE_LIST_PARAM));
                    FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().developForm().programForm(), context.get(PROGRAM_FORM_LIST_PARAM));
                    FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().formativeOrgUnit(), context.get(FORMATIVE_ORG_UNIT_LIST_PARAM));
                    FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().educationLevelHighSchool(), context.get(EDUCATION_LEVEL_LIST_PARAM));
                    return dql.where(eq(property(alias, Group.archival()), value(false)));
                });
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Student.class)
                .customize((alias, dql, context, filter) -> {
                    FilterUtils.applySelectFilter(dql, alias, Student.course(), context.get(COURSE_LIST_PARAM));
                    FilterUtils.applySelectFilter(dql, alias, Student.educationOrgUnit().developForm().programForm(), context.get(PROGRAM_FORM_LIST_PARAM));
                    FilterUtils.applySelectFilter(dql, alias, Student.educationOrgUnit().formativeOrgUnit(), context.get(FORMATIVE_ORG_UNIT_LIST_PARAM));
                    FilterUtils.applySelectFilter(dql, alias, Student.educationOrgUnit().educationLevelHighSchool(), context.get(EDUCATION_LEVEL_LIST_PARAM));
                    FilterUtils.applySelectFilter(dql, alias, Student.group(), context.get(GROUP_LIST_PARAM));
                    return dql;
                })
                .order(Student.person().identityCard().lastName())
                .filter(Student.person().identityCard().lastName())
                .filter(Student.person().identityCard().firstName())
                .filter(Student.person().identityCard().middleName())
                .pageable(true);
    }


}