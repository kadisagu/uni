package ru.tandemservice.unitutor.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Типы дисциплин"
 * Имя сущности : electiveDisciplineType
 * Файл data.xml : unitutor.data.xml
 */
public interface ElectiveDisciplineTypeCodes
{
    /** Константа кода (code) элемента : Дисциплины по выбору (title) */
    String DPV = "01";
    /** Константа кода (code) элемента : Общеуниверситетские дисциплины по выбору (title) */
    String UDV = "02";
    /** Константа кода (code) элемента : Общеуниверситетские факультативы (title) */
    String UF = "03";

    Set<String> CODES = ImmutableSet.of(DPV, UDV, UF);
}
