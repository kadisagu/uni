package ru.tandemservice.unitutor.entity;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.unitutor.entity.gen.*;

/** @see ru.tandemservice.unitutor.entity.gen.Orientation2EduPlanVersionGen */
public class Orientation2EduPlanVersion extends Orientation2EduPlanVersionGen
{
    public static final String P_HAS_ANNOTATION = "hasAnnotation";

    public boolean isHasAnnotation() {
        return !StringUtils.isEmpty(this.getAnnotation());
    }
}