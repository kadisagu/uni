package ru.tandemservice.unitutor.utils;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class TutorModelUtils {

    /**
     * Направления подготовки на подразделениях
     *
     * @param orgUnitIds
     *
     * @return
     */
    public static DQLFullCheckSelectModel getEducationLevelHighSchoolListBuilderModel(final List<Long> orgUnitIds) {

        return new DQLFullCheckSelectModel("fullTitle") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EducationLevelsHighSchool.class, alias)
                        .where(in(
                                property(EducationLevelsHighSchool.orgUnit().id().fromAlias(alias)),
                                orgUnitIds))
                        .order(property(EducationLevelsHighSchool.code().fromAlias(alias)))
                        .order(property(EducationLevelsHighSchool.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle(), filter);

                return builder;
            }
        };

    }

    /**
     * Академические группы на подразделениях
     *
     * @param orgUnitIds
     *
     * @return
     */
    public static DQLFullCheckSelectModel getAcademicalGroupListBuilderModel(final List<Long> orgUnitIds) {

        return new DQLFullCheckSelectModel("title") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Group.class, alias)
                        .where(in(property(Group.parent().id().fromAlias(alias)), orgUnitIds))
                        .order(property(Group.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, Group.title(), filter);

                return builder;
            }
        };

    }

    /**
     * Студенты на подразделениях
     *
     * @param orgUnitIds
     *
     * @return
     */
    public static DQLFullCheckSelectModel getStudentListBuilderModel(final List<Long> orgUnitIds) {

        return new DQLFullCheckSelectModel(Student.titleWithFio())
        {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Student.class, alias)
                        .where(in(property(Student.educationOrgUnit().formativeOrgUnit().id().fromAlias(alias)), orgUnitIds))
                        .where(eq(property(Student.status().active().fromAlias(alias)), value(Boolean.TRUE)))
                        .where(eq(property(Student.archival().fromAlias(alias)), value(Boolean.FALSE)))
                        .order(property(Student.person().identityCard().fullFio().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().fullFio(), filter);

                return builder;
            }
        };

    }

    /**
     * Студенты академических групп
     * @return
     */
    public static DQLFullCheckSelectModel getStudent2GroupListBuilderModel(final List<Group> groupList) {

        return new DQLFullCheckSelectModel(Student.titleWithFio().s())
        {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Student.class, alias)
                        .where(in(property(Student.group().fromAlias(alias)), groupList))
                        .where(eq(property(Student.status().active().fromAlias(alias)), value(Boolean.TRUE)))
                        .where(eq(property(Student.archival().fromAlias(alias)), value(Boolean.FALSE)))
                        .order(property(Student.person().identityCard().fullFio().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().fullFio(), filter);

                return builder;
            }
        };

    }

    /**
     * Названия дисциплин
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static List<IdentifiableWrapper> getDisciplineTitleList() {

        List<IdentifiableWrapper> disciplineTitleList = new ArrayList<IdentifiableWrapper>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppRegistryElement.class, "d")
                .column(EppRegistryElement.title().fromAlias("d").s())
                .distinct();

        List<String> titles = UniDaoFacade.getCoreDao().getList(builder);

        Long iwd = 0L;

        for (String title : titles)
            disciplineTitleList.add(new IdentifiableWrapper(iwd++, title));

        return disciplineTitleList;
    }

}
