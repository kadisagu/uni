package ru.tandemservice.unitutor.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unitutor.entity.EppEduPlanVersionExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение версии учебного плана
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanVersionExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.EppEduPlanVersionExt";
    public static final String ENTITY_NAME = "eppEduPlanVersionExt";
    public static final int VERSION_HASH = -434354203;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String L_COURSE = "course";

    private EppEduPlanVersion _eduPlanVersion;     // Версия учебного плана
    private Course _course;     // Курс начала профилизации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Версия учебного плана. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Курс начала профилизации.
     */
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс начала профилизации.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEduPlanVersionExtGen)
        {
            setEduPlanVersion(((EppEduPlanVersionExt)another).getEduPlanVersion());
            setCourse(((EppEduPlanVersionExt)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanVersionExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanVersionExt.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanVersionExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanVersionExt> _dslPath = new Path<EppEduPlanVersionExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanVersionExt");
    }
            

    /**
     * @return Версия учебного плана. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitutor.entity.EppEduPlanVersionExt#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Курс начала профилизации.
     * @see ru.tandemservice.unitutor.entity.EppEduPlanVersionExt#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends EppEduPlanVersionExt> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitutor.entity.EppEduPlanVersionExt#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Курс начала профилизации.
     * @see ru.tandemservice.unitutor.entity.EppEduPlanVersionExt#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return EppEduPlanVersionExt.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanVersionExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
