/* $Id$ */
package ru.tandemservice.unitutor.base.bo.MassStudentSubscribe.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.base.bo.MassStudentSubscribe.logic.StudentSubscribeDSHandler;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 12.11.2015
 */
@Configuration
public class MassStudentSubscribeAdd extends BusinessComponentManager
{

    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String EDUCATION_YEAR_PART_DS = "yearPartDS";
    public static final String GROUP_DS = "groupListDS";
    public static final String STATE_DS = "stateDS";
    public static final String STUDENTS_SUBSCRIBE_DS = "studentSubscribeDS";

    public static final String ORG_UNIT = "orgUnit";
    public static final String GROUP_LIST = "groups";
    public static final String STATES_LIST = "states";
    public static final String EDU_YEAR = "educationYear";
    public static final String EDU_YEAR_PART = "yearDistributionPart";
    public static final String ELECTIVE_DISCIPLINE_CODE = "electiveDisciplineCode";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_YEAR_DS, EducationCatalogsManager.instance().eduYearDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR_PART_DS, EducationCatalogsManager.instance().yearPartDSHandler()))
                .addDataSource(selectDS(GROUP_DS, groupDS()).addColumn(Group.title().s()))
                .addDataSource(selectDS(STATE_DS, studentStateDS()))
                .addDataSource(searchListDS(STUDENTS_SUBSCRIBE_DS, studentsSubscribesColumns(), studentSubscribesDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler groupDS()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                OrgUnit orgUnit = context.get(ORG_UNIT);
                if (null != orgUnit)
                    dql.where(in(property(alias, Group.educationOrgUnit().formativeOrgUnit()), orgUnit));
                dql.where(eq(property(alias, Group.archival()), value(Boolean.FALSE)));
            }
        }
            .pageable(true)
                .order(Group.title())
                .filter(Group.title());
    }


    @Bean
    public IDefaultComboDataSourceHandler studentStateDS()
    {
        return StudentStatus.defaultSelectDSHandler(getName());
    }

    @Bean
    public ColumnListExtPoint studentsSubscribesColumns()
    {
        return columnListExtPointBuilder(STUDENTS_SUBSCRIBE_DS)
                .addColumn(textColumn("number", "number").clickable(false)
                                   .merger(entity -> ((Student) entity.getProperty("student")).getId().toString()))
                .addColumn(publisherColumn("name", "student." + Student.fullTitle())
                                   .publisherLinkResolver(new DefaultPublisherLinkResolver()
                                   {
                                       @Override
                                       public String getComponentName(IEntity entity) { return null; }

                                       @Override
                                       public Object getParameters(IEntity entity)
                                       {
                                           return new ParametersMap()
                                                   .add(PublisherActivator.PUBLISHER_ID_KEY, ((Student) entity.getProperty("student")).getId())
                                                   .add("selectedStudentTab", "studentTab");
                                       }
                                   })
                                   .merger(entity -> ((Student) entity.getProperty("student")).getId().toString())

                                   .width("180")
                                   .create())
                .addColumn(textColumn("count", "count").visible("ui:notDPV").merger(entity -> ((Student) entity.getProperty("student")).getId().toString()))
                .addColumn(textColumn("group", "discipline.parentName").visible("ui:DPV")
                                   .merger(iEntity -> ((Student) iEntity.getProperty("student")).getId().toString() + "." + iEntity.getProperty("discipline.parentName")))
                .addColumn(checkboxColumn("subscribed").controlInHeader(false).create())
                .addColumn(textColumn("index", "discipline.eppWorkPlanRegistryElementRow.number").visible("ui:DPV"))
                .addColumn(textColumn("disciplineName", "discipline.eppRegistryElementPart." + EppRegistryElementPart.titleWithNumber()))
                .create();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentSubscribesDSHandler()
    {
        return new StudentSubscribeDSHandler(getName());
    }
}