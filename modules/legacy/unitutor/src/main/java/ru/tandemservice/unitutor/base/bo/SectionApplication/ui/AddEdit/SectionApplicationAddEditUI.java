/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SectionApplication.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.entity.SectionApplication;
import ru.tandemservice.unitutor.entity.SportSection;

/**
 * @author Andrey Andreev
 * @since 24.08.2016
 */
@Input({@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "sectionApplicationId")})
public class SectionApplicationAddEditUI extends UIPresenter
{
    private Long _sectionApplicationId;
    private SectionApplication _sectionApplication;

    @Override
    public void onComponentRefresh()
    {
        if (_sectionApplicationId != null)
        {
            _sectionApplication = DataAccessServices.dao().getNotNull(SectionApplication.class, _sectionApplicationId);
            if (!_sectionApplication.isFromUni())
                throw new ApplicationException("Заявке не может редктироваться в UNI");

            Student student = _sectionApplication.getStudent();

            _uiSettings.set(SectionApplicationAddEdit.EDUCATION_YEAR_PARAM, _sectionApplication.getSportSection().getEducationYear());

            _uiSettings.set(SectionApplicationAddEdit.SEX_PARAM, student.getPerson().getIdentityCard().getSex());
            _uiSettings.set(SectionApplicationAddEdit.STUDENT_STATUS_PARAM, student.getStatus());

            _uiSettings.set(SectionApplicationAddEdit.FORMATIVE_ORG_UNIT_PARAM, student.getEducationOrgUnit().getFormativeOrgUnit());
            _uiSettings.set(SectionApplicationAddEdit.PROGRAM_FORM_PARAM, student.getEducationOrgUnit().getDevelopForm().getProgramForm());

            _uiSettings.set(SectionApplicationAddEdit.EDUCATION_LEVEL_PARAM, student.getEducationOrgUnit().getEducationLevelHighSchool());

            _uiSettings.set(SectionApplicationAddEdit.COURSE_PARAM, student.getCourse());
            _uiSettings.set(SectionApplicationAddEdit.GROUP_PARAM, student.getGroup());
        }
        else
        {
            _sectionApplication = new SectionApplication(true);
            _uiSettings.set(SectionApplicationAddEdit.EDUCATION_YEAR_PARAM, EducationYear.getCurrentRequired());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SectionApplicationAddEdit.APPLICATION_ID, _sectionApplicationId);

        dataSource.put(SectionApplicationAddEdit.EDUCATION_YEAR_PARAM, _uiSettings.get(SectionApplicationAddEdit.EDUCATION_YEAR_PARAM));

        dataSource.put(SectionApplicationAddEdit.COURSE_PARAM, _uiSettings.get(SectionApplicationAddEdit.COURSE_PARAM));
        dataSource.put(SectionApplicationAddEdit.GROUP_PARAM, _uiSettings.get(SectionApplicationAddEdit.GROUP_PARAM));

        dataSource.put(SectionApplicationAddEdit.SEX_PARAM, _uiSettings.get(SectionApplicationAddEdit.SEX_PARAM));
        dataSource.put(SectionApplicationAddEdit.STUDENT_STATUS_PARAM, _uiSettings.get(SectionApplicationAddEdit.STUDENT_STATUS_PARAM));

        dataSource.put(SectionApplicationAddEdit.FORMATIVE_ORG_UNIT_PARAM, _uiSettings.get(SectionApplicationAddEdit.FORMATIVE_ORG_UNIT_PARAM));
        dataSource.put(SectionApplicationAddEdit.PROGRAM_FORM_PARAM, _uiSettings.get(SectionApplicationAddEdit.PROGRAM_FORM_PARAM));

        dataSource.put(SectionApplicationAddEdit.EDUCATION_LEVEL_PARAM, _uiSettings.get(SectionApplicationAddEdit.EDUCATION_LEVEL_PARAM));

        if (dataSource.getName().equals(SectionApplicationAddEdit.STUDENT_DS))
            dataSource.put(SectionApplicationAddEdit.SPORT_SECTION_PARAM, _sectionApplication.getSportSection());

        if (dataSource.getName().equals(SectionApplicationAddEdit.SPORT_SECTION_DS))
            dataSource.put(SectionApplicationAddEdit.STUDENT_PARAM, _sectionApplication.getStudent());
    }


    public boolean isAddMode()
    {
        return null == _sectionApplicationId;
    }

    //Listeners
    public void onClickSave()
    {
        DataAccessServices.dao().saveOrUpdate(_sectionApplication);
        deactivate();
    }

    public void onClickApply()
    {
        getSettings().save();
        DataAccessServices.dao().saveOrUpdate(_sectionApplication);

        SportSection sportSection = _sectionApplication.getSportSection();

        _sectionApplicationId = null;
        _sectionApplication = new SectionApplication(true);
        _sectionApplication.setSportSection(sportSection);
    }

    //Getters and Setters
    public SectionApplication getSectionApplication()
    {
        return _sectionApplication;
    }

    public void setSectionApplication(SectionApplication sectionApplication)
    {
        _sectionApplication = sectionApplication;
    }

    public Long getSectionApplicationId()
    {
        return _sectionApplicationId;
    }

    public void setSectionApplicationId(Long sectionApplicationId)
    {
        _sectionApplicationId = sectionApplicationId;
    }
}