package ru.tandemservice.unitutor.ws;


import ru.tandemservice.unitutor.ws.types.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT)
public interface IWsDisciplines {

    /**
     * Тест веб-сервиса на доступность
     *
     * @return
     */
    @WebMethod
    public String getTestMessage();


    /**
     * Список текущего и следующего уч. годов
     *
     * @return
     */
    @WebMethod
    @WebResult(name = "educationYearWrap")
    public List<WsEducationYearWrap> getEducationYearList();


    /**
     * Список дисциплин ДПВ учебного года для выбора
     *
     * @param studentParam
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod
    @WebResult(name = "disciplineWrap")
    public List<WsDisciplineWrap> getWsDisciplinesNextEppYear(
            @WebParam(name = "studentParam") String studentParam,
            @WebParam(name = "educationYearId") String educationYearId)
            throws Exception;


    /**
     * Список дисциплин УДВ(УФ) учебного года для выбора
     *
     * @param studentParam
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod
    @WebResult(name = "electiveWrap")
    public List<WsElectiveWrap> getWsElectiveNextEppYear(
            @WebParam(name = "studentParam") String studentParam,
            @WebParam(name = "electiveType") String electiveType,
            @WebParam(name = "educationYearId") String educationYearId)
            throws Exception;

    /**
     * Дисциплины по выбору, выбранные студентом в личном кабинете
     *
     * @param studentId
     * @param disciplineIds
     * @param needSendMail
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod
    public String subscribeToElectiveDiscipline(
            @WebParam(name = "studentParam") String studentId,
            @WebParam(name = "disciplineSubscribes") String disciplineIds,
            @WebParam(name = "needSendMail") String needSendMail,
            @WebParam(name = "electiveType") String electiveType,
            @WebParam(name = "educationYearId") String educationYearId)
            throws Exception;

    /**
     * Версия учебного плана для студента (Base64 encoded)
     *
     * @param studentParam
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod
    public String getEduPlanVersion(
            @WebParam(name = "studentParam") String studentParam)
            throws Exception;

    /**
     * Заявление студента на ДПВ (Base64 encoded)
     *
     * @param studentParam
     * @param id
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod
    public String getElectiveApplication(
            @WebParam(name = "studentParam") String studentParam,
            @WebParam(name = "id") String id)
            throws Exception;

    /**
     * Аннотация для дисциплины
     *
     * @param idStr
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod
    public String getAnnotation(
            @WebParam(name = "idStr") String idStr)
            throws Exception;

    /**
     * Направленности
     *
     * @param studentParam
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod
    @WebResult(name = "orientationWrap")
    public List<WsOrientationWrap> getWsOrientations(
            @WebParam(name = "studentParam") String studentParam)
            throws Exception;

    /**
     * Направленности, выбранные студентом в личном кабинете
     *
     * @param studentParam
     * @param orientationSubscribe
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod
    public String subscribeToOrientation(
            @WebParam(name = "studentParam") String studentParam,
            @WebParam(name = "orientationSubscribe") String orientationSubscribe)
            throws Exception;

    /**
     * Список студентов по логину
     * @param studentParam
     * @return
     * @throws Exception
     */
    @WebMethod
    @WebResult(name = "studentWrap")
    List<WsStudentWrap> getStudentsList(@WebParam(name = "studentParam") String studentParam) throws Exception;



    @WebMethod
    @WebResult(name = "workPlanWrap")
    List<WsWorkPlanInfoWrap> getCountElectiveDiscipline(
            @WebParam(name = "studentParam") String studentParam,
            @WebParam(name = "educationYearId") String educationYearId,
            @WebParam(name = "electiveType") String electiveType) throws Exception;

    /**
     * @param studentParam Student Id as String
     * @return Список спортивных секций доступных студенту
     * @throws Exception
     */
    @WebMethod
    @WebResult(name = "sportSectionWrap")
    List<WsSportSectionWrap> getWsSportSections(@WebParam(name = "studentParam") String studentParam) throws Exception;

    /**
     * @param studentParam Student Id as String
     * @param createApplication SportSection Id as String секция на которую подается заявление
     * @param withdrawApplication SportSection Id as String секция заявление на которую отзывается
     * @return описание результата
     * @throws Exception
     */
    String updateApplications(@WebParam(name = "studentParam") String studentParam,
                              @WebParam(name = "createApplication") String createApplication,
                              @WebParam(name = "withdrawApplication") String withdrawApplication) throws Exception;
}
