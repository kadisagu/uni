package ru.tandemservice.unitutor.component.registry.RegistryBase;

import org.apache.cxf.common.util.StringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unitutor.component.awp.base.IFilterProperties;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline2Course;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline2EduGroup;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public abstract class RegistryBaseDAO<T extends RegistryBaseModel> extends
        UniDao<T> implements IRegistryBaseDAO<T>
{

    @Override
    public void prepare(T model) {
        prepareModel(model);
        super.prepare(model);
    }

    protected void prepareModel(T model) {

        // Значение по умолчанию - текущий учебный год из соответствующей настройки системы
        if (model.getSettings().get(IFilterProperties.EDUCATION_YEAR_FILTER) == null) {
            EducationYear currentEducationYear = get(EducationYear.class, EducationYear.current().s(), Boolean.TRUE);
            model.getSettings().set(IFilterProperties.EDUCATION_YEAR_FILTER, currentEducationYear);
        }

        prepareCommonFilters(model);
    }

    @Override
    public void prepareCommonFilters(T model) {

        // учебный год
        model.setEducationYearModel(new LazySimpleSelectModel<>(EducationYear.class, EducationYear.title().s()));

        // часть года
        model.setEducationYearPartModel(new DQLFullCheckSelectModel("title") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                final DQLSelectBuilder dqlIn = DqlDisciplineBuilder.getDqlInYearDistributionPart(model.getFilterEducationYear());
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(YearDistributionPart.class, alias)
                        .where(in(
                                property(YearDistributionPart.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(property(YearDistributionPart.title().fromAlias(alias)));
                FilterUtils.applySimpleLikeFilter(builder, alias, YearDistributionPart.title(), filter);
                return builder;
            }
        });

        // курс
        model.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));

        // направления подготовки
//		model.setEducationLevelHighSchoolModel(new EducationLevelsHighSchoolSelectModel());
        //УГН
        model.setEduGroupModel(EduProgramSubjectGroup.getEduGroupModel());

    }

    public List<RegistryWrapper> getWrapperList(List<Object[]> data) {

        List<RegistryWrapper> wrapperList = new ArrayList<>();

        for (Object[] objs : data) {
            ElectiveDiscipline electiveDiscipline = (ElectiveDiscipline) objs[0];
            Long subscribe = 0L;
            if (objs[1] != null)
                subscribe = (Long) objs[1];

            String ccaction = "";
            if (objs[2] != null)
                ccaction = (String) objs[2];


            String annotation = "";
            if (objs[3] != null)
                annotation = (String) objs[3];

            RegistryWrapper wrap = new RegistryWrapper();
            wrap.setElectiveDiscipline(electiveDiscipline);

            wrap.setFormOfControl(ccaction);
            wrap.setPlacesSubscribed(subscribe);

            Long freePlacesToSubscribe;
            Long startPlaces = 0L;
            if (electiveDiscipline.getPlacesCount() != null)
                startPlaces = electiveDiscipline.getPlacesCount();

            freePlacesToSubscribe = startPlaces - subscribe;

            wrap.setFreePlacesToSubscribe(freePlacesToSubscribe);

            if (!StringUtils.isEmpty(annotation))
                wrap.setContainAnnotation(true);

            wrapperList.add(wrap);
        }

        return wrapperList;
    }

    public DQLSelectBuilder getElectiveDisciplineDQL(T model, String alias, String electiveDisciplineTypeCode) {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ElectiveDiscipline.class, alias)
                .where(DQLExpressions.eqValue(property(ElectiveDiscipline.type().code().fromAlias(alias)), electiveDisciplineTypeCode));

        patchElectiveDisciplineDQL(model, dql, alias);

        // сколько уже записалось
        dql.joinDataSource(alias, DQLJoinType.left, DqlDisciplineBuilder.getElectiveDisciplineSUMMSubDQL(model.getFilterEducationYear(), electiveDisciplineTypeCode).buildQuery(),
                           "dqlCount", eq(property(ElectiveDiscipline.id().fromAlias(alias)), property("dqlCount.id")));

        // джойним вид итогового контроля, может произойти умножение записей, но нас это не сильно волнует
        dql.joinEntity(alias, DQLJoinType.left, EppRegistryElementPartFControlAction.class, "EppRegistryElementPartFControlAction",
                eq(property(ElectiveDiscipline.registryElementPart().id().fromAlias(alias)),
                property(EppRegistryElementPartFControlAction.part().id().fromAlias("EppRegistryElementPartFControlAction"))))

                .joinPath(DQLJoinType.left, EppRegistryElementPartFControlAction.controlAction().fromAlias("EppRegistryElementPartFControlAction"), "controlAction");

        dql.joinPath(DQLJoinType.left, ElectiveDiscipline.registryElementPart().fromAlias(alias), "registryElementPart");
        // джойним расширение дисциплины
        dql.joinEntity("registryElementPart", DQLJoinType.left, EppRegistryElementExt.class, "EppRegistryElementExt",
                eq(property(EppRegistryElementPart.registryElement().id().fromAlias("registryElementPart")),
                   property(EppRegistryElementExt.eppRegistryElement().id().fromAlias("EppRegistryElementExt"))));

        // формируем вывод
        dql.column(alias)
                // сколько записалось итого
                .column("dqlCount.subscribe")
                        // тип контрольного мероприятия
                .column(EppFControlActionType.title().fromAlias("controlAction").s())
                .column(EppRegistryElementExt.annotation().fromAlias("EppRegistryElementExt").s());

        return dql;
    }


    /**
     * Фильтрация согласно выставленным фильтрам
     */
    public DQLSelectBuilder patchElectiveDisciplineDQL(T model, DQLSelectBuilder dql, String alias) {

        // Учебный год
        dql.where(eq(property(ElectiveDiscipline.educationYear().fromAlias(alias)), DQLExpressions.value(model.getFilterEducationYear())));

        // Часть учебного года
        dql.where(eq(property(ElectiveDiscipline.yearPart().fromAlias(alias)), DQLExpressions.value(model.getFilterYearDistributionPart())));

        // Курс
        if (model.getFilterCourse() != null && !model.getFilterCourse().isEmpty()) {

            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(ElectiveDiscipline2Course.class, "rel")
                    .where(DQLExpressions.in(property(ElectiveDiscipline2Course.course().fromAlias("rel")), model.getFilterCourse()))
                    .column(ElectiveDiscipline2Course.electiveDiscipline().id().fromAlias("rel").s());

            dql.where(DQLExpressions.in(property(ElectiveDiscipline.id().fromAlias(alias)), subBuilder.buildQuery()));
//			FilterUtils.applySelectFilter(dql, ElectiveDiscipline.course().fromAlias(alias), model.getFilterCourse());
        }

        if (model.getFilterEduGroupList() != null && !model.getFilterEduGroupList().isEmpty()) {
            List<Long> ids = UniBaseUtils.getIdList(model.getFilterEduGroupList());

            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(ElectiveDiscipline2EduGroup.class, "rel")
                    .where(DQLExpressions.in(property(ElectiveDiscipline2EduGroup.groupId().fromAlias("rel")), ids))
                    .column(ElectiveDiscipline2EduGroup.electiveDiscipline().id().fromAlias("rel").s());

            dql.where(DQLExpressions.in(property(ElectiveDiscipline.id().fromAlias(alias)), subBuilder.buildQuery()));
        }
        /*
		// Направление подготовки (специальность)
		if (model.getFilterEducationLevelsHighSchoolList() != null){
			FilterUtils.applySelectFilter(dql, ElectiveDiscipline.educationLevel().fromAlias(alias), model.getFilterEducationLevelsHighSchoolList());
		}
		*/
        // По названиям дисциплин
//		List<EppRegistryElement> filterDisciplineList = getFilterEppRegistryElement(model.getFilterDiscipline());
        if (model.getFilterDiscipline() != null && !model.getFilterDiscipline().isEmpty()) {
            FilterUtils.applySelectFilter(dql, ElectiveDiscipline.registryElementPart().registryElement().fromAlias(alias), model.getFilterDiscipline());
        }

        return dql;
    }

    /**
     * Добавление возможности сортировок
     */
    public void patchOrder(T model, DQLSelectBuilder dql, String alias) {

        EntityOrder order = model.getDataSource().getEntityOrder();
        String columnName = order.getColumnName();

        columnName = columnName.replace("null.electiveDiscipline.", "");
        columnName = columnName.replace("electiveDiscipline.", "");
        columnName = columnName.replace("placesSubscribed", "subscribe");

        EntityOrder orderSelect = new EntityOrder(columnName, order.getDirection());

        if (columnName.contains("subscribe")) {
            dql.order("dqlCount.subscribe", orderSelect.getDirection());
        }
        else {
            // применяем сортировки
            DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(ElectiveDiscipline.class, alias);
            orderRegistry.applyOrder(dql, orderSelect);
        }
    }

    /*
    @SuppressWarnings("rawtypes")
    protected List<IdentifiableWrapper> getDisciplineListSelectModel(String electiveDisciplineTypeCode){
        List<IdentifiableWrapper> disciplineTitleList = new ArrayList<IdentifiableWrapper>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
            .fromEntity(ElectiveDiscipline.class, "ed")
            .column(ElectiveDiscipline.registryElementPart().registryElement().fromAlias("ed").s())
            .where(DQLExpressions.eqValue(DQLExpressions.property(ElectiveDiscipline.type().code().fromAlias("ed")), electiveDisciplineTypeCode))
//            .predicate(DQLPredicateType.distinct)
            .order(DQLExpressions.property(ElectiveDiscipline.registryElementPart().registryElement().title().fromAlias("ed")));

        List<EppRegistryElement> lst = getList(builder);

        Long iwd = 0L;
        for (EppRegistryElement item : lst){
            disciplineTitleList.add(new IdentifiableWrapper(iwd++, item.getTitleWithNumber()));
        }

        return disciplineTitleList;
    }
    */
    protected ISelectModel getDisciplineListSelectModel(final String electiveDisciplineTypeCode) {
        return new DQLFullCheckSelectModel(EppRegistryElement.titleWithNumber().s()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(ElectiveDiscipline.class, "ed")
                        .column(ElectiveDiscipline.registryElementPart().registryElement().id().fromAlias("ed").s())
                        .where(DQLExpressions.eqValue(property(ElectiveDiscipline.type().code().fromAlias("ed")), electiveDisciplineTypeCode))
                        .distinct();

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, alias)
                        .where(DQLExpressions.in(property(EppRegistryElement.id().fromAlias(alias)), subBuilder.buildQuery()))
                        .order(property(EppRegistryElement.title().fromAlias(alias)));

                FilterUtils.applyLikeFilter(builder, filter, EppRegistryElement.number().fromAlias(alias), EppRegistryElement.title().fromAlias(alias));

                return builder;
            }
        };
    }

    protected List<EppRegistryElement> getFilterEppRegistryElement(List<String> disciplines) {
        if (disciplines != null && !disciplines.isEmpty()) {
            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElement.class, "EppRegistryElement")
                    .column("EppRegistryElement")
                    .where(DQLExpressions.in(property(EppRegistryElement.title().fromAlias("EppRegistryElement")), disciplines));

            return getList(dql);
        }
        return null;
    }
}
