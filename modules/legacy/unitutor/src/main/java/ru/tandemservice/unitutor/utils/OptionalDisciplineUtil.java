package ru.tandemservice.unitutor.utils;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.gen.EppStudent2WorkPlanGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;


import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class OptionalDisciplineUtil {


    /**
     * Формируем MAP с формами контроля для для частей элементов реестра
     *
     * @param lstRegistryElementPartId
     *
     * @return
     */
    public static Map<Long, String> getMapFormsOfControlTitles(List<Long> lstRegistryElementPartId)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartFControlAction.class, "efca")
                .where(in(
                        property(EppRegistryElementPartFControlAction.part().id().fromAlias("efca")),
                        lstRegistryElementPartId
                ))
                        // id части
                .column(EppRegistryElementPartFControlAction.part().id().fromAlias("efca").s())
                        // название контрольного мероприятия
                .column(EppRegistryElementPartFControlAction.controlAction().title().fromAlias("efca").s());

        List<Object[]> lst = UniDaoFacade.getCoreDao().getList(builder);

        Map<Long, String> retVal = new HashMap<>();

        for (Object[] objs : lst) {
            Long id = (Long) objs[0];
            String actionType = (String) objs[1];


            if (retVal.containsKey(id)) {
                String title = retVal.get(id) + ", " + actionType;
                retVal.put(id, title);
            }
            else
                retVal.put(id, actionType);

        }

        return retVal;

    }

    /**
     * Список итоговых форм контроля по части дисциплины из РУПа
     *
     * @param partId - id части дисциплины
     */
    public static List<EppRegistryElementPartFControlAction> getFormOfControlList(Long partId) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartFControlAction.class, "efca")
                .where(eqValue(
                        property(EppRegistryElementPartFControlAction.part().id().fromAlias("efca")),
                        partId
                ))
                .column("efca");

        return UniDaoFacade.getCoreDao().getList(builder);
    }

    /**
     * <TODO> близнец ниже описан
     * Названия итоговых форм контроля
     *
     * @param formOfControlList
     *
     * @return
     */
    public static String getFormsOfControlTitles(List<EppRegistryElementPartFControlAction> formOfControlList) {
        StringBuilder formOfControlBuilder = new StringBuilder("");
        for (EppRegistryElementPartFControlAction formControl : formOfControlList) {
            String controlTitle = formControl.getControlAction().getTitle();
            if (StringUtils.isEmpty(formOfControlBuilder.toString()))
                formOfControlBuilder.append(controlTitle);
            else formOfControlBuilder.append(", ").append(controlTitle);
        }
        return formOfControlBuilder.toString();
    }

    /**
     * Названия итоговых форм контроля по части дисциплины из РУПа
     *
     * @param partId
     *
     * @return
     */
    public static String getFormsOfControlTitles(Long partId) {
        List<EppRegistryElementPartFControlAction> formOfControlList = getFormOfControlList(partId);
        StringBuilder formOfControlBuilder = new StringBuilder("");
        for (EppRegistryElementPartFControlAction formControl : formOfControlList) {
            String controlTitle = formControl.getControlAction().getTitle();
            if (StringUtils.isEmpty(formOfControlBuilder.toString()))
                formOfControlBuilder.append(controlTitle);
            else formOfControlBuilder.append(", ").append(controlTitle);
        }
        return formOfControlBuilder.toString();
    }

    /**
     * Список групп студентов сотрудника-тьютора
     */
    public static List<Group> getTutorGroups(Employee tutor) {

        if (tutor == null)
            return null;

        List<EmployeePost> posts = tutor.getPosts();

        List<Group> tutorGroups = null;

        if (posts != null && !posts.isEmpty()) {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(Group.class, "g")
                    .where(in(property(Group.curator().fromAlias("g")), Collections.unmodifiableList(posts)))
                    .where(eqValue(property(Group.archival().fromAlias("g")), Boolean.FALSE))
                    .column("g");

            tutorGroups = UniDaoFacade.getCoreDao().getList(builder);
        }
        return tutorGroups;
    }

    /**
     * Подразделения, на которых сотрудник работает тьютором
     *
     * @param tutor
     *
     * @return
     */
    public static List<Long> getOrgUnitList4Tutor(Employee tutor) {
        if (tutor == null)
            return null;

        List<EmployeePost> posts = tutor.getPosts();
        List<Long> orgUnits = null;

        if (posts != null && !posts.isEmpty()) {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(EmployeePost.class, "ep")
                    .joinEntity("ep", DQLJoinType.inner, Group.class, "g",
                                eq(property(EmployeePost.id().fromAlias("ep")), property(Group.curator().id().fromAlias("g"))))
                    .where(in(property(Group.curator().fromAlias("g")), Collections.unmodifiableList(posts)))
                    .where(eqValue(property(Group.archival().fromAlias("g")), Boolean.FALSE))
                    .column(EmployeePost.orgUnit().id().fromAlias("ep").s())
                    .distinct();

            orgUnits = UniDaoFacade.getCoreDao().getList(builder);
        }

        return orgUnits;
    }

    /**
     * Проверка существует ли актуальная подписка на данную ДПВ
     *
     * @param electiveDiscipline
     * @param student
     *
     * @return
     */
    public static boolean isAlreadySubscribed(ElectiveDiscipline electiveDiscipline, Student student, EducationYear educationYear) {
        DQLSelectBuilder checkBuilder = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, "eds")
                .joinPath(DQLJoinType.inner, ElectiveDisciplineSubscribe.electiveDiscipline().fromAlias("eds"))
                .where(eqValue(property(ElectiveDisciplineSubscribe.student().id().fromAlias("eds")),
                        student.getId()))
                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().id().fromAlias("eds")),
                        electiveDiscipline.getId()))
                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().id().fromAlias("eds")),
                        educationYear.getId()))
                .where(isNull(ElectiveDisciplineSubscribe.removalDate().fromAlias("eds")));

        int resultSize = UniDaoFacade.getCoreDao().getList(checkBuilder).size();

        return (resultSize > 0);
    }

    /**
     * Список связей РУПов и студентов за указанный учебный год
     *
     * @param studentList
     * @param educationYear
     *
     * @return
     */
    public static List<EppStudent2WorkPlan> getStudent2WorkPlanList(List<Student> studentList, Integer educationYear) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "s2wp")
                .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("s2wp"), "wp")
                .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("s2wp"), "s2epv")
                .where(isNull(property(EppStudent2WorkPlanGen.removalDate().fromAlias("s2wp"))))
                .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
                .where(in(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), studentList))
                .column("s2wp");

        if (educationYear != null) {
            builder.where(eqValue(
                    property(EppStudent2WorkPlan.cachedEppYear().educationYear().intValue().fromAlias("s2wp")),
                    educationYear));
        }

        return UniDaoFacade.getCoreDao().getList(builder);

    }

    /**
     * Текущий пользователь системы
     *
     * @return
     */
    public static String getCurrentUser() {
        Person currentUser = PersonManager.instance().dao().getPerson(ContextLocal.getUserContext().getPrincipalContext());
        if (currentUser == null)
            return "Администратор";

        return StringUtils.trimToEmpty(currentUser.getFullFio());
    }

    /**
     * Источник, изменивший запись о подписке на ДПВ (со стороны Тандема)
     */
    public static String getNewSource() {
        StringBuilder newSourceBuilder = new StringBuilder("");
        newSourceBuilder.append(getCurrentUser())
                .append(", ").append(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(Calendar.getInstance().getTime()));

        return newSourceBuilder.toString();
    }


    /**
     * РУПы студента за указанный учебный год
     *
     * @param student
     * @param educationYear
     *
     * @return
     */
    public static List<EppWorkPlanBase> getStudentWorkPlanList(Student student, int educationYear) {
        List<EppStudent2WorkPlan> student2WorkPlanList = getStudent2WorkPlanList(Collections.singletonList(student), educationYear);

        List<EppWorkPlanBase> workPlanList = new ArrayList<EppWorkPlanBase>();
        for (EppStudent2WorkPlan s2wp : student2WorkPlanList) {
            workPlanList.add(s2wp.getWorkPlan());
        }

        return workPlanList;
    }

    /**
     * Список дисциплин, входящих в группу дисциплин по выбору указанного студента на указанный учебный год
     *
     * @param student
     * @param educationYear
     *
     * @return
     */
    public static List<EppWorkPlanRegistryElementRow> getOptionalDisciplines4Student(Student student, int educationYear) {

        List<EppWorkPlanBase> workPlanList = getStudentWorkPlanList(student, educationYear);

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, "wp_row")
                .where(in(
                        property(EppWorkPlanRegistryElementRow.id().fromAlias("wp_row")),
                        TutorBuilderUtils.getDisciplineSubDQL(workPlanList).buildQuery()))
                .column("wp_row");

        return UniDaoFacade.getCoreDao().getList(builder);

    }


    /**
     * Список дисциплин, входящих в группу дисциплин по выбору указанного студента на указанный учебный год
     *
     * @param student
     * @param educationYear
     *
     * @return List <Object []>{
     * [0] - EppWorkPlanRegistryElementRow,
     * [1] - EppRegistryElement,
     * [2] - EppRegistryElementPart,
     * [3] - ElectiveDiscipline
     * [4] - Integer (Количество записей на данную дисциплину)
     * }
     */
    public static List<Object[]> getOptionalDisciplinesData(Student student, int educationYear) {

        List<EppWorkPlanBase> workPlanList = getStudentWorkPlanList(student, educationYear);

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, "wp_row")
                .joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias("wp_row"))
                .where(in(property(EppWorkPlanRegistryElementRow.id().fromAlias("wp_row")),
                        TutorBuilderUtils.getDisciplineSubDQL(workPlanList).buildQuery()))
                .joinEntity("wp_row", DQLJoinType.left, ElectiveDiscipline.class, "ed",
                            eq(property(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias("wp_row")),
                               property(ElectiveDiscipline.registryElementPart().id().fromAlias("ed"))))
                        // добавляем вычисляемый DataSource
                .joinDataSource("ed", DQLJoinType.left, TutorBuilderUtils.getDisciplinePlacesDSSubDQL().buildQuery(),"ds",
                                eq(property(ElectiveDiscipline.id().fromAlias("ed")),property("ds.id")))
                .column("wp_row")
                .column(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias("wp_row").s())
                .column(EppWorkPlanRegistryElementRow.registryElementPart().fromAlias("wp_row").s())
                .column("ed")
                .column("ds.subscribe");

        return UniDaoFacade.getCoreDao().getList(builder);

    }

    /**
     * Актуальные подписки студента на дисциплины по выбору на указанный учебный год
     *
     * @param student
     * @param educationYear
     *
     * @return
     */
    public static List<ElectiveDisciplineSubscribe> getStudentSubscribes(Student student, EducationYear educationYear) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, "eds")
                .where(eqValue(property(ElectiveDisciplineSubscribe.student().id().fromAlias("eds")),student.getId()))
                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().id().fromAlias("eds")),educationYear.getId()))
                .where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias("eds"))))
                .column("eds");

        return UniDaoFacade.getCoreDao().getList(builder);

    }

    /**
     * Возвращает идентификаторы дисциплин по выбору, на которые подписан студент(только актуальные) на указанный учебный год
     *
     * @param student
     * @param educationYear
     *
     * @return
     */
    public static List<Long> getOptionalDisciplineIdsFromSubscribes(Student student, EducationYear educationYear) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, "eds")
                .where(eqValue(property(ElectiveDisciplineSubscribe.student().id().fromAlias("eds")),student.getId()))
                .where(eqValue(property(ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().id().fromAlias("eds")),educationYear.getId()))
                .where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias("eds"))))
                .column(ElectiveDisciplineSubscribe.electiveDiscipline().id().fromAlias("eds").s());

        return UniDaoFacade.getCoreDao().getList(builder);

    }

    /**
     * ДПВ на указанный год
     *
     * @param registryElement
     * @param educationYear
     *
     * @return
     */
    public static ElectiveDiscipline getElectiveDiscipline(EppRegistryElement registryElement, EducationYear educationYear) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ElectiveDiscipline.class, TutorBuilderUtils.ELECTIVE_DISCIPLINE_ALIAS)
                .where(eqValue(property(ElectiveDiscipline.registryElementPart().registryElement().id().fromAlias(TutorBuilderUtils.ELECTIVE_DISCIPLINE_ALIAS)),
                        registryElement.getId()))
                .where(eqValue(property(ElectiveDiscipline.educationYear().id().fromAlias(TutorBuilderUtils.ELECTIVE_DISCIPLINE_ALIAS)),
                        educationYear.getId()))
                .column(TutorBuilderUtils.ELECTIVE_DISCIPLINE_ALIAS);

        List<ElectiveDiscipline> disciplineList = UniDaoFacade.getCoreDao().getList(builder);
        return disciplineList.isEmpty() ? null : disciplineList.get(0);

    }


    /**
     * Версия учебного плана
     *
     * @param student
     *
     * @return
     */
    public static List<Long> getEduPlanVersion(Student student) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
                .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))
                .where(eqValue(property(EppStudent2EduPlanVersion.student().id().fromAlias("s2epv")), student.getId()))
                .column(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("s2epv")));

        List<Long> eduPlanVersions = UniDaoFacade.getCoreDao().getList(builder);
        return eduPlanVersions;
    }

    /**
     * Актуальные подписки на ДПВ
     *
     * @param electiveDiscipline
     *
     * @return List
     * Список подписок
     */
    public static List<ElectiveDisciplineSubscribe> getSubscribes(ElectiveDiscipline electiveDiscipline) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, "eds")
                .joinPath(DQLJoinType.inner, ElectiveDisciplineSubscribe.electiveDiscipline().fromAlias("eds"))
                .where(eqValue(
                        property(ElectiveDisciplineSubscribe.electiveDiscipline().id().fromAlias("eds")),
                        electiveDiscipline.getId()))
                .where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias("eds"))))
                .column("eds");

        return UniDaoFacade.getCoreDao().getList(builder);

    }

    /**
     * Актуальные подписки на ДПВ
     *
     * @param electiveDisciplines
     *
     * @return Map<Название ДПВ, Список подписок>
     */
    public static Map<String, List<ElectiveDisciplineSubscribe>> getSubscribes(List<ElectiveDiscipline> electiveDisciplines) {

        Map<String, List<ElectiveDisciplineSubscribe>> subscribesMap = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, "eds")
                .joinPath(DQLJoinType.inner, ElectiveDisciplineSubscribe.electiveDiscipline().fromAlias("eds"))
                .where(in(
                        property(ElectiveDisciplineSubscribe.electiveDiscipline().fromAlias("eds")),
                        electiveDisciplines))
                .where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias("eds"))))
                .column("eds");

        List<ElectiveDisciplineSubscribe> subscribes = UniDaoFacade.getCoreDao().getList(builder);

        for (ElectiveDisciplineSubscribe subscribe : subscribes) {

            String discipline = subscribe.getElectiveDiscipline().getRegistryElementPart().getRegistryElement().getTitle();

            if (subscribesMap.containsKey(discipline))
                subscribesMap.get(discipline).add(subscribe);
            else {
                List<ElectiveDisciplineSubscribe> lst = new ArrayList<>();
                lst.add(subscribe);
                subscribesMap.put(discipline, lst);
            }
        }

        return subscribesMap;
    }

    /**
     * Возвращает группу для ДПВ
     *
     * @param registryElementRow
     * @param workPlanList
     *
     * @return
     */
    public static Long getGroupImRowId(EppWorkPlanRegistryElementRow registryElementRow, List<EppWorkPlanBase> workPlanList) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, "wp_row")
                .where(in(
                        property(EppWorkPlanRegistryElementRow.workPlan().fromAlias("wp_row")),
                        workPlanList))
                .joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().fromAlias("wp_row"), "erep")
                .joinEntity("erep", DQLJoinType.inner, EppEpvRegistryRow.class, "eerr",
                            eq(property(EppRegistryElementPart.registryElement().fromAlias("erep")),
                                    property(EppEpvRegistryRow.registryElement().fromAlias("eerr"))))
                .joinEntity("eerr", DQLJoinType.inner, EppEpvGroupImRow.class, "eegir",
                            eq(property(EppEpvRegistryRow.parent().id().fromAlias("eerr")),
                                    property(EppEpvGroupImRow.id().fromAlias("eegir"))))
                .where(eqValue(property(EppEpvGroupImRow.owner().id().fromAlias("eegir")),
                        registryElementRow.getWorkPlan().getBlock().getId()))
                .where(eqValue(property(EppWorkPlanRegistryElementRow.id().fromAlias("wp_row")),
                        registryElementRow.getId()))
                .column(EppEpvGroupImRow.id().fromAlias("eegir").s());

        List<Long> groupInRowList = UniDaoFacade.getCoreDao().getList(builder);

        if (!groupInRowList.isEmpty())
            return groupInRowList.get(0);

        return null;
    }

    /**
     * Аннотации для дисциплин
     */
    public static Map<Long, String> getAnnotationMapByRegistryElement(List<EppRegistryElement> lst) {

        if (lst == null || lst.isEmpty())
            return new HashMap<>();

        return getAnnotationMap(CommonDAO.ids(lst));
    }

    /**
     * Аннотации для дисциплин
     *
     * @param lst
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map<Long, String> getAnnotationMap(List<?> lst) {

        if (lst == null || lst.isEmpty())
            return new HashMap<>();

        String propertyPath = "";

        Object type = lst.get(0);

        if (type instanceof EppRegistryElementPart)
            propertyPath = EppRegistryElementPart.L_REGISTRY_ELEMENT + "." + EppRegistryElement.P_ID;
        else if (type instanceof ElectiveDiscipline)
            propertyPath = ElectiveDiscipline.L_REGISTRY_ELEMENT_PART + "." + EppRegistryElementPart.L_REGISTRY_ELEMENT + "." + EppRegistryElement.P_ID;
        else if (type instanceof ElectiveDisciplineSubscribe)
            propertyPath = ElectiveDisciplineSubscribe.L_ELECTIVE_DISCIPLINE + "." + ElectiveDiscipline.L_REGISTRY_ELEMENT_PART + "." + EppRegistryElementPart.L_REGISTRY_ELEMENT + "." + EppRegistryElement.P_ID;

        Collection<Long> ids = !StringUtils.isEmpty(propertyPath) ? ((Collection<Long>) CollectionUtils.collect(lst, new BeanToPropertyValueTransformer(propertyPath))) : null;

        return getAnnotationMap(ids);
    }

    /**
     * Map<id EppRegistryElement, String сама аннотация>
     *
     * @param ids
     *
     * @return
     */
    public static Map<Long, String> getAnnotationMap(Collection<Long> ids) {

        if (ids == null || ids.isEmpty())
            return new HashMap<>();

        Map<Long, String> resultMap = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementExt.class, "ext")
                .where(in(property(EppRegistryElementExt.eppRegistryElement().id().fromAlias("ext")), ids))
                .column("ext");

        List<EppRegistryElementExt> registryElementExts = UniDaoFacade.getCoreDao().getList(builder);

        for (EppRegistryElementExt ext : registryElementExts)
            resultMap.put(ext.getEppRegistryElement().getId(), ext.getAnnotation());

        return resultMap;
    }


    /**
     * Map<id EppWorkPlanRegistryElementRow, String сама аннотация>
     */
    public static Map<Long, String> getAnnotationMapByWorkPlanRow(List<EppWorkPlanRegistryElementRow> workPlanRegistryElementRows) {

        if (workPlanRegistryElementRows == null || workPlanRegistryElementRows.isEmpty())
            return new HashMap<>();

        // id дисциплины на список id строк РУПа
        Map<Long, List<Long>> registryElementRow2registryElement = new HashMap<>();
        for (EppWorkPlanRegistryElementRow elementRow : workPlanRegistryElementRows) {
            if (registryElementRow2registryElement.containsKey(elementRow.getRegistryElement().getId()))
                registryElementRow2registryElement.get(elementRow.getRegistryElement().getId()).add(elementRow.getId());
            else {
                List<Long> lst = new ArrayList<>();
                lst.add(elementRow.getId());
                registryElementRow2registryElement.put(elementRow.getRegistryElement().getId(), lst);
            }

        }

        Map<Long, String> resultMap = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementExt.class, "ext")
                .where(in(property(EppRegistryElementExt.eppRegistryElement().id().fromAlias("ext")),
                        registryElementRow2registryElement.keySet()))
                .column("ext");

        List<EppRegistryElementExt> registryElementExts = UniDaoFacade.getCoreDao().getList(builder);

        for (EppRegistryElementExt ext : registryElementExts) {
            List<Long> registryElementRows = registryElementRow2registryElement.get(ext.getEppRegistryElement().getId());
            if (!StringUtils.isEmpty(ext.getAnnotation())) {
                for (Long registryElementRowId : registryElementRows)
                    resultMap.put(registryElementRowId, ext.getAnnotation());
            }
        }

        return resultMap;
    }

    public static Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> getSubscribesMap(Student student, List<ElectiveDiscipline> electiveDisciplines) {
        Map<ElectiveDiscipline, ElectiveDisciplineSubscribe> subscribesMap = new HashMap<ElectiveDiscipline, ElectiveDisciplineSubscribe>();

        if (electiveDisciplines != null && !electiveDisciplines.isEmpty() && student != null) {

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(ElectiveDisciplineSubscribe.class, "eds")
                    .where(in(property(ElectiveDisciplineSubscribe.electiveDiscipline().fromAlias("eds")), electiveDisciplines))
                    .where(eqValue(property(ElectiveDisciplineSubscribe.student().fromAlias("eds")), student))
                    .column("eds");

            List<ElectiveDisciplineSubscribe> subscribes = UniDaoFacade.getCoreDao().getList(dql);
            for (ElectiveDisciplineSubscribe subscribe : subscribes) {
                subscribesMap.put(subscribe.getElectiveDiscipline(), subscribe);
            }
        }

        return subscribesMap;
    }

}
