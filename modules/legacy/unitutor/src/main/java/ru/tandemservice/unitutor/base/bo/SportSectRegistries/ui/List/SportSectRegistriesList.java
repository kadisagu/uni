/* $Id$ */
package ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.logic.DefaultActiveCatalogComboDSHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.SportSectRegistriesManager;
import ru.tandemservice.unitutor.base.bo.SportSectRegistries.ui.Pub.SportSectRegistriesPub;
import ru.tandemservice.unitutor.entity.SportSection;
import ru.tandemservice.unitutor.entity.SportSection2CourseRel;
import ru.tandemservice.unitutor.entity.SportSection2SexRel;
import ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel;
import ru.tandemservice.unitutor.entity.catalog.SportSectionType;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Andrey Andreev
 * @since 22.08.2016
 */
@Configuration
public class SportSectRegistriesList extends BusinessComponentManager
{
    public static final String SECTION_SEARCH_LIST_DS = "sectionSearchListDS";
    public static final String SEARCH_LIST_COLUMN_TITLE = "titleColumn";
    public static final String SEARCH_LIST_COLUMN_EDUCATION_YEAR = "educationYear";
    public static final String SEARCH_LIST_COLUMN_SECTION_TYPE = "sectionType";
    public static final String SEARCH_LIST_COLUMN_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String SEARCH_LIST_COLUMN_PHYSICAL_FITNESS_LEVEL = "physicalFitnessLevel";
    public static final String SEARCH_LIST_COLUMN_COURSE = "course";
    public static final String SEARCH_LIST_COLUMN_SEX = "sex";
    public static final String SEARCH_LIST_COLUMN_TUTOR_ORG_UNIT = "tutorOrgUnit";
    public static final String SEARCH_LIST_COLUMN_PLACE = "place";
    public static final String SEARCH_LIST_COLUMN_TRAINER = "trainerColumn";
    public static final String SEARCH_LIST_COLUMN_FIRSTCLASS = "firstClass";
    public static final String SEARCH_LIST_COLUMN_SECONDCLASS = "secondClass";
    public static final String SEARCH_LIST_COLUMN_NUMBERS = "numbers";
    public static final String SEARCH_LIST_COLUMN_FREE_NUMBERS = "freeNumbers";

    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String EDUCATION_YEAR_LIST_PARAM = "educationYearList";

    public static final String SECTION_NAME_PARAM = "sectionName";

    public static final String SECTION_TYPE_DS = "sectionTypeDS";
    public static final String SECTION_TYPE_PARAM = "sectionType";

    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String FORMATIVE_ORG_UNIT_LIST_PARAM = "formativeOrgUnitList";

    public static final String PHYSICAL_FITNESS_LEVEL_DS = "physicalFitnessLevelDS";
    public static final String PHYSICAL_FITNESS_LEVEL_LIST_PARAM = "physicalFitnessLevelList";

    public static final String EXIST_FREE_PLACES_PARAM = "existFreePlaces";

    public static final String COURSE_DS = "courseDS";
    public static final String COURSE_LIST_PARAM = "courseList";

    public static final String SEX_DS = "sexDS";
    public static final String SEX_LIST_PARAM = "sexList";

    public static final String TUTOR_ORG_UNIT_DS = "tutorOrgUnitDS";
    public static final String TUTOR_ORG_UNIT_LIST_PARAM = "tutorOrgUnitList";

    public static final String PLACE_DS = "placeDS";
    public static final String PLACE_LIST_PARAM = "placeList";

    public static final String TRAINER_DS = "trainerDS";
    public static final String TRAINER_LIST_PARAM = "trainerList";

    public static final String DAY_OF_WEEK_DS = "dayOfWeekDS";
    public static final String FIRST_CLASS_DAY_PARAM = "firstClassDay";
    public static final String SECOND_CLASS_DAY_PARAM = "secondClassDay";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SECTION_SEARCH_LIST_DS, sectionSearchListDSColumnExtPoint(), sectionSearchListDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR_DS, educationYearDSHandler()))
                .addDataSource(selectDS(SECTION_TYPE_DS, sectionTypeDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(PHYSICAL_FITNESS_LEVEL_DS, physicalFitnessLevelDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(SEX_DS, sexDSHandler()))
                .addDataSource(selectDS(TUTOR_ORG_UNIT_DS, tutorOrgUnitDSHandler()))
                .addDataSource(selectDS(PLACE_DS, SportSectRegistriesManager.instance().sportPlaceDSHandler())
                                       .addColumn("title", "", source -> ((UniplacesPlace) source).getTitleWithLocation()))
                .addDataSource(selectDS(TRAINER_DS, SportSectRegistriesManager.instance().trainerDSHandler()))
                .addDataSource(selectDS(DAY_OF_WEEK_DS, SportSectRegistriesManager.instance().dayOfWeekDSHandler()))
                .create();
    }


    @Bean
    public ColumnListExtPoint sectionSearchListDSColumnExtPoint()
    {
        IPublisherLinkResolver sectionResolver = new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(final IEntity entity)
            {
                return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId());
            }

            @Override
            public String getComponentName(final IEntity entity)
            {
                return SportSectRegistriesPub.class.getSimpleName();
            }
        };

        return columnListExtPointBuilder(SECTION_SEARCH_LIST_DS)
                .addColumn(publisherColumn(SEARCH_LIST_COLUMN_TITLE, SportSection.title().s()).publisherLinkResolver(sectionResolver).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_EDUCATION_YEAR, SportSection.educationYear().title().s()).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_SECTION_TYPE, SportSection.type().title().s()).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_FORMATIVE_ORG_UNIT, SportSection.formativeOrgUnit().shortTitle().s()).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_PHYSICAL_FITNESS_LEVEL, SportSection.physicalFitnessLevel().title().s()).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_COURSE, SEARCH_LIST_COLUMN_COURSE))
                .addColumn(textColumn(SEARCH_LIST_COLUMN_SEX, SEARCH_LIST_COLUMN_SEX))
                .addColumn(textColumn(SEARCH_LIST_COLUMN_TUTOR_ORG_UNIT, SportSection.tutorOrgUnit().shortTitle().s()).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_PLACE, SportSection.place().s()).formatter(source -> ((UniplacesPlace) source).getTitleWithLocation()).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_TRAINER, SEARCH_LIST_COLUMN_TRAINER).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_FIRSTCLASS, SEARCH_LIST_COLUMN_FIRSTCLASS).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_SECONDCLASS, SEARCH_LIST_COLUMN_SECONDCLASS).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_NUMBERS, SportSection.numbers().s()).order())
                .addColumn(textColumn(SEARCH_LIST_COLUMN_FREE_NUMBERS, SEARCH_LIST_COLUMN_FREE_NUMBERS).order())
                .addColumn(actionColumn("edit", new Icon("edit"), "onClickEdit").permissionKey("editSection_List"))
                .addColumn(actionColumn("delete", new Icon("delete"), "onClickDelete").permissionKey("deleteSection_List")
                                   .alert(FormattedMessage.with().template("sectionSearchListDS.delete.alert").parameter(SportSection.title().s()).create()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sectionSearchListDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                String alias = "e";
                DQLSelectBuilder builder = getSportSectionListBuilder(alias, context);

                OrderDescription titleOrder = new OrderDescription(alias, SportSection.title(), OrderDirection.asc, false);

                builder.joinPath(DQLJoinType.left, SportSection.formativeOrgUnit().fromAlias(alias), "foe");

                EntityOrder entityOrder = input.getEntityOrder();
                if (entityOrder.getColumnName().equals(SEARCH_LIST_COLUMN_FREE_NUMBERS))
                    builder.order(SEARCH_LIST_COLUMN_FREE_NUMBERS, entityOrder.getDirection());

                DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(SportSection.class, alias);
                orderRegistry
                        .setOrders(SportSection.title(), new OrderDescription(SportSection.title()))
                        .setOrders(SportSection.educationYear().title(), new OrderDescription(SportSection.educationYear().title()))
                        .setOrders(SportSection.type().title(), new OrderDescription(SportSection.type().title()))
                        .setOrders(SportSection.formativeOrgUnit().shortTitle(), new OrderDescription("foe", OrgUnit.shortTitle()), titleOrder)
                        .setOrders(SportSection.physicalFitnessLevel().title(), new OrderDescription(SportSection.physicalFitnessLevel().title()))
                        .setOrders(SportSection.place().title(), new OrderDescription(SportSection.place().title()))

                        .setOrders(SEARCH_LIST_COLUMN_FIRSTCLASS,
                                   new OrderDescription(SportSection.firstClassDay()),
                                   new OrderDescription(SportSection.firstClassTime()), titleOrder)

                        .setOrders(SEARCH_LIST_COLUMN_SECONDCLASS,
                                   new OrderDescription(SportSection.secondClassDay()),
                                   new OrderDescription(SportSection.secondClassTime()), titleOrder)

                        .setOrders(SportSection.numbers(), new OrderDescription(SportSection.numbers()));

                builder.joinPath(DQLJoinType.left, SportSection.trainer().fromAlias(alias), "tr")
                        .joinPath(DQLJoinType.left, PpsEntry.person().fromAlias("tr"), "tr1")
                        .joinPath(DQLJoinType.left, Person.identityCard().fromAlias("tr1"), "tr2");
                orderRegistry.setOrders(SEARCH_LIST_COLUMN_TRAINER, new OrderDescription("tr2", IdentityCard.lastName()), new OrderDescription("tr2", IdentityCard.firstName()), new OrderDescription("tr2", IdentityCard.middleName()));

                orderRegistry.applyOrder(builder, entityOrder);

                return wrapSportSectionList(DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build());
            }
        };
    }

    protected DQLSelectBuilder getSportSectionListBuilder(String alias, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(SportSection.class, alias)
                .column(property(alias))
                .column(SportSection.getFreePlacesExpression(alias), SEARCH_LIST_COLUMN_FREE_NUMBERS);

        FilterUtils.applySelectFilter(builder, alias, SportSection.educationYear(), context.get(EDUCATION_YEAR_LIST_PARAM));
        FilterUtils.applySimpleLikeFilter(builder, alias, SportSection.title(), context.get(SECTION_NAME_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.type(), context.get(SECTION_TYPE_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.formativeOrgUnit(), context.get(FORMATIVE_ORG_UNIT_LIST_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.physicalFitnessLevel(), context.get(PHYSICAL_FITNESS_LEVEL_LIST_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.tutorOrgUnit(), context.get(TUTOR_ORG_UNIT_LIST_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.place(), context.get(PLACE_LIST_PARAM));
        FilterUtils.applySelectFilter(builder, alias, SportSection.trainer(), context.get(TRAINER_LIST_PARAM));

        DataWrapper firstDay = context.get(FIRST_CLASS_DAY_PARAM);
        if (firstDay != null)
            FilterUtils.applySelectFilter(builder, alias, SportSection.firstClassDay(), firstDay.<DayOfWeek>getWrapped().getValue());
        DataWrapper secondDay = context.get(SECOND_CLASS_DAY_PARAM);
        if (secondDay != null)
            FilterUtils.applySelectFilter(builder, alias, SportSection.secondClassDay(), secondDay.<DayOfWeek>getWrapped().getValue());

        DataWrapper existFreePlaces = context.get(EXIST_FREE_PLACES_PARAM);
        if (existFreePlaces != null)
        {
            IDQLExpression e = gt(SportSection.getFreePlacesExpression(alias), value(0));

            if (existFreePlaces.getId().equals(TwinComboDataSourceHandler.YES_ID))
                builder.where(e);
            else
                builder.where(not(e));
        }

        SportSection.addWhereExistsCourseRel(builder, property(alias), context.get(COURSE_LIST_PARAM));
        SportSection.addWhereExistsSexRel(builder, property(alias), context.get(SEX_LIST_PARAM));

        return builder;
    }

    protected DSOutput wrapSportSectionList(DSOutput output)
    {
        ICommonDAO dao = DataAccessServices.dao();
        List<Long> sectionIds = output.getRecordList().stream()
                .map(o -> (SportSection) ((Object[]) o)[0]).map(EntityBase::getId).collect(Collectors.toList());
        Map<Long, StringBuilder> coursesBySection = dao.getList(SportSection2CourseRel.class,
                                                                SportSection2CourseRel.section().id().s(), sectionIds)
                .stream()
                .sorted((o1, o2) -> Integer.compare(o1.getCourse().getIntValue(), o2.getCourse().getIntValue()))
                .collect(Collectors.groupingBy(rel -> rel.getSection().getId(),
                                               Collectors.mapping(rel -> rel,
                                                                  Collector.of(StringBuilder::new, (b, rel) -> {
                                                                      if (b.length() > 0)
                                                                          b.append(", ");
                                                                      b.append(rel.getCourse().getTitle());
                                                                  }, StringBuilder::append)
                                               )));

        Map<Long, StringBuilder> sexBySection = dao.getList(SportSection2SexRel.class,
                                                            SportSection2SexRel.section().id().s(), sectionIds)
                .stream()
                .collect(Collectors.groupingBy(rel -> rel.getSection().getId(),
                                               Collectors.mapping(rel -> rel,
                                                                  Collector.of(StringBuilder::new, (b, rel) -> {
                                                                      if (b.length() > 0)
                                                                          b.append(", ");
                                                                      b.append(rel.getSex().getShortTitle());
                                                                  }, StringBuilder::append)
                                               )));

        CollectionUtils.transform(output.getRecordList(), in -> {
            if (in instanceof DataWrapper)
                return in;

            SportSection section = (SportSection) ((Object[]) in)[0];

            StringBuilder courses = coursesBySection.get(section.getId());
            StringBuilder sexes = sexBySection.get(section.getId());

            DataWrapper wrapper = new DataWrapper(section);
            wrapper.setProperty(SEARCH_LIST_COLUMN_FREE_NUMBERS, ((Object[]) in)[1]);
            wrapper.setProperty(SEARCH_LIST_COLUMN_FIRSTCLASS, section.getFirstClassInWeek());
            wrapper.setProperty(SEARCH_LIST_COLUMN_SECONDCLASS, section.getSecondClassInWeek());
            wrapper.setProperty(SEARCH_LIST_COLUMN_TRAINER, section.getTrainer() == null ? "" : section.getTrainer().getFio());
            wrapper.setProperty(SEARCH_LIST_COLUMN_COURSE, courses == null ? "" : courses.toString());
            wrapper.setProperty(SEARCH_LIST_COLUMN_SEX, sexes == null ? "" : sexes.toString());

            return wrapper;
        });

        return output;
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sectionTypeDSHandler()
    {
        return SportSectionType.defaultSelectDSHandler(this.getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> physicalFitnessLevelDSHandler()
    {
        return PhysicalFitnessLevel.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return Course.defaultSelectDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sexDSHandler()
    {
        return new DefaultActiveCatalogComboDSHandler(this.getName(), Sex.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler tutorOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
                .customize((alias, dql, context, filter) -> dql.where(exists(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit().s(), property(alias))))
                .order(OrgUnit.title())
                .where(OrgUnit.archival(), false)
                .filter(OrgUnit.title());
    }
}