package ru.tandemservice.unitutor.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Уровень физической подготовки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PhysicalFitnessLevelGen extends EntityBase
 implements INaturalIdentifiable<PhysicalFitnessLevelGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel";
    public static final String ENTITY_NAME = "physicalFitnessLevel";
    public static final int VERSION_HASH = -1340088288;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_DESCRIPTION = "description";

    private String _code;     // Системный код
    private String _title;     // Наименование
    private String _description;     // Описание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Наименование. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Наименование. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Описание.
     */
    @Length(max=255)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PhysicalFitnessLevelGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((PhysicalFitnessLevel)another).getCode());
            }
            setTitle(((PhysicalFitnessLevel)another).getTitle());
            setDescription(((PhysicalFitnessLevel)another).getDescription());
        }
    }

    public INaturalId<PhysicalFitnessLevelGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<PhysicalFitnessLevelGen>
    {
        private static final String PROXY_NAME = "PhysicalFitnessLevelNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof PhysicalFitnessLevelGen.NaturalId) ) return false;

            PhysicalFitnessLevelGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PhysicalFitnessLevelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PhysicalFitnessLevel.class;
        }

        public T newInstance()
        {
            return (T) new PhysicalFitnessLevel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "description":
                    return obj.getDescription();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "description":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "description":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "description":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PhysicalFitnessLevel> _dslPath = new Path<PhysicalFitnessLevel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PhysicalFitnessLevel");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Наименование. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    public static class Path<E extends PhysicalFitnessLevel> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _description;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(PhysicalFitnessLevelGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Наименование. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(PhysicalFitnessLevelGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.unitutor.entity.catalog.PhysicalFitnessLevel#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(PhysicalFitnessLevelGen.P_DESCRIPTION, this);
            return _description;
        }

        public Class getEntityClass()
        {
            return PhysicalFitnessLevel.class;
        }

        public String getEntityName()
        {
            return "physicalFitnessLevel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
