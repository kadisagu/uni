/* $Id$ */
package ru.tandemservice.unitutor.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Ekaterina Zvereva
 * @since 16.02.2016
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "workPlanWrap", namespace = "http://ws.unitutor.tandemservice.ru/")
public class WsWorkPlanInfoWrap implements Serializable
{
    private String term;
    private Long eduYearId;
    private Integer electiveCount;
    private String electiveDisciplineType;

    public WsWorkPlanInfoWrap()
    {
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public Long getEduYearId()
    {
        return eduYearId;
    }

    public void setEduYearId(Long eduYearId)
    {
        this.eduYearId = eduYearId;
    }

    public Integer getElectiveCount()
    {
        return electiveCount;
    }

    public void setElectiveCount(Integer electiveCount)
    {
        this.electiveCount = electiveCount;
    }

    public String getElectiveDisciplineType()
    {
        return electiveDisciplineType;
    }

    public void setElectiveDisciplineType(String electiveDisciplineType)
    {
        this.electiveDisciplineType = electiveDisciplineType;
    }
}