package ru.tandemservice.unitutor.component.awp.move.MoveSubcscibe;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unitutor.dao.IDaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;

import java.util.*;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        final Model model = component.getModel();
        model.setSettings(component.getSettings());

        super.onRefreshComponent(component);
        prepareDataSourse_A(component);
        prepareDataSourse_B(component);

        IDAO dao = getDao();
        dao.prepare(model);
    }

    private void prepareDataSourse_B(IBusinessComponent component)
    {
        final Model model = component.getModel();
        if (model.getDataSourceB() != null)
            return;

        DynamicListDataSource<ElectiveDisciplineSubscribe> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSourceB(model);
        });
        model.setDataSourceB(dataSource);
        makeColumn(component, dataSource, false);
    }

    private void makeColumn(IBusinessComponent component, DynamicListDataSource<ElectiveDisciplineSubscribe> dataSource, boolean isA)
    {

        CheckboxColumn selectColumn = new CheckboxColumn("select");
        if (isA)
            selectColumn.setListener("onCheckSelectionA");
        else
            selectColumn.setListener("onCheckSelectionB");

        //selectColumn.setControlInHeader(false);
        //selectColumn.setClickable(true);

        dataSource.addColumn(selectColumn);

        dataSource.addColumn(new SimpleColumn("Название ДПВ",
                                              ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().registryElement().title()).setClickable(true).setOrderable(true));


        dataSource.addColumn(new SimpleColumn("Тип записи", ElectiveDisciplineSubscribe.electiveDiscipline().type().shortTitle()).setClickable(false).setOrderable(false).setWidth("100"));

        dataSource.addColumn(new PublisherLinkColumn("№ в реестре",
                                                     ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart().registryElement().number())
                                     .setResolver(new IPublisherLinkResolver() {
                                         @Override
                                         public Object getParameters(IEntity entity) {
                                             final Map<String, Object> parameters = new HashMap<>();
                                             parameters.put(PublisherActivator.PUBLISHER_ID_KEY, ((ElectiveDisciplineSubscribe) entity).getElectiveDiscipline().getRegistryElementPart().getRegistryElement().getId());
                                             return parameters;
                                         }

                                         @Override
                                         public String getComponentName(IEntity paramIEntity) {
                                             return null;
                                         }
                                     })
                                     .setClickable(true)
                                     .setOrderable(true)
                                     .setWidth("50"));

        dataSource.addColumn(new PublisherLinkColumn("ФИО студента",
                                                     ElectiveDisciplineSubscribe.student().person().identityCard().fullFio())
                                     .setResolver(new IPublisherLinkResolver() {
                                         @Override
                                         public Object getParameters(IEntity entity) {
                                             final Map<String, Object> parameters = new HashMap<>();
                                             parameters.put(PublisherActivator.PUBLISHER_ID_KEY, ((ElectiveDisciplineSubscribe) entity).getStudent().getId());
                                             return parameters;
                                         }

                                         @Override
                                         public String getComponentName(IEntity paramIEntity) {
                                             return null;
                                         }
                                     })
                                     .setWidth("250")
                                     .setClickable(true)
                                     .setOrderable(true));


        //dataSource.addColumn(new BooleanColumn("Актуальность ДПВ", ElectiveDisciplineSubscribe.electiveDiscipline().actualRow()).setOrderable(false));
        //dataSource.addColumn(new DateColumn("Дата и время записи", ElectiveDisciplineSubscribe.subscribeOn()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Источник записи", ElectiveDisciplineSubscribe.source()).setClickable(false).setOrderable(false).setWidth("150"));

        //dataSource.addColumn(new SimpleColumn("Дата утраты актуальности", ElectiveDisciplineSubscribe.removalDate(), DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false));
        //dataSource.addColumn(new ActionColumn("Переместить", "daemon", "onClickMove", "Переместить запись?"));

        final Model model = component.getModel();
        if (isA)
            model.setDataSourceA(dataSource);
        else
            model.setDataSourceB(dataSource);
    }

    private void prepareDataSourse_A(IBusinessComponent component)
    {
        final Model model = component.getModel();
        if (model.getDataSourceA() != null)
            return;


        DynamicListDataSource<ElectiveDisciplineSubscribe> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSourceA(model);
        });
        model.setDataSourceA(dataSource);
        makeColumn(component, dataSource, true);
    }

    public void onSearchParamsChange(IBusinessComponent component)
    {
        final Model model = component.getModel();
        component.saveSettings();
        model.setSettings(component.getSettings());
        ((IDAO) Controller.this.getDao()).prepareFilters(model);

        onCheckSelectionA(component);
    }


    public void onCheckSelectionA(IBusinessComponent component)
    {

        final Model model = component.getModel();
        model.setWarning(false);
        model.setWarningMessage("");

        if (model.getFilterB_ElectiveDiscipline() != null) {
            ElectiveDiscipline ed = model.getFilterB_ElectiveDiscipline();
            Collection<IEntity> lst = ((CheckboxColumn) model.getDataSourceA().getColumn("select")).getSelectedObjects();
            if (ed.getPlacesCount() == null)
                return;


            String msg = "";

            int selected = lst.size();
            Long free = ed.getFreePlacesToSubscribe() - selected;
            msg = "Всего мест: " + ed.getPlacesCount() + " было записано: " + ed.getSubscribersCount() + " Выделено для переноса " + selected;

            if (free < 0) {
                model.setWarning(true);// Свободных мест НЕТ
                model.setWarningMessage("Свободных мест НЕТ " + msg);
            }
            else {
                model.setWarning(false);
                model.setWarningMessage(msg);
            }


        }
        else {
            model.setWarning(true);
            model.setWarningMessage("Не выбрана ДПВ для переноса");
        }

    }

    /**
     * При выделении в окне Б разрешаем только перемещенные записи
     */
    public void onCheckSelectionB(IBusinessComponent component)
    {
        final Model model = component.getModel();
        String moved = IDaoStudentSubscribe.MOVED_ROW;

        Collection<IEntity> lst = ((CheckboxColumn) model.getDataSourceB().getColumn("select")).getSelectedObjects();

        List<IEntity> removeList = new ArrayList<>();

        if (lst != null) {
            for (IEntity ent : lst) {
                ElectiveDisciplineSubscribe eds = (ElectiveDisciplineSubscribe) ent;

                if (eds.getSource() == null || !eds.getSource().contains(moved))
                    removeList.add(ent);
            }
        }
        if (!removeList.isEmpty())
            ((CheckboxColumn) model.getDataSourceB().getColumn("select")).getSelectedObjects().removeAll(removeList);
    }

    public void onClickMoveStudent(IBusinessComponent component)
    {
        component.saveSettings();

        Model model = component.getModel();
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSourceA().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать ДПВ для переноса");

        if (model.getFilterB_ElectiveDiscipline() == null)
            throw new ApplicationException("Не выбрана ДПВ (Б) для переноса");

        IDAO idao = getDao();
        idao.moveSubscribe(entityList, model.getFilterB_ElectiveDiscipline(), model.isSendMail());


    }

    public void onClickCanselMoveStudent(IBusinessComponent component)
    {
        component.saveSettings();
        Model model = component.getModel();
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSourceB().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать ДПВ для отмены переноса");

        IDAO idao = getDao();

        idao.undoSubscribe(entityList, model.getElectiveDiscipline(), model.isSendMail());


    }

    public void onSelectAll_A(IBusinessComponent component)
    {
        component.saveSettings();
        Model model = component.getModel();
        List<ElectiveDisciplineSubscribe> entityList = model.getDataSourceA().getEntityList();
        CheckboxColumn ck = (CheckboxColumn) model.getDataSourceA().getColumn("select");
        List<IEntity> selected = new ArrayList<>();
        selected.addAll(entityList);
        ck.setSelectedObjects(selected);

        onCheckSelectionA(component);
    }

    public void onSelectAll_B(IBusinessComponent component)
    {
        component.saveSettings();
        Model model = component.getModel();
        List<ElectiveDisciplineSubscribe> entityList = model.getDataSourceB().getEntityList();

        CheckboxColumn ck = (CheckboxColumn) model.getDataSourceB().getColumn("select");

        List<IEntity> selected = new ArrayList<>();
        selected.addAll(entityList);
        ck.setSelectedObjects(selected);

        onCheckSelectionA(component);
    }

    public void onDeSelect_B(IBusinessComponent component)
    {
        component.saveSettings();
        Model model = component.getModel();

        Collection<IEntity> collection = new ArrayList<>();
        ((CheckboxColumn) model.getDataSourceB().getColumn("select")).setSelectedObjects(collection);

        onCheckSelectionA(component);
    }

    public void onDeSelect_A(IBusinessComponent component)
    {
        component.saveSettings();
        Model model = component.getModel();

        Collection<IEntity> collection = new ArrayList<>();
        ((CheckboxColumn) model.getDataSourceA().getColumn("select")).setSelectedObjects(collection);

        onCheckSelectionA(component);
    }
}
