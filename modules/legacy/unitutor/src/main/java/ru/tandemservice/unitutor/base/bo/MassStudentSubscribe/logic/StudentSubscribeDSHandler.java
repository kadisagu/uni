/* $Id$ */
package ru.tandemservice.unitutor.base.bo.MassStudentSubscribe.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unitutor.base.bo.MassStudentSubscribe.ui.Add.MassStudentSubscribeAdd;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.dao.DisciplineWrap;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 13.11.2015
 */
public class StudentSubscribeDSHandler extends DefaultSearchDataSourceHandler
{

    public StudentSubscribeDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        List<Group> groupList = context.get(MassStudentSubscribeAdd.GROUP_LIST);
        if (groupList == null || groupList.isEmpty()) return ListOutputBuilder.get(input, Collections.emptyList()).build();

        DQLOrderDescriptionRegistry registry = new DQLOrderDescriptionRegistry(Student.class, "s");

        EducationYear year = context.get(MassStudentSubscribeAdd.EDU_YEAR);
        YearDistributionPart yearPart = context.get(MassStudentSubscribeAdd.EDU_YEAR_PART);

        List<StudentStatus> stateList = context.get(MassStudentSubscribeAdd.STATES_LIST);
        String electiveDisciplineTypeCode = context.get(MassStudentSubscribeAdd.ELECTIVE_DISCIPLINE_CODE);
        OrgUnit orgUnit = context.get(MassStudentSubscribeAdd.ORG_UNIT);
        boolean isCathedra = orgUnit != null && OrgUnitTypeCodes.CATHEDRA.equals(orgUnit.getOrgUnitType().getCode());

        DQLSelectBuilder dqlIn = DqlDisciplineBuilder.getDqlInStudent(year, yearPart,
                                                      null, groupList, null, isCathedra ? null : Collections.singletonList(orgUnit), isCathedra ? Collections.singletonList(orgUnit) : null);
        DQLSelectBuilder builder = registry.buildDQLSelectBuilder()
                .where(in(property(Student.id().fromAlias("s")),
                          dqlIn.buildQuery()));
        if (stateList != null && !stateList.isEmpty())
            builder.where(in(property(Student.status().fromAlias("s")), stateList));

        builder.order(property(Student.person().identityCard().fullFio().fromAlias("s")));

        List<Student> studentList = builder.createStatement(context.getSession()).list();
        List<DataWrapper> resultList = createWrappedStudentList(studentList, year, yearPart, electiveDisciplineTypeCode);

        final DSOutput output = ListOutputBuilder.get(input, resultList).build();

        output.setCountRecord(Math.max(1, output.getRecordList().size()));

        return output;
    }

    protected List<DataWrapper> createWrappedStudentList(List<Student> students, EducationYear year, YearDistributionPart yearPart, String disciplineTypeCode)
    {
        List<DataWrapper> result = new ArrayList<>();
        int number=0;
        for (Student student : students) {
            int count=1;
            number++;

            StringBuilder warning = new StringBuilder();
            List<DisciplineWrap> disciplineList;
            if (ElectiveDisciplineTypeCodes.DPV.equals(disciplineTypeCode))
            {
                disciplineList = DaoStudentSubscribe.instanse().getDisciplineWrapList(year, yearPart, student, warning, false, false, disciplineTypeCode);
            }
            else
            {
                List<ElectiveDiscipline> retVal = DaoStudentSubscribe.instanse().getElectiveFromRegistryList(year, yearPart, student,
                                                                                                             disciplineTypeCode, warning);
                count = DaoStudentSubscribe.instanse().getCountElectiveInWorkPlan(year, yearPart, student, disciplineTypeCode);
                disciplineList = retVal.stream()
                        .map(e -> new DisciplineWrap(null, e, e.getRegistryElementPart().getNumber(), "")).collect(Collectors.toList());
                disciplineList.stream().forEach(e -> e.setEppRegistryElementPart(e.getElectiveDiscipline().getRegistryElementPart()));
            }

            for (DisciplineWrap disc : disciplineList)
            {

                DataWrapper wrapper = new DataWrapper();
                wrapper.setId(GeneratorIds.generateNewId());
                wrapper.set("number", number);
                wrapper.set("student", student);
                wrapper.set("count", count);
                wrapper.set("discipline", disc);
                result.add(wrapper);
            }
        }

        return result;
    }
}