package ru.tandemservice.unitutor.mailprint;

import com.google.common.collect.Lists;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimail.mailprint.BaseMailPrint;
import ru.tandemservice.unimail.mailprint.IMailPrint;
import ru.tandemservice.unitutor.dao.DaoStudentSubscribe;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubcribeBySakai extends BaseMailPrint
{
    private static IMailPrint _IMailPrint = null;

    public static IMailPrint Instanse()
    {
        if (_IMailPrint == null)
            _IMailPrint = (IMailPrint) ApplicationRuntime.getBean("SubcribeBySakai");
        return _IMailPrint;
    }


    @Override
    protected Map<String, Object> getMapParameters(IEntity entity, Object params)
    {
        Long transactionId = (Long) params;
        Student student = (Student) entity;

        Map<String, Object> mapRetVal = new HashMap<>();
        mapRetVal.put("fio", student.getPerson().getIdentityCard().getFullFio());
        if (student.getPerson().isMale())
            mapRetVal.put("genderSuffics", "Уважаемый");
        else
            mapRetVal.put("genderSuffics", "Уважаемая");

        String subscibeName = "";
        List<ElectiveDisciplineSubscribe> subscribed = getElectiveDisciplineAndFillEducationYear(student, transactionId, mapRetVal, true, false);

        if (subscribed != null && !subscribed.isEmpty()) {
            if (subscribed.size() > 1)
                subscibeName = "Вы отправили заявку на выбор дисциплин:";
            else
                subscibeName = "Вы отправили заявку на выбор дисциплины";
        }

        String[][] subcribeInTransaction = getDisciplinetable(subscribed);
        mapRetVal.put("subscibeName", subscibeName);
        mapRetVal.put("subcribeInTransaction", subcribeInTransaction);

        // студент может пойти в отказ, и отписаться
        String unsubscibeName = "";
        List<ElectiveDisciplineSubscribe> unsubscribed = getElectiveDisciplineAndFillEducationYear(student, transactionId, mapRetVal, false, false);

        if (unsubscribed != null && !unsubscribed.isEmpty()) {
            if (unsubscribed.size() > 1)
                unsubscibeName = "Вы отказались от изучения дисциплин:";
            else
                unsubscibeName = "Вы отказались от изучения дисциплины";
        }

        String[][] unsubcribeInTransaction = getDisciplinetable(unsubscribed);
        mapRetVal.put("unsubscibeName", unsubscibeName);
        mapRetVal.put("unsubcribeInTransaction", unsubcribeInTransaction);

        // таблица актуальных подписок
        List<ElectiveDisciplineSubscribe> lstActual = DaoStudentSubscribe.instanse().getSubscribedElectiveDiscipline(Lists.newArrayList(student), null, null, true, null);
        String[][] subcribeActual = getDisciplinetable(lstActual);
        mapRetVal.put("subcribeActual", subcribeActual);

        return mapRetVal;
    }

    protected List<ElectiveDisciplineSubscribe> getElectiveDisciplineAndFillEducationYear(Student student, Long transactionId, Map<String, Object> map,
                                                                             boolean actualRow, boolean sakaiSourseOnly)
    {
        List<ElectiveDisciplineSubscribe> retVal = new ArrayList<>();

        List<ElectiveDisciplineSubscribe> lst = DaoStudentSubscribe.instanse().getSubscribedElectiveDiscipline(Lists.newArrayList(student), null, null, true, transactionId);

        boolean has = false;
        for (ElectiveDisciplineSubscribe eds : lst) {

            if (!has) {
                map.put("educationYearNumber", Integer.toString(eds.getElectiveDiscipline().getEducationYear().getIntValue()));
                map.put("educationYearTitle", eds.getElectiveDiscipline().getEducationYear().getTitle());

                has = true;
            }

            if (sakaiSourseOnly) {
                // нужно только из сакая
                if (!eds.getSourceCode().equals(ElectiveDisciplineSubscribe.SAKAI_SOURCE))
                    continue;
            }

            if (actualRow) {
                if (eds.getRemovalDate() == null)
                    retVal.add(eds);
            }
            else {
                if (eds.getRemovalDate() != null)
                    retVal.add(eds);
            }
        }

        if (!has) {
            map.put("educationYearNumber", "");
            map.put("educationYearTitle", "");
        }
        return retVal;
    }

    /**
     * Таблица с дисциплинами для вывода на печать
     */
    protected String[][] getDisciplinetable(List<ElectiveDisciplineSubscribe> lst)
    {
        List<String[]> list = new ArrayList<>();
        boolean hasRow = false;
        for (ElectiveDisciplineSubscribe eds : lst) {
            String[] row = new String[]
                    {
                            eds.getElectiveDiscipline().getRegistryElementPart().getTitle() + " '" + eds.getElectiveDiscipline().getYearPart().getTitle() + "'"
                    };
            list.add(row);
            hasRow = true;
        }
        if (!hasRow) {
            String[] row = new String[]{};
            list.add(row);
        }
        return list.toArray(new String[][]{});
    }

    @Override
    protected String getTemplateCode()
    {
        return "dpvSakai";
    }

    @Override
    protected Person getPerson(IEntity entity)
    {
        Student student = (Student) entity;
        return student.getPerson();
    }

}
