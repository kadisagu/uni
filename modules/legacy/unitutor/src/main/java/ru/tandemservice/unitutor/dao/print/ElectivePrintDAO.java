package ru.tandemservice.unitutor.dao.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unitutor.entity.ElectiveDiscipline;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.catalog.TutorTemplateDocument;
import ru.tandemservice.unitutor.utils.OptionalDisciplineUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ElectivePrintDAO extends UniBaseDao implements IElectivePrintDAO {

    @Override
    public RtfDocument generateDocument(List<ElectiveDiscipline> electiveDisciplines) {

        // готовим шаблон для страницы
        RtfDocument template = getTemplate().getClone();
        // готовим документ
        RtfDocument document = template.getClone();
        document.getElementList().clear();

        if (!electiveDisciplines.isEmpty()) {
            boolean hasManyPage = false;
            for (ElectiveDiscipline electiveDiscipline : electiveDisciplines) {

                String discipline = electiveDiscipline.getRegistryElementPart().getRegistryElement().getTitle();

                List<ElectiveDisciplineSubscribe> subscribes = OptionalDisciplineUtil.getSubscribes(electiveDiscipline);
                if (subscribes.isEmpty())
                    continue;

                if (hasManyPage)
                    document.addElement(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                RtfDocument groupPage = generatePage(template, subscribes, discipline);
                document.getElementList().addAll(groupPage.getElementList());

                hasManyPage = true;
            }
        }
        return document;
    }

    protected RtfDocument generatePage(RtfDocument template, List<ElectiveDisciplineSubscribe> subscribes, String discipline) {
        RtfDocument page = template.getClone();

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = getTableModidier(subscribes);

        im.put("discipline", discipline);

        im.modify(page);
        tm.modify(page);

        return page;
    }

    private RtfTableModifier getTableModidier(List<ElectiveDisciplineSubscribe> subscribes) {
        RtfTableModifier tm = new RtfTableModifier();
        List<String[]> tableData = new ArrayList<String[]>();
        int counter = 1;
        Collections.sort(subscribes, new Comparator<ElectiveDisciplineSubscribe>() {

            @Override
            public int compare(ElectiveDisciplineSubscribe eds1, ElectiveDisciplineSubscribe eds2) {
                return eds1.getStudent().getPerson().getFullFio().compareTo(eds2.getStudent().getPerson().getFullFio());
            }

        });

        for (ElectiveDisciplineSubscribe subscribe : subscribes) {
            String[] arr = new String[4];
            Student student = subscribe.getStudent();
            Group studentGroup = student.getGroup();
            arr[0] = "" + counter++;
            arr[1] = StringUtils.trimToEmpty(student.getBookNumber());
            arr[2] = student.getPerson().getFullFio();
            if (studentGroup != null)
                arr[3] = StringUtils.trimToEmpty(student.getGroup().getTitle());
            else arr[3] = "";
            tableData.add(arr);
        }

        tm.put("students", tableData.toArray(new String[][]{}));

        return tm;
    }

    private RtfDocument getTemplate() {
        ITemplateDocument template = getCatalogItem(TutorTemplateDocument.class, TutorTemplateDocument.ELECTIVE_SUBSCRIBES_TEMPLATE);
        return new RtfReader().read(template.getContent());
    }

}
