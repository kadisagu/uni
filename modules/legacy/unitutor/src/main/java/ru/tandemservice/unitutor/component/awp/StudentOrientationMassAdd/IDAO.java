package ru.tandemservice.unitutor.component.awp.StudentOrientationMassAdd;


import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitutor.entity.Student2Orientation;

import java.util.Map;

public interface IDAO extends IUniDao<Model> {

    void onMassAdd(Map<Student, Student2Orientation> map, Model model);
}
