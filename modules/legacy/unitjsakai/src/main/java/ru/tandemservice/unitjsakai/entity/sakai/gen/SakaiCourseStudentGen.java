package ru.tandemservice.unitjsakai.entity.sakai.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourseStudent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Студент на курсе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SakaiCourseStudentGen extends EntityBase
 implements INaturalIdentifiable<SakaiCourseStudentGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitjsakai.entity.sakai.SakaiCourseStudent";
    public static final String ENTITY_NAME = "sakaiCourseStudent";
    public static final int VERSION_HASH = -1857428560;
    private static IEntityMeta ENTITY_META;

    public static final String L_SAKAI_COURSE = "sakaiCourse";
    public static final String L_STUDENT = "student";

    private SakaiCourse _sakaiCourse;     // Электронный учебный курс (ЭУК) в системе Sakai
    private Student _student;     // Студент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Sakai. Свойство не может быть null.
     */
    @NotNull
    public SakaiCourse getSakaiCourse()
    {
        return _sakaiCourse;
    }

    /**
     * @param sakaiCourse Электронный учебный курс (ЭУК) в системе Sakai. Свойство не может быть null.
     */
    public void setSakaiCourse(SakaiCourse sakaiCourse)
    {
        dirty(_sakaiCourse, sakaiCourse);
        _sakaiCourse = sakaiCourse;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SakaiCourseStudentGen)
        {
            if (withNaturalIdProperties)
            {
                setSakaiCourse(((SakaiCourseStudent)another).getSakaiCourse());
                setStudent(((SakaiCourseStudent)another).getStudent());
            }
        }
    }

    public INaturalId<SakaiCourseStudentGen> getNaturalId()
    {
        return new NaturalId(getSakaiCourse(), getStudent());
    }

    public static class NaturalId extends NaturalIdBase<SakaiCourseStudentGen>
    {
        private static final String PROXY_NAME = "SakaiCourseStudentNaturalProxy";

        private Long _sakaiCourse;
        private Long _student;

        public NaturalId()
        {}

        public NaturalId(SakaiCourse sakaiCourse, Student student)
        {
            _sakaiCourse = ((IEntity) sakaiCourse).getId();
            _student = ((IEntity) student).getId();
        }

        public Long getSakaiCourse()
        {
            return _sakaiCourse;
        }

        public void setSakaiCourse(Long sakaiCourse)
        {
            _sakaiCourse = sakaiCourse;
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SakaiCourseStudentGen.NaturalId) ) return false;

            SakaiCourseStudentGen.NaturalId that = (NaturalId) o;

            if( !equals(getSakaiCourse(), that.getSakaiCourse()) ) return false;
            if( !equals(getStudent(), that.getStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSakaiCourse());
            result = hashCode(result, getStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSakaiCourse());
            sb.append("/");
            sb.append(getStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SakaiCourseStudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SakaiCourseStudent.class;
        }

        public T newInstance()
        {
            return (T) new SakaiCourseStudent();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sakaiCourse":
                    return obj.getSakaiCourse();
                case "student":
                    return obj.getStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sakaiCourse":
                    obj.setSakaiCourse((SakaiCourse) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sakaiCourse":
                        return true;
                case "student":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sakaiCourse":
                    return true;
                case "student":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sakaiCourse":
                    return SakaiCourse.class;
                case "student":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SakaiCourseStudent> _dslPath = new Path<SakaiCourseStudent>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SakaiCourseStudent");
    }
            

    /**
     * @return Электронный учебный курс (ЭУК) в системе Sakai. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourseStudent#getSakaiCourse()
     */
    public static SakaiCourse.Path<SakaiCourse> sakaiCourse()
    {
        return _dslPath.sakaiCourse();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourseStudent#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    public static class Path<E extends SakaiCourseStudent> extends EntityPath<E>
    {
        private SakaiCourse.Path<SakaiCourse> _sakaiCourse;
        private Student.Path<Student> _student;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Sakai. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourseStudent#getSakaiCourse()
     */
        public SakaiCourse.Path<SakaiCourse> sakaiCourse()
        {
            if(_sakaiCourse == null )
                _sakaiCourse = new SakaiCourse.Path<SakaiCourse>(L_SAKAI_COURSE, this);
            return _sakaiCourse;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourseStudent#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

        public Class getEntityClass()
        {
            return SakaiCourseStudent.class;
        }

        public String getEntityName()
        {
            return "sakaiCourseStudent";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
