package ru.tandemservice.unitjsakai.ws.logic.wrap;

import java.util.ArrayList;
import java.util.List;


/**
 * Академическая группа студента
 */
public class GroupType {

    private String name;
    private List<CourseType> course;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public List<CourseType> getCourse() {
        if (course == null) {
            course = new ArrayList<>();
        }
        return this.course;
    }

}
