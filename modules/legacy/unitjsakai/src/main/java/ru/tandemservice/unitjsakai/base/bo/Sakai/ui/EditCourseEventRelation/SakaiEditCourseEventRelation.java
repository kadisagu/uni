/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.ui.EditCourseEventRelation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unitjsakai.base.bo.Sakai.logic.SakaiCourseDSHandler;


/**
 * @author Ekaterina Zvereva
 * @since 18.12.2015
 */
@Configuration
public class SakaiEditCourseEventRelation extends BusinessComponentManager
{
    public static final String COURSE_DS = "courseDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return new SakaiCourseDSHandler(getName(), true);
    }

}