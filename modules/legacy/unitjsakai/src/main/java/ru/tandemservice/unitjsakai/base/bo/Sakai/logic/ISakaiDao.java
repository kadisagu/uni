/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.logic;

import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel;
import ru.tandemservice.unitjsakai.ws.logic.tutor.wrap.TutorSiteWrap;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;


import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 13.12.2015
 */
public interface ISakaiDao
{
    void saveCourseForPps(PpsEntry tutor, TrJournal journal, SakaiCourse course, String description, boolean isNew);

    void saveSakaiEventRelation(PpsEntry tutor, SakaiTrJournalEventRel eventRelation, SakaiCourse course, TrJournal journal);

    void addNewStudentsToCoursesSakai(String sessionId, TrJournal journal, SakaiCourse course);

    void getGradesFromSakai(SakaiCourse2PpsEntryRel relation);

    List<TutorSiteWrap> prepareSakaiCoursesList(PpsEntry tutor, TrJournal journal);
}