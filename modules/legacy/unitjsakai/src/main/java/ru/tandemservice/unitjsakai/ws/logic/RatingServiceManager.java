/* $Id$ */
package ru.tandemservice.unitjsakai.ws.logic;

import com.google.common.primitives.Longs;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.brs.util.BrsIRatingValueFormatter;
import ru.tandemservice.unitjsakai.ws.logic.wrap.*;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent;
import ru.tandemservice.unitraining.base.entity.journal.gen.TrJournalGroupStudentGen;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 07.12.2015
 */
public class RatingServiceManager extends UniBaseDao implements IRatingServiceManager
{
    public static final String MARK_COLUMN_KEY = "mark"; // Текстовая оценка
    public static final String MAX_POINTS_CODE = "max_points";
    public static final String GROUP_TITLE_WITHOUT_GROUP = "Без группы";
    public static final String DELETED_MARK_SIGN = "-";


    @Override
    public String getStudentRatingForSakai(String studentId)
    {
        Long stId = Longs.tryParse(studentId);
        Student student = (stId == null) ? null : DataAccessServices.dao().get(Student.class, stId);

        if (student == null)
            return null;

        List<TrJournalGroupStudent> journals = prepareStudentTrJournal(student);
        StudentType studentPortal = prepareStudentData(student, journals);
        studentPortal.setName(student.getPerson().getFullFio());

        return new RatingReportBuilder().studentTypeToString(studentPortal);
    }

    private List<TrJournalGroupStudent> prepareStudentTrJournal(Student student)
    {
        DQLSelectBuilder stuSlotDQL = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "e")
                .where(eq(property("e", TrJournalGroupStudent.studentWpe().student()), value(student)))
                .where(eq(property(TrJournalGroupStudent.group().journal().yearPart().year().educationYear().current().fromAlias("e")), value(Boolean.TRUE)));
        return getList(stuSlotDQL);

    }

    private String getRightValue(ISessionBrsDao.IRatingValue value, String measureUnit)
    {
        if (null == value) return null;
        return null != value.getMessage() ? value.getMessage() : (DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(value.getValue()) + (null != measureUnit ? measureUnit : ""));
    }


    private StudentType prepareStudentData(Student student, List<TrJournalGroupStudent> journals)
    {
        Collections.sort(journals, new JournalsComparator());
        // Кэш дополнительных атрибутов журнала. Ключ - TrJournal.id, значение - мап (ключ - код параметра) всех дополнительных атрибутов журнала
        Map<Long, Map<String, ISessionBrsDao.IRatingAdditParamDef>> additParamDefMap = new HashMap<>();
        // Подготавливаем Мап с фио преподавателей для групп журнала. Ключ TrJournalGroup.id, значение - ФИО преподавателей через запятую
        Map<Long, String> tutorsMap = prepareTutorsMap(journals.stream().map(TrJournalGroupStudentGen::getGroup).collect(Collectors.toList()));

        // Кэш для рейтингов. Ключ - TrJournal.id, значение - рейтинг студентов по журналу
        Map<Long, IBrsDao.ICurrentRatingCalc> ratingMap = new HashMap<>();

        // Подготавливаем Мап событий студента. Ключ - EppStudentWorkPlanElement.id, событие студента
        Map<Long, List<TrEduGroupEventStudent>> studentToEventsMap = prepareStudentEventsMap(student);

        // Подготавливаем Мап коэффициентов БРС для КМ. Ключ - TrJournalEvent.id, значение - список коэффициентов БРС для КМ журнала
        Map<Long, List<TrBrsCoefficientValue>> brsCoeffsMap = prepareJournalEventBrsCoefficients(journals.stream().map(e -> e.getGroup().getJournal()).collect(Collectors.toList()));

        // Подготавливаем Мап оценок. Ключ - пара(EppStudentWorkPlanElement.id, TrEduGroupEvent.id), значение - пара(оценка, дата оценки)
        Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Date>> markActualMap = prepareMarkActualMap(student);

        StudentType studentPortal = new StudentType();
        studentPortal.setRatingtitle("Текущий рейтинг студента на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        studentPortal.setGroups(new GroupsType());

        GroupType groupPortal = createStudentType(student);
        studentPortal.getGroups().getGroup().add(groupPortal);

        for (TrJournalGroupStudent journalStudent : journals)
        {
            EppStudentWorkPlanElement slot = journalStudent.getStudentWpe();
            TrJournalGroup group = journalStudent.getGroup();
            TrJournal journal = group.getJournal();
            // Получаем закэшированный набор дополнительных атрибутов журнала, либо поднимаем его из базы и кэшируем
            Map<String, ISessionBrsDao.IRatingAdditParamDef> journalAdditParamDefMap = additParamDefMap.get(journal.getId());
            if (null == journalAdditParamDefMap)
            {
                try
                {
                    journalAdditParamDefMap = TrBrsCoefficientManager.instance().brsDao().getRatingAdditionalParamDefinitions(journal);
                    additParamDefMap.put(journal.getId(), journalAdditParamDefMap);
                }
                catch (Exception e)
                {
                    e.printStackTrace(System.err);
                }
            }


            CourseType coursePortal = new CourseType(journal, slot);

            if (!groupPortal.getCourse().contains(coursePortal))
            {
                coursePortal.setName(journal.getRegistryElementPart().getRegistryElement().getTitle());
                if (null != tutorsMap.get(group.getId()))
                    coursePortal.setTeacherName(tutorsMap.get(group.getId()));
                else
                    coursePortal.setTeacherName("");

                // Получаем закэшированный рейтинг студентов по журналу, либо поднимаем его из базы и кэшируем
                IBrsDao.ICurrentRatingCalc rating = ratingMap.get(journal.getId());
                if (null == rating)
                {
                    try
                    {
                        rating = TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(journal);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace(System.err);
                    }
                }

                ratingMap.put(journal.getId(), rating);

                if (null != rating)
                {
                    IBrsDao.IStudentCurrentRatingData studentRating = rating.getCurrentRating(slot);
                    ISessionBrsDao.IRatingValue ratValue = studentRating.getRatingValue();
                    if (null != ratValue)
                    {
                        coursePortal.setRatingFinal(null != ratValue.getMessage() ? ratValue.getMessage() : (BrsIRatingValueFormatter.instance.format(ratValue) + "%"));
                    }

                    coursePortal.setResult(getRightValue(studentRating.getRatingAdditParam(MARK_COLUMN_KEY), null));
                }


                groupPortal.getCourse().add(coursePortal);

                if (studentToEventsMap.containsKey(slot.getId()))
                {
                    Collections.sort(studentToEventsMap.get(slot.getId()), (o1, o2) -> {
                        ScheduleEvent e1 = o1.getEvent().getScheduleEvent();
                        ScheduleEvent e2 = o2.getEvent().getScheduleEvent();
                        if (e1 == null && e2 == null) return 0;
                        if (e1 != null && e2 != null) return e1.getDurationBegin().compareTo(e2.getDurationBegin());
                        if (e1 != null) return 1;
                        else return -1;
                    });

                    for (TrEduGroupEventStudent eventStud : studentToEventsMap.get(slot.getId()))
                    {
                        TaskType taskPortal = new TaskType();
                        taskPortal.setName(eventStud.getEvent().getJournalEvent().getTheme());
                        taskPortal.setTypeName(eventStud.getEvent().getJournalEvent().getType().getTitle());

                        Date dueDate = null != eventStud.getEvent().getScheduleEvent() ? eventStud.getEvent().getScheduleEvent().getDurationBegin() : null;
                        taskPortal.setDue(DateFormatter.DEFAULT_DATE_FORMATTER.format(dueDate));

                        List<TrBrsCoefficientValue> coefficientValueList = brsCoeffsMap.get(eventStud.getEvent().getJournalEvent().getId());
                        if (null != coefficientValueList)
                        {
                            for (TrBrsCoefficientValue brsCoefficientValue : coefficientValueList)
                            {
                                if (MAX_POINTS_CODE.equals(brsCoefficientValue.getDefinition().getUserCode()))
                                    taskPortal.setMaxPoints(brsCoefficientValue.getValueStr());
                            }
                        }

                        CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(slot.getId(), eventStud.getEvent().getId());
                        CoreCollectionUtils.Pair<Double, Date> actualMark = markActualMap.get(key);

                        if (actualMark != null)
                        {
                            MarkType markPortal = new MarkType();
                            if (actualMark.getX() != null)
                            {
                                //заполняем поле оценки и дату оценки
                                markPortal.setValue(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(actualMark.getX()));
                                markPortal.setMdate(DateFormatter.DEFAULT_DATE_FORMATTER.format(actualMark.getY()));
                            }
                            else
                            {
                                // если есть история но нет оценки ставим "-"
                                markPortal.setValue(DELETED_MARK_SIGN);
                                // дату не ставим
                                markPortal.setMdate(null);
                            }
                            taskPortal.setMark(markPortal);
                        }

                        coursePortal.getTask().add(taskPortal);
                    }
                }
            }

        }
        return studentPortal;
    }


    private Map<Long, List<TrEduGroupEventStudent>> prepareStudentEventsMap(Student student)
    {
        DQLSelectBuilder eventDQL = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEventStudent.class, "event").column("event")
                .where(eq(property("event", TrEduGroupEventStudent.studentWpe().student()), value(student)))
                .where(eq(property("event", TrEduGroupEventStudent.event().journalEvent().journalModule().journal().yearPart().year().educationYear().current()), value(Boolean.TRUE)));
        final List<TrEduGroupEventStudent> events = IUniBaseDao.instance.get().getList(eventDQL);
        Map<Long, List<TrEduGroupEventStudent>> studentToEventsMap = new HashMap<>();
        for (TrEduGroupEventStudent event : events)
        {
            Long studentSlotId = event.getStudentWpe().getId();
            List<TrEduGroupEventStudent> studEventsList = studentToEventsMap.get(studentSlotId);
            if (null == studEventsList) studEventsList = new ArrayList<>();
            studEventsList.add(event);
            studentToEventsMap.put(studentSlotId, studEventsList);
        }

        return studentToEventsMap;
    }

    private Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Date>> prepareMarkActualMap(Student student)
    {
        // Получение наборов слот в журнале-событие-оценка-дата_оценки
        // Берутся только те оценки, у которых есть история.
        // Дата оценки берется из последней (по дате) записи в истории

        DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "m")
                        .where(eq(property("m", TrEduGroupEventStudent.studentWpe().student()), value(student)))
                        .where(eq(property("m", TrEduGroupEventStudent.event().journalEvent().journalModule().journal().yearPart().year().educationYear().current()), value(Boolean.TRUE)))

        );

        int eventColumnIdx = dql.column(property("m", TrEduGroupEventStudent.event().id()));
        int slotColumnIdx = dql.column(property("m", TrEduGroupEventStudent.studentWpe().id()));
        int markColumnIdx = dql.column(property("m", TrEduGroupEventStudent.P_GRADE_AS_LONG));
        int markDateColumnInd = dql.column(property("m", TrEduGroupEventStudent.event().scheduleEvent().durationBegin()));


        Map<CoreCollectionUtils.Pair<Long, Long>, CoreCollectionUtils.Pair<Double, Date>> markActualMap = new HashMap<>();

        Iterable<Object[]> rows = scrollRows(dql.getDql().createStatement(getSession()));
        for (Object[] item : rows)
        {
            Long eventId = (Long) item[eventColumnIdx];
            Long slotId = (Long) item[slotColumnIdx];
            Long markAsLong = (Long) item[markColumnIdx];
            Double mark = (markAsLong != null) ? (markAsLong * 0.01d) : null;
            Date markDate = (Date) item[markDateColumnInd];

            CoreCollectionUtils.Pair<Long, Long> key = new CoreCollectionUtils.Pair<>(slotId, eventId);
            CoreCollectionUtils.Pair<Double, Date> value = new CoreCollectionUtils.Pair<>(mark, markDate);

            markActualMap.put(key, value);
        }
        return markActualMap;
    }

    private Map<Long, List<TrBrsCoefficientValue>> prepareJournalEventBrsCoefficients(List<TrJournal> journals)
    {
        // Поднимаем коэффициенты для КМ в рамках журнала
        List<TrBrsCoefficientValue> brsCoeffsList = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "e").column("e")
                .joinEntity("e", DQLJoinType.inner, TrJournalEvent.class, "ev", eq(property(TrBrsCoefficientValue.owner().id().fromAlias("e")), property(TrJournalEvent.id().fromAlias("ev"))))
                .where(in(property("ev", TrJournalEvent.journalModule().journal()), journals))
                .createStatement(getSession()).list();

        Map<Long, List<TrBrsCoefficientValue>> brsCoeffsMap = new HashMap<>();

        for (TrBrsCoefficientValue brsCoefficientValue : brsCoeffsList)
        {
            Long ownerId = brsCoefficientValue.getOwner().getId();
            List<TrBrsCoefficientValue> ownerBrsCoeffsList = brsCoeffsMap.get(ownerId);
            if (null == ownerBrsCoeffsList) ownerBrsCoeffsList = new ArrayList<>();
            ownerBrsCoeffsList.add(brsCoefficientValue);
            brsCoeffsMap.put(ownerId, ownerBrsCoeffsList);
        }

        return brsCoeffsMap;
    }


    private static GroupType createStudentType(Student student)
    {
        GroupType groupType = new GroupType();
        groupType.setName(getGroupTitle(student));
        return groupType;
    }

    private static String getGroupTitle(Student student)
    {
        return (null != student.getGroup() ? student.getGroup().getTitle() : GROUP_TITLE_WITHOUT_GROUP);
    }


    private final class JournalsComparator implements Comparator<TrJournalGroupStudent>
    {

        @Override
        public int compare(TrJournalGroupStudent o1, TrJournalGroupStudent o2)
        {

            final EppWorkPlanRow sourceRow1 = o1.getStudentWpe().getSourceRow();
            final EppWorkPlanRow sourceRow2 = o2.getStudentWpe().getSourceRow();
            final int term1 = sourceRow1 == null ? 0 : sourceRow1.getWorkPlan().getWorkPlan().getTerm().getIntValue();
            final int term2 = sourceRow2 == null ? 0 : sourceRow2.getWorkPlan().getWorkPlan().getTerm().getIntValue();
            if (term1 != term2)
                return Integer.compare(term2, term1);

            String title1 = o1.getGroup().getJournal().getRegistryElementPart().getRegistryElement().getTitle();
            String title2 = o2.getGroup().getJournal().getRegistryElementPart().getRegistryElement().getTitle();
            return title1.compareTo(title2);
        }
    }

    private Map<Long, String> prepareTutorsMap(List<TrJournalGroup> journalGroups)
    {
        DQLSelectBuilder pps = new DQLSelectBuilder()
                .fromEntity(EppPpsCollectionItem.class, "rel")
                .predicate(DQLPredicateType.distinct)
                .joinEntity("rel", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "grp", eq(property(EppPpsCollectionItem.list().fromAlias("rel")), property("grp")))
                .joinEntity("grp", DQLJoinType.inner, TrJournalGroup.class, "jRel", eq(property(TrJournalGroup.group().fromAlias("jRel")), property("grp")))
                .where(eq(property(TrJournalGroup.journal().yearPart().year().educationYear().current().fromAlias("jRel")), value(Boolean.TRUE)))
                .where(in(property("jRel"), journalGroups))
                .order(property(TrJournalGroup.id().fromAlias("jRel")))
                .order(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("rel")));

        DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(pps);

        int idIdx = dql.column(property(TrJournalGroup.id().fromAlias("jRel")));
        int fioIdx = dql.column(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("rel")));

        Map<Long, String> tutorsMap = new HashMap<>();

        for (Object[] item : scrollRows(dql.getDql().createStatement(getSession())))
        {
            Long grpId = (Long) item[idIdx];
            String tutors = tutorsMap.get(grpId);
            if (null == tutors) tutors = (String) item[fioIdx];
            else tutors += ", " + item[fioIdx];
            tutorsMap.put(grpId, tutors);
        }
        return tutorsMap;
    }
}