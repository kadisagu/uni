package ru.tandemservice.unitjsakai.ws.logic.wrap;


/**
 * Данные об оценке по контрольному мероприятию
 */
public class MarkType
{

    private String mdate;
    private String value;

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String value) {
        this.mdate = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
