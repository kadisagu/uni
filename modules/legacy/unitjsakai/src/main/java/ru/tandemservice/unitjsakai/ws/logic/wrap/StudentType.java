/* $Id$ */
package ru.tandemservice.unitjsakai.ws.logic.wrap;



/**
 * @author Ekaterina Zvereva
 * @since 03.12.2015
 */
public class StudentType
{

    private String ratingtitle;
    private String name;
    private GroupsType groups;


    public String getRatingtitle() {
        return ratingtitle;
    }

    public void setRatingtitle(String value) {
        this.ratingtitle = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public GroupsType getGroups() {
        return groups;
    }

    public void setGroups(GroupsType value) {
        this.groups = value;
    }

}