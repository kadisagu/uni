package ru.tandemservice.unitjsakai.entity.sakai.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь КМ из структуры чтения с мероприятием ЭУК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SakaiTrJournalEventRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel";
    public static final String ENTITY_NAME = "sakaiTrJournalEventRel";
    public static final int VERSION_HASH = 279349272;
    private static IEntityMeta ENTITY_META;

    public static final String L_EVENT = "event";
    public static final String L_COURSE_RELATION = "courseRelation";
    public static final String P_SAKAI_EVENT_ID = "sakaiEventId";
    public static final String P_SAKAI_EVENT_NAME = "sakaiEventName";

    private TrJournalEvent _event;     // Мероприятие в структуре чтения реализации дисциплины
    private SakaiCourse2PpsEntryRel _courseRelation;     // Связь ЭУК с преподавателем
    private String _sakaiEventId;     // Идентификатор мероприятия ЭУК
    private String _sakaiEventName;     // Название мероприятия ЭУК

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Мероприятие в структуре чтения реализации дисциплины. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public TrJournalEvent getEvent()
    {
        return _event;
    }

    /**
     * @param event Мероприятие в структуре чтения реализации дисциплины. Свойство не может быть null и должно быть уникальным.
     */
    public void setEvent(TrJournalEvent event)
    {
        dirty(_event, event);
        _event = event;
    }

    /**
     * @return Связь ЭУК с преподавателем. Свойство не может быть null.
     */
    @NotNull
    public SakaiCourse2PpsEntryRel getCourseRelation()
    {
        return _courseRelation;
    }

    /**
     * @param courseRelation Связь ЭУК с преподавателем. Свойство не может быть null.
     */
    public void setCourseRelation(SakaiCourse2PpsEntryRel courseRelation)
    {
        dirty(_courseRelation, courseRelation);
        _courseRelation = courseRelation;
    }

    /**
     * @return Идентификатор мероприятия ЭУК. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSakaiEventId()
    {
        return _sakaiEventId;
    }

    /**
     * @param sakaiEventId Идентификатор мероприятия ЭУК. Свойство не может быть null.
     */
    public void setSakaiEventId(String sakaiEventId)
    {
        dirty(_sakaiEventId, sakaiEventId);
        _sakaiEventId = sakaiEventId;
    }

    /**
     * @return Название мероприятия ЭУК. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSakaiEventName()
    {
        return _sakaiEventName;
    }

    /**
     * @param sakaiEventName Название мероприятия ЭУК. Свойство не может быть null.
     */
    public void setSakaiEventName(String sakaiEventName)
    {
        dirty(_sakaiEventName, sakaiEventName);
        _sakaiEventName = sakaiEventName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SakaiTrJournalEventRelGen)
        {
            setEvent(((SakaiTrJournalEventRel)another).getEvent());
            setCourseRelation(((SakaiTrJournalEventRel)another).getCourseRelation());
            setSakaiEventId(((SakaiTrJournalEventRel)another).getSakaiEventId());
            setSakaiEventName(((SakaiTrJournalEventRel)another).getSakaiEventName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SakaiTrJournalEventRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SakaiTrJournalEventRel.class;
        }

        public T newInstance()
        {
            return (T) new SakaiTrJournalEventRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "event":
                    return obj.getEvent();
                case "courseRelation":
                    return obj.getCourseRelation();
                case "sakaiEventId":
                    return obj.getSakaiEventId();
                case "sakaiEventName":
                    return obj.getSakaiEventName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "event":
                    obj.setEvent((TrJournalEvent) value);
                    return;
                case "courseRelation":
                    obj.setCourseRelation((SakaiCourse2PpsEntryRel) value);
                    return;
                case "sakaiEventId":
                    obj.setSakaiEventId((String) value);
                    return;
                case "sakaiEventName":
                    obj.setSakaiEventName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "event":
                        return true;
                case "courseRelation":
                        return true;
                case "sakaiEventId":
                        return true;
                case "sakaiEventName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "event":
                    return true;
                case "courseRelation":
                    return true;
                case "sakaiEventId":
                    return true;
                case "sakaiEventName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "event":
                    return TrJournalEvent.class;
                case "courseRelation":
                    return SakaiCourse2PpsEntryRel.class;
                case "sakaiEventId":
                    return String.class;
                case "sakaiEventName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SakaiTrJournalEventRel> _dslPath = new Path<SakaiTrJournalEventRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SakaiTrJournalEventRel");
    }
            

    /**
     * @return Мероприятие в структуре чтения реализации дисциплины. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel#getEvent()
     */
    public static TrJournalEvent.Path<TrJournalEvent> event()
    {
        return _dslPath.event();
    }

    /**
     * @return Связь ЭУК с преподавателем. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel#getCourseRelation()
     */
    public static SakaiCourse2PpsEntryRel.Path<SakaiCourse2PpsEntryRel> courseRelation()
    {
        return _dslPath.courseRelation();
    }

    /**
     * @return Идентификатор мероприятия ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel#getSakaiEventId()
     */
    public static PropertyPath<String> sakaiEventId()
    {
        return _dslPath.sakaiEventId();
    }

    /**
     * @return Название мероприятия ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel#getSakaiEventName()
     */
    public static PropertyPath<String> sakaiEventName()
    {
        return _dslPath.sakaiEventName();
    }

    public static class Path<E extends SakaiTrJournalEventRel> extends EntityPath<E>
    {
        private TrJournalEvent.Path<TrJournalEvent> _event;
        private SakaiCourse2PpsEntryRel.Path<SakaiCourse2PpsEntryRel> _courseRelation;
        private PropertyPath<String> _sakaiEventId;
        private PropertyPath<String> _sakaiEventName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Мероприятие в структуре чтения реализации дисциплины. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel#getEvent()
     */
        public TrJournalEvent.Path<TrJournalEvent> event()
        {
            if(_event == null )
                _event = new TrJournalEvent.Path<TrJournalEvent>(L_EVENT, this);
            return _event;
        }

    /**
     * @return Связь ЭУК с преподавателем. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel#getCourseRelation()
     */
        public SakaiCourse2PpsEntryRel.Path<SakaiCourse2PpsEntryRel> courseRelation()
        {
            if(_courseRelation == null )
                _courseRelation = new SakaiCourse2PpsEntryRel.Path<SakaiCourse2PpsEntryRel>(L_COURSE_RELATION, this);
            return _courseRelation;
        }

    /**
     * @return Идентификатор мероприятия ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel#getSakaiEventId()
     */
        public PropertyPath<String> sakaiEventId()
        {
            if(_sakaiEventId == null )
                _sakaiEventId = new PropertyPath<String>(SakaiTrJournalEventRelGen.P_SAKAI_EVENT_ID, this);
            return _sakaiEventId;
        }

    /**
     * @return Название мероприятия ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel#getSakaiEventName()
     */
        public PropertyPath<String> sakaiEventName()
        {
            if(_sakaiEventName == null )
                _sakaiEventName = new PropertyPath<String>(SakaiTrJournalEventRelGen.P_SAKAI_EVENT_NAME, this);
            return _sakaiEventName;
        }

        public Class getEntityClass()
        {
            return SakaiTrJournalEventRel.class;
        }

        public String getEntityName()
        {
            return "sakaiTrJournalEventRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
