/* $Id$ */
package ru.tandemservice.unitjsakai.ws.logic;

import com.google.common.primitives.Longs;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.OrderDataGen;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unitjsakai.ws.wrappers.WsOrderWrap;
import ru.tandemservice.unitjsakai.ws.wrappers.WsStudentWrap;


import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 01.12.2015
 */
public class SakaiServiceManager extends UniBaseDao implements ISakaiServiceManager
{


    @Override
    public List<WsStudentWrap> getStudentListByLogin(String login)
    {
        if (login == null || login.isEmpty())
            throw new ApplicationException("Не указан параметр поиска студента");

        // ищем по логину
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "st")
                .where(eq(property("st", Student.principal().login()), value(login)))
                .where(eq(property("st", Student.archival()), value(false)));
        List<Student> result = getList(builder);

        if (result.isEmpty())
            throw new ApplicationException("По параметру studentParam = " + login + " студент не найден");
        List<WsStudentWrap> resultList = new ArrayList<>(result.size());
        for (Student student : result)
        {
            WsStudentWrap wrap = new WsStudentWrap(student);
            resultList.add(wrap);

        }

        return resultList;
    }

    @Override
    public String getReportHTML(String studentId)
    {
        // проверяем наличие студента
        Long stId = Longs.tryParse(studentId);
        Student student = (stId == null)? null : DataAccessServices.dao().get(Student.class, stId);
        if (student == null)
            return getErrorInfo("В системе TU студент не найден", null);
        return createReportHTML(student);
    }

    private String createReportHTML(Student student)
    {
        // нужно получить таблицу (2-х мерный массив строк)
        String[][] rows = getTableRows(student);

        // все в шаблон
        String[] headers = _getTableHeader();
        return _fillFakeTable(student, rows, headers);
    }


    private String[] _getTableHeader()
    {
        return new String[]{"Год", "Семестр", "Дисциплина", "Форма контроля", "Оценка"};
    }

    private String _fillFakeTable(Student student, String[][] rows, String[] header)
    {
        String head = "<table class='listHier' cellspacing='0' cellpadding='0'>";
        String footer = "</table>";

        StringBuilder retVal = new StringBuilder()
                .append(head);

        // заголовок
        String main;

        StringBuilder strBuilder = new StringBuilder("Студент: ");
        strBuilder.append(student.getPerson().getFullFio());
        if (null != student.getGroup())
            strBuilder.append(" | Группа: ").append(student.getGroup().getTitle());

        main = strBuilder.toString();

        retVal.append("<tr><th colspan='")
                .append(header.length).append("'>")
                .append(main)
                .append("</th></tr>");
        retVal.append(_fillTableHeader(header));

        for (String[] str : rows) {
            // это строки
            retVal.append("<tr>");

            // внутри столбцы
            for (String s : str) {
                if (s.toLowerCase().contains("<td"))
                    retVal.append(s);
                else
                    retVal.append("<td>").append(s).append("</td>");
            }
            retVal.append("</tr>");
        }
        retVal.append(footer);
        return retVal.toString();
    }


    /**
     * Формируем заголовок таблицы
     */
    private String _fillTableHeader(String[] header)
    {
        StringBuilder retVal = new StringBuilder();

        retVal.append("<tr>");
        for (String s : header) {
            retVal.append("<th>")
                    .append(s)
                    .append("</th>");

        }
        retVal.append("</tr>");
        return retVal.toString();
    }

    /**
     * получим массив строк
     */
    private String[][] getTableRows(Student student)
    {

        List<String[]> lines = new ArrayList<>();

        //список мероприятий студента из РУП
        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, "eppSlot")
                .column("eppSlot")

                        // учебный год
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().year().educationYear().fromAlias("eppSlot")
                        , "eduyear")
                        // часть года
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().part().fromAlias("eppSlot")
                        , "part")
                        // тип КМ
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.type().fromAlias("eppSlot")
                        , "type")
                        // тип дисциплина
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().fromAlias("eppSlot")
                        , "regelem")
                .where(eq(property(EppStudentWpeCAction.studentWpe().student().fromAlias("eppSlot")), value(student)))
                .where(isNull(EppStudentWpeCAction.removalDate().fromAlias("eppSlot")))

                        // сортировки
                .order(property(EducationYear.intValue().fromAlias("eduyear")))
                .order(property(YearDistributionPart.number().fromAlias("part")))
                .order(property(EppFControlActionType.priority().fromAlias("type")))
                .order(property(EppRegistryElement.title().fromAlias("regelem")));
        List<EppStudentWpeCAction> actionList = getList(dql);

        for (EppStudentWpeCAction a : actionList) {
            // на основе слотов получаем оценки
            SessionMark totMark = null;
//            if (!(a.getStudentWpe().getRegistryElementPart().getRegistryElement() instanceof EppRegistryDiscipline))
//                continue;

            //Берем итоговую оценку
            final DQLSelectBuilder markDql = new DQLSelectBuilder()
                    .fromEntity(SessionMark.class, "mark")
                    .column("mark")
                    .fetchPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias("mark"), "document")
//                    .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                    .where(eq(property("mark", SessionMark.slot().studentWpeCAction()), value(a)))
                    .order(property("mark", SessionMark.id()));
            List<SessionMark> markList = getList(markDql);
            for (SessionMark mark : markList) {
                if (mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument && !mark.isInSession()) {
                    // итоговые оценки - в зачетке
//                    System.out.println(mark.getValueTitle());
                    totMark = mark;
                }
            }

            List<String> lst = _getReportRow(a, totMark);
            lines.add(lst.toArray(new String[lst.size()]));
        }

        return lines.toArray(new String[][]{});
    }

    private List<String> _getReportRow(EppStudentWpeCAction slot,
                                       SessionMark totMark)
    {
        List<String> retVal = new ArrayList<>();

        // год + семестр
//        String yt = slot.getStudentWpe().getYear().getEducationYear().getTitle() + " " + slot.getStudentWpe().getPart().getShortTitle();
        retVal.add(slot.getStudentWpe().getYear().getEducationYear().getTitle());
        retVal.add(slot.getStudentWpe().getPart().getShortTitle());

        // дисциплина
        retVal.add(slot.getStudentWpe().getRegistryElementPart().getRegistryElement().getTitle());

        // форма контроля
        retVal.add(slot.getType().getTitle());

        // итоговая оценка
        if (totMark == null)
            retVal.add("-");
        else {
            boolean isPositive = totMark.getValueItem().isCachedPositiveStatus();


            if (!isPositive) {
                // покрасим (но как?)
                retVal.add("<td class='colored'>" + totMark.getValueShortTitle() + "</td>");
            }
            else
                retVal.add(totMark.getValueShortTitle());
        }
        return retVal;
    }


    private String getErrorInfo(String string, Exception ex) {

        String retVal = "";

        retVal += "<table border='0' cellspacing='0' >";
        retVal += "<tr><td>";

        retVal += "<center><h3 class='red'>" + string + "</h3></center>";

        if (ex != null) {
            retVal += "<tr><td>";
            retVal += "<h3>" + ex.getMessage() + "</h2>";
            retVal += "</td></tr>";
        }
        retVal += "</td></tr>";
        retVal += "</table border>";

        return retVal;
    }


    @Override
    public List<WsOrderWrap> getWsOrders(String studentId) {
        Long stId = Longs.tryParse(studentId);
        Student student = (stId == null)? null : DataAccessServices.dao().get(Student.class, stId);

        if (student == null)
            return new ArrayList<>();

        List<WsOrderWrap> orderWrapList = getWrapOrderList(student);

        Collections.sort(orderWrapList, (wrap1, wrap2) -> {
            try {
                return wrap2.getOrderDate().compareTo(wrap1.getOrderDate());
            }
            catch (Exception e) {

            }

            return 0;
        });

        return orderWrapList;
    }

    private List<WsOrderWrap> getWrapOrderList(Student student) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "ext")
                .where(eq(property("ext", AbstractStudentExtract.entity()), value(student)))
                .where(eq(property("ext", AbstractStudentExtract.state().code()), value(ExtractStatesCodes.FINISHED)))
                .column("ext");

        List<AbstractStudentExtract> rowList = builder.createStatement(getSession()).list();
        List<WsOrderWrap> resultList = new ArrayList<>();

        for (AbstractStudentExtract extract : rowList) {
            AbstractStudentOrder order = extract.getParagraph().getOrder();
            WsOrderWrap wrap = new WsOrderWrap(extract.getType().getTitle(), order);
            resultList.add(wrap);
        }

        OrderData orderData = getByNaturalId(new OrderDataGen.NaturalId(student));
        if (orderData != null)
            resultList.add(new WsOrderWrap("Приказ о зачислении", orderData.getEduEnrollmentOrderNumber(), orderData.getEduEnrollmentOrderDate(),
                                           orderData.getEduEnrollmentOrderEnrDate()));

        return resultList;
    }


//    protected List<Student> getStudentFromSakai(String studentParam)
//    {
//        if (studentParam == null || studentParam.isEmpty())
//            throw new ApplicationException("Не указан параметр поиска студента");
//
//        List<Student> result;
////        if (isLdap()) {
//            // ищем по логину
//            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "st")
//                    .where(eq(property("st", Student.principal().login()), value(studentParam)))
//                    .where(eq(property("st", Student.archival()), value(false)));
//            result = getList(builder);
////        }
////        else {
////            int personalNumber = 0;
////            try {
////                // из сакая может прийти "st123"
////                personalNumber = Integer.parseInt(studentParam.replace(STUDENT_PREF, ""));
////            }
////            catch (Exception ex) {
////                throw new ApplicationException("studentParam=" + studentParam + " нельзя преобразовать к целому числу, ошибка поиска студента");
////            }
//
//            // передан персональный номер
////            result = UniDaoFacade.getCoreDao().getList(Student.class, Student.personalNumber(), studentParam);
////        }
//
//        if (result.isEmpty())
//            throw new ApplicationException("По параметру studentParam = " + studentParam + " студент не найден");
//
//        return result;
//    }


    /**
     * Проверка аутентификации по ЛДАП
     */
//    private boolean isLdap()
//    {
//        // смотрим на настройки Сакая uniSakaiSettings_t
//        if (EntityRuntime.getMeta("ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings") != null) {
//            MQBuilder builder = new MQBuilder("ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings", "e");
//            builder.add(MQExpression.eq("e", "stringKey", "unisakai.useLDAP"));
//            builder.addSelect("e", new Object[]{"stringValue"});
//            List<String> lst = builder.getResultList(getSession());
//
//            if (lst == null || lst.isEmpty()) {
//                return false;
//            }
//            else {
//                String val = lst.get(0);
//                if (val == null)
//                    return false;
//                else {
//                    val = val.toLowerCase();
//                    return  (val.equals("true"));
//                }
//            }
//
//        }
//        else {
//            // модуль сакая не подключен (почему-то)
//            throw new ApplicationException("Не найдены настройки интеграции с Сакай, возможно модуль сакая не подключен");
//        }
//
//    }
}