/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.logic;

import com.google.common.primitives.Doubles;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourseStudent;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel;
import ru.tandemservice.unitjsakai.entity.sakai.gen.SakaiCourse2PpsEntryRelGen;
import ru.tandemservice.unitjsakai.ws.SakaiTutorGetter;
import ru.tandemservice.unitjsakai.ws.logic.tutor.wrap.TutorSiteWrap;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroupStudent;


import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 13.12.2015
 */
public class SakaiDao extends UniBaseDao implements ISakaiDao
{
    @Override
    public void saveCourseForPps(PpsEntry tutor, TrJournal journal, SakaiCourse course, String description, boolean isNew)
    {
        /**
         * Если курс новый - надо создать его в Сакай
          */
        String sessionId = null;
        if (isNew)
        {
            try
            {
                sessionId = SakaiTutorGetter.loginToSakay();
                String result = SakaiTutorGetter.getTutorService().getSakaiTutor()
                        .createNewSiteWithTutor(sessionId, tutor.getPrincipal().getLogin(), course.getSakaiCourseId(), course.getTitle(), description);
                if (!"success".equals(result))
                    throw new ApplicationException("Не удалось создать сайт в системе Сакай.");
            }
            catch (Exception e)
            {
                e.printStackTrace();
                throw new ApplicationException("Не удалось создать сайт в системе Сакай.");
            }

        }

        /**
         * Сохранить ЭУК, SakaiCourseToPps
         */
        SakaiCourse site = get(SakaiCourse.class, SakaiCourse.P_SAKAI_COURSE_ID, course.getSakaiCourseId());
        if (site == null)
            save(course);
        else
        {
            site.update(course);
            update(site);
            course = site;
        }

        SakaiCourse2PpsEntryRel relation = new SakaiCourse2PpsEntryRel(tutor, journal, course);
        saveOrUpdate(relation);


        /**
         * Подписать студентов в Сакае
         */
        addNewStudentsToCoursesSakai(sessionId, journal, course);
    }


    /**
     * Сохранение связки мероприятия журнала с мероприятием курса
     */
    @Override
    public void saveSakaiEventRelation(PpsEntry tutor, SakaiTrJournalEventRel eventRelation, SakaiCourse course, TrJournal journal)
    {
        SakaiCourse2PpsEntryRel sakaiCourse2PpsEntryRel = getByNaturalId(new SakaiCourse2PpsEntryRelGen.NaturalId(course, tutor, journal));
        if (sakaiCourse2PpsEntryRel == null)
            throw new ApplicationException("Указанный курс Сакая не привязан у текущему преподавателю и выбранной дисциплине.");

        eventRelation.setCourseRelation(sakaiCourse2PpsEntryRel);
        saveOrUpdate(eventRelation);
    }


    /**
     * Подписать студентов на курс в Сакае
     */
    @Override
    public void addNewStudentsToCoursesSakai(String sessionId, TrJournal journal, SakaiCourse course)
    {

        List<TrJournalGroup> groupList = getList(TrJournalGroup.class, TrJournalGroup.journal(), journal);
        List<Long> studentIds = getPropertiesList(TrJournalGroupStudent.class, TrJournalGroupStudent.group(), groupList, true, TrJournalGroupStudent.studentWpe().student().id());
        List<Long> studentIdsExclude = getPropertiesList(SakaiCourseStudent.class, SakaiCourseStudent.sakaiCourse(), course, true, SakaiCourseStudent.student().id());
        studentIds.removeAll(studentIdsExclude);
        if (studentIds.isEmpty())
            return;
        List<Student> studentList = getList(Student.class, studentIds);
        if (course.isUserInGroup())
        {
            Map<String, List<Student>> studentsMap = new HashMap<>();
            studentList.stream().forEach(e -> {
                if (e.getGroup() == null)
                    SafeMap.safeGet(studentsMap, "Без группы", key -> new ArrayList<>()).add(e);
                else
                    SafeMap.safeGet(studentsMap, e.getGroup().getTitle(), key -> new ArrayList<>()).add(e);
            });
            for (Map.Entry<String, List<Student>> entry : studentsMap.entrySet())
                subscribeStudentToSite(sessionId, course, entry.getKey(), entry.getValue());
        }
        else
            subscribeStudentToSite(sessionId, course, null, studentList);


    }

    private void subscribeStudentToSite(String sessionId, SakaiCourse course, String groupName, List<Student> studentList)
    {
        List<String> logins = studentList.stream().map(e-> e.getPrincipal().getLogin()).collect(Collectors.toList());
        try
        {
            if (StringUtils.isEmpty(sessionId))
                sessionId = SakaiTutorGetter.loginToSakay();

            // Сервис возвращает логины студентов, которых не удалось подписать на сайт
            String resultStudents = SakaiTutorGetter.getTutorService().getSakaiTutor().addStudentsToSite(sessionId, course.getSakaiCourseId(), groupName, logins.toArray(new String[logins.size()]));
            List<String> removeLogins = Arrays.asList(resultStudents.split(","));

            List<Student> students = studentList.stream().filter(e -> !removeLogins.contains(e.getPrincipal().getLogin())).collect(Collectors.toList());

            for (Student student : students)
            {
                SakaiCourseStudent course2Student = new SakaiCourseStudent();
                course2Student.setStudent(student);
                course2Student.setSakaiCourse(course);
                save(course2Student);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    /**
     * Получение оценок из Сакая
     */
    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    public void getGradesFromSakai(SakaiCourse2PpsEntryRel relation)
    {

        List<Student> students = getPropertiesList(SakaiCourseStudent.class, SakaiCourseStudent.sakaiCourse(), relation.getSakaiCourse(), false, SakaiCourseStudent.student());
        List<String> logins = students.stream().map(e -> e.getPrincipal().getLogin()).collect(Collectors.toList());
        List<SakaiTrJournalEventRel> events = getList(SakaiTrJournalEventRel.class, SakaiTrJournalEventRel.courseRelation(), relation);
        List<String> resultList = new ArrayList<>();
        try
        {
            String sessionId = SakaiTutorGetter.loginToSakay();
            for (SakaiTrJournalEventRel item : events)
            {
                resultList.add(SakaiTutorGetter.getTutorService().getSakaiTutor().getStudentsMarkFromSakai(sessionId,
                                       relation.getSakaiCourse().getSakaiCourseId(), item.getSakaiEventId(), logins.toArray(new String[logins.size()])));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        updateMarkData(resultList, relation);


    }

    /**
    * Получить курсы Сакая, привязанные к журналу.
     */
    private List<TutorSiteWrap> getExistingSites(PpsEntry tutor, TrJournal journal)
    {
       DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SakaiCourse2PpsEntryRel.class, "s")
                .where(eq(property("s", SakaiCourse2PpsEntryRel.trJournal()), value(journal)))
               .where(eq(property("s", SakaiCourse2PpsEntryRel.ppsEntry()), value(tutor)))
               .column(property("s", SakaiCourse2PpsEntryRel.sakaiCourse()));
        List<SakaiCourse> courses = getList(builder);
        List<TutorSiteWrap> resultList = new ArrayList<>(courses.size());
        for (SakaiCourse course : courses)
        {
            TutorSiteWrap wrap = new TutorSiteWrap();
            wrap.setTutorLogin(tutor.getPrincipal().getLogin());
            wrap.setSakaiSiteTitle(course.getTitle());
            wrap.setSakaiSiteId(course.getSakaiCourseId());
            resultList.add(wrap);

        }
        return resultList;
    }


    /**
     * Синхронизировать оценки с данными Сакая
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    private void updateMarkData(List<String> itemList, SakaiCourse2PpsEntryRel relation)
    {
        if (itemList.isEmpty())
            return;

        Map<CoreCollectionUtils.Pair<String, String>, String> gradeMap = new HashMap<>();
        for (String item : itemList)
            SakaiXMLParser.getMarkFromXML(gradeMap, item);

        DQLSelectColumnNumerator dql1 = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "ge")

                        // Джойним события курса
                        .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.event().fromAlias("ge"), "gee")
                        .joinEntity("ge", DQLJoinType.inner, SakaiTrJournalEventRel.class, "rel",
                                    eq(property("gee", TrEduGroupEvent.L_JOURNAL_EVENT), property("rel", SakaiTrJournalEventRel.event())))

                                // Джойним участников курсов
                        .joinPath(DQLJoinType.inner, SakaiTrJournalEventRel.courseRelation().fromAlias("rel"), "crel")
                        .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.studentWpe().fromAlias("ge"), "slot")
                        .joinEntity("ge", DQLJoinType.inner, SakaiCourseStudent.class, "s", and(
                                eq(property("crel", SakaiCourse2PpsEntryRel.L_SAKAI_COURSE), property("s", SakaiCourseStudent.L_SAKAI_COURSE)),
                                eq(property("slot", EppStudentWorkPlanElement.L_STUDENT), property("s", SakaiCourseStudent.L_STUDENT))
                        ))

                        .where(eq(property("ge", TrEduGroupEventStudent.event().journalEvent().journalModule().journal()), value(relation.getTrJournal())))
        );

        int columnIdCol = dql1.column(property("rel", SakaiTrJournalEventRel.P_SAKAI_EVENT_ID));
        int memberPrimaryIdCol = dql1.column(property("s", SakaiCourseStudent.student().principal().login()));
        int markCol = dql1.column("ge");

        List<Object[]> items = dql1.getDql().createStatement(getSession()).list();
        for (Object[] item : items)
        {
            CoreCollectionUtils.Pair<String, String> key = new CoreCollectionUtils.Pair<>((String) item[memberPrimaryIdCol], (String) item[columnIdCol]);
            String sakaiGrade = gradeMap.get(key);
            if (sakaiGrade != null)
            {
                Double sakaiMark = Doubles.tryParse(sakaiGrade);
                TrEduGroupEventStudent mark = (TrEduGroupEventStudent) item[markCol];
                if (sakaiMark != null && !sakaiMark.equals(mark.getGrade()))
                {
                    mark.setGrade(sakaiMark);
                    getSession().update(mark);
                }
                gradeMap.remove(key);
            }
        }

    }


    /**
     *  данные для селекта курсов Сакая
     */
    @Override
    public List<TutorSiteWrap> prepareSakaiCoursesList(PpsEntry tutor, TrJournal journal)
    {
        String login = tutor.getPrincipal().getLogin();
        List<TutorSiteWrap> retVal = new ArrayList<>();
        try
        {
            String  sessionId = SakaiTutorGetter.loginToSakay();
            String result = SakaiTutorGetter.getTutorService().getSakaiTutor().getTutorSites(sessionId, login);
            if (StringUtils.isNotEmpty(result))
            {
                retVal = SakaiXMLParser.getTutorSiteFromXML(result);
                List<TutorSiteWrap> existing = getExistingSites(tutor, journal);
                retVal.removeAll(existing);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Collections.sort(retVal, Comparator.comparing(TutorSiteWrap::getSakaiSiteTitle));
        return retVal;
    }
}