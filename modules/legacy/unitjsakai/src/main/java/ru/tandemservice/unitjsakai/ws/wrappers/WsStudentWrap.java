/* $Id$ */
package ru.tandemservice.unitjsakai.ws.wrappers;

import ru.tandemservice.uni.entity.employee.Student;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * @author Ekaterina Zvereva
 * @since 01.12.2015
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "studentWrap", namespace = "http://ws.unitjsakai.tandemservice.ru/")
public class WsStudentWrap implements Serializable
{
    private String studentId;      // Идентификатор студента
    private String educationOrgUnit; // Направление подготовки

    public WsStudentWrap(){}

    public String getStudentId()
    {
        return studentId;
    }

    public void setStudentId(String studentId)
    {
        this.studentId = studentId;
    }

    public String getEducationOrgUnit()
    {
        return educationOrgUnit;
    }

    public void setEducationOrgUnit(String educationOrgUnit)
    {
        this.educationOrgUnit = educationOrgUnit;
    }

    public WsStudentWrap(Student student)
    {
        studentId = String.valueOf(student.getId());
        educationOrgUnit = student.getEducationOrgUnit().getTitle();
    }
}