/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.ui.TutorTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unitjsakai.base.bo.Sakai.SakaiManager;
import ru.tandemservice.unitjsakai.base.bo.Sakai.logic.SakaiCourseDSHandler;
import ru.tandemservice.unitjsakai.base.bo.Sakai.ui.AddCourseToTrJournal.SakaiAddCourseToTrJournal;
import ru.tandemservice.unitjsakai.base.bo.Sakai.ui.AddCourseToTrJournal.SakaiAddCourseToTrJournalUI;
import ru.tandemservice.unitjsakai.base.bo.Sakai.ui.EditCourseEventRelation.SakaiEditCourseEventRelation;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel;
import ru.tandemservice.unitraining.base.bo.TrHomePage.TrHomePageManager;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalStructureEdit.TrHomePageJournalStructureEditUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;


/**
 * @author Ekaterina Zvereva
 * @since 09.12.2015
 */
public class SakaiTutorTabUI extends UIPresenter
{
    private boolean _empty;
    private TrJournal _journal;
    private PpsEntry _ppsEntry;

    @Override
    public void onComponentRefresh()
    {
        IPrincipalContext principalContext = _uiConfig.getUserContext().getPrincipalContext();
        Person person = PersonManager.instance().dao().getPerson(principalContext);
        if (principalContext instanceof EmployeePost)
        {
            EmployeePost4PpsEntry ep4ppsEntry = DataAccessServices.dao().get(EmployeePost4PpsEntry.class, EmployeePost4PpsEntry.employeePost().id(), principalContext.getId());
            if (null != ep4ppsEntry)
                _ppsEntry = ep4ppsEntry.getPpsEntry();
        }
        else
            _ppsEntry = null;

        _empty = TrHomePageManager.instance().dao().getStructureCount(person, TrHomePageJournalStructureEditUI.ALLOW_STATES) == 0;
        _journal = getSettings().get("journal");
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case SakaiTutorTab.SAKAI_TR_JOURNAL_DS:
                dataSource.put(SakaiTutorTab.SAKAI_TR_JOURNAL_PPS_PARAM, getPpsEntry());
                break;
            case SakaiTutorTab.COURSE_DS:
                if (getPpsEntry() != null)
                    dataSource.put(SakaiCourseDSHandler.PPS_ENTRY_FILTER_PARAM, getPpsEntry());
                dataSource.put(SakaiCourseDSHandler.TR_JOURNAL, getJournal());
                break;
            case SakaiTutorTab.STRUCTURE_VIEW_DS:
                dataSource.put(SakaiTutorTab.TR_JOURNAL_PARAM, getJournal());
                break;
        }
    }

    public void onClickAddCourse()
    {
        _uiActivation.asRegionDialog(SakaiAddCourseToTrJournal.class)
                .parameter(SakaiAddCourseToTrJournalUI.TR_JOURNAL_CONTEXT_PARAM, getJournal())
                .parameter(SakaiAddCourseToTrJournalUI.PPS_ENTRY_PARAM, getPpsEntry())
                .activate();
    }

    public void onClickGetGrades()
    {
        for (Object item : _uiConfig.getDataSource(SakaiTutorTab.COURSE_DS).getRecords())
        {
            SakaiCourse2PpsEntryRel relation = (SakaiCourse2PpsEntryRel) item;
            SakaiManager.instance().dao().getGradesFromSakai(relation);
        }
    }

    public void onClickDeleteEUCourse()
    {
       DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(SakaiEditCourseEventRelation.class)
                .parameter(IUIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .parameter("tutorId", getPpsEntry().getId())
                .parameter("journalId", getJournal().getId())
                .activate();
    }

    public void onClickAddNewStudentsToCourse()
    {
        for (Object item : _uiConfig.getDataSource(SakaiTutorTab.COURSE_DS).getRecords())
        {
            SakaiCourse2PpsEntryRel relation = (SakaiCourse2PpsEntryRel) item;
            SakaiManager.instance().dao().addNewStudentsToCoursesSakai(null, getJournal(), relation.getSakaiCourse());
        }
    }

    public void onClickChangeGroupping()
    {
        SakaiCourse course = DataAccessServices.dao().get(SakaiCourse2PpsEntryRel.class, getListenerParameterAsLong()).getSakaiCourse();
        course.setUserInGroup(!course.isUserInGroup());
        DataAccessServices.dao().update(course);
    }

    public void onDeleteEntityFromList()
    {
        SakaiTrJournalEventRel rel = DataAccessServices.dao().get(SakaiTrJournalEventRel.class, SakaiTrJournalEventRel.L_EVENT, getListenerParameterAsLong());
        if (rel != null)
            DataAccessServices.dao().delete(rel);
    }

    public boolean isEmpty()
    {
        return _empty;
    }

    public TrJournal getJournal()
    {
        return _journal;
    }

    public void setJournal(TrJournal journal)
    {
        _journal = journal;
        getSettings().set("journal", journal);
    }

    public PpsEntry getPpsEntry()
    {
        return _ppsEntry;
    }
}