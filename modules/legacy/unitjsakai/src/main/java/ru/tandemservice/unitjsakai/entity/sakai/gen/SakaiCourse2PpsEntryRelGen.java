package ru.tandemservice.unitjsakai.entity.sakai.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь ЭУК с преподавателем
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SakaiCourse2PpsEntryRelGen extends EntityBase
 implements INaturalIdentifiable<SakaiCourse2PpsEntryRelGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel";
    public static final String ENTITY_NAME = "sakaiCourse2PpsEntryRel";
    public static final int VERSION_HASH = -741332265;
    private static IEntityMeta ENTITY_META;

    public static final String L_SAKAI_COURSE = "sakaiCourse";
    public static final String L_PPS_ENTRY = "ppsEntry";
    public static final String L_TR_JOURNAL = "trJournal";

    private SakaiCourse _sakaiCourse;     // Электронный учебный курс (ЭУК) в системе Sakai
    private PpsEntry _ppsEntry;     // Запись в реестре ППС
    private TrJournal _trJournal;     // Реализация дисциплины

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Sakai. Свойство не может быть null.
     */
    @NotNull
    public SakaiCourse getSakaiCourse()
    {
        return _sakaiCourse;
    }

    /**
     * @param sakaiCourse Электронный учебный курс (ЭУК) в системе Sakai. Свойство не может быть null.
     */
    public void setSakaiCourse(SakaiCourse sakaiCourse)
    {
        dirty(_sakaiCourse, sakaiCourse);
        _sakaiCourse = sakaiCourse;
    }

    /**
     * @return Запись в реестре ППС. Свойство не может быть null.
     */
    @NotNull
    public PpsEntry getPpsEntry()
    {
        return _ppsEntry;
    }

    /**
     * @param ppsEntry Запись в реестре ППС. Свойство не может быть null.
     */
    public void setPpsEntry(PpsEntry ppsEntry)
    {
        dirty(_ppsEntry, ppsEntry);
        _ppsEntry = ppsEntry;
    }

    /**
     * @return Реализация дисциплины. Свойство не может быть null.
     */
    @NotNull
    public TrJournal getTrJournal()
    {
        return _trJournal;
    }

    /**
     * @param trJournal Реализация дисциплины. Свойство не может быть null.
     */
    public void setTrJournal(TrJournal trJournal)
    {
        dirty(_trJournal, trJournal);
        _trJournal = trJournal;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SakaiCourse2PpsEntryRelGen)
        {
            if (withNaturalIdProperties)
            {
                setSakaiCourse(((SakaiCourse2PpsEntryRel)another).getSakaiCourse());
                setPpsEntry(((SakaiCourse2PpsEntryRel)another).getPpsEntry());
                setTrJournal(((SakaiCourse2PpsEntryRel)another).getTrJournal());
            }
        }
    }

    public INaturalId<SakaiCourse2PpsEntryRelGen> getNaturalId()
    {
        return new NaturalId(getSakaiCourse(), getPpsEntry(), getTrJournal());
    }

    public static class NaturalId extends NaturalIdBase<SakaiCourse2PpsEntryRelGen>
    {
        private static final String PROXY_NAME = "SakaiCourse2PpsEntryRelNaturalProxy";

        private Long _sakaiCourse;
        private Long _ppsEntry;
        private Long _trJournal;

        public NaturalId()
        {}

        public NaturalId(SakaiCourse sakaiCourse, PpsEntry ppsEntry, TrJournal trJournal)
        {
            _sakaiCourse = ((IEntity) sakaiCourse).getId();
            _ppsEntry = ((IEntity) ppsEntry).getId();
            _trJournal = ((IEntity) trJournal).getId();
        }

        public Long getSakaiCourse()
        {
            return _sakaiCourse;
        }

        public void setSakaiCourse(Long sakaiCourse)
        {
            _sakaiCourse = sakaiCourse;
        }

        public Long getPpsEntry()
        {
            return _ppsEntry;
        }

        public void setPpsEntry(Long ppsEntry)
        {
            _ppsEntry = ppsEntry;
        }

        public Long getTrJournal()
        {
            return _trJournal;
        }

        public void setTrJournal(Long trJournal)
        {
            _trJournal = trJournal;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof SakaiCourse2PpsEntryRelGen.NaturalId) ) return false;

            SakaiCourse2PpsEntryRelGen.NaturalId that = (NaturalId) o;

            if( !equals(getSakaiCourse(), that.getSakaiCourse()) ) return false;
            if( !equals(getPpsEntry(), that.getPpsEntry()) ) return false;
            if( !equals(getTrJournal(), that.getTrJournal()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getSakaiCourse());
            result = hashCode(result, getPpsEntry());
            result = hashCode(result, getTrJournal());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getSakaiCourse());
            sb.append("/");
            sb.append(getPpsEntry());
            sb.append("/");
            sb.append(getTrJournal());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SakaiCourse2PpsEntryRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SakaiCourse2PpsEntryRel.class;
        }

        public T newInstance()
        {
            return (T) new SakaiCourse2PpsEntryRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sakaiCourse":
                    return obj.getSakaiCourse();
                case "ppsEntry":
                    return obj.getPpsEntry();
                case "trJournal":
                    return obj.getTrJournal();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sakaiCourse":
                    obj.setSakaiCourse((SakaiCourse) value);
                    return;
                case "ppsEntry":
                    obj.setPpsEntry((PpsEntry) value);
                    return;
                case "trJournal":
                    obj.setTrJournal((TrJournal) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sakaiCourse":
                        return true;
                case "ppsEntry":
                        return true;
                case "trJournal":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sakaiCourse":
                    return true;
                case "ppsEntry":
                    return true;
                case "trJournal":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sakaiCourse":
                    return SakaiCourse.class;
                case "ppsEntry":
                    return PpsEntry.class;
                case "trJournal":
                    return TrJournal.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SakaiCourse2PpsEntryRel> _dslPath = new Path<SakaiCourse2PpsEntryRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SakaiCourse2PpsEntryRel");
    }
            

    /**
     * @return Электронный учебный курс (ЭУК) в системе Sakai. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel#getSakaiCourse()
     */
    public static SakaiCourse.Path<SakaiCourse> sakaiCourse()
    {
        return _dslPath.sakaiCourse();
    }

    /**
     * @return Запись в реестре ППС. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel#getPpsEntry()
     */
    public static PpsEntry.Path<PpsEntry> ppsEntry()
    {
        return _dslPath.ppsEntry();
    }

    /**
     * @return Реализация дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel#getTrJournal()
     */
    public static TrJournal.Path<TrJournal> trJournal()
    {
        return _dslPath.trJournal();
    }

    public static class Path<E extends SakaiCourse2PpsEntryRel> extends EntityPath<E>
    {
        private SakaiCourse.Path<SakaiCourse> _sakaiCourse;
        private PpsEntry.Path<PpsEntry> _ppsEntry;
        private TrJournal.Path<TrJournal> _trJournal;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Электронный учебный курс (ЭУК) в системе Sakai. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel#getSakaiCourse()
     */
        public SakaiCourse.Path<SakaiCourse> sakaiCourse()
        {
            if(_sakaiCourse == null )
                _sakaiCourse = new SakaiCourse.Path<SakaiCourse>(L_SAKAI_COURSE, this);
            return _sakaiCourse;
        }

    /**
     * @return Запись в реестре ППС. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel#getPpsEntry()
     */
        public PpsEntry.Path<PpsEntry> ppsEntry()
        {
            if(_ppsEntry == null )
                _ppsEntry = new PpsEntry.Path<PpsEntry>(L_PPS_ENTRY, this);
            return _ppsEntry;
        }

    /**
     * @return Реализация дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel#getTrJournal()
     */
        public TrJournal.Path<TrJournal> trJournal()
        {
            if(_trJournal == null )
                _trJournal = new TrJournal.Path<TrJournal>(L_TR_JOURNAL, this);
            return _trJournal;
        }

        public Class getEntityClass()
        {
            return SakaiCourse2PpsEntryRel.class;
        }

        public String getEntityName()
        {
            return "sakaiCourse2PpsEntryRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
