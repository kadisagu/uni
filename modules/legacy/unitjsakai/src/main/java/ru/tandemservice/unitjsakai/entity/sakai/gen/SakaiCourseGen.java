package ru.tandemservice.unitjsakai.entity.sakai.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Электронный учебный курс (ЭУК) в системе Sakai
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SakaiCourseGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse";
    public static final String ENTITY_NAME = "sakaiCourse";
    public static final int VERSION_HASH = 1280623261;
    private static IEntityMeta ENTITY_META;

    public static final String P_SAKAI_COURSE_ID = "sakaiCourseId";
    public static final String P_TITLE = "title";
    public static final String P_INTERNAL = "internal";
    public static final String P_USER_IN_GROUP = "userInGroup";

    private String _sakaiCourseId;     // Идентификатор Sakai (primary)
    private String _title; 
    private boolean _internal = true;     // Курс создан из Юни
    private boolean _userInGroup = false;     // Разбивать пользователей по блокам

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор Sakai (primary). Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getSakaiCourseId()
    {
        return _sakaiCourseId;
    }

    /**
     * @param sakaiCourseId Идентификатор Sakai (primary). Свойство не может быть null и должно быть уникальным.
     */
    public void setSakaiCourseId(String sakaiCourseId)
    {
        dirty(_sakaiCourseId, sakaiCourseId);
        _sakaiCourseId = sakaiCourseId;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Курс создан из Юни. Свойство не может быть null.
     */
    @NotNull
    public boolean isInternal()
    {
        return _internal;
    }

    /**
     * @param internal Курс создан из Юни. Свойство не может быть null.
     */
    public void setInternal(boolean internal)
    {
        dirty(_internal, internal);
        _internal = internal;
    }

    /**
     * @return Разбивать пользователей по блокам. Свойство не может быть null.
     */
    @NotNull
    public boolean isUserInGroup()
    {
        return _userInGroup;
    }

    /**
     * @param userInGroup Разбивать пользователей по блокам. Свойство не может быть null.
     */
    public void setUserInGroup(boolean userInGroup)
    {
        dirty(_userInGroup, userInGroup);
        _userInGroup = userInGroup;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SakaiCourseGen)
        {
            setSakaiCourseId(((SakaiCourse)another).getSakaiCourseId());
            setTitle(((SakaiCourse)another).getTitle());
            setInternal(((SakaiCourse)another).isInternal());
            setUserInGroup(((SakaiCourse)another).isUserInGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SakaiCourseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SakaiCourse.class;
        }

        public T newInstance()
        {
            return (T) new SakaiCourse();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "sakaiCourseId":
                    return obj.getSakaiCourseId();
                case "title":
                    return obj.getTitle();
                case "internal":
                    return obj.isInternal();
                case "userInGroup":
                    return obj.isUserInGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "sakaiCourseId":
                    obj.setSakaiCourseId((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "internal":
                    obj.setInternal((Boolean) value);
                    return;
                case "userInGroup":
                    obj.setUserInGroup((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "sakaiCourseId":
                        return true;
                case "title":
                        return true;
                case "internal":
                        return true;
                case "userInGroup":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "sakaiCourseId":
                    return true;
                case "title":
                    return true;
                case "internal":
                    return true;
                case "userInGroup":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "sakaiCourseId":
                    return String.class;
                case "title":
                    return String.class;
                case "internal":
                    return Boolean.class;
                case "userInGroup":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SakaiCourse> _dslPath = new Path<SakaiCourse>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SakaiCourse");
    }
            

    /**
     * @return Идентификатор Sakai (primary). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse#getSakaiCourseId()
     */
    public static PropertyPath<String> sakaiCourseId()
    {
        return _dslPath.sakaiCourseId();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Курс создан из Юни. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse#isInternal()
     */
    public static PropertyPath<Boolean> internal()
    {
        return _dslPath.internal();
    }

    /**
     * @return Разбивать пользователей по блокам. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse#isUserInGroup()
     */
    public static PropertyPath<Boolean> userInGroup()
    {
        return _dslPath.userInGroup();
    }

    public static class Path<E extends SakaiCourse> extends EntityPath<E>
    {
        private PropertyPath<String> _sakaiCourseId;
        private PropertyPath<String> _title;
        private PropertyPath<Boolean> _internal;
        private PropertyPath<Boolean> _userInGroup;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор Sakai (primary). Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse#getSakaiCourseId()
     */
        public PropertyPath<String> sakaiCourseId()
        {
            if(_sakaiCourseId == null )
                _sakaiCourseId = new PropertyPath<String>(SakaiCourseGen.P_SAKAI_COURSE_ID, this);
            return _sakaiCourseId;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(SakaiCourseGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Курс создан из Юни. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse#isInternal()
     */
        public PropertyPath<Boolean> internal()
        {
            if(_internal == null )
                _internal = new PropertyPath<Boolean>(SakaiCourseGen.P_INTERNAL, this);
            return _internal;
        }

    /**
     * @return Разбивать пользователей по блокам. Свойство не может быть null.
     * @see ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse#isUserInGroup()
     */
        public PropertyPath<Boolean> userInGroup()
        {
            if(_userInGroup == null )
                _userInGroup = new PropertyPath<Boolean>(SakaiCourseGen.P_USER_IN_GROUP, this);
            return _userInGroup;
        }

        public Class getEntityClass()
        {
            return SakaiCourse.class;
        }

        public String getEntityName()
        {
            return "sakaiCourse";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
