/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.logic;

import org.tandemframework.core.CoreCollectionUtils;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.tandemservice.unitjsakai.base.bo.Sakai.ui.EditCourseEventRelation.SakaiEventWrapper;
import ru.tandemservice.unitjsakai.ws.logic.tutor.wrap.TutorSiteWrap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 23.12.2015
 */
public class SakaiXMLParser
{
    /**
     * Получение оценок от сервиса Сакая
     */
    public static void getMarkFromXML(Map<CoreCollectionUtils.Pair<String, String>, String> gradeMap, String dataXml)
    {
        try
        {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(dataXml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("item");

            for (int i = 0; i < nodes.getLength(); i++)
            {
                Element element = (Element) nodes.item(i);

                NodeList title = element.getElementsByTagName("assignment");
                Element line = (Element) title.item(0);
                String assignmentId = getCharacterDataFromElement(line);

                NodeList grade = element.getElementsByTagName("grade");
                line = (Element) grade.item(0);
                String mark = getCharacterDataFromElement(line);

                NodeList student = element.getElementsByTagName("student");
                line = (Element) student.item(0);
                String studentLogin = getCharacterDataFromElement(line);
                CoreCollectionUtils.Pair<String, String> key = new CoreCollectionUtils.Pair<>(studentLogin, assignmentId);
                gradeMap.put(key, mark);

            }
        }
        catch (ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }

    }


    /**
     *  Получение курсов преподавателя
     */
    public static List<TutorSiteWrap> getTutorSiteFromXML(String xmlRecords)
    {
        List<TutorSiteWrap> resultList = new ArrayList<>();
        try
        {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xmlRecords));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("item");

            for (int i = 0; i < nodes.getLength(); i++)
            {
                Element element = (Element) nodes.item(i);
                TutorSiteWrap wrap = new TutorSiteWrap();

                NodeList name = element.getElementsByTagName("siteId");
                Element line = (Element) name.item(0);
                wrap.setSakaiSiteId(getCharacterDataFromElement(line));

                NodeList title = element.getElementsByTagName("siteTitle");
                line = (Element) title.item(0);
                wrap.setSakaiSiteTitle(getCharacterDataFromElement(line));

                resultList.add(wrap);
            }
        }
        catch (ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }
        return resultList;

    }

    /**
     * Получение мероприятий курсов
     */
    public static List<SakaiEventWrapper> getSakaiEventsFromXML(String xmlString)
    {
        List<SakaiEventWrapper> resultList = new ArrayList<>();
        try
        {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xmlString));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("item");

            for (int i = 0; i < nodes.getLength(); i++)
            {
                Element element = (Element) nodes.item(i);
                SakaiEventWrapper wrap = new SakaiEventWrapper();

                NodeList site = element.getElementsByTagName("siteId");
                Element line = (Element) site.item(0);
                wrap.setSiteId(getCharacterDataFromElement(line));

                NodeList assNode = element.getElementsByTagName("assignment");
                Element assignment = (Element) assNode.item(0);

                NodeList itemId = assignment.getElementsByTagName("assignmentId");
                line = (Element) itemId.item(0);
                wrap.setAssignmentId(getCharacterDataFromElement(line));

                NodeList title = assignment.getElementsByTagName("assignmentName");
                line = (Element) title.item(0);
                wrap.setAssignmentTitle(getCharacterDataFromElement(line));

                resultList.add(wrap);
            }
        }
        catch (ParserConfigurationException e)
        {
            e.printStackTrace();
        }
        catch (SAXException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return resultList;
    }


    private static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }
}