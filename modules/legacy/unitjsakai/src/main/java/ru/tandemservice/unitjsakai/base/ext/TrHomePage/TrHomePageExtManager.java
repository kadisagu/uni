/* $Id$ */
package ru.tandemservice.unitjsakai.base.ext.TrHomePage;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 08.12.2015
 */
@Configuration
public class TrHomePageExtManager extends BusinessObjectExtensionManager
{
}