/**
 * SakaiTutorServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unitjsakai.ws.logic.tutor;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uni.UniProperties;

public class SakaiTutorServiceLocator extends org.apache.axis.client.Service implements ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutorService {

    public SakaiTutorServiceLocator() {
    }


    public SakaiTutorServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SakaiTutorServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SakaiTutor
    private java.lang.String SakaiTutor_address = "/sakai-axis/SakaiTutor.jws";

    public String getSakaiTutorAddress()
    {
        if (ApplicationRuntime.getProperty(UniProperties.SAKAI_HOST)==null)
            throw new ApplicationException("Не указаны настройки для связи с сервером САКАЙ.");
        return ApplicationRuntime.getProperty(UniProperties.SAKAI_HOST) + SakaiTutor_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SakaiTutorWSDDServiceName = "SakaiTutor";

    public java.lang.String getSakaiTutorWSDDServiceName() {
        return SakaiTutorWSDDServiceName;
    }

    public void setSakaiTutorWSDDServiceName(java.lang.String name) {
        SakaiTutorWSDDServiceName = name;
    }

    public ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutor_PortType getSakaiTutor() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getSakaiTutorAddress());
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSakaiTutor(endpoint);
    }

    public ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutor_PortType getSakaiTutor(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutorSoapBindingStub _stub = new ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutorSoapBindingStub(portAddress, this);
            _stub.setPortName(getSakaiTutorWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSakaiTutorEndpointAddress(java.lang.String address) {
        SakaiTutor_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutor_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutorSoapBindingStub _stub = new ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutorSoapBindingStub(new java.net.URL(getSakaiTutorAddress()), this);
                _stub.setPortName(getSakaiTutorWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SakaiTutor".equals(inputPortName)) {
            return getSakaiTutor();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName(getSakaiTutorAddress(), "SakaiTutor");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName(getSakaiTutorAddress(), "SakaiTutor"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SakaiTutor".equals(portName)) {
            setSakaiTutorEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
