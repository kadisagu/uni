/* $Id$ */
package ru.tandemservice.unitjsakai.ws.logic.tutor.wrap;

import java.io.Serializable;

/**
 * @author Ekaterina Zvereva
 * @since 14.12.2015
 */
public class TutorSiteWrap implements Serializable
{
    private String _sakaiSiteId;
    private String _sakaiSiteTitle;
    private String _tutorLogin;

    public String getSakaiSiteId()
    {
        return _sakaiSiteId;
    }

    public void setSakaiSiteId(String sakaiSiteId)
    {
        _sakaiSiteId = sakaiSiteId;
    }

    public String getSakaiSiteTitle()
    {
        return _sakaiSiteTitle;
    }

    public void setSakaiSiteTitle(String sakaiSiteTitle)
    {
        _sakaiSiteTitle = sakaiSiteTitle;
    }

    public String getTutorLogin()
    {
        return _tutorLogin;
    }

    public void setTutorLogin(String tutorLogin)
    {
        _tutorLogin = tutorLogin;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TutorSiteWrap that = (TutorSiteWrap) o;

        return _sakaiSiteId.equals(that._sakaiSiteId);

    }

    @Override
    public int hashCode()
    {
        return _sakaiSiteId.hashCode();
    }
}