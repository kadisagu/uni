package ru.tandemservice.unitjsakai.entity.sakai;

import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unitjsakai.entity.sakai.gen.*;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/** @see ru.tandemservice.unitjsakai.entity.sakai.gen.SakaiCourse2PpsEntryRelGen */
public class SakaiCourse2PpsEntryRel extends SakaiCourse2PpsEntryRelGen
{

    public SakaiCourse2PpsEntryRel()
    {

    }

    public SakaiCourse2PpsEntryRel(PpsEntry tutor, TrJournal journal, SakaiCourse course)
    {
        setPpsEntry(tutor);
        setSakaiCourse(course);
        setTrJournal(journal);
    }
}