package ru.tandemservice.unitjsakai.ws.logic.wrap;

import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import java.util.ArrayList;
import java.util.List;


/**
 * Данные реализации дисциплины
 */
public class CourseType
{
    private TrJournal _journal; //журнал
    private EppStudentWorkPlanElement _slot;
    private String name; //название
    private String teacherName; // преподаватель
    private String ratingFinal; //рейтинг
    private String result;
    private List<TaskType> task; //список работ

    public CourseType(TrJournal journal, EppStudentWorkPlanElement slot)
    {
        _journal = journal;
        _slot = slot;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String value) {
        this.teacherName = value;
    }


    public String getRatingFinal() {
        return ratingFinal;
    }

    public void setRatingFinal(String value) {
        this.ratingFinal = value;
    }


    public String getResult() {
        return result;
    }

    public void setResult(String value) {
        this.result = value;
    }

    public List<TaskType> getTask() {
        if (task == null) {
            task = new ArrayList<>();
        }
        return this.task;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CourseType that = (CourseType) o;

        if (!_journal.equals(that._journal)) return false;
        return _slot.equals(that._slot);

    }

    @Override
    public int hashCode()
    {
        int result = _journal.hashCode();
        result = 31 * result + _slot.hashCode();
        return result;
    }
}
