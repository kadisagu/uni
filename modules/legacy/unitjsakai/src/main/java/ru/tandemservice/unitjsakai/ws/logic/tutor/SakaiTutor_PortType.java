/**
 * SakaiTutor_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unitjsakai.ws.logic.tutor;

public interface SakaiTutor_PortType extends java.rmi.Remote {
    public java.lang.String getTutorSites(java.lang.String sessionId, java.lang.String login) throws java.rmi.RemoteException;
    public java.lang.String createNewSiteWithTutor(java.lang.String sessionId, java.lang.String tutorId, java.lang.String siteId, java.lang.String siteTitle, java.lang.String siteDescription) throws java.rmi.RemoteException;
    public java.lang.String addStudentsToSite(java.lang.String sessionId, java.lang.String siteId, java.lang.String groupTitle, java.lang.String[] usersId) throws java.rmi.RemoteException;
    public java.lang.String getAssignmentsForSites(java.lang.String sessionId, java.lang.String siteId) throws java.rmi.RemoteException;
    public java.lang.String getStudentsMarkFromSakai(java.lang.String sessionId, java.lang.String siteId, java.lang.String assignmentId, java.lang.String[] studentsLogins) throws java.rmi.RemoteException;
}
