/**
 * SakaiTutorService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unitjsakai.ws.logic.tutor;

public interface SakaiTutorService extends javax.xml.rpc.Service {
    public java.lang.String getSakaiTutorAddress();

    public ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutor_PortType getSakaiTutor() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutor_PortType getSakaiTutor(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
