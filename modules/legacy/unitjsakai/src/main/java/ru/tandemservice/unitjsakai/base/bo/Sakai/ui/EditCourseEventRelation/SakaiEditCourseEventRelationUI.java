/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.ui.EditCourseEventRelation;

import org.apache.axis.utils.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unitjsakai.ws.SakaiTutorGetter;
import ru.tandemservice.unitjsakai.base.bo.Sakai.logic.SakaiXMLParser;
import ru.tandemservice.unitjsakai.base.bo.Sakai.SakaiManager;
import ru.tandemservice.unitjsakai.base.bo.Sakai.logic.SakaiCourseDSHandler;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;


import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 18.12.2015
 */
@Input({
        @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "eventId", required = true),
        @Bind(key = "tutorId", binding = "tutorId", required = true),
        @Bind(key = "journalId", binding = "journalId", required = true)
})
public class SakaiEditCourseEventRelationUI extends UIPresenter
{
    private Long _eventId;
    private Long _tutorId;
    private Long _journalId;

    private PpsEntry _ppsEntry;
    private TrJournal _journal;
    private TrJournalEvent _event;
    private SakaiEventWrapper _sakaiEvent;
    private SakaiCourse _course;
    private SakaiTrJournalEventRel _relation;

    private List<SakaiEventWrapper> sakaiEventList = new ArrayList<>();

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _ppsEntry = DataAccessServices.dao().get(PpsEntry.class, _tutorId);
        _journal = DataAccessServices.dao().get(TrJournal.class, _journalId);
        _event = DataAccessServices.dao().get(TrJournalEvent.class, _eventId);
        _relation = DataAccessServices.dao().getUnique(SakaiTrJournalEventRel.class, SakaiTrJournalEventRel.event().s(), _event);
        if (_relation != null)
            _sakaiEvent = new SakaiEventWrapper(_relation.getCourseRelation().getSakaiCourse().getSakaiCourseId(), _relation.getSakaiEventId(),_relation.getSakaiEventName());
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SakaiEditCourseEventRelation.COURSE_DS.equals(dataSource.getName()))
        {
            dataSource.put(SakaiCourseDSHandler.TR_JOURNAL, _journal);
            dataSource.put(SakaiCourseDSHandler.PPS_ENTRY_FILTER_PARAM, _ppsEntry);
        }
    }


    public void updateEvents()
    {
        try
        {
            String sessionId = SakaiTutorGetter.loginToSakay();
            String result = SakaiTutorGetter.getTutorService().getSakaiTutor().getAssignmentsForSites(sessionId, _course.getSakaiCourseId());
            if (!StringUtils.isEmpty(result))
            {
                sakaiEventList = SakaiXMLParser.getSakaiEventsFromXML(result);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    public void onClickSave()
    {
        if (_relation == null)
            _relation = new SakaiTrJournalEventRel();
        _relation.setEvent(_event);
        _relation.setSakaiEventId(getSakaiEvent().getAssignmentId());
        _relation.setSakaiEventName(getSakaiEvent().getAssignmentTitle());
        SakaiManager.instance().dao().saveSakaiEventRelation(getPpsEntry(), _relation, _course, _journal);
        deactivate();
    }

    public Long getEventId()
    {
        return _eventId;
    }

    public void setEventId(Long eventId)
    {
        _eventId = eventId;
    }

    public Long getTutorId()
    {
        return _tutorId;
    }

    public void setTutorId(Long tutorId)
    {
        _tutorId = tutorId;
    }

    public List<SakaiEventWrapper> getSakaiEventList()
    {
        return sakaiEventList;
    }

    public void setSakaiEventList(List<SakaiEventWrapper> sakaiEventList)
    {
        this.sakaiEventList = sakaiEventList;
    }

    public PpsEntry getPpsEntry()
    {
        return _ppsEntry;
    }

    public void setPpsEntry(PpsEntry ppsEntry)
    {
        _ppsEntry = ppsEntry;
    }

    public Long getJournalId()
    {
        return _journalId;
    }

    public void setJournalId(Long journalId)
    {
        _journalId = journalId;
    }

    public TrJournal getJournal()
    {
        return _journal;
    }

    public void setJournal(TrJournal journal)
    {
        _journal = journal;
    }

    public TrJournalEvent getEvent()
    {
        return _event;
    }

    public void setEvent(TrJournalEvent event)
    {
        _event = event;
    }

    public SakaiEventWrapper getSakaiEvent()
    {
        return _sakaiEvent;
    }

    public void setSakaiEvent(SakaiEventWrapper sakaiEvent)
    {
        _sakaiEvent = sakaiEvent;
    }
}