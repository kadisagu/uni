/* $Id$ */
package ru.tandemservice.unitjsakai.ws.logic;

import ru.tandemservice.unitjsakai.ws.logic.wrap.CourseType;
import ru.tandemservice.unitjsakai.ws.logic.wrap.GroupType;
import ru.tandemservice.unitjsakai.ws.logic.wrap.StudentType;
import ru.tandemservice.unitjsakai.ws.logic.wrap.TaskType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author Ekaterina Zvereva
 * @since 07.12.2015
 */
public class RatingReportBuilder
{

    public static final String CRITERIA_SEPARATOR = "<br/> ";

    public static final String START_ROW = "<tr><td>";
    public static final String END_ROW = "</td></tr>";
    public static final String START_ROW_HEAD = "<tr><th>";
    public static final String END_ROW_HEAD = "</th></tr>";
    public static final String NEXT_ROW_HEAD = "</th><th>";
    public static final String START_COLUMN = "<td>";
    public static final String END_COLUMN = "</td>";
    public static final String HEAD = "<table class='listHier' cellspacing='0' cellpadding='0'>";
    public static final String FOOTER = "</table>";
    public static final String DIV_START = "<div>";
    public static final String DIV_END = "</div>";

    public String studentTypeToString(StudentType student)
    {

        StringBuilder builder = new StringBuilder();
        builder.append("<h2>").append(DIV_START)
                .append(student.getName())
                .append(DIV_END)
                .append("</h2>")

                .append("<h2>").append(DIV_START)
                .append(student.getRatingtitle())
                .append(DIV_END)
                .append("</h2>");
        if (student.getGroups() == null)
            return builder.toString();

        for (GroupType group : student.getGroups().getGroup())
        {
            builder.append(DIV_START)
                    .append(group.getName())
                    .append(DIV_END);
            for (CourseType course : group.getCourse())
            {
                builder.append(DIV_START)
                        .append(course.getName())
                        .append(DIV_END)
                        .append(DIV_START)
                        .append("Преподаватель - ")
                        .append(course.getTeacherName())
                        .append(DIV_END);

                //Рейтинг
                builder.append(DIV_START)
                        .append("Рейтинг текущий")
                        .append(" - ")
                        .append(course.getRatingFinal() == null ? "" : course.getRatingFinal())
                        .append(DIV_END);

                builder.append(CRITERIA_SEPARATOR);
                if (course.getTask().isEmpty())
                    continue;

                appendTaskHeader(builder);
                for (TaskType task : course.getTask())
                {
                    builder.append(START_ROW)
                            .append(task.getName()==null? "":task.getName())
                            .append(END_COLUMN).append(START_COLUMN)
                            .append(task.getTypeName() == null? "":task.getTypeName())
                            .append(END_COLUMN).append(START_COLUMN)
                            .append(task.getDue()==null? "":task.getDue())
                            .append(END_COLUMN).append(START_COLUMN)
                            .append(task.getMaxPoints() == null ? "" : task.getMaxPoints())
                            .append(END_COLUMN).append(START_COLUMN)
                            .append((task.getMark() == null || task.getMark().getMdate() == null) ? "" : task.getMark().getMdate())
                            .append(END_COLUMN).append(START_COLUMN)
                            .append((task.getMark() == null || task.getMark().getValue()==null)? "":task.getMark().getValue())
                            .append(END_ROW);
                }
                builder.append(FOOTER);
                builder.append(CRITERIA_SEPARATOR);


            }
        }

        return builder.toString();
    }


    private void appendTaskHeader(StringBuilder builder)
    {
        builder.append(HEAD)
                .append(START_ROW_HEAD);
        String[] header = {"Задание", "Тип", "Дата", "Макс балл", "Дата сдачи", "Баллы"};
        builder.append(new ArrayList<>(Arrays.asList(header)).stream().collect(Collectors.joining(NEXT_ROW_HEAD)))
                .append(END_ROW_HEAD);

    }
}