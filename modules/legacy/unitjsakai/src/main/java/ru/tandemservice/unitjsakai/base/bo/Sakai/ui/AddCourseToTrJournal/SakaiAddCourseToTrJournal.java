/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.ui.AddCourseToTrJournal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;

import java.util.*;


/**
 * @author Ekaterina Zvereva
 * @since 09.12.2015
 */
@Configuration
public class SakaiAddCourseToTrJournal extends BusinessComponentManager
{
    public static final String ACTION_DS = "actionDS";
    public static final IEntity CREATE_NEW_COURSE_ITEM = new IdentifiableWrapper(1L, "Создать новый курс");
    public static final IEntity LINK_COURSE_ITEM = new IdentifiableWrapper(2L, "Привязать существующий курс");

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ACTION_DS, actionDSHandler()))
                .create();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> actionDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(CREATE_NEW_COURSE_ITEM, LINK_COURSE_ITEM));
    }

}