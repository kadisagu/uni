/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unitjsakai.base.bo.Sakai.logic.SakaiDao;
import ru.tandemservice.unitjsakai.base.bo.Sakai.logic.ISakaiDao;


/**
 * @author Ekaterina Zvereva
 * @since 08.12.2015
 */
@Configuration
public class SakaiManager extends BusinessObjectManager
{
    public static SakaiManager instance()
    {
        return instance(SakaiManager.class);
    }

    @Bean
    public ISakaiDao dao()
    {
        return new SakaiDao();
    }
}