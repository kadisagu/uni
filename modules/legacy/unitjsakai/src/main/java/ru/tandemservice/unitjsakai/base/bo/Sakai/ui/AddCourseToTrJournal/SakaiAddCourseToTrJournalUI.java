/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.ui.AddCourseToTrJournal;

import com.google.common.base.Charsets;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unitjsakai.base.bo.Sakai.SakaiManager;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import ru.tandemservice.unitjsakai.ws.logic.tutor.wrap.TutorSiteWrap;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Ekaterina Zvereva
 * @since 09.12.2015
 */
@Input({
        @Bind(key = SakaiAddCourseToTrJournalUI.TR_JOURNAL_CONTEXT_PARAM, binding = "trJournal", required = true),
        @Bind(key = SakaiAddCourseToTrJournalUI.PPS_ENTRY_PARAM, binding = "tutor")
})
public class SakaiAddCourseToTrJournalUI extends UIPresenter
{
    public static final String TR_JOURNAL_CONTEXT_PARAM = "trJournal";
    public static final String PPS_ENTRY_PARAM = "ppsEntry";

    private TrJournal _trJournal;
    private PpsEntry _tutor;
    private IEntity _action;
    private String _courseTitle;
    private String _courseDescription;
    private String _courseId;
    private List<TutorSiteWrap> _coursesDs = new ArrayList<>();
    private TutorSiteWrap _siteWrap;
    private SakaiCourse _sakaiCourse = new SakaiCourse();
    private boolean init = false;

    @Override
    public void onComponentRefresh()
    {
        if (!init)
        {
            init = true;
            _coursesDs = SakaiManager.instance().dao().prepareSakaiCoursesList(_tutor, _trJournal);
        }
    }


    public void onClickSave()
    {
        if (!isCreateNew())
        {
            _sakaiCourse.setSakaiCourseId(_siteWrap.getSakaiSiteId());
            _sakaiCourse.setTitle(_siteWrap.getSakaiSiteTitle());
        }
        else
        {
            String guid = UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis() + SakaiCourse.class.getSimpleName()).getBytes(Charsets.UTF_8)).toString();
            _sakaiCourse.setSakaiCourseId(guid);
        }
        SakaiManager.instance().dao().saveCourseForPps(getTutor(), getTrJournal(), _sakaiCourse, getCourseDescription(), isCreateNew());
        deactivate();
    }


    public TrJournal getTrJournal()
    {
        return _trJournal;
    }

    public void setTrJournal(TrJournal trJournal)
    {
        _trJournal = trJournal;
    }


    public PpsEntry getTutor()
    {
        return _tutor;
    }

    public void setTutor(PpsEntry tutor)
    {
        _tutor = tutor;
    }


    public boolean isActionSelected()
    {
        return _action != null;
    }

    public IEntity getAction()
    {
        return _action;
    }

    public void setAction(IEntity action)
    {
        _action = action;
    }

    public boolean isCreateNew()
    {
        return _action != null && SakaiAddCourseToTrJournal.CREATE_NEW_COURSE_ITEM.getId().equals(_action.getId());
    }

    public String getCourseTitle()
    {
        return _courseTitle;
    }

    public void setCourseTitle(String courseTitle)
    {
        _courseTitle = courseTitle;
    }

    public String getCourseDescription()
    {
        return _courseDescription;
    }

    public void setCourseDescription(String courseDescription)
    {
        _courseDescription = courseDescription;
    }

    public TutorSiteWrap getSiteWrap()
    {
        return _siteWrap;
    }

    public void setSiteWrap(TutorSiteWrap siteWrap)
    {
        _siteWrap = siteWrap;
    }

    public String getCourseId()
    {
        return _courseId;
    }

    public void setCourseId(String courseId)
    {
        _courseId = courseId;
    }
}