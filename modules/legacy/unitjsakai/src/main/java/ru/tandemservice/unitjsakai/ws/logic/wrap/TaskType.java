package ru.tandemservice.unitjsakai.ws.logic.wrap;


/**
 * Данные контрольного мероприятия из реализации дисциплины
 */
public class TaskType
{

    private String name;
    private String due; //дата сдачи
    private String maxPoints;
    private String typeName;
    private MarkType mark;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDue() {
        return due;
    }

    public void setDue(String value) {
        this.due = value;
    }

    public String getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(String value) {
        this.maxPoints = value;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String value) {
        this.typeName = value;
    }


    public MarkType getMark()
    {
        return mark;
    }

    public void setMark(MarkType mark)
    {
        this.mark = mark;
    }
}
