/* $Id$ */
package ru.tandemservice.unitjsakai.ws.wrappers;

import ru.tandemservice.movestudent.entity.AbstractStudentOrder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Ekaterina Zvereva
 * @since 01.12.2015
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "orderWrap", namespace = "http://ws.unitjsakai.tandemservice.ru/")
public class WsOrderWrap implements Serializable
{

    /**
     * Название приказа
     */
    private String title;
    /**
     * Номер приказа
     */
    private String number;
    /**
     * Дата приказа
     */
    private Date orderDate;
    /**
     * Дата вступления приказа в силу
     */
    private Date orderStartDate;


    public WsOrderWrap()
    {
    }

    public WsOrderWrap(String title, AbstractStudentOrder order)
    {
        this.title = title;
        this.number = order.getNumber();
        this.orderDate = order.getCreateDate();
        this.orderStartDate = order.getCommitDate();
    }

    public WsOrderWrap (String title, String number, Date dateCreate, Date dateCommit)
    {
        this.title = title;
        this.number = number;
        this.orderStartDate = dateCommit;
        this.orderDate = dateCreate;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public Date getOrderDate()
    {
        return orderDate;
    }

    public void setOrderDate(Date orderDate)
    {
        this.orderDate = orderDate;
    }

    public Date getOrderStartDate()
    {
        return orderStartDate;
    }

    public void setOrderStartDate(Date orderStartDate)
    {
        this.orderStartDate = orderStartDate;
    }

}