/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.ui.TutorTab;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.unitjsakai.base.bo.Sakai.logic.SakaiCourseDSHandler;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiTrJournalEventRel;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAddon;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 09.12.2015
 */
@Configuration
public class SakaiTutorTab extends BusinessComponentManager
{
    public static final String COURSE_DS = "courseDS";
    public static final String STRUCTURE_VIEW_DS = "structureViewDS";
    public static final String SAKAI_TR_JOURNAL_DS = "sakaiTrJournalDS";
    public static final String SAKAI_TR_JOURNAL_PPS_PARAM = "ppsEntry";

    public static final String SAKAI_COURSE_TITLE_VIEW_PROPERTY = "sakaiCourseTitle";
    public static final String SAKAI_EVENT_NAME_VIEW_PROPERTY = "sakaiEventName";
    public static final String SAKAI_DISABLED_EDIT_VIEW_PROPERTY = "disabled_edit";
    public static final String SAKAI_DISABLED_DELETE_VIEW_PROPERTY = "disabled_delete";

    public static final String TR_JOURNAL_PARAM = "trJournal";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(SAKAI_TR_JOURNAL_DS, sakaiTrJournalDSHandler()).addColumn(TrJournal.P_CALCULATED_TITLE))
                .addDataSource(searchListDS(COURSE_DS, getCourseDS(), courseDSHandler()))
                .addDataSource(searchListDS(STRUCTURE_VIEW_DS, getStructureViewDS(), structureViewDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getCourseDS()
    {
        return columnListExtPointBuilder(COURSE_DS)
                .addColumn(textColumn(SakaiCourse.P_TITLE, SakaiCourse2PpsEntryRel.sakaiCourse().title()).order())
                .addColumn(textColumn(SakaiCourse.P_SAKAI_COURSE_ID, SakaiCourse2PpsEntryRel.sakaiCourse().sakaiCourseId()).order())
                .addColumn(toggleColumn(SakaiCourse.P_USER_IN_GROUP, SakaiCourse2PpsEntryRel.sakaiCourse().userInGroup())
                                   .toggleOnListener("onClickChangeGroupping").toggleOffListener("onClickChangeGroupping"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEUCourse").alert("Удалить ЭУК?"))
                .create();
    }

    @Bean
    public ColumnListExtPoint getStructureViewDS()
    {
        return columnListExtPointBuilder(STRUCTURE_VIEW_DS)
                .addColumn(textColumn("title", "title").treeable(true))
                .addColumn(textColumn("course", SAKAI_COURSE_TITLE_VIEW_PROPERTY))
                .addColumn(textColumn("sakaiEvent", SAKAI_EVENT_NAME_VIEW_PROPERTY))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled(SAKAI_DISABLED_EDIT_VIEW_PROPERTY))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).disabled(SAKAI_DISABLED_DELETE_VIEW_PROPERTY).alert("Удалить связь с мероприятием ЭУК?"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return new SakaiCourseDSHandler(getName(), false);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sakaiTrJournalDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), TrJournal.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                PpsEntry pps = context.get(SAKAI_TR_JOURNAL_PPS_PARAM);

                // Выводим те реализации, где текущий пользователь указан в качестве преподавателя или в качестве ответственного за формирование.
                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(EppPpsCollectionItem.class, "rel").column("rel.id")
                        .joinEntity("rel", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "grp", eq(property("rel", EppPpsCollectionItem.list()), property("grp")))
                        .joinEntity("grp", DQLJoinType.inner, TrJournalGroup.class, "jRel", eq(property("jRel", TrJournalGroup.group()), property("grp")))
                        .where(eq(property("jRel", TrJournalGroup.journal()), property(alias)))
                        .where(eq(property("rel", EppPpsCollectionItem.pps()), value(pps)));

                dql.where(or(
                        eq(property(alias, TrJournal.responsible()), value(pps)),
                        exists(subBuilder.buildQuery())
                ));

                dql.where(in(property(TrJournal.state().code().fromAlias(alias)), Arrays.asList(EppState.STATE_FORMATIVE, EppState.STATE_ACCEPTABLE, EppState.STATE_ACCEPTED)));
            }
        };

        handler.order(TrJournal.registryElementPart().registryElement().title())
                .order(TrJournal.title())
                .filter(TrJournal.title())
                .filter(TrJournal.registryElementPart().registryElement().title())
                .pageable(true);

        return handler;
    }

    public static class Row extends DataWrapper
    {
        private final Row _parent;

        @Override
        public Row getHierarhyParent()
        {
            return _parent;
        }

        public Row(IEntity entity, Row parent)
        {
            super(entity);
            _parent = parent;
        }

        @Override
        public boolean equals(Object obj)
        {
            return obj instanceof Row && getId().equals(((Row) obj).getId());
        }
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> structureViewDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                TrJournal journal = context.get(TR_JOURNAL_PARAM);
                if (journal == null)
                    return new DSOutput(input);

                DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                        new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "e")
                                .joinEntity("e", DQLJoinType.left, SakaiTrJournalEventRel.class, "rel",
                                            eq(property("e.id"), property("rel", SakaiTrJournalEventRel.event())))
                                .joinPath(DQLJoinType.left, SakaiTrJournalEventRel.courseRelation().fromAlias("rel"), "jrel")
                                .joinPath(DQLJoinType.left, SakaiCourse2PpsEntryRel.sakaiCourse().fromAlias("jrel"), "c")
                                .where(eq(property("e", TrJournalEvent.journalModule().journal()), value(journal)))
                                .where(notInstanceOf("e", TrEventAddon.class)) // нужны только основные события
                                .order(property("e", TrJournalEvent.P_NUMBER))
                );
                int event_col_idx = dql.column("e");
                int course_title_col_idx = dql.column(property("c", SakaiCourse.title()));
                int bb_event_name_col_idx = dql.column(property("rel", SakaiTrJournalEventRel.P_SAKAI_EVENT_NAME));

                List<Object[]> items = dql.getDql().createStatement(context.getSession()).list();
                List<Row> wrappers = new ArrayList<>();
                for (Object[] item : items)
                {
                    Row parentRow = new Row(((TrJournalEvent)item[event_col_idx]).getJournalModule(), null);
                    parentRow.setProperty(SAKAI_DISABLED_EDIT_VIEW_PROPERTY, true);
                    parentRow.setProperty(SAKAI_DISABLED_DELETE_VIEW_PROPERTY, true);
                    int idx = wrappers.indexOf(parentRow);
                    if (idx < 0)
                        wrappers.add(parentRow);
                    else
                        parentRow = wrappers.get(idx);

                    Row eventRow = new Row(((TrJournalEvent)item[event_col_idx]), parentRow);

                    String courseTitle = StringUtils.trimToEmpty((String) item[course_title_col_idx]);
                    String eventTitle = StringUtils.trimToEmpty((String) item[bb_event_name_col_idx]);
                    eventRow.setProperty(SAKAI_COURSE_TITLE_VIEW_PROPERTY, courseTitle);
                    eventRow.setProperty(SAKAI_EVENT_NAME_VIEW_PROPERTY, eventTitle);
                    eventRow.setProperty(SAKAI_DISABLED_EDIT_VIEW_PROPERTY, false);
                    eventRow.setProperty(SAKAI_DISABLED_DELETE_VIEW_PROPERTY, StringUtils.isEmpty(eventTitle));

                    wrappers.add(eventRow);
                }

                return ListOutputBuilder.get(input, wrappers).pageable(false).build();
            }
        };
    }
}