/* $Id$ */
package ru.tandemservice.unitjsakai.ws.logic;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unitjsakai.ws.wrappers.WsOrderWrap;
import ru.tandemservice.unitjsakai.ws.wrappers.WsStudentWrap;


import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 01.12.2015
 */
public interface ISakaiServiceManager
{
    SpringBeanCache<ISakaiServiceManager> instance = new SpringBeanCache<>(ISakaiServiceManager.class.getName());

    List<WsStudentWrap> getStudentListByLogin(String login);

    String getReportHTML(String studentId);

    List<WsOrderWrap> getWsOrders(String studentId);

}