/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.ui.EditCourseEventRelation;

/**
 * @author Ekaterina Zvereva
 * @since 18.12.2015
 */
public class SakaiEventWrapper
{

    private String _assignmentId;
    private String _assignmentTitle;
    private String _siteId;

    public SakaiEventWrapper(String siteId, String taskId, String taskName)
    {
        setSiteId(siteId);
        setAssignmentId(taskId);
        setAssignmentTitle(taskName);
    }

    public SakaiEventWrapper() {}

    public String getAssignmentId()
    {
        return _assignmentId;
    }

    public void setAssignmentId(String assignmentId)
    {
        _assignmentId = assignmentId;
    }

    public String getAssignmentTitle()
    {
        return _assignmentTitle;
    }

    public void setAssignmentTitle(String assignmentTitle)
    {
        _assignmentTitle = assignmentTitle;
    }

    public String getSiteId()
    {
        return _siteId;
    }

    public void setSiteId(String siteId)
    {
        _siteId = siteId;
    }
}