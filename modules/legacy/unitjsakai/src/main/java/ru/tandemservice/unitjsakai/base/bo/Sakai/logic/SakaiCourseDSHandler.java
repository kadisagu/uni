/* $Id$ */
package ru.tandemservice.unitjsakai.base.bo.Sakai.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse;
import ru.tandemservice.unitjsakai.entity.sakai.SakaiCourse2PpsEntryRel;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;


import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 09.12.2015
 */
public class SakaiCourseDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String TR_JOURNAL = "regElement";
    public static final String PPS_ENTRY_FILTER_PARAM = "ppsEntry";
    public static final String COURSE_ID_FILTER_PARAM = "courseSakaiId";

    private boolean _isCombo = false;

    public static final IdentifiableWrapper YES_ITEM = new IdentifiableWrapper(1L, "Да");

    public SakaiCourseDSHandler(String ownerId, boolean isCombo)
    {
        super(ownerId, SakaiCourse2PpsEntryRel.class);
        _isCombo = isCombo;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        TrJournal journal = context.get(TR_JOURNAL);
        PpsEntry ppsEntry = context.get(PPS_ENTRY_FILTER_PARAM);

        if (ppsEntry == null)
            return new DSOutput(input);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SakaiCourse2PpsEntryRel.class, "s")
                .joinPath(DQLJoinType.inner, SakaiCourse2PpsEntryRel.sakaiCourse().fromAlias("s"), "course");

        if (_isCombo)
            builder.column(property("course"));
        else
            builder.column(property("s"));

        // Для выпадающих списков
        Set keys = input.getPrimaryKeys();
        if (keys != null && !keys.isEmpty())
        {
            if (keys.size() == 1)
                builder.where(eq(property("course.id"), commonValue(keys.iterator().next())));
            else
                builder.where(in("course.id", keys));
        }

        String idFilter = context.get(COURSE_ID_FILTER_PARAM);
        if (idFilter != null)
            builder.where(likeUpper(property("course", SakaiCourse.sakaiCourseId()), value(CoreStringUtils.escapeLike(idFilter))));

        //фильтр по дисциплине
        if (journal != null)
            builder.where(eq(property("s", SakaiCourse2PpsEntryRel.trJournal()), value(journal)));

        //фильтр по преподавателю
        builder.where(eq(property("s", SakaiCourse2PpsEntryRel.ppsEntry()), value(ppsEntry)));


        if (input.getEntityOrder() != null)
            builder.order(property("s", input.getEntityOrder().getKeyString()), input.getEntityOrder().getDirection());
        else
        {
            // Для выпадающих списков
            builder.order(property("course", SakaiCourse.title()));
            builder.order(property("course", SakaiCourse.sakaiCourseId()));
        }

        // Для выпадающих списков
        if (input.getCountRecord() == 0)
            input.setCountRecord(50);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}