package ru.tandemservice.unitjsakai.ws.logic.wrap;

import java.util.ArrayList;
import java.util.List;


/**
 * Академические группы студента
 */
public class GroupsType {

    private List<GroupType> group;

    public List<GroupType> getGroup() {
        if (group == null) {
            group = new ArrayList<>();
        }
        return this.group;
    }

}
