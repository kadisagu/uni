/* $Id$ */
package ru.tandemservice.unitjsakai.ws;


import ru.tandemservice.unitjsakai.ws.wrappers.WsOrderWrap;
import ru.tandemservice.unitjsakai.ws.wrappers.WsStudentWrap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 01.12.2015
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT)
public interface ISakaiConnectionService
{

        /**
         * Список студентов по логину
         */
        @WebMethod
        @WebResult(name = "studentWrap")
        List<WsStudentWrap> getStudentsByLogin(@WebParam(name = "studentParam") String studentParam) throws Exception;

        /**
         * Данные о приказах
         * @throws Exception
         */
        @WebMethod
        @WebResult(name = "orderWrap")
        List<WsOrderWrap> getWsOrdersForStudent(@WebParam(name = "studentId") String studentId) throws Exception;

        /**
         * Данные об успеваемости
         */
        @WebMethod
        String getReportContent(@WebParam(name = "studentId") String studentId);


        /**
         * Данные рейтинга
         */
        @WebMethod
        String getRatingContent(@WebParam(name = "studentId") String studentId);
}