/* $Id$ */
package ru.tandemservice.unitjsakai.ws;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.unitjsakai.ws.logic.IRatingServiceManager;
import ru.tandemservice.unitjsakai.ws.logic.ISakaiServiceManager;
import ru.tandemservice.unitjsakai.ws.wrappers.WsOrderWrap;
import ru.tandemservice.unitjsakai.ws.wrappers.WsStudentWrap;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;


/**
 * @author Ekaterina Zvereva
 * @since 29.11.2015
 */
@WebService(serviceName="SakaiConnectionService")
public class SakaiConnectionService implements ISakaiConnectionService
{

    /**
     * Список студентов по логину
     */
    @Override
    @WebMethod
    @WebResult(name = "studentWrap")
    public List<WsStudentWrap> getStudentsByLogin(@WebParam(name = "studentParam") String login) throws Exception
    {
        if (!StringUtils.isEmpty(login))
            return ISakaiServiceManager.instance.get().getStudentListByLogin(login);
        return null;
    }

    @Override
    @WebMethod
    @WebResult(name = "orderWrap")
    public List<WsOrderWrap> getWsOrdersForStudent(@WebParam(name = "studentId") String studentId) throws Exception {
        if (StringUtils.isNotEmpty(studentId)) {
            return ISakaiServiceManager.instance.get().getWsOrders(studentId);
        }

        return null;
    }


    @WebMethod
    public String getReportContent(@WebParam(name = "studentId") String studentId)
    {
        if (StringUtils.isNotEmpty(studentId))
            return ISakaiServiceManager.instance.get().getReportHTML(studentId);

        return null;
    }

    @Override
    @WebMethod
    public String getRatingContent(@WebParam(name = "studentId") String studentId)
    {
        if (StringUtils.isNotEmpty(studentId))
            return IRatingServiceManager.instance.get().getStudentRatingForSakai(studentId);

        return null;
    }

}