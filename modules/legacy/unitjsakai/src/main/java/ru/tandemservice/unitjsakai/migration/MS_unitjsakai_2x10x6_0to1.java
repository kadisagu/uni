package ru.tandemservice.unitjsakai.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unitjsakai_2x10x6_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность sakaiCourse

		// создано обязательное свойство userInGroup
		{
			// создать колонку
			tool.createColumn("sakaicourse_t", new DBColumn("useringroup_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultUserInGroup = false;
			tool.executeUpdate("update sakaicourse_t set useringroup_p=? where useringroup_p is null", defaultUserInGroup);

			// сделать колонку NOT NULL
			tool.setColumnNullable("sakaicourse_t", "useringroup_p", false);

		}


    }
}