/* $Id$ */
package ru.tandemservice.unitjsakai.ws;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uni.UniProperties;
import ru.tandemservice.unitjsakai.ws.logic.loginService.SakaiLoginService;
import ru.tandemservice.unitjsakai.ws.logic.loginService.SakaiLoginServiceLocator;
import ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutorService;
import ru.tandemservice.unitjsakai.ws.logic.tutor.SakaiTutorServiceLocator;


/**
 * @author Ekaterina Zvereva
 * @since 15.12.2015
 */
public class SakaiTutorGetter
{

    private static SakaiTutorService _sakaiTutorService = null;
    private static SakaiLoginService _sakaiLoginService = null;

        public static SakaiTutorService getTutorService() throws Exception
        {
            //оставлено для отладки
            String path = ApplicationRuntime.getProperty(UniProperties.SAKAI_HOST);

            if (_sakaiTutorService != null)
                return _sakaiTutorService;
            else
            {
                _sakaiTutorService = new SakaiTutorServiceLocator();
                return _sakaiTutorService;
            }

        }


        public static SakaiLoginService getLoginService() throws Exception
        {
            if (_sakaiLoginService != null)
                return _sakaiLoginService;
            else
            {
                _sakaiLoginService = new SakaiLoginServiceLocator();
                return _sakaiLoginService;
            }
        }


    public static String loginToSakay() throws Exception
    {
        String sessionId = "";
        try {
            sessionId = getLoginService().getSakaiLogin().login(ApplicationRuntime.getProperty(UniProperties.SAKAI_LOGIN), ApplicationRuntime.getProperty(UniProperties.SAKAI_PASSWORD));
        }
        catch (Exception ex) {
            throw ex;
        }
        return sessionId;
    }

}