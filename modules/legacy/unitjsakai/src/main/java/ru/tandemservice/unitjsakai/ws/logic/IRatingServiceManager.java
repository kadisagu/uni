/* $Id$ */
package ru.tandemservice.unitjsakai.ws.logic;

import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author Ekaterina Zvereva
 * @since 07.12.2015
 */
public interface IRatingServiceManager
{
    SpringBeanCache<IRatingServiceManager> instance = new SpringBeanCache<>(IRatingServiceManager.class.getName());

    String getStudentRatingForSakai(String studentId);
}