package ru.tandemservice.unisakai.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unisakai.entity.catalog.gen.*;

/** @see ru.tandemservice.unisakai.entity.catalog.gen.UniSakaiTemplateDocumentGen */
public class UniSakaiTemplateDocument extends UniSakaiTemplateDocumentGen implements ITemplateDocument
{
        public byte[] getContent() {
            return CommonBaseUtil.getTemplateContent(this);
        }

}