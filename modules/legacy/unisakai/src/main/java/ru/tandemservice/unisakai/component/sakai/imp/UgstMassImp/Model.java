package ru.tandemservice.unisakai.component.sakai.imp.UgstMassImp;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.UserSakai;

import java.util.*;


@Input(
        {
                @Bind(key = "ids", binding = "regpartIds", required = true),
                @Bind(key = "idYear", binding = "idYear", required = true),
                @Bind(key = "idPart", binding = "idPart", required = true)
        }
)
public class Model
{
    public static final String MUTEX = "lockString";
    private boolean processEnd = false;
    private IDataSettings _settings;
    private boolean processTi;
    private boolean processSt;

    private ISelectModel _developFormListModel;
    private ISelectModel _developTechListModel;


    private List<Long> regpartIds = Collections.emptyList();
    private Long idYear;
    private Long idPart;


    private ISelectModel yearModel;
    private List<HSelectOption> partsList;

    private String title = "";

    private Map<String, UserSakai> sakaiUsers = null;
    private Map<String, List<SiteSakai>> sakaiSites = null;

    public ISelectModel getYearModel()
    {
        return this.yearModel;
    }

    public void setYearModel(final ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public List<HSelectOption> getPartsList()
    {
        return this.partsList;
    }

    public void setPartsList(final List<HSelectOption> partsList)
    {
        this.partsList = partsList;
    }

    public void setDevelopFormListModel(ISelectModel _developFormListModel) {
        this._developFormListModel = _developFormListModel;
    }

    public ISelectModel getDevelopFormListModel() {
        return _developFormListModel;
    }

    public void setDevelopTechListModel(ISelectModel _developTechListModel) {
        this._developTechListModel = _developTechListModel;
    }

    public ISelectModel getDevelopTechListModel() {
        return _developTechListModel;
    }


    public void setRegpartIds(List<Long> regpartIds) {
        this.regpartIds = regpartIds;
    }

    public List<Long> getRegpartIds() {
        return regpartIds;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }


    protected void setYear(EducationYear value)
    {
        this.getSettings().set("year", value);
    }

    public EducationYear getYear()
    {
        final Object year = this.getSettings().get("year");
        return year instanceof EducationYear ? (EducationYear) year : null;
    }

    public YearDistributionPart getPart()
    {
        final Object part = this.getSettings().get("part");
        return part instanceof YearDistributionPart ? (YearDistributionPart) part : null;
    }

    protected void setPart(YearDistributionPart value)
    {
        this.getSettings().set("part", value);
    }

    public void setIdYear(Long idYear) {
        this.idYear = idYear;
    }

    public Long getIdYear() {
        return idYear;
    }

    public void setIdPart(Long idPart) {
        this.idPart = idPart;
    }

    public Long getIdPart() {
        return idPart;
    }

    public void setProcessTi(boolean processTi) {
        this.processTi = processTi;
    }

    public boolean getProcessTi() {
        return processTi;
    }

    public void setProcessSt(boolean processSt) {
        this.processSt = processSt;
    }

    public boolean getProcessSt() {
        return processSt;
    }

    public void setProcessEnd(boolean processEnd) {
        this.processEnd = processEnd;
    }

    public boolean getProcessEnd() {
        return processEnd;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void AddTitle(String msg)
    {
        this.title += msg;
    }

    @SuppressWarnings("rawtypes")
    public List<DevelopForm> getDevelopFormSelected()
    {
        List<DevelopForm> retVal = new ArrayList<DevelopForm>();
        Object developFormList = _settings.get("developFormList");

        if (developFormList instanceof Collection) {
            if (!((Collection) developFormList).isEmpty()) {
                for (Object obj : (Collection) developFormList) {
                    DevelopForm ent = (DevelopForm) obj;
                    if (ent != null)
                        retVal.add(ent);
                }
            }
        }
        return retVal;
    }

    @SuppressWarnings("rawtypes")
    public List<DevelopTech> getDevelopTechSelected()
    {
        List<DevelopTech> retVal = new ArrayList<DevelopTech>();
        Object developTechList = _settings.get("developTechList");

        if (developTechList instanceof Collection) {
            if (!((Collection) developTechList).isEmpty()) {
                for (Object obj : (Collection) developTechList) {
                    DevelopTech ent = (DevelopTech) obj;
                    if (ent != null)
                        retVal.add(ent);
                }
            }
        }
        return retVal;
    }

    public void setSakaiUsers(Map<String, UserSakai> sakaiUsers) {
        this.sakaiUsers = sakaiUsers;
    }

    public Map<String, UserSakai> getSakaiUsers() {
        return sakaiUsers;
    }

    public void setSakaiSites(Map<String, List<SiteSakai>> sakaiSites) {
        this.sakaiSites = sakaiSites;
    }

    public Map<String, List<SiteSakai>> getSakaiSites() {
        return sakaiSites;
    }

    public void setStrongListTi(boolean strongListTi)
    {
        this.getSettings().set("strongListTi", strongListTi);
    }

    public boolean getStrongListTi()
    {
        final Object val = this.getSettings().get("strongListTi");
        if (val != null)
            return (Boolean) val;
        else
            return false;
    }

    public void setStrongListSt(boolean strongListSt)
    {
        this.getSettings().set("strongListSt", strongListSt);

    }

    public boolean getStrongListSt()
    {
        final Object val = this.getSettings().get("strongListSt");
        if (val != null)
            return (Boolean) val;
        else
            return false;
    }

    public void setFiterInvert(boolean fiterInvert)
    {
        this.getSettings().set("fiterInvert", fiterInvert);

    }

    public boolean getFiterInvert()
    {
        final Object val = this.getSettings().get("fiterInvert");
        if (val != null)
            return (Boolean) val;
        else
            return false;
    }


    public boolean getDeleteWithMark()
    {
        final Object val = this.getSettings().get("deleteWithMark");
        if (val != null)
            return (Boolean) val;
        else
            return false;
    }

    public void setDeleteWithMark(boolean fiterInvert)
    {
        this.getSettings().set("deleteWithMark", fiterInvert);

    }


}
