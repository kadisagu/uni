package ru.tandemservice.unisakai.component.sakai.imp.TeacherImp;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;

import java.util.List;

public class DAO extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.DAO implements IDAO
{

    @Override
    public List<WrapperImp> getListData(ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model)
    {
        // нужен список преподавателей, с которым будем работать
        return getListDataTeacher(model);
    }

    @Override
    public void prepare(ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model)
    {
        super.prepare(model);

        // теперь доп фильтр
        ((Model)model).setOrgUnitListModel(new LazySimpleSelectModel<>(getOrgUnitListForEmployeePost()).setSortProperty(OrgUnit.P_FULL_TITLE));
    }
}
