package ru.tandemservice.unisakai.component.sakai.imp.UgstImp;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupRowGen;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;

import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon;
import ru.tandemservice.unisakai.entity.catalog.codes.SakaiEntityStateCodes;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.Tools;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.DAO implements IDAO
{
    @Override
    public List<WrapperImp> getListData(ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model)
    {
        return getListDataUgs((Model)model);
    }

    @Override
    public EppRegistryElementPart getEppPartById(Long id)
    {
        return get(EppRegistryElementPart.class, id);
    }

    @Override
    public void prepare(ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model modelBase)
    {
        Model model = (Model) modelBase;
        //org.codehaus.groovy.runtime.callsite.PogoGetPropertySite;

        // только если нет модели годов
        if (model.getYearModel() == null) {
            // учебные года только из реальных групп
            model.setYearModel(new EducationYearModel() {
                @Override
                protected DQLSelectBuilder query(String alias, String filter)
                {
                    DQLSelectBuilder eduYear = new DQLSelectBuilder()
                            .fromEntity(EppRealEduGroupRow.class, alias)
                            .column(property(EppRealEduGroupRowGen.group().summary().yearPart().year().educationYear().id().fromAlias(alias)))
                            .distinct();


                    final DQLSelectBuilder dql = new DQLSelectBuilder();
                    dql.fromEntity(EducationYear.class, alias);
                    dql.where(in(property(EducationYear.id().fromAlias(alias)), eduYear.buildQuery()));
                    dql.order(property(EducationYear.intValue().fromAlias(alias)));
                    return dql;
                }
            });
        }

        String _selKey = "";
        if (model.getYear() != null)
            _selKey = model.getYear().getTitle();

        if (model.getPartsList() == null || (!_selKey.equals(model.getLastYearKey()))) {
            // части учебных годов
            DQLSelectBuilder partsDQL = new DQLSelectBuilder()
                    .fromEntity(EppRealEduGroupRow.class, "obj")
                    .column(property(EppRealEduGroupRowGen.group().summary().yearPart().part().fromAlias("obj")))
                    .distinct();

            if (model.getYear() != null)
                partsDQL.where(eq(property(EppRealEduGroupRowGen.group().summary().yearPart().year().educationYear().fromAlias("obj")), value(model.getYear())));

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(YearDistributionPart.class, "part")
                    .column("part")
                    .where(in(property(YearDistributionPart.id().fromAlias("part")), partsDQL.buildQuery()))
                    .order(property(YearDistributionPart.code().fromAlias("part")));

            Set<YearDistributionPart> parts = new LinkedHashSet<>(dql.createStatement(getSession()).<YearDistributionPart>list());
            model.setPartsList(HierarchyUtil.listHierarchyNodesWithParents(new ArrayList<IHierarchyItem>(parts), false));
            model.setLastYearKey(_selKey);
        }

        super.prepare(model);
    }

    /**
     * Список УГС
     */
    @Override
    public List<WrapperImp> getListDataUgs(Model model)
    {
        IDataSettings settings = model.getSettings();
        List<WrapperImp> base = getBaseWrapper(model, settings);

        List<WrapperImp> retVal = new ArrayList<>();
        retVal.addAll(base);

        // ищем сайты, которых нет в тандеме
        if (model.getSakaisites() != null) {
            // первичные ключи дисциплин
            List<Long> lstEmpty = model.getSakaiIdDiscipline(Tools.PREF_SITE_UGS1);
            List<String> sakaiExistKey = model.getSakaiSiteExistKey(Tools.PREF_SITE_UGS1);

            if (lstEmpty.size() > 0) {
                // теперь для сайтов дисциплин учитываем контрольное мероприятие


                // получим список EppRegistryElementPart и именно его посравниваем
                DQLSelectBuilder dqlDisExist = new DQLSelectBuilder();
                dqlDisExist.fromEntity(EppRegistryElementPartFControlAction.class, "reg");

                dqlDisExist.column(property(EppRegistryElementPartFControlAction.part().registryElement().id().fromAlias("reg")));
                dqlDisExist.column(property(EppRegistryElementPartFControlAction.part().number().fromAlias("reg")));

                // code от контрольного мероприятия
                dqlDisExist.column(property(EppRegistryElementPartFControlAction.controlAction().code().fromAlias("reg")));

                dqlDisExist.where(in(property(EppRegistryElementPartFControlAction.part().registryElement().id().fromAlias("reg")), lstEmpty));


                List<Object[]> hasList = dqlDisExist.createStatement(getSession()).list();

                for (Object[] objs : hasList) {
                    Long idRegElem = (Long) objs[0];
                    Integer partNum = (Integer) objs[1];
                    String cactionType = (String) objs[2];

                    String siteId = Tools.getSiteSakaaiId(idRegElem, partNum, cactionType);
                    sakaiExistKey.remove(siteId);
                }

                // все что осталось в sakaiExistKey не существует в тандеме
                SakaiEntityState state = model.getStateMap().get("05");
                int idVrem = 0;

                for (String key : sakaiExistKey) {
                    List<SiteSakai> sites = model.getSakaisites().get(key);

                    for (SiteSakai site : sites) {
                        idVrem++;
                        WrapperImp wr = new WrapperImp();
                        wr.setId(makeFaleId(idVrem));
                        wr.setEntityType("Site");
                        wr.setDescription(site.getFullTitle());
                        wr.setTag(site);
                        wr.setName(site.getShortdesc());
                        wr.setSakaiState(state);
                        retVal.add(wr);
                    }
                }

            }
        }
        return applySakaiStateAndNameFilter(retVal, settings);
    }

    @SuppressWarnings("rawtypes")
    private List<WrapperImp> getBaseWrapper(Model model, IDataSettings settings)
    {
        String filterKey = "";

        if (model.getYear() != null)
            filterKey += model.getYear().getTitle();
        if (model.getPart() != null)
            filterKey += model.getPart().getTitle();

        Collection collectionDevelopFormList = getDevelopFormList(settings);
        Collection collectionDevelopTechList = getDevelopTechList(settings);

        filterKey += getCollectionKey(collectionDevelopFormList);
        filterKey += getCollectionKey(collectionDevelopTechList);

        if (model.getSettings().get("name") != null)
            filterKey += model.getSettings().get("name");

        if (model.getSettings().get("number") != null)
            filterKey += model.getSettings().get("number");

        if (model.getWrapperImpList() != null && model.getFilterKey().equals(filterKey))
            return model.getWrapperImpList();
        else {

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EppRealEduGroupRow.class, "edugrp")
                    .column(property(EppRealEduGroupRowGen.group().activityPart().id().fromAlias("edugrp")));

            dql.joinPath(DQLJoinType.inner, EppRealEduGroupRowGen.studentWpePart().studentWpe().sourceRow().workPlan().fromAlias("edugrp"), "wpbase");
            // приджойним
            dql.joinEntity("wpbase", DQLJoinType.inner, EppWorkPlan.class, "wp", DQLExpressions.eq(
                                   DQLExpressions.property("wpbase.id"),
                                   DQLExpressions.property("wp.id"))
            );

//TODO DEV-6870
//			  if (collectionDevelopTechList!=null)
//		    	   dql.where(in(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().developTech().fromAlias("wp")), collectionDevelopTechList ));
//
//			  if (collectionDevelopFormList!=null)
//				  dql.where(in(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().developForm().fromAlias("wp")), collectionDevelopFormList ));

            // фильтр по годам
            if (model.getYear() != null)
                dql.where(eq(property(EppRealEduGroupRowGen.group().summary().yearPart().year().educationYear().id().fromAlias("edugrp")), value(model.getYear().getId())));

            // фильтр по частям
            if (model.getPart() != null)
                dql.where(eq(property(EppRealEduGroupRowGen.group().summary().yearPart().part().id().fromAlias("edugrp")), value(model.getPart().getId())));


            // вытащим базовую запись реестра дисциплины

            // вынимаем сами дисциплины
            DQLSelectBuilder dqlDis = new DQLSelectBuilder();

            dqlDis.fromEntity(EppRegistryElementPartFControlAction.class, "regca");
            dqlDis.joinPath(DQLJoinType.inner, EppRegistryElementPartFControlAction.part().fromAlias("regca"), "reg");

            //dqlDis.fromEntity(EppRegistryElementPart.class, "reg");
            dqlDis.joinPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("reg"), "regbase");

            // приджойним указание на эталонный сайт
            dqlDis.joinEntity("reg", DQLJoinType.left, UniSakaiSiteEtalon.class, "etalon", DQLExpressions.and(
                    DQLExpressions.eq(DQLExpressions.property(EppRegistryElementPart.registryElement().id().fromAlias("reg")), DQLExpressions.property(UniSakaiSiteEtalon.registryElement().id().fromAlias("etalon"))),
                    DQLExpressions.eq(DQLExpressions.property(EppRegistryElementPart.number().fromAlias("reg")), DQLExpressions.property(UniSakaiSiteEtalon.part().fromAlias("etalon")))
            ));


            // фильтр по имени
            if (model.getName() != null)
                dqlDis.where(
                        likeUpper(property(EppRegistryElement.shortTitle().fromAlias("regbase"))
                                , value(("%" + model.getName() + "%").toUpperCase())));


            // фильтр по номеру дисциплины
            if (model.getNumber() != null)
                dqlDis.where(
                        likeUpper(property(EppRegistryElement.number().fromAlias("regbase"))
                                , value(("%" + model.getNumber() + "%").toUpperCase())));


            // PartFControlAction id
            dqlDis.column(property(EppRegistryElementPartFControlAction.id().fromAlias("regca")));

            // part number
            dqlDis.column(property(EppRegistryElementPart.number().fromAlias("reg")));

            // eppregelemId
            dqlDis.column(property(EppRegistryElementPart.registryElement().id().fromAlias("reg")));

            // eppregelem fullTitle
            dqlDis.column(property(EppRegistryElementPart.registryElement().fullTitle().fromAlias("reg")));
            // eppregelem shortTitle
            dqlDis.column(property(EppRegistryElementPart.registryElement().shortTitle().fromAlias("reg")));
            // эталон
            dqlDis.column(property(UniSakaiSiteEtalon.etalonSiteId().fromAlias("etalon")));
            // номер дисциплины
            dqlDis.column(property(EppRegistryElementPart.registryElement().number().fromAlias("reg")));

            // тип контрольного мероприятия
            dqlDis.column(property(EppRegistryElementPartFControlAction.controlAction().code().fromAlias("regca")));

            // название контрольного мероприятия
            // dqlDis.column(property(EppRegistryElementPartFControlAction.controlAction().title().fromAlias("regca")));


            dqlDis.where(in(property(EppRegistryElementPart.id().fromAlias("reg")), dql.buildQuery()));

            //dqlDis.order(property(EppRegistryElementPart.registryElement().fullTitle().fromAlias("reg")));
            //dqlDis.order(property(EppRegistryElementPart.number().fromAlias("reg")));
            List<Object[]> regList = dqlDis.createStatement(getSession()).list();
            List<WrapperImp> retVal = new ArrayList<>();

            // список уникальных значений
            for (Object[] objs : regList) {
                Long PartFControlAction = (Long) objs[0];
                int partNumber = (Integer) objs[1];
                Long eppRegElemId = (Long) objs[2];
                String fullTitle = (String) objs[3];
                String shortTitle = (String) objs[4];
                String numberEpp = (String) objs[6];

                String cactionType = (String) objs[7];
                // String cactionTypeTitle = (String) objs[8];


                String etalonName = null;

                if (objs[5] != null)
                    etalonName = (String) objs[5];

                String advDsk = "";
                if (etalonName != null)
                    advDsk += "( эталон: " + etalonName + ")";

                // EppRegistryElement regElem = get(EppRegistryElement.class, eppRegElemId);

                SiteSakai site = Tools.getSiteSakai(eppRegElemId, fullTitle, shortTitle, PartFControlAction, partNumber, cactionType);

                if (model.getSakaisites() == null)//с сакаем связи нет
                {
                    SakaiEntityState state = model.getStateMap().get("04");
                    WrapperImp wr = new WrapperImp();
                    wr.setId(PartFControlAction);
                    wr.setEntityType("Дисциплина");
                    wr.setDescription(site.getFullTitle() + advDsk);
                    wr.setTag(site);
                    wr.setName(site.getShortdesc() + " №" + numberEpp);
                    wr.setSakaiState(state);
                    retVal.add(wr);
                }
                else {
                    if (model.getSakaisites().containsKey(site.getSiteLogicId())) {
                        List<SiteSakai> siteInSakais = model.getSakaisites().get(site.getSiteLogicId());
                        for (SiteSakai siteInSakai : siteInSakais) {
                            siteInSakai.setEntityId(site.getEntityId());
                            SakaiEntityState state;
                            if (siteInSakai.isEqualSite(site))
                                state = model.getStateMap().get(SakaiEntityStateCodes.PRESENT_IN_SAKAI);
                            else
                                state = model.getStateMap().get(SakaiEntityStateCodes.PRESENT_IN_SAKAI_NOT_EQUALS);

                            String warning = "";
                            if (siteInSakais.size() > 1)
                                warning = "Более одного сайта для ID ";

                            WrapperImp wr = new WrapperImp();
                            wr.setId(PartFControlAction);
                            wr.setEntityType("Дисциплина");
                            wr.setDescription(warning + siteInSakai.getFullTitle() + advDsk);
                            wr.setTag(siteInSakai);
                            wr.setName(warning + siteInSakai.getShortdesc() + " №" + numberEpp);

                            wr.setSakaiState(state);
                            retVal.add(wr);
                        }
                    }
                    else {

                        SakaiEntityState state = model.getStateMap().get("01");
                        WrapperImp wr = new WrapperImp();
                        wr.setId(PartFControlAction);
                        wr.setEntityType("Дисциплина");
                        wr.setDescription(site.getFullTitle() + advDsk);
                        wr.setTag(site);
                        wr.setName(site.getShortdesc() + " №" + numberEpp);
                        wr.setSakaiState(state);
                        retVal.add(wr);
                    }
                }
            }

            model.setFilterKey(filterKey);
            model.setWrapperImpList(retVal);

            // нужно отдать через копирование
            return retVal;
        }
    }

    @SuppressWarnings("rawtypes")
    private String getCollectionKey(Collection collectionList) {
        String retVal = "";

        if (collectionList != null) {
            for (Object obj : collectionList) {
                if (obj instanceof IEntity) {
                    retVal += Long.toString(((IEntity) obj).getId());
                }
            }
        }
        return retVal;
    }

}
