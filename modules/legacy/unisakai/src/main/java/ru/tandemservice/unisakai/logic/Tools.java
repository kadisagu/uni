package ru.tandemservice.unisakai.logic;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.w3c.dom.CharacterData;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisakai.component.sakai.SakaiGroupMark.MarkInfo;
import ru.tandemservice.unisakai.component.sakai.SakaiGroupMark.StudentWrap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.*;

public class Tools {
    public static String PREF_SITE_GROUP = "siteGrp";
    public static String PREF_SITE_UGS1 = "site:";
    public static String PREF_SITE_UGS2 = "part";
    public static String PREF_SITE_UGS3 = "caction";

    public static String PREF_TIME = "time";

    public static String getSiteSakaaiId(Long disciplineId, Integer part, String cactionCode)
    {
        return PREF_SITE_UGS1 + Long.toString(disciplineId) + PREF_SITE_UGS2 + ":" + Integer.toString(part) + PREF_SITE_UGS3 + ":" + cactionCode;
    }

    public static String getSiteSakaaiId(Long groupId)
    {
        return PREF_SITE_GROUP + ":" + Long.toString(groupId);
    }

    public static SiteSakai getSiteSakai(Group group)
    {
        String groupID = group.getId().toString();
        String groupName = group.getTitle();
        String course = group.getCourse().getTitle();

        String siteIdNew = PREF_SITE_GROUP + ":" + groupID;

        String title = "# " + group.getTitle() + " курс " + course;
        String description = "# " + groupName + " курс " + course;
        String shortDesc = groupName + " к " + course;


        SiteSakai retVal
                = new SiteSakai(addTimeInfoToIdSite(siteIdNew), title, description, shortDesc, groupID, course, "");

        retVal.setEntityId(group.getId());
        return retVal;
    }

    public static String addTimeInfoToIdSite(String siteId)
    {

        String tm = Long.toString(new Date().getTime());
        return siteId + PREF_TIME + ":" + tm;
    }

    /**
     * Нежелательный метод, работает медленно
     *
     * @param elem
     * @param part
     *
     * @return
     */
    public static SiteSakai getSiteSakaiByEntity(EppRegistryElement elem, Integer part, String cacctionCode)
    {
        return getSiteSakai(elem.getId(), elem.getFullTitle(), elem.getShortTitle(), null, part, cacctionCode);
    }

    public static SiteSakai getSiteSakaiByEntity(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction)
    {
        EppRegistryElement elem = eppRegistryElementPartFControlAction.getPart().getRegistryElement();

        return getSiteSakai(
                elem.getId()
                , elem.getFullTitle()
                , elem.getShortTitle()
                , null
                , eppRegistryElementPartFControlAction.getPart().getNumber()
                , eppRegistryElementPartFControlAction.getControlAction().getCode());
    }

    public static SiteSakai getSiteSakaiByEntity(EppRegistryElement elem, int part, String cactionCode)
    {
        return getSiteSakai(elem.getId(), elem.getFullTitle(), elem.getShortTitle(), null, part, cactionCode);
    }


    public static SiteSakai getSiteSakai(Long id, String fullTitle, String shortTitle, Long PartFControlAction, Integer part, String cactionCode)
    {
        String disciplineId = Long.toString(id);
        String disciplineName = fullTitle;
        String disciplinePart = Integer.toString(part);
        String disciplineShortName = shortTitle;

        String siteIdNew = PREF_SITE_UGS1 + disciplineId + PREF_SITE_UGS2 + ":" + disciplinePart + PREF_SITE_UGS3 + ":" + cactionCode;

        String title = SiteSakai.getShortName(disciplineShortName, disciplinePart, cactionCode);
        String description = SiteSakai.getFullName(disciplineName, disciplinePart, cactionCode);

        SiteSakai retVal = new SiteSakai(addTimeInfoToIdSite(siteIdNew), title, description, description, disciplineId, disciplinePart, cactionCode);
        retVal.setEntityId(PartFControlAction);
        return retVal;
    }


    public static String getStudentEid(Student st)
    {
        return getStudentEid(st.getPersonalNumber());
    }

    public static String getStudentEid(String personalNumber)
    {
        return UserSakai.PREF_STUDENT + personalNumber;
    }


    public static UserSakai getUser(PersonRole pr) {  //throws Exception
        if (pr instanceof Student) {
            return Tools.getUserStu((Student) pr);
        }
        else if (pr instanceof Employee) {
            return Tools.getUserEmp((Employee) pr);
        }
        else {
            //unknown user type
            return null;
            //throw new Exception("Tools.getUser(): Unknown type of parameter: " + pr.getClass().getSimpleName());
        }
    }

    /**
     * Возвращает eid персоны для Sakai
     *
     * @param pr      - персона
     * @param useLDAP - используется ли авторизация LDAP
     */
    public static String getPersonEid(PersonRole pr, boolean useLDAP) {
        if (pr == null) return "";
        if (useLDAP)
            return pr.getPrincipal().getLogin();
        else {
            UserSakai user = Tools.getUser(pr);
            return user.getEid();
        }
    }

    //Для построения полной карты eid по всем студентам, когда нет возможности передавать полный Student
    public static String getPersonEid(StudentWrap st, boolean useLDAP) {
        if (st == null) return "";
        if (useLDAP)
            return st.getPrincipalLogin();
        else {
            return UserSakai.PREF_STUDENT + st.getPesonalNumber();
        }
    }


    private static UserSakai getUserStu(Student st)
    {

        String _id = UserSakai.PREF_STUDENT + Long.toString(st.getId());
        String _eid = getStudentEid(st);

        String _firstName = st.getPerson().getIdentityCard().getFirstName();
        String _lastName = st.getPerson().getIdentityCard().getLastName();

        // контактную информацию берем у персоны
        String _email=null;
        if (st.getPerson().getContactData() != null)
            _email = st.getPerson().getContactData().getEmail();

        String _password = st.getPrincipal().getPasswordHash();

        UserSakai user = new UserSakai(_id, _eid, _email == null? "":_email, "", _firstName, _lastName);
        user.setPassword(_password);

        return user;
    }

    public static UserSakai getUserStudent(Long entityId, String personalNumber, String firstName, String lastName, String email, String pwd)
    {
        String _id = UserSakai.PREF_STUDENT + Long.toString(entityId);
        String _eid = getStudentEid(personalNumber);

        UserSakai user = new UserSakai(_id, _eid, email == null? "":email, "", firstName, lastName);
        user.setPassword(pwd);

        return user;
    }


    private static UserSakai getUserEmp(Employee st)
    {
        String pref = UserSakai.PREF_EMPLOYEE;

        String _id = pref + Long.toString(st.getId());
        String _eid = pref + st.getEmployeeCode();

        String _firstName = st.getPerson().getIdentityCard().getFirstName();
        String _lastName = st.getPerson().getIdentityCard().getLastName();
        String _email = "";
        if (st.getPerson().getContactData() != null)
            _email = st.getPerson().getContactData().getEmail();

        if (_email == null)
            _email = "";

        String _password = st.getPrincipal().getPasswordHash();

        UserSakai user = new UserSakai(_id, _eid, _email, "", _firstName, _lastName);
        user.setPassword(_password);

        return user;
    }

    public static Long MakeFakeId(int iter)
    {
        Calendar ca1 = Calendar.getInstance();
        Calendar ca2 = Calendar.getInstance();

        ca1.set(1970, 01, 01, 0, 0, 0);
        //ca2.set(2070, 01, 01, 0, 0, 0);
        ca2.add(Calendar.YEAR, 10);
        ca2.add(Calendar.SECOND, iter);

        Long delta = ca2.getTimeInMillis() - ca1.getTimeInMillis();
        Long delta_ss = delta / 1000;

        return delta_ss * 1048576000
                + ((((1 * 256) | 255) * 4096) | (9999 & 4095));

    }

    public static UserSakai getUserByEid(
            String eid
            , Map<String, UserSakai> users
    )
    {

        if (users == null) return null;
        for (String key : users.keySet()) {
            UserSakai u = users.get(key);
            if (u.getEid().equals(eid))
                return u;
        }
        return null;
    }

    public static String GetTargetNamespace(String xml) throws Exception
    {
        String ns = "";
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;

        db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("wsdl:definitions");
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);

            ns = element.getAttribute("targetNamespace");
        }
        return ns;
    }

    /**
     * Сайты из сакая по их ID
     *
     * @param xml
     *
     * @return
     *
     * @throws Exception
     */
    public static Map<String, SiteSakai> getSiteSakaiById(String xml) throws Exception
    {
        Map<String, SiteSakai> retVal = new HashMap<String, SiteSakai>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;

        db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("item");

        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);

            NodeList siteId = element.getElementsByTagName("siteId");
            String _siteId = getCharacterDataFromElement((Element) siteId.item(0));

            NodeList title = element.getElementsByTagName("title");
            String _title = getCharacterDataFromElement((Element) title.item(0));

            NodeList description = element.getElementsByTagName("description");
            String _description = getCharacterDataFromElement((Element) description.item(0));

            NodeList shortdesc = element.getElementsByTagName("shortdesc");
            String _shortdesc = getCharacterDataFromElement((Element) shortdesc.item(0));

            NodeList disciplineId = element.getElementsByTagName("disciplineId");
            String _disciplineId = getCharacterDataFromElement((Element) disciplineId.item(0));

            NodeList disciplinePart = element.getElementsByTagName("disciplinePart");
            String _disciplinePart = getCharacterDataFromElement((Element) disciplinePart.item(0));

            NodeList cactiontype = element.getElementsByTagName("cactiontype");
            String _cactiontype = getCharacterDataFromElement((Element) cactiontype.item(0));

            Map<String, List<String>> siteUserMap = new HashMap<String, List<String>>();
            NodeList siteMembersCollection = element.getElementsByTagName("members");
            if (siteMembersCollection.getLength() > 0) {
                NodeList siteMembers = ((Element) siteMembersCollection.item(0)).getElementsByTagName("user");
                for (int j = 0; j < siteMembers.getLength(); j++) {
                    Element member = (Element) siteMembers.item(j);
                    String memberEid = member.getAttribute("eid");
                    String role = member.getAttribute("role");
                    if (!siteUserMap.containsKey(role)) {
                        siteUserMap.put(role, new ArrayList<String>());
                    }
                    if (!"admin".equals(memberEid))
                        siteUserMap.get(role).add(memberEid);
                }
            }

            SiteSakai site = new SiteSakai(_siteId, _title, _description, _shortdesc, _disciplineId, _disciplinePart, _cactiontype);
            site.setSiteUserMap(siteUserMap);
            retVal.put(_siteId, site);
        }
        return retVal;
    }

    public static Map<String, List<SiteSakai>> getSiteSakaiByLogicId(String xml) throws Exception
    {
        Map<String, SiteSakai> map = getSiteSakaiById(xml);
        Map<String, List<SiteSakai>> retVal = new HashMap<>();

        for (SiteSakai ss : map.values()) {
            String logicId = ss.getSiteLogicId();
            if (retVal.containsKey(logicId))
                retVal.get(logicId).add(ss);
            else {
                ArrayList<SiteSakai> al = new ArrayList<>();
                al.add(ss);
                retVal.put(logicId, al);
            }
        }
        return retVal;
    }

    public static Map<String, UserSakai> getSakaiUsers(String xml) throws Exception
    {
        Map<String, UserSakai> retVal = new HashMap<>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;

        db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));

        Document doc = db.parse(is);


        NodeList nodes = doc.getElementsByTagName("item");

        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);

            NodeList userId = element.getElementsByTagName("userId");
            String _userId = getCharacterDataFromElement((Element) userId.item(0));

            NodeList eid = element.getElementsByTagName("eid");
            String _eid = getCharacterDataFromElement((Element) eid.item(0));

            NodeList email = element.getElementsByTagName("email");
            String _email = getCharacterDataFromElement((Element) email.item(0));

            NodeList type = element.getElementsByTagName("type");
            String _type = getCharacterDataFromElement((Element) type.item(0));

            NodeList firstName = element.getElementsByTagName("firstName");
            String _firstName = getCharacterDataFromElement((Element) firstName.item(0));

            NodeList lastName = element.getElementsByTagName("lastName");
            String _lastName = getCharacterDataFromElement((Element) lastName.item(0));

			           /* <TODO> Майк - выкини из xml почтовый адрес
			           NodeList postAddr = element.getElementsByTagName("postAddr");
			           String _postAddr = getCharacterDataFromElement((Element) postAddr.item(0));
			           */
            UserSakai user = new UserSakai(_userId, _eid, _email, _type, _firstName, _lastName);

            // меняем ключ с eid на первичный ключ
            retVal.put(_userId, user);
        }
        return retVal;
    }

    public static Map<String, MarkInfo> getSiteMark(String xml) throws Exception
    {
        Map<String, MarkInfo> retVal = new HashMap<>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;

        db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("item");

        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);

            String _grade = element.getAttribute("grade");
            String _userid = element.getAttribute("userid");

            MarkInfo mi = new MarkInfo(_grade, _userid);
            retVal.put(_userid, mi);
        }

        return retVal;
    }


    private static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }

    public static void addSiteToCollection(
            Map<String, List<SiteSakai>> map
            , SiteSakai site)
    {
        String eid = site.getSiteLogicId();
        if (map.containsKey(eid)) {
            // а может есть такой?
            boolean has = false;
            for (SiteSakai s : map.get(eid)) {
                if (s.getSiteid().equals(site.getSiteid()))
                    has = true;
            }
            if (!has)
                map.get(eid).add(site);
        }
        else {
            List<SiteSakai> lst = new ArrayList<>();
            lst.add(site);
            map.put(eid, lst);
        }

    }


    public static SiteSakai getSiteSakaiMaxTime(Map<String, List<SiteSakai>> map, SiteSakai site)
    {
        String eid = site.getSiteLogicId();
        return getSiteSakaiMaxTime(map, eid);
    }

    public static SiteSakai getSiteSakaiMaxTime(Map<String, List<SiteSakai>> map, String logicId)
    {
        if (map == null)
            return null;

        if (map.containsKey(logicId)) {
            long max = 0;
            List<SiteSakai> lst = map.get(logicId);
            SiteSakai retVal = null;

            for (SiteSakai s : lst) {
                long cmax = 1;
                if (s.getSiteLogicId().contains(PREF_TIME)) {
                    String _eid = s.getSiteLogicId();
                    cmax = Long.getLong(_eid.substring(_eid.indexOf(PREF_TIME) + PREF_TIME.length() + 1));
                }
                if (cmax >= max) {
                    max = cmax;
                    retVal = s;
                }
            }

            return retVal;

        }
        else
            return null;

    }

}
