package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisakai.logic.ActionSakai;
import ru.tandemservice.unisakai.logic.SiteSakai;

import java.util.List;
import java.util.Map;


public class Model {

    private ActionSakai actionSakai = null;
    private Map<String, List<SiteSakai>> sites = null;
    public static final String MUTEX = "lockString";

    private String title = "";

    private List<WrapperGM> lstWrap;

    private IDataSettings settings;

    private ISelectModel yearModel;
    private List<HSelectOption> partsList;

    private DynamicListDataSource<WrapperGM> dataSource;

    private boolean useLDAP = false;

    public DynamicListDataSource<WrapperGM> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(final DynamicListDataSource<WrapperGM> dataSource)
    {
        this.dataSource = dataSource;
    }


    public void addTitle(String string)
    {

        title += string;
    }


    public String getName()
    {
        if (this.getSettings().get("name") == null)
            return null;
        else
            return (String) this.getSettings().get("name");
    }


    public void setName(String name)
    {
        this.getSettings().set("name", name);
    }

    public String getNumber()
    {
        if (this.getSettings().get("number") == null)
            return null;
        else
            return (String) this.getSettings().get("number");
    }

    public void setNumber(String number)
    {
        this.getSettings().set("number", number);
    }

    protected EducationYear getYear()
    {
        final Object year = this.getSettings().get("year");
        return year instanceof EducationYear ? (EducationYear) year : null;
    }

    protected void setYear(EducationYear value)
    {
        this.getSettings().set("year", value);
    }

    protected YearDistributionPart getPart()
    {
        final Object part = this.getSettings().get("part");
        return part instanceof YearDistributionPart ? (YearDistributionPart) part : null;
    }

    protected void setPart(YearDistributionPart value)
    {
        this.getSettings().set("part", value);
    }


    public IDataSettings getSettings()
    {
        return this.settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        this.settings = settings;
    }


    public ISelectModel getYearModel()
    {
        return this.yearModel;
    }

    public void setYearModel(final ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public List<HSelectOption> getPartsList()
    {
        return this.partsList;
    }

    public void setPartsList(final List<HSelectOption> partsList)
    {
        this.partsList = partsList;
    }

    public void setLstWrap(List<WrapperGM> lstWrap) {
        this.lstWrap = lstWrap;
    }

    public List<WrapperGM> getLstWrap() {
        return lstWrap;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }


    public void setActionSakai(ActionSakai actionSakai) {
        this.actionSakai = actionSakai;
    }

    public ActionSakai getActionSakai() {
        return actionSakai;
    }

    public void setSites(Map<String, List<SiteSakai>> sites) {
        this.sites = sites;
    }

    public Map<String, List<SiteSakai>> getSites() {
        return sites;
    }

    public boolean isUseLDAP() {
        return useLDAP;
    }

    public void setUseLDAP(boolean useLDAP) {
        this.useLDAP = useLDAP;
    }


}
