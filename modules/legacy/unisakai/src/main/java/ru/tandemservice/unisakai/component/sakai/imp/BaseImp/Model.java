package ru.tandemservice.unisakai.component.sakai.imp.BaseImp;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.logic.ActionSakai;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.UserSakai;

import javax.management.timer.Timer;
import java.util.*;

public abstract class Model
{

    public static final Object MUTEX = new Object();

    private ISelectModel _developFormListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _sakaiStateListModel;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _producingOrgUnitListModel;
    private ActionSakai actionSakai = null;
    private String title = "";
    private DynamicListDataSource<WrapperImp> dataSource;
    private Map<String, SakaiEntityState> stateMap = null;
    private int processedModule = 0;
    private IDataSettings _settings;
    private Date lastDate = new Date();

    private boolean useLDAP = false;

    private List<WrapperImp> wrapperImpList = null;
    private String filterKey = "";

    public Boolean isElaspend()
    {
        Date _elaspend = new Date(lastDate.getTime() + 15 * Timer.ONE_SECOND);
        return !(_elaspend.getTime() > (new Date()).getTime());

    }

    public void TimerClear()
    {
        lastDate = new Date();
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }


    public String getName()
    {
        if (getSettings().get("name") == null)
            return null;
        else
            return (String) getSettings().get("name");
    }

    public void setName(String name)
    {
        getSettings().set("name", name);
    }

    public void setDevelopFormListModel(ISelectModel _developFormListModel) {
        this._developFormListModel = _developFormListModel;
    }

    public ISelectModel getDevelopFormListModel() {
        return _developFormListModel;
    }

    public void setDevelopTechListModel(ISelectModel _developTechListModel) {
        this._developTechListModel = _developTechListModel;
    }

    public ISelectModel getDevelopTechListModel() {
        return _developTechListModel;
    }


    public void addTitle(String msg)
    {
        this.title += msg;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDataSource(DynamicListDataSource<WrapperImp> dataSource) {
        this.dataSource = dataSource;
    }

    public DynamicListDataSource<WrapperImp> getDataSource() {
        return dataSource;
    }

    public void setSakaiStateListModel(ISelectModel _sakaiStateListModel) {

        this._sakaiStateListModel = _sakaiStateListModel;
    }

    public ISelectModel getSakaiStateListModel() {
        return _sakaiStateListModel;
    }


    public void setStateMap(Map<String, SakaiEntityState> stateMap) {
        this.stateMap = stateMap;
    }

    public Map<String, SakaiEntityState> getStateMap() {
        return stateMap;
    }

    public void setProcessedModule(int processedModule) {
        this.processedModule = processedModule;
    }


    public int getProcessedModule() {
        return processedModule;
    }

    public void AddProcessed()
    {
        processedModule++;

    }


    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getProducingOrgUnitListModel()
    {
        return _producingOrgUnitListModel;
    }

    public void setProducingOrgUnitListModel(ISelectModel producingOrgUnitListModel)
    {
        _producingOrgUnitListModel = producingOrgUnitListModel;
    }

    private Map<String, UserSakai> sakaiusers = null;
    private Map<String, List<SiteSakai>> sakaisites = null;

    public void setSakaiusers(Map<String, UserSakai> sakaiusers) {
        this.sakaiusers = sakaiusers;
    }

    public Map<String, UserSakai> getSakaiusers() {
        return sakaiusers;
    }

    public List<Long> getSakaiIdUser(String pref)
    {

        List<Long> lst = new ArrayList<>();

        if (getSakaiusers() != null) {
            for (String key : getSakaiusers().keySet()) {
                UserSakai user = getSakaiusers().get(key);

                if (user.getId().startsWith(pref) || user.getId().startsWith("tmp_" + pref)) {
                    Long id = Long.parseLong(user.getId().replace(pref, ""));
                    lst.add(id);
                }
            }
        }


        return lst;
    }

    public List<Long> getSakaiIdDiscipline(String pref)
    {
        List<Long> lst = new ArrayList<Long>();

        if (getSakaisites() != null) {
            for (String key : getSakaisites().keySet()) {
                List<SiteSakai> lstSite = getSakaisites().get(key);

                for (SiteSakai site : lstSite) {
                    if (site.getSiteid().startsWith(pref)) {
                        if (site.getDisciplineId() != null && site.getDisciplineId().length() > 0) {
                            Long id = Long.parseLong(site.getDisciplineId());
                            lst.add(id);
                        }
                    }
                }
            }
        }
        return lst;
    }


    public List<String> getSakaiSiteExistKey(String pref)
    {
        List<String> lst = new ArrayList<String>();

        if (getSakaisites() != null) {
            for (String key : getSakaisites().keySet()) {
                if (key.startsWith(pref))
                    lst.add(key);
            }
        }
        return lst;
    }

    public List<UserSakai> getSakaiUserNotTandem(String pref, List<Long> lst)
    {

        List<UserSakai> retVal = new ArrayList<>();

        Map<String, UserSakai> map = new HashMap<String, UserSakai>();

        for (String key : getSakaiusers().keySet()) {
            map.put(key, getSakaiusers().get(key));
        }

        for (Long id : lst) {
            String key = pref + Long.toString(id);
            if (map.containsKey(key))
                map.remove(key);
        }

        for (String key : map.keySet()) {
            if (key.startsWith(pref))
                retVal.add(map.get(key));
        }
        return retVal;
    }

    public void setSakaisites(Map<String, List<SiteSakai>> sakaisites) {
        this.sakaisites = sakaisites;
    }

    public Map<String, List<SiteSakai>> getSakaisites() {
        return sakaisites;
    }

    public void setActionSakai(ActionSakai actionSakai) {
        this.actionSakai = actionSakai;
    }

    public ActionSakai getActionSakai() {
        return actionSakai;
    }

    public void setWrapperImpList(List<WrapperImp> wrapperImpList) {
        this.wrapperImpList = wrapperImpList;
    }

    public List<WrapperImp> getWrapperImpList() {
        return wrapperImpList;
    }

    public void setFilterKey(String filterKey) {
        this.filterKey = filterKey;
    }

    public String getFilterKey() {
        return filterKey;
    }

    public boolean isUseLDAP() {
        return useLDAP;
    }

    public void setUseLDAP(boolean useLDAP) {
        this.useLDAP = useLDAP;
    }

}
