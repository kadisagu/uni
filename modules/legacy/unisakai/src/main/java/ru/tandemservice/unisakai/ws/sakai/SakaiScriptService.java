/**
 * SakaiScriptService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unisakai.ws.sakai;

public interface SakaiScriptService extends javax.xml.rpc.Service {
    public String getSakaiScriptAddress();

    public ru.tandemservice.unisakai.ws.sakai.SakaiScript_PortType getSakaiScript() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unisakai.ws.sakai.SakaiScript_PortType getSakaiScript(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
