package ru.tandemservice.unisakai.component.sakai.imp.UgstPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisakai.dao.wrappers.WrapperStudentToSiteSakai;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.logic.ActionSakai;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.Tools;
import ru.tandemservice.unisakai.logic.UserSakai;
import ru.tandemservice.unisakai.util.sprav.KeyValues;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


@Input({@Bind(key = "idEppRegistryElementPartFControlAction", binding = "idEppRegistryElementPartFControlAction", required = true)})
public class Model {
    public static final String MUTEX = "lockString";

    private String title = "";
    private Long idEppRegistryElementPartFControlAction;
    private EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction;
    private ActionSakai actionSakai = null;
    private SiteSakai site = null;
    private Map<String, UserSakai> users = null;
    private Map<String, List<SiteSakai>> sites = null;
    private boolean processEnd = false;
    private DynamicListDataSource<WrapperStudentToSiteSakai> dataSource;
    private IDataSettings _settings;

    private ISelectModel yearModel;
    private List<HSelectOption> partsList;
    private ISelectModel developFormListModel;
    private ISelectModel developTechListModel;
    private ISelectModel sakaiStateListModel;

    private String sakaiSiteEtalon;

    private boolean useLDAP = false;


    public void setIdEppRegistryElementPartFControlAction(Long idEppRegistryElementPartFControlAction) {
        this.idEppRegistryElementPartFControlAction = idEppRegistryElementPartFControlAction;
    }

    public Long getIdEppRegistryElementPartFControlAction() {
        return idEppRegistryElementPartFControlAction;
    }

    public void setEppRegistryElementPartFControlAction(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction) {
        this.eppRegistryElementPartFControlAction = eppRegistryElementPartFControlAction;
    }

    public EppRegistryElementPartFControlAction getEppRegistryElementPartFControlAction() {
        return eppRegistryElementPartFControlAction;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    public void setDataSource(DynamicListDataSource<WrapperStudentToSiteSakai> dataSource) {
        this.dataSource = dataSource;
    }

    public DynamicListDataSource<WrapperStudentToSiteSakai> getDataSource() {
        return dataSource;
    }

    public String getSiteSakaiHref()
    {
        EppRegistryElementPartFControlAction epcat = getEppRegistryElementPartFControlAction();
        String siteLogicId = Tools.getSiteSakaaiId(epcat.getPart().getRegistryElement().getId(), epcat.getPart().getNumber(), epcat.getControlAction().getCode());

        SiteSakai site = Tools.getSiteSakaiMaxTime(getSites(), siteLogicId);

        String link = "";
        try {
            link = KeyValues.getStringValue("app.mailtemplate.link", true);
        }
        catch (Exception e) {
        }

        String siteHref = link + "/portal/site/";

        if (site != null)
            siteHref += site.getSiteid();
        return siteHref;
    }

    public void setActionSakai(ActionSakai actionSakai) {
        this.actionSakai = actionSakai;
    }

    public ActionSakai getActionSakai() {
        return actionSakai;
    }

    public void setSite(SiteSakai site) {
        this.site = site;
    }

    public SiteSakai getSite() {
        return site;
    }

    public void setUsers(Map<String, UserSakai> users) {
        this.users = users;
    }

    public Map<String, UserSakai> getUsers() {
        return users;
    }

    public void setSites(Map<String, List<SiteSakai>> sites) {
        this.sites = sites;
    }

    public Map<String, List<SiteSakai>> getSites() {
        return sites;
    }

    public void setProcessEnd(boolean processEnd) {
        this.processEnd = processEnd;
    }

    public boolean isProcessEnd() {
        return processEnd;
    }

    protected void setYear(EducationYear value)
    {
        this.getSettings().set("year", value);
    }

    // EducationYear // YearDistributionPart
    protected EducationYear getYear()
    {
        final Object year = this.getSettings().get("year");
        return year instanceof EducationYear ? (EducationYear) year : null;
    }

    protected YearDistributionPart getPart()
    {
        final Object part = this.getSettings().get("part");
        return part instanceof YearDistributionPart ? (YearDistributionPart) part : null;
    }

    protected void setPart(YearDistributionPart value)
    {
        this.getSettings().set("part", value);
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public void setYearModel(ISelectModel yearModel) {
        this.yearModel = yearModel;
    }

    public ISelectModel getYearModel() {
        return yearModel;
    }

    public void setPartsList(List<HSelectOption> partsList) {
        this.partsList = partsList;
    }

    public List<HSelectOption> getPartsList() {
        return partsList;
    }

    @SuppressWarnings("rawtypes")
    public List<DevelopForm> getDevelopFormSelected()
    {
        List<DevelopForm> retVal = new ArrayList<DevelopForm>();
        Object developFormList = _settings.get("developFormList");

        if (developFormList instanceof Collection) {
            if (!((Collection) developFormList).isEmpty()) {
                for (Object obj : (Collection) developFormList) {
                    DevelopForm ent = (DevelopForm) obj;
                    if (ent != null)
                        retVal.add(ent);
                }
            }
        }
        return retVal;
    }

    @SuppressWarnings("rawtypes")
    public List<DevelopTech> getDevelopTechSelected()
    {
        List<DevelopTech> retVal = new ArrayList<DevelopTech>();
        Object developTechList = _settings.get("developTechList");

        if (developTechList instanceof Collection) {
            if (!((Collection) developTechList).isEmpty()) {
                for (Object obj : (Collection) developTechList) {
                    DevelopTech ent = (DevelopTech) obj;
                    if (ent != null)
                        retVal.add(ent);
                }
            }
        }
        return retVal;
    }


    //
    @SuppressWarnings("rawtypes")
    public List<SakaiEntityState> getSakaiStateSelected()
    {
        List<SakaiEntityState> retVal = new ArrayList<SakaiEntityState>();
        Object objList = _settings.get("sakaiStateList");

        if (objList instanceof Collection) {
            if (!((Collection) objList).isEmpty()) {
                for (Object obj : (Collection) objList) {
                    SakaiEntityState ent = (SakaiEntityState) obj;
                    if (ent != null)
                        retVal.add(ent);
                }
            }
        }
        return retVal;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel) {
        this.developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopFormListModel() {
        return developFormListModel;
    }

    public void setDevelopTechListModel(ISelectModel developTechListModel) {
        this.developTechListModel = developTechListModel;
    }

    public ISelectModel getDevelopTechListModel() {
        return developTechListModel;
    }

    public void setSakaiStateListModel(ISelectModel sakaiStateListModel) {
        this.sakaiStateListModel = sakaiStateListModel;
    }

    public ISelectModel getSakaiStateListModel() {
        return sakaiStateListModel;
    }

    public void setSakaiSiteEtalon(String sakaiSiteEtalon) {
        this.sakaiSiteEtalon = sakaiSiteEtalon;
    }

    public String getSakaiSiteEtalon() {
        return sakaiSiteEtalon;
    }

    public boolean isUseLDAP() {
        return useLDAP;
    }

    public void setUseLDAP(boolean useLDAP) {
        this.useLDAP = useLDAP;
    }


}
