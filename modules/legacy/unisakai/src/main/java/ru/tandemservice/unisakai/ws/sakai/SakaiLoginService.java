/**
 * SakaiLoginService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unisakai.ws.sakai;

public interface SakaiLoginService extends javax.xml.rpc.Service {
    public String getSakaiLoginAddress();

    public ru.tandemservice.unisakai.ws.sakai.SakaiLogin_PortType getSakaiLogin() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unisakai.ws.sakai.SakaiLogin_PortType getSakaiLogin(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
