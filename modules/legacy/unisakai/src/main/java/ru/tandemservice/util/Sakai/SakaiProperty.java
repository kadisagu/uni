package ru.tandemservice.util.Sakai;

public class SakaiProperty {
    public String LOGIN = "";
    public String PASSWORD = "";
    public String STUDENT_ROLE = "";
    public String TUTOR_ROLE = "";
    public String TEMPLATE_SITE = "";
    public String SITE_JOINERROLE = "";
    public String SITE_TYPE = "";
    public String TEMPLATE_SITE_GROUP = "";

    public boolean USE_LDAP_AUTH = false;

}
