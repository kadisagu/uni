package ru.tandemservice.unisakai.uniepp.component.registry.RegElementPartEdit;

/*
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import java.util.List;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
*/

import ru.tandemservice.unisakai.dao.SakaiDaoFacade;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;


@SuppressWarnings("rawtypes")
public class DAO
        extends ru.tandemservice.uniepp.component.registry.RegElementPartEdit.DAO
{

    @Override
    public void prepare(ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Model modelExt)
    {
        // зачитаем свое
        EppRegistryElementPart epart = modelExt.getElement();
        UniSakaiSiteEtalon etalon = getEtalon(epart);

        Model model = (Model) modelExt;
        if (etalon != null)
            model.setSakaiEtalonSiteId(etalon.getEtalonSiteId());
        else
            model.setSakaiEtalonSiteId("");

        super.prepare(model);
    }


    private UniSakaiSiteEtalon getEtalon(EppRegistryElementPart regelem)
    {
        return SakaiDaoFacade.getSakaiDao().getEtalon(regelem);
    }


    @Override
    public void save(ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Model modelExt)
    {
        // сначала мы
        Model model = (Model) modelExt;

        _saveEtalon(model);
        super.save(modelExt);
    }


    private void _saveEtalon(Model model)
    {

        String etalon = "";
        if (model.getSakaiEtalonSiteId() != null)
            etalon = model.getSakaiEtalonSiteId();

        EppRegistryElementPart epart = model.getElement();
        SakaiDaoFacade.getSakaiDao().saveEtalon(epart, etalon);

		/*
        UniSakaiSiteEtalon siteEtalon = getEtalon(epart);
		if (siteEtalon==null)
		{
			siteEtalon = new UniSakaiSiteEtalon();
			siteEtalon.setRegistryElement(epart.getRegistryElement());
			siteEtalon.setPart(epart.getNumber());
			
			siteEtalon.setEtalonSiteId(etalon);
			getSession().save(siteEtalon);
		}
		else
		{
			siteEtalon.setEtalonSiteId(etalon);
			getSession().update(siteEtalon);
		}
		*/
    }
}
