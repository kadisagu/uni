package ru.tandemservice.unisakai.util.sprav;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings;

public class KeyValues {
    public final static String UNI_SAKAI_USELDAP = "unisakai.useLDAP";

    public static String getStringValue(String key, boolean errorUp) throws Exception
    {
        UniSakaiSettings val = UniDaoFacade.getCoreDao().get(UniSakaiSettings.class, UniSakaiSettings.P_STRING_KEY, key);

        if (val == null) {
            if (errorUp)
                throw new Exception("Ключ " + key + " не задан");
            else
                return null;
        }
        else {
            return val.getStringValue();
        }


    }

    public static String getTest()
    {

        return "SakaiScript";
    }
}
