package ru.tandemservice.unisakai.component.sakai.imp.UgstMassDel;

import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisakai.logic.ActionSakai;

import java.util.List;

public class Controller extends ru.tandemservice.unisakai.component.sakai.imp.UgstMassImp.Controller
{
    public static String KEY = "sakai.imp.UgstMassDel";

    @Override
    public String getSettingsKey()
    {
        return KEY;
    }

    @Override
    protected void _doPart(ru.tandemservice.unisakai.component.sakai.imp.UgstMassImp.Model model
                    , EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction) throws Exception
    {

        // кандидат на переопределение
        // собираем значения фильтров и в путь
        EducationYear year = model.getYear();
        YearDistributionPart partYear = model.getPart();
        boolean syncTi = model.getProcessTi();
        boolean syncSt = model.getProcessSt();

        // списки:
        List<DevelopForm> lstDevForm = model.getDevelopFormSelected();
        List<DevelopTech> lstDevTech = model.getDevelopTechSelected();

        ActionSakai as = ActionSakai.getActionSakai();


        boolean strongListSt = model.getStrongListSt();
        boolean filterInvert = model.getFiterInvert();

        as.DeleteUserFromSite(eppRegistryElementPartFControlAction, year, partYear, syncTi, syncSt, strongListSt,
                        filterInvert, model.getDeleteWithMark(), lstDevForm, lstDevTech, model.getSakaiSites());

    }
}
