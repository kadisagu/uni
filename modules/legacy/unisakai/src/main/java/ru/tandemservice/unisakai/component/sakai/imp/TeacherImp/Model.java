package ru.tandemservice.unisakai.component.sakai.imp.TeacherImp;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

public class Model extends ru.tandemservice.unisakai.component.sakai.imp.StudentImp.Model {

    private ISelectModel orgUnitListModel;

    public void setOrgUnitListModel(ISelectModel orgUnitListModel) {
        this.orgUnitListModel = orgUnitListModel;
    }

    public ISelectModel getOrgUnitListModel() {
        return orgUnitListModel;
    }

}
