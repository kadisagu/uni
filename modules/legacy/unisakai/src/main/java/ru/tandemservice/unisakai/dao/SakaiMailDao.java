package ru.tandemservice.unisakai.dao;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisakai.logic.UserSakai;
import ru.tandemservice.unisakai.util.sprav.KeyValues;

import java.awt.*;

public class SakaiMailDao extends UniBaseDao implements ISakaiMailDao {
    @Override
    public void MailSendInfoNewUser(UserSakai user) throws Exception
    {
        if (!user.isHasEmail())
            return;

        String subject = KeyValues.getStringValue("app.mailtemplate.newuser.subject", true);

        String text = KeyValues.getStringValue("app.mailtemplate.newuser", true);
        text = text.replace("{academy}", TopOrgUnit.getInstance().getShortTitle());
        text = text.replace("{app.mailtemplate.link}", KeyValues.getStringValue("app.mailtemplate.link", true));

        // app.mail.from заменить в миграции на app.mailtemplate.email
        text = text.replace("{app.mailtemplate.email}", KeyValues.getStringValue("app.mailtemplate.email", true));
        text = text.replace("{app.mail.from}", KeyValues.getStringValue("app.mailtemplate.email", true));

        text = text.replace("{userName}", user.getEid());
        text = text.replace("{userPassword}", user.getPassword());

//        MessageType mTypeMail = getByCode(MessageType.class, MessageTypeCodes.INFO);
//        _AddMailSend(user.getIdLong(), mTypeMail, subject, text);


    }

    private Person getPersonByIdUserSakai(Long idLong)
    {
        if (idLong == null)
            return null;
        else {
            IEntity entity = get(idLong);
            if (entity instanceof Student) {
                Student student = (Student) entity;
                return student.getPerson();
            }
            if (entity instanceof Employee) {
                Employee employee = (Employee) entity;
                return employee.getPerson();
            }

            return null;
        }
    }

    @Override
    public void MailSendInfoUpdateUser(UserSakai user) throws Exception
    {
        String canSend = KeyValues.getStringValue("app.mailtemplate.notify.onupdate", false);
        if (!"true".equals(canSend)) return;

        if (!user.isHasEmail())
            return;

        if (user.getEid().contains("tmp_"))
            return;

        String subject = KeyValues.getStringValue("app.mailtemplate.edituser.subject", true);
        String text = KeyValues.getStringValue("app.mailtemplate.edituser", true);

        text = text.replace("{academy}", TopOrgUnit.getInstance().getShortTitle());
        text = text.replace("{app.mailtemplate.link}", KeyValues.getStringValue("app.mailtemplate.link", true));
        text = text.replace("{app.link}", KeyValues.getStringValue("app.mailtemplate.link", true));


        // app.mail.from заменить в миграции на app.mailtemplate.email
        text = text.replace("{app.mailtemplate.email}", KeyValues.getStringValue("app.mailtemplate.email", true));
        text = text.replace("{app.mail.from}", KeyValues.getStringValue("app.mailtemplate.email", true));


        text = text.replace("{userName}", user.getEid());
        text = text.replace("{userPassword}", user.getPassword());

        text = text.replace("{lastName}", user.getLastName());
        text = text.replace("{firstName}", user.getFirstName());

//        MessageType mTypeMail = getByCode(MessageType.class, MessageTypeCodes.INFO);
//        _AddMailSend(user.getIdLong(), mTypeMail, subject, text);

    }


//    private void _AddMailSend(Long id, MessageType type, String subject, String text)
//    {
//        Person person = getPersonByIdUserSakai(id);
//        MailContentType contentType = getCatalogItem(MailContentType.class, MailContentTypeCodes.PLAIN_TEXT);
//
//        if (person != null) {
//            MailDaoFacade.getMailDao().addNewMail(person, subject, text, type, contentType);
//        }
//
//    }


}
