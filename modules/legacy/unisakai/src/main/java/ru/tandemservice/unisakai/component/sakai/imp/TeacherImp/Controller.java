package ru.tandemservice.unisakai.component.sakai.imp.TeacherImp;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;
import ru.tandemservice.unisakai.logic.Tools;
import ru.tandemservice.unisakai.logic.UserSakai;

import java.util.Map;

public class Controller extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Controller
{

    @Override
    public boolean needLoadSakaiSites() {
        return false;
    }


    @Override
    public boolean needLoadSakaiUsers() {
        return true;
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        // сначала активацим старое
        super.onRefreshComponent(component);
    }

    @Override
    public void MakeDataColumn(DynamicListDataSource<WrapperImp> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Сущность", "entityType").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Имя", "name").setClickable(true));
        dataSource.addColumn(new SimpleColumn("Описание", "description").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Статус в Sakai", "sakaiInfo").setClickable(false));
    }

    @Override
    public void onPostProcess()
    {

    }

    @Override
    /*
	 * Обработка одной записи
	 */
    public void ProcessRow(WrapperImp row, ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model) throws Exception
    {
        // Обрабатываем только студентов
        Employee employee = (Employee) row.getTag();

        if (employee == null)
            return;

        Map<String, UserSakai> sakaiusers = model.getSakaiusers();
        if (sakaiusers == null)
            return;

        if (!model.getActionSakai().isConnect())
            return;
        UserSakai user = Tools.getUser(employee);

        // обрабатываем только пользователей с электронной почтой (для преподов значения не имеет)
        if (user.isCanUse()) {
            if (sakaiusers.containsKey(user.getId())) {
                // пользователь есть
                UserSakai userExist = sakaiusers.get(user.getId());

                if (userExist.hashCode() != user.hashCode()) {
                    // возможно сменился eid
                    // если у пользователя сменился  eid и по новому eid существует пользователь
                    // то нужно пользователя уничтожить с новым eid и только потом опдейтить
                    if (!userExist.getEid().equals(user.getEid())) {
                        UserSakai existUser = getOtherUserByEid(user, sakaiusers);
                        if (existUser != null) {
                            // eid уже кем-то занят - следовательно занятый eid
                            // нужно освободить
                            existUser.setEid("tmp_" + existUser.getId()); // заменяем eid на id

                            model.getActionSakai().UpdateUser(existUser);
                            sakaiusers.put(existUser.getId(), existUser);
                        }
                    }
                    model.getActionSakai().UpdateUser(user);
                }
            }
            else {
                // пользователя нет
                // но возможно этот еид занят и нужно тогда убить пользователя
                UserSakai existUser = getOtherUserByEid(user, sakaiusers);
                if (existUser != null) {
                    // eid уже кем-то занят - следовательно занятый eid
                    // нужно освободить
                    existUser.setEid("tmp_" + existUser.getId()); // заменяем eid на id
                    sakaiusers.put(existUser.getId(), existUser);
                    model.getActionSakai().UpdateUser(existUser);
                }
                model.getActionSakai().NewUser(user);
            }
        }
    }

    @Override
    public String getSettingsKey()
    {
        return "sakai.imp.TiImp";
    }


}
