package ru.tandemservice.unisakai.component.sakai.imp.BaseImp;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.process.*;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisakai.logic.ActionSakai;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.UserSakai;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class Controller extends AbstractBusinessController<IDAO, Model> {

    public abstract boolean needLoadSakaiUsers();

    public abstract boolean needLoadSakaiSites();


    @Override
    @SuppressWarnings("unchecked")
    public void onRefreshComponent(final IBusinessComponent component)
    {

        Model model = component.getModel();

        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey()));

        getDao().prepare( model);
        if (model.getDataSource() == null)
        {
            // забор с данными
            DynamicListDataSource<WrapperImp> dataSource = new DynamicListDataSource<>(component, component1 -> {
                getDao().refreshDataSource(Controller.this.getModel(component1));
            }, 30);

            MakeDataColumn(dataSource);
            model.setDataSource(dataSource);
        }

        // переустановим сессию
        // работа по подключению сессии
        try {
            ActionSakai as = ActionSakai.getActionSakai();
            as.Reconnect();
            boolean useLDAP = as.getSakaiScripts().getSakaiProperty().USE_LDAP_AUTH;
            model.setUseLDAP(useLDAP);
            model.getDataSource().getColumn("sakaiInfo").setVisible(!useLDAP);

            model.setActionSakai(as);
            model.setTitle("Связь с Sakai установлена");
        }
        catch (Exception e) {
            model.setTitle("Ошибка соединения с Sakai " + e.getMessage());
        }

        // спросим всех пользователей сакая
        if (!model.isUseLDAP() && needLoadSakaiUsers() && (model.getSakaiusers() == null || model.isElaspend())) {
            _loadSakaiUser(model);
            model.TimerClear();
        }

        // спросим все сайты сакая
        if (needLoadSakaiSites() && (model.getSakaisites() == null || model.isElaspend())) {
            _loadSakaiSites(model);
            model.TimerClear();
        }

    }

    private void _loadSakaiSites(Model model)
    {

        try {
            // создадим по ним хэш таблицу
            Map<String, List<SiteSakai>> sakaiusers = model.getActionSakai().getSakaiSites();
            model.setSakaisites(sakaiusers);
        }
        catch (Exception ex) {
            model.addTitle(ex.getMessage());
            model.setSakaisites(null);
        }
    }

    private void _loadSakaiUser(Model model)
    {
        try {
            // создадим по ним хэш таблицу
            Map<String, UserSakai> sakaiusers = model.getActionSakai().getSakaiUsers();
            model.setSakaiusers(sakaiusers);
        }
        catch (Exception ex) {
            model.addTitle(ex.getMessage());
            model.setSakaiusers(null);
        }
    }

    public void MakeDataColumn(DynamicListDataSource<WrapperImp> dataSource)
    {
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Сущность", "entityType"));
        dataSource.addColumn(new SimpleColumn("Имя", "name"));
        dataSource.addColumn(new SimpleColumn("Описание", "description"));
        dataSource.addColumn(new SimpleColumn("Статус в Sakai", "sakaiInfo"));
    }

    @SuppressWarnings("unchecked")
    public void onClickSearch(final IBusinessComponent component)
    {
        Model model = getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        getDao().prepare(model);

    }

    public void onClickClear(final IBusinessComponent context)
    {
        IDataSettings settings = getModel(context).getSettings();
        settings.set("name", null);
        settings.set("number", null);
        settings.set("part", null);
        settings.set("developFormList", null);
        settings.set("developTechList", null);
        settings.set("sakaiStateList", null);
        DataSettingsFacade.saveSettings(settings);
        this.onClickSearch(context);
    }

    public void onSearchParamsChange(final IBusinessComponent component)
    {
        onClickSearch(component);
    }


    public abstract String getSettingsKey();

    @Override
    public void deactivate(final IBusinessComponent component) {

        System.out.print("deactivate");
        super.deactivate(component);
    }

    @SuppressWarnings("unchecked")
    public void onClickSite(final IBusinessComponent context) {

        final Model model = getModel(context);
        if (!model.getActionSakai().isConnect()) return;

        final List<WrapperImp> lst = getProcessList(model);

        // делаем именно тут
        model.setTitle("");
        model.setProcessedModule(0);
        synchronized (Model.MUTEX) {
            final IBackgroundProcess process = new BackgroundProcessBase() {
                @Override
                public ProcessResult run(final ProcessState state) {
                    try {
                        try {
                            Thread.sleep(1000);
                        }
                        catch (final Throwable t) {
                        }

                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        StringBuilder errors = new StringBuilder();
                        try {
                            state.setMaxValue(100);
                            state.setCurrentValue(0);
                            state.setDisplayMode(ProcessDisplayMode.percent);

                            int i = 0;
                            for (WrapperImp row : lst) {
                                state.setCurrentValue(100 * i / lst.size());
                                Thread.yield();

                                if (row.getTag() instanceof PersonRole) {
                                    String result = model.getActionSakai().syncMySite((PersonRole) row.getTag());
                                    if (!"success".equals(result)) {
                                        if (errors.length() > 0) errors.append(", ");
                                        errors.append(((PersonRole) row.getTag()).getId());
                                    }
                                }

                                row = null;
                                i++;
                                model.setProcessedModule(i);
                            }

                            model.addTitle("Обработано " + Integer.toString(model.getProcessedModule()) + " сущностей ");
                            if (errors.length() > 0) {
                                model.addTitle("Ошибка вызова WS в Sakai для syncMySite users:" + errors.toString());
                                return null;

                                //может стоит отображать в диалоге??
                                //return new ProcessResult(resultString, true);
                            }
                            else {
                                return null;
                            }

                        }
                        catch (Exception ex) {
                            throw ex;
                        }
                        finally {
                            onPostProcess();
                            eventLock.release();
                        }
                    }
                    catch (final Throwable t) {
                        return new ProcessResult("Произошла ошибка " + t.getMessage(), true); // закрываем диалог
                    }
                } //public ProcessResult run
            };

            new BackgroundProcessHolder().start("Обработка учебных модулей", process);
        } //synchronized
    }

    @SuppressWarnings("unchecked")
    public void onClick(final IBusinessComponent context)
    {
        final Model model = getModel(context);
        final List<WrapperImp> lst = getProcessList(model);

        // делаем именно тут
        model.setTitle("");
        model.setProcessedModule(0);

        synchronized (Model.MUTEX) {
            final IBackgroundProcess process = new BackgroundProcessBase() {
                @Override
                public ProcessResult run(final ProcessState state) {

                    try {

                        try {
                            Thread.sleep(1000);
                        }
                        catch (final Throwable t) {
                        }

                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        try {
                            state.setMaxValue(100);
                            state.setCurrentValue(0);
                            state.setDisplayMode(ProcessDisplayMode.percent);

                            int i = 0;

                            for (WrapperImp row : lst) {
                                state.setCurrentValue(100 * i / lst.size());
                                Thread.yield();

                                try {
                                    ProcessRow(row, model);

                                    row = null;
                                }
                                catch (Exception ex) {
                                    String msg = "Ошибка при обработке " + row.getName() + " " + ex.getMessage();
                                    model.addTitle(msg);
                                    throw ex;
                                }
                                i++;
                                model.setProcessedModule(i);
                            }
                            model.addTitle("Обработано " + Integer.toString(model.getProcessedModule()) + " сущностей ");
                            return null; // закрываем диалог
                        }
                        finally {
                            onPostProcess();
                            eventLock.release();
                        }
                    }
                    catch (final Throwable t) {
                        return new ProcessResult("Произошла ошибка " + t.getMessage(), true); // закрываем диалог
                    }
                }
            };
            new BackgroundProcessHolder().start("Обработка учебных модулей", process);
        }
    }

    public List<WrapperImp> getProcessList(Model model)
    {
        return getDao().getListData(model);
    }

    public abstract void onPostProcess();

    /**
     * Обработка одной строки сущности
     *
     * @param row
     * @param model
     *
     * @throws Exception
     */
    public abstract void ProcessRow(WrapperImp row, Model model) throws Exception;

    public UserSakai getOtherUserByEid(UserSakai user, Map<String, UserSakai> sakaiusers)
    {
        // ищем простым перебором
        for (String key : sakaiusers.keySet()) {
            UserSakai exist = sakaiusers.get(key);

            if (exist.getEid().equals(user.getEid())) {
                if (!exist.getId().equals(user.getId())) {
                    return exist;
                }
            }
        }
        return null;
    }

    protected List<Long> getSelectedIdsSiteDisciplineId(final IBusinessComponent context)
    {
        Model model = context.getModel();
        List<Long> ids = new ArrayList<>();

        CheckboxColumn ck = (CheckboxColumn) model.getDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        for (IEntity e : lst) {
            WrapperImp w = (WrapperImp) e;
            if (w != null) {
                SiteSakai site = (SiteSakai) w.getTag();
                if (site != null) {
                    Long id = Long.parseLong(site.getDisciplineId());
                    ids.add(id);
                }
            }
        }

        List<IEntity> empty = new ArrayList<IEntity>();
        ck.setSelectedObjects(empty);

        return ids;
    }


    protected List<Long> getSelectedIdsEppRegistryElementPart(final IBusinessComponent context)
    {
        Model model = context.getModel();
        List<Long> ids = new ArrayList<>();

        CheckboxColumn ck = (CheckboxColumn) model.getDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        for (IEntity e : lst) {
            WrapperImp w = (WrapperImp) e;
            if (w != null) {
                SiteSakai site = (SiteSakai) w.getTag();
                if (site != null) {
                    if (site.getEntityId() != null)
                        ids.add(site.getEntityId());
                }
            }
        }

        List<IEntity> empty = new ArrayList<>();
        ck.setSelectedObjects(empty);

        return ids;
    }

    protected List<SiteSakai> getSelectedSakaiSite(final IBusinessComponent context)
    {
        Model model = context.getModel();
        List<SiteSakai> ids = new ArrayList<>();

        CheckboxColumn ck = (CheckboxColumn) model.getDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        for (IEntity e : lst) {
            WrapperImp w = (WrapperImp) e;
            if (w != null) {
                SiteSakai site = (SiteSakai) w.getTag();
                if (site != null)
                    ids.add(site);
            }
        }

        List<IEntity> empty = new ArrayList<>();
        ck.setSelectedObjects(empty);
        return ids;
    }

}
