package ru.tandemservice.unisakai.util.Sakai;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;

import java.util.HashMap;
import java.util.Map;

public class SakaiMark {

    private static Map<String, SessionMarkCatalogItem> mapMark = new HashMap<String, SessionMarkCatalogItem>();

    public static Map<String, SessionMarkCatalogItem> getMapMark()
    {
        if (mapMark.keySet().size() == 0)
            _iniMap();

        return mapMark;
    }

    private static void _iniMap()
    {

        SessionMarkCatalogItem mark5 = UniDaoFacade.getCoreDao().getByCode(SessionMarkCatalogItem.class, "scale5.5");
        SessionMarkCatalogItem mark4 = UniDaoFacade.getCoreDao().getByCode(SessionMarkCatalogItem.class, "scale5.4");
        SessionMarkCatalogItem mark3 = UniDaoFacade.getCoreDao().getByCode(SessionMarkCatalogItem.class, "scale5.3");
        SessionMarkCatalogItem mark2 = UniDaoFacade.getCoreDao().getByCode(SessionMarkCatalogItem.class, "scale5.2");

        SessionMarkCatalogItem mark2_5 = UniDaoFacade.getCoreDao().getByCode(SessionMarkCatalogItem.class, "scale2.5");
        SessionMarkCatalogItem mark2_2 = UniDaoFacade.getCoreDao().getByCode(SessionMarkCatalogItem.class, "scale2.2");

        mapMark.put("отлично", mark5);
        mapMark.put("хорошо", mark4);
        mapMark.put("удовлетворительно", mark3);
        mapMark.put("неудовлетворительно", mark2);
        mapMark.put("плохо", mark2);


        mapMark.put("зачтено", mark2_5);
        mapMark.put("не зачтено", mark2_2);

        mapMark.put("зачет", mark2_5);
        mapMark.put("незачет", mark2_2);


    }

    public static SessionMarkCatalogItem getMark(String sakaiMark)
    {
        Map<String, SessionMarkCatalogItem> map = getMapMark();
        if (map.containsKey(sakaiMark))
            return map.get(sakaiMark);
        else
            return null;
    }
}
