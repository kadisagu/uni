package ru.tandemservice.unisakai.component.catalog.uniSakaiSettings.UniSakaiSettingsAddEdit;

import org.hibernate.Session;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        // final Session session = getSession();


        //st = getSession().get(Student.class, 100);


        if (model.getUniSakaiSettingsId() != null) {
            UniSakaiSettings setting = (UniSakaiSettings) this.getNotNull(model.getUniSakaiSettingsId());
            model.setUniSakaiSettings(setting);


        }
        else {
            model.setUniSakaiSettings(null);
        }


    }


    @Override
    public void update(Model model)
    {
        Session session = getSession();
        if (model.getUniSakaiSettings() != null)
            session.update(model.getUniSakaiSettings());
    }


}
