package ru.tandemservice.unisakai.util.Sakai;

import java.util.ArrayList;
import java.util.List;

/**
 * Результат импорта оценок
 *
 * @author Администратор
 */
public class ImportInfo {

    public class RowInfo {


        private String cell1;
        private String cell2;

        public void setCell1(String cell1) {
            this.cell1 = cell1;
        }

        public String getCell1() {
            return cell1;
        }

        public void setCell2(String cell2) {
            this.cell2 = cell2;
        }

        public String getCell2() {
            return cell2;
        }


    }

    private boolean success = true;
    private String totalRezult = "";

    private List<RowInfo> rows = new ArrayList<RowInfo>();

    public void AddTotal(String msg)
    {
        totalRezult += msg;
    }

    public void setTotalRezult(String totalRezult) {
        this.totalRezult = totalRezult;
    }

    public String getTotalRezult() {
        return totalRezult;
    }

    public void setRows(List<RowInfo> rows) {
        this.rows = rows;
    }

    public List<RowInfo> getRows() {
        return rows;
    }


    public void AddRow(String c1, String c2)
    {
        RowInfo ri = new RowInfo();
        ri.setCell1(c1);
        ri.setCell2(c2);

        rows.add(ri);

    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
