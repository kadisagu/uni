package ru.tandemservice.unisakai.entity.catalog;

import ru.tandemservice.unisakai.entity.catalog.gen.SakaiEntityStateGen;

/**
 * Состояние сущности в САКАЕ
 */
public class SakaiEntityState extends SakaiEntityStateGen {
    public static String SAKAI_NO = "01";
    public static String SAKAI_YES = "02";
    public static String SAKAI_NOT_EQUALS = "03";
    public static String SAKAI_NO_INFO = "04";
    public static String SAKAI_NOT_IN_UNI = "05";


}