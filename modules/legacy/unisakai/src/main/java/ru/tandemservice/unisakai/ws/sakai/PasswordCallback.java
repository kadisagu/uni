package ru.tandemservice.unisakai.ws.sakai;

import org.apache.commons.lang.StringUtils;
import org.apache.ws.security.WSPasswordCallback;
import org.tandemframework.core.runtime.ApplicationRuntime;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

public class PasswordCallback implements CallbackHandler {
    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
    {
        if (callbacks.length != 1)
            throw new IllegalArgumentException();

        String password = ApplicationRuntime.getProperty("ws.password");

        if (StringUtils.isEmpty(password))
            throw new RuntimeException("All web services are disabled, set property \"ws.password\" for inclusion.");

        if (!password.equals(((WSPasswordCallback) callbacks[0]).getPassword()))
            throw new RuntimeException("Wrong password");
    }
}
