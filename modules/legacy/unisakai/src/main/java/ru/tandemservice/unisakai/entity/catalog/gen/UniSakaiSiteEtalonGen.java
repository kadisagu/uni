package ru.tandemservice.unisakai.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Эталонные сайты к частям дисциплин
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniSakaiSiteEtalonGen extends EntityBase
 implements INaturalIdentifiable<UniSakaiSiteEtalonGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon";
    public static final String ENTITY_NAME = "uniSakaiSiteEtalon";
    public static final int VERSION_HASH = 600715679;
    private static IEntityMeta ENTITY_META;

    public static final String L_REGISTRY_ELEMENT = "registryElement";
    public static final String P_PART = "part";
    public static final String P_ETALON_SITE_ID = "etalonSiteId";

    private EppRegistryElement _registryElement;     // Элемент реестра
    private int _part;     // часть дисциплины
    private String _etalonSiteId;     // Эталонный сайт в Сакае

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    /**
     * @return часть дисциплины. Свойство не может быть null.
     */
    @NotNull
    public int getPart()
    {
        return _part;
    }

    /**
     * @param part часть дисциплины. Свойство не может быть null.
     */
    public void setPart(int part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return Эталонный сайт в Сакае.
     */
    @Length(max=255)
    public String getEtalonSiteId()
    {
        return _etalonSiteId;
    }

    /**
     * @param etalonSiteId Эталонный сайт в Сакае.
     */
    public void setEtalonSiteId(String etalonSiteId)
    {
        dirty(_etalonSiteId, etalonSiteId);
        _etalonSiteId = etalonSiteId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniSakaiSiteEtalonGen)
        {
            if (withNaturalIdProperties)
            {
                setRegistryElement(((UniSakaiSiteEtalon)another).getRegistryElement());
                setPart(((UniSakaiSiteEtalon)another).getPart());
            }
            setEtalonSiteId(((UniSakaiSiteEtalon)another).getEtalonSiteId());
        }
    }

    public INaturalId<UniSakaiSiteEtalonGen> getNaturalId()
    {
        return new NaturalId(getRegistryElement(), getPart());
    }

    public static class NaturalId extends NaturalIdBase<UniSakaiSiteEtalonGen>
    {
        private static final String PROXY_NAME = "UniSakaiSiteEtalonNaturalProxy";

        private Long _registryElement;
        private int _part;

        public NaturalId()
        {}

        public NaturalId(EppRegistryElement registryElement, int part)
        {
            _registryElement = ((IEntity) registryElement).getId();
            _part = part;
        }

        public Long getRegistryElement()
        {
            return _registryElement;
        }

        public void setRegistryElement(Long registryElement)
        {
            _registryElement = registryElement;
        }

        public int getPart()
        {
            return _part;
        }

        public void setPart(int part)
        {
            _part = part;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UniSakaiSiteEtalonGen.NaturalId) ) return false;

            UniSakaiSiteEtalonGen.NaturalId that = (NaturalId) o;

            if( !equals(getRegistryElement(), that.getRegistryElement()) ) return false;
            if( !equals(getPart(), that.getPart()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRegistryElement());
            result = hashCode(result, getPart());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRegistryElement());
            sb.append("/");
            sb.append(getPart());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniSakaiSiteEtalonGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniSakaiSiteEtalon.class;
        }

        public T newInstance()
        {
            return (T) new UniSakaiSiteEtalon();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "registryElement":
                    return obj.getRegistryElement();
                case "part":
                    return obj.getPart();
                case "etalonSiteId":
                    return obj.getEtalonSiteId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
                case "part":
                    obj.setPart((Integer) value);
                    return;
                case "etalonSiteId":
                    obj.setEtalonSiteId((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "registryElement":
                        return true;
                case "part":
                        return true;
                case "etalonSiteId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "registryElement":
                    return true;
                case "part":
                    return true;
                case "etalonSiteId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "registryElement":
                    return EppRegistryElement.class;
                case "part":
                    return Integer.class;
                case "etalonSiteId":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniSakaiSiteEtalon> _dslPath = new Path<UniSakaiSiteEtalon>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniSakaiSiteEtalon");
    }
            

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    /**
     * @return часть дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon#getPart()
     */
    public static PropertyPath<Integer> part()
    {
        return _dslPath.part();
    }

    /**
     * @return Эталонный сайт в Сакае.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon#getEtalonSiteId()
     */
    public static PropertyPath<String> etalonSiteId()
    {
        return _dslPath.etalonSiteId();
    }

    public static class Path<E extends UniSakaiSiteEtalon> extends EntityPath<E>
    {
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;
        private PropertyPath<Integer> _part;
        private PropertyPath<String> _etalonSiteId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

    /**
     * @return часть дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon#getPart()
     */
        public PropertyPath<Integer> part()
        {
            if(_part == null )
                _part = new PropertyPath<Integer>(UniSakaiSiteEtalonGen.P_PART, this);
            return _part;
        }

    /**
     * @return Эталонный сайт в Сакае.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon#getEtalonSiteId()
     */
        public PropertyPath<String> etalonSiteId()
        {
            if(_etalonSiteId == null )
                _etalonSiteId = new PropertyPath<String>(UniSakaiSiteEtalonGen.P_ETALON_SITE_ID, this);
            return _etalonSiteId;
        }

        public Class getEntityClass()
        {
            return UniSakaiSiteEtalon.class;
        }

        public String getEntityName()
        {
            return "uniSakaiSiteEtalon";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
