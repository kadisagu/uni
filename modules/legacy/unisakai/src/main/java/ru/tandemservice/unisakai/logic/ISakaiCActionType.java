package ru.tandemservice.unisakai.logic;

/**
 * Типы контрольных мероприятий
 *
 * @author vch
 */
public interface ISakaiCActionType {
    /**
     * Тип контрольного мероприятия не определен
     */
    static String CACTIONTYPE_UNDIFINE = "0";

}
