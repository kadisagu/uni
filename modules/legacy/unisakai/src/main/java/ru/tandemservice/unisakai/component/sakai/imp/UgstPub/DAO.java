package ru.tandemservice.unisakai.component.sakai.imp.UgstPub;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupRowGen;
import ru.tandemservice.unisakai.dao.SakaiDaoFacade;
import ru.tandemservice.unisakai.dao.wrappers.WrapperStudentToSiteSakai;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.Tools;
import ru.tandemservice.unisakai.logic.UserSakai;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        if (model.getIdEppRegistryElementPartFControlAction() != null) {
            EppRegistryElementPartFControlAction part = getNotNull(EppRegistryElementPartFControlAction.class, model.getIdEppRegistryElementPartFControlAction());
            model.setEppRegistryElementPartFControlAction(part);
        }

        // получим эталон
        UniSakaiSiteEtalon etalon = SakaiDaoFacade.getSakaiDao().getEtalon(model.getEppRegistryElementPartFControlAction().getPart());

        if (etalon == null)
            model.setSakaiSiteEtalon("");
        else
            model.setSakaiSiteEtalon(etalon.getEtalonSiteId());

        if (model.getDevelopFormListModel() == null)
            model.setDevelopFormListModel(new LazySimpleSelectModel<DevelopForm>(DevelopForm.class).setSortProperty(DevelopForm.P_CODE));
        if (model.getDevelopTechListModel() == null)
            model.setDevelopTechListModel(new LazySimpleSelectModel<DevelopTech>(DevelopTech.class).setSortProperty(DevelopTech.P_CODE));
        if (model.getSakaiStateListModel() == null)
            model.setSakaiStateListModel(new LazySimpleSelectModel<SakaiEntityState>(SakaiEntityState.class).setSortProperty(SakaiEntityState.P_CODE));


        // учебные года только из реальных групп
        model.setYearModel(new EducationYearModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder eduYear = new DQLSelectBuilder()
                        .fromEntity(EppRealEduGroupRow.class, alias)
                        .column(property(EppRealEduGroupRowGen.group().summary().yearPart().year().educationYear().id().fromAlias(alias)))
                        .distinct();

                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationYear.class, alias);
                dql.where(in(property(EducationYear.id().fromAlias(alias)), eduYear.buildQuery()));
                dql.order(property(EducationYear.intValue().fromAlias(alias)));
                return dql;
            }
        });

        // части учебных годов
        DQLSelectBuilder partsDQL = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroupRow.class, "obj")
                .column(property(EppRealEduGroupRowGen.group().summary().yearPart().part().id().fromAlias("obj")))
                .distinct();

        if (model.getYear() != null)
            partsDQL.where(eq(property(EppRealEduGroupRowGen.group().summary().yearPart().year().educationYear().fromAlias("obj")), value(model.getYear())));

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(YearDistributionPart.class, "part")
                .column("part")
                .where(in(property(YearDistributionPart.id().fromAlias("part")), partsDQL.buildQuery()))
                .order(property(YearDistributionPart.code().fromAlias("part")));


        Set<YearDistributionPart> parts = new LinkedHashSet<YearDistributionPart>(dql.createStatement(getSession()).<YearDistributionPart>list());
        model.setPartsList(HierarchyUtil.listHierarchyNodesWithParents(new ArrayList<IHierarchyItem>(parts), false));


        if (model.getSites() == null)
            sakaiInfoToModel(model);

    }

    public void sakaiInfoToModel(Model model)
    {
        SiteSakai site = null;
        Map<String, UserSakai> users = null;
        Map<String, List<SiteSakai>> sites = null;

        try {
            sites = model.getActionSakai().getSakaiSites();
            if (!model.isUseLDAP())
                users = model.getActionSakai().getSakaiUsers();

            SiteSakai siteVrem = Tools.getSiteSakaiByEntity(model.getEppRegistryElementPartFControlAction().getPart().getRegistryElement(),
                            model.getEppRegistryElementPartFControlAction().getPart().getNumber(), model.getEppRegistryElementPartFControlAction().getControlAction().getCode());

            site = Tools.getSiteSakaiMaxTime(sites, siteVrem);
        }
        catch (Exception e) {
            site = null;
            users = null;
        }

        model.setSite(site);
        model.setUsers(users);
        model.setSites(sites);
    }

    @Override
    public void refreshDataSource(Model model)
    {

        // просчитаем с учетом статуса
        List<WrapperStudentToSiteSakai> itemList = SakaiDaoFacade.getSakaiDao().getStudents4Pub(
                model.getEppRegistryElementPartFControlAction()
                , model.getEppRegistryElementPartFControlAction().getPart().getNumber()
                , model.getYear()
                , model.getPart()
                , model.getDevelopFormSelected()
                , model.getDevelopTechSelected()
                , model.getSakaiStateSelected()
                , model.getSite()
                , model.getUsers()
                , model.isUseLDAP());


        DynamicListDataSource<WrapperStudentToSiteSakai> dataSource = model.getDataSource();
        UniBaseUtils.createPage(dataSource, itemList);
    }
}
