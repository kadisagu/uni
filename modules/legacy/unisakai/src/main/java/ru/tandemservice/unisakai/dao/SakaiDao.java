package ru.tandemservice.unisakai.dao;


import org.hibernate.Session;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unisakai.dao.wrappers.WrapperStudentToSiteSakai;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon;
import ru.tandemservice.unisakai.entity.catalog.codes.SakaiEntityStateCodes;
import ru.tandemservice.unisakai.entity.catalog.gen.UniSakaiSiteEtalonGen;
import ru.tandemservice.unisakai.logic.ActionSakai;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.Tools;
import ru.tandemservice.unisakai.logic.UserSakai;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class SakaiDao extends UniBaseDao implements ISakaiDao {


    public List<EppRegistryElementPartFControlAction> getEppRegistryElementPartFControlActionList(List<Long> ids)
    {
        Session session = getSession();

        DQLSelectBuilder dqlDis = new DQLSelectBuilder();
        dqlDis.fromEntity(EppRegistryElementPartFControlAction.class, "reg");
        dqlDis.column("reg");
        dqlDis.where(in(property(EppRegistryElementPartFControlAction.id().fromAlias("reg")), ids));

        List<EppRegistryElementPartFControlAction> regList = dqlDis.createStatement(session).list();

        return regList;
    }

    @Override
    public List<Employee> getEmployee4Site
            (
                    EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction
            )
    {
        List<Employee> retVal = new ArrayList<Employee>();
        // EppPpsCollectionItem.pps()
        DQLSelectBuilder dql = new DQLSelectBuilder();

        dql.fromEntity(EppRealEduGroup4ActionType.class, "grp");
        // приджойним   EppPpsCollectionItem
        dql.joinEntity("grp", DQLJoinType.inner, EppPpsCollectionItem.class, "eppitem", DQLExpressions.eq(
                               DQLExpressions.property("grp.id"),
                               DQLExpressions.property("eppitem.list"))
        );

        // приджойним сотрудника
        dql.joinEntity("eppitem", DQLJoinType.inner, PpsEntryByEmployeePost.class, "ppspost", DQLExpressions.eq(
                               DQLExpressions.property("eppitem.pps"),
                               DQLExpressions.property("ppspost.id"))
        );

        dql.joinEntity("ppspost", DQLJoinType.inner, EmployeePost4PpsEntry.class, "ep4pps", eq(property("ppspost.id"), property("ep4pps", EmployeePost4PpsEntry.ppsEntry().id())));

        // первичный ключ сотрудника
        dql.column(property("ep4pps", EmployeePost4PpsEntry.employeePost().employee().id()));

        dql.where(eq(property(EppRealEduGroup4ActionType.activityPart().number().fromAlias("grp")), value(eppRegistryElementPartFControlAction.getPart().getNumber())));
        dql.where(eq(property(EppRealEduGroup4ActionType.activityPart().registryElement().id().fromAlias("grp")), value(eppRegistryElementPartFControlAction.getPart().getRegistryElement().getId())));

        // по форме контроля
        dql.where(eq(property(EppRealEduGroup4ActionType.type().code().fromAlias("grp")), value(eppRegistryElementPartFControlAction.getControlAction().getCode())));


        DQLSelectBuilder dqlEmpl = new DQLSelectBuilder();
        dqlEmpl.fromEntity(Employee.class, "emp", true);
        dqlEmpl.where(in(property(Employee.id().fromAlias("emp")), dql.buildQuery()));

        List<Employee> lstEmpl = dqlEmpl.createStatement(getSession()).list();

        for (Employee emp : lstEmpl) {
            Tools.getUser(emp);
            retVal.add(emp);
        }


        return retVal;
    }

    @Override
    public List<Student> getStudents4Site(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, EducationYear year,
                    YearDistributionPart partYear, List<DevelopForm> lstDevForm, List<DevelopTech> lstDevTech)
    {

        return getStudents4Site(eppRegistryElementPartFControlAction, year, partYear, lstDevForm, lstDevTech, true);
    }

    @Override
    public List<Student> getStudents4Site(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, EducationYear year,
                    YearDistributionPart partYear, List<DevelopForm> lstDevForm, List<DevelopTech> lstDevTech, boolean fakeLoad)

    {

        // возможно нужно смотреть только на контрольные мероприятия
        // но наши аналитики ни сном ни духом
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppRealEduGroup4ActionTypeRow.class, "edugrp");
        dql.joinPath(DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.studentWpePart().studentWpe().sourceRow().workPlan().fromAlias("edugrp"), "wpbase");

        // приджойним
        dql.joinEntity("wpbase", DQLJoinType.inner, EppWorkPlan.class, "wp", DQLExpressions.eq(
                               DQLExpressions.property("wpbase.id"),
                               DQLExpressions.property("wp.id"))
        );

//TODO DEV-6870
//	  if (lstDevTech!=null && lstDevTech.size()>0)
//   	   	dql.where(in(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().developTech().fromAlias("wp")), lstDevTech ));
//
//	  if (lstDevForm!=null && lstDevForm.size()>0)
//		  dql.where(in(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().developForm().fromAlias("wp")), lstDevForm ));


        // фильтр по годам
        if (year != null)
            dql.where(eq(property(EppRealEduGroup4ActionTypeRow.group().summary().yearPart().year().educationYear().id().fromAlias("edugrp")), value(year.getId())));

        // фильтр по частям
        if (partYear != null)
            dql.where(eq(property(EppRealEduGroup4ActionTypeRow.group().summary().yearPart().part().id().fromAlias("edugrp")), value(partYear.getId())));

        // первичный ключ студента
        dql.column(property(EppRealEduGroup4ActionTypeRow.studentWpePart().studentWpe().student().id().fromAlias("edugrp")));


        dql.where(eq(property(EppRealEduGroup4ActionTypeRow.group().activityPart().number().fromAlias("edugrp")), value(eppRegistryElementPartFControlAction.getPart().getNumber())));
        dql.where(eq(property(EppRealEduGroup4ActionTypeRow.group().activityPart().registryElement().id().fromAlias("edugrp")), value(eppRegistryElementPartFControlAction.getPart().getRegistryElement().getId())));

        // форма контроля
        dql.where(eq(property(EppRealEduGroup4ActionTypeRow.studentWpePart().type().code().fromAlias("edugrp")), value(eppRegistryElementPartFControlAction.getControlAction().getCode())));
        // актуальное
        dql.where(isNull(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("edugrp")));

        DQLSelectBuilder dqlStu = new DQLSelectBuilder();

        dqlStu.fromEntity(Student.class, "student", true);

        dqlStu.joinPath(DQLJoinType.inner, Student.status().fromAlias("student"), "studentStatus")
                .fetchPath(DQLJoinType.inner, Student.person().fromAlias("student"), "person")
                .fetchPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "identityCard");

        dqlStu.where(DQLExpressions.eq(
                property(StudentStatus.active().fromAlias("studentStatus")),
                value(Boolean.TRUE)));

        dqlStu.where(DQLExpressions.eq(
                property(Student.archival().fromAlias("student")),
                value(Boolean.FALSE)));

        dqlStu.where(in(property(Student.id().fromAlias("student")), dql.buildQuery()));
        dqlStu.column("student");

        List<Student> lstStu = dqlStu.createStatement(getSession()).list();

        if (fakeLoad) {
            List<Student> retVal = new ArrayList<Student>();
            for (Student stu : lstStu) {
                _fakeLoad(stu);
                retVal.add(stu);
            }
            return retVal;
        }
        else
            return lstStu;

    }

    @Override
    public List<WrapperStudentToSiteSakai> getStudents4Pub(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction,
                    Integer part, EducationYear yearEduc, YearDistributionPart partYear, List<DevelopForm> developFormSelected, List<DevelopTech> developTechSelected,
                    List<SakaiEntityState> sakaiStates, SiteSakai site, Map<String, UserSakai> users, boolean useLDAP)
    {

        List<WrapperStudentToSiteSakai> lists = new ArrayList<WrapperStudentToSiteSakai>();
        // Map<Long, WrapperStudentToSiteSakai> map = new HashMap<Long, WrapperStudentToSiteSakai>();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppRealEduGroup4ActionTypeRow.class, "edugrp");
        dql.joinPath(DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.studentWpePart().studentWpe().sourceRow().workPlan().fromAlias("edugrp"), "wpbase");

        // учебный год
        dql.joinPath(DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.group().summary().yearPart().year().educationYear().fromAlias("edugrp"), "eduyear");
        // часть года
        dql.joinPath(DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.group().summary().yearPart().part().fromAlias("edugrp"), "part");

        // студент
        dql.joinPath(DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.studentWpePart().studentWpe().student().fromAlias("edugrp"), "student");

        // приджойним
        dql.joinEntity("wpbase", DQLJoinType.inner, EppWorkPlan.class, "wp", DQLExpressions.eq(
                               DQLExpressions.property("wpbase.id"),
                               DQLExpressions.property("wp.id"))
        );

        // форма контроля
        dql.where(eq(property(EppRealEduGroup4ActionTypeRow.studentWpePart().type().code().fromAlias("edugrp")), value(eppRegistryElementPartFControlAction.getControlAction().getCode())));
        // актуальное
        dql.where(isNull(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("edugrp")));


        dql.where(eq(property(EppRealEduGroup4ActionTypeRow.group().activityPart().number().fromAlias("edugrp")), value(part)));
        dql.where(eq(property(EppRealEduGroup4ActionTypeRow.group().activityPart().registryElement().id().fromAlias("edugrp")), value(eppRegistryElementPartFControlAction.getPart().getRegistryElement().getId())));

//TODO DEV-6870
//		  if (developTechSelected!=null && developTechSelected.size()>0)
//	   	   	dql.where(in(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().developTech().fromAlias("wp")), developTechSelected ));
//
//		  if (developFormSelected!=null && developFormSelected.size()>0)
//			  dql.where(in(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().developForm().fromAlias("wp")), developFormSelected ));


        // фильтр по годам
        if (yearEduc != null)
            dql.where(eq(property(EppRealEduGroup4ActionTypeRow.group().summary().yearPart().year().educationYear().id().fromAlias("edugrp")), value(yearEduc.getId())));

        // фильтр по частям
        if (partYear != null)
            dql.where(eq(property(EppRealEduGroup4ActionTypeRow.group().summary().yearPart().part().id().fromAlias("edugrp")), value(partYear.getId())));


        dql.column(property(EducationYear.id().fromAlias("eduyear")), "eduyearId");
        dql.column(property(YearDistributionPart.id().fromAlias("part")), "partId");
        dql.column(property(Student.id().fromAlias("student")), "studentId");

        dql.group(property(EducationYear.id().fromAlias("eduyear")));
        dql.group(property(YearDistributionPart.id().fromAlias("part")));
        dql.group(property(Student.id().fromAlias("student")));


        DQLSelectBuilder rezDQL = new DQLSelectBuilder().fromDataSource(dql.buildQuery(), "main");

        // год
        rezDQL.joinEntity("main", DQLJoinType.inner, EducationYear.class, "year", DQLExpressions.eq(
                                  DQLExpressions.property("main.eduyearId"),
                                  DQLExpressions.property("year.id"))
        );

        // часть года
        rezDQL.joinEntity("main", DQLJoinType.inner, YearDistributionPart.class, "part", DQLExpressions.eq(
                                  DQLExpressions.property("main.partId"),
                                  DQLExpressions.property("part.id"))
        );

        // приджойним студента
        rezDQL.joinEntity("main", DQLJoinType.inner, Student.class, "student", DQLExpressions.eq(
                                  DQLExpressions.property("main.studentId"),
                                  DQLExpressions.property("student.id"))
        );

        rezDQL.joinPath(DQLJoinType.inner, Student.status().fromAlias("student"), "studentStatus")
                .fetchPath(DQLJoinType.inner, Student.person().fromAlias("student"), "p")
                .fetchPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "idCard");

        rezDQL.where(DQLExpressions.eq(
                property(StudentStatus.active().fromAlias("studentStatus")),
                value(Boolean.TRUE)));

        rezDQL.where(DQLExpressions.eq(
                property(Student.archival().fromAlias("student")),
                value(Boolean.FALSE)));


        rezDQL.column("year");
        rezDQL.column("part");
        rezDQL.column("student");

        List<Object[]> lst = rezDQL.createStatement(getSession()).list();
        List<String> userTandemList = new ArrayList<String>();

        for (Object[] row : lst) {
            EducationYear year = (EducationYear) row[0];
            YearDistributionPart prt = (YearDistributionPart) row[1];
            Student student = (Student) row[2];
            String eid = Tools.getPersonEid(student, useLDAP);

            SakaiEntityState state = DataAccessServices.dao().getByCode(SakaiEntityState.class, SakaiEntityStateCodes.NO_INFO);

            WrapperStudentToSiteSakai wrapper = new WrapperStudentToSiteSakai();
            wrapper.setStudent(student);
            wrapper.setYear(year);
            wrapper.setPartYear(prt);
            wrapper.setState(state);
            wrapper.setEid(eid);

            // можно статус посчитать
            if (site != null) {
                state = site.getUserState(student, users, useLDAP);
                wrapper.setState(state);
                userTandemList.add(eid);
            }
            lists.add(wrapper);
        }

        if (site != null) {
            List<String> notInUni = new ArrayList<String>();
            try {
                ActionSakai as = ActionSakai.getActionSakai();
                notInUni = site.getUserNotInUni(userTandemList, as.getSakaiScripts().getSakaiProperty().STUDENT_ROLE);
            }
            catch (Exception ex) {
            } //

            int i = 0;
            // этих в uni нет
            for (String eid : notInUni) {
                i++;
                WrapperStudentToSiteSakai wrapper = new WrapperStudentToSiteSakai();
                wrapper.setStudent(null);
                wrapper.setYear(null);
                wrapper.setPartYear(null);

                wrapper.setState(DataAccessServices.dao().getByCode(SakaiEntityState.class, SakaiEntityStateCodes.NOT_IN_UNI));

                wrapper.setId(Tools.MakeFakeId(i));

                wrapper.setEid(eid);

                UserSakai uS = Tools.getUserByEid(eid, users);

                if (uS != null)
                    wrapper.setTitle(uS.getLastName() + " " + uS.getFirstName());
                else
                    wrapper.setTitle("Нет данных по пользователю");


                lists.add(wrapper);
            }
        }

        //накладываем фильтр на статусы
        List<WrapperStudentToSiteSakai> retVal = new ArrayList<WrapperStudentToSiteSakai>();
        for (WrapperStudentToSiteSakai wr : lists) {
            boolean canAdd = false;
            // проверка на статус
            if (sakaiStates.size() > 0) {
                for (SakaiEntityState ss : sakaiStates) {
                    if (wr.getState() == null)
                        canAdd = true;
                    else {
                        if (ss.equals(wr.getState()))
                            canAdd = true;
                    }
                }
            }
            else
                canAdd = true;

            if (canAdd) retVal.add(wr);
        }

        // нужна сортировка
        Collections.sort(retVal, new Comparator<WrapperStudentToSiteSakai>() {
            @Override
            public int compare(WrapperStudentToSiteSakai o1, WrapperStudentToSiteSakai o2)
            {
                String s1 = o1.getTitle();
                String s2 = o2.getTitle();

                return s1.compareToIgnoreCase(s2);
            }
        });

        return retVal;
    }


    @Override
    public List<Student> getStudents4Site(Group group)
    {
        List<Student> retVal = new ArrayList<Student>();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Student.class, "student");
        dql.where(in(property(Student.group().fromAlias("student")), value(group)));
        dql.where(in(property(Student.status().active().fromAlias("student")), value(Boolean.TRUE)));
        List<Student> lstStu = dql.createStatement(getSession()).list();

        for (Student stu : lstStu) {
            _fakeLoad(stu);
            retVal.add(stu);
        }
        return retVal;
    }

    private void _fakeLoad(Student stu)
    {
        // для избежания неприятностей - нужно загрузить все свойства
        Tools.getUser(stu);

    }

    @Override
    public Group getGroup(Long id)
    {
        Group group = (Group) getSession().get(Group.class, id);
        _fakeLoadGroup(group);
        return group;
    }

    @SuppressWarnings("unused")
    private void _fakeLoadGroup(Group group)
    {
        String _title = group.getCourse().getTitle();
    }


    @Override
    public Map<String, String> getMarkMapForSite(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, boolean usePersonalNumber)
    {

        Map<String, String> retVal = new HashMap<String, String>();

        DQLSelectBuilder dql_action_slot = getDQLSBEppStudentWpCActionSlot(eppRegistryElementPartFControlAction, "eppSlot");
        dql_action_slot.column(property(EppStudentWpeCAction.id().fromAlias("eppSlot")));

        // берем оценки к слотам
        DQLSelectBuilder markDql = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "mark")
                .column("mark")
                .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                .where(in(property(SessionMark.slot().studentWpeCAction().id().fromAlias("mark")), dql_action_slot.buildQuery()));

        List<SessionMark> lst = markDql.createStatement(getSession()).<SessionMark>list();

        for (SessionMark sm : lst) {
            if (sm.getSlot().getDocument() instanceof SessionStudentGradeBookDocument
                            && !sm.isInSession())
            {
                // итоговые оценки - в семестровом журнале
                String keyStudent = "";
                if (usePersonalNumber)
                    keyStudent = sm.getSlot().getActualStudent().getPerNumber();
                else
                    keyStudent = Long.toString(sm.getSlot().getActualStudent().getId());

                String code = sm.getValueItem().getCode();

                retVal.put(keyStudent, code);
            }
        }

        return retVal;
    }

    private DQLSelectBuilder getDQLSBEppStudentWpCActionSlot(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, String alias)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppStudentWpeCAction.class, alias);

        // только по нужной нам дисциплине
        dql.where(eq(property(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().fromAlias(alias)),
                     value(eppRegistryElementPartFControlAction.getPart().getRegistryElement())));

        // часть дисциплины
        dql.where(eq(property(EppStudentWpeCAction.studentWpe().registryElementPart().number().fromAlias(alias)), value(eppRegistryElementPartFControlAction.getPart().getNumber())));

        // форма контроля
        dql.where(eq(property(EppStudentWpeCAction.type().code().fromAlias(alias)), value(eppRegistryElementPartFControlAction.getControlAction().getCode())));


        // только актуальные оценки (не должно быть даты неактуальности)
        dql.where(isNull(EppStudentWpeCAction.removalDate().fromAlias(alias)));

        // связь с дисциплиной реестра
        dql.joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().fromAlias(alias), "regelem");


        dql.joinEntity("regelem", DQLJoinType.inner, EppRegistryDiscipline.class, "erd", eq(property("regelem.id"),property("erd.id")));
        return dql;
    }

    public UniSakaiSiteEtalon getEtalon(EppRegistryElementPart regelem)
    {
        return getByNaturalId(new UniSakaiSiteEtalonGen.NaturalId(regelem.getRegistryElement(), regelem.getNumber()));
    }

    public UniSakaiSiteEtalon saveEtalon(EppRegistryElementPart regelem, String etalonName)
    {
        String etalon = "";
        if (etalonName != null)
            etalon = etalonName.trim();

        EppRegistryElementPart epart = regelem;
        UniSakaiSiteEtalon siteEtalon = getEtalon(epart);
        if (siteEtalon == null && etalon.length() > 0) {
            siteEtalon = new UniSakaiSiteEtalon();
            siteEtalon.setRegistryElement(epart.getRegistryElement());
            siteEtalon.setPart(epart.getNumber());

            siteEtalon.setEtalonSiteId(etalon);
            getSession().save(siteEtalon);
        }
        else if (siteEtalon != null && etalon.length() > 0) {
            siteEtalon.setEtalonSiteId(etalon);
            update(siteEtalon);
        }
        else if (siteEtalon != null) {
            delete(siteEtalon);
            return null;
        }

        return siteEtalon;
    }

    @Override
    public UniSakaiSiteEtalon getEtalon(EppRegistryElement regElem, Integer part)
    {
        return getByNaturalId(new UniSakaiSiteEtalonGen.NaturalId(regElem, part));
    }

    private static Map<String, String> MAPCActionType = null;

    @Override
    public String getCActionTitle(String cactionCode)
    {

        if (MAPCActionType == null) {
            MAPCActionType = new HashMap<String, String>();

            List<EppFControlActionType> lst = getCatalogItemList(EppFControlActionType.class);

            for (EppFControlActionType ca : lst) {
                MAPCActionType.put(ca.getCode(), ca.getTitle());
            }
        }

        if (MAPCActionType.containsKey(cactionCode))
            return MAPCActionType.get(cactionCode);
        else
            return "тип контрольного мероприятия " + cactionCode + " неизвестен ";

    }


    @Override
    public EppRegistryElementPartFControlAction getEppRegistryElementPartFControlAction(Long id)
    {
        EppRegistryElementPartFControlAction retVal = get(EppRegistryElementPartFControlAction.class, id);

        EppRegistryElement elem = retVal.getPart().getRegistryElement();
        elem.getId();
        elem.getFullTitle();
        elem.getShortTitle();

        retVal.getPart().getNumber();
        retVal.getControlAction().getCode();

        return retVal;
    }

}
