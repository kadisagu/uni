package ru.tandemservice.unisakai.component.catalog.uniSakaiSettings.UniSakaiSettingsAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        // прежде всего в модели активируем орг юнит
        getDao().prepare(model);


    }


    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().update(model);
        deactivate(component);
    }

}
