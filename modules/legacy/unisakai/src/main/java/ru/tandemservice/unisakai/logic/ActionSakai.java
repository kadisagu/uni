package ru.tandemservice.unisakai.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisakai.dao.ISakaiDao;
import ru.tandemservice.unisakai.dao.SakaiDaoFacade;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon;
import ru.tandemservice.unisakai.ws.sakai.SakaiScriptService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Регистрируем через BEAN
 * собираем все необходимое для работы
 *
 * @author Администратор
 */
public class ActionSakai {
    public static ActionSakai getActionSakai() throws Exception
    {
        ActionSakai as = (ActionSakai) ApplicationRuntime.getBean("ActionSakai");
        as.Prepare();

        return as;
    }

    private SakaiScripts sakaiScripts = null;
    private String sessionId = "";


    /**
     * нулевой конструктор
     *
     * @throws Exception
     */
    public ActionSakai() throws Exception
    {
    }

    public void Prepare() throws Exception
    {

        if (sakaiScripts == null) {
            try {
                sakaiScripts = new SakaiScripts();
                // готовим соединение
                sakaiScripts.Prepare();
            }
            catch (Exception ex) {
                sakaiScripts = null;
                throw ex;
            }
        }
        else
            sakaiScripts.UpdateProperty();
    }

    /*
     * Переустановить связь с сакаем
     */
    public void Reconnect() throws Exception
    {
        try {
            CloseConnect();
        }
        catch (Exception ex) {
        }

        // подготовим, возможно сменился коннекшин стринг
        getSakaiScripts().Prepare();

        // если логин не состоялся, то сбросим в кеше
        try {
            sessionId = getSakaiScripts().LoginToSakay();
        }
        catch (Exception ex) {
            SakaiGetter.ClearSakaiWSCache();
            throw ex;
        }

        // при реконекте проверяем вызов пустой функции (случай - слетел наймспейс)
        try {
            @SuppressWarnings("unused")
            String test = getSakaiScripts().getScriptService().getSakaiScript().check("test");
            System.out.print(test);
        }
        catch (Exception ex) {
            SakaiGetter.ClearSakaiWSCache();
        }


    }

    protected void CloseConnect() throws Exception
    {
        if (isConnect()) {
            try {
                getSakaiScripts().getLoginService().getSakaiLogin().logout(sessionId);
            }
            catch (Exception ex) {
                sessionId = "";
                throw ex;
            }

        }

    }

    public SakaiScripts getSakaiScripts()
    {
        return sakaiScripts;
    }

    public String getSessionId()
    {
        return sessionId;
    }

    public boolean isConnect()
    {
        return !StringUtils.isEmpty(sessionId);
    }


    public void DropUser(UserSakai user) throws Exception
    {
        // по пользователю желательно получить сведения о всех сайтах - на которых он есть
        // и первым делом выкинуть его из memberov

        String _rezult = getSakaiScripts().getScriptService().getSakaiScript().removeUser(sessionId, user.getEid());

        if (!_rezult.equals("success"))
            throw new Exception("Ошибка вызова WS в Sakai для removeUser user.getEid=" + user.getEid());
    }

    public Map<String, UserSakai> getSakaiUsers() throws Exception
    {
        String _xml = getSakaiScripts().getScriptService().getSakaiScript().getAllUsersUni(sessionId);

        if (_xml.contains("exception"))
            throw new Exception("Ошибка вызова WS в Sakai для getAllUsersUni");

        // создадим по ним хэш таблицу
        return Tools.getSakaiUsers(_xml);
    }

    public Map<String, List<SiteSakai>> getSakaiSites() throws Exception
    {
        String _xml = getSakaiScripts().getScriptService().getSakaiScript().getAllSitesUni(sessionId);
        if (_xml.contains("exception"))
            throw new Exception("Ошибка вызова WS в Sakai для getAllSitesUni");

        // создадим по ним хэш таблицу
        return Tools.getSiteSakaiByLogicId(_xml);
    }


    public void UpdateUser(UserSakai user) throws Exception
    {
        // юзер есть
        String _rezult = getSakaiScripts().getScriptService().getSakaiScript().changeUserNameAndEmail(sessionId, user.getId(), user.getEid(),
                        user.getFirstName(), user.getLastName(), user.getEmail(), "");

        if (!_rezult.equals("success"))
            throw new Exception("Ошибка вызова WS в Sakai для changeUserNameAndEmail user.getEid=" + user.getEid());
        else
            // пользователю можно послать письмо счастья
            SakaiDaoFacade.getSakaiMailDao().MailSendInfoUpdateUser(user);

    }

    public void NewUser(UserSakai user) throws Exception
    {

        String _rezult = getSakaiScripts().getScriptService().getSakaiScript().addNewUserById(sessionId, user.getId(), user.getEid(), user.getFirstName(),
                user.getLastName(), user.getEmail(), user.getType(), user.getPassword(), "");

        if (!_rezult.equals("success"))
            throw new Exception("Ошибка вызова WS в Sakai для addNewUserById user.getEid=" + user.getEid() + _rezult);
        else {
            // пользователю можно послать письмо счастья
            SakaiDaoFacade.getSakaiMailDao().MailSendInfoNewUser(user);
        }
    }


    public void SaveOrUpdateSiteD(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, EducationYear year,
                    YearDistributionPart partYear, boolean syncTi, boolean syncSt, boolean strongListTi, boolean strongListSt, List<DevelopForm> lstDevForm,
                    List<DevelopTech> lstDevTech, Map<String, List<SiteSakai>> sakaiSites, Map<String, UserSakai> sakaiUsers) throws Exception
    {

        eppRegistryElementPartFControlAction = SakaiDaoFacade.getSakaiDao().getEppRegistryElementPartFControlAction(eppRegistryElementPartFControlAction.getId());

        // нужно найти
        SiteSakai site = Tools.getSiteSakaiByEntity(
                eppRegistryElementPartFControlAction.getPart().getRegistryElement()
                , eppRegistryElementPartFControlAction.getPart().getNumber()
                , eppRegistryElementPartFControlAction.getControlAction().getCode()
        );
        boolean need = false;
        SiteSakai siteSakai = Tools.getSiteSakaiMaxTime(sakaiSites, site);
        String exsistId = null;

        // проверяем сайты на основное совпадение
        if (siteSakai != null) {
            exsistId = siteSakai.getSiteid();

            // сравним 2 сайта
            if (!siteSakai.isEqualSite(site))
                need = true;
        }
        else {
            need = true;
            // создали новый сайт
            // нужно добавить его в коллекцию
            Tools.addSiteToCollection(sakaiSites, site);
        }

        if (need) {
            UniSakaiSiteEtalon etalonEntity = SakaiDaoFacade.getSakaiDao().getEtalon(eppRegistryElementPartFControlAction.getPart().getRegistryElement(),
                                                                                  eppRegistryElementPartFControlAction.getPart().getNumber());
            String etalon = null;
            if (etalonEntity != null && etalonEntity.getEtalonSiteId() != null)
                etalon = etalonEntity.getEtalonSiteId();
            SaveOrUpdateSite(site, exsistId, etalon);
        }

        //  тут занимается наполнением сайтов
        if (syncTi) {
            TeachersToSite(eppRegistryElementPartFControlAction, strongListTi, sakaiSites, sakaiUsers);
        }

        if (syncSt) {
            StudentsToSite(eppRegistryElementPartFControlAction, strongListSt, year, partYear, lstDevForm, lstDevTech, sakaiSites, sakaiUsers);
        }
    }

    public void SaveOrUpdateSiteD(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, Map<String, List<SiteSakai>> sakaiSites) throws Exception
    {
        SaveOrUpdateSiteD(eppRegistryElementPartFControlAction, null, null, false, false, false, false, null, null, sakaiSites, null);
    }


    private void StudentsToSite(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, boolean strongList,
            EducationYear year, YearDistributionPart partYear, List<DevelopForm> lstDevForm, List<DevelopTech> lstDevTech,
            Map<String, List<SiteSakai>> sakaiSites, Map<String, UserSakai> sakaiUsers) throws Exception
    {

        boolean useLDAP = getSakaiScripts().getSakaiProperty().USE_LDAP_AUTH;

        // получим полный список студентов
        List<Student> lst = SakaiDaoFacade.getSakaiDao().getStudents4Site(eppRegistryElementPartFControlAction, year, partYear,
                lstDevForm, lstDevTech);

        SiteSakai site = Tools.getSiteSakaiByEntity(eppRegistryElementPartFControlAction.getPart().getRegistryElement(),
                                            eppRegistryElementPartFControlAction.getPart().getNumber(), eppRegistryElementPartFControlAction.getControlAction().getCode());
        SiteSakai siteSakai = Tools.getSiteSakaiMaxTime(sakaiSites, site);

        //Студенты сайта
        List<String> siteStudents = siteSakai.getSiteUsersByRole(getSakaiScripts().getSakaiProperty().STUDENT_ROLE);

        List<String> processUserList = new ArrayList<>();

        for (Student st : lst) {
            String eid = getPersonEid(st);
            if (!useLDAP) {
                UserSakai user = Tools.getUser(st);
                SaveOrUpdateUser(user, sakaiUsers);
            }

            // подпишем пользователя на сайт
            if (!siteStudents.contains(eid))    // нет на сайте
                UserToSite(eid, siteSakai, getSakaiScripts().getSakaiProperty().STUDENT_ROLE);

            processUserList.add(eid);
        }

        // Если активировано строгое соответствие, лишних отписываем
        if (strongList) {
            siteStudents.removeAll(processUserList);
            for (String removeKey : siteStudents) {
                UserRemoveFromSite(removeKey, site);
            }
        }

    }


    /**
     * Разместить преподователей на сайте
     */
    private void TeachersToSite(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, boolean strongList,
            Map<String, List<SiteSakai>> sakaiSites, Map<String, UserSakai> sakaiUsers) throws Exception
    {
        boolean useLDAP = getSakaiScripts().getSakaiProperty().USE_LDAP_AUTH;

        // получим полный список преподов
        List<Employee> lst = SakaiDaoFacade.getSakaiDao().getEmployee4Site(eppRegistryElementPartFControlAction);

        SiteSakai site = Tools.getSiteSakaiByEntity(eppRegistryElementPartFControlAction);
        SiteSakai siteSakai = Tools.getSiteSakaiMaxTime(sakaiSites, site);

        //Преподаватели сайта
        List<String> siteOwners = siteSakai.getSiteUsersByRole(getSakaiScripts().getSakaiProperty().TUTOR_ROLE);
        List<String> precessUserList = new ArrayList<>();

        for (Employee empl : lst) {
            String eid = getPersonEid(empl);
            if (!useLDAP) {
                UserSakai user = Tools.getUser(empl);
                SaveOrUpdateUser(user, sakaiUsers);
            }

            // подпишем пользователя на сайт
            if (!siteOwners.contains(eid))
                UserToSite(eid, siteSakai, getSakaiScripts().getSakaiProperty().TUTOR_ROLE);

            precessUserList.add(eid);
        }

        // Если активировано строгое соответствие, отпишем лишних
        if (strongList) {
            siteOwners.removeAll(precessUserList);
            for (String removeKey : siteOwners) {
                UserRemoveFromSite(removeKey, site);
            }
        }
    }

    public void UserRemoveFromSite(String userEid, SiteSakai site) throws Exception
    {
        getSakaiScripts().getScriptService().getSakaiScript().removeMemberFromSite(sessionId, site.getSiteid(), userEid);
    }

    public void UserToSite(String userEid, SiteSakai site, String role) throws Exception
    {
        getSakaiScripts().getScriptService().getSakaiScript().addMemberToSiteWithRole(sessionId, site.getSiteid(), userEid, role);
    }


    public void SaveOrUpdateUser(UserSakai user, Map<String, UserSakai> sakaiUsers) throws Exception
    {
        // проверим юзера
        if (sakaiUsers.containsKey(user.getId())) {
            // есть
            UserSakai existUser = sakaiUsers.get(user.getId());

            if (!existUser.isEqual(user)) {
                // eid может быть занят - его нужно освободить
                UserSakai existOtherUser = getOtherUserByEid(user, sakaiUsers);

                if (existOtherUser != null) {
                    // eid уже кем-то занят - следовательно занятый eid
                    // нужно освободить
                    existOtherUser.setEid("tmp_" + existOtherUser.getId()); // заменяем eid на id
                    UpdateUser(existOtherUser);
                    sakaiUsers.put(existOtherUser.getId(), existOtherUser);
                }
                // обновим пользака
                UpdateUser(user);
            }

        }
        else {
            UserSakai existUser = getOtherUserByEid(user, sakaiUsers);
            if (existUser != null) {
                // eid уже кем-то занят - следовательно занятый eid
                // нужно освободить
                existUser.setEid("tmp_" + existUser.getId()); // заменяем eid на id
                sakaiUsers.put(existUser.getId(), existUser);
                UpdateUser(existUser);
            }
            NewUser(user);
        }
    }


    private UserSakai getOtherUserByEid(UserSakai user, Map<String, UserSakai> sakaiUsers)
    {
        // ищем простым перебором
        for (String key : sakaiUsers.keySet()) {
            UserSakai exist = sakaiUsers.get(key);

            if (exist.getEid().equals(user.getEid())) {
                if (!exist.getId().equals(user.getId())) {
                    return exist;
                }
            }
        }
        return null;
    }


    /**
     * Это сайт дисциплины
     * @throws Exception
     */
    private void SaveOrUpdateSite(SiteSakai site, String exsistId, String etalonSite) throws Exception
    {
        String id = site.getSiteid();
        if (exsistId != null && exsistId.length() > 0)
            id = exsistId;

        SaveOrUpdateSite(id, site.getTitle(), site.getDescription(), site.getShortdesc(), site.getDisciplineId(),
                site.getDisciplinePart(), getSakaiScripts().getSakaiProperty().TEMPLATE_SITE, etalonSite, ISakaiType.TYPE_DISCIPLINE, site.getCactionType()
        );
    }

    private void SaveOrUpdateSiteAgs(SiteSakai site, String exsistId) throws Exception
    {
        String id = site.getSiteid();
        if (exsistId != null && exsistId.length() > 0)
            id = exsistId;

        SaveOrUpdateSite(id, site.getTitle(), site.getDescription(), site.getShortdesc(), site.getDisciplineId(), site.getDisciplinePart(),
                getSakaiScripts().getSakaiProperty().TEMPLATE_SITE_GROUP, null, ISakaiType.TYPE_GROUP, ISakaiCActionType.CACTIONTYPE_UNDIFINE);
    }


    private void SaveOrUpdateSite(String siteId, String title, String description, String shortDesc, String disciplineId,
                    String disciplinePart, String templateSite, String etalonSite, String siteType, String cActionType) throws Exception
    {

//        String _siteIdVrem = siteId;

        SakaiScriptService _scriptService = getSakaiScripts().getScriptService();


        if (!_scriptService.getSakaiScript().checkForSite(sessionId, siteId)) {

            // нужно проверить наличие шаблонного сайта
            if (!_scriptService.getSakaiScript().checkForSite(sessionId, templateSite))
                // шаблонного сайта нет - поднимем ошибку
                throw new Exception("В Sakai не найден шаблонный сайт с именем " + templateSite + " ");
            String result = _scriptService.getSakaiScript().copySiteShort(sessionId, templateSite, siteId, title, description,
                            shortDesc, false, getSakaiScripts().getSakaiProperty().SITE_JOINERROLE, false, true, getSakaiScripts().getSakaiProperty().SITE_TYPE);

            if (!result.equals("success"))
                throw new Exception("Ошибка вызова WS в Sakai для copySiteShort id=" + siteId + " result = " + result);


            // копируем ресурсы с сайта на сайт
            if (etalonSite != null && etalonSite.length() > 0) {
                if (!_scriptService.getSakaiScript().checkForSite(sessionId, etalonSite))
                    // шаблонного сайта нет - поднимем ошибку
                    throw new Exception("В Sakai не найден шаблонный сайт с именем " + etalonSite + " ");


                result = _scriptService.getSakaiScript().importToolContent(sessionId, etalonSite, siteId);

                if (!result.equals("success"))
                    throw new Exception("Ошибка вызова WS в Sakai для importToolContent id=" + siteId + " result = " + result);
            }
        }
        String result = _scriptService.getSakaiScript().setSitePropertyUni(sessionId, siteId, title, description, shortDesc,
                       disciplineId, disciplinePart, siteType, cActionType);
        if (!result.equals("success"))
            throw new Exception("Ошибка вызова WS в Sakai для changeSite id=" + siteId);
    }


    public void DeleteUserFromSite(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, EducationYear year,
                    YearDistributionPart partYear, boolean useTi, boolean useSt, boolean useStudentFilter, boolean filterInvert,
                    boolean deleteWithMark, List<DevelopForm> lstDevForm, List<DevelopTech> lstDevTech, Map<String, List<SiteSakai>> sakaiSites) throws Exception
    {

        eppRegistryElementPartFControlAction = SakaiDaoFacade.getSakaiDao().getEppRegistryElementPartFControlAction(eppRegistryElementPartFControlAction.getId());

        SiteSakai siteVrem = Tools.getSiteSakaiByEntity(eppRegistryElementPartFControlAction);
        SiteSakai site = Tools.getSiteSakaiMaxTime(sakaiSites, siteVrem);

        // проверяем сайты на основное совпадение
        if (site != null) {
            // 1 - отпишем преподов от сайта
            // с преподами поступаем просто - всех ti отписать
            if (useTi)
                _deleteTiFromSite(site, sakaiSites);

            if (useSt)
                _deleteStudentsFromSite(site, eppRegistryElementPartFControlAction, year, partYear, useStudentFilter, filterInvert,
                                deleteWithMark, lstDevForm, lstDevTech, sakaiSites);

        }


    }


    /**
     * Отписать студентов от сайта
     */
    private void _deleteStudentsFromSite(SiteSakai site, EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction,
            EducationYear year, YearDistributionPart partYear, boolean useStudentFilter, boolean filterInvert, boolean deleteWithMark,
            List<DevelopForm> lstDevForm, List<DevelopTech> lstDevTech, Map<String, List<SiteSakai>> sakaiSites) throws Exception
    {

        SiteSakai siteSakai = Tools.getSiteSakaiMaxTime(sakaiSites, site);

        //Студенты сайта
        List<String> siteStudents = siteSakai.getSiteUsersByRole(getSakaiScripts().getSakaiProperty().STUDENT_ROLE);


        // список еид для удаления с сайта
        List<String> eidDropList = new ArrayList<>();

        // получим полный список студентов
        if (useStudentFilter)
        {
            List<Student> lst = SakaiDaoFacade.getSakaiDao().getStudents4Site(eppRegistryElementPartFControlAction, year, partYear, lstDevForm, lstDevTech);

            if (!filterInvert) {
                // только тех, кто нашелся по фильтру
                for (Student st : lst) {
                    String eid = getPersonEid(st);
                    if (siteStudents.contains(eid))
                        eidDropList.add(eid);
                }
            }
            else {
                // инверсия фильтра - оставим тех, кто в фильтре
                Map<String, String> mapTU = new HashMap<>();

                for (Student st : lst) {
                    String eid = getPersonEid(st);
                    if (!mapTU.containsKey(eid))
                        mapTU.put(eid, eid);
                }

                for (String eid : siteStudents) {
                    // если по eid есть студент - не трогаем
                    if (!mapTU.containsKey(eid))
                        eidDropList.add(eid);
                }
            }
        }


        if (deleteWithMark) {
            ISakaiDao dao = SakaiDaoFacade.getSakaiDao();
            Map<String, String> markMark = dao.getMarkMapForSite(eppRegistryElementPartFControlAction, true);

            for (String sKey : siteStudents) {
                String pn = sKey.replace("tmp_" + UserSakai.PREF_STUDENT, "").replace(UserSakai.PREF_STUDENT, "");
                if (markMark.containsKey(pn)) {
                    String markKey = markMark.get(pn);
                    if (MarkTools.isMarkPositive(markKey)) {
                        // ну есть у студента оценка
                        if (!eidDropList.contains(pn))
                            eidDropList.add(sKey); // в массив на отписку от сайта
                    }
                }
            }
        }

        for (String eid : eidDropList) {
            UserRemoveFromSite(eid, site);
        }
    }


    /**
     * отписать преподавателей от сайта
     * @throws Exception
     */
    private void _deleteTiFromSite(SiteSakai site, Map<String, List<SiteSakai>> sakaiSites) throws Exception
    {
        SiteSakai siteSakai = Tools.getSiteSakaiMaxTime(sakaiSites, site);

        //Преподаватели сайта
        List<String> siteOwners = siteSakai.getSiteUsersByRole(getSakaiScripts().getSakaiProperty().TUTOR_ROLE);

        // выкидываем всех
        for (String eid : siteOwners) {
            UserRemoveFromSite(eid, site);
        }
    }


    /**
     * Синхронизация академической
     * группы студентов
    */
    public void SaveOrUpdateSiteAgs(Group group, boolean syncSt, boolean strongListSt, Map<String, List<SiteSakai>> sakaiSites,
            Map<String, UserSakai> sakaiUsers) throws Exception
    {
        SiteSakai site = Tools.getSiteSakai(group);
        SiteSakai siteExist = Tools.getSiteSakaiMaxTime(sakaiSites, site);
        // проверяем сайты на основное совпадение
        if (siteExist != null) {
            // сравним 2 сайта
            if (!siteExist.isEqualSite(site))
                SaveOrUpdateSiteAgs(site, siteExist.getSiteid());
        }
        else {
            // создали новый сайт
            // нужно добавить его в коллекцию
            Tools.addSiteToCollection(sakaiSites, site);
            SaveOrUpdateSiteAgs(site, null);
        }
        if (syncSt)
            StudentsToSite(group, strongListSt, sakaiSites, sakaiUsers);
    }

    private void StudentsToSite(Group group, boolean strongList, Map<String, List<SiteSakai>> sakaiSites, Map<String, UserSakai> sakaiUsers) throws Exception
    {
        boolean useLDAP = getSakaiScripts().getSakaiProperty().USE_LDAP_AUTH;

        // получим полный список студентов
        List<Student> lst = SakaiDaoFacade.getSakaiDao().getStudents4Site(group);

        SiteSakai site = Tools.getSiteSakai(group);
        SiteSakai siteSakai = Tools.getSiteSakaiMaxTime(sakaiSites, site);

        //Студенты сайта
        List<String> siteStudents = siteSakai.getSiteUsersByRole(getSakaiScripts().getSakaiProperty().STUDENT_ROLE);

        List<String> processUserList = new ArrayList<>();

        for (Student st : lst) {
            String eid = getPersonEid(st);
            if (!useLDAP) {
                UserSakai user = Tools.getUser(st);
                SaveOrUpdateUser(user, sakaiUsers);
            }

            // подпишем пользователя на сайт
            if (!siteStudents.contains(eid))  // нет на сайте
                UserToSite(eid, siteSakai, getSakaiScripts().getSakaiProperty().STUDENT_ROLE);
            processUserList.add(eid);
        }

        // Если активировано строгое соответствие, лишних отписываем
        if (strongList) {
            siteStudents.removeAll(processUserList);
            for (String removeKey : siteStudents)
                UserRemoveFromSite(removeKey, site);
        }
    }

    /**
     * Отписать студентов от АГС
     * (академической группы студентов)
     *
     * @param group
     * @param sakaiSites
     *
     * @throws Exception
     */
    public void DeleteUserFromSite(Group group, Map<String, List<SiteSakai>> sakaiSites) throws Exception
    {
        SiteSakai site = Tools.getSiteSakai(group);
        SiteSakai siteExsist = Tools.getSiteSakaiMaxTime(sakaiSites, site);
        if (siteExsist != null)
            _deleteStudentsFromSite(siteExsist, sakaiSites);

    }


    /**
     * Удалить всех студентов с сайта
     *
     * @param site
     * @param sakaiSites
     *
     * @throws Exception
     */
    private void _deleteStudentsFromSite(SiteSakai site, Map<String, List<SiteSakai>> sakaiSites) throws Exception
    {
        SiteSakai siteSakai = Tools.getSiteSakaiMaxTime(sakaiSites, site);

        //Студенты сайта
        List<String> siteStudents = siteSakai.getSiteUsersByRole(getSakaiScripts().getSakaiProperty().STUDENT_ROLE);

        for (String eid : siteStudents)
            UserRemoveFromSite(eid, site);

    }


    /**
     * Сейчас сайты не удаляются
     *
     * @param siteId
     * @param sakaiSites
     *
     * @throws Exception
     */
    public void DeleteSite(String siteId, Map<String, List<SiteSakai>> sakaiSites) throws Exception
    {

        try {
            // 1 - посмотрим, а есть-ли смысл
            boolean needDel = false;
            if (sakaiSites.containsKey(siteId))
                needDel = true;

            if (!needDel)
                needDel = getSakaiScripts().getScriptService().getSakaiScript().checkForSite(sessionId, siteId);

            if (needDel)
                getSakaiScripts().getScriptService().getSakaiScript().removeSite(sessionId, siteId);
        }
        catch (Exception ex) {
            throw new Exception("Ошибка удаления сайта из Sakai " + ex.getMessage());
        }
    }

    /**
     * Sync personal site for entity
     */
    public String syncMySite(String id) throws Exception
    {
        return getSakaiScripts().getScriptService().getSakaiScript().syncMySite(sessionId, id);
    }

    public String syncMySiteByEid(String eid) throws Exception
    {
        return getSakaiScripts().getScriptService().getSakaiScript().syncMySiteByEid(sessionId, eid);
    }

    public String syncMySite(PersonRole tag) throws Exception
    {
        return syncMySiteByEid(getPersonEid(tag));
    }

    public String getPersonEid(PersonRole pr) {
        return Tools.getPersonEid(pr, getSakaiScripts().getSakaiProperty().USE_LDAP_AUTH);
    }

}
