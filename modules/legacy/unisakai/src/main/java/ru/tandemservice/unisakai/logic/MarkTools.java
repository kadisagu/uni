package ru.tandemservice.unisakai.logic;

/**
 * Вспомогательный класс для работы с оценками
 *
 * @author vch
 */
public class MarkTools {
    public static String[] POSITIVE_MARK = new String[]{"5.5", "5.4", "5.3", "2.5", "state.4"};

    public static boolean isMarkPositive(String markKey)
    {
        for (String s : POSITIVE_MARK) {
            if (markKey.indexOf(s) != -1)
                return true;
        }
        return false;

    }
}
