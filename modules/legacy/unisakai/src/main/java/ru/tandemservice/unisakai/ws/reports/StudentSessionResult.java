package ru.tandemservice.unisakai.ws.reports;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisakai.logic.MarkTools;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class StudentSessionResult extends UniBaseDao implements IWsReport {

    private Student _student = null;


    @Override
    public String getReportHTML(Student student, Object[] params)
    {
        _student = student;

        // нужно получить таблицу (2-х мерный массив строк)
        String[][] rows = getTableRows();

        // все в шаблон
        String[] headers = _getTableHeader();
        return _fillFakeTable(rows, headers);
    }

    private String[] _getTableHeader()
    {
        return new String[]{"Семестр", "Дисциплина", "Форма контроля", "Оценка"};
    }

    private String _fillFakeTable(String[][] rows, String[] header)
    {
        String head = "<table class='listHier' cellspacing='0' cellpadding='0'>";
        String footer = "</table>";

        String retVal = "";
        retVal += head;

        // заголовок
        String main;

        StringBuilder strBuilder = new StringBuilder("Студент: ");
        strBuilder.append(_student.getPerson().getFullFio());
        if (null != _student.getGroup())
            strBuilder.append(" | Группа: ").append(_student.getGroup().getTitle());

        main = strBuilder.toString();


        retVal += _fillTableHeaderGlobal(main, header);
        retVal += _fillTableHeader(header);

        for (String[] str : rows) {
            // это строки
            retVal += "<tr>";

            // внутри столбцы
            for (String s : str) {
                if (s.toLowerCase().contains("<td"))
                    retVal += s;
                else
                    retVal += "<td>" + s + "</td>";
            }
            retVal += "</tr>";
        }
        retVal += footer;
        return retVal;
    }

    private String _fillTableHeaderGlobal(String main, String[] header)
    {
        String retVal = "";

        retVal += "<tr><th colspan='" + header.length + "'>";

        retVal += main;

        retVal += "</th></tr>";

        return retVal;
    }

    /**
     * Формируем заголовок таблицы
     */
    private String _fillTableHeader(String[] header)
    {
        String retVal = "";

        retVal += "<tr>";
        for (String s : header) {
            retVal += "<th>";
            retVal += s;
            retVal += "</th>";

        }
        retVal += "</tr>";
        return retVal;
    }

    /**
     * получим из тандема массив строк
     */
    private String[][] getTableRows()
    {

        List<String[]> lines = new ArrayList<>();

        //список мероприятий студента из РУП
        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, "eppSlot")
                .column("eppSlot")

                        // учебный год
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().year().educationYear().fromAlias("eppSlot")
                        , "eduyear")
                        // часть года
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().part().fromAlias("eppSlot")
                        , "part")
                        // тип КМ
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.type().fromAlias("eppSlot")
                        , "type")
                        // тип дисциплина
                .fetchPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().fromAlias("eppSlot")
                        , "regelem")
                .where(eq(property(EppStudentWpeCAction.studentWpe().student().fromAlias("eppSlot")), value(_student)))
                .where(isNull(EppStudentWpeCAction.removalDate().fromAlias("eppSlot")))

                        // сортировки
                .order(property(EducationYear.intValue().fromAlias("eduyear")))
                .order(property(YearDistributionPart.number().fromAlias("part")))
                .order(property(EppFControlActionType.priority().fromAlias("type")))
                .order(property(EppRegistryElement.title().fromAlias("regelem")));
        List<EppStudentWpeCAction> actionList = dql.createStatement(getSession()).<EppStudentWpeCAction>list();

        for (EppStudentWpeCAction a : actionList) {
            // на основе слотов получаем оценки
            SessionMark totMark = null;
            if (!(a.getStudentWpe().getRegistryElementPart().getRegistryElement() instanceof EppRegistryDiscipline))
                continue;

            //Берем итоговую оценку
            final DQLSelectBuilder markDql = new DQLSelectBuilder()
                    .fromEntity(SessionMark.class, "mark")
                    .column("mark")
                    .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                    .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                    .where(eq(property(SessionMark.slot().studentWpeCAction().fromAlias("mark")), value(a)))
                    .order(property(SessionMark.id().fromAlias("mark")));

            for (final SessionMark mark : markDql.createStatement(getSession()).<SessionMark>list()) {
                if (mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument && !mark.isInSession()) {
                    // итоговые оценки - в зачетке
                    System.out.println(mark.getValueTitle());
                    totMark = mark;
                }
            }

            List<String> lst = _getReportRow(a, totMark);
            lines.add(lst.toArray(new String[lst.size()]));
        }

        return lines.toArray(new String[][]{});
    }

    private List<String> _getReportRow(EppStudentWpeCAction slot,
                                       SessionMark totMark)
    {
        List<String> retVal = new ArrayList<>();

        // год + семестр
        String yt = slot.getStudentWpe().getYear().getEducationYear().getTitle() + " " + slot.getStudentWpe().getPart().getShortTitle();
        retVal.add(yt);

        // дисциплина
        retVal.add(slot.getStudentWpe().getRegistryElementPart().getRegistryElement().getTitle());

        // форма контроля
        retVal.add(slot.getType().getTitle());

        // итоговая оценка
        if (totMark == null)
            retVal.add("-");
        else {
            boolean isPositive = MarkTools.isMarkPositive(totMark.getValueItem().getCode());


            if (!isPositive) {
                // покрасим (но как?)
                retVal.add("<td class='colored'>" + totMark.getValueShortTitle() + "</td>");
            }
            else
                retVal.add(totMark.getValueShortTitle());
        }
        return retVal;
    }

}
