package ru.tandemservice.unisakai.component.sakai.imp.StudentDel;



import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;

import java.util.List;

public class DAO extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.DAO implements IDAO {
    @Override
    public List<WrapperImp> getListData(ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model)
    {
        return getListDataStudent(model);
    }
}
