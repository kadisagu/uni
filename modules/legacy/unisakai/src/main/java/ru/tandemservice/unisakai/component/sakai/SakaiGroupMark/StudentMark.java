package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

import org.hibernate.Session;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotMarkGradeValue;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class StudentMark {

    private static final EntityCodes<SessionObject> SESSION_OBJECT_ENTITY_CODES = new EntityCodes<SessionObject>(SessionObject.class);
    private static final EntityCodes<SessionStudentGradeBookDocument> STG_ENTITY_CODES = new EntityCodes<SessionStudentGradeBookDocument>(SessionStudentGradeBookDocument.class);

    private Session _session;

    private Map<String, List<EppGradeScale>> mapEppGradeScale = new HashMap<String, List<EppGradeScale>>();
    private Map<String, List<Object[]>> mapMarkInfo = new HashMap<String, List<Object[]>>();
    private Map<String, Map<Long, List<SessionDocumentSlot>>> mapSessionDoc = new HashMap<String, Map<Long, List<SessionDocumentSlot>>>();


    public StudentMark(Session session)
    {
        setSession(session);
    }


    /**
     * Максимальная оценка по студенту
     */
    public WrappeStudentMark getMaxStudentMark(Long studentId, EppRegistryElement regElem, int part, String caction)
    {
        WrappeStudentMark retVal = null;

        List<WrappeStudentMark> lst = getStudentMarks(studentId, regElem, part, caction);

        for (WrappeStudentMark m : lst) {
            if (retVal == null)
                retVal = m;
            else {
                // Меньшее значение поля priority - более высокий приоритет.
                if (retVal.getSessionMarkGrade().getPriority() > m.getSessionMarkGrade().getPriority())
                    retVal = m;
            }
        }
        return retVal;
    }

    /**
     * Найти подходящую ведомость
     */
    public SessionDocumentSlot getSessionBulletin(Long studentId, EppRegistryElement regElem, int partNum, String caction, SessionMarkCatalogItem mark)
    {
        // получим разом все документы
        Map<Long, List<SessionDocumentSlot>> lst = getStudentDocument(null, regElem, partNum, caction);

        SessionDocumentSlot retVal = null;

        for (Long id : lst.keySet()) {
            if (!id.equals(studentId))
                continue;

            for (SessionDocumentSlot sds : lst.get(id)) {
                SessionDocument sd = sds.getDocument();
                EppRegistryElementPart part = sds.getStudentWpeCAction().getStudentWpe().getRegistryElementPart();
                if (sd instanceof SessionBulletinDocument) {
                    SessionBulletinDocument bulliten = (SessionBulletinDocument) sd;
                    EppFControlActionType controlActionType = DataAccessServices.dao().get(EppFControlActionType.class, EppFControlActionType.eppGroupType(), bulliten.getGroup().getType());
                    // шкала оценок по умолчанию
                    EppGradeScale defGradeScale = controlActionType.getDefaultGradeScale();
                    // шкала оценок может быть назначена на дисциплину
                    List<EppGradeScale> lstCa = _getGradeScale(part, controlActionType);
                    if (lstCa != null && lstCa.size() > 0) {
                        for (EppGradeScale ent : lstCa) {
                            defGradeScale = ent;
                        }
                    }
                    String _code = defGradeScale.getCode();
                    if (mark.getCode().indexOf(_code) != -1)
                        retVal = sds;
                }
            }
        }
        return retVal;
    }

    private List<EppGradeScale> _getGradeScale(
            EppRegistryElementPart part,
            EppFControlActionType controlActionType
    )
    {

        String key = Long.toString(part.getId()) + Long.toString(controlActionType.getId());

        if (mapEppGradeScale.containsKey(key))
            return mapEppGradeScale.get(key);
        else {

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(EppRegistryElementPartFControlAction.class, "gs")
                    .joinPath(DQLJoinType.inner, EppRegistryElementPartFControlAction.gradeScale().fromAlias("gs"), "gsgs")
                    .column("gsgs")
                    .where(eq(property(EppRegistryElementPartFControlAction.part().fromAlias("gs")), value(part)))
                    .where(eq(property(EppRegistryElementPartFControlAction.controlAction().fromAlias("gs")), value(controlActionType)));

            List<EppGradeScale> lstCa = dql.createStatement(getSession()).list();
            mapEppGradeScale.put(key, lstCa);
            return lstCa;
        }

    }


    /**
     * Все документы по студенту
     * (на основании которых могут быть оценки)
     * Только актуальные
     */
    public Map<Long, List<SessionDocumentSlot>> getStudentDocument(Long studentId, EppRegistryElement regElem, int part, String caction)
    {

        String key = Long.toString(regElem.getId()) + Integer.toString(part) + caction;
        if (studentId != null)
            key += Long.toString(studentId);

        if (mapSessionDoc.containsKey(key))
            return mapSessionDoc.get(key);
        else {
            Map<Long, List<SessionDocumentSlot>> map = new HashMap<Long, List<SessionDocumentSlot>>();

            List<Short> entityCodes = new ArrayList<Short>();
            entityCodes.addAll(SESSION_OBJECT_ENTITY_CODES.get());
            entityCodes.addAll(STG_ENTITY_CODES.get());


            DQLSelectBuilder dqlEppSlot = _getEppSlot(studentId, regElem, part, caction);

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SessionDocumentSlot.class, "slot")
                    .column(property(SessionDocumentSlot.actualStudent().id().fromAlias("slot")))
                    .column("slot")
                    .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot").s(), "doc")
                            //.where(eq(property(SessionDocumentSlot.actualStudent().id().fromAlias("slot")), value(studentId)))
                            // только актуальные документы
                    .where(isNull(property(SessionDocumentSlot.studentWpeCAction().removalDate().fromAlias("slot"))))
                    .where(notIn(property("slot", SessionDocumentSlot.document().clazz()), entityCodes))

                            // все закрытые документы в лес
                    .where(isNull(property(SessionDocumentSlot.document().closeDate().fromAlias("slot"))))
                            // фильтр студент дисциплина часть
                    .where(in(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), dqlEppSlot.buildQuery()));


            List<Object[]> lst = dql.createStatement(getSession()).list();

            for (Object[] objs : lst) {
                Long stId = (Long) objs[0];
                SessionDocumentSlot sd = (SessionDocumentSlot) objs[1];

                if (map.containsKey(stId))
                    map.get(stId).add(sd);
                else {
                    List<SessionDocumentSlot> arr = new ArrayList<SessionDocumentSlot>();
                    arr.add(sd);
                    map.put(stId, arr);
                }
            }
            //
            mapSessionDoc.put(key, map);
            return map;
        }
    }


    /**
     * Мероприятия студента
     *
     * @return
     */
    private DQLSelectBuilder _getEppSlot(Long studentId, EppRegistryElement regElem, int part, String caction)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppStudentWpeCAction.class, "eppSlot");

        dql.column(property(EppStudentWpeCAction.id().fromAlias("eppSlot")));

        // только по студенту
        if (studentId != null)
            dql.where(eq(property(EppStudentWpeCAction.studentWpe().student().id().fromAlias("eppSlot")), value(studentId)));

        // только по нужной нам дисциплине
        dql.where(eq(property(EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().fromAlias("eppSlot")), value(regElem)));

        // часть дисциплины
        dql.where(eq(property(EppStudentWpeCAction.studentWpe().registryElementPart().number().fromAlias("eppSlot")), value(part)));

        // по форме контроля
        dql.where(eq(property(EppStudentWpeCAction.type().code().fromAlias("eppSlot")), value(caction)));


        // только актуальные оценки (не должно быть даты неактуальности)
        dql.where(isNull(EppStudentWpeCAction.removalDate().fromAlias("eppSlot")));

        // список слотов
        // List<Long> lstSlotTest = dql.createStatement(getSession()).list();
        return dql;
    }

    public List<WrappeStudentMark> getStudentMarks(Long studentId, EppRegistryElement regElem, int part, String caction)
    {
        String key = Long.toString(regElem.getId()) + Integer.toString(part) + caction;

        List<Object[]> lstMark = null;
        if (mapMarkInfo.containsKey(key))
            lstMark = mapMarkInfo.get(key);
        else {
            // на основе слотов можно отобрать итоговые оценки в сессию
            final DQLSelectBuilder markDql = new DQLSelectBuilder();
            markDql.fromEntity(SessionMark.class, "mark");

            markDql.column(property(Student.id().fromAlias("student")));
            markDql.column("mark");

            // по всем студентам
            DQLSelectBuilder dqlEppSlot = _getEppSlot(null, regElem, part, caction);

            // по студенту
            markDql.where(in(property(SessionMark.slot().studentWpeCAction().fromAlias("mark")), dqlEppSlot.buildQuery()));

            // только в сессию (но не факт - нам пойдет любая)
            // markDql.where(eq(property(SessionMark.slot().inSession().fromAlias("mark")), value(true)));

            // склеим 2 джойна - нахрена из кода дандема пока не понятно (скорей всего под оценку должен быть документ)
            markDql.fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot");
            markDql.fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document");
            markDql.joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().studentWpe().student().fromAlias("mark"), "student");


            // регулярная оценка (в сессию)
            markDql.joinEntity("mark", DQLJoinType.inner, SessionSlotRegularMark.class, "regularMark", eq(property("regularMark.id"), property("mark.id")));

            // нужно только оценки из шкалы оценок (не статусы)
            markDql.joinEntity("regularMark", DQLJoinType.inner, SessionSlotMarkGradeValue.class, "ssmgv", eq(property("ssmgv.id"), property("regularMark.id")));

            markDql.joinEntity("ssmgv", DQLJoinType.inner, SessionMarkGradeValueCatalogItem.class, "smgv", eq(property("ssmgv." + SessionSlotMarkGradeValue.L_VALUE),property("smgv.id")));

            lstMark = markDql.createStatement(getSession()).list();
            mapMarkInfo.put(key, lstMark);
        }


        List<WrappeStudentMark> lst = new ArrayList<WrappeStudentMark>();

        for (Object[] objs : lstMark) {
            Long stId = (Long) objs[0];

            if (stId.equals(studentId)) {
                SessionMark sm = (SessionMark) objs[1];
                SessionMarkCatalogItem smi = sm.getValueItem();
                SessionMarkGradeValueCatalogItem markItem = (SessionMarkGradeValueCatalogItem) smi;
                WrappeStudentMark wrapper = new WrappeStudentMark(stId, sm, markItem);
                lst.add(wrapper);
            }
        }
        return lst;
    }


    public void setSession(Session _session) {
        this._session = _session;
    }


    public Session getSession() {
        return _session;
    }
}
