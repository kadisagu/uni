package ru.tandemservice.unisakai.logic;

import org.apache.commons.lang.StringUtils;

/**
 * Пользователь сакая
 *
 * @author vch
 */
public class UserSakai {
    public final static String PREF_STUDENT = "st";
    public final static String PREF_EMPLOYEE = "ti";

    private String id;
    private String eid;
    private String email;
    private String type;
    private String firstName;
    private String lastName;
    private String password;

    public UserSakai(String Id,String Eid,String Email,String Type,String FirstName, String LastName)
    {
        this.id = Id;
        this.eid = Eid;
        this.email = Email;
        this.type = Type;
        this.firstName = FirstName;
        this.lastName = LastName;

        if (StringUtils.isEmpty(email))
            email = "null@mail.ru";
    }

    public boolean isHasEmail()
    {
        return !(email.equals("null@mail.ru"));
    }

    public boolean isCanUse()
    {
       return !(isStudent() && !isHasEmail());
    }

    public boolean isStudent()
    {
        return (id.startsWith(PREF_STUDENT));
    }

    public Long getIdLong()
    {
        String sLong = id.replace(PREF_STUDENT, "").replace(PREF_EMPLOYEE, "");

        return Long.parseLong(sLong);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getEid() {
        return eid;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public int hashCode()
    {
        String s = this.toString();
        return s.hashCode();
    }

    public boolean isEqual(UserSakai compare)
    {
        return (compare.hashCode() == hashCode());
    }

    @Override
    public String toString() {

        String retVal = "";

        retVal += "id: " + getId();
        retVal += " eid: " + getEid();
        retVal += " type: " + getType();
        retVal += " email: " + getEmail();
        retVal += " firstName: " + getFirstName();
        retVal += " lastName: " + getLastName();
        return retVal;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getFullFio()
    {
        String retVal = getLastName();
        if (getFirstName() != null) retVal += " " + getFirstName();
        return retVal;
    }
}
