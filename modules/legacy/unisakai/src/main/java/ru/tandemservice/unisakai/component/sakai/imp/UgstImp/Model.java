package ru.tandemservice.unisakai.component.sakai.imp.UgstImp;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;

public class Model extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model {

    private ISelectModel yearModel;
    private List<HSelectOption> partsList;
    private String lastYearKey = "";

    public ISelectModel getYearModel()
    {
        return this.yearModel;
    }

    public void setYearModel(final ISelectModel yearModel)
    {
        this.yearModel = yearModel;
    }

    public List<HSelectOption> getPartsList()
    {
        return this.partsList;
    }

    public void setPartsList(final List<HSelectOption> partsList)
    {
        this.partsList = partsList;
    }

    protected void setYear(EducationYear value)
    {
        this.getSettings().set("year", value);
    }

    protected EducationYear getYear()
    {
        final Object year = this.getSettings().get("year");
        return year instanceof EducationYear ? (EducationYear) year : null;
    }

    protected YearDistributionPart getPart()
    {
        final Object part = this.getSettings().get("part");
        return part instanceof YearDistributionPart ? (YearDistributionPart) part : null;
    }

    protected void setPart(YearDistributionPart value)
    {
        this.getSettings().set("part", value);
    }

    public void setLastYearKey(String lastYearKey) {
        this.lastYearKey = lastYearKey;
    }

    public String getLastYearKey() {
        return lastYearKey;
    }

    public String getNumber()
    {
        if (this.getSettings().get("number") == null)
            return null;
        else
            return (String) this.getSettings().get("number");
    }

    public void setNumber(String number)
    {
        this.getSettings().set("number", number);
    }
}
