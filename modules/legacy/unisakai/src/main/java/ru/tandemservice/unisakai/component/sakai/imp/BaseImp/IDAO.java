package ru.tandemservice.unisakai.component.sakai.imp.BaseImp;


import ru.tandemservice.uni.dao.IUniDao;

import java.util.List;

public interface IDAO extends IUniDao<Model> {
    void refreshDataSource(Model model);

    List<WrapperImp> getListData(Model model);

    List<WrapperImp> getListDataStudent(Model model);

    List<WrapperImp> getListDataTeacher(Model model);
}
