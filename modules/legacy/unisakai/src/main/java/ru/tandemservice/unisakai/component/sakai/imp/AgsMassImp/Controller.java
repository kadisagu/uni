package ru.tandemservice.unisakai.component.sakai.imp.AgsMassImp;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.process.*;
import org.tandemframework.core.settings.DataSettingsFacade;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisakai.dao.SakaiDaoFacade;
import ru.tandemservice.unisakai.logic.ActionSakai;

import java.util.Collection;

public class Controller<U extends IDAO, M extends Model>
        extends AbstractBusinessController<IDAO, Model>
{
    public static String KEY = "sakai.imp.AgsMassImp";

    public String getSettingsKey()
    {
        return KEY;
    }


    protected boolean useSiteIdList()
    {
        return false;
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        Model model = getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey()));
        getDao().prepare(model);
    }

    public void onClickExit(final IBusinessComponent context)
    {
        final Model model = context.getModel();
        DataSettingsFacade.saveSettings(model.getSettings());
        deactivate(context);
    }


    public void onEnd(final IBusinessComponent context)
    {
        onClickExit(context);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void onClickApply(final IBusinessComponent context) throws Exception
    {
        final Model model = context.getModel();
        // final M model = context.getModel();

        DataSettingsFacade.saveSettings(model.getSettings());

        final Collection ids;// =
        if (useSiteIdList())
            ids = model.getSiteIds();
        else
            ids = model.getGroupIds();


        ActionSakai as = ActionSakai.getActionSakai();
        model.setSakaiSites(as.getSakaiSites());
        //считываем пользователей из Sakai только если работаем не через LDAP
        if (!as.getSakaiScripts().getSakaiProperty().USE_LDAP_AUTH)
            model.setSakaiUsers(as.getSakaiUsers());

        synchronized (Model.MUTEX) {
            final IBackgroundProcess process = new BackgroundProcessBase() {


                @Override
                public ProcessResult run(final ProcessState state) {

                    try {

                        try {
                            Thread.sleep(1000);
                        }
                        catch (final Throwable t) {
                        }

                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        try {
                            state.setMaxValue(100);
                            state.setCurrentValue(0);
                            state.setDisplayMode(ProcessDisplayMode.percent);

                            int i = 0;

                            // просматриваем по дисциплинам
                            for (Object id : ids) {
                                state.setCurrentValue(100 * i / ids.size());
                                Thread.yield();

                                _do(model, id);
                                i++;
                            }
                            return null; // закрываем диалог

                        }
                        catch (Exception ex) {
                            throw ex;
                        }
                        finally {
                            model.setProcessEnd(true);

                            eventLock.release();

                            try {
                                Thread.sleep(1000);
                            }
                            catch (final Throwable t) {
                            }
                        }

                    }
                    catch (final Throwable t) {
                        model.setProcessEnd(true);
                        return new ProcessResult("Произошла ошибка " + t.getMessage(), true); // закрываем диалог
                    }
                }
            };
            BackgroundProcessHolder bHolder = new BackgroundProcessHolder();
            model.setProcessEnd(false);
            bHolder.start("Обработка", process);
        }

        System.out.print("Has End");
    }

    /**
     * Основное действие над одним кодом ID
     */
    protected void _do(Model model, Object idObj) throws Exception
    {
        boolean syncSt = model.getProcessSt();
        boolean strongListSt = model.getStrongListSt();
        ActionSakai as = ActionSakai.getActionSakai();

        Long id = (Long) idObj;
        Group group = SakaiDaoFacade.getSakaiDao().getGroup(id);

        if (group == null)
            return;

        as.SaveOrUpdateSiteAgs(group, syncSt, strongListSt, model.getSakaiSites(), model.getSakaiUsers());
    }

}
