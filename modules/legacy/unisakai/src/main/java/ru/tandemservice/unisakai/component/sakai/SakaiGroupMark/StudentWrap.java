package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

public class StudentWrap {

    private String pesonalNumber;
    private Long id;
    private String fio;
    private String principalLogin;
    private String eid; //eid Sakai

    public void setPesonalNumber(String pesonalNumber) {
        this.pesonalNumber = pesonalNumber;
    }

    public String getPesonalNumber() {
        return pesonalNumber;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getFio() {
        return fio;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getPrincipalLogin() {
        return principalLogin;
    }

    public void setPrincipalLogin(String principalLogin) {
        this.principalLogin = principalLogin;
    }

}
