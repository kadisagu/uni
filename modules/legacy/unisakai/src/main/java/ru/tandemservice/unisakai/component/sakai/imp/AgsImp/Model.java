package ru.tandemservice.unisakai.component.sakai.imp.AgsImp;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

public class Model extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model
{

    private ISelectModel courseListModel;
    private ISelectModel territorialOrgUnitListModel;
    private ISelectModel educationLevelsModel;
    private ISelectModel qualificationListModel;
    private ISelectModel developPeriodListModel;
    private ISelectModel developConditionListModel;


    private String grpTitle = "";


    public void setCourseListModel(ISelectModel courseListModel) {
        this.courseListModel = courseListModel;
    }

    public ISelectModel getCourseListModel() {
        return courseListModel;
    }

    public void setTerritorialOrgUnitListModel(
            ISelectModel territorialOrgUnitListModel)
    {
        this.territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public ISelectModel getTerritorialOrgUnitListModel() {
        return territorialOrgUnitListModel;
    }

    public void setEducationLevelsModel(ISelectModel educationLevelsModel) {
        this.educationLevelsModel = educationLevelsModel;
    }

    public ISelectModel getEducationLevelsModel() {
        return educationLevelsModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel) {
        this.qualificationListModel = qualificationListModel;
    }

    public ISelectModel getQualificationListModel() {
        return qualificationListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel) {
        this.developPeriodListModel = developPeriodListModel;
    }

    public ISelectModel getDevelopPeriodListModel() {
        return developPeriodListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel) {
        this.developConditionListModel = developConditionListModel;
    }

    public ISelectModel getDevelopConditionListModel() {
        return developConditionListModel;
    }

    public void setGrpTitle(String grpTitle) {
        this.grpTitle = grpTitle;
    }

    public String getGrpTitle() {
        return grpTitle;
    }


}
