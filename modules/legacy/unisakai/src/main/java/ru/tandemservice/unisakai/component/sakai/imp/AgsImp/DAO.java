package ru.tandemservice.unisakai.component.sakai.imp.AgsImp;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.entity.catalog.codes.SakaiEntityStateCodes;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.Tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DAO extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.DAO implements IDAO
{


    @Override
    public void prepare(final ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model modelBase)
    {
        Model model = (Model) modelBase;
        // Group grp = null;
        /*// это есть в базовой модели
		model.setProducingOrgUnitListModel(new LazySimpleSelectModel<OrgUnit>(UniDaoFacade.getOrgstructDao().getOrgUnitList(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)));
	    model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, model.getPrincipalContext()));
	    model.setDevelopFormListModel(new LazySimpleSelectModel<DevelopForm>(DevelopForm.class, this).setSortProperty(DevelopForm.P_CODE));
	    */
        model.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setTerritorialOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL));
        model.setEducationLevelsModel(new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE) {
            @Override
            @SuppressWarnings({"unchecked", "rawtypes"})
            public ListResult findValues(String filter)
            {
                List<OrgUnit> formativeOrgUnitList = (List<OrgUnit>) model.getSettings().get("formativeOrgUnitList");
                List<OrgUnit> territorialOrgUnitList = (List<OrgUnit>) model.getSettings().get("territorialOrgUnitList");
                List<EducationLevelsHighSchool> list = UniDaoFacade.getEducationLevelDao().getEducationLevelsHighSchoolList(formativeOrgUnitList, territorialOrgUnitList, filter);
                return new ListResult<>(list);
            }
        });
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setDevelopPeriodListModel(new LazySimpleSelectModel<>(DevelopPeriod.class).setSortProperty(DevelopPeriod.P_PRIORITY));
        model.setDevelopConditionListModel(new LazySimpleSelectModel<>(DevelopCondition.class).setSortProperty(DevelopForm.P_CODE));

        // model.setDevelopTechListModel(new LazySimpleSelectModel<DevelopTech>(DevelopTech.class, this).setSortProperty(DevelopTech.P_CODE));

        super.prepare(model);
    }


    @Override
    public List<WrapperImp> getListData(ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model)
    {
        return getListDataAgs((Model)model);
    }


    @SuppressWarnings("rawtypes")
    @Override
    public List<WrapperImp> getListDataAgs(Model model)
    {

        IDataSettings settings = model.getSettings();

        // значения фильтров
        Object getTitle = settings.get("grpTitle");
        Object courseList = settings.get("courseList");
        Object formativeOrgUnitList = settings.get("formativeOrgUnitList");
        Object territorialOrgUnitList = settings.get("territorialOrgUnitList");
        Object producingOrgUnitList = settings.get("producingOrgUnitList");
        Object eduLevelHighSchool = settings.get("eduLevelHighSchool");
        Object qualification = settings.get("qualification");
        Object developFormList = settings.get("developFormList");
        Object developConditionList = settings.get("developConditionList");
        Object developPeriodList = settings.get("developPeriodList");

        Object developTechList = settings.get("developTechList");

        // собираем фильтр
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Group.class, "grp").column("grp");

        if (getTitle != null)
            dql.where(like(property(Group.title().fromAlias("grp")), value("%" + getTitle + "%")));

        if (courseList != null && !((List) courseList).isEmpty())
            dql.where(in(property(Group.course().fromAlias("grp")), (Collection) courseList));

        if (formativeOrgUnitList != null && !((List) formativeOrgUnitList).isEmpty())
            dql.where(in(property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("grp")), (Collection) formativeOrgUnitList));

        if (territorialOrgUnitList != null && !((List) territorialOrgUnitList).isEmpty())
            dql.where(in(property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("grp")), (Collection) territorialOrgUnitList));

        if (producingOrgUnitList != null && !((List) producingOrgUnitList).isEmpty())
            dql.where(in(property(Group.educationOrgUnit().educationLevelHighSchool().orgUnit().fromAlias("grp")), (Collection) producingOrgUnitList));

        if (eduLevelHighSchool != null)
            dql.where(eq(property(Group.educationOrgUnit().educationLevelHighSchool().fromAlias("grp")), commonValue(eduLevelHighSchool)));

        if (qualification != null)
            dql.where(in(property(Group.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("grp")), (Collection) qualification));

        if (developFormList != null && !((List) developFormList).isEmpty())
            dql.where(in(property(Group.educationOrgUnit().developForm().fromAlias("grp")), (Collection) developFormList));

        if (developConditionList != null && !((List) developConditionList).isEmpty())
            dql.where(in(property(Group.educationOrgUnit().developCondition().fromAlias("grp")), (Collection) developConditionList));

        if (developPeriodList != null && !((List) developPeriodList).isEmpty())
            dql.where(in(property(Group.educationOrgUnit().developPeriod().fromAlias("grp")), (Collection) developPeriodList));


        if (developTechList != null && !((List) developTechList).isEmpty())
            dql.where(in(property(Group.educationOrgUnit().developTech().fromAlias("grp")), (Collection) developTechList));


        List<Group> lst = dql.createStatement(getSession()).list();
        List<WrapperImp> retVal = new ArrayList<>();

        for (Group group : lst) {
            SiteSakai site = Tools.getSiteSakai(group);

            if (model.getSakaisites().containsKey(site.getSiteLogicId())) {
                List<SiteSakai> sitesInSakai = model.getSakaisites().get(site.getSiteLogicId());

                for (SiteSakai siteInSakai : sitesInSakai) {
                    SakaiEntityState state;

                    if (siteInSakai.isEqualSite(site))
                        state = model.getStateMap().get(SakaiEntityStateCodes.PRESENT_IN_SAKAI);
                    else
                        state = model.getStateMap().get(SakaiEntityStateCodes.PRESENT_IN_SAKAI_NOT_EQUALS);

                    String warning = "";

                    if (sitesInSakai.size() > 1)
                        warning = "Более 1-го логического ID ";

                    WrapperImp wr = new WrapperImp();
                    wr.setId(group.getId());
                    wr.setEntityType("Группа");
                    wr.setDescription(warning + siteInSakai.getFullTitle());
                    wr.setTag(siteInSakai);
                    wr.setName(siteInSakai.getShortdesc());
                    wr.setSakaiState(state);
                    retVal.add(wr);

                }
            }
            else {
                SakaiEntityState state = model.getStateMap().get(SakaiEntityStateCodes.NOT_IN_SAKAI);

                WrapperImp wr = new WrapperImp();
                wr.setId(group.getId());
                wr.setEntityType("Группа");
                wr.setDescription(site.getFullTitle());
                wr.setTag(site);
                wr.setName(site.getShortdesc());
                wr.setSakaiState(state);
                retVal.add(wr);
            }

        }

        // ищем сайты, которых нет в тандеме
        if (model.getSakaisites() != null) {
            // первичные ключи групп
            List<Long> lstEmpty = model.getSakaiIdDiscipline(Tools.PREF_SITE_GROUP + ":");
            List<String> sakaiExistKey = model.getSakaiSiteExistKey(Tools.PREF_SITE_GROUP + ":");

            if (lstEmpty.size() > 0) {
                // получим список EppRegistryElementPart и именно его посравниваем
                DQLSelectBuilder dqlGrpExist = new DQLSelectBuilder();
                dqlGrpExist.fromEntity(Group.class, "reg");
                dqlGrpExist.column(property(Group.id().fromAlias("reg")));
                dqlGrpExist.where(in(property(Group.id().fromAlias("reg")), lstEmpty));
                List<Object> hasList = dqlGrpExist.createStatement(getSession()).list();
                for (Object obj : hasList) {
                    Long idSite = (Long) obj;
                    String siteId = Tools.getSiteSakaaiId(idSite);
                    sakaiExistKey.remove(siteId);
                }

                // все что осталось в sakaiExistKey не существует в тандеме
                SakaiEntityState state = model.getStateMap().get(SakaiEntityStateCodes.NOT_IN_UNI);

                int idVrem = 0;
                for (String key : sakaiExistKey) {
                    List<SiteSakai> sites = model.getSakaisites().get(key);

                    for (SiteSakai site : sites) {
                        idVrem++;
                        WrapperImp wr = new WrapperImp();
                        wr.setId(makeFaleId(idVrem));

                        wr.setEntityType("Группы нет");
                        wr.setDescription(site.getFullTitle());
                        wr.setTag(site);
                        wr.setName(site.getShortdesc());
                        wr.setSakaiState(state);

                        retVal.add(wr);
                    }
                }

            }
        }

        return applySakaiStateAndNameFilter(retVal, settings);
    }


}
