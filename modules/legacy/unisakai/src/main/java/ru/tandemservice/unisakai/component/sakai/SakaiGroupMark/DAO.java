package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

import org.hibernate.Session;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;

import ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.Tools;
import ru.tandemservice.unisakai.util.Sakai.ImportInfo;
import ru.tandemservice.unisakai.util.Sakai.SakaiMark;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO.MarkData;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DAO extends UniDao<Model> implements IDAO {


    public void sakaiInfoToModel(Model model)
    {
        Map<String, List<SiteSakai>> sites;
        try {
            sites = model.getActionSakai().getSakaiSites();
        }
        catch (Exception e) {
            sites = null;
        }
        model.setSites(sites);
    }

    /**
     * Сохраняем результат работы
     *
     * @param wrap
     * @param info
     */
    @Override
    public void saveReportInNewTransaction(WrapperGM wrap, ImportInfo info)
    {

        Session session = getSession();

        // 1 - грохнем ранее заданные итоги (в последствии можно сохранять для потомков)
        List<Long> lst = wrap.getListMarkImport();

        for (Long idReport : lst) {
            UniSakaiMarkImport rep = get(idReport);
            if (rep != null) {
                session.delete(rep);
                session.flush();
            }
        }

        // новое
        UniSakaiMarkImport rez = new UniSakaiMarkImport();

        rez.setCreateDate(new Date());
        rez.setIsSuccess(info.isSuccess());
        String prim;
        if (info.isSuccess()) {
            prim = "без ошибок";

            if (info.getRows().size() < 1)
                prim += " пустая";
        }
        else {
            prim = "ошибки";
        }

        rez.setPrim(prim);

        EppRegistryElement epp = get(EppRegistryElement.class, wrap.getIdRegistryElement());


        rez.setPart(wrap.getPart());
        rez.setRegistryElement(epp);
        rez.setCactioncode(wrap.getCactionCode());

        DatabaseFile content = _makeContent(wrap, info);

        session.save(content);
        session.flush();

        // теперь контент
        rez.setContent(content);

        session.save(rez);
        session.flush();
    }


    private DatabaseFile _makeContent(WrapperGM wrap, ImportInfo info)
    {
        MakeReport report = new MakeReport();
        return report.getContent(wrap, info);
    }

    /**
     * Импорт оценок по врапперу
     *
     * @param model
     * @param wrap
     *
     * @throws Exception
     */
    @Override
    public List<Exception> doMarkImportInNewTransaction(Model model, WrapperGM wrap, ImportInfo info) throws Exception
    {
        List<Exception> retVal = new ArrayList<>();

        Session session = getSession();
        StudentMark smProcess = new StudentMark(session);

        // из сакая получаем сайт с оценками
        EppRegistryElement elem = get(EppRegistryElement.class, wrap.getIdRegistryElement());

        SiteSakai siteVrem = Tools.getSiteSakaiByEntity(elem, wrap.getPart(), wrap.getCactionCode());
        SiteSakai site = Tools.getSiteSakaiMaxTime(model.getSites(), siteVrem);
//        String siteId = site.getSiteid();
        if (site == null) return retVal;

        String _xml = model.getActionSakai().getSakaiScripts().getScriptService().getSakaiScript().getMarkSiteStudent(model.getActionSakai().getSessionId(), site.getSiteid());
        Map<String, MarkInfo> mapMarks = Tools.getSiteMark(_xml);

        boolean useLDAP = model.isUseLDAP();

        if (mapMarks.keySet().size() == 0)
            info.AddTotal("На сайте нет информации об оценках");
        else {
            // разом получим все eid студентов из тандема
            Map<String, StudentWrap> lstStudentPersonalNumber = _getStudentPersonalNumber(useLDAP);


            // можно переносить оценки в студента
            for (String eid : mapMarks.keySet()) {
                StudentWrap st = lstStudentPersonalNumber.get(eid);

                if (st == null) {
                    info.AddRow("Студент " + eid, "Не найдено соответствие в Тандеме");
                    info.setSuccess(false);
                }
                else {
                    MarkInfo markInfo = mapMarks.get(eid);

                    // смотрим оценку
                    String _mark = markInfo.getMark().toLowerCase();
                    SessionMarkCatalogItem sakaiMark = SakaiMark.getMark(_mark);

                    if (sakaiMark == null) {
                        info.AddRow(st.getFio(), " Для оценки (" + _mark + ") не найдено соответствия");
                        info.setSuccess(false);
                    }
                    else {
                        EppRegistryElement regElem = get(EppRegistryElement.class, wrap.getIdRegistryElement());

                        // все оценки студента
                        WrappeStudentMark maxMark = smProcess.getMaxStudentMark(st.getId(), regElem, wrap.getPart(), wrap.getCactionCode());

                        // оценку ставим, только если итоги отличны (по макимальной оценке)

                        if (maxMark == null|| maxMark.getSessionMarkGrade().getPriority() != sakaiMark.getPriority())
                        {
                            // ищем ведомость
                            SessionDocumentSlot bulliten = smProcess.getSessionBulletin(st.getId(), regElem, wrap.getPart(), wrap.getCactionCode(), sakaiMark);
                            if (bulliten == null) {
                                info.AddRow(st.getFio(), " Для оценки (" + _mark + ") не найдено подходящего документа");
                                info.setSuccess(false);
                            }
                            else {
                                if (!bulliten.getDocument().isEditable()) {
                                    // обработка ситуации документ закрыт, но оценка есть
                                    if (maxMark != null && maxMark.getSessionMarkGrade().getPriority() == sakaiMark.getPriority()) {
                                        info.AddRow(st.getFio(), " Документ " + bulliten.getDocument().getTitle() + " закрыт, выставлено ранее");
                                        info.setSuccess(true);
                                    }
                                    else {
                                        info.AddRow(st.getFio(), " Документ " + bulliten.getDocument().getTitle() + " закрыт ");
                                        info.setSuccess(false);
                                    }
                                }
                                else {
                                    // пробуем поставить оценку
                                    try {
                                        // проверим наличие комиссии
                                        // если комиссии нет - оценку убираем
                                        // проверим наличие pps в комиссии

                                        final DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                                                .fromEntity(SessionComissionPps.class, "rel")
                                                .column("rel")
                                                .joinPath(DQLJoinType.inner, SessionComissionPps.commission().fromAlias("rel"), "comm")
                                                .where(eq(property(SessionComission.id().fromAlias("comm")), DQLExpressions.value(bulliten.getCommission())));
                                        List<SessionComissionPps> lstPps = tutorDQL.createStatement(session).<SessionComissionPps>list();


                                        if (lstPps.size() == 0) {
                                            // нет комиссии
                                            info.AddRow(st.getFio(), "Документ " + bulliten.getDocument().getTitle() + " нет преподавателей ");
                                            info.setSuccess(false);
                                        }
                                        else {
                                            MarkData md = getMark(bulliten, sakaiMark);
                                            SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(bulliten, md);
                                            info.AddRow(st.getFio(), "Оценка (" + _mark + ") выставлена ");

                                            session.flush();
                                        }

                                    }
                                    catch (Exception ex) {
                                        info.AddRow(st.getFio(), "Оценка (" + _mark + ") не сохранена в документе № " + bulliten.getDocument().getNumber() + " ошибка: " + ex.getMessage());
                                        session.clear();
                                        retVal.add(ex);
                                        throw ex;
                                    }
                                }
                            }
                        }
                        else
                            info.AddRow(st.getFio(), " Оценка (" + _mark + ") проставлена ранее ");
                    }
                }
            }
        }

        return retVal;
    }


    private Map<String, StudentWrap> _getStudentPersonalNumber(boolean useLDAP)
    {
        Map<String, StudentWrap> retVal = new HashMap<>();

        // формируем dql
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Student.class, "student")
                .joinPath(DQLJoinType.inner, Student.status().fromAlias("student"), "studentStatus")
                .joinPath(DQLJoinType.inner, Student.person().fromAlias("student"), "person")
                .joinPath(DQLJoinType.inner, Student.principal().fromAlias("student"), "principal")
                .joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "identityCard");

        dql.where(DQLExpressions.eq(property(StudentStatus.active().fromAlias("studentStatus")), value(Boolean.TRUE)));

        dql.where(DQLExpressions.eq(
                property(Student.archival().fromAlias("student")),
                value(Boolean.FALSE)));

        dql.column(property(Student.id().fromAlias("student")));
        dql.column(property(Student.personalNumber().fromAlias("student")));
        dql.column(property(IdentityCard.lastName().fromAlias("identityCard")));
        dql.column(property(IdentityCard.firstName().fromAlias("identityCard")));
        dql.column(property(IdentityCard.middleName().fromAlias("identityCard")));
        dql.column(property(Principal.login().fromAlias("principal")));


        List<Object[]> lst = dql.createStatement(getSession()).list();

        for (Object[] objs : lst) {

            StudentWrap st = new StudentWrap();
            st.setId((Long) objs[0]);
            st.setPesonalNumber(Integer.toString((Integer) objs[1]));
            String fio = objs[2] + " " + objs[3] + " " + objs[4];

            st.setPrincipalLogin((String) objs[5]);
            st.setFio(fio);

            String eid = Tools.getPersonEid(st, useLDAP);
            st.setEid(eid);

            retVal.put(eid, st);
        }
        return retVal;
    }

    private MarkData getMark(SessionDocumentSlot bulliten,
                             SessionMarkCatalogItem mark)
    {

        SessionDocument sd = bulliten.getDocument();
        Date performDate = new Date();

        if (sd instanceof SessionBulletinDocument) {
            SessionBulletinDocument bul = (SessionBulletinDocument) sd;
            performDate = bul.getPerformDate();
        }

        if (performDate == null)
            performDate = new Date();

        final Date pd = performDate;
        final SessionMarkCatalogItem mv = mark;
        return new MarkData() {

            @Override
            public Double getPoints()
            {
                return null;
            }

            @Override
            public Date getPerformDate()
            {
                return pd;
            }

            @Override
            public SessionMarkCatalogItem getMarkValue()
            {
                return mv;
            }

            @Override
            public String getComment()
            {
                return null;
            }
        };
    }

    public void prepare(final Model model)
    {

        model.setYearModel(new EducationYearModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder sessionObjectExists = new DQLSelectBuilder()
                        .fromEntity(SessionObject.class, alias)
                        .column(property(SessionObject.educationYear().id().fromAlias(alias)))
                        .distinct();

                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationYear.class, alias);
                dql.where(in(property(EducationYear.id().fromAlias(alias)), sessionObjectExists.buildQuery()));
                dql.order(property(EducationYear.intValue().fromAlias(alias)));
                return dql;
            }
        });


        DQLSelectBuilder partsDQL = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, "obj")
                .column(property(SessionObject.yearDistributionPart().fromAlias("obj")))

                .distinct()
                .order(property(SessionObject.yearDistributionPart().code().fromAlias("obj")));

        if (model.getYear() != null)
            partsDQL.where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(model.getYear())));

        Set<YearDistributionPart> parts = new LinkedHashSet<YearDistributionPart>(partsDQL.createStatement(getSession()).<YearDistributionPart>list());
        model.setPartsList(HierarchyUtil.listHierarchyNodesWithParents(new ArrayList<IHierarchyItem>(parts), false));


    }

    @Override
    public void refreshDataSource(Model model)
    {
        List<WrapperGM> itemList = _getListData(model);
        DynamicListDataSource<WrapperGM> dataSource = model.getDataSource();

        // применим сортировки
        final EntityOrder entOrder = dataSource.getEntityOrder();
        final String columnName = entOrder.getColumnName().replace(".null", "");

        // System.out.println(columnName);
        // <TODO> vch
        // откуда то для ряда свойств стали появлятся приписки, типа .null
        // откуда это появляется - х.з (заметила Таня уже на сильно старой весрии)
        // для имен полей, совп. с TU сущностями
        // при повышении версии эту странность надо учесть

        if (!columnName.endsWith(".null"))
            // применим сортировку
            Collections.sort(itemList, new Comparator<WrapperGM>() {
                                 @Override
                                 public int compare(WrapperGM o1, WrapperGM o2)
                                 {
                                     // получим свойства
                                     Object obj1 = o1.getProperty(columnName);
                                     Object obj2 = o2.getProperty(columnName);


                                     String s1 = "";
                                     String s2 = "";

                                     if (obj1 != null)
                                         s1 = (String) obj1;
                                     if (obj2 != null)
                                         s2 = (String) obj2;

                                     if (entOrder.getDirection() == OrderDirection.asc)
                                         return s1.compareToIgnoreCase(s2);
                                     else
                                         return s2.compareToIgnoreCase(s1);
                                 }
                             }
            );
        UniBaseUtils.createPage(dataSource, itemList);
    }


    /**
     * Формируем обертку
     *
     * @param model
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    private List<WrapperGM> _getListData(Model model)
    {
        if (model.getYear() == null || model.getPart() == null)
            return new ArrayList<>();

        //EppRegistryElementPartFControlAction t; - это нужно найти

        // получим все группы
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroup4ActionTypeRow.class, "row")
                .where(isNull(property(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("row"))))

                        // .fetchPath(DQLJoinType.inner, IEppRealEduGroupRowGen.group().fromAlias("row").s(), "grp")

                .joinPath(DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.group().fromAlias("row"), "grp")
                .joinPath(DQLJoinType.inner, EppRealEduGroup4ActionType.activityPart().fromAlias("grp"), "regelempart")
                .joinPath(DQLJoinType.inner, EppRegistryElementPart.registryElement().fromAlias("regelempart"), "regelem")
                .joinPath(DQLJoinType.inner, EppRealEduGroup4ActionType.summary().yearPart().year().educationYear().fromAlias("grp"), "eduYear")

                        // контрольное мероприятия
                .joinPath(DQLJoinType.inner, EppRealEduGroup4ActionType.type().fromAlias("grp"), "caction")


                        // результат импорта
                .joinEntity("row", DQLJoinType.left, UniSakaiMarkImport.class, "printmi", DQLExpressions.and(
                        DQLExpressions.eq(DQLExpressions.property(UniSakaiMarkImport.part().fromAlias("printmi")), DQLExpressions.property(EppRegistryElementPart.number().fromAlias("regelempart"))),
                        DQLExpressions.eq(DQLExpressions.property(UniSakaiMarkImport.registryElement().id().fromAlias("printmi")), DQLExpressions.property(EppRegistryElement.id().fromAlias("regelem"))),

                        // контрольное мероприятие
                        DQLExpressions.eq(DQLExpressions.property(UniSakaiMarkImport.cactioncode().fromAlias("printmi")), DQLExpressions.property(EppFControlActionType.code().fromAlias("caction")))
                ))
                .column(property(EppRegistryElementPart.id().fromAlias("regelempart")), "regelempart")
                .column(property(EppRegistryElement.id().fromAlias("regelem")), "regelem")
                .column(property(EducationYear.title().fromAlias("eduYear")), "eduYear")
                .column(property(UniSakaiMarkImport.id().fromAlias("printmi")), "printId")
                .column(property(UniSakaiMarkImport.createDate().fromAlias("printmi")), "printDate")
                .column(property(UniSakaiMarkImport.prim().fromAlias("printmi")), "printPrim")

                        // тип контрольного мероприятия и название контрольного мероприятия
                .column(property(EppRealEduGroup4ActionType.type().code().fromAlias("grp")), "cactioncode")
                .column(property(EppRealEduGroup4ActionType.type().title().fromAlias("grp")), "cactiontitle");


        // год
        if (model.getYear() != null)
            dql.where(eq(property(EppRealEduGroup4ActionType.summary().yearPart().year().educationYear().id().fromAlias("grp")), value(model.getYear().getId())));
        // часть
        if (model.getPart() != null)
            dql.where(eq(property(EppRealEduGroup4ActionType.summary().yearPart().part().id().fromAlias("grp")), value(model.getPart().getId())));

        // вставь фильтр по имени part.getTitle()
        if (model.getName() != null)
            dql.where(
                    likeUpper(
                            property(
                                    EppRegistryElement.shortTitle().fromAlias("regelem")
                            )
                            , value(("%" + model.getName() + "%").toUpperCase())));

        // вставь фильтр по номеру дисциплины
        if (model.getNumber() != null)
            dql.where(
                    likeUpper(
                            property(
                                    EppRegistryElement.number().fromAlias("regelem")
                            )
                            , value(("%" + model.getNumber() + "%").toUpperCase())));


        dql.group(property(EppRegistryElementPart.id().fromAlias("regelempart")))
                .group(property(EppRegistryElement.id().fromAlias("regelem")))
                .group(property(EducationYear.title().fromAlias("eduYear")))
                .group(property(UniSakaiMarkImport.id().fromAlias("printmi")))
                .group(property(UniSakaiMarkImport.createDate().fromAlias("printmi")))
                .group(property(UniSakaiMarkImport.prim().fromAlias("printmi")))

                .group(property(EppRealEduGroup4ActionType.type().code().fromAlias("grp")))
                .group(property(EppRealEduGroup4ActionType.type().title().fromAlias("grp")));

        //List<Object[]> rows = dql.createStatement(getSession()).list();

        DQLSelectBuilder dqlRez = new DQLSelectBuilder()
                .fromDataSource(dql.buildQuery(), "mt")
                .column("mt.regelempart")
                .column("mt.regelem")
                .column("mt.eduYear")
                .column("mt.printId")
                .column("mt.printDate")
                .column("mt.printPrim")


                .joinEntity("mt", DQLJoinType.inner, EppRegistryElementPart.class, "part", DQLExpressions.eq(
                        DQLExpressions.property("mt.regelempart"),
                        DQLExpressions.property(EppRegistryElementPart.id().fromAlias("part"))
                ))

                .joinEntity("mt", DQLJoinType.inner, EppRegistryElement.class, "reg", DQLExpressions.eq(
                        DQLExpressions.property("mt.regelem"),
                        DQLExpressions.property(EppRegistryElement.id().fromAlias("reg"))
                ))

                .column(property(EppRegistryElementPart.number().fromAlias("part")))
                .column(property(EppRegistryElement.shortTitle().fromAlias("reg")))
                .column(property(EppRegistryElement.number().fromAlias("reg")))

                .column("mt.cactioncode")
                .column("mt.cactiontitle");


        // dqlRez.addColumn("part");

        List<Object[]> rows = dqlRez.createStatement(getSession()).list();
        Map<String, WrapperGM> map = new HashMap<>();

        int i = 0;

        for (Object[] objs : rows) {
            //EppRegistryElementPart part = (EppRegistryElementPart) objs[6]; //get(_idPart);
            Long _idDiscipline = (Long) objs[1];
            String eduYear = (String) objs[2];
            int _partNum = (Integer) objs[6];
            String _disciplineShortTitle = (String) objs[7];
            String _disciplineNumber = (String) objs[8];
            String _caction = (String) objs[9];


            String _key = Long.toString(_idDiscipline) + "_" + Integer.toString(_partNum) + "_" + _caction;

            Long miId = null;
            Date miCreateDate = null;
            String miPrim = null;


            if (objs[3] != null && objs[4] != null && objs[5] != null) {
                miId = (Long) objs[3];
                miCreateDate = (Date) objs[4];
                miPrim = (String) objs[5];
            }

            if (!map.containsKey(_key)) {
                i++;
                String _groupName = _disciplineShortTitle + " №" + _disciplineNumber;

                WrapperGM wrap = new WrapperGM(Tools.MakeFakeId(i), _idDiscipline, _partNum, _caction, eduYear, _groupName);

                if (miId != null) {
                    wrap.setMarkImport(miId, miCreateDate, miPrim);
                    wrap.AddMarkImport(miId);
                }

                map.put(_key, wrap);
            }
            else {
                WrapperGM wrap = map.get(_key);
                if (miId != null) {
                    wrap.setMarkImport(miId, miCreateDate, miPrim);
                    wrap.AddMarkImport(miId);
                }
            }
        }
        return new ArrayList(map.values());
    }
}
