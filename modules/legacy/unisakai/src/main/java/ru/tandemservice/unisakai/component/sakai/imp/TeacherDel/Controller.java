package ru.tandemservice.unisakai.component.sakai.imp.TeacherDel;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;
import ru.tandemservice.unisakai.logic.UserSakai;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class Controller extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Controller
{
    @Override
    public boolean needLoadSakaiSites() {
        return false;
    }

    @Override
    public boolean needLoadSakaiUsers() {
        return true;
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        // сначала активацим старое
        super.onRefreshComponent(component);
    }

    @Override
    public void MakeDataColumn(DynamicListDataSource<WrapperImp> dataSource)
    {
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Сущность", "entityType").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Имя", "name").setClickable(true));
        dataSource.addColumn(new SimpleColumn("Описание", "description").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Статус в Sakai", "sakaiInfo").setClickable(false));
    }


    @Override
    public List<WrapperImp> getProcessList(ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model)
    {
        CheckboxColumn ck = (CheckboxColumn) model.getDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        List<WrapperImp> lstWr = new ArrayList<>();
        for (IEntity e : lst) {
            WrapperImp w = (WrapperImp) e;
            if (w != null)
                lstWr.add(w);
        }
        List<IEntity> empty = new ArrayList<>();
        ck.setSelectedObjects(empty);
        return lstWr;
    }

    @Override
    public void ProcessRow(WrapperImp row, ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model) throws Exception
    {

        Map<String, UserSakai> sakaiusers = model.getSakaiusers();
        if (sakaiusers == null)
            return;

        if (!model.getActionSakai().isConnect())
            return;

        if (row.getTag() instanceof UserSakai) {
            UserSakai userDrop = (UserSakai) row.getTag();

            model.getActionSakai().DropUser(userDrop);
        }
        else {
            String _id = Long.toString(row.getId());
            _removeByKey(UserSakai.PREF_EMPLOYEE + _id, sakaiusers, (Model)model);
            _removeByKey("tmp_" + UserSakai.PREF_EMPLOYEE + _id, sakaiusers, (Model)model);
        }
    }

    private void _removeByKey(String key, Map<String, UserSakai> sakaiusers, Model model) throws Exception
    {
        if (sakaiusers.containsKey(key)) {
            UserSakai userDrop = sakaiusers.get(key);
            model.getActionSakai().DropUser(userDrop);
        }
    }

    @Override
    public String getSettingsKey()
    {
        return "sakai.imp.TiDel";
    }

    @Override
    public void onPostProcess()
    {
    }

}
