package ru.tandemservice.unisakai.component.sakai.imp.AgsImp;


import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;

import java.util.List;


public interface IDAO extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.IDAO
{
    List<WrapperImp> getListDataAgs(Model model);

}
