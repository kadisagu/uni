package ru.tandemservice.unisakai.uniepp.component.registry.RegElementPartEdit;

public class Model
        extends ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Model
{

    private String sakaiEtalonSiteId = "";


    public void setSakaiEtalonSiteId(String sakaiEtalonSiteId) {
        this.sakaiEtalonSiteId = sakaiEtalonSiteId;
    }

    public String getSakaiEtalonSiteId() {
        return sakaiEtalonSiteId;
    }

}
