package ru.tandemservice.unisakai.logic;

import org.tandemframework.shared.person.base.entity.PersonRole;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unisakai.dao.SakaiDaoFacade;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.entity.catalog.codes.SakaiEntityStateCodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SiteSakai {

    private String siteId;
    private String title;
    private String description;
    private String shortdesc;
    private String disciplineId;
    private String disciplinePart;
    private String cactionType;

    private Long entityId = null;

    /**
     * Карта роль - пользователи этой роли на сайте
     */
    private Map<String, List<String>> siteUserMap = new HashMap<>();


    public SiteSakai(String siteId, String title, String description, String shortdesc, String disciplineId, String disciplinePart, String cactionType)
    {
        this.siteId = siteId;
        this.title = title;
        this.description = description;
        this.shortdesc = shortdesc;
        this.disciplineId = disciplineId;
        this.disciplinePart = disciplinePart;
        this.setCactionType(cactionType);
    }

    public void setSiteid(String siteid) {
        this.siteId = siteid;
    }

    public String getSiteid() {
        return siteId;
    }

    /**
     * Логический ключ сайта
     */
    public String getSiteLogicId()
    {
        if (getSiteid().startsWith(Tools.PREF_SITE_UGS1))
            return Tools.PREF_SITE_UGS1 + getDisciplineId() + Tools.PREF_SITE_UGS2 + ":" + getDisciplinePart() + Tools.PREF_SITE_UGS3 + ":" + cactionType;


        if (getSiteid().startsWith(Tools.PREF_SITE_GROUP))
            return Tools.PREF_SITE_GROUP + ":" + getDisciplineId();

        // для неизвестно чего - на выходе что-то
        String retVal = "TU:" + getDisciplineId();
        if (getDisciplinePart() != null)
            retVal = "Part:" + getDisciplinePart();
        return retVal;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setShortdesc(String shortdesc) {
        this.shortdesc = shortdesc;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public void setDisciplineId(String disciplineId) {
        this.disciplineId = disciplineId;
    }

    public String getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplinePart(String disciplinePart) {
        this.disciplinePart = disciplinePart;
    }

    public String getDisciplinePart() {
        return disciplinePart;
    }

    public static String getFullName(String Name, String Part, String cactionCode)
    {
        String cactionTypeTitle = SakaiDaoFacade.getSakaiDao().getCActionTitle(cactionCode);

        return Name + " часть " + Part + " " + cactionTypeTitle;
    }

    public static String getShortName(String Name, String Part, String cactionCode)
    {
        String cactionTypeTitle = SakaiDaoFacade.getSakaiDao().getCActionTitle(cactionCode);
        return Name + " ч: " + Part + cactionTypeTitle;
    }


    public boolean isEqualSite(SiteSakai compareSite)
    {
        return (this.disciplineId.equals(compareSite.getDisciplineId())
                        && this.disciplinePart.equals(compareSite.getDisciplinePart())
                        && this.title.equals(compareSite.getTitle())
                        && this.description.equals(compareSite.getDescription()));
    }

    public String getFullTitle()
    {
        return "SiteId: " + getSiteid() + " " + getDescription();
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public SakaiEntityState getUserState(PersonRole person,Map<String, UserSakai> users,boolean useLDAP)
    {
        List<String> map = getSiteUsers();

        String eid = Tools.getPersonEid(person, useLDAP);

        // самое главное - нет на сайте
        if (!map.contains(eid))
            return UniDaoFacade.getCoreDao().getCatalogItem(SakaiEntityState.class, SakaiEntityStateCodes.NOT_IN_SITE);

        if (useLDAP)
            return UniDaoFacade.getCoreDao().getCatalogItem(SakaiEntityState.class, SakaiEntityStateCodes.PRESENT_IN_SAKAI);

        UserSakai userTandem = Tools.getUser(person);
        UserSakai userSakai = null;
        if (users != null)
            userSakai = users.get(userTandem.getId());

        if (userSakai == null) //невероятный случай, поскольку мы уже проверили, что на сайте он есть
            return UniDaoFacade.getCoreDao().getCatalogItem(SakaiEntityState.class, SakaiEntityStateCodes.NOT_IN_SAKAI);
        else {
            if (userSakai.isEqual(userTandem))
                return UniDaoFacade.getCoreDao().getCatalogItem(SakaiEntityState.class, SakaiEntityStateCodes.PRESENT_IN_SAKAI);
            else
                return UniDaoFacade.getCoreDao().getCatalogItem(SakaiEntityState.class, SakaiEntityStateCodes.PRESENT_IN_SAKAI_NOT_EQUALS);
        }
    }

    /**
     * eid пользователей, вне тандема
     *
     * @param userTandemList
     *
     * @return
     */
    public List<String> getUserNotInUni(List<String> userTandemList, String role)
    {
        List<String> retVal = new ArrayList<>();

        //список eid пользователей сайта
        List<String> siteUsers = getSiteUsersByRole(role);
        for (String s : siteUsers) {
            if (!userTandemList.contains(s))
                retVal.add(s);
        }

        return retVal;
    }

    /**
     * Возвращает список Eid пользователей сайта для роли
     *
     * @param role
     */
    public List<String> getSiteUsersByRole(String role) {
        List<String> res = getSiteUserMap().get(role);
        if (res == null) res = new ArrayList<>();
        return res;
    }

    /**
     * Возвращает список Eid всех пользователей сайта
     */
    public List<String> getSiteUsers() {
        List<String> res = new ArrayList<>();
        for (String role : getSiteUserMap().keySet()) {
            res.addAll(getSiteUserMap().get(role));
        }
        return res;
    }


    public void setCactionType(String cactionType) {
        this.cactionType = cactionType;
    }

    public String getCactionType() {
        return cactionType;
    }

    private Map<String, List<String>> getSiteUserMap() {
        return siteUserMap;
    }

    public void setSiteUserMap(Map<String, List<String>> siteUserMap) {
        this.siteUserMap = siteUserMap;
    }
}
