package ru.tandemservice.unisakai.dao.wrappers;

import org.tandemframework.core.bean.IFastBean;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.logic.Tools;

/**
 * Обертка - студент на сайте сакая
 * (в публикаторе сайта)
 *
 * @author Администратор
 */
public class WrapperStudentToSiteSakai extends EntityBase implements IEntity {
    private Student student;
    private EducationYear year;
    private YearDistributionPart partYear;
    private Long _id;

    private String eid;
    private String title;


    private SakaiEntityState state;

    public void setStudent(Student student)
    {
        this.student = student;

        if (this.student != null) {
            setId(student.getId());
            setEid(Tools.getStudentEid(student));
            setTitle(student.getPerson().getFio());
        }
        else {
            // а нефиг
        }


    }

    public Student getStudent() {
        return student;
    }

    public void setYear(EducationYear year) {
        this.year = year;
    }

    public EducationYear getYear() {
        return year;
    }

    public void setPartYear(YearDistributionPart partYear) {
        this.partYear = partYear;
    }

    public YearDistributionPart getPartYear() {
        return partYear;
    }

    @Override
    public Long getId() {

        return _id;
    }

    @Override
    public void setId(Long id)
    {
        _id = id;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public IFastBean getFastBean()
    {
        return null;
    }

    public void setState(SakaiEntityState state) {
        this.state = state;
    }

    public SakaiEntityState getState() {
        return state;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getEid() {
        return eid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }


    public String getStudentNumber()
    {
        if (getStudent() != null)
            return getStudent().getPersonalNumber();
        else
            return "";

    }

    public String getYearString()
    {
        if (getYear() != null)
            return getYear().getTitle();
        else
            return "";
    }


    public String getPartYearString()
    {
        if (getPartYear() != null)
            return getPartYear().getTitle();
        else
            return "";
    }


}
