package ru.tandemservice.unisakai.component.catalog.uniSakaiSettings.UniSakaiSettingsPub;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSettingsType;


import java.util.List;

public class Model {

    private IDataSettings _settings;


    private ISelectModel _uniSakaiSettingsTypeModel;
    private List<UniSakaiSettingsType> _uniSakaiSettingsTypes;
    private DynamicListDataSource<UniSakaiSettings> _dataSource;


    public void setDataSource(DynamicListDataSource<UniSakaiSettings> _dataSource)
    {
        this._dataSource = _dataSource;
    }

    public DynamicListDataSource<UniSakaiSettings> getDataSource()
    {
        return _dataSource;
    }


    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public void setUniSakaiSettingsTypeModel(ISelectModel _uniSakaiSettingsTypeModel)
    {
        this._uniSakaiSettingsTypeModel = _uniSakaiSettingsTypeModel;
    }

    public ISelectModel getUniSakaiSettingsTypeModel() {
        return _uniSakaiSettingsTypeModel;
    }

    public void setUniSakaiSettingsTypes(List<UniSakaiSettingsType> uniSakaiSettingsTypes)
    {
        this._uniSakaiSettingsTypes = uniSakaiSettingsTypes;
    }

    public List<UniSakaiSettingsType> getUniSakaiSettingsTypes() {
        return _uniSakaiSettingsTypes;
    }
}
