package ru.tandemservice.unisakai.component.sakai.imp.UgstMassImp;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.process.*;
import org.tandemframework.core.settings.DataSettingsFacade;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisakai.dao.SakaiDaoFacade;
import ru.tandemservice.unisakai.logic.ActionSakai;

import java.util.List;

public class Controller<I extends IDAO, M extends Model> extends AbstractBusinessController<IDAO, Model> {
    public static String KEY = "sakai.imp.UgstMassImp";

    public String getSettingsKey()
    {
        return KEY;
    }


    @Override
    public void onRefreshComponent(IBusinessComponent component) {


        Model model = getModel(component);
        if (!model.getProcessEnd()) {
            model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey()));
            getDao().prepare(model);
        }
    }

    public void onClickExit(final IBusinessComponent context)
    {
        final Model model = context.getModel();
        DataSettingsFacade.saveSettings(model.getSettings());

        deactivate(context);
    }


    public void onEnd(final IBusinessComponent context)
    {
        onClickExit(context);
    }

    public void onClickApply(final IBusinessComponent context) throws Exception
    {

        final Model model = context.getModel();
        DataSettingsFacade.saveSettings(model.getSettings());

        final List<EppRegistryElementPartFControlAction> lst = SakaiDaoFacade.getSakaiDao().getEppRegistryElementPartFControlActionList(model.getRegpartIds());

        ActionSakai as = ActionSakai.getActionSakai();
        model.setSakaiSites(as.getSakaiSites());
        if (!as.getSakaiScripts().getSakaiProperty().USE_LDAP_AUTH)
            model.setSakaiUsers(as.getSakaiUsers());

        synchronized (Model.MUTEX) {
            final IBackgroundProcess process = new BackgroundProcessBase() {


                @Override
                public ProcessResult run(final ProcessState state) {

                    try {

                        try {
                            Thread.sleep(1000);
                        }
                        catch (final Throwable t) {
                        }

                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        try {
                            state.setMaxValue(100);
                            state.setCurrentValue(0);
                            state.setDisplayMode(ProcessDisplayMode.percent);

                            int i = 0;

                            // просматриваем по дисциплинам
                            for (EppRegistryElementPartFControlAction re : lst) {
                                state.setCurrentValue(100 * i / lst.size());
                                Thread.yield();
                                _doParts(model, re);
                                i++;
                            }
                            return null; // закрываем диалог

                        }
                        catch (Exception ex) {
                            throw ex;
                        }
                        finally {
                            eventLock.release();
                            try {
                                Thread.sleep(100);
                            }
                            catch (final Throwable t) {
                            }
                            model.setProcessEnd(true);
                        }

                    }
                    catch (final Throwable t) {
                        model.setProcessEnd(true);
                        return new ProcessResult("Произошла ошибка " + t.getMessage(), true); // закрываем диалог
                    }
                }
            };

            BackgroundProcessHolder bHolder = new BackgroundProcessHolder();
            model.setProcessEnd(false);
            bHolder.start("Обработка", process);
        }

    }

    private void _doParts
            (
                    Model model
                    , EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction
            ) throws Exception
    {
        _doPart(model, eppRegistryElementPartFControlAction);
    }

    /**
     * Синхронизация сайта ТУТ
     */
    protected void _doPart(Model model, EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction) throws Exception
    {
        // кандидат на переопределение
        // собираем значения фильтров и в путь
        EducationYear year = model.getYear();
        YearDistributionPart partYear = model.getPart();
        boolean syncTi = model.getProcessTi();
        boolean syncSt = model.getProcessSt();

        // списки:
        List<DevelopForm> lstDevForm = model.getDevelopFormSelected();
        List<DevelopTech> lstDevTech = model.getDevelopTechSelected();

        ActionSakai as = ActionSakai.getActionSakai();


        boolean strongListTi = model.getStrongListTi();
        boolean strongListSt = model.getStrongListSt();


        as.SaveOrUpdateSiteD(eppRegistryElementPartFControlAction, year, partYear, syncTi, syncSt, strongListTi,
                        strongListSt, lstDevForm, lstDevTech, model.getSakaiSites(), model.getSakaiUsers());

    }
}
