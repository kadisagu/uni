package ru.tandemservice.unisakai.component.sakai.imp.AgsMassSiteDel;


import ru.tandemservice.unisakai.logic.ActionSakai;

public class Controller extends ru.tandemservice.unisakai.component.sakai.imp.AgsMassImp.Controller<IDAO, Model>
{

    public static String KEY = "sakai.imp.AgsMassSiteDel";

    @Override
    public String getSettingsKey()
    {
        return KEY;
    }

    @Override
    protected boolean useSiteIdList()
    {
        return true;
    }

    @Override
    protected void _do(ru.tandemservice.unisakai.component.sakai.imp.AgsMassImp.Model model, Object idObj) throws Exception
    {
        // Удалить группы
        String siteId = (String) idObj;
        ActionSakai as = ActionSakai.getActionSakai();
        as.DeleteSite(siteId, model.getSakaiSites());
    }
}
