package ru.tandemservice.unisakai.dao;

import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisakai.dao.wrappers.WrapperStudentToSiteSakai;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSiteEtalon;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.UserSakai;

import java.util.List;
import java.util.Map;

public interface ISakaiDao {

     List<EppRegistryElementPartFControlAction> getEppRegistryElementPartFControlActionList(List<Long> ids);

    EppRegistryElementPartFControlAction getEppRegistryElementPartFControlAction(Long id);


    /**
     * Возвращает список преподавателей,
     * подходящий для сайта
     */
    List<Employee> getEmployee4Site(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction);


    /**
     * Возвращает список студентов,
     * подходящий для сайта
     */
    List<Student> getStudents4Site(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, EducationYear year,
            YearDistributionPart partYear, List<DevelopForm> lstDevForm, List<DevelopTech> lstDevTech);

    List<Student> getStudents4Site(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, EducationYear year,
                                   YearDistributionPart partYear, List<DevelopForm> lstDevForm, List<DevelopTech> lstDevTech, boolean fakeLoad);

    List<WrapperStudentToSiteSakai> getStudents4Pub(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, Integer part,
                    EducationYear year, YearDistributionPart partYear, List<DevelopForm> developFormSelected, List<DevelopTech> developTechSelected,
                    List<SakaiEntityState> sakaiStates, SiteSakai site, Map<String, UserSakai> users, boolean useLDAP);


    /*
     * Список студентов для сайта АГС
     */
    List<Student> getStudents4Site(Group group);

    Group getGroup(Long id);

    /**
     * Возвращает полную карту итоговых оценок
     */
    Map<String, String> getMarkMapForSite(EppRegistryElementPartFControlAction eppRegistryElementPartFControlAction, boolean usePersonalNumber);

    UniSakaiSiteEtalon getEtalon(EppRegistryElementPart regelem);

    UniSakaiSiteEtalon saveEtalon(EppRegistryElementPart regelem, String etalonName);

    UniSakaiSiteEtalon getEtalon(EppRegistryElement regElem, Integer part);

    String getCActionTitle(String cactionCode);

}
