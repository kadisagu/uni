package ru.tandemservice.unisakai.entity.catalog;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unisakai.entity.catalog.gen.*;

import java.util.Date;

/** @see ru.tandemservice.unisakai.entity.catalog.gen.UniSakaiMarkImportGen */
public class UniSakaiMarkImport extends UniSakaiMarkImportGen implements IStorableReport
{
        @Override
        public Date getFormingDate()
        {
            return getCreateDate();
        }

}