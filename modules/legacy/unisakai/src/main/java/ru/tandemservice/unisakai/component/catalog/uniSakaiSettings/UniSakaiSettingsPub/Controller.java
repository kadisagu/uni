package ru.tandemservice.unisakai.component.catalog.uniSakaiSettings.UniSakaiSettingsPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {

        Model model = getModel(component);

        // на потом - хранить настройки фильтров
        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey(model)));


        createDataList(component);


        getDao().prepare(model);

    }

    public void onClickEdit(IBusinessComponent context)
    {
        Long _idEdit = (Long) context.getListenerParameter();
        // печать личной карточки студента - перенаправляем на свою реализацию
        activateInRoot(context, new ComponentActivator("ru.tandemservice.unisakai.component.catalog.uniSakaiSettings.UniSakaiSettingsAddEdit", ParametersMap.createWith("uniSakaiSettingsId", _idEdit)));
    }

    private void createDataList(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<UniSakaiSettings> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareCustomDataSource(getModel(component1), getModel(component1).getDataSource());
        }, 10);


        dataSource.addColumn(new SimpleColumn("Тип свойства", UniSakaiSettings.uniSakaiSettingsType().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Имя свойства", UniSakaiSettings.P_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Ключ", UniSakaiSettings.P_STRING_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Значение", UniSakaiSettings.P_STRING_VALUE).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setPermissionKey("sakaiSettingsEdit"));

        model.setDataSource(dataSource);
    }

    private String getSettingsKey(Model model)
    {
        return "unisakai.filter";
    }

    public void onClickApply(final IBusinessComponent context)
    {
        // Model model = getModel(context);
        //UniDaoFacade.getSettingsManager().saveSettings(model.getSettings());
        refreshData(context);
    }

    /**
     * обновить данные после смены фильтра
     */
    private void refreshData(final IBusinessComponent context)
    {
        Model model = getModel(context);
        model.getDataSource().refresh();
    }

}
