package ru.tandemservice.unisakai.logic;


import ru.tandemservice.unisakai.util.Sakai.SakaiParams;
import ru.tandemservice.unisakai.util.Sakai.SakaiProperty;
import ru.tandemservice.unisakai.ws.sakai.SakaiLoginService;
import ru.tandemservice.unisakai.ws.sakai.SakaiScriptService;

public class SakaiScripts {
    private SakaiLoginService _loginService;
    private SakaiScriptService _scriptService;

    private String sessionId = "";
    private SakaiProperty sakaiProperty = new SakaiProperty();

    public SakaiProperty getSakaiProperty()
    {
        return sakaiProperty;
    }

    public SakaiScripts() throws Exception
    {
        sakaiProperty = SakaiParams.FillParams();

    }

    public void Prepare() throws Exception
    {
        try {
            _scriptService = SakaiGetter.getScritpService();
        }
        catch (Exception e) {
            _scriptService = null;
            throw new Exception("Ошибка получения _scriptService" + e.getMessage());
        }

        try {
            _loginService = SakaiGetter.getLoginService();
        }
        catch (Exception e) {
            _loginService = null;
            throw new Exception("Ошибка получения _loginService" + e.getMessage());
        }
    }

    public String LoginToSakay() throws Exception
    {
        sessionId = "";
        try {
            sessionId = _loginService.getSakaiLogin().login(sakaiProperty.LOGIN, sakaiProperty.PASSWORD);
        }
        catch (Exception ex) {
            sessionId = "";
            throw ex;
        }
        return sessionId;
    }

    public SakaiLoginService getLoginService() {
        return _loginService;
    }

    public SakaiScriptService getScriptService() {
        return _scriptService;
    }

    public void UpdateProperty() throws Exception
    {
        sakaiProperty = SakaiParams.FillParams();
    }
}
