package ru.tandemservice.unisakai.util.Sakai;


import ru.tandemservice.unisakai.util.sprav.KeyValues;

public class SakaiParams {

    public static SakaiProperty FillParams() throws Exception
    {
        SakaiProperty prop = new SakaiProperty();

        prop.LOGIN = KeyValues.getStringValue("unisakai.login", true);
        prop.PASSWORD = KeyValues.getStringValue("unisakai.password", true);

        // нужно читать из конфига
        prop.STUDENT_ROLE = KeyValues.getStringValue("unisakai.studentRole", true); //"access";
        prop.TUTOR_ROLE = KeyValues.getStringValue("unisakai.tutorRole", true);


        // это нужно читать из конфига
        prop.TEMPLATE_SITE = KeyValues.getStringValue("unisakai.templateSite", true);
        prop.SITE_JOINERROLE = KeyValues.getStringValue("unisakai.siteJoinerrole", true);
        prop.SITE_TYPE = KeyValues.getStringValue("unisakai.siteType", true);

        prop.TEMPLATE_SITE_GROUP = KeyValues.getStringValue("unisakai.templateGroupSite", true);

        prop.USE_LDAP_AUTH = Boolean.parseBoolean(KeyValues.getStringValue(KeyValues.UNI_SAKAI_USELDAP, true));

        return prop;
    }


}
