/**
 * SakaiLoginServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unisakai.ws.sakai;

public class SakaiLoginServiceLocator extends org.apache.axis.client.Service implements ru.tandemservice.unisakai.ws.sakai.SakaiLoginService {

    public SakaiLoginServiceLocator() {
    }


    public SakaiLoginServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SakaiLoginServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SakaiLogin
    private String SakaiLogin_address = "http://sakai2.pomorsu.ru/sakai-axis/SakaiLogin.jws";

    public String getSakaiLoginAddress() {
        return SakaiLogin_address;
    }

    // The WSDD service name defaults to the port name.
    private String SakaiLoginWSDDServiceName = "SakaiLogin";

    public String getSakaiLoginWSDDServiceName() {
        return SakaiLoginWSDDServiceName;
    }

    public void setSakaiLoginWSDDServiceName(String name) {
        SakaiLoginWSDDServiceName = name;
    }

    public ru.tandemservice.unisakai.ws.sakai.SakaiLogin_PortType getSakaiLogin() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SakaiLogin_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSakaiLogin(endpoint);
    }

    public ru.tandemservice.unisakai.ws.sakai.SakaiLogin_PortType getSakaiLogin(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ru.tandemservice.unisakai.ws.sakai.SakaiLoginSoapBindingStub _stub = new ru.tandemservice.unisakai.ws.sakai.SakaiLoginSoapBindingStub(portAddress, this);
            _stub.setPortName(getSakaiLoginWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSakaiLoginEndpointAddress(String address) {
        SakaiLogin_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ru.tandemservice.unisakai.ws.sakai.SakaiLogin_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ru.tandemservice.unisakai.ws.sakai.SakaiLoginSoapBindingStub _stub = new ru.tandemservice.unisakai.ws.sakai.SakaiLoginSoapBindingStub(new java.net.URL(SakaiLogin_address), this);
                _stub.setPortName(getSakaiLoginWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("SakaiLogin".equals(inputPortName)) {
            return getSakaiLogin();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://sakai2.pomorsu.ru/sakai-axis/SakaiLogin.jws", "SakaiLoginService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://sakai2.pomorsu.ru/sakai-axis/SakaiLogin.jws", "SakaiLogin"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException
    {

        if ("SakaiLogin".equals(portName))
        {
            setSakaiLoginEndpointAddress(address);
        }
        else
        { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
