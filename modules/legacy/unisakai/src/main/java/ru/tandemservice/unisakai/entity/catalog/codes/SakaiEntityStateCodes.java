package ru.tandemservice.unisakai.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние сущности в Sakai"
 * Имя сущности : sakaiEntityState
 * Файл data.xml : sprav.data.xml
 */
public interface SakaiEntityStateCodes
{
    /** Константа кода (code) элемента : Нет в Sakai (title) */
    String NOT_IN_SAKAI = "01";
    /** Константа кода (code) элемента : Есть в Sakai (title) */
    String PRESENT_IN_SAKAI = "02";
    /** Константа кода (code) элемента : Не совпадает (title) */
    String PRESENT_IN_SAKAI_NOT_EQUALS = "03";
    /** Константа кода (code) элемента : Нет данных (title) */
    String NO_INFO = "04";
    /** Константа кода (code) элемента : Нет в UNI (title) */
    String NOT_IN_UNI = "05";
    /** Константа кода (code) элемента : Не подписан на сайт (title) */
    String NOT_IN_SITE = "06";

    Set<String> CODES = ImmutableSet.of(NOT_IN_SAKAI, PRESENT_IN_SAKAI, PRESENT_IN_SAKAI_NOT_EQUALS, NO_INFO, NOT_IN_UNI, NOT_IN_SITE);
}
