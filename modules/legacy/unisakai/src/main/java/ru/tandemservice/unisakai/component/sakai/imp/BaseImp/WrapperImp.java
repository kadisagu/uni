package ru.tandemservice.unisakai.component.sakai.imp.BaseImp;

import org.tandemframework.core.bean.IFastBean;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;

public class WrapperImp extends EntityBase implements IEntity {
    private long _id;

    private String entityType = "";
    private String name = "";
    private String description = "";

    private SakaiEntityState sakaiState = null;

    private Object tag = null;

    @Override
    public Long getId() {

        return _id;
    }

    @Override
    public void setId(Long id)
    {
        _id = id;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public IFastBean getFastBean()
    {
        return null;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setSakaiState(SakaiEntityState sakaiState) {
        this.sakaiState = sakaiState;
    }

    public SakaiEntityState getSakaiState() {
        return sakaiState;
    }

    public String getSakaiInfo()
    {
        return getSakaiState() == null? "" :getSakaiState().getTitle();
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public Object getTag() {
        return tag;
    }

}
