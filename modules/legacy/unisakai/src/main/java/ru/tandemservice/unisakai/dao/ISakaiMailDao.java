package ru.tandemservice.unisakai.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unisakai.logic.UserSakai;


public interface ISakaiMailDao {
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void MailSendInfoNewUser(UserSakai user) throws Exception;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void MailSendInfoUpdateUser(UserSakai user) throws Exception;

}
