package ru.tandemservice.util;

import org.springframework.util.StringUtils;

public class Mail {

    public static void notBlank(String string, String name)
    {
        if (!StringUtils.hasLength(string))
            throw new RuntimeException("Field '" + name + "' cannot be empty");
    }

}
