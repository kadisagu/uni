package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

import org.tandemframework.core.bean.IFastBean;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unisakai.dao.SakaiDaoFacade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * обертка вокруг представления для забора
 *
 * @author vch
 */
public class WrapperGM extends EntityBase implements IEntity {

    // private EducationYear year = null;
    private String eduYear = null;
    private Long idRegistryElement;
    private int part = 0;
    private String cactionCode;

    private List<Long> reportList = new ArrayList<Long>();


    private String groupName;
    private long _id;
    private Long miId = null;
    private Date miCreateDate = null;
    private String miPrim = null;

    public WrapperGM
            (
                    Long id
                    , Long idRegistryElement
                    , int part
                    , String cactionCode
                    , String eduYear
                    , String groupName
            )
    {
        _id = id;
        this.idRegistryElement = idRegistryElement;
        this.part = part;
        this.cactionCode = cactionCode;
        this.eduYear = eduYear;
        this.groupName = groupName;
    }

    @Override
    public Long getId() {

        return _id;
    }

    @Override
    public void setId(Long id)
    {
        _id = id;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public IFastBean getFastBean()
    {
        return null;
    }

	
	
	
	/*
    public void setPart(int part) {
		this.part = part;
	}
	public int getPart() {
		return part;
	}
	*/

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }
	
	/*
	public void setDisciplineId(Long disciplineId) {
		this.disciplineId = disciplineId;
	}
	public Long getDisciplineId() {
		return disciplineId;
	}
	*/


    public String getEduYear()
    {
        return eduYear;

    }

    public void setEduYear(String val)
    {
        eduYear = val;
    }

    public String getTermPart()
    {
        return Integer.toString(getPart());
    }

    public void setMarkImport
            (
                    Long miId, Date miCreateDate, String miPrim
            )
    {
        this.miId = miId;
        this.miCreateDate = miCreateDate;
        this.miPrim = miPrim;
    }

    public void AddMarkImport(Long id)
    {
        if (id != null) {
            if (!reportList.contains(id))
                reportList.add(id);
            else
                System.out.print("Has");
        }


    }

    public Long getMarkImportId()
    {
        return this.miId;
    }


    public String getImportRezult()
    {
        if (miPrim == null)
            return "не производился";
        else {
            return miPrim;
        }


    }

    @SuppressWarnings("static-access")
    public String getCreateDateString()
    {
        if (miCreateDate == null)
            return "";
        else {

            DateFormatter formatter = DateFormatter.DEFAULT_DATE_FORMATTER;
            return formatter.DATE_FORMATTER_WITH_TIME.format(miCreateDate);
        }
    }

    public List<Long> getListMarkImport()
    {
        return reportList;
    }


    public void setPart(int part) {
        this.part = part;
    }

    public int getPart() {
        return part;
    }

    public Long getIdRegistryElement() {
        return idRegistryElement;
    }


    public String getCactionCode() {
        return cactionCode;
    }


    public String getCactionTitle()
    {
        return SakaiDaoFacade.getSakaiDao().getCActionTitle(cactionCode);

    }
}
