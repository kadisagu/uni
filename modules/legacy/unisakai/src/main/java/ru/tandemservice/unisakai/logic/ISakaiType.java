package ru.tandemservice.unisakai.logic;

public interface ISakaiType {
    static String TYPE_GROUP = "G";
    static String TYPE_DISCIPLINE = "D";
}
