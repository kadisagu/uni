package ru.tandemservice.unisakai.component.sakai.imp.UgstMassDel;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

@Input({
                @Bind(key = "ids", binding = "regpartIds", required = true),
                @Bind(key = "idYear", binding = "idYear", required = true),
                @Bind(key = "idPart", binding = "idPart", required = true)
        })
public class Model extends ru.tandemservice.unisakai.component.sakai.imp.UgstMassImp.Model
{

}
