package ru.tandemservice.unisakai.component.sakai.imp.UgstPub;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    void refreshDataSource(Model model);


    void sakaiInfoToModel(Model model);

}
