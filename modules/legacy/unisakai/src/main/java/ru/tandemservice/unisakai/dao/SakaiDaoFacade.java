package ru.tandemservice.unisakai.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * Точка доступа к разным дао
 *
 * @author Администратор
 */
public class SakaiDaoFacade {
    private static ISakaiDao _sakaiDao;
    private static ISakaiMailDao _sakaiMailDao;


    public static ISakaiDao getSakaiDao()
    {
        if (_sakaiDao == null)
            _sakaiDao = (ISakaiDao) ApplicationRuntime.getBean("sakaiDao");
        return _sakaiDao;
    }


    public static ISakaiMailDao getSakaiMailDao()
    {
        if (_sakaiMailDao == null)
            _sakaiMailDao = (ISakaiMailDao) ApplicationRuntime.getBean("sakaiMailDao");
        return _sakaiMailDao;
    }
}
