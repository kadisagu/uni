package ru.tandemservice.unisakai.component.sakai.imp.BaseImp;

import org.hibernate.Session;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.entity.catalog.codes.SakaiEntityStateCodes;
import ru.tandemservice.unisakai.logic.Tools;
import ru.tandemservice.unisakai.logic.UserSakai;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

// import org.tandemframework.hibsupport.dql.DQLExpressions;


public abstract class DAO extends UniDao<Model> implements IDAO {

    public static List<String> getListStatus()
    {
        List<String> retVal = new ArrayList<>();
        retVal.add("1"); //активный
        retVal.add("4"); //отложить защиту
        retVal.add("5"); //отп акад без посещения
        retVal.add("6"); //отп без с посещения
        retVal.add("7"); //отп с посещения
        retVal.add("20"); //берем роды
        retVal.add("21"); //уход за реб

        return retVal;
    }

    @Override
    public void prepare(Model model)
    {

        if (model.getDevelopFormListModel() == null)
            model.setDevelopFormListModel(new LazySimpleSelectModel<>(DevelopForm.class).setSortProperty(DevelopForm.P_CODE));

        if (model.getDevelopTechListModel() == null)
            model.setDevelopTechListModel(new LazySimpleSelectModel<>(DevelopTech.class).setSortProperty(DevelopTech.P_CODE));

        if (model.getSakaiStateListModel() == null)
            model.setSakaiStateListModel(new LazySimpleSelectModel<>(SakaiEntityState.class).setSortProperty(SakaiEntityState.P_CODE));

        if (model.getFormativeOrgUnitListModel() == null)
            model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));

        if (model.getProducingOrgUnitListModel() == null)
            model.setProducingOrgUnitListModel(new LazySimpleSelectModel<>(UniDaoFacade.getOrgstructDao().getOrgUnitList(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)));


        if (model.getStateMap() == null) {
            Session session = getSession();

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SakaiEntityState.class, "es")
                    .column("es");


            List<SakaiEntityState> lst = dql.createStatement(session).list();

            Map<String, SakaiEntityState> map = new HashMap<>();
            for (SakaiEntityState s : lst) {
                map.put(s.getCode(), s);
            }

            model.setStateMap(map);
        }

    }


    @Override
    public void refreshDataSource(Model model)
    {
        List<WrapperImp> itemList = getListData(model);

        DynamicListDataSource<WrapperImp> dataSource = model.getDataSource();

        final EntityOrder entOrder = dataSource.getEntityOrder();
        // dataSource.setOrder(entOrder.getColumnName(), entOrder.getDirection());
        if (entOrder != null)
            // применим сортировку
            Collections.sort(itemList, (o1, o2) -> {
                // получим свойства
                Object obj1 = o1.getProperty(entOrder.getColumnName());
                Object obj2 = o2.getProperty(entOrder.getColumnName());

                String s1 = "";
                String s2 = "";

                if (obj1 != null)
                    s1 = (String) obj1;
                if (obj2 != null)
                    s2 = (String) obj2;

                if (entOrder.getDirection() == OrderDirection.asc)
                    return s1.compareToIgnoreCase(s2);
                else
                    return s2.compareToIgnoreCase(s1);
            }

            );
        UniBaseUtils.createPage(dataSource, itemList);
    }

    public abstract List<WrapperImp> getListData(Model model);


    @SuppressWarnings("rawtypes")
    protected Collection getDevelopFormList(IDataSettings settings)
    {
        Object developFormList = settings.get("developFormList");
        Collection collectionDevelopFormList = null;

        if (developFormList instanceof Collection) {
            if (!((Collection) developFormList).isEmpty())
                collectionDevelopFormList = (Collection) developFormList;
        }

        return collectionDevelopFormList;

    }

    @SuppressWarnings("rawtypes")
    protected Collection getDevelopTechList(IDataSettings settings)
    {
        Object developTechList = settings.get("developTechList");
        Collection collectionDevelopTechList = null;

        if (developTechList instanceof Collection) {
            if (!((Collection) developTechList).isEmpty())
                collectionDevelopTechList = (Collection) developTechList;
        }

        return collectionDevelopTechList;
    }


    @SuppressWarnings("rawtypes")
    protected Collection getSakaiStateList(IDataSettings settings)
    {
        Object sakaiStateList = settings.get("sakaiStateList");
        Collection collectionSakaiStateList = null;
        if (sakaiStateList instanceof Collection) {
            if (!((Collection) sakaiStateList).isEmpty())
                collectionSakaiStateList = (Collection) sakaiStateList;
        }

        return collectionSakaiStateList;
    }

    @SuppressWarnings("rawtypes")
    public List<WrapperImp> getListDataTeacher(Model model)
    {
        List<WrapperImp> retVal = new ArrayList<>();
        IDataSettings settings = model.getSettings();
        Object _orgUnitList = settings.get("orgUnitList");

        List<OrgUnit> orgUnitList = new ArrayList<>();

        if (_orgUnitList instanceof Collection) {
            if (!((Collection) _orgUnitList).isEmpty()) {
                Collection collectionOrgUnitList = (Collection) _orgUnitList;

                for (Object ou : collectionOrgUnitList) {
                    OrgUnit u = (OrgUnit) ou;
                    if (u != null)
                        orgUnitList.add(u);
                }
            }
        }
        // выбираем все должности
        List<String> lstTPoz = new ArrayList<>();
        lstTPoz.add("profLecStaff");
        lstTPoz.add("workStaff1");

        // ваыбираем преподов
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EmployeePost.class, "employeepost")
                .column(property(EmployeePost.employee().id().fromAlias("employeepost")))
                .where(eq(property(EmployeePost.employee().archival().fromAlias("employeepost")), value(Boolean.FALSE)))
                .where(eq(property(EmployeePost.postStatus().active().fromAlias("employeepost")), value(Boolean.TRUE)))

                .where(in(property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().code().fromAlias("employeepost")), lstTPoz));

        if (orgUnitList.size() > 0)
            dql.where(in(property(EmployeePost.orgUnit().fromAlias("employeepost")), orgUnitList));


        // List<Long> lstId = dql.createStatement(getSession()).list();

        // выбираем всех человеков
        DQLSelectBuilder dqlEmp = new DQLSelectBuilder()
                .fromEntity(Employee.class, "employee")
                .fetchPath(DQLJoinType.inner, Employee.principal().fromAlias("employee"), "principal")
                .column("employee")
                .where(in(property(Employee.id().fromAlias("employee")), dql.buildQuery()));

        // фильтр по имени
        if (model.getName() != null)
            dql.where(likeUpper(property(Employee.person().identityCard().lastName().fromAlias("employee"))
                            , value(CoreStringUtils.escapeLike(model.getName()))));

        List<Employee> emplLst = dqlEmp.createStatement(getSession()).list();


        for (Employee empl : emplLst) {
            UserSakai empSakai = null;
            if (!model.isUseLDAP())
                empSakai = Tools.getUser(empl);

            String advTag = "";
            if (empSakai != null && !empSakai.isHasEmail())
                advTag = "нет e-mail ";

            WrapperImp wr = new WrapperImp();
            wr.setId(empl.getId());
            wr.setEntityType("Преподаватель");
            wr.setDescription(advTag + empl.getFullTitle());
            wr.setTag(empl);
            wr.setName(empl.getPerson().getFio());


            if (model.getSakaiusers() != null && empSakai != null)
            {
                SakaiEntityState state;
                // подберем сакай пользователя
                if (model.getSakaiusers().containsKey(empSakai.getId())) {
                    UserSakai sakaiUser = model.getSakaiusers().get(empSakai.getId());
                    if (sakaiUser.hashCode() == empSakai.hashCode())
                        state = model.getStateMap().get(SakaiEntityStateCodes.PRESENT_IN_SAKAI);
                    else
                        state = model.getStateMap().get(SakaiEntityStateCodes.PRESENT_IN_SAKAI_NOT_EQUALS);
                }
                else
                    state = model.getStateMap().get(SakaiEntityStateCodes.NOT_IN_SAKAI);
                wr.setSakaiState(state);
            }
            else
                wr.setSakaiState(null);
            retVal.add(wr);
        }

        // список преподов, которых более нет в тандеме
        if (model.getSakaiusers() != null) {
            List<Long> lstEmpty = model.getSakaiIdUser(UserSakai.PREF_EMPLOYEE);
            if (lstEmpty.size() > 0) {
                // условие одно - не архивный
                DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                        .fromEntity(Employee.class, "emp")
                        .column(property(Employee.id().fromAlias("emp")))
                        .where(in(property(Employee.id().fromAlias("emp")), lstEmpty))
                        .where(eq(property(Employee.archival().fromAlias("emp")), value(Boolean.FALSE)));

                List<Long> lstSt = dqlIn.createStatement(getSession()).list();
                List<UserSakai> empyUser = model.getSakaiUserNotTandem(UserSakai.PREF_EMPLOYEE, lstSt);

                SakaiEntityState state = model.getStateMap().get(SakaiEntityStateCodes.NOT_IN_UNI);
                int idVrem = 0;

                for (UserSakai us : empyUser) {
                    idVrem++;

                    WrapperImp wr = new WrapperImp();
                    wr.setId(makeFaleId(idVrem));

                    wr.setEntityType("Sakai");
                    wr.setDescription(us.getId() + " " + us.getEid());
                    wr.setTag(us);
                    wr.setName(us.getLastName() + " " + us.getFirstName());
                    wr.setSakaiState(state);
                    retVal.add(wr);
                }
            }
        }
        return applySakaiStateAndNameFilter(retVal, settings);
    }


    @SuppressWarnings("rawtypes")
    public List<WrapperImp> getListDataStudent(Model model)
    {
        List<WrapperImp> retVal = new ArrayList<>();

        IDataSettings settings = model.getSettings();
        Collection collectionDevelopFormList = getDevelopFormList(settings);
        Collection collectionDevelopTechList = getDevelopTechList(settings);
        Collection collectionSakaiStateList = getSakaiStateList(settings);

        // выбираем студентов
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Student.class, "student")
                .column(Student.id().fromAlias("student").s(), "studentId") // Long - 0
                .column(Student.personalNumber().fromAlias("student").s(), "personalNumber") //int - 1
                .column(Student.person().identityCard().firstName().fromAlias("student").s(), "firstName") // - 2
                .column(Student.person().identityCard().lastName().fromAlias("student").s(), "lastName") //- 3
                .column(Student.person().identityCard().middleName().fromAlias("student").s(), "middleName")//-4
                .column(Student.person().contactData().email().fromAlias("student").s(), "email")//-5
                .column(Student.principal().passwordHash().fromAlias("student").s(), "pwd") // строка -6

                .where(eq(property(Student.status().active().fromAlias("student")), value(Boolean.TRUE)));

        // фильтр по имени
        if (model.getName() != null)
            dql.where(likeUpper(property(Student.person().identityCard().lastName().fromAlias("student")),
                                value(CoreStringUtils.escapeLike(model.getName()))));


        if (collectionDevelopFormList != null)
            dql.where(in(property(Student.educationOrgUnit().developForm().fromAlias("student")), collectionDevelopFormList));

        if (collectionDevelopTechList != null)
            dql.where(in(property(Student.educationOrgUnit().developTech().fromAlias("student")), collectionDevelopTechList));

        List<Object[]> lst = dql.createStatement(getSession()).list();

        for (Object[] studentObjs : lst) {
            Long studentId = (Long) studentObjs[0];
            String studentPersonalNumber = (String) studentObjs[1];
            String firstName = (String) studentObjs[2];
            String lastName = (String) studentObjs[3];
            String middleName = (String) studentObjs[4];
            String email = (String) studentObjs[5];
            String pwd = (String) studentObjs[6];

            String fio = lastName;
            if (firstName != null) fio += " " + firstName;
            if (middleName != null) fio += " " + middleName;

            UserSakai stSakai = Tools.getUserStudent(studentId, studentPersonalNumber, firstName, lastName, email, pwd);
            String advTag = "";
            if (!stSakai.isHasEmail())
                advTag = "нет e-mail ";

            WrapperImp wr = new WrapperImp();
            wr.setId(studentId);
            wr.setEntityType("Студент");
            wr.setDescription(advTag + fio);
            wr.setTag(stSakai);
            wr.setName(fio);

            if (model.getSakaiusers() != null) {
                SakaiEntityState state;
                // подберем сакай пользователя
                if (model.getSakaiusers().containsKey(stSakai.getId())) {
                    UserSakai sakaiUser = model.getSakaiusers().get(stSakai.getId());
                    if (sakaiUser.hashCode() == stSakai.hashCode())
                        state = model.getStateMap().get(SakaiEntityStateCodes.PRESENT_IN_SAKAI);
                    else
                        state = model.getStateMap().get(SakaiEntityStateCodes.PRESENT_IN_SAKAI_NOT_EQUALS);
                }
                else
                    state = model.getStateMap().get(SakaiEntityStateCodes.NOT_IN_SAKAI);
                wr.setSakaiState(state);
            }
            else
                wr.setSakaiState(null);
            retVal.add(wr);
        }

        // список студентов, которых более нет в тандеме
        if (model.getSakaiusers() != null) {
            List<Long> lstEmpty = model.getSakaiIdUser(UserSakai.PREF_STUDENT);
            if (lstEmpty.size() > 0) {
                DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                        .fromEntity(Student.class, "student")
                        .column(property(Student.id().fromAlias("student")))
                        .where(in(property(Student.id().fromAlias("student")), lstEmpty))
                        .where(in(property(Student.status().code().fromAlias("student")), getListStatus()));

                List<Long> lstSt = dqlIn.createStatement(getSession()).list();
                List<UserSakai> empyUser = model.getSakaiUserNotTandem(UserSakai.PREF_STUDENT, lstSt);

                SakaiEntityState state = model.getStateMap().get(SakaiEntityStateCodes.NOT_IN_UNI);
                int idVrem = 0;

                for (UserSakai us : empyUser) {
                    idVrem++;
                    WrapperImp wr = new WrapperImp();
                    wr.setId(makeFaleId(idVrem));
                    wr.setEntityType("Sakai");
                    wr.setDescription(us.getId() + " " + us.getEid());
                    wr.setTag(us);
                    wr.setName(us.getLastName() + " " + us.getFirstName());
                    wr.setSakaiState(state);
                    retVal.add(wr);
                }
            }
        }

        List<WrapperImp> remove = new ArrayList<>();
        if (collectionSakaiStateList != null && collectionSakaiStateList.size() > 0) {
            for (WrapperImp w : retVal) {
                boolean _has = false;
                for (Object stObj : collectionSakaiStateList) {
                    SakaiEntityState st = (SakaiEntityState) stObj;
                    if (w.getSakaiState() == null)
                        _has = true;
                    else {
                        if (w.getSakaiState().getCode().equals(st.getCode()))
                            _has = true;
                    }
                }

                if (!_has)
                    remove.add(w);

            }

            retVal.removeAll(remove);

        }
        return applySakaiStateAndNameFilter(retVal, settings);
    }

    protected Long makeFaleId(int iter)
    {
        Calendar ca1 = Calendar.getInstance();
        Calendar ca2 = Calendar.getInstance();

        ca1.set(1970, 01, 01, 0, 0, 0);
        //ca2.set(2070, 01, 01, 0, 0, 0);
        ca2.add(Calendar.YEAR, 10);
        ca2.add(Calendar.SECOND, iter);

        Long delta = ca2.getTimeInMillis() - ca1.getTimeInMillis();
        Long delta_ss = delta / 1000;

        return delta_ss * 1048576000
                + ((((1 * 256) | 255) * 4096) | (9999 & 4095));

    }

    @SuppressWarnings("rawtypes")
    protected List<WrapperImp> applySakaiStateAndNameFilter(List<WrapperImp> lst, IDataSettings settings)
    {
        Object sakaiStateList = settings.get("sakaiStateList");
        Collection collectionSakaiStateList = null;
        if (sakaiStateList instanceof Collection) {
            if (!((Collection) sakaiStateList).isEmpty())
                collectionSakaiStateList = (Collection) sakaiStateList;
        }

        String name = null;

        if (settings.get("name") != null)
            name = settings.get("name");

        List<WrapperImp> remove = new ArrayList<>();
        if ((collectionSakaiStateList != null && collectionSakaiStateList.size() > 0) || name != null) {
            for (WrapperImp w : lst) {
                boolean _has = true;
                boolean _hasName = true;
                if (collectionSakaiStateList != null && collectionSakaiStateList.size() > 0) {
                    _has = false;
                    for (Object stObj : collectionSakaiStateList) {
                        SakaiEntityState st = (SakaiEntityState) stObj;
                        if (w.getSakaiState() == null)
                            _has = true;
                        else {
                            if (w.getSakaiState().getCode().equals(st.getCode()))
                                _has = true;
                        }
                    }
                }

                if (name != null) {
                    if (!w.getName().toUpperCase().contains(name.toUpperCase()))
                        _hasName = false;
                }

                if (!_has || !_hasName)
                    remove.add(w);
            }

            lst.removeAll(remove);
        }

        return lst;
    }

    protected List<OrgUnit> getOrgUnitListForEmployeePost()
    {
        // выбираем преподов
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EmployeePost.class, "employeepost")
                .column(property(EmployeePost.orgUnit().id().fromAlias("employeepost")))
                .where(eq(property(EmployeePost.employee().archival().fromAlias("employeepost")), value(Boolean.FALSE)))
                .where(eq(property(EmployeePost.postStatus().active().fromAlias("employeepost")), value(Boolean.TRUE)))
                .distinct();

        // все орг unit
        DQLSelectBuilder dqlOU = new DQLSelectBuilder()
                .fromEntity(OrgUnit.class, "orgunit")
                .column("orgunit")
                .where(in(property(OrgUnit.id().fromAlias("orgunit")), dql.buildQuery()))
                .order(property(OrgUnit.fullTitle().fromAlias("orgunit")));

        return dqlOU.createStatement(getSession()).list();

    }

}
