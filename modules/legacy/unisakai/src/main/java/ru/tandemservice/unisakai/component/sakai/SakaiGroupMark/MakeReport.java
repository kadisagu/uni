package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiTemplateDocument;
import ru.tandemservice.unisakai.util.Sakai.ImportInfo;

import java.util.ArrayList;
import java.util.List;


public class MakeReport {

    private RtfDocument getTemplate()
    {

        ITemplateDocument result = UniDaoFacade.getCoreDao().getCatalogItem(UniSakaiTemplateDocument.class, "mark.import");
        return new RtfReader().read(result.getContent());

    }

    public DatabaseFile getContent(WrapperGM wrap, ImportInfo info)
    {

        RtfDocument resultDoc = makeReport(wrap, info);

        return createFile(resultDoc);
    }

    private DatabaseFile createFile(RtfDocument document)
    {
        DatabaseFile result = new DatabaseFile();
        result.setContent(RtfUtil.toByteArray(document));
        return result;
    }

    private RtfDocument makeReport(WrapperGM wrap, ImportInfo info)
    {

        // 1 - получим шаблон
        RtfDocument document = getTemplate().getClone();
        // RtfDocument resultDoc = document.getClone();

        // resultDoc.getElementList().clear();


        // Таблица в шаблоне - это T
        RtfTableModifier tablemod = createTableModifier();
        RtfInjectModifier varmod = createInjectModifier();

        // 2 - модифачим
        tablemod = processTableModifier(tablemod, info);
        varmod = processInjectModifier(varmod, info);

        // 3 - применяем к документу
        tablemod.modify(document);
        varmod.modify(document);


        return document;

    }

    private RtfInjectModifier processInjectModifier(RtfInjectModifier varmod, ImportInfo info)
    {
        varmod.put("rezult", info.getTotalRezult());
        return varmod;
    }

    private RtfTableModifier processTableModifier(RtfTableModifier tablemod, ImportInfo info)
    {
        List<String[]> table = new ArrayList<String[]>();
        List<ImportInfo.RowInfo> lst = info.getRows();

        for (ImportInfo.RowInfo row : lst) {

            String[] strs = new String[]{row.getCell1(), row.getCell2()};
            table.add(strs);
        }


        String[][] t = table.toArray(new String[][]{});
        tablemod.put("T", t);
        return tablemod;

    }

    private RtfTableModifier createTableModifier()
    {
        RtfTableModifier retVal = new RtfTableModifier();
        return retVal;
    }


    private RtfInjectModifier createInjectModifier()
    {
        RtfInjectModifier retVal = new RtfInjectModifier();
        return retVal;
    }


}
