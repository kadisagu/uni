package ru.tandemservice.unisakai.component.catalog.uniSakaiSettings.UniSakaiSettingsAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings;

@Input({
        @Bind(key = "uniSakaiSettingsId", binding = "uniSakaiSettingsId")

})
@Output({
        @Bind(key = "uniSakaiSettingsId", binding = "uniSakaiSettingsId")
})
public class Model {
    private Long _uniSakaiSettingsId;
    private UniSakaiSettings _uniSakaiSettings;


    public void setUniSakaiSettingsId(Long _uniSakaiSettingsId) {
        this._uniSakaiSettingsId = _uniSakaiSettingsId;
    }

    public Long getUniSakaiSettingsId() {
        return _uniSakaiSettingsId;
    }

    public void setUniSakaiSettings(UniSakaiSettings _uniSakaiSettings) {
        this._uniSakaiSettings = _uniSakaiSettings;
    }

    public UniSakaiSettings getUniSakaiSettings() {
        return _uniSakaiSettings;
    }

    public boolean getLongMessage()
    {
        return  (_uniSakaiSettings != null && _uniSakaiSettings.getStringValue() != null && _uniSakaiSettings.getStringValue().length() > 50);
    }


}
