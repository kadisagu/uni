package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionMark;

/**
 * Обертка вокруг оценки
 * Для студента
 *
 * @author Администратор
 */
public class WrappeStudentMark {
    private Long _studentId;

    private SessionMark sessionMark;

    private SessionMarkGradeValueCatalogItem sessionMarkGrade;


    public WrappeStudentMark
            (
                    Long studentId,
                    SessionMark sessionMark,
                    SessionMarkGradeValueCatalogItem sessionMarkGrade
            )
    {

        this._studentId = studentId;
        this.sessionMark = sessionMark;
        this.sessionMarkGrade = sessionMarkGrade;
    }

    public void setStudentId(Long _studentId) {
        this._studentId = _studentId;
    }

    public Long getStudentId() {
        return _studentId;
    }

    public void setSessionMark(SessionMark sessionMark) {
        this.sessionMark = sessionMark;
    }

    public SessionMark getSessionMark() {
        return sessionMark;
    }

    public void setSessionMarkGrade(SessionMarkGradeValueCatalogItem sessionMarkGrade) {
        this.sessionMarkGrade = sessionMarkGrade;
    }

    public SessionMarkGradeValueCatalogItem getSessionMarkGrade() {
        return sessionMarkGrade;
    }


}
