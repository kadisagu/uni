package ru.tandemservice.unisakai.component.sakai.imp.AgsMassImp;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.UserSakai;

import java.util.Collections;
import java.util.List;
import java.util.Map;


@Input({@Bind(key = "ids", binding = "groupIds")})
public class Model {
    public static final String MUTEX = "lockString";

    private String title = "";

    private List<Long> groupIds = Collections.emptyList();
    private List<String> siteIds = Collections.emptyList();


    private boolean processSt;
    private IDataSettings settings;

    private Map<String, UserSakai> sakaiUsers = null;
    private Map<String, List<SiteSakai>> sakaiSites = null;

    private boolean processEnd = false;


    public void setProcessSt(boolean processSt) {
        this.processSt = processSt;
    }

    public boolean getProcessSt() {
        return processSt;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSakaiUsers(Map<String, UserSakai> sakaiUsers) {
        this.sakaiUsers = sakaiUsers;
    }

    public Map<String, UserSakai> getSakaiUsers() {
        return sakaiUsers;
    }

    public void setSakaiSites(Map<String, List<SiteSakai>> sakaiSites) {
        this.sakaiSites = sakaiSites;
    }

    public Map<String, List<SiteSakai>> getSakaiSites() {
        return sakaiSites;
    }

    public void setGroupIds(List<Long> groupIds) {
        this.groupIds = groupIds;
    }

    public List<Long> getGroupIds() {
        return groupIds;
    }

    public void setStrongListSt(boolean strongListSt)
    {
        this.getSettings().set("strongListSt", strongListSt);

    }

    public boolean getStrongListSt()
    {
        final Object val = this.getSettings().get("strongListSt");
        if (val != null)
            return (Boolean) val;
        else
            return false;
    }

    public void setProcessEnd(boolean processEnd) {
        this.processEnd = processEnd;
    }

    public boolean getProcessEnd() {
        return processEnd;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setSiteIds(List<String> siteIds) {
        this.siteIds = siteIds;
    }

    public List<String> getSiteIds() {
        return siteIds;
    }


}
