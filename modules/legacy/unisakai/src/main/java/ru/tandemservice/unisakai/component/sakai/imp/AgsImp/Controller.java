package ru.tandemservice.unisakai.component.sakai.imp.AgsImp;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;
import ru.tandemservice.unisakai.logic.SiteSakai;

import java.util.List;
import java.util.stream.Collectors;


public class Controller extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Controller
{

    @Override
    public boolean needLoadSakaiSites() {
        return true;
    }


    @Override
    public boolean needLoadSakaiUsers() {
        return false;
    }

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        // сначала активацим старое
        super.onRefreshComponent(component);
    }

    @Override
    public void MakeDataColumn(DynamicListDataSource<WrapperImp> dataSource)
    {
        dataSource.addColumn(new CheckboxColumn("select"));
        // dataSource.addColumn(new ActionColumn("Сущность", "", "onClickEntity").setTextKey("entityType").setDisplayHeader(true));
        dataSource.addColumn(new SimpleColumn("Сущность", "entityType").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Имя", "name").setClickable(true));

        dataSource.addColumn(new SimpleColumn("Описание", "description").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Статус в Sakai", "sakaiInfo").setClickable(false));
    }


    @Override
    public String getSettingsKey()
    {
        return "sakai.imp.AgsImp";
    }

    @Override
    public void onPostProcess()
    {
    }


    @Override
    /**
     * Данный метод построчной обработки записей, имеет отношение к базовому функционалу
     * вызывается из onClick()
     */
    public void ProcessRow(WrapperImp row, ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model) throws Exception
    {
    }

    public void onClick(final IBusinessComponent context)
    {
        List<Long> ids = getSelectedIdsSiteDisciplineId(context);

        context.createChildRegion("listRegion", new ComponentActivator(
                ru.tandemservice.unisakai.component.sakai.imp.AgsMassImp.Model.class.getPackage().getName(),
                new ParametersMap().add("ids", ids)));
    }


    public void onClickDelete(final IBusinessComponent context)
    {
        List<Long> ids = getSelectedIdsSiteDisciplineId(context);

        context.createChildRegion("listRegion", new ComponentActivator(
                ru.tandemservice.unisakai.component.sakai.imp.AgsMassDel.Model.class.getPackage().getName(),
                new ParametersMap().add("ids", ids)));
    }


    public void onClickDeleteSite(final IBusinessComponent context)
    {
        List<SiteSakai> sites = getSelectedSakaiSite(context);

        if (sites.size() > 0) {

            List<String> idSites = sites.stream().map(SiteSakai::getSiteid).collect(Collectors.toList());
            context.createChildRegion("listRegion", new ComponentActivator(
                    ru.tandemservice.unisakai.component.sakai.imp.AgsMassSiteDel.Model.class.getPackage().getName(),
                    new ParametersMap().add("idsSite", idSites)));
        }
    }


}
