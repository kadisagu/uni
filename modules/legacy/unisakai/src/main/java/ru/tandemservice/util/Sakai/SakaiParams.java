package ru.tandemservice.util.Sakai;

import ru.tandemservice.util.sprav.KeyValues;

public class SakaiParams {

    public static SakaiProperty FillParams() throws Exception
    {
        SakaiProperty prop = new SakaiProperty();

        String _login = KeyValues.getStringValue("unisakai.login", true);
        String _pwd = KeyValues.getStringValue("unisakai.password", true);

        prop.LOGIN = _login;
        prop.PASSWORD = _pwd;

        // нужно читать из конфига
        String studentRole = KeyValues.getStringValue("unisakai.studentRole", true); //"access";
        String tutorRole = KeyValues.getStringValue("unisakai.tutorRole", true); //"maintain";

        prop.STUDENT_ROLE = studentRole;
        prop.TUTOR_ROLE = tutorRole;


        // это нужно читать из конфига
        String _templateSite = KeyValues.getStringValue("unisakai.templateSite", true); //"8ee70eed-9315-443c-b157-402964d1aa4c";
        String _templateSiteGroup = KeyValues.getStringValue("unisakai.templateGroupSite", true);


        String joinerrole = KeyValues.getStringValue("unisakai.siteJoinerrole", true); // "access";
        String type = KeyValues.getStringValue("unisakai.siteType", true); // "project";

        prop.TEMPLATE_SITE = _templateSite;
        prop.SITE_JOINERROLE = joinerrole;
        prop.SITE_TYPE = type;

        prop.TEMPLATE_SITE_GROUP = _templateSiteGroup;

        String _use_ldap = KeyValues.getStringValue(KeyValues.UNI_SAKAI_USELDAP, true);
        prop.USE_LDAP_AUTH = Boolean.parseBoolean(_use_ldap);

        return prop;
    }


}
