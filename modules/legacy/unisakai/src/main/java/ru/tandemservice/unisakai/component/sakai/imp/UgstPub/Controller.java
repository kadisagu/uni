package ru.tandemservice.unisakai.component.sakai.imp.UgstPub;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.process.*;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisakai.dao.wrappers.WrapperStudentToSiteSakai;
import ru.tandemservice.unisakai.entity.catalog.SakaiEntityState;
import ru.tandemservice.unisakai.logic.ActionSakai;
import ru.tandemservice.unisakai.logic.SiteSakai;
import ru.tandemservice.unisakai.logic.Tools;
import ru.tandemservice.unisakai.logic.UserSakai;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {

        Model model = component.getModel();
        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey()));

        try {
            ActionSakai as = ActionSakai.getActionSakai();
            as.Reconnect();

            model.setActionSakai(as);
            model.setUseLDAP(as.getSakaiScripts().getSakaiProperty().USE_LDAP_AUTH);

            model.setTitle("Связь с Sakai установлена");
        }
        catch (Exception e) {
            model.setTitle("Ошибка соединения с Sakai " + e.getMessage());
        }

        getDao().prepare((Model) component.getModel());

        if (model.getDataSource() == null) {
            // забор с данными
            DynamicListDataSource<WrapperStudentToSiteSakai> dataSource
                    = new DynamicListDataSource<>(component, component1 -> {
                Controller.this.getDao().refreshDataSource(Controller.this.getModel(component1));
            }, 20);

            MakeDataColumn(dataSource);
            model.setDataSource(dataSource);
        }
    }

    private void MakeDataColumn(DynamicListDataSource<WrapperStudentToSiteSakai> dataSource)
    {
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(new SimpleColumn("Студент", "title").setClickable(true));
        dataSource.addColumn(new SimpleColumn("Логин", "eid").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Статус", "state." + SakaiEntityState.P_TITLE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Номер студента", "studentNumber").setClickable(false));
        dataSource.addColumn(new SimpleColumn("уч.год ", "yearString").setClickable(false));
        dataSource.addColumn(new SimpleColumn("часть года ", "partYearString").setClickable(false));
    }

    public void onClickExit(final IBusinessComponent context)
    {
        deactivate(context);
    }


    /**
     * Синхронизировать сайт
     *
     * @param context
     *
     * @throws Exception
     */
    public void onClick(final IBusinessComponent context) throws Exception
    {
        Model model = context.getModel();
        List<WrapperStudentToSiteSakai> lst = getSelectedWrapperStudent(context);

        getDao().sakaiInfoToModel(model);
        doList(context, lst, true, true);
    }


    @SuppressWarnings("rawtypes")
    private void doList(final IBusinessComponent context, final Collection lst, final boolean toSite, final boolean isStudent) throws Exception
    {
        final Model model = context.getModel();

        synchronized (Model.MUTEX) {
            final IBackgroundProcess process = new BackgroundProcessBase() {


                @Override
                public ProcessResult run(final ProcessState state) {

                    try {

                        try {
                            Thread.sleep(1000);
                        }
                        catch (final Throwable t) {
                        }

                        final IEventServiceLock eventLock = CoreServices.eventService().lock();
                        try {
                            state.setMaxValue(100);
                            state.setCurrentValue(0);
                            state.setDisplayMode(ProcessDisplayMode.percent);

                            int i = 0;

                            // 1 - обновим сайт
                            model.getActionSakai().SaveOrUpdateSiteD
                                    (
                                            model.getEppRegistryElementPartFControlAction()
                                            , model.getSites()
                                    );

                            if (model.getSite() == null) {
                                // значит строкой выше создан сайт
                                SiteSakai siteVrem = Tools.getSiteSakaiByEntity(
                                        model.getEppRegistryElementPartFControlAction().getPart().getRegistryElement()
                                        , model.getEppRegistryElementPartFControlAction().getPart().getNumber()
                                        , model.getEppRegistryElementPartFControlAction().getControlAction().getCode()
                                );

                                SiteSakai siteNew = Tools.getSiteSakaiMaxTime(model.getSites(), siteVrem);

                                if (siteNew != null)
                                    model.setSite(siteNew);
                            }

                            // просматриваем по дисциплинам
                            for (Object obj : lst) {
                                state.setCurrentValue(100 * i / lst.size());
                                Thread.yield();

                                if (isStudent) {
                                    _doPartsSt(model, toSite, obj);
                                }
                                else {
                                    _doPartsTi(model, toSite, obj);
                                }

                                i++;
                            }

                            // перегружаем данные
                            getDao().sakaiInfoToModel(model);
                            return null; // закрываем диалог

                        }
                        catch (Exception ex) {
                            throw ex;
                        }
                        finally {
                            eventLock.release();
                            try {
                                Thread.sleep(100);
                            }
                            catch (final Throwable t) {
                            }
                            model.setProcessEnd(true);
                        }

                    }
                    catch (final Throwable t) {
                        model.setProcessEnd(true);
                        return new ProcessResult("Произошла ошибка " + t.getMessage(), true); // закрываем диалог
                    }
                }
            };

            BackgroundProcessHolder bHolder = new BackgroundProcessHolder();
            model.setProcessEnd(false);
            bHolder.start("Обработка", process);
        }
    }

    private void _doPartsTi(Model model
            , boolean toSite
            , Object obj)
    {


    }

    /**
     * Обработка студента
     *
     * @param model
     * @param toSite
     * @param obj
     *
     * @throws Exception
     */
    private void _doPartsSt(Model model, boolean toSite,
                            Object obj) throws Exception
    {

        if (model.getSite() == null)
            return;

        WrapperStudentToSiteSakai wr = (WrapperStudentToSiteSakai) obj;

        String eid = model.getActionSakai().getPersonEid(wr.getStudent());
        if (wr != null && wr.getStudent() != null && !model.isUseLDAP()) {
            UserSakai user = Tools.getUser(wr.getStudent());
            model.getActionSakai().SaveOrUpdateUser(user, model.getUsers());
        }


        if (toSite)
            model.getActionSakai().UserToSite(eid, model.getSite(), model.getActionSakai().getSakaiScripts().getSakaiProperty().STUDENT_ROLE);
        else
            model.getActionSakai().UserRemoveFromSite(eid, model.getSite());
    }


    /**
     * Отписать персон с сайта
     *
     * @param context
     *
     * @throws Exception
     */
    public void onClickDelete(final IBusinessComponent context) throws Exception
    {
        Model model = context.getModel();
        List<WrapperStudentToSiteSakai> lst = getSelectedWrapperStudent(context);

        getDao().sakaiInfoToModel(model);
        doList(context, lst, false, true);

    }

    private List<WrapperStudentToSiteSakai> getSelectedWrapperStudent(final IBusinessComponent context)
    {

        Model model = context.getModel();
        List<WrapperStudentToSiteSakai> lst = new ArrayList<>();

        CheckboxColumn ck = (CheckboxColumn) model.getDataSource().getColumn("select");

        Collection<IEntity> lstEnt = ck.getSelectedObjects();

        for (IEntity e : lstEnt) {
            WrapperStudentToSiteSakai w = (WrapperStudentToSiteSakai) e;
            if (w != null) {
                lst.add(w);
            }
        }
        return lst;
    }

    public String getSettingsKey()
    {
        return "sakai.imp.UgstImp";
    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);

        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();

        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onSearchParamsChange(final IBusinessComponent component)
    {
        onClickSearch(component);
    }

}
