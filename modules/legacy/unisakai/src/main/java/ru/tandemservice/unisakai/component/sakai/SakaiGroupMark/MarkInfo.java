package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

public class MarkInfo {


    private String mark;
    private String userEID;

    public MarkInfo(String mark, String userEID)
    {
        this.mark = mark;
        this.userEID = userEID;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getMark() {
        return mark;
    }

    public void setUserEID(String userEID) {
        this.userEID = userEID;
    }

    public String getUserEID() {
        return userEID;
    }
}
