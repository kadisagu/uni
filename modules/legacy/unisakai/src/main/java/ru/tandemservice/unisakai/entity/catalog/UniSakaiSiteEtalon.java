package ru.tandemservice.unisakai.entity.catalog;

import ru.tandemservice.unisakai.entity.catalog.gen.UniSakaiSiteEtalonGen;

/**
 * Эталонные сайты к частям дисциплин
 */
public class UniSakaiSiteEtalon extends UniSakaiSiteEtalonGen {
    @Override
    public String getEtalonSiteId()
    {
        if (super.getEtalonSiteId() != null)
            return super.getEtalonSiteId().trim();
        else
            return super.getEtalonSiteId();
    }
}