package ru.tandemservice.unisakai.ws.sakai;

import com.google.common.primitives.Longs;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unisakai.ws.reports.IWsReport;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author vch
 *         Сервис построения отчетов для Сакая
 *         2012/02/09
 */
@WebService(serviceName = "TandemReport")
public class TandemReport {

    @WebMethod
    public String getTestMessage()
    {
        return "testMessage";
    }

    @WebMethod
    public String getReportContent(@WebParam(name = "beanName") String beanName, @WebParam(name = "userEid") String userEid, @WebParam(name = "pwd") String pwd)
    {
        // 110237
        // 1 - проверим пароль
        // 2 - получим bean
        if (!ApplicationRuntime.containsBean(beanName))
            return "error Нет библиотеки для построения отчета";

        // запускаем цикл проверок
        // 1 - нет студента в системе
        IWsReport report = null;
        try {
            report = (IWsReport) ApplicationRuntime.getBean(beanName);
        }
        catch (Exception ex) {
            return getErrorInfo("Ошибка построения отчета, нельзя получить библиотеку " + beanName, ex);
        }

        if (report == null)
            return getErrorInfo("Ошибка построения отчета, нет библиотеки " + beanName, null);

        // проверяем наличие студента
        Long studentId = Longs.tryParse(userEid);
        Student student = (studentId == null)? null : DataAccessServices.dao().get(Student.class, studentId);
        if (student == null)
            return getErrorInfo("В системе TU нет студента с логином  " + userEid, null);

        try {
            return report.getReportHTML(student, new Object[]{});
        }
        catch (Exception ex) {
            return "error Ошибка построения отчета, система TU недоступна " + ex.getMessage();
        }
    }


    private String getErrorInfo(String string, Exception ex) {

        String retVal = "";

        retVal += "<table border='0' cellspacing='0' >";
        retVal += "<tr><td>";

        retVal += "<center><h3 class='red'>" + string + "</h3></center>";

        if (ex != null) {
            retVal += "<tr><td>";
            retVal += "<h3>" + ex.getMessage() + "</h2>";
            retVal += "</td></tr>";
        }
        retVal += "</td></tr>";
        retVal += "</table border>";

        return retVal;
    }
}
