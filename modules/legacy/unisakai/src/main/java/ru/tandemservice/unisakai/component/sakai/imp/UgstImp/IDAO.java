package ru.tandemservice.unisakai.component.sakai.imp.UgstImp;


import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;

import java.util.List;

public interface IDAO
        extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.IDAO
{
    List<WrapperImp> getListDataUgs(Model model);


    EppRegistryElementPart getEppPartById(Long id);
}
