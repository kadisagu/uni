package ru.tandemservice.unisakai.component.sakai.imp.AgsMassDel;


import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisakai.dao.SakaiDaoFacade;
import ru.tandemservice.unisakai.logic.ActionSakai;

public class Controller extends ru.tandemservice.unisakai.component.sakai.imp.AgsMassImp.Controller<IDAO, Model>
{

    public static String KEY = "sakai.imp.AgsMassDel";

    public String getSettingsKey()
    {
        return KEY;
    }


    @Override
    protected void _do(ru.tandemservice.unisakai.component.sakai.imp.AgsMassImp.Model model, Object idObj) throws Exception
    {
        Long id = (Long) idObj;

        // сие отписывает персон от сайтов
        boolean syncSt = model.getProcessSt();

        ActionSakai as = ActionSakai.getActionSakai();

        Group group = SakaiDaoFacade.getSakaiDao().getGroup(id);

        if (group == null)
            return;
        if (!syncSt)
            return;

        as.DeleteUserFromSite(group, model.getSakaiSites());
    }
}
