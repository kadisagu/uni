package ru.tandemservice.unisakai.component.sakai.imp.UgstMassImp;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupRowGen;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        // final Session session = getSession();
        if (model.getIdYear() != null)
            model.setYear(getNotNull(EducationYear.class, model.getIdYear()));

        if (model.getIdPart() != null)
            model.setPart(getNotNull(YearDistributionPart.class, model.getIdPart()));


        // учебные года только из реальных групп
        model.setYearModel(new EducationYearModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder eduYear = new DQLSelectBuilder()
                        .fromEntity(EppRealEduGroupRow.class, alias)
                        .column(property(EppRealEduGroupRowGen.group().summary().yearPart().year().educationYear().id().fromAlias(alias)))
                        .distinct();


                final DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationYear.class, alias);
                dql.where(in(property(EducationYear.id().fromAlias(alias)), eduYear.buildQuery()));
                dql.order(property(EducationYear.intValue().fromAlias(alias)));
                return dql;
            }
        });


        // части учебных годов
        DQLSelectBuilder partsDQL = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroupRow.class, "obj")
                .column(property(EppRealEduGroupRowGen.group().summary().yearPart().part().fromAlias("obj")))
                .distinct();

        if (model.getYear() != null)
            partsDQL.where(eq(property(EppRealEduGroupRowGen.group().summary().yearPart().year().educationYear().fromAlias("obj")), value(model.getYear())));

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(YearDistributionPart.class, "part")
                .column("part")
                .where(in(property(YearDistributionPart.id().fromAlias("part")), partsDQL.buildQuery()))
                .order(property(YearDistributionPart.code().fromAlias("part")));

        Set<YearDistributionPart> parts = new LinkedHashSet<YearDistributionPart>(dql.createStatement(getSession()).<YearDistributionPart>list());
        model.setPartsList(HierarchyUtil.listHierarchyNodesWithParents(new ArrayList<IHierarchyItem>(parts), false));


        model.setDevelopFormListModel(new LazySimpleSelectModel<DevelopForm>(DevelopForm.class).setSortProperty(DevelopForm.P_CODE));
        model.setDevelopTechListModel(new LazySimpleSelectModel<DevelopTech>(DevelopTech.class).setSortProperty(DevelopTech.P_CODE));
    }
}
