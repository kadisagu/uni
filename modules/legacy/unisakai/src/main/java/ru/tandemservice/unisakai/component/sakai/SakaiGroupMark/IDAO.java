package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.unisakai.util.Sakai.ImportInfo;

import java.util.List;

public interface IDAO extends IUniDao<Model> {
    void refreshDataSource(Model model);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    List<Exception> doMarkImportInNewTransaction(Model model, WrapperGM wrap, ImportInfo info) throws Exception;

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void saveReportInNewTransaction(WrapperGM wrap, ImportInfo info);


//	 void setSakaiSession (Model model) throws Exception;
//	 void loginToSakai(Model model) throws Exception;

    void sakaiInfoToModel(Model model);
}
