package ru.tandemservice.unisakai.ws.reports;


import ru.tandemservice.uni.entity.employee.Student;

/**
 * интерфейс работы с отчетами
 *
 * @author vch
 */
public interface IWsReport {
    /**
     * Отдает сформированный HTML отчет
     *
     * @param params
     *
     * @return
     */
    String getReportHTML(Student student, Object[] params);


}
