package ru.tandemservice.unisakai.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Импорт оценок
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniSakaiMarkImportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport";
    public static final String ENTITY_NAME = "uniSakaiMarkImport";
    public static final int VERSION_HASH = 1573299688;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_IS_SUCCESS = "isSuccess";
    public static final String P_PRIM = "prim";
    public static final String L_REGISTRY_ELEMENT = "registryElement";
    public static final String P_PART = "part";
    public static final String P_CACTIONCODE = "cactioncode";

    private DatabaseFile _content;     // Печатная форма
    private Date _createDate;     // Дата импорта
    private boolean _isSuccess;     // Завершилось успешно
    private String _prim;     // Примечание
    private EppRegistryElement _registryElement;     // Элемент реестра
    private int _part;     // часть дисциплины
    private String _cactioncode;     // код контрольного мероприятия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата импорта. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата импорта. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Завершилось успешно. Свойство не может быть null.
     */
    @NotNull
    public boolean isIsSuccess()
    {
        return _isSuccess;
    }

    /**
     * @param isSuccess Завершилось успешно. Свойство не может быть null.
     */
    public void setIsSuccess(boolean isSuccess)
    {
        dirty(_isSuccess, isSuccess);
        _isSuccess = isSuccess;
    }

    /**
     * @return Примечание.
     */
    @Length(max=255)
    public String getPrim()
    {
        return _prim;
    }

    /**
     * @param prim Примечание.
     */
    public void setPrim(String prim)
    {
        dirty(_prim, prim);
        _prim = prim;
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElement getRegistryElement()
    {
        return _registryElement;
    }

    /**
     * @param registryElement Элемент реестра. Свойство не может быть null.
     */
    public void setRegistryElement(EppRegistryElement registryElement)
    {
        dirty(_registryElement, registryElement);
        _registryElement = registryElement;
    }

    /**
     * @return часть дисциплины. Свойство не может быть null.
     */
    @NotNull
    public int getPart()
    {
        return _part;
    }

    /**
     * @param part часть дисциплины. Свойство не может быть null.
     */
    public void setPart(int part)
    {
        dirty(_part, part);
        _part = part;
    }

    /**
     * @return код контрольного мероприятия.
     */
    @Length(max=255)
    public String getCactioncode()
    {
        return _cactioncode;
    }

    /**
     * @param cactioncode код контрольного мероприятия.
     */
    public void setCactioncode(String cactioncode)
    {
        dirty(_cactioncode, cactioncode);
        _cactioncode = cactioncode;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniSakaiMarkImportGen)
        {
            setContent(((UniSakaiMarkImport)another).getContent());
            setCreateDate(((UniSakaiMarkImport)another).getCreateDate());
            setIsSuccess(((UniSakaiMarkImport)another).isIsSuccess());
            setPrim(((UniSakaiMarkImport)another).getPrim());
            setRegistryElement(((UniSakaiMarkImport)another).getRegistryElement());
            setPart(((UniSakaiMarkImport)another).getPart());
            setCactioncode(((UniSakaiMarkImport)another).getCactioncode());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniSakaiMarkImportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniSakaiMarkImport.class;
        }

        public T newInstance()
        {
            return (T) new UniSakaiMarkImport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "createDate":
                    return obj.getCreateDate();
                case "isSuccess":
                    return obj.isIsSuccess();
                case "prim":
                    return obj.getPrim();
                case "registryElement":
                    return obj.getRegistryElement();
                case "part":
                    return obj.getPart();
                case "cactioncode":
                    return obj.getCactioncode();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "isSuccess":
                    obj.setIsSuccess((Boolean) value);
                    return;
                case "prim":
                    obj.setPrim((String) value);
                    return;
                case "registryElement":
                    obj.setRegistryElement((EppRegistryElement) value);
                    return;
                case "part":
                    obj.setPart((Integer) value);
                    return;
                case "cactioncode":
                    obj.setCactioncode((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "createDate":
                        return true;
                case "isSuccess":
                        return true;
                case "prim":
                        return true;
                case "registryElement":
                        return true;
                case "part":
                        return true;
                case "cactioncode":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "createDate":
                    return true;
                case "isSuccess":
                    return true;
                case "prim":
                    return true;
                case "registryElement":
                    return true;
                case "part":
                    return true;
                case "cactioncode":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "createDate":
                    return Date.class;
                case "isSuccess":
                    return Boolean.class;
                case "prim":
                    return String.class;
                case "registryElement":
                    return EppRegistryElement.class;
                case "part":
                    return Integer.class;
                case "cactioncode":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniSakaiMarkImport> _dslPath = new Path<UniSakaiMarkImport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniSakaiMarkImport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата импорта. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Завершилось успешно. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#isIsSuccess()
     */
    public static PropertyPath<Boolean> isSuccess()
    {
        return _dslPath.isSuccess();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getPrim()
     */
    public static PropertyPath<String> prim()
    {
        return _dslPath.prim();
    }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getRegistryElement()
     */
    public static EppRegistryElement.Path<EppRegistryElement> registryElement()
    {
        return _dslPath.registryElement();
    }

    /**
     * @return часть дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getPart()
     */
    public static PropertyPath<Integer> part()
    {
        return _dslPath.part();
    }

    /**
     * @return код контрольного мероприятия.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getCactioncode()
     */
    public static PropertyPath<String> cactioncode()
    {
        return _dslPath.cactioncode();
    }

    public static class Path<E extends UniSakaiMarkImport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Boolean> _isSuccess;
        private PropertyPath<String> _prim;
        private EppRegistryElement.Path<EppRegistryElement> _registryElement;
        private PropertyPath<Integer> _part;
        private PropertyPath<String> _cactioncode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата импорта. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(UniSakaiMarkImportGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Завершилось успешно. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#isIsSuccess()
     */
        public PropertyPath<Boolean> isSuccess()
        {
            if(_isSuccess == null )
                _isSuccess = new PropertyPath<Boolean>(UniSakaiMarkImportGen.P_IS_SUCCESS, this);
            return _isSuccess;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getPrim()
     */
        public PropertyPath<String> prim()
        {
            if(_prim == null )
                _prim = new PropertyPath<String>(UniSakaiMarkImportGen.P_PRIM, this);
            return _prim;
        }

    /**
     * @return Элемент реестра. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getRegistryElement()
     */
        public EppRegistryElement.Path<EppRegistryElement> registryElement()
        {
            if(_registryElement == null )
                _registryElement = new EppRegistryElement.Path<EppRegistryElement>(L_REGISTRY_ELEMENT, this);
            return _registryElement;
        }

    /**
     * @return часть дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getPart()
     */
        public PropertyPath<Integer> part()
        {
            if(_part == null )
                _part = new PropertyPath<Integer>(UniSakaiMarkImportGen.P_PART, this);
            return _part;
        }

    /**
     * @return код контрольного мероприятия.
     * @see ru.tandemservice.unisakai.entity.catalog.UniSakaiMarkImport#getCactioncode()
     */
        public PropertyPath<String> cactioncode()
        {
            if(_cactioncode == null )
                _cactioncode = new PropertyPath<String>(UniSakaiMarkImportGen.P_CACTIONCODE, this);
            return _cactioncode;
        }

        public Class getEntityClass()
        {
            return UniSakaiMarkImport.class;
        }

        public String getEntityName()
        {
            return "uniSakaiMarkImport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
