package ru.tandemservice.unisakai.component.sakai.SakaiGroupMark;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisakai.logic.ActionSakai;
import ru.tandemservice.unisakai.util.Sakai.ImportInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        final String settingsKey = "sakai.group.mark";
        model.setSettings(UniBaseUtils.getDataSettings(component, settingsKey));

        try {
            ActionSakai as = ActionSakai.getActionSakai();
            as.Reconnect();
            model.setActionSakai(as);
            model.setTitle("Связь с Sakai установлена");
            model.setUseLDAP(as.getSakaiScripts().getSakaiProperty().USE_LDAP_AUTH);
        }
        catch (Exception e) {
            model.setTitle("Ошибка соединения с Sakai " + e.getMessage());
        }

        this.getDao().prepare(model);

        if (model.getDataSource() == null) {
            // забор с данными
            DynamicListDataSource<WrapperGM> dataSource = new DynamicListDataSource<>(component, component1 -> {
                Controller.this.getDao().refreshDataSource(Controller.this.getModel(component1));
            });


            dataSource.addColumn(new CheckboxColumn("select"));
            dataSource.addColumn(new SimpleColumn("Группа", "groupName"));
            dataSource.addColumn(new MultiValuesColumn("Контрольное мероприятие", "cactionTitle").setOrderable(false));

            dataSource.addColumn(new MultiValuesColumn("Учебный год", "eduYear").setOrderable(false));
            dataSource.addColumn(new MultiValuesColumn("Часть года", "termPart"));

            dataSource.addColumn(new SimpleColumn("Дата", "createDateString").setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Результат", "importRezult"));
            dataSource.addColumn(new ActionColumn("Отчет", "edit_mark", "onClickMark"));

            model.setDataSource(dataSource);
        }

    }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        this.getDao().prepare(model);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        this.getModel(context).getSettings().clear();
        this.onClickSearch(context);
    }

    public void onSearchParamsChange(final IBusinessComponent component)
    {
        //final Model model = this.getModel(component);
        //UniDaoFacade.getSettingsManager().saveSettings(model.getSettings());
        //this.getDao().prepare(model);

        // теперь поиск
        onClickSearch(component);
    }


    public void onClickMark(final IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        List<WrapperGM> lst = this.getModel(component).getDataSource().getEntityList();

        WrapperGM wfind = null;

        for (WrapperGM w : lst) {
            if (w.getId().equals(id))
                wfind = w;
        }


        if (wfind != null && wfind.getMarkImportId() != null) {
            Long idRep = wfind.getMarkImportId();
            BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(idRep), true);

//            activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT,
//                                                             new ParametersMap().add("reportId", idRep).add("extension", "rtf").add("zip", Boolean.FALSE)));
        }
    }

    public void onClickGroup(final IBusinessComponent component) throws Exception
    {
        Model model = this.getModel(component);
        model.setLstWrap(null);

        CheckboxColumn ck = (CheckboxColumn) model.getDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();
        List<WrapperGM> lstWrap = new ArrayList<>();

        for (IEntity ent : lst) {
            WrapperGM wrap = (WrapperGM) ent;
            if (wrap != null)
                lstWrap.add(wrap);
        }


        if (lstWrap.size() > 0) {
            model.setLstWrap(lstWrap);
            try {
                this.getDao().sakaiInfoToModel(model);
            }
            catch (Exception e) {
                model.addTitle(e.getMessage());
                throw e;
            }

            synchronized (Model.MUTEX) {
                for (WrapperGM wrap : lstWrap) {
                    ImportInfo info = new ImportInfo();
                    try {
                        List<Exception> rv = this.getDao().doMarkImportInNewTransaction(model, wrap, info);

                        if (rv.size() > 0) {
                            String errMsg = "Ошибки импорта ";
                            info.setSuccess(false);

                            info.AddTotal(errMsg);
                        }

                    }
                    catch (Exception e) {
                        info.setSuccess(false);
                        info.AddTotal("Ошибка импорта " + e.getMessage());
                    }

                    try {
                        // нужно попробовать сохранить отчет
                        this.getDao().saveReportInNewTransaction(wrap, info);
                    }
                    catch (Exception e) {
                        throw e;
                    }
                }
            }
        }
    }
}
