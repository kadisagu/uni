package ru.tandemservice.unisakai.component.catalog.uniSakaiSettings.UniSakaiSettingsPub;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSettings;
import ru.tandemservice.unisakai.entity.catalog.UniSakaiSettingsType;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        model.setUniSakaiSettingsTypeModel(new LazySimpleSelectModel<UniSakaiSettingsType>(UniSakaiSettingsType.class).setSortProperty(UniSakaiSettingsType.P_PRIORITY));

    }

    @SuppressWarnings("unchecked")
    public <T extends IEntity> void prepareCustomDataSource(Model model, DynamicListDataSource<T> dataSource)
    {

        List<UniSakaiSettings> itemList = _getDataList(model);

        int count = Math.max(itemList.size() + 1, 2);
        dataSource.setTotalSize(count);
        dataSource.setCountRow(count);
        dataSource.createPage((List<T>) itemList);
    }

    @SuppressWarnings("unchecked")
    private List<UniSakaiSettings> _getDataList(Model model)
    {
        Session _session = getSession();

        Criteria criteria = _session.createCriteria(UniSakaiSettings.class, "tbl");
        criteria.createAlias("tbl." + UniSakaiSettings.L_UNI_SAKAI_SETTINGS_TYPE, "ttype");


        if (model.getUniSakaiSettingsTypes() != null && model.getUniSakaiSettingsTypes().size() != 0)
            criteria.add(Restrictions.in("tbl." + UniSakaiSettings.L_UNI_SAKAI_SETTINGS_TYPE, model.getUniSakaiSettingsTypes()));


        criteria.addOrder(Order.asc("ttype." + UniSakaiSettings.P_PRIORITY));
        criteria.addOrder(Order.asc(UniSakaiSettings.P_PRIORITY));

        List<UniSakaiSettings> lst = criteria.list();

        return lst;
    }


}
