/* $Id$ */
package ru.tandemservice.unisakai.ws.sakai;

import org.apache.axis.message.SOAPHeaderElement;
import org.apache.ws.security.WSConstants;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.GenericHandler;
import javax.xml.rpc.handler.MessageContext;
import javax.xml.rpc.handler.soap.SOAPMessageContext;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPMessage;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author Ekaterina Zvereva
 * @since 14.09.2015
 */
public class SecurityHandler extends GenericHandler
{
    @Override
    public boolean handleRequest(MessageContext context)
    {
        try
        {
            SOAPMessage msg = ((SOAPMessageContext) context).getMessage();

            SOAPElement header = msg.getSOAPPart().getEnvelope().getHeader();
            SOAPElement sec = header.addChildElement(WSConstants.WSSE_LN, WSConstants.WSSE_PREFIX, WSConstants.WSSE_NS);
            ((SOAPHeaderElement) sec).setActor(null);

            // Timestamp

            SOAPElement timestamp = sec.addChildElement(WSConstants.TIMESTAMP_TOKEN_LN, WSConstants.WSU_PREFIX, WSConstants.WSU_NS);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            format.setTimeZone(TimeZone.getTimeZone("GMT"));
            Calendar cal = Calendar.getInstance();
            SOAPElement created = timestamp.addChildElement(WSConstants.CREATED_LN, WSConstants.WSU_PREFIX);
            created.addTextNode(format.format(cal.getTime()));

            cal.add(Calendar.MINUTE, 5);
            SOAPElement Expires = timestamp.addChildElement(WSConstants.EXPIRES_LN, WSConstants.WSU_PREFIX);
            Expires.addTextNode(format.format(cal.getTime()));

            // UsernameToken

            SOAPElement usernameToken = sec.addChildElement(WSConstants.USERNAME_TOKEN_LN, WSConstants.WSSE_PREFIX);

            SOAPElement username = usernameToken.addChildElement(WSConstants.USERNAME_LN, WSConstants.WSSE_PREFIX);
            username.addTextNode("university");

            SOAPElement password = usernameToken.addChildElement(WSConstants.PASSWORD_LN, WSConstants.WSSE_PREFIX);
            password.setAttribute(WSConstants.PASSWORD_TYPE_ATTR, WSConstants.PASSWORD_TEXT);
            password.addTextNode("123456");

//            System.out.println(NsiReactorUtils.getXmlFormatted(msg.getSOAPPart().getEnvelope().toString()));

            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return super.handleRequest(context);
    }

    @Override
    public QName[] getHeaders()
    {
        return new QName[0];  //To change body of implemented methods use File | Settings | File Templates.
    }
}