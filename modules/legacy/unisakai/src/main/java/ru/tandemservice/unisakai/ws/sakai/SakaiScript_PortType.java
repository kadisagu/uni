/**
 * SakaiScript_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unisakai.ws.sakai;

public interface SakaiScript_PortType extends java.rmi.Remote {
    public String removeUser(String sessionid, String eid) throws java.rmi.RemoteException;
    public String getUserId(String sessionid, String eid) throws java.rmi.RemoteException;
    public String getUserId(String sessionid) throws java.rmi.RemoteException;
    public String getUserType(String sessionid, String userid) throws java.rmi.RemoteException;
    public String getSiteTitle(String sessionid, String siteid) throws java.rmi.RemoteException;
    public String removeAuthzGroup(String sessionid, String authzgroupid) throws java.rmi.RemoteException;
    public String getSiteDescription(String sessionid, String siteid) throws java.rmi.RemoteException;
    public String getUserDisplayName(String sessionid) throws java.rmi.RemoteException;
    public String getUserDisplayName(String sessionid, String userid) throws java.rmi.RemoteException;
    public String getUserEmail(String sessionid) throws java.rmi.RemoteException;
    public String getUserEmail(String sessionid, String userid) throws java.rmi.RemoteException;
    public boolean checkForUser(String sessionid, String eid) throws java.rmi.RemoteException;
    public boolean checkForSite(String sessionid, String siteid) throws java.rmi.RemoteException;
    public String removeSite(String sessionid, String siteid) throws java.rmi.RemoteException;
    public String getSiteSkin(String sessionid, String siteid) throws java.rmi.RemoteException;
    public boolean addMemberToGroup(String sessionid, String siteid, String groupid, String userid) throws java.rmi.RemoteException;
    public String addNewUser(String sessionid, String eid, String firstname, String lastname, String email, String type, String password) throws java.rmi.RemoteException;
    public String addNewUser(String sessionid, String id, String eid, String firstname, String lastname, String email, String type, String password) throws java.rmi.RemoteException;
    public String importToolContent(String sessionid, String sourcesiteid, String destinationsiteid) throws java.rmi.RemoteException;
    public String addNewSite(String sessionid, String siteid, String title, String description, String shortdesc, String iconurl, String infourl, boolean joinable, String joinerrole, boolean published, boolean publicview, String skin, String type) throws java.rmi.RemoteException;
    public String getSiteProperty(String sessionid, String siteid, String propname) throws java.rmi.RemoteException;
    public String checkSession(String sessionid) throws java.rmi.RemoteException;
    public String addNewUserById(String sessionid, String id, String eid, String firstname, String lastname, String email, String type, String password, String postal_addr) throws java.rmi.RemoteException;
    public String changeUserInfo(String sessionid, String eid, String firstname, String lastname, String email, String type, String password) throws java.rmi.RemoteException;
    public String changeUserName(String sessionid, String eid, String firstname, String lastname) throws java.rmi.RemoteException;
    public String changeUserNameAndEmail(String sessionid, String id, String eid, String firstname, String lastname, String email, String postal_addr) throws java.rmi.RemoteException;
    public String changeUserEmail(String sessionid, String eid, String email) throws java.rmi.RemoteException;
    public String changeUserType(String sessionid, String eid, String type) throws java.rmi.RemoteException;
    public String changeUserPassword(String sessionid, String eid, String password) throws java.rmi.RemoteException;
    public String addGroupToSite(String sessionid, String siteid, String grouptitle, String groupdesc) throws java.rmi.RemoteException;
    public String getGroupsInSite(String sessionid, String siteid) throws java.rmi.RemoteException;
    public String addNewAuthzGroup(String sessionid, String authzgroupid) throws java.rmi.RemoteException;
    public String addNewRoleToAuthzGroup(String sessionid, String authzgroupid, String roleid, String description) throws java.rmi.RemoteException;
    public String removeAllRolesFromAuthzGroup(String sessionid, String authzgroupid) throws java.rmi.RemoteException;
    public String removeRoleFromAuthzGroup(String sessionid, String authzgroupid, String roleid) throws java.rmi.RemoteException;
    public String allowFunctionForRole(String sessionid, String authzgroupid, String roleid, String functionname) throws java.rmi.RemoteException;
    public String disallowAllFunctionsForRole(String sessionid, String authzgroupid, String roleid) throws java.rmi.RemoteException;
    public String disallowFunctionForRole(String sessionid, String authzgroupid, String roleid, String functionname) throws java.rmi.RemoteException;
    public String setRoleDescription(String sessionid, String authzgroupid, String roleid, String description) throws java.rmi.RemoteException;
    public String addMemberToAuthzGroupWithRole(String sessionid, String eid, String authzgroupid, String roleid) throws java.rmi.RemoteException;
    public String removeMemberFromAuthzGroup(String sessionid, String eid, String authzgroupid) throws java.rmi.RemoteException;
    public String removeAllMembersFromAuthzGroup(String sessionid, String authzgroupid) throws java.rmi.RemoteException;
    public String setRoleForAuthzGroupMaintenance(String sessionid, String authzgroupid, String roleid) throws java.rmi.RemoteException;
    public String addMemberToSiteWithRole(String sessionid, String siteid, String eid, String roleid) throws java.rmi.RemoteException;
    public String copySiteShort(String sessionid, String siteidtocopy, String newsiteid, String title, String description, String shortdesc, boolean joinable, String joinerrole, boolean published, boolean publicview, String type) throws java.rmi.RemoteException;
    public String copySite(String sessionid, String siteidtocopy, String newsiteid, String title, String description, String shortdesc, String iconurl, String infourl, boolean joinable, String joinerrole, boolean published, boolean publicview, String skin, String type) throws java.rmi.RemoteException;
    public String addNewPageToSite(String sessionid, String siteid, String pagetitle, int pagelayout) throws java.rmi.RemoteException;
    public String addNewPageToSite(String sessionid, String siteid, String pagetitle, int pagelayout, int position, boolean popup) throws java.rmi.RemoteException;
    public String removePageFromSite(String sessionid, String siteid, String pagetitle) throws java.rmi.RemoteException;
    public String addNewToolToPage(String sessionid, String siteid, String pagetitle, String tooltitle, String toolid, String layouthints) throws java.rmi.RemoteException;
    public String addConfigPropertyToTool(String sessionid, String siteid, String pagetitle, String tooltitle, String propname, String propvalue) throws java.rmi.RemoteException;
    public String addConfigPropertyToPage(String sessionid, String siteid, String pagetitle, String propname, String propvalue) throws java.rmi.RemoteException;
    public boolean checkForMemberInAuthzGroupWithRole(String sessionid, String eid, String authzgroupid, String role) throws java.rmi.RemoteException;
    public String getSitesUserCanAccess(String sessionid) throws java.rmi.RemoteException;
    public String getSitesUserCanAccess(String sessionid, String userid) throws java.rmi.RemoteException;
    public String getAllSitesForUser(String sessionid) throws java.rmi.RemoteException;
    public String getAllSitesForUser(String sessionid, String userid) throws java.rmi.RemoteException;
    public boolean isSiteJoinable(String sessionid, String siteid) throws java.rmi.RemoteException;
    public String changeSiteTitle(String sessionid, String siteid, String title) throws java.rmi.RemoteException;
    public String changeSiteSkin(String sessionid, String siteid, String skin) throws java.rmi.RemoteException;
    public String changeSiteJoinable(String sessionid, String siteid, boolean joinable, String joinerrole, boolean publicview) throws java.rmi.RemoteException;
    public String changeSiteIconUrl(String sessionid, String siteid, String iconurl) throws java.rmi.RemoteException;
    public String changeSiteDescription(String sessionid, String siteid, String description) throws java.rmi.RemoteException;
    public String setSiteProperty(String sessionid, String siteid, String propname, String propvalue) throws java.rmi.RemoteException;
    public String removeSiteProperty(String sessionid, String siteid, String propname) throws java.rmi.RemoteException;
    public boolean checkForRoleInAuthzGroup(String sessionid, String authzgroupid, String roleid) throws java.rmi.RemoteException;
    public String searchForUsers(String sessionid, String criteria, int first, int last) throws java.rmi.RemoteException;
    public boolean checkForAuthzGroup(String sessionid, String authzgroupid) throws java.rmi.RemoteException;
    public String removeMemberFromSite(String sessionid, String siteid, String eid) throws java.rmi.RemoteException;
    public boolean checkForUserInAuthzGroup(String sessionid, String authzgroupid, String eid) throws java.rmi.RemoteException;
    public String getUsersInAuthzGroupWithRole(String sessionid, String authzgroupid, String authzgrouproles) throws java.rmi.RemoteException;
    public String getUsersInAuthzGroup(String sessionid, String authzgroupid) throws java.rmi.RemoteException;
    public String copyCalendarEvents(String sessionid, String sourceSiteId, String targetSiteId) throws java.rmi.RemoteException;
    public String addNewToolToAllWorkspaces(String sessionid, String toolid, String pagetitle, String tooltitle, int pagelayout, int position, boolean popup) throws java.rmi.RemoteException;
    public String copyRole(String sessionid, String authzgroupid1, String authzgroupid2, String roleid, String description, boolean removeBeforeSync) throws java.rmi.RemoteException;
    public String copyRole(String sessionid, String authzgroupid1, String authzgroupid2, String roleid, String description) throws java.rmi.RemoteException;
    public String getAllUsers(String sessionid) throws java.rmi.RemoteException;
    public String getSessionForUser(String sessionid, String eid, boolean wsonly) throws java.rmi.RemoteException;
    public String getSessionForUser(String sessionid, String eid) throws java.rmi.RemoteException;
    public String getPagesAndToolsForSite(String sessionid, String siteid) throws java.rmi.RemoteException;
    public String getPagesAndToolsForSite(String sessionid, String userid, String siteid) throws java.rmi.RemoteException;
    public String copyResources(String sessionid, String sourcesiteid, String destinationsiteid) throws java.rmi.RemoteException;
    public String getSiteUsers(String sessionid, String siteid) throws java.rmi.RemoteException;
    public String setSitePropertyUni(String sessionid, String siteid, String title, String description, String shortdesc, String disciplineId, String disciplinePart, String siteType, String CActionType) throws java.rmi.RemoteException;
    public String getAllSitesUni(String sessionid) throws java.rmi.RemoteException;
    public String getAllUsersUni(String sessionid) throws java.rmi.RemoteException;
    public String getMarkSiteStudent(String sessionid, String siteid) throws java.rmi.RemoteException;
    public String check(String string) throws java.rmi.RemoteException;
    public String syncMySite(String sessionid, String userId) throws java.rmi.RemoteException;
    public String syncMySiteByEid(String sessionid, String eid) throws java.rmi.RemoteException;
    public String syncSites(String sessionid, String siteId, String masterSiteId) throws java.rmi.RemoteException;
}
