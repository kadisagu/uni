package ru.tandemservice.unisakai.component.sakai.imp.UgstImp;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisakai.component.sakai.imp.BaseImp.WrapperImp;
import ru.tandemservice.unisakai.dao.SakaiDaoFacade;
import ru.tandemservice.unisakai.logic.SiteSakai;

import java.util.ArrayList;
import java.util.List;

public class Controller extends ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Controller
{

    @Override
    public boolean needLoadSakaiSites() {
        return true;
    }


    @Override
    public boolean needLoadSakaiUsers() {
        return false;
    }


    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        // сначала активацим старое
        super.onRefreshComponent(component);
    }

    @Override
    public void MakeDataColumn(DynamicListDataSource<WrapperImp> dataSource)
    {
        dataSource.addColumn(new CheckboxColumn("select"));
        //dataSource.addColumn(new SimpleColumn("Сущность", "entityType").setClickable(false));
        dataSource.addColumn(new ActionColumn("Сущность", "", "onClickEntity").setTextKey("entityType").setDisplayHeader(true));
        dataSource.addColumn(new SimpleColumn("Имя", "name").setClickable(true));
        dataSource.addColumn(new SimpleColumn("Описание", "description").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Статус в Sakai", "sakaiInfo").setClickable(false));
    }

    @Override
    public void onPostProcess()
    {

    }

    /**
     * Обработка одной строки сущности
     */
    @Override
    public void ProcessRow(WrapperImp row, ru.tandemservice.unisakai.component.sakai.imp.BaseImp.Model model) throws Exception
    {

    }


    private void _clearCache(IBusinessComponent context)
    {
        Model model = context.getModel();
        model.setFilterKey("");
    }

    public void onClickEntity(final IBusinessComponent context)
    {
        Long id = context.getListenerParameter();

        EppRegistryElementPartFControlAction part = SakaiDaoFacade.getSakaiDao().getEppRegistryElementPartFControlAction(id);

        if (part != null) {
            _clearCache(context);

            // можно открыть публикатор
            context.createChildRegion("listRegion", new ComponentActivator(
                    ru.tandemservice.unisakai.component.sakai.imp.UgstPub.Model.class.getPackage().getName(),
                    new ParametersMap().add("idEppRegistryElementPartFControlAction", id)));
        }
    }


    public void onClick(final IBusinessComponent context)
    {
        _clearCache(context);

        List<Long> ids = getSelectedIdsEppRegistryElementPart(context);

        ParametersMap map = new ParametersMap();
        map.add("ids", ids);

        Model model = context.getModel();

        if (model.getYear() != null)
            map.add("idYear", model.getYear().getId());

        if (model.getPart() != null)
            map.add("idPart", model.getPart().getId());

        // нужно передать список технологий и форм освоения
        // год и часть года

        IDataSettings settingKeyOther = UniBaseUtils.getDataSettings(context, ru.tandemservice.unisakai.component.sakai.imp.UgstMassImp.Controller.KEY);
        // технология и форма освоения через настройки
        settingKeyOther.set("developTechList", model.getSettings().get("developTechList"));
        settingKeyOther.set("developFormList", model.getSettings().get("developFormList"));

        DataSettingsFacade.saveSettings(settingKeyOther);

        context.createChildRegion("listRegion", new ComponentActivator(
                ru.tandemservice.unisakai.component.sakai.imp.UgstMassImp.Model.class.getPackage().getName(), map));
    }

    public void onClickDelete(final IBusinessComponent context)
    {
        _clearCache(context);

        List<Long> ids = getSelectedIdsEppRegistryElementPart(context);

        ParametersMap map = new ParametersMap();
        map.add("ids", ids);

        Model model = context.getModel();

        if (model.getYear() != null)
            map.add("idYear", model.getYear().getId());

        if (model.getPart() != null)
            map.add("idPart", model.getPart().getId());

        // нужно передать список технологий и форм освоения
        // год и часть года

        IDataSettings settingKeyOther = UniBaseUtils.getDataSettings(context, ru.tandemservice.unisakai.component.sakai.imp.UgstMassDel.Controller.KEY);

        // технология и форма освоения через настройки
        settingKeyOther.set("developTechList", model.getSettings().get("developTechList"));
        settingKeyOther.set("developFormList", model.getSettings().get("developFormList"));

        DataSettingsFacade.saveSettings(settingKeyOther);


        context.createChildRegion("listRegion", new ComponentActivator(
                ru.tandemservice.unisakai.component.sakai.imp.UgstMassDel.Model.class.getPackage().getName(), map));
    }

    public void onClickDeleteSite(final IBusinessComponent context)
    {
        _clearCache(context);

        List<SiteSakai> sites = getSelectedSakaiSite(context);
        if (sites.size() > 0) {

            List<String> idSites = new ArrayList<String>();
            for (SiteSakai site : sites) {
                idSites.add(site.getSiteid());
            }

            ParametersMap map = new ParametersMap();
            map.add("idsSite", idSites);

            context.createChildRegion("listRegion", new ComponentActivator(
                    ru.tandemservice.unisakai.component.sakai.imp.AgsMassSiteDel.Model.class.getPackage().getName(), map));
        }
    }

    @Override
    public String getSettingsKey()
    {
        return "sakai.imp.UgstImp";
    }
}
