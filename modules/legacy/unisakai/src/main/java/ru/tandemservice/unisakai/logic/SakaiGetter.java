package ru.tandemservice.unisakai.logic;


import ru.tandemservice.unisakai.util.sprav.KeyValues;
import ru.tandemservice.unisakai.ws.sakai.*;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;
import java.util.List;

public class SakaiGetter {

    private static String _keySakaiScriptService = "";
    private static String _keySakaiLoginService = "";

    private static SakaiScriptService _sakaiScriptService = null;
    private static SakaiLoginService _sakaiLoginService = null;


    public static SakaiScriptService getScritpService() throws Exception
    {

        String path = KeyValues.getStringValue("unisakai.server", true);
        String key = path + "/sakai-axis/SakaiScript.jws?wsdl";

        if (key.equals(_keySakaiScriptService))
            return _sakaiScriptService;
        else
        {
            SakaiScriptServiceLocator locator = new SakaiScriptServiceLocator();

            try
            {
                HandlerRegistry registry = locator.getHandlerRegistry();
                QName servicePort = new QName(path, "SakayScriptServicePort");
                List handlerChain = registry.getHandlerChain(servicePort);
                HandlerInfo info = new HandlerInfo();
                info.setHandlerClass(SecurityHandler.class);
                handlerChain.add(info);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            _keySakaiScriptService = key;
            _sakaiScriptService = locator;
            return _sakaiScriptService;
        }

    }


    public static SakaiLoginService getLoginService() throws Exception
    {
        String path = KeyValues.getStringValue("unisakai.server", true);
        String key = path + "/sakai-axis/SakaiLogin.jws?wsdl";

        if (key.equals(_keySakaiLoginService))
            return _sakaiLoginService;
        else
        {
            SakaiLoginServiceLocator locator = new SakaiLoginServiceLocator();

            try
            {
                HandlerRegistry registry = locator.getHandlerRegistry();
                QName servicePort = new QName(path, "SakayLoginServicePort");
                List handlerChain = registry.getHandlerChain(servicePort);
                HandlerInfo info = new HandlerInfo();
                info.setHandlerClass(SecurityHandler.class);
                handlerChain.add(info);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            _keySakaiLoginService = key;
            _sakaiLoginService = locator;
            return _sakaiLoginService;
        }
    }

    public static void ClearSakaiWSCache()
    {
        _keySakaiLoginService = "";
        _keySakaiScriptService = "";
    }


}
