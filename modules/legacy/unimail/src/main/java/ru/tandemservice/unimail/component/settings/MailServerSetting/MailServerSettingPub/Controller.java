package ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {

        final Model model = component.getModel();

        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<MailServerSetting> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", MailServerSetting.title()).setClickable(true).setOrderable(true));
        dataSource.addColumn((new ToggleColumn("По умолчанию", MailServerSetting.useDefault()).toggleOnListener("onToggleOnUseDefault").toggleOffListener("onToggleOffUseDefault")).setDisabledProperty("useDefault"));
        dataSource.addColumn((new ToggleColumn("Использовать", MailServerSetting.enabled()).toggleOnListener("onToggleOnEnabled").toggleOffListener("onToggleOffEnabled")));
        dataSource.addColumn((new SimpleColumn("Ссылка на сайт", MailServerSetting.mailSiteLink()).setClickable(false).setOrderable(false)));
        dataSource.addColumn((new SimpleColumn("Хост", MailServerSetting.host()).setClickable(false).setOrderable(false)));
        dataSource.addColumn((new SimpleColumn("Адрес почты", MailServerSetting.mailAddress()).setClickable(false).setOrderable(false)));
        dataSource.addColumn((new SimpleColumn("Имя почтового ящика (для авторизации)", MailServerSetting.username()).setClickable(false).setOrderable(false)));
        dataSource.addColumn((new SimpleColumn("Пароль почтового ящика (для авторизации)", MailServerSetting.password()).setClickable(false).setOrderable(false)));
        dataSource.addColumn((new ToggleColumn("Отправка писем в режиме отладки", MailServerSetting.useDebugMode()).toggleOnListener("onToggleOnUseDebugMode").toggleOffListener("onToggleOffUseDebugMode")));
        dataSource.addColumn((new SimpleColumn("Список адресатов (для режима отладки)", MailServerSetting.debugList()).setClickable(false).setOrderable(false)));
        dataSource.addColumn((new SimpleColumn("Порт", MailServerSetting.port()).setClickable(false).setOrderable(false)));
        dataSource.addColumn((new ToggleColumn("Использовать защищённые протоколы (SSL/TLS)", MailServerSetting.useSSL()).toggleOnListener("onToggleOnUseSSL").toggleOffListener("onToggleOffUseSSL")));
        dataSource.addColumn((new SimpleColumn("Код", MailServerSetting.code()).setClickable(false).setOrderable(false)));
        dataSource.addColumn((new SimpleColumn("Пользовательский код", MailServerSetting.userCode()).setClickable(false).setOrderable(false)));

        dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEdit").setPermissionKey("mailServerSettingsEdit"));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete", "Вы действительно хотите удалить настройку почтового сервера «{0}» вместе с сообщениями, отправленными с данного почтового сервера и логами отправки?",
                                              new Object[]{MailServerSetting.title()}).setPermissionKey("mailServerSettingsDelete").setDisabledProperty("useDefault"));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = component.getModel();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = component.getModel();
        model.getSettings().clear();
        getDao().prepare(model);
    }

    public void onClickEdit(IBusinessComponent component) {

        ParametersMap paramMap = new ParametersMap().add("mailServerSettingId",
                                           component.getListenerParameter());

        activateInRoot(component,
                       new ComponentActivator(ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingAddEdit.Controller.class.getPackage().getName(), paramMap));
    }

    public void onClickAddMailSetting(IBusinessComponent component) {
        activateInRoot(component,
                       new ComponentActivator(ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingAddEdit.Controller.class.getPackage().getName()));
    }

    public void onClickDelete(IBusinessComponent component) {
        Model model = component.getModel();
        MailServerSetting mailServerSetting = model.getDataSource().getRecordById((Long) component.getListenerParameter());
        getDao().deleteMailServerSetting(mailServerSetting);
    }

    public void onToggleOnUseDefault(IBusinessComponent component) {
        changeSetting(component, MailServerSetting.P_USE_DEFAULT, Boolean.TRUE);
    }

    public void onToggleOffUseDefault(IBusinessComponent component) {
        changeSetting(component, MailServerSetting.P_USE_DEFAULT, Boolean.FALSE);
    }

    public void onToggleOnEnabled(IBusinessComponent component) {
        changeSetting(component, MailServerSetting.P_ENABLED, Boolean.TRUE);
    }

    public void onToggleOffEnabled(IBusinessComponent component) {
        changeSetting(component, MailServerSetting.P_ENABLED, Boolean.FALSE);
    }

    public void onToggleOnUseDebugMode(IBusinessComponent component) {
        changeSetting(component, MailServerSetting.P_USE_DEBUG_MODE, Boolean.TRUE);
    }

    public void onToggleOffUseDebugMode(IBusinessComponent component) {
        changeSetting(component, MailServerSetting.P_USE_DEBUG_MODE, Boolean.FALSE);
    }

    public void onToggleOnUseSSL(IBusinessComponent component) {
        changeSetting(component, MailServerSetting.P_USE_S_S_L, Boolean.TRUE);
    }

    public void onToggleOffUseSSL(IBusinessComponent component) {
        changeSetting(component, MailServerSetting.P_USE_S_S_L, Boolean.FALSE);
    }

    private void changeSetting(IBusinessComponent component, String field, Boolean value) {
        Model model = component.getModel();
        MailServerSetting mailServerSetting = model.getDataSource().getRecordById((Long) component.getListenerParameter());

        getDao().changeSetting(mailServerSetting, field, value);
    }

}
