package ru.tandemservice.unimail.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Шаблоны электронных писем"
 * Имя сущности : mailTemplate
 * Файл data.xml : unimail.data.xml
 */
public interface MailTemplateCodes
{
    /** Константа кода (code) элемента : Тестовый шаблон (title) */
    String CELEBRATING = "1";
    /** Константа кода (code) элемента : Изменение РП (title) */
    String S2WP = "s2wp";
    /** Константа кода (code) элемента : Подписка на ДПВ из личного кабинета (title) */
    String DPV_SAKAI = "dpvSakai";
    /** Константа кода (code) элемента : Подписка на ДПВ преподавателем (title) */
    String DPV_TUTOR = "dpvTutor";

    Set<String> CODES = ImmutableSet.of(CELEBRATING, S2WP, DPV_SAKAI, DPV_TUTOR);
}
