package ru.tandemservice.unimail.component.catalog.mailTemplate.MailTemplateAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unimail.entity.catalog.MailTemplate;


public class Model extends DefaultCatalogAddEditModel<MailTemplate> {
    private ISelectModel mailContentTypeList;
    private ISelectModel messageTypeList;

    public ISelectModel getMailContentTypeList() {
        return mailContentTypeList;
    }

    public void setMailContentTypeList(ISelectModel mailContentTypeList) {
        this.mailContentTypeList = mailContentTypeList;
    }

    public ISelectModel getMessageTypeList() {
        return messageTypeList;
    }

    public void setMessageTypeList(ISelectModel messageTypeList) {
        this.messageTypeList = messageTypeList;
    }
}
