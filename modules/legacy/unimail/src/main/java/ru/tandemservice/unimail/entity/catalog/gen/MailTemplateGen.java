package ru.tandemservice.unimail.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unimail.entity.catalog.MailContentType;
import ru.tandemservice.unimail.entity.catalog.MailTemplate;
import ru.tandemservice.unimail.entity.catalog.MessageType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шаблоны электронных писем
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MailTemplateGen extends EntityBase
 implements INaturalIdentifiable<MailTemplateGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimail.entity.catalog.MailTemplate";
    public static final String ENTITY_NAME = "mailTemplate";
    public static final int VERSION_HASH = -1332803388;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_MAIL_CONTENT_TYPE = "mailContentType";
    public static final String L_MESSAGE_TYPE = "messageType";
    public static final String P_STRING_VALUE = "stringValue";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private MailContentType _mailContentType;     // Тип содержимого письма
    private MessageType _messageType;     // Тип сообщения
    private String _stringValue;     // Текст шаблона
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Тип содержимого письма. Свойство не может быть null.
     */
    @NotNull
    public MailContentType getMailContentType()
    {
        return _mailContentType;
    }

    /**
     * @param mailContentType Тип содержимого письма. Свойство не может быть null.
     */
    public void setMailContentType(MailContentType mailContentType)
    {
        dirty(_mailContentType, mailContentType);
        _mailContentType = mailContentType;
    }

    /**
     * @return Тип сообщения. Свойство не может быть null.
     */
    @NotNull
    public MessageType getMessageType()
    {
        return _messageType;
    }

    /**
     * @param messageType Тип сообщения. Свойство не может быть null.
     */
    public void setMessageType(MessageType messageType)
    {
        dirty(_messageType, messageType);
        _messageType = messageType;
    }

    /**
     * @return Текст шаблона. Свойство не может быть null.
     */
    @NotNull
    public String getStringValue()
    {
        return _stringValue;
    }

    /**
     * @param stringValue Текст шаблона. Свойство не может быть null.
     */
    public void setStringValue(String stringValue)
    {
        dirty(_stringValue, stringValue);
        _stringValue = stringValue;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MailTemplateGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((MailTemplate)another).getCode());
            }
            setMailContentType(((MailTemplate)another).getMailContentType());
            setMessageType(((MailTemplate)another).getMessageType());
            setStringValue(((MailTemplate)another).getStringValue());
            setTitle(((MailTemplate)another).getTitle());
        }
    }

    public INaturalId<MailTemplateGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<MailTemplateGen>
    {
        private static final String PROXY_NAME = "MailTemplateNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof MailTemplateGen.NaturalId) ) return false;

            MailTemplateGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MailTemplateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MailTemplate.class;
        }

        public T newInstance()
        {
            return (T) new MailTemplate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "mailContentType":
                    return obj.getMailContentType();
                case "messageType":
                    return obj.getMessageType();
                case "stringValue":
                    return obj.getStringValue();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "mailContentType":
                    obj.setMailContentType((MailContentType) value);
                    return;
                case "messageType":
                    obj.setMessageType((MessageType) value);
                    return;
                case "stringValue":
                    obj.setStringValue((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "mailContentType":
                        return true;
                case "messageType":
                        return true;
                case "stringValue":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "mailContentType":
                    return true;
                case "messageType":
                    return true;
                case "stringValue":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "mailContentType":
                    return MailContentType.class;
                case "messageType":
                    return MessageType.class;
                case "stringValue":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MailTemplate> _dslPath = new Path<MailTemplate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MailTemplate");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Тип содержимого письма. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getMailContentType()
     */
    public static MailContentType.Path<MailContentType> mailContentType()
    {
        return _dslPath.mailContentType();
    }

    /**
     * @return Тип сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getMessageType()
     */
    public static MessageType.Path<MessageType> messageType()
    {
        return _dslPath.messageType();
    }

    /**
     * @return Текст шаблона. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getStringValue()
     */
    public static PropertyPath<String> stringValue()
    {
        return _dslPath.stringValue();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends MailTemplate> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private MailContentType.Path<MailContentType> _mailContentType;
        private MessageType.Path<MessageType> _messageType;
        private PropertyPath<String> _stringValue;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(MailTemplateGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Тип содержимого письма. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getMailContentType()
     */
        public MailContentType.Path<MailContentType> mailContentType()
        {
            if(_mailContentType == null )
                _mailContentType = new MailContentType.Path<MailContentType>(L_MAIL_CONTENT_TYPE, this);
            return _mailContentType;
        }

    /**
     * @return Тип сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getMessageType()
     */
        public MessageType.Path<MessageType> messageType()
        {
            if(_messageType == null )
                _messageType = new MessageType.Path<MessageType>(L_MESSAGE_TYPE, this);
            return _messageType;
        }

    /**
     * @return Текст шаблона. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getStringValue()
     */
        public PropertyPath<String> stringValue()
        {
            if(_stringValue == null )
                _stringValue = new PropertyPath<String>(MailTemplateGen.P_STRING_VALUE, this);
            return _stringValue;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.unimail.entity.catalog.MailTemplate#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MailTemplateGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return MailTemplate.class;
        }

        public String getEntityName()
        {
            return "mailTemplate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
