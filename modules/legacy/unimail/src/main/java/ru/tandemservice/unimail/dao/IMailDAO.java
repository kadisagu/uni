package ru.tandemservice.unimail.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unimail.entity.MailMessage;
import ru.tandemservice.unimail.entity.catalog.MessageType;
import ru.tandemservice.unimail.entity.catalog.MailContentType;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;


public interface IMailDAO {

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void doMailSend() throws Exception;

    /**
     * @param person          Персона (не обязательно, может быть null)
     * @param mailTo          (не обязательно, может быть null)
     * @param subject
     * @param text
     * @param messageType     (тип сообщения, notNull)
     * @param mailContentType (Content-type, notNull)
     * @param mailSetting     (настройки почтового сервера, notNull)
     */
    public MailMessage addNewMail(Person person, String mailTo, String subject,
                              String text, MessageType messageType,
                                  MailContentType mailContentType, MailServerSetting mailSetting);

    /**
     * сокращенная сигнатура mailSetting - почтовый сервер по умолчанию
     * mailContentType = text/html
     *
     * @param person
     * @param subject
     * @param text
     * @param messageType
     */
    public MailMessage addNewMail(Person person, String subject, String text,MessageType messageType);

    public MailMessage addNewMail(Person person, String subject, String text, MessageType messageType, MailContentType mailContentType);

    /**
     * Используемая настройка сервера для рассылки по умолчанию
     */
    public MailServerSetting getDefaultSetting();
}
