package ru.tandemservice.unimail.dao;

import org.apache.commons.lang.StringUtils;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimail.entity.MailMessage;
import ru.tandemservice.unimail.entity.MailLog;
import ru.tandemservice.unimail.entity.catalog.MailState;
import ru.tandemservice.unimail.entity.catalog.MessageType;
import ru.tandemservice.unimail.entity.catalog.MailContentType;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;
import ru.tandemservice.unimail.entity.catalog.MailState;
import ru.tandemservice.unimail.entity.catalog.codes.MailContentTypeCodes;
import ru.tandemservice.unimail.entity.catalog.codes.MailStateCodes;
import ru.tandemservice.unimail.entity.catalog.codes.MailStateCodes;
import ru.tandemservice.unimail.util.AppProperty;
import ru.tandemservice.unimail.validators.MailValidator;

import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;


public class MailDAO extends UniBaseDao implements IMailDAO {

    private static volatile boolean isRun = false;

    public static final SyncDaemon mailDaemon = new SyncDaemon(MailDAO.class.getName(), 10) {

        @Override
        protected void main() {

            if (!AppProperty.RUN_DEMON())
                return;

            if (!isRun) {
                try {
                    isRun = true;
                    MailDaoFacade.getMailDao().doMailSend();
                }
                catch (Exception e) {
                    this.logger.error(e.getMessage(), e);
                    Debug.exception(e.getMessage(), e);
                }
                finally {
                    isRun = false;
                }
            }
        }

    };

    @Override
    public void doMailSend() throws Exception {

        List<MailMessage> lstMails = getMailsToSend();

        for (MailMessage mail : lstMails) {
            // проверяем включена ли настройка отправки
            if (!mail.getMailSetting().getEnabled()) {
                // почту не отправлять
                // нужно поставить признак - игнорировать
                setMailState(mail, MailStateCodes.IGNORE, "Отправки почтовых уведомлений отключена в настройках", false);
                continue;
            }

            // осуществляем проверку настроек и рассылку
            if (checkBeforeSend(mail))
                send(mail);
        }

    }

    private void send(MailMessage mailMessage) throws Exception {

        JavaMailSender sender = MailDaoFacade.getMailConfigurator().getPreparedMailSender(mailMessage.getMailSetting());

        Date sendDate = Calendar.getInstance().getTime();
        MimeMessage message = getMessage(sender, mailMessage);
        boolean noError = true;

        try {
            mailMessage.setSendDate(sendDate);
            sender.send(message);
            mailMessage.setMailState(getCatalogItem(MailState.class, MailStateCodes.IS_SENDED));
        }
        catch (MailException e) {
            noError = false;
            mailSendError(mailMessage, e);
        }

        if (noError) {
            getSession().saveOrUpdate(mailMessage);
            MailLog mailLog = new MailLog(mailMessage, sendDate, null, noError);
            getSession().saveOrUpdate(mailLog);
        }
    }

    private MimeMessage getMessage(JavaMailSender sender, MailMessage mail) throws Exception {

        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setTo(mail.getRecipient());

        // обязательно нужно указать, от кого почта
        String mailFrom = mail.getMailSetting().getMailAddress();
        helper.setFrom(mailFrom);

        if (mail.getContentType().getCode().equals(MailContentTypeCodes.HTML_TEXT))
            helper.setText(mail.getText(), true);
        else
            helper.setText(mail.getText());

        message.setSubject(mail.getSubject());

        return message;
    }

    private boolean checkBeforeSend(MailMessage mail) throws Exception {

        boolean canUse = false;
        boolean correctAddress = false;
        boolean correctPersonAddress = false;


        MailValidator mailValidator = new MailValidator();
        if (mailValidator.validate(mail.getRecipient())) {
            correctAddress = true;
            correctPersonAddress = true;
        }
        else {
            // проверяем отдельно корректность адреса на персоне
            if (mail.getPerson() != null && mail.getPerson().getContactData() != null) {
                correctPersonAddress = mailValidator.validate(mail.getPerson().getContactData().getEmail());
            }
            else
                correctPersonAddress = true;
        }


        if (mail.getMailSetting().getUseDebugMode()) {

            // адреса из списка настройки
            String debugAdressesList = mail.getMailSetting().getDebugList();
            if (!StringUtils.isEmpty(debugAdressesList)) {
                String[] addrs = debugAdressesList.split(";");
                for (String addr : addrs) {
                    if (addr.length() > 1) {
                        if (!StringUtils.isEmpty(mail.getRecipient())
                                && mail.getRecipient().toLowerCase().contains(addr.toLowerCase()))
                            canUse = true;
                    }
                }
            }

            if (!canUse) {

                String msg = "В настройках системы включен режим 'app.mail.debugmode' = true, в таком режиме сообщения отправляются только на адреса электронной почты из списка 'app.mail.debuglist'";
                String stateCode = MailStateCodes.IGNORE;
                if (!correctAddress) {
                    msg = "E-mail адресата некорректен";
                    stateCode = MailStateCodes.INCORRECT;
                    if (!correctPersonAddress) {
                        msg = "E-mail адрес у персоны указан некорректно";
                        stateCode = MailStateCodes.INCORRECT_PERSON_EMAIL;
                    }
                }
                setMailState(mail, stateCode, msg, false);
            }
        }
        else {
            if (correctAddress)
                canUse = true;
            else {
                String stateCode = MailStateCodes.INCORRECT;
                String msg = "E-mail адресата некорректен";
                if (!correctPersonAddress) {
                    msg = "E-mail адрес у персоны указан некорректно";
                    stateCode = MailStateCodes.INCORRECT_PERSON_EMAIL;
                }

                setMailState(mail, stateCode, msg, false);
                canUse = false;
            }
        }

        return  (correctAddress && canUse);
    }

    private void setMailState(MailMessage mail, String mailStateCode, String logMsg, boolean noError)
    {
        mail.setMailState(getCatalogItem(MailState.class, mailStateCode));

        MailLog mailLog = new MailLog(mail, Calendar.getInstance().getTime(), logMsg, noError);
        getSession().saveOrUpdate(mail);
        getSession().saveOrUpdate(mailLog);

    }

    private void mailSendError(MailMessage mail, Exception e)
    {
        setMailState(mail, MailStateCodes.ERROR_SEND, e.getMessage(), false);
    }

    /**
     * Список сообщений для отправки (только CREATED)
     */
    private List<MailMessage> getMailsToSend() {

        List<String> stateCodes = new ArrayList<>();

        stateCodes.add(MailStateCodes.CREATED);
        stateCodes.add(MailStateCodes.ERROR_SEND);

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(MailMessage.class, "mail")
                .where(in(property(MailMessage.mailState().code().fromAlias("mail")), stateCodes))
                .column("mail");

        return getList(dql);
    }

    @Override
    public MailMessage addNewMail(Person person, String mailTo, String subject,
                              String text, MessageType messageType,
                              MailContentType mailContentType, MailServerSetting mailSetting)
    {

        MailState mailState = getCatalogItem(MailState.class, MailStateCodes.CREATED);
        Date createDate = Calendar.getInstance().getTime();
        MailMessage mail = new MailMessage(person, mailTo, subject, text, messageType, mailContentType, mailSetting);
        mail.setCreateDate(createDate);
        mail.setMailState(mailState);
        getSession().save(mail);

        return mail;
    }

    @Override
    public MailMessage addNewMail(Person person, String subject, String text,
                                  MessageType messageType)
    {

        MailContentType contentType = getCatalogItem(MailContentType.class, MailContentTypeCodes.HTML_TEXT);
        return addNewMail(person, subject, text, messageType, contentType);
    }

    public MailMessage addNewMail(Person person, String subject, String text, MessageType messageType, MailContentType mailContentType)
    {
        MailState mailState = getCatalogItem(MailState.class, MailStateCodes.CREATED);
        Date createDate = Calendar.getInstance().getTime();

        MailMessage mail = new MailMessage(person, subject, text, messageType);
        mail.setMailState(mailState);
        mail.setCreateDate(createDate);
        mail.setMailSetting(getDefaultSetting());
        mail.setContentType(mailContentType);
        getSession().save(mail);

        return mail;
    }

    @Override
    public MailServerSetting getDefaultSetting() {
        return MailDaoFacade.getMailConfigurator().getDefaultSetting();
    }
}
