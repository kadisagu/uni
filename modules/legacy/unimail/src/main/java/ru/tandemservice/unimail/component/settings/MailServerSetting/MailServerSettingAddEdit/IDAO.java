package ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    void saveOrUpdate(Model model);
}
