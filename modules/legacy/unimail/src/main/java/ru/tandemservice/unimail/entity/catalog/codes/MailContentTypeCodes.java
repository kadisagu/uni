package ru.tandemservice.unimail.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Используемые типы электронных писем"
 * Имя сущности : mailContentType
 * Файл data.xml : unimail.data.xml
 */
public interface MailContentTypeCodes
{
    /** Константа кода (code) элемента : text/plain (title) */
    String PLAIN_TEXT = "01";
    /** Константа кода (code) элемента : text/html (title) */
    String HTML_TEXT = "02";

    Set<String> CODES = ImmutableSet.of(PLAIN_TEXT, HTML_TEXT);
}
