package ru.tandemservice.unimail.component.mail.MailList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unimail.entity.MailMessage;


public class Model {

    private IDataSettings settings;
    private DynamicListDataSource<MailMessage> dataSource;
    private ISelectModel mailStateListModel;
    private ISelectModel messageTypeListModel;
    private ISelectModel mailSettingSelectModel;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<MailMessage> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<MailMessage> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getMailStateListModel() {
        return mailStateListModel;
    }

    public void setMailStateListModel(ISelectModel mailStateListModel) {
        this.mailStateListModel = mailStateListModel;
    }

    public ISelectModel getMessageTypeListModel() {
        return messageTypeListModel;
    }

    public void setMessageTypeListModel(ISelectModel messageTypeListModel) {
        this.messageTypeListModel = messageTypeListModel;
    }

    public ISelectModel getMailSettingSelectModel() {
        return mailSettingSelectModel;
    }

    public void setMailSettingSelectModel(ISelectModel mailSettingSelectModel) {
        this.mailSettingSelectModel = mailSettingSelectModel;
    }
}