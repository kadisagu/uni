package ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingAddEdit;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;


public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        if (model.getMailServerSettingId() != null)
            model.setMailServerSetting(getNotNull(MailServerSetting.class, model.getMailServerSettingId()));
        else model.setMailServerSetting(new MailServerSetting());
    }

    @Override
    public void saveOrUpdate(Model model) {
        if (StringUtils.isEmpty(model.getMailServerSetting().getCode()))
            model.getMailServerSetting().setCode(String.valueOf(System.nanoTime()));
        saveOrUpdate(model.getMailServerSetting());
    }
}
