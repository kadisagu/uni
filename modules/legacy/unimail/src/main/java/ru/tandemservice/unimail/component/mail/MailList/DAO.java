package ru.tandemservice.unimail.component.mail.MailList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimail.entity.MailMessage;
import ru.tandemservice.unimail.entity.catalog.MessageType;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;
import ru.tandemservice.unimail.entity.catalog.MailState;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

public class DAO extends UniDao<Model> implements IDAO {

    private static final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(MailMessage.class, "mail");

    @Override
    public void prepare(Model model) {
        model.setMailStateListModel(getMailStateSelectableModel(model));
        model.setMessageTypeListModel(getMessageTypeSelectableModel(model));
        model.setMailSettingSelectModel(getMailSettingSelectableModel(model));
        if (model.getSettings().get("mailSetting") == null) {
            model.getSettings().set("mailSetting", getNotNull(MailServerSetting.class, MailServerSetting.useDefault(), Boolean.TRUE));
        }
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DQLSelectBuilder dql = getDataSourceBuilder(model);
        order.applyOrder(dql, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());
		
    }


    @SuppressWarnings("unchecked")
    private DQLSelectBuilder getDataSourceBuilder(Model model) {

        IDataSettings settings = model.getSettings();

        Date fromDate = settings.get("fromDate");
        Date toDate = settings.get("toDate");
        Object mailSetting = settings.get("mailSetting");
        Object messageType = settings.get("messageType");
        Object mailState = settings.get("mailState");
        Object email = settings.get("email");
        Object personFio = settings.get("fio");

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(MailMessage.class, "mail").column("mail");


        if (mailSetting != null)
            builder.where(eq(
                    property(MailMessage.mailSetting().id().fromAlias("mail")),
                    value(((MailServerSetting) mailSetting).getId())));

        if (fromDate != null) {
            builder.where(DQLExpressions.ge(
                    property(MailMessage.createDate().fromAlias("mail")),
                    DQLExpressions.valueDate(fromDate)
            ));
        }

        if (toDate != null) {
            builder.where(DQLExpressions.le(
                    property(MailMessage.createDate().fromAlias("mail")),
                    DQLExpressions.valueDate(toDate)
            ));
        }

        if (messageType != null && !((List<MessageType>) messageType).isEmpty()) {
            builder.where(DQLExpressions.in(
                    property(MailMessage.messageType().id().fromAlias("mail")),
                    CommonDAO.ids((List<MessageType>) messageType)));
        }

        if (mailState != null && !((List<MailState>) mailState).isEmpty()) {
            builder.where(DQLExpressions.in(
                    property(MailMessage.mailState().id().fromAlias("mail")),
                    CommonDAO.ids((List<MailState>) mailState)));
        }

        if (email != null && !StringUtils.isEmpty((String) email)) {
            FilterUtils.applyLikeFilter(builder, (String) email, MailMessage.mailTo().fromAlias("mail"),
                    MailMessage.person().contactData().email().fromAlias("mail"));
        }

        if (personFio != null && !StringUtils.isEmpty((String) personFio))
            FilterUtils.applyLikeFilter(builder, (String) personFio, MailMessage.person().identityCard().fullFio().fromAlias("mail"));

        return builder;
    }

    private DQLFullCheckSelectModel getMailStateSelectableModel(final Model model) {

        return new DQLFullCheckSelectModel("title") {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(MailState.class, alias)
                        .order(property(MailState.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(dql, alias, MailState.title(), filter);

                return dql;
            }
        };
    }

    private DQLFullCheckSelectModel getMessageTypeSelectableModel(final Model model) {

        return new DQLFullCheckSelectModel("title") {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(MessageType.class, alias)
                        .order(property(MessageType.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(dql, alias, MessageType.title(), filter);

                return dql;
            }
        };
    }

    private DQLFullCheckSelectModel getMailSettingSelectableModel(final Model model) {

        return new DQLFullCheckSelectModel("title") {

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                return new DQLSelectBuilder()
                        .fromEntity(MailServerSetting.class, alias)
                        .order(property(MailServerSetting.title().fromAlias(alias)));
            }
        };
    }


    @Override
    public void deleteMailMessage(MailMessage message) {
        new DQLDeleteBuilder(MailMessage.class)
                .where(eq(property(MailMessage.id()), value(message.getId())
                )).createStatement(getSession()).execute();
    }

    @Override
    public void updateMailStates(Model model, Collection<IEntity> entityList, Long mailStateId) {

        MailState mailState = getNotNull(MailState.class, mailStateId);

        for (IEntity entity : entityList) {
            MailMessage message = (MailMessage) entity;
            message.setMailState(mailState);
            saveOrUpdate(message);
        }

    }

}
