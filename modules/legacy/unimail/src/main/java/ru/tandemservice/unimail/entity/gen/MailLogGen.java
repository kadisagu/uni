package ru.tandemservice.unimail.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unimail.entity.MailLog;
import ru.tandemservice.unimail.entity.MailMessage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Логирование электронных сообщений
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MailLogGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimail.entity.MailLog";
    public static final String ENTITY_NAME = "mailLog";
    public static final int VERSION_HASH = 1323942296;
    private static IEntityMeta ENTITY_META;

    public static final String L_MAIL_MESSAGE = "mailMessage";
    public static final String P_LOG_DATE = "logDate";
    public static final String P_ERROR = "error";
    public static final String P_NO_ERROR = "noError";

    private MailMessage _mailMessage;     // Сообщение, отправляемое по электронной почте
    private Date _logDate;     // Дата/время записи
    private String _error;     // Описание ошибки
    private Boolean _noError;     // Отметка о том, что все прошло успешно

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сообщение, отправляемое по электронной почте. Свойство не может быть null.
     */
    @NotNull
    public MailMessage getMailMessage()
    {
        return _mailMessage;
    }

    /**
     * @param mailMessage Сообщение, отправляемое по электронной почте. Свойство не может быть null.
     */
    public void setMailMessage(MailMessage mailMessage)
    {
        dirty(_mailMessage, mailMessage);
        _mailMessage = mailMessage;
    }

    /**
     * @return Дата/время записи. Свойство не может быть null.
     */
    @NotNull
    public Date getLogDate()
    {
        return _logDate;
    }

    /**
     * @param logDate Дата/время записи. Свойство не может быть null.
     */
    public void setLogDate(Date logDate)
    {
        dirty(_logDate, logDate);
        _logDate = logDate;
    }

    /**
     * @return Описание ошибки.
     */
    public String getError()
    {
        return _error;
    }

    /**
     * @param error Описание ошибки.
     */
    public void setError(String error)
    {
        dirty(_error, error);
        _error = error;
    }

    /**
     * @return Отметка о том, что все прошло успешно.
     */
    public Boolean getNoError()
    {
        return _noError;
    }

    /**
     * @param noError Отметка о том, что все прошло успешно.
     */
    public void setNoError(Boolean noError)
    {
        dirty(_noError, noError);
        _noError = noError;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MailLogGen)
        {
            setMailMessage(((MailLog)another).getMailMessage());
            setLogDate(((MailLog)another).getLogDate());
            setError(((MailLog)another).getError());
            setNoError(((MailLog)another).getNoError());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MailLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MailLog.class;
        }

        public T newInstance()
        {
            return (T) new MailLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mailMessage":
                    return obj.getMailMessage();
                case "logDate":
                    return obj.getLogDate();
                case "error":
                    return obj.getError();
                case "noError":
                    return obj.getNoError();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mailMessage":
                    obj.setMailMessage((MailMessage) value);
                    return;
                case "logDate":
                    obj.setLogDate((Date) value);
                    return;
                case "error":
                    obj.setError((String) value);
                    return;
                case "noError":
                    obj.setNoError((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mailMessage":
                        return true;
                case "logDate":
                        return true;
                case "error":
                        return true;
                case "noError":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mailMessage":
                    return true;
                case "logDate":
                    return true;
                case "error":
                    return true;
                case "noError":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mailMessage":
                    return MailMessage.class;
                case "logDate":
                    return Date.class;
                case "error":
                    return String.class;
                case "noError":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MailLog> _dslPath = new Path<MailLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MailLog");
    }
            

    /**
     * @return Сообщение, отправляемое по электронной почте. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailLog#getMailMessage()
     */
    public static MailMessage.Path<MailMessage> mailMessage()
    {
        return _dslPath.mailMessage();
    }

    /**
     * @return Дата/время записи. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailLog#getLogDate()
     */
    public static PropertyPath<Date> logDate()
    {
        return _dslPath.logDate();
    }

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.unimail.entity.MailLog#getError()
     */
    public static PropertyPath<String> error()
    {
        return _dslPath.error();
    }

    /**
     * @return Отметка о том, что все прошло успешно.
     * @see ru.tandemservice.unimail.entity.MailLog#getNoError()
     */
    public static PropertyPath<Boolean> noError()
    {
        return _dslPath.noError();
    }

    public static class Path<E extends MailLog> extends EntityPath<E>
    {
        private MailMessage.Path<MailMessage> _mailMessage;
        private PropertyPath<Date> _logDate;
        private PropertyPath<String> _error;
        private PropertyPath<Boolean> _noError;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сообщение, отправляемое по электронной почте. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailLog#getMailMessage()
     */
        public MailMessage.Path<MailMessage> mailMessage()
        {
            if(_mailMessage == null )
                _mailMessage = new MailMessage.Path<MailMessage>(L_MAIL_MESSAGE, this);
            return _mailMessage;
        }

    /**
     * @return Дата/время записи. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailLog#getLogDate()
     */
        public PropertyPath<Date> logDate()
        {
            if(_logDate == null )
                _logDate = new PropertyPath<Date>(MailLogGen.P_LOG_DATE, this);
            return _logDate;
        }

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.unimail.entity.MailLog#getError()
     */
        public PropertyPath<String> error()
        {
            if(_error == null )
                _error = new PropertyPath<String>(MailLogGen.P_ERROR, this);
            return _error;
        }

    /**
     * @return Отметка о том, что все прошло успешно.
     * @see ru.tandemservice.unimail.entity.MailLog#getNoError()
     */
        public PropertyPath<Boolean> noError()
        {
            if(_noError == null )
                _noError = new PropertyPath<Boolean>(MailLogGen.P_NO_ERROR, this);
            return _noError;
        }

        public Class getEntityClass()
        {
            return MailLog.class;
        }

        public String getEntityName()
        {
            return "mailLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
