package ru.tandemservice.unimail.component.mail.MailPub;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.unimail.entity.MailLog;

import java.util.Collection;

public interface IDAO extends IUniDao<Model> {

    void deleteMailLogRecord(MailLog mailLog);

    void deleteMailLogRecords(Collection<IEntity> entityList);
}
