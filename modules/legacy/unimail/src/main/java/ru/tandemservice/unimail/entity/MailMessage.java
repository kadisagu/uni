package ru.tandemservice.unimail.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unimail.entity.catalog.MailContentType;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;
import ru.tandemservice.unimail.entity.catalog.MessageType;
import ru.tandemservice.unimail.entity.gen.*;

import java.text.SimpleDateFormat;

/** @see ru.tandemservice.unimail.entity.gen.MailMessageGen */
public class MailMessage extends MailMessageGen
{
    public MailMessage() {
    }

    public MailMessage(Person person, String subject, String text, MessageType messageType) {
        setPerson(person);
        setSubject(subject);
        setText(text);
        setMessageType(messageType);
    }

    public MailMessage(Person person, String mailTo, String subject, String text, MessageType messageType,
                       MailContentType mailContentType, MailServerSetting mailSetting)
    {
        setPerson(person);
        setMailTo(mailTo);
        setSubject(subject);
        setText(text);
        setMessageType(messageType);
        setContentType(mailContentType);
        setMailSetting(mailSetting);
    }

    @EntityDSLSupport
    public String getTitle() {
        return new StringBuilder("Сообщение ")
                .append("для ").append(this.getRecipient())
                .append(" от ")
                .append(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(this.getCreateDate())).toString();
    }

    @EntityDSLSupport
    public String getRecipient() {
        String recipient = "";

        if (!StringUtils.isEmpty(this.getMailTo()))
            recipient = this.getMailTo();
        else if (this.getPerson() != null && this.getPerson().getContactData() != null)
            recipient = StringUtils.trimToEmpty(this.getPerson().getEmail());

        return recipient;
    }
}