package ru.tandemservice.unimail.component.mail.MailPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimail.entity.MailLog;

import java.util.Collection;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        model.setSettings(component.getSettings());
        onClickClear(component);
        prepareDataSource(component);
    }

    public void prepareDataSource(IBusinessComponent component) {

        final Model model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<MailLog> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new CheckboxColumn("select"));

        dataSource.addColumn(new DateColumn("Время записи", MailLog.logDate(), "dd.MM.yyyy HH:mm").setOrderable(true));

        dataSource.addColumn(new SimpleColumn("Кому", MailLog.mailMessage().recipient()).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Отсутсвие ошибки", MailLog.noError()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", MailLog.mailMessage().mailState().title()).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Описание ошибки", MailLog.error()).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete", "Вы подтверждаете удаление записи из лога для сообщения «{0}» ?",
                                              new Object[]{MailLog.mailMessage().title()}).setPermissionKey("mailLogRecordDelete"));

        model.setDataSource(dataSource);

    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = component.getModel();
        DataSettingsFacade.saveSettings(model.getSettings());
        model.getDataSource().refresh();

    }

    public void onClickClear(IBusinessComponent component) {
        Model model = component.getModel();
        model.getSettings().clear();
        getDao().prepare(model);
    }

    public void onClickDelete(IBusinessComponent component) {
        Model model = component.getModel();
        MailLog mailLog = model.getDataSource().getRecordById((Long) component.getListenerParameter());
        getDao().deleteMailLogRecord(mailLog);
    }

    public void onClickDeleteMailLogRecords(IBusinessComponent component) {
        Model model = component.getModel();

        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать записи для удаления из лога");

        getDao().deleteMailLogRecords(entityList);
    }
}
