package ru.tandemservice.unimail.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состояние отправки электронных писем"
 * Имя сущности : mailState
 * Файл data.xml : unimail.data.xml
 */
public interface MailStateCodes
{
    /** Константа кода (code) элемента : Для отправки (title) */
    String CREATED = "01";
    /** Константа кода (code) элемента : Отправлено (title) */
    String IS_SENDED = "02";
    /** Константа кода (code) элемента : Ошибка отправки (title) */
    String ERROR_SEND = "03";
    /** Константа кода (code) элемента : Игнорировать отправку (title) */
    String IGNORE = "04";
    /** Константа кода (code) элемента : Не отправлять (title) */
    String DO_NOT_SEND = "05";
    /** Константа кода (code) элемента : Неверен e-mail (title) */
    String INCORRECT = "06";
    /** Константа кода (code) элемента : Неверен e-mail персоны (title) */
    String INCORRECT_PERSON_EMAIL = "07";

    Set<String> CODES = ImmutableSet.of(CREATED, IS_SENDED, ERROR_SEND, IGNORE, DO_NOT_SEND, INCORRECT, INCORRECT_PERSON_EMAIL);
}
