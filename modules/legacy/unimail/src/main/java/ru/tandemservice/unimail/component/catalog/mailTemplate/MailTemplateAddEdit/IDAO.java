package ru.tandemservice.unimail.component.catalog.mailTemplate.MailTemplateAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unimail.entity.catalog.MailTemplate;


public interface IDAO extends IDefaultCatalogAddEditDAO<MailTemplate, Model> {
}
