/* $Id$ */
package ru.tandemservice.unimail.util;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author Ekaterina Zvereva
 * @since 01.10.2015
 */
public class AppProperty
{
    /**
     * Запускать демонов в режиме отладки или нет
     *
     * @return
     */
    public static boolean RUN_DEMON()
    {
        if (DEBUG_MODE())
        {
            // если ключ не указан - исполнять
            if (ApplicationRuntime.getProperty("debug.rundemon") == null)
                return false;
            else {
                // ключ указан
                return Boolean.parseBoolean(ApplicationRuntime.getProperty("debug.rundemon"));
            }
        }
        else
            // в боевом режиме - исполняем
            return true;

    }

    public static boolean DEBUG_MODE()
    {
        // по умолчанию - false
        return Boolean.parseBoolean(ApplicationRuntime.getProperty("debug.mode"));
    }
}