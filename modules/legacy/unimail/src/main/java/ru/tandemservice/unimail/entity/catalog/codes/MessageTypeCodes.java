package ru.tandemservice.unimail.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип сообщения"
 * Имя сущности : messageType
 * Файл data.xml : unimail.data.xml
 */
public interface MessageTypeCodes
{
    /** Константа кода (code) элемента : Подписали на сайт (title) */
    String SUBSCIBE_TO_SITE = "01";
    /** Константа кода (code) элемента : Изменили информацию о пользователе (title) */
    String CHANGED_USER_INFO = "02";
    /** Константа кода (code) элемента : Информационное сообщение (title) */
    String INFO = "03";
    /** Константа кода (code) элемента : Подписка на ДПВ (title) */
    String OPTIONAL_DISCIPLINE_SUBSCRIBE = "04";
    /** Константа кода (code) элемента : Смена РП (title) */
    String S2WP = "05";
    /** Константа кода (code) элемента : Подписка на ДПВ преподавателем (title) */
    String OPTIONAL_DISCIPLINE_SUBSCRIBE_TUTOR = "06";

    Set<String> CODES = ImmutableSet.of(SUBSCIBE_TO_SITE, CHANGED_USER_INFO, INFO, OPTIONAL_DISCIPLINE_SUBSCRIBE, S2WP, OPTIONAL_DISCIPLINE_SUBSCRIBE_TUTOR);
}
