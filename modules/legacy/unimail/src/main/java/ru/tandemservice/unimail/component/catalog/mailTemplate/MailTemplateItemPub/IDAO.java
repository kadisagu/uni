package ru.tandemservice.unimail.component.catalog.mailTemplate.MailTemplateItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.unimail.entity.catalog.MailTemplate;


public interface IDAO extends IDefaultCatalogItemPubDAO<MailTemplate, Model> {
}
