package ru.tandemservice.unimail.component.mail.MailPub;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unimail.entity.MailLog;
import ru.tandemservice.unimail.entity.MailMessage;

import java.util.List;

@State(keys = {"publisherId"}, bindings = {"mailId"})
public class Model {

    public static final Long ERROR = 0L;
    public static final Long NO_ERROR = 1L;

    private ISelectModel noError;
    private Long _mailId;
    private MailMessage _mailMessage;

    public final static List<IdentifiableWrapper> ERROR_ITEMS = ImmutableList.of(
            new IdentifiableWrapper(ERROR, "Да"),
            new IdentifiableWrapper(NO_ERROR, "Нет")
    );

    private DynamicListDataSource<MailLog> dataSource;
    private IDataSettings settings;

    public ISelectModel getNoError() {
        return noError;
    }

    public void setNoError(ISelectModel noError) {
        this.noError = noError;
    }

    public DynamicListDataSource<MailLog> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<MailLog> dataSource) {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public Long getMailId()
    {
        return _mailId;
    }

    public void setMailId(Long mailId)
    {
        this._mailId = mailId;
    }

    public MailMessage getMailMessage()
    {
        return _mailMessage;
    }

    public void setMailMessage(MailMessage mailMessage)
    {
        this._mailMessage = mailMessage;
    }
}
