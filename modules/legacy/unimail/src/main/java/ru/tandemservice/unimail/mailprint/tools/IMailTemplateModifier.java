package ru.tandemservice.unimail.mailprint.tools;


public interface IMailTemplateModifier {
    /**
     * Указывает значение к метке в шаблоне
     *
     * @param variable метка
     * @param value    значение
     */
    void addVariable(String variable, String value);

    /**
     * Указывает таблицу к метке в шаблоне
     */
    void addTable(String table, String[][] value);

    /**
     * Убирает все не заполненные метки в шаблоне E-mail
     */
    void clearAllVariables();


    /**
     * Заменяет метки в шаблоне
     */
    String modify();

}
