package ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingPub;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;

import java.util.List;


public class Model {

    public final static Long YES_OPTION = 1L;
    public final static Long NO_OPTION = 2L;

    public final static List<IdentifiableWrapper> ENABLED_ITEMS = ImmutableList.of(
            new IdentifiableWrapper(YES_OPTION, "Да"),
            new IdentifiableWrapper(NO_OPTION, "Нет")
    );

    public final static List<IdentifiableWrapper> SECURE_ITEMS = ImmutableList.of(
            new IdentifiableWrapper(YES_OPTION, "Да"),
            new IdentifiableWrapper(NO_OPTION, "Нет")
    );

    public final static List<IdentifiableWrapper> DEBUG_MODE_ITEMS = ImmutableList.of(
            new IdentifiableWrapper(YES_OPTION, "Да"),
            new IdentifiableWrapper(NO_OPTION, "Нет")
    );

    private DynamicListDataSource<MailServerSetting> dataSource;
    private IDataSettings settings;

    private ISelectModel debugModeSelectModel;
    private ISelectModel enableModeSelectModel;
    private ISelectModel secureModeSelectModel;

    public DynamicListDataSource<MailServerSetting> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<MailServerSetting> dataSource) {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getDebugModeSelectModel() {
        return debugModeSelectModel;
    }

    public void setDebugModeSelectModel(ISelectModel debugModeSelectModel) {
        this.debugModeSelectModel = debugModeSelectModel;
    }

    public ISelectModel getEnableModeSelectModel() {
        return enableModeSelectModel;
    }

    public void setEnableModeSelectModel(ISelectModel enableModeSelectModel) {
        this.enableModeSelectModel = enableModeSelectModel;
    }

    public ISelectModel getSecureModeSelectModel() {
        return secureModeSelectModel;
    }

    public void setSecureModeSelectModel(ISelectModel secureModeSelectModel) {
        this.secureModeSelectModel = secureModeSelectModel;
    }


    @SuppressWarnings("rawtypes")
    public Long getEnableMode() {
        if (getSettings().get("enableMode") == null)
            return null;
        else
            return ((IdentifiableWrapper) getSettings().get("enableMode")).getId();
    }

    @SuppressWarnings("rawtypes")
    public Long getDebugMode() {
        if (getSettings().get("debugMode") == null)
            return null;
        else
            return ((IdentifiableWrapper) getSettings().get("debugMode")).getId();
    }

    @SuppressWarnings("rawtypes")
    public Long getSecureMode() {
        if (getSettings().get("secureMode") == null)
            return null;
        else
            return ((IdentifiableWrapper) getSettings().get("secureMode")).getId();
    }
}
