package ru.tandemservice.unimail.validators;

public class Test {

    /**
     * @param args
     */
    public static void main(String[] args)
    {

        MailValidator validator = new MailValidator();
        System.out.println(validator.validate("raaaa....aaa@mail.ru"));
        System.out.println(validator.validate("ramec@mail.ru"));
        System.out.println(validator.validate("ramec_test@mail.ru"));
        System.out.println(validator.validate("ramec@test@mail.ru"));

    }

}
