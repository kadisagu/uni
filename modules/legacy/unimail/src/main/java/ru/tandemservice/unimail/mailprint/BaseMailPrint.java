package ru.tandemservice.unimail.mailprint;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimail.dao.MailDAO;
import ru.tandemservice.unimail.dao.MailDaoFacade;
import ru.tandemservice.unimail.entity.MailMessage;
import ru.tandemservice.unimail.entity.catalog.MailTemplate;
import ru.tandemservice.unimail.mailprint.tools.MailTemplateModifier;

import java.util.Map;


public abstract class BaseMailPrint extends UniBaseDao implements IMailPrint {

    @Override
    public MailMessage makeMailMessage(IEntity entity, Object params)
    {
        // 1 - получим шаблон
        MailTemplate template = getByCode(MailTemplate.class, getTemplateCode());
        if (template == null)
            throw new ApplicationException("Шаблон электронного письма для кода='" + getTemplateCode() + "' не задан");

        // шаблон есть - получим текст шаблона
        String tmp = template.getStringValue();

        if (tmp == null || tmp.isEmpty())
            throw new ApplicationException("Шаблон электронного письма для кода='" + getTemplateCode() + "' пуст");
        else {
            String message = modifayTemplate(tmp, entity, params);
            // формируем письмо, кладем в очередь

            return MailDaoFacade.getMailDao().addNewMail(getPerson(entity), getSubject(entity, template),
                                      message, template.getMessageType(), template.getMailContentType());
        }
    }

    protected String modifayTemplate(String tmp, IEntity entity, Object params)
    {
        MailTemplateModifier modifier = new MailTemplateModifier(tmp);

        Map<String, Object> map = getMapParameters(entity, params);

        for (String key : map.keySet()) {
            Object value = map.get(key);
            if (value != null) {
                if (value instanceof String)
                    modifier.addVariable(key, (String) value);

                if (value instanceof String[][])
                    modifier.addTable(key, (String[][]) value);
            }
        }

        return modifier.modify();
    }

    /**
     * Получаем параметры для заполнения шаблона
     */
    protected abstract Map<String, Object> getMapParameters(IEntity entity, Object params);


    /**
     * код шаблона
     */
    protected abstract String getTemplateCode();

    protected abstract Person getPerson(IEntity entity);

    /**
     * Заголовок письма по умолчанию
     *
     * @param entity
     * @param template
     *
     * @return
     */
    protected String getSubject(IEntity entity, MailTemplate template)
    {
        return template.getTitle();
    }

}
