package ru.tandemservice.unimail.component.mail.MailStateEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Return;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unimail.entity.catalog.MailState;

@Return({
        @Bind(key = "id", binding = "mailState.id")
})
public class Model {

    private ISelectModel mailStateSelectModel;
    private MailState _mailState;

    public MailState getMailState() {
        return _mailState;
    }

    public void setMailState(MailState mailState) {
        this._mailState = mailState;
    }

    public ISelectModel getMailStateSelectModel() {
        return mailStateSelectModel;
    }

    public void setMailStateSelectModel(ISelectModel mailStateSelectModel) {
        this.mailStateSelectModel = mailStateSelectModel;
    }
}
