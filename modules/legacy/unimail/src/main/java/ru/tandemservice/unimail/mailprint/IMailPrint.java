package ru.tandemservice.unimail.mailprint;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unimail.entity.MailMessage;

public interface IMailPrint {

    MailMessage makeMailMessage(IEntity entity, Object params);
}
