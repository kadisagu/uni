package ru.tandemservice.unimail.component.mail.MailList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.unimail.dao.MailDAO;
import ru.tandemservice.unimail.entity.MailMessage;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        model.setSettings(component.getSettings());

        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {

        final Model model = component.getModel();

        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<MailMessage> dataSource = UniUtils.createDataSource(component, getDao());

        dataSource.addColumn(new CheckboxColumn("select"));

        dataSource.addColumn(new PublisherLinkColumn("Название",
                                                     MailMessage.title()).setResolver(new IPublisherLinkResolver() {
            @Override
            public Object getParameters(IEntity entity) {
                final Map<String, Object> parameters = new HashMap<String, Object>();
                parameters.put(PublisherActivator.PUBLISHER_ID_KEY, ((MailMessage) entity).getId());
                return parameters;
            }

            @Override
            public String getComponentName(IEntity ientity) {
                return ru.tandemservice.unimail.component.mail.MailPub.Controller.class.getPackage().getName();
            }
        }).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Кому", MailMessage.mailTo()).setClickable(false).setOrderable(true));

        dataSource.addColumn(new PublisherLinkColumn("Персона",
                                                     MailMessage.person().identityCard().fullFio()).setResolver(new IPublisherLinkResolver() {
            @Override
            public Object getParameters(IEntity entity) {
                final Map<String, Object> parameters = new HashMap<String, Object>();
                if (((MailMessage) entity).getPerson() != null)
                    parameters.put(PublisherActivator.PUBLISHER_ID_KEY, ((MailMessage) entity).getPerson().getId());
                return parameters;
            }

            @Override
            public String getComponentName(IEntity paramIEntity) {
                return null;
            }
        }).setClickable(true).setOrderable(true));

        dataSource.addColumn(new DateColumn("Дата создания", MailMessage.createDate(), "dd.MM.yyyy HH:mm").setClickable(false).setOrderable(true));
        dataSource.addColumn(new DateColumn("Дата отправки", MailMessage.sendDate(), "dd.MM.yyyy HH:mm").setClickable(false).setOrderable(true));
        dataSource.addColumn((new SimpleColumn("Тема", MailMessage.subject()).setClickable(false).setOrderable(true)));
        dataSource.addColumn((new SimpleColumn("Тип сообщения", MailMessage.messageType().title()).setClickable(false).setOrderable(true)));
        dataSource.addColumn((new SimpleColumn("Cостояние сообщения", MailMessage.mailState().title()).setClickable(false).setOrderable(true)));
        dataSource.addColumn((new SimpleColumn("Content-type", MailMessage.contentType().title()).setClickable(false).setOrderable(false)));
        dataSource.addColumn((new SimpleColumn("Используемая настройка", MailMessage.mailSetting().title()).setOrderable(false)));

        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete", "Вы подтверждаете удаление «{0}» ?",
                                              new Object[]{MailMessage.title()}).setPermissionKey("mailDelete"));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component) {

        saveSettings(component);
        onRefreshComponent(component);
    }

    private void saveSettings(IBusinessComponent component)
    {
        Model model = component.getModel();
        component.saveSettings();
        model.setSettings(component.getSettings());
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.getSettings().clear();
        component.saveSettings();

        onRefreshComponent(component);

    }

    public void onClickDelete(IBusinessComponent component) {
        Model model = component.getModel();
        MailMessage message = model.getDataSource().getRecordById((Long) component.getListenerParameter());
        getDao().deleteMailMessage(message);
    }

    public void onClickCallMailService(IBusinessComponent component) {
        MailDAO.mailDaemon.wakeUpDaemon();
        component.refresh();
    }

    public void onClickChangeMailState(IBusinessComponent component) {

        saveSettings(component);

        Model model = component.getModel();
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Для изменения состояния сообщений необходимо выбрать сообщения!");

        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(ru.tandemservice.unimail.component.mail.MailStateEdit.Controller.class.getPackage().getName()));
    }

    /**
     * срабатывает при return биндинге
     */
    @Override
    public void bindSetReturned(IBusinessComponent component, String param, Map<String, Object> values) {

        Model model = component.getModel();
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (values == null || entityList == null || entityList.isEmpty())
            return;

        if (values.get(MailMessage.P_ID) != null) {
            Long newMailStateId = (Long) values.get(MailMessage.P_ID);
            ((IDAO) getDao()).updateMailStates(model, entityList, newMailStateId);
        }

        clearCheckboxes(component);
    }

    private void clearCheckboxes(IBusinessComponent component) {
        Model model = component.getModel();
        ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects().clear();
    }

}
