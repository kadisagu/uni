package ru.tandemservice.unimail.component.mail.MailPub;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimail.entity.MailLog;
import ru.tandemservice.unimail.entity.MailMessage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setMailMessage(getNotNull(MailMessage.class, model.getMailId()));
        model.setNoError(new LazySimpleSelectModel<>(Model.ERROR_ITEMS));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<MailLog> dataSource = model.getDataSource();
        DQLSelectBuilder dql = getDataSourceBuilder(model);
        new DQLOrderDescriptionRegistry(MailLog.class, "ml").applyOrder(dql, dataSource.getEntityOrder());
        UniBaseUtils.createPage(dataSource, dql, getSession());
    }


    @SuppressWarnings("rawtypes")
    private DQLSelectBuilder getDataSourceBuilder(Model model) {

        IDataSettings settings = model.getSettings();

        Date fromDate = settings.get("fromDate");
        Date toDate = settings.get("toDate");
        IdentifiableWrapper noError = settings.get("noError");

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(MailLog.class, "ml")
                .where(eq(property(MailLog.mailMessage().id().fromAlias("ml")), value(model.getMailMessage().getId())));

        if (fromDate != null)
            builder.where(ge(property(MailLog.logDate().fromAlias("ml")), valueDate(fromDate)));

        if (toDate != null)
            builder.where(DQLExpressions.le(property(MailLog.logDate().fromAlias("ml")), valueDate(toDate)));

        if (noError != null) {
            if (noError.getId().equals(Model.ERROR))
                builder.where(eqValue(property(MailLog.noError().fromAlias("ml")), Boolean.FALSE));
            else {
                builder.where(eqValue(property(MailLog.noError().fromAlias("ml")), Boolean.TRUE));
            }
        }

        return builder;
    }

    @Override
    public void deleteMailLogRecord(MailLog mailLog) {
        new DQLDeleteBuilder(MailLog.class)
                .where(eq(property(MailLog.id()), value(mailLog.getId())))
                .createStatement(getSession()).execute();
    }

    @Override
    public void deleteMailLogRecords(Collection<IEntity> entityList) {

        List<Long> mailLogIds = new ArrayList<>();
        for (IEntity entity : entityList)
            mailLogIds.add(entity.getId());

        new DQLDeleteBuilder(MailLog.class)
                .where(in(property(MailLog.id()),mailLogIds))
                .createStatement(getSession()).execute();

    }

}
