package ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {

        model.setEnableModeSelectModel(new LazySimpleSelectModel<>(Model.ENABLED_ITEMS));
        model.setDebugModeSelectModel(new LazySimpleSelectModel<>(Model.DEBUG_MODE_ITEMS));
        model.setSecureModeSelectModel(new LazySimpleSelectModel<>(Model.SECURE_ITEMS));
    }

    @Override
    public void prepareListDataSource(Model model) {

        UniBaseUtils.createPage(model.getDataSource(), getMailServerSettingList(model));

        if (model.getDataSource().getEntityOrder() != null) {
            Collections.sort(model.getDataSource().getEntityList(), new EntityComparator<>(model.getDataSource().getEntityOrder()));
        }
    }

    private List<MailServerSetting> getMailServerSettingList(Model model) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(MailServerSetting.class, "ms")
                .column("ms");

        if (model.getEnableMode() != null)
                builder.where(eq(property(MailServerSetting.enabled().fromAlias("ms")), value(model.getEnableMode().equals(Model.YES_OPTION))));

        if (model.getDebugMode() != null)
                builder.where(eq(property(MailServerSetting.useDebugMode().fromAlias("ms")), value(model.getDebugMode().equals(Model.YES_OPTION))));

        if (model.getSecureMode() != null)
                builder.where(eq(property(MailServerSetting.useSSL().fromAlias("ms")), value(model.getSecureMode().equals(Model.YES_OPTION))));

        return getList(builder);
    }

    @Override
    public void changeSetting(MailServerSetting mailServerSetting, String fieldName, Boolean value) {

        if (fieldName.equals(MailServerSetting.P_USE_DEFAULT)) {
            changeDefaultSetting(mailServerSetting);
        }
        else {
            try {
                Method method = mailServerSetting.getClass().getMethod("set" + StringUtils.capitalize(fieldName), Boolean.class);
                method.invoke(mailServerSetting, value);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void changeDefaultSetting(MailServerSetting mailServerSetting) {
        lock(MailServerSetting.class.getName());

        executeAndClear(new DQLUpdateBuilder(MailServerSetting.class)
                                .set("useDefault",value(Boolean.FALSE))
                                .where(eq(property("useDefault"), value(Boolean.TRUE))));

        executeAndClear(new DQLUpdateBuilder(MailServerSetting.class)
                                .set("useDefault", caseExpr(eq(property("id"), value(mailServerSetting.getId())), value(Boolean.TRUE), value(Boolean.FALSE))));

    }

    @Override
    public void deleteMailServerSetting(MailServerSetting mailServerSetting) {
        new DQLDeleteBuilder(MailServerSetting.class)
                .where(eq(property(MailServerSetting.id()),value(mailServerSetting.getId())))
                .createStatement(getSession()).execute();
    }
}