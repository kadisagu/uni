package ru.tandemservice.unimail.mailprint.tools;

import java.util.Hashtable;
import java.util.Map;

public class MailTemplateModifier implements IMailTemplateModifier {
    private Map<String, String> _name2value = new Hashtable<>();
    private String pattern = "[{][^{^}]*[}]";
    private String template;

    public MailTemplateModifier(String template)
    {
        this.template = template;
    }

    @Override
    public void addVariable(String variable, String value) {
        this._name2value.put(variable, value == null ? "" : value);
    }

    @Override
    public void addTable(String variable, String[][] value) {
        StringBuilder result = new StringBuilder();
        if (value == null || value.length == 0 || value[0].length == 0) {
            this._name2value.put(variable, "");
            return;
        }
        for (String[] row : value) {
            result.append("<tr>");
            for (String cell : row) {
                result.append("<td>");
                result.append(cell);
                result.append("</td>");
            }
            result.append("</tr>");
        }
        this._name2value.put(variable, result.toString());
    }

    @Override
    public void clearAllVariables()
    {
        template = template.replaceAll(pattern, "");
    }

    @Override
    public String modify()
    {
        for (Map.Entry<String, String> item : _name2value.entrySet()) {
            template = template.replace("{" + item.getKey() + "}", item.getValue());
        }
        clearAllVariables();

        return template;
    }

}
