package ru.tandemservice.unimail.entity;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unimail.entity.gen.MailLogGen;

import java.util.Date;

/**
 * Логирование электронных сообщений
 */
public class MailLog extends MailLogGen implements IEntity {
    public MailLog() {
    }

    public MailLog(MailMessage mail, Date logDate, String error, boolean noError) {
        setMailMessage(mail);
        setLogDate(logDate);
        setError(error);
        setNoError(noError);
    }
}