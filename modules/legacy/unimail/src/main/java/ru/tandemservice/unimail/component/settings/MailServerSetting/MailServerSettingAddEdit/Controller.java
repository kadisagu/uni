package ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = component.getModel();
        getDao().saveOrUpdate(model);
        deactivate(component);
    }
}
