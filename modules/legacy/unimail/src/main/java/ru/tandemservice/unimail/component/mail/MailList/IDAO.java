package ru.tandemservice.unimail.component.mail.MailList;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.unimail.entity.MailMessage;

import java.util.Collection;

public interface IDAO extends IUniDao<Model> {

    @Transactional
    public void deleteMailMessage(MailMessage message);

    @Transactional
    public void updateMailStates(Model model, Collection<IEntity> entityList, Long mailStateId);
}
