package ru.tandemservice.unimail.component.catalog.mailTemplate.MailTemplatePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unimail.entity.catalog.MailTemplate;

public class Controller extends DefaultCatalogPubController<MailTemplate, Model, IDAO> {


    @Override
    protected DynamicListDataSource<MailTemplate> createListDataSource(final IBusinessComponent context) {

        final DynamicListDataSource<MailTemplate> dataSource = super.createListDataSource(context);
        dataSource.addColumn(new SimpleColumn("Тип содержимого письма", MailTemplate.mailContentType().title().s()).setClickable(false).setOrderable(false), 1);
        dataSource.addColumn(new SimpleColumn("Тип сообщения", MailTemplate.messageType().title().s()).setClickable(false).setOrderable(false), 2);
        dataSource.addColumn(new SimpleColumn("Текст шаблона", MailTemplate.P_STRING_VALUE).setClickable(false).setOrderable(false).setWidth("50%"), 3);

        return dataSource;
    }

}
