package ru.tandemservice.unimail.validators;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.impl.EmailValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MailValidator {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    //"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


    private Pattern pattern;

    private Matcher matcher;

    public MailValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    public boolean validate(String email)
    {
        if (!StringUtils.isEmpty(email)) {
            EmailValidator validate = new EmailValidator();
            return validate.isValid(email, null);

            //matcher = pattern.matcher(email);
            //return matcher.matches();
        }


        return false;
    }
}
