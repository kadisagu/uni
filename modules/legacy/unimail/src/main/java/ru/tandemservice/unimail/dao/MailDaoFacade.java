package ru.tandemservice.unimail.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

public class MailDaoFacade {

    private static IMailDAO _mailDao;
    private static IMailConfigurator _mailConfigurator;

    public static IMailDAO getMailDao() {

        if (_mailDao == null)
            _mailDao = (IMailDAO) ApplicationRuntime.getBean(IMailDAO.class.getName());
        return _mailDao;
    }

    public static IMailConfigurator getMailConfigurator() {

        if (_mailConfigurator == null)
            _mailConfigurator = (IMailConfigurator) ApplicationRuntime.getBean(IMailConfigurator.class.getName());
        return _mailConfigurator;
    }

}
