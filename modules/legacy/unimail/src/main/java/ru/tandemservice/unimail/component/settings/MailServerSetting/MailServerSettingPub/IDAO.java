package ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingPub;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;

public interface IDAO extends IUniDao<Model> {

    void changeSetting(MailServerSetting mailServerSetting, String fieldName, Boolean value);

    void deleteMailServerSetting(MailServerSetting mailServerSetting);
}
