package ru.tandemservice.unimail.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unimail.entity.MailMessage;
import ru.tandemservice.unimail.entity.catalog.MailContentType;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;
import ru.tandemservice.unimail.entity.catalog.MailState;
import ru.tandemservice.unimail.entity.catalog.MessageType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сообщения, отправляемые по электронной почте
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MailMessageGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimail.entity.MailMessage";
    public static final String ENTITY_NAME = "mailMessage";
    public static final int VERSION_HASH = 45610946;
    private static IEntityMeta ENTITY_META;

    public static final String L_MAIL_SETTING = "mailSetting";
    public static final String P_MAIL_TO = "mailTo";
    public static final String L_PERSON = "person";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_SEND_DATE = "sendDate";
    public static final String P_SUBJECT = "subject";
    public static final String P_TEXT = "text";
    public static final String L_MESSAGE_TYPE = "messageType";
    public static final String L_CONTENT_TYPE = "contentType";
    public static final String L_MAIL_STATE = "mailState";
    public static final String P_RECIPIENT = "recipient";
    public static final String P_TITLE = "title";

    private MailServerSetting _mailSetting;     // Настройки почтового сервера
    private String _mailTo;     // Отправить на адрес
    private Person _person;     // Персона (проверяем, если значение адреса mailTo неверно или пусто)
    private Date _createDate;     // Дата создания сообщения
    private Date _sendDate;     // Дата отправки
    private String _subject;     // Заголовок письма
    private String _text;     // Тело письма
    private MessageType _messageType;     // Тип сообщения
    private MailContentType _contentType;     // Content-Type
    private MailState _mailState;     // Состояние отправки электронных писем

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Настройки почтового сервера. Свойство не может быть null.
     */
    @NotNull
    public MailServerSetting getMailSetting()
    {
        return _mailSetting;
    }

    /**
     * @param mailSetting Настройки почтового сервера. Свойство не может быть null.
     */
    public void setMailSetting(MailServerSetting mailSetting)
    {
        dirty(_mailSetting, mailSetting);
        _mailSetting = mailSetting;
    }

    /**
     * @return Отправить на адрес.
     */
    @Length(max=255)
    public String getMailTo()
    {
        return _mailTo;
    }

    /**
     * @param mailTo Отправить на адрес.
     */
    public void setMailTo(String mailTo)
    {
        dirty(_mailTo, mailTo);
        _mailTo = mailTo;
    }

    /**
     * @return Персона (проверяем, если значение адреса mailTo неверно или пусто).
     */
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона (проверяем, если значение адреса mailTo неверно или пусто).
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Дата создания сообщения. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата создания сообщения. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата отправки.
     */
    public Date getSendDate()
    {
        return _sendDate;
    }

    /**
     * @param sendDate Дата отправки.
     */
    public void setSendDate(Date sendDate)
    {
        dirty(_sendDate, sendDate);
        _sendDate = sendDate;
    }

    /**
     * @return Заголовок письма.
     */
    @Length(max=255)
    public String getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Заголовок письма.
     */
    public void setSubject(String subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    /**
     * @return Тело письма. Свойство не может быть null.
     */
    @NotNull
    public String getText()
    {
        return _text;
    }

    /**
     * @param text Тело письма. Свойство не может быть null.
     */
    public void setText(String text)
    {
        dirty(_text, text);
        _text = text;
    }

    /**
     * @return Тип сообщения. Свойство не может быть null.
     */
    @NotNull
    public MessageType getMessageType()
    {
        return _messageType;
    }

    /**
     * @param messageType Тип сообщения. Свойство не может быть null.
     */
    public void setMessageType(MessageType messageType)
    {
        dirty(_messageType, messageType);
        _messageType = messageType;
    }

    /**
     * @return Content-Type. Свойство не может быть null.
     */
    @NotNull
    public MailContentType getContentType()
    {
        return _contentType;
    }

    /**
     * @param contentType Content-Type. Свойство не может быть null.
     */
    public void setContentType(MailContentType contentType)
    {
        dirty(_contentType, contentType);
        _contentType = contentType;
    }

    /**
     * @return Состояние отправки электронных писем. Свойство не может быть null.
     */
    @NotNull
    public MailState getMailState()
    {
        return _mailState;
    }

    /**
     * @param mailState Состояние отправки электронных писем. Свойство не может быть null.
     */
    public void setMailState(MailState mailState)
    {
        dirty(_mailState, mailState);
        _mailState = mailState;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MailMessageGen)
        {
            setMailSetting(((MailMessage)another).getMailSetting());
            setMailTo(((MailMessage)another).getMailTo());
            setPerson(((MailMessage)another).getPerson());
            setCreateDate(((MailMessage)another).getCreateDate());
            setSendDate(((MailMessage)another).getSendDate());
            setSubject(((MailMessage)another).getSubject());
            setText(((MailMessage)another).getText());
            setMessageType(((MailMessage)another).getMessageType());
            setContentType(((MailMessage)another).getContentType());
            setMailState(((MailMessage)another).getMailState());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MailMessageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MailMessage.class;
        }

        public T newInstance()
        {
            return (T) new MailMessage();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mailSetting":
                    return obj.getMailSetting();
                case "mailTo":
                    return obj.getMailTo();
                case "person":
                    return obj.getPerson();
                case "createDate":
                    return obj.getCreateDate();
                case "sendDate":
                    return obj.getSendDate();
                case "subject":
                    return obj.getSubject();
                case "text":
                    return obj.getText();
                case "messageType":
                    return obj.getMessageType();
                case "contentType":
                    return obj.getContentType();
                case "mailState":
                    return obj.getMailState();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mailSetting":
                    obj.setMailSetting((MailServerSetting) value);
                    return;
                case "mailTo":
                    obj.setMailTo((String) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "sendDate":
                    obj.setSendDate((Date) value);
                    return;
                case "subject":
                    obj.setSubject((String) value);
                    return;
                case "text":
                    obj.setText((String) value);
                    return;
                case "messageType":
                    obj.setMessageType((MessageType) value);
                    return;
                case "contentType":
                    obj.setContentType((MailContentType) value);
                    return;
                case "mailState":
                    obj.setMailState((MailState) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mailSetting":
                        return true;
                case "mailTo":
                        return true;
                case "person":
                        return true;
                case "createDate":
                        return true;
                case "sendDate":
                        return true;
                case "subject":
                        return true;
                case "text":
                        return true;
                case "messageType":
                        return true;
                case "contentType":
                        return true;
                case "mailState":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mailSetting":
                    return true;
                case "mailTo":
                    return true;
                case "person":
                    return true;
                case "createDate":
                    return true;
                case "sendDate":
                    return true;
                case "subject":
                    return true;
                case "text":
                    return true;
                case "messageType":
                    return true;
                case "contentType":
                    return true;
                case "mailState":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mailSetting":
                    return MailServerSetting.class;
                case "mailTo":
                    return String.class;
                case "person":
                    return Person.class;
                case "createDate":
                    return Date.class;
                case "sendDate":
                    return Date.class;
                case "subject":
                    return String.class;
                case "text":
                    return String.class;
                case "messageType":
                    return MessageType.class;
                case "contentType":
                    return MailContentType.class;
                case "mailState":
                    return MailState.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MailMessage> _dslPath = new Path<MailMessage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MailMessage");
    }
            

    /**
     * @return Настройки почтового сервера. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getMailSetting()
     */
    public static MailServerSetting.Path<MailServerSetting> mailSetting()
    {
        return _dslPath.mailSetting();
    }

    /**
     * @return Отправить на адрес.
     * @see ru.tandemservice.unimail.entity.MailMessage#getMailTo()
     */
    public static PropertyPath<String> mailTo()
    {
        return _dslPath.mailTo();
    }

    /**
     * @return Персона (проверяем, если значение адреса mailTo неверно или пусто).
     * @see ru.tandemservice.unimail.entity.MailMessage#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Дата создания сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата отправки.
     * @see ru.tandemservice.unimail.entity.MailMessage#getSendDate()
     */
    public static PropertyPath<Date> sendDate()
    {
        return _dslPath.sendDate();
    }

    /**
     * @return Заголовок письма.
     * @see ru.tandemservice.unimail.entity.MailMessage#getSubject()
     */
    public static PropertyPath<String> subject()
    {
        return _dslPath.subject();
    }

    /**
     * @return Тело письма. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getText()
     */
    public static PropertyPath<String> text()
    {
        return _dslPath.text();
    }

    /**
     * @return Тип сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getMessageType()
     */
    public static MessageType.Path<MessageType> messageType()
    {
        return _dslPath.messageType();
    }

    /**
     * @return Content-Type. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getContentType()
     */
    public static MailContentType.Path<MailContentType> contentType()
    {
        return _dslPath.contentType();
    }

    /**
     * @return Состояние отправки электронных писем. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getMailState()
     */
    public static MailState.Path<MailState> mailState()
    {
        return _dslPath.mailState();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unimail.entity.MailMessage#getRecipient()
     */
    public static SupportedPropertyPath<String> recipient()
    {
        return _dslPath.recipient();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unimail.entity.MailMessage#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends MailMessage> extends EntityPath<E>
    {
        private MailServerSetting.Path<MailServerSetting> _mailSetting;
        private PropertyPath<String> _mailTo;
        private Person.Path<Person> _person;
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _sendDate;
        private PropertyPath<String> _subject;
        private PropertyPath<String> _text;
        private MessageType.Path<MessageType> _messageType;
        private MailContentType.Path<MailContentType> _contentType;
        private MailState.Path<MailState> _mailState;
        private SupportedPropertyPath<String> _recipient;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Настройки почтового сервера. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getMailSetting()
     */
        public MailServerSetting.Path<MailServerSetting> mailSetting()
        {
            if(_mailSetting == null )
                _mailSetting = new MailServerSetting.Path<MailServerSetting>(L_MAIL_SETTING, this);
            return _mailSetting;
        }

    /**
     * @return Отправить на адрес.
     * @see ru.tandemservice.unimail.entity.MailMessage#getMailTo()
     */
        public PropertyPath<String> mailTo()
        {
            if(_mailTo == null )
                _mailTo = new PropertyPath<String>(MailMessageGen.P_MAIL_TO, this);
            return _mailTo;
        }

    /**
     * @return Персона (проверяем, если значение адреса mailTo неверно или пусто).
     * @see ru.tandemservice.unimail.entity.MailMessage#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Дата создания сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(MailMessageGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата отправки.
     * @see ru.tandemservice.unimail.entity.MailMessage#getSendDate()
     */
        public PropertyPath<Date> sendDate()
        {
            if(_sendDate == null )
                _sendDate = new PropertyPath<Date>(MailMessageGen.P_SEND_DATE, this);
            return _sendDate;
        }

    /**
     * @return Заголовок письма.
     * @see ru.tandemservice.unimail.entity.MailMessage#getSubject()
     */
        public PropertyPath<String> subject()
        {
            if(_subject == null )
                _subject = new PropertyPath<String>(MailMessageGen.P_SUBJECT, this);
            return _subject;
        }

    /**
     * @return Тело письма. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getText()
     */
        public PropertyPath<String> text()
        {
            if(_text == null )
                _text = new PropertyPath<String>(MailMessageGen.P_TEXT, this);
            return _text;
        }

    /**
     * @return Тип сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getMessageType()
     */
        public MessageType.Path<MessageType> messageType()
        {
            if(_messageType == null )
                _messageType = new MessageType.Path<MessageType>(L_MESSAGE_TYPE, this);
            return _messageType;
        }

    /**
     * @return Content-Type. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getContentType()
     */
        public MailContentType.Path<MailContentType> contentType()
        {
            if(_contentType == null )
                _contentType = new MailContentType.Path<MailContentType>(L_CONTENT_TYPE, this);
            return _contentType;
        }

    /**
     * @return Состояние отправки электронных писем. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.MailMessage#getMailState()
     */
        public MailState.Path<MailState> mailState()
        {
            if(_mailState == null )
                _mailState = new MailState.Path<MailState>(L_MAIL_STATE, this);
            return _mailState;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unimail.entity.MailMessage#getRecipient()
     */
        public SupportedPropertyPath<String> recipient()
        {
            if(_recipient == null )
                _recipient = new SupportedPropertyPath<String>(MailMessageGen.P_RECIPIENT, this);
            return _recipient;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unimail.entity.MailMessage#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(MailMessageGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return MailMessage.class;
        }

        public String getEntityName()
        {
            return "mailMessage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getRecipient();

    public abstract String getTitle();
}
