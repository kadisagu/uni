package ru.tandemservice.unimail.component.settings.MailServerSetting.MailServerSettingAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;


@Input({@Bind(key = "mailServerSettingId", binding = "mailServerSettingId")})
public class Model {

    private Long mailServerSettingId;
    private MailServerSetting mailServerSetting;

    public MailServerSetting getMailServerSetting() {
        return mailServerSetting;
    }

    public Long getMailServerSettingId() {
        return mailServerSettingId;
    }

    public void setMailServerSetting(MailServerSetting mailServerSetting) {
        this.mailServerSetting = mailServerSetting;
    }

    public void setMailServerSettingId(Long mailServerSettingId) {
        this.mailServerSettingId = mailServerSettingId;
    }
}
