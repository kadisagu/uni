package ru.tandemservice.unimail.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки для почтовых уведомлений
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MailServerSettingGen extends EntityBase
 implements INaturalIdentifiable<MailServerSettingGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unimail.entity.catalog.MailServerSetting";
    public static final String ENTITY_NAME = "mailServerSetting";
    public static final int VERSION_HASH = -1094720934;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_USE_DEFAULT = "useDefault";
    public static final String P_ENABLED = "enabled";
    public static final String P_MAIL_SITE_LINK = "mailSiteLink";
    public static final String P_HOST = "host";
    public static final String P_MAIL_ADDRESS = "mailAddress";
    public static final String P_USERNAME = "username";
    public static final String P_PASSWORD = "password";
    public static final String P_USE_DEBUG_MODE = "useDebugMode";
    public static final String P_DEBUG_LIST = "debugList";
    public static final String P_PORT = "port";
    public static final String P_USE_S_S_L = "useSSL";
    public static final String P_USER_CODE = "userCode";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private Boolean _useDefault;     // Использовать по умолчанию
    private Boolean _enabled;     // Использовать настройку
    private String _mailSiteLink;     // Ссылка на сайт из писем
    private String _host;     // Хост почты
    private String _mailAddress;     // Адрес почты
    private String _username;     // Имя почтового ящика (для авторизации)
    private String _password;     // Пароль почтового ящика (для авторизации)
    private Boolean _useDebugMode;     // Отправка писем в режиме отладки (только на адреса из фильтра)
    private String _debugList;     // Кому отсылать письма? (только в режиме отладки)
    private int _port;     // Порт SMTP сервера
    private Boolean _useSSL;     // Использовать защищённые протоколы (SSL/TLS)
    private String _userCode;     // Пользовательский код
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Использовать по умолчанию. Свойство должно быть уникальным.
     */
    public Boolean getUseDefault()
    {
        return _useDefault;
    }

    /**
     * @param useDefault Использовать по умолчанию. Свойство должно быть уникальным.
     */
    public void setUseDefault(Boolean useDefault)
    {
        dirty(_useDefault, useDefault);
        _useDefault = useDefault;
    }

    /**
     * @return Использовать настройку.
     */
    public Boolean getEnabled()
    {
        return _enabled;
    }

    /**
     * @param enabled Использовать настройку.
     */
    public void setEnabled(Boolean enabled)
    {
        dirty(_enabled, enabled);
        _enabled = enabled;
    }

    /**
     * @return Ссылка на сайт из писем.
     */
    @Length(max=255)
    public String getMailSiteLink()
    {
        return _mailSiteLink;
    }

    /**
     * @param mailSiteLink Ссылка на сайт из писем.
     */
    public void setMailSiteLink(String mailSiteLink)
    {
        dirty(_mailSiteLink, mailSiteLink);
        _mailSiteLink = mailSiteLink;
    }

    /**
     * @return Хост почты. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getHost()
    {
        return _host;
    }

    /**
     * @param host Хост почты. Свойство не может быть null.
     */
    public void setHost(String host)
    {
        dirty(_host, host);
        _host = host;
    }

    /**
     * @return Адрес почты.
     */
    @Length(max=255)
    public String getMailAddress()
    {
        return _mailAddress;
    }

    /**
     * @param mailAddress Адрес почты.
     */
    public void setMailAddress(String mailAddress)
    {
        dirty(_mailAddress, mailAddress);
        _mailAddress = mailAddress;
    }

    /**
     * @return Имя почтового ящика (для авторизации).
     */
    @Length(max=255)
    public String getUsername()
    {
        return _username;
    }

    /**
     * @param username Имя почтового ящика (для авторизации).
     */
    public void setUsername(String username)
    {
        dirty(_username, username);
        _username = username;
    }

    /**
     * @return Пароль почтового ящика (для авторизации).
     */
    @Length(max=255)
    public String getPassword()
    {
        return _password;
    }

    /**
     * @param password Пароль почтового ящика (для авторизации).
     */
    public void setPassword(String password)
    {
        dirty(_password, password);
        _password = password;
    }

    /**
     * @return Отправка писем в режиме отладки (только на адреса из фильтра).
     */
    public Boolean getUseDebugMode()
    {
        return _useDebugMode;
    }

    /**
     * @param useDebugMode Отправка писем в режиме отладки (только на адреса из фильтра).
     */
    public void setUseDebugMode(Boolean useDebugMode)
    {
        dirty(_useDebugMode, useDebugMode);
        _useDebugMode = useDebugMode;
    }

    /**
     * @return Кому отсылать письма? (только в режиме отладки).
     */
    @Length(max=255)
    public String getDebugList()
    {
        return _debugList;
    }

    /**
     * @param debugList Кому отсылать письма? (только в режиме отладки).
     */
    public void setDebugList(String debugList)
    {
        dirty(_debugList, debugList);
        _debugList = debugList;
    }

    /**
     * @return Порт SMTP сервера. Свойство не может быть null.
     */
    @NotNull
    public int getPort()
    {
        return _port;
    }

    /**
     * @param port Порт SMTP сервера. Свойство не может быть null.
     */
    public void setPort(int port)
    {
        dirty(_port, port);
        _port = port;
    }

    /**
     * @return Использовать защищённые протоколы (SSL/TLS).
     */
    public Boolean getUseSSL()
    {
        return _useSSL;
    }

    /**
     * @param useSSL Использовать защищённые протоколы (SSL/TLS).
     */
    public void setUseSSL(Boolean useSSL)
    {
        dirty(_useSSL, useSSL);
        _useSSL = useSSL;
    }

    /**
     * @return Пользовательский код. Свойство должно быть уникальным.
     */
    @Length(max=255)
    public String getUserCode()
    {
        return _userCode;
    }

    /**
     * @param userCode Пользовательский код. Свойство должно быть уникальным.
     */
    public void setUserCode(String userCode)
    {
        dirty(_userCode, userCode);
        _userCode = userCode;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MailServerSettingGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((MailServerSetting)another).getCode());
            }
            setUseDefault(((MailServerSetting)another).getUseDefault());
            setEnabled(((MailServerSetting)another).getEnabled());
            setMailSiteLink(((MailServerSetting)another).getMailSiteLink());
            setHost(((MailServerSetting)another).getHost());
            setMailAddress(((MailServerSetting)another).getMailAddress());
            setUsername(((MailServerSetting)another).getUsername());
            setPassword(((MailServerSetting)another).getPassword());
            setUseDebugMode(((MailServerSetting)another).getUseDebugMode());
            setDebugList(((MailServerSetting)another).getDebugList());
            setPort(((MailServerSetting)another).getPort());
            setUseSSL(((MailServerSetting)another).getUseSSL());
            setUserCode(((MailServerSetting)another).getUserCode());
            setTitle(((MailServerSetting)another).getTitle());
        }
    }

    public INaturalId<MailServerSettingGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<MailServerSettingGen>
    {
        private static final String PROXY_NAME = "MailServerSettingNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof MailServerSettingGen.NaturalId) ) return false;

            MailServerSettingGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MailServerSettingGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MailServerSetting.class;
        }

        public T newInstance()
        {
            return (T) new MailServerSetting();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "useDefault":
                    return obj.getUseDefault();
                case "enabled":
                    return obj.getEnabled();
                case "mailSiteLink":
                    return obj.getMailSiteLink();
                case "host":
                    return obj.getHost();
                case "mailAddress":
                    return obj.getMailAddress();
                case "username":
                    return obj.getUsername();
                case "password":
                    return obj.getPassword();
                case "useDebugMode":
                    return obj.getUseDebugMode();
                case "debugList":
                    return obj.getDebugList();
                case "port":
                    return obj.getPort();
                case "useSSL":
                    return obj.getUseSSL();
                case "userCode":
                    return obj.getUserCode();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "useDefault":
                    obj.setUseDefault((Boolean) value);
                    return;
                case "enabled":
                    obj.setEnabled((Boolean) value);
                    return;
                case "mailSiteLink":
                    obj.setMailSiteLink((String) value);
                    return;
                case "host":
                    obj.setHost((String) value);
                    return;
                case "mailAddress":
                    obj.setMailAddress((String) value);
                    return;
                case "username":
                    obj.setUsername((String) value);
                    return;
                case "password":
                    obj.setPassword((String) value);
                    return;
                case "useDebugMode":
                    obj.setUseDebugMode((Boolean) value);
                    return;
                case "debugList":
                    obj.setDebugList((String) value);
                    return;
                case "port":
                    obj.setPort((Integer) value);
                    return;
                case "useSSL":
                    obj.setUseSSL((Boolean) value);
                    return;
                case "userCode":
                    obj.setUserCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "useDefault":
                        return true;
                case "enabled":
                        return true;
                case "mailSiteLink":
                        return true;
                case "host":
                        return true;
                case "mailAddress":
                        return true;
                case "username":
                        return true;
                case "password":
                        return true;
                case "useDebugMode":
                        return true;
                case "debugList":
                        return true;
                case "port":
                        return true;
                case "useSSL":
                        return true;
                case "userCode":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "useDefault":
                    return true;
                case "enabled":
                    return true;
                case "mailSiteLink":
                    return true;
                case "host":
                    return true;
                case "mailAddress":
                    return true;
                case "username":
                    return true;
                case "password":
                    return true;
                case "useDebugMode":
                    return true;
                case "debugList":
                    return true;
                case "port":
                    return true;
                case "useSSL":
                    return true;
                case "userCode":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "useDefault":
                    return Boolean.class;
                case "enabled":
                    return Boolean.class;
                case "mailSiteLink":
                    return String.class;
                case "host":
                    return String.class;
                case "mailAddress":
                    return String.class;
                case "username":
                    return String.class;
                case "password":
                    return String.class;
                case "useDebugMode":
                    return Boolean.class;
                case "debugList":
                    return String.class;
                case "port":
                    return Integer.class;
                case "useSSL":
                    return Boolean.class;
                case "userCode":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MailServerSetting> _dslPath = new Path<MailServerSetting>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MailServerSetting");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Использовать по умолчанию. Свойство должно быть уникальным.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUseDefault()
     */
    public static PropertyPath<Boolean> useDefault()
    {
        return _dslPath.useDefault();
    }

    /**
     * @return Использовать настройку.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getEnabled()
     */
    public static PropertyPath<Boolean> enabled()
    {
        return _dslPath.enabled();
    }

    /**
     * @return Ссылка на сайт из писем.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getMailSiteLink()
     */
    public static PropertyPath<String> mailSiteLink()
    {
        return _dslPath.mailSiteLink();
    }

    /**
     * @return Хост почты. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getHost()
     */
    public static PropertyPath<String> host()
    {
        return _dslPath.host();
    }

    /**
     * @return Адрес почты.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getMailAddress()
     */
    public static PropertyPath<String> mailAddress()
    {
        return _dslPath.mailAddress();
    }

    /**
     * @return Имя почтового ящика (для авторизации).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUsername()
     */
    public static PropertyPath<String> username()
    {
        return _dslPath.username();
    }

    /**
     * @return Пароль почтового ящика (для авторизации).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getPassword()
     */
    public static PropertyPath<String> password()
    {
        return _dslPath.password();
    }

    /**
     * @return Отправка писем в режиме отладки (только на адреса из фильтра).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUseDebugMode()
     */
    public static PropertyPath<Boolean> useDebugMode()
    {
        return _dslPath.useDebugMode();
    }

    /**
     * @return Кому отсылать письма? (только в режиме отладки).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getDebugList()
     */
    public static PropertyPath<String> debugList()
    {
        return _dslPath.debugList();
    }

    /**
     * @return Порт SMTP сервера. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getPort()
     */
    public static PropertyPath<Integer> port()
    {
        return _dslPath.port();
    }

    /**
     * @return Использовать защищённые протоколы (SSL/TLS).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUseSSL()
     */
    public static PropertyPath<Boolean> useSSL()
    {
        return _dslPath.useSSL();
    }

    /**
     * @return Пользовательский код. Свойство должно быть уникальным.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUserCode()
     */
    public static PropertyPath<String> userCode()
    {
        return _dslPath.userCode();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends MailServerSetting> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _useDefault;
        private PropertyPath<Boolean> _enabled;
        private PropertyPath<String> _mailSiteLink;
        private PropertyPath<String> _host;
        private PropertyPath<String> _mailAddress;
        private PropertyPath<String> _username;
        private PropertyPath<String> _password;
        private PropertyPath<Boolean> _useDebugMode;
        private PropertyPath<String> _debugList;
        private PropertyPath<Integer> _port;
        private PropertyPath<Boolean> _useSSL;
        private PropertyPath<String> _userCode;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(MailServerSettingGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Использовать по умолчанию. Свойство должно быть уникальным.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUseDefault()
     */
        public PropertyPath<Boolean> useDefault()
        {
            if(_useDefault == null )
                _useDefault = new PropertyPath<Boolean>(MailServerSettingGen.P_USE_DEFAULT, this);
            return _useDefault;
        }

    /**
     * @return Использовать настройку.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getEnabled()
     */
        public PropertyPath<Boolean> enabled()
        {
            if(_enabled == null )
                _enabled = new PropertyPath<Boolean>(MailServerSettingGen.P_ENABLED, this);
            return _enabled;
        }

    /**
     * @return Ссылка на сайт из писем.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getMailSiteLink()
     */
        public PropertyPath<String> mailSiteLink()
        {
            if(_mailSiteLink == null )
                _mailSiteLink = new PropertyPath<String>(MailServerSettingGen.P_MAIL_SITE_LINK, this);
            return _mailSiteLink;
        }

    /**
     * @return Хост почты. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getHost()
     */
        public PropertyPath<String> host()
        {
            if(_host == null )
                _host = new PropertyPath<String>(MailServerSettingGen.P_HOST, this);
            return _host;
        }

    /**
     * @return Адрес почты.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getMailAddress()
     */
        public PropertyPath<String> mailAddress()
        {
            if(_mailAddress == null )
                _mailAddress = new PropertyPath<String>(MailServerSettingGen.P_MAIL_ADDRESS, this);
            return _mailAddress;
        }

    /**
     * @return Имя почтового ящика (для авторизации).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUsername()
     */
        public PropertyPath<String> username()
        {
            if(_username == null )
                _username = new PropertyPath<String>(MailServerSettingGen.P_USERNAME, this);
            return _username;
        }

    /**
     * @return Пароль почтового ящика (для авторизации).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getPassword()
     */
        public PropertyPath<String> password()
        {
            if(_password == null )
                _password = new PropertyPath<String>(MailServerSettingGen.P_PASSWORD, this);
            return _password;
        }

    /**
     * @return Отправка писем в режиме отладки (только на адреса из фильтра).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUseDebugMode()
     */
        public PropertyPath<Boolean> useDebugMode()
        {
            if(_useDebugMode == null )
                _useDebugMode = new PropertyPath<Boolean>(MailServerSettingGen.P_USE_DEBUG_MODE, this);
            return _useDebugMode;
        }

    /**
     * @return Кому отсылать письма? (только в режиме отладки).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getDebugList()
     */
        public PropertyPath<String> debugList()
        {
            if(_debugList == null )
                _debugList = new PropertyPath<String>(MailServerSettingGen.P_DEBUG_LIST, this);
            return _debugList;
        }

    /**
     * @return Порт SMTP сервера. Свойство не может быть null.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getPort()
     */
        public PropertyPath<Integer> port()
        {
            if(_port == null )
                _port = new PropertyPath<Integer>(MailServerSettingGen.P_PORT, this);
            return _port;
        }

    /**
     * @return Использовать защищённые протоколы (SSL/TLS).
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUseSSL()
     */
        public PropertyPath<Boolean> useSSL()
        {
            if(_useSSL == null )
                _useSSL = new PropertyPath<Boolean>(MailServerSettingGen.P_USE_S_S_L, this);
            return _useSSL;
        }

    /**
     * @return Пользовательский код. Свойство должно быть уникальным.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getUserCode()
     */
        public PropertyPath<String> userCode()
        {
            if(_userCode == null )
                _userCode = new PropertyPath<String>(MailServerSettingGen.P_USER_CODE, this);
            return _userCode;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.unimail.entity.catalog.MailServerSetting#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MailServerSettingGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return MailServerSetting.class;
        }

        public String getEntityName()
        {
            return "mailServerSetting";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
