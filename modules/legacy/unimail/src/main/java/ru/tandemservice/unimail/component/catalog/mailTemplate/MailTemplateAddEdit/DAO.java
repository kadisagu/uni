package ru.tandemservice.unimail.component.catalog.mailTemplate.MailTemplateAddEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unimail.entity.catalog.MessageType;
import ru.tandemservice.unimail.entity.catalog.MailContentType;
import ru.tandemservice.unimail.entity.catalog.MailTemplate;


public class DAO extends DefaultCatalogAddEditDAO<MailTemplate, Model> implements IDAO {
    @Override
    public void prepare(Model model) {
        super.prepare(model);
        Model modelExt = (Model) model;
        modelExt.setMailContentTypeList(new LazySimpleSelectModel<>(IUniBaseDao.instance.get().getCatalogItemList(MailContentType.class)));
        modelExt.setMessageTypeList(new LazySimpleSelectModel<>(IUniBaseDao.instance.get().getCatalogItemList(MessageType.class)));
    }
}
