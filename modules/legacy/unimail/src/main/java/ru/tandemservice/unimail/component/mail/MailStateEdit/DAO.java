package ru.tandemservice.unimail.component.mail.MailStateEdit;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unimail.entity.catalog.MailState;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setMailState(new MailState());
        model.setMailStateSelectModel(getMailStateSelectableModel());
    }

    private DQLFullCheckSelectModel getMailStateSelectableModel() {

        return new DQLFullCheckSelectModel("title") {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(MailState.class, alias)
                        .order(DQLExpressions.property(MailState.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(dql, alias, MailState.title(), filter);

                return dql;
            }
        };
    }

}
