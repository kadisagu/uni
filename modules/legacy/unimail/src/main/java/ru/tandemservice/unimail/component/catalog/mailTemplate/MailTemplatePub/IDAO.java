package ru.tandemservice.unimail.component.catalog.mailTemplate.MailTemplatePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.unimail.entity.catalog.MailTemplate;

public interface IDAO extends IDefaultCatalogPubDAO<MailTemplate, Model> {
}
