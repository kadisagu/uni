package ru.tandemservice.unimail.dao;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.util.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;

import java.util.Properties;


public class MailConfigurator extends UniBaseDao implements IMailConfigurator {

    private JavaMailSenderImpl mailSender;

    public JavaMailSender getDefaultMailSender() {
        return configureMailSender(getDefaultSetting());
    }

    @Override
    public JavaMailSender getPreparedMailSender(MailServerSetting mailServerSetting) {
        return configureMailSender(mailServerSetting);
    }

    @Override
    public MailServerSetting getDefaultSetting()
    {
        MailServerSetting retVal = get(MailServerSetting.class, MailServerSetting.useDefault(), Boolean.TRUE);
        if (retVal == null)
            throw new ApplicationException("Не найдены настройки почтового сервера с признаком 'Сервер по умолчанию'");
        return retVal;
    }

    private JavaMailSender configureMailSender(MailServerSetting mailServerSetting) {

        // рассылка отключена
        if (!mailServerSetting.getEnabled())
            return null;

        // инициализация sender-a
        mailSender = new JavaMailSenderImpl();

        String host = mailServerSetting.getHost();
        String name = mailServerSetting.getUsername();
        String password = mailServerSetting.getPassword();
        Integer port = mailServerSetting.getPort();

        if (!StringUtils.hasLength(name))
            name = null;

        if (!StringUtils.hasLength(password))
            password = null;


        mailSender.setHost(host);
        mailSender.setUsername(name);
        mailSender.setPassword(password);
        mailSender.setPort(port);
        mailSender.setDefaultEncoding("UTF-8");

        // если используется SSL/TLS
        Properties mailProps = new Properties();
        if (mailServerSetting.getUseSSL()) {
            mailProps.put("mail.smtps.auth", "true");
            mailProps.put("mail.smtp.starttls.enable", "true");
            mailSender.setJavaMailProperties(mailProps);
            mailSender.setProtocol("smtps");
        }

        return mailSender;
    }
}
