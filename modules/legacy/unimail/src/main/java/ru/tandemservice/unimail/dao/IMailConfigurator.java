package ru.tandemservice.unimail.dao;

import org.springframework.mail.javamail.JavaMailSender;
import ru.tandemservice.unimail.entity.catalog.MailServerSetting;

public interface IMailConfigurator {

    /**
     * Получить сконфигурированный JavaMailSender на основании настройки рассылки по умолчанию
     *
     * @return Сконфигурированный JavaMailSender, а если настройка рассылки отключена - null
     *
     * @throws Exception
     */
    public JavaMailSender getDefaultMailSender();

    /**
     * Сконфигурированный JavaMailSender
     *
     * @param mailServerSetting
     *
     * @return
     *
     * @throws Exception
     */
    public JavaMailSender getPreparedMailSender(MailServerSetting mailServerSetting);

    /**
     * Используемая настройка сервера для рассылки по умолчанию
     *
     * @return
     */
    public MailServerSetting getDefaultSetting();

}
