package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditController;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.uni.dao.UniDaoFacade;

public class Controller extends DefaultCatalogAddEditController<ExternalSystem, Model, IDAO> {
    public Controller() {

    }

    @Override
    public void onRefreshComponent(IBusinessComponent context) {
        super.onRefreshComponent(context);
        Model model = (Model) getModel(context);
        ((IDAO) getDao()).prepare(model);

        prepareDataSource(context);

    }

    public void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);

        if (model.getDataSource() != null) {
            return;
        }
        DynamicListDataSource<ExternalSystemWS> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSource(model, component1.getSettings());
        });
        dataSource.addColumn(new SimpleColumn("Тип Web службы", ExternalSystemWS.wsType().title()));
        dataSource.addColumn(new SimpleColumn("URL Web службы, включая WSDL", ExternalSystemWS.wsUrl()));
        dataSource.addColumn(new SimpleColumn("TargetNamespace Web службы", ExternalSystemWS.wsTargetNamespace()));
        dataSource.addColumn(new SimpleColumn("ServiceName", ExternalSystemWS.wsServiceName()));
        dataSource.addColumn(new SimpleColumn("ServicePort", ExternalSystemWS.wsServicePort()));
        dataSource.addColumn(new SimpleColumn("Тип прокси", ExternalSystemWS.wsProxyType().title()));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItemWs").setPermissionKey("editExternalSystemWS"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItemWs", "Удалить элемент «{0}» из справочника?", new Object[]{ExternalSystemWS.wsUrl()}).setPermissionKey("deleteExternalSystemWS"));

        model.setDataSource(dataSource);
    }

    public void onClickAddWs(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        activate(component, new ComponentActivator("ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemAddEdit.WsAddEdit", new ParametersMap().add("externalSystemId", model.getCatalogItem().getId())));
//    	component.createRegion("wsAddEdit", new ComponentActivator("ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemAddEdit.WsAddEdit" /*, new UniMap().add("externalSystemId", model.getCatalogItem().getId())*/));
    }

    public void onClickEditItemWs(IBusinessComponent component) {
        activate(component, new ComponentActivator("ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemAddEdit.WsAddEdit", new ParametersMap().add("externalSystemWsId", component.getListenerParameter())));
    }

    public void onClickDeleteItemWs(IBusinessComponent component) {
        UniDaoFacade.getCoreDao().delete((Long) component.getListenerParameter());

//    	((IDAO)getDao()).deleteRow(component);
        ((Model) getModel(component)).getDataSource().refresh();
    }
}
