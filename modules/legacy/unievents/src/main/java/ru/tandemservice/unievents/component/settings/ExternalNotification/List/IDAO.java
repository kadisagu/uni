package ru.tandemservice.unievents.component.settings.ExternalNotification.List;


import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    @Transactional
    public void confirmExtNotification(Model model);
}
