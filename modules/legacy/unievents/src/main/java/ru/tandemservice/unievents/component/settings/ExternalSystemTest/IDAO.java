package ru.tandemservice.unievents.component.settings.ExternalSystemTest;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void onTest(Model model);

    public void onForm(Model model) throws Exception;
}
