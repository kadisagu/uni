package ru.tandemservice.unievents.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


/**
 * Веб служба
 * отвечает за передачу событий об изменении объектов
 *
 * @author vch
 */
@WebService
public interface IEventsManager {

    /**
     * уведомить внешную систему о наступлении события
     *
     * @param userName
     * @param userPassword
     * @param eventId
     * @param entityId
     * @param eventType
     * @param entityType
     *
     * @return
     */
    @WebMethod
    public String RaiseEvents
    (
            @WebParam(name = "userName") String userName
            , @WebParam(name = "userPassword") String userPassword
            , @WebParam(name = "eventId") Long eventId
            , @WebParam(name = "entityId") String entityId
            , @WebParam(name = "eventType") String eventType
            , @WebParam(name = "entityType") String entityType
    ) throws Exception;

}
