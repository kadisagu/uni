package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

public interface IDAO extends IDefaultCatalogItemPubDAO<ExternalSystem, Model> {
}
