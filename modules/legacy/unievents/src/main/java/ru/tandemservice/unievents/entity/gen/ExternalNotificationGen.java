package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.ExternalNotification;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalNotificationEventType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Уведомления из внешних систем
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExternalNotificationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.ExternalNotification";
    public static final String ENTITY_NAME = "externalNotification";
    public static final int VERSION_HASH = 1522205644;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTITY_FOR_DAEMON_W_S = "entityForDaemonWS";
    public static final String P_DATE_NOTIFICATION = "dateNotification";
    public static final String P_EXTERNAL_ENTITY_ID = "externalEntityId";
    public static final String P_ENTITY_ID = "entityId";
    public static final String L_EXTERNAL_ENTITY_TYPE = "externalEntityType";
    public static final String P_NOTIFICATION = "notification";
    public static final String P_HAS_ACCEPT = "hasAccept";
    public static final String L_EXTERNAL_NOTIFICATION_EVENT_TYPE = "externalNotificationEventType";
    public static final String L_EMPLOYEE_POST = "employeePost";

    private EntityForDaemonWS _entityForDaemonWS;     // Изменения объектов для демона
    private Date _dateNotification;     // Дата уведомления
    private String _externalEntityId;     // ссылка на сущность из внешней системы
    private Long _entityId;     // ссылка на сущность (тандемовская)
    private ExternalEntityType _externalEntityType;     // Тип объекта внешней системы
    private String _notification;     // Текст уведомления
    private boolean _hasAccept;     // Пользователь прочитал уведомление
    private ExternalNotificationEventType _externalNotificationEventType;     // Тип события
    private EmployeePost _employeePost;     // Сотрудник, принявший уведомление

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Изменения объектов для демона.
     */
    public EntityForDaemonWS getEntityForDaemonWS()
    {
        return _entityForDaemonWS;
    }

    /**
     * @param entityForDaemonWS Изменения объектов для демона.
     */
    public void setEntityForDaemonWS(EntityForDaemonWS entityForDaemonWS)
    {
        dirty(_entityForDaemonWS, entityForDaemonWS);
        _entityForDaemonWS = entityForDaemonWS;
    }

    /**
     * @return Дата уведомления. Свойство не может быть null.
     */
    @NotNull
    public Date getDateNotification()
    {
        return _dateNotification;
    }

    /**
     * @param dateNotification Дата уведомления. Свойство не может быть null.
     */
    public void setDateNotification(Date dateNotification)
    {
        dirty(_dateNotification, dateNotification);
        _dateNotification = dateNotification;
    }

    /**
     * @return ссылка на сущность из внешней системы.
     */
    @Length(max=255)
    public String getExternalEntityId()
    {
        return _externalEntityId;
    }

    /**
     * @param externalEntityId ссылка на сущность из внешней системы.
     */
    public void setExternalEntityId(String externalEntityId)
    {
        dirty(_externalEntityId, externalEntityId);
        _externalEntityId = externalEntityId;
    }

    /**
     * @return ссылка на сущность (тандемовская).
     */
    public Long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId ссылка на сущность (тандемовская).
     */
    public void setEntityId(Long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Тип объекта внешней системы.
     */
    public ExternalEntityType getExternalEntityType()
    {
        return _externalEntityType;
    }

    /**
     * @param externalEntityType Тип объекта внешней системы.
     */
    public void setExternalEntityType(ExternalEntityType externalEntityType)
    {
        dirty(_externalEntityType, externalEntityType);
        _externalEntityType = externalEntityType;
    }

    /**
     * @return Текст уведомления.
     */
    public String getNotification()
    {
        return _notification;
    }

    /**
     * @param notification Текст уведомления.
     */
    public void setNotification(String notification)
    {
        dirty(_notification, notification);
        _notification = notification;
    }

    /**
     * @return Пользователь прочитал уведомление. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasAccept()
    {
        return _hasAccept;
    }

    /**
     * @param hasAccept Пользователь прочитал уведомление. Свойство не может быть null.
     */
    public void setHasAccept(boolean hasAccept)
    {
        dirty(_hasAccept, hasAccept);
        _hasAccept = hasAccept;
    }

    /**
     * @return Тип события.
     */
    public ExternalNotificationEventType getExternalNotificationEventType()
    {
        return _externalNotificationEventType;
    }

    /**
     * @param externalNotificationEventType Тип события.
     */
    public void setExternalNotificationEventType(ExternalNotificationEventType externalNotificationEventType)
    {
        dirty(_externalNotificationEventType, externalNotificationEventType);
        _externalNotificationEventType = externalNotificationEventType;
    }

    /**
     * @return Сотрудник, принявший уведомление.
     */
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник, принявший уведомление.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExternalNotificationGen)
        {
            setEntityForDaemonWS(((ExternalNotification)another).getEntityForDaemonWS());
            setDateNotification(((ExternalNotification)another).getDateNotification());
            setExternalEntityId(((ExternalNotification)another).getExternalEntityId());
            setEntityId(((ExternalNotification)another).getEntityId());
            setExternalEntityType(((ExternalNotification)another).getExternalEntityType());
            setNotification(((ExternalNotification)another).getNotification());
            setHasAccept(((ExternalNotification)another).isHasAccept());
            setExternalNotificationEventType(((ExternalNotification)another).getExternalNotificationEventType());
            setEmployeePost(((ExternalNotification)another).getEmployeePost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExternalNotificationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExternalNotification.class;
        }

        public T newInstance()
        {
            return (T) new ExternalNotification();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityForDaemonWS":
                    return obj.getEntityForDaemonWS();
                case "dateNotification":
                    return obj.getDateNotification();
                case "externalEntityId":
                    return obj.getExternalEntityId();
                case "entityId":
                    return obj.getEntityId();
                case "externalEntityType":
                    return obj.getExternalEntityType();
                case "notification":
                    return obj.getNotification();
                case "hasAccept":
                    return obj.isHasAccept();
                case "externalNotificationEventType":
                    return obj.getExternalNotificationEventType();
                case "employeePost":
                    return obj.getEmployeePost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityForDaemonWS":
                    obj.setEntityForDaemonWS((EntityForDaemonWS) value);
                    return;
                case "dateNotification":
                    obj.setDateNotification((Date) value);
                    return;
                case "externalEntityId":
                    obj.setExternalEntityId((String) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "externalEntityType":
                    obj.setExternalEntityType((ExternalEntityType) value);
                    return;
                case "notification":
                    obj.setNotification((String) value);
                    return;
                case "hasAccept":
                    obj.setHasAccept((Boolean) value);
                    return;
                case "externalNotificationEventType":
                    obj.setExternalNotificationEventType((ExternalNotificationEventType) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityForDaemonWS":
                        return true;
                case "dateNotification":
                        return true;
                case "externalEntityId":
                        return true;
                case "entityId":
                        return true;
                case "externalEntityType":
                        return true;
                case "notification":
                        return true;
                case "hasAccept":
                        return true;
                case "externalNotificationEventType":
                        return true;
                case "employeePost":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityForDaemonWS":
                    return true;
                case "dateNotification":
                    return true;
                case "externalEntityId":
                    return true;
                case "entityId":
                    return true;
                case "externalEntityType":
                    return true;
                case "notification":
                    return true;
                case "hasAccept":
                    return true;
                case "externalNotificationEventType":
                    return true;
                case "employeePost":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityForDaemonWS":
                    return EntityForDaemonWS.class;
                case "dateNotification":
                    return Date.class;
                case "externalEntityId":
                    return String.class;
                case "entityId":
                    return Long.class;
                case "externalEntityType":
                    return ExternalEntityType.class;
                case "notification":
                    return String.class;
                case "hasAccept":
                    return Boolean.class;
                case "externalNotificationEventType":
                    return ExternalNotificationEventType.class;
                case "employeePost":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExternalNotification> _dslPath = new Path<ExternalNotification>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExternalNotification");
    }
            

    /**
     * @return Изменения объектов для демона.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getEntityForDaemonWS()
     */
    public static EntityForDaemonWS.Path<EntityForDaemonWS> entityForDaemonWS()
    {
        return _dslPath.entityForDaemonWS();
    }

    /**
     * @return Дата уведомления. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getDateNotification()
     */
    public static PropertyPath<Date> dateNotification()
    {
        return _dslPath.dateNotification();
    }

    /**
     * @return ссылка на сущность из внешней системы.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getExternalEntityId()
     */
    public static PropertyPath<String> externalEntityId()
    {
        return _dslPath.externalEntityId();
    }

    /**
     * @return ссылка на сущность (тандемовская).
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Тип объекта внешней системы.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getExternalEntityType()
     */
    public static ExternalEntityType.Path<ExternalEntityType> externalEntityType()
    {
        return _dslPath.externalEntityType();
    }

    /**
     * @return Текст уведомления.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getNotification()
     */
    public static PropertyPath<String> notification()
    {
        return _dslPath.notification();
    }

    /**
     * @return Пользователь прочитал уведомление. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#isHasAccept()
     */
    public static PropertyPath<Boolean> hasAccept()
    {
        return _dslPath.hasAccept();
    }

    /**
     * @return Тип события.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getExternalNotificationEventType()
     */
    public static ExternalNotificationEventType.Path<ExternalNotificationEventType> externalNotificationEventType()
    {
        return _dslPath.externalNotificationEventType();
    }

    /**
     * @return Сотрудник, принявший уведомление.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    public static class Path<E extends ExternalNotification> extends EntityPath<E>
    {
        private EntityForDaemonWS.Path<EntityForDaemonWS> _entityForDaemonWS;
        private PropertyPath<Date> _dateNotification;
        private PropertyPath<String> _externalEntityId;
        private PropertyPath<Long> _entityId;
        private ExternalEntityType.Path<ExternalEntityType> _externalEntityType;
        private PropertyPath<String> _notification;
        private PropertyPath<Boolean> _hasAccept;
        private ExternalNotificationEventType.Path<ExternalNotificationEventType> _externalNotificationEventType;
        private EmployeePost.Path<EmployeePost> _employeePost;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Изменения объектов для демона.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getEntityForDaemonWS()
     */
        public EntityForDaemonWS.Path<EntityForDaemonWS> entityForDaemonWS()
        {
            if(_entityForDaemonWS == null )
                _entityForDaemonWS = new EntityForDaemonWS.Path<EntityForDaemonWS>(L_ENTITY_FOR_DAEMON_W_S, this);
            return _entityForDaemonWS;
        }

    /**
     * @return Дата уведомления. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getDateNotification()
     */
        public PropertyPath<Date> dateNotification()
        {
            if(_dateNotification == null )
                _dateNotification = new PropertyPath<Date>(ExternalNotificationGen.P_DATE_NOTIFICATION, this);
            return _dateNotification;
        }

    /**
     * @return ссылка на сущность из внешней системы.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getExternalEntityId()
     */
        public PropertyPath<String> externalEntityId()
        {
            if(_externalEntityId == null )
                _externalEntityId = new PropertyPath<String>(ExternalNotificationGen.P_EXTERNAL_ENTITY_ID, this);
            return _externalEntityId;
        }

    /**
     * @return ссылка на сущность (тандемовская).
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(ExternalNotificationGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Тип объекта внешней системы.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getExternalEntityType()
     */
        public ExternalEntityType.Path<ExternalEntityType> externalEntityType()
        {
            if(_externalEntityType == null )
                _externalEntityType = new ExternalEntityType.Path<ExternalEntityType>(L_EXTERNAL_ENTITY_TYPE, this);
            return _externalEntityType;
        }

    /**
     * @return Текст уведомления.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getNotification()
     */
        public PropertyPath<String> notification()
        {
            if(_notification == null )
                _notification = new PropertyPath<String>(ExternalNotificationGen.P_NOTIFICATION, this);
            return _notification;
        }

    /**
     * @return Пользователь прочитал уведомление. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#isHasAccept()
     */
        public PropertyPath<Boolean> hasAccept()
        {
            if(_hasAccept == null )
                _hasAccept = new PropertyPath<Boolean>(ExternalNotificationGen.P_HAS_ACCEPT, this);
            return _hasAccept;
        }

    /**
     * @return Тип события.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getExternalNotificationEventType()
     */
        public ExternalNotificationEventType.Path<ExternalNotificationEventType> externalNotificationEventType()
        {
            if(_externalNotificationEventType == null )
                _externalNotificationEventType = new ExternalNotificationEventType.Path<ExternalNotificationEventType>(L_EXTERNAL_NOTIFICATION_EVENT_TYPE, this);
            return _externalNotificationEventType;
        }

    /**
     * @return Сотрудник, принявший уведомление.
     * @see ru.tandemservice.unievents.entity.ExternalNotification#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

        public Class getEntityClass()
        {
            return ExternalNotification.class;
        }

        public String getEntityName()
        {
            return "externalNotification";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
