package ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.AddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.LinkWrapper;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

@Input({
        @org.tandemframework.core.component.Bind(key = "linkId", binding = "link.id"),
        @org.tandemframework.core.component.Bind(key = "meta", binding = "meta"),
        @org.tandemframework.core.component.Bind(key = "externalSystem", binding = "externalSystem")

})
public class Model {
    private ExternalEntitiesMap link = new ExternalEntitiesMap();
    private IEntityMeta meta;

    private ISelectModel entityModel;
    private LinkWrapper entityWrapper;

    private ExternalSystem externalSystem;

    public ExternalEntitiesMap getLink() {
        return link;
    }

    public void setLink(ExternalEntitiesMap link) {
        this.link = link;
    }

    public IEntityMeta getMeta() {
        return meta;
    }

    public void setMeta(IEntityMeta meta) {
        this.meta = meta;
    }

    public LinkWrapper getEntityWrapper() {
        return entityWrapper;
    }

    public void setEntityWrapper(LinkWrapper entityWrapper) {
        this.entityWrapper = entityWrapper;
    }

    public ISelectModel getEntityModel() {
        return entityModel;
    }

    public void setEntityModel(ISelectModel entityModel) {
        this.entityModel = entityModel;
    }

    public ExternalSystem getExternalSystem() {
        return externalSystem;
    }

    public void setExternalSystem(ExternalSystem externalSystem) {
        this.externalSystem = externalSystem;
    }

}
