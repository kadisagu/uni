package ru.tandemservice.unievents.ws.bean;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.bo.OrgUnit.OrgUnitManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessEventManagerBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProcessEventBean_OrgUnit extends BaseProcessEventBean {
    public static final String BEAN_NAME = "eventManagerBean.tsogu_ou.OrgUnit1.0";

    public static final String ELEMENT_ID = "OrgUnitId";
    public static final String ELEMENT_PARENTID = "OrgUnitParentId";
    public static final String ELEMENT_TITLE = "OrgUnitTitle";
    public static final String ELEMENT_SHORTTITLE = "OrgUnitShortTitle";
    public static final String ELEMENT_TYPECODE = "OrgUnitTypeCode";
    public static final String ELEMENT_TYPEID = "OrgUnitTypeId";
    public static final String ELEMENT_INTCODE = "OrgUnitInternalCode";


    @Override
    protected IRequest createRequest(EntityForDaemonWS event) {
        return null;
    }

    public static String getEventTitle(String eventType) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("changeTitle", "Изменение наименования подразделения");
        map.put("toDelete", "Пометка подразделения на удаление");
        map.put("fromDelete", "Снятие отметки на удаление подразделения");
        map.put("afterDelete", "Удаление подразделения");
        map.put("changeParent", "Изменение родителя подразделения");
        map.put("afterInsert", "Добавление подразделения");

        return map.get(eventType);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void doSaveEntity(IProcessEventManagerBean.SaveData saveData, IProcessEventManagerBean.BodyElement entityInfo, ExternalSystem externalSystem) {
        //сохраним orgUnit через его дао
        OrgUnit orgUnit = (OrgUnit) saveData.getMainEntity();
        OrgUnitManager.instance().dao().saveOrUpdateOrgUnit(orgUnit);
        saveData.getItems().remove(orgUnit);

        super.doSaveEntity(saveData, entityInfo, externalSystem);
    }

    @Override
    protected IEntity findEntity(IProcessEventManagerBean.BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId) {
        String typeId = entityInfo.find(ELEMENT_TYPEID).getValue();
        String typeCode = entityInfo.find(ELEMENT_TYPECODE).getValue();
        OrgUnitType ouType = getOrgUnitType(typeId, typeCode, externalSystem);
        if (ouType == null)
            throw new ApplicationException("Не найден тип подразделения OrgUnitTypeId = " + typeId + ", OrgUnitTypeCode = " + typeCode);

        OrgUnit parent = findCatalogItem(OrgUnit.class, entityInfo.find(ELEMENT_PARENTID).getValue(), externalSystem);
        if (parent == null)
            throw new ApplicationException("Не найдено родительское подразделение OrgUnitParentId = " + entityInfo.find(ELEMENT_PARENTID).getValue());

        MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", OrgUnit.parent(), parent))
                .add(MQExpression.eq("e", OrgUnit.title(), entityInfo.find(ELEMENT_TITLE).getValue()))
                .add(MQExpression.eq("e", OrgUnit.orgUnitType(), ouType));

        List<OrgUnit> list = getList(builder);
        return (list.size() == 1) ? list.get(0) : null;
    }

    @Override
    protected IProcessEventManagerBean.SaveData fillEntity(IEntity entityTU, IProcessEventManagerBean.BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId) {
        OrgUnit orgUnit = (OrgUnit) entityTU;
        IProcessEventManagerBean.SaveData saveData = new IProcessEventManagerBean.SaveData();

        String title = entityInfo.find(ELEMENT_TITLE).getValue();
        OrgUnit parent = findCatalogItem(OrgUnit.class, entityInfo.find(ELEMENT_PARENTID).getValue(), externalSystem);
        if (parent == null)
            throw new ApplicationException("Не найдено родительское подразделение OrgUnitParentId = " + entityInfo.find(ELEMENT_PARENTID).getValue());

        String typeId = entityInfo.find(ELEMENT_TYPEID).getValue();
        String typeCode = entityInfo.find(ELEMENT_TYPECODE).getValue();
        OrgUnitType ouType = getOrgUnitType(typeId, typeCode, externalSystem);
        if (ouType == null)
            throw new ApplicationException("Не найден тип подразделения OrgUnitTypeId = " + typeId + ", OrgUnitTypeCode = " + typeCode);

        if (orgUnit == null) {
            orgUnit = new OrgUnit();

            orgUnit.setArchival(false);

            orgUnit.setFullTitle(title + " (" + ouType.getTitle() + ")");
            orgUnit.setTerritorialTitle(title);
            orgUnit.setTerritorialFullTitle(title);
            orgUnit.setTerritorialShortTitle(title);

            if (StringUtils.isEmpty(entityInfo.find(ELEMENT_SHORTTITLE).getValue()))
                orgUnit.setShortTitle(title);

            if (StringUtils.isEmpty(entityInfo.find(ELEMENT_INTCODE).getValue())
                    && ("institute".equals(ouType.getCode()) || "faculty".equals(ouType.getCode()))
                    )
                throw new ApplicationException("Не указан внутренний код подразделения");
        }

        orgUnit.setParent(parent);
        orgUnit.setOrgUnitType(ouType);
        orgUnit.setTitle(title);

        if (!StringUtils.isEmpty(entityInfo.find(ELEMENT_SHORTTITLE).getValue()))
            orgUnit.setShortTitle(entityInfo.find(ELEMENT_SHORTTITLE).getValue());

        if (!StringUtils.isEmpty(entityInfo.find(ELEMENT_INTCODE).getValue()))
            orgUnit.setDivisionCode(entityInfo.find(ELEMENT_INTCODE).getValue());

        saveData.setMainEntity(orgUnit);
        saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(orgUnit, false));

        return saveData;
    }

    @Override
    protected String getAnswerBeanName() {
        return BEAN_NAME;
    }

    @Override
    protected Class<? extends IEntity> getEntityClass() {
        return OrgUnit.class;
    }

    @Override
    protected String getExternalId(IProcessEventManagerBean.BodyElement entityInfo, String forRequestId) {
        return entityInfo.find(ELEMENT_ID).getValue();
    }

    private OrgUnitType getOrgUnitType(String externalId, String code, ExternalSystem externalSystem) {
        try {
            OrgUnitType ou = findCatalogItem(OrgUnitType.class, externalId, externalSystem);
            if (ou != null)
                return ou;
        }
        catch (Exception ex) {
        }

        return getCatalogItem(OrgUnitType.class, code);
    }
}
