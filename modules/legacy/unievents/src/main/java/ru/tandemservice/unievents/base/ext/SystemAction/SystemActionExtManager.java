package ru.tandemservice.unievents.base.ext.SystemAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unievents.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager {


    public static SystemActionExtManager instance() {
        return instance(SystemActionExtManager.class);
    }

    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("unievents_checkBeanPrefics", new SystemActionDefinition("unievents", "checkBeanPrefics", "onCheckBeanPrefics", SystemActionPubExt.SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }

}
