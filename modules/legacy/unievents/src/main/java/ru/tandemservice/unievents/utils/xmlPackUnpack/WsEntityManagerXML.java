package ru.tandemservice.unievents.utils.xmlPackUnpack;

import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Модель для построения запросов/ответов для
 * WS EntityManager
 *
 * @author vch
 */
public abstract class WsEntityManagerXML
        implements ILogin, IXmlPackUnpack
{

    public abstract List<IRequest> getRequests();

    public abstract void addRequests(List<IRequest> requests);

    public void addRequests(IRequest request)
    {
        List<IRequest> al = new ArrayList<IRequest>();
        al.add(request);
        addRequests(al);
    }

    public abstract List<IResponse> getResponses();

    public abstract void addResponses(List<IResponse> responses);

    public void addResponses(IResponse response)
    {
        List<IResponse> al = new ArrayList<IResponse>();
        al.add(response);
        addResponses(al);
    }

    public abstract List<IError> getErrors();

    /**
     * Добавить ошибку
     *
     * @param error
     */
    public abstract void addError(IError error);

    /**
     * Очистить сведения об ошибках
     */
    public abstract void clearError();


}
