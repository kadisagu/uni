package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemAddEdit.WsAddEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.WsProxyType;
import ru.tandemservice.unievents.entity.catalog.WsType;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {

        model.setWsTypeModel(new LazySimpleSelectModel<WsType>(WsType.class));
        model.setWsProxyTypeModel(new LazySimpleSelectModel<WsProxyType>(WsProxyType.class));

        if (model.getExternalSystemWsId() != null)
            model.setExternalSystemWS(getNotNull(ExternalSystemWS.class, model.getExternalSystemWsId()));
        else
            model.setExternalSystemWS(new ExternalSystemWS());
        if (model.getExternalSystemId() != null) {
            model.setExternalSystem(getNotNull(ExternalSystem.class, model.getExternalSystemId()));
            model.getExternalSystemWS().setExternalSystem(model.getExternalSystem());
        }
    }

    @Override
    public void update(Model model) {

        saveOrUpdate(model.getExternalSystemWS());
    }
}
