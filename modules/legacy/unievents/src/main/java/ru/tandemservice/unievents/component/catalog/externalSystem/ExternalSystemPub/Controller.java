package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemPub;

import jxl.write.WriteException;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

import java.io.IOException;

public class Controller extends DefaultCatalogPubController<ExternalSystem, Model, IDAO> {


    @Override
    public void onRefreshComponent(IBusinessComponent context) {
        super.onRefreshComponent(context);
        Model model = (Model) getModel(context);
        prepareListDataSource(model, context);
    }

    public void onClickPrintElements(IBusinessComponent component) throws IOException, WriteException
    {
        byte[] content = getDao().prepareForPrint(getModel(component));
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("CatalogItems.xls").document(content), false);
    }

    private void prepareListDataSource(final Model model, final IBusinessComponent context)
    {
        if (model.getDs() == null) {
            DynamicListDataSource<Wrapper> dataSource = new DynamicListDataSource<>(context, this);
//             Model model = context.getModel();

            //колонка публикатора:
            dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", "externalSystem.title"));

            //кастомные колонки:

            // <TODO> рефактор
             /*
             dataSource.addColumn(new SimpleColumn("URL Веб службы", ExternalSystem.P_WS_URL));
             dataSource.addColumn(new SimpleColumn("TargetNamespace", ExternalSystem.P_WS_TARGET_NAMESPACE));
             dataSource.addColumn(new SimpleColumn("ServiceName", ExternalSystem.P_WS_SERVICE_NAME));
             dataSource.addColumn(new SimpleColumn("ServicePort", ExternalSystem.P_WS_SERVICE_PORT));
             */
            // dataSource.addColumn(new SimpleColumn("Имя пользователя", ));
            dataSource.addColumn(new BooleanColumn("Используется", "externalSystem.useThisExternalSystem"));
            dataSource.addColumn(new SimpleColumn("Префик bean", "externalSystem.beanPref"));
            dataSource.addColumn(new SimpleColumn("Web службы", "ws"));


            //action-колонки:
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
            if (model.isUserCatalog()) {
                dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", new Object[]{"externalSystem.title"}).setPermissionKey(model.getCatalogItemDelete()));
            }

            model.setDs(dataSource);
        }
    }


}
