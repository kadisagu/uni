package ru.tandemservice.unievents.ws.bean.interfaces;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Обработка очереди сообщений от внешней системы
 *
 * @author vch
 */
public interface IProcessEventManagerBean
{

    public static final String SYNC_ANSWER_TYPE = "sync";
    public static final String ASYNC_ANSWER_TYPE = "async";

    /**
     * Обработать событие
     * Может быть сформирован запроса во внешную систему
     *
     * @param event
     */
    public XmlDocWS processEvent(EntityForDaemonWS event) throws Exception;

    /**
     * Разбор ответа внешней системы
     * userName, password, requests взяты из запроса во внешную систему
     * могут использоваться при реализации логики
     *
     * @param xdoc
     * @param userName
     * @param password
     * @param response
     */
    public List<SaveData> processAnswer
    (
            XmlDocWS xdoc
            , String userName
            , String password
            , List<IRequest> requestList
            , IResponse response
    ) throws Exception;

	
	/*
    public String getBeanName();
	public void setBeanName(String beanName);
	*/

    public static enum EventType
    {
        INSERT, UPDATE, DELETE
    }

    public static class BodyElement
    {
        String name;
        String value;

        List<BodyElement> children = new ArrayList<>();

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }

        public List<BodyElement> getChildren()
        {
            return children;
        }

        public void setChildren(List<BodyElement> children)
        {
            this.children = children;
        }

        public boolean hasChildren()
        {
            return children != null && children.size() > 0;
        }

        public boolean isExists()
        {
            return name != null;
        }

        public BodyElement find(String elementName, boolean errorUpIfEmpty)
        {
            for (BodyElement element : children)
                if (element.getName().toLowerCase().equals(elementName.toLowerCase()))
                    return element;

            if (errorUpIfEmpty)
                throw new ApplicationException("В документе не найден атрибут с именем " + elementName);
            else
                return new BodyElement();
        }

        public BodyElement find(String elementName)
        {
            return find(elementName, false);
        }

        public BodyElement findNotNull(String elementName)
        {
            return find(elementName, true);
        }


        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder().append(name).append(": ");

            if (hasChildren())
            {
                sb.append("[\n");

                List<String> childList = new ArrayList<>();
                for (BodyElement child : children)
                    childList.add(child.toString());

                sb.append(StringUtils.join(childList, "\n"));
                sb.append("\n]");

            } else
                sb.append(value);

            return sb.toString();
        }

        /**
         * пустой атрибут
         *
         * @return
         */
        public boolean isEmpty()
        {
            if (name == null && children.isEmpty())
                return true;
            else
                return false;
        }
    }

    public static class SaveData
    {
        private IEntity mainEntity;
        private boolean saveLink = false;
        private String externalId;
        private List<SaveDataItem> items = new ArrayList<>();

        public String getExternalId()
        {
            return externalId;
        }

        public void setExternalId(String externalId)
        {
            this.externalId = externalId;
        }

        public IEntity getMainEntity()
        {
            return mainEntity;
        }

        public void setMainEntity(IEntity mainEntity)
        {
            this.mainEntity = mainEntity;
        }

        public boolean isSaveLink()
        {
            return saveLink;
        }

        public void setSaveLink(boolean saveLink)
        {
            this.saveLink = saveLink;
        }

        public List<SaveDataItem> getItems()
        {
            return items;
        }

        public void setItems(List<SaveDataItem> items)
        {
            this.items = items;
        }
    }

    public static class SaveDataItem
    {
        private IEntity entity;
        private boolean delete = false;

        /**
         * @param entity
         * @param delete
         */
        public SaveDataItem(IEntity entity, boolean delete)
        {
            this.entity = entity;
            this.delete = delete;
        }

        public IEntity getEntity()
        {
            return entity;
        }

        public void setEntity(IEntity entity)
        {
            this.entity = entity;
        }

        public boolean isDelete()
        {
            return delete;
        }

        public void setDelete(boolean delete)
        {
            this.delete = delete;
        }
    }
}
