package ru.tandemservice.unievents.component.settings.ExternalSystemTest;

import com.google.common.collect.ImmutableList;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.logging.bo.EventLog.util.Catalog;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.ws.IEntityManager;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessBean;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {


    public static final Long SEND_REQUEST = 0L;
    public static final Long SEND_ANSWER = 1L;

    public static final Long SYNC = 0L;
    public static final Long ASYNC = 1L;

    @Override
    public void prepare(final Model model) {
        model.setMethodModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(SEND_REQUEST, "sendRequest"),
                new IdentifiableWrapper(SEND_ANSWER, "sendAnswer"))));
        model.setExternalSystemModel(new UniQueryFullCheckSelectModel(ExternalSystem.title().s()) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(ExternalSystem.ENTITY_CLASS, alias)
                        .addOrder(alias, ExternalSystem.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, ExternalSystem.title(), filter));

                return builder;
            }
        });

        model.setEntityMetaModel(new BaseSingleSelectModel() {
            @Override
            public ListResult<Model.MetaWrapper> findValues(String filter) {
                List<IEntityMeta> list = Catalog.getInstance().getEntityTypes();

                List<Model.MetaWrapper> resultList = new ArrayList<>();
                for (IEntityMeta meta : list) {
                    if (meta.isAbstract() || meta.isInterface())
                        continue;

                    if (!StringUtils.isEmpty(filter) && !meta.getName().contains(filter) && !meta.getTitle().contains(filter))
                        continue;

                    Model.MetaWrapper wrapper = new Model.MetaWrapper(meta);
                    resultList.add(wrapper);
                }

                return new ListResult<>(resultList);
            }

            @Override
            public Object getValue(Object obj) {
                short code = Short.parseShort("" + obj);
                if (obj != null)
                    return new Model.MetaWrapper(EntityRuntime.getMeta(code));
                return null;
            }
        });

        model.setEntityModel(new BaseSingleSelectModel() {
            @Override
            public ListResult<EntityWrapper> findValues(String filter) {
                List<EntityWrapper> result = new ArrayList<>();
                Model.MetaWrapper meta = model.getSettings().get("entityName");
                if (meta != null) {
                    List list = getList(meta.getMeta().getEntityClass());
                    for (Object entity : list) {
                        EntityWrapper wrapper = new EntityWrapper((IEntity) entity);
                        if (!StringUtils.isEmpty(filter) && !wrapper.getTitle().contains(filter))
                            continue;

                        result.add(wrapper);
                    }
                }
                return new ListResult<>(result);
            }

            @Override
            public Object getValue(Object obj) {
                if (obj == null)
                    return null;

                IEntity entity = getNotNull((Long) obj);
                return new EntityWrapper(entity);
            }
        });
        model.setTypeModel(new LazySimpleSelectModel<>(ImmutableList.of(
                new IdentifiableWrapper(SYNC, "sync"),
                new IdentifiableWrapper(ASYNC, "async"))));
    }

    public void onForm(Model model) throws Exception {
        IDataSettings settings = model.getSettings();
        Model.MetaWrapper meta = (Model.MetaWrapper) settings.get("entityName");
        EntityWrapper entityWrapper = (EntityWrapper) settings.get("entityWrapper");
        ExternalSystem externalSystem = (ExternalSystem) settings.get("externalSyst");
        String deepth = (String) settings.get("deepth");
        String beanName = (String) settings.get("beanName");
        IdentifiableWrapper type = (IdentifiableWrapper) settings.get("type");

        String filterProperty = (String) settings.get("filterProperty");
        String filterValue = (String) settings.get("filterValue");
        String pageNumber = (String) settings.get("pageNumber");

        StringBuilder parameter = new StringBuilder();

        // entityType зависит от типа данных, заполняем всегда
        {
            String entityType = loadXML("parameter.xml");
            parameter.append(entityType.replace("$insertName$", "EntityType").replace("$insertValue$", String.valueOf(meta.getMeta().getName())));
        }

        // если выбран экземпляр объекта, то указываем параметры запроса
        if (entityWrapper != null) {
            String entityId = loadXML("parameter.xml");
            parameter.append(entityId.replace("$insertName$", "EntityId").replace("$insertValue$", String.valueOf(entityWrapper.getId())));
        }
        else {
            // если экземпляр объекта не выбран, то укажем ?
            String entityId = loadXML("parameter.xml");
            parameter.append(entityId.replace("$insertName$", "EntityId").replace("$insertValue$", "?"));

        }

        if (deepth != null) {
            String deepthXML = loadXML("parameter.xml");
            parameter.append("\n").append(deepthXML.replace("$insertName$", "deepth").replace("$insertValue$", deepth));
        }

        //номер страницы
        if (!StringUtils.isEmpty(pageNumber)) {
            String xml = loadXML("parameter.xml");
            parameter.append("\n").append(xml.replace("$insertName$", StringUtils.capitalize(IProcessBean.PARAM_PAGE)).replace("$insertValue$", pageNumber));
        }

        //имя свойства для фильтра
        if (!StringUtils.isEmpty(filterProperty)) {
            String xml = loadXML("parameter.xml");
            parameter.append("\n").append(xml.replace("$insertName$", StringUtils.capitalize(IProcessBean.PARAM_FILTER_PROPERTY)).replace("$insertValue$", filterProperty));
        }

        //значение свойства для фильтра
        if (!StringUtils.isEmpty(filterValue)) {
            String xml = loadXML("parameter.xml");
            parameter.append("\n").append(xml.replace("$insertName$", StringUtils.capitalize(IProcessBean.PARAM_FILTER_VALUE)).replace("$insertValue$", filterValue));
        }

        String xml = loadXML("inputXML.xml");

        settings.set("inputXML", xml.replace("$user$", externalSystem.getWsUserName())
                .replace("$psw$", externalSystem.getWsPassword())
                .replace("$beanName$", beanName)
                .replace("$type$", type.getId().equals(SYNC) ? "sync" : "async")
                .replace("$par$", parameter));

    }

    private String loadXML(String file) throws Exception {
        InputStream in = this.getClass().getResourceAsStream(file);
        String result = IOUtils.toString(in);
        in.close();
        return result;
    }

    public void onTest(Model model) {
        IDataSettings settings = model.getSettings();
        ExternalSystem externalSystem = (ExternalSystem) settings.get("externalSystem");
        IdentifiableWrapper method = (IdentifiableWrapper) settings.get("method");
        String inputXML = (String) settings.get("inputXML");

        try {
            IEntityManager rmi = DaoFacade.getWebClientManager().getEntityManager(externalSystem);
            if (method.getId().equals(SEND_REQUEST))
                model.setOutputXML(rmi.sendRequest(inputXML));
            else if (method.getId().equals(SEND_ANSWER))
                model.setOutputXML(rmi.sendAnswer(inputXML));

        }
        catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationException(e.getMessage());
        }
    }
}
