package ru.tandemservice.unievents.base.bo.EntityForDaemonWS.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unievents.base.bo.EntityForDaemonWS.logic.DaemonWsLogDSHandler;
import ru.tandemservice.unievents.entity.DaemonWsLog;

@Configuration
public class EntityForDaemonWSPub extends BusinessComponentManager {
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS("daemonWsLogDS", daemonWsLogDS(), daemonWsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint daemonWsLogDS()
    {
        return columnListExtPointBuilder("daemonWsLogDS")
                .addColumn(publisherColumn("date", DaemonWsLog.logDate()).width("10%").formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
                .addColumn(textColumn("status", DaemonWsLog.statusDaemonProcessingWs().title().s()).order().create())
                .addColumn(textColumn("errorComment", DaemonWsLog.errorComment().s()).order().create()).create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> daemonWsDSHandler()
    {
        return new DaemonWsLogDSHandler(getName());
    }
}

