package ru.tandemservice.unievents.dao;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IError;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;

import javax.annotation.Nullable;
import java.util.List;

public interface IEntityManagerDao
{

    String GLOBAL_DAEMON_LOCK = IEntityManagerDao.class.getName() + ".global-lock";
    SpringBeanCache<IEntityManagerDao> instance = new SpringBeanCache<>(IEntityManagerDao.class.getName());
    /**
     * Регистрируем асинхронный запрос
     */
    @Transactional
    void addRequestAsync(boolean isInternal, String beanName, List<IParameter> params, String requestId, String userName, String userPass);

    /**
     * Обработать запрос
     */
    IResponse processRequest(String beanName, List<IParameter> paramList, String requestId, List<IError> errorList, String userName, String password);

    @Transactional
    void registerResult(Long id, @Nullable String xml, List<IError> errorList, Exception ex, String userName, String userPass);

    @Transactional
    void loadAndCreateJobs();

    @Transactional
    void saveLog(boolean internal, String method, String requestXml, String responseXml, boolean hasError, Exception ex);

    /**
     * Посылаем ответ (асинхронный)
     * во внешную систему
     */
    void sendAsyncAnswerToRemoteSystem(String xml, String username, String password);
}
