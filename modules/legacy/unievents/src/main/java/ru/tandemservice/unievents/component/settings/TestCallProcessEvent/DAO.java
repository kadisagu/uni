package ru.tandemservice.unievents.component.settings.TestCallProcessEvent;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessEventManagerBean;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    public void onTest(Model model)
    {

        IDataSettings settings = model.getSettings();
        String benaName = (String) settings.get("beanName");
        Long entityForDaemonWSId = (Long) settings.get("entityForDaemonWSId");


        List<EntityForDaemonWS> entityForDaemonWSList = getList(EntityForDaemonWS.class, EntityForDaemonWS.entityId(), entityForDaemonWSId.toString());
        EntityForDaemonWS entityForDaemonWS = null;
        if (entityForDaemonWSList != null && !entityForDaemonWSList.isEmpty())
            entityForDaemonWS = entityForDaemonWSList.get(0);

        if (entityForDaemonWS == null) {
            throw new ApplicationException("Сущность с id=" + entityForDaemonWSId + " не найдена");
        }
        try {
            IProcessEventManagerBean bean = (IProcessEventManagerBean) ApplicationRuntime.getBean(benaName);
            XmlDocWS xdoc = bean.processEvent(entityForDaemonWS);

            if (xdoc == null)
                model.setResultXML("XmlDocWS null");
            else
                model.setResultXML(xdoc.getStringDocument());

        }
        catch (NoSuchBeanDefinitionException e) {
            throw new ApplicationException("Bean с именем " + benaName + " не найден");
        }
        catch (ClassCastException e) {
            throw new ApplicationException("Bean с именем " + benaName + " не является наследником IProcessEventManagerBean");
        }
        catch (Exception e) {
            String eMsg = "";

            if (e.getMessage() != null)
                eMsg = e.getMessage();

            model.setResultXML("Ошибка " + eMsg);
            //throw new ApplicationException(e.getMessage());
        }


    }
}
