package ru.tandemservice.unievents.component.catalog.externalEntityType.ExternalEntityTypeAddEdit;


import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;

public class Model extends DefaultCatalogAddEditModel<ExternalEntityType> {

    private ISelectModel externalSystemModel;

    public ISelectModel getExternalSystemModel() {
        return externalSystemModel;
    }

    public void setExternalSystemModel(ISelectModel externalSystemModel) {
        this.externalSystemModel = externalSystemModel;
    }
}
