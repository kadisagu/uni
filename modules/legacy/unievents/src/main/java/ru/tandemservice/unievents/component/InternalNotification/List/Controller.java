package ru.tandemservice.unievents.component.InternalNotification.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DateColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.unievents.entity.InternalNotification;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        if (model.getSettings() == null)
            model.setSettings(component.getSettings());

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<InternalNotification> dataSource = UniBaseUtils.createDataSource(component, getDao());
        createColumns(dataSource);

        model.setDataSource(dataSource);
    }

    private void createColumns(
            DynamicListDataSource<InternalNotification> dataSource)
    {

        dataSource.addColumn(new DateColumn("Дата и время", InternalNotification.eventDate(), DateFormatter.PATTERN_WITH_TIME).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Тип объекта", InternalNotification.entityType().title()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Тип события", InternalNotification.eventType().title()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Id", InternalNotification.entityId()).setOrderable(true));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onEditRow").setPermissionKey("editInternalNotification"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onDeleteRow", "Удалить?").setPermissionKey("deleteInternalNotification"));

    }

    public void onEditRow(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        IEntity entity = getDao().get(id);
        UniMap map = new UniMap();

        activate(
                component,
                new PublisherActivator(entity, map)
        );
    }

    public void onDeleteRow(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        getDao().delete(id);
    }


    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        Model model = getModel(component);
        model.getDataSource().refresh();
    }


    public void onClickClear(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);

    }

    public void onClickAdd(IBusinessComponent component)
    {
        UniMap map = new UniMap();

        ContextLocal.createDesktop("PersonShellDialog",
                                   new ComponentActivator(
                                           ru.tandemservice.unievents.component.InternalNotification.Add.Controller.class.getPackage().getName()
                                           , map));
    }
}
