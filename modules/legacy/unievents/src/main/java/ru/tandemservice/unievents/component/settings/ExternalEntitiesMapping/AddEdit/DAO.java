package ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.AddEdit;

import org.apache.cxf.common.util.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.LinkWrapper;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.uni.dao.UniDao;

import java.util.ArrayList;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(final Model model)
    {
        if (model.getLink().getId() == null) {
            model.getLink().setMetaType(model.getMeta().getName());
            model.getLink().setExternalSystem(model.getExternalSystem());
        }
        else {
            model.setLink(getNotNull(ExternalEntitiesMap.class, model.getLink().getId()));
            IEntity entity = getNotNull(model.getLink().getEntityId());
            model.setEntityWrapper(new LinkWrapper(null, entity));
            model.setMeta(EntityRuntime.getMeta(entity));
        }

        model.setEntityModel(new BaseSingleSelectModel() {
            @Override
            public ListResult<LinkWrapper> findValues(String filter) {
                List<LinkWrapper> result = new ArrayList<LinkWrapper>();

                List<IEntity> list = getList(model.getMeta().getEntityClass(), new String[]{});
                for (IEntity entity : list) {
                    LinkWrapper wrapper = new LinkWrapper(null, entity);
                    if (!StringUtils.isEmpty(filter) && !wrapper.getTitle().contains(filter))
                        continue;

                    result.add(wrapper);
                }

                return new ListResult<LinkWrapper>(result);
            }

            @Override
            public Object getValue(Object obj) {
                if (obj == null)
                    return null;

                IEntity entity = getNotNull((Long) obj);
                return new LinkWrapper(null, entity);
            }
        });
    }

    @Override
    public void update(Model model) {
        model.getLink().setEntityId(model.getEntityWrapper().getId());
        saveOrUpdate(model.getLink());
    }
}
