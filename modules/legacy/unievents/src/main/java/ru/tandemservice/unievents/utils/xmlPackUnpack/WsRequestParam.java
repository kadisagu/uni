package ru.tandemservice.unievents.utils.xmlPackUnpack;

import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: dvorlov
 * Date: 08.02.13
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
public class WsRequestParam implements IParameter, Serializable {
    private static final long serialVersionUID = -5872201780779394519L;

    private String parameterName = "", parameterValue = "";

    public WsRequestParam(String parameterName, String parameterValue) {
        this.parameterName = parameterName;
        this.parameterValue = parameterValue;
    }

    public WsRequestParam() {
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }
}
