package ru.tandemservice.unievents.base.bo.ExternalSystemType.logic;

import ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.uni.dao.UniBaseDao;


public class ExternalSystemDao extends UniBaseDao implements IExternalSystemDao {
    @Override
    public ExternalSystemNeedEntityType getExternalSystemNeedEntityType(Long id) {
        return get(ExternalSystemNeedEntityType.class, id);
    }

    @Override
    public void saveExternalSystemNeedEntityType(ExternalSystemNeedEntityType extSysType)
    {
        saveOrUpdate(extSysType);
    }

    //изменение isUse компонента
    @Override
    public void changeInUseEntry(Long id, Boolean isUse) {
        ExternalSystemNeedEntityType entry = get(ExternalSystemNeedEntityType.class, id);
        entry.setIsUse(isUse);
        update(entry);
    }

    @Override
    public ExternalSystem getExternalSystem(Long id) {
        return get(ExternalSystem.class, id);
    }
}
