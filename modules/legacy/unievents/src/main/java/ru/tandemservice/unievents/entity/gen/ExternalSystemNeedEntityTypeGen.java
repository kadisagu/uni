package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.IntegrationBusinessObject;
import ru.tandemservice.unievents.entity.catalog.IntegrationType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Типы сущностей для внешней системы
 *
 * Перечисляем типы сущностей, события изменения по которым необходимо формировать
 * и через вызов web службы, уведомлять внешную систему
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExternalSystemNeedEntityTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType";
    public static final String ENTITY_NAME = "externalSystemNeedEntityType";
    public static final int VERSION_HASH = 1726615464;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTERNAL_SYSTEM = "externalSystem";
    public static final String L_INTEGRATION_TYPE = "integrationType";
    public static final String L_BUSINESS_OBJECT = "businessObject";
    public static final String P_ENTITY_TYPE = "entityType";
    public static final String P_IS_USE = "isUse";
    public static final String P_ENTITY_TYPE_NAME = "entityTypeName";

    private ExternalSystem _externalSystem;     // Внешеая система
    private IntegrationType _integrationType;     // Тип интеграции
    private IntegrationBusinessObject _businessObject;     // Бизнес объект в интеграции
    private String _entityType;     // Тип объекта
    private boolean _isUse;     // Используется
    private String _entityTypeName;     // Описание типа объекта

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Внешеая система. Свойство не может быть null.
     */
    @NotNull
    public ExternalSystem getExternalSystem()
    {
        return _externalSystem;
    }

    /**
     * @param externalSystem Внешеая система. Свойство не может быть null.
     */
    public void setExternalSystem(ExternalSystem externalSystem)
    {
        dirty(_externalSystem, externalSystem);
        _externalSystem = externalSystem;
    }

    /**
     * @return Тип интеграции. Свойство не может быть null.
     */
    @NotNull
    public IntegrationType getIntegrationType()
    {
        return _integrationType;
    }

    /**
     * @param integrationType Тип интеграции. Свойство не может быть null.
     */
    public void setIntegrationType(IntegrationType integrationType)
    {
        dirty(_integrationType, integrationType);
        _integrationType = integrationType;
    }

    /**
     * @return Бизнес объект в интеграции.
     */
    public IntegrationBusinessObject getBusinessObject()
    {
        return _businessObject;
    }

    /**
     * @param businessObject Бизнес объект в интеграции.
     */
    public void setBusinessObject(IntegrationBusinessObject businessObject)
    {
        dirty(_businessObject, businessObject);
        _businessObject = businessObject;
    }

    /**
     * @return Тип объекта.
     */
    @Length(max=255)
    public String getEntityType()
    {
        return _entityType;
    }

    /**
     * @param entityType Тип объекта.
     */
    public void setEntityType(String entityType)
    {
        dirty(_entityType, entityType);
        _entityType = entityType;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isIsUse()
    {
        return _isUse;
    }

    /**
     * @param isUse Используется. Свойство не может быть null.
     */
    public void setIsUse(boolean isUse)
    {
        dirty(_isUse, isUse);
        _isUse = isUse;
    }

    /**
     * @return Описание типа объекта.
     */
    @Length(max=255)
    public String getEntityTypeName()
    {
        return _entityTypeName;
    }

    /**
     * @param entityTypeName Описание типа объекта.
     */
    public void setEntityTypeName(String entityTypeName)
    {
        dirty(_entityTypeName, entityTypeName);
        _entityTypeName = entityTypeName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExternalSystemNeedEntityTypeGen)
        {
            setExternalSystem(((ExternalSystemNeedEntityType)another).getExternalSystem());
            setIntegrationType(((ExternalSystemNeedEntityType)another).getIntegrationType());
            setBusinessObject(((ExternalSystemNeedEntityType)another).getBusinessObject());
            setEntityType(((ExternalSystemNeedEntityType)another).getEntityType());
            setIsUse(((ExternalSystemNeedEntityType)another).isIsUse());
            setEntityTypeName(((ExternalSystemNeedEntityType)another).getEntityTypeName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExternalSystemNeedEntityTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExternalSystemNeedEntityType.class;
        }

        public T newInstance()
        {
            return (T) new ExternalSystemNeedEntityType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "externalSystem":
                    return obj.getExternalSystem();
                case "integrationType":
                    return obj.getIntegrationType();
                case "businessObject":
                    return obj.getBusinessObject();
                case "entityType":
                    return obj.getEntityType();
                case "isUse":
                    return obj.isIsUse();
                case "entityTypeName":
                    return obj.getEntityTypeName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "externalSystem":
                    obj.setExternalSystem((ExternalSystem) value);
                    return;
                case "integrationType":
                    obj.setIntegrationType((IntegrationType) value);
                    return;
                case "businessObject":
                    obj.setBusinessObject((IntegrationBusinessObject) value);
                    return;
                case "entityType":
                    obj.setEntityType((String) value);
                    return;
                case "isUse":
                    obj.setIsUse((Boolean) value);
                    return;
                case "entityTypeName":
                    obj.setEntityTypeName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "externalSystem":
                        return true;
                case "integrationType":
                        return true;
                case "businessObject":
                        return true;
                case "entityType":
                        return true;
                case "isUse":
                        return true;
                case "entityTypeName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "externalSystem":
                    return true;
                case "integrationType":
                    return true;
                case "businessObject":
                    return true;
                case "entityType":
                    return true;
                case "isUse":
                    return true;
                case "entityTypeName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "externalSystem":
                    return ExternalSystem.class;
                case "integrationType":
                    return IntegrationType.class;
                case "businessObject":
                    return IntegrationBusinessObject.class;
                case "entityType":
                    return String.class;
                case "isUse":
                    return Boolean.class;
                case "entityTypeName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExternalSystemNeedEntityType> _dslPath = new Path<ExternalSystemNeedEntityType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExternalSystemNeedEntityType");
    }
            

    /**
     * @return Внешеая система. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getExternalSystem()
     */
    public static ExternalSystem.Path<ExternalSystem> externalSystem()
    {
        return _dslPath.externalSystem();
    }

    /**
     * @return Тип интеграции. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getIntegrationType()
     */
    public static IntegrationType.Path<IntegrationType> integrationType()
    {
        return _dslPath.integrationType();
    }

    /**
     * @return Бизнес объект в интеграции.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getBusinessObject()
     */
    public static IntegrationBusinessObject.Path<IntegrationBusinessObject> businessObject()
    {
        return _dslPath.businessObject();
    }

    /**
     * @return Тип объекта.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getEntityType()
     */
    public static PropertyPath<String> entityType()
    {
        return _dslPath.entityType();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#isIsUse()
     */
    public static PropertyPath<Boolean> isUse()
    {
        return _dslPath.isUse();
    }

    /**
     * @return Описание типа объекта.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getEntityTypeName()
     */
    public static PropertyPath<String> entityTypeName()
    {
        return _dslPath.entityTypeName();
    }

    public static class Path<E extends ExternalSystemNeedEntityType> extends EntityPath<E>
    {
        private ExternalSystem.Path<ExternalSystem> _externalSystem;
        private IntegrationType.Path<IntegrationType> _integrationType;
        private IntegrationBusinessObject.Path<IntegrationBusinessObject> _businessObject;
        private PropertyPath<String> _entityType;
        private PropertyPath<Boolean> _isUse;
        private PropertyPath<String> _entityTypeName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Внешеая система. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getExternalSystem()
     */
        public ExternalSystem.Path<ExternalSystem> externalSystem()
        {
            if(_externalSystem == null )
                _externalSystem = new ExternalSystem.Path<ExternalSystem>(L_EXTERNAL_SYSTEM, this);
            return _externalSystem;
        }

    /**
     * @return Тип интеграции. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getIntegrationType()
     */
        public IntegrationType.Path<IntegrationType> integrationType()
        {
            if(_integrationType == null )
                _integrationType = new IntegrationType.Path<IntegrationType>(L_INTEGRATION_TYPE, this);
            return _integrationType;
        }

    /**
     * @return Бизнес объект в интеграции.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getBusinessObject()
     */
        public IntegrationBusinessObject.Path<IntegrationBusinessObject> businessObject()
        {
            if(_businessObject == null )
                _businessObject = new IntegrationBusinessObject.Path<IntegrationBusinessObject>(L_BUSINESS_OBJECT, this);
            return _businessObject;
        }

    /**
     * @return Тип объекта.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getEntityType()
     */
        public PropertyPath<String> entityType()
        {
            if(_entityType == null )
                _entityType = new PropertyPath<String>(ExternalSystemNeedEntityTypeGen.P_ENTITY_TYPE, this);
            return _entityType;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#isIsUse()
     */
        public PropertyPath<Boolean> isUse()
        {
            if(_isUse == null )
                _isUse = new PropertyPath<Boolean>(ExternalSystemNeedEntityTypeGen.P_IS_USE, this);
            return _isUse;
        }

    /**
     * @return Описание типа объекта.
     * @see ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType#getEntityTypeName()
     */
        public PropertyPath<String> entityTypeName()
        {
            if(_entityTypeName == null )
                _entityTypeName = new PropertyPath<String>(ExternalSystemNeedEntityTypeGen.P_ENTITY_TYPE_NAME, this);
            return _entityTypeName;
        }

        public Class getEntityClass()
        {
            return ExternalSystemNeedEntityType.class;
        }

        public String getEntityName()
        {
            return "externalSystemNeedEntityType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
