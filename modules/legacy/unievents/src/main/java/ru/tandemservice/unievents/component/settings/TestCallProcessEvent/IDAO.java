package ru.tandemservice.unievents.component.settings.TestCallProcessEvent;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void onTest(Model model);
}
