package ru.tandemservice.unievents.events;

import org.hibernate.engine.SessionImplementor;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unievents.dao.externalentitymap.DaoExternalEntities;
import ru.tandemservice.unievents.dao.externalentitymap.IDaoExternalEntities;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;

import javax.transaction.Synchronization;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: dvorlov
 * vch перенес из модуля integra
 */
public class ClearExternalEntitiesMap
        implements IDSetEventListener
{

    private static final ThreadLocal<Sync> syncs = new ThreadLocal<Sync>();

    public void init() {
        DSetEventManager.getInstance().registerListener(
                DSetEventType.afterDelete, EntityBase.class, this);
    }


    @Override
    public void onEvent(DSetEvent event)
    {
        if (event.getMultitude().getEntityMeta().getName().equals(ExternalEntitiesMap.ENTITY_NAME))
            return;


        Sync sync = getSyncSafe(event.getContext());
        if (event.getMultitude().isSingular()) {
            Long id = event.getMultitude().getSingularEntity().getId();
            sync.idsLong.add(id);
        }
        else {
            List<Long> allIds = new DQLSelectBuilder()
                    .fromDataSource(event.getMultitude().getSource(), "s").column(Person.id().fromAlias("s").s())
                    .createStatement(event.getContext()).<Long>list();
            sync.idsLong.addAll(allIds);
        }
    }

    private Sync getSyncSafe(DQLExecutionContext context)
    {
        Sync sync = syncs.get();
        if (sync == null) {
            syncs.set(sync = new Sync(context.getSessionImplementor()));
            context.getTransaction().registerSynchronization(sync);
        }
        return sync;
    }

    private static class Sync implements Synchronization {
        private SessionImplementor sessionImplementor;

        /**
         * id сущностей на удаление
         */
        private Set<Long> idsLong = new HashSet<Long>();

        private Sync(SessionImplementor sessionImplementor) {
            this.sessionImplementor = sessionImplementor;
        }

        @Override
        public void beforeCompletion()
        {
            boolean hasUpdate = false;

            /**
             * Пробуем удалить
             */
            if (idsLong.size() > 0) {
                IDaoExternalEntities dao = DaoExternalEntities.instanse();
                hasUpdate = dao.onDeleteExternalEntities(idsLong);
            }

            if (hasUpdate)
                sessionImplementor.flush();
        }

        @Override
        public void afterCompletion(int status) {
            syncs.remove();
        }
    }
}
