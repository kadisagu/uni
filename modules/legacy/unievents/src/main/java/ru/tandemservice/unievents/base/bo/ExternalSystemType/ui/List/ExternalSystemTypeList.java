package ru.tandemservice.unievents.base.bo.ExternalSystemType.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.ExternalSystemTypeManager;
import ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

@Configuration
public class ExternalSystemTypeList extends BusinessComponentManager {
    public static final String LIST_DS = "listDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ExternalSystemTypeManager.EXTERNAL_SYSTEM_DS, ExternalSystemTypeManager.instance().externalSystemDSHandler()))
                .addDataSource(selectDS(ExternalSystemTypeManager.TYPE_DS, ExternalSystemTypeManager.instance().entityTypeDSHandler()))
                .addDataSource(searchListDS(LIST_DS, columnListHandler(), externalSystemTypeDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnListHandler() {
        return columnListExtPointBuilder(LIST_DS)
                .addColumn(textColumn("externalSystem", ExternalSystemNeedEntityType.externalSystem().title()).order())
                .addColumn(textColumn("entityType", ExternalSystemNeedEntityType.entityType()).order())
                .addColumn(textColumn("entityTypeName", ExternalSystemNeedEntityType.entityTypeName()).order())
                .addColumn(textColumn("integrationType", ExternalSystemNeedEntityType.integrationType().title()).order())
                .addColumn(textColumn("businessObject", ExternalSystemNeedEntityType.businessObject().title()))
                .addColumn(toggleColumn("isUse", ExternalSystemNeedEntityType.isUse()).toggleOffListener("onUnsetInUseEntry").toggleOnListener("onSetInUseEntry").permissionKey("unieventsExternalSystemTypeChangeIsUse"))
                .addColumn(actionColumn(("edit"), new Icon("edit"), "onEditEntry")
                                   .permissionKey("unieventsExternalSystemTypeEdit"))
                .addColumn(actionColumn("delete", new Icon("delete"), "onDeleteEntry")
                                   .alert(FormattedMessage.with().template(LIST_DS + ".delete.alert").create())
                                   .permissionKey("unieventsExternalSystemTypeDelete"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> listDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), ExternalSystemNeedEntityType.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> externalSystemTypeDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), ExternalSystemNeedEntityType.class, ExternalSystemNeedEntityType.entityTypeName()) {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {

                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(ExternalSystemNeedEntityType.class, "e")
                        .column("e");

                ExternalSystem es = (ExternalSystem) context.get(ExternalSystemTypeListUI.EXT_SYSTEM_FILTER);
                if (es != null) {
                    dql.where(DQLExpressions.eq(DQLExpressions.property(ExternalSystemNeedEntityType.externalSystem().fromAlias("e")), DQLExpressions.value(es)));
                }

                IIdentifiableWrapper typeObjectWrapper = (IIdentifiableWrapper) context.get(ExternalSystemTypeListUI.ENTITY_TYPE_FILTER);
                //фильтр по entityTypeName
                if (typeObjectWrapper != null) {
                    dql.where(DQLExpressions.like(DQLExpressions.property(ExternalSystemNeedEntityType.entityTypeName().fromAlias("e")), DQLExpressions.value(typeObjectWrapper.getTitle())));
                }


                //определим order by
                /*
                dql.order(DQLExpressions.property(ExternalSystemNeedEntityType.externalSystem().title().fromAlias("e")), OrderDirection.asc);
                dql.order(DQLExpressions.property(ExternalSystemNeedEntityType.entityType().fromAlias("e")), OrderDirection.asc);
                */
                DQLOrderDescriptionRegistry _orderDescriptionRegistry = new DQLOrderDescriptionRegistry(ExternalSystemNeedEntityType.class, "e");
                _orderDescriptionRegistry.applyOrder(dql, input.getEntityOrder());

                return DQLSelectOutputBuilder.get(input, dql, context.getSession()).pageable(true).build();
            }
        };
    }


}
