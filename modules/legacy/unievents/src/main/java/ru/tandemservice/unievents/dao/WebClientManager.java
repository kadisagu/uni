package ru.tandemservice.unievents.dao;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.codes.WsProxyTypeCodes;
import ru.tandemservice.unievents.entity.catalog.codes.WsTypeCodes;
import ru.tandemservice.unievents.ws.EntitiManagerProxy;
import ru.tandemservice.unievents.ws.EventsManagerProxy;
import ru.tandemservice.unievents.ws.IEntityManager;
import ru.tandemservice.unievents.ws.IEventsManager;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * менеджер для работы с web клиентами
 * внешних систем
 *
 * @author vch
 */
public class WebClientManager extends UniBaseDao implements IWebClientManager
{

    Map<String, Object> mapEventsManager = new HashMap<>();

    @Override
    public void clearCache()
    {
        mapEventsManager = new HashMap<>();
        _mapExternalSystemWS = new HashMap<>();
    }

    @Override
    public void removeClientWS(ExternalSystem externalSystem, String wsTypeCodes)
    {
        String key = getExternalWsKey(externalSystem, wsTypeCodes);

        if (mapEventsManager.containsKey(key))
            mapEventsManager.remove(key);
    }


    private String getExternalWsKey(ExternalSystem externalSystem, String wsTypeCodes)
    {
        return wsTypeCodes + ":" + Long.toString(externalSystem.getId());
    }

    private Class getClassByWsTypeCode(String wsTypeCodes)
    {
        Class wsType = null;
        if (wsTypeCodes.equals(WsTypeCodes.ENTITY_MANAGER))
            wsType = IEntityManager.class;

        if (wsTypeCodes.equals(WsTypeCodes.EVENT_MANAGER))
            wsType = IEventsManager.class;

        if (wsType == null)
            throw new ApplicationException("Для кода веб службы = " + wsTypeCodes + " не найден интерфейс для приведения типа");

        return wsType;
    }

    private Object getWebServiceForExternalSystem(ExternalSystem externalSystem, String wsTypeCodes)
            throws Exception
    {
        /*
        // <TODO>
		EventManagerLocator elocator = new EventManagerLocator();
		EventManagerPortType em = elocator.getEventManagerSoap();
		
		*/

        // получим настройку
        ExternalSystemWS externalSystemWS = _getExternalSystemWS(externalSystem, wsTypeCodes);
        String key = getExternalWsKey(externalSystem, wsTypeCodes);

        if (mapEventsManager.containsKey(key))
            return mapEventsManager.get(key);
        else
        {
            Object retVal = null;
            Class wsType = getClassByWsTypeCode(wsTypeCodes);
//            String pwd = externalSystemWS.getWsPassword();

            if (externalSystemWS.getWsUrl() == null)
                throw new ApplicationException("Для внешней системы не указан WsUrl");

            boolean fullMode = false;
            if (
                    externalSystemWS.getWsServiceName() != null
                            && externalSystemWS.getWsServiceName().length() > 0
                            && externalSystemWS.getWsServicePort() != null
                            && externalSystemWS.getWsServicePort().length() > 0
                    )
                fullMode = true;


            if (externalSystemWS.getWsProxyType() != null)
            {
//                if (externalSystemWS.getWsProxyType().getCode().equals(WsProxyTypeCodes.JAXRPC))
//                    retVal = _createWsManagerRMI(externalSystemWS, wsType);

                if (externalSystemWS.getWsProxyType().getCode().equals(WsProxyTypeCodes.JAXWS)|| externalSystemWS.getWsProxyType().getCode().equals(WsProxyTypeCodes.JAXRPC))
                    retVal = _createWsManager(externalSystemWS, wsType);

                if (externalSystemWS.getWsProxyType().getCode().equals(WsProxyTypeCodes.NO_WSDL))
                    retVal = _createWsManagerSimple(externalSystemWS, wsType);
            } else
            {
                // выбираем способ работы с web службой
                if (fullMode && wsTypeCodes.equals(WsTypeCodes.ENTITY_MANAGER))
                {
                    // по старинке (есть 2 варианта)
//                    if (wsTypeCodes.equals(WsTypeCodes.ENTITY_MANAGER))
                        retVal = _createWsManagerSimple(externalSystemWS, wsType);
//                    else
//                        retVal = _createWsManager(externalSystemWS, wsType);
                } else
                    retVal = _createWsManager(externalSystemWS, wsType);
            }

            if (retVal == null)
                throw new ApplicationException("Прокси служба не создана, нет описания");

            mapEventsManager.put(key, retVal);
            return retVal;
        }
    }

    private Object _createWsManagerSimple(ExternalSystemWS externalSystemWS, Class wsType) throws Exception
    {
        if (IEventsManager.class.equals(wsType))
        {
            return new EventsManagerProxy(externalSystemWS);
        } else
        {
            return new EntitiManagerProxy(externalSystemWS);
        }
    }

    Map<String, ExternalSystemWS> _mapExternalSystemWS = new HashMap<>();

    private ExternalSystemWS _getExternalSystemWS(
            ExternalSystem externalSystem, String wsTypeCodes)
    {
        String key = Long.toString(externalSystem.getId()) + wsTypeCodes;

        if (_mapExternalSystemWS.containsKey(key))
            return _mapExternalSystemWS.get(key);
        else
        {
            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(ExternalSystemWS.class, "esystem");
            dql.column("esystem");
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(ExternalSystemWS.externalSystem().fromAlias("esystem")),
                    DQLExpressions.value(externalSystem)));

            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(ExternalSystemWS.wsType().code().fromAlias("esystem")),
                    DQLExpressions.value(wsTypeCodes)));


            List<ExternalSystemWS> lst = getList(dql);

            if (lst.isEmpty())
                throw new ApplicationException("Не найдены настройки web службы для внешней всистемы " + externalSystem.getTitle() + " код службы = " + wsTypeCodes);


            ExternalSystemWS retVal = lst.get(0);
            _mapExternalSystemWS.put(key, retVal);
            return retVal;
        }
    }

    @Override
    public IEventsManager getEventsManager(ExternalSystem externalSystem) throws Exception
    {
        Object retVal = getWebServiceForExternalSystem(externalSystem, WsTypeCodes.EVENT_MANAGER);
        return (IEventsManager) retVal;
    }


    @Override
    public IEntityManager getEntityManager(ExternalSystem externalSystem) throws Exception
    {
        Object retVal = getWebServiceForExternalSystem(externalSystem, WsTypeCodes.ENTITY_MANAGER);
        return (IEntityManager) retVal;
    }


    private Object _createWsManager(ExternalSystemWS externalSystem, Class proxyInterface) throws Exception
    {
        String urlStr = externalSystem.getWsUrl();

        urlStr = urlStr.replace("?wsdl", "");

        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(proxyInterface);
        factory.setAddress(urlStr);

        String username = externalSystem.getWsUsername();
        String password = externalSystem.getWsPassword();
        if (!StringUtils.isEmpty(username) || !StringUtils.isEmpty(password))
        {
            factory.setUsername(username);
            factory.setPassword(password);
        }
        // Object test = PrivateAccessor.invokePrivateMethod(client, "sendRequest", new Object[]{"<root></root>"});
        return factory.create();
    }

}
