package ru.tandemservice.unievents.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation", "ConstantConditions", "ConstantIfStatement"})
public class MS_unievents_2x9x3_0to1 extends IndependentMigrationScript
{

    @Override
    public boolean isRequired()
    {
        return true;
    }

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.3"),
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // NOTE модуль unibasermc отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if (ApplicationRuntime.hasModule("unibasermc"))
                throw new RuntimeException("Module 'unibasermc' is not deleted");
        }

        MigrationUtils.removeModuleFromVersion_s(tool, "unibasermc");

        // удалить сущность studentStatusExt
        if (tool.tableExists("studentstatusext_t"))
        {
            // удалить таблицу
            tool.dropTable("studentstatusext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("studentStatusExt");

        }

        // удалить сущность studentExt
        if (tool.tableExists("studentext_t"))
        {
            // удалить таблицу
            tool.dropTable("studentext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("studentExt");

        }

        // удалить сущность rmcSettingsType
        if (tool.tableExists("rmcsettingstype_t"))
        {
            // удалить таблицу
            tool.dropTable("rmcsettingstype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("rmcSettingsType");

        }

        // удалить сущность rmcSettingsGroupType
        if (tool.tableExists("rmcsettingsgrouptype_t"))
        {
            // удалить таблицу
            tool.dropTable("rmcsettingsgrouptype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("rmcSettingsGroupType");

        }

        // удалить сущность rmcSettings
        if (tool.tableExists("rmcsettings_t"))
        {
            // удалить таблицу
            tool.dropTable("rmcsettings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("rmcSettings");

        }

        // удалить сущность personForeignLanguageExt
        if (tool.tableExists("personforeignlanguageext_t"))
        {
            // удалить таблицу
            tool.dropTable("personforeignlanguageext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("personForeignLanguageExt");

        }

        // удалить сущность educationLevelStageExt
        if (tool.tableExists("educationlevelstageext_t"))
        {
            tool.executeUpdate("DELETE FROM declinableproperty_t where declinable_id in (select id from educationlevelstageext_t)");

            // удалить таблицу
            tool.dropTable("educationlevelstageext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("educationLevelStageExt");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // NOTE модуль movestudentrmc отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if (ApplicationRuntime.hasModule("movestudentrmc"))
                throw new RuntimeException("Module 'movestudentrmc' is not deleted");
        }

        MigrationUtils.removeModuleFromVersion_s(tool, "movestudentrmc");

        // удалить персистентный интерфейс ru.tandemservice.movestudentrmc.entity.IStudentPractice
        if (tool.viewExists("istudentpractice_v"))
        {
            // удалить view
            tool.dropView("istudentpractice_v");

        }

        // удалить персистентный интерфейс ru.tandemservice.movestudentrmc.entity.IStudentGrant
        if (tool.viewExists("istudentgrant_v"))
        {
            // удалить view
            tool.dropView("istudentgrant_v");

        }

        // удалить персистентный интерфейс ru.tandemservice.movestudentrmc.entity.IRepresentOrder
        if (tool.viewExists("irepresentorder_v"))
        {
            // удалить view
            tool.dropView("irepresentorder_v");

        }

        // удалить персистентный интерфейс ru.tandemservice.movestudentrmc.entity.IRepresent2Student
        if (tool.viewExists("irepresent2student_v"))
        {
            // удалить view
            tool.dropView("irepresent2student_v");

        }

        // удалить персистентный интерфейс ru.tandemservice.movestudentrmc.entity.IDocRepresentWithNewGroup
        if (tool.viewExists("idocrepresentwithnewgroup_v"))
        {
            // удалить view
            tool.dropView("idocrepresentwithnewgroup_v");

        }

        // удалить персистентный интерфейс ru.tandemservice.movestudentrmc.entity.IDocRepresentCancel
        if (tool.viewExists("idocrepresentcancel_v"))
        {
            // удалить view
            tool.dropView("idocrepresentcancel_v");

        }

        // удалить персистентный интерфейс ru.tandemservice.movestudentrmc.entity.IAbstractRepresentation
        if (tool.viewExists("iabstractrepresentation_v"))
        {
            // удалить view
            tool.dropView("iabstractrepresentation_v");

        }

        // удалить персистентный интерфейс ru.tandemservice.movestudentrmc.entity.IAbstractOrder
        if (tool.viewExists("iabstractorder_v"))
        {
            // удалить view
            tool.dropView("iabstractorder_v");

        }

        // удалить сущность typeTemplateRepresent
        if (tool.tableExists("typetemplaterepresent_t"))
        {
            // удалить таблицу
            tool.dropTable("typetemplaterepresent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("typeTemplateRepresent");

        }

        // удалить сущность travelPaymentData
        if (tool.tableExists("travelpaymentdata_t"))
        {
            // удалить таблицу
            tool.dropTable("travelpaymentdata_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("travelPaymentData");

        }

        // удалить сущность studentVKRTheme
        if (tool.tableExists("studentvkrtheme_t"))
        {
            // удалить таблицу
            tool.dropTable("studentvkrtheme_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("studentVKRTheme");

        }

        // удалить сущность studentQualificationThemes
        if (tool.tableExists("studentqualificationthemes_t"))
        {
            // удалить таблицу
            tool.dropTable("studentqualificationthemes_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("studentQualificationThemes");

        }

        // удалить сущность studentPracticeData
        if (tool.tableExists("studentpracticedata_t"))
        {
            // удалить таблицу
            tool.dropTable("studentpracticedata_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("studentPracticeData");

        }

        // удалить сущность studentGrantEntityHistory
        if (tool.tableExists("studentgrantentityhistory_t"))
        {
            // удалить таблицу
            tool.dropTable("studentgrantentityhistory_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("studentGrantEntityHistory");

        }

        // удалить сущность studentGrantEntity
        if (tool.tableExists("studentgrantentity_t"))
        {
            // удалить таблицу
            tool.dropTable("studentgrantentity_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("studentGrantEntity");

        }

        // удалить сущность stuGrantStatus
        if (tool.tableExists("stugrantstatus_t"))
        {
            // удалить таблицу
            tool.dropTable("stugrantstatus_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("stuGrantStatus");

        }

        // удалить сущность representationType
        if (tool.tableExists("representationtype_t"))
        {
            // удалить таблицу
            tool.dropTable("representationtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representationType");

        }

        // удалить сущность representationReason
        if (tool.tableExists("representationreason_t"))
        {
            // удалить таблицу
            tool.dropTable("representationreason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representationReason");

        }

        // удалить сущность representationBasementRelation
        if (tool.tableExists("rprsnttnbsmntrltn_t"))
        {
            // удалить таблицу
            tool.dropTable("rprsnttnbsmntrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representationBasementRelation");

        }

        // удалить сущность representationBasement
        if (tool.tableExists("representationbasement_t"))
        {
            // удалить таблицу
            tool.dropTable("representationbasement_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representationBasement");

        }

        // удалить сущность representationAdditionalDocumentsRelation
        if (tool.tableExists("rprsnttnaddtnldcmntsrltn_t"))
        {
            // удалить таблицу
            tool.dropTable("rprsnttnaddtnldcmntsrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representationAdditionalDocumentsRelation");

        }

        // удалить сущность representWeekendProlong
        {
            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("representWeekendProlong");

        }

        // удалить сущность representWeekendOut
        if (tool.tableExists("representweekendout_t"))
        {
            // удалить таблицу
            tool.dropTable("representweekendout_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representWeekendOut");

        }

        // удалить сущность representWeekend
        if (tool.tableExists("representweekend_t"))
        {
            // удалить таблицу
            tool.dropTable("representweekend_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representWeekend");

        }

        // удалить сущность representTravel
        if (tool.tableExists("representtravel_t"))
        {
            // удалить таблицу
            tool.dropTable("representtravel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representTravel");

        }

        // удалить сущность representTransfer
        if (tool.tableExists("representtransfer_t"))
        {
            // удалить таблицу
            tool.dropTable("representtransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representTransfer");

        }

        // удалить сущность representSupport
        if (tool.tableExists("representsupport_t"))
        {
            // удалить таблицу
            tool.dropTable("representsupport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representSupport");

        }

        // удалить сущность representStudentTicket
        if (tool.tableExists("representstudentticket_t"))
        {
            // удалить таблицу
            tool.dropTable("representstudentticket_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representStudentTicket");

        }

        // удалить сущность representRecertificationTraining
        if (tool.tableExists("rprsntrcrtfctntrnng_t"))
        {
            // удалить таблицу
            tool.dropTable("rprsntrcrtfctntrnng_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representRecertificationTraining");

        }

        // удалить сущность representPersonWorkPlan
        if (tool.tableExists("representpersonworkplan_t"))
        {
            // удалить таблицу
            tool.dropTable("representpersonworkplan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representPersonWorkPlan");

        }

        // удалить сущность representMakeReprimand
        {
            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("representMakeReprimand");

        }

        // удалить сущность representGrantSuspend
        if (tool.tableExists("representgrantsuspend_t"))
        {
            // удалить таблицу
            tool.dropTable("representgrantsuspend_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representGrantSuspend");

        }

        // удалить сущность representGrantResume
        if (tool.tableExists("representgrantresume_t"))
        {
            // удалить таблицу
            tool.dropTable("representgrantresume_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representGrantResume");

        }

        // удалить сущность representGrantCancelAndDestination
        if (tool.tableExists("rprsntgrntcnclanddstntn_t"))
        {
            // удалить таблицу
            tool.dropTable("rprsntgrntcnclanddstntn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representGrantCancelAndDestination");

        }

        // удалить сущность representGrantCancel
        if (tool.tableExists("representgrantcancel_t"))
        {
            // удалить таблицу
            tool.dropTable("representgrantcancel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representGrantCancel");

        }

        // удалить сущность representGrant
        if (tool.tableExists("representgrant_t"))
        {
            // удалить таблицу
            tool.dropTable("representgrant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representGrant");

        }

        // удалить сущность representExtensionSession
        if (tool.tableExists("representextensionsession_t"))
        {
            // удалить таблицу
            tool.dropTable("representextensionsession_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representExtensionSession");

        }

        // удалить сущность representExcludeOut
        if (tool.tableExists("representexcludeout_t"))
        {
            // удалить таблицу
            tool.dropTable("representexcludeout_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representExcludeOut");

        }

        // удалить сущность representExclude
        if (tool.tableExists("representexclude_t"))
        {
            // удалить таблицу
            tool.dropTable("representexclude_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representExclude");

        }

        // удалить сущность representEnrollmentTransfer
        if (tool.tableExists("representenrollmenttransfer_t"))
        {
            // удалить таблицу
            tool.dropTable("representenrollmenttransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representEnrollmentTransfer");

        }

        // удалить сущность representDiplomAndExclude
        if (tool.tableExists("representdiplomandexclude_t"))
        {
            // удалить таблицу
            tool.dropTable("representdiplomandexclude_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representDiplomAndExclude");

        }

        // удалить сущность representCourseTransfer
        if (tool.tableExists("representcoursetransfer_t"))
        {
            // удалить таблицу
            tool.dropTable("representcoursetransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representCourseTransfer");

        }

        // удалить сущность representChangeQualificationTheme
        if (tool.tableExists("representchngqualtheme_t"))
        {
            // удалить таблицу
            tool.dropTable("representchngqualtheme_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representChangeQualificationTheme");

        }

        // удалить сущность representChangeName
        if (tool.tableExists("representchangename_t"))
        {
            // удалить таблицу
            tool.dropTable("representchangename_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representChangeName");

        }

        // удалить сущность representAdmissionAttendClasses
        if (tool.tableExists("rprsntadmssnattndclsss_t"))
        {
            // удалить таблицу
            tool.dropTable("rprsntadmssnattndclsss_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representAdmissionAttendClasses");

        }

        // удалить сущность representation
        if (tool.tableExists("representation_t"))
        {
            // удалить таблицу
            tool.dropTable("representation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representation");

        }

        // удалить сущность representGrantsOld
        if (tool.tableExists("representgrantsold_t"))
        {
            // удалить таблицу
            tool.dropTable("representgrantsold_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representGrantsOld");

        }

        // удалить сущность representGrantType
        if (tool.tableExists("representgranttype_t"))
        {
            // удалить таблицу
            tool.dropTable("representgranttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("representGrantType");

        }

        // удалить сущность relTypeGrantView
        if (tool.tableExists("reltypegrantview_t"))
        {
            // удалить таблицу
            tool.dropTable("reltypegrantview_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relTypeGrantView");

        }

        // удалить сущность relStudentCustomStateRepresent
        if (tool.tableExists("rlstdntcstmsttrprsnt_t"))
        {
            // удалить таблицу
            tool.dropTable("rlstdntcstmsttrprsnt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relStudentCustomStateRepresent");

        }

        // удалить сущность relRepresentationReasonOSSP
        if (tool.tableExists("relrepresentationreasonossp_t"))
        {
            // удалить таблицу
            tool.dropTable("relrepresentationreasonossp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relRepresentationReasonOSSP");

        }

        // удалить сущность relRepresentationReasonBasic
        if (tool.tableExists("relrepresentationreasonbasic_t"))
        {
            // удалить таблицу
            tool.dropTable("relrepresentationreasonbasic_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relRepresentationReasonBasic");

        }

        // удалить сущность relRepresentationBasementDocument
        if (tool.tableExists("rlrprsnttnbsmntdcmnt_t"))
        {
            // удалить таблицу
            tool.dropTable("rlrprsnttnbsmntdcmnt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relRepresentationBasementDocument");

        }

        // удалить сущность relRepresentGrants
        if (tool.tableExists("relrepresentgrants_t"))
        {
            // удалить таблицу
            tool.dropTable("relrepresentgrants_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relRepresentGrants");

        }

        // удалить сущность relOrderCategoryRepresentationType
        if (tool.tableExists("rlordrctgryrprsnttntyp_t"))
        {
            // удалить таблицу
            tool.dropTable("rlordrctgryrprsnttntyp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relOrderCategoryRepresentationType");

        }

        // удалить сущность relListRepresentStudentsOldData
        if (tool.tableExists("rel_lst_rep_stu_olddata_t"))
        {
            // удалить таблицу
            tool.dropTable("rel_lst_rep_stu_olddata_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relListRepresentStudentsOldData");

        }

        // удалить сущность relListRepresentStudents
        if (tool.tableExists("rellistrepresentstudents_t"))
        {
            // удалить таблицу
            tool.dropTable("rellistrepresentstudents_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relListRepresentStudents");

        }

        // удалить сущность relGrantViewOrgUnit
        if (tool.tableExists("relgrantvieworgunit_t"))
        {
            // удалить таблицу
            tool.dropTable("relgrantvieworgunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relGrantViewOrgUnit");

        }

        // удалить сущность relDocumentTypeKind
        if (tool.tableExists("reldocumenttypekind_t"))
        {
            // удалить таблицу
            tool.dropTable("reldocumenttypekind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relDocumentTypeKind");

        }

        // удалить сущность relCheckOnorderRepresentType
        if (tool.tableExists("relcheckonorderrepresenttype_t"))
        {
            // удалить таблицу
            tool.dropTable("relcheckonorderrepresenttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relCheckOnorderRepresentType");

        }

        // удалить сущность relCheckOnorderGrantView
        if (tool.tableExists("relcheckonordergrantview_t"))
        {
            // удалить таблицу
            tool.dropTable("relcheckonordergrantview_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("relCheckOnorderGrantView");

        }

        // удалить сущность qualificationExamType
        if (tool.tableExists("qualificationexamtype_t"))
        {
            // удалить таблицу
            tool.dropTable("qualificationexamtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("qualificationExamType");

        }

        // удалить сущность printTemplate
        if (tool.tableExists("printtemplate_t"))
        {
            // удалить таблицу
            tool.dropTable("printtemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("printTemplate");

        }

        // удалить сущность principal2Visa
        if (tool.tableExists("principal2visa_t"))
        {
            // удалить таблицу
            tool.dropTable("principal2visa_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("principal2Visa");

        }

        // удалить сущность practicePaymentData
        if (tool.tableExists("practicepaymentdata_t"))
        {
            // удалить таблицу
            tool.dropTable("practicepaymentdata_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("practicePaymentData");

        }

        // удалить сущность practiceBase
        if (tool.tableExists("practicebase_t"))
        {
            // удалить таблицу
            tool.dropTable("practicebase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("practiceBase");

        }

        // удалить сущность personNARFU
        if (tool.tableExists("personnarfu_t"))
        {
            // удалить таблицу
            tool.dropTable("personnarfu_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("personNARFU");

        }

        // удалить сущность osspPgo
        if (tool.tableExists("ossppgo_t"))
        {
            // удалить таблицу
            tool.dropTable("ossppgo_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("osspPgo");

        }

        // удалить сущность osspGrantsView
        if (tool.tableExists("osspgrantsview_t"))
        {
            // удалить таблицу
            tool.dropTable("osspgrantsview_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("osspGrantsView");

        }

        // удалить сущность osspGrants
        if (tool.tableExists("osspgrants_t"))
        {
            // удалить таблицу
            tool.dropTable("osspgrants_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("osspGrants");

        }

        // удалить сущность orderCategory
        if (tool.tableExists("ordercategory_t"))
        {
            // удалить таблицу
            tool.dropTable("ordercategory_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("orderCategory");

        }

        // удалить сущность narfuDocument
        if (tool.tableExists("narfudocument_t"))
        {
            // удалить таблицу
            tool.dropTable("narfudocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("narfuDocument");

        }

        // удалить сущность movestudentOrderStates
        if (tool.tableExists("movestudentorderstates_t"))
        {
            // удалить таблицу
            tool.dropTable("movestudentorderstates_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("movestudentOrderStates");

        }

        // удалить сущность movestudentExtractStates
        if (tool.tableExists("movestudentextractstates_t"))
        {
            // удалить таблицу
            tool.dropTable("movestudentextractstates_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("movestudentExtractStates");

        }

        // удалить сущность listRepresentHistoryItem
        if (tool.tableExists("listrepresenthistoryitem_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresenthistoryitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentHistoryItem");

        }

        // удалить сущность listRepresentWeekend
        if (tool.tableExists("listrepresentweekend_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentweekend_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentWeekend");

        }

        // удалить сущность listRepresentTravel
        if (tool.tableExists("listrepresenttravel_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresenttravel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentTravel");

        }

        // удалить сущность listRepresentTransfer
        if (tool.tableExists("listrepresenttransfer_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresenttransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentTransfer");

        }

        // удалить сущность listRepresentSupport
        if (tool.tableExists("listrepresentsupport_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentsupport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentSupport");

        }

        // удалить сущность listRepresentSocialGrant
        if (tool.tableExists("listrepresentsocialgrant_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentsocialgrant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentSocialGrant");

        }

        // удалить сущность listRepresentQualificationThemes
        if (tool.tableExists("listrepresentqualthemes_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentqualthemes_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentQualificationThemes");

        }

        // удалить сущность listRepresentQualificationAdmission
        if (tool.tableExists("listrepresentqualadm_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentqualadm_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentQualificationAdmission");

        }

        // удалить сущность listRepresentProfileTransfer
        if (tool.tableExists("listrepresentprofiletransfer_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentprofiletransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentProfileTransfer");

        }

        // удалить сущность listRepresentPractice
        if (tool.tableExists("listrepresentpractice_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentpractice_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentPractice");

        }

        // удалить сущность listRepresentGrantSuspend
        if (tool.tableExists("listrepresentgrantsuspend_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentgrantsuspend_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentGrantSuspend");

        }

        // удалить сущность listRepresentGrantResume
        if (tool.tableExists("listrepresentgrantresume_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentgrantresume_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentGrantResume");

        }

        // удалить сущность listRepresentGrantCancelAndDestination
        if (tool.tableExists("lstrprsntgrntcnclanddstntn_t"))
        {
            // удалить таблицу
            tool.dropTable("lstrprsntgrntcnclanddstntn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentGrantCancelAndDestination");

        }

        // удалить сущность listRepresentGrantCancel
        if (tool.tableExists("listrepresentgrantcancel_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentgrantcancel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentGrantCancel");

        }

        // удалить сущность listRepresentGrant
        if (tool.tableExists("listrepresentgrant_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentgrant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentGrant");

        }

        // удалить сущность listRepresentExclude
        if (tool.tableExists("listrepresentexclude_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentexclude_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentExclude");

        }

        // удалить сущность listRepresentDiplomAndExclude
        if (tool.tableExists("lst_rep_diplom_and_excl_t"))
        {
            // удалить таблицу
            tool.dropTable("lst_rep_diplom_and_excl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentDiplomAndExclude");

        }

        // удалить сущность listRepresentCourseTransfer
        if (tool.tableExists("listrepresentcoursetransfer_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentcoursetransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentCourseTransfer");

        }

        // удалить сущность listRepresentBudgetTransfer
        if (tool.tableExists("listrepresentbudgettransfer_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresentbudgettransfer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentBudgetTransfer");

        }

        // удалить сущность listRepresentAdmissionToPractice
        if (tool.tableExists("listrepradmissionpractice_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepradmissionpractice_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresentAdmissionToPractice");

        }

        // удалить сущность listRepresent
        if (tool.tableExists("listrepresent_t"))
        {
            // удалить таблицу
            tool.dropTable("listrepresent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listRepresent");

        }

        // удалить сущность listOrder
        if (tool.tableExists("listorder_t"))
        {
            // удалить таблицу
            tool.dropTable("listorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listOrder");

        }

        // удалить сущность listOrdListRepresent
        if (tool.tableExists("listordlistrepresent_t"))
        {
            // удалить таблицу
            tool.dropTable("listordlistrepresent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("listOrdListRepresent");

        }

        // удалить сущность grantView
        if (tool.tableExists("grantview_t"))
        {
            // удалить таблицу
            tool.dropTable("grantview_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("grantView");

        }

        // удалить сущность grantType
        if (tool.tableExists("granttype_t"))
        {
            // удалить таблицу
            tool.dropTable("granttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("grantType");

        }

        // удалить сущность grantEntity
        if (tool.tableExists("grantentity_t"))
        {
            // удалить таблицу
            tool.dropTable("grantentity_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("grantEntity");

        }

        // удалить сущность grant2Grant
        if (tool.tableExists("grant2grant_t"))
        {
            // удалить таблицу
            tool.dropTable("grant2grant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("grant2Grant");

        }

        // удалить сущность grant
        if (tool.tableExists("grant_t"))
        {
            // удалить таблицу
            tool.dropTable("grant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("grant");

        }

        // удалить сущность extractTextRelation
        if (tool.tableExists("extracttextrelation_t"))
        {
            // удалить таблицу
            tool.dropTable("extracttextrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("extractTextRelation");

        }

        // удалить сущность documentTypeForRepresent
        if (tool.tableExists("documenttypeforrepresent_t"))
        {
            // удалить таблицу
            tool.dropTable("documenttypeforrepresent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("documentTypeForRepresent");

        }

        // удалить сущность documentType
        if (tool.tableExists("documenttype_t"))
        {
            // удалить таблицу
            tool.dropTable("documenttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("documentType");

        }

        // удалить сущность documentOrder
        if (tool.tableExists("documentorder_t"))
        {
            // удалить таблицу
            tool.dropTable("documentorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("documentOrder");

        }

        // удалить сущность documentKind
        if (tool.tableExists("documentkind_t"))
        {
            // удалить таблицу
            tool.dropTable("documentkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("documentKind");

        }

        // удалить сущность document
        if (tool.tableExists("document_t"))
        {
            // удалить таблицу
            tool.dropTable("document_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("document");

        }

        // удалить сущность docRepresentStudentIC
        if (tool.tableExists("docrepresentstudentic_t"))
        {
            // удалить таблицу
            tool.dropTable("docrepresentstudentic_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("docRepresentStudentIC");

        }

        // удалить сущность docRepresentStudentDocuments
        if (tool.tableExists("docrepresentstudentdocuments_t"))
        {
            // удалить таблицу
            tool.dropTable("docrepresentstudentdocuments_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("docRepresentStudentDocuments");

        }

        // удалить сущность docRepresentStudentBase
        if (tool.tableExists("docrepresentstudentbase_t"))
        {
            // удалить таблицу
            tool.dropTable("docrepresentstudentbase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("docRepresentStudentBase");

        }

        // удалить сущность docRepresentOrderCancel
        if (tool.tableExists("docrepresentordercancel_t"))
        {
            // удалить таблицу
            tool.dropTable("docrepresentordercancel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("docRepresentOrderCancel");

        }

        // удалить сущность docRepresentGrants
        if (tool.tableExists("docrepresentgrants_t"))
        {
            // удалить таблицу
            tool.dropTable("docrepresentgrants_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("docRepresentGrants");

        }

        // удалить сущность docRepresentBasics
        if (tool.tableExists("docrepresentbasics_t"))
        {
            // удалить таблицу
            tool.dropTable("docrepresentbasics_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("docRepresentBasics");

        }

        // удалить сущность docRecertificationTrainingDiscipline
        if (tool.tableExists("dcrcrtfctntrnngdscpln_t"))
        {
            // удалить таблицу
            tool.dropTable("dcrcrtfctntrnngdscpln_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("docRecertificationTrainingDiscipline");

        }

        // удалить сущность docOrdRepresent
        if (tool.tableExists("docordrepresent_t"))
        {
            // удалить таблицу
            tool.dropTable("docordrepresent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("docOrdRepresent");

        }

        // удалить сущность docListRepresentBasics
        if (tool.tableExists("doclistrepresentbasics_t"))
        {
            // удалить таблицу
            tool.dropTable("doclistrepresentbasics_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("docListRepresentBasics");

        }

        // удалить сущность checkOnOrder
        if (tool.tableExists("checkonorder_t"))
        {
            // удалить таблицу
            tool.dropTable("checkonorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("checkOnOrder");

        }

        // удалить сущность basement
        if (tool.tableExists("basement_t"))
        {
            // удалить таблицу
            tool.dropTable("basement_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("basement");

        }

        // удалить сущность additionalDocument
        if (tool.tableExists("additionaldocument_t"))
        {
            // удалить таблицу
            tool.dropTable("additionaldocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("additionalDocument");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // NOTE модуль movestudentbasermc отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if (ApplicationRuntime.hasModule("movestudentbasermc"))
                throw new RuntimeException("Module 'movestudentbasermc' is not deleted");
        }

        MigrationUtils.removeModuleFromVersion_s(tool, "movestudentbasermc");

        // удалить сущность studentOrdersTypes
        if (tool.tableExists("studentorderstypes_t"))
        {
            // удалить таблицу
            tool.dropTable("studentorderstypes_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("studentOrdersTypes");

        }

        // удалить сущность stdExtractT2OrdersT
        if (tool.tableExists("stdextractt2orderst_t"))
        {
            // удалить таблицу
            tool.dropTable("stdextractt2orderst_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("stdExtractT2OrdersT");

        }

        // удалить сущность orderList
        if (tool.tableExists("orderlist_t"))
        {
            // удалить таблицу
            tool.dropTable("orderlist_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("orderList");

        }
    }
}