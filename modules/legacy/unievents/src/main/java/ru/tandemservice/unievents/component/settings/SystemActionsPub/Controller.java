package ru.tandemservice.unievents.component.settings.SystemActionsPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uni.dao.IUniDao;

public class Controller extends AbstractBusinessController<IUniDao<Model>, Model> {
    public void onTestWsEntityUI(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(
                ru.tandemservice.unievents.component.settings.WsEntityTestUIPub.Controller.class.getPackage().getName()));
    }

}