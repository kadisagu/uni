package ru.tandemservice.unievents.dao;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.List;


public class DaoEventsBean
        extends UniBaseDao
        implements IDaoEventsBean
{

    @Override
    public ExternalEntitiesMap getLink(String externalId, String metaType,
                                       ExternalSystem externalSystem)
    {

        MQBuilder builder = new MQBuilder(ExternalEntitiesMap.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", ExternalEntitiesMap.externalId(), externalId))
                        // фильтр по префиксу внешней системы
                .add(MQExpression.eq("e", ExternalEntitiesMap.externalSystem().beanPref(), externalSystem.getBeanPref()));

        if (metaType != null)
            builder.add(MQExpression.eq("e", ExternalEntitiesMap.metaType(), metaType));

        List<ExternalEntitiesMap> linkList = builder.getResultList(getSession());

        // если мы получили записей >1, то ищем по типам объектов
        // или исключение?
        if (!linkList.isEmpty()) {
            if (linkList.size() == 1) {
                // всего одна запись
                return linkList.get(0);
            }
            else {
                // записей много, однозначно не дело
                throw new ApplicationException("Для внешней системы " + externalSystem.getTitle() + " не удалось однозначно сопоставить сущность с внешним id = '" + externalId + "'");
            }

        }
        else
            return null;
    }

}
