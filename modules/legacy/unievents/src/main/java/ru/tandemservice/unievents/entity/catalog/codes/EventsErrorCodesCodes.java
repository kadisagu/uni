package ru.tandemservice.unievents.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Коды ошибок"
 * Имя сущности : eventsErrorCodes
 * Файл data.xml : unievents.data.xml
 */
public interface EventsErrorCodesCodes
{
    /** Константа кода (code) элемента : Для указанного имени пользователя и пароля, внешная система не найдена (title) */
    String NO_EXTERNAL_SYSTEM_BY_USER_NAME_PWD = "1";
    /** Константа кода (code) элемента : Ошибка WS TU (make4Daemon) (title) */
    String WS_TU_MAKE4_DAEMON = "2";
    /** Константа кода (code) элемента : Системная ошибка преобразования DOM документа к строке (title) */
    String D_O_M_TO_STRING_ERROR = "100";
    /** Константа кода (code) элемента : Системная ошибка преобразования текста в DOM документ (title) */
    String STRING_TO_DOM = "101";
    /** Константа кода (code) элемента : Отсутствует информация об идентификации для асинхронных запросов (title) */
    String LOGIN_REQUIRED = "200";
    /** Константа кода (code) элемента : Не найден обработчик (title) */
    String NOT_FOUND_BEAN = "201";
    /** Константа кода (code) элемента : Обработчик не вернул результат (title) */
    String BEAN_RETURN_NULL = "202";
    /** Константа кода (code) элемента : Ошибка создания асинхронного запроса. (title) */
    String ASYNC_JOB_ADD = "203";
    /** Константа кода (code) элемента : Пользовательская ошибка. (title) */
    String APPLICATION_EXCEPTION = "300";
    /** Константа кода (code) элемента : Системная ошибка. (title) */
    String SYSTEM_EXCEPTION = "301";
    /** Константа кода (code) элемента : Неизвестный тип ошибки (title) */
    String U_N_D_I_F_I_N_E_E_R_R_O_R = "1000";

    Set<String> CODES = ImmutableSet.of(NO_EXTERNAL_SYSTEM_BY_USER_NAME_PWD, WS_TU_MAKE4_DAEMON, D_O_M_TO_STRING_ERROR, STRING_TO_DOM, LOGIN_REQUIRED, NOT_FOUND_BEAN, BEAN_RETURN_NULL, ASYNC_JOB_ADD, APPLICATION_EXCEPTION, SYSTEM_EXCEPTION, U_N_D_I_F_I_N_E_E_R_R_O_R);
}
