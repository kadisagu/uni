package ru.tandemservice.unievents.component.settings.ManagerLogView;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unievents.entity.EntityManagerLog;

public class Model {

    public static final Long THRIFTY_NO = 0L;
    public static final Long THRIFTY_YES = 1L;

    private DynamicListDataSource<EntityManagerLog> dataSource;
    private IDataSettings settings;
    private ISelectModel internalModel;
    private ISelectModel okModel;


    public ISelectModel getInternalModel() {
        return internalModel;
    }

    public void setInternalModel(ISelectModel internalModel) {
        this.internalModel = internalModel;
    }

    public ISelectModel getOkModel() {
        return okModel;
    }

    public void setOkModel(ISelectModel okModel) {
        this.okModel = okModel;
    }

    public DynamicListDataSource<EntityManagerLog> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<EntityManagerLog> dataSource) {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }


}
