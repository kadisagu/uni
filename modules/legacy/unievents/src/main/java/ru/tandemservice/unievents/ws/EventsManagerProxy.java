package ru.tandemservice.unievents.ws;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.description.OperationDesc;
import org.apache.axis.description.ParameterDesc;
import org.apache.commons.lang.StringUtils;
import ru.tandemservice.unievents.entity.ExternalSystemWS;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import java.net.URL;

public class EventsManagerProxy
        implements IEventsManager
{

    @Override
    public String RaiseEvents(
            String userName
            , String userPassword
            , Long eventId
            , String entityId
            , String eventType
            , String entityType) throws Exception
    {
        return runCall(userName, userPassword, eventId, entityId, eventType, entityType);
    }


    /**
     * конечная точка
     */
    private URL endpoint = null;

    private ExternalSystemWS externalSystemWS = null;

    public EventsManagerProxy(ExternalSystemWS externalSystemWS)
            throws Exception
    {
        this.externalSystemWS = externalSystemWS;
        _makeEndPoint();

    }

    private void _makeEndPoint() throws Exception {
        String urlStr = externalSystemWS.getWsUrl();
        urlStr = urlStr.replace("?wsdl", "");
        endpoint = new URL(urlStr);
    }


    private String runCall(
            String userName
            , String userPassword
            , Long eventId
            , String entityId
            , String eventType
            , String entityType) throws Exception
    {

        String methodsName = "RaiseEvents";

        String soapActionURI = _makeSOAPActionURI();
        QName qName = _makeOperationName(methodsName);

        OperationDesc operDsk = getOperationDesk();

        Call _call = createCall();

        _call.setOperation(operDsk);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI(soapActionURI + methodsName);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(qName);

        if (externalSystemWS != null) {
            String username = externalSystemWS.getWsUsername();
            String password = externalSystemWS.getWsPassword();
            if (!StringUtils.isEmpty(username) || !StringUtils.isEmpty(password)) {
                _call.setUsername(username);
                _call.setPassword(password);
            }
        }

        try {
            Object _resp = _call.invoke(new java.lang.Object[]
                                                {
                                                        userName
                                                        , userPassword
                                                        , eventId
                                                        , entityId
                                                        , eventType
                                                        , entityType
                                                }
            );
            if (_resp instanceof java.rmi.RemoteException) {
                throw (java.rmi.RemoteException) _resp;
            }
            else {
                try {
                    return (String) _resp;
                }
                catch (java.lang.Exception _exception) {
                    return (String) org.apache.axis.utils.JavaUtils
                            .convert(_resp, java.lang.String.class);
                }
            }
        }
        catch (org.apache.axis.AxisFault axisFaultException) {
            throw axisFaultException;
        }

    }

    private QName _makeOperationName(String operationName) {
        if (externalSystemWS.getWsTargetNamespace() != null)
            return new QName(externalSystemWS.getWsTargetNamespace(),
                             operationName);
        else
            return new QName(operationName);
    }

    private String _makeSOAPActionURI() {
        // "http://std.tsogu.ru#EntityManager:sendRequest"
        String retVal = "";
        if (externalSystemWS.getWsTargetNamespace() != null)
            retVal += externalSystemWS.getWsTargetNamespace();

        if (externalSystemWS.getWsServiceName() != null) {
            if (!retVal.equals(""))
                retVal += "#";
            retVal += externalSystemWS.getWsServiceName();
        }

        if (!retVal.equals(""))
            retVal += ":";

        return retVal;
    }

    private Call createCall() throws ServiceException {
        Service service = new Service();
        Call call = (Call) service.createCall();

        call.setTargetEndpointAddress(endpoint);

        if (externalSystemWS.getWsUsername() != null)
            call.setUsername(externalSystemWS.getWsUsername());

        if (externalSystemWS.getWsPassword() != null)
            call.setUsername(externalSystemWS.getWsPassword());

        if (externalSystemWS.getWsServicePort() != null)
            call.setPortName(new QName(externalSystemWS.getWsServicePort()));

        return call;
    }

    private OperationDesc getOperationDesk()
    {
        /*
		 *   String userName
				, String userPassword
				, Long eventId
				, String entityId
				, String eventType
				, String entityType	
		 */
				/*
				 * <xs:element name="userName" type="xs:string"/>
<xs:element name="userPassword" type="xs:string"/>
<xs:element name="eventId" type="xs:long"/>
<xs:element name="entityId" type="xs:string"/>
<xs:element name="eventType" type="xs:string"/>
<xs:element name="entityType" type="xs:string"/>
				 */

        OperationDesc oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RaiseEvents");

        ParameterDesc param1 = _greateParams("userName", "http://www.w3.org/2001/XMLSchema", "string", String.class);
        oper.addParameter(param1);

        ParameterDesc param2 = _greateParams("userPassword", "http://www.w3.org/2001/XMLSchema", "string", String.class);
        oper.addParameter(param2);

        ParameterDesc param3 = _greateParams("eventId", "http://www.w3.org/2001/XMLSchema", "long", Long.class);
        oper.addParameter(param3);

        ParameterDesc param4 = _greateParams("entityId", "http://www.w3.org/2001/XMLSchema", "string", String.class);
        oper.addParameter(param4);

        ParameterDesc param5 = _greateParams("eventType", "http://www.w3.org/2001/XMLSchema", "string", String.class);
        oper.addParameter(param5);

        ParameterDesc param6 = _greateParams("entityType", "http://www.w3.org/2001/XMLSchema", "string", String.class);
        oper.addParameter(param6);


        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);

        //oper.setReturnQName(new javax.xml.namespace.QName("http://std.tsogu.ru", "return"));
        //oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        //oper.setUse(org.apache.axis.constants.Use.LITERAL);

        return oper;
    }

    private ParameterDesc _greateParams(String parameterName, String qnameUri,
                                        String qnameLocalPart, Class paramType)
    {
        QName qnameParam = _getQNameParameters(parameterName);
        ParameterDesc param = new org.apache.axis.description.ParameterDesc(
                qnameParam
                , ParameterDesc.IN
                , new javax.xml.namespace.QName(qnameUri, qnameLocalPart)
                , paramType, false, false);

        return param;
    }

    private QName _getQNameParameters(String parameterName)
    {
        QName qnameParam = null;
        if (externalSystemWS.getWsTargetNamespace() != null)
            qnameParam = new QName(externalSystemWS.getWsTargetNamespace(), parameterName);
        else
            qnameParam = new QName(parameterName);

        return qnameParam;
    }
}
