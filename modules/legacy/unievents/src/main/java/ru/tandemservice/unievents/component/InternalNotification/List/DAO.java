package ru.tandemservice.unievents.component.InternalNotification.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unievents.entity.InternalNotification;
import ru.tandemservice.unievents.entity.catalog.IntegrationEventType;
import ru.tandemservice.unievents.entity.catalog.InternalEntityType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Date;
import java.util.List;

public class DAO
        extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {

        model.setEntityTypeModel(new LazySimpleSelectModel<>(InternalEntityType.class, InternalEntityType.title().s())
                                         .setSortProperty(InternalEntityType.title().s())
                                         .setSearchProperty(InternalEntityType.title().s()));

        model.setEventTypeModel(new LazySimpleSelectModel<>(IntegrationEventType.class)
                                        .setSortProperty("title")
                                        .setSearchProperty(IntegrationEventType.title().s()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<InternalNotification> dataSource = model.getDataSource();

        IDataSettings settings = model.getSettings();
        List<InternalEntityType> entityTypes = settings.get("entityTypes");
        List<IntegrationEventType> eventTypes = settings.get("eventTypes");
        Date fromDate = settings.get("fromDate");
        Date toDate = settings.get("toDate");
        String id = settings.get("idString");

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(InternalNotification.class, "en");

        FilterUtils.applySelectFilter(builder, "en", InternalNotification.entityType(), entityTypes);
        FilterUtils.applySelectFilter(builder, "en", InternalNotification.eventType(), eventTypes);

        FilterUtils.applySimpleLikeFilter(builder, "en", InternalNotification.entityId(), id);
        FilterUtils.applyBetweenFilter(builder, "en", InternalNotification.P_EVENT_DATE, fromDate, toDate);

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(InternalNotification.class, "en");
        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}
