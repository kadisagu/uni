package ru.tandemservice.unievents.entity;

import ru.tandemservice.unievents.entity.gen.InternalNotificationGen;

/**
 * Уведомления из TU
 * <p/>
 * Уведомления, формируемые в TU, которые необходимо разослать внешним системам.
 * Уведомления могут формироваться по сущностям, которыз в TU нет, например ОХ.
 * <p/>
 * На уведомления можно подписать внешн. систему. Отслеживаем только afterInsert (по сущности internalNotification)
 */
public class InternalNotification extends InternalNotificationGen {
}