package ru.tandemservice.unievents.base.bo.ExternalSystemType.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.ExternalSystemTypeManager;
import ru.tandemservice.unievents.entity.catalog.IntegrationBusinessObject;
import ru.tandemservice.unievents.entity.catalog.IntegrationType;

@Configuration
public class ExternalSystemTypeAdd extends BusinessComponentManager {
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ExternalSystemTypeManager.EXTERNAL_SYSTEM_DS, ExternalSystemTypeManager.instance().externalSystemDSHandler()))
                .addDataSource(selectDS(ExternalSystemTypeManager.TYPE_DS, ExternalSystemTypeManager.instance().entityTypeDSHandler()))
                .addDataSource(selectDS(ExternalSystemTypeManager.INTEGRATION_TYPE_DS, integrationTypeFormDSHandler()))
                .addDataSource(selectDS(ExternalSystemTypeManager.INTEGRATION_BUSINESS_OBJECT_DS, integrationBusinessObjectDSHandler()))

                .create();

        // integrationBusinessObject
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> integrationTypeFormDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), IntegrationType.class, IntegrationType.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> integrationBusinessObjectDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), IntegrationBusinessObject.class, IntegrationBusinessObject.title());
    }

}
