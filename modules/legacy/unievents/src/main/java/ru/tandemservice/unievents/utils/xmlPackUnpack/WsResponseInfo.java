package ru.tandemservice.unievents.utils.xmlPackUnpack;

import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dvorlov
 */
public class WsResponseInfo implements IResponse
{

    private Map<String, IParameter> parameters = new HashMap<String, IParameter>();

    private String forRequestId = "";

    private String requestBody = "";
    private List<BodyElement> bodyList;


    @Override
    public List<IParameter> getParameters() {
        return new ArrayList<IParameter>(parameters.values());
    }

    @Override
    public void setParameters(List<IParameter> parameters) {
        this.parameters = new HashMap<String, IParameter>();
        for (IParameter param : parameters)
            this.parameters.put(param.getParameterName(), param);
    }

    @Override
    public IParameter getParameter(String paramName) {
        return parameters.get(paramName);
    }

    @Override
    public boolean hasParameter(String paramName) {
        return parameters.containsKey(paramName);
    }


    @Override
    public String getForRequestId() {
        return forRequestId;
    }

    @Override
    public void setForRequestId(String forRequestId) {
        this.forRequestId = forRequestId;
    }

    @Override
    public String getRequestBody() {
        return requestBody;
    }

    @Override
    public List<BodyElement> getBodyList() {
        return this.bodyList;
    }

    @Override
    public void setRequestBody(List<BodyElement> bodyList, String requestBody) {
        this.bodyList = bodyList;
        this.requestBody = requestBody;
    }
    /*
    @Override
    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }
    */
}
