package ru.tandemservice.unievents.businessObject;


import ru.tandemservice.unievents.events.ISubscribeEventsDAO.EntityInfo;
import ru.tandemservice.uni.dao.UniBaseDao;


public class TestBusinessObjectIntegration
        extends UniBaseDao implements IBusinessObjectIntegration

{

    @Override
    public boolean run(EntityInfo entity)
    {
        System.out.println(entity.getId());
        return false;
    }

}
