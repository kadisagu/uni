package ru.tandemservice.unievents.ws.bean;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsLoginInfo;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsRequestInfo;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsRequestParam;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.ILogin;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessEventManagerBean;
import ru.tandemservice.uni.dao.UniBaseDao;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Базовый класс для обработки сообщений
 * от внешней системы
 * к ОХ и TU
 *
 * @author chuk
 */
public abstract class BaseProcessEventManagerBean
        extends UniBaseDao implements IProcessEventManagerBean
{
    public static final String EVENT_INSERT = "afterInsert";
    public static final String EVENT_UPDATE = "afterUpdate";
    public static final String EVENT_DELETE = "afterDelete";
    protected static Long SYNC = 1L;

    @Override
    public XmlDocWS processEvent(EntityForDaemonWS event) throws Exception {
        XmlDocWS doc = new XmlDocWS();

        IRequest request = createRequest(event);
        if (request == null)
            return null;

        doc.addRequest(request);

        doc.setLogin(createLogin(event));

        return doc;
    }

    protected ILogin createLogin(EntityForDaemonWS event) {
        //TODO: прописать сюда внутрений EntityManager
        ILogin loginInfo = new WsLoginInfo();
        loginInfo.setUserName(event.getExternalSystem().getWsUserName());
        loginInfo.setPassword(event.getExternalSystem().getWsPassword());
        return loginInfo;
    }

    protected IRequest createRequest(EntityForDaemonWS event) {
        IRequest request = null;
        EventType eventType = getEventType(event.getEventType());

        // нет Саша, так низя
        if (eventType == null) {
            String eType = "";
            if (event.getEventType() != null)
                eType = event.getEventType();

            throw new ApplicationException("Не известный тип события: " + eType);
        }

        switch (eventType) {
            case INSERT:
                request = doEventInsert(event.getEntityId(), event.getEntityType(), event.getExternalSystem(), event.getId());
                break;
            case UPDATE:
                request = doEventUpdate(event.getEntityId(), event.getEntityType(), event.getExternalSystem(), event.getId());
                break;

            // если нам пришло событие удаления, то по такому событию мы не формируем запрос
            // мы пытаемся просто удалить линк и сущность в тандеме
            // хотя кто так просил делать?
            // то что так Гришук написал, оно понятно - а поговорить. (этот комент Чук написал)
            case DELETE:
                request = doEventDelete(event.getEntityId(), event.getEntityType(), event.getExternalSystem(), event.getId());
                break;

            default:
                throw new ApplicationException("Не известный тип события: " + event.getEventType());
        }

        return request;
    }

    protected abstract IRequest doEventDelete(
            String entityId
            , String entityType
            , ExternalSystem externalSystem
            , Long id);

    protected EventType getEventType(String eventType)
    {
        if (EVENT_INSERT.equals(eventType))
            return EventType.INSERT;
        else if (EVENT_UPDATE.equals(eventType))
            return EventType.UPDATE;
        else if (EVENT_DELETE.equals(eventType))
            return EventType.DELETE;

        return null;
    }

    /**
     * Имя бина, которое указываем в запросе к внешней системе
     * Во внешней системе люди ищут именно этот бин
     *
     * @param entityType
     *
     * @return
     */
    protected String getRequestBeanName(String entityType)
    {
        return new StringBuilder()
                .append(entityType)
                .append("1.0")
                .toString();
    }

    protected IRequest doEventInsert(String extId, String entityType, ExternalSystem externalSystem, Long entityForDaemonWSId)
    {
        IRequest request = createRequest(extId, entityType, externalSystem, entityForDaemonWSId);
        fillInsertParameters(request, extId, entityType);

        return request;
    }

    protected IRequest doEventUpdate(String extId, String entityType, ExternalSystem externalSystem, Long entityForDaemonWSId)
    {
        IRequest request = createRequest(extId, entityType, externalSystem, entityForDaemonWSId);
        fillUpdateParameters(request, extId, entityType);

        return request;
    }

    protected IRequest createRequest(String extId, String entityType, ExternalSystem externalSystem, Long entityForDaemonWSId)
    {
        IRequest request = new WsRequestInfo();
        request.setBeanName(getRequestBeanName(entityType));
        request.setAnswerType(SYNC_ANSWER_TYPE);
        request.setRequestId(entityForDaemonWSId.toString());

        fillRequestParameters(request, extId, entityType);

        return request;
    }

    protected void fillInsertParameters(
            IRequest request
            , String extId
            , String entityType
    )
    {
    }

    protected void fillUpdateParameters
            (
                    IRequest request
                    , String extId
                    , String entityType
            )
    {
    }

    protected void fillRequestParameters(
            IRequest request
            , String extId
            , String entityType
    )
    {
        List<IParameter> paramList = new ArrayList<IParameter>();
        paramList.add(new WsRequestParam("EntityType", entityType));
        if (!StringUtils.isEmpty(extId))
            paramList.add(new WsRequestParam("EntityId", extId.toString()));
        request.setParameters(paramList);
    }


    /////////////////////////////////////////////////////////
    //// обработка ответов
    /////////////////////////////////////////////////////////
    @Override
    public List<SaveData> processAnswer(XmlDocWS xdoc
            , String userName
            , String password
            , List<IRequest> requestList
            , IResponse response) throws Exception
    {
        List<BodyElement> entityList = getResponseList(response);
        ExternalSystem externalSystem = DaoFacade.getDaoEvents().getExternalSystem(userName, password);

        // setForRequestId(response.getForRequestId());
        // идем по вложенным сущностям

        List<SaveData> retVal = new ArrayList<>();
        for (BodyElement entityInfo : entityList) {
            SaveData saveData = saveEntity(entityInfo, externalSystem, response);
            retVal.add(saveData);
        }
        return retVal;
    }

    protected abstract SaveData saveEntity(BodyElement entityInfo,
                                           ExternalSystem externalSystem, IResponse response);


    protected List<BodyElement> getResponseList(IResponse response) throws Exception
    {
        String bodyXml = response.getRequestBody();
        bodyXml = "<Root>" + bodyXml + "</Root>";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        StringReader sr = null;

        try {
            sr = new StringReader(bodyXml);
            InputSource is = new InputSource(sr);
            Document doc = builder.parse(is);
            Element root = doc.getDocumentElement();
            List<BodyElement> list = parseXmL(root);

            if (getResponseAttributeName() == null)
                return list;
            else {
                List<BodyElement> retVal = new ArrayList<>();
                filtrateBodyElementList(list, retVal, getResponseAttributeName());
                return retVal;
            }
        }
        finally {
            if (sr != null)
                sr.close();
        }
    }

    private void filtrateBodyElementList
            (
                    List<BodyElement> list
                    , List<BodyElement> listFiltrate
                    , String attributeName

            )
    {
        for (BodyElement element : list) {
            if (element.getName() != null && element.getName().trim().equals(attributeName))
                listFiltrate.add(element);

            // рекурсивно по детям
            List<BodyElement> lst = element.getChildren();
            if (!lst.isEmpty())
                filtrateBodyElementList(lst, listFiltrate, attributeName);
        }

    }


    /**
     * имя xml аттрибута, содержащего ответы по сущности
     *
     * @return
     */
    protected String getResponseAttributeName()
    {
        return null;
    }


    private List<BodyElement> parseXmL(Node root)
    {
        List<BodyElement> resultList = new ArrayList<BodyElement>();
        NodeList nList = root.getChildNodes();

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == 3)
                continue;

            // коментарии пропускаем
            if (node.getNodeName().equals("#comment"))
                continue;

            BodyElement element = new BodyElement();
            element.setName(node.getNodeName());

            if (node.getChildNodes().getLength() == 1 && node.getChildNodes().item(0).getNodeType() == 3) {
                element.setValue(node.getTextContent());
            }
            else if (node.hasChildNodes()) {
                List<BodyElement> children = parseXmL(node);
                element.setChildren(children);
            }


            resultList.add(element);
        }

        return resultList;
    }


    /**
     * 'yyyy-MM-dd HH:mm:ss' или 'dd.MM.yyyy HH:mm:ss' или 'dd.MM.yyyy' или 'yyyy-MM-dd'
     *
     * @param dateString
     *
     * @return
     */
    protected Date getDateByString(String dateString)
    {
        if (dateString != null && !dateString.isEmpty()) {

            List<SimpleDateFormat> lstDataFormat = new ArrayList<>();
            lstDataFormat.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            lstDataFormat.add(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss"));
            lstDataFormat.add(new SimpleDateFormat("dd.MM.yyyy"));
            lstDataFormat.add(new SimpleDateFormat("yyyy-MM-dd"));

            String formatError = "";
            for (SimpleDateFormat sdf : lstDataFormat) {
                try {
                    Date retVal = sdf.parse(dateString);
                    return retVal;
                }
                catch (Exception ex) {
                    formatError = ex.getMessage();
                }
            }
            // ни один формат даты не подошел - взводим исключение
            throw new ApplicationException("Не правильный формат поля даты. Вы указали  " + dateString + " " + formatError);
        }
        else
            return null;
    }

}
