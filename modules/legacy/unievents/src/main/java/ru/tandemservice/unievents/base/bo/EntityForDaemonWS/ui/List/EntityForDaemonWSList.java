package ru.tandemservice.unievents.base.bo.EntityForDaemonWS.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.logic.EntityDSHandler;
import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.unievents.dao.IEntityTypeDao.EntityType;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.catalog.EntityForDaemonDirection;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.StatusDaemonProcessingWs;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Configuration
public class EntityForDaemonWSList extends BusinessComponentManager {
    public static final String LIST_DS = "listDS";
    public static final String EXTERNAL_SYSTEM_DS = "externalSystemDS";
    public static final String STATUS_DS = "statusDS";
    public static final String DIRECTION_DS = "directionDS";
    public static final String META_DS = "metaDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(LIST_DS, columnListHandler(), entityDSHandler()))
                .addDataSource(selectDS(DIRECTION_DS, directionDSHandler()))
                .addDataSource(selectDS(EXTERNAL_SYSTEM_DS, externalSystemDSHandler()))
                .addDataSource(selectDS(STATUS_DS, statusDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .addDataSource(selectDS(META_DS, metaDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint columnListHandler() {
        return columnListExtPointBuilder(LIST_DS)
                .addColumn(checkboxColumn("selectUnit"))
                .addColumn(booleanColumn("canBePass", "entity." + EntityForDaemonWS.canBePass()).order())
                .addColumn(textColumn("entityId", "entity." + EntityForDaemonWS.entityId()))

                .addColumn(publisherColumn("externalSystem", "entity." + EntityForDaemonWS.externalSystem().title()).order())

                .addColumn(textColumn("direction", "entity." + EntityForDaemonWS.direction().title()))
                .addColumn(textColumn("eventDate", "entity." + EntityForDaemonWS.eventDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("entityName", "meta.title").order())
                .addColumn(textColumn("entityType", "entity." + EntityForDaemonWS.entityType()))
                .addColumn(dateColumn("lastDate", "entity." + EntityForDaemonWS.lastDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order())
                .addColumn(textColumn("statusDaemonProcessingWs", "entity." + EntityForDaemonWS.statusDaemonProcessingWs().title()).order())
                .addColumn(textColumn("processCount", "entity." + EntityForDaemonWS.processedCount()))
                .addColumn(textColumn("lastErrorComment", "entity." + EntityForDaemonWS.lastErrorComment()))

                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> entityDSHandler() {

        return new DefaultSearchDataSourceHandler(getName()) {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {
                Date dateFrom = context.get(EntityForDaemonWSListUI.DATE_FROM_FILTER);
                Date dateTo = context.get(EntityForDaemonWSListUI.DATE_TO_FILTER);
                List<StatusDaemonProcessingWs> statusesList = context.get(EntityForDaemonWSListUI.STATUSES_FILTER);
                List<ExternalSystem> externalSystems = context.get(EntityForDaemonWSListUI.EXTERNAL_SYSTEMS_FILTER);
                EntityForDaemonDirection direction = context.get(EntityForDaemonWSListUI.DIRECTION_FILTER);
                Boolean accepted = context.get(EntityForDaemonWSListUI.ACCEPTED_FILTER);
                String entityType = context.get(EntityForDaemonWSListUI.META_FILTER);

                String entityId = null;
                if (context.get(EntityForDaemonWSListUI.ENTITYID_FILTER) != null)
                    entityId = context.get(EntityForDaemonWSListUI.ENTITYID_FILTER).toString();

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder.fromEntity(EntityForDaemonWS.class, "s").column("s");


                FilterUtils.applyBetweenFilter(builder, "s", EntityForDaemonWS.eventDate().getPath(), dateFrom, dateTo);
                FilterUtils.applySelectFilter(builder, "s", EntityForDaemonWS.externalSystem().getPath(), externalSystems);
                FilterUtils.applySelectFilter(builder, "s", EntityForDaemonWS.statusDaemonProcessingWs().getPath(), statusesList);
                FilterUtils.applySelectFilter(builder, "s", EntityForDaemonWS.direction(), direction);

                FilterUtils.applySelectFilter(builder, "s", EntityForDaemonWS.accepted(), accepted);
                FilterUtils.applyLikeFilter(builder, entityId, EntityForDaemonWS.entityId().fromAlias("s"));


                // и тут забыли
                if (entityType != null) {
                    entityType = entityType.toUpperCase();
                    FilterUtils.applyLikeFilter(builder, entityType, EntityForDaemonWS.entityType().fromAlias("s"));
                }

                builder.order(DQLExpressions.property(EntityForDaemonWS.externalSystem().title().fromAlias("s")), OrderDirection.asc);
                builder.order(DQLExpressions.property(EntityForDaemonWS.eventDate().fromAlias("s")), OrderDirection.asc);

                IDQLStatement statement = builder.createStatement(context.getSession());
                List<EntityForDaemonWS> entities = statement.list();

                List<DataWrapper> rowsList = new ArrayList<DataWrapper>(input.getCountRecord());

                for (EntityForDaemonWS entity : entities) {
                    DataWrapper dw = new DataWrapper(entity.getId(), entity.getEntityType(), entity);
                    dw.setProperty("entity", entity);

                    EntityType meta = DaoFacade.getEntityTypeDao().getEntityType(entity.getEntityType());
                    if (meta == null)
                        meta = new EntityType(entity.getId(), true, entity.getEntityType(), "неизвестно");

                    dw.setProperty("meta", meta);
                    rowsList.add(dw);
                }

                Collections.sort(rowsList, new EntityComparator<DataWrapper>(input.getEntityOrder()));

                return ListOutputBuilder.get(input, rowsList).pageable(true).build();
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> statusDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), StatusDaemonProcessingWs.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> externalSystemDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), ExternalSystem.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> directionDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), EntityForDaemonDirection.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> metaDSHandler() {
        return new EntityDSHandler(getName());
    }
}
