package ru.tandemservice.unievents.base.bo.EntityForDaemonWS.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unievents.base.bo.EntityForDaemonWS.EntityForDaemonWSManager;
import ru.tandemservice.unievents.entity.DaemonWsLog;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.ws.bean.ProcessEventBean_OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.List;


@State({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "eventId")})
public class EntityForDaemonWSPubUI extends UIPresenter {
    private Long _eventId;
    EntityForDaemonWS entity;
    List<DaemonWsLog> logList;

    DaemonWsLog curLogEntity;
    String curLogDateAsString;

    private OrgUnit orgUnit;

    public void onClickApply() {
        this.entity.setAccepted(true);
        IUniBaseDao.instance.get().saveOrUpdate(this.entity);
        deactivate();
    }

    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals("daemonWsLogDS")) {
            dataSource.put("eventId", getEventId());
        }
    }

    @Override
    public void onComponentRefresh()
    {
        entity = EntityForDaemonWSManager.instance().getDao().getEntityById(getEventId());
        logList = EntityForDaemonWSManager.instance().getDao().getLogsByEntityWs(getEventId());

        if (getIsOrgUnit()) {
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ExternalEntitiesMap.class, "e")
                    .where(DQLExpressions.eq(
                            DQLExpressions.property(ExternalEntitiesMap.metaType().fromAlias("e")),
                            DQLExpressions.value("orgUnit")
                    ))
                    .where(DQLExpressions.eq(
                            DQLExpressions.property(ExternalEntitiesMap.externalId().fromAlias("e")),
                            DQLExpressions.value(entity.getEntityId())
                    ))
                    .where(DQLExpressions.eq(
                            DQLExpressions.property(ExternalEntitiesMap.externalSystem().fromAlias("e")),
                            DQLExpressions.value(this.entity.getExternalSystem())
                    ))
                    .column("e");

            List<ExternalEntitiesMap> list = IUniBaseDao.instance.get().getList(dql);
            if (!list.isEmpty())
                this.orgUnit = IUniBaseDao.instance.get().get(list.get(0).getEntityId());
            else {
                this.orgUnit = new OrgUnit();
                this.orgUnit.setTitle("Не известно");
            }
        }
    }

    public boolean getIsOrgUnit() {
        return "OrgUnit".equals(this.entity.getEntityType());
    }

    public OrgUnit getOrgUnit() {
        return this.orgUnit;
    }

    public String getEventTitle() {
        return ProcessEventBean_OrgUnit.getEventTitle(this.entity.getEventType());
    }

    public Long getEventId() {
        return _eventId;
    }

    public void setEventId(Long eventId) {
        this._eventId = eventId;
    }

    public EntityForDaemonWS getEntity() {
        return entity;
    }

    public void setEntity(EntityForDaemonWS entity) {
        this.entity = entity;
    }

    public List<DaemonWsLog> getLogList() {
        return logList;
    }

    public void setLogList(List<DaemonWsLog> logList) {
        this.logList = logList;
    }

    public DaemonWsLog getCurLogEntity() {
        return curLogEntity;
    }

    public void setCurLogEntity(DaemonWsLog curLogEntity) {
        this.curLogEntity = curLogEntity;
        this.setCurLogDateAsString(DateFormatter.DATE_FORMATTER_WITH_TIME.format(curLogEntity.getLogDate()));
    }

    public String getCurLogDateAsString() {
        return curLogDateAsString;
    }

    public void setCurLogDateAsString(String curLogDateAsString) {
        this.curLogDateAsString = curLogDateAsString;
    }
}
