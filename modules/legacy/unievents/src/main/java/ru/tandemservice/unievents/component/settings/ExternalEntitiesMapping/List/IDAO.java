package ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.List;

import ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.LinkWrapper;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.uni.dao.IUniDao;

import java.util.List;

public interface IDAO extends IUniDao<Model> {

    public List<LinkWrapper> getEntityList(
            Model.MetaWrapper wrapper
            , ExternalSystem externalSystem
            , String filter
            , String externalId
            , int startRow
            , int countRow
    );

    public void fillMeta(Model.MetaWrapper wrapper);
}
