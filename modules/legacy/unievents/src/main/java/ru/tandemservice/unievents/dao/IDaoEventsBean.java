package ru.tandemservice.unievents.dao;

import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

public interface IDaoEventsBean {
    /**
     * дай линк на сущность
     *
     * @param externalId
     * @param metaType
     * @param externalSystem
     *
     * @return
     */
    public ExternalEntitiesMap getLink(String externalId, String metaType, ExternalSystem externalSystem);

}
