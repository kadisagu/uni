package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unievents.entity.RequestInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Список запросов во внешние системы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RequestInfoGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.RequestInfo";
    public static final String ENTITY_NAME = "requestInfo";
    public static final int VERSION_HASH = 1202299050;
    private static IEntityMeta ENTITY_META;

    public static final String P_ANSWER_BEAN_NAME = "answerBeanName";

    private String _answerBeanName;     // Имя бина, который будет орабатывать ответ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Имя бина, который будет орабатывать ответ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAnswerBeanName()
    {
        return _answerBeanName;
    }

    /**
     * @param answerBeanName Имя бина, который будет орабатывать ответ. Свойство не может быть null.
     */
    public void setAnswerBeanName(String answerBeanName)
    {
        dirty(_answerBeanName, answerBeanName);
        _answerBeanName = answerBeanName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RequestInfoGen)
        {
            setAnswerBeanName(((RequestInfo)another).getAnswerBeanName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RequestInfoGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RequestInfo.class;
        }

        public T newInstance()
        {
            return (T) new RequestInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "answerBeanName":
                    return obj.getAnswerBeanName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "answerBeanName":
                    obj.setAnswerBeanName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "answerBeanName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "answerBeanName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "answerBeanName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RequestInfo> _dslPath = new Path<RequestInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RequestInfo");
    }
            

    /**
     * @return Имя бина, который будет орабатывать ответ. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.RequestInfo#getAnswerBeanName()
     */
    public static PropertyPath<String> answerBeanName()
    {
        return _dslPath.answerBeanName();
    }

    public static class Path<E extends RequestInfo> extends EntityPath<E>
    {
        private PropertyPath<String> _answerBeanName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Имя бина, который будет орабатывать ответ. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.RequestInfo#getAnswerBeanName()
     */
        public PropertyPath<String> answerBeanName()
        {
            if(_answerBeanName == null )
                _answerBeanName = new PropertyPath<String>(RequestInfoGen.P_ANSWER_BEAN_NAME, this);
            return _answerBeanName;
        }

        public Class getEntityClass()
        {
            return RequestInfo.class;
        }

        public String getEntityName()
        {
            return "requestInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
