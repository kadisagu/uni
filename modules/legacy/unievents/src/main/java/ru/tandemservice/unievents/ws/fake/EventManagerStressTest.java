package ru.tandemservice.unievents.ws.fake;

import ru.tandemservice.unievents.ws.IEventsManager;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Для тестирования под нагрузкой
 *
 * @author vch
 */
@WebService(serviceName = "EventManagerStressTest")
public class EventManagerStressTest
        implements IEventsManager
{

    @Override
    @WebMethod
    public String RaiseEvents(
            @WebParam(name = "userName") String userName
            , @WebParam(name = "userPassword") String userPassword
            , @WebParam(name = "eventId") Long eventId
            , @WebParam(name = "entityId") String entityId
            , @WebParam(name = "eventType") String eventType
            , @WebParam(name = "entityType") String entityType
    ) throws Exception
    {

        StringBuilder parameter = new StringBuilder();
        parameter.append("UserName=" + userName)
                .append("\n")
                .append("userPassword=" + userPassword)
                .append("\n")
                .append("eventId=" + String.valueOf(eventId))
                .append("\n")
                .append("entityId=" + String.valueOf(entityId))
                .append("\n")
                .append("eventType=" + String.valueOf(eventType))
                .append("\n")
                .append("entityType=" + String.valueOf(entityType))
                .append("\n")
                .append(" ВЫВОДИМ ОШИБКИ, ЧТОБ В TU ПРОДОЛЖАЛИСЬ СОБЫТИЯ ")
                .append("\n")
                .append(" error error ERROR error error !!!");

        return parameter.toString();
    }

}
