package ru.tandemservice.unievents.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.unievents.events.ISubscribeEventsDAO;

public class DaoFacade
{

    public static IDaoEvents _daoEvents;
    public static IWebClientManager _webClientManager;
    public static IEntityManagerDao _entityManagerDao;
    private static ISubscribeEventsDAO _subscribeEventsDAO;
    private static IEntityTypeDao _entityTypeDao;
    private static IDaoEventsBean _daoEventsBean;


    public static IDaoEventsBean getDaoEventsBean()
    {
        if (_daoEventsBean == null)
            _daoEventsBean = (IDaoEventsBean) ApplicationRuntime.getBean("daoEventsBean");
        return _daoEventsBean;
    }


    public static IDaoEvents getDaoEvents()
    {
        if (_daoEvents == null)
            _daoEvents = (IDaoEvents) ApplicationRuntime.getBean("daoEvents");
        return _daoEvents;
    }

    public static IWebClientManager getWebClientManager()
    {
        if (_webClientManager == null)
            _webClientManager = (IWebClientManager) ApplicationRuntime.getBean(IWebClientManager.class.getName());
        return _webClientManager;
    }

    public static IEntityManagerDao getEntityManagerDao()
    {
        if (_entityManagerDao == null)
            _entityManagerDao = IEntityManagerDao.instance.get();
        return _entityManagerDao;
    }

    public static ISubscribeEventsDAO getSubscribeEventsDAO()
    {
        if (_subscribeEventsDAO == null)
            _subscribeEventsDAO = (ISubscribeEventsDAO) ApplicationRuntime.getBean(ISubscribeEventsDAO.BEAN_NAME);

        return _subscribeEventsDAO;
    }

    public static IEntityTypeDao getEntityTypeDao()
    {
        if (_entityTypeDao == null)
            _entityTypeDao = (IEntityTypeDao) ApplicationRuntime.getBean(IEntityTypeDao.BEAN_NAME);
        return _entityTypeDao;
    }


}
