package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemPub;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.FormatterColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.JExcelUtil;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unibase.UniBaseUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Number;
import java.util.*;


public class DAO extends DefaultCatalogPubDAO<ExternalSystem, Model> implements IDAO {
    public DAO() {
    }

    @Override
    public void prepareListDataSource(Model model, IDataSettings settings) {
        DynamicListDataSource<Wrapper> ds = model.getDs();

        MQBuilder builder = new MQBuilder(model.getItemClass().getName(), "ci");

        applyFilters(model, builder);

        getOrderDescriptionRegistry().applyOrder(builder, model.getDs().getEntityOrder());

        List<ExternalSystem> list = getList(builder);
        List<Wrapper> result = new ArrayList<>();
        for (ExternalSystem system : list) {
            Wrapper wrapper = new Wrapper();
            wrapper.setExternalSystem(system);
            wrapper.setWs(getTypesWs(system));
            result.add(wrapper);
        }

        UniBaseUtils.createPage(ds, result);
    }

    public String getTypesWs(ExternalSystem system) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ExternalSystemWS.class, "ws")
                .column(DQLExpressions.property(ExternalSystemWS.wsType().title().fromAlias("ws")))
                .where(DQLExpressions.eq(DQLExpressions.property(ExternalSystemWS.externalSystem().id().fromAlias("ws")),
                                         DQLExpressions.value(system)));

        List<String> list = getList(builder);
        if (list.isEmpty())
            return "";
        Set<String> set = new HashSet<>();
        for (String type : list) {
            set.add(type);
        }

        return StringUtils.join(set, ", ");
    }

    @Override
    public byte[] prepareForPrint(Model model) throws WriteException, IOException
    {

        long rowCount = model.getDs().getCountRow();
        boolean mustFindNearestCountRowValue = model.getDs().isMustFindNearestCountRowValue();
        model.getDs().setMustFindNearestCountRowValue(false);
        model.getDs().setCountRow(1000000);


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);
        WritableSheet sheet = workbook.createSheet("Справочник", 0);
        sheet.getSettings().setCopies(1);

        WritableFont arial10bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableCellFormat tableHeader = new WritableCellFormat(arial10bold);
        tableHeader.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        tableHeader.setBackground(jxl.format.Colour.GREY_25_PERCENT);
        tableHeader.setAlignment(jxl.format.Alignment.LEFT);

        WritableFont arial16bold = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
        WritableCellFormat docHeader = new WritableCellFormat(arial16bold);
        String str = "Элементы справочника «" + model.getCatalogTitle() + "» на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date());
        sheet.addCell(new Label(0, 0, str, docHeader));
        sheet.addCell(new Label(0, 2, "№", tableHeader));
        sheet.setColumnView(0, 6);

        WritableCellFormat common = new WritableCellFormat();
        common.setWrap(true);
        common.setAlignment(jxl.format.Alignment.LEFT);
        List<Integer> columnWidth = new ArrayList<>();
        CellView view = new CellView();

        int col = 1;
        IDataSettings settings = DataSettingsFacade.getSettings(model.getSettings().getSettingsOwner(), model.getCatalogCode() + "..columns");
        List<AbstractColumn> hiddenColumns = new ArrayList<>();

        for (AbstractColumn column : model.getDs().getColumns()) {
            if (settings.get(column.getName()) != null) {
                hiddenColumns.add(column);
                continue;
            }

            if (column instanceof FormatterColumn || column instanceof BooleanColumn) {
                String name = column.getCaption();
                int size = calculateColumnWidth(name);
                columnWidth.add(size);
                view.setSize(size);
                sheet.setColumnView(col, view);
                sheet.addCell(new Label(col++, 2, name, tableHeader));
            }
        }

        List list = model.getDs().getEntityList();
        int row = 3;

        if (!model.isHierarchical())
            for (Object obj : list) {
                writeRow(sheet, model, (IEntity) obj, 0, row, common, columnWidth, view, hiddenColumns);
                row++;
            }
        else {
            List<HSelectOption> l = HierarchyUtil.listHierarchyNodesWithParents(list, true);
            for (HSelectOption obj : l) {
                writeRow(sheet, model, (IEntity) obj.getObject(), obj.getLevel(), row, common, columnWidth, view, hiddenColumns);
                row++;
            }
        }

        model.getDs().setMustFindNearestCountRowValue(mustFindNearestCountRowValue);
        model.getDs().setCountRow(rowCount);
        workbook.write();
        workbook.close();
        return out.toByteArray();

    }

    private int calculateColumnWidth(String string)
    {
        int max = 400;
        int size = string.length() * 9;
        if (size > max) return max * 256 / 7;
        return size * 256 / 7;
    }

    @SuppressWarnings("unchecked")
    private void writeRow(WritableSheet sheet, Model model, IEntity entity, int level, int row, WritableCellFormat common, List<Integer> columnWidth, CellView view, List<AbstractColumn> hiddenColumns) throws WriteException
    {

        int col = 1;

        sheet.addCell(JExcelUtil.getNumberNullableCell(0, row, row - 2, common));

        for (AbstractColumn column : model.getDs().getColumns()) {
            //текущая колонка скрыта в таблице
            if (hiddenColumns.contains(column))
                continue;

            String string = "";
            if (column instanceof BooleanColumn) {
                IndicatorColumn.Item item = ((BooleanColumn) column).getContent(entity);
                sheet.addCell(new Label(col, row, item.getLabel(), common));
                string = item.getLabel();
            }
            if (column instanceof FormatterColumn) {
                IFormatter formatter = ((FormatterColumn) column).getFormatter();

                Object value;
                try {
                    value = entity.getProperty(column.getKey());
                }
                catch (Exception ex) {
                    value = null;
                }

                if (formatter != null && !(value instanceof Number && formatter instanceof SimpleFormatter))
                    value = formatter.format(value);

                string = null != value ? value.toString() : "";

                if (value instanceof Number)
                    sheet.addCell(new jxl.write.Number(col, row, ((Number) value).doubleValue(), common));
                else {
                    for (int i = 0; (col == 1) && (i < level); i++)
                        string = "  ".concat(string);
                    sheet.addCell(new Label(col, row, string, common));
                }
            }
            if (column instanceof FormatterColumn || column instanceof BooleanColumn) {
                int size = calculateColumnWidth(string);
                if (size > columnWidth.get(col - 1))
                    columnWidth.set(col - 1, size);
                view.setSize(columnWidth.get(col - 1));
                sheet.setColumnView(col, view);

                col++;
            }
        }

    }
}
