package ru.tandemservice.unievents.component.settings.ExternalNotification.List;


import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unievents.entity.ExternalNotification;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
        model.setSettings(component.getSettings());
        prepareDataSource(component);
    }


    public void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ExternalNotification> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        }, 10);

        dataSource.addColumn(new SimpleColumn("Тип объекта", ExternalNotification.externalEntityType().title()));
        dataSource.addColumn(new SimpleColumn("Тип события", ExternalNotification.externalNotificationEventType().title()));
        dataSource.addColumn(new DateColumn("Дата и время", ExternalNotification.dateNotification(), DateFormatter.PATTERN_WITH_TIME));

        dataSource.addColumn(new PublisherLinkColumn("Событие", ExternalNotification.entityForDaemonWS() + ".title").setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                // это caf компонент
                return null;
            }

            @Override
            public Object getParameters(IEntity ientity)
            {
                @SuppressWarnings("unchecked")
                ExternalNotification entity = (ExternalNotification) ((ViewWrapper<ExternalNotification>) ientity).getEntity();

                if (entity.getEntityForDaemonWS() != null)
                    return ParametersMap.createWith("publisherId", (entity.getEntityForDaemonWS().getId()));
                else
                    return null;
            }
        }).setOrderable(false)); // не сортируем


        dataSource.addColumn(new PublisherLinkColumn("Сотрудник, принявший уведомление", ExternalNotification.employeePost().titleWithShortPostTypeAndStatus())
                                     .setResolver(new IPublisherLinkResolver() {
                                         @Override
                                         public String getComponentName(IEntity ientity) {
                                             // это caf компонент (вроде тандем перевел в CAF)
                                             return null;
                                         }

                                         @Override
                                         public Object getParameters(IEntity ientity)
                                         {
                                             @SuppressWarnings("unchecked")
                                             ExternalNotification entity = (ExternalNotification) ((ViewWrapper<ExternalNotification>) ientity).getEntity();
                                             if (entity.getEmployeePost() != null)
                                                 return ParametersMap.createWith("publisherId", (entity.getEmployeePost().getId()));
                                             else
                                                 return null;
                                         }
                                     })
                                     .setOrderable(false)); // не сортируем

        // внешная система
        dataSource.addColumn(new PublisherLinkColumn("Внешняя система", ExternalNotification.entityForDaemonWS().externalSystem().title()).setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                // это caf компонент
                return null;
            }

            @Override
            public Object getParameters(IEntity ientity)
            {
                @SuppressWarnings("unchecked")
                ExternalNotification entity = (ExternalNotification) ((ViewWrapper<ExternalNotification>) ientity).getEntity();

                if (entity.getEntityForDaemonWS() != null && entity.getEntityForDaemonWS().getExternalSystem() != null)
                    return ParametersMap.createWith("publisherId", (entity.getEntityForDaemonWS().getExternalSystem().getId()));
                else
                    return null;
            }
        }).setOrderable(false)); // не сортируем

        dataSource.addColumn(new SimpleColumn("ID внешней системы", ExternalNotification.externalEntityId()).setOrderable(false));

        // ссылка на сущность тандема
        dataSource.addColumn(new PublisherLinkColumn("Сущность TU", "entityTuTitle").setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }

            @Override
            public Object getParameters(IEntity ientity)
            {
                @SuppressWarnings("unchecked")
                ExternalNotification entity = (ExternalNotification) ((ViewWrapper<ExternalNotification>) ientity).getEntity();

                if (entity.getEntityId() != null)
                    return ParametersMap.createWith("publisherId", entity.getEntityId());
                else
                    return null;
            }
        }).setOrderable(false)); // не сортируем

        dataSource.addColumn(new SimpleColumn("Текст информационного сообщения", ExternalNotification.notification()));
        dataSource.addColumn(new BooleanColumn("Пользователь прочитал уведомление", ExternalNotification.hasAccept()));
        dataSource.addColumn(new ActionColumn("Подтверждение", ActionColumn.EDIT, "onClickSubmit", "Подтвердить уведомление?").setPermissionKey("externalNotificationConfirm"));
        model.setDataSource(dataSource);

    }

    public void onClickSubmit(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        Model model = getModel(component);
        model.setIdExtNotification(id);
        getDao().confirmExtNotification(model);
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
        Model model = getModel(component);
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);
    }

}
