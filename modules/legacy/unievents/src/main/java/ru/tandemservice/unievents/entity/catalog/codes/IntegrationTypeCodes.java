package ru.tandemservice.unievents.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип интеграции"
 * Имя сущности : integrationType
 * Файл data.xml : unievents.data.xml
 */
public interface IntegrationTypeCodes
{
    /** Константа кода (code) элемента : Очередь сообщений (title) */
    String EVENTS = "1";
    /** Константа кода (code) элемента : Бизнес объект (title) */
    String BUSINES_OBJECT = "2";

    Set<String> CODES = ImmutableSet.of(EVENTS, BUSINES_OBJECT);
}
