package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.WsProxyType;
import ru.tandemservice.unievents.entity.catalog.WsType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Web службы внешних систем
 *
 * Список web служб, доступных для внешней системы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExternalSystemWSGen extends EntityBase
 implements INaturalIdentifiable<ExternalSystemWSGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.ExternalSystemWS";
    public static final String ENTITY_NAME = "externalSystemWS";
    public static final int VERSION_HASH = 1494183375;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTERNAL_SYSTEM = "externalSystem";
    public static final String L_WS_TYPE = "wsType";
    public static final String P_WS_URL = "wsUrl";
    public static final String P_WS_TARGET_NAMESPACE = "wsTargetNamespace";
    public static final String P_WS_SERVICE_NAME = "wsServiceName";
    public static final String P_WS_SERVICE_PORT = "wsServicePort";
    public static final String P_WS_USERNAME = "wsUsername";
    public static final String P_WS_PASSWORD = "wsPassword";
    public static final String L_WS_PROXY_TYPE = "wsProxyType";

    private ExternalSystem _externalSystem;     // Внешная система
    private WsType _wsType;     // Тип web службы
    private String _wsUrl;     // URL Веб службы, включая WSDL
    private String _wsTargetNamespace;     // TargetNamespace Веб службы
    private String _wsServiceName;     // ServiceName
    private String _wsServicePort;     // ServicePort
    private String _wsUsername;     // Пользователь на soap
    private String _wsPassword;     // Пароль на soap
    private WsProxyType _wsProxyType;     // Тип прокси

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Внешная система. Свойство не может быть null.
     */
    @NotNull
    public ExternalSystem getExternalSystem()
    {
        return _externalSystem;
    }

    /**
     * @param externalSystem Внешная система. Свойство не может быть null.
     */
    public void setExternalSystem(ExternalSystem externalSystem)
    {
        dirty(_externalSystem, externalSystem);
        _externalSystem = externalSystem;
    }

    /**
     * @return Тип web службы. Свойство не может быть null.
     */
    @NotNull
    public WsType getWsType()
    {
        return _wsType;
    }

    /**
     * @param wsType Тип web службы. Свойство не может быть null.
     */
    public void setWsType(WsType wsType)
    {
        dirty(_wsType, wsType);
        _wsType = wsType;
    }

    /**
     * @return URL Веб службы, включая WSDL.
     */
    @Length(max=255)
    public String getWsUrl()
    {
        return _wsUrl;
    }

    /**
     * @param wsUrl URL Веб службы, включая WSDL.
     */
    public void setWsUrl(String wsUrl)
    {
        dirty(_wsUrl, wsUrl);
        _wsUrl = wsUrl;
    }

    /**
     * @return TargetNamespace Веб службы.
     */
    @Length(max=255)
    public String getWsTargetNamespace()
    {
        return _wsTargetNamespace;
    }

    /**
     * @param wsTargetNamespace TargetNamespace Веб службы.
     */
    public void setWsTargetNamespace(String wsTargetNamespace)
    {
        dirty(_wsTargetNamespace, wsTargetNamespace);
        _wsTargetNamespace = wsTargetNamespace;
    }

    /**
     * @return ServiceName.
     */
    @Length(max=255)
    public String getWsServiceName()
    {
        return _wsServiceName;
    }

    /**
     * @param wsServiceName ServiceName.
     */
    public void setWsServiceName(String wsServiceName)
    {
        dirty(_wsServiceName, wsServiceName);
        _wsServiceName = wsServiceName;
    }

    /**
     * @return ServicePort.
     */
    @Length(max=255)
    public String getWsServicePort()
    {
        return _wsServicePort;
    }

    /**
     * @param wsServicePort ServicePort.
     */
    public void setWsServicePort(String wsServicePort)
    {
        dirty(_wsServicePort, wsServicePort);
        _wsServicePort = wsServicePort;
    }

    /**
     * @return Пользователь на soap.
     */
    @Length(max=255)
    public String getWsUsername()
    {
        return _wsUsername;
    }

    /**
     * @param wsUsername Пользователь на soap.
     */
    public void setWsUsername(String wsUsername)
    {
        dirty(_wsUsername, wsUsername);
        _wsUsername = wsUsername;
    }

    /**
     * @return Пароль на soap.
     */
    @Length(max=255)
    public String getWsPassword()
    {
        return _wsPassword;
    }

    /**
     * @param wsPassword Пароль на soap.
     */
    public void setWsPassword(String wsPassword)
    {
        dirty(_wsPassword, wsPassword);
        _wsPassword = wsPassword;
    }

    /**
     * @return Тип прокси.
     */
    public WsProxyType getWsProxyType()
    {
        return _wsProxyType;
    }

    /**
     * @param wsProxyType Тип прокси.
     */
    public void setWsProxyType(WsProxyType wsProxyType)
    {
        dirty(_wsProxyType, wsProxyType);
        _wsProxyType = wsProxyType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExternalSystemWSGen)
        {
            if (withNaturalIdProperties)
            {
                setExternalSystem(((ExternalSystemWS)another).getExternalSystem());
                setWsType(((ExternalSystemWS)another).getWsType());
            }
            setWsUrl(((ExternalSystemWS)another).getWsUrl());
            setWsTargetNamespace(((ExternalSystemWS)another).getWsTargetNamespace());
            setWsServiceName(((ExternalSystemWS)another).getWsServiceName());
            setWsServicePort(((ExternalSystemWS)another).getWsServicePort());
            setWsUsername(((ExternalSystemWS)another).getWsUsername());
            setWsPassword(((ExternalSystemWS)another).getWsPassword());
            setWsProxyType(((ExternalSystemWS)another).getWsProxyType());
        }
    }

    public INaturalId<ExternalSystemWSGen> getNaturalId()
    {
        return new NaturalId(getExternalSystem(), getWsType());
    }

    public static class NaturalId extends NaturalIdBase<ExternalSystemWSGen>
    {
        private static final String PROXY_NAME = "ExternalSystemWSNaturalProxy";

        private Long _externalSystem;
        private Long _wsType;

        public NaturalId()
        {}

        public NaturalId(ExternalSystem externalSystem, WsType wsType)
        {
            _externalSystem = ((IEntity) externalSystem).getId();
            _wsType = ((IEntity) wsType).getId();
        }

        public Long getExternalSystem()
        {
            return _externalSystem;
        }

        public void setExternalSystem(Long externalSystem)
        {
            _externalSystem = externalSystem;
        }

        public Long getWsType()
        {
            return _wsType;
        }

        public void setWsType(Long wsType)
        {
            _wsType = wsType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof ExternalSystemWSGen.NaturalId) ) return false;

            ExternalSystemWSGen.NaturalId that = (NaturalId) o;

            if( !equals(getExternalSystem(), that.getExternalSystem()) ) return false;
            if( !equals(getWsType(), that.getWsType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExternalSystem());
            result = hashCode(result, getWsType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExternalSystem());
            sb.append("/");
            sb.append(getWsType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExternalSystemWSGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExternalSystemWS.class;
        }

        public T newInstance()
        {
            return (T) new ExternalSystemWS();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "externalSystem":
                    return obj.getExternalSystem();
                case "wsType":
                    return obj.getWsType();
                case "wsUrl":
                    return obj.getWsUrl();
                case "wsTargetNamespace":
                    return obj.getWsTargetNamespace();
                case "wsServiceName":
                    return obj.getWsServiceName();
                case "wsServicePort":
                    return obj.getWsServicePort();
                case "wsUsername":
                    return obj.getWsUsername();
                case "wsPassword":
                    return obj.getWsPassword();
                case "wsProxyType":
                    return obj.getWsProxyType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "externalSystem":
                    obj.setExternalSystem((ExternalSystem) value);
                    return;
                case "wsType":
                    obj.setWsType((WsType) value);
                    return;
                case "wsUrl":
                    obj.setWsUrl((String) value);
                    return;
                case "wsTargetNamespace":
                    obj.setWsTargetNamespace((String) value);
                    return;
                case "wsServiceName":
                    obj.setWsServiceName((String) value);
                    return;
                case "wsServicePort":
                    obj.setWsServicePort((String) value);
                    return;
                case "wsUsername":
                    obj.setWsUsername((String) value);
                    return;
                case "wsPassword":
                    obj.setWsPassword((String) value);
                    return;
                case "wsProxyType":
                    obj.setWsProxyType((WsProxyType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "externalSystem":
                        return true;
                case "wsType":
                        return true;
                case "wsUrl":
                        return true;
                case "wsTargetNamespace":
                        return true;
                case "wsServiceName":
                        return true;
                case "wsServicePort":
                        return true;
                case "wsUsername":
                        return true;
                case "wsPassword":
                        return true;
                case "wsProxyType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "externalSystem":
                    return true;
                case "wsType":
                    return true;
                case "wsUrl":
                    return true;
                case "wsTargetNamespace":
                    return true;
                case "wsServiceName":
                    return true;
                case "wsServicePort":
                    return true;
                case "wsUsername":
                    return true;
                case "wsPassword":
                    return true;
                case "wsProxyType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "externalSystem":
                    return ExternalSystem.class;
                case "wsType":
                    return WsType.class;
                case "wsUrl":
                    return String.class;
                case "wsTargetNamespace":
                    return String.class;
                case "wsServiceName":
                    return String.class;
                case "wsServicePort":
                    return String.class;
                case "wsUsername":
                    return String.class;
                case "wsPassword":
                    return String.class;
                case "wsProxyType":
                    return WsProxyType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExternalSystemWS> _dslPath = new Path<ExternalSystemWS>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExternalSystemWS");
    }
            

    /**
     * @return Внешная система. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getExternalSystem()
     */
    public static ExternalSystem.Path<ExternalSystem> externalSystem()
    {
        return _dslPath.externalSystem();
    }

    /**
     * @return Тип web службы. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsType()
     */
    public static WsType.Path<WsType> wsType()
    {
        return _dslPath.wsType();
    }

    /**
     * @return URL Веб службы, включая WSDL.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsUrl()
     */
    public static PropertyPath<String> wsUrl()
    {
        return _dslPath.wsUrl();
    }

    /**
     * @return TargetNamespace Веб службы.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsTargetNamespace()
     */
    public static PropertyPath<String> wsTargetNamespace()
    {
        return _dslPath.wsTargetNamespace();
    }

    /**
     * @return ServiceName.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsServiceName()
     */
    public static PropertyPath<String> wsServiceName()
    {
        return _dslPath.wsServiceName();
    }

    /**
     * @return ServicePort.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsServicePort()
     */
    public static PropertyPath<String> wsServicePort()
    {
        return _dslPath.wsServicePort();
    }

    /**
     * @return Пользователь на soap.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsUsername()
     */
    public static PropertyPath<String> wsUsername()
    {
        return _dslPath.wsUsername();
    }

    /**
     * @return Пароль на soap.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsPassword()
     */
    public static PropertyPath<String> wsPassword()
    {
        return _dslPath.wsPassword();
    }

    /**
     * @return Тип прокси.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsProxyType()
     */
    public static WsProxyType.Path<WsProxyType> wsProxyType()
    {
        return _dslPath.wsProxyType();
    }

    public static class Path<E extends ExternalSystemWS> extends EntityPath<E>
    {
        private ExternalSystem.Path<ExternalSystem> _externalSystem;
        private WsType.Path<WsType> _wsType;
        private PropertyPath<String> _wsUrl;
        private PropertyPath<String> _wsTargetNamespace;
        private PropertyPath<String> _wsServiceName;
        private PropertyPath<String> _wsServicePort;
        private PropertyPath<String> _wsUsername;
        private PropertyPath<String> _wsPassword;
        private WsProxyType.Path<WsProxyType> _wsProxyType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Внешная система. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getExternalSystem()
     */
        public ExternalSystem.Path<ExternalSystem> externalSystem()
        {
            if(_externalSystem == null )
                _externalSystem = new ExternalSystem.Path<ExternalSystem>(L_EXTERNAL_SYSTEM, this);
            return _externalSystem;
        }

    /**
     * @return Тип web службы. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsType()
     */
        public WsType.Path<WsType> wsType()
        {
            if(_wsType == null )
                _wsType = new WsType.Path<WsType>(L_WS_TYPE, this);
            return _wsType;
        }

    /**
     * @return URL Веб службы, включая WSDL.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsUrl()
     */
        public PropertyPath<String> wsUrl()
        {
            if(_wsUrl == null )
                _wsUrl = new PropertyPath<String>(ExternalSystemWSGen.P_WS_URL, this);
            return _wsUrl;
        }

    /**
     * @return TargetNamespace Веб службы.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsTargetNamespace()
     */
        public PropertyPath<String> wsTargetNamespace()
        {
            if(_wsTargetNamespace == null )
                _wsTargetNamespace = new PropertyPath<String>(ExternalSystemWSGen.P_WS_TARGET_NAMESPACE, this);
            return _wsTargetNamespace;
        }

    /**
     * @return ServiceName.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsServiceName()
     */
        public PropertyPath<String> wsServiceName()
        {
            if(_wsServiceName == null )
                _wsServiceName = new PropertyPath<String>(ExternalSystemWSGen.P_WS_SERVICE_NAME, this);
            return _wsServiceName;
        }

    /**
     * @return ServicePort.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsServicePort()
     */
        public PropertyPath<String> wsServicePort()
        {
            if(_wsServicePort == null )
                _wsServicePort = new PropertyPath<String>(ExternalSystemWSGen.P_WS_SERVICE_PORT, this);
            return _wsServicePort;
        }

    /**
     * @return Пользователь на soap.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsUsername()
     */
        public PropertyPath<String> wsUsername()
        {
            if(_wsUsername == null )
                _wsUsername = new PropertyPath<String>(ExternalSystemWSGen.P_WS_USERNAME, this);
            return _wsUsername;
        }

    /**
     * @return Пароль на soap.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsPassword()
     */
        public PropertyPath<String> wsPassword()
        {
            if(_wsPassword == null )
                _wsPassword = new PropertyPath<String>(ExternalSystemWSGen.P_WS_PASSWORD, this);
            return _wsPassword;
        }

    /**
     * @return Тип прокси.
     * @see ru.tandemservice.unievents.entity.ExternalSystemWS#getWsProxyType()
     */
        public WsProxyType.Path<WsProxyType> wsProxyType()
        {
            if(_wsProxyType == null )
                _wsProxyType = new WsProxyType.Path<WsProxyType>(L_WS_PROXY_TYPE, this);
            return _wsProxyType;
        }

        public Class getEntityClass()
        {
            return ExternalSystemWS.class;
        }

        public String getEntityName()
        {
            return "externalSystemWS";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
