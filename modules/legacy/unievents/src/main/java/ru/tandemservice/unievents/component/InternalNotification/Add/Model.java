package ru.tandemservice.unievents.component.InternalNotification.Add;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unievents.entity.InternalNotification;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entity.id")
})
public class Model {
    private InternalNotification entity = new InternalNotification();
    private ISelectModel entityTypeModel;
    private ISelectModel eventTypeModel;


    public InternalNotification getEntity() {
        return entity;
    }

    public void setEntity(InternalNotification entity) {
        this.entity = entity;
    }

    public ISelectModel getEntityTypeModel() {
        return entityTypeModel;
    }

    public void setEntityTypeModel(ISelectModel entityTypeModel) {
        this.entityTypeModel = entityTypeModel;
    }

    public ISelectModel getEventTypeModel() {
        return eventTypeModel;
    }

    public void setEventTypeModel(ISelectModel eventTypeModel) {
        this.eventTypeModel = eventTypeModel;
    }

}
