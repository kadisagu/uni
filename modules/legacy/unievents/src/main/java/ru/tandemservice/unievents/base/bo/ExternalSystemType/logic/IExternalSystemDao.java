package ru.tandemservice.unievents.base.bo.ExternalSystemType.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

public interface IExternalSystemDao extends INeedPersistenceSupport {
    public ExternalSystemNeedEntityType getExternalSystemNeedEntityType(Long id);

    public ExternalSystem getExternalSystem(Long id);

    public void saveExternalSystemNeedEntityType(ExternalSystemNeedEntityType extSysType);

    public void changeInUseEntry(Long id, Boolean isUse);
}
