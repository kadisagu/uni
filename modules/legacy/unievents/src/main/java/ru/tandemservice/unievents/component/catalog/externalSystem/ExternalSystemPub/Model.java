package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

public class Model extends DefaultCatalogPubModel<ExternalSystem> {
    public Model() {

    }

    private DynamicListDataSource<Wrapper> ds;

    public DynamicListDataSource<Wrapper> getDs() {
        return ds;
    }

    public void setDs(DynamicListDataSource<Wrapper> ds) {
        this.ds = ds;
    }


}
