package ru.tandemservice.unievents.component.catalog.externalEntityType.ExternalEntityTypePub;


import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;

public interface IDAO extends IDefaultCatalogPubDAO<ExternalEntityType, Model> {
}
