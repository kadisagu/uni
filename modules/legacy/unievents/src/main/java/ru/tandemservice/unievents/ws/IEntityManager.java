package ru.tandemservice.unievents.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * интерфейс для доступа к объектам
 * через web службу
 *
 * @author vch
 */
@WebService
public interface IEntityManager {
    @WebMethod
    public String sendRequest(@WebParam(name = "request") String request) throws Exception;

    @WebMethod
    public String sendAnswer(@WebParam(name = "answer") String answer) throws Exception;
}
