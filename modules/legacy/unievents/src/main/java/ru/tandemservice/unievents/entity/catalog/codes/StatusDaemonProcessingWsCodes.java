package ru.tandemservice.unievents.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "статус обработки демоном"
 * Имя сущности : statusDaemonProcessingWs
 * Файл data.xml : unievents.data.xml
 */
public interface StatusDaemonProcessingWsCodes
{
    /** Константа кода (code) элемента : Не обработана (title) */
    String NOT_PROCESSING = "1";
    /** Константа кода (code) элемента : Обработана (title) */
    String PROCESSED = "2";
    /** Константа кода (code) элемента : Ошибка обработки (title) */
    String ERROR = "3";
    /** Константа кода (code) элемента : Пропуск события (title) */
    String PASS_EVENT = "4";

    Set<String> CODES = ImmutableSet.of(NOT_PROCESSING, PROCESSED, ERROR, PASS_EVENT);
}
