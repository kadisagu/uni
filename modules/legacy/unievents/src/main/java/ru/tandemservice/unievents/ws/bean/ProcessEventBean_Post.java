package ru.tandemservice.unievents.ws.bean;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;

import java.util.HashMap;
import java.util.Map;

public class ProcessEventBean_Post extends CatalogProcessEventBean {
    public static final String BEAN_NAME = "eventManagerBean.tsogu_ou.Post1.0";

    public static final String ELEMENT_ID = "PostId";
    public static final String ELEMENT_TITLE = "PostTitle";
    public static final String ELEMENT_CODE = "PostCode";
    public static final String ELEMENT_TYPECODE = "EmployeeTypeCode";
    public static final String ELEMENT_TYPEID = "EmployeeTypeId";

    @Override
    protected Map<String, ParseInfo> getMappings() {
        Map<String, ParseInfo> map = new HashMap<String, ParseInfo>();

        // для поиска (ищем только по полю code)
        map.put(ELEMENT_CODE, new ParseInfo(ELEMENT_CODE, Post.code().s(), String.class, null, true, false));

        // обязательное поле - но флаг не ставим, упадет по ошибке
        map.put(ELEMENT_TITLE, new ParseInfo(ELEMENT_TITLE, Post.title().s(), String.class, null, true, true));

        map.put(ELEMENT_TYPECODE, new ParseInfo(ELEMENT_TYPECODE, Post.employeeType().s(), EmployeeType.class, NodeParsers.ENTITY_BY_CODE, false, false));
        map.put(ELEMENT_TYPEID, new ParseInfo(ELEMENT_TYPEID, Post.employeeType().s(), EmployeeType.class, NodeParsers.ENTITY_BY_EXTERNALID, false, false));

        return map;
    }

    @Override
    protected String getAnswerBeanName() {
        return BEAN_NAME;
    }

    @Override
    protected Class<? extends IEntity> getEntityClass() {
        return Post.class;
    }

    @Override
    protected String getExternalId(BodyElement entityInfo, String forRequestId) {
        return entityInfo.find(ELEMENT_ID).getValue();
    }

}
