package ru.tandemservice.unievents.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Бизнес объект в интеграции"
 * Имя сущности : integrationBusinessObject
 * Файл data.xml : unievents.data.xml
 */
public interface IntegrationBusinessObjectCodes
{
    /** Константа кода (code) элемента : Тестовый бизнес объект (title) */
    String TEST_OBJECT = "1";

    Set<String> CODES = ImmutableSet.of(TEST_OBJECT);
}
