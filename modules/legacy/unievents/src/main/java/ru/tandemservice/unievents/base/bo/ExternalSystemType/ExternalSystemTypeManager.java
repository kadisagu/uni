package ru.tandemservice.unievents.base.bo.ExternalSystemType;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.logic.EntityDSHandler;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.logic.ExternalSystemDao;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.logic.IExternalSystemDao;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

@Configuration
public class ExternalSystemTypeManager extends BusinessObjectManager {
    public static final String EXTERNAL_SYSTEM_DS = "externalSystemDS";
    public static final String TYPE_DS = "typeDS";
    public static final String INTEGRATION_TYPE_DS = "integrationTypeDS";
    public static final String INTEGRATION_BUSINESS_OBJECT_DS = "integrationBusinessObjectTypeDS";


    public static ExternalSystemTypeManager instance()
    {
        return instance(ExternalSystemTypeManager.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> externalSystemDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), ExternalSystem.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> entityTypeDSHandler() {
        return new EntityDSHandler(getName());
    }

    @Bean
    public IExternalSystemDao dao() {
        return new ExternalSystemDao();
    }
}
