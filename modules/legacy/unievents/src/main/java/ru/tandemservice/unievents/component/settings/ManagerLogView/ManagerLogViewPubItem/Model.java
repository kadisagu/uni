package ru.tandemservice.unievents.component.settings.ManagerLogView.ManagerLogViewPubItem;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unievents.entity.EntityManagerLog;


@Input({@Bind(key = "entityId", binding = "entityId")})
public class Model {

    private Long entityId;
    private EntityManagerLog entityManagerLog;

    private String request;
    private String response;


    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public EntityManagerLog getEntityManagerLog() {
        return entityManagerLog;
    }

    public void setEntityManagerLog(EntityManagerLog entityManagerLog) {
        this.entityManagerLog = entityManagerLog;
    }


}
