package ru.tandemservice.unievents.utils.xmlPackUnpack;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * используем для формирования запросов/ответов
 *
 * @author vch
 */
public class XmlDocWS {

    private Document doc = null;
    private Element root = null;
    /**
     * для хранения ошибок
     */
    private Element errorRoot = null;
    private Element requestsRoot = null;
    private Element responsesRoot = null;
    private Element loginRoot = null;

    public List<IError> getErrors() {
        List<IError> errors = new ArrayList<IError>();
        NodeList errorElements = unpackNodeListFromBranch(getRoot(), "Error", "Errors");
        if (errorElements != null) {
            for (int i = 0; i < errorElements.getLength(); i++) {
                WsErrorInfo wsErrorInfo = new WsErrorInfo();
                wsErrorInfo.setErrorCode(unpackElementaryNode((Element) errorElements.item(i), "Code"));
                wsErrorInfo.setErrorMessage(unpackElementaryNode((Element) errorElements.item(i), "Message"));
                errors.add(wsErrorInfo);
            }
            return errors;
        }
        return null;
    }

    public boolean hasErrors() {
        List<IError> errors = getErrors();
        return errors != null && errors.size() > 0;
    }

    public ILogin getLogin() {
        NodeList loginElements = getRoot().getElementsByTagName("Login");
        if (loginElements != null && loginElements.getLength() == 1) {
            ILogin login = new WsLoginInfo();
            login.setPassword(unpackElementaryNode((Element) loginElements.item(0), "Password"));
            login.setUserName(unpackElementaryNode((Element) loginElements.item(0), "UserName"));
            return login;
        }
        return null;
    }

    public List<IRequest> getRequests() {
        List<IRequest> requests = new ArrayList<IRequest>();
        NodeList requestElements = unpackNodeListFromBranch(getRoot(), "Request", "Requests");
        if (requestElements != null) {
            for (int i = 0; i < requestElements.getLength(); i++) {
                IRequest request = new WsRequestInfo();
                request.setRequestId(unpackElementaryNode((Element) requestElements.item(i), "RequestId"));
                request.setBeanName(unpackElementaryNode((Element) requestElements.item(i), "BeanName"));
                request.setAnswerType(unpackElementaryNode((Element) requestElements.item(i), "AnswerType"));
                List<IParameter> parameters = getParameters((Element) requestElements.item(i));
                if (parameters != null)
                    request.setParameters(parameters);
                requests.add(request);
            }
            return requests;
        }
        return null;
    }

    public List<IResponse> getResponses() throws TransformerException {
        List<IResponse> responses = new ArrayList<IResponse>();
        NodeList responseElements = unpackNodeListFromBranch(getRoot(), "Response", "Responses");
        if (responseElements != null && responseElements.getLength() > 0) {
            for (int i = 0; i < responseElements.getLength(); i++) {
                IResponse response = new WsResponseInfo();
                response.setForRequestId(unpackElementaryNode((Element) responseElements.item(i), "ForRequestId"));

                // для запроса получим параметры (их может не быть!!!)
                List<IParameter> parameters = getParameters((Element) responseElements.item(i));
                if (parameters != null)
                    response.setParameters(parameters);

                NodeList bodyList = ((Element) responseElements.item(i)).getElementsByTagName("RequestBody");
                // возможен нюанс, найдется человек, который все сделаетне так
                // хоть в спеке и указано четко RequestBody, но нашелся тот, кто указао RequestBody ResponseBody

                if (bodyList == null || bodyList.getLength() == 0)
                    bodyList = ((Element) responseElements.item(i)).getElementsByTagName("ResponseBody");

                if (bodyList != null && bodyList.getLength() != 0) {
                    response.setRequestBody(null, unpackNodeToXML(bodyList.item(0)));
                }
                responses.add(response);
            }
            return responses;
        }
        return null;
    }

    public List<IParameter> getParameters(Element requestElement) {
        NodeList parameterElements = unpackNodeListFromBranch(requestElement, "Parameter", "Parameters");
        List<IParameter> parameters = new ArrayList<IParameter>();
        if (parameterElements != null && parameterElements.getLength() > 0) {
            for (int i = 0; i < parameterElements.getLength(); i++) {
                IParameter parameter = new WsRequestParam();
                parameter.setParameterName(unpackElementaryNode((Element) parameterElements.item(i), "ParameterName"));
                parameter.setParameterValue(unpackElementaryNode((Element) parameterElements.item(i), "ParameterValue"));
                parameters.add(parameter);
            }
            return parameters;
        }
        return null;
    }


    public XmlDocWS() throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder = null;
        docBuilder = docFactory.newDocumentBuilder();

        doc = docBuilder.newDocument();

        // root elements
        root = doc.createElement("Root");
        doc.appendChild(root);
    }

    public XmlDocWS(String xml)
            throws ParserConfigurationException, IOException, SAXException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        StringReader sr = null;

        try {
            sr = new StringReader(xml);
            InputSource is = new InputSource(sr);
            doc = builder.parse(is);
            root = doc.getDocumentElement();
        }
        finally {
            if (sr != null)
                sr.close();
        }
    }

    public Document getDoc() {
        return doc;
    }

    public Element getRoot() {
        return root;
    }

    public String getStringDocument()
    {
        String output = "";
        StringWriter writer = new StringWriter();

        try {

            // в строку
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer;

            transformer = tf.newTransformer();
            //transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
            //		"yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            StreamResult sr = new StreamResult(writer);
            transformer.transform(new DOMSource(doc), sr);

            output = writer.getBuffer().toString();


        }
        catch (Exception ex) {
            output = "<Root><Errors><Error><Code>100</Code><Message>Системная ошибка преобразования DOM документа к строке</Message></Error></Errors></Root>";
        }
        finally {
            try {
                writer.close();
            }
            catch (IOException e) {

            }
        }
        return output;
    }

    public static String getSystemError()
    {
        return "<Root><Errors><Error><Code>100</Code><Message>Системная ошибка преобразования DOM документа к строке</Message></Error></Errors></Root>";

    }

    public void addError(IError error)
    {
        if (error != null) {
            if (errorRoot == null)
                errorRoot = XmlPackUnpack.PackToElement(getDoc(), getRoot(), "Errors", "");

            Element errorElem = XmlPackUnpack.PackToElement(getDoc(), errorRoot, "Error", "");
            XmlPackUnpack.PackToElement(getDoc(), errorElem, "Code", error.getErrorCode());
            XmlPackUnpack.PackToElement(getDoc(), errorElem, "Message", error.getErrorMessage());
        }
    }

    public void addRequest(IRequest request) {
        if (request != null) {
            if (requestsRoot == null)
                requestsRoot = packEmptyBranchToElement(getRoot(), "Requests");

            Element requestElement = packEmptyBranchToElement(requestsRoot, "Request");

            if (!request.getRequestId().isEmpty())
                XmlPackUnpack.PackToElement(getDoc(), requestElement, "RequestId", request.getRequestId());

            // упаковка параметров
            if (!request.getParameters().isEmpty()) {
                Element paramsRoot = packEmptyBranchToElement(requestElement, "Parameters");
                List<IParameter> params = request.getParameters();
                for (IParameter current : params) {
                    Element parameter = packEmptyBranchToElement(paramsRoot, "Parameter");
                    XmlPackUnpack.PackToElement(getDoc(), parameter, "ParameterName", current.getParameterName());
                    XmlPackUnpack.PackToElement(getDoc(), parameter, "ParameterValue", current.getParameterValue());
                }
            }

            if (!request.getBeanName().isEmpty())
                XmlPackUnpack.PackToElement(getDoc(), requestElement, "BeanName", request.getBeanName());
            if (request.getAnswerType().isEmpty())
                request.setAnswerType(IRequest.DEFAULT_TYPE);
            XmlPackUnpack.PackToElement(getDoc(), requestElement, "AnswerType", request.getAnswerType());
        }
    }

    public void addResponse(IResponse response) {
        if (response != null) {
            if (responsesRoot == null) {
                responsesRoot = packEmptyBranchToElement(getRoot(), "Responses");
            }

            Element responseElement = packEmptyBranchToElement(responsesRoot, "Response");

            // могут быть параметры
            // упаковка параметров
            if (!response.getParameters().isEmpty()) {
                Element paramsRoot = packEmptyBranchToElement(responseElement, "Parameters");
                List<IParameter> params = response.getParameters();
                for (IParameter current : params) {
                    Element parameter = packEmptyBranchToElement(paramsRoot, "Parameter");
                    XmlPackUnpack.PackToElement(getDoc(), parameter, "ParameterName", current.getParameterName());
                    XmlPackUnpack.PackToElement(getDoc(), parameter, "ParameterValue", current.getParameterValue());
                }
            }

            Element responseBody = XmlPackUnpack.PackToElement(getDoc(), responseElement, "RequestBody", response.getRequestBody());
            if (response.getBodyList() != null && !response.getBodyList().isEmpty()) {
                for (IResponse.BodyElement el : response.getBodyList())
                    XmlPackUnpack.PackToElementRec(doc, responseBody, el);
            }
            else
                XmlPackUnpack.PackToElement(getDoc(), responseElement, "RequestBody", response.getRequestBody());


        }
    }

    public void setLogin(ILogin login) throws Exception {
        if (login != null) {
            if (loginRoot == null) {
                loginRoot = XmlPackUnpack.PackToElement(getDoc(), getRoot(), "Login", "");
                XmlPackUnpack.PackToElement(getDoc(), loginRoot, "UserName", login.getUserName());
                XmlPackUnpack.PackToElement(getDoc(), loginRoot, "Password", login.getPassword());
            }
            else
                throw new Exception("more than one login element in the same document");
        }
    }

    private Element packEmptyBranchToElement(Element rootElement, String elementName) {
        return XmlPackUnpack.PackToElement(getDoc(), rootElement, elementName, "");
    }

    private NodeList unpackNodeListFromBranch(Element rootElement, String elementName, String branchName) {
        NodeList elementList = rootElement.getElementsByTagName(branchName);
        if (elementList.getLength() != 0)
            return ((Element) elementList.item(0)).getElementsByTagName(elementName);
        return null;
    }

    private String unpackElementaryNode(Element rootElement, String elementName) {
        NodeList elements = rootElement.getElementsByTagName(elementName);
        if (elements != null && elements.getLength() != 0) {
            return elements.item(0).getTextContent();
        }
        return null;
    }

    private String unpackNodeToXML(Node element) throws TransformerException
    {
        //print xml body of the node
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        NodeList childNodes = element.getChildNodes();
        StringBuilder sb = new StringBuilder();

        StringWriter sw = new StringWriter();

        try {
            for (int i = 0; i < childNodes.getLength(); i++) {
                DOMSource source = new DOMSource(childNodes.item(i));

                sw = new StringWriter();

                StreamResult result = new StreamResult(sw);
                transformer.transform(source, result);
                sb.append(result.getWriter().toString());

                try {
                    sw.close();
                    sw = null;
                }
                catch (IOException e) {

                }
            }
        }
        finally {
            if (sw != null) {
                try {
                    sw.close();
                }
                catch (IOException e) {

                }
            }
        }
        return sb.toString();

    }

    public String getErrorMessage()
    {
        String retVal = "";

        List<IError> lst = getErrors();

        if (!lst.isEmpty()) {
            for (IError err : lst) {
                retVal += "код: " + err.getErrorCode() + " " + err.getErrorMessage();
            }
        }

        return retVal;
    }
}