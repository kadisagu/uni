package ru.tandemservice.unievents.base.bo.EntityForDaemonWS;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unievents.base.bo.EntityForDaemonWS.logic.Dao;
import ru.tandemservice.unievents.base.bo.EntityForDaemonWS.logic.IDao;

@Configuration
public class EntityForDaemonWSManager extends BusinessObjectManager {

    public static EntityForDaemonWSManager instance()
    {
        return instance(EntityForDaemonWSManager.class);
    }

    @Bean
    public IDao getDao() {
        return new Dao();
    }
}
