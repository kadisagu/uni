package ru.tandemservice.unievents.dao;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.shared.commonbase.logging.bo.EventLog.util.Catalog;
import ru.tandemservice.unievents.entity.catalog.InternalEntityType;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.*;


public class EntityTypeDao extends UniBaseDao implements IEntityTypeDao
{

    private static List<EntityType> entityTypeList = null;

    private static Map<Long, EntityType> mapById = null;
    private static Map<String, EntityType> mapByCode = null;

    @Override
    public synchronized List<EntityType> getEntityTypeList()
    {
        if (entityTypeList == null)
        {
            // заполним данными
            List<IEntityMeta> entityTypes = Catalog.getInstance().getEntityTypes();
            List<EntityType> result = new ArrayList<>();
            for (IEntityMeta meta : entityTypes)
            {
                if (meta.getEntityCode() != null)
                {
                    EntityType wrapper = new EntityType(meta.getEntityCode().longValue(), true, meta.getName(), meta.getTitle());
                    result.add(wrapper);
                }
            }
            // типы ОХ
            List<InternalEntityType> itList = getList(InternalEntityType.class);

            for (InternalEntityType it : itList)
            {
                EntityType wrapper = new EntityType(it.getId(), false, it.getEntityCode(), it.getTitle());
                result.add(wrapper);
            }
            Collections.sort(result, ITitled.TITLED_COMPARATOR);
            entityTypeList = new ArrayList<>();
            entityTypeList.addAll(result);

            // разложим в мапы
            mapById = new HashMap<>();
            mapByCode = new HashMap<>();
            for (EntityType et : entityTypeList)
            {
                mapById.put(et.getId(), et);
                mapByCode.put(et.getEntityCode().toLowerCase(), et);
            }

        }
        return entityTypeList;
    }

    @Override
    public EntityType getEntityType(Long id)
    {
        if (mapById == null)
            getEntityTypeList();

        if (mapById.containsKey(id))
            return mapById.get(id);
        else
            return null;
    }

    @Override
    public EntityType getEntityType(String entityCode)
    {
        if (mapByCode == null)
            getEntityTypeList();

        if (mapByCode.containsKey(entityCode.toLowerCase()))
            return mapByCode.get(entityCode.toLowerCase());
        else
            return null;

    }
}
