package ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces;

import java.util.List;

/**
 * Запрос
 *
 * @author vch
 */
public interface IRequest {
    public static final String SYNC_TYPE = "sync";
    public static final String ASYNC_TYPE = "async";
    public static final String DEFAULT_TYPE = ASYNC_TYPE;

    public String getRequestId();

    public void setRequestId(String requestId);

    public String getBeanName();

    public void setBeanName(String beanName);

    public String getAnswerType();

    public void setAnswerType(String answerType);

    public boolean isSyncAnswer();

    public List<IParameter> getParameters();

    public void setParameters(List<IParameter> params);

    public IParameter getParameter(String paramName);

    public boolean hasParameter(String paramName);

}
