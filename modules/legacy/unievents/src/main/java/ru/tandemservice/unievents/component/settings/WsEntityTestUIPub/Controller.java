package ru.tandemservice.unievents.component.settings.WsEntityTestUIPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsEntityManagerXML;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsEntityManagerXMLIstu;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsLoginInfo;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IError;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.ILogin;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;
import ru.tandemservice.uni.dao.IUniDao;

import java.util.List;

public class Controller extends AbstractBusinessController<IUniDao<Model>, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
    }

    public void onClickApply(IBusinessComponent component) throws Exception {
        Model model = getModel(component);

        WsEntityManagerXML entityManagerXMLIstu = new WsEntityManagerXMLIstu();
        try {
            entityManagerXMLIstu.unpackFromXML(model.getXmlString());

            List<IRequest> requests = entityManagerXMLIstu.getRequests();
            List<IResponse> responses = entityManagerXMLIstu.getResponses();
            List<IError> errors = entityManagerXMLIstu.getErrors();

            ILogin login = new WsLoginInfo();
            login.setPassword(entityManagerXMLIstu.getPassword());
            login.setUserName(entityManagerXMLIstu.getUserName());

            WsEntityManagerXML otherManagerXMLIstu = new WsEntityManagerXMLIstu();
            if (requests != null)
                otherManagerXMLIstu.addRequests(requests);

            if (responses != null)
                otherManagerXMLIstu.addResponses(responses);
            if (errors != null) {
                for (IError error : errors)
                    otherManagerXMLIstu.addError(error);
            }
            otherManagerXMLIstu.setUserName(login.getUserName());
            otherManagerXMLIstu.setPassword(login.getPassword());

            model.setSerializedXML(otherManagerXMLIstu.packToXml());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            model.setSerializedXML("Возникла ошибка");
        }
    }
}
