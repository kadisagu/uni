package ru.tandemservice.unievents.base.bo.EntityForDaemonWS.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.logging.bo.EventLog.util.Catalog;
import ru.tandemservice.unievents.entity.DaemonWsLog;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dao extends UniBaseDao implements IDao {
    private Map<String, IEntityMeta> metasMap = new HashMap<String, IEntityMeta>();


    @Override
    public void deleteEntities(Collection<IEntity> entities) {
        //на вход получаем список DataWrapper, из него необходимо вытащить EntityForDaemonWS:
        for (IEntity dwEntity : entities) {
            if (dwEntity instanceof DataWrapper) {
                EntityForDaemonWS entity = (EntityForDaemonWS) dwEntity.getProperty("entity");
                delete(entity);
            }
        }
    }

    @Override
    public List<DaemonWsLog> getLogsByEntityWs(Long entityId)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(DaemonWsLog.class, "log");
        dql.column("log");
        dql.order(DQLExpressions.property(DaemonWsLog.logDate().fromAlias("log")), OrderDirection.desc);

        dql.where(DQLExpressions.eq(
                DQLExpressions.property(DaemonWsLog.entityForDaemonWS().id().fromAlias("log")),
                DQLExpressions.value(entityId)
        ));
        return getList(dql);
    }

    @Override
    public EntityForDaemonWS getEntityById(Long entityId) {
        return get(EntityForDaemonWS.class, entityId);
    }

    @Override
    public DaemonWsLog getLogEntityById(Long entityId) {
        return get(DaemonWsLog.class, entityId);
    }

    @Override
    public IEntityMeta getMetaByName(String metaName) {
        if (metasMap.size() == 0) {
            //инициализируем мап :
            List<IEntityMeta> metasList = Catalog.getInstance().getEntityTypes();
            for (IEntityMeta meta : metasList) {
                metasMap.put(meta.getName(), meta);
            }
        }

        return metasMap.get(StringUtils.uncapitalize(metaName));
    }

}
