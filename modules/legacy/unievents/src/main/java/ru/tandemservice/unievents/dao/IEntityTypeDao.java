package ru.tandemservice.unievents.dao;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;

import java.util.List;

public interface IEntityTypeDao
{

    String BEAN_NAME = "ru.tandemservice.unievents.dao.IEntityTypeDao";

    /**
     * Список типов сущностей (как TU так и OX)
     *
     * @return
     */
    List<EntityType> getEntityTypeList();

    EntityType getEntityType(Long id);

    EntityType getEntityType(String entityCode);

    class EntityType extends IdentifiableWrapper<IEntity>
    {

        /**
         * сущность тандема
         */
        private boolean tuEntity;

        private String entityCode;

        private static final long serialVersionUID = 1L;

        public EntityType(Long id, boolean tuEntity, String entityCode, String title)
        {
            super(id, title);
            this.setEntityCode(entityCode);
            this.setTuEntity(tuEntity);
        }

        public String getEntityCode()
        {
            return entityCode;
        }

        public void setEntityCode(String entityCode)
        {
            this.entityCode = entityCode;
        }

        /**
         * сущность тандема
         */
        public boolean isTuEntity()
        {
            return tuEntity;
        }

        public void setTuEntity(boolean tuEntity)
        {
            this.tuEntity = tuEntity;
        }

    }
}
