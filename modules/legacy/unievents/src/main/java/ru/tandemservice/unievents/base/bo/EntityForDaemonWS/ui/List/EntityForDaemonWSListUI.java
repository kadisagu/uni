package ru.tandemservice.unievents.base.bo.EntityForDaemonWS.ui.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.State;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.unievents.base.bo.EntityForDaemonWS.EntityForDaemonWSManager;
import ru.tandemservice.unievents.dao.DaoEventsDaemon;
//import org.tandemframework.shared.commonbase.base.bo.Common.logic.YesNoSimpleTitledComboDataSourceHandler;

@State({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "publisherId")})
public class EntityForDaemonWSListUI extends UIPresenter {
    public static final String DATE_FROM_FILTER = "dateFrom";
    public static final String DATE_TO_FILTER = "dateTo";
    public static final String STATUSES_FILTER = "statusesList";
    public static final String EXTERNAL_SYSTEMS_FILTER = "externalSystemsList";
    public static final String DIRECTION_FILTER = "direction";
    public static final String ACCEPTED_FILTER = "accepted";
    public static final String META_FILTER = "meta";
    public static final String ENTITYID_FILTER = "entityId";


    private Long _publisherId;

    public void onCallDaemon() {
        DaoEventsDaemon.DAEMON_TOExternal.wakeUpDaemon();
        this.onComponentRefresh();
    }

    public void onDeleteEvents() {
        DynamicListDataSource ds = (DynamicListDataSource) this.getConfig().getDataSource(EntityForDaemonWSList.LIST_DS).getResult();
        CheckboxColumn checkboxColumn = (CheckboxColumn) ds.getColumn("selectUnit");
        EntityForDaemonWSManager.instance().getDao().deleteEntities(checkboxColumn.getSelectedObjects());

        this.onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource ds)
    {
        if (ds.getName().equals(EntityForDaemonWSList.LIST_DS)) {
            ds.put(DATE_FROM_FILTER, getSettings().get(DATE_FROM_FILTER));
            ds.put(DATE_TO_FILTER, getSettings().get(DATE_TO_FILTER));
            ds.put(STATUSES_FILTER, getSettings().get(STATUSES_FILTER));
            ds.put(EXTERNAL_SYSTEMS_FILTER, getSettings().get(EXTERNAL_SYSTEMS_FILTER));
            ds.put(DIRECTION_FILTER, getSettings().get(DIRECTION_FILTER));
            ds.put(META_FILTER, getSettings().get(META_FILTER));
            ds.put(ENTITYID_FILTER, getSettings().get(ENTITYID_FILTER));


            DataWrapper wrapper = getSettings().get(ACCEPTED_FILTER);
            Boolean accepted = TwinComboDataSourceHandler.getSelectedValue(wrapper);
            ds.put(ACCEPTED_FILTER, accepted);

            wrapper = getSettings().get(META_FILTER);
            String entityType = wrapper != null ? StringUtils.capitalize(EntityRuntime.getMeta(wrapper.getId().shortValue()).getName()) : null;
            ds.put(META_FILTER, entityType);
        }
    }

    private Boolean getAccetpedParam() {
        return TwinComboDataSourceHandler.getSelectedValue(getSettings().get(ACCEPTED_FILTER));
    }

    public void onClickSearch() {
        getSettings().save();
    }

    public void onClickClear() {
        getSettings().clear();
    }


    public Long getPublisherId() {
        return _publisherId;
    }

    public void setPublisherId(Long _publisherId) {
        this._publisherId = _publisherId;
    }

}
