package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.InternalNotification;
import ru.tandemservice.unievents.entity.catalog.EntityForDaemonDirection;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.StatusDaemonProcessingWs;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Изменения объектов для демона
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntityForDaemonWSGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.EntityForDaemonWS";
    public static final String ENTITY_NAME = "entityForDaemonWS";
    public static final int VERSION_HASH = -1685595533;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTERNAL_SYSTEM = "externalSystem";
    public static final String L_DIRECTION = "direction";
    public static final String P_EVENT_DATE = "eventDate";
    public static final String P_ENTITY_ID = "entityId";
    public static final String P_ENTITY_TYPE = "entityType";
    public static final String P_EVENT_TYPE = "eventType";
    public static final String P_LAST_DATE = "lastDate";
    public static final String L_STATUS_DAEMON_PROCESSING_WS = "statusDaemonProcessingWs";
    public static final String P_LAST_ERROR_COMMENT = "lastErrorComment";
    public static final String P_PROCESSED_COUNT = "processedCount";
    public static final String P_ACCEPTED = "accepted";
    public static final String P_CAN_BE_PASS = "canBePass";
    public static final String L_INTERNAL_NOTIFICATION = "internalNotification";

    private ExternalSystem _externalSystem;     // Внешная система
    private EntityForDaemonDirection _direction;     // Направление
    private Date _eventDate;     // Дата изменения
    private String _entityId;     // Id сущности
    private String _entityType;     // Тип сущности
    private String _eventType;     // Тип события
    private Date _lastDate;     // Дата последней обработки
    private StatusDaemonProcessingWs _statusDaemonProcessingWs;     // статус обработки демоном WS
    private String _lastErrorComment;     // Описание последней ошибки
    private Long _processedCount;     // Количество обработок
    private boolean _accepted;     // Приянто оператором
    private boolean _canBePass;     // может быть пропущено
    private InternalNotification _internalNotification;     // Уведомления из TU

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Внешная система. Свойство не может быть null.
     */
    @NotNull
    public ExternalSystem getExternalSystem()
    {
        return _externalSystem;
    }

    /**
     * @param externalSystem Внешная система. Свойство не может быть null.
     */
    public void setExternalSystem(ExternalSystem externalSystem)
    {
        dirty(_externalSystem, externalSystem);
        _externalSystem = externalSystem;
    }

    /**
     * @return Направление. Свойство не может быть null.
     */
    @NotNull
    public EntityForDaemonDirection getDirection()
    {
        return _direction;
    }

    /**
     * @param direction Направление. Свойство не может быть null.
     */
    public void setDirection(EntityForDaemonDirection direction)
    {
        dirty(_direction, direction);
        _direction = direction;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getEventDate()
    {
        return _eventDate;
    }

    /**
     * @param eventDate Дата изменения. Свойство не может быть null.
     */
    public void setEventDate(Date eventDate)
    {
        dirty(_eventDate, eventDate);
        _eventDate = eventDate;
    }

    /**
     * @return Id сущности. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Id сущности. Свойство не может быть null.
     */
    public void setEntityId(String entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Тип сущности. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEntityType()
    {
        return _entityType;
    }

    /**
     * @param entityType Тип сущности. Свойство не может быть null.
     */
    public void setEntityType(String entityType)
    {
        dirty(_entityType, entityType);
        _entityType = entityType;
    }

    /**
     * @return Тип события.
     */
    @Length(max=255)
    public String getEventType()
    {
        return _eventType;
    }

    /**
     * @param eventType Тип события.
     */
    public void setEventType(String eventType)
    {
        dirty(_eventType, eventType);
        _eventType = eventType;
    }

    /**
     * @return Дата последней обработки.
     */
    public Date getLastDate()
    {
        return _lastDate;
    }

    /**
     * @param lastDate Дата последней обработки.
     */
    public void setLastDate(Date lastDate)
    {
        dirty(_lastDate, lastDate);
        _lastDate = lastDate;
    }

    /**
     * @return статус обработки демоном WS. Свойство не может быть null.
     */
    @NotNull
    public StatusDaemonProcessingWs getStatusDaemonProcessingWs()
    {
        return _statusDaemonProcessingWs;
    }

    /**
     * @param statusDaemonProcessingWs статус обработки демоном WS. Свойство не может быть null.
     */
    public void setStatusDaemonProcessingWs(StatusDaemonProcessingWs statusDaemonProcessingWs)
    {
        dirty(_statusDaemonProcessingWs, statusDaemonProcessingWs);
        _statusDaemonProcessingWs = statusDaemonProcessingWs;
    }

    /**
     * @return Описание последней ошибки.
     */
    public String getLastErrorComment()
    {
        return _lastErrorComment;
    }

    /**
     * @param lastErrorComment Описание последней ошибки.
     */
    public void setLastErrorComment(String lastErrorComment)
    {
        dirty(_lastErrorComment, lastErrorComment);
        _lastErrorComment = lastErrorComment;
    }

    /**
     * @return Количество обработок.
     *
     * Это формула "(select count(log.id) from DaemonWsLog log where log.entityForDaemonWS.id=id)".
     */
    public Long getProcessedCount()
    {
        return _processedCount;
    }

    /**
     * @param processedCount Количество обработок.
     *
     * @deprecated Это формула.
     */
    @Deprecated // Это формула.
    public void setProcessedCount(Long processedCount)
    {
        dirty(_processedCount, processedCount);
        _processedCount = processedCount;
    }

    /**
     * @return Приянто оператором. Свойство не может быть null.
     */
    @NotNull
    public boolean isAccepted()
    {
        return _accepted;
    }

    /**
     * @param accepted Приянто оператором. Свойство не может быть null.
     */
    public void setAccepted(boolean accepted)
    {
        dirty(_accepted, accepted);
        _accepted = accepted;
    }

    /**
     * @return может быть пропущено. Свойство не может быть null.
     */
    @NotNull
    public boolean isCanBePass()
    {
        return _canBePass;
    }

    /**
     * @param canBePass может быть пропущено. Свойство не может быть null.
     */
    public void setCanBePass(boolean canBePass)
    {
        dirty(_canBePass, canBePass);
        _canBePass = canBePass;
    }

    /**
     * @return Уведомления из TU.
     */
    public InternalNotification getInternalNotification()
    {
        return _internalNotification;
    }

    /**
     * @param internalNotification Уведомления из TU.
     */
    public void setInternalNotification(InternalNotification internalNotification)
    {
        dirty(_internalNotification, internalNotification);
        _internalNotification = internalNotification;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntityForDaemonWSGen)
        {
            setExternalSystem(((EntityForDaemonWS)another).getExternalSystem());
            setDirection(((EntityForDaemonWS)another).getDirection());
            setEventDate(((EntityForDaemonWS)another).getEventDate());
            setEntityId(((EntityForDaemonWS)another).getEntityId());
            setEntityType(((EntityForDaemonWS)another).getEntityType());
            setEventType(((EntityForDaemonWS)another).getEventType());
            setLastDate(((EntityForDaemonWS)another).getLastDate());
            setStatusDaemonProcessingWs(((EntityForDaemonWS)another).getStatusDaemonProcessingWs());
            setLastErrorComment(((EntityForDaemonWS)another).getLastErrorComment());
            setProcessedCount(((EntityForDaemonWS)another).getProcessedCount());
            setAccepted(((EntityForDaemonWS)another).isAccepted());
            setCanBePass(((EntityForDaemonWS)another).isCanBePass());
            setInternalNotification(((EntityForDaemonWS)another).getInternalNotification());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntityForDaemonWSGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntityForDaemonWS.class;
        }

        public T newInstance()
        {
            return (T) new EntityForDaemonWS();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "externalSystem":
                    return obj.getExternalSystem();
                case "direction":
                    return obj.getDirection();
                case "eventDate":
                    return obj.getEventDate();
                case "entityId":
                    return obj.getEntityId();
                case "entityType":
                    return obj.getEntityType();
                case "eventType":
                    return obj.getEventType();
                case "lastDate":
                    return obj.getLastDate();
                case "statusDaemonProcessingWs":
                    return obj.getStatusDaemonProcessingWs();
                case "lastErrorComment":
                    return obj.getLastErrorComment();
                case "processedCount":
                    return obj.getProcessedCount();
                case "accepted":
                    return obj.isAccepted();
                case "canBePass":
                    return obj.isCanBePass();
                case "internalNotification":
                    return obj.getInternalNotification();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "externalSystem":
                    obj.setExternalSystem((ExternalSystem) value);
                    return;
                case "direction":
                    obj.setDirection((EntityForDaemonDirection) value);
                    return;
                case "eventDate":
                    obj.setEventDate((Date) value);
                    return;
                case "entityId":
                    obj.setEntityId((String) value);
                    return;
                case "entityType":
                    obj.setEntityType((String) value);
                    return;
                case "eventType":
                    obj.setEventType((String) value);
                    return;
                case "lastDate":
                    obj.setLastDate((Date) value);
                    return;
                case "statusDaemonProcessingWs":
                    obj.setStatusDaemonProcessingWs((StatusDaemonProcessingWs) value);
                    return;
                case "lastErrorComment":
                    obj.setLastErrorComment((String) value);
                    return;
                case "processedCount":
                    obj.setProcessedCount((Long) value);
                    return;
                case "accepted":
                    obj.setAccepted((Boolean) value);
                    return;
                case "canBePass":
                    obj.setCanBePass((Boolean) value);
                    return;
                case "internalNotification":
                    obj.setInternalNotification((InternalNotification) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "externalSystem":
                        return true;
                case "direction":
                        return true;
                case "eventDate":
                        return true;
                case "entityId":
                        return true;
                case "entityType":
                        return true;
                case "eventType":
                        return true;
                case "lastDate":
                        return true;
                case "statusDaemonProcessingWs":
                        return true;
                case "lastErrorComment":
                        return true;
                case "processedCount":
                        return true;
                case "accepted":
                        return true;
                case "canBePass":
                        return true;
                case "internalNotification":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "externalSystem":
                    return true;
                case "direction":
                    return true;
                case "eventDate":
                    return true;
                case "entityId":
                    return true;
                case "entityType":
                    return true;
                case "eventType":
                    return true;
                case "lastDate":
                    return true;
                case "statusDaemonProcessingWs":
                    return true;
                case "lastErrorComment":
                    return true;
                case "processedCount":
                    return true;
                case "accepted":
                    return true;
                case "canBePass":
                    return true;
                case "internalNotification":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "externalSystem":
                    return ExternalSystem.class;
                case "direction":
                    return EntityForDaemonDirection.class;
                case "eventDate":
                    return Date.class;
                case "entityId":
                    return String.class;
                case "entityType":
                    return String.class;
                case "eventType":
                    return String.class;
                case "lastDate":
                    return Date.class;
                case "statusDaemonProcessingWs":
                    return StatusDaemonProcessingWs.class;
                case "lastErrorComment":
                    return String.class;
                case "processedCount":
                    return Long.class;
                case "accepted":
                    return Boolean.class;
                case "canBePass":
                    return Boolean.class;
                case "internalNotification":
                    return InternalNotification.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntityForDaemonWS> _dslPath = new Path<EntityForDaemonWS>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntityForDaemonWS");
    }
            

    /**
     * @return Внешная система. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getExternalSystem()
     */
    public static ExternalSystem.Path<ExternalSystem> externalSystem()
    {
        return _dslPath.externalSystem();
    }

    /**
     * @return Направление. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getDirection()
     */
    public static EntityForDaemonDirection.Path<EntityForDaemonDirection> direction()
    {
        return _dslPath.direction();
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getEventDate()
     */
    public static PropertyPath<Date> eventDate()
    {
        return _dslPath.eventDate();
    }

    /**
     * @return Id сущности. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getEntityId()
     */
    public static PropertyPath<String> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Тип сущности. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getEntityType()
     */
    public static PropertyPath<String> entityType()
    {
        return _dslPath.entityType();
    }

    /**
     * @return Тип события.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getEventType()
     */
    public static PropertyPath<String> eventType()
    {
        return _dslPath.eventType();
    }

    /**
     * @return Дата последней обработки.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getLastDate()
     */
    public static PropertyPath<Date> lastDate()
    {
        return _dslPath.lastDate();
    }

    /**
     * @return статус обработки демоном WS. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getStatusDaemonProcessingWs()
     */
    public static StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs> statusDaemonProcessingWs()
    {
        return _dslPath.statusDaemonProcessingWs();
    }

    /**
     * @return Описание последней ошибки.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getLastErrorComment()
     */
    public static PropertyPath<String> lastErrorComment()
    {
        return _dslPath.lastErrorComment();
    }

    /**
     * @return Количество обработок.
     *
     * Это формула "(select count(log.id) from DaemonWsLog log where log.entityForDaemonWS.id=id)".
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getProcessedCount()
     */
    public static PropertyPath<Long> processedCount()
    {
        return _dslPath.processedCount();
    }

    /**
     * @return Приянто оператором. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#isAccepted()
     */
    public static PropertyPath<Boolean> accepted()
    {
        return _dslPath.accepted();
    }

    /**
     * @return может быть пропущено. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#isCanBePass()
     */
    public static PropertyPath<Boolean> canBePass()
    {
        return _dslPath.canBePass();
    }

    /**
     * @return Уведомления из TU.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getInternalNotification()
     */
    public static InternalNotification.Path<InternalNotification> internalNotification()
    {
        return _dslPath.internalNotification();
    }

    public static class Path<E extends EntityForDaemonWS> extends EntityPath<E>
    {
        private ExternalSystem.Path<ExternalSystem> _externalSystem;
        private EntityForDaemonDirection.Path<EntityForDaemonDirection> _direction;
        private PropertyPath<Date> _eventDate;
        private PropertyPath<String> _entityId;
        private PropertyPath<String> _entityType;
        private PropertyPath<String> _eventType;
        private PropertyPath<Date> _lastDate;
        private StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs> _statusDaemonProcessingWs;
        private PropertyPath<String> _lastErrorComment;
        private PropertyPath<Long> _processedCount;
        private PropertyPath<Boolean> _accepted;
        private PropertyPath<Boolean> _canBePass;
        private InternalNotification.Path<InternalNotification> _internalNotification;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Внешная система. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getExternalSystem()
     */
        public ExternalSystem.Path<ExternalSystem> externalSystem()
        {
            if(_externalSystem == null )
                _externalSystem = new ExternalSystem.Path<ExternalSystem>(L_EXTERNAL_SYSTEM, this);
            return _externalSystem;
        }

    /**
     * @return Направление. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getDirection()
     */
        public EntityForDaemonDirection.Path<EntityForDaemonDirection> direction()
        {
            if(_direction == null )
                _direction = new EntityForDaemonDirection.Path<EntityForDaemonDirection>(L_DIRECTION, this);
            return _direction;
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getEventDate()
     */
        public PropertyPath<Date> eventDate()
        {
            if(_eventDate == null )
                _eventDate = new PropertyPath<Date>(EntityForDaemonWSGen.P_EVENT_DATE, this);
            return _eventDate;
        }

    /**
     * @return Id сущности. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getEntityId()
     */
        public PropertyPath<String> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<String>(EntityForDaemonWSGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Тип сущности. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getEntityType()
     */
        public PropertyPath<String> entityType()
        {
            if(_entityType == null )
                _entityType = new PropertyPath<String>(EntityForDaemonWSGen.P_ENTITY_TYPE, this);
            return _entityType;
        }

    /**
     * @return Тип события.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getEventType()
     */
        public PropertyPath<String> eventType()
        {
            if(_eventType == null )
                _eventType = new PropertyPath<String>(EntityForDaemonWSGen.P_EVENT_TYPE, this);
            return _eventType;
        }

    /**
     * @return Дата последней обработки.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getLastDate()
     */
        public PropertyPath<Date> lastDate()
        {
            if(_lastDate == null )
                _lastDate = new PropertyPath<Date>(EntityForDaemonWSGen.P_LAST_DATE, this);
            return _lastDate;
        }

    /**
     * @return статус обработки демоном WS. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getStatusDaemonProcessingWs()
     */
        public StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs> statusDaemonProcessingWs()
        {
            if(_statusDaemonProcessingWs == null )
                _statusDaemonProcessingWs = new StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs>(L_STATUS_DAEMON_PROCESSING_WS, this);
            return _statusDaemonProcessingWs;
        }

    /**
     * @return Описание последней ошибки.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getLastErrorComment()
     */
        public PropertyPath<String> lastErrorComment()
        {
            if(_lastErrorComment == null )
                _lastErrorComment = new PropertyPath<String>(EntityForDaemonWSGen.P_LAST_ERROR_COMMENT, this);
            return _lastErrorComment;
        }

    /**
     * @return Количество обработок.
     *
     * Это формула "(select count(log.id) from DaemonWsLog log where log.entityForDaemonWS.id=id)".
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getProcessedCount()
     */
        public PropertyPath<Long> processedCount()
        {
            if(_processedCount == null )
                _processedCount = new PropertyPath<Long>(EntityForDaemonWSGen.P_PROCESSED_COUNT, this);
            return _processedCount;
        }

    /**
     * @return Приянто оператором. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#isAccepted()
     */
        public PropertyPath<Boolean> accepted()
        {
            if(_accepted == null )
                _accepted = new PropertyPath<Boolean>(EntityForDaemonWSGen.P_ACCEPTED, this);
            return _accepted;
        }

    /**
     * @return может быть пропущено. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#isCanBePass()
     */
        public PropertyPath<Boolean> canBePass()
        {
            if(_canBePass == null )
                _canBePass = new PropertyPath<Boolean>(EntityForDaemonWSGen.P_CAN_BE_PASS, this);
            return _canBePass;
        }

    /**
     * @return Уведомления из TU.
     * @see ru.tandemservice.unievents.entity.EntityForDaemonWS#getInternalNotification()
     */
        public InternalNotification.Path<InternalNotification> internalNotification()
        {
            if(_internalNotification == null )
                _internalNotification = new InternalNotification.Path<InternalNotification>(L_INTERNAL_NOTIFICATION, this);
            return _internalNotification;
        }

        public Class getEntityClass()
        {
            return EntityForDaemonWS.class;
        }

        public String getEntityName()
        {
            return "entityForDaemonWS";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
