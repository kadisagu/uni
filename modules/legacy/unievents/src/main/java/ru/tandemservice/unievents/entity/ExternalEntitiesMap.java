package ru.tandemservice.unievents.entity;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unievents.entity.gen.ExternalEntitiesMapGen;

/**
 * Идентификатор сущности внешней системы
 * <p/>
 * Идентификаторы сущностей внешних систем вместе с привязками к соответствующим сущностям в Юни. Требуется для интеграции с внешними системами.
 */
public class ExternalEntitiesMap extends ExternalEntitiesMapGen {
    /**
     * временная переменная, используется при сохранении сущности
     */
    private IEntity entityTUTempLink = null;

    public void setEntityTUTempLink(IEntity entityTUTempLink) {
        this.entityTUTempLink = entityTUTempLink;
    }

    public IEntity getEntityTUTempLink() {
        return entityTUTempLink;
    }

}