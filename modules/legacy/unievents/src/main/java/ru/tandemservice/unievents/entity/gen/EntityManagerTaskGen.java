package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unievents.entity.EntityManagerTask;
import ru.tandemservice.unievents.entity.catalog.StatusDaemonProcessingWs;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Асинхронный запрос EntityManager (в обоих направлениях)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntityManagerTaskGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.EntityManagerTask";
    public static final String ENTITY_NAME = "entityManagerTask";
    public static final int VERSION_HASH = 1604709560;
    private static IEntityMeta ENTITY_META;

    public static final String P_INTERNAL = "internal";
    public static final String P_REQUEST_ID = "requestId";
    public static final String P_REQUEST_DATE = "requestDate";
    public static final String P_RESPONSE_DATE = "responseDate";
    public static final String P_REQUEST_BEAN_NAME = "requestBeanName";
    public static final String P_USER_NAME = "userName";
    public static final String P_USER_PASS = "userPass";
    public static final String L_STATUS = "status";
    public static final String P_INWORK = "inwork";
    public static final String L_RESPONSE = "response";

    private boolean _internal;     // Наш сервис
    private String _requestId;     // ID запроса
    private Date _requestDate;     // Дата обработки
    private Date _responseDate;     // Дата обработки
    private String _requestBeanName;     // Обработчик запроса
    private String _userName;     // Пользователь внешней системы
    private String _userPass;     // Пароль пользователя внешней системы
    private StatusDaemonProcessingWs _status;     // статус обработки демоном
    private boolean _inwork;     // В работе
    private DatabaseFile _response;     // Результат обработки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Наш сервис. Свойство не может быть null.
     */
    @NotNull
    public boolean isInternal()
    {
        return _internal;
    }

    /**
     * @param internal Наш сервис. Свойство не может быть null.
     */
    public void setInternal(boolean internal)
    {
        dirty(_internal, internal);
        _internal = internal;
    }

    /**
     * @return ID запроса. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getRequestId()
    {
        return _requestId;
    }

    /**
     * @param requestId ID запроса. Свойство не может быть null и должно быть уникальным.
     */
    public void setRequestId(String requestId)
    {
        dirty(_requestId, requestId);
        _requestId = requestId;
    }

    /**
     * @return Дата обработки.
     */
    public Date getRequestDate()
    {
        return _requestDate;
    }

    /**
     * @param requestDate Дата обработки.
     */
    public void setRequestDate(Date requestDate)
    {
        dirty(_requestDate, requestDate);
        _requestDate = requestDate;
    }

    /**
     * @return Дата обработки.
     */
    public Date getResponseDate()
    {
        return _responseDate;
    }

    /**
     * @param responseDate Дата обработки.
     */
    public void setResponseDate(Date responseDate)
    {
        dirty(_responseDate, responseDate);
        _responseDate = responseDate;
    }

    /**
     * @return Обработчик запроса. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getRequestBeanName()
    {
        return _requestBeanName;
    }

    /**
     * @param requestBeanName Обработчик запроса. Свойство не может быть null.
     */
    public void setRequestBeanName(String requestBeanName)
    {
        dirty(_requestBeanName, requestBeanName);
        _requestBeanName = requestBeanName;
    }

    /**
     * @return Пользователь внешней системы.
     */
    @Length(max=255)
    public String getUserName()
    {
        return _userName;
    }

    /**
     * @param userName Пользователь внешней системы.
     */
    public void setUserName(String userName)
    {
        dirty(_userName, userName);
        _userName = userName;
    }

    /**
     * @return Пароль пользователя внешней системы.
     */
    @Length(max=255)
    public String getUserPass()
    {
        return _userPass;
    }

    /**
     * @param userPass Пароль пользователя внешней системы.
     */
    public void setUserPass(String userPass)
    {
        dirty(_userPass, userPass);
        _userPass = userPass;
    }

    /**
     * @return статус обработки демоном. Свойство не может быть null.
     */
    @NotNull
    public StatusDaemonProcessingWs getStatus()
    {
        return _status;
    }

    /**
     * @param status статус обработки демоном. Свойство не может быть null.
     */
    public void setStatus(StatusDaemonProcessingWs status)
    {
        dirty(_status, status);
        _status = status;
    }

    /**
     * @return В работе. Свойство не может быть null.
     */
    @NotNull
    public boolean isInwork()
    {
        return _inwork;
    }

    /**
     * @param inwork В работе. Свойство не может быть null.
     */
    public void setInwork(boolean inwork)
    {
        dirty(_inwork, inwork);
        _inwork = inwork;
    }

    /**
     * @return Результат обработки.
     */
    public DatabaseFile getResponse()
    {
        return _response;
    }

    /**
     * @param response Результат обработки.
     */
    public void setResponse(DatabaseFile response)
    {
        dirty(_response, response);
        _response = response;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntityManagerTaskGen)
        {
            setInternal(((EntityManagerTask)another).isInternal());
            setRequestId(((EntityManagerTask)another).getRequestId());
            setRequestDate(((EntityManagerTask)another).getRequestDate());
            setResponseDate(((EntityManagerTask)another).getResponseDate());
            setRequestBeanName(((EntityManagerTask)another).getRequestBeanName());
            setUserName(((EntityManagerTask)another).getUserName());
            setUserPass(((EntityManagerTask)another).getUserPass());
            setStatus(((EntityManagerTask)another).getStatus());
            setInwork(((EntityManagerTask)another).isInwork());
            setResponse(((EntityManagerTask)another).getResponse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntityManagerTaskGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntityManagerTask.class;
        }

        public T newInstance()
        {
            return (T) new EntityManagerTask();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "internal":
                    return obj.isInternal();
                case "requestId":
                    return obj.getRequestId();
                case "requestDate":
                    return obj.getRequestDate();
                case "responseDate":
                    return obj.getResponseDate();
                case "requestBeanName":
                    return obj.getRequestBeanName();
                case "userName":
                    return obj.getUserName();
                case "userPass":
                    return obj.getUserPass();
                case "status":
                    return obj.getStatus();
                case "inwork":
                    return obj.isInwork();
                case "response":
                    return obj.getResponse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "internal":
                    obj.setInternal((Boolean) value);
                    return;
                case "requestId":
                    obj.setRequestId((String) value);
                    return;
                case "requestDate":
                    obj.setRequestDate((Date) value);
                    return;
                case "responseDate":
                    obj.setResponseDate((Date) value);
                    return;
                case "requestBeanName":
                    obj.setRequestBeanName((String) value);
                    return;
                case "userName":
                    obj.setUserName((String) value);
                    return;
                case "userPass":
                    obj.setUserPass((String) value);
                    return;
                case "status":
                    obj.setStatus((StatusDaemonProcessingWs) value);
                    return;
                case "inwork":
                    obj.setInwork((Boolean) value);
                    return;
                case "response":
                    obj.setResponse((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "internal":
                        return true;
                case "requestId":
                        return true;
                case "requestDate":
                        return true;
                case "responseDate":
                        return true;
                case "requestBeanName":
                        return true;
                case "userName":
                        return true;
                case "userPass":
                        return true;
                case "status":
                        return true;
                case "inwork":
                        return true;
                case "response":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "internal":
                    return true;
                case "requestId":
                    return true;
                case "requestDate":
                    return true;
                case "responseDate":
                    return true;
                case "requestBeanName":
                    return true;
                case "userName":
                    return true;
                case "userPass":
                    return true;
                case "status":
                    return true;
                case "inwork":
                    return true;
                case "response":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "internal":
                    return Boolean.class;
                case "requestId":
                    return String.class;
                case "requestDate":
                    return Date.class;
                case "responseDate":
                    return Date.class;
                case "requestBeanName":
                    return String.class;
                case "userName":
                    return String.class;
                case "userPass":
                    return String.class;
                case "status":
                    return StatusDaemonProcessingWs.class;
                case "inwork":
                    return Boolean.class;
                case "response":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntityManagerTask> _dslPath = new Path<EntityManagerTask>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntityManagerTask");
    }
            

    /**
     * @return Наш сервис. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#isInternal()
     */
    public static PropertyPath<Boolean> internal()
    {
        return _dslPath.internal();
    }

    /**
     * @return ID запроса. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getRequestId()
     */
    public static PropertyPath<String> requestId()
    {
        return _dslPath.requestId();
    }

    /**
     * @return Дата обработки.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getRequestDate()
     */
    public static PropertyPath<Date> requestDate()
    {
        return _dslPath.requestDate();
    }

    /**
     * @return Дата обработки.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getResponseDate()
     */
    public static PropertyPath<Date> responseDate()
    {
        return _dslPath.responseDate();
    }

    /**
     * @return Обработчик запроса. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getRequestBeanName()
     */
    public static PropertyPath<String> requestBeanName()
    {
        return _dslPath.requestBeanName();
    }

    /**
     * @return Пользователь внешней системы.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getUserName()
     */
    public static PropertyPath<String> userName()
    {
        return _dslPath.userName();
    }

    /**
     * @return Пароль пользователя внешней системы.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getUserPass()
     */
    public static PropertyPath<String> userPass()
    {
        return _dslPath.userPass();
    }

    /**
     * @return статус обработки демоном. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getStatus()
     */
    public static StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs> status()
    {
        return _dslPath.status();
    }

    /**
     * @return В работе. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#isInwork()
     */
    public static PropertyPath<Boolean> inwork()
    {
        return _dslPath.inwork();
    }

    /**
     * @return Результат обработки.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getResponse()
     */
    public static DatabaseFile.Path<DatabaseFile> response()
    {
        return _dslPath.response();
    }

    public static class Path<E extends EntityManagerTask> extends EntityPath<E>
    {
        private PropertyPath<Boolean> _internal;
        private PropertyPath<String> _requestId;
        private PropertyPath<Date> _requestDate;
        private PropertyPath<Date> _responseDate;
        private PropertyPath<String> _requestBeanName;
        private PropertyPath<String> _userName;
        private PropertyPath<String> _userPass;
        private StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs> _status;
        private PropertyPath<Boolean> _inwork;
        private DatabaseFile.Path<DatabaseFile> _response;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Наш сервис. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#isInternal()
     */
        public PropertyPath<Boolean> internal()
        {
            if(_internal == null )
                _internal = new PropertyPath<Boolean>(EntityManagerTaskGen.P_INTERNAL, this);
            return _internal;
        }

    /**
     * @return ID запроса. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getRequestId()
     */
        public PropertyPath<String> requestId()
        {
            if(_requestId == null )
                _requestId = new PropertyPath<String>(EntityManagerTaskGen.P_REQUEST_ID, this);
            return _requestId;
        }

    /**
     * @return Дата обработки.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getRequestDate()
     */
        public PropertyPath<Date> requestDate()
        {
            if(_requestDate == null )
                _requestDate = new PropertyPath<Date>(EntityManagerTaskGen.P_REQUEST_DATE, this);
            return _requestDate;
        }

    /**
     * @return Дата обработки.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getResponseDate()
     */
        public PropertyPath<Date> responseDate()
        {
            if(_responseDate == null )
                _responseDate = new PropertyPath<Date>(EntityManagerTaskGen.P_RESPONSE_DATE, this);
            return _responseDate;
        }

    /**
     * @return Обработчик запроса. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getRequestBeanName()
     */
        public PropertyPath<String> requestBeanName()
        {
            if(_requestBeanName == null )
                _requestBeanName = new PropertyPath<String>(EntityManagerTaskGen.P_REQUEST_BEAN_NAME, this);
            return _requestBeanName;
        }

    /**
     * @return Пользователь внешней системы.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getUserName()
     */
        public PropertyPath<String> userName()
        {
            if(_userName == null )
                _userName = new PropertyPath<String>(EntityManagerTaskGen.P_USER_NAME, this);
            return _userName;
        }

    /**
     * @return Пароль пользователя внешней системы.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getUserPass()
     */
        public PropertyPath<String> userPass()
        {
            if(_userPass == null )
                _userPass = new PropertyPath<String>(EntityManagerTaskGen.P_USER_PASS, this);
            return _userPass;
        }

    /**
     * @return статус обработки демоном. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getStatus()
     */
        public StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs> status()
        {
            if(_status == null )
                _status = new StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs>(L_STATUS, this);
            return _status;
        }

    /**
     * @return В работе. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#isInwork()
     */
        public PropertyPath<Boolean> inwork()
        {
            if(_inwork == null )
                _inwork = new PropertyPath<Boolean>(EntityManagerTaskGen.P_INWORK, this);
            return _inwork;
        }

    /**
     * @return Результат обработки.
     * @see ru.tandemservice.unievents.entity.EntityManagerTask#getResponse()
     */
        public DatabaseFile.Path<DatabaseFile> response()
        {
            if(_response == null )
                _response = new DatabaseFile.Path<DatabaseFile>(L_RESPONSE, this);
            return _response;
        }

        public Class getEntityClass()
        {
            return EntityManagerTask.class;
        }

        public String getEntityName()
        {
            return "entityManagerTask";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
