package ru.tandemservice.unievents.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.IntegrationBusinessObject;
import ru.tandemservice.unievents.entity.catalog.IntegrationType;
import ru.tandemservice.unievents.entity.catalog.codes.IntegrationTypeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.List;


public class FillSprav
        extends UniBaseDao implements IFillSprav
{

    @Override
    public ExternalSystem getOrCreateExternalSystem(String code, String title)
    {
        ExternalSystem es = getByCode(ExternalSystem.class, code);
        if (es == null) {
            es = new ExternalSystem();
            es.setCode(code);
        }
        es.setTitle(title);
        saveOrUpdate(es);
        return es;

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public void addEntityTypeToExternalSystem(ExternalSystem externalSystem,
                                              IntegrationBusinessObject businessObject,
                                              Class<?> clazz)
    {
        String name = clazz.getName();
        IEntityMeta meta = EntityRuntime.getMeta(name);
        if (meta == null)
            throw new ApplicationException("Для класса '" + name + "' нет IEntityMeta");

        //meta.getEntityClass();
        // проверим, что на интересующую нас сущность подписок нет
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ExternalSystemNeedEntityType.class, "s")
                .column("s.id")
                .where(DQLExpressions.eq(DQLExpressions.property(ExternalSystemNeedEntityType.externalSystem().id().fromAlias("s")), DQLExpressions.value(externalSystem.getId())))
                .where(DQLExpressions.eq(DQLExpressions.property(ExternalSystemNeedEntityType.entityType().fromAlias("s")), DQLExpressions.value(meta.getName())));
        List<Long> lst = getList(dql);
        if (lst.isEmpty()) {
            // создаем запись
            ExternalSystemNeedEntityType net = new ExternalSystemNeedEntityType();


            net.setBusinessObject(businessObject);
            IntegrationType integrationType = getByCode(IntegrationType.class, IntegrationTypeCodes.BUSINES_OBJECT);

            net.setIntegrationType(integrationType);
            net.setIsUse(false);
            net.setEntityType(meta.getName());
            net.setEntityTypeName(meta.getTitle());
            net.setExternalSystem(externalSystem);

            save(net);
        }
    }

}
