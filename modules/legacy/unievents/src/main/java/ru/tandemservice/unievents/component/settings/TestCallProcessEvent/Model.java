package ru.tandemservice.unievents.component.settings.TestCallProcessEvent;

import org.tandemframework.core.settings.IDataSettings;

public class Model {

    private String resultXML;
    private IDataSettings settings;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public String getResultXML() {
        return resultXML;
    }

    public void setResultXML(String resultXML) {
        this.resultXML = resultXML;
    }

}
