package ru.tandemservice.unievents.component.settings.ProcResponseEntityManager;

import org.tandemframework.core.settings.IDataSettings;

public class Model {

    private IDataSettings settings;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

}
