package ru.tandemservice.unievents.ws.bean;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceStatusType;

import java.util.HashMap;
import java.util.Map;

public class ProcessEventBean_ScienceStatus extends CatalogProcessEventBean {
    public static final String BEAN_NAME = "eventManagerBean.tsogu_ou.ScienceStatus1.0";

    public static final String ELEMENT_ID = "ScienceStatusId";
    public static final String ELEMENT_TITLE = "ScienceStatusTitle";
    public static final String ELEMENT_TYPECODE = "ScienceStatusTypeCode";
    public static final String ELEMENT_TYPEID = "ScienceStatusTypeId";
    public static final String ELEMENT_SHORTTITLE = "ScienceStatusShortTitle";

    @Override
    protected Map<String, ParseInfo> getMappings() {
        Map<String, ParseInfo> map = new HashMap<String, ParseInfo>();

        map.put(ELEMENT_TITLE, new ParseInfo(ELEMENT_TITLE, ScienceStatus.title().s(), String.class, null, true, false));
        map.put(ELEMENT_SHORTTITLE, new ParseInfo(ELEMENT_SHORTTITLE, ScienceStatus.shortTitle().s(), String.class, null, false, false));

        map.put(ELEMENT_TYPECODE, new ParseInfo(ELEMENT_TYPECODE, ScienceStatus.type().s(), ScienceStatusType.class, NodeParsers.ENTITY_BY_CODE, false, true));
        map.put(ELEMENT_TYPEID, new ParseInfo(ELEMENT_TYPEID, ScienceStatus.type().s(), ScienceStatusType.class, NodeParsers.ENTITY_BY_EXTERNALID, false, true));

        return map;
    }

    @Override
    protected String getAnswerBeanName() {
        return BEAN_NAME;
    }

    @Override
    protected Class<? extends IEntity> getEntityClass() {
        return ScienceStatus.class;
    }

    @Override
    protected String getExternalId(BodyElement entityInfo, String forRequestId) {
        return entityInfo.find(ELEMENT_ID).getValue();
    }

}
