package ru.tandemservice.unievents.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.IntegrationBusinessObject;


public interface IFillSprav {
    public static final SpringBeanCache<IFillSprav> instance = new SpringBeanCache<IFillSprav>(IFillSprav.class.getName());

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    ExternalSystem getOrCreateExternalSystem(String code, String title);

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    void addEntityTypeToExternalSystem(ExternalSystem externalSystem, IntegrationBusinessObject businessObject, Class<?> clazz);


}
