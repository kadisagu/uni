package ru.tandemservice.unievents.ws.bean;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeePostStatusCodes;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessEventManagerBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class ProcessEventBean_Employee extends BaseProcessEventBean {
    public static final String BEAN_NAME = "eventManagerBean.tsogu_ou.Employee1.0";

    protected static final String ELEMENT_ID = "EmployeeId";
    protected static final String ELEMENT_EMPLOYEECODE = "EmployeeCode";

    protected static final String ELEMENT_EMPLOYEEDEGREES = "AcademicDegrees";
    protected static final String ELEMENT_EMPLOYEEDEGREE_ID = "AcademicDegreeId";//звание

    protected static final String ELEMENT_EMPLOYEESTATUSES = "AcademicStatuses";
    protected static final String ELEMENT_EMPLOYEESTATUS_ID = "AcademicStatusId";//степень

    protected static final String ELEMENT_PERSON = "Person";
    protected static final String ELEMENT_PERSON_ID = "PersonId";
    protected static final String ELEMENT_PERSON_LNAME = "LastName";
    protected static final String ELEMENT_PERSON_FNAME = "FirstName";
    protected static final String ELEMENT_PERSON_MNAME = "MiddleName";
    protected static final String ELEMENT_PERSON_SEX = "SexCode";

    protected static final String ELEMENT_EMPLOYEEPOSTS = "EmployeePosts";
    protected static final String ELEMENT_EMPLOYEEPOST = "EmployeePost";
    protected static final String ELEMENT_EMPLOYEEPOST_OU = "OrgUnit";
    protected static final String ELEMENT_EMPLOYEEPOST_OUID = "OrgUnitId";
    protected static final String ELEMENT_EMPLOYEEPOST_TYPEID = "PostTypeId";
    protected static final String ELEMENT_EMPLOYEEPOST_ID = "PostId";
    protected static final String ELEMENT_EMPLOYEEPOST_COMPETITIONID = "CompetitionTypeId";
    protected static final String ELEMENT_EMPLOYEEPOST_COMPETITIONDATE = "CompetitionDate";

    protected static final String ELEMENT_EmployeePostId = "EmployeePostId";
    protected static final String ELEMENT_EmployeePostStatusId = "EmployeePostStatusId";
    protected static final String ELEMENT_SET_EXISTED_POSTS = "SetExistedPost";


    /**
     * удалять ли лишние должности у уже существующего сотрудника
     * <p/>
     * Изменения от Афонина
     * <p/>
     * НИКОГДА НЕ УДАЛЯТЬ!!! Проставить статус
     */
    protected boolean SET_EXISTED_POSTS = true;

    @Override
    protected String getAnswerBeanName() {
        return BEAN_NAME;
    }

    @Override
    protected Class<? extends IEntity> getEntityClass() {
        return Employee.class;
    }

    @Override
    protected String getExternalId(IProcessEventManagerBean.BodyElement entityInfo, String forRequestId) {
        return entityInfo.findNotNull(ELEMENT_ID).getValue();
    }

    @Override
    protected IEntity findEntity(IProcessEventManagerBean.BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId)
    {
        Employee employee = null;

        //ищем совпадение по employeeCode
        employee = get(Employee.class, Employee.employeeCode(), entityInfo.findNotNull(ELEMENT_EMPLOYEECODE).getValue());
        if (employee != null)
            return employee;

        // проверяем, а удалять ли лишние должности
        String dropEP = entityInfo.find(ELEMENT_SET_EXISTED_POSTS, false).getValue();
        if (dropEP != null && !dropEP.isEmpty()) {
            if (dropEP.trim().toLowerCase().equals("false"))
                SET_EXISTED_POSTS = false;

            if (dropEP.trim().toLowerCase().equals("0"))
                SET_EXISTED_POSTS = false;
        }

        IProcessEventManagerBean.BodyElement personInfo = entityInfo.findNotNull(ELEMENT_PERSON);
        MQBuilder builder = new MQBuilder(Employee.ENTITY_CLASS, "e");

        // ищем по персон Id
        String personId = personInfo.findNotNull(ELEMENT_PERSON_ID).getValue();
        ExternalEntitiesMap link = getLink(personId, Person.ENTITY_NAME, externalSystem);
        if (link != null) {
            // есть связь с персоной
            builder.add(MQExpression.eq("e", Employee.person().id(), link.getEntityId()));
        }
        else {
            //ищем совпадение по ФИО + пол
            boolean male = "1".equals(personInfo.findNotNull(ELEMENT_PERSON_SEX).getValue());
            builder.add(MQExpression.eq("e", Employee.person().identityCard().lastName(), personInfo.findNotNull(ELEMENT_PERSON_LNAME).getValue()))
                    .add(MQExpression.eq("e", Employee.person().identityCard().firstName(), personInfo.findNotNull(ELEMENT_PERSON_FNAME).getValue()))
                    .add(MQExpression.eq("e", Employee.person().identityCard().middleName(), personInfo.findNotNull(ELEMENT_PERSON_MNAME).getValue()))
                    .add(MQExpression.eq("e", Employee.person().identityCard().sex().code(), male ? SexCodes.MALE : SexCodes.FEMALE));
        }

        List<Employee> list = getList(builder);
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    protected void afterSaveOrUpdateSaveDataItem(
            IProcessEventManagerBean.SaveDataItem item
            , IProcessEventManagerBean.SaveData saveData)
    {
        if (item.getEntity() instanceof Person) {
            Person person = (Person) item.getEntity();
            // сохранили персону
            // до этого сохраняли identityCard

            IdentityCard identityCard = _findIdentityCard(person, saveData);
            if (identityCard != null) {
                if (identityCard.getPerson() == null) {
                    identityCard.setPerson(person);
                    saveOrUpdate(identityCard);
                    getSession().flush();
                }
            }
        }
    }


    private IdentityCard _findIdentityCard(Person person, IProcessEventManagerBean.SaveData saveData)
    {
        IdentityCard findByInstanse = null;
        IdentityCard findById = null;

        for (IProcessEventManagerBean.SaveDataItem item : saveData.getItems()) {
            if (item.getEntity() instanceof IdentityCard) {
                findByInstanse = (IdentityCard) item.getEntity();
                if (person.getIdentityCard() != null
                        && person.getIdentityCard().getId() != null
                        && person.getIdentityCard().getId().equals(person.getIdentityCard()))
                {
                    findById = findByInstanse;
                }
            }
        }
        if (findById != null)
            return findById;

        if (findByInstanse != null)
            return findByInstanse;

        return null;
    }

    @Override
    protected IProcessEventManagerBean.SaveData fillEntity(IEntity entityTU, IProcessEventManagerBean.BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId)
    {
        Employee employee = (Employee) entityTU;

        IProcessEventManagerBean.SaveData saveData = new IProcessEventManagerBean.SaveData();
        Person person = null;
        IdentityCard identityCard = null;

        IProcessEventManagerBean.BodyElement personInfo = entityInfo.findNotNull(ELEMENT_PERSON);

        // ну не нашли сотрудника
        if (employee == null) {
            employee = new Employee();
            // персону нужно попробовать найти дабы не наплодить лишнего
            person = findPerson(entityInfo, externalSystem);

            if (person == null) {
                person = new Person();
                PersonContactData pcd = new PersonContactData();
                person.setContactData(pcd);

                identityCard = new IdentityCard();
                person.setIdentityCard(identityCard);

                //заполняем значениями по умолчанию (только для новой персоны)
                IdentityCardType cardType = getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII);
                identityCard.setCardType(cardType);
                identityCard.setSeria("0000");
                identityCard.setNumber("000000");

                AddressCountry russia = getNotNull(AddressCountry.class, AddressCountry.code(), 0);
                identityCard.setCitizenship(russia);

                // линк для персоны нужно создать
                String personId = personInfo.findNotNull(ELEMENT_PERSON_ID).getValue();

                ExternalEntitiesMap linkPerson = doSaveLink(person, personId, externalSystem, false);

                saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(pcd, false));
                saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(identityCard, false));
                saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(person, false));
                if (linkPerson != null)
                    saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(linkPerson, false));

            }
            else {
                identityCard = person.getIdentityCard();

                saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(identityCard, false));
                saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(person, false));
            }

            employee.setPerson(person);

            // замыкает всегда employee
            saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(employee, false));

        }
        else {
            // сотрудник есть
            person = employee.getPerson();
            identityCard = person.getIdentityCard();

            saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(identityCard, false));
            saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(person, false));

            saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(employee, false));
        }

        saveData.setMainEntity(employee);
        employee.setEmployeeCode(entityInfo.findNotNull(ELEMENT_EMPLOYEECODE).getValue());


        identityCard.setLastName(personInfo.findNotNull(ELEMENT_PERSON_LNAME).getValue());
        identityCard.setFirstName(personInfo.findNotNull(ELEMENT_PERSON_FNAME).getValue());
        identityCard.setMiddleName(personInfo.findNotNull(ELEMENT_PERSON_MNAME).getValue());

        //PersonId, пока пропускаем
        // vch.
        // а почему? А почему линк не сотворить?

        boolean male = "1".equals(personInfo.findNotNull(ELEMENT_PERSON_SEX).getValue());
        Sex sex = getCatalogItem(Sex.class, male ? SexCodes.MALE : SexCodes.FEMALE);
        identityCard.setSex(sex);

        // получаем, даже если атрибута нет
        // так как степени и звания нужно синхронизировать, если на персоне есть, а в xml нет
        // то трем в персоне
        List<IProcessEventManagerBean.BodyElement> degreeList = entityInfo.find(ELEMENT_EMPLOYEEDEGREES).getChildren();
        fillAcademicDegrees(saveData, person, degreeList, externalSystem);

        List<IProcessEventManagerBean.BodyElement> statusList = entityInfo.find(ELEMENT_EMPLOYEESTATUSES).getChildren();
        fillAcademicStatuses(saveData, person, statusList, externalSystem);

        List<IProcessEventManagerBean.BodyElement> postList = entityInfo.findNotNull(ELEMENT_EMPLOYEEPOSTS).getChildren();
        if (!postList.isEmpty())
            fillEmployeePosts(postList, saveData, externalSystem);


        return saveData;
    }

    protected Person findPerson(
            IProcessEventManagerBean.BodyElement entityInfo
            , ExternalSystem externalSystem
    )
    {
        // персону ищем для начала в линках
        // если не нашли в линках, то по полному совпадению фио (без пола конечно)
        IProcessEventManagerBean.BodyElement personInfo = entityInfo.findNotNull(ELEMENT_PERSON);
        String personId = personInfo.findNotNull(ELEMENT_PERSON_ID).getValue();
        ExternalEntitiesMap link = getEasyLink(personId, externalSystem, Person.ENTITY_NAME);

        if (link != null && link.getMetaType().toLowerCase().equals(Person.ENTITY_NAME.toLowerCase()))
            // есть персона
            return get(link.getEntityId());
        else {
            // персоны нет
            // персону иначе не ищем, создаем
            /*
			MQBuilder builder = new MQBuilder(Person.ENTITY_CLASS, "e")
				.add(MQExpression.eq("e", Person.identityCard().lastName(), personInfo.findNotNull(ELEMENT_PERSON_LNAME).getValue()))
				.add(MQExpression.eq("e", Person.identityCard().firstName(), personInfo.findNotNull(ELEMENT_PERSON_FNAME).getValue()))
				.add(MQExpression.eq("e", Person.identityCard().middleName(), personInfo.findNotNull(ELEMENT_PERSON_MNAME).getValue()));
			List<Person> list = getList(builder);
			return !list.isEmpty() ? list.get(0) : null;
			*/
            return null;
        }

    }

    protected void fillAcademicDegrees(IProcessEventManagerBean.SaveData saveData, Person person, List<IProcessEventManagerBean.BodyElement> degreeList, ExternalSystem externalSystem)
    {
        List<PersonAcademicDegree> existList = getList(PersonAcademicDegree.class, PersonAcademicDegree.person(), person, new String[0]);

        List<ScienceDegree> addList = new ArrayList<ScienceDegree>();
        for (IProcessEventManagerBean.BodyElement el : degreeList) {
            String externalId = el.getValue();

            // ищем по коду из внешней системы
            ScienceDegree degree = findCatalogItem(ScienceDegree.class, externalId, externalSystem);
            if (degree == null)
                throw new ApplicationException("Не найдена ученая степень для AcademicDegreeId = " + externalId);
            addList.add(degree);
        }

        // проверим, что у персоны уже есть
        for (PersonAcademicDegree pad : existList) {
            if (!addList.contains(pad.getAcademicDegree()))
                // метим на удаление
                saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(pad, true)); //ставим на удаление лишние
            else
                addList.remove(pad.getAcademicDegree()); //не надо добавлять уже существующие
        }

        //добавим новые внешней системы
        for (ScienceDegree entity : addList) {
            PersonAcademicDegree pad = new PersonAcademicDegree();
            pad.setPerson(person);
            pad.setAcademicDegree(entity);

            saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(pad, false));
        }
    }

    protected void fillAcademicStatuses(IProcessEventManagerBean.SaveData saveData, Person person, List<IProcessEventManagerBean.BodyElement> ststusList, ExternalSystem externalSystem) {
        List<PersonAcademicStatus> existList = getList(PersonAcademicStatus.class, PersonAcademicStatus.person(), person, new String[0]);

        List<ScienceStatus> addList = new ArrayList<ScienceStatus>();
        for (IProcessEventManagerBean.BodyElement el : ststusList) {
            String externalId = el.getValue();

            ScienceStatus status = findCatalogItem(ScienceStatus.class, externalId, externalSystem);
            if (status == null)
                throw new ApplicationException("Не найдено ученое звание для AcademicStatusId = " + externalId);

            addList.add(status);
        }

        for (PersonAcademicStatus pas : existList) {
            if (!addList.contains(pas.getAcademicStatus()))
                saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(pas, true)); //ставим на удаление лишние
            else
                addList.remove(pas.getAcademicStatus()); //не надо добавлять уже существующие
        }

        //добавим новые внешней системы
        for (ScienceStatus entity : addList) {
            PersonAcademicStatus pas = new PersonAcademicStatus();
            pas.setPerson(person);
            pas.setAcademicStatus(entity);

            saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(pas, false));
        }
    }

    public void fillEmployeePosts(List<IProcessEventManagerBean.BodyElement> postList, IProcessEventManagerBean.SaveData saveData, ExternalSystem externalSystem)
    {
        Employee employee = (Employee) saveData.getMainEntity();
        List<EmployeePost> currentList = getList(EmployeePost.class, EmployeePost.employee(), employee);

        for (IProcessEventManagerBean.BodyElement postInfo : postList) {
            //поиск подразделения
            String orgUnitId = postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_OUID).getValue();
            String orgUnitTitle = postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_OU).getValue();

            OrgUnit orgUnit = findOrgUnit(orgUnitId, orgUnitTitle, externalSystem);
            if (orgUnit == null)
                throw new ApplicationException("Не найдено подразделение OrgUnitId = " + orgUnitId + ",  OrgUnit = " + orgUnitTitle);

            PostType postType = findCatalogItem(PostType.class, postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_TYPEID).getValue(), externalSystem);
            if (postType == null)
                throw new ApplicationException("Не найден тип назначения на должность для postTypeId = " + postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_TYPEID).getValue());

            Post post = findPost(postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_ID).getValue(), externalSystem);
            if (post == null)
                throw new ApplicationException("Не найдена должность сотрудника ОУ для PostId = " + postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_ID).getValue());


            // сергей по согласованию с закасчиком просил искать employeePost по id
            // делаю не совсем по постановке
            String employeePostId = postInfo.find(ELEMENT_EmployeePostId, false).getValue();

            String employeePostStatusId = postInfo.find(ELEMENT_EmployeePostStatusId, false).getValue();
            if (employeePostStatusId == null)
                throw new ApplicationException("Не указан атрибут '" + ELEMENT_EmployeePostStatusId + "'");
            // найти значение
            ExternalEntitiesMap link = getEasyLink(employeePostStatusId, externalSystem, EmployeePostStatus.ENTITY_NAME);
            EmployeePostStatus postStatus = null;

            if (link == null)
                throw new ApplicationException("Для значения атрибута '" + ELEMENT_EmployeePostStatusId + "' = " + employeePostStatusId + " не найдено значение ");
            else
                postStatus = get(link.getEntityId());


            CompetitionAssignmentType competitionAssignmentType = findCatalogItem(CompetitionAssignmentType.class, postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_COMPETITIONID).getValue(), externalSystem);
            // Афонин попросил снять признак обязательности
            // if (competitionAssignmentType == null)
            //	throw new ApplicationException("Не найден 'Типы конкурсного назначения на должность' для CompetitionTypeId = " + postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_COMPETITIONID).getValue());

            // найдем среди должностей
            EmployeePost employeePost = findFromExisted(externalSystem, currentList, orgUnit, post, postType, employeePostId);

            if (employeePost == null) {
                employeePost = new EmployeePost();

                employeePost.setEmployee(employee);
                employeePost.setOrgUnit(orgUnit);

                EmployeeWeekWorkLoad hours40 = getNotNull(EmployeeWeekWorkLoad.class, EmployeeWeekWorkLoad.dflt(), Boolean.TRUE);
                employeePost.setWeekWorkLoad(hours40);

                EmployeeWorkWeekDuration days5 = getNotNull(EmployeeWorkWeekDuration.class, EmployeeWorkWeekDuration.dflt(), Boolean.TRUE);
                employeePost.setWorkWeekDuration(days5);

                //EmployeePostStatus postStatus = getCatalogItem(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_ACTIVE);
                employeePost.setPostStatus(postStatus);

                // только для новой записи
                //поиск подходящего "Справочник: Должности и профессии, отнесенные к ПКГ и КУ"
                MQBuilder builder = new MQBuilder(PostBoundedWithQGandQL.ENTITY_CLASS, "e")
                        .add(MQExpression.eq("e", PostBoundedWithQGandQL.post().id(), post.getId()))
                        .add(MQExpression.eq("e", PostBoundedWithQGandQL.profQualificationGroup().userCode(), "0")) //без группы
                        .add(MQExpression.eq("e", PostBoundedWithQGandQL.qualificationLevel().userCode(), "0"));    //"без уровня"
                List<PostBoundedWithQGandQL> postBoundedList = getList(builder);

                PostBoundedWithQGandQL postBounded = !postBoundedList.isEmpty() ? postBoundedList.get(0) : null;

                if (postBounded == null) {
                    postBounded = new PostBoundedWithQGandQL();
                    postBounded.setEnabled(true);

                    postBounded.setCode(getNextCodeSprav(PostBoundedWithQGandQL.class, externalSystem.getBeanPref()));

                    postBounded.setTitle(post.getTitle());
                    postBounded.setPost(post);

                    QualificationLevel qualificationLevel = get(QualificationLevel.class, QualificationLevel.userCode(), "0");
                    if (qualificationLevel == null)
                        throw new ApplicationException("Не найдена позиция в справочнике Квалификационный уровень для user_code = 0");

                    ProfQualificationGroup profQualificationGroup = get(ProfQualificationGroup.class, ProfQualificationGroup.userCode(), "0");
                    if (profQualificationGroup == null)
                        throw new ApplicationException("Не найдена позиция в справочнике Профессионально-квалификационная группа для user_code = 0");

                    postBounded.setProfQualificationGroup(profQualificationGroup); //без группы
                    postBounded.setQualificationLevel(qualificationLevel); ///"без уровня"

                    saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(postBounded, false));

                }
                else if (!postBounded.isEnabled()) {
                    // если запрещено, то разрешить
                    postBounded.setEnabled(true);
                    saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(postBounded, false));
                }

                //поиск подходящего Настройка «Связь типов подразделений и должностей»
                builder = new MQBuilder(OrgUnitTypePostRelation.ENTITY_CLASS, "e")
                        .add(MQExpression.eq("e", OrgUnitTypePostRelation.orgUnitType(), orgUnit.getOrgUnitType()))
                        .add(MQExpression.eq("e", OrgUnitTypePostRelation.postBoundedWithQGandQL().id(), postBounded.getId()));
                List<OrgUnitTypePostRelation> relList = getList(builder);

                OrgUnitTypePostRelation postRelation = !relList.isEmpty() ? relList.get(0) : null;
                if (postRelation == null) {
                    postRelation = new OrgUnitTypePostRelation();

                    postRelation.setOrgUnitType(orgUnit.getOrgUnitType());
                    postRelation.setPostBoundedWithQGandQL(postBounded);
                    postRelation.setMultiPost(true);

                    saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(postRelation, false));

                }
                else {
                    //TODO: надо дописать проверку за множественность назначений
                    // <TODO> vch - это дописать в доку
                    postRelation.setMultiPost(true);
                    saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(postRelation, false));
                }
                employeePost.setPostRelation(postRelation);
            }
            else
                // должность была найдена
                currentList.remove(employeePost);


            if (competitionAssignmentType != null)
                employeePost.setCompetitionType(competitionAssignmentType);

            employeePost.setPostType(postType);
            employeePost.setPostStatus(postStatus);


            try {
                employeePost.setPostDate(new SimpleDateFormat("yyyy-MM-dd").parse(postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_COMPETITIONDATE).getValue()));
            }
            catch (Exception ex) {
                throw new ApplicationException("Не правильный формат поля даты назначения " + postInfo.findNotNull(ELEMENT_EMPLOYEEPOST_COMPETITIONDATE).getValue());
            }

            saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(employeePost, false));

            if (employeePostId != null) {
                ExternalEntitiesMap linkemployeePost = doSaveLink(employeePost, employeePostId, externalSystem, false);
                if (linkemployeePost != null)
                    saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(linkemployeePost, false));
            }

        }

        //ставим на удаление лишние должности сотрудника
        if (SET_EXISTED_POSTS) {
            for (EmployeePost ent : currentList) {
                // переводим в состояние удален
                if (!ent.getPostStatus().getCode().equals(EmployeePostStatusCodes.STATUS_FIRED)) {
                    EmployeePostStatus eps = getByCode(EmployeePostStatus.class, EmployeePostStatusCodes.STATUS_FIRED);
                    ent.setPostStatus(eps);
                    saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(ent, true));
                }
            }
        }
    }

    protected EmployeePost findFromExisted(
            ExternalSystem externalSystem
            , List<EmployeePost> currentList
            , OrgUnit orgUnit
            , Post post
            , PostType postType
            // , CompetitionAssignmentType competitionAssignmentType
            , String employeePostId
    )
    {
        if (employeePostId != null) {
            // id указан - строго поиском через линк
            ExternalEntitiesMap link = getEasyLink(employeePostId, externalSystem, EmployeePost.ENTITY_NAME);
            if (link != null)
                return get(link.getEntityId());
            else
                return null;
        }
        else {
            for (EmployeePost employeePost : currentList) {
                if (!employeePost.getOrgUnit().equals(orgUnit))
                    continue;
                if (!employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().equals(post))
                    continue;
                if (!employeePost.getPostType().equals(postType))
                    continue;

                return employeePost;
            }
        }
        // по CompetitionAssignmentType - не ищем
        return null;
    }

    /**
     * Должность ищем сначала
     * по сопоставлению с внешней системой, затем по полю code
     *
     * @param externalId
     * @param externalSystem
     *
     * @return
     */
    protected Post findPost(String externalId, ExternalSystem externalSystem)
    {
        Post post = findCatalogItem(Post.class, externalId, externalSystem);
        if (post != null)
            return post;

        return get(Post.class, Post.code(), externalId);
    }

    protected OrgUnit findOrgUnit(String orgUnitId, String orgUnitTitle, ExternalSystem externalSystem)
    {
        OrgUnit orgUnit = findCatalogItem(OrgUnit.class, orgUnitId, externalSystem);
        if (orgUnit != null)
            return orgUnit;

        // если не нашли по коду внешней системы, ищем по названию
        List<OrgUnit> list = getList(OrgUnit.class, OrgUnit.title(), orgUnitTitle);
        if (list.size() == 1) {
            // в принципе линк можно и прописать?
            return list.get(0);
        }

        return null;
    }
}
