package ru.tandemservice.unievents.component.catalog.externalEntityType.ExternalEntityTypeAddEdit;


import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;

public interface IDAO extends IDefaultCatalogAddEditDAO<ExternalEntityType, Model> {
}
