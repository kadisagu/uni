package ru.tandemservice.unievents.component.settings.ManagerLogView.ManagerLogViewPubItem;

import ru.tandemservice.unievents.entity.EntityManagerLog;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        if (model.getEntityId() != null) {
            model.setEntityManagerLog(getNotNull(EntityManagerLog.class, model.getEntityId()));
            model.setRequest(new String(model.getEntityManagerLog().getRequest().getContent()));
            model.setResponse(new String(model.getEntityManagerLog().getResponse().getContent()));
        }

    }
}
