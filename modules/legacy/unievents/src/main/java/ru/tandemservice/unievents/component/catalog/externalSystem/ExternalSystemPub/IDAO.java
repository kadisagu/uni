package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemPub;

import jxl.write.WriteException;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

import java.io.IOException;

public interface IDAO extends IDefaultCatalogPubDAO<ExternalSystem, Model> {
    byte[] prepareForPrint(Model paramModel) throws WriteException, IOException;
}
