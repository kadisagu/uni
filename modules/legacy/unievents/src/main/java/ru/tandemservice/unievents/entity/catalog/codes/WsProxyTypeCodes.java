package ru.tandemservice.unievents.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип прокси для ws"
 * Имя сущности : wsProxyType
 * Файл data.xml : unievents.data.xml
 */
public interface WsProxyTypeCodes
{
    /** Константа кода (code) элемента : jaxws (только по wsdl) (title) */
    String JAXWS = "1";
    /** Константа кода (code) элемента : jaxrpc (title) */
    String JAXRPC = "2";
    /** Константа кода (code) элемента : Не полный WSDL (title) */
    String NO_WSDL = "3";

    Set<String> CODES = ImmutableSet.of(JAXWS, JAXRPC, NO_WSDL);
}
