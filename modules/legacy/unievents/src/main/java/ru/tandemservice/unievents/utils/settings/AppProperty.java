package ru.tandemservice.unievents.utils.settings;

import org.tandemframework.core.runtime.ApplicationRuntime;

public class AppProperty
{

    /**
     * Запускать демонов в режиме отладки или нет
     */
    public static boolean RUN_DEMON()
    {
        if (DEBUG_MODE())
        {
            // если ключ не указан - исполнять
            if (ApplicationRuntime.getProperty("debug.rundemon") == null)
                return true;
            else
            {
                // ключ указан
                return Boolean.parseBoolean(ApplicationRuntime.getProperty("debug.rundemon"));
            }
        } else
            // в боевом режиме - исполняем
            return true;

    }

    private static boolean DEBUG_MODE()
    {
        // по умолчанию - false
        return Boolean.parseBoolean(ApplicationRuntime.getProperty("debug.mode"));
    }

}
