package ru.tandemservice.unievents.component.catalog.externalEntityType.ExternalEntityTypeItemPub;


import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;

public interface IDAO extends IDefaultCatalogItemPubDAO<ExternalEntityType, Model> {

}
