package ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces;


import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public interface IXmlPackUnpack {
    /**
     * упаковать в XML для отправки через web службу
     *
     * @return
     */
    public String packToXml() throws Exception;

    /**
     * распаковать все содержимое из XML
     *
     * @param xml
     */
    public void unpackFromXML(String xml) throws IOException, SAXException, ParserConfigurationException, TransformerException;
}
