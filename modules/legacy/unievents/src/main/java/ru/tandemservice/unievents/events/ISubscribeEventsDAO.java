package ru.tandemservice.unievents.events;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import ru.tandemservice.unievents.dao.IDaoEvents;

import java.util.List;


public interface ISubscribeEventsDAO {
    public static final String BEAN_NAME = "ru.tandemservice.unievents.events.ISubscribeEventsDAO";

    public List<EntityInfo> getEntityList(DSetEvent event, List<EntityInfo> initialList);

    public static class EntityInfo {
        private Long id;
        private String classname;
        private DSetEventType eventType = null;
        private boolean fromChild;
        private IEntity entity;
        private IDaoEvents.ExternalSystemNeedEntity externalSystemNeedEntity;


        /**
         * событие можно пропустить при обработке
         */
        private boolean canBePass = false;

        public EntityInfo(Long id, String classname, DSetEventType eventType, IEntity entity, boolean fromChild)
        {
            this.id = id;
            this.classname = classname;
            this.eventType = eventType;
            this.fromChild = fromChild;
            this.entity = entity;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getClassname() {
            return classname;
        }

        public void setClassname(String classname) {
            this.classname = classname;
        }

        public DSetEventType getEventType() {
            return eventType;
        }

        /**
         * Порождено поиском зависимых объектов
         *
         * @return
         */
        public boolean isFromChild()
        {
            return fromChild;
        }

        public void setCanBePass(boolean canBePass) {
            this.canBePass = canBePass;
        }

        public boolean isCanBePass() {
            return canBePass;
        }

        public IEntity getEntity() {
            return entity;
        }

        public void setEntity(IEntity entity) {
            this.entity = entity;
        }

        public IDaoEvents.ExternalSystemNeedEntity getExternalSystemNeedEntity() {
            return externalSystemNeedEntity;
        }

        public void setExternalSystemNeedEntity(IDaoEvents.ExternalSystemNeedEntity externalSystemNeedEntity) {
            this.externalSystemNeedEntity = externalSystemNeedEntity;
        }

        public String getExecuteKey()
        {

            String retVal = Long.toString(getId());
            if (this.getExternalSystemNeedEntity() != null)
                retVal += this.getExternalSystemNeedEntity().getIntegrationTypeCode();

            return retVal;
        }

    }
}
