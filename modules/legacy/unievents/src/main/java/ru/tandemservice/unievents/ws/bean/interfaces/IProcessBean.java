package ru.tandemservice.unievents.ws.bean.interfaces;

import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;

import java.util.List;

/**
 * бин готовит оветы внешним системам
 * на запросы через EntityManager
 *
 * @author vch
 */
public interface IProcessBean
{

    int RECURSION_DEEPTH = 1;
    /**
     * Параметр запроса: список IDs через запятую (если пусто, то все)
     */
    String PARAM_ID = "entityId";

    /**
     * Параметр запроса: сущности какого типа надо возвращать
     */
    String PARAM_ENTITYTYPE = "entityType";

    /**
     * Параметр запроса: какая глубина детализации
     */
    String PARAM_DEEPTH = "deepth";

    /**
     * Номер страницы, используется когда отдаются ВСЕ сущности
     * (нумерация от 0)
     */
    String PARAM_PAGE = "pageNumber";

    /**
     * Имя свойства для фильтрации когда отдаются ВСЕ сущности
     */
    String PARAM_FILTER_PROPERTY = "filterProperty";

    /**
     * Значение свойства для фильтрации когда отдаются ВСЕ сущности
     */
    String PARAM_FILTER_VALUE = "filterValue";

    String PARAM_SAME_ENTITY = "sameentity";

    String PARAM_WRITE_COMMENT = "writecomment";

    IResponse process(List<IParameter> paramList, String requestId, String userName, String password) throws Exception;
}
