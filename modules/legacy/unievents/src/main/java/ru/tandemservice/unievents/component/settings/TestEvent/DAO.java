package ru.tandemservice.unievents.component.settings.TestEvent;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.catalog.EntityForDaemonDirection;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.StatusDaemonProcessingWs;
import ru.tandemservice.unievents.entity.catalog.codes.StatusDaemonProcessingWsCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;

import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {

        model.setExternalSystemModel(new UniQueryFullCheckSelectModel(new String[]{ExternalSystem.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(ExternalSystem.ENTITY_CLASS, alias)
                        .addOrder(alias, ExternalSystem.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, ExternalSystem.title(), filter));

                return builder;
            }
        });

        model.setDirectionModel(new UniQueryFullCheckSelectModel(new String[]{EntityForDaemonDirection.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(EntityForDaemonDirection.ENTITY_CLASS, alias)
                        .addOrder(alias, EntityForDaemonDirection.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, EntityForDaemonDirection.title(), filter));

                return builder;
            }
        });

        model.setEventDate(new Date());
    }

    @Override
    public void update(Model model) {
        IDataSettings settings = model.getSettings();

        ExternalSystem externalSystem = (ExternalSystem) settings.get("externalSystem");
        EntityForDaemonDirection direction = (EntityForDaemonDirection) settings.get("direction");
        String entityId = (String) settings.get("entityId");
        String entityType = (String) settings.get("entityType");
        String eventType = (String) settings.get("eventType");

        EntityForDaemonWS entity = new EntityForDaemonWS();

        entity.setExternalSystem(externalSystem);
        entity.setDirection(direction);
        entity.setEventDate(model.getEventDate());
        entity.setEntityId(entityId);
        entity.setEntityType(entityType);
        entity.setEventType(eventType);
        entity.setCanBePass(false);

        entity.setStatusDaemonProcessingWs(getNotNull(StatusDaemonProcessingWs.class, StatusDaemonProcessingWs.code(), StatusDaemonProcessingWsCodes.NOT_PROCESSING));

        saveOrUpdate(entity);
        model.setEntityForDaemonWS(entity);
    }

}
