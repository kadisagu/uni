package ru.tandemservice.unievents.component.settings.ManagerLogView;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unievents.entity.EntityManagerLog;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Date;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setInternalModel(new LazySimpleSelectModel<>(ImmutableList.of(new IdentifiableWrapper(Model.THRIFTY_YES, "Да"), new IdentifiableWrapper(Model.THRIFTY_NO, "Нет"))));
        model.setOkModel(new LazySimpleSelectModel<>(ImmutableList.of(new IdentifiableWrapper(Model.THRIFTY_YES, "Да"), new IdentifiableWrapper(Model.THRIFTY_NO, "Нет"))));
    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<EntityManagerLog> dataSource = model.getDataSource();

        DQLSelectBuilder builder = createBuilder(model);
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    public DQLSelectBuilder createBuilder(Model model) {

        IDataSettings settings = model.getSettings();

        Object internal = settings.get("internal");
        String method = (String) settings.get("method");
        Date fromDate = (Date) settings.get("fromDate");
        Date toDate = (Date) settings.get("toDate");
        Object ok = settings.get("ok");
        String errorComment = (String) settings.get("errorComment");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntityManagerLog.class, "eml");

        if ((internal instanceof IdentifiableWrapper))
            builder.where(DQLExpressions.eq(DQLExpressions.property(EntityManagerLog.internal().fromAlias("eml")), DQLExpressions.value(Model.THRIFTY_YES.equals(((IdentifiableWrapper) internal).getId()))));

        FilterUtils.applySelectFilter(builder, "eml", EntityManagerLog.method().s(), method);

        if (fromDate != null)
            builder.where(DQLExpressions.ge(
                    DQLExpressions.property(EntityManagerLog.requestDate().fromAlias("eml")),
                    DQLExpressions.valueDate(fromDate)
            ));

        if (toDate != null)
            builder.where(DQLExpressions.le(
                    DQLExpressions.property(EntityManagerLog.requestDate().fromAlias("eml")),
                    DQLExpressions.valueDate(toDate)
            ));

        if ((ok instanceof IdentifiableWrapper)) {
            builder.where(DQLExpressions.eq(DQLExpressions.property(EntityManagerLog.ok().fromAlias("eml")), DQLExpressions.value(Model.THRIFTY_YES.equals(((IdentifiableWrapper) ok).getId()))));
        }
        FilterUtils.applySimpleLikeFilter(builder, "eml", EntityManagerLog.method().s(), errorComment);


        // применим сортировку
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(EntityManagerLog.class, "eml");
        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        return builder;
    }

}
