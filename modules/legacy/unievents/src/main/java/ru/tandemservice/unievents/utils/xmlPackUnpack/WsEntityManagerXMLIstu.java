package ru.tandemservice.unievents.utils.xmlPackUnpack;

import org.xml.sax.SAXException;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IError;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.ILogin;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dvorlov
 * Date: 07.02.13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
public class WsEntityManagerXMLIstu extends WsEntityManagerXML {
    private List<IRequest> requests = new ArrayList<IRequest>();
    private List<IResponse> responses = new ArrayList<IResponse>();
    private List<IError> errors = new ArrayList<IError>();
    private ILogin login = new WsLoginInfo();

    @Override
    public List<IRequest> getRequests() {
        return requests;
    }

    @Override
    public void addRequests(List<IRequest> requests) {
        this.requests.addAll(requests);
    }

    @Override
    public List<IResponse> getResponses() {
        return responses;
    }

    @Override
    public void addResponses(List<IResponse> responses) {
        this.responses.addAll(responses);
    }

    @Override
    public List<IError> getErrors() {
        return errors;
    }

    @Override
    public void addError(IError error) {
        this.errors.add(error);
    }

    @Override
    public void clearError() {
        errors.clear();
    }

    @Override
    public String getUserName() {
        return login.getUserName();
    }

    @Override
    public void setUserName(String userName) {
        login.setUserName(userName);
    }

    @Override
    public String getPassword() {
        return login.getPassword();
    }

    @Override
    public void setPassword(String password) {
        login.setPassword(password);
    }

    @Override
    public String packToXml() throws Exception {
        XmlDocWS docWS = new XmlDocWS();
        docWS.setLogin(login);

        if (!requests.isEmpty()) {
            for (IRequest request : requests)
                docWS.addRequest(request);
        }

        if (!responses.isEmpty()) {
            for (IResponse response : responses)
                docWS.addResponse(response);
        }

        if (!errors.isEmpty()) {
            for (IError error : errors)
                docWS.addError(error);
        }

        return docWS.getStringDocument();
    }

    @Override
    public void unpackFromXML(String xml) throws IOException, SAXException, ParserConfigurationException, TransformerException {
        XmlDocWS docWS = new XmlDocWS(xml);
        login = docWS.getLogin();
        requests = docWS.getRequests();
        responses = docWS.getResponses();
        errors = docWS.getErrors();
    }
}
