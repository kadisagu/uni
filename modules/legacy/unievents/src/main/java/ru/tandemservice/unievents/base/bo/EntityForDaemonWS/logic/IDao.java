package ru.tandemservice.unievents.base.bo.EntityForDaemonWS.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import ru.tandemservice.unievents.entity.DaemonWsLog;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;

import java.util.Collection;
import java.util.List;

public interface IDao extends INeedPersistenceSupport {
    public void deleteEntities(Collection<IEntity> entities);

    public EntityForDaemonWS getEntityById(Long entityId);

    public DaemonWsLog getLogEntityById(Long entityId);

    public List<DaemonWsLog> getLogsByEntityWs(Long entityId);

    public IEntityMeta getMetaByName(String metaName);
}
