package ru.tandemservice.unievents.component.settings.ExternalSystemTest;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.io.Serializable;

public class Model {

    private ISelectModel externalSystemModel;
    private ISelectModel methodModel;
    private String outputXML;
    private IDataSettings settings;
    private ISelectModel entityMetaModel;
    private ISelectModel entityModel;
    private ISelectModel typeModel;

    public ISelectModel getTypeModel() {
        return typeModel;
    }

    public void setTypeModel(ISelectModel typeModel) {
        this.typeModel = typeModel;
    }

    public ISelectModel getEntityMetaModel() {
        return entityMetaModel;
    }

    public void setEntityMetaModel(ISelectModel entityMetaModel) {
        this.entityMetaModel = entityMetaModel;
    }

    public ISelectModel getEntityModel() {
        return entityModel;
    }

    public void setEntityModel(ISelectModel entityModel) {
        this.entityModel = entityModel;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getExternalSystemModel() {
        return externalSystemModel;
    }

    public void setExternalSystemModel(ISelectModel externalSystemModel) {
        this.externalSystemModel = externalSystemModel;
    }

    public String getOutputXML() {
        return outputXML;
    }

    public void setOutputXML(String outputXML) {
        this.outputXML = outputXML;
    }

    public ISelectModel getMethodModel() {
        return methodModel;
    }

    public void setMethodModel(ISelectModel methodModel) {
        this.methodModel = methodModel;
    }

    public static class MetaWrapper extends EntityBase implements Serializable {
        private static final long serialVersionUID = 5704509812956418149L;
        private short metaCode;

        public MetaWrapper(IEntityMeta meta) {
            this.metaCode = meta.getEntityCode();
        }

        @Override
        public Long getId() {
            return (long) metaCode;
        }

        public String getTitle() {
            return getMeta().getTitle();
        }

        public IEntityMeta getMeta() {
            return EntityRuntime.getMeta(this.metaCode);
        }
    }

}
