package ru.tandemservice.unievents.component.settings.ManagerLogView;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unievents.entity.EntityManagerLog;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(DataSettingsFacade.getSettings(component, "ManagerLogView.filter"));
        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);

    }

    public void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);

        DynamicListDataSource<EntityManagerLog> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSource(model);
        });
        dataSource.addColumn(new BooleanColumn("Наш сервис", EntityManagerLog.internal()).setClickable(false));

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Имя метода", EntityManagerLog.method());
        linkColumn.setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return "ru.tandemservice.unievents.component.settings.ManagerLogView.ManagerLogViewPubItem";
            }

            @Override
            public Object getParameters(IEntity entity) {
                return ParametersMap.createWith("entityId", entity.getId());
            }
        });

        dataSource.addColumn(linkColumn.setClickable(true).setPermissionKey("managerLogViewPubItem"));
        dataSource.addColumn(new DateColumn("Время вызова", EntityManagerLog.requestDate(), "dd.MM.yyyy HH:mm").setClickable(false));
        dataSource.addColumn(new BooleanColumn("Нет ошибок", EntityManagerLog.ok()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Описание ошибки", EntityManagerLog.errorComment()).setClickable(false));

        dataSource.setOrder(EntityManagerLog.requestDate().s(), OrderDirection.desc);

        model.setDataSource(dataSource);

    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        model.getDataSource().refresh();

    }

    public void onClickClear(IBusinessComponent context)
    {
        Model model = (Model) getModel(context);
        model.getSettings().clear();
        DataSettingsFacade.saveSettings(model.getSettings());
        model.getDataSource().refresh();
    }
}
