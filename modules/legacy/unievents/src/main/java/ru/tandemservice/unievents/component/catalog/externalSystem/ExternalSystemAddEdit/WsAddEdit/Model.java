package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemAddEdit.WsAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

@Input({@Bind(key = "externalSystemId", binding = "externalSystemId"),
        @Bind(key = "externalSystemWsId", binding = "externalSystemWsId")
})
public class Model {

    private Long externalSystemId;
    private ExternalSystem externalSystem;

    private Long externalSystemWsId;
    private ExternalSystemWS externalSystemWS;
    private ISelectModel wsTypeModel;
    private ISelectModel wsProxyTypeModel;


    public Long getExternalSystemWsId() {
        return externalSystemWsId;
    }

    public void setExternalSystemWsId(Long externalSystemWsId) {
        this.externalSystemWsId = externalSystemWsId;
    }

    public Long getExternalSystemId() {
        return externalSystemId;
    }

    public void setExternalSystemId(Long externalSystemId) {
        this.externalSystemId = externalSystemId;
    }

    public ExternalSystem getExternalSystem() {
        return externalSystem;
    }

    public void setExternalSystem(ExternalSystem externalSystem) {
        this.externalSystem = externalSystem;
    }

    public ExternalSystemWS getExternalSystemWS() {
        return externalSystemWS;
    }

    public void setExternalSystemWS(ExternalSystemWS externalSystemWS) {
        this.externalSystemWS = externalSystemWS;
    }

    public ISelectModel getWsTypeModel() {
        return wsTypeModel;
    }

    public void setWsTypeModel(ISelectModel wsTypeModel) {
        this.wsTypeModel = wsTypeModel;
    }

    public void setWsProxyTypeModel(ISelectModel wsProxyTypeModel) {
        this.wsProxyTypeModel = wsProxyTypeModel;
    }

    public ISelectModel getWsProxyTypeModel() {
        return wsProxyTypeModel;
    }


}
