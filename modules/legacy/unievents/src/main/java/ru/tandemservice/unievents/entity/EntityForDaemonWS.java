package ru.tandemservice.unievents.entity;

import ru.tandemservice.unievents.entity.gen.EntityForDaemonWSGen;

/**
 * Изменения объектов для демона
 */
public class EntityForDaemonWS extends EntityForDaemonWSGen
{
    public String getTitle()
    {
        return "Событие от '" + this.getExternalSystem().getTitle() + "' EntityId= '" + this.getEntityId() + "' Тип '" + this.getEventType() + "' для '" + this.getEntityType() + "'";
    }
}