package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemPub;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

public class Wrapper extends EntityBase {

    private ExternalSystem externalSystem;
    private String ws;

    @Override
    public Long getId() {
        return externalSystem.getId();
    }

    @Override
    public void setId(Long id) {
        super.setId(externalSystem.getId());
    }

    public ExternalSystem getExternalSystem() {
        return externalSystem;
    }

    public void setExternalSystem(ExternalSystem externalSystem) {
        this.externalSystem = externalSystem;
    }

    public String getWs() {
        return ws;
    }

    public void setWs(String ws) {
        this.ws = ws;
    }


}
