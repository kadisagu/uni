package ru.tandemservice.unievents.utils.xmlPackUnpack;

import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IError;

public class WsErrorInfo implements IError {
    private String errorCode = "";
    private String errorMessage = "";

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }


}
