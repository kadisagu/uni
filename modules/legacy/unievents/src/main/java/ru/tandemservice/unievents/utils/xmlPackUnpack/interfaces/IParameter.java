package ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces;

public interface IParameter {
    public String getParameterName();

    public void setParameterName(String parameterName);

    public String getParameterValue();

    public void setParameterValue(String parameterValue);
}
