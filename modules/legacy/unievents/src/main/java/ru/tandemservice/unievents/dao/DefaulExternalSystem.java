package ru.tandemservice.unievents.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

/**
 * возвращает внешную систему, используемую по умолчанию
 *
 * @author vch
 */
public class DefaulExternalSystem {
    public static String BEAN_NAME = "defaulExternalSystem";

    private static DefaulExternalSystem _dao = null;

    public static DefaulExternalSystem instanse()
    {
        if (_dao == null)
            _dao = (DefaulExternalSystem) ApplicationRuntime.getBean(BEAN_NAME);

        return _dao;
    }

    /**
     * Внешная система, используемая по умолчанию
     *
     * @return
     */
    public ExternalSystem getDefaultExternalSystem()
    {

        return null;
    }
}
