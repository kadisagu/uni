package ru.tandemservice.unievents.component.catalog.externalEntityType.ExternalEntityTypePub;


import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.List;

public class DAO extends DefaultCatalogPubDAO<ExternalEntityType, Model> implements IDAO {

    @Override
    public void prepareListDataSource(Model model, IDataSettings settings) {
        DynamicListDataSource<ExternalEntityType> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(model.getItemClass().getName(), "ci");

        applyFilters(model, builder);
        getOrderDescriptionRegistry().applyOrder(builder, model.getDataSource().getEntityOrder());

        List<ExternalEntityType> list = getList(builder);
        UniBaseUtils.createPage(dataSource, list);
    }

}
