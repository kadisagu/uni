package ru.tandemservice.unievents.component.InternalNotification.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unievents.entity.InternalNotification;


public class Model {
    private IDataSettings settings;
    private DynamicListDataSource<InternalNotification> dataSource;

    private ISelectModel entityTypeModel;
    private ISelectModel eventTypeModel;


    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<InternalNotification> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<InternalNotification> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getEntityTypeModel() {
        return entityTypeModel;
    }

    public void setEntityTypeModel(ISelectModel entityTypeModel) {
        this.entityTypeModel = entityTypeModel;
    }

    public ISelectModel getEventTypeModel() {
        return eventTypeModel;
    }

    public void setEventTypeModel(ISelectModel eventTypeModel) {
        this.eventTypeModel = eventTypeModel;
    }
}
