package ru.tandemservice.unievents.ws.fake;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.interceptor.InInterceptors;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.unievents.dao.IEntityManagerDao;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsLoginInfo;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;
import ru.tandemservice.unievents.ws.IEntityManager;
import ru.tandemservice.uni.dao.IUniBaseDao;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.InputStream;
import java.util.List;


@WebService(serviceName = "FakeEntityManager")
@InInterceptors(interceptors = {"ru.tandemservice.unievents.ws.fake.FakeAuthAuthorizationInterceptor"})
public class FakeEntityManager implements IEntityManager {

    public static final String USER = "1C-ent";
    public static final String PASS = "1C";

    public static final String BEAN_PREF = "tsogu_ou";

    @Override
    @WebMethod
    public String sendRequest(@WebParam(name = "request") String request) throws Exception {
        doFillLinks();
        XmlDocWS requestDoc = new XmlDocWS(request);

        String result = doSendRequest(requestDoc);
        XmlDocWS resultDoc = new XmlDocWS(result);

        resultDoc.setLogin(new WsLoginInfo(USER, PASS));

        IEntityManagerDao dao = IEntityManagerDao.instance.get();
        dao.saveLog(false, "sendRequest", request, resultDoc.getStringDocument(), false, null);

        result = resultDoc.getStringDocument();
        String requestId = requestDoc.getRequests().get(0).getRequestId();
        result = result.replace("request1111111", requestId);

        return result;
    }

    @Override
    @WebMethod
    public String sendAnswer(@WebParam(name = "answer") String answer) throws Exception {
        return "";
    }

    private String doSendRequest(XmlDocWS requestDoc) throws Exception {
        String entityType = requestDoc.getRequests().get(0).getParameter("EntityType").getParameterValue();

        InputStream in = this.getClass().getResourceAsStream("fake_" + entityType + ".xml");
        String result = IOUtils.toString(in);
        in.close();

        return result;
    }


    private ExternalSystem getExternalSystemByBeanName(String beanName)
    {
        IUniBaseDao dao = IUniBaseDao.instance.get();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .column("ent")
                .fromEntity(ExternalSystem.class, "ent")
                .where(DQLExpressions.eq(DQLExpressions.property(ExternalSystem.beanPref().fromAlias("ent")), DQLExpressions.value(beanName)));

        List<ExternalSystem> lst = dao.getList(dql);

        if (lst != null && !lst.isEmpty())
            return lst.get(0);
        else
            return null;

    }

    private void doFillLinks() {
        IUniBaseDao dao = IUniBaseDao.instance.get();

        ExternalSystem extSysyem = getExternalSystemByBeanName(BEAN_PREF);

        //AcademicStatusId
        ExternalEntitiesMap link = dao.get(ExternalEntitiesMap.class, ExternalEntitiesMap.externalId(), "as12345");
        if (link == null) {
            ScienceStatus entity = dao.getCatalogItem(ScienceStatus.class, "10"); //доцент
            link = new ExternalEntitiesMap();
            link.setExternalId("as12345");
            link.setEntityId(entity.getId());
            link.setMetaType(EntityRuntime.getMeta(entity).getName());
            link.setExternalSystem(extSysyem);

            dao.saveOrUpdate(link);
        }
        link = dao.get(ExternalEntitiesMap.class, ExternalEntitiesMap.externalId(), "as123456");
        if (link == null) {
            ScienceStatus entity = dao.getCatalogItem(ScienceStatus.class, "11");
            link = new ExternalEntitiesMap();
            link.setExternalId("as123456");
            link.setEntityId(entity.getId());
            link.setMetaType(EntityRuntime.getMeta(entity).getName());
            link.setExternalSystem(extSysyem);

            dao.saveOrUpdate(link);
        }

        //AcademicDegreeId
        link = dao.get(ExternalEntitiesMap.class, ExternalEntitiesMap.externalId(), "ad12345");
        if (link == null) {
            ScienceDegree entity = dao.getCatalogItem(ScienceDegree.class, "26"); //"Кандидат военных наук"
            link = new ExternalEntitiesMap();
            link.setExternalId("ad12345");
            link.setEntityId(entity.getId());
            link.setMetaType(EntityRuntime.getMeta(entity).getName());
            link.setExternalSystem(extSysyem);

            dao.saveOrUpdate(link);
        }
        link = dao.get(ExternalEntitiesMap.class, ExternalEntitiesMap.externalId(), "ad123456");
        if (link == null) {
            ScienceDegree entity = dao.getCatalogItem(ScienceDegree.class, "22");
            link = new ExternalEntitiesMap();
            link.setExternalId("ad123456");
            link.setEntityId(entity.getId());
            link.setMetaType(EntityRuntime.getMeta(entity).getName());
            link.setExternalSystem(extSysyem);

            dao.saveOrUpdate(link);
        }

        //PostType
        link = dao.get(ExternalEntitiesMap.class, ExternalEntitiesMap.externalId(), "pt12345");
        if (link == null) {
            PostType entity = dao.getCatalogItem(PostType.class, "1"); //"Основная"
            link = new ExternalEntitiesMap();
            link.setExternalId("pt12345");
            link.setEntityId(entity.getId());
            link.setMetaType(EntityRuntime.getMeta(entity).getName());
            link.setExternalSystem(extSysyem);

            dao.saveOrUpdate(link);
        }
        link = dao.get(ExternalEntitiesMap.class, ExternalEntitiesMap.externalId(), "pt123456");
        if (link == null) {
            PostType entity = dao.getCatalogItem(PostType.class, "8"); //"По совмещению"
            link = new ExternalEntitiesMap();
            link.setExternalId("pt123456");
            link.setEntityId(entity.getId());
            link.setMetaType(EntityRuntime.getMeta(entity).getName());
            link.setExternalSystem(extSysyem);

            dao.saveOrUpdate(link);
        }

        //EmployeeType
        link = dao.get(ExternalEntitiesMap.class, ExternalEntitiesMap.externalId(), "et-2");
        if (link == null) {
            EmployeeType entity = dao.getCatalogItem(EmployeeType.class, "workStaff"); //Рабочий состав

            link = new ExternalEntitiesMap();
            link.setExternalId("et-2");
            link.setEntityId(entity.getId());
            link.setMetaType(EntityRuntime.getMeta(entity).getName());
            link.setExternalSystem(extSysyem);

            dao.saveOrUpdate(link);
        }

        //OrgUnitType
        link = dao.get(ExternalEntitiesMap.class, ExternalEntitiesMap.externalId(), "out-2");
        if (link == null) {
            OrgUnitType entity = dao.getCatalogItem(OrgUnitType.class, "cathedra");

            link = new ExternalEntitiesMap();
            link.setExternalId("out-2");
            link.setEntityId(entity.getId());
            link.setMetaType(EntityRuntime.getMeta(entity).getName());
            link.setExternalSystem(extSysyem);

            dao.saveOrUpdate(link);
        }

        //OrgUnit
        link = dao.get(ExternalEntitiesMap.class, ExternalEntitiesMap.externalId(), "ou-top");
        if (link == null) {
            OrgUnit entity = TopOrgUnit.getInstance();

            link = new ExternalEntitiesMap();
            link.setExternalId("ou-top");
            link.setEntityId(entity.getId());
            link.setMetaType(EntityRuntime.getMeta(entity).getName());
            link.setExternalSystem(extSysyem);

            dao.saveOrUpdate(link);
        }


    }
}
