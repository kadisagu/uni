package ru.tandemservice.unievents.dao.externalentitymap;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DaoExternalEntities extends UniBaseDao implements IDaoExternalEntities {
    private static IDaoExternalEntities _dao;

    public static IDaoExternalEntities instanse()
    {
        if (_dao == null)
            _dao = (IDaoExternalEntities) ApplicationRuntime.getBean("unievents.daoEvents");
        return _dao;

    }

    public static final SyncDaemon DAEMON_INTEGRA_MAKE_ENTITY_TYPE = new SyncDaemon
            (
                    DaoExternalEntities.class.getName() + ".makeEntityType", 1)
    {
        @Override
        protected void main()
        {
            IDaoExternalEntities dao = instanse();
            dao.fileEntityTypeFields();
        }
    };

	/*
    @Override
    public List<ExternalEntitiesMap> getExternalByEntityId(List<Long> list)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(ExternalEntitiesMap.class, "e");
        dql.column("e");
        dql.where(DQLExpressions.in(ExternalEntitiesMap.entityId().fromAlias("e"),list));

        return getList(dql);
    }

    @Override
    public List<ExternalEntitiesMap> getExternalByEntity(List<EntityBase> list)
    {
    	
        List<Long> listLong = new ArrayList<Long>();
        for(EntityBase entBase : list){
            listLong.add(entBase.getId());
        }

        return getExternalByEntityId(listLong);
    }
    */

    public boolean onDeleteExternalEntities(Set<Long> idsLong)
    {
        boolean retVal = false;
        List<Long> ids = new ArrayList<Long>();

        if (idsLong != null && idsLong.size() > 0) {
            int i = 0;
            for (Long id : idsLong) {
                i++;
                ids.add(id);

                if (i > 999) {
                    // массовое удаление записей
                    boolean _hasDrop = _dropEntity(ids);
                    if (_hasDrop)
                        retVal = _hasDrop;

                    ids = new ArrayList<Long>();
                    i = 0;
                }
            }

            boolean _hasDrop = _dropEntity(ids);
            if (_hasDrop)
                retVal = _hasDrop;

        }

        return retVal;
    }

    private boolean _dropEntity(List<Long> ids)
    {
        boolean retVal = false;

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(ExternalEntitiesMap.class)
                .where(DQLExpressions.in(DQLExpressions.property(ExternalEntitiesMap.entityId()), ids));
        int rowCount = deleteBuilder.createStatement(getSession()).execute();

        if (rowCount > 0)
            retVal = true;

        return retVal;
    }


    public void fileEntityTypeFields()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(ExternalEntitiesMap.class, "e");
        dql.column("e");
        dql.where(DQLExpressions.isNull(ExternalEntitiesMap.metaType().fromAlias("e")));

        List<ExternalEntitiesMap> lst = getList(dql);

        for (ExternalEntitiesMap link : lst) {
            Long id = link.getEntityId();

            IEntityMeta metaType = EntityRuntime.getMeta(id);

            if (metaType == null) {
                delete(link);
            }
            else {
                link.setMetaType(metaType.getName());
                saveOrUpdate(link);
            }
        }
    }

}
