package ru.tandemservice.unievents.dao.externalentitymap;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.Set;

public interface IDaoExternalEntities extends IUniBaseDao {

    // vch выкинул странные методы (их кто-то обявил, но не использовал)
    /*
	public List<ExternalEntitiesMap> getExternalByEntity(List<EntityBase> list);
    public List<ExternalEntitiesMap> getExternalByEntityId(List<Long> list);
    */

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean onDeleteExternalEntities(Set<Long> idsLong);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void fileEntityTypeFields();
}
