package ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.List;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.LinkWrapper;

import java.io.Serializable;

public class Model {

    private IDataSettings settings;
    private ISelectModel entityMetaModel;
    private ISelectModel externalSystemModel;
    private DynamicListDataSource<LinkWrapper> dataSource;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getEntityMetaModel() {
        return entityMetaModel;
    }

    public void setEntityMetaModel(ISelectModel entityMetaModel) {
        this.entityMetaModel = entityMetaModel;
    }

    public DynamicListDataSource<LinkWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<LinkWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getExternalSystemModel() {
        return externalSystemModel;
    }

    public void setExternalSystemModel(ISelectModel externalSystemModel) {
        this.externalSystemModel = externalSystemModel;
    }

    public static class MetaWrapper extends EntityBase implements Serializable {
        private static final long serialVersionUID = 5704509812956418149L;
        private short metaCode;

        public MetaWrapper(IEntityMeta meta) {
            this.metaCode = meta.getEntityCode();
        }

        @Override
        public Long getId() {
            return (long) metaCode;
        }

        public String getTitle() {
            return getMeta().getTitle();
        }

        public IEntityMeta getMeta() {
            return EntityRuntime.getMeta(this.metaCode);
        }
    }
}
