package ru.tandemservice.unievents.component.settings.ExternalNotification.List;


import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.LinkWrapper;
import ru.tandemservice.unievents.entity.ExternalNotification;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalNotificationEventType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {

        model.setExtEntityTypeModel(new LazySimpleSelectModel<>(ExternalEntityType.class, ExternalEntityType.title().s())
                                            .setSortProperty(ExternalEntityType.title().s())
                                            .setSearchProperty(ExternalEntityType.title().s()));

        model.setExtNotificationEventTypeModel(new LazySimpleSelectModel<>(ExternalNotificationEventType.class)
                                                       .setSortProperty("title")
                                                       .setSearchProperty(ExternalNotificationEventType.title().s()));

        List<String> statusList = new ArrayList<>();
        statusList.add("Да");
        statusList.add("Нет");
        model.setProcessingStatusModel(statusList);
    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<ExternalNotification> dataSource = model.getDataSource();

        IDataSettings settings = model.getSettings();

        // extNotificationEvent это блин externalNotificationEventType
        List<ExternalNotificationEventType> extNotificationEvent = settings.get("extNotificationEvent");

        // ну а это типы объектов вроде
        List<ExternalEntityType> extEntityType = settings.get("extEntityType");


        String processingStatus = settings.get("processingStatus");
        Boolean status = null;
        if (processingStatus != null)
            if (processingStatus.equals("Да"))
                status = true;
            else if (processingStatus.equals("Нет"))
                status = false;


        // этот фильтр применяем явно не так
        // это титле от сущностей, пока не применяем
        // String titleQuery = (String) settings.get("titleQuery");

        String notificationQuery = settings.get("notificationQuery");
        Date fromDate = settings.get("fromDate");
        Date toDate = settings.get("toDate");

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(ExternalNotification.class, "en");

        FilterUtils.applySelectFilter(builder, "en", ExternalNotification.externalEntityType(), extEntityType);
        FilterUtils.applySelectFilter(builder, "en", ExternalNotification.externalNotificationEventType(), extNotificationEvent);

        FilterUtils.applySelectFilter(builder, "en", ExternalNotification.hasAccept(), status);

        // FilterUtils.applySimpleLikeFilter(builder, "en", ExternalNotification.externalEntityType().title(), titleQuery);

        FilterUtils.applySimpleLikeFilter(builder, "en", ExternalNotification.notification(), notificationQuery);
        FilterUtils.applyBetweenFilter(builder, "en", ExternalNotification.P_DATE_NOTIFICATION, fromDate, toDate);

        // применяем сотрировки
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(ExternalNotification.class, "en");
        orderRegistry.applyOrder(builder, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());

        // достроим во враппере
        List<ViewWrapper<ExternalNotification>> patchedList = ViewWrapper.getPatchedList(dataSource);
        for (ViewWrapper<ExternalNotification> wrapper : patchedList) {
            ExternalNotification notification = wrapper.getEntity();

            String entityTuTitle = "Не найдено";
            if (notification.getEntityId() != null) {
                // есть сопоставление с TU
                IEntity entity = get(notification.getEntityId());
                if (entity != null) {
                    entityTuTitle = getEntityTitle(entity);
                }
            }
            wrapper.setViewProperty("entityTuTitle", entityTuTitle);
        }

    }

    private String getEntityTitle(IEntity entity)
    {
        return LinkWrapper.getTitleWithType(entity);
    }

    @Override
    public void confirmExtNotification(Model model) {
        if (getExecutorPost() == null)
            throw new ApplicationException("Подтвердить событие может только пользователь");

        ExternalNotification externalNotification = get(ExternalNotification.class, model.getIdExtNotification());
        externalNotification.setHasAccept(true);
        externalNotification.setEmployeePost(getExecutorPost());
    }

    private EmployeePost getExecutorPost() {
        EmployeePost executorPost;
        if (UserContext.getInstance() != null) {
            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            executorPost = DataAccessServices.dao().get(EmployeePost.class, EmployeePost.id(), principalContext.getId());
            if (executorPost != null)
                return executorPost;
            else
                return null;
        }
        else
            return null;
    }
}
