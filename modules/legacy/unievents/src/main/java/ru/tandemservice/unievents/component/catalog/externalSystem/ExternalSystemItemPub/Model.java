package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemItemPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubModel;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

public class Model extends DefaultCatalogItemPubModel<ExternalSystem> {
    public Model() {

    }

    private DynamicListDataSource<ExternalSystemWS> dataSource;

    public DynamicListDataSource<ExternalSystemWS> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExternalSystemWS> dataSource) {
        this.dataSource = dataSource;
    }


}
