package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unievents.entity.EntityManagerLog;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог вызовов EntityNamager (в обоих направлениях)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntityManagerLogGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.EntityManagerLog";
    public static final String ENTITY_NAME = "entityManagerLog";
    public static final int VERSION_HASH = -143924043;
    private static IEntityMeta ENTITY_META;

    public static final String P_INTERNAL = "internal";
    public static final String P_METHOD = "method";
    public static final String P_REQUEST_DATE = "requestDate";
    public static final String P_OK = "ok";
    public static final String L_REQUEST = "request";
    public static final String L_RESPONSE = "response";
    public static final String P_ERROR_COMMENT = "errorComment";

    private boolean _internal;     // Наш сервис
    private String _method;     // Имя метода
    private Date _requestDate;     // Время вызова
    private boolean _ok;     // Нет ошибок
    private DatabaseFile _request;     // XML на входе
    private DatabaseFile _response;     // XML на выходе (или стек ошибок)
    private String _errorComment;     // Описание ошибки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Наш сервис. Свойство не может быть null.
     */
    @NotNull
    public boolean isInternal()
    {
        return _internal;
    }

    /**
     * @param internal Наш сервис. Свойство не может быть null.
     */
    public void setInternal(boolean internal)
    {
        dirty(_internal, internal);
        _internal = internal;
    }

    /**
     * @return Имя метода. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getMethod()
    {
        return _method;
    }

    /**
     * @param method Имя метода. Свойство не может быть null.
     */
    public void setMethod(String method)
    {
        dirty(_method, method);
        _method = method;
    }

    /**
     * @return Время вызова. Свойство не может быть null.
     */
    @NotNull
    public Date getRequestDate()
    {
        return _requestDate;
    }

    /**
     * @param requestDate Время вызова. Свойство не может быть null.
     */
    public void setRequestDate(Date requestDate)
    {
        dirty(_requestDate, requestDate);
        _requestDate = requestDate;
    }

    /**
     * @return Нет ошибок. Свойство не может быть null.
     */
    @NotNull
    public boolean isOk()
    {
        return _ok;
    }

    /**
     * @param ok Нет ошибок. Свойство не может быть null.
     */
    public void setOk(boolean ok)
    {
        dirty(_ok, ok);
        _ok = ok;
    }

    /**
     * @return XML на входе.
     */
    public DatabaseFile getRequest()
    {
        return _request;
    }

    /**
     * @param request XML на входе.
     */
    public void setRequest(DatabaseFile request)
    {
        dirty(_request, request);
        _request = request;
    }

    /**
     * @return XML на выходе (или стек ошибок).
     */
    public DatabaseFile getResponse()
    {
        return _response;
    }

    /**
     * @param response XML на выходе (или стек ошибок).
     */
    public void setResponse(DatabaseFile response)
    {
        dirty(_response, response);
        _response = response;
    }

    /**
     * @return Описание ошибки.
     */
    public String getErrorComment()
    {
        return _errorComment;
    }

    /**
     * @param errorComment Описание ошибки.
     */
    public void setErrorComment(String errorComment)
    {
        dirty(_errorComment, errorComment);
        _errorComment = errorComment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntityManagerLogGen)
        {
            setInternal(((EntityManagerLog)another).isInternal());
            setMethod(((EntityManagerLog)another).getMethod());
            setRequestDate(((EntityManagerLog)another).getRequestDate());
            setOk(((EntityManagerLog)another).isOk());
            setRequest(((EntityManagerLog)another).getRequest());
            setResponse(((EntityManagerLog)another).getResponse());
            setErrorComment(((EntityManagerLog)another).getErrorComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntityManagerLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntityManagerLog.class;
        }

        public T newInstance()
        {
            return (T) new EntityManagerLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "internal":
                    return obj.isInternal();
                case "method":
                    return obj.getMethod();
                case "requestDate":
                    return obj.getRequestDate();
                case "ok":
                    return obj.isOk();
                case "request":
                    return obj.getRequest();
                case "response":
                    return obj.getResponse();
                case "errorComment":
                    return obj.getErrorComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "internal":
                    obj.setInternal((Boolean) value);
                    return;
                case "method":
                    obj.setMethod((String) value);
                    return;
                case "requestDate":
                    obj.setRequestDate((Date) value);
                    return;
                case "ok":
                    obj.setOk((Boolean) value);
                    return;
                case "request":
                    obj.setRequest((DatabaseFile) value);
                    return;
                case "response":
                    obj.setResponse((DatabaseFile) value);
                    return;
                case "errorComment":
                    obj.setErrorComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "internal":
                        return true;
                case "method":
                        return true;
                case "requestDate":
                        return true;
                case "ok":
                        return true;
                case "request":
                        return true;
                case "response":
                        return true;
                case "errorComment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "internal":
                    return true;
                case "method":
                    return true;
                case "requestDate":
                    return true;
                case "ok":
                    return true;
                case "request":
                    return true;
                case "response":
                    return true;
                case "errorComment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "internal":
                    return Boolean.class;
                case "method":
                    return String.class;
                case "requestDate":
                    return Date.class;
                case "ok":
                    return Boolean.class;
                case "request":
                    return DatabaseFile.class;
                case "response":
                    return DatabaseFile.class;
                case "errorComment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntityManagerLog> _dslPath = new Path<EntityManagerLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntityManagerLog");
    }
            

    /**
     * @return Наш сервис. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#isInternal()
     */
    public static PropertyPath<Boolean> internal()
    {
        return _dslPath.internal();
    }

    /**
     * @return Имя метода. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getMethod()
     */
    public static PropertyPath<String> method()
    {
        return _dslPath.method();
    }

    /**
     * @return Время вызова. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getRequestDate()
     */
    public static PropertyPath<Date> requestDate()
    {
        return _dslPath.requestDate();
    }

    /**
     * @return Нет ошибок. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#isOk()
     */
    public static PropertyPath<Boolean> ok()
    {
        return _dslPath.ok();
    }

    /**
     * @return XML на входе.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getRequest()
     */
    public static DatabaseFile.Path<DatabaseFile> request()
    {
        return _dslPath.request();
    }

    /**
     * @return XML на выходе (или стек ошибок).
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getResponse()
     */
    public static DatabaseFile.Path<DatabaseFile> response()
    {
        return _dslPath.response();
    }

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getErrorComment()
     */
    public static PropertyPath<String> errorComment()
    {
        return _dslPath.errorComment();
    }

    public static class Path<E extends EntityManagerLog> extends EntityPath<E>
    {
        private PropertyPath<Boolean> _internal;
        private PropertyPath<String> _method;
        private PropertyPath<Date> _requestDate;
        private PropertyPath<Boolean> _ok;
        private DatabaseFile.Path<DatabaseFile> _request;
        private DatabaseFile.Path<DatabaseFile> _response;
        private PropertyPath<String> _errorComment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Наш сервис. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#isInternal()
     */
        public PropertyPath<Boolean> internal()
        {
            if(_internal == null )
                _internal = new PropertyPath<Boolean>(EntityManagerLogGen.P_INTERNAL, this);
            return _internal;
        }

    /**
     * @return Имя метода. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getMethod()
     */
        public PropertyPath<String> method()
        {
            if(_method == null )
                _method = new PropertyPath<String>(EntityManagerLogGen.P_METHOD, this);
            return _method;
        }

    /**
     * @return Время вызова. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getRequestDate()
     */
        public PropertyPath<Date> requestDate()
        {
            if(_requestDate == null )
                _requestDate = new PropertyPath<Date>(EntityManagerLogGen.P_REQUEST_DATE, this);
            return _requestDate;
        }

    /**
     * @return Нет ошибок. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#isOk()
     */
        public PropertyPath<Boolean> ok()
        {
            if(_ok == null )
                _ok = new PropertyPath<Boolean>(EntityManagerLogGen.P_OK, this);
            return _ok;
        }

    /**
     * @return XML на входе.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getRequest()
     */
        public DatabaseFile.Path<DatabaseFile> request()
        {
            if(_request == null )
                _request = new DatabaseFile.Path<DatabaseFile>(L_REQUEST, this);
            return _request;
        }

    /**
     * @return XML на выходе (или стек ошибок).
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getResponse()
     */
        public DatabaseFile.Path<DatabaseFile> response()
        {
            if(_response == null )
                _response = new DatabaseFile.Path<DatabaseFile>(L_RESPONSE, this);
            return _response;
        }

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.unievents.entity.EntityManagerLog#getErrorComment()
     */
        public PropertyPath<String> errorComment()
        {
            if(_errorComment == null )
                _errorComment = new PropertyPath<String>(EntityManagerLogGen.P_ERROR_COMMENT, this);
            return _errorComment;
        }

        public Class getEntityClass()
        {
            return EntityManagerLog.class;
        }

        public String getEntityName()
        {
            return "entityManagerLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
