package ru.tandemservice.unievents.utils.xmlPackUnpack;

import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.ILogin;

/**
 * Created with IntelliJ IDEA.
 * User: dvorlov
 * Date: 08.02.13
 * Time: 12:50
 * To change this template use File | Settings | File Templates.
 */
public class WsLoginInfo implements ILogin
{
    private String userName, password;

    public WsLoginInfo(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public WsLoginInfo() {
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }
}
