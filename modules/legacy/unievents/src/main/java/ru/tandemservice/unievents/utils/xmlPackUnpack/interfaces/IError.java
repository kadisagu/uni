package ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: dvorlov
 */
public interface IError {
    public void setErrorCode(String errorCode);

    public String getErrorCode();

    public void setErrorMessage(String errorMessage);

    public String getErrorMessage();
}
