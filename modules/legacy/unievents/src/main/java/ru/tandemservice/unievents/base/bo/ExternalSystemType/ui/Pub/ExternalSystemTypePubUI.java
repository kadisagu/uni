package ru.tandemservice.unievents.base.bo.ExternalSystemType.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.State;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.ExternalSystemTypeManager;
import ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType;

@State({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "publisherId")})
public class ExternalSystemTypePubUI extends UIPresenter {
    private Long _publisherId;
    ExternalSystemNeedEntityType model;

    @Override
    public void onComponentRefresh() {
        if (getPublisherId() != null) {
            model = ExternalSystemTypeManager.instance().dao().getExternalSystemNeedEntityType(getPublisherId());
        }
    }

    public Long getPublisherId() {
        return _publisherId;
    }

    public void setPublisherId(Long _publisherId) {
        this._publisherId = _publisherId;
    }

    public ExternalSystemNeedEntityType getModel() {
        return model;
    }

    public void setModel(ExternalSystemNeedEntityType model) {
        this.model = model;
    }
}
