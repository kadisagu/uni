package ru.tandemservice.unievents.events;

import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unievents.dao.IDaoEvents;

import java.util.*;


public class SubscribeEventsDAO extends UniBaseDao implements ISubscribeEventsDAO
{

    @Override
    public List<EntityInfo> getEntityList(DSetEvent event, List<EntityInfo> initialList)
    {

        List<EntityInfo> resultList = new ArrayList<>();
        resultList.addAll(initialList);

        // формируем мар из типов сущностей, которые нужны внешним системам
        List<IDaoEvents.ExternalSystemNeedEntity> lst = DaoFacade.getDaoEvents().getExternalSystemNeedEntityList();

        resultList = getEntityList(resultList, event.getEventType());
        resultList = getFilteredList(resultList, lst);
        return resultList;
    }


    protected List<EntityInfo> getEntityList(List<EntityInfo> list, DSetEventType eventType)
    {
        if (list.isEmpty()) { return list; }

        String classname = list.get(0).getClassname();
        Set<Long> idSet = new HashSet<Long>();

        Map<Long, EntityInfo> mapInitial = new HashMap<Long, EntityInfo>();

        for (EntityInfo info : list) {
            idSet.add(info.getId());
            mapInitial.put(info.getId(), info);
        }

        Map<String, Set<Long>> map = new HashMap<String, Set<Long>>();
        map.put(classname, idSet);
        addParentToMap(map);

        List<EntityInfo> resultList = new ArrayList<EntityInfo>();
        for (Map.Entry<String, Set<Long>> entry : map.entrySet())
            for (long id : entry.getValue()) {
                // круто, вот тут и скушали все afterDelete
                EntityInfo entryInfo = mapInitial.get(id);
                if (entryInfo == null) {
                    // возникло на основе связанного объекта
                    // следовательно это должен быть update???
                    // но это не всегда так, а если каскадное удаление
                    // то как быть?
                    // пока оставляем логику afterUpdate
                    entryInfo = new EntityInfo(id, entry.getKey(), DSetEventType.afterUpdate, null, true);
                }

                // дополнительная проверка на возможный пропуск событий
                canBePassEvents(entryInfo);

                resultList.add(entryInfo);
            }

        return resultList;
    }

    /**
     * Проставляем признак, событие можно пропустить
     *
     * @param entryInfo
     */
    protected void canBePassEvents(EntityInfo entryInfo)
    {

    }

    /**
     * Найти родительские связи и дописать в мап
     *
     * @param map
     */
    protected void addParentToMap(Map<String, Set<Long>> map)
    {
        //перекрыть и указать где надо искать
    }

    /**
     * отфильтровываем лишние сущности
     * (начальный набор сущностей может пополнится родительскими связями)
     *
     * @param list
     * @param needEntitySet
     * @return
     */
    protected List<EntityInfo> getFilteredList(List<EntityInfo> list, List<IDaoEvents.ExternalSystemNeedEntity> needEntity)
    {
        List<EntityInfo> resultList = new ArrayList<EntityInfo>();

        Set<String> needEntitySet = new HashSet<>();
        for (IDaoEvents.ExternalSystemNeedEntity ne : needEntity)
            needEntitySet.add(ne.getEntityType());

        for (EntityInfo info : list)
            if (isNeed(info, needEntitySet)) {
                // возможно, что нужно создать 2 записи
                // к каждой из записей нужно дописать ссылку на ExternalSystemNeedEntity
                List<EntityInfo> lstToAdd = getEntityInfoList(info, needEntity);
                resultList.addAll(lstToAdd);
            }

        return resultList;
    }

    private List<EntityInfo> getEntityInfoList(EntityInfo info, List<IDaoEvents.ExternalSystemNeedEntity> needEntity)
    {
        List<EntityInfo> retVal = new ArrayList<>();
        for (IDaoEvents.ExternalSystemNeedEntity ne : needEntity) {

            if (ne.getEntityType().toLowerCase().equals(info.getClassname().toLowerCase())) {
                if (retVal.isEmpty()) {
                    info.setExternalSystemNeedEntity(ne);
                    retVal.add(info);
                }
                else {
                    // нужна копия
                    EntityInfo eiCopy = new EntityInfo(info.getId(), info.getClassname(), info.getEventType(), info.getEntity(), info.isFromChild());
                    eiCopy.setCanBePass(info.isCanBePass());

                    eiCopy.setExternalSystemNeedEntity(ne);
                    retVal.add(eiCopy);
                }
            }
        }
        return retVal;
    }


    protected Set<Long> getSaved(Map<String, Set<Long>> map, String classname)
    {
        if (!map.containsKey(classname)) { map.put(classname, new HashSet<Long>()); }

        return map.get(classname);
    }

    protected boolean isNeed(EntityInfo info, Set<String> needEntitySet)
    {
        return needEntitySet.contains(info.getClassname());
    }
}
