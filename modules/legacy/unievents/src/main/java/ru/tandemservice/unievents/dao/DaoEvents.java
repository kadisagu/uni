package ru.tandemservice.unievents.dao;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType;
import ru.tandemservice.unievents.entity.InternalNotification;
import ru.tandemservice.unievents.entity.catalog.EntityForDaemonDirection;
import ru.tandemservice.unievents.entity.catalog.EventsErrorCodes;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.StatusDaemonProcessingWs;
import ru.tandemservice.unievents.entity.catalog.codes.EntityForDaemonDirectionCodes;
import ru.tandemservice.unievents.entity.catalog.codes.StatusDaemonProcessingWsCodes;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsErrorInfo;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DaoEvents
        extends UniBaseDao
        implements IDaoEvents
{

    private List<ExternalSystemNeedEntity> cacheList = null;
    private List<ExternalSystem> cacheExternalSystem = null;

    @Override
    public void clearCache()
    {
        synchronized (IDaoEvents.MUTEX) {
            cacheList = null;
            cacheExternalSystem = null;
        }
    }

    private String lastEvents = "";

    public void clearLastEvents()
    {
        lastEvents = "";
    }

    @Override
    public List<ExternalSystemNeedEntity> getExternalSystemNeedEntityList()
    {
        synchronized (IDaoEvents.MUTEX) {
            if (cacheList != null)
                return cacheList;
            else {
                cacheList = _loadCacheExternalSystemNeedEntity();
                return cacheList;
            }
        }
    }

    private List<ExternalSystemNeedEntity> _loadCacheExternalSystemNeedEntity()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(ExternalSystemNeedEntityType.class, "ent")
                .column("ent")
                .where(eq(property(ExternalSystemNeedEntityType.isUse().fromAlias("ent")), value(Boolean.TRUE)));

        List<ExternalSystemNeedEntityType> lst = dql.createStatement(getSession()).list();

        List<ExternalSystemNeedEntity> retVal = new ArrayList<ExternalSystemNeedEntity>();
        for (ExternalSystemNeedEntityType ent : lst) {
            String boCode = null;
            String boName = null;
            if (ent.getBusinessObject() != null) {
                boCode = ent.getBusinessObject().getCode();
                boName = ent.getBusinessObject().getBeanName();
            }

            ExternalSystemNeedEntity es = new ExternalSystemNeedEntity
                    (
                            ent.getExternalSystem().getId()
                            , ent.getEntityType()
                            , ent.getIntegrationType().getCode()
                            , boCode
                            , boName
                    );

            retVal.add(es);
        }
        return retVal;
    }


    private StatusDaemonProcessingWs statusProcessing = null;
    private EntityForDaemonDirection directionToExternal = null;
    private EntityForDaemonDirection directionFromExternal = null;
    private EntityForDaemonDirection directionToExternalByInternalNotification = null;


    @Override
    public boolean make4Daemon(
            String eventType
            , String entityType
            , boolean canBePass
            , String id
            , List<Object> prevSource
            , Date eventDate
            , InternalNotification internalNotification)
    {

        String _key = "";
        if (eventType != null)
            _key += eventType;

        if (entityType != null)
            _key += entityType;

        if (id != null)
            _key += id;

        if (canBePass)
            _key += "true";
        else
            _key += "false";

        if (lastEvents.equals(_key))
            return false;
        else
            lastEvents = _key;

        boolean toExternalSystem = true;
        if (directionToExternal == null)
            directionToExternal = getByCode(EntityForDaemonDirection.class, EntityForDaemonDirectionCodes.TO_EXTERNAL_SYSTEM);

        if (directionToExternalByInternalNotification == null)
            directionToExternalByInternalNotification = getByCode(EntityForDaemonDirection.class, EntityForDaemonDirectionCodes.TO_EXTERNAL_SYSTEM_INTERNAL_NOTIFICATION);

        if (directionFromExternal == null)
            directionFromExternal = getByCode(EntityForDaemonDirection.class, EntityForDaemonDirectionCodes.FROM_EXTERNAL_SYSTEM);


        if (statusProcessing == null)
            statusProcessing = getByCode(StatusDaemonProcessingWs.class, StatusDaemonProcessingWsCodes.NOT_PROCESSING);

        // по id объект можно не искать
        // все что есть 'лишнего' в сигнатуре - на перспективу (для передачи значения свойст до и после изменения)

        // ищем id внешних систем, для которых нужны упомянутые значения

        // если событие из InternalNotification, то подпискиков нужно искать
        // немного иначе
        List<Long> externalIds = getExternalSystemIdList(entityType, internalNotification);

        boolean hasAdd = false;
        if (externalIds != null && externalIds.size() > 0) {

            for (Long externalId : externalIds) {
                if (eventExists(externalId, eventType, entityType, id, canBePass))
                    continue;

                ExternalSystem es = new ExternalSystem();
                es.setId(externalId);

                EntityForDaemonWS ent = new EntityForDaemonWS();
                ent.setExternalSystem(es);

                ent.setEntityType(entityType);
                ent.setEventType(eventType);
                ent.setEntityId(id);
                ent.setEventDate(eventDate);
                ent.setStatusDaemonProcessingWs(statusProcessing);
                ent.setCanBePass(canBePass);

                if (toExternalSystem) {
                    if (internalNotification != null) {
                        ent.setDirection(directionToExternalByInternalNotification);
                        ent.setInternalNotification(internalNotification);
                    }
                    else
                        ent.setDirection(directionToExternal);
                }
                else
                    ent.setDirection(directionFromExternal);

                save(ent);
                //IUniBaseDao.instance.get().saveOrUpdate(ent);
                hasAdd = true;
            }
        }

        return hasAdd;
    }

    @Override
    public boolean make4Daemon(ExternalSystem externalSystem, String eventType, String entityType, String id) {
        if (directionFromExternal == null)
            directionFromExternal = getByCode(EntityForDaemonDirection.class, EntityForDaemonDirectionCodes.FROM_EXTERNAL_SYSTEM);
        if (statusProcessing == null)
            statusProcessing = getByCode(StatusDaemonProcessingWs.class, StatusDaemonProcessingWsCodes.NOT_PROCESSING);

        EntityForDaemonWS ent = new EntityForDaemonWS();

        ent.setExternalSystem(externalSystem);
        ent.setEntityType(entityType);
        ent.setEventType(eventType);
        ent.setEntityId(id);
        ent.setEventDate(new Date());
        ent.setStatusDaemonProcessingWs(statusProcessing);
        ent.setCanBePass(false);

        ent.setDirection(directionFromExternal);

        save(ent);

        return false;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private boolean eventExists(
            Long externalId
            , String eventType
            , String entityType
            , String id
            , boolean canBePass
    )
    {
        List<String> codeList = Arrays.asList(StatusDaemonProcessingWsCodes.NOT_PROCESSING, StatusDaemonProcessingWsCodes.ERROR);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntityForDaemonWS.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EntityForDaemonWS.externalSystem().id().fromAlias("e")), externalId))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EntityForDaemonWS.eventType().fromAlias("e")), eventType))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EntityForDaemonWS.entityType().fromAlias("e")), entityType))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EntityForDaemonWS.entityId().fromAlias("e")), id))

                .where(DQLExpressions.eqValue(DQLExpressions.property(EntityForDaemonWS.canBePass().fromAlias("e")), canBePass))

                .where(DQLExpressions.in(DQLExpressions.property(EntityForDaemonWS.statusDaemonProcessingWs().code().fromAlias("e")), codeList));

        boolean retVal = this.getCount(dql) > 0;

        return retVal;
    }

    private List<Long> getExternalSystemIdList(
            String entityType
            , InternalNotification internalNotification)
    {
        List<Long> retVal = new ArrayList<Long>();
        List<ExternalSystemNeedEntity> lst = getExternalSystemNeedEntityList();
        String findType = entityType;
        for (ExternalSystemNeedEntity ent : lst) {
            if (ent.getEntityType().toLowerCase().equals(findType.toLowerCase())) {
                retVal.add(ent.getExternalSystemId());
            }
        }
        return retVal;
    }

    private List<ExternalSystem> _getListExternalSystem()
    {
        if (cacheExternalSystem != null)
            return cacheExternalSystem;
        else {
            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(ExternalSystem.class, "ent")
                    .column("ent");

            List<ExternalSystem> lst = dql.createStatement(getSession()).list();

            cacheExternalSystem = new ArrayList<ExternalSystem>();
            for (ExternalSystem es : lst) {
                cacheExternalSystem.add(es);
            }
            return cacheExternalSystem;
        }
    }

    public ExternalSystem getExternalSystem(String userName, String userPassword)
    {
        List<ExternalSystem> lst = _getListExternalSystem();

        // ищем простым перебором
        for (ExternalSystem es : lst) {
            if (
                    es.getWsUserName() != null
                            && es.getWsUserName().equals(userName)
                            && es.getWsPassword() != null
                            && es.getWsPassword().equals(userPassword)
                    )
                return es;
        }

        return null;
    }

    public WsErrorInfo getWsErrorInfo(String errorCode, String errorMsg)
    {
        if (errorCode == null)
            return null;

        EventsErrorCodes eventsErrorCode = getByCode(EventsErrorCodes.class, errorCode);

        // неизвестный код ошибки
        if (eventsErrorCode == null)
            eventsErrorCode = getByCode(EventsErrorCodes.class, "1000");

        String _code = eventsErrorCode.getCode();
        String _title = eventsErrorCode.getTitle();

        if (errorMsg != null)
            _title += " " + errorMsg;

        WsErrorInfo retVal = new WsErrorInfo();
        retVal.setErrorCode(_code);
        retVal.setErrorMessage(_title);

        return retVal;

    }

}
