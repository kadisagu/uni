package ru.tandemservice.unievents.businessObject;

import ru.tandemservice.unievents.events.ISubscribeEventsDAO.EntityInfo;

public interface IBusinessObjectIntegration {
    boolean run(EntityInfo entity);

}
