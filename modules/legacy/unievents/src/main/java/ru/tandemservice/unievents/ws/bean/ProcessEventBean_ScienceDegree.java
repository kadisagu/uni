package ru.tandemservice.unievents.ws.bean;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceDegreeType;

import java.util.HashMap;
import java.util.Map;

public class ProcessEventBean_ScienceDegree extends CatalogProcessEventBean {
    public static final String BEAN_NAME = "eventManagerBean.tsogu_ou.ScienceDegree1.0";

    public static final String ELEMENT_ID = "ScienceDegreeId";
    public static final String ELEMENT_TITLE = "ScienceDegreeTitle";
    public static final String ELEMENT_TYPECODE = "ScienceDegreeTypeCode";
    public static final String ELEMENT_TYPEID = "ScienceDegreeTypeId";
    public static final String ELEMENT_SHORTTITLE = "ScienceDegreeShortTitle";

    @Override
    protected Map<String, ParseInfo> getMappings() {
        Map<String, ParseInfo> map = new HashMap<String, ParseInfo>();

        map.put(ELEMENT_TITLE, new ParseInfo(ELEMENT_TITLE, ScienceDegree.title().s(), String.class, null, true, false));
        map.put(ELEMENT_SHORTTITLE, new ParseInfo(ELEMENT_SHORTTITLE, ScienceDegree.shortTitle().s(), String.class, null, false, false));

        map.put(ELEMENT_TYPECODE, new ParseInfo(ELEMENT_TYPECODE, ScienceDegree.type().s(), ScienceDegreeType.class, NodeParsers.ENTITY_BY_CODE, false, true));
        map.put(ELEMENT_TYPEID, new ParseInfo(ELEMENT_TYPEID, ScienceDegree.type().s(), ScienceDegreeType.class, NodeParsers.ENTITY_BY_EXTERNALID, false, true));

        return map;
    }

    @Override
    protected String getAnswerBeanName() {
        return BEAN_NAME;
    }

    @Override
    protected Class<? extends IEntity> getEntityClass() {
        return ScienceDegree.class;
    }

    @Override
    protected String getExternalId(BodyElement entityInfo, String forRequestId) {
        return entityInfo.find(ELEMENT_ID).getValue();
    }

}
