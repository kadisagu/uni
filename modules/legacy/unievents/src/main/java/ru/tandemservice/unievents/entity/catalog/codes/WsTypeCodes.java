package ru.tandemservice.unievents.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип web службы"
 * Имя сущности : wsType
 * Файл data.xml : unievents.data.xml
 */
public interface WsTypeCodes
{
    /** Константа кода (code) элемента : Уведомления о событиях (title) */
    String EVENT_MANAGER = "1";
    /** Константа кода (code) элемента : Доступ к объектам (title) */
    String ENTITY_MANAGER = "2";

    Set<String> CODES = ImmutableSet.of(EVENT_MANAGER, ENTITY_MANAGER);
}
