package ru.tandemservice.unievents.ws;

import java.rmi.Remote;

/**
 * Для вызова через rmi (по старинке)
 *
 * @author vch
 */

public interface IEventsManagerRMI extends Remote, IEventsManager {

}
