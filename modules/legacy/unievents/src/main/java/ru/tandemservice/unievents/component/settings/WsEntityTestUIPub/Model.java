package ru.tandemservice.unievents.component.settings.WsEntityTestUIPub;

public class Model {
    String xmlString = "", serializedXML = "";

    public Model() throws Exception {

    }

    public String getXmlString() throws Exception {
        return xmlString;
    }

    public String getSerializedXML() throws Exception {
        return serializedXML;
    }

    public void setSerializedXML(String xml) {
        this.serializedXML = xml;
    }

    public void setXmlString(String xmlString) {
        this.xmlString = xmlString;
    }
}
