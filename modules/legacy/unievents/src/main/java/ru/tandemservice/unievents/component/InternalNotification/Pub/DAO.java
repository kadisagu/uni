package ru.tandemservice.unievents.component.InternalNotification.Pub;

import ru.tandemservice.unievents.entity.InternalNotification;
import ru.tandemservice.uni.dao.UniDao;

public class DAO
        extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setEntity(getNotNull(InternalNotification.class, model.getEntity().getId()));
    }
}
