package ru.tandemservice.unievents.base.bo.EntityForDaemonWS.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unievents.entity.DaemonWsLog;


public class DaemonWsLogDSHandler extends DefaultSearchDataSourceHandler {
    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public DaemonWsLogDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context) {

        Long entityId = context.get("eventId");
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(DaemonWsLog.class, "log");
        dql.column("log");
        //    dql.order(DQLExpressions.property(DaemonWsLog.logDate().fromAlias("log")), OrderDirection.desc);

        dql.where(DQLExpressions.eq(
                DQLExpressions.property(DaemonWsLog.entityForDaemonWS().id().fromAlias("log")),
                DQLExpressions.value(entityId)
        ));
        this.registry.applyOrder(dql, dsInput.getEntityOrder());
        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(DaemonWsLog.class, "log");
    }


}
