package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unievents.entity.EntityManagerTask;
import ru.tandemservice.unievents.entity.EntityManagerTaskParam;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параметры тасков для асинхронной обработки запросов EntityManager
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntityManagerTaskParamGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.EntityManagerTaskParam";
    public static final String ENTITY_NAME = "entityManagerTaskParam";
    public static final int VERSION_HASH = -1502666368;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";
    public static final String P_PARAM_NAME = "paramName";
    public static final String P_PARAM_VALUE = "paramValue";

    private EntityManagerTask _parent;     // Таска для асинхронной обработки запросов EntityManager
    private String _paramName;     // Имя параметра
    private String _paramValue;     // Значение параметра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Таска для асинхронной обработки запросов EntityManager. Свойство не может быть null.
     */
    @NotNull
    public EntityManagerTask getParent()
    {
        return _parent;
    }

    /**
     * @param parent Таска для асинхронной обработки запросов EntityManager. Свойство не может быть null.
     */
    public void setParent(EntityManagerTask parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Имя параметра. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getParamName()
    {
        return _paramName;
    }

    /**
     * @param paramName Имя параметра. Свойство не может быть null.
     */
    public void setParamName(String paramName)
    {
        dirty(_paramName, paramName);
        _paramName = paramName;
    }

    /**
     * @return Значение параметра. Свойство не может быть null.
     */
    @NotNull
    public String getParamValue()
    {
        return _paramValue;
    }

    /**
     * @param paramValue Значение параметра. Свойство не может быть null.
     */
    public void setParamValue(String paramValue)
    {
        dirty(_paramValue, paramValue);
        _paramValue = paramValue;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntityManagerTaskParamGen)
        {
            setParent(((EntityManagerTaskParam)another).getParent());
            setParamName(((EntityManagerTaskParam)another).getParamName());
            setParamValue(((EntityManagerTaskParam)another).getParamValue());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntityManagerTaskParamGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntityManagerTaskParam.class;
        }

        public T newInstance()
        {
            return (T) new EntityManagerTaskParam();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "parent":
                    return obj.getParent();
                case "paramName":
                    return obj.getParamName();
                case "paramValue":
                    return obj.getParamValue();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "parent":
                    obj.setParent((EntityManagerTask) value);
                    return;
                case "paramName":
                    obj.setParamName((String) value);
                    return;
                case "paramValue":
                    obj.setParamValue((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "parent":
                        return true;
                case "paramName":
                        return true;
                case "paramValue":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "parent":
                    return true;
                case "paramName":
                    return true;
                case "paramValue":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "parent":
                    return EntityManagerTask.class;
                case "paramName":
                    return String.class;
                case "paramValue":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntityManagerTaskParam> _dslPath = new Path<EntityManagerTaskParam>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntityManagerTaskParam");
    }
            

    /**
     * @return Таска для асинхронной обработки запросов EntityManager. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTaskParam#getParent()
     */
    public static EntityManagerTask.Path<EntityManagerTask> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Имя параметра. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTaskParam#getParamName()
     */
    public static PropertyPath<String> paramName()
    {
        return _dslPath.paramName();
    }

    /**
     * @return Значение параметра. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTaskParam#getParamValue()
     */
    public static PropertyPath<String> paramValue()
    {
        return _dslPath.paramValue();
    }

    public static class Path<E extends EntityManagerTaskParam> extends EntityPath<E>
    {
        private EntityManagerTask.Path<EntityManagerTask> _parent;
        private PropertyPath<String> _paramName;
        private PropertyPath<String> _paramValue;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Таска для асинхронной обработки запросов EntityManager. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTaskParam#getParent()
     */
        public EntityManagerTask.Path<EntityManagerTask> parent()
        {
            if(_parent == null )
                _parent = new EntityManagerTask.Path<EntityManagerTask>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Имя параметра. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTaskParam#getParamName()
     */
        public PropertyPath<String> paramName()
        {
            if(_paramName == null )
                _paramName = new PropertyPath<String>(EntityManagerTaskParamGen.P_PARAM_NAME, this);
            return _paramName;
        }

    /**
     * @return Значение параметра. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.EntityManagerTaskParam#getParamValue()
     */
        public PropertyPath<String> paramValue()
        {
            if(_paramValue == null )
                _paramValue = new PropertyPath<String>(EntityManagerTaskParamGen.P_PARAM_VALUE, this);
            return _paramValue;
        }

        public Class getEntityClass()
        {
            return EntityManagerTaskParam.class;
        }

        public String getEntityName()
        {
            return "entityManagerTaskParam";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
