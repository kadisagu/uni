package ru.tandemservice.unievents.base.bo.ExternalSystemType.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.unievents.dao.IEntityTypeDao;

import java.util.ArrayList;
import java.util.List;

public class EntityDSHandler extends SimpleTitledComboDataSourceHandler {
    public EntityDSHandler(String ownerId) {
        super(ownerId);
        this.filtered(true);

        List<IEntityTypeDao.EntityType> result = DaoFacade.getEntityTypeDao().getEntityTypeList();
        addAll(result);
    }

    // перегрузить execute необходимо, потому что в дефолтной реализации фильтрация идет по contains, а не по startsWith 
    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        DSOutput output = super.execute(input, context);
        String filter = StringUtils.trim(input.getComboFilterByValue()).toUpperCase();
        if (StringUtils.isNotBlank(filter)) {
            List<IIdentifiableWrapper> list = output.getRecordList();
            List<Object> resultList = new ArrayList<>(list.size());

            for (IIdentifiableWrapper wrapper : list) {
                if (wrapper.getTitle().toUpperCase().contains(filter)) {
                    resultList.add(wrapper);
                }
            }

            output.setRecordList(resultList);
        }
        return output;
    }

}
