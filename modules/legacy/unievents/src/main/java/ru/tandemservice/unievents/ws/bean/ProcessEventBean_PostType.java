package ru.tandemservice.unievents.ws.bean;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;

import java.util.HashMap;
import java.util.Map;

public class ProcessEventBean_PostType extends CatalogProcessEventBean {
    public static final String BEAN_NAME = "eventManagerBean.tsogu_ou.PostType1.0";

    public static final String ELEMENT_ID = "PostTypeId";
    public static final String ELEMENT_TITLE = "PostTypeTitle";
    public static final String ELEMENT_SHORTTITLE = "PostTypeShortTitle";
    public static final String ELEMENT_CODE = "PostTypeCode";


    @Override
    protected Map<String, ParseInfo> getMappings() {
        Map<String, ParseInfo> map = new HashMap<String, ParseInfo>();

        map.put(ELEMENT_TITLE, new ParseInfo(ELEMENT_TITLE, PostType.title().s(), String.class, null, false, false));
        map.put(ELEMENT_SHORTTITLE, new ParseInfo(ELEMENT_SHORTTITLE, PostType.shortTitle().s(), String.class, null, false, false));
        map.put(ELEMENT_CODE, new ParseInfo(ELEMENT_CODE, Post.userCode().s(), String.class, null, false, false));
        return map;
    }

    @Override
    protected String getAnswerBeanName() {
        return BEAN_NAME;
    }

    @Override
    protected Class<? extends IEntity> getEntityClass() {
        return PostType.class;
    }

    @Override
    protected String getExternalId(BodyElement entityInfo, String forRequestId) {
        return entityInfo.find(ELEMENT_ID).getValue();
    }
}
