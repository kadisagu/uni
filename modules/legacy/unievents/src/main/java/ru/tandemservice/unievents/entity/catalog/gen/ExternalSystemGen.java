package ru.tandemservice.unievents.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Внешная система
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExternalSystemGen extends EntityBase
 implements INaturalIdentifiable<ExternalSystemGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.catalog.ExternalSystem";
    public static final String ENTITY_NAME = "externalSystem";
    public static final int VERSION_HASH = -340154420;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_USE_THIS_EXTERNAL_SYSTEM = "useThisExternalSystem";
    public static final String P_WS_USER_NAME = "wsUserName";
    public static final String P_WS_PASSWORD = "wsPassword";
    public static final String P_BEAN_PREF = "beanPref";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _useThisExternalSystem;     // Используется
    private String _wsUserName;     // Имя пользователя
    private String _wsPassword;     // Пароль
    private String _beanPref;     // префик bean
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Используется. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseThisExternalSystem()
    {
        return _useThisExternalSystem;
    }

    /**
     * @param useThisExternalSystem Используется. Свойство не может быть null.
     */
    public void setUseThisExternalSystem(boolean useThisExternalSystem)
    {
        dirty(_useThisExternalSystem, useThisExternalSystem);
        _useThisExternalSystem = useThisExternalSystem;
    }

    /**
     * @return Имя пользователя.
     */
    @Length(max=255)
    public String getWsUserName()
    {
        return _wsUserName;
    }

    /**
     * @param wsUserName Имя пользователя.
     */
    public void setWsUserName(String wsUserName)
    {
        dirty(_wsUserName, wsUserName);
        _wsUserName = wsUserName;
    }

    /**
     * @return Пароль.
     */
    @Length(max=255)
    public String getWsPassword()
    {
        return _wsPassword;
    }

    /**
     * @param wsPassword Пароль.
     */
    public void setWsPassword(String wsPassword)
    {
        dirty(_wsPassword, wsPassword);
        _wsPassword = wsPassword;
    }

    /**
     * @return префик bean.
     */
    @Length(max=255)
    public String getBeanPref()
    {
        return _beanPref;
    }

    /**
     * @param beanPref префик bean.
     */
    public void setBeanPref(String beanPref)
    {
        dirty(_beanPref, beanPref);
        _beanPref = beanPref;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExternalSystemGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((ExternalSystem)another).getCode());
            }
            setUseThisExternalSystem(((ExternalSystem)another).isUseThisExternalSystem());
            setWsUserName(((ExternalSystem)another).getWsUserName());
            setWsPassword(((ExternalSystem)another).getWsPassword());
            setBeanPref(((ExternalSystem)another).getBeanPref());
            setTitle(((ExternalSystem)another).getTitle());
        }
    }

    public INaturalId<ExternalSystemGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<ExternalSystemGen>
    {
        private static final String PROXY_NAME = "ExternalSystemNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof ExternalSystemGen.NaturalId) ) return false;

            ExternalSystemGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExternalSystemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExternalSystem.class;
        }

        public T newInstance()
        {
            return (T) new ExternalSystem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "useThisExternalSystem":
                    return obj.isUseThisExternalSystem();
                case "wsUserName":
                    return obj.getWsUserName();
                case "wsPassword":
                    return obj.getWsPassword();
                case "beanPref":
                    return obj.getBeanPref();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "useThisExternalSystem":
                    obj.setUseThisExternalSystem((Boolean) value);
                    return;
                case "wsUserName":
                    obj.setWsUserName((String) value);
                    return;
                case "wsPassword":
                    obj.setWsPassword((String) value);
                    return;
                case "beanPref":
                    obj.setBeanPref((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "useThisExternalSystem":
                        return true;
                case "wsUserName":
                        return true;
                case "wsPassword":
                        return true;
                case "beanPref":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "useThisExternalSystem":
                    return true;
                case "wsUserName":
                    return true;
                case "wsPassword":
                    return true;
                case "beanPref":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "useThisExternalSystem":
                    return Boolean.class;
                case "wsUserName":
                    return String.class;
                case "wsPassword":
                    return String.class;
                case "beanPref":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExternalSystem> _dslPath = new Path<ExternalSystem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExternalSystem");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#isUseThisExternalSystem()
     */
    public static PropertyPath<Boolean> useThisExternalSystem()
    {
        return _dslPath.useThisExternalSystem();
    }

    /**
     * @return Имя пользователя.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getWsUserName()
     */
    public static PropertyPath<String> wsUserName()
    {
        return _dslPath.wsUserName();
    }

    /**
     * @return Пароль.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getWsPassword()
     */
    public static PropertyPath<String> wsPassword()
    {
        return _dslPath.wsPassword();
    }

    /**
     * @return префик bean.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getBeanPref()
     */
    public static PropertyPath<String> beanPref()
    {
        return _dslPath.beanPref();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends ExternalSystem> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _useThisExternalSystem;
        private PropertyPath<String> _wsUserName;
        private PropertyPath<String> _wsPassword;
        private PropertyPath<String> _beanPref;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(ExternalSystemGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Используется. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#isUseThisExternalSystem()
     */
        public PropertyPath<Boolean> useThisExternalSystem()
        {
            if(_useThisExternalSystem == null )
                _useThisExternalSystem = new PropertyPath<Boolean>(ExternalSystemGen.P_USE_THIS_EXTERNAL_SYSTEM, this);
            return _useThisExternalSystem;
        }

    /**
     * @return Имя пользователя.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getWsUserName()
     */
        public PropertyPath<String> wsUserName()
        {
            if(_wsUserName == null )
                _wsUserName = new PropertyPath<String>(ExternalSystemGen.P_WS_USER_NAME, this);
            return _wsUserName;
        }

    /**
     * @return Пароль.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getWsPassword()
     */
        public PropertyPath<String> wsPassword()
        {
            if(_wsPassword == null )
                _wsPassword = new PropertyPath<String>(ExternalSystemGen.P_WS_PASSWORD, this);
            return _wsPassword;
        }

    /**
     * @return префик bean.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getBeanPref()
     */
        public PropertyPath<String> beanPref()
        {
            if(_beanPref == null )
                _beanPref = new PropertyPath<String>(ExternalSystemGen.P_BEAN_PREF, this);
            return _beanPref;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unievents.entity.catalog.ExternalSystem#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ExternalSystemGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return ExternalSystem.class;
        }

        public String getEntityName()
        {
            return "externalSystem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
