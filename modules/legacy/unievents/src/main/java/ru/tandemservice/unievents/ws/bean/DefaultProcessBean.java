package ru.tandemservice.unievents.ws.bean;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.meta.entity.IPropertyMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsResponseInfo;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse.BodyElement;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessBean;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.text.SimpleDateFormat;
import java.util.*;


public class DefaultProcessBean extends UniBaseDao implements IProcessBean
{

    private static final int PAGE_SIZE = 1000;


    @Override
    public IResponse process(List<IParameter> paramList, String requestId, String userName, String password)
    {
        IResponse response = new WsResponseInfo();

        response.setForRequestId(requestId);
        ExternalSystem externalSystem = DaoFacade.getDaoEvents().getExternalSystem(userName, password);

        try
        {
            List<BodyElement> elementList = doProcess(paramList, externalSystem);
            response.setRequestBody(elementList, null);
        } catch (Exception ex)
        {
            throw new ApplicationException("Ошибка обработки", ex);
        }

        return response;
    }

    protected List<BodyElement> doProcess(List<IParameter> paramList, ExternalSystem externalSystem) throws Exception
    {
        List<EntityBase> entityList = getEntityList(paramList);

        List<BodyElement> resultList = new ArrayList<>();
        for (EntityBase entity : entityList)
        {
            BodyElement entityElement = getEntityElement(entity, paramList, externalSystem);
            resultList.add(entityElement);
        }

        return resultList;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    protected BodyElement getEntityElement(EntityBase entity, List<IParameter> paramList, ExternalSystem externalSystem)
    {
        Map<Long, Integer> idSet = new HashMap<>();
        int deepth = getDeepth(paramList);
        int sameentity = getSameEntity(paramList);
        boolean writeComment = getWriteComment(paramList);

        return createElementByProperty(null, entity, externalSystem, entity, deepth, idSet, sameentity, writeComment);
    }

    private boolean getWriteComment(List<IParameter> paramList)
    {
        String writeComment = getParameterValue(paramList, PARAM_WRITE_COMMENT, null);
        return !StringUtils.isEmpty(writeComment) && Boolean.parseBoolean(writeComment);

    }

    protected BodyElement createElementByProperty(String propertyName, Object propertyValue, ExternalSystem externalSystem, EntityBase entityBase, int deepth, Map<Long, Integer> idSet, int maxCountSameEntity, boolean writeComment)
    {
        BodyElement element;
        String elementName;

        if (
                propertyValue instanceof EntityBase
                        && deepth > 0
                        && getMapCount(idSet, ((IEntity) propertyValue).getId()) < maxCountSameEntity // более 10-ти раз одно и то-же не пакуем
                )
        {
            Long idEntity = ((IEntity) propertyValue).getId();
            mapPut(idSet, idEntity);

            EntityBase entity = (EntityBase) propertyValue;
            IEntityMeta meta = EntityRuntime.getMeta(entity);

            elementName = StringUtils.capitalize(meta.getName());

            element = new BodyElement(elementName);
            element.setExternalId(getExternalId(entity, externalSystem));

            Collection<String> propertyList = meta.getPropertyNames();
            for (String name : propertyList)
            {
                if (!isPropertyShow(name))
                    continue;

                Object value = entity.getProperty(name);
                if (value == null)
                    continue; // null не упаковываем

                // по имени нужно найти описание
                String comment = null;
                if (value instanceof EntityBase)
                {
                    IPropertyMeta propertyMeta = meta.getProperty(name);
                    if (propertyMeta != null && writeComment)
                        comment = propertyMeta.getTitle();
                }
                BodyElement el = createElementByProperty(name, value, externalSystem, entity, deepth - 1, idSet, maxCountSameEntity, writeComment);
                if (comment != null)
                    el.setComment(comment);

                element.getChildren().add(el);
            }

            return element;
        }


        String extComment = null;
        if (
                propertyValue instanceof EntityBase
                        && getMapCount(idSet, ((IEntity) propertyValue).getId()) >= maxCountSameEntity
                )
        {
            extComment = " (Отмена повторной упаковки аналогичной сущности)";
        }

        IPropertyMeta propertyMeta;
        if (entityBase != null)
        {
            IEntityMeta meta = EntityRuntime.getMeta(entityBase);
            propertyMeta = meta.getProperty(propertyName);
        } else
        {
            throw new IllegalStateException("IPropertyMeta must be not null");
        }

        elementName = StringUtils.capitalize(propertyName);
        String externalId = null;
        if (propertyValue instanceof EntityBase)
        {
            elementName += "Id";
            externalId = getExternalId((EntityBase) propertyValue, externalSystem);
        }

        element = new BodyElement(elementName);
        element.setExternalId(externalId);
        element.setValue(getElementValue(propertyValue));

        String comment = propertyMeta.getTitle();
        if (extComment != null && comment != null)
            comment += extComment;

        if (writeComment)
            element.setComment(comment);

        return element;
    }

    private void mapPut(Map<Long, Integer> map, Long idEntity)
    {

        Integer count = 0;
        if (map.containsKey(idEntity))
        {
            count = map.get(idEntity);
            count++;
            map.put(idEntity, count);
        } else
        {
            map.put(idEntity, count);
        }
    }

    private int getMapCount(Map<Long, Integer> map, Long idEntity)
    {
        int retVal = 0;
        if (map.containsKey(idEntity))
        {
            retVal = map.get(idEntity);
        }
        return retVal;
    }

    protected String getExternalId(EntityBase entity, ExternalSystem externalSystem)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(entity.getFastBean().getBeanClass(), "e")
                .joinEntity(
                        "e",
                        DQLJoinType.inner,
                        ExternalEntitiesMap.class,
                        "l",
                        DQLExpressions.eq(
                                DQLExpressions.property("e.id"),
                                DQLExpressions.property(ExternalEntitiesMap.entityId().fromAlias("l"))
                        ))
                .where(DQLExpressions.eq(
                        DQLExpressions.property("e.id"),
                        DQLExpressions.value(entity.getId())
                ))
                .column("l");
        List<ExternalEntitiesMap> list = getList(dql);

        // если возвратилась одна запись, то есть сопоставление
        // но это сопоставление невесть с чем
        // не факт, что имеено с нужной нам внешней системой
        if (list.size() == 1)
            return list.get(0).getExternalId();
        else if (list.size() > 1)
        {
            for (ExternalEntitiesMap link : list)
                if (StringUtils.equals(link.getExternalSystem().getBeanPref(), externalSystem.getBeanPref()))
                    return link.getExternalId();
        }
        return null;
    }

    /**
     * Возвращает значение свойства в строковом виде
     */
    protected String getElementValue(Object value)
    {
        if (value == null)
            return null;
        else if (value instanceof EntityBase)
            return ((EntityBase) value).getId().toString();
        else if (value instanceof Date)
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((Date) value);

        return value.toString();
    }

    /**
     * Показывать ли данное свойство
     */
    protected boolean isPropertyShow(String propertyName)
    {
        return !"class".equals(propertyName);
    }

    /**
     * Возвращает список сущностей
     */
    protected List<EntityBase> getEntityList(List<IParameter> paramList)
    {
        String idStr = getParameterValue(paramList, PARAM_ID, "");
        String[] arr = idStr.split(",");

        List<Long> idList = new ArrayList<>();
        for (String anArr : arr)
            if (!StringUtils.isEmpty(anArr))
                idList.add(Long.parseLong(anArr));

        if (idList.isEmpty())
        {
            String entityType = getParameterValue(paramList, PARAM_ENTITYTYPE, null);
            IEntityMeta meta = EntityRuntime.getMeta(entityType);

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(meta.getEntityClass(), "e")
                    .column("e");

            //филтр
            String filterProperty = getParameterValue(paramList, PARAM_FILTER_PROPERTY, null);
            String filterValue = getParameterValue(paramList, PARAM_FILTER_VALUE, null);
            if (!StringUtils.isEmpty(filterProperty))
            {
                PropertyEditor editor = PropertyEditorManager.findEditor(meta.getPropertyByThroughPath(filterProperty).getValueType());
                editor.setAsText(filterValue);
                Object value = editor.getValue();
                dql.where(DQLExpressions.eq(
                        DQLExpressions.property("e." + filterProperty),
                        DQLExpressions.commonValue(value)
                ));
            }

            List<EntityBase> resultList = dql.createStatement(getSession()).list();

            //paging
            String pageNumber = getParameterValue(paramList, PARAM_PAGE, null);
            if (!StringUtils.isEmpty(pageNumber))
            {
                int page = Integer.parseInt(pageNumber);

                int fromIndex = page * PAGE_SIZE;
                if (fromIndex > resultList.size())
                    fromIndex = resultList.size();
                int toIndex = fromIndex + PAGE_SIZE;
                if (toIndex > resultList.size())
                    toIndex = resultList.size();
                resultList = resultList.subList(fromIndex, toIndex);
            }

            return resultList;
        } else
            return getListByIds(idList);
    }

    /**
     * глубина детализации возвращаемой entity
     */
    protected int getDeepth(List<IParameter> paramList)
    {
        String deepth = getParameterValue(paramList, PARAM_DEEPTH, null);
        if (!StringUtils.isEmpty(deepth))
            return Integer.parseInt(deepth);

        return RECURSION_DEEPTH;
    }

    protected int getSameEntity(List<IParameter> paramList)
    {
        String sameentity = getParameterValue(paramList, PARAM_SAME_ENTITY, null);
        if (!StringUtils.isEmpty(sameentity))
            return Integer.parseInt(sameentity);
        return 1;
    }


    /**
     * Вернуть значение параметра paramName, если его нет то вернет defaultValue
     */
    protected String getParameterValue(List<IParameter> paramList, String paramName, String defaultValue)
    {
        // поиск не должен зависеть от регистра
        // vch 2013/08/29
        for (IParameter param : paramList)
            if (paramName.toLowerCase().equals(param.getParameterName().toLowerCase()))
                return param.getParameterValue();
        return defaultValue;
    }

//Не у далено на случай если понадобится восстановить функциональность, что бывало уже не раз
//    protected List<Long> getListLong(String idStr)
//    {
//        String[] arr = idStr.split(",");
//        List<Long> idList = new ArrayList<>();
//        for (String anArr : arr)
//            if (!StringUtils.isEmpty(anArr))
//                idList.add(Long.parseLong(anArr));
//
//        return idList;
//    }

    protected BodyElement addChild(BodyElement element, String attrName, Object attrValue, String comment)
    {
        BodyElement child = new BodyElement(attrName);

        String valueStr = getElementValue(attrValue);
        child.setValue(valueStr);

        if (comment != null)
            child.setComment(comment);

        element.getChildren().add(child);
        return child;
    }

    protected BodyElement addChild(BodyElement element, String attrName, Object attrValue)
    {
        return addChild(element, attrName, attrValue, null);
    }
}
