package ru.tandemservice.unievents.component.InternalNotification.Pub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.unievents.entity.InternalNotification;


@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entity.id")
})
public class Model {
    private InternalNotification entity = new InternalNotification();

    public InternalNotification getEntity() {
        return entity;
    }

    public void setEntity(InternalNotification entity) {
        this.entity = entity;
    }

}
