package ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.logging.bo.EventLog.util.Catalog;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.LinkWrapper;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setEntityMetaModel(new BaseSingleSelectModel() {
            @Override
            public ListResult<Model.MetaWrapper> findValues(String filter) {
                List<IEntityMeta> list = Catalog.getInstance().getEntityTypes();

                List<Model.MetaWrapper> resultList = new ArrayList<>();
                for (IEntityMeta meta : list) {
                    if (meta.isAbstract() || meta.isInterface())
                        continue;

                    if (!StringUtils.isEmpty(filter) && !meta.getName().contains(filter) && !meta.getTitle().contains(filter))
                        continue;

                    Model.MetaWrapper wrapper = new Model.MetaWrapper(meta);
                    resultList.add(wrapper);
                }

                return new ListResult<>(resultList);
            }

            @Override
            public Object getValue(Object obj) {
                short code = Short.parseShort("" + obj);
                if (obj != null)
                    return new Model.MetaWrapper(EntityRuntime.getMeta(code));
                return null;
            }
        });

        model.setExternalSystemModel(new UniQueryFullCheckSelectModel("title") {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(ExternalSystem.ENTITY_CLASS, alias)
                        .addOrder(alias, ExternalSystem.title());
                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, ExternalSystem.title(), filter));

                return builder;
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Model.MetaWrapper meta = model.getSettings().get("entityName");
        ExternalSystem externalSystem = null;
        if (model.getSettings().get("externalSystem") != null) {
            externalSystem = model.getSettings().get("externalSystem");
        }
        String filterTitle = model.getSettings().get("title");
        String externalId = model.getSettings().get("externalId");

        // 2 варианта (тип указан, тип не указан)
        DynamicListDataSource<LinkWrapper> dataSource = model.getDataSource();
        DQLSelectBuilder dql = getDql(meta, externalSystem, externalId);
        final Number count = dql.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
        dataSource.setTotalSize((null == count ? 0 : count.longValue()));

        // начальная, кол-во записей (читаем из забора)
        long startRow = dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow();
        long countRow = dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow();

        List<LinkWrapper> list = getEntityList(meta, externalSystem, filterTitle, externalId, (int) startRow, (int) countRow);
        dataSource.createPage(list);

    }

    private DQLSelectBuilder getDql(Model.MetaWrapper wrapper, ExternalSystem externalSystem, String externalId)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ExternalEntitiesMap.class, "l");
        dql.column("l");

        if (wrapper != null) {
            dql.joinEntity(
                    "l",
                    DQLJoinType.inner,
                    wrapper.getMeta().getEntityClass(),
                    "e",
                    DQLExpressions.eq(
                            DQLExpressions.property(ExternalEntitiesMap.entityId().fromAlias("l")),
                            DQLExpressions.property("e.id")
                    ));

            dql.column("e");
        }
        else {
            // не указали тип объекта
            dql.column(DQLExpressions.property(ExternalEntitiesMap.entityId().fromAlias("l")));
            //Catalog.getInstance().getEntityTypeById(id)
        }

        if (externalSystem != null)
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(ExternalEntitiesMap.externalSystem().beanPref().fromAlias("l")),
                    DQLExpressions.value(externalSystem.getBeanPref())
            ));

        if (externalId != null)
            dql.where(DQLExpressions.likeUpper(
                    DQLExpressions.property(ExternalEntitiesMap.externalId().fromAlias("l")),
                    DQLExpressions.value(externalId.toUpperCase())
            ));


        return dql;

    }

    @Override
    public List<LinkWrapper> getEntityList(
            Model.MetaWrapper wrapper
            , ExternalSystem externalSystem
            , String filter
            , String externalId
            , int startRow
            , int countRow)
    {
        List<LinkWrapper> resultList = new ArrayList<>();


        DQLSelectBuilder dql = getDql(wrapper, externalSystem, externalId);


        List<Object[]> list;

        if (startRow >= 0 && countRow >= 0)
            list = dql.createStatement(getSession()).setFirstResult(startRow).setMaxResults(countRow).list();
        else
            list = getList(dql);


        Map<Long, IEntity> mapEntity = new HashMap<>();
        Map<Class, List<Long>> mapTypeIds = new HashMap<>();

        if (wrapper == null) {
            // раскладываем id строго по типам
            for (Object[] arr : list) {
                Long id = (Long) arr[1];
                IEntityMeta metaType = EntityRuntime.getMeta(id);
                Class clazz = metaType.getEntityClass();

                List<Long> lst;
                if (mapTypeIds.containsKey(clazz))
                    lst = mapTypeIds.get(clazz);
                else {
                    lst = new ArrayList<>();
                    mapTypeIds.put(clazz, lst);
                }

                if (!lst.contains(id))
                    lst.add(id);
            }

            // теперь читаем через in
            // Саша - ну что тут за херня была вписана,а?

            for (Map.Entry<Class, List<Long>> clazzEntry : mapTypeIds.entrySet()) {

                List lstEntity = getList(clazzEntry.getKey(), clazzEntry.getValue());
                for (Object ent : lstEntity) {
                    mapEntity.put(((IEntity) ent).getId(), (IEntity) ent);
                }
            }
        }

        for (Object[] arr : list) {
            ExternalEntitiesMap link = (ExternalEntitiesMap) arr[0];
            IEntity entity = null;

            if (arr[1] != null && arr[1] instanceof IEntity)
                entity = (IEntity) arr[1];

            if (entity == null) {
                //ищем по id
                Long id = (Long) arr[1];
                if (id != null && mapEntity.containsKey(id))
                    entity = mapEntity.get(id);
                else
                    entity = null;
            }

            LinkWrapper obj = new LinkWrapper(link, entity);
            if (!StringUtils.isEmpty(filter) && !obj.getTitle().contains(filter))
                continue;
            resultList.add(obj);
        }

        return resultList;
    }

    @Override
    public void fillMeta(Model.MetaWrapper wrapper)
    {
        List<LinkWrapper> list = getEntityList(wrapper, null, null, null, -1, -1);

        if (wrapper.getMeta() == null)
            return;

        IEntityMeta meta = wrapper.getMeta();
        for (LinkWrapper obj : list) {
            if (!StringUtils.isEmpty(obj.getLink().getMetaType()))
                continue;

            obj.getLink().setMetaType(meta.getName());
            IUniBaseDao.instance.get().saveOrUpdate(obj.getLink());
        }
        //this.flushClearAndRefresh();
    }
}
