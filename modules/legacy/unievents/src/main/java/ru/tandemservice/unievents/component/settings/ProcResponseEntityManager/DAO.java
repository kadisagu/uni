package ru.tandemservice.unievents.component.settings.ProcResponseEntityManager;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.ILogin;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessEventManagerBean;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    public void onTest(Model model) throws Exception {

        IDataSettings settings = model.getSettings();

        String requestXML = (String) settings.get("xmlRequest");
        String beanName = (String) settings.get("beanName");
        String xmlAnswerParam = (String) settings.get("xmlAnswer");

        XmlDocWS xmlRequest = null;

        try {
            xmlRequest = createXmlDocWS(requestXML);
        }
        catch (Exception ex) {
            String msg = "";
            if (ex.getMessage() != null)
                throw new ApplicationException("Ошибка разбора xmlRequest " + msg);
        }
        if (xmlRequest == null)
            throw new ApplicationException("xmlRequest == null");


        // ответ
        XmlDocWS xmlAnswer = null;
        try {
            xmlAnswer = createXmlDocWS(xmlAnswerParam);
        }
        catch (Exception ex) {
            String msg = "";
            if (ex.getMessage() != null)
                throw new ApplicationException("Ошибка разбора xmlAnswer " + msg);
        }

        if (xmlAnswer == null)
            throw new ApplicationException("xmlAnswer == null");


        String userName = null;
        String password = null;
        List<IRequest> requests = null;

        if (xmlRequest != null) {
            ILogin iLogin = xmlRequest.getLogin();
            userName = iLogin.getUserName();
            password = iLogin.getPassword();
            requests = xmlRequest.getRequests();
        }
        try {
            IProcessEventManagerBean bean = (IProcessEventManagerBean) ApplicationRuntime.getBean(beanName);
            List<IResponse> iResponses = xmlAnswer.getResponses();
            for (IResponse response : iResponses)
                bean.processAnswer(xmlAnswer, userName, password, requests, response);

        }
        catch (NoSuchBeanDefinitionException e) {
            throw new ApplicationException("Bean с именем " + beanName + " не найден");
        }
        catch (ClassCastException e) {
            throw new ApplicationException("Bean с именем " + beanName + " не является наследником IProcessEventManagerBean");
        }
        /*
		catch (Exception e) 
		{
			String msg=null;
			msg = e.getMessage();
			if (msg==null)
				msg = e.toString();
			
			throw new ApplicationException(msg);
		}
		*/


    }

    protected XmlDocWS createXmlDocWS(String xml) {
        XmlDocWS result;
        try {
            result = xml != null ? new XmlDocWS(xml) : null;
        }
        catch (Exception e) {
            result = null;
        }
        return result;
    }
}
