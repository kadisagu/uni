package ru.tandemservice.unievents.events;

import org.apache.log4j.Logger;
import org.hibernate.engine.SessionImplementor;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.unievents.businessObject.IBusinessObjectIntegration;
import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.unievents.dao.IDaoEvents;
import ru.tandemservice.unievents.dao.IEntityTypeDao.EntityType;
import ru.tandemservice.unievents.entity.InternalNotification;
import ru.tandemservice.unievents.entity.catalog.codes.IntegrationTypeCodes;
import ru.tandemservice.unievents.events.ISubscribeEventsDAO.EntityInfo;
import ru.tandemservice.uni.dao.IUniBaseDao;

import javax.transaction.Synchronization;
import java.util.*;


/**
 * Изменения объектов внешней системе
 *
 * @author vch
 */
public class SubscribeEvents implements IDSetEventListener
{

    private static String lockString = "LockEvents";
    private static final ThreadLocal<Sync> syncs = new ThreadLocal<Sync>();

    private static final Logger log = Logger.getLogger(SubscribeEvents.class);

    /**
     * в спринге прописан как стартовый метод
     */
    public void init()
    {
        DSetEventManager.getInstance().registerListener(
            DSetEventType.afterInsert, EntityBase.class, this);
        DSetEventManager.getInstance().registerListener(
            DSetEventType.afterUpdate, EntityBase.class, this);
        DSetEventManager.getInstance().registerListener(
            DSetEventType.afterDelete, EntityBase.class, this);
    }

    /**
     * формируем начальный список объектов (на основе события)
     *
     * @param event
     * @return
     */
    private List<EntityInfo> getInitialList(DSetEvent event)
    {
        IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
        List<EntityInfo> retVal = new ArrayList<>();

        if (event.getMultitude().isSingular()) {
            IEntity entity = event.getMultitude().getSingularEntity();
            EntityInfo ei = new EntityInfo(entity.getId(), entityMeta.getName(), event.getEventType(), entity, false);
            retVal.add(ei);
        }
        else {
            List<Long> ids = new DQLSelectBuilder()
                .fromDataSource(event.getMultitude().getSource(), "s")
                .column("s.id")
                .createStatement(event.getContext())
                .<Long>list();
            for (Long id : ids) {
                EntityInfo ei = new EntityInfo(id, entityMeta.getName(), event.getEventType(), null, false);
                retVal.add(ei);
            }
        }
        return retVal;
    }


    @Override
    public void onEvent(DSetEvent event)
    {
        // if (Debug.isEnabled()) return;

        synchronized (lockString) {

            IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
            String className = entityMeta.getName();

            if (className.toLowerCase().equals("ExternalSystemNeedEntityType".toLowerCase())
                || className.toLowerCase().equals("ExternalSystem".toLowerCase())
                || className.toLowerCase().equals("ExternalSystemWS".toLowerCase())) {
                DaoFacade.getDaoEvents().clearCache();
                DaoFacade.getWebClientManager().clearCache();
            }

            if (className.toLowerCase().equals("EntityForDaemonWS".toLowerCase())
                && (event.getEventType() == DSetEventType.afterUpdate || event.getEventType() == DSetEventType.afterDelete)) {
                // забыть последнее
                DaoFacade.getDaoEvents().clearLastEvents();
            }

            List<ISubscribeEventsDAO.EntityInfo> entityList = new ArrayList<>();
            List<EntityInfo> initialList = getInitialList(event);

            if (className.toLowerCase().equals(InternalNotification.ENTITY_NAME.toLowerCase())) {
                // рассматриваем только случай - после вставки
                // ибо это уведомления, вопросы редактирования/удаления уведомлений
                // не должны попадать в очередь
                if (event.getEventType() == DSetEventType.afterInsert) {
                    // в листе инициализации меняем тип объекта (внутренние уведомления OX имеют свои собственные типы
                    replaceEntityTypeForInternalEvents(initialList);

                    entityList = DaoFacade.getSubscribeEventsDAO().getEntityList(event, initialList);
                }
                else {
                    // entityList - пустой, события пропускаем
                }
            }
            else {
                entityList = DaoFacade.getSubscribeEventsDAO().getEntityList(event, initialList);
            }

            if (!entityList.isEmpty()) {
                List<Object> prevSource = null;//new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "s").createStatement(event.getContext()).list();

                // ошибка формирования sql выражения, баг это тандема или не баг
                // хрен его знает, при оказии нужно заявить, но сейчас особой необходимости нет
                // vch 2014/02/24
                // trayToFillMyltityde(entityList, event);

                // и помним, исполнение начнется только после подтверждения транзакции
                // как итог, список объектов, передаваемый в entitySet
                // может быть больше entityList на момент начала исполнения
                // beforeCompletion

                Sync sync = getSyncSafe(event.getContext());
                for (ISubscribeEventsDAO.EntityInfo info : entityList) {
                    sync.entitySet.add(info);
                    sync.prevSource.put(info.getId(), prevSource);
                }

            }

        }
    }

    private void replaceEntityTypeForInternalEvents(List<EntityInfo> initialList)
    {

        for (EntityInfo ei : initialList) {
            InternalNotification in = null;

            if (ei.getEntity() != null && ei.getEntity() instanceof InternalNotification) {
                in = (InternalNotification) ei.getEntity();
            }
            else {
                // получить по id
                in = IUniBaseDao.instance.get().get(InternalNotification.class, ei.getId());
            }

            if (in != null) {
                // меняем тип
                ei.setClassname(in.getEntityType().getEntityCode());
            }
        }
    }

    /**
     * Пытаемся заполнить значения
     * объектов из временных таблиц
     * Актуально только для multitude
     *
     * @param entityList
     * @param event      возникает ошибка исполнения
     *                   <p>
     *                   select def_0.id as s_2, def_0.discriminator as s_3, def_0.null as s_4, def_0.null as s_5, def_0.null as s_6, def_0.null as s_7, def_0.null as s_8 from HT_39_tmp def_0
     *                   <p>
     *                   такое действительно исполнить нельзя
     *                   поэтому работаем только через s.id
     */
    protected void tryToFillMyltityde(List<EntityInfo> entityList, DSetEvent event) {
        if (!event.getMultitude().isSingular()) {
            List<Object> entities = new ArrayList<>();
            try {
                entities = new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "s").column("s").createStatement(event.getContext()).list();
            }
            catch (Exception ex) {
                String msg = "Ошибка в trayToFillMyltityde";
                if (ex.getMessage() != null) { msg += ex.getMessage(); }
                System.out.println(msg);
                log.error(ex.getMessage(), ex);
            }

            if (entities != null && !entities.isEmpty()) {
                for (Object entityObj : entities) {
                    if (entityObj instanceof IEntity) {
                        IEntity entity = (IEntity) entityObj;
                        for (EntityInfo ei : entityList) {
                            if (ei.getId().equals(entity.getId())) { ei.setEntity(entity); }
                        }
                    }
                }
            }
        }
    }

    private Sync getSyncSafe(DQLExecutionContext context)
    {
        Sync sync = syncs.get();
        if (sync == null) {
            syncs.set(sync = new Sync(context.getSessionImplementor()));
            context.getTransaction().registerSynchronization(sync);
        }
        return sync;
    }


    private static class Sync implements Synchronization
    {
        private SessionImplementor sessionImplementor;

        /**
         * список id сущностей, которые изменились
         */
        private Set<EntityInfo> entitySet = new HashSet<EntityInfo>();

        //мап значений , которые хранились в бд до совершения операции:
        private Map<Long, List<Object>> prevSource = new HashMap<Long, List<Object>>();

        private Sync(SessionImplementor sessionImplementor)
        {
            this.sessionImplementor = sessionImplementor;
        }

        @Override
        public void beforeCompletion()
        {
            boolean hasUpdate = false;

            // события по одним и тем-же объектам нужно гасить
            // если идет цепочка insert/update по одному id
            // оставить только первое
            // для delete естественно все оставляем
            Map<String, EntityInfo> mapExecute = new HashMap<String, EntityInfo>();

            for (EntityInfo info : entitySet) {
                String executeKey = info.getExecuteKey();
                if (mapExecute.containsKey(executeKey)) {
                    // обработано ранее
                    // смотрим на тип события
                    // если тип события первого insert/update
                    // и сейчас у нас не delete, смело поглащаем
                    EntityInfo first = mapExecute.get(executeKey);
                    if (
                        (first.getEventType().equals(DSetEventType.afterInsert) || first.getEventType().equals(DSetEventType.afterUpdate))

                            // при делете однозначно выводим
                            && !info.getEventType().equals(DSetEventType.afterDelete)
                            // одинаковый пасс события
                            && first.isCanBePass() == info.isCanBePass()
                        ) {
                        // пропуск события
                    }
                    else {
                        // такое нужно исполнить (при этом по сущности уже было событие)
                        //hasUpdate = dao.make4Daemon(info.getEventType().name(), info.getClassname(), info.isCanBePass(), info.getId(), prevSource.get(info.getId()));
                        hasUpdate = makeAction(info);

                    }
                }
                else {
                    // событие по сущности возникло первый раз
                    //hasUpdate =     dao.make4Daemon(info.getEventType().name(), info.getClassname(), info.isCanBePass(), info.getId(), prevSource.get(info.getId()));
                    hasUpdate = makeAction(info);

                }
                // перезапись (всегда)
                mapExecute.put(executeKey, info);
            }

            if (hasUpdate) { sessionImplementor.flush(); }
        }

        private boolean makeAction(EntityInfo info)
        {
            boolean hasUpdate = false;

            if (info.getExternalSystemNeedEntity() == null
                || info.getExternalSystemNeedEntity().getIntegrationTypeCode().equals(IntegrationTypeCodes.EVENTS)) {
                IDaoEvents dao = DaoFacade.getDaoEvents();

                String eventType = info.getEventType().name();
                String entityType = info.getClassname();
                boolean canBePass = info.isCanBePass();
                String id = Long.toString(info.getId());
                List<Object> ps = prevSource.get(info.getId());
                Date eventDate = new Date();
                InternalNotification in = null;

                EntityType et = DaoFacade.getEntityTypeDao().getEntityType(info.getClassname());

                if (et != null && !et.isTuEntity()) {
                    // меняем типы события, типы оюъекта
                    // стандартно в очередь

                    if (info.getEntity() != null && info.getEntity() instanceof InternalNotification) {
                        in = (InternalNotification) info.getEntity();
                    }
                    else
                    // получить по id
                    { in = IUniBaseDao.instance.get().get(InternalNotification.class, info.getId()); }

                    eventType = in.getEventType().getCode();
                    entityType = in.getEntityType().getEntityCode();
                    id = in.getEntityId();
                    eventDate = in.getEventDate();

                }
                hasUpdate = dao.make4Daemon
                    (
                        eventType
                        , entityType
                        , canBePass
                        , id
                        , ps
                        , eventDate
                        , in
                    );

            }

            if (info.getExternalSystemNeedEntity() != null
                && info.getExternalSystemNeedEntity().getIntegrationTypeCode().equals(IntegrationTypeCodes.BUSINES_OBJECT))
            // интеграция через бизнес объекты
            { hasUpdate = makeBeanAction(info); }

            return hasUpdate;
        }

        private boolean makeBeanAction(EntityInfo info)
        {
            String beanName = info.getExternalSystemNeedEntity().getBusinessObjectName();

            if (beanName == null || beanName.isEmpty()) { return false; }
            else {
                IBusinessObjectIntegration bean = (IBusinessObjectIntegration) ApplicationRuntime.getBean(beanName);
                return bean.run(info);
            }
        }

        @Override
        public void afterCompletion(int status)
        {
            syncs.remove();
        }
    }

}
