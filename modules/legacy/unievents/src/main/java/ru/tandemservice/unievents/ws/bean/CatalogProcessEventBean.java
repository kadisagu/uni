package ru.tandemservice.unievents.ws.bean;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessEventManagerBean;

import java.util.List;
import java.util.Map;

public abstract class CatalogProcessEventBean extends BaseProcessEventBean {

    @Override
    protected IEntity findEntity(IProcessEventManagerBean.BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId)
    {
        IEntityMeta meta = EntityRuntime.getMeta(getEntityClass());
        for (Map.Entry<String, ParseInfo> entry : getMappings().entrySet()) {
            ParseInfo pInfo = entry.getValue();
            if (!pInfo.isKey())
                continue;

            IProcessEventManagerBean.BodyElement bodyElement = entityInfo.find(pInfo.getNodeName());
            if (!bodyElement.isExists())
                continue; // если в xml не указано - пропускаем

            String value = bodyElement.getValue();

            MQBuilder builder = new MQBuilder(meta.getClassName(), "e")
                    .add(MQExpression.eq("e", entry.getValue().getPropertyName(), value));
            List<IEntity> list = getList(builder);

            if (list.size() == 1)
                return list.get(0);
        }

        return null;
    }


    @Override
    protected IProcessEventManagerBean.SaveData fillEntity(IEntity entityTU, IProcessEventManagerBean.BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId)
    {
        IProcessEventManagerBean.SaveData saveData = new IProcessEventManagerBean.SaveData();
        ICatalogItem item = (ICatalogItem) entityTU;

        if (item == null) {
            try {
                item = (ICatalogItem) getEntityClass().newInstance();
            }
            catch (InstantiationException e) {
                throw new ApplicationException("", e);
            }
            catch (IllegalAccessException e) {
                throw new ApplicationException("", e);
            }

            item.setCode(getNewCatalogItemCode(getEntityClass()));
        }

        for (Map.Entry<String, ParseInfo> entry : getMappings().entrySet()) {
            ParseInfo pInfo = entry.getValue();
            IProcessEventManagerBean.BodyElement bodyElement = entityInfo.find(pInfo.getNodeName());

            if (!bodyElement.isExists())
                continue; // если в xml не указано - пропускаем

            Object value = pInfo.getValue(bodyElement.getValue(), externalSystem.getBeanPref());
            if (pInfo.isNotNullProperty() && value == null)
                throw new ApplicationException("Для атрибута " + pInfo.getNodeName() + " свойство объекта " + pInfo.getPropertyName() + " не найдено значение");

            item.setProperty(pInfo.getPropertyName(), value);
        }

        saveData.setMainEntity(item);
        saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(item, false));

        return saveData;
    }

    public static String getNewCatalogItemCode(Class catalogItemClass) {
        int count;
        for (
                count = DataAccessServices.dao().getCount(catalogItemClass);
                CatalogManager.instance().dao().getCatalogItem(catalogItemClass, Integer.toString(count)) != null;
                count++
                )
            ;
        return String.valueOf(count);
    }

    ////////////////////////////////////////////////////////////////////////////////
    protected abstract Map<String, ParseInfo> getMappings();

    //////////////////////////////////////////////////////////////////////////////

    public static class ParseInfo {
        private String nodeName;
        private String propertyName;
        private NodeParsers.INodeParser parser;
        private boolean key;
        private Class returnClass;
        private boolean notNullProperty;


        /**
         * @param nodeName        - имя ноды в XML
         * @param propertyName    - имя свойства в объекте
         * @param returnClass     - тип свойства в объекте
         * @param parserName      - имя парсера
         * @param key             - является ли поле ключем (нужно ли искать среди уже существующих)
         * @param notNullProperty - обязательное значение поля
         */
        public ParseInfo(
                String nodeName
                , String propertyName
                , Class returnClass
                , String parserName
                , boolean key
                , boolean notNullProperty)
        {
            this.nodeName = nodeName;
            this.propertyName = propertyName;

            if (parserName != null)
                this.parser = NodeParsers.getParser(parserName);

            this.returnClass = returnClass;
            this.key = key;
            this.notNullProperty = notNullProperty;
        }

        public Object getValue(String value, String beanPrefics) {
            if (parser != null)
                return parser.parse(value, beanPrefics, returnClass);

            return value;
        }

        public String getPropertyName() {
            return propertyName;
        }

        /**
         * является ли поле ключем (нужно ли искать среди уже существующих)
         *
         * @return
         */
        public boolean isKey() {
            return key;
        }

        public String getNodeName() {
            return nodeName;
        }

        /**
         * Обязательность поля
         *
         * @return
         */
        public boolean isNotNullProperty() {
            return notNullProperty;
        }
    }

}
