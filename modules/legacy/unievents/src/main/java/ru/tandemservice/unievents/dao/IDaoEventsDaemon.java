package ru.tandemservice.unievents.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

import java.util.List;
import java.util.Map;

public interface IDaoEventsDaemon {
    public static final SpringBeanCache<IDaoEventsDaemon> instance = new SpringBeanCache<IDaoEventsDaemon>(IDaoEventsDaemon.class.getName());


    /**
     * Обработка события, наступившего в TU
     *
     * @param events
     *
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public boolean doEvents(EntityForDaemonWS events);

    /**
     * Готовим список из очереди сообщений
     *
     * @param forExternalSystem true - для внешней системы
     *
     * @return
     */
    public Map<ExternalSystem, List<EntityForDaemonWS>> getMapEntityForDaemonWS(boolean forExternalSystem);

    /**
     * Обработка события, присланного из внешней системы
     *
     * @param entity
     *
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public boolean doEventsFromExternalSystem(EntityForDaemonWS entity);
}
