package ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces;

import java.util.ArrayList;
import java.util.List;

/**
 * Ответы web службы
 *
 * @author vch
 */
public interface IResponse {
    public String getForRequestId();

    public void setForRequestId(String forRequestId);

    public String getRequestBody();

    public List<BodyElement> getBodyList();

    public void setRequestBody(List<BodyElement> bodyList, String requestBody);

    public List<IParameter> getParameters();

    public void setParameters(List<IParameter> params);

    public IParameter getParameter(String paramName);

    public boolean hasParameter(String paramName);

    public static class BodyElement {
        private String name;
        private String value;
        private String comment = null;

        private List<BodyElement> children = new ArrayList<BodyElement>();
        private String externalId;

        public BodyElement(String name) {
            this.name = name;
        }

        public BodyElement getChild(String name) {
            for (BodyElement child : this.getChildren())
                if (child.getName().equals(name))
                    return child;

            return null;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }

        public List<BodyElement> getChildren()
        {
            return children;
        }

        public void setChildren(List<BodyElement> children)
        {
            this.children = children;
        }

        public String getExternalId()
        {
            return externalId;
        }

        public void setExternalId(String externalId)
        {
            this.externalId = externalId;
        }

        public void setComment(String comment)
        {
            this.comment = comment;
        }

        public String getComment()
        {
            return comment;
        }

    }
}
