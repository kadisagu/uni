package ru.tandemservice.unievents.ws;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.description.OperationDesc;
import org.apache.axis.description.ParameterDesc;
import ru.tandemservice.unievents.entity.ExternalSystemWS;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import java.net.URL;


public class EntitiManagerProxy implements IEntityManager {

    /**
     * конечная точка
     */
    private URL endpoint = null;

    private ExternalSystemWS externalSystemWS = null;

    public EntitiManagerProxy(ExternalSystemWS externalSystemWS)
            throws Exception
    {
        this.externalSystemWS = externalSystemWS;
        _makeEndPoint();

    }

    private void _makeEndPoint() throws Exception {
        String urlStr = externalSystemWS.getWsUrl();
        urlStr = urlStr.replace("?wsdl", "");
        endpoint = new URL(urlStr);
    }


    private String runCall(String methodsName, String request, String parameterName) throws Exception
    {
        String soapActionURI = _makeSOAPActionURI();
        QName qName = _makeOperationName(methodsName);

        OperationDesc operDsk = getOperationDesk(methodsName, parameterName);

        Call _call = createCall();

        _call.setOperation(operDsk);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI(soapActionURI + methodsName);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(qName);


        try {
            Object _resp = _call.invoke(new java.lang.Object[]{request});
            if (_resp instanceof java.rmi.RemoteException) {
                throw (java.rmi.RemoteException) _resp;
            }
            else {
                try {
                    return (String) _resp;
                }
                catch (java.lang.Exception _exception) {
                    return (String) org.apache.axis.utils.JavaUtils
                            .convert(_resp, java.lang.String.class);
                }
            }
        }
        catch (org.apache.axis.AxisFault axisFaultException) {
            throw axisFaultException;
        }

    }

    @Override
    public String sendRequest(String request) throws Exception
    {
        return runCall("sendRequest", request, "request");
    }

    private QName _makeOperationName(String operationName) {
        if (externalSystemWS.getWsTargetNamespace() != null)
            return new QName(externalSystemWS.getWsTargetNamespace(),
                             operationName);
        else
            return new QName(operationName);
    }

    private String _makeSOAPActionURI() {
        // "http://std.tsogu.ru#EntityManager:sendRequest"
        String retVal = "";
        if (externalSystemWS.getWsTargetNamespace() != null)
            retVal += externalSystemWS.getWsTargetNamespace();

        if (externalSystemWS.getWsServiceName() != null) {
            if (!retVal.equals(""))
                retVal += "#";
            retVal += externalSystemWS.getWsServiceName();
        }

        if (!retVal.equals(""))
            retVal += ":";

        return retVal;
    }

    private Call createCall() throws ServiceException {
        Service service = new Service();
        Call call = (Call) service.createCall();

        call.setTargetEndpointAddress(endpoint);

        if (externalSystemWS.getWsUsername() != null)
            call.setUsername(externalSystemWS.getWsUsername());

        if (externalSystemWS.getWsPassword() != null)
            call.setPassword(externalSystemWS.getWsPassword());

        if (externalSystemWS.getWsServicePort() != null)
            call.setPortName(new QName(externalSystemWS.getWsServicePort()));

        return call;
    }

    @Override
    public String sendAnswer(String answer) throws Exception {
        return runCall("sendAnswer", answer, "answer");
    }


    private OperationDesc getOperationDesk(String operationName, String parameterName)
    {
        QName qnameParam = null;
        if (externalSystemWS.getWsTargetNamespace() != null)
            qnameParam = new QName(externalSystemWS.getWsTargetNamespace(), parameterName);
        else
            qnameParam = new QName(parameterName);

        OperationDesc oper = new org.apache.axis.description.OperationDesc();
        ParameterDesc param;
        param = new org.apache.axis.description.ParameterDesc(
                qnameParam
                , ParameterDesc.IN
                , new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string")
                , java.lang.String.class, false, false);

        oper.setName(operationName);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);

        //oper.setReturnQName(new javax.xml.namespace.QName("http://std.tsogu.ru", "return"));
        //oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        //oper.setUse(org.apache.axis.constants.Use.LITERAL);

        return oper;
    }
}
