package ru.tandemservice.unievents.component.settings.TestCallProcessEvent;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());
    }

    public void onClickApply(IBusinessComponent component) {
        component.saveSettings();
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).onTest(model);
    }

}
