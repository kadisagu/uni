package ru.tandemservice.unievents.ws.fake;

import ru.tandemservice.unievents.ws.IEntityManager;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Date;

/**
 * Для тестирования под нагрузкой
 *
 * @author vch
 */
@WebService(serviceName = "EntityManagerStressTest")
public class EntityManagerStressTest
        implements IEntityManager
{

    @Override
    @WebMethod
    public String sendRequest(String request) throws Exception
    {

        return "sendRequest Abracadabra Test Test Фигня всякая " + (new Date());
    }

    @Override
    @WebMethod
    public String sendAnswer(String answer) throws Exception {

        return "sendAnswer Abracadabra Test Test Фигня всякая " + (new Date());
    }

}
