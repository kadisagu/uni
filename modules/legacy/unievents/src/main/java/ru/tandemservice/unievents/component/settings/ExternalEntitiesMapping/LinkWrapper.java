package ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;

public class LinkWrapper extends EntityBase {
    private ExternalEntitiesMap link;
    private IEntity entityTU;

    public LinkWrapper(ExternalEntitiesMap link, IEntity entityTU) {
        if (link != null)
            setId(link.getId());
        else
            setId(entityTU.getId());

        this.link = link;
        this.entityTU = entityTU;
    }

    public static String getTitle(IEntity entity)
    {
        // IEntityMeta meta = EntityRuntime.getMeta(entity.getClass());
        if (entity == null)
            return "null (сущность не присвоена)";


        String title = null;
        try {
            title = (String) entity.getProperty("fullTitle");
        }
        catch (org.springframework.beans.InvalidPropertyException ex) {
            title = null;
        }

        if (title != null)
            return title;

        try {
            title = (String) entity.getProperty("title");
        }
        catch (org.springframework.beans.InvalidPropertyException ex) {
            title = null;
        }

        if (title != null)
            return title;

        return entity.toString();
    }

    public static String getTitleWithType(IEntity entity)
    {
        IEntityMeta meta = EntityRuntime.getMeta(entity.getClass());
        String title = null;
        try {
            title = (String) entity.getProperty("fullTitle");
        }
        catch (org.springframework.beans.InvalidPropertyException ex) {
            title = null;
        }

        if (title == null) {
            try {
                // выведем тип сущности
                String titlePart = (String) entity.getProperty("title");
                title = meta.getTitle() + ":" + titlePart;
            }
            catch (org.springframework.beans.InvalidPropertyException ex) {
                title = null;
            }
        }

        if (title != null)
            return title;

        return entity.toString();
    }


    public String getTitle() {
        return getTitle(this.entityTU);
    }

    public ExternalEntitiesMap getLink() {
        return link;
    }

    public void setLink(ExternalEntitiesMap link) {
        this.link = link;
    }

    public IEntity getEntityTU() {
        return entityTU;
    }

    public void setEntityTU(IEntity entityTU) {
        this.entityTU = entityTU;
    }

}
