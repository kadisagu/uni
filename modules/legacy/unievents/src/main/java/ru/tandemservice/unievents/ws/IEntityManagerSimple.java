package ru.tandemservice.unievents.ws;

public interface IEntityManagerSimple {
    public String sendRequest(String request);

    public String sendAnswer(String answer);
}
