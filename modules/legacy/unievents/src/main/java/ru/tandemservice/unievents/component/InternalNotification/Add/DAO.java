package ru.tandemservice.unievents.component.InternalNotification.Add;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unievents.entity.InternalNotification;
import ru.tandemservice.unievents.entity.catalog.IntegrationEventType;
import ru.tandemservice.unievents.entity.catalog.InternalEntityType;
import ru.tandemservice.uni.dao.UniDao;

public class DAO
        extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        if (model.getEntity().getId() != null)
            model.setEntity(getNotNull(InternalNotification.class, model.getEntity().getId()));

        model.setEntityTypeModel(new LazySimpleSelectModel<InternalEntityType>(InternalEntityType.class, InternalEntityType.title().s())
                                         .setSortProperty(InternalEntityType.title().s())
                                         .setSearchProperty(InternalEntityType.title().s()));

        model.setEventTypeModel(new LazySimpleSelectModel<IntegrationEventType>(IntegrationEventType.class)
                                        .setSortProperty("title")
                                        .setSearchProperty(IntegrationEventType.title().s()));
    }

    @Override
    public void update(Model model)
    {
        InternalNotification entity = model.getEntity();
        saveOrUpdate(entity);
    }
}
