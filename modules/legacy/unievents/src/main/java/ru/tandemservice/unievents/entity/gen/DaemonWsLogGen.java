package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unievents.entity.DaemonWsLog;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.catalog.StatusDaemonProcessingWs;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог работы демона
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DaemonWsLogGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.DaemonWsLog";
    public static final String ENTITY_NAME = "daemonWsLog";
    public static final int VERSION_HASH = -1348552647;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTITY_FOR_DAEMON_W_S = "entityForDaemonWS";
    public static final String P_LOG_DATE = "logDate";
    public static final String L_STATUS_DAEMON_PROCESSING_WS = "statusDaemonProcessingWs";
    public static final String P_ERROR_COMMENT = "errorComment";

    private EntityForDaemonWS _entityForDaemonWS;     // Изменения объектов для демона
    private Date _logDate;     // Дата обработки
    private StatusDaemonProcessingWs _statusDaemonProcessingWs;     // статус обработки демоном WS
    private String _errorComment;     // Описание ошибки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Изменения объектов для демона. Свойство не может быть null.
     */
    @NotNull
    public EntityForDaemonWS getEntityForDaemonWS()
    {
        return _entityForDaemonWS;
    }

    /**
     * @param entityForDaemonWS Изменения объектов для демона. Свойство не может быть null.
     */
    public void setEntityForDaemonWS(EntityForDaemonWS entityForDaemonWS)
    {
        dirty(_entityForDaemonWS, entityForDaemonWS);
        _entityForDaemonWS = entityForDaemonWS;
    }

    /**
     * @return Дата обработки. Свойство не может быть null.
     */
    @NotNull
    public Date getLogDate()
    {
        return _logDate;
    }

    /**
     * @param logDate Дата обработки. Свойство не может быть null.
     */
    public void setLogDate(Date logDate)
    {
        dirty(_logDate, logDate);
        _logDate = logDate;
    }

    /**
     * @return статус обработки демоном WS. Свойство не может быть null.
     */
    @NotNull
    public StatusDaemonProcessingWs getStatusDaemonProcessingWs()
    {
        return _statusDaemonProcessingWs;
    }

    /**
     * @param statusDaemonProcessingWs статус обработки демоном WS. Свойство не может быть null.
     */
    public void setStatusDaemonProcessingWs(StatusDaemonProcessingWs statusDaemonProcessingWs)
    {
        dirty(_statusDaemonProcessingWs, statusDaemonProcessingWs);
        _statusDaemonProcessingWs = statusDaemonProcessingWs;
    }

    /**
     * @return Описание ошибки.
     */
    public String getErrorComment()
    {
        return _errorComment;
    }

    /**
     * @param errorComment Описание ошибки.
     */
    public void setErrorComment(String errorComment)
    {
        dirty(_errorComment, errorComment);
        _errorComment = errorComment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DaemonWsLogGen)
        {
            setEntityForDaemonWS(((DaemonWsLog)another).getEntityForDaemonWS());
            setLogDate(((DaemonWsLog)another).getLogDate());
            setStatusDaemonProcessingWs(((DaemonWsLog)another).getStatusDaemonProcessingWs());
            setErrorComment(((DaemonWsLog)another).getErrorComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DaemonWsLogGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DaemonWsLog.class;
        }

        public T newInstance()
        {
            return (T) new DaemonWsLog();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityForDaemonWS":
                    return obj.getEntityForDaemonWS();
                case "logDate":
                    return obj.getLogDate();
                case "statusDaemonProcessingWs":
                    return obj.getStatusDaemonProcessingWs();
                case "errorComment":
                    return obj.getErrorComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityForDaemonWS":
                    obj.setEntityForDaemonWS((EntityForDaemonWS) value);
                    return;
                case "logDate":
                    obj.setLogDate((Date) value);
                    return;
                case "statusDaemonProcessingWs":
                    obj.setStatusDaemonProcessingWs((StatusDaemonProcessingWs) value);
                    return;
                case "errorComment":
                    obj.setErrorComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityForDaemonWS":
                        return true;
                case "logDate":
                        return true;
                case "statusDaemonProcessingWs":
                        return true;
                case "errorComment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityForDaemonWS":
                    return true;
                case "logDate":
                    return true;
                case "statusDaemonProcessingWs":
                    return true;
                case "errorComment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityForDaemonWS":
                    return EntityForDaemonWS.class;
                case "logDate":
                    return Date.class;
                case "statusDaemonProcessingWs":
                    return StatusDaemonProcessingWs.class;
                case "errorComment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DaemonWsLog> _dslPath = new Path<DaemonWsLog>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DaemonWsLog");
    }
            

    /**
     * @return Изменения объектов для демона. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.DaemonWsLog#getEntityForDaemonWS()
     */
    public static EntityForDaemonWS.Path<EntityForDaemonWS> entityForDaemonWS()
    {
        return _dslPath.entityForDaemonWS();
    }

    /**
     * @return Дата обработки. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.DaemonWsLog#getLogDate()
     */
    public static PropertyPath<Date> logDate()
    {
        return _dslPath.logDate();
    }

    /**
     * @return статус обработки демоном WS. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.DaemonWsLog#getStatusDaemonProcessingWs()
     */
    public static StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs> statusDaemonProcessingWs()
    {
        return _dslPath.statusDaemonProcessingWs();
    }

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.unievents.entity.DaemonWsLog#getErrorComment()
     */
    public static PropertyPath<String> errorComment()
    {
        return _dslPath.errorComment();
    }

    public static class Path<E extends DaemonWsLog> extends EntityPath<E>
    {
        private EntityForDaemonWS.Path<EntityForDaemonWS> _entityForDaemonWS;
        private PropertyPath<Date> _logDate;
        private StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs> _statusDaemonProcessingWs;
        private PropertyPath<String> _errorComment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Изменения объектов для демона. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.DaemonWsLog#getEntityForDaemonWS()
     */
        public EntityForDaemonWS.Path<EntityForDaemonWS> entityForDaemonWS()
        {
            if(_entityForDaemonWS == null )
                _entityForDaemonWS = new EntityForDaemonWS.Path<EntityForDaemonWS>(L_ENTITY_FOR_DAEMON_W_S, this);
            return _entityForDaemonWS;
        }

    /**
     * @return Дата обработки. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.DaemonWsLog#getLogDate()
     */
        public PropertyPath<Date> logDate()
        {
            if(_logDate == null )
                _logDate = new PropertyPath<Date>(DaemonWsLogGen.P_LOG_DATE, this);
            return _logDate;
        }

    /**
     * @return статус обработки демоном WS. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.DaemonWsLog#getStatusDaemonProcessingWs()
     */
        public StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs> statusDaemonProcessingWs()
        {
            if(_statusDaemonProcessingWs == null )
                _statusDaemonProcessingWs = new StatusDaemonProcessingWs.Path<StatusDaemonProcessingWs>(L_STATUS_DAEMON_PROCESSING_WS, this);
            return _statusDaemonProcessingWs;
        }

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.unievents.entity.DaemonWsLog#getErrorComment()
     */
        public PropertyPath<String> errorComment()
        {
            if(_errorComment == null )
                _errorComment = new PropertyPath<String>(DaemonWsLogGen.P_ERROR_COMMENT, this);
            return _errorComment;
        }

        public Class getEntityClass()
        {
            return DaemonWsLog.class;
        }

        public String getEntityName()
        {
            return "daemonWsLog";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
