package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unievents.entity.InternalNotification;
import ru.tandemservice.unievents.entity.catalog.IntegrationEventType;
import ru.tandemservice.unievents.entity.catalog.InternalEntityType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Уведомления из TU
 *
 * Уведомления, формируемые в TU, которые необходимо разослать внешним системам.
 * Уведомления могут формироваться по сущностям, которыз в TU нет, например ОХ.
 * 
 * На уведомления можно подписать внешн. систему. Отслеживаем только afterInsert (по сущности internalNotification)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class InternalNotificationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.InternalNotification";
    public static final String ENTITY_NAME = "internalNotification";
    public static final int VERSION_HASH = 2045343409;
    private static IEntityMeta ENTITY_META;

    public static final String P_EVENT_DATE = "eventDate";
    public static final String P_ENTITY_ID = "entityId";
    public static final String L_ENTITY_TYPE = "entityType";
    public static final String L_EVENT_TYPE = "eventType";

    private Date _eventDate;     // Дата изменения
    private String _entityId;     // Id сущности
    private InternalEntityType _entityType;     // Тип сущности
    private IntegrationEventType _eventType;     // Тип события

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата изменения. Свойство не может быть null.
     */
    @NotNull
    public Date getEventDate()
    {
        return _eventDate;
    }

    /**
     * @param eventDate Дата изменения. Свойство не может быть null.
     */
    public void setEventDate(Date eventDate)
    {
        dirty(_eventDate, eventDate);
        _eventDate = eventDate;
    }

    /**
     * @return Id сущности. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Id сущности. Свойство не может быть null.
     */
    public void setEntityId(String entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Тип сущности. Свойство не может быть null.
     */
    @NotNull
    public InternalEntityType getEntityType()
    {
        return _entityType;
    }

    /**
     * @param entityType Тип сущности. Свойство не может быть null.
     */
    public void setEntityType(InternalEntityType entityType)
    {
        dirty(_entityType, entityType);
        _entityType = entityType;
    }

    /**
     * @return Тип события. Свойство не может быть null.
     */
    @NotNull
    public IntegrationEventType getEventType()
    {
        return _eventType;
    }

    /**
     * @param eventType Тип события. Свойство не может быть null.
     */
    public void setEventType(IntegrationEventType eventType)
    {
        dirty(_eventType, eventType);
        _eventType = eventType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof InternalNotificationGen)
        {
            setEventDate(((InternalNotification)another).getEventDate());
            setEntityId(((InternalNotification)another).getEntityId());
            setEntityType(((InternalNotification)another).getEntityType());
            setEventType(((InternalNotification)another).getEventType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends InternalNotificationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) InternalNotification.class;
        }

        public T newInstance()
        {
            return (T) new InternalNotification();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eventDate":
                    return obj.getEventDate();
                case "entityId":
                    return obj.getEntityId();
                case "entityType":
                    return obj.getEntityType();
                case "eventType":
                    return obj.getEventType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eventDate":
                    obj.setEventDate((Date) value);
                    return;
                case "entityId":
                    obj.setEntityId((String) value);
                    return;
                case "entityType":
                    obj.setEntityType((InternalEntityType) value);
                    return;
                case "eventType":
                    obj.setEventType((IntegrationEventType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eventDate":
                        return true;
                case "entityId":
                        return true;
                case "entityType":
                        return true;
                case "eventType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eventDate":
                    return true;
                case "entityId":
                    return true;
                case "entityType":
                    return true;
                case "eventType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eventDate":
                    return Date.class;
                case "entityId":
                    return String.class;
                case "entityType":
                    return InternalEntityType.class;
                case "eventType":
                    return IntegrationEventType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<InternalNotification> _dslPath = new Path<InternalNotification>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "InternalNotification");
    }
            

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.InternalNotification#getEventDate()
     */
    public static PropertyPath<Date> eventDate()
    {
        return _dslPath.eventDate();
    }

    /**
     * @return Id сущности. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.InternalNotification#getEntityId()
     */
    public static PropertyPath<String> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Тип сущности. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.InternalNotification#getEntityType()
     */
    public static InternalEntityType.Path<InternalEntityType> entityType()
    {
        return _dslPath.entityType();
    }

    /**
     * @return Тип события. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.InternalNotification#getEventType()
     */
    public static IntegrationEventType.Path<IntegrationEventType> eventType()
    {
        return _dslPath.eventType();
    }

    public static class Path<E extends InternalNotification> extends EntityPath<E>
    {
        private PropertyPath<Date> _eventDate;
        private PropertyPath<String> _entityId;
        private InternalEntityType.Path<InternalEntityType> _entityType;
        private IntegrationEventType.Path<IntegrationEventType> _eventType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата изменения. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.InternalNotification#getEventDate()
     */
        public PropertyPath<Date> eventDate()
        {
            if(_eventDate == null )
                _eventDate = new PropertyPath<Date>(InternalNotificationGen.P_EVENT_DATE, this);
            return _eventDate;
        }

    /**
     * @return Id сущности. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.InternalNotification#getEntityId()
     */
        public PropertyPath<String> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<String>(InternalNotificationGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Тип сущности. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.InternalNotification#getEntityType()
     */
        public InternalEntityType.Path<InternalEntityType> entityType()
        {
            if(_entityType == null )
                _entityType = new InternalEntityType.Path<InternalEntityType>(L_ENTITY_TYPE, this);
            return _entityType;
        }

    /**
     * @return Тип события. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.InternalNotification#getEventType()
     */
        public IntegrationEventType.Path<IntegrationEventType> eventType()
        {
            if(_eventType == null )
                _eventType = new IntegrationEventType.Path<IntegrationEventType>(L_EVENT_TYPE, this);
            return _eventType;
        }

        public Class getEntityClass()
        {
            return InternalNotification.class;
        }

        public String getEntityName()
        {
            return "internalNotification";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
