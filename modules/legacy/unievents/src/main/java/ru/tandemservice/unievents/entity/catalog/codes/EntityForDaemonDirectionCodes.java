package ru.tandemservice.unievents.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Направление уведомления (из внешней системы/в внеш. сист)"
 * Имя сущности : entityForDaemonDirection
 * Файл data.xml : unievents.data.xml
 */
public interface EntityForDaemonDirectionCodes
{
    /** Константа кода (code) элемента : В внешную систему (title) */
    String TO_EXTERNAL_SYSTEM = "1";
    /** Константа кода (code) элемента : Из внешней системы (title) */
    String FROM_EXTERNAL_SYSTEM = "2";
    /** Константа кода (code) элемента : Уведомления TU в внешние системы (title) */
    String TO_EXTERNAL_SYSTEM_INTERNAL_NOTIFICATION = "3";

    Set<String> CODES = ImmutableSet.of(TO_EXTERNAL_SYSTEM, FROM_EXTERNAL_SYSTEM, TO_EXTERNAL_SYSTEM_INTERNAL_NOTIFICATION);
}
