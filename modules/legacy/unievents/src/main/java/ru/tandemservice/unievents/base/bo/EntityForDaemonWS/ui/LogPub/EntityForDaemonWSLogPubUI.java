package ru.tandemservice.unievents.base.bo.EntityForDaemonWS.ui.LogPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.State;
import ru.tandemservice.unievents.base.bo.EntityForDaemonWS.EntityForDaemonWSManager;
import ru.tandemservice.unievents.entity.DaemonWsLog;

@State({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "logEntityId")})
public class EntityForDaemonWSLogPubUI extends UIPresenter {
    private Long logEntityId;
    private DaemonWsLog model;

    @Override
    public void onComponentRefresh() {
        model = EntityForDaemonWSManager.instance().getDao().getLogEntityById(getLogEntityId());
    }

    public Long getLogEntityId() {
        return logEntityId;
    }

    public void setLogEntityId(Long logEntityId) {
        this.logEntityId = logEntityId;
    }

    public DaemonWsLog getModel() {
        return model;
    }

    public void setModel(DaemonWsLog model) {
        this.model = model;
    }
}
