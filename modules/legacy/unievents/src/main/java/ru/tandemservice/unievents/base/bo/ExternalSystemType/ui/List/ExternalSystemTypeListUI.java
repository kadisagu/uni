package ru.tandemservice.unievents.base.bo.ExternalSystemType.ui.List;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.ExternalSystemTypeManager;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.ui.Add.ExternalSystemTypeAdd;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

@State({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "publisherId")})
public class ExternalSystemTypeListUI extends UIPresenter {
    public final static String EXT_SYSTEM_FILTER = "externalSystem";
    public final static String ENTITY_TYPE_FILTER = "typeObject";
    private Long _publisherId;


    public void onSetInUseEntry() {
        ExternalSystemTypeManager.instance().dao().changeInUseEntry(getListenerParameterAsLong(), true);
    }

    public void onUnsetInUseEntry() {
        ExternalSystemTypeManager.instance().dao().changeInUseEntry(getListenerParameterAsLong(), false);
    }

    public void onClickAddType() {
        ExternalSystem es = getSettings().get(EXT_SYSTEM_FILTER);
        if (es != null) {
            // сохраним настройки для правильного отображения фильтра
            getSettings().save();

            getActivationBuilder().asRegion(ExternalSystemTypeAdd.class).parameter("externalSystemId", es.getId()).activate();
        }
        else {
            //выкидываем исключение, если фильтр не заполнен :
            throw new ApplicationException("для добавления необходимо заполнить фильтр «Внешная системаа»");
        }
    }

    public void onEditEntry() {
        getActivationBuilder().asRegion(ExternalSystemTypeAdd.class).parameter("publisherId", getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntry() {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickSearch() {
        getSettings().save();
    }

    public void onClickClear() {
        getSettings().clear();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource ds) {
        if (ds.getName().equals(ExternalSystemTypeList.LIST_DS)) {
            //помещаем значения фильтров в контекст LIST_DS
            ds.put(EXT_SYSTEM_FILTER, getSettings().get(EXT_SYSTEM_FILTER));
            ds.put(ENTITY_TYPE_FILTER, getSettings().get(ENTITY_TYPE_FILTER));
        }
    }

    public Long getPublisherId() {
        return _publisherId;
    }

    public void setPublisherId(Long _publisherId) {
        this._publisherId = _publisherId;
    }
}
