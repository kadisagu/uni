package ru.tandemservice.unievents.ws;

import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.codes.EventsErrorCodesCodes;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsErrorInfo;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(serviceName = "EventManager")
public class EventManager
        implements IEventsManager
{

    @Override
    @WebMethod
    public String RaiseEvents(
            @WebParam(name = "userName") String userName
            , @WebParam(name = "userPassword") String userPassword
            , @WebParam(name = "eventId") Long eventId
            , @WebParam(name = "entityId") String entityId
            , @WebParam(name = "eventType") String eventType
            , @WebParam(name = "entityType") String entityType
    ) throws Exception
    {
        // находим внешную систему
        ExternalSystem externalSystem = DaoFacade.getDaoEvents().getExternalSystem(userName, userPassword);
        if (externalSystem == null)
            // внешней системы нет (по имени пользователя и паролю ничего не нашли)
            return errorCode(EventsErrorCodesCodes.NO_EXTERNAL_SYSTEM_BY_USER_NAME_PWD, null);
        else {
            try {
                // внешная система есть - можно прописать в очередь на обработку
                DaoFacade.getDaoEvents().make4Daemon(externalSystem, eventType, entityType, entityId);
            }
            catch (Exception ex) {
                // ошибка исполнения
                return errorCode(EventsErrorCodesCodes.WS_TU_MAKE4_DAEMON, ex.getMessage());
            }
        }
        // значит ошибок не было
        return errorCode(null, null);
    }

    private String errorCode(String code, String message) throws Exception
    {
        WsErrorInfo error = DaoFacade.getDaoEvents().getWsErrorInfo(code, message);
        // формируем ответ
        XmlDocWS doc = new XmlDocWS();
        doc.addError(error);

        return doc.getStringDocument();
    }

}
