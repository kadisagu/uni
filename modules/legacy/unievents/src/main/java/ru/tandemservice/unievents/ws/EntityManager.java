package ru.tandemservice.unievents.ws;

import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.unievents.dao.IEntityManagerDao;
import ru.tandemservice.unievents.entity.catalog.codes.EventsErrorCodesCodes;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsErrorInfo;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(serviceName = "EntityManager")
public class EntityManager implements IEntityManager
{

    /**
     * Из внешней системы пришел запрос
     */
    @Override
    @WebMethod
    public String sendRequest(@WebParam(name = "request") String xml) throws Exception
    {

        IEntityManagerDao dao = IEntityManagerDao.instance.get();

        XmlDocWS resultDoc = new XmlDocWS();

        try
        {
            XmlDocWS requestDoc = createDocument(xml);
            if (requestDoc.hasErrors())
                // ошибка преобразования документа из внешней системы
                resultDoc = requestDoc;
            else
            {
                // Документ преобразован успешно
                ILogin loginInfo = requestDoc.getLogin();

                // обрабатывает, только если регистрационные данные верны
                boolean hasExternalSystem = checkExternalSystem(resultDoc, loginInfo);

                // если нет внешней системы - вообще ничего не делаем
                if (hasExternalSystem)
                {
                    // система есть, разбираем запросы
                    List<IRequest> requestList = requestDoc.getRequests();
                    if (requestList == null)
                        requestList = new ArrayList<>();

                    for (IRequest request : requestList)
                        // не разбеляем (синхронно/асинхронно)
                        doRequest(resultDoc, request, loginInfo);
                }
            }


            dao.saveLog(true, "sendRequest", xml, resultDoc.getStringDocument(), resultDoc.hasErrors(), null);

            return resultDoc.getStringDocument();

        } catch (Exception ex)
        {
            dao.saveLog(true, "sendRequest", xml, null, true, ex);
            throw ex;
        }
    }

    /**
     * вненая система прислала ответ на асинхронный запрос
     */
    @Override
    @WebMethod
    public String sendAnswer(@WebParam(name = "answer") String answer)
    {

        IEntityManagerDao dao = IEntityManagerDao.instance.get();
        dao.saveLog(true, "sendAnswer", answer, "", false, null);
        // TODO Auto-generated method stub
        return "<Root><TODO>sendAnswer</TODO></Root>";
    }


    private void doRequest(XmlDocWS doc, IRequest request, ILogin loginInfo)
    {
        if (request.isSyncAnswer())
            doRequestSync(doc, request, loginInfo);
        else
            doRequestAsync(doc, request, loginInfo);

    }

    /**
     * Синхронная обработка запроса
     */
    private void doRequestSync(XmlDocWS doc, IRequest request, ILogin loginInfo)
    {

        String requestId = request.getRequestId();
        List<IParameter> paramList = request.getParameters();
        String beanName = request.getBeanName();
        List<IError> errorList = new ArrayList<>();

        IEntityManagerDao dao = IEntityManagerDao.instance.get();

        IResponse response = dao.processRequest(beanName, paramList, requestId, errorList, loginInfo.getUserName(), loginInfo.getPassword());

        // ответ пишем всегда (даже при наличии ошибок)
        if (response != null)
            doc.addResponse(response);

        if (!errorList.isEmpty())
        {
            for (IError error : errorList)
                doc.addError(error);
        }

    }

    /**
     * асинхронная обработка запроса
     */
    private void doRequestAsync(XmlDocWS doc, IRequest request, ILogin loginInfo)
    {
        String requestId = request.getRequestId();
        List<IParameter> paramList = request.getParameters();
        String beanName = request.getBeanName();

        IEntityManagerDao dao = IEntityManagerDao.instance.get();
        try
        {
            dao.addRequestAsync(true, beanName, paramList, requestId, loginInfo.getUserName(), loginInfo.getPassword());
        } catch (Exception ex)
        {
            addError(doc, EventsErrorCodesCodes.ASYNC_JOB_ADD, ex.getMessage());
        }
    }

    private void addError(XmlDocWS doc, String errorCode, String message)
    {
        WsErrorInfo error = DaoFacade.getDaoEvents().getWsErrorInfo(errorCode, message);
        if (error != null)
            doc.addError(error);
    }

    /**
     * Наличие внешней системы
     * Если внешней системы нет, то код ошибки пишем в документ
     */
    private boolean checkExternalSystem(XmlDocWS doc, ILogin loginInfo)
    {
        if (loginInfo == null)
        {
            addError(doc, EventsErrorCodesCodes.LOGIN_REQUIRED, null);
            return false;
        }

        if (DaoFacade.getDaoEvents().getExternalSystem(loginInfo.getUserName(), loginInfo.getPassword()) == null)
        {
            addError(doc, EventsErrorCodesCodes.NO_EXTERNAL_SYSTEM_BY_USER_NAME_PWD, null);
            return false;
        }

        return true;
    }

    private XmlDocWS createDocument(String xml) throws Exception
    {
        XmlDocWS result;
        try
        {
            result = new XmlDocWS(xml);
        } catch (Exception ex)
        {
            result = new XmlDocWS();
            addError(result, EventsErrorCodesCodes.STRING_TO_DOM, ex.getMessage());
        }

        return result;
    }

}
