package ru.tandemservice.unievents.component.settings.ExternalNotification.List;


import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unievents.entity.ExternalNotification;

import java.util.List;

public class Model {

    private ISelectModel extEntityTypeModel;
    private ISelectModel extNotificationEventTypeModel;
    private List<String> processingStatusModel;
    private IDataSettings settings;
    private DynamicListDataSource<ExternalNotification> dataSource;
    private Long idExtNotification;

    public ISelectModel getExtEntityTypeModel() {
        return extEntityTypeModel;
    }

    public void setExtEntityTypeModel(ISelectModel extEntityTypeModel) {
        this.extEntityTypeModel = extEntityTypeModel;
    }

    public ISelectModel getExtNotificationEventTypeModel() {
        return extNotificationEventTypeModel;
    }

    public void setExtNotificationEventTypeModel(ISelectModel extNotificationEventTypeModel) {
        this.extNotificationEventTypeModel = extNotificationEventTypeModel;
    }

    public List<String> getProcessingStatusModel() {
        return processingStatusModel;
    }

    public void setProcessingStatusModel(List<String> processingStatusModel) {
        this.processingStatusModel = processingStatusModel;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<ExternalNotification> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExternalNotification> dataSource) {
        this.dataSource = dataSource;
    }

    public Long getIdExtNotification() {
        return idExtNotification;
    }

    public void setIdExtNotification(Long idExtNotification) {
        this.idExtNotification = idExtNotification;
    }
}
