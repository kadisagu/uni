package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemItemPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.DefaultCatalogItemPubController;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

public class Controller extends DefaultCatalogItemPubController<ExternalSystem, Model, IDAO> {
    public Controller() {

    }

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);
        prepareDataSource(component);
    }

    public void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);

        if (model.getDataSource() != null) {
            return;
        }
        DynamicListDataSource<ExternalSystemWS> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) Controller.this.getDao()).prepareListDataSource(model, component1.getSettings());
        });
        dataSource.addColumn(new SimpleColumn("Тип Web службы", ExternalSystemWS.wsType().title()));
        dataSource.addColumn(new SimpleColumn("URL Web службы, включая WSDL", ExternalSystemWS.wsUrl()));
        dataSource.addColumn(new SimpleColumn("TargetNamespace Web службы", ExternalSystemWS.wsTargetNamespace()));
        dataSource.addColumn(new SimpleColumn("ServiceName", ExternalSystemWS.wsServiceName()));
        dataSource.addColumn(new SimpleColumn("ServicePort", ExternalSystemWS.wsServicePort()));
        dataSource.addColumn(new SimpleColumn("Тип прокси", ExternalSystemWS.wsProxyType().title()));

        model.setDataSource(dataSource);
    }
}
