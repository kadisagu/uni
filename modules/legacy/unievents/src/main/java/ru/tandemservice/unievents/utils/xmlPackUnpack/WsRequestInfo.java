package ru.tandemservice.unievents.utils.xmlPackUnpack;

import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dvorlov
 */
public class WsRequestInfo implements IRequest
{
    private Map<String, IParameter> parameters = new HashMap<String, IParameter>();
    private String beanName = "";
    private String requestId = "";
    private String answerType = "";

    @Override
    public List<IParameter> getParameters() {
        return new ArrayList<IParameter>(parameters.values());
    }

    @Override
    public void setParameters(List<IParameter> parameters) {
        this.parameters = new HashMap<String, IParameter>();
        for (IParameter param : parameters)
            this.parameters.put(param.getParameterName(), param);
    }

    @Override
    public IParameter getParameter(String paramName) {
        return parameters.get(paramName);
    }

    @Override
    public boolean hasParameter(String paramName) {
        return parameters.containsKey(paramName);
    }

    @Override
    public String getBeanName() {
        return beanName;
    }

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    @Override
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String getAnswerType() {
        return answerType;
    }

    @Override
    public boolean isSyncAnswer() {
        return !"async".equals(getAnswerType());
    }

    @Override
    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }
}
