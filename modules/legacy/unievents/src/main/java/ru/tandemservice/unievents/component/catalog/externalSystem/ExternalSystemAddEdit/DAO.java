package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemAddEdit;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends DefaultCatalogAddEditDAO<ExternalSystem, Model> implements IDAO {
    public DAO() {

    }

    @Override
    public void prepareListDataSource(Model model, IDataSettings settings) {
        ExternalSystem externalSystem = model.getCatalogItem();
        DynamicListDataSource<ExternalSystemWS> dataSource = model.getDataSource();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ExternalSystemWS.class, "ws")
                .column("ws")
                .where(DQLExpressions.eq(DQLExpressions.property(ExternalSystemWS.externalSystem().fromAlias("ws")),
                                         DQLExpressions.value(externalSystem)));
        dataSource.setCountRow(getCount(builder));
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }
}
