package ru.tandemservice.unievents.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Идентификатор сущности внешней системы
 *
 * Идентификаторы сущностей внешних систем вместе с привязками к соответствующим сущностям в Юни. Требуется для интеграции с внешними системами.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExternalEntitiesMapGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unievents.entity.ExternalEntitiesMap";
    public static final String ENTITY_NAME = "externalEntitiesMap";
    public static final int VERSION_HASH = -1105751877;
    private static IEntityMeta ENTITY_META;

    public static final String P_EXTERNAL_ID = "externalId";
    public static final String P_ENTITY_ID = "entityId";
    public static final String P_META_TYPE = "metaType";
    public static final String P_EXTERNAL_TYPE_CODE = "externalTypeCode";
    public static final String P_BEAN_PREF_DEPRICATED = "beanPrefDepricated";
    public static final String L_EXTERNAL_SYSTEM = "externalSystem";

    private String _externalId;     // Id сущности во внешней системе
    private long _entityId;     // Id сущности в TU
    private String _metaType;     // Тип объекта в TU
    private String _externalTypeCode;     // Тип объекта во внешней системе
    private String _beanPrefDepricated;     // Бин префикс внешней системы (устаревшее поле)
    private ExternalSystem _externalSystem;     // Внешная система

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Id сущности во внешней системе. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getExternalId()
    {
        return _externalId;
    }

    /**
     * @param externalId Id сущности во внешней системе. Свойство не может быть null.
     */
    public void setExternalId(String externalId)
    {
        dirty(_externalId, externalId);
        _externalId = externalId;
    }

    /**
     * @return Id сущности в TU. Свойство не может быть null.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Id сущности в TU. Свойство не может быть null.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Тип объекта в TU.
     */
    @Length(max=255)
    public String getMetaType()
    {
        return _metaType;
    }

    /**
     * @param metaType Тип объекта в TU.
     */
    public void setMetaType(String metaType)
    {
        dirty(_metaType, metaType);
        _metaType = metaType;
    }

    /**
     * @return Тип объекта во внешней системе.
     */
    @Length(max=255)
    public String getExternalTypeCode()
    {
        return _externalTypeCode;
    }

    /**
     * @param externalTypeCode Тип объекта во внешней системе.
     */
    public void setExternalTypeCode(String externalTypeCode)
    {
        dirty(_externalTypeCode, externalTypeCode);
        _externalTypeCode = externalTypeCode;
    }

    /**
     * @return Бин префикс внешней системы (устаревшее поле).
     */
    @Length(max=255)
    public String getBeanPrefDepricated()
    {
        return _beanPrefDepricated;
    }

    /**
     * @param beanPrefDepricated Бин префикс внешней системы (устаревшее поле).
     */
    public void setBeanPrefDepricated(String beanPrefDepricated)
    {
        dirty(_beanPrefDepricated, beanPrefDepricated);
        _beanPrefDepricated = beanPrefDepricated;
    }

    /**
     * @return Внешная система. Свойство не может быть null.
     */
    @NotNull
    public ExternalSystem getExternalSystem()
    {
        return _externalSystem;
    }

    /**
     * @param externalSystem Внешная система. Свойство не может быть null.
     */
    public void setExternalSystem(ExternalSystem externalSystem)
    {
        dirty(_externalSystem, externalSystem);
        _externalSystem = externalSystem;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExternalEntitiesMapGen)
        {
            setExternalId(((ExternalEntitiesMap)another).getExternalId());
            setEntityId(((ExternalEntitiesMap)another).getEntityId());
            setMetaType(((ExternalEntitiesMap)another).getMetaType());
            setExternalTypeCode(((ExternalEntitiesMap)another).getExternalTypeCode());
            setBeanPrefDepricated(((ExternalEntitiesMap)another).getBeanPrefDepricated());
            setExternalSystem(((ExternalEntitiesMap)another).getExternalSystem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExternalEntitiesMapGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExternalEntitiesMap.class;
        }

        public T newInstance()
        {
            return (T) new ExternalEntitiesMap();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "externalId":
                    return obj.getExternalId();
                case "entityId":
                    return obj.getEntityId();
                case "metaType":
                    return obj.getMetaType();
                case "externalTypeCode":
                    return obj.getExternalTypeCode();
                case "beanPrefDepricated":
                    return obj.getBeanPrefDepricated();
                case "externalSystem":
                    return obj.getExternalSystem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "externalId":
                    obj.setExternalId((String) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "metaType":
                    obj.setMetaType((String) value);
                    return;
                case "externalTypeCode":
                    obj.setExternalTypeCode((String) value);
                    return;
                case "beanPrefDepricated":
                    obj.setBeanPrefDepricated((String) value);
                    return;
                case "externalSystem":
                    obj.setExternalSystem((ExternalSystem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "externalId":
                        return true;
                case "entityId":
                        return true;
                case "metaType":
                        return true;
                case "externalTypeCode":
                        return true;
                case "beanPrefDepricated":
                        return true;
                case "externalSystem":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "externalId":
                    return true;
                case "entityId":
                    return true;
                case "metaType":
                    return true;
                case "externalTypeCode":
                    return true;
                case "beanPrefDepricated":
                    return true;
                case "externalSystem":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "externalId":
                    return String.class;
                case "entityId":
                    return Long.class;
                case "metaType":
                    return String.class;
                case "externalTypeCode":
                    return String.class;
                case "beanPrefDepricated":
                    return String.class;
                case "externalSystem":
                    return ExternalSystem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExternalEntitiesMap> _dslPath = new Path<ExternalEntitiesMap>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExternalEntitiesMap");
    }
            

    /**
     * @return Id сущности во внешней системе. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getExternalId()
     */
    public static PropertyPath<String> externalId()
    {
        return _dslPath.externalId();
    }

    /**
     * @return Id сущности в TU. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Тип объекта в TU.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getMetaType()
     */
    public static PropertyPath<String> metaType()
    {
        return _dslPath.metaType();
    }

    /**
     * @return Тип объекта во внешней системе.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getExternalTypeCode()
     */
    public static PropertyPath<String> externalTypeCode()
    {
        return _dslPath.externalTypeCode();
    }

    /**
     * @return Бин префикс внешней системы (устаревшее поле).
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getBeanPrefDepricated()
     */
    public static PropertyPath<String> beanPrefDepricated()
    {
        return _dslPath.beanPrefDepricated();
    }

    /**
     * @return Внешная система. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getExternalSystem()
     */
    public static ExternalSystem.Path<ExternalSystem> externalSystem()
    {
        return _dslPath.externalSystem();
    }

    public static class Path<E extends ExternalEntitiesMap> extends EntityPath<E>
    {
        private PropertyPath<String> _externalId;
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _metaType;
        private PropertyPath<String> _externalTypeCode;
        private PropertyPath<String> _beanPrefDepricated;
        private ExternalSystem.Path<ExternalSystem> _externalSystem;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Id сущности во внешней системе. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getExternalId()
     */
        public PropertyPath<String> externalId()
        {
            if(_externalId == null )
                _externalId = new PropertyPath<String>(ExternalEntitiesMapGen.P_EXTERNAL_ID, this);
            return _externalId;
        }

    /**
     * @return Id сущности в TU. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(ExternalEntitiesMapGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Тип объекта в TU.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getMetaType()
     */
        public PropertyPath<String> metaType()
        {
            if(_metaType == null )
                _metaType = new PropertyPath<String>(ExternalEntitiesMapGen.P_META_TYPE, this);
            return _metaType;
        }

    /**
     * @return Тип объекта во внешней системе.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getExternalTypeCode()
     */
        public PropertyPath<String> externalTypeCode()
        {
            if(_externalTypeCode == null )
                _externalTypeCode = new PropertyPath<String>(ExternalEntitiesMapGen.P_EXTERNAL_TYPE_CODE, this);
            return _externalTypeCode;
        }

    /**
     * @return Бин префикс внешней системы (устаревшее поле).
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getBeanPrefDepricated()
     */
        public PropertyPath<String> beanPrefDepricated()
        {
            if(_beanPrefDepricated == null )
                _beanPrefDepricated = new PropertyPath<String>(ExternalEntitiesMapGen.P_BEAN_PREF_DEPRICATED, this);
            return _beanPrefDepricated;
        }

    /**
     * @return Внешная система. Свойство не может быть null.
     * @see ru.tandemservice.unievents.entity.ExternalEntitiesMap#getExternalSystem()
     */
        public ExternalSystem.Path<ExternalSystem> externalSystem()
        {
            if(_externalSystem == null )
                _externalSystem = new ExternalSystem.Path<ExternalSystem>(L_EXTERNAL_SYSTEM, this);
            return _externalSystem;
        }

        public Class getEntityClass()
        {
            return ExternalEntitiesMap.class;
        }

        public String getEntityName()
        {
            return "externalEntitiesMap";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
