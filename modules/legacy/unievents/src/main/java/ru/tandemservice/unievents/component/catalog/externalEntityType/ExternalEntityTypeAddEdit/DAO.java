package ru.tandemservice.unievents.component.catalog.externalEntityType.ExternalEntityTypeAddEdit;


import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.uni.util.FilterUtils;

public class DAO extends DefaultCatalogAddEditDAO<ExternalEntityType, Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setExternalSystemModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(ExternalSystem.class, alias);
                FilterUtils.applySimpleLikeFilter(builder, alias, ExternalSystem.title(), filter);
                return builder;
            }
        });
    }
}
