package ru.tandemservice.unievents.base.bo.ExternalSystemType.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.ExternalSystemTypeManager;
import ru.tandemservice.unievents.base.bo.ExternalSystemType.ui.List.ExternalSystemTypeList;
import ru.tandemservice.unievents.dao.DaoFacade;
import ru.tandemservice.unievents.entity.ExternalSystemNeedEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.dao.IEntityTypeDao;

/**
 * entityId - если добавляем новую сущность, то из ui списочного представления нужно передать код сущности
 */
@Input({@Bind(key = "publisherId", binding = "publisherId"),
        @Bind(key = "externalSystemId", binding = "externalSystemId")})
public class ExternalSystemTypeAddUI extends UIPresenter {
    private Long _publisherId;
    private Long externalSystemId;

    ExternalSystemNeedEntityType model;
    IEntityTypeDao.EntityType entityWrapper;

    @Override
    public void onComponentRefresh() {
        if (getPublisherId() == null) {
            model = new ExternalSystemNeedEntityType();
            if (getExternalSystemId() != null) {
                ExternalSystem es = ExternalSystemTypeManager.instance().dao().getExternalSystem(getExternalSystemId());
                model.setExternalSystem(es);
            }
        }
        else {
            //сущность открыли на редактирование
            model = ExternalSystemTypeManager.instance().dao().getExternalSystemNeedEntityType(getPublisherId());

            IEntityTypeDao.EntityType et = DaoFacade.getEntityTypeDao().getEntityType(model.getEntityType());
            if (et != null)
                setEntityWrapper(et);

        }
    }

    public void onClickApply() {
        if (getModel() != null) {
            ExternalSystemNeedEntityType model = getModel();

            IEntityTypeDao.EntityType entityMeta = getEntityWrapper();
            model.setEntityType(entityMeta.getEntityCode());
            model.setEntityTypeName(entityMeta.getTitle());

            ExternalSystemTypeManager.instance().dao().saveExternalSystemNeedEntityType(model);

            // в общий список
            getActivationBuilder().asDesktopRoot(ExternalSystemTypeList.class).activate();
        }
    }


    public Long getPublisherId() {
        return _publisherId;
    }

    public void setPublisherId(Long _publisherId) {
        this._publisherId = _publisherId;
    }

    public ExternalSystemNeedEntityType getModel() {
        return model;
    }

    public void setModel(ExternalSystemNeedEntityType model) {
        this.model = model;
    }

    public Long getExternalSystemId() {
        return externalSystemId;
    }

    public void setExternalSystemId(Long externalSystemId) {
        this.externalSystemId = externalSystemId;
    }

    public IEntityTypeDao.EntityType getEntityWrapper() {
        return entityWrapper;
    }

    public void setEntityWrapper(IEntityTypeDao.EntityType entityWrapper) {
        this.entityWrapper = entityWrapper;
    }
}
