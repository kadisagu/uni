package ru.tandemservice.unievents.utils.xmlPackUnpack;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;

public class XmlPackUnpack {
    public static Element PackToElement
            (
                    Document doc
                    , Element rootElement
                    , String elementName
                    , String elementValue)
    {

        Element newElem = doc.createElement(elementName);
        rootElement.appendChild(newElem);
        newElem.setTextContent(elementValue);

        return newElem;
    }

    public static Element PackToElementRec(Document doc, Element rootElement, IResponse.BodyElement bodyElement) {
        Element newElem = doc.createElement(bodyElement.getName());
        rootElement.appendChild(newElem);

        if (bodyElement.getExternalId() != null)
            newElem.setAttribute("externalId", bodyElement.getExternalId());

        if (bodyElement.getComment() != null) {
            Comment comment = doc.createComment(bodyElement.getComment());
            rootElement.insertBefore(comment, newElem);
        }

        if (!bodyElement.getChildren().isEmpty()) {
            for (IResponse.BodyElement el : bodyElement.getChildren())
                PackToElementRec(doc, newElem, el);
        }
        else
            newElem.setTextContent(bodyElement.getValue());

        return newElem;
    }
}
