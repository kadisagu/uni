package ru.tandemservice.unievents.entity;

import ru.tandemservice.unievents.entity.gen.ExternalSystemNeedEntityTypeGen;

/**
 * Типы сущностей для внешней системы
 * <p/>
 * Перечисляем типы сущностей, события изменения по которым необходимо формировать
 * и через вызов web службы, уведомлять внешную систему
 */
public class ExternalSystemNeedEntityType extends ExternalSystemNeedEntityTypeGen {
}