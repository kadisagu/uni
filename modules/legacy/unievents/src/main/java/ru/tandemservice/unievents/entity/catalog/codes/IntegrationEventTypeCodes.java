package ru.tandemservice.unievents.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип события"
 * Имя сущности : integrationEventType
 * Файл data.xml : unievents.data.xml
 */
public interface IntegrationEventTypeCodes
{
    /** Константа кода (code) элемента : После добавления (title) */
    String AFTER_INSERT = "afterInsert";
    /** Константа кода (code) элемента : После обновления (title) */
    String AFTER_UPDATE = "afterUpdate";
    /** Константа кода (code) элемента : После удаления (title) */
    String AFTER_DELETE = "afterDelete";

    Set<String> CODES = ImmutableSet.of(AFTER_INSERT, AFTER_UPDATE, AFTER_DELETE);
}
