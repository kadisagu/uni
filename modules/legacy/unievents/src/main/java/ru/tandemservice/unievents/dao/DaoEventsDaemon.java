package ru.tandemservice.unievents.dao;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.unievents.entity.DaemonWsLog;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.StatusDaemonProcessingWs;
import ru.tandemservice.unievents.entity.catalog.codes.StatusDaemonProcessingWsCodes;
import ru.tandemservice.unievents.entity.catalog.codes.WsTypeCodes;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.ILogin;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;
import ru.tandemservice.unievents.ws.IEntityManager;
import ru.tandemservice.unievents.ws.IEventsManager;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessEventManagerBean;
import ru.tandemservice.unievents.utils.settings.AppProperty;
import ru.tandemservice.uni.dao.UniBaseDao;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DaoEventsDaemon extends UniBaseDao implements IDaoEventsDaemon
{

    public static final String BEAN_PREFIX = "eventManagerBean.";

    /**
     * демон обработки очереди сообщений
     * из внешней системы
     */
    public static final SyncDaemon DAEMON_FromExternal = new SyncDaemon(
            DaoEventsDaemon.class.getName() + ".fromExternal", 1 /* 1 раз в минуту */)
    {
        @Override
        protected void main()
        {
            if (!AppProperty.RUN_DEMON())
                return;

            IDaoEventsDaemon dao = IDaoEventsDaemon.instance.get();

            // получим список уведомлений из внешних систем
            Map<ExternalSystem, List<EntityForDaemonWS>> map = dao.getMapEntityForDaemonWS(false);
            Set<ExternalSystem> keySet = map.keySet();

            // обрабатываем отдельно для каждой внешней системы
            // пока не разпоточиваем, все в одной нитке
            for (ExternalSystem externalSystem : keySet)
            {
                List<EntityForDaemonWS> lst = map.get(externalSystem);
                // обработка до первого исключения,
                // как только настало исключение, очередь
                // далее не обрабатываем
                doEventsListFromExternalSystem(lst, dao);
            }
        }

        private void doEventsListFromExternalSystem(List<EntityForDaemonWS> lst, IDaoEventsDaemon dao)
        {
            for (EntityForDaemonWS entity : lst)
            {
                boolean success = dao.doEventsFromExternalSystem(entity);
                if (!success)
                    return;
            }
        }
    };


    /**
     * демон отправки событий во внешние системы
     */
    public static final SyncDaemon DAEMON_TOExternal = new SyncDaemon(
            DaoEventsDaemon.class.getName(), 1 /* 1 раз в минуту */)
    {
        @Override
        protected void main()
        {
            if (!AppProperty.RUN_DEMON())
                return;

            IDaoEventsDaemon dao = IDaoEventsDaemon.instance.get();

            Map<ExternalSystem, List<EntityForDaemonWS>> map = dao.getMapEntityForDaemonWS(true);
            Set<ExternalSystem> keySet = map.keySet();

            for (ExternalSystem externalSystem : keySet)
            {
                List<EntityForDaemonWS> lst = map.get(externalSystem);
                // данный список обрабатываем до первого исключения
                // как только настало исключение - останавливаем,
                // так как дальнейшая обработка лишена смысла (рушиться последовательность событий)
                doEventsList(lst, dao);
            }
        }

        private void doEventsList(List<EntityForDaemonWS> lst, IDaoEventsDaemon dao)
        {
            for (EntityForDaemonWS entity : lst)
            {
                boolean success = dao.doEvents(entity);
                if (!success)
                    return;
            }
        }
    };

    /**
     * Получаем бин для обработки события
     */
    private IProcessEventManagerBean findBean(EntityForDaemonWS events)
    {
        IProcessEventManagerBean retVal = findBean(makeBeanName(events, true));
        if (retVal == null)
        {
            // специфичного для внешней системы бина нет,
            // следовательно поищем общий для всех систем
            retVal = findBean(makeBeanName(events, false));
        }
        return retVal;
    }

    private String makeBeanName(EntityForDaemonWS events, boolean withBeanPrefics)
    {
        // бин должен зависить от внешней системы

        String beanPrefics = "";

        if (withBeanPrefics)
            beanPrefics = events.getExternalSystem().getBeanPref() + ".";


        String beanName
                = DaoEventsDaemon.BEAN_PREFIX
                + beanPrefics
                + events.getEntityType()
                + "1.0";

        return beanName;
    }

    private IProcessEventManagerBean findBean(String beanName)
    {
        //TODO: вынести кудато в настройки сопоставление бинов
        try
        {
            IProcessEventManagerBean bean = (IProcessEventManagerBean) ApplicationRuntime.getBean(beanName);
            return bean;

        } catch (Exception ex)
        {
        }

        return null;
    }

    public boolean doEventsFromExternalSystem(EntityForDaemonWS event)
    {
        ExternalSystem externalSystem = event.getExternalSystem();

        IWebClientManager manager = DaoFacade.getWebClientManager();
        IEntityManager webClient;

        try
        {
            webClient = manager.getEntityManager(externalSystem);
        } catch (Exception ex)
        {
            // ошибка, web клиента нет
            manager.removeClientWS(externalSystem, WsTypeCodes.ENTITY_MANAGER);
            _writeResult(event, ex);
            return false;
        }

        // клиента получили
        // нужно получить бин, который сформирует запрос
        IProcessEventManagerBean bean = findBean(event);
        String beanName = makeBeanName(event, true);

        if (bean == null)
        {
            // обрабатывать нечем, явная ошибка
            _writeResult(event, new ApplicationException("Нет bean для событий от внешней системы " + event.getExternalSystem().getTitle() + " ожидаемое имя " + beanName));
            return false;
        }

        // бин есть, можно обрабатывать
        XmlDocWS xdoc;
        try
        {
            // шаг номер один - готовим запрос
            xdoc = bean.processEvent(event);
        } catch (Exception ex)
        {
            // запрос приготовить не удалось
            _writeResult(event, new ApplicationException("Ошибка в bean.ProcessEvent(event): " + ex.getMessage()));
            return false;
        }

        if (xdoc != null)
        {
//			// возможна ситуация, что в xdoc есть ошибки
//			// не продолжаем в этом случае
//			if (xdoc.hasErrors())
//			{
//				// запрос приготовить не удалось
//				_writeResult(event, new ApplicationException("bean.ProcessEvent(event) возвратил xdoc, содержащий ошибки: " + xdoc.getErrorMessage()));
//				return false;
//			}
//			else
//				

            {
                // во вложенном документе ошибок нет, следовательно можно послать запрос
                // помним, запрос может быть асинхронным
                _registerAsyncRequest(beanName, xdoc, xdoc.getRequests());

                // не обращая внимание на содержимое - посылаем во внешку
                String answer = null;
                String xml = null;
                try
                {
                    xml = xdoc.getStringDocument();
                    answer = webClient.sendRequest(xml);

                    // запись в лог
                    DaoFacade.getEntityManagerDao().saveLog(false, "sendRequest", xml, answer, false, null);
                } catch (Exception ex)
                {
                    _writeResult(event, new ApplicationException("webClient.sendRequest вызвал ошибку " + ex.getMessage()));

                    // запись в лог
                    DaoFacade.getEntityManagerDao().saveLog(false, "sendRequest", xml, answer, true, ex);

                    return false;
                }

                if (answer != null)
                {
                    // от внешней системы пришел ответ
                    // передадим минимальные сведения о самом запросе
                    ILogin ilogin = xdoc.getLogin();

                    // сформированные запросы
                    List<IRequest> requestList = xdoc.getRequests();

                    // внешная система ответила синхронно
                    try
                    {
                        _processSyncAnswerFromExternalSystem(
                                answer
                                , ilogin.getUserName()
                                , ilogin.getPassword()
                                , requestList
                                , bean
                        );
                    } catch (Exception ex)
                    {
                        StringWriter sw = new StringWriter();
                        ex.printStackTrace(new PrintWriter(sw));
                        _writeResult(event, new ApplicationException(new StringBuilder()
                                .append("processSyncAnswerFromExternalSystem вызвал ошибку ")
                                .append(ex.getMessage())
                                .append("\n")
                                .append(sw.toString())
                                .toString())
                        );

                        return false;
                    }
                }
            }
        }

        _writeResult(event, null);
        return true;
    }

    /**
     * Обработка ответа внешней системы
     *
     * @throws Exception
     * @throws IOException
     * @throws ParserConfigurationException
     */
    private void _processSyncAnswerFromExternalSystem
    (
            String answer
            , String userName
            , String password
            , List<IRequest> requestList
            , IProcessEventManagerBean bean
    )
            throws Exception
    {
        // в сигнатуре вызова есть bean
        // это тот бин, которым мы готовили запрос

        // ответ внешней системы на запрос
        XmlDocWS responseDoc = new XmlDocWS(answer);

        // если ответ внешней системы содержит ошибки?
        if (responseDoc.hasErrors())
            throw new ApplicationException(" Ответ внешней системы содержит ошибки " + responseDoc.getErrorMessage());

        for (IResponse response : responseDoc.getResponses())
        {
            bean.processAnswer(responseDoc, userName, password, requestList, response);
        }
    }

//	/**
//	 * Подбираем бин для кода запроса
//	 * @param requestId
//	 * @param requestList
//	 * @return
//	 */
//	private String _findBeanName(String requestId, List<IRequest> requestList) 
//	{
//		if (requestId==null || requestId=="")
//			return null;
//		else
//		{
//			// ищем среди запросов
//			if (!requestList.isEmpty())
//			{
//				for (IRequest request : requestList)
//				{
//					if (request.getRequestId()!=null && request.getRequestId().equals(requestId))
//						return request.getBeanName();
//				}
//			}
//			return null;
//		}
//	}


    /**
     * регистрируем асинхронные запросы
     */
    private void _registerAsyncRequest(String beanName, XmlDocWS xdoc, List<IRequest> requests)
    {
        if (!requests.isEmpty())
        {
            ILogin ilogin = xdoc.getLogin();

            for (IRequest request : requests)
            {
                if (!request.isSyncAnswer())
                {
                    // ответ придет асинхронно
                    // данные запроса нужно записать
                    DaoFacade.getEntityManagerDao().addRequestAsync(false, beanName, request.getParameters(), request.getRequestId(), ilogin.getUserName(), ilogin.getPassword());
                }
            }
        }
    }

    /**
     * Обработка одного события
     * true - обработка успешна, можно делать дальше
     * false - ошибка обработки, далее такую внешную систему не обрабатываем
     */
    public boolean doEvents(EntityForDaemonWS events)
    {
        // такое вообще можно не обрабатывать
        if (events.isCanBePass())
        {
            if (statusPass == null)
                statusPass = getByCode(StatusDaemonProcessingWs.class, StatusDaemonProcessingWsCodes.PASS_EVENT);

            events.setLastDate(new Date());
            events.setStatusDaemonProcessingWs(statusPass);
            events.setLastErrorComment("Пропуск обработки по требованию заказчика");
            saveOrUpdate(events);
            getSession().flush();

            return true;
        }

        ExternalSystem externalSystem = events.getExternalSystem();
        // зная externalSystem - ищем web службу
        IWebClientManager manager = DaoFacade.getWebClientManager();
        IEventsManager webClient = null;

        try
        {
            webClient = manager.getEventsManager(externalSystem);
        } catch (Exception ex)
        {
            // ошибка, web клиента нет
            manager.removeClientWS(externalSystem, WsTypeCodes.EVENT_MANAGER);
            _writeResult(events, ex);
            return false;
        }

        // клиента получили - шлем уведомление
        String userName = externalSystem.getWsUserName();
        String userPassword = externalSystem.getWsPassword();
        String result;
        try
        {
            result = webClient.RaiseEvents(
                    userName
                    , userPassword
                    , events.getId()
                    , events.getEntityId()
                    , events.getEventType()
                    , events.getEntityType()
            );
        } catch (Exception ex)
        {
            manager.removeClientWS(externalSystem, WsTypeCodes.EVENT_MANAGER);
            _writeResult(events, ex);
            return false;
        }

        // проверяем ответ, если в ответе ошибки нет, все нормально
        // если в ответе ошибка, то обработку прекращаем
        if (result != null && result.toLowerCase().contains("error"))
        {
            // есть ошибка
            Exception ex = new Exception(result);
            _writeResult(events, ex);
            return false;
        }

        _writeResult(events, null);
        return true;
    }


    private StatusDaemonProcessingWs statusProcessed = null; //getByCode(StatusDaemonProcessingWs.class, StatusDaemonProcessingWsCodes.PROCESSED);
    private StatusDaemonProcessingWs statusError = null; //getByCode(StatusDaemonProcessingWs.class, StatusDaemonProcessingWsCodes.ERROR);
    private StatusDaemonProcessingWs statusPass = null;

    private void _writeResult(EntityForDaemonWS events, Exception ex)
    {
        // ErrorLog el;

        if (statusProcessed == null)
            statusProcessed = getByCode(StatusDaemonProcessingWs.class, StatusDaemonProcessingWsCodes.PROCESSED);

        if (statusError == null)
            statusError = getByCode(StatusDaemonProcessingWs.class, StatusDaemonProcessingWsCodes.ERROR);


        if (statusPass == null)
            statusPass = getByCode(StatusDaemonProcessingWs.class, StatusDaemonProcessingWsCodes.PASS_EVENT);


        events.setLastDate(new Date());
        if (ex != null)
        {
            String errMsg = "";
            if (ex.getMessage() == null)
                errMsg = "null error";

            // были ошибки
            events.setStatusDaemonProcessingWs(statusError);
            events.setLastErrorComment(errMsg);

            // детализация ошибки
            DaemonWsLog log = new DaemonWsLog();
            log.setEntityForDaemonWS(events);
            log.setLogDate(new Date());
            log.setStatusDaemonProcessingWs(statusError);

            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            log.setErrorComment(new StringBuilder()
                    .append(ex.getMessage())
                    .append("\n")
                    .append(sw.toString())
                    .toString()
            );

            save(log);
        } else
        {
            // без ошибок
            events.setStatusDaemonProcessingWs(statusProcessed);
            events.setLastErrorComment("");
        }

        saveOrUpdate(events);

        getSession().flush();


    }

    public Map<ExternalSystem, List<EntityForDaemonWS>> getMapEntityForDaemonWS(boolean forExternalSystem)
    {

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EntityForDaemonWS.class, "ent")
                .column("ent")
                .where(ne(property(EntityForDaemonWS.statusDaemonProcessingWs().code().fromAlias("ent")), value(StatusDaemonProcessingWsCodes.PROCESSED)))
                .where(ne(property(EntityForDaemonWS.statusDaemonProcessingWs().code().fromAlias("ent")), value(StatusDaemonProcessingWsCodes.PASS_EVENT)))

                // направление (во внешную систему/из внешней)
                .where(eq(property(EntityForDaemonWS.direction().toExternal().fromAlias("ent")), value(forExternalSystem)))

                // фильтр по признаку система используется
                .where(eq(property(EntityForDaemonWS.externalSystem().useThisExternalSystem().fromAlias("ent")), value(Boolean.TRUE)))
                .order(DQLExpressions.property(EntityForDaemonWS.eventDate().fromAlias("ent")), OrderDirection.asc);

        List<EntityForDaemonWS> lst = dql.createStatement(getSession()).list();

        Map<ExternalSystem, List<EntityForDaemonWS>> map = new LinkedHashMap<>();
        for (EntityForDaemonWS ent : lst)
        {
            ExternalSystem externalSystem = ent.getExternalSystem();

            List<EntityForDaemonWS> lstMap;
            if (map.containsKey(externalSystem))
                lstMap = map.get(externalSystem);
            else
            {
                lstMap = new ArrayList<>();
                map.put(externalSystem, lstMap);
            }
            lstMap.add(ent);
        }
        return map;
    }

}
