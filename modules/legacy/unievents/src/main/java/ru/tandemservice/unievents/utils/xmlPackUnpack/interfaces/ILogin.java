package ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces;

public interface ILogin {

    public String getUserName();

    public void setUserName(String userName);


    public String getPassword();

    public void setPassword(String password);


}
