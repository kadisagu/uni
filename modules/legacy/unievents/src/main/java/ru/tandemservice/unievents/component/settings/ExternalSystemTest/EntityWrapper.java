package ru.tandemservice.unievents.component.settings.ExternalSystemTest;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.io.Serializable;


public class EntityWrapper extends EntityBase implements Serializable {

    private static final long serialVersionUID = -7211503220544105666L;

    public EntityWrapper(IEntity entity) {
        setId(entity.getId());
    }

    public static String getTitle(IEntity entity)
    {

        String title = null;
        try {
            title = (String) entity.getProperty("fullTitle");
        }
        catch (org.springframework.beans.InvalidPropertyException ex) {
            title = null;
        }

        if (title != null)
            return title;

        try {
            title = (String) entity.getProperty("title");
        }
        catch (org.springframework.beans.InvalidPropertyException ex) {
            title = null;
        }

        if (title != null)
            return title;

        return entity.toString();
    }

    public String getTitle() {
        return getTitle(getEntityTU());
    }

    public IEntity getEntityTU() {
        return IUniBaseDao.instance.get().getNotNull(getId());
    }

}
