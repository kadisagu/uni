package ru.tandemservice.unievents.ws.bean;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.ExternalNotification;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;
import ru.tandemservice.unievents.entity.catalog.ExternalNotificationEventType;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

import java.util.Date;

public class ProcessEventBean_ExternalNotification
        extends BaseProcessEventBean
{

    public static final String BEAN_NAME = "eventManagerBean.ExternalNotification1.0";
    public static final String ELEMENT_ID = "ExternalNotificationId";


    public static final String ELEMENT_ExternalNotification = "ExternalNotification";

    @Override
    protected String getAnswerBeanName()
    {
        return BEAN_NAME;
    }

    @Override
    protected Class<? extends IEntity> getEntityClass()
    {
        return ExternalNotification.class;
    }

    @Override
    protected String getExternalId(BodyElement entityInfo, String forRequestId) {
        return entityInfo.find(ELEMENT_ID).getValue();
    }

    @Override
    protected SaveData fillEntity(IEntity entityTU, BodyElement entityInfo,
                                  ExternalSystem externalSystem
            , String forRequestId)
    {

        // тип события
        EventType eventTypeTu = getEventTypeForRequestId(forRequestId);

        if (eventTypeTu == null)
            throw new ApplicationException("Не корректный тип события в очереди сообщений");

        if (eventTypeTu == EventType.DELETE && entityTU == null)
            throw new ApplicationException("Тип события 'afterDelete', не найдена сущность для удаления");


        // <TODO><VCH>2013/09/19 не нашлось и не надо, игнорируем такое, просто создадим новую сущность
        // клиент не понимает!!! Вообще блин не понимает
        /*
		if (eventTypeTu==EventType.UPDATE && entityTU==null)
			throw new ApplicationException("Тип события 'afterUpdate', не найдена сущность для обновления");
		*/

        // прежде всего нужно знать тип события
        SaveData saveData = new SaveData();

        if (entityTU == null)
            entityTU = new ExternalNotification();


        if (eventTypeTu == EventType.UPDATE || eventTypeTu == EventType.INSERT)
            _fillEntity((ExternalNotification) entityTU, entityInfo, externalSystem, forRequestId);

        // исключительно для создания линка
        saveData.setMainEntity(entityTU);

        // сущность для добавления/обновления/удаления

        SaveDataItem item = null;
        if (eventTypeTu == EventType.DELETE)
            item = new SaveDataItem(entityTU, true);
        else
            item = new SaveDataItem(entityTU, false);

        saveData.getItems().add(item);
        return saveData;
    }

    private void _fillEntity(
            ExternalNotification entity
            , BodyElement entityInfo
            , ExternalSystem externalSystem
            , String forRequestId
    )
    {

        if (entity == null)
            throw new ApplicationException("fillEntity пытается заполнить пустой объект");


        BodyElement externalNotification = null;

        if (entityInfo.getName().equals(ELEMENT_ExternalNotification))
            externalNotification = entityInfo;
        if (externalNotification == null)
            externalNotification = entityInfo.find(ELEMENT_ExternalNotification);

        if (externalNotification == null)
            throw new ApplicationException("В ответе нет элемента " + ELEMENT_ExternalNotification);

        EntityForDaemonWS entityForDaemonWS = getEntityForDaemonWS(forRequestId, true);
        entity.setEntityForDaemonWS(entityForDaemonWS);

        // ссылка на сущность внешней системы (о изменении чего сказано в уведомлении)
        String entityId = externalNotification.find("EntityId").getValue();
        entity.setExternalEntityId(entityId);

        ExternalEntitiesMap link = getLink(entityId, null, externalSystem);
        if (link != null) {
            // есть и тандемовский аналог
            Long tuEntityId = link.getEntityId();
            entity.setEntityId(tuEntityId);
        }

        String externalEntityType = entityInfo.find("ExternalEntityType").getValue();

        // по externalEntityType ищем соотв. позицию
        ExternalEntityType extType = findExternalEntityType(externalSystem, externalEntityType);
        if (extType != null)
            entity.setExternalEntityType(extType);


        Date dateNotification = getDateByString(externalNotification.find("DateNotification").getValue());
        entity.setDateNotification(dateNotification);

        String notification = entityInfo.find("Notification").getValue();
        entity.setNotification(notification);


        String externalNotificationEventType = entityInfo.find("ExternalNotificationEventType").getValue();
        ExternalNotificationEventType eventType = findExternalNotificationEventType(externalSystem, externalNotificationEventType);

        if (eventType != null)
            entity.setExternalNotificationEventType(eventType);

    }

    private ExternalNotificationEventType findExternalNotificationEventType
            (
                    ExternalSystem externalSystem
                    , String externalNotificationEventType
            )
    {
        ExternalNotificationEventType entity = getByCode(ExternalNotificationEventType.class, externalNotificationEventType);
        if (entity == null) {
            entity = new ExternalNotificationEventType();
            entity.setCode(externalNotificationEventType);
            entity.setTitle(externalNotificationEventType);
            saveOrUpdate(entity);
            return entity;
        }
        else
            return entity;
    }

    private ExternalEntityType findExternalEntityType(
            ExternalSystem externalSystem
            , String externalEntityType)
    {

        ExternalEntityType entity = getByCode(ExternalEntityType.class, externalEntityType);
        if (entity == null) {
            entity = new ExternalEntityType();
            entity.setCode(externalEntityType);
            entity.setTitle(externalEntityType);
            entity.setExternalSystem(externalSystem);

            saveOrUpdate(entity);
            return entity;
        }
        else
            return entity;


    }

    @Override
    protected IEntity findEntity(BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId)
    {
        // уже пытались искать сущность через линки
        // тут, только в том случае, если не нашли в линках
        // считаем, что обязана найтись через линки
        return null;
    }


}
