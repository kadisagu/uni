package ru.tandemservice.unievents.component.settings.TestEvent;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;

import java.util.Date;

public class Model {

    private ISelectModel externalSystemModel;
    private ISelectModel directionModel;
    private IDataSettings settings;
    private Date eventDate;
    private EntityForDaemonWS entityForDaemonWS;

    public EntityForDaemonWS getEntityForDaemonWS() {
        return entityForDaemonWS;
    }

    public void setEntityForDaemonWS(EntityForDaemonWS entityForDaemonWS) {
        this.entityForDaemonWS = entityForDaemonWS;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public ISelectModel getExternalSystemModel() {
        return externalSystemModel;
    }

    public void setExternalSystemModel(ISelectModel externalSystemModel) {
        this.externalSystemModel = externalSystemModel;
    }

    public ISelectModel getDirectionModel() {
        return directionModel;
    }

    public void setDirectionModel(ISelectModel directionModel) {
        this.directionModel = directionModel;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

}
