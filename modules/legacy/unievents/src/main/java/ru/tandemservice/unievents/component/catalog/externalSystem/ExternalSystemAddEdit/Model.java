package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemAddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.unievents.entity.ExternalSystemWS;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

public class Model extends DefaultCatalogAddEditModel<ExternalSystem> {
    public Model() {

    }

    private DynamicListDataSource<ExternalSystemWS> dataSource;


    public DynamicListDataSource<ExternalSystemWS> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExternalSystemWS> dataSource) {
        this.dataSource = dataSource;
    }


}
