package ru.tandemservice.unievents.component.catalog.externalSystem.ExternalSystemAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;

public interface IDAO extends IDefaultCatalogAddEditDAO<ExternalSystem, Model> {
}
