package ru.tandemservice.unievents.component.InternalNotification.Pub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = component.getModel();
        getDao().prepare(model);
    }

}
