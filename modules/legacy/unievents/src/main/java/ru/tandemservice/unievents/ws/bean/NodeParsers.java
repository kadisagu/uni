package ru.tandemservice.unievents.ws.bean;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.List;


public class NodeParsers {

    public static final String ENTITY_BY_CODE = "toEntityByCode";
    public static final String ENTITY_BY_EXTERNALID = "toEntityByExternalId";

    private static UniMap parserMap = new UniMap()
            .add(ENTITY_BY_CODE, new INodeParser() {
                public Object parse(String stringValue, String beanPref, Class clazz)
                {
                    DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(clazz, "e")
                            .where(DQLExpressions.eq(DQLExpressions.property("e.code"), DQLExpressions.value(stringValue)));
                    List<Object> list = IUniBaseDao.instance.get().getList(dql);

                    return !list.isEmpty() ? list.get(0) : null;
                }
            })
            .add(ENTITY_BY_EXTERNALID, new INodeParser() {
                public Object parse(String stringValue, String beanPref, Class clazz)
                {
                    DQLSelectBuilder dql = createDql(clazz);
                    dql.where(DQLExpressions.eq(DQLExpressions.property(ExternalEntitiesMap.externalId().fromAlias("l")), DQLExpressions.value(stringValue)));

                    List<Object[]> list = IUniBaseDao.instance.get().getList(dql);

                    if (list.size() == 1) {
                        Object[] arr = list.get(0);
                        String beanPrefics = (String) arr[1];
                        if (!beanPref.equals(beanPrefics))
                            throw new ApplicationException("В справочнике сопоставлений для  Id = " + stringValue + " указан некорректный префикс БИН");

                        return list.get(0)[0];
                    }
                    else if (list.size() > 1) {
                        for (Object[] arr : list) {
                            Object entity = (Object) arr[0];
                            String beanPrefics = (String) arr[1];

                            if (beanPref.equals(beanPrefics))
                                return entity;
                        }
                        throw new ApplicationException("Нельзя однозначно сопоставить сущность по Id = " + stringValue);

                    }
                    else
                        return null;
                }
            });


    public static INodeParser getParser(String parserName) {
        return (INodeParser) parserMap.get(parserName);
    }

    public static interface INodeParser {
        public Object parse(String stringValue, String beanPrefics, Class clazz);
    }

    private static DQLSelectBuilder createDql(Class clazz)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(clazz, "e")
                .joinEntity(
                        "e",
                        DQLJoinType.inner,
                        ExternalEntitiesMap.class,
                        "l",
                        DQLExpressions.eq(
                                DQLExpressions.property("e.id"),
                                DQLExpressions.property(ExternalEntitiesMap.entityId().fromAlias("l")))
                )
                .column("e")
                .column(ExternalEntitiesMap.externalSystem().beanPref().fromAlias("l").s());

        return dql;
    }
}
