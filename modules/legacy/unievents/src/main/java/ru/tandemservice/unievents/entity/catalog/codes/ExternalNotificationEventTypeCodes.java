package ru.tandemservice.unievents.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип события в уведомлении внешней системы"
 * Имя сущности : externalNotificationEventType
 * Файл data.xml : unievents.data.xml
 */
public interface ExternalNotificationEventTypeCodes
{
    /** Константа кода (code) элемента : Тип события не определен (title) */
    String NO_DEFINE = "1";

    Set<String> CODES = ImmutableSet.of(NO_DEFINE);
}
