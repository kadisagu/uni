package ru.tandemservice.unievents.dao;

import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.ws.IEntityManager;
import ru.tandemservice.unievents.ws.IEventsManager;

/**
 * Интерфейс работы с web клиентами
 *
 * @author vch
 */
public interface IWebClientManager
{

    /**
     * удалить из кеша все клиентские прокси для web служб
     */
    void clearCache();

    /**
     * удалить прокси для внешней системы
     */
    void removeClientWS(ExternalSystem externalSystem, String wsTypeCodes);

    /**
     * Получить прокси для внешней системы
     */
    IEventsManager getEventsManager(ExternalSystem externalSystem) throws Exception;

    /**
     * Получить прокси для внешней системы
     */
    IEntityManager getEntityManager(ExternalSystem externalSystem) throws Exception;
}
