package ru.tandemservice.unievents.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unievents.entity.InternalNotification;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsErrorInfo;

import java.util.Date;
import java.util.List;

public interface IDaoEvents {

    public static final String MUTEX = "lockStringForDao";

    /**
     * очистить кеш
     */
    public void clearCache();

    public void clearLastEvents();

    public List<ExternalSystemNeedEntity> getExternalSystemNeedEntityList();

    /**
     * Регистрируем событие возникшее внутри TU
     *
     * @param eventType
     * @param entityType
     * @param id
     * @param prevSource
     *
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean make4Daemon
    (
            String eventType
            , String entityType
            , boolean canBePass
            , String id
            , List<Object> prevSource
            , Date eventDate
            , InternalNotification internalNotification
    );

    /**
     * Регистрируем событие извне
     *
     * @param externalSystem - кто прислал событие (внешний EntityManager)
     * @param eventType
     * @param entityType
     * @param id
     *
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean make4Daemon(
            ExternalSystem externalSystem
            , String eventType
            , String entityType
            , String id);

    /**
     * Нужные типы объектов для внешней системы
     *
     * @author vch
     */
    public class ExternalSystemNeedEntity {
        private Long externalSystemId = 0L;
        private String entityType = "";
        private String integrationTypeCode = "";
        private String businessObjectCode = null;
        private String businessObjectName = null;


        public ExternalSystemNeedEntity
                (
                        Long externalSystemId
                        , String entityType
                        , String integrationTypeCode
                        , String businessObjectCode
                        , String businessObjectName
                )
        {
            setExternalSystemId(externalSystemId);
            setEntityType(entityType);
            setIntegrationTypeCode(integrationTypeCode);
            setBusinessObjectCode(businessObjectCode);
            setBusinessObjectName(businessObjectName);
        }


        public void setExternalSystemId(Long externalSystemId)
        {
            this.externalSystemId = externalSystemId;
        }

        public Long getExternalSystemId()
        {
            return externalSystemId;
        }

        public void setEntityType(String entityType)
        {
            this.entityType = entityType;
        }

        public String getEntityType()
        {
            return entityType;
        }


        public String getIntegrationTypeCode() {
            return integrationTypeCode;
        }


        public void setIntegrationTypeCode(String integrationTypeCode) {
            this.integrationTypeCode = integrationTypeCode;
        }


        public String getBusinessObjectCode() {
            return businessObjectCode;
        }


        public void setBusinessObjectCode(String businessObjectCode) {
            this.businessObjectCode = businessObjectCode;
        }


        public String getBusinessObjectName() {
            return businessObjectName;
        }


        public void setBusinessObjectName(String businessObjectName) {
            this.businessObjectName = businessObjectName;
        }
    }


    /**
     * Получить внешную систему по имени пользователя и паролю
     *
     * @param userName
     * @param userPassword
     *
     * @return
     */
    public ExternalSystem getExternalSystem(String userName, String userPassword);

    /**
     * сведения об ошибке
     *
     * @param errorCode
     * @param errorMsg
     *
     * @return
     */
    public WsErrorInfo getWsErrorInfo(String errorCode, String errorMsg);
}
