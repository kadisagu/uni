package ru.tandemservice.unievents.dao;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unievents.entity.EntityManagerLog;
import ru.tandemservice.unievents.entity.EntityManagerTask;
import ru.tandemservice.unievents.entity.EntityManagerTaskParam;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.entity.catalog.StatusDaemonProcessingWs;
import ru.tandemservice.unievents.entity.catalog.codes.EventsErrorCodesCodes;
import ru.tandemservice.unievents.entity.catalog.codes.StatusDaemonProcessingWsCodes;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsErrorInfo;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsLoginInfo;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsRequestParam;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IError;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.ILogin;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;
import ru.tandemservice.unievents.ws.IEntityManager;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessBean;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;

import javax.annotation.Nullable;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class EntityManagerDao extends UniBaseDao implements IEntityManagerDao
{
    public static final String ENABLE_PROPERTY = "unievents.ws.entity.manager.daemon.enabled";
    public static final String BEAN_PREFIX = "entityManagerBean.";

    public static final SyncDaemon DAEMON = new SyncDaemon(EntityManagerDao.class.getName(), 20, IEntityManagerDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            final IEntityManagerDao dao = IEntityManagerDao.instance.get();
            try
            {
                dao.loadAndCreateJobs();
            } catch (final Throwable e)
            {
                this.logger.warn(e.getMessage(), e);
            }
        }
    };


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Добавляем асинхронный запрос
     * (в очередь на обработку)
     */
    @Override
    public synchronized void addRequestAsync(boolean isInternal, String beanName, List<IParameter> paramList, String requestId, String userName, String userPass)
    {
        // все ошибки на этапе добавления асинхронного запроса
        // пишем в ответ (см. контекст вызова)
        if (StringUtils.isEmpty(requestId))
            throw new ApplicationException("Не указан ID для асинхронного запроса");

        if (this.getCount(EntityManagerTask.class, EntityManagerTask.requestId().s(), requestId) > 0)
            throw new ApplicationException("Асинхронный запрос " + requestId + " уже существует");

        EntityManagerTask task = new EntityManagerTask();
        task.setInternal(isInternal);
        task.setRequestDate(new Date());
        task.setInwork(false);

        task.setRequestBeanName(beanName);
        task.setRequestId(requestId);
        task.setUserName(userName);
        task.setUserPass(userPass);

        StatusDaemonProcessingWs status = getCatalogItem(StatusDaemonProcessingWs.class, StatusDaemonProcessingWsCodes.NOT_PROCESSING);
        task.setStatus(status);

        saveOrUpdate(task);

        if (paramList != null && !paramList.isEmpty())
            for (IParameter param : paramList)
            {
                EntityManagerTaskParam requestParam = new EntityManagerTaskParam();
                requestParam.setParent(task);
                requestParam.setParamName(param.getParameterName());
                requestParam.setParamValue(param.getParameterValue());

                saveOrUpdate(requestParam);
            }
    }

    /**
     * обработка запроса
     */
    @Override
    public IResponse processRequest(String beanName, List<IParameter> paramList, String requestId, List<IError> errorList, String userName, String password)
    {
        String requestStr = !StringUtils.isEmpty(requestId) ? " (запрос: " + requestId + ", " : " (";

        IProcessBean bean = findBean(beanName);

        final IDaoEvents daoEvents = DaoFacade.getDaoEvents();
        if (bean == null)
        {
            // нет бина - ошибка на выход
            WsErrorInfo error = daoEvents.getWsErrorInfo(EventsErrorCodesCodes.NOT_FOUND_BEAN, requestStr + "название бина: " + beanName + ")");
            errorList.add(error);
            return null;
        }

        IResponse response = null;
        try
        {
            response = bean.process(paramList, requestId, userName, password);
        } catch (ApplicationException ex)
        {
            WsErrorInfo error = daoEvents.getWsErrorInfo(EventsErrorCodesCodes.APPLICATION_EXCEPTION, ex.getMessage());
            errorList.add(error);

        } catch (Exception ex)
        {
            String message = "Системная ошибка";
            if (ex.getMessage() != null && !ex.getMessage().isEmpty())
                message += ex.getMessage();

            WsErrorInfo error = daoEvents.getWsErrorInfo(EventsErrorCodesCodes.SYSTEM_EXCEPTION, message);
            errorList.add(error);
        }

        if (response == null)
        {
            WsErrorInfo error = daoEvents.getWsErrorInfo(EventsErrorCodesCodes.BEAN_RETURN_NULL, requestStr + "название бина: " + beanName + ")");
            errorList.add(error);
            return null;
        } else
        {
            response.setForRequestId(requestId);
            return response;
        }
    }

    public synchronized void loadAndCreateJobs()
    {
        if (!ApplicationRuntime.existProperty(ENABLE_PROPERTY) || !("true".equals(ApplicationRuntime.getProperty(ENABLE_PROPERTY))))
            return;

        MQBuilder builder = new MQBuilder(EntityManagerTask.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", EntityManagerTask.inwork(), Boolean.FALSE))
                .add(MQExpression.eq("e", EntityManagerTask.status().code(), StatusDaemonProcessingWsCodes.NOT_PROCESSING));

        List<EntityManagerTask> list = getList(builder);
        for (EntityManagerTask task : list)
        {
            //догружаем параметры бина (потенциальные тормоза)
            List<EntityManagerTaskParam> requestParamList = getList(EntityManagerTaskParam.class, EntityManagerTaskParam.parent(), task);

            List<IParameter> params = new ArrayList<>();
            for (EntityManagerTaskParam requestParam : requestParamList)
            {
                IParameter param = new WsRequestParam();
                param.setParameterName(requestParam.getParamName());
                param.setParameterValue(requestParam.getParamValue());

                params.add(param);
            }

            Job job = new Job(task.getId(), task.getRequestBeanName(), params, task.getRequestId(), task.getUserName(), task.getUserPass());
            job.run();
            task.setInwork(true);
            saveOrUpdate(task);
        }
    }

    @Override
    public void registerResult(Long id, @Nullable String xml, List<IError> errorList, Exception ex, String userName, String userPass)
    {
        String content = xml;
        boolean _hasError = false;

        if (ex != null)
        {
            if (errorList == null)
                errorList = new ArrayList<>();
            WsErrorInfo error = DaoFacade.getDaoEvents().getWsErrorInfo(EventsErrorCodesCodes.U_N_D_I_F_I_N_E_E_R_R_O_R, ex.getMessage());
            errorList.add(error);

        }
        if (content == null)
        {
            if (errorList != null && !errorList.isEmpty())
                content = _createErrorDocument(errorList);

        }
        if (ex != null || (errorList != null && !errorList.isEmpty()))
            _hasError = true;


        DatabaseFile dFile = new DatabaseFile();
        dFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_XML);
        dFile.setContent(content != null ? content.getBytes() : null);
        saveOrUpdate(dFile);

        EntityManagerTask task = getNotNull(EntityManagerTask.class, id);
        task.setResponseDate(new Date());
        task.setResponse(dFile);
        task.setInwork(false);

        String status_code = StatusDaemonProcessingWsCodes.PROCESSED;
        if (_hasError)
            status_code = StatusDaemonProcessingWsCodes.ERROR;

        StatusDaemonProcessingWs status = getCatalogItem(StatusDaemonProcessingWs.class, status_code);

        task.setStatus(status);
        saveOrUpdate(status);
    }

    private String _createErrorDocument(List<IError> errorList)
    {
        XmlDocWS result;
        String retVal;
        try
        {
            result = new XmlDocWS();

            ILogin loginInfo = new WsLoginInfo();
            loginInfo.setUserName("");
            loginInfo.setPassword("");
            result.setLogin(loginInfo);

            if (!errorList.isEmpty())
            {
                for (IError error : errorList)
                    result.addError(error);
            }

            retVal = result.getStringDocument();
        } catch (Exception ex)
        {
            retVal = XmlDocWS.getSystemError();
        }
        return retVal;
    }

    @Override
//    @Transactional
    public void saveLog(boolean internal, String method, String requestXml, String responseXml, boolean hasError, Exception ex)
    {
        String otherError = "";

        // если кто-то забыл
        if (ex != null)
        {
            hasError = true;
            otherError = ex.getMessage();
        }

        EntityManagerLog log = new EntityManagerLog();
        log.setOk(true);

        DatabaseFile dFile = new DatabaseFile();
        dFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_XML);
        dFile.setContent(requestXml != null ? requestXml.getBytes() : new byte[0]);
//        IUniBaseDao.instance.get().saveOrUpdate(dFile);
        saveOrUpdate(dFile);
        log.setRequest(dFile);

        if (hasError)
            log.setOk(false);
        else
            log.setOk(true);

        DatabaseFile dFilerResp = new DatabaseFile();
        dFilerResp.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_XML);
        dFilerResp.setContent(responseXml != null ? responseXml.getBytes() : new byte[0]);
        saveOrUpdate(dFilerResp);
//        IUniBaseDao.instance.get().saveOrUpdate(dFilerResp);
        log.setResponse(dFilerResp);

        log.setInternal(internal);
        log.setMethod(method);
        log.setRequestDate(new Date());

        log.setErrorComment(otherError);
        saveOrUpdate(log);
//        IUniBaseDao.instance.get().saveOrUpdate(log);
    }


    @Override
    public void sendAsyncAnswerToRemoteSystem(String xml, String username, String password)
    {
        try
        {
            ExternalSystem externalSystem = DaoFacade.getDaoEvents().getExternalSystem(username, password);
            IEntityManager rmi = DaoFacade.getWebClientManager().getEntityManager(externalSystem);
            String answer = rmi.sendAnswer(xml);

            this.saveLog(false, "sendAnswer", xml, answer, false, null);
        } catch (Exception ex)
        {
            this.saveLog(false, "sendAnswer", xml, "", true, ex);
            throw new ApplicationException(ex.getMessage());
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void initDao() throws Exception
    {
        super.initDao();

        //сбросить 'в работе' при старте системы
        List<EntityManagerTask> list = IUniBaseDao.instance.get().getList(EntityManagerTask.class, EntityManagerTask.inwork(), Boolean.TRUE);
        for (EntityManagerTask entity : list)
        {
            entity.setInwork(false);
            IUniBaseDao.instance.get().saveOrUpdate(entity);
        }
    }

    private IProcessBean findBean(String beanName)
    {
        try
        {
            return (IProcessBean) ApplicationRuntime.getBean(BEAN_PREFIX + beanName);
        } catch (Exception ex)
        {
            return null;
        }
    }

    /////////////////// таска для jobExecutor ////////////////////////////////////////////

    /**
     * Эта работа исполняет асинхронный запрос к TU
     * Результат должен быть отправлен внешней системе
     */
    private static class Job implements Runnable
    {

        private Long id;

        private String beanName;
        private List<IParameter> paramList;
        private String requestId;

        private String userName;
        private String userPass;

        public Job(Long id, String beanName, List<IParameter> paramList, String requestId, String userName, String userPass)
        {
            this.id = id;
            this.beanName = beanName;
            this.paramList = paramList;
            this.requestId = requestId;

            this.userName = userName;
            this.userPass = userPass;
        }

        @Override
        public void run()
        {
            IEntityManagerDao dao = IEntityManagerDao.instance.get();
            // список возможных ошибок
            List<IError> errorList = new ArrayList<>();
//            String xml = null;
            try
            {
                // готовим ответ на асинхронный запрос из вне
                IResponse response = dao.processRequest(beanName, paramList, requestId, errorList, userName, userPass);
                // response может быть null, но errorList может содержать значения
                // готовим xml ответ
                String xml = prepareResponse(response, errorList);

                dao.sendAsyncAnswerToRemoteSystem(xml, userName, userPass);
                // результат исполнения в таску
                dao.registerResult(id, xml, errorList, null, userName, userPass);
            } catch (ParserConfigurationException ex)
            {
                dao.registerResult(id, null, errorList, ex, userName, userPass);
            }
        }

        /**
         * готовим xml строку
         * с ответом внешней системе
         *
         * @throws ParserConfigurationException
         */
        private String prepareResponse(IResponse response, List<IError> errorList) throws ParserConfigurationException
        {

            XmlDocWS doc;
            doc = new XmlDocWS();
            doc.addResponse(response);
            if (errorList != null && errorList.isEmpty())
            {
                for (IError error : errorList)
                    doc.addError(error);
            }
            return doc.getStringDocument();
        }
    }
}
