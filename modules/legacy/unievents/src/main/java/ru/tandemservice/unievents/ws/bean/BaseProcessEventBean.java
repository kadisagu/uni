package ru.tandemservice.unievents.ws.bean;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unievents.entity.EntityForDaemonWS;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.RequestInfo;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.utils.xmlPackUnpack.XmlDocWS;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Базовый bean для обработки очереди сообщений
 * от внешней системы (сообщение из очереди, формирование запроса, обработка ответа)
 * для объектов TU
 *
 * @author chuk
 */
public abstract class BaseProcessEventBean extends BaseProcessEventManagerBean {

    //do delete
    @Override
    protected IRequest doEventDelete(String extId, String entityType, ExternalSystem externalSystem, Long entityForDaemonWSId) {
        ExternalEntitiesMap link = getLink(extId, entityType, externalSystem);

        if (link == null)
            return null; //TODO: или бросить exception что нет объекта?

        IEntity entity = getNotNull(link.getEntityId());
        doDelete(entity, link);

        return null;
    }

    /**
     * перекрыть если в лоб удалять нельзя
     *
     * @param entity
     */
    protected void doDelete(IEntity entity, ExternalEntitiesMap link) {
        this.delete(entity);
        this.delete(link);
    }


    /**
     * зачем Гришук это написал так?
     * Формировать id запроса таким вот странным образом
     * В принципе не ясно - зачем?
     * Для асинхронного вызова да, надо, для синхронного нет
     * Отключаю сию хрень
     *
     * @param answerBeanName
     *
     * @return
     */
    protected RequestInfo saveRequestInfo(String answerBeanName) {
        RequestInfo info = new RequestInfo();
        info.setAnswerBeanName(answerBeanName);
        saveOrUpdate(info);

        return info;
    }

	/*
	protected IEntityMeta getEntityMeta(String entityType) {
		return EntityRuntime.getMeta(entityType);
	}
	*/


    /**
     * полное имя бина
     * vch (а нахрена Гришук это вписал сюда)
     *
     * @return
     */
    protected abstract String getAnswerBeanName();

    //////////////////////////////////////// ОБРАБОТКА РЕЗУЛЬТАТА ///////////////////////////////////////////////////////

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<SaveData> processAnswer(XmlDocWS xdoc
            , String userName
            , String password
            , List<IRequest> requestList
            , IResponse response) throws Exception
    {
        return super.processAnswer(xdoc, userName, password, requestList, response);
    }

	
	/*
	private String forRequestId = null;
	protected void setForRequestId(String forRequestId)
	{
		this.forRequestId = forRequestId;
	}
	protected String getForRequestId()
	{
		return forRequestId;
	}
   */

    private Map<String, EntityForDaemonWS> mapEntityForDaemonWS = new HashMap<String, EntityForDaemonWS>();

    /**
     * Возвращает EntityForDaemonWS на основе
     * известного forRequestId, может использовать кеш
     *
     * @return
     */
    protected EntityForDaemonWS getEntityForDaemonWS(String forRequestId, boolean useCache)
    {
        if (useCache) {
            if (mapEntityForDaemonWS.containsKey(forRequestId))
                return mapEntityForDaemonWS.get(forRequestId);
        }

        // получить
        EntityForDaemonWS retVal = null;
        if (forRequestId != null && !forRequestId.isEmpty()) {
            Long id = Long.parseLong(forRequestId);
            retVal = get(EntityForDaemonWS.class, id);
        }
        mapEntityForDaemonWS.put(forRequestId, retVal);

        return retVal;

    }

    protected EventType getEventTypeForRequestId(String forRequestId)
    {
        EntityForDaemonWS event = getEntityForDaemonWS(forRequestId, true);
        if (event == null)
            return null;
        else {
            String eventTypeString = event.getEventType();
            return getEventType(eventTypeString);
        }
    }

    @Override
    protected SaveData saveEntity(
            BodyElement entityInfo
            , ExternalSystem externalSystem
            , IResponse response
    )
    {
        synchronized (SYNC) {
            String forRequestId = response.getForRequestId();

            IEntity entity = null;
            String externalId = getExternalId(entityInfo, forRequestId);

            ExternalEntitiesMap link = null;
            if (externalId != null) {
                link = getLink(externalId, EntityRuntime.getMeta(getEntityClass()).getName(), externalSystem);
                if (link != null)
                    entity = get(link.getEntityId());
                else
                    entity = findEntity(entityInfo, externalSystem, forRequestId);
            }
            else
                entity = findEntity(entityInfo, externalSystem, forRequestId);


            SaveData saveData = fillEntity(entity, entityInfo, externalSystem, forRequestId);

            // если линк не найден, ставим признак - сохранить линк
            saveData.setSaveLink(link == null);
            saveData.setExternalId(externalId);

            doSaveEntity(saveData, entityInfo, externalSystem);

            return saveData;
        }
    }

    /**
     * По данному типу ищем линк
     *
     * @return
     */
    protected abstract Class<? extends IEntity> getEntityClass();

    /**
     * вернуть Id во внешней системе
     *
     * @param entityInfo
     *
     * @return
     */
    protected abstract String getExternalId(BodyElement entityInfo, String forRequestId);

    //

    /**
     * если entityTU == null, то создается новая сущность
     * Вернуть список объектов для сохранения
     *
     * @param entityTU
     * @param entityInfo
     *
     * @return
     */
    protected abstract SaveData fillEntity(IEntity entityTU, BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId);

    protected void doSaveEntity(SaveData saveData, BodyElement entityInfo, ExternalSystem externalSystem) {
        for (SaveDataItem item : saveData.getItems()) {
            if (item.getEntity() == null)
                continue;

            if (item.isDelete())
                this.delete(item.getEntity());
            else {
                // обработка ссылки
                if (item.getEntity() instanceof ExternalEntitiesMap) {
                    ExternalEntitiesMap link = (ExternalEntitiesMap) item.getEntity();
                    if (link.getEntityTUTempLink() != null)
                        link.setEntityId(link.getEntityTUTempLink().getId());
                }

                IEntity entity = item.getEntity();
                if (entity.getId() != null && entity.getId() > 0)
                    getSession().saveOrUpdate(item.getEntity());
                else
                    getSession().save(item.getEntity());
                getSession().flush();

                afterSaveOrUpdateSaveDataItem(item, saveData);

            }
        }

        if (saveData.isSaveLink()) {
            // а если линк не найден и id внешний не указан, что тогда?
            if (saveData.getExternalId() != null && saveData.getMainEntity() != null)
                doSaveLink(saveData.getMainEntity(), saveData.getExternalId(), externalSystem, true);
        }
    }

    /**
     * для организации возможности
     * записи свойств после сохранения
     * например ситуация Person/IdentityCard
     * <p/>
     * После сохранения Person у IdentityCard нужно выставить setPerson()
     *
     * @param item
     * @param saveData
     */
    protected void afterSaveOrUpdateSaveDataItem(SaveDataItem item,
                                                 SaveData saveData)
    {

    }

    /**
     * генерируем следующее значение для поля code_p
     *
     * @param clazz
     * @param preficsCode
     *
     * @return
     */
    protected String getNextCodeSprav(Class clazz, String preficsCode)
    {
        // 1 - количество записей
        int count = getCount(clazz);
        count++;
        String codeNext = preficsCode + Integer.toString(count);

        while (get(clazz, "code", codeNext) != null) {
            count++;
            codeNext = preficsCode + Integer.toString(count);
        }

        return codeNext;

    }

    protected ExternalEntitiesMap doSaveLink(IEntity entity, String externalId, ExternalSystem externalSystem, boolean saveLink)
    {
        IEntityMeta meta = EntityRuntime.getMeta(entity);
        ExternalEntitiesMap linkCheck = getEasyLink(externalId, externalSystem, meta.getName());

        if
                (
                linkCheck == null
                        || entity.getId().longValue() != linkCheck.getEntityId()
                )
        {
            // линк нужно создать
            ExternalEntitiesMap link = new ExternalEntitiesMap();

            if (entity.getId() != null)
                link.setEntityId(entity.getId());
            else
                link.setEntityTUTempLink(entity);

            link.setExternalId(externalId);


            link.setMetaType(meta.getName());
            link.setExternalSystem(externalSystem);

            if (saveLink) {
                if (entity.getId() == null)
                    throw new ApplicationException("ExternalEntitiesMap сохранить нельзя, сущность тандема еще не сохранена");

                getSession().saveOrUpdate(link);
            }

            return link;
        }
        else
            // линк есть
            return null;
    }

    /**
     * Получить простой линк
     *
     * @param externalId
     * @param externalSystem
     *
     * @return
     */
    protected ExternalEntitiesMap getEasyLink(String externalId, ExternalSystem externalSystem, String metaType)
    {

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ExternalEntitiesMap.class, "e")
                // по id из внешней системы
                .where(DQLExpressions.eq(DQLExpressions.property(ExternalEntitiesMap.externalId().fromAlias("e")), DQLExpressions.value(externalId)))
                        // по бин префиксу
                .where(DQLExpressions.eq(DQLExpressions.property(ExternalEntitiesMap.externalSystem().beanPref().fromAlias("e")), DQLExpressions.value(externalSystem.getBeanPref())));

        List<ExternalEntitiesMap> lst = getList(dql);

        ExternalEntitiesMap retVal = null;

        if (lst.size() == 1) {
            retVal = lst.get(0);
            // проверяем мета тип
            if (metaType != null) {
                if (retVal.getMetaType() != null && !retVal.getMetaType().toUpperCase().equals(metaType.toUpperCase()))
                    throw new ApplicationException("Для id внешней системы = '" + externalId + "' получен линк с другим типом сущности, проверте сопоставления");

            }
        }
        else {
            if (lst.isEmpty())
                retVal = null;
            else {
                // если записей несколько, то обязательно проверяем на тип возвращаемого значения
                if (metaType == null)
                    throw new ApplicationException("Нельзя однозначно определить ссылку на объек с id внешней системы = '" + externalId + "'");

                for (ExternalEntitiesMap link : lst) {
                    if (link.getMetaType() != null && link.getMetaType().toUpperCase().equals(metaType.toUpperCase()))
                        retVal = link;
                }
            }
        }

        return retVal;
    }

    /**
     * Найти сущность, если сущность не нашли = null
     *
     * @param entityInfo
     *
     * @return
     */
    protected abstract IEntity findEntity(
            BodyElement entityInfo
            , ExternalSystem externalSystem
            , String forRequestId);

    /**
     * По id объекта внешней системы ищем объект TU
     *
     * @param externalId
     * @param externalSystem
     *
     * @return
     */
    protected ExternalEntitiesMap getLink(String externalId, String metaType, ExternalSystem externalSystem)
    {
        // Сашина (Гришука) реализация странная
        // id во внешних системах может быть уникален только в рамках одной внешней системы
        // если несколько внешних систем сопоставят сущность по одинаковому id, то что?
		/*
		IEntityMeta meta = EntityRuntime.getMeta(getEntityClass());
		
		MQBuilder builder = new MQBuilder(ExternalEntitiesMap.ENTITY_CLASS, "e")
			.add(MQExpression.eq("e", ExternalEntitiesMap.externalId(), externalId))
			.add(MQExpression.eq("e", ExternalEntitiesMap.metaType(), meta.getName()));
		
		List<ExternalEntitiesMap> linkList = builder.getResultList(getSession());
		
		return !linkList.isEmpty() ? linkList.get(0) : null;
		*/

        // IEntityMeta meta = EntityRuntime.getMeta(getEntityClass());

        MQBuilder builder = new MQBuilder(ExternalEntitiesMap.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", ExternalEntitiesMap.externalId(), externalId))
                        // фильтр по префиксу внешней системы
                .add(MQExpression.eq("e", ExternalEntitiesMap.externalSystem().beanPref(), externalSystem.getBeanPref()));

        if (metaType != null)
            builder.add(MQExpression.eq("e", ExternalEntitiesMap.metaType(), metaType));

        List<ExternalEntitiesMap> linkList = builder.getResultList(getSession());

        // если мы получили записей >1, то ищем по типам объектов
        // или исключение?
        if (!linkList.isEmpty()) {
            if (linkList.size() == 1) {
                // всего одна запись
                return linkList.get(0);
            }
            else {
                // записей много
                // применим фильтр по мета типу
                Class<? extends IEntity> clazz = getEntityClass();
                if (clazz == null)
                    throw new ApplicationException("getEntityClass возвратил null");

                IEntityMeta meta = EntityRuntime.getMeta(clazz);

                int count = 0;
                ExternalEntitiesMap retVal = null;
                for (ExternalEntitiesMap fEntity : linkList) {
                    if (fEntity.getMetaType() != null
                            && fEntity.getMetaType().equals(meta.getName()))
                    {
                        count++;
                        retVal = fEntity;
                    }
                }
                if (count == 0 || count > 1)
                    throw new ApplicationException("Для внешней системы " + externalSystem.getTitle() + " не удалось однозначно сопоставить сущность с внешним id = '" + externalId + "'");

                return retVal;
            }

        }
        else
            return null;

        //return !linkList.isEmpty() ? linkList.get(0) : null;

    }

    /**
     * Поиск по коду из внешней системы
     *
     * @param <T>
     * @param clazz
     * @param externalId
     * @param externalSystem
     *
     * @return
     */
    protected <T extends IEntity> T findCatalogItem(Class<T> clazz, String externalId, ExternalSystem externalSystem)
    {
        return (T) NodeParsers.getParser(NodeParsers.ENTITY_BY_EXTERNALID).parse(externalId, externalSystem.getBeanPref(), clazz);
    }


    /**
     * атрибут, в который вложены параметры
     * для бина (задел на потом)
     *
     * @return
     */
    protected String getParametersAttributeName()
    {
        return "Parameters";
    }


}
