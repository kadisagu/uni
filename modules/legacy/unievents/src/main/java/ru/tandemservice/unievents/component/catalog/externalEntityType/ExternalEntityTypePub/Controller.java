package ru.tandemservice.unievents.component.catalog.externalEntityType.ExternalEntityTypePub;


import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.unievents.entity.catalog.ExternalEntityType;

public class Controller extends DefaultCatalogPubController<ExternalEntityType, Model, IDAO> {

    @Override
    protected DynamicListDataSource<ExternalEntityType> createListDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        DynamicListDataSource<ExternalEntityType> dataSource = new DynamicListDataSource<ExternalEntityType>(component, this);

        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", ExternalEntityType.P_TITLE));
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Код", ExternalEntityType.P_CODE));
        dataSource.addColumn(new SimpleColumn("Внешняя система", ExternalEntityType.externalSystem().title()));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        if (model.isUserCatalog())
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", new Object[]{"title"}).setPermissionKey(model.getCatalogItemDelete()));

        return dataSource;
    }
}
