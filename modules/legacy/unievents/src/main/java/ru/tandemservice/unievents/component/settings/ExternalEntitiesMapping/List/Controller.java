package ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.LinkWrapper;
import ru.tandemservice.unievents.entity.ExternalEntitiesMap;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());

        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component, model);
    }

    private void prepareDataSource(IBusinessComponent component, final Model model) {
        DynamicListDataSource<LinkWrapper> dataSource = model.getDataSource();
        if (dataSource != null)
            return;

        dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        });

        // dataSource.addColumn(new SimpleColumn("Объект TU", "title").setOrderable(false));

        // ссылка на сущность тандема
        dataSource.addColumn(new PublisherLinkColumn("Объект TU", "title").setResolver(new IPublisherLinkResolver() {
            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }

            @Override
            public Object getParameters(IEntity ientity)
            {
                Long id = null;

                if (ientity instanceof LinkWrapper)
                    id = ((LinkWrapper) ientity).getLink().getEntityId();


                if (ientity instanceof ViewWrapper) {
                    ViewWrapper vw = (ViewWrapper) ientity;
                    if (vw.getEntity() != null
                            && (vw.getEntity() instanceof ExternalEntitiesMap))
                        id = ((ExternalEntitiesMap) vw.getEntity()).getEntityId();
                }

                if (id != null)
                    return ParametersMap.createWith("publisherId", id);
                else
                    return null;
            }
        }).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("ID сущности TU", "link." + ExternalEntitiesMap.P_ENTITY_ID).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Тип объекта TU", "link.metaType").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Id сущности во внешней системе", "link.externalId").setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Внешняя система", "link.externalSystem." + ExternalSystem.P_BEAN_PREF).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete").setOrderable(false));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
        Model model = getModel(component);
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);

        model.getSettings().clear();
        onClickSearch(component);

    }

    public void onClickFillMeta(IBusinessComponent component) {
        Model model = getModel(component);
        Model.MetaWrapper meta = (Model.MetaWrapper) model.getSettings().get("entityName");
        ((IDAO) getDao()).fillMeta(meta);

        model.getDataSource().refresh();
    }

    public void onClickAdd(IBusinessComponent component) {
        Model model = getModel(component);


        if (model.getSettings().get("entityName") == null)
            throw new ApplicationException("Не указан тип сущности");

        if (model.getSettings().get("externalSystem") == null)
            throw new ApplicationException("Не выбрана внешная система");


        Model.MetaWrapper meta = (Model.MetaWrapper) model.getSettings().get("entityName");
        ParametersMap map = new ParametersMap();
        map.add("meta", meta.getMeta());
        map.add("externalSystem", model.getSettings().get("externalSystem"));

        activate(
                component,
                new ComponentActivator("ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.AddEdit",
                                       map)
        );
    }

    public void onClickEdit(IBusinessComponent component) {
        Long id = component.getListenerParameter();

        activate(
                component,
                new ComponentActivator("ru.tandemservice.unievents.component.settings.ExternalEntitiesMapping.AddEdit", ParametersMap.createWith("linkId", id))
        );
    }

    public void onClickDelete(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        ((IDAO) getDao()).delete(id);
    }
}
