/**
 * $Id$
 */
package ru.tandemservice.unisp;

import org.tandemframework.core.runtime.ApplicationRuntime;

import ru.tandemservice.unisp.component.extract.IStudentPaymentParagraphPrint;
import ru.tandemservice.unisp.component.order.IOrderExtractsListCustomizer;
import ru.tandemservice.unisp.component.order.IOrderReportPrint;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;

/**
 * @author dseleznev
 * Created on: 22.12.2009
 */
public class UnispUtils
{
    public static double getWrappedLongValue(long value)
    {
        return (0.001d * value);
    }
    
    public static long getUnwrappedDoubleValue(double value)
    {
        return (long) (value * 1000.0d);
    }
    
    public static IOrderExtractsListCustomizer getExtractsListComponentCustomizer(StudentPaymentExtractType type) throws ClassNotFoundException, IllegalAccessException, InstantiationException
    {
        return (IOrderExtractsListCustomizer) Class.forName(IOrderExtractsListCustomizer.class.getPackage().getName() + ".o" + type.getIndex() + ".StudentPaymentsOrderExtractsListCustomizer").newInstance();
    }
    
    public static IOrderReportPrint getOrderReportPrintController(StudentPaymentExtractType type)
    {
        return (IOrderReportPrint) ApplicationRuntime.getBean("studentPaymentsOrder" + type.getIndex() + "_orderReportPrint");
    }
    
    public static IStudentPaymentParagraphPrint getOrderParagraphPrintController(StudentPaymentExtractType type)
    {
        return (IStudentPaymentParagraphPrint) ApplicationRuntime.getBean("studentPaymentsOrder" + type.getIndex() + "_paragraphPrint");
    }
}