package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToReason;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь параграфа с причиной
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentParagraphToReasonGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToReason";
    public static final String ENTITY_NAME = "studentPaymentParagraphToReason";
    public static final int VERSION_HASH = 288278220;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARAGRAPH = "paragraph";
    public static final String L_REASON = "reason";
    public static final String P_COMMENT = "comment";

    private StudentPaymentParagraph _paragraph;     // Параграф
    private StudentPaymentExtractReason _reason;     // Причина
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Параграф.
     */
    public StudentPaymentParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Параграф.
     */
    public void setParagraph(StudentPaymentParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    /**
     * @return Причина.
     */
    public StudentPaymentExtractReason getReason()
    {
        return _reason;
    }

    /**
     * @param reason Причина.
     */
    public void setReason(StudentPaymentExtractReason reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentParagraphToReasonGen)
        {
            setParagraph(((StudentPaymentParagraphToReason)another).getParagraph());
            setReason(((StudentPaymentParagraphToReason)another).getReason());
            setComment(((StudentPaymentParagraphToReason)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentParagraphToReasonGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentParagraphToReason.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentParagraphToReason();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "paragraph":
                    return obj.getParagraph();
                case "reason":
                    return obj.getReason();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "paragraph":
                    obj.setParagraph((StudentPaymentParagraph) value);
                    return;
                case "reason":
                    obj.setReason((StudentPaymentExtractReason) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "paragraph":
                        return true;
                case "reason":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "paragraph":
                    return true;
                case "reason":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "paragraph":
                    return StudentPaymentParagraph.class;
                case "reason":
                    return StudentPaymentExtractReason.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentParagraphToReason> _dslPath = new Path<StudentPaymentParagraphToReason>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentParagraphToReason");
    }
            

    /**
     * @return Параграф.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToReason#getParagraph()
     */
    public static StudentPaymentParagraph.Path<StudentPaymentParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    /**
     * @return Причина.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToReason#getReason()
     */
    public static StudentPaymentExtractReason.Path<StudentPaymentExtractReason> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToReason#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends StudentPaymentParagraphToReason> extends EntityPath<E>
    {
        private StudentPaymentParagraph.Path<StudentPaymentParagraph> _paragraph;
        private StudentPaymentExtractReason.Path<StudentPaymentExtractReason> _reason;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Параграф.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToReason#getParagraph()
     */
        public StudentPaymentParagraph.Path<StudentPaymentParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new StudentPaymentParagraph.Path<StudentPaymentParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

    /**
     * @return Причина.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToReason#getReason()
     */
        public StudentPaymentExtractReason.Path<StudentPaymentExtractReason> reason()
        {
            if(_reason == null )
                _reason = new StudentPaymentExtractReason.Path<StudentPaymentExtractReason>(L_REASON, this);
            return _reason;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToReason#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(StudentPaymentParagraphToReasonGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return StudentPaymentParagraphToReason.class;
        }

        public String getEntityName()
        {
            return "studentPaymentParagraphToReason";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
