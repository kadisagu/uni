/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.orgunit.OrgUnitOrdersList;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author dseleznev
 * Created on: 22.12.2009
 */
@State(keys = "orgUnitId", bindings = "orgUnitId")
@Input(keys = "studentPaymentsTabSecModel", bindings = "parentSecModel")
public class Model
{
    private Long _orgUnitId;
    private OrgUnit orgUnit;
    private CommonPostfixPermissionModel _parentSecModel;
    private DynamicListDataSource<StudentPaymentsOrder> _dataSource;
    private CommonPostfixPermissionModel _secModel;
    private IDataSettings _settings;
    
    private ISelectModel _orderTypesListModel;

    public boolean isOrderAddingDisabled()
    {
        return _orderTypesListModel.findValues("").getObjects().size() == 0;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public Long getOrgUnitId()
    {
        return this._orgUnitId;
    }

    public void setOrgUnitId(final Long orgUnitId)
    {
        this._orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    public CommonPostfixPermissionModel getParentSecModel()
    {
        return _parentSecModel;
    }

    public void setParentSecModel(CommonPostfixPermissionModel parentSecModel)
    {
        _parentSecModel = parentSecModel;
    }

    public DynamicListDataSource<StudentPaymentsOrder> getDataSource()
    {
        return this._dataSource;
    }

    public void setDataSource(final DynamicListDataSource<StudentPaymentsOrder> dataSource)
    {
        this._dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public ISelectModel getOrderTypesListModel()
    {
        return _orderTypesListModel;
    }

    public void setOrderTypesListModel(ISelectModel orderTypesListModel)
    {
        this._orderTypesListModel = orderTypesListModel;
    }
}