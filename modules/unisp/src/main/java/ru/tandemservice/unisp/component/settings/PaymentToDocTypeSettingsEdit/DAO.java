/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.PaymentToDocTypeSettingsEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.catalog.gen.StudentPaymentGen;
import ru.tandemservice.unisp.entity.orders.StudentPaymentToExtractTypeRel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 21.12.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setExtractType(get(StudentPaymentExtractType.class, model.getStudentPaymentExtractTypeId()));

        model.setSelectedPaymentsList(getSelectedPaymentsList(model));
        model.setStudentPaymentsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(StudentPayment.ENTITY_CLASS, "sp");
                builder.add(MQExpression.like("sp", StudentPaymentGen.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("sp", StudentPaymentGen.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
    }

    @Override
    public void update(Model model)
    {
        MQBuilder validateBuilder = new MQBuilder(StudentPaymentToExtractTypeRel.ENTITY_CLASS, "rel");
        validateBuilder.add(MQExpression.notEq("rel", StudentPaymentToExtractTypeRel.L_FIRST, model.getDocType()));
        validateBuilder.add(MQExpression.in("rel", StudentPaymentToExtractTypeRel.L_SECOND, model.getSelectedPaymentsList()));
        if(validateBuilder.getResultCount(getSession()) > 0)
        {
            StudentPaymentToExtractTypeRel errRel = validateBuilder.<StudentPaymentToExtractTypeRel>getResultList(getSession()).get(0);
            throw new ApplicationException("Выплата «" + errRel.getSecond().getTitle() + "» уже связана с типом приказа «" + errRel.getFirst().getTitle() + "»");
        }

        List<Long> paymentsToStay = new ArrayList<>();
        MQBuilder builder = new MQBuilder(StudentPaymentToExtractTypeRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StudentPaymentToExtractTypeRel.L_FIRST, model.getDocType()));
        List<StudentPaymentToExtractTypeRel> relsList = builder.getResultList(getSession());
        for(StudentPaymentToExtractTypeRel rel : relsList)
        {
            if(!model.getSelectedPaymentsList().contains(rel.getSecond()))
            {
                getSession().delete(rel);
            }
            else
            {
                paymentsToStay.add(rel.getSecond().getId());
            }
        }

        for (StudentPayment payment : model.getSelectedPaymentsList())
        {
            if (!paymentsToStay.contains(payment.getId()))
            {
                StudentPaymentToExtractTypeRel rel = new StudentPaymentToExtractTypeRel();
                rel.setFirst(model.getDocType());
                rel.setSecond(payment);
                getSession().save(rel);
            }
        }
    }

    private List<StudentPayment> getSelectedPaymentsList(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentPaymentToExtractTypeRel.ENTITY_CLASS, "rel", new String[] {StudentPaymentToExtractTypeRel.L_SECOND});
        builder.add(MQExpression.eq("rel", StudentPaymentToExtractTypeRel.L_FIRST, model.getDocType()));
        builder.addOrder("rel", StudentPaymentToExtractTypeRel.L_SECOND + "." + StudentPaymentGen.P_TITLE);
        return builder.getResultList(getSession());
    }
}