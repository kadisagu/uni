/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.catalog.studentPayment.StudentPaymentPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unisp.entity.catalog.PaymentKind;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;

/**
 * @author AutoGenerator
 * Created on 19.12.2009
 */
@SuppressWarnings({"unchecked"})
public class Controller extends DefaultCatalogPubController<StudentPayment, Model, IDAO>
{
    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        super.onRefreshComponent(context);
        prepareListDataSource(getModel(context), context);
    }

    private void prepareListDataSource(final Model model, final IBusinessComponent context)
    {
        if (model.getOwnDataSource() == null)
            model.setOwnDataSource(createListDataSource(context));
    }

    @Override
    protected DynamicListDataSource createListDataSource(IBusinessComponent context)
    {
        Model model = getModel(context);
        DynamicListDataSource<StudentPaymentItemWrapper> dataSource = new DynamicListDataSource<>(context, this);
        dataSource.addColumn(getCatalogItemLinkColumn(model, "Название", StudentPaymentItemWrapper.P_TITLE).setOrderable(false).setRequired(true).setTreeColumn(true));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", StudentPaymentItemWrapper.P_SHORT_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Характер выплаты", new String[] {StudentPaymentItemWrapper.L_KIND, PaymentKind.P_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Сумма", StudentPaymentItemWrapper.P_VALUE)
                                     .setFormatter(source -> source == null ? null : MoneyFormatter.ruMoneyFormatter(3).format((Long) source))
                                     .setClickable(false).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Фиксированное значение", StudentPaymentItemWrapper.P_FIXED_VALUE));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()).setDisabledProperty(StudentPaymentItemWrapper.P_ROOT_NODE));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", StudentPaymentItemWrapper.P_TITLE).setPermissionKey(model.getCatalogItemDelete()).setDisabledProperty(StudentPaymentItemWrapper.P_ROOT_NODE));
        return dataSource;
    }

    @Override
    protected FormatterColumn getCatalogItemLinkColumn(Model model, String caption, Object titleKey)
    {
        final String[] items = StringUtils.split(model.getItemClass().getName(), '.');
        PublisherLinkColumn linkColumn = new PublisherLinkColumn(caption, titleKey);
        linkColumn.setResolver(new IPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return entity.getId();
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                if(((StudentPaymentItemWrapper)entity).getCatalogItem() instanceof StudentPayment)
                    return items[0] + '.' + items[1] + '.' + items[2] + ".component.catalog.studentPayment.StudentPaymentItemPub";
                else
                    return null;
            }
        });
        return linkColumn;
    }
}