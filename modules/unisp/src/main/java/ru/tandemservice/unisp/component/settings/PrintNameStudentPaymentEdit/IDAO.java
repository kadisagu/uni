package ru.tandemservice.unisp.component.settings.PrintNameStudentPaymentEdit;

/**
 * 
 * @author nkokorina
 * Created on: 18.02.2010
 */

public interface IDAO
{
    void prepare(Model model);

    void update(Model model);

}
