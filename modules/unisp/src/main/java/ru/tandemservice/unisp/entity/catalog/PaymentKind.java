package ru.tandemservice.unisp.entity.catalog;

import ru.tandemservice.unisp.entity.catalog.gen.PaymentKindGen;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;

/**
 * Характер выплат
 */
public class PaymentKind extends PaymentKindGen
{
    public static final String CODE_PER_MONTH   = "1";
    public static final String CODE_SINGLE      = "2";

    public long getPaymentTotalCost(final StudentPaymentAppointmentExtract e) {
        if (CODE_PER_MONTH.equals(getCode())) {
            return e.getMonthCount()*e.getPaymentCost();

        } else if (CODE_SINGLE.equals(getCode())) {
            return e.getPaymentCost();

        }
        return 0;
    }
}