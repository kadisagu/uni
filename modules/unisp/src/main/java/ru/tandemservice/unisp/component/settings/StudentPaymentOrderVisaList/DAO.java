// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.StudentPaymentOrderVisaList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisaGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 24.02.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        MQBuilder builder = new MQBuilder(OrgUnitToKindRelation.ENTITY_CLASS, "rel", new String[]{OrgUnitToKindRelation.orgUnit().s()});
        builder.add(MQExpression.eq("rel", OrgUnitToKindRelation.orgUnitKind().code().s(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        builder.add(MQExpression.eq("rel", OrgUnitToKindRelation.orgUnit().archival().s(), Boolean.FALSE));
        model.setOrgUnitModel(new LazySimpleSelectModel<OrgUnit>(builder.<OrgUnit>getResultList(getSession()), OrgUnit.P_FULL_TITLE));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        Object orgUnit = model.getSettings().get("orgUnit");

        List<Model.VisaWrapper> result = new ArrayList<Model.VisaWrapper>();

        MQBuilder emptyGroups = new MQBuilder(StudentPaymentOrderVisaGroup.ENTITY_CLASS, "e");
        if (null != orgUnit)
            emptyGroups.add(MQExpression.eq("e", StudentPaymentOrderVisaGroup.orgUnit().s(), orgUnit));
        emptyGroups.add(MQExpression.notExists(new MQBuilder(StudentPaymentOrderVisa.ENTITY_CLASS, "visa").add(MQExpression.eqProperty("visa", StudentPaymentOrderVisa.group().id().s(), "e", "id"))));
        emptyGroups.addOrder("e", StudentPaymentOrderVisaGroup.title().s());
        for (StudentPaymentOrderVisaGroup group : emptyGroups.<StudentPaymentOrderVisaGroup>getResultList(getSession()))
            result.add(new Model.VisaWrapper(group));

        MQBuilder visas = new MQBuilder(StudentPaymentOrderVisa.ENTITY_CLASS, "e");
        if (null != orgUnit)
            visas.add(MQExpression.eq("e", StudentPaymentOrderVisa.group().orgUnit().s(), orgUnit));
        visas.addOrder("e", StudentPaymentOrderVisa.group().title().s());
        visas.addOrder("e", StudentPaymentOrderVisa.groupsMemberVising().index().s());
        visas.addOrder("e", StudentPaymentOrderVisa.priority().s());

        for (StudentPaymentOrderVisa visa : visas.<StudentPaymentOrderVisa>getResultList(getSession()))
            result.add(new Model.VisaWrapper(visa));

        model.getDataSource().setCountRow(result.size());
        UniBaseUtils.createPage(model.getDataSource(), result);
    }

    @Override
    @SuppressWarnings({"unchecked", "deprecation"})
    public void updatePriority(Long id, boolean up)
    {
        UniBaseUtils.changePriority(id, (List) getItemList(id), StudentPaymentOrderVisa.priority().s(), up, getSession());
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        final Long id = (Long) component.getListenerParameter();
        final IEntity entity = get(id);
        StudentPaymentOrderVisaGroup group = (entity instanceof StudentPaymentOrderVisa) ? ((StudentPaymentOrderVisa) entity).getGroup() : (StudentPaymentOrderVisaGroup) entity;
        for (StudentPaymentOrderVisa item : getItemList(id))
            getSession().delete(item);
        getSession().delete(group);
    }

    private List<StudentPaymentOrderVisa> getItemList(Long id)
    {
        final IEntity entity = get(id);
        MQBuilder builder = new MQBuilder(StudentPaymentOrderVisa.ENTITY_CLASS, "e");
        if (entity instanceof StudentPaymentOrderVisa)
        {
            builder.add(MQExpression.eq("e", StudentPaymentOrderVisa.group().s(), ((StudentPaymentOrderVisa) entity).getGroup()));
            builder.add(MQExpression.eq("e", StudentPaymentOrderVisa.groupsMemberVising().s(), ((StudentPaymentOrderVisa) entity).getGroupsMemberVising()));
        }
        else
            builder.add(MQExpression.eq("e", StudentPaymentOrderVisa.group().s(), entity));
        builder.addOrder("e", StudentPaymentOrderVisa.P_PRIORITY);
        return builder.getResultList(getSession());
    }

    private List<StudentPaymentOrderVisa> getItemListToDel(Long id)
    {
        final IEntity entity = get(id);
        MQBuilder builder = new MQBuilder(StudentPaymentOrderVisa.ENTITY_CLASS, "e");
        if (entity instanceof StudentPaymentOrderVisa)
            builder.add(MQExpression.eq("e", StudentPaymentOrderVisa.group().s(), ((StudentPaymentOrderVisa) entity).getGroup()));
        else
            builder.add(MQExpression.eq("e", StudentPaymentOrderVisa.group().s(), entity));
        builder.addOrder("e", StudentPaymentOrderVisa.P_PRIORITY);
        return builder.getResultList(getSession());
    }
}
