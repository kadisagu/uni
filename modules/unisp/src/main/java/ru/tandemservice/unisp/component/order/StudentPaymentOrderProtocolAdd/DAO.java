// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolAdd;

import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.GroupCaptainStudent;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.unisession.dao.sessionObject.ISessionObjectDAO;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisp.entity.catalog.UnispDocumentTemplate;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;
import ru.tandemservice.unisp.entity.settings.StudentPaymentProtocolVisa;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 13.01.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    @SuppressWarnings("deprecation")
    public void prepare(final Model model)
    {
        model.setOrder(getNotNull(StudentPaymentsOrder.class, model.getOrder().getId()));
        final OrgUnit orgUnit = model.getOrder().getOrgUnit();
        model.setCommissionModel(new OrderExecutorSelectModel());
        model.setQualificationModel(new QualificationModel(getSession()));
        model.setCourseModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setGroupModel(new BaseMultiSelectModel("id", new String[]{"title"}, null)
        {
            @Override
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                MQBuilder builder = builder();
                filter = StringUtils.trimToNull(filter);
                if (null != filter)
                    builder.add(MQExpression.like("g", Group.title().s(), filter));

                final List<Group> groupList = builder.getResultList(session, 0, 49);

                final Group withoutGrop = new Group();
                withoutGrop.setId(-1L);
                withoutGrop.setTitle("вне групп");
                groupList.add(0, withoutGrop);

                return new ListResult<>(groupList, builder.getResultCount(session) + 1);
            }

            @Override
            @SuppressWarnings("unchecked")
            public List getValues(Set primaryKeys)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                if (primaryKeys.contains(-1L))
                    model.setWithNullGroup(true);
                else
                    model.setWithNullGroup(false);

                MQBuilder builder = builder();
                builder.add(MQExpression.in("g", "id", primaryKeys));

                return builder.getResultList(session);
            }

            private MQBuilder builder()
            {
                MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "g");

                builder.add(MQExpression.eq("g", Group.educationOrgUnit().formativeOrgUnit().s(), orgUnit));
                if (model.getCourseList() != null && !model.getCourseList().isEmpty())
                    builder.add(MQExpression.in("g", Group.course().s(), model.getCourseList()));
                builder.addOrder("g", Group.title().s());
                return builder;
            }
        });
        model.setWorkPlanRowKindModel(new CommonMultiSelectModel(EppWorkPlanRowKind.id().s(), EppWorkPlanRowKind.title().s())
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppWorkPlanRowKind.class, "b")
                        .column("b")
                        .where(likeUpper(property(EppWorkPlanRowKind.title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter, true))))
                        .order(property(EppWorkPlanRowKind.title().fromAlias("b")));

                if (set != null)
                    builder.where(in(property(EppWorkPlanRowKind.id().fromAlias("b")), set));

                return new DQLListResultBuilder(builder);
            }
        });
        model.setCaptainModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                MQBuilder builder = builder();
                filter = StringUtils.trimToNull(filter);
                if (null != filter)
                    builder().add(MQExpression.or(MQExpression.like("idc", IdentityCard.lastName().s(), filter), MQExpression.like("g", Group.title().s(), filter)));
                return new ListResult<>(builder.<Student>getResultList(session, 0, 50), builder.getResultCount(session));
            }

            @Override
            public Object getValue(Object key)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                MQBuilder builder = builder();
                builder.add(MQExpression.eq("s", "id", key));
                List resultList = builder.getResultList(session);
                return resultList.isEmpty() ? null : resultList.iterator().next();
            }

            private MQBuilder builder()
            {
                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
                builder.add(MQExpression.in("s", "id", new MQBuilder(GroupCaptainStudent.ENTITY_CLASS, "g", new String[]{GroupCaptainStudent.student().id().s()}).add(MQExpression.eq("g", GroupCaptainStudent.group().educationOrgUnit().formativeOrgUnit().s(), orgUnit))));
                builder.addJoinFetch("s", Student.person().identityCard().s(), "idc");
                builder.addOrder("idc", IdentityCard.lastName().s());
                return builder;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                //Петров Петр Петрович | группа ФБУ-06-1 (1 курс)
                Student student = (Student) value;
                Group group = student.getGroup();
                return student.getPerson().getFullFio() + " | группа " + group.getTitle() + " (" + group.getCourse().getTitle() + " курс)";
            }
        });
        model.setWorkPlanRowKindList(getDefaultWorkPlanRowKindList());
        if (!model.isInitialized())
        {
            EmployeePost head = EmployeeManager.instance().dao().getHead(orgUnit);
            model.setChair(head);
            MQBuilder builder = new MQBuilder(StudentPaymentProtocolVisa.ENTITY_CLASS, "rel", new String[]{StudentPaymentProtocolVisa.employeePost().s()});
            builder.add(MQExpression.eq("rel", StudentPaymentProtocolVisa.orgUnit().s(), orgUnit));
            builder.addOrder("rel", StudentPaymentProtocolVisa.employeePost().employee().person().identityCard().lastName().s());
            model.setCommission(builder.getResultList(getSession()));
            model.setInitialized(true);
            model.getProtocol().setStudentsWithoutExtracts(true);
            model.getProtocol().setGroupsWithoutExtracts(true);
        }
        reloadModelEntities(model);
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        StudentPaymentOrderProtocol report = model.getProtocol();

        report.setFormingDate(new Date());
        report.setOrder(model.getOrder());
        report.setChair(null);
        if (model.getChair() != null)
            report.setChair(model.getChair().getTitleWithOrgUnit());
        report.setCommission(null);
        if (model.getCommission() != null && !model.getCommission().isEmpty())
        {
            final String commission = UniStringUtils.join(model.getCommission(), "titleWithOrgUnit", "; ");
            report.setCommission(commission.length() > 255 ? commission.substring(0, 253) + ".." : commission);
        }
        report.setCaptain(null);
        if (model.isCaptainsFromGroup())
            report.setCaptain("свой староста в составе комиссии для каждой группы");
        else if (null != model.getCaptain())
        {
            Student student = model.getCaptain();
            Group group = student.getGroup();
            report.setCaptain(student.getPerson().getFullFio() + " | группа " + group.getTitle() + " (" + group.getCourse().getTitle() + " курс)");
        }
        report.setQualification(null);
        if (CollectionUtils.isNotEmpty(model.getQualificationList()))
            report.setQualification(StringUtils.join(model.getQualificationList(), "; "));

        report.setCourse(null);
        if (CollectionUtils.isNotEmpty(model.getCourseList()))
            report.setCourse(StringUtils.join(model.getCourseList(), "; "));

        report.setGroup(null);
        if (CollectionUtils.isNotEmpty(model.getGroupList()))
            report.setGroup(StringUtils.join(model.getGroupList(), "; "));

        if (model.isWithNullGroup())
            report.setGroup("вне групп; " + (model.getGroupList() != null && !model.getGroupList().isEmpty() ? StringUtils.trimToEmpty(report.getGroup()) : ""));

        byte[] data = RtfUtil.toByteArray(print(model));
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        session.save(content);

        report.setContent(content);
        session.save(report);
    }

    protected RtfDocument print(Model model)
    {
        RtfDocument template = new RtfReader().read(getCatalogItem(UnispDocumentTemplate.class, "studentPaymentOrderProtocol").getContent());

        Map<Group, List<Student>> groupMap = getGroupMap(model);
        List<Long> studentIds = collectIds(groupMap);

        Map<Long, List<String>> marksInfoByStudentId = getMarksInfo(studentIds, model.getWorkPlanRowKindList());

        Map<Long, String> studentsWithExtracts = getStudentsWithExtractsIds(model);

        RtfDocument result = null;

        for (Group group : groupMap.keySet())
        {
            RtfDocument page = template.getClone();

            fillHeader(page, group, model);
            fillVisas(page, model);
            boolean printPage = printStudentsTable(model, page, groupMap.get(group), marksInfoByStudentId, studentsWithExtracts);
            if (!printPage)
                continue;

            if (null == result)
                result = page;
            else
            {
                RtfUtil.modifySourceList(result.getHeader(), page.getHeader(), page.getElementList());
                result.getElementList().addAll(page.getElementList());
            }
        }

        if (null == result)
            throw new ApplicationException("Нет студентов для включения в протокол.");

        return result;
    }

    protected Map<Long, String> getStudentsWithExtractsIds(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentPaymentAppointmentExtract.ENTITY_CLASS, "extract", new String[]{StudentPaymentAppointmentExtract.entity().id().s(), StudentPaymentAppointmentExtract.paymentType().shortTitle().s()});
        builder.add(MQExpression.eq("extract", StudentPaymentExtract.paragraph().order().s(), model.getOrder()));
        Map<Long, String> result = new HashMap<>();
        for (Object[] row : builder.<Object[]>getResultList(getSession()))
            result.put((Long) row[0], (String) row[1]);
        return result;
    }

    protected HashMap<Long, List<String>> getMarksInfo(
            final List<Long> studentIds,
            final Collection<EppWorkPlanRowKind> workPlanRowKinds
    )
    {
        final Map<Long, List<String>> studentSessionMarkSummaryMap = studentSessionMarkSummary(studentIds, workPlanRowKinds);
        HashMap<Long, List<String>> result = new HashMap<>();
        for (Long student : studentIds)
        {
            List<String> markList = studentSessionMarkSummaryMap.get(student);
            if (markList.isEmpty())
            {
                result.put(student, Arrays.asList("0", "0", "0", "0"));
                continue;
            }

            int otl = 0;
            int hor = 0;
            int ud = 0;
            int zach = 0;
            for (String markValueCode : markList)
            {
                if (SessionMarkGradeValueCatalogItemCodes.OTLICHNO.equals(markValueCode))
                    otl++;
                if (SessionMarkGradeValueCatalogItemCodes.HOROSHO.equals(markValueCode))
                    hor++;
                if (SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO.equals(markValueCode))
                    ud++;
                if (SessionMarkGradeValueCatalogItemCodes.ZACHTENO.equals(markValueCode))
                    zach++;
            }
            result.put(student, Arrays.asList(String.valueOf(otl), String.valueOf(hor), String.valueOf(ud), String.valueOf(zach)));
        }
        return result;
    }

    protected HashMap<Group, List<Student>> getGroupMap(Model model)
    {
        MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "st");
        builder.add(MQExpression.eq("st", Student.archival().s(), Boolean.FALSE));
        builder.add(MQExpression.eq("st", Student.status().active().s(), Boolean.TRUE));

        FilterUtils.applySelectFilter(builder, "st", Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), model.getQualificationList());

        if (CollectionUtils.isNotEmpty(model.getCourseList()))
        {
            builder.add(MQExpression.in("st", Student.group().course().s(), model.getCourseList()));
            builder.add(MQExpression.in("st", Student.course().s(), model.getCourseList()));
        }
        if (CollectionUtils.isNotEmpty(model.getGroupList()) || model.isWithNullGroup())
        {
            if (model.isWithNullGroup())
            {
                builder.add(MQExpression.eq("st", Student.educationOrgUnit().formativeOrgUnit().s(), model.getOrder().getOrgUnit()));

                builder.add(MQExpression.or(
                        MQExpression.isNull("st", Student.group().s()),
                        MQExpression.in("st", Student.group().s(), model.getGroupList())));
            } else
            {
                builder.add(MQExpression.eq("st", Student.group().educationOrgUnit().formativeOrgUnit().s(), model.getOrder().getOrgUnit()));
                builder.add(MQExpression.in("st", Student.group().s(), model.getGroupList()));
            }
        }
        builder.addLeftJoinFetch("st", Student.group().s(), "grp");
        builder.addLeftJoinFetch("grp", Group.course().s(), "c");
        builder.addLeftJoinFetch("st", Student.person().identityCard().s(), "idc");
        builder.addLeftJoinFetch("st", Student.compensationType().s(), "ct");
        builder.addOrder("st", Student.course().code().s());
        builder.addOrder("st", Student.group().title().s());
        builder.addOrder("st", Student.person().identityCard().lastName().s());
        builder.addOrder("st", Student.person().identityCard().firstName().s());
        builder.addOrder("st", Student.person().identityCard().middleName().s());

        HashMap<Group, List<Student>> result = new LinkedHashMap<>();
        for (Student student : builder.<Student>getResultList(getSession()))
        {
            List<Student> students = result.get(student.getGroup());
            if (null == students)
                result.put(student.getGroup(), students = new ArrayList<>());
            students.add(student);
        }
        return result;
    }

    protected void fillHeader(RtfDocument page, Group group, Model model)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();

        String captain = "";
        if (null != group)
        {
            List<Student> studentList = UniDaoFacade.getGroupDao().getCaptainStudentList(group);
            for (Student student : studentList)
            {
                captain += (captain.isEmpty() ? "" : ", ") + student.getPerson().getFio();
            }
        }

        TopOrgUnit academy = TopOrgUnit.getInstance();
        modifier.put("vuzTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() == null ? "" : academy.getTitle() : academy.getNominativeCaseTitle());
        OrgUnit orgUnit = model.getOrder().getOrgUnit();
        modifier.put("ouTitle", orgUnit.getNominativeCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getNominativeCaseTitle());
        modifier.put("protocolNum", StringUtils.trimToEmpty(model.getProtocol().getProtocolNumber()));
        modifier.put("protocolDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocol().getProtocolDate()));
        modifier.put("orderPrNum", StringUtils.trimToEmpty(model.getProtocol().getCommOrderNumber()));
        modifier.put("orderPrDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocol().getCommOrderDate()));
        modifier.put("sBeg", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocol().getSessionStartDate()));
        modifier.put("sEnd", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocol().getSessionEndDate()));
        modifier.put("dayT", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocol().getRetakeDate()));
        modifier.put("leaderGroup", model.isCaptainsFromGroup() ? captain : model.getCaptain() == null ? "" : model.getCaptain().getPerson().getFio());
        modifier.put("groupTitle", group != null ? group.getTitle() : "");
        modifier.put("ouTitleG", StringUtils.uncapitalize(orgUnit.getGenitiveCaseTitle() == null ? orgUnit.getTitle() : orgUnit.getGenitiveCaseTitle()));
        modifier.put("course", group != null ? group.getCourse().getTitle() : "");

        modifier.modify(page);
    }

    protected void fillVisas(RtfDocument page, Model model)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();

        modifier.put("chairVISA", model.getChair() == null ? "" : model.getChair().getPerson().getFio());
        List<String> commission = CommonBaseUtil.getPropertiesList(model.getCommission(), EmployeePost.employee().person().fio().s());
        modifier.put("VISAS", StringUtils.join(commission, "; "));

        modifier.modify(page);

        RtfTableModifier tableModifier = new RtfTableModifier();

        List<String[]> visas = new ArrayList<>();
        for (String visa : commission)
            visas.add(new String[]{"", "", "", "", visa});
        if (visas.isEmpty())
            visas.add(new String[]{"", "Члены комиссии:"});
        else
            visas.get(0)[1] = "Члены комиссии:";
        tableModifier.put("V", visas.toArray(new String[visas.size()][]));

        tableModifier.modify(page);
    }

    protected boolean printStudentsTable(Model model, RtfDocument page, List<Student> students, Map<Long, List<String>> marksInfoByStudentId, Map<Long, String> studentsWithExtracts)
    {
        List<String[]> tableContent = new ArrayList<>();
        final Set<Integer> markRowBold = new HashSet<>();
        int i = 0;
        boolean existStudentWithExtract = false;
        for (Student student : students)
        {
            String type = studentsWithExtracts.get(student.getId());
            final boolean extractExist = null != type;
            if (!model.getProtocol().isStudentsWithoutExtracts() && !extractExist)
                continue;
            List<String> row = new ArrayList<>();
            row.add(String.valueOf(++i));
            row.add(student.getPerson().getFullFio());
            row.add(student.getBookNumber());
            row.add(UniDefines.COMPENSATION_TYPE_CONTRACT.equals(student.getCompensationType().getCode()) ? "дог." : "");
            row.addAll(marksInfoByStudentId.get(student.getId()));
            if (extractExist)
            {
                markRowBold.add(i);
                existStudentWithExtract = true;
            }
            row.add(type == null ? "отказать" : "назначить");
            row.add(StringUtils.trimToEmpty(type));
            tableContent.add(row.toArray(new String[row.size()]));
        }
        if (!model.getProtocol().isGroupsWithoutExtracts() && !existStudentWithExtract)
            return false;
        RtfTableModifier modifier = new RtfTableModifier();
        final int rowCount = tableContent.size();
        modifier.put("T", tableContent.toArray(new String[rowCount][]));
        modifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            @SuppressWarnings("deprecation")
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int i = 0; i < rowCount; i++)
                    if (markRowBold.contains(i + 1))
                        UniRtfUtil.setRowBold(newRowList.get(startIndex + i));
            }
        });
        modifier.modify(page);
        return true;
    }

    protected ArrayList<Long> collectIds(Map<Group, List<Student>> groupMap)
    {
        ArrayList<Long> list = new ArrayList<>();
        for (List<Student> students : groupMap.values())
            list.addAll(UniBaseUtils.getIdList(students));
        return list;
    }

    private List<EppWorkPlanRowKind> getDefaultWorkPlanRowKindList()
    {
        final ArrayList<EppWorkPlanRowKind> kinds = new ArrayList<>();
        kinds.add(getNotNull(EppWorkPlanRowKind.class, EppWorkPlanRowKind.code(), EppWorkPlanRowKindCodes.MAIN));
        kinds.add(getNotNull(EppWorkPlanRowKind.class, EppWorkPlanRowKind.code(), EppWorkPlanRowKindCodes.SELECTED));
        return kinds;
    }

    /**
     * Итоговые оценки в сессию за последний семестр из набора актуальных МСРП-ФК основной обр. программы.
     *
     * @param studentIds список id студентов
     * @return [student.id -> список кодов оценок, всегда notNull ]
     */
    private Map<Long, List<String>> studentSessionMarkSummary(
            final Collection<Long> studentIds,
            final Collection<EppWorkPlanRowKind> workPlanRowKinds
    )
    {
        final Map<Long, Integer> lastTermNumberMap = ISessionObjectDAO.instance.get().lastTermNumberMap(studentIds);

        final Map<Long, List<String>> resultMarkMap = SafeMap.get(ArrayList.class);
        for (List<Long> elements : Iterables.partition(studentIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionMark.class, "m")
                    .column(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")))
                    .column(property(SessionMark.slot().studentWpeCAction().studentWpe().term().intValue().fromAlias("m")))
                    .column(property(SessionMark.cachedMarkValue().code().fromAlias("m")))
                    .where(in(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")), elements))
                    .where(eq(property(SessionMark.slot().inSession().fromAlias("m")), value(true)))
                    .where(isNull(property(SessionMark.slot().studentWpeCAction().studentWpe().removalDate().fromAlias("m"))))
                    .where(in(property(SessionMark.slot().document().id().fromAlias("m")),
                            new DQLSelectBuilder().fromEntity(SessionStudentGradeBookDocument.class, "gb")
                                    .column(property(SessionStudentGradeBookDocument.id().fromAlias("gb")))
                                    .buildQuery())
                    );

            if (workPlanRowKinds != null && !workPlanRowKinds.isEmpty())
                builder.where(in(property(SessionMark.slot().studentWpeCAction().studentWpe().sourceRow().kind().fromAlias("m")), workPlanRowKinds));

            final List<Object[]> list = builder.createStatement(getSession()).list();
            for (Object[] objects : list)
            {
                final Long studentId = (Long) objects[0];
                final Integer term = (Integer) objects[1];
                final String markValueCode = (String) objects[2];

                final Integer lastTerm = lastTermNumberMap.get(studentId);
                if (lastTerm != null && lastTerm.equals(term))
                {
                    resultMarkMap.get(studentId)
                            .add(markValueCode);
                }
            }
        }

        return resultMarkMap;
    }
}
