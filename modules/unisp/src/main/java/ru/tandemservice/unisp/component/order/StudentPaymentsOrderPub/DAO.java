/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentsOrderPub;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;
import ru.tandemservice.unisp.dao.UnispDaoFacade;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol;
import ru.tandemservice.unisp.entity.orders.StudentPaymentOrderRelation;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author dseleznev
 *         Created on: 24.12.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder((StudentPaymentsOrder) getNotNull(model.getOrderId()));
        List<StudentPaymentOrderProtocol> protocols = getList(StudentPaymentOrderProtocol.class, StudentPaymentOrderProtocol.order().s(), model.getOrder(), StudentPaymentOrderProtocol.formingDate().s());
        if (!protocols.isEmpty())
            model.setProtocol(protocols.get(protocols.size() - 1));
        model.setSecModel(new CommonPostfixPermissionModel(model.getSecPostfix()));
        model.setVisingStatus(UnimvDaoFacade.getVisaDao().getVisingStatus(model.getOrder()));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentPaymentExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER, model.getOrder()));

        if (null != model.getStudentLastName())
            builder.add(MQExpression.like("e", IAbstractExtract.L_ENTITY + "." + Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME, CoreStringUtils.escapeLike(model.getStudentLastName())));
        if (null != model.getStudentFirstName())
            builder.add(MQExpression.like("e", IAbstractExtract.L_ENTITY + "." + Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME, CoreStringUtils
                    .escapeLike(model.getStudentFirstName())));

        builder.addOrder("e", IAbstractExtract.L_ENTITY + "." + Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
        builder.addOrder("e", IAbstractExtract.L_ENTITY + "." + Student.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
        builder.addOrder("e", IAbstractExtract.P_CREATE_DATE);
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void updateIndividualParagraph(long extractId)
    {
        StudentPaymentExtract extract = get(StudentPaymentExtract.class, extractId);
        if (extract != null)
        {
            extract.setIndividualParagraph(!extract.isIndividualParagraph());
            update(extract);
        }
    }

    @SuppressWarnings("unchecked")
    public void saveListOrderText(StudentPaymentsOrder order)
    {
        //1. проверяем существование печатной формы
        Criteria c = getSession().createCriteria(StudentPaymentOrderRelation.class);
        c.add(Restrictions.eq(StudentPaymentOrderRelation.L_ORDER, order));
        StudentPaymentOrderRelation rel = (StudentPaymentOrderRelation) c.uniqueResult();
        if (rel == null)
        {
            //2. если нету - создаем
            rel = new StudentPaymentOrderRelation();
            rel.setOrder(order);
        }


        if (order.getParagraphCount() == 0)
            throw new ApplicationException("Невозможно отправить на печать пустой приказ.");

        final byte[] template = UnispDaoFacade.getUnispExtractDAO().getTemplate(order.getType(), 1);
        IPrintFormCreator<StudentPaymentsOrder> formCreator = (IPrintFormCreator) ApplicationRuntime.getBean("studentPaymentsOrder" + order.getType().getIndex() + "_orderPrint");
        final RtfDocument document = formCreator.createPrintForm(template, order);
        rel.setText(RtfUtil.toByteArray(document));
        getSession().saveOrUpdate(rel);
    }

    @Override
    public void doSendToCoordination(Model model, IPersistentPersonable initiator)
    {
        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на согласование, так как он уже на согласовании.");

        if (StringUtils.isEmpty(model.getOrder().getNumber()))
            throw new ApplicationException("Нельзя отправить приказ на согласование без номера.");

        if (model.getOrder().getCommitDate() == null)
            throw new ApplicationException("Нельзя отправить приказ на согласование без даты приказа.");

        //2. надо сохранить печатную форму приказа
        saveListOrderText(model.getOrder());

        //3. отправляем на согласование
        ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
        sendToCoordinationService.init(model.getOrder(), initiator);
        sendToCoordinationService.execute();
    }

    @Override
    public void doSendToFormative(Model model)
    {
        //1. надо проверить, что у приказа сейчас не идет процедура согласования
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
            throw new ApplicationException("Нельзя отправить приказ на формирование, так как он на согласовании.");

        //2. надо сменить состояние на формируется
        model.getOrder().setState(getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
        update(model.getOrder());
    }

    @Override
    public void doReject(Model model)
    {
        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(model.getOrder());
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено пользователем с публикатора");
            rejectService.execute();
        }
        else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(model.getOrder(), UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(model.getOrder());
            touchService.execute();
        }
    }

    @Override
    public void doCommit(Model model)
    {
        if (model.getOrder().getNumber() == null)
            throw new ApplicationException("Нельзя проводить приказ без номера.");

        if (model.getOrder().getCommitDate() == null)
            throw new ApplicationException("Нельзя проводить приказ без даты приказа.");

        //обновляем печатную форму
        saveListOrderText(model.getOrder());

        MoveDaoFacade.getMoveDao().doCommitOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
    }

    @Override
    public void doRollback(Model model)
    {
        MoveDaoFacade.getMoveDao().doRollbackOrder(model.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_IN_ORDER), null);
    }
}