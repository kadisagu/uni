package ru.tandemservice.unisp.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unisp.UnispUtils;
import ru.tandemservice.unisp.entity.catalog.gen.StudentPaymentGen;

/**
 * Выплаты
 */
public class StudentPayment extends StudentPaymentGen implements IPrintablePayment, IHierarchyItem
{
    public static final String P_VALUE_AS_DOUBLE = "valueAsDouble";

    public double getValueAsDouble()
    {
        return UnispUtils.getWrappedLongValue(getValue());
    }

    public void setValueAsDouble(final double value)
    {
        setValue(UnispUtils.getUnwrappedDoubleValue(value));
    }

    public String getValueMoneyFormatted()
    {
        return MoneyFormatter.ruMoneyFormatter(3).format(getValue());
    }

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getType();
    }

}