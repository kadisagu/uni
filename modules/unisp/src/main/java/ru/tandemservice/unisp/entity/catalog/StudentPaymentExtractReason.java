package ru.tandemservice.unisp.entity.catalog;

import java.util.List;

import ru.tandemservice.unisp.dao.UnispDaoFacade;
import ru.tandemservice.unisp.entity.catalog.gen.StudentPaymentExtractReasonGen;

/**
 * Причины документов о назначении выплат
 */
public class StudentPaymentExtractReason extends StudentPaymentExtractReasonGen
{
    public static final String P_BASICS = "basics";
    
    @SuppressWarnings("unchecked")
    public List<StudentPaymentExtractBasic> getBasics()
    {
        return UnispDaoFacade.getUnispDAO().getReasonToBasicsList(this);
    }
}