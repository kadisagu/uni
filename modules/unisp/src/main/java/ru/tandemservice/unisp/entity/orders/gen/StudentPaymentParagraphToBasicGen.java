package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractBasic;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToBasic;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь параграфа с основанием
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentParagraphToBasicGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToBasic";
    public static final String ENTITY_NAME = "studentPaymentParagraphToBasic";
    public static final int VERSION_HASH = -912999781;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARAGRAPH = "paragraph";
    public static final String L_BASIC = "basic";
    public static final String P_COMMENT = "comment";

    private StudentPaymentParagraph _paragraph;     // Параграф
    private StudentPaymentExtractBasic _basic;     // Основание
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Параграф.
     */
    public StudentPaymentParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Параграф.
     */
    public void setParagraph(StudentPaymentParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    /**
     * @return Основание.
     */
    public StudentPaymentExtractBasic getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание.
     */
    public void setBasic(StudentPaymentExtractBasic basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentParagraphToBasicGen)
        {
            setParagraph(((StudentPaymentParagraphToBasic)another).getParagraph());
            setBasic(((StudentPaymentParagraphToBasic)another).getBasic());
            setComment(((StudentPaymentParagraphToBasic)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentParagraphToBasicGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentParagraphToBasic.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentParagraphToBasic();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "paragraph":
                    return obj.getParagraph();
                case "basic":
                    return obj.getBasic();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "paragraph":
                    obj.setParagraph((StudentPaymentParagraph) value);
                    return;
                case "basic":
                    obj.setBasic((StudentPaymentExtractBasic) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "paragraph":
                        return true;
                case "basic":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "paragraph":
                    return true;
                case "basic":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "paragraph":
                    return StudentPaymentParagraph.class;
                case "basic":
                    return StudentPaymentExtractBasic.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentParagraphToBasic> _dslPath = new Path<StudentPaymentParagraphToBasic>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentParagraphToBasic");
    }
            

    /**
     * @return Параграф.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToBasic#getParagraph()
     */
    public static StudentPaymentParagraph.Path<StudentPaymentParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    /**
     * @return Основание.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToBasic#getBasic()
     */
    public static StudentPaymentExtractBasic.Path<StudentPaymentExtractBasic> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToBasic#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends StudentPaymentParagraphToBasic> extends EntityPath<E>
    {
        private StudentPaymentParagraph.Path<StudentPaymentParagraph> _paragraph;
        private StudentPaymentExtractBasic.Path<StudentPaymentExtractBasic> _basic;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Параграф.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToBasic#getParagraph()
     */
        public StudentPaymentParagraph.Path<StudentPaymentParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new StudentPaymentParagraph.Path<StudentPaymentParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

    /**
     * @return Основание.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToBasic#getBasic()
     */
        public StudentPaymentExtractBasic.Path<StudentPaymentExtractBasic> basic()
        {
            if(_basic == null )
                _basic = new StudentPaymentExtractBasic.Path<StudentPaymentExtractBasic>(L_BASIC, this);
            return _basic;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToBasic#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(StudentPaymentParagraphToBasicGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return StudentPaymentParagraphToBasic.class;
        }

        public String getEntityName()
        {
            return "studentPaymentParagraphToBasic";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
