// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol;

/**
 * @author oleyba
 * @since 16.02.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSecModel(new CommonPostfixPermissionModel("studentPaymentOrder"));
        getDao().prepare(model);
        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<StudentPaymentOrderProtocol> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new IndicatorColumn("Иконка", null).defaultIndicator(new IndicatorColumn.Item("report", "Протокол")).setOrderable(false).setClickable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата формирования", StudentPaymentOrderProtocol.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new IndicatorColumn("Печать", null, "onClickPrintReport").defaultIndicator(new IndicatorColumn.Item("printer", "Печать")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey(model.getSecModel().getPermission("printProtocol")).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteReport", "Удалить протокол от «{0}»?", StudentPaymentOrderProtocol.P_FORMING_DATE).setPermissionKey(model.getSecModel().getPermission("deleteProtocol")));
        dataSource.setOrder(StudentPaymentOrderProtocol.P_FORMING_DATE, OrderDirection.desc);
        model.setDataSource(dataSource);
    }

    public void onClickAddReport(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolAdd"));
    }

    public void onClickSearch(IBusinessComponent component)
    {
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).setFormingDate(null);
        onClickSearch(component);
    }

    public void onClickPrintReport(IBusinessComponent component) throws Exception
    {
        activateInRoot(component, new ComponentActivator(IUniComponents.DOWNLOAD_STORABLE_REPORT, new ParametersMap()
                .add("reportId", component.getListenerParameter())
                .add("extension", "rtf")
                .add("zip", true)
        ));
    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        getDao().deleteRow(component);
    }
}