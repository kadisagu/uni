package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Абстрактная выписка из списочного приказа по выплатам студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentExtractGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentExtract";
    public static final String ENTITY_NAME = "studentPaymentExtract";
    public static final int VERSION_HASH = -1787761514;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_NUMBER = "number";
    public static final String P_COMMITTED = "committed";
    public static final String P_STUDENT_TITLE = "studentTitle";
    public static final String P_INDIVIDUAL_PARAGRAPH = "individualParagraph";
    public static final String L_ENTITY = "entity";
    public static final String L_STATE = "state";
    public static final String L_TYPE = "type";
    public static final String L_PARAGRAPH = "paragraph";
    public static final String P_STUDENT_TITLE_STR = "studentTitleStr";
    public static final String P_STUDENT_STATUS_STR = "studentStatusStr";
    public static final String P_STUDENT_CATEGORY_STR = "studentCategoryStr";
    public static final String P_PERSONAL_NUMBER_STR = "personalNumberStr";
    public static final String P_COURSE_STR = "courseStr";
    public static final String P_GROUP_STR = "groupStr";
    public static final String P_COMPENSATION_TYPE_STR = "compensationTypeStr";
    public static final String P_FORMATIVE_ORG_UNIT_STR = "formativeOrgUnitStr";
    public static final String P_TERRITORIAL_ORG_UNIT_STR = "territorialOrgUnitStr";
    public static final String P_EDUCATION_LEVEL_HIGH_SCHOOL_STR = "educationLevelHighSchoolStr";
    public static final String P_DEVELOP_FORM_STR = "developFormStr";
    public static final String P_DEVELOP_CONDITION_STR = "developConditionStr";
    public static final String P_DEVELOP_TECH_STR = "developTechStr";
    public static final String P_DEVELOP_PERIOD_STR = "developPeriodStr";

    private Date _createDate;     // Дата формирования
    private Integer _number;     // Номер выписки в параграфе
    private boolean _committed;     // Проведена
    private String _studentTitle;     // ФИО студента в корректном падеже
    private boolean _individualParagraph;     // При печати использовать шаблон выписки на одного студента
    private Student _entity;     // Студент
    private ExtractStates _state;     // Состояние выписки
    private StudentPaymentExtractType _type;     // Тип документа о назначении выплат
    private StudentPaymentParagraph _paragraph;     // Параграф приказа о назначении выплат студентам
    private String _studentTitleStr;     // ФИО студента в именительном падеже
    private String _studentStatusStr;     // Состояние
    private String _studentCategoryStr;     // Категория
    private String _personalNumberStr;     // Личный номер
    private String _courseStr;     // Курс
    private String _groupStr;     // Группа
    private String _compensationTypeStr;     // Вид возмещения затрат
    private String _formativeOrgUnitStr;     // Формирующее подр.
    private String _territorialOrgUnitStr;     // Территориальное подр.
    private String _educationLevelHighSchoolStr;     // Направление подготовки (специальность)
    private String _developFormStr;     // Форма освоения
    private String _developConditionStr;     // Условие освоения
    private String _developTechStr;     // Технология освоения
    private String _developPeriodStr;     // Срок освоения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Номер выписки в параграфе.
     */
    public Integer getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер выписки в параграфе.
     */
    public void setNumber(Integer number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Проведена. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommitted()
    {
        return _committed;
    }

    /**
     * @param committed Проведена. Свойство не может быть null.
     */
    public void setCommitted(boolean committed)
    {
        dirty(_committed, committed);
        _committed = committed;
    }

    /**
     * @return ФИО студента в корректном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentTitle()
    {
        return _studentTitle;
    }

    /**
     * @param studentTitle ФИО студента в корректном падеже. Свойство не может быть null.
     */
    public void setStudentTitle(String studentTitle)
    {
        dirty(_studentTitle, studentTitle);
        _studentTitle = studentTitle;
    }

    /**
     * @return При печати использовать шаблон выписки на одного студента. Свойство не может быть null.
     */
    @NotNull
    public boolean isIndividualParagraph()
    {
        return _individualParagraph;
    }

    /**
     * @param individualParagraph При печати использовать шаблон выписки на одного студента. Свойство не может быть null.
     */
    public void setIndividualParagraph(boolean individualParagraph)
    {
        dirty(_individualParagraph, individualParagraph);
        _individualParagraph = individualParagraph;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getEntity()
    {
        return _entity;
    }

    /**
     * @param entity Студент. Свойство не может быть null.
     */
    public void setEntity(Student entity)
    {
        dirty(_entity, entity);
        _entity = entity;
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     */
    @NotNull
    public ExtractStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние выписки. Свойство не может быть null.
     */
    public void setState(ExtractStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Тип документа о назначении выплат. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип документа о назначении выплат. Свойство не может быть null.
     */
    public void setType(StudentPaymentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Параграф приказа о назначении выплат студентам.
     */
    public StudentPaymentParagraph getParagraph()
    {
        return _paragraph;
    }

    /**
     * @param paragraph Параграф приказа о назначении выплат студентам.
     */
    public void setParagraph(StudentPaymentParagraph paragraph)
    {
        dirty(_paragraph, paragraph);
        _paragraph = paragraph;
    }

    /**
     * @return ФИО студента в именительном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    /**
     * @param studentTitleStr ФИО студента в именительном падеже. Свойство не может быть null.
     */
    public void setStudentTitleStr(String studentTitleStr)
    {
        dirty(_studentTitleStr, studentTitleStr);
        _studentTitleStr = studentTitleStr;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentStatusStr()
    {
        return _studentStatusStr;
    }

    /**
     * @param studentStatusStr Состояние. Свойство не может быть null.
     */
    public void setStudentStatusStr(String studentStatusStr)
    {
        dirty(_studentStatusStr, studentStatusStr);
        _studentStatusStr = studentStatusStr;
    }

    /**
     * @return Категория. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentCategoryStr()
    {
        return _studentCategoryStr;
    }

    /**
     * @param studentCategoryStr Категория. Свойство не может быть null.
     */
    public void setStudentCategoryStr(String studentCategoryStr)
    {
        dirty(_studentCategoryStr, studentCategoryStr);
        _studentCategoryStr = studentCategoryStr;
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPersonalNumberStr()
    {
        return _personalNumberStr;
    }

    /**
     * @param personalNumberStr Личный номер. Свойство не может быть null.
     */
    public void setPersonalNumberStr(String personalNumberStr)
    {
        dirty(_personalNumberStr, personalNumberStr);
        _personalNumberStr = personalNumberStr;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCourseStr()
    {
        return _courseStr;
    }

    /**
     * @param courseStr Курс. Свойство не может быть null.
     */
    public void setCourseStr(String courseStr)
    {
        dirty(_courseStr, courseStr);
        _courseStr = courseStr;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroupStr()
    {
        return _groupStr;
    }

    /**
     * @param groupStr Группа.
     */
    public void setGroupStr(String groupStr)
    {
        dirty(_groupStr, groupStr);
        _groupStr = groupStr;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCompensationTypeStr()
    {
        return _compensationTypeStr;
    }

    /**
     * @param compensationTypeStr Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationTypeStr(String compensationTypeStr)
    {
        dirty(_compensationTypeStr, compensationTypeStr);
        _compensationTypeStr = compensationTypeStr;
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getFormativeOrgUnitStr()
    {
        return _formativeOrgUnitStr;
    }

    /**
     * @param formativeOrgUnitStr Формирующее подр.. Свойство не может быть null.
     */
    public void setFormativeOrgUnitStr(String formativeOrgUnitStr)
    {
        dirty(_formativeOrgUnitStr, formativeOrgUnitStr);
        _formativeOrgUnitStr = formativeOrgUnitStr;
    }

    /**
     * @return Территориальное подр..
     */
    @Length(max=255)
    public String getTerritorialOrgUnitStr()
    {
        return _territorialOrgUnitStr;
    }

    /**
     * @param territorialOrgUnitStr Территориальное подр..
     */
    public void setTerritorialOrgUnitStr(String territorialOrgUnitStr)
    {
        dirty(_territorialOrgUnitStr, territorialOrgUnitStr);
        _territorialOrgUnitStr = territorialOrgUnitStr;
    }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationLevelHighSchoolStr()
    {
        return _educationLevelHighSchoolStr;
    }

    /**
     * @param educationLevelHighSchoolStr Направление подготовки (специальность). Свойство не может быть null.
     */
    public void setEducationLevelHighSchoolStr(String educationLevelHighSchoolStr)
    {
        dirty(_educationLevelHighSchoolStr, educationLevelHighSchoolStr);
        _educationLevelHighSchoolStr = educationLevelHighSchoolStr;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopFormStr()
    {
        return _developFormStr;
    }

    /**
     * @param developFormStr Форма освоения. Свойство не может быть null.
     */
    public void setDevelopFormStr(String developFormStr)
    {
        dirty(_developFormStr, developFormStr);
        _developFormStr = developFormStr;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopConditionStr()
    {
        return _developConditionStr;
    }

    /**
     * @param developConditionStr Условие освоения. Свойство не может быть null.
     */
    public void setDevelopConditionStr(String developConditionStr)
    {
        dirty(_developConditionStr, developConditionStr);
        _developConditionStr = developConditionStr;
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopTechStr()
    {
        return _developTechStr;
    }

    /**
     * @param developTechStr Технология освоения. Свойство не может быть null.
     */
    public void setDevelopTechStr(String developTechStr)
    {
        dirty(_developTechStr, developTechStr);
        _developTechStr = developTechStr;
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDevelopPeriodStr()
    {
        return _developPeriodStr;
    }

    /**
     * @param developPeriodStr Срок освоения. Свойство не может быть null.
     */
    public void setDevelopPeriodStr(String developPeriodStr)
    {
        dirty(_developPeriodStr, developPeriodStr);
        _developPeriodStr = developPeriodStr;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentExtractGen)
        {
            setCreateDate(((StudentPaymentExtract)another).getCreateDate());
            setNumber(((StudentPaymentExtract)another).getNumber());
            setCommitted(((StudentPaymentExtract)another).isCommitted());
            setStudentTitle(((StudentPaymentExtract)another).getStudentTitle());
            setIndividualParagraph(((StudentPaymentExtract)another).isIndividualParagraph());
            setEntity(((StudentPaymentExtract)another).getEntity());
            setState(((StudentPaymentExtract)another).getState());
            setType(((StudentPaymentExtract)another).getType());
            setParagraph(((StudentPaymentExtract)another).getParagraph());
            setStudentTitleStr(((StudentPaymentExtract)another).getStudentTitleStr());
            setStudentStatusStr(((StudentPaymentExtract)another).getStudentStatusStr());
            setStudentCategoryStr(((StudentPaymentExtract)another).getStudentCategoryStr());
            setPersonalNumberStr(((StudentPaymentExtract)another).getPersonalNumberStr());
            setCourseStr(((StudentPaymentExtract)another).getCourseStr());
            setGroupStr(((StudentPaymentExtract)another).getGroupStr());
            setCompensationTypeStr(((StudentPaymentExtract)another).getCompensationTypeStr());
            setFormativeOrgUnitStr(((StudentPaymentExtract)another).getFormativeOrgUnitStr());
            setTerritorialOrgUnitStr(((StudentPaymentExtract)another).getTerritorialOrgUnitStr());
            setEducationLevelHighSchoolStr(((StudentPaymentExtract)another).getEducationLevelHighSchoolStr());
            setDevelopFormStr(((StudentPaymentExtract)another).getDevelopFormStr());
            setDevelopConditionStr(((StudentPaymentExtract)another).getDevelopConditionStr());
            setDevelopTechStr(((StudentPaymentExtract)another).getDevelopTechStr());
            setDevelopPeriodStr(((StudentPaymentExtract)another).getDevelopPeriodStr());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentExtractGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentExtract.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("StudentPaymentExtract is abstract");
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "number":
                    return obj.getNumber();
                case "committed":
                    return obj.isCommitted();
                case "studentTitle":
                    return obj.getStudentTitle();
                case "individualParagraph":
                    return obj.isIndividualParagraph();
                case "entity":
                    return obj.getEntity();
                case "state":
                    return obj.getState();
                case "type":
                    return obj.getType();
                case "paragraph":
                    return obj.getParagraph();
                case "studentTitleStr":
                    return obj.getStudentTitleStr();
                case "studentStatusStr":
                    return obj.getStudentStatusStr();
                case "studentCategoryStr":
                    return obj.getStudentCategoryStr();
                case "personalNumberStr":
                    return obj.getPersonalNumberStr();
                case "courseStr":
                    return obj.getCourseStr();
                case "groupStr":
                    return obj.getGroupStr();
                case "compensationTypeStr":
                    return obj.getCompensationTypeStr();
                case "formativeOrgUnitStr":
                    return obj.getFormativeOrgUnitStr();
                case "territorialOrgUnitStr":
                    return obj.getTerritorialOrgUnitStr();
                case "educationLevelHighSchoolStr":
                    return obj.getEducationLevelHighSchoolStr();
                case "developFormStr":
                    return obj.getDevelopFormStr();
                case "developConditionStr":
                    return obj.getDevelopConditionStr();
                case "developTechStr":
                    return obj.getDevelopTechStr();
                case "developPeriodStr":
                    return obj.getDevelopPeriodStr();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "committed":
                    obj.setCommitted((Boolean) value);
                    return;
                case "studentTitle":
                    obj.setStudentTitle((String) value);
                    return;
                case "individualParagraph":
                    obj.setIndividualParagraph((Boolean) value);
                    return;
                case "entity":
                    obj.setEntity((Student) value);
                    return;
                case "state":
                    obj.setState((ExtractStates) value);
                    return;
                case "type":
                    obj.setType((StudentPaymentExtractType) value);
                    return;
                case "paragraph":
                    obj.setParagraph((StudentPaymentParagraph) value);
                    return;
                case "studentTitleStr":
                    obj.setStudentTitleStr((String) value);
                    return;
                case "studentStatusStr":
                    obj.setStudentStatusStr((String) value);
                    return;
                case "studentCategoryStr":
                    obj.setStudentCategoryStr((String) value);
                    return;
                case "personalNumberStr":
                    obj.setPersonalNumberStr((String) value);
                    return;
                case "courseStr":
                    obj.setCourseStr((String) value);
                    return;
                case "groupStr":
                    obj.setGroupStr((String) value);
                    return;
                case "compensationTypeStr":
                    obj.setCompensationTypeStr((String) value);
                    return;
                case "formativeOrgUnitStr":
                    obj.setFormativeOrgUnitStr((String) value);
                    return;
                case "territorialOrgUnitStr":
                    obj.setTerritorialOrgUnitStr((String) value);
                    return;
                case "educationLevelHighSchoolStr":
                    obj.setEducationLevelHighSchoolStr((String) value);
                    return;
                case "developFormStr":
                    obj.setDevelopFormStr((String) value);
                    return;
                case "developConditionStr":
                    obj.setDevelopConditionStr((String) value);
                    return;
                case "developTechStr":
                    obj.setDevelopTechStr((String) value);
                    return;
                case "developPeriodStr":
                    obj.setDevelopPeriodStr((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "number":
                        return true;
                case "committed":
                        return true;
                case "studentTitle":
                        return true;
                case "individualParagraph":
                        return true;
                case "entity":
                        return true;
                case "state":
                        return true;
                case "type":
                        return true;
                case "paragraph":
                        return true;
                case "studentTitleStr":
                        return true;
                case "studentStatusStr":
                        return true;
                case "studentCategoryStr":
                        return true;
                case "personalNumberStr":
                        return true;
                case "courseStr":
                        return true;
                case "groupStr":
                        return true;
                case "compensationTypeStr":
                        return true;
                case "formativeOrgUnitStr":
                        return true;
                case "territorialOrgUnitStr":
                        return true;
                case "educationLevelHighSchoolStr":
                        return true;
                case "developFormStr":
                        return true;
                case "developConditionStr":
                        return true;
                case "developTechStr":
                        return true;
                case "developPeriodStr":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "number":
                    return true;
                case "committed":
                    return true;
                case "studentTitle":
                    return true;
                case "individualParagraph":
                    return true;
                case "entity":
                    return true;
                case "state":
                    return true;
                case "type":
                    return true;
                case "paragraph":
                    return true;
                case "studentTitleStr":
                    return true;
                case "studentStatusStr":
                    return true;
                case "studentCategoryStr":
                    return true;
                case "personalNumberStr":
                    return true;
                case "courseStr":
                    return true;
                case "groupStr":
                    return true;
                case "compensationTypeStr":
                    return true;
                case "formativeOrgUnitStr":
                    return true;
                case "territorialOrgUnitStr":
                    return true;
                case "educationLevelHighSchoolStr":
                    return true;
                case "developFormStr":
                    return true;
                case "developConditionStr":
                    return true;
                case "developTechStr":
                    return true;
                case "developPeriodStr":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "number":
                    return Integer.class;
                case "committed":
                    return Boolean.class;
                case "studentTitle":
                    return String.class;
                case "individualParagraph":
                    return Boolean.class;
                case "entity":
                    return Student.class;
                case "state":
                    return ExtractStates.class;
                case "type":
                    return StudentPaymentExtractType.class;
                case "paragraph":
                    return StudentPaymentParagraph.class;
                case "studentTitleStr":
                    return String.class;
                case "studentStatusStr":
                    return String.class;
                case "studentCategoryStr":
                    return String.class;
                case "personalNumberStr":
                    return String.class;
                case "courseStr":
                    return String.class;
                case "groupStr":
                    return String.class;
                case "compensationTypeStr":
                    return String.class;
                case "formativeOrgUnitStr":
                    return String.class;
                case "territorialOrgUnitStr":
                    return String.class;
                case "educationLevelHighSchoolStr":
                    return String.class;
                case "developFormStr":
                    return String.class;
                case "developConditionStr":
                    return String.class;
                case "developTechStr":
                    return String.class;
                case "developPeriodStr":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentExtract> _dslPath = new Path<StudentPaymentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentExtract");
    }
            

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#isCommitted()
     */
    public static PropertyPath<Boolean> committed()
    {
        return _dslPath.committed();
    }

    /**
     * @return ФИО студента в корректном падеже. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getStudentTitle()
     */
    public static PropertyPath<String> studentTitle()
    {
        return _dslPath.studentTitle();
    }

    /**
     * @return При печати использовать шаблон выписки на одного студента. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#isIndividualParagraph()
     */
    public static PropertyPath<Boolean> individualParagraph()
    {
        return _dslPath.individualParagraph();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getEntity()
     */
    public static Student.Path<Student> entity()
    {
        return _dslPath.entity();
    }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getState()
     */
    public static ExtractStates.Path<ExtractStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Тип документа о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getType()
     */
    public static StudentPaymentExtractType.Path<StudentPaymentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Параграф приказа о назначении выплат студентам.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getParagraph()
     */
    public static StudentPaymentParagraph.Path<StudentPaymentParagraph> paragraph()
    {
        return _dslPath.paragraph();
    }

    /**
     * @return ФИО студента в именительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getStudentTitleStr()
     */
    public static PropertyPath<String> studentTitleStr()
    {
        return _dslPath.studentTitleStr();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getStudentStatusStr()
     */
    public static PropertyPath<String> studentStatusStr()
    {
        return _dslPath.studentStatusStr();
    }

    /**
     * @return Категория. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getStudentCategoryStr()
     */
    public static PropertyPath<String> studentCategoryStr()
    {
        return _dslPath.studentCategoryStr();
    }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getPersonalNumberStr()
     */
    public static PropertyPath<String> personalNumberStr()
    {
        return _dslPath.personalNumberStr();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getCourseStr()
     */
    public static PropertyPath<String> courseStr()
    {
        return _dslPath.courseStr();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getGroupStr()
     */
    public static PropertyPath<String> groupStr()
    {
        return _dslPath.groupStr();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getCompensationTypeStr()
     */
    public static PropertyPath<String> compensationTypeStr()
    {
        return _dslPath.compensationTypeStr();
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getFormativeOrgUnitStr()
     */
    public static PropertyPath<String> formativeOrgUnitStr()
    {
        return _dslPath.formativeOrgUnitStr();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getTerritorialOrgUnitStr()
     */
    public static PropertyPath<String> territorialOrgUnitStr()
    {
        return _dslPath.territorialOrgUnitStr();
    }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getEducationLevelHighSchoolStr()
     */
    public static PropertyPath<String> educationLevelHighSchoolStr()
    {
        return _dslPath.educationLevelHighSchoolStr();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getDevelopFormStr()
     */
    public static PropertyPath<String> developFormStr()
    {
        return _dslPath.developFormStr();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getDevelopConditionStr()
     */
    public static PropertyPath<String> developConditionStr()
    {
        return _dslPath.developConditionStr();
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getDevelopTechStr()
     */
    public static PropertyPath<String> developTechStr()
    {
        return _dslPath.developTechStr();
    }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getDevelopPeriodStr()
     */
    public static PropertyPath<String> developPeriodStr()
    {
        return _dslPath.developPeriodStr();
    }

    public static class Path<E extends StudentPaymentExtract> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<Integer> _number;
        private PropertyPath<Boolean> _committed;
        private PropertyPath<String> _studentTitle;
        private PropertyPath<Boolean> _individualParagraph;
        private Student.Path<Student> _entity;
        private ExtractStates.Path<ExtractStates> _state;
        private StudentPaymentExtractType.Path<StudentPaymentExtractType> _type;
        private StudentPaymentParagraph.Path<StudentPaymentParagraph> _paragraph;
        private PropertyPath<String> _studentTitleStr;
        private PropertyPath<String> _studentStatusStr;
        private PropertyPath<String> _studentCategoryStr;
        private PropertyPath<String> _personalNumberStr;
        private PropertyPath<String> _courseStr;
        private PropertyPath<String> _groupStr;
        private PropertyPath<String> _compensationTypeStr;
        private PropertyPath<String> _formativeOrgUnitStr;
        private PropertyPath<String> _territorialOrgUnitStr;
        private PropertyPath<String> _educationLevelHighSchoolStr;
        private PropertyPath<String> _developFormStr;
        private PropertyPath<String> _developConditionStr;
        private PropertyPath<String> _developTechStr;
        private PropertyPath<String> _developPeriodStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(StudentPaymentExtractGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Номер выписки в параграфе.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(StudentPaymentExtractGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Проведена. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#isCommitted()
     */
        public PropertyPath<Boolean> committed()
        {
            if(_committed == null )
                _committed = new PropertyPath<Boolean>(StudentPaymentExtractGen.P_COMMITTED, this);
            return _committed;
        }

    /**
     * @return ФИО студента в корректном падеже. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getStudentTitle()
     */
        public PropertyPath<String> studentTitle()
        {
            if(_studentTitle == null )
                _studentTitle = new PropertyPath<String>(StudentPaymentExtractGen.P_STUDENT_TITLE, this);
            return _studentTitle;
        }

    /**
     * @return При печати использовать шаблон выписки на одного студента. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#isIndividualParagraph()
     */
        public PropertyPath<Boolean> individualParagraph()
        {
            if(_individualParagraph == null )
                _individualParagraph = new PropertyPath<Boolean>(StudentPaymentExtractGen.P_INDIVIDUAL_PARAGRAPH, this);
            return _individualParagraph;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getEntity()
     */
        public Student.Path<Student> entity()
        {
            if(_entity == null )
                _entity = new Student.Path<Student>(L_ENTITY, this);
            return _entity;
        }

    /**
     * @return Состояние выписки. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getState()
     */
        public ExtractStates.Path<ExtractStates> state()
        {
            if(_state == null )
                _state = new ExtractStates.Path<ExtractStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Тип документа о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getType()
     */
        public StudentPaymentExtractType.Path<StudentPaymentExtractType> type()
        {
            if(_type == null )
                _type = new StudentPaymentExtractType.Path<StudentPaymentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Параграф приказа о назначении выплат студентам.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getParagraph()
     */
        public StudentPaymentParagraph.Path<StudentPaymentParagraph> paragraph()
        {
            if(_paragraph == null )
                _paragraph = new StudentPaymentParagraph.Path<StudentPaymentParagraph>(L_PARAGRAPH, this);
            return _paragraph;
        }

    /**
     * @return ФИО студента в именительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getStudentTitleStr()
     */
        public PropertyPath<String> studentTitleStr()
        {
            if(_studentTitleStr == null )
                _studentTitleStr = new PropertyPath<String>(StudentPaymentExtractGen.P_STUDENT_TITLE_STR, this);
            return _studentTitleStr;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getStudentStatusStr()
     */
        public PropertyPath<String> studentStatusStr()
        {
            if(_studentStatusStr == null )
                _studentStatusStr = new PropertyPath<String>(StudentPaymentExtractGen.P_STUDENT_STATUS_STR, this);
            return _studentStatusStr;
        }

    /**
     * @return Категория. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getStudentCategoryStr()
     */
        public PropertyPath<String> studentCategoryStr()
        {
            if(_studentCategoryStr == null )
                _studentCategoryStr = new PropertyPath<String>(StudentPaymentExtractGen.P_STUDENT_CATEGORY_STR, this);
            return _studentCategoryStr;
        }

    /**
     * @return Личный номер. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getPersonalNumberStr()
     */
        public PropertyPath<String> personalNumberStr()
        {
            if(_personalNumberStr == null )
                _personalNumberStr = new PropertyPath<String>(StudentPaymentExtractGen.P_PERSONAL_NUMBER_STR, this);
            return _personalNumberStr;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getCourseStr()
     */
        public PropertyPath<String> courseStr()
        {
            if(_courseStr == null )
                _courseStr = new PropertyPath<String>(StudentPaymentExtractGen.P_COURSE_STR, this);
            return _courseStr;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getGroupStr()
     */
        public PropertyPath<String> groupStr()
        {
            if(_groupStr == null )
                _groupStr = new PropertyPath<String>(StudentPaymentExtractGen.P_GROUP_STR, this);
            return _groupStr;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getCompensationTypeStr()
     */
        public PropertyPath<String> compensationTypeStr()
        {
            if(_compensationTypeStr == null )
                _compensationTypeStr = new PropertyPath<String>(StudentPaymentExtractGen.P_COMPENSATION_TYPE_STR, this);
            return _compensationTypeStr;
        }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getFormativeOrgUnitStr()
     */
        public PropertyPath<String> formativeOrgUnitStr()
        {
            if(_formativeOrgUnitStr == null )
                _formativeOrgUnitStr = new PropertyPath<String>(StudentPaymentExtractGen.P_FORMATIVE_ORG_UNIT_STR, this);
            return _formativeOrgUnitStr;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getTerritorialOrgUnitStr()
     */
        public PropertyPath<String> territorialOrgUnitStr()
        {
            if(_territorialOrgUnitStr == null )
                _territorialOrgUnitStr = new PropertyPath<String>(StudentPaymentExtractGen.P_TERRITORIAL_ORG_UNIT_STR, this);
            return _territorialOrgUnitStr;
        }

    /**
     * @return Направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getEducationLevelHighSchoolStr()
     */
        public PropertyPath<String> educationLevelHighSchoolStr()
        {
            if(_educationLevelHighSchoolStr == null )
                _educationLevelHighSchoolStr = new PropertyPath<String>(StudentPaymentExtractGen.P_EDUCATION_LEVEL_HIGH_SCHOOL_STR, this);
            return _educationLevelHighSchoolStr;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getDevelopFormStr()
     */
        public PropertyPath<String> developFormStr()
        {
            if(_developFormStr == null )
                _developFormStr = new PropertyPath<String>(StudentPaymentExtractGen.P_DEVELOP_FORM_STR, this);
            return _developFormStr;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getDevelopConditionStr()
     */
        public PropertyPath<String> developConditionStr()
        {
            if(_developConditionStr == null )
                _developConditionStr = new PropertyPath<String>(StudentPaymentExtractGen.P_DEVELOP_CONDITION_STR, this);
            return _developConditionStr;
        }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getDevelopTechStr()
     */
        public PropertyPath<String> developTechStr()
        {
            if(_developTechStr == null )
                _developTechStr = new PropertyPath<String>(StudentPaymentExtractGen.P_DEVELOP_TECH_STR, this);
            return _developTechStr;
        }

    /**
     * @return Срок освоения. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtract#getDevelopPeriodStr()
     */
        public PropertyPath<String> developPeriodStr()
        {
            if(_developPeriodStr == null )
                _developPeriodStr = new PropertyPath<String>(StudentPaymentExtractGen.P_DEVELOP_PERIOD_STR, this);
            return _developPeriodStr;
        }

        public Class getEntityClass()
        {
            return StudentPaymentExtract.class;
        }

        public String getEntityName()
        {
            return "studentPaymentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
