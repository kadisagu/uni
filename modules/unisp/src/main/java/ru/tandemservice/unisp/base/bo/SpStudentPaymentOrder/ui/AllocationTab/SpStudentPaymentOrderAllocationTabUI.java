/**
 * $Id$
 */
package ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.ui.AllocationTab;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.SpStudentPaymentOrderManager;
import ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.logic.allocationTab.StudentPaymentOrderAllocationSearchListDSHandler;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Alexander Shaburov
 * @since 14.02.13
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "order.id")
public class SpStudentPaymentOrderAllocationTabUI extends AbstractUniStudentListUI
{
    public static final String PROP_ORDER = "order";
    public static final String PROP_GROUP = "group";
    public static final String PROP_SP_HAS_NO_RELATION = "spHasNoRelation";
    public static final String PROP_DEBT_STATUS = "debtStatus";
    public static final String PROP_MARKS_IN_SESSION = "marksInSession";
    public static final String PROP_PERFORMANCE_STATUS = "performanceStatus";
    public static final String PROP_LAST_SESSION_STATUS = "lastSessionStatus";
    public static final String PROP_REG_STRUCTURE_LIST = "regStructureList";
    public static final String PROP_WORK_PLAN_ROW_KIND_LIST = "workPlanRowKindList";

    private CommonPostfixPermissionModel _secModel;

    private StudentPaymentsOrder _order = new StudentPaymentsOrder();

    @Override
    public void onComponentRefresh()
    {
        final ICommonDAO dao = DataAccessServices.dao();
        _order = dao.get(StudentPaymentsOrder.class, _order.getId());
        _secModel = new CommonPostfixPermissionModel(getSecPostfix());
        super.onComponentRefresh();

        UniEduProgramEducationOrgUnitAddon util = (UniEduProgramEducationOrgUnitAddon) getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
        if (util != null)
        {
            util.configWhere(EducationOrgUnit.formativeOrgUnit(), _order.getOrgUnit(), true);
            util.configWhere(EducationOrgUnit.territorialOrgUnit(), _order.getOrgUnit(), true);
            util.configUseFilters(
                    UniEduProgramEducationOrgUnitAddon.Filters.FORMATIVE_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.TERRITORIAL_ORG_UNIT,
                    UniEduProgramEducationOrgUnitAddon.Filters.EDUCATION_LEVEL_HIGH_SCHOOL,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_CONDITION,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_FORM,
                    UniEduProgramEducationOrgUnitAddon.Filters.DEVELOP_PERIOD);
        }
        Collection<EppWorkPlanRowKind> workPlanRowKindList = getSettings().get(PROP_WORK_PLAN_ROW_KIND_LIST);
        if (workPlanRowKindList == null)
        {
            workPlanRowKindList = new ArrayList<>(EppWorkPlanRowKindCodes.CODES.size());
            workPlanRowKindList.add(dao.getNotNull(EppWorkPlanRowKind.class, EppWorkPlanRowKind.code(), EppWorkPlanRowKindCodes.MAIN));
            workPlanRowKindList.add(dao.getNotNull(EppWorkPlanRowKind.class, EppWorkPlanRowKind.code(), EppWorkPlanRowKindCodes.SELECTED));
            getSettings().set(PROP_WORK_PLAN_ROW_KIND_LIST, workPlanRowKindList);
        }
    }

    public void onClickAddSelectedStudentExtracts()
    {

        ErrorCollector errors = UserContext.getInstance().getErrorCollector();

        final Collection<Long> ids = UniBaseDao.ids(getConfig().<BaseSearchListDataSource>getDataSource(SpStudentPaymentOrderAllocationTab.STUDENT_SEARCH_LIST_DS).getOptionColumnSelectedObjects(SpStudentPaymentOrderAllocationTab.CHECKBOX_COLUMN));
        if (ids.isEmpty())
        {
            errors.add(getConfig().getProperty("studentSearchListDS.no-checked-error"));
            return;
        }
        getActivationBuilder().asRegion(ru.tandemservice.unisp.component.order.StudentPaymentOrderAllocationDialog.Model.class.getPackage().getName())
                .parameters(new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, _order.getId()).add("studentIds", ids))
                .activate();
    }

    public void onClickEditStudentPayments()
    {

    }

    public void onClickDeleteStudentPayments()
    {
        SpStudentPaymentOrderManager.instance().allocationDao().deleteStudentPayments(_order, getListenerParameterAsLong());
    }

    public boolean isEditDisable()
    {
        final DataWrapper wrapper = getConfig().getDataSource(SpStudentPaymentOrderAllocationTab.STUDENT_SEARCH_LIST_DS).getCurrent();
        return wrapper.<Student>getWrapped().isArchival() || Boolean.TRUE.equals(wrapper.getProperty(StudentPaymentOrderAllocationSearchListDSHandler.EMPTY_COLUMN));
    }

    public boolean isDeleteDisable()
    {
        final DataWrapper wrapper = getConfig().getDataSource(SpStudentPaymentOrderAllocationTab.STUDENT_SEARCH_LIST_DS).getCurrent();
        return Boolean.TRUE.equals(wrapper.getProperty(StudentPaymentOrderAllocationSearchListDSHandler.EMPTY_COLUMN));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);

        dataSource.put(PROP_ORDER, _order);
        dataSource.put(PROP_GROUP, getSettings().get(PROP_GROUP));
        dataSource.put(PROP_SP_HAS_NO_RELATION, getSettings().get(PROP_SP_HAS_NO_RELATION));
        dataSource.put(PROP_DEBT_STATUS, getSettings().get(PROP_DEBT_STATUS));
        dataSource.put(PROP_MARKS_IN_SESSION, getSettings().get(PROP_MARKS_IN_SESSION));
        dataSource.put(PROP_PERFORMANCE_STATUS, getSettings().get(PROP_PERFORMANCE_STATUS));
        dataSource.put(PROP_LAST_SESSION_STATUS, getSettings().get(PROP_LAST_SESSION_STATUS));
        dataSource.put(PROP_REG_STRUCTURE_LIST, getSettings().get(PROP_REG_STRUCTURE_LIST));
        dataSource.put(PROP_WORK_PLAN_ROW_KIND_LIST, getSettings().get(PROP_WORK_PLAN_ROW_KIND_LIST));
    }

    @Override
    public String getSettingsKey()
    {
        return "OrderAllignmentList." +
                String.valueOf(null == _order ? null : _order.getOrgUnit().getId()) +
                "." + _order.getType().getCode() + ".filter";
    }

    // Getters & Setters
    public String getSecPostfix()
    {
        return "studentPaymentOrder";
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public StudentPaymentsOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentPaymentsOrder order)
    {
        _order = order;
    }
}
