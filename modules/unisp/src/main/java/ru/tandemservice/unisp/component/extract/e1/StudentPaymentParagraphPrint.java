/**
 * $Id$
 */
package ru.tandemservice.unisp.component.extract.e1;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unisp.component.extract.IStudentPaymentParagraphPrint;
import ru.tandemservice.unisp.component.order.CommonStudentPaymentOrderPrint;
import ru.tandemservice.unisp.component.order.VirtualParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraphToReason;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint.GRAMMA_CASE_POSTFIX;

/**
 * @author dseleznev
 * Created on: 13.01.2010
 */
public class StudentPaymentParagraphPrint implements IStudentPaymentParagraphPrint<StudentPaymentAppointmentExtract>
{
    @Override
    public RtfInjectModifier createParagraphInjectModifier(VirtualParagraph<StudentPaymentAppointmentExtract> paragraph, int paragraphNumber)
    {
        StudentPaymentAppointmentExtract extract = (StudentPaymentAppointmentExtract) paragraph.getFirstExtract();
        StudentPaymentsOrder order = extract.getParagraph().getOrder();
        OrgUnit orgUnit = order.getOrgUnit();
        Group grp = paragraph.getFirstExtract().getEntity().getGroup();
        StudentPaymentParagraphToReason rel = UniDaoFacade.getCoreDao().get(StudentPaymentParagraphToReason.class, StudentPaymentParagraphToReason.paragraph().s(), extract.getParagraph());
        EduProgramSubject programSubject = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();
        RtfInjectModifier injectModifier = new RtfInjectModifier()
            .put("orderNumber", order.getNumber())
            .put("commitDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()))
            .put("group", grp != null ? grp.getTitle() : "без группы")
            .put("course", extract.getEntity().getCourse().getTitle())
            .put("paymentBeginDate", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getPaymentStartDate()))
            .put("paymentEndDate", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getPaymentStopDate()))
            .put("formativeOrgUnit", null != orgUnit ? (null != orgUnit.getNominativeCaseTitle() ? orgUnit.getNominativeCaseTitle() : orgUnit.getFullTitle()) : "")
            .put("paragraphNumber", String.valueOf(paragraphNumber))
            .put("paymentReasonPrintTitle", rel.getReason().getPrintTitle()!=null?rel.getReason().getPrintTitle():rel.getReason().getTitle())
            .put("paymentPrintTitle", extract.getPaymentType().getAccusativeCaseTitle()!=null?extract.getPaymentType().getAccusativeCaseTitle():extract.getPaymentType().getTitle());

        CommonExtractPrint.initOrgUnit(injectModifier, orgUnit, "formativeOrgUnit", "");
        injectModifier.put("programSubjectTitleWithCode", programSubject != null ? programSubject.getTitleWithCode() : "");


        //Старый вариант меток
        CommonExtractPrint.initEduProgramKind("eduProgramKind_P1", injectModifier, programSubject != null? programSubject.getEduProgramKind(): null);
        //Новый вариант меток
        CommonExtractPrint.initEduProgramKind("eduProgramKind", injectModifier, programSubject != null? programSubject.getEduProgramKind():null);
        //Старый вариант меток
        initEduLevel("eduLevel_P1", injectModifier, programSubject == null? null : programSubject.getEduProgramKind().getEduLevel());
        //Новый вариант меток
        initEduLevel("eduLevel", injectModifier, programSubject == null? null : programSubject.getEduProgramKind().getEduLevel());

        CommonStudentPaymentOrderPrint.initDevelopForm(injectModifier, extract.getEntity().getGroup().getEducationOrgUnit().getDevelopForm(), "");
        return injectModifier;
    }

    private static void initEduLevel(String label, RtfInjectModifier modifier, EduLevel eduLevel)
    {
        modifier.put(label, eduLevel==null? "" : eduLevel.getTitle().toLowerCase());
        Map<InflectorVariant, String> declinableMap = DeclinableManager.instance().dao().getPropertyValuesMap(eduLevel, EduProgramKind.P_TITLE);
        for(Map.Entry<GrammaCase, String> entry :GRAMMA_CASE_POSTFIX.entrySet())
        {
            String value = declinableMap.get(DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(entry.getKey()));
            modifier.put(label+"_" + entry.getValue(), value == null?  eduLevel==null? "" : eduLevel.getTitle().toLowerCase() : value.toLowerCase());
        }
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(VirtualParagraph<StudentPaymentAppointmentExtract> paragraph)
    {
        DoubleFormatter formatter = new DoubleFormatter(2, true, true);
        RtfTableModifier tableModifier = new RtfTableModifier();

        int cnt = 1;
        double sumPayments = 0d;
        List<String[]> paragraphDataLines = new ArrayList<String[]>();
        for(StudentPaymentAppointmentExtract extract : paragraph.getExtractsList())
        {
            sumPayments += extract.getPaymentCostAsDouble();
            paragraphDataLines.add(new String[] {String.valueOf(cnt++), extract.getEntity().getPerson().getFullFio(), formatter.format(extract.getPaymentCostAsDouble())});
        }

        OrgUnit orgUnit = paragraph.getFirstExtract().getParagraph().getOrder().getOrgUnit();

        if(null != orgUnit && null != orgUnit.getHead())
        {
            EmployeePost post = (EmployeePost) orgUnit.getHead();
            StringBuilder builder = new StringBuilder(OrgUnitTypeCodes.FACULTY.equals(orgUnit.getOrgUnitType().getCode()) ? "Декан " : "Директор ");
            builder.append(null != orgUnit.getGenitiveCaseTitle() ? orgUnit.getGenitiveCaseTitle() : orgUnit.getShortTitle());
            tableModifier.put("OU_LEADER_VISA", new String[][]{{builder.toString(), post.getPerson().getIdentityCard().getIof()}});
        }
        else
        {
            tableModifier.put("OU_LEADER_VISA", new String[][]{{"", ""}});
        }

        tableModifier.put("T", paragraphDataLines.toArray(new String[][]{}));
        tableModifier.put("S", new String[][] {{"", "Итого по группе:", formatter.format(sumPayments)}});
        return tableModifier;
    }

    @Override
    public String createParagraphText(VirtualParagraph<StudentPaymentAppointmentExtract> paragraph)
    {
        // TODO Auto-generated method stub
        return null;
    }
}