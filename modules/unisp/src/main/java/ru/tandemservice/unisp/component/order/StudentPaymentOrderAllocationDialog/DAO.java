/* $Id: DAO.java 10975 2009-12-24 11:28:03Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderAllocationDialog;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.GlobalFinancingSource;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.unisp.dao.IUnispExtractDAO;
import ru.tandemservice.unisp.dao.IUnispExtractDAO.IStudentPaymentDescription;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractBasic;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.catalog.gen.StudentPaymentGen;
import ru.tandemservice.unisp.entity.orders.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setOrder(getNotNull(StudentPaymentsOrder.class, model.getOrder().getId()));

        model.getStudentPaymentExtractTypeModel().setSource(new UniQueryFullCheckSelectModel() {
            @Override protected MQBuilder query(final String alias, final String filter) {
                final MQBuilder builder = new MQBuilder(StudentPaymentExtractType.ENTITY_CLASS, alias);
                builder.add(MQExpression.eq(alias, StudentPaymentExtractType.L_PARENT, model.getOrder().getType()));
                builder.addOrder(alias, StudentPaymentExtractType.P_CODE);
                return builder;
            }
        });

        model.getStudentPaymentModel().setSource(new UniQueryFullCheckSelectModel() {
            @Override protected MQBuilder query(final String alias, final String filter) {
                final StudentPaymentExtractType extractType = model.getStudentPaymentExtractTypeModel().getValue();
                if (null == extractType) { return null; }

                final MQBuilder idsBuilder = new MQBuilder(StudentPaymentToExtractTypeRel.ENTITY_CLASS, alias, new String[] {StudentPaymentToExtractTypeRel.L_SECOND + ".id"});
                idsBuilder.add(MQExpression.eq(alias, StudentPaymentToExtractTypeRel.L_FIRST, extractType));

                final MQBuilder builder = new MQBuilder(StudentPayment.ENTITY_CLASS, alias);
                builder.add(MQExpression.in(alias, "id", idsBuilder));
                builder.addOrder(alias, StudentPaymentGen.P_TITLE);
                return builder;
            }
        });

        model.getReasonModel().setSource(new UniQueryFullCheckSelectModel() {
            @Override protected MQBuilder query(final String alias, final String filter) {
                final StudentPaymentExtractType extractType = model.getStudentPaymentExtractTypeModel().getValue();
                if (null == extractType) { return null; }

                final MQBuilder idsBuilder = new MQBuilder(StudentPaymentReasonToExtractTypeRel.ENTITY_CLASS, alias, new String[] {StudentPaymentReasonToExtractTypeRel.L_SECOND + ".id"});
                idsBuilder.add(MQExpression.eq(alias, StudentPaymentReasonToExtractTypeRel.L_FIRST, extractType));

                final MQBuilder builder = new MQBuilder(StudentPaymentExtractReason.ENTITY_CLASS, alias);
                builder.add(MQExpression.in(alias, "id", idsBuilder));
                builder.addOrder(alias, StudentPaymentExtractReason.P_TITLE);
                return builder;
            }
        });

        model.getBasicModel().setSource(new UniQueryFullCheckSelectModel() {
            @Override protected MQBuilder query(final String alias, final String filter) {
                final StudentPaymentExtractReason reason = model.getReasonModel().getValue();
                if (null == reason) { return null; }

                final MQBuilder idsBuilder = new MQBuilder(StudentPaymentReasonToBasicsRel.ENTITY_CLASS, alias, new String[] {StudentPaymentReasonToBasicsRel.L_SECOND + ".id"});
                idsBuilder.add(MQExpression.eq(alias, StudentPaymentReasonToBasicsRel.L_FIRST, reason));

                final MQBuilder builder = new MQBuilder(StudentPaymentExtractBasic.ENTITY_CLASS, alias);
                builder.add(MQExpression.in(alias, "id", idsBuilder));
                builder.addOrder(alias, StudentPaymentExtractBasic.P_TITLE);
                return builder;
            }
        });

        model.getFinancingSourceModel().setSource(new StaticSelectModel("id", "title", getCatalogItemList(GlobalFinancingSource.class)));
    }

    @Override
    public void prepareListDataSource(final Model model)
    {
        final SimpleListDataSource<ViewWrapper<Student>> ds = model.getDataSource();
        final List<Student> studentList = new ArrayList<>(model.getStudentIds().size());
        BatchUtils.execute(model.getStudentIds(), 100, ids -> {
            final MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
            builder.add(MQExpression.in("s", "id", ids));
            studentList.addAll(builder.<Student>getResultList(getSession()));
        });

        final StudentPaymentExtractType extractType = model.getStudentPaymentExtractTypeModel().getValue();
        final GlobalFinancingSource financingSource = model.getFinancingSourceModel().getValue();
        final StudentPayment payment = model.getStudentPaymentModel().getValue();

        final List<ViewWrapper<Student>> studentWrapperList = ViewWrapper.getPatchedList(studentList);
        for (final ViewWrapper<Student> s: studentWrapperList) {
            final StudentPaymentAppointmentExtract appointment = new StudentPaymentAppointmentExtract();
            appointment.setup(s.getEntity(), null);
            appointment.setType(extractType);
            appointment.setPaymentType(payment);
            appointment.setFinancingSource(financingSource);
            appointment.setPaymentCostAsDouble(model.getPaymentCost());
            appointment.setPaymentStartDate(model.getPaymentStartDate());
            appointment.setPaymentStopDate(model.getPaymentStopDate());
            s.setViewProperty(Model.EXTRACT_COLUMN, appointment);
        }

        ds.setEntityCollection(studentWrapperList);
        refreshListDataSource(model);
    }

    @Override

    public void refreshListDataSource(final Model model)
    {

        final List<ViewWrapper<Student>> studentWrapperList = model.getDataSource().getEntityList();
        final ArrayList<StudentPaymentAppointmentExtract> extractsList = extracts(studentWrapperList);

        /* intersections */ {
            final Map<Long, Collection<IStudentPaymentDescription>> intersections = IUnispExtractDAO.instance.get().findIntersections(extractsList);
            for (final ViewWrapper<Student> s: studentWrapperList) {
                final Collection<IStudentPaymentDescription> intersection = intersections.get(s.getId() /* поскольку в списке extractsList идентификаторы пустые, то адресация будет идти по студентам */);
                final StringBuilder sb = new StringBuilder();
                for (final IUnispExtractDAO.IStudentPaymentDescription e: intersection) {
                    if (sb.length() > 0) { sb.append("\n"); }
                    sb.append(e.getTitle());
                }
                s.setViewProperty(Model.INTERSECTION_COLUMN, sb.toString());
            }
        }

        /* total cost */ {
            final Map<Long, List<IStudentPaymentDescription>> extractsMap = IUnispExtractDAO.instance.get().getTotalStudentExtractListMap(model.getOrder(), null);
            IUnispExtractDAO.instance.get().applyStudentPaymentAppointmentList(extractsMap, extractsList);
            model.setTotalOrderPaymentCostAsDouble(IUnispExtractDAO.instance.get().getTotalOrderMonthFactFund(extractsMap));
        }


    }

    @SuppressWarnings("unchecked")
    private ArrayList<StudentPaymentAppointmentExtract> extracts(final List<ViewWrapper<Student>> studentWrapperList) {
        return (ArrayList<StudentPaymentAppointmentExtract>) CollectionUtils.collect(studentWrapperList, input -> ((ViewWrapper<Student>)input).getProperty(Model.EXTRACT_COLUMN));
    }


    @Override
    public void update(final Model model)
    {
        final Session session = getSession();
        final SimpleListDataSource<ViewWrapper<Student>> ds = model.getDataSource();
        final List<ViewWrapper<Student>> studentWrapperList = ds.getEntityList();
        final ArrayList<StudentPaymentAppointmentExtract> extractsList = extracts(studentWrapperList);
        final StudentPaymentParagraph paragraph = IUnispExtractDAO.instance.get().doCreateAppointmentExtractList(model.getOrder(), extractsList, "currentEntity");
        if (null != paragraph) {
            final List<StudentPaymentExtractBasic> basicList = model.getBasicModel().getValue();
            if (null != basicList) {

                for (final StudentPaymentExtractBasic basic: basicList) {
                    final StudentPaymentParagraphToBasic relation = new StudentPaymentParagraphToBasic();
                    relation.setParagraph(paragraph);
                    relation.setBasic(basic);
                    relation.setComment(StringUtils.trimToEmpty(model.getBasicCommentMap().get(basic.getId())));
                    session.save(relation);
                }
            }
            StudentPaymentExtractReason reason = model.getReasonModel().getValue();
            if (null != reason)
            {
                StudentPaymentParagraphToReason rel = new StudentPaymentParagraphToReason();
                rel.setParagraph(paragraph);
                rel.setReason(reason);
                if (reason.isCommentable())
                    rel.setComment(model.getReasonComment());
                session.save(rel);
            }
        }
    }
}