package ru.tandemservice.unisp.component.settings.PrintNameStudentPayment;

import java.util.ArrayList;
import java.util.List;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentType;

/**
 * 
 * @author nkokorina
 * Created on: 18.02.2010
 */

public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepareListDataSource(Model model)
    {
        List<IEntity> rows = new ArrayList<IEntity>();
        rows.addAll(new MQBuilder(StudentPayment.ENTITY_CLASS, "sp").<IEntity>getResultList(getSession()));
        rows.addAll(new MQBuilder(StudentPaymentType.ENTITY_CLASS, "spt").<IEntity>getResultList(getSession()));
        UniBaseUtils.createPage(model.getDataSource(), rows);
    }

}
