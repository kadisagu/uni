package ru.tandemservice.unisp.entity.orders;

import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unisp.entity.orders.gen.StudentPaymentOrderProtocolGen;

/**
 * Протокол заседания стипендиальной комиссии
 */
public class StudentPaymentOrderProtocol extends StudentPaymentOrderProtocolGen implements IStorableReport
{
}