package ru.tandemservice.unisp.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import org.tandemframework.shared.organization.base.util.IOrgUnitLPGVisibilityResolver;
import org.tandemframework.shared.organization.base.util.IOrgUnitPermissionCustomizer;
import ru.tandemservice.uni.dao.IOrgstructDAO;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author root
 */
public class StudentPaymentOrgUnitPermissionModifier implements ISecurityConfigMetaMapModifier, IOrgUnitLPGVisibilityResolver, IOrgUnitPermissionCustomizer
{
    private static final String PERMISSION_GROUP_ORDERS_NAME = "StudentPaymentOrdersTabPermissionGroup";

    @Override
    public void modify(final Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        final SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("unisp");
        config.setName("unisp-orgunit-sec-config");
        config.setTitle("");

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            final String code = description.getCode();
            // таб "Студенты"
            final PermissionGroupMeta permissionGroup = PermissionMetaUtil.createPermissionGroup(config, code + "StudentsPermissionGroup", "Вкладка «Студенты»");

            // Вкладка «Выплаты» на табе «Студенты»
            final PermissionGroupMeta permissionGroupOrders = PermissionMetaUtil.createPermissionGroup(permissionGroup, code + PERMISSION_GROUP_ORDERS_NAME, "Вкладка «Выплаты»");
            PermissionMetaUtil.createPermission(permissionGroupOrders, "orgUnit_viewStudentPaymentOrdersTab_" + code, "Просмотр");
            PermissionMetaUtil.createPermission(permissionGroupOrders, "add_studentPaymentOrders_" + code, "Добавление приказа");
            PermissionMetaUtil.createPermission(permissionGroupOrders, "edit_studentPaymentOrders_" + code, "Редактирование приказа");
            PermissionMetaUtil.createPermission(permissionGroupOrders, "print_studentPaymentOrders_" + code, "Печать приказа");
            PermissionMetaUtil.createPermission(permissionGroupOrders, "delete_studentPaymentOrders_" + code, "Удаление приказа");

            final PermissionGroupMeta permissionGroupOrderTypes = PermissionMetaUtil.createPermissionGroup(permissionGroupOrders, permissionGroupOrders.getName() + "_orderTypes", "Работа с типами приказов о назначении выплат");
            for (final StudentPaymentExtractType type : UniDaoFacade.getCoreDao().getList(StudentPaymentExtractType.class, StudentPaymentExtractType.L_PARENT, (Object) null, StudentPaymentExtractType.P_CODE))
            {
                PermissionMetaUtil.createPermission(permissionGroupOrderTypes, "workWithOrders_" + code + "_" + type.getCode(), type.getTitle());
            }

        }

        securityConfigMetaMap.put(config.getName(), config);
    }

    @Override
    public void customize(OrgUnitType orgUnitType, Map<String, PermissionGroupMeta> permissionGroupMap)
    {
        boolean hidden = IOrgstructDAO.instance.get().hidePermissionsBasedOnFormingAndTerritorialKind(orgUnitType);
        permissionGroupMap.get(orgUnitType.getCode() + PERMISSION_GROUP_ORDERS_NAME).setHidden(hidden);
    }

    @Override
    public List<ILPGVisibilityResolver> getResolverList()
    {
        return Arrays.<ILPGVisibilityResolver>asList(new ILPGVisibilityResolver()
        {
            @Override
            public String getGroupName()
            {
                return "studentPaymentLocal";
            }

            @Override
            public boolean isHidden(OrgUnitType orgUnitType)
            {
                return IOrgstructDAO.instance.get().hidePermissionsBasedOnFormingAndTerritorialKind(orgUnitType);
            }
        });
    }
}
