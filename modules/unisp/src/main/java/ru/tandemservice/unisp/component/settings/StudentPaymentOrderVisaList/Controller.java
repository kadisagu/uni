// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.StudentPaymentOrderVisaList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDefine;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisp.IUnispComponents;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa;

/**
 * @author oleyba
 * @since 24.02.2010
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null) return;

        IMergeRowIdResolver mergeRowIdResolver = entity -> {
            final Model.VisaWrapper wrapper = (Model.VisaWrapper) entity;
            return wrapper.getVisa() == null ? wrapper.getId().toString() : wrapper.getVisa().getGroup().getId().toString();
        };

        IMergeRowIdResolver groupsVisingMergeResolver = entity -> {
            final Model.VisaWrapper wrapper = (Model.VisaWrapper) entity;
            return wrapper.getVisa().getGroupsMemberVising().getId().toString() + wrapper.getVisa().getGroup().getTitle();
        };

        DynamicListDataSource<Model.VisaWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", "title").setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа участников визирования", Model.VisaWrapper.visa().groupsMemberVising().title().s()).setOrderable(false).setClickable(false).setMergeRowIdResolver(groupsVisingMergeResolver));
        dataSource.addColumn(new SimpleColumn("Название должности (звание) в документе", Model.VisaWrapper.visa().possibleVisa().title()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ФИО сотрудника", Model.VisaWrapper.visa().possibleVisa().entity().person().identityCard().fullFio()).setOrderable(false).setClickable(false));

        dataSource.addColumn(new ActionColumn("Повысить приоритет", CommonBaseDefine.ICO_UP, "onClickUp").setDisabledProperty("group").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", CommonBaseDefine.ICO_DOWN, "onClickDown").setDisabledProperty("group").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить группу виз «{0}»?", "title").setMergeRows(true).setMergeRowIdResolver(mergeRowIdResolver));

        model.setDataSource(dataSource);
    }

    public void onClickAddVisaGroup(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUnispComponents.STUDENT_PAYMENT_ORDER_VISA_ADD_EDIT));
    }

    public void onClickEdit(IBusinessComponent component)
    {
        Long id = (Long) component.getListenerParameter();
        final IEntity entity = getDao().get(id);
        if (StudentPaymentOrderVisa.class.isInstance(entity))
            id = getDao().getNotNull(StudentPaymentOrderVisa.class, id).getGroup().getId();
        component.createDefaultChildRegion(new ComponentActivator(
                IUnispComponents.STUDENT_PAYMENT_ORDER_VISA_ADD_EDIT,
                new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, id)));
    }

    public void onClickUp(IBusinessComponent component)
    {
        getDao().updatePriority((Long) component.getListenerParameter(), true);
    }

    public void onClickDown(IBusinessComponent component)
    {
        getDao().updatePriority((Long) component.getListenerParameter(), false);
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);
        component.refresh();
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
        getDao().prepare(getModel(component));
    }
}