package ru.tandemservice.unisp.component.settings.PrintNameStudentPayment;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisp.IUnispComponents;
import ru.tandemservice.unisp.entity.catalog.IPrintablePayment;

/**
 * 
 * @author nkokorina
 * Created on: 18.02.2010
 */

public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }

        DynamicListDataSource<IEntity> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", IPrintablePayment.P_TITLE).setTreeColumn(true).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Именительный падеж", IPrintablePayment.P_NOMINATIVE_CASE_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Родительный падеж", IPrintablePayment.P_GENITIVE_CASE_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дательный падеж", IPrintablePayment.P_DATIVE_CASE_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Винительный падеж", IPrintablePayment.P_ACCUSATIVE_CASE_TITLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Предложный падеж", IPrintablePayment.P_PREPOSITIONAL_CASE_TITLE).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem"));

        model.setDataSource(dataSource);
    }

    public void onClickEditItem(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUnispComponents.PAYMENTS_PRINT_NAME_EDIT, new ParametersMap().add("studentPaymentId", component.getListenerParameter())));
    }
}
