package ru.tandemservice.unisp.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.unisp.entity.catalog.gen.StudentPaymentTypeGen;

/**
 * Типы выплат
 */
public class StudentPaymentType extends StudentPaymentTypeGen implements IPrintablePayment, IHierarchyItem
{
    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return null;
    }
}