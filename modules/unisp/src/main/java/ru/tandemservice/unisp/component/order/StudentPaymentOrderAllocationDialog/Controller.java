/* $Id: Controller.java 10991 2009-12-25 08:24:27Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderAllocationDialog;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Iterator;
import java.util.List;


/**
 * @author vdanilov
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickAssign(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        final SimpleListDataSource<ViewWrapper<Student>> dataSource = model.getDataSource();
        dataSource.getColumns().clear();

        dataSource.addColumn(new SimpleColumn("ФИО", Student.FIO_KEY, RawFormatter.INSTANCE).setClickable(false).setOrderable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Дата рождения", Student.person().identityCard().birthDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", Student.PASSPORT_KEY).setClickable(false).setOrderable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("№ зачетной книжки", Student.P_BOOK_NUMBER).setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Состояние", Student.STATUS_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Категория обучаемого", Student.L_STUDENT_CATEGORY + "." + StudentCategory.P_TITLE).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Целевой прием", Student.P_TARGET_ADMISSION, YesNoFormatter.INSTANCE).setOrderable(false).setClickable(false));

        dataSource.addColumn(new SimpleColumn("Форма освоения", Student.educationOrgUnit().developForm().s()+".shortTitle").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", Student.educationOrgUnit().developCondition().s()+".shortTitle").setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", Student.educationOrgUnit().developPeriod().title().s()).setOrderable(false).setClickable(false));

        dataSource.addColumn(new BlockColumn<Object>("paymentCost", "Сумма").setRequired(true));
        dataSource.addColumn(new BlockColumn<Object>("paymentDateRange", "Период").setRequired(true));

        dataSource.addColumn(new SimpleColumn("Конфликты", Model.INTERSECTION_COLUMN, NewLineFormatter.NOBR_IN_LINES).setRequired(true).setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Исключить", ActionColumn.DELETE, "onClickExcludeStudent", "Исключить студента «{0}»?", Student.FIO_KEY));

        getDao().prepareListDataSource(getModel(component));
    }

    public void onClickExcludeStudent(final IBusinessComponent component)
    {
        final Long id = component.getListenerParameter();
        if (null == id) { return; }

        final Model model = getModel(component);
        final List<ViewWrapper<Student>> list = model.getDataSource().getEntityList();
        final Iterator<ViewWrapper<Student>> it = list.iterator();
        while (it.hasNext()) {
            if (id.equals(it.next().getId())) { it.remove(); }
        }

    }


    public void onClickRefreshTotalCost(final IBusinessComponent component)
    {
        getDao().refreshListDataSource(getModel(component));
    }

    public void onClickApply(final IBusinessComponent component)
    {
        onClickRefreshTotalCost(component);
        getDao().update(getModel(component));
        deactivate(component);
    }

    public void onChangeStudentPayment(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        model.setPaymentCost(
                null==model.getStudentPaymentModel().getValue() ? 0d : model.getStudentPaymentModel().getValue().getValueAsDouble()
        );
    }
}