/**
 *$Id$
 */
package ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder;

import com.beust.jcommander.internal.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractReadAggregateHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.logic.allocationTab.IStudentPaymentOrderAllocationDao;
import ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.logic.allocationTab.StudentPaymentOrderAllocationDao;
import ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.logic.allocationTab.StudentPaymentOrderAllocationSearchListDSHandler;

import java.util.List;
import java.util.Set;

/**
 * @author Alexander Shaburov
 * @since 14.02.13
 */
@Configuration
public class SpStudentPaymentOrderManager extends BusinessObjectManager
{
    public static SpStudentPaymentOrderManager instance()
    {
        return instance(SpStudentPaymentOrderManager.class);
    }

    public static final String REG_STRUCTURE_DS = "regStructureDS";

    public static final Long LAST_SESSION_ALL_MANDATORY_PASSED = 1L;
    public static final Long LAST_SESSION_NOT_ALL_MANDATORY_PASSED = 2L;

    public static final Long PERFORMANCE_5ALL = 1L;
    public static final Long PERFORMANCE_4SINGLE_5ALL = 2L;
    public static final Long PERFORMANCE_4ALL_5ALL_INC = 3L;
    public static final Long PERFORMANCE_4ALL_5ALL_EXC = 4L;
    public static final Long PERFORMANCE_4ALL = 5L;

    public static final Long DEBT_STATUS_DEBT = 1L;
    public static final Long DEBT_STATUS_NO_DEBT = 2L;

    @Bean
    public IStudentPaymentOrderAllocationDao allocationDao()
    {
        return new StudentPaymentOrderAllocationDao();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new StudentPaymentOrderAllocationSearchListDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> lastSessionStatusDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(LAST_SESSION_ALL_MANDATORY_PASSED, "lastSession.all_mandatory_passed")
                .addRecord(LAST_SESSION_NOT_ALL_MANDATORY_PASSED, "lastSession.not_all_mandatory_passed");
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> performanceStatusDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(PERFORMANCE_5ALL, "performanceStatus.5all")
                .addRecord(PERFORMANCE_4ALL, "performanceStatus.4all")
                .addRecord(PERFORMANCE_4SINGLE_5ALL, "performanceStatus.4single_5all")
                .addRecord(PERFORMANCE_4ALL_5ALL_INC, "performanceStatus.4all_5all_inc")
                .addRecord(PERFORMANCE_4ALL_5ALL_EXC, "performanceStatus.4all_5all_exc");
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> debtStatusDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addRecord(DEBT_STATUS_DEBT, "debtStatus.debt")
                .addRecord(DEBT_STATUS_NO_DEBT, "debtStatus.noDebt");
    }

    @Bean
    public UIDataSourceConfig regStructureDSConfig()
    {
        return SelectDSConfig.with(REG_STRUCTURE_DS, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(regStructureDSHandler())
                .treeable(true)
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> regStructureDSHandler()
    {
        return new AbstractReadAggregateHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Set keys = input.getPrimaryKeys();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryStructure.class, "s");

                if (!CollectionUtils.isEmpty(keys))
                {
                    builder.where(DQLExpressions.in("s.id", keys));
                    return ListOutputBuilder.get(input, createStatement(builder).list()).build();
                }
                List options = HierarchyUtil.listHierarchyItemsWithParents(Lists.newArrayList(createStatement(builder).list()));
                return ListOutputBuilder.get(input, options).build();
            }
        };
    }
}
