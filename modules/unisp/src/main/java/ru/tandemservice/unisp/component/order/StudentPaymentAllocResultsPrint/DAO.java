/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentAllocResultsPrint;

import java.util.Collections;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.UnispDocumentTemplate;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 18.01.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final int STYLE_FIRST_MERGE_CELL = 1;
    private static final int STYLE_NEXT_MERGE_CELL = 2;
    private static final int[] CELL_COURSE_NUMBERS_TO_VERTICAL_MERGING = new int[] {0, 1, 2, 6};
    private static final int[] CELL_GROUP_NUMBERS_TO_VERTICAL_MERGING = new int[] {1, 2, 3, 7};

    public static Comparator<PairKey<Course, StudentPayment>> COURSE_PAYMENT_COMPARATOR = (key1, key2) ->
        {
            if(!key1.getFirst().equals(key2.getFirst())) return key1.getFirst().getIntValue() < key2.getFirst().getIntValue() ? -1 : 1;
            if(null != key1.getSecond() && null != key2.getSecond() && !key1.getSecond().equals(key2.getSecond())) return key1.getSecond().getCode().compareTo(key2.getSecond().getCode());
            if(null == key1.getSecond()) return 1;
            return -1;
        };

    public static Comparator<TripletKey<Course, Group, StudentPayment>> COURSE_GROUP_PAYMENT_COMPARATOR = (key1, key2) ->
        {
            if(!key1.getFirst().equals(key2.getFirst())) return key1.getFirst().getIntValue() < key2.getFirst().getIntValue() ? -1 : 1;
            if(null != key1.getSecond() && null != key2.getSecond() && !key1.getSecond().equals(key2.getSecond())) return key1.getSecond().getTitle().compareTo(key2.getSecond().getTitle());
            if(null != key1.getThird() && null != key2.getThird() && !key1.getThird().equals(key2.getThird())) return key1.getThird().getCode().compareTo(key2.getThird().getCode());
            if(null == key1.getThird()) return 1;
            return -1;
        };


    @Override
    public void prepare(Model model)
    {
        model.setOrder(getNotNull(StudentPaymentsOrder.class, model.getOrder().getId()));
    }

    @Override
    public RtfDocument print(Model model)
    {
        RtfDocument template = new RtfReader().read(getCatalogItem(UnispDocumentTemplate.class, "studentPaymentAllocResults").getContent());

        RtfTableModifier modifier = new RtfTableModifier();
        prepareCourseStatistics(model.getOrder(), modifier);
        prepareGroupStatistics(model.getOrder(), modifier);

        modifier.modify(template);

        return template;
    }

    @SuppressWarnings("unchecked")
    private void prepareCourseStatistics(StudentPaymentsOrder order, RtfTableModifier modifier)
    {
        final List<PairKey<Course, StudentPayment>> pairKeysList = new ArrayList<>();

        Map<Course, Long> sumPaymentStat = new HashMap<>();
        Map<Course, CoreCollectionUtils.Pair<Integer, Integer>> compTypesStat = new HashMap<>();
        Map<PairKey<Course, StudentPayment>, CoreCollectionUtils.Pair<Integer, Long>> paymentStat = new HashMap<>();

        MQBuilder studBuilder = new MQBuilder(Student.ENTITY_CLASS, "s");
        studBuilder.addJoinFetch("s", Student.L_COURSE, "c");
        studBuilder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
        studBuilder.add(MQExpression.eq("s", Student.L_STATUS + "." + StudentStatus.P_ACTIVE, Boolean.TRUE));
        studBuilder.add(MQExpression.eq("s", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, order.getOrgUnit()));
        List<Student> studentsList = studBuilder.getResultList(getSession());

        for(Student student : studentsList)
        {
            PairKey<Course, StudentPayment> key = new PairKey<>(student.getCourse(), null);
            if (!pairKeysList.contains(key)) pairKeysList.add(key);

            CoreCollectionUtils.Pair<Integer, Integer> compTypeCnt = compTypesStat.get(student.getCourse());
            if (null == compTypeCnt) compTypeCnt = new CoreCollectionUtils.Pair<>(0, 0);
            if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(student.getCompensationType().getCode()))
                compTypeCnt.setX(compTypeCnt.getX() + 1);
            else
                compTypeCnt.setY(compTypeCnt.getY() + 1);
            compTypesStat.put(student.getCourse(), compTypeCnt);
        }


        List<PairKey<Course, StudentPayment>> pairKeysToRemove = new ArrayList<>();

        for (IAbstractParagraph<StudentPaymentsOrder> paragraph : order.getParagraphList())
        {
            for (StudentPaymentAppointmentExtract extract : (List<StudentPaymentAppointmentExtract>)paragraph.getExtractList())
            {
                Course course = extract.getEntity().getCourse();
                PairKey<Course, StudentPayment> key = new PairKey<>(course, extract.getPaymentType());

                PairKey<Course, StudentPayment> emptyKey = new PairKey<>(extract.getEntity().getCourse(), null);
                if(!pairKeysToRemove.contains(emptyKey)) pairKeysToRemove.add(emptyKey);
                if(!pairKeysList.contains(key)) pairKeysList.add(key);

                Long sumPayments = sumPaymentStat.get(course);
                if (null == sumPayments) sumPayments = 0L;
                sumPayments += extract.getPaymentCost();
                sumPaymentStat.put(course, sumPayments);

                CoreCollectionUtils.Pair<Integer, Long> singlePaymentStat = paymentStat.get(key);
                if (null == singlePaymentStat) singlePaymentStat = new CoreCollectionUtils.Pair<>(0, 0L);
                singlePaymentStat.setY(singlePaymentStat.getY() + extract.getPaymentCost());
                singlePaymentStat.setX(singlePaymentStat.getX() + 1);
                paymentStat.put(key, singlePaymentStat);
            }
        }

        pairKeysList.removeAll(pairKeysToRemove);
        Collections.sort(pairKeysList, COURSE_PAYMENT_COMPARATOR);

        int budgetCnt = 0;
        int contractCnt = 0;
        int assignCnt = 0;
        long totalPayment = 0L;
        Course prevKey = null;
        final Map<PairKey<Course, StudentPayment>, Integer> stylesMap = new HashMap<>();

        boolean firstMergeCellApplyed = false;
        List<String[]> lines = new ArrayList<>();
        MoneyFormatter frmttr = MoneyFormatter.ruMoneyFormatter(3);
        for (PairKey<Course, StudentPayment> key : pairKeysList)
        {
            String[] line = new String[7];
            line[0] = key.getFirst().getTitle();
            line[1] = null != compTypesStat.get(key.getFirst()) ? String.valueOf(compTypesStat.get(key.getFirst()).getX()) : "";
            line[2] = null != compTypesStat.get(key.getFirst()) ? String.valueOf(compTypesStat.get(key.getFirst()).getY()) : "";
            line[3] = null != key.getSecond() ? key.getSecond().getShortTitle() : "";
            line[4] = null != paymentStat.get(key) ? String.valueOf(paymentStat.get(key).getX()) : "";
            line[5] = null != paymentStat.get(key) ? frmttr.format(paymentStat.get(key).getY()) : "";
            line[6] = null != sumPaymentStat.get(key.getFirst()) ? frmttr.format(sumPaymentStat.get(key.getFirst())) : "";
            lines.add(line);

            if (null != paymentStat.get(key))
            {
                assignCnt += paymentStat.get(key).getX();
                totalPayment += paymentStat.get(key).getY();
            }

            if(null == prevKey || !prevKey.equals(key.getFirst()))
            {
                budgetCnt += null != compTypesStat.get(key.getFirst()) ? compTypesStat.get(key.getFirst()).getX() : 0;
                contractCnt += null != compTypesStat.get(key.getFirst()) ? compTypesStat.get(key.getFirst()).getY() : 0;
            }

            if(null != prevKey)
            {
                if(prevKey.equals(key.getFirst()))
                {
                    stylesMap.put(key, STYLE_NEXT_MERGE_CELL);
                    if(!firstMergeCellApplyed)
                    {
                        firstMergeCellApplyed = true;
                        stylesMap.put(pairKeysList.get(pairKeysList.indexOf(key) - 1), STYLE_FIRST_MERGE_CELL);
                    }
                }
                else
                    firstMergeCellApplyed = false;
            }
            prevKey = key.getFirst();
        }

        modifier.put("TC", lines.toArray(new String[][] {}));
        modifier.put("SC", new String[][] { { "Итого:", String.valueOf(budgetCnt), String.valueOf(contractCnt), "", String.valueOf(assignCnt), "", frmttr.format(totalPayment) } });

        modifier.put("TC", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int rowIdx = startIndex;
                for (PairKey<Course, StudentPayment> key : pairKeysList)
                {
                    Integer style = stylesMap.get(key);
                    List<RtfCell> cells = newRowList.get(rowIdx++).getCellList();

                    if(null != style)
                    {
                        if(STYLE_FIRST_MERGE_CELL == style)
                        {
                            for (int cellIdx : CELL_COURSE_NUMBERS_TO_VERTICAL_MERGING)
                                cells.get(cellIdx).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                        }
                        else if(STYLE_NEXT_MERGE_CELL == style)
                        {
                            for (int cellIdx : CELL_COURSE_NUMBERS_TO_VERTICAL_MERGING)
                                cells.get(cellIdx).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                        }
                    }
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void prepareGroupStatistics(StudentPaymentsOrder order, RtfTableModifier modifier)
    {
        final List<TripletKey<Course, Group, StudentPayment>> tripletKeysList = new ArrayList<>();

        Map<PairKey<Course, Group>, Long> sumPaymentStat = new HashMap<>();
        Map<PairKey<Course, Group>, CoreCollectionUtils.Pair<Integer, Integer>> compTypesStat = new HashMap<>();
        Map<TripletKey<Course, Group, StudentPayment>, CoreCollectionUtils.Pair<Integer, Long>> paymentStat = new HashMap<>();

        MQBuilder studBuilder = new MQBuilder(Student.ENTITY_CLASS, "s");
        studBuilder.addJoinFetch("s", Student.L_COURSE, "c");
        studBuilder.add(MQExpression.eq("s", Student.P_ARCHIVAL, Boolean.FALSE));
        studBuilder.add(MQExpression.eq("s", Student.L_STATUS + "." + StudentStatus.P_ACTIVE, Boolean.TRUE));
        studBuilder.add(MQExpression.eq("s", Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, order.getOrgUnit()));
        List<Student> studentsList = studBuilder.getResultList(getSession());

        for(Student student : studentsList)
        {
            PairKey<Course, Group> pairKey = new PairKey<>(student.getCourse(), student.getGroup());
            TripletKey<Course, Group, StudentPayment> key = new TripletKey<>(student.getCourse(), student.getGroup(), null);
            if (!tripletKeysList.contains(key)) tripletKeysList.add(key);

            CoreCollectionUtils.Pair<Integer, Integer> compTypeCnt = compTypesStat.get(pairKey);
            if (null == compTypeCnt) compTypeCnt = new CoreCollectionUtils.Pair<>(0, 0);
            if (UniDefines.COMPENSATION_TYPE_BUDGET.equals(student.getCompensationType().getCode()))
                compTypeCnt.setX(compTypeCnt.getX() + 1);
            else
                compTypeCnt.setY(compTypeCnt.getY() + 1);
            compTypesStat.put(pairKey, compTypeCnt);
        }

        List<TripletKey<Course, Group, StudentPayment>> tripletKeysToRemove = new ArrayList<>();

        for (IAbstractParagraph<StudentPaymentsOrder> paragraph : order.getParagraphList())
        {
            for (StudentPaymentAppointmentExtract extract : (List<StudentPaymentAppointmentExtract>)paragraph.getExtractList())
            {
                Group group = extract.getEntity().getGroup();
                Course course = extract.getEntity().getCourse();

                PairKey<Course, Group> pairKey = new PairKey<>(course, group);
                TripletKey<Course, Group, StudentPayment> tripletKey = new TripletKey<>(course, group, extract.getPaymentType());

                TripletKey<Course, Group, StudentPayment> emptyKey = new TripletKey<>(extract.getEntity().getCourse(), extract.getEntity().getGroup(), null);
                if(!tripletKeysToRemove.contains(emptyKey)) tripletKeysToRemove.add(emptyKey);
                if (!tripletKeysList.contains(tripletKey)) tripletKeysList.add(tripletKey);

                Long sumPayments = sumPaymentStat.get(pairKey);
                if (null == sumPayments) sumPayments = 0L;
                sumPayments += extract.getPaymentCost();
                sumPaymentStat.put(pairKey, sumPayments);

                CoreCollectionUtils.Pair<Integer, Long> singlePaymentStat = paymentStat.get(tripletKey);
                if (null == singlePaymentStat) singlePaymentStat = new CoreCollectionUtils.Pair<>(0, 0L);
                singlePaymentStat.setY(singlePaymentStat.getY() + extract.getPaymentCost());
                singlePaymentStat.setX(singlePaymentStat.getX() + 1);
                paymentStat.put(tripletKey, singlePaymentStat);
            }
        }

        tripletKeysList.removeAll(tripletKeysToRemove);
        Collections.sort(tripletKeysList, COURSE_GROUP_PAYMENT_COMPARATOR);

        int budgetCnt = 0;
        int contractCnt = 0;
        int assignCnt = 0;
        long totalPayment = 0L;
        Course prevCourseKey = null;
        PairKey<Course, Group> prevGroupKey = null;

        final Map<TripletKey<Course, Group, StudentPayment>, Integer> groupStylesMap = new HashMap<>();
        final Map<TripletKey<Course, Group, StudentPayment>, Integer> courseStylesMap = new HashMap<>();

        boolean firstCourseMergeCellApplyed = false;
        boolean firstGroupMergeCellApplyed = false;
        List<String[]> lines = new ArrayList<>();
        MoneyFormatter frmttr = MoneyFormatter.ruMoneyFormatter(3);
        for (TripletKey<Course, Group, StudentPayment> tripletKey : tripletKeysList)
        {
            PairKey<Course, Group> pairKey = new PairKey<>(tripletKey.getFirst(), tripletKey.getSecond());

            String[] line = new String[8];
            line[0] = tripletKey.getFirst().getTitle();
            line[1] = null != tripletKey.getSecond() ? tripletKey.getSecond().getTitle() : "Без группы";
            line[2] = null != compTypesStat.get(pairKey) ? String.valueOf(compTypesStat.get(pairKey).getX()) : "";
            line[3] = null != compTypesStat.get(pairKey) ? String.valueOf(compTypesStat.get(pairKey).getY()) : "";
            line[4] = null != tripletKey.getThird() ? tripletKey.getThird().getShortTitle() : "";
            line[5] = null != paymentStat.get(tripletKey) ? String.valueOf(paymentStat.get(tripletKey).getX()) : "";
            line[6] = null != paymentStat.get(tripletKey) ? frmttr.format(paymentStat.get(tripletKey).getY()) : "";
            line[7] = null != sumPaymentStat.get(pairKey) ? frmttr.format(sumPaymentStat.get(pairKey)) : "";
            lines.add(line);

            if (null != paymentStat.get(tripletKey))
            {
                assignCnt += paymentStat.get(tripletKey).getX();
                totalPayment += paymentStat.get(tripletKey).getY();
            }

            if(null != prevCourseKey)
            {
                if(prevCourseKey.equals(tripletKey.getFirst()))
                {
                    courseStylesMap.put(tripletKey, STYLE_NEXT_MERGE_CELL);
                    if(!firstCourseMergeCellApplyed)
                    {
                        firstCourseMergeCellApplyed = true;
                        courseStylesMap.put(tripletKeysList.get(tripletKeysList.indexOf(tripletKey) - 1), STYLE_FIRST_MERGE_CELL);
                    }
                }
                else
                    firstCourseMergeCellApplyed = false;
            }
            prevCourseKey = tripletKey.getFirst();

            if(null == prevGroupKey || !prevGroupKey.equals(pairKey))
            {
                budgetCnt += null != compTypesStat.get(pairKey) ? compTypesStat.get(pairKey).getX() : 0;
                contractCnt += null != compTypesStat.get(pairKey) ? compTypesStat.get(pairKey).getY() : 0;
            }

            if(null != prevGroupKey)
            {
                if(prevGroupKey.equals(pairKey))
                {
                    groupStylesMap.put(tripletKey, STYLE_NEXT_MERGE_CELL);
                    if(!firstGroupMergeCellApplyed)
                    {
                        firstGroupMergeCellApplyed = true;
                        groupStylesMap.put(tripletKeysList.get(tripletKeysList.indexOf(tripletKey) - 1), STYLE_FIRST_MERGE_CELL);
                    }
                }
                else
                    firstGroupMergeCellApplyed = false;
            }
            prevGroupKey = pairKey;
        }

        modifier.put("TG", lines.toArray(new String[][] {}));
        modifier.put("SG", new String[][] { { "Итого:", "", String.valueOf(budgetCnt), String.valueOf(contractCnt), "", String.valueOf(assignCnt), "", frmttr.format(totalPayment) } });

        modifier.put("TG", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int rowIdx = startIndex;
                for (TripletKey<Course, Group, StudentPayment> key : tripletKeysList)
                {
                    List<RtfCell> cells = newRowList.get(rowIdx++).getCellList();

                    Integer courseStyle = courseStylesMap.get(key);
                    if(null != courseStyle)
                    {
                        if(STYLE_FIRST_MERGE_CELL == courseStyle) cells.get(0).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                        else if(STYLE_NEXT_MERGE_CELL == courseStyle) cells.get(0).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    }

                    Integer groupStyle = groupStylesMap.get(key);
                    if(null != groupStyle)
                    {
                        if(STYLE_FIRST_MERGE_CELL == groupStyle)
                        {
                            for (int cellIdx : CELL_GROUP_NUMBERS_TO_VERTICAL_MERGING)
                                cells.get(cellIdx).setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                        }
                        else if(STYLE_NEXT_MERGE_CELL == groupStyle)
                        {
                            for (int cellIdx : CELL_GROUP_NUMBERS_TO_VERTICAL_MERGING)
                                cells.get(cellIdx).setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                        }
                    }
                }
            }
        });
    }

}