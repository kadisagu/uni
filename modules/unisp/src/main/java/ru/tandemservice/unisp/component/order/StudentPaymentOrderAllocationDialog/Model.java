/* $Id: Model.java 10975 2009-12-24 11:28:03Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderAllocationDialog;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.SimpleListDataSource;

import ru.tandemservice.uni.entity.catalog.GlobalFinancingSource;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractBasic;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author vdanilov
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "order.id"),
    @Bind(key = "studentIds", binding = "studentIds")
})
public class Model
{
    private StudentPaymentsOrder order = new StudentPaymentsOrder();
    public StudentPaymentsOrder getOrder() { return this.order; }
    public void setOrder(final StudentPaymentsOrder order) { this.order = order; }

    private Collection<Long> studentIds;
    public Collection<Long> getStudentIds() { return this.studentIds; }
    public void setStudentIds(final Collection<Long> studentIds) { this.studentIds = studentIds; }


    private final SelectModel<StudentPaymentExtractType> studentPaymentExtractTypeModel = new SelectModel<StudentPaymentExtractType>();
    public SelectModel<StudentPaymentExtractType> getStudentPaymentExtractTypeModel() { return this.studentPaymentExtractTypeModel; }

    private final SelectModel<StudentPayment> studentPaymentModel = new SelectModel<StudentPayment>();
    public SelectModel<StudentPayment> getStudentPaymentModel() { return this.studentPaymentModel; }

    private final SelectModel<GlobalFinancingSource> financingSourceModel = new SelectModel<GlobalFinancingSource>();
    public SelectModel<GlobalFinancingSource> getFinancingSourceModel() { return this.financingSourceModel; }

    private final SelectModel<StudentPaymentExtractReason> reasonModel = new SelectModel<StudentPaymentExtractReason>();
    public SelectModel<StudentPaymentExtractReason> getReasonModel() { return this.reasonModel; }

    private final SelectModel<List<StudentPaymentExtractBasic>> basicModel = new SelectModel<List<StudentPaymentExtractBasic>>();
    public SelectModel<List<StudentPaymentExtractBasic>> getBasicModel() { return this.basicModel; }

    public static final String EXTRACT_COLUMN = "extractColumn";
    public static final String INTERSECTION_COLUMN = "intersectionColumn";

    private final SimpleListDataSource<ViewWrapper<Student>> dataSource = new SimpleListDataSource<ViewWrapper<Student>>(Collections.<ViewWrapper<Student>>emptyList()) {
        @Override public long getCountRow() { return getTotalSize(); }
        @Override public long getStartRow() { return 0; }
    };
    public SimpleListDataSource<ViewWrapper<Student>> getDataSource() { return this.dataSource; }


    private Date paymentStartDate;
    public Date getPaymentStartDate() { return this.paymentStartDate; }
    public void setPaymentStartDate(final Date paymentStartDate) { this.paymentStartDate = paymentStartDate; }

    private Date paymentStopDate;
    public Date getPaymentStopDate() { return this.paymentStopDate; }
    public void setPaymentStopDate(final Date paymentStopDate) { this.paymentStopDate = paymentStopDate; }

    private Double paymentCost;
    public Double getPaymentCost() { return this.paymentCost; }
    public void setPaymentCost(final Double paymentCost) { this.paymentCost = paymentCost; }

    private String reasonComment;
    public String getReasonComment() { return reasonComment; }
    public void setReasonComment(String reasonComment) { this.reasonComment = reasonComment; }
    public boolean isReasonCommentable() { return reasonModel.getValue() != null && reasonModel.getValue().isCommentable(); }

    private final Map<Long, String> basicCommentMap = new HashMap<Long, String>();
    public Map<Long, String> getBasicCommentMap() { return this.basicCommentMap; }

    private StudentPaymentExtractBasic currentBasic;
    public StudentPaymentExtractBasic getCurrentBasic() { return this.currentBasic; }
    public void setCurrentBasic(final StudentPaymentExtractBasic currentBasic) { this.currentBasic = currentBasic; }

    public boolean isCurrentBasicCommentable() {
        final StudentPaymentExtractReason reason = getReasonModel().getValue();
        if ((null != reason) && reason.isCommentable()) { return true; }
        return ((null != currentBasic) && currentBasic.isCommentable());
    }

    public String getCurrentBasicComment() { return basicCommentMap.get(currentBasic.getId()); }
    public void setCurrentBasicComment(final String comment) { basicCommentMap.put(currentBasic.getId(), comment); }


    private Double totalOrderPaymentCostAsDouble;
    public Double getTotalOrderPaymentCostAsDouble() { return this.totalOrderPaymentCostAsDouble; }
    public void setTotalOrderPaymentCostAsDouble(final Double totalOrderPaymentCostAsDouble) { this.totalOrderPaymentCostAsDouble = totalOrderPaymentCostAsDouble; }

    public String getTotalOrderPaymentString() {
        final Double totalOrderPaymentCostAsDouble = getTotalOrderPaymentCostAsDouble();
        if (null == totalOrderPaymentCostAsDouble) { return ""; }

        final String prefix = "Распределено "+RuMoneyFormatter.INSTANCE.format(totalOrderPaymentCostAsDouble);
        final double plannedFundAsDouble = getOrder().getPlannedFundAsDouble();
        if (plannedFundAsDouble <= 0) { return prefix; }

        final double rate = 100.0d * totalOrderPaymentCostAsDouble / plannedFundAsDouble;
        return prefix + ",\n " + ((int)rate) + "% планируемой суммы (" + RuMoneyFormatter.INSTANCE.format(plannedFundAsDouble) + ")";
    }

    public Validator getGlobalPaymentStartDateValidator() {
        return new Required();
    }

    public Validator getGlobalPaymentStopDateValidator() {
        return new Required() {
            @Override public void validate(final IFormComponent field, final ValidationMessages messages, final Object object) throws ValidatorException {
                super.validate(field, messages, object);
                if (((Date)object).before(getPaymentStartDate())) {
                    throw new ValidatorException("Дата окночания периода должна быть после даты начала");
                }
            }
        };
    }


    public Validator getCurentEntityPaymentStartDateValidator() {
        return new Required();
    }

    public Validator getCurentEntityPaymentStopDateValidator() {
        final StudentPaymentAppointmentExtract extract = (StudentPaymentAppointmentExtract) getDataSource().getCurrentEntity().getProperty(EXTRACT_COLUMN);
        return new Required() {
            @Override public void validate(final IFormComponent field, final ValidationMessages messages, final Object object) throws ValidatorException {
                super.validate(field, messages, object);
                if (!((Date)object).after(extract.getPaymentStartDate())) {
                    throw new ValidatorException("Дата окночания периода должна быть больше даты его начала.");
                }
            }
        };
    }

    public boolean isSumDisabled()
    {
       return (null == studentPaymentModel.getValue()) || studentPaymentModel.getValue().isFixedValue();
    }

    public boolean isShowComments()
    {
        if (basicModel.getValue() == null && reasonModel.getValue() == null) return false;
        if (reasonModel.getValue() != null && reasonModel.getValue().isCommentable()) return true;
        if (basicModel.getValue() != null)
            for (StudentPaymentExtractBasic basic : basicModel.getValue())
                if (basic.isCommentable()) return true;
        return false;
    }
}