/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.orgunit.OrgUnitOrdersList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unisp.dao.UnispDaoFacade;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 * Created on: 22.12.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("o");

    @Override
    public void prepare(final Model model)
    {
        String secPrefix = "studentPaymentOrders";
        if (null != model.getOrgUnitId())
        {
            model.setOrgUnit((OrgUnit)getNotNull(model.getOrgUnitId()));
            secPrefix = secPrefix + "_" + StringUtils.uncapitalize(model.getOrgUnit().getOrgUnitType().getCode());
        }
        model.setSecModel(new CommonPostfixPermissionModel(secPrefix));
        model.setOrderTypesListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(final String filter)
            {
                return new ListResult<>(UnispDaoFacade.getUnispExtractDAO().getStudentPaymentOrderTypesList(filter, model.getOrgUnit()));
            }
        });
    }

    @Override
    public void refreshRequiredOrderTypeFieldSettings(final Model model)
    {
        if((null == model.getSettings().get("orderType")) && !model.getOrderTypesListModel().findValues("").getObjects().isEmpty()) {
            model.getSettings().set("orderType", model.getOrderTypesListModel().findValues("").getObjects().get(0));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareListDataSource(final Model model)
    {
        final Object orderType = model.getSettings().get("orderType");
        final String orderNumber = model.getSettings().get("orderNumber");

        if (null == orderType)
        {
            UniBaseUtils.createPage(model.getDataSource(), new ArrayList<StudentPaymentsOrder>());
        }
        else
        {
            final MQBuilder builder = new MQBuilder(StudentPaymentsOrder.ENTITY_CLASS, "o");
            builder.add(MQExpression.eq("o", IAbstractOrder.L_TYPE, orderType));
            builder.add(MQExpression.eq("o", StudentPaymentsOrder.L_ORG_UNIT, model.getOrgUnit()));
            if (null != orderNumber) {
                builder.add(MQExpression.like("o", IAbstractOrder.P_NUMBER, CoreStringUtils.escapeLike(orderNumber)));
            }
            _orderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
            UniBaseUtils.createPage(model.getDataSource(), builder, getSession());

            // get statistics for view properties
            final String hql = "select " + IAbstractExtract.L_PARAGRAPH+"."+IAbstractParagraph.L_ORDER + ".id, count(id) from " + StudentPaymentExtract.ENTITY_CLASS + " where " + IAbstractExtract.L_PARAGRAPH+"."+IAbstractParagraph.L_ORDER + " is not null group by " + IAbstractExtract.L_PARAGRAPH+"."+IAbstractParagraph.L_ORDER;
            final Map<Long, Integer> id2count = new HashMap<>();
            for (final Object[] row : (List<Object[]>) getSession().createQuery(hql).list())
            {
                final Long id = (Long) row[0];
                final Number count = (Number) row[1];
                id2count.put(id, count == null ? 0 : count.intValue());
            }

            // inject view properties
            for (final ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
            {
                final Integer value = id2count.get(wrapper.getId());
                wrapper.setViewProperty(StudentPaymentsOrder.P_COUNT_EXTRACT, value == null ? 0 : value);
                wrapper.setViewProperty("disabledPrint", value == null);
            }
        }
    }
}