/**
 *$Id$
 */
package ru.tandemservice.unisp.component.settings.StudentPaymentOrderVisaAddEdit;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa;

import java.util.List;

/**
 * Create by ashaburov
 * Date 17.11.11
 */
class Row implements IEntity
{
    private Long _id;
    private boolean _first;
    private StudentPaymentOrderVisa _visaItem;

    //Constructors

    public Row(Long id, StudentPaymentOrderVisa visaItem, boolean first)
    {
        super();

        if (!first && visaItem == null)
            throw new NullPointerException("Property 'visaItem' should be not null.");

        _id = id;
        _visaItem = visaItem;
        _first = first;
    }

    //Calculate

    public static Long getNewId(List<Row> rowList)
    {
        Long newId = 1L;
        boolean repeat;
        do
        {
            repeat = false;
            for (Row mainRow : rowList)
                if (mainRow.getId().equals(newId))
                    {
                        repeat = true;
                        newId++;
                    }
        }
        while (repeat);
        return newId;
    }

    @Override
    public boolean equals(Object obj)
    {
        Row row = (Row) obj;
        return this.getId().equals(row.getId());
    }

    //Getters & Setters

    public boolean isFirst()
    {
        return _first;
    }

    public void setFirst(boolean first)
    {
        _first = first;
    }

    public StudentPaymentOrderVisa getVisaItem()
    {
        return _visaItem;
    }

    public void setVisaItem(StudentPaymentOrderVisa visaItem)
    {
        _visaItem = visaItem;
    }

    @Override
    public void setId(Long id)
    {
        _id = id;
    }

    @Override
    public Object getProperty(Object propertyPath)
    {
        throw new UnsupportedOperationException("Class not support this method.");
    }

    @Override
    public void setProperty(String propertyPath, Object propertyValue)
    {
        throw new UnsupportedOperationException("Class not support this method.");
    }

    @Override
    public Long getId()
    {
        return _id;
    }
}
