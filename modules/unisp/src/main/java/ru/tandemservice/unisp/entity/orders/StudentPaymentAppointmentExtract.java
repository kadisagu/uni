package ru.tandemservice.unisp.entity.orders;

import java.util.Calendar;

import org.tandemframework.core.view.formatter.DateFormatter;

import ru.tandemservice.unisp.UnispDefines;
import ru.tandemservice.unisp.UnispUtils;
import ru.tandemservice.unisp.entity.orders.gen.StudentPaymentAppointmentExtractGen;

/**
 * Выписка о назначении выплаты
 */
public class StudentPaymentAppointmentExtract extends StudentPaymentAppointmentExtractGen
{
    public static final String P_PAYMENT_COST_AS_DOUBLE = "paymentCostAsDouble";
    public static final String P_FORMATTED_PAYMENT_PERIOD = "formattedPaymentPeriod";

    public void setPaymentCostAsDouble(final Double property)
    {
        setPaymentCost(UnispUtils.getUnwrappedDoubleValue(null == property ? 0d : property.doubleValue()));
    }

    public Double getPaymentCostAsDouble()
    {
        return UnispUtils.getWrappedLongValue(getPaymentCost());
    }

    public int getMonthCount()
    {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(this.getPaymentStartDate());
        int i = 0;
        while (cal.getTime().before(getPaymentStopDate()))
        {
            cal.add(Calendar.MONTH, 1);
            i++;
        }
        return i;
    }

    public long getPaymentTotalCost()
    {
        return getPaymentType().getKind().getPaymentTotalCost(this);
    }

    public Double getPaymentTotalCostAsDouble()
    {
        return UnispUtils.getWrappedLongValue(getPaymentTotalCost());
    }
    
    public String getFormattedPaymentPeriod()
    {
        //TODO: 
        if (UnispDefines.PAYMENT_KIND_MONTHLY_PAYMENT.equals(getPaymentType().getKind().getCode()))
        {
            StringBuilder builder = new StringBuilder("c ");
            builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getPaymentStartDate())).append(" по ");
            builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(getPaymentStopDate()));
            return builder.toString();
        }
        else
        {
            return "единовременно";
        }
    }
}