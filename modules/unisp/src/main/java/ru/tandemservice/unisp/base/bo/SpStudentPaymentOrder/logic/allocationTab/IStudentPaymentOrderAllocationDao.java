/**
 *$Id$
 */
package ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.logic.allocationTab;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author Alexander Shaburov
 * @since 14.02.13
 */
public interface IStudentPaymentOrderAllocationDao extends INeedPersistenceSupport
{
    void deleteStudentPayments(final StudentPaymentsOrder order, final Long studentId);
}
