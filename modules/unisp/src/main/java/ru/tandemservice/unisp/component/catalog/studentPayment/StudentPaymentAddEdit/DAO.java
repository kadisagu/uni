/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.catalog.studentPayment.StudentPaymentAddEdit;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.unisp.entity.catalog.PaymentKind;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentType;

/**
 * @author AutoGenerator
 * Created on 19.12.2009
 */
public class DAO extends DefaultCatalogAddEditDAO<StudentPayment, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setPaymentTypesList(CatalogManager.instance().dao().getCatalogItemListOrderByCode(StudentPaymentType.class));
        model.setPaymentKindsList(CatalogManager.instance().dao().getCatalogItemListOrderByCode(PaymentKind.class));
    }

    @Override
    public void update(Model model)
    {
        super.update(model);
    }
}