// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.StudentPaymentOrderVisaAddEdit;

import org.hibernate.Session;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.ui.PossibleVisaListModel;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisaGroup;

import java.util.List;

/**
 * @author oleyba
 * @since 24.02.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        MQBuilder builder = new MQBuilder(OrgUnitToKindRelation.ENTITY_CLASS, "rel", new String[]{OrgUnitToKindRelation.orgUnit().s()});
        builder.add(MQExpression.eq("rel", OrgUnitToKindRelation.orgUnitKind().code().s(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        builder.add(MQExpression.eq("rel", OrgUnitToKindRelation.orgUnit().archival().s(), Boolean.FALSE));
        model.setOrgUnitModel(new LazySimpleSelectModel<OrgUnit>(builder.<OrgUnit>getResultList(getSession()), OrgUnit.P_FULL_TITLE));

        if (model.isEditForm())
        {
            if (model.getToDelVisaList() == null || model.getToDelVisaList().isEmpty())
            {
                model.setGroup(get(StudentPaymentOrderVisaGroup.class, model.getGroup().getId()));
                List<StudentPaymentOrderVisa> itemList = getList(StudentPaymentOrderVisa.class, StudentPaymentOrderVisa.group().s(), model.getGroup(), StudentPaymentOrderVisa.priority().s());

                model.setToDelVisaList(itemList);

                for (StudentPaymentOrderVisa item : itemList)
                {
                    Row row = new Row(Row.getNewId(model.getRowList()), item, false);
                    model.getRowList().add(row);

                    if (!model.getExcludeGroupsMemberVising().contains(item.getGroupsMemberVising()))
                        model.getExcludeGroupsMemberVising().add(item.getGroupsMemberVising());
                }
            }
        }

        model.setPossibleVisaListModel(new PossibleVisaListModel(getSession()));

        model.setGroupsMemberModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                MQBuilder builder = new MQBuilder(GroupsMemberVising.ENTITY_CLASS, "b");
                builder.add(MQExpression.notIn("b", GroupsMemberVising.id().s(), ids(model.getExcludeGroupsMemberVising())));
                builder.add(MQExpression.like("b", GroupsMemberVising.title().s(), CoreStringUtils.escapeLike(filter)));
                if (o != null)
                    builder.add(MQExpression.eq("b", GroupsMemberVising.id().s(), o));

                return new MQListResultBuilder(builder);
            }
        });

        Row firstElement = new Row(0L, null, true);
        if (!model.getRowList().contains(firstElement))
            model.getRowList().add(0, firstElement);
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getRowList() == null || model.getRowList().size() == 1)
            errors.add("Группа виз не может быть пустой.");

        MQBuilder builder = new MQBuilder(StudentPaymentOrderVisaGroup.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", StudentPaymentOrderVisaGroup.title().s(), model.getGroup().getTitle().trim()));
        if (model.isEditForm())
            builder.add(MQExpression.notEq("e", "id", model.getGroup().getId()));
        if (builder.getResultCount(getSession()) > 0)
            errors.add("Название группы виз должно быть уникальным.", "title");
    }

    @Override
    public void update(Model model)
    {
        final Session session = getSession();

        // удаляем группу виз
        if (model.isEditForm())
            for (StudentPaymentOrderVisa forDelete : model.getToDelVisaList())
                session.delete(forDelete);

        // после всех delete надо делать flush, иначе будет ошибка уникальности
        session.flush();

        // создаем группу виз заново
        session.saveOrUpdate(model.getGroup());
        for (Row row : model.getRowList())
            if (!row.isFirst())
                session.save(row.getVisaItem());
    }

    @Override
    public void prepareVisaGroupDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getRowDataSource(), model.getRowList());
    }
}
