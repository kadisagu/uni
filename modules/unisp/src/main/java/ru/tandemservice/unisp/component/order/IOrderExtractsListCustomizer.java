/**
 * $Id$
 */
package ru.tandemservice.unisp.component.order;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

/**
 * @author dseleznev
 * Created on: 11.01.2010
 */
public interface IOrderExtractsListCustomizer
{
    @SuppressWarnings("unchecked")
    void customizeParagraphList(DynamicListDataSource dataSource, IBusinessComponent component);
}