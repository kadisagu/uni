/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisp.dao;

import java.util.List;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractBasic;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author dseleznev
 * Created on: 18.12.2009
 */
public interface IUnispDAO<Model> extends IUniDao<Model>
{
    String UNISP_DAO_BEAN_NAME = "unispDao";
    
    int getExtractsCount(StudentPaymentsOrder order);
    
    boolean isStudentPaymentsTabVisible(OrgUnit orgUnit);
    
    List<StudentPayment> getPaymentToExtractTypePaymentsList(StudentPaymentExtractType extractType);
    
    List<StudentPaymentExtractReason> getReasonToExtractTypePaymentsList(StudentPaymentExtractType extractType);
    
    List<StudentPaymentExtractBasic> getReasonToBasicsList(StudentPaymentExtractReason extractReason);
}