// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderVisaGroupAdd;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisaGroup;

import java.util.List;

/**
 * @author oleyba
 * @since 26.02.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        StudentPaymentsOrder order = getNotNull(StudentPaymentsOrder.class, model.getDocumentId());

        MQBuilder builder = new MQBuilder(StudentPaymentOrderVisaGroup.ENTITY_CLASS, "g");
        builder.add(MQExpression.or(MQExpression.eq("g", StudentPaymentOrderVisaGroup.orgUnit().s(), order.getOrgUnit()), MQExpression.isNull("g", StudentPaymentOrderVisaGroup.orgUnit().s())));
        builder.addOrder("g", StudentPaymentOrderVisaGroup.P_TITLE);
        model.setGroupModel(new LazySimpleSelectModel<>(builder.<StudentPaymentOrderVisaGroup>getResultList(getSession())));
    }

    @Override
    public void update(Model model)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentPaymentOrderVisa.class, "i");
        builder.column("i");
        builder.where(DQLExpressions.eq(DQLExpressions.property("i", StudentPaymentOrderVisa.L_GROUP), DQLExpressions.value(model.getGroup())));
        builder.order(DQLExpressions.property("i", StudentPaymentOrderVisa.P_PRIORITY));
        List<StudentPaymentOrderVisa> itemList = builder.createStatement(new DQLExecutionContext(getSession())).list();

        IAbstractDocument document = get(model.getDocumentId());
        for (StudentPaymentOrderVisa item : itemList)
        {
//            if (UnimvDaoFacade.getVisaDao().getVisa(document, possibleVisa) == null)
//            {
                Visa visa = new Visa();
                visa.setMandatory(false);
                visa.setPossibleVisa(item.getPossibleVisa());
                visa.setDocument(document);
                visa.setGroupMemberVising(item.getGroupsMemberVising());
                visa.setIndex(getCount(Visa.class, Visa.L_DOCUMENT, document));
                save(visa);
//            }
        }
    }
}
