/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.ReasonToBasicSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisp.IUnispComponents;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;

/**
 * @author dseleznev
 * Created on: 28.12.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }
    
    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (getModel(component).getDataSource() != null) return;
        
        DynamicListDataSource<StudentPaymentExtractReason> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        });
        
        dataSource.addColumn(new SimpleColumn("Причина", StudentPaymentExtractReason.P_TITLE).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Основания", "title", StudentPaymentExtractReason.P_BASICS).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        getModel(component).setDataSource(dataSource);
    }
    
    public void onClickEdit(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUnispComponents.REASON_TO_BASIC_EDIT, new ParametersMap().add("studentPaymentExtractReasonId", (Long) component.getListenerParameter())));
    }
}