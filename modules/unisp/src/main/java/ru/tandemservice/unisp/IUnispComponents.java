/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisp;

/**
 * @author dseleznev
 * Created on: 18.12.2009
 */
public interface IUnispComponents
{

    String USED_STUDENT_PAYMENT_ORDER_TYPES_EDIT = "ru.tandemservice.unisp.component.settings.UsedStudentPaymentOrderTypesSettingsEdit";

    String DOC_TYPE_TO_REASONS_COMMENT_EDIT = "ru.tandemservice.unisp.component.settings.ReasonsCommentSettingsEdit";
    String DOC_TYPE_TO_PAYMENTS_EDIT = "ru.tandemservice.unisp.component.settings.PaymentToDocTypeSettingsEdit";
    String DOC_TYPE_TO_REASON_EDIT = "ru.tandemservice.unisp.component.settings.ReasonToDocTypeSettingsEdit";
    String REASON_TO_BASIC_EDIT = "ru.tandemservice.unisp.component.settings.ReasonToBasicSettingsEdit";
    String PAYMENTS_PRINT_NAME_EDIT = "ru.tandemservice.unisp.component.settings.PrintNameStudentPaymentEdit";

    String STUDENT_PAYMENTS_ORDER_PUB = "ru.tandemservice.unisp.component.order.StudentPaymentsOrderPub";
    String STUDENT_PAYMENTS_ORDER_ADD_EDIT = "ru.tandemservice.unisp.component.order.StudentPaymentsOrderAdd";

    String STUDENT_PAYMENTS_ORDER_PRINT = "ru.tandemservice.unisp.component.order.StudentPaymentsOrderPrint";
    String PROTOCOL_PRINT = "ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolPrint";
    String PAYMENTS_ALLOC_RESULTS_PRINT = "ru.tandemservice.unisp.component.order.StudentPaymentAllocResultsPrint";

    String STUDENT_PAYMENT_ORDER_VISA_ADD_EDIT = "ru.tandemservice.unisp.component.settings.StudentPaymentOrderVisaAddEdit";
    String STUDENT_PAYMENT_ORDER_VISA_GROUP_ADD = "ru.tandemservice.unisp.component.order.StudentPaymentOrderVisaGroupAdd";
}