// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolPub;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol;

/**
 * @author oleyba
 * @since 16.02.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setProtocol(get(StudentPaymentOrderProtocol.class, model.getProtocol().getId()));
    }
}
