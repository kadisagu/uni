/**
 * $Id$
 */
package ru.tandemservice.unisp.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author dseleznev
 * Created on: 18.12.2009
 */
public class UnispDaoFacade
{
    private static IUnispDAO _unispDAO;

    public static IUnispDAO getUnispDAO()
    {
        if (_unispDAO == null)
            _unispDAO = (IUnispDAO)ApplicationRuntime.getBean(IUnispDAO.UNISP_DAO_BEAN_NAME);
        return _unispDAO;
    }
    
    public static IUnispExtractDAO getUnispExtractDAO()
    {
        return IUnispExtractDAO.instance.get();
    }
    
    public static IUnispPrintDAO getUnispPrintDAO()
    {
        return IUnispPrintDAO.instance.get();
    }
}