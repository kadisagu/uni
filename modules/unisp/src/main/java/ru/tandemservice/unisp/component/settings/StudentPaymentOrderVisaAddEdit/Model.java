// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.StudentPaymentOrderVisaAddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisaGroup;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author oleyba
 * @since 24.02.2010
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "group.id")
public class Model
{
    private StudentPaymentOrderVisaGroup group = new StudentPaymentOrderVisaGroup();

    private ISelectModel _possibleVisaListModel;
    private ISelectModel _orgUnitModel;

    private List<IPossibleVisa> _possibleVisaList;

    private List<StudentPaymentOrderVisa> _toDelVisaList; //список виз на удаление, при редактировании

    private List<Row> _rowList = new ArrayList<>();
    private ISingleSelectModel _groupsMemberModel;
    private GroupsMemberVising _groupsMemberVising;
    private DynamicListDataSource<Row> _rowDataSource;
    private Boolean _disabledAddButton;
    private List<GroupsMemberVising> _excludeGroupsMemberVising = new ArrayList<>();

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;
        }

        @Override
        public Location getLocation()
        {
            return null;
        }
    };

    public List<StudentPaymentOrderVisa> getToDelVisaList()
    {
        return _toDelVisaList;
    }

    public void setToDelVisaList(List<StudentPaymentOrderVisa> toDelVisaList)
    {
        _toDelVisaList = toDelVisaList;
    }

    public List<Row> getRowList()
    {
        return _rowList;
    }

    public void setRowList(List<Row> rowList)
    {
        _rowList = rowList;
    }

    public ISingleSelectModel getGroupsMemberModel()
    {
        return _groupsMemberModel;
    }

    public void setGroupsMemberModel(ISingleSelectModel groupsMemberModel)
    {
        _groupsMemberModel = groupsMemberModel;
    }

    public GroupsMemberVising getGroupsMemberVising()
    {
        return _groupsMemberVising;
    }

    public void setGroupsMemberVising(GroupsMemberVising groupsMemberVising)
    {
        _groupsMemberVising = groupsMemberVising;
    }

    public DynamicListDataSource<Row> getRowDataSource()
    {
        return _rowDataSource;
    }

    public void setRowDataSource(DynamicListDataSource<Row> rowDataSource)
    {
        _rowDataSource = rowDataSource;
    }

    public Boolean getDisabledAddButton()
    {
        return _disabledAddButton;
    }

    public void setDisabledAddButton(Boolean disabledAddButton)
    {
        _disabledAddButton = disabledAddButton;
    }

    public List<GroupsMemberVising> getExcludeGroupsMemberVising()
    {
        return _excludeGroupsMemberVising;
    }

    public void setExcludeGroupsMemberVising(List<GroupsMemberVising> excludeGroupsMemberVising)
    {
        _excludeGroupsMemberVising = excludeGroupsMemberVising;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    public boolean isEditForm()
    {
        return group.getId() != null;
    }

    public ISelectModel getPossibleVisaListModel()
    {
        return _possibleVisaListModel;
    }

    public void setPossibleVisaListModel(ISelectModel possibleVisaListModel)
    {
        _possibleVisaListModel = possibleVisaListModel;
    }

    public ISelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public List<IPossibleVisa> getPossibleVisaList()
    {
        return _possibleVisaList;
    }

    public void setPossibleVisaList(List<IPossibleVisa> possibleVisaList)
    {
        _possibleVisaList = possibleVisaList;
    }

    public StudentPaymentOrderVisaGroup getGroup()
    {
        return group;
    }

    public void setGroup(StudentPaymentOrderVisaGroup group)
    {
        this.group = group;
    }
}
