package ru.tandemservice.unisp.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ilunin
 * @since 26.09.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unisp_2x6x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.15"),
            new ScriptDependency("org.tandemframework.shared", "1.6.8")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // словарь ключей доступа, которые необходимо изменить
        Map<String, String> permissionKeyMap = new HashMap<>();
        permissionKeyMap.put("viewOrdersTab_studentPayments_", "orgUnit_viewStudentPaymentOrdersTab_");

        tool.table("accessmatrix_t").constraints().clear();

        // хранимый запрос обновления ключей доступа
        PreparedStatement permissionKeyStatement = tool.prepareStatement("update accessmatrix_t set permissionkey_p = ? where permissionkey_p = ?");

        // получить список кодов типов подразделений
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        SQLSelectQuery selectOrgUnitTypesQuery = new SQLSelectQuery()
                .from(SQLFrom.table("orgunittype_t", "outype"))
                .column("outype.code_p", "code")
                .distinct();
        String selectQuerySql = translator.toSql(selectOrgUnitTypesQuery);
        Statement statement = tool.getConnection().createStatement();
        statement.execute(selectQuerySql);
        ResultSet result = statement.getResultSet();

        // заменить необходимые вхождения
        while (result.next())
        {
            String code = result.getString("code");
            for (Map.Entry<String, String> permissionKeyEntry: permissionKeyMap.entrySet())
            {
                permissionKeyStatement.setString(1, permissionKeyEntry.getValue() + code );
                permissionKeyStatement.setString(2, permissionKeyEntry.getKey() + code );
                permissionKeyStatement.execute();
            }
        }

    }
}
