/* $Id: DAO.java 11106 2010-01-11 11:00:41Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderAllocationResultTab;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.util.cache.SafeMap.Callback;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.unisp.component.order.StudentPaymentOrderAllocationResultTab.Model.Filter;
import ru.tandemservice.unisp.component.order.StudentPaymentOrderAllocationResultTab.Model.Node;
import ru.tandemservice.unisp.dao.IUnispExtractDAO;
import ru.tandemservice.unisp.dao.IUnispExtractDAO.IStudentPaymentDescription;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;
import ru.tandemservice.unisp.entity.orders.gen.StudentPaymentAppointmentExtractGen;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author vdanilov
 */
public class DAO extends UniDao<Model> implements IDAO
{

    private static final Filter FILTER_STUDENT = new Filter(StudentPaymentAppointmentExtractGen.P_STUDENT_TITLE, "Студент", null);
    private static final Filter FILTER_CATEGORY = new Filter(StudentPaymentAppointmentExtractGen.P_STUDENT_CATEGORY_STR, "Категория студента", null);
    private static final Filter FILTER_GROUP = new Filter(StudentPaymentAppointmentExtractGen.P_GROUP_STR, "Группа", "Вне групп");
    private static final Filter FILTER_COURSE = new Filter(StudentPaymentAppointmentExtractGen.P_COURSE_STR, "Курс", null);
    private static final Filter FILTER_COMPENSATION = new Filter(StudentPaymentAppointmentExtractGen.P_COMPENSATION_TYPE_STR, "Возмещение затрат", null);
    private static final Filter FILTER_EDU_HS = new Filter(StudentPaymentAppointmentExtractGen.P_EDUCATION_LEVEL_HIGH_SCHOOL_STR, "Направление подготовки", null);
    private static final Filter FILTER_DEVELOP_PERIOD = new Filter(StudentPaymentAppointmentExtractGen.P_DEVELOP_PERIOD_STR, "Срок освоения", null);
    private static final Filter FILTER_DEVELOP_TECH = new Filter(StudentPaymentAppointmentExtractGen.P_DEVELOP_TECH_STR, "Технология освоения", null);
    private static final Filter FILTER_DEVELOP_CONDITION = new Filter(StudentPaymentAppointmentExtractGen.P_DEVELOP_CONDITION_STR, "Условие освоения", null);
    private static final Filter FILTER_DEVELOP_FORM = new Filter(StudentPaymentAppointmentExtractGen.P_DEVELOP_FORM_STR, "Форма освоения", null);
    private static final Filter FILTER_TERRITORIAL_OU = new Filter(StudentPaymentAppointmentExtractGen.P_TERRITORIAL_ORG_UNIT_STR, "Территориальное подразделение", null);
    private static final Filter FILTER_FORMATIVE_OU = new Filter(StudentPaymentAppointmentExtractGen.P_FORMATIVE_ORG_UNIT_STR, "Формирующее подразделение", null);

    // !ВАЖНО! - все сравнения проводятся по строкам (не по идентификаторам)


    @Override
    public void prepare(final Model model) {
        model.setOrder(get(StudentPaymentsOrder.class, model.getOrder().getId()));
        model.setData(IUnispExtractDAO.instance.get().getTotalStudentExtractListMap(model.getOrder(), null));
        refreshFilterData(model);
    }


    private static final Callback<Long, SelectModel<Collection<IdentifiableWrapper<IEntity>>>> SELECT_MODEL_CALLBACK = key -> new SelectModel<>();

    private void refreshFilterData(final Model model) {
        final List<Filter> filters = preparefilters(model);
        final Map<Long, Map<Long, IdentifiableWrapper<IEntity>>> filterWrapperMap = new HashMap<>();
        for (final Model.Filter filter: filters) {
            filterWrapperMap.put(filter.getId(), new HashMap<>());
        }

        final Map<IStudentPaymentDescription, StudentPaymentAppointmentExtract> appointmentMap = getAppointmentMap();
        for (final List<IStudentPaymentDescription> payments: model.getData().values()) {
            for (final IStudentPaymentDescription payment: payments) {
                final StudentPaymentAppointmentExtract appointment = appointmentMap.get(payment);
                for (final Model.Filter filter: filters) {
                    filterWrapperMap.get(filter.getId()).put(appointment.getId(), filter.wrap(appointment));
                }
            }
        }


        final Map<IIdentifiable, Object> systemRoot = new HashMap<>();
        final Map<IIdentifiable, Object> root = walk(systemRoot, new IdentifiableWrapper<>(0L, model.getOrder().getTitle()));
        /* распределение выплат */ {
            for (final Map.Entry<IStudentPaymentDescription, StudentPaymentAppointmentExtract> entry: appointmentMap.entrySet()) {
                final IStudentPaymentDescription payment = entry.getKey();
                final StudentPaymentAppointmentExtract appointment = entry.getValue();
                Map<IIdentifiable, Object> current = root;
                for (final Model.Filter filter: model.getSelectedFilters()) {
                    final IdentifiableWrapper<IEntity> wrapper = filterWrapperMap.get(filter.getId()).get(appointment.getId());
                    current = walk(current, wrapper);
                }
                current.put(payment, payment);
            }
        }


        final Map<Long, SelectModel<Collection<IdentifiableWrapper<IEntity>>>> filtersDataMap = model.getFiltersDataMap();

        final Collection<Map.Entry<IStudentPaymentDescription, StudentPaymentAppointmentExtract>> state = new ArrayList<>(appointmentMap.entrySet());
        for (final Filter filter: model.getSelectedFilters()) {
            final Map<Long, IdentifiableWrapper<IEntity>> availableFilterValues = filterWrapperMap.get(filter.getId());
            final SelectModel<Collection<IdentifiableWrapper<IEntity>>> selectModel = SafeMap.safeGet(filtersDataMap, filter.getId(), SELECT_MODEL_CALLBACK);
            final Set<IdentifiableWrapper<IEntity>> selectedValues = (((null == selectModel.getValue()) || selectModel.getValue().isEmpty()) ? null : new HashSet<>(selectModel.getValue()));

            final Collection<IdentifiableWrapper<IEntity>> selectableElements = new HashSet<>();
            final Iterator<Entry<IStudentPaymentDescription, StudentPaymentAppointmentExtract>> it = state.iterator();
            while (it.hasNext()) {
                final Map.Entry<IStudentPaymentDescription, StudentPaymentAppointmentExtract> entry = it.next();
                final IdentifiableWrapper<IEntity> value = availableFilterValues.get(entry.getKey().getId());
                selectableElements.add(value);

                if ((null != selectedValues) && !selectedValues.contains(value)) {
                    it.remove();
                }
            }

            final ListResult listResult = new ListResult<>(selectableElements);
            Collections.sort(listResult.getObjects(), (Comparator)ITitled.TITLED_COMPARATOR);

            selectModel.setSource(new FullCheckSelectModel() {
                @Override public ListResult findValues(final String f) {
                    return listResult;
                }
            });
        }


        final Set<Long> selectedIds = new HashSet<>();
        for (final Map.Entry<IStudentPaymentDescription, StudentPaymentAppointmentExtract> entry: state) {
            selectedIds.add(entry.getKey().getId());
        }


        final List<Model.Node> nodeList = new ArrayList<>();
        collect(null, systemRoot, nodeList, selectedIds, 1L);

        if (model.isOnlySelectedElements()) {
            CollectionUtils.filter(nodeList, object -> ((Node)object).isSelected());
        }

        model.getDataSource().setEntityCollection(nodeList);
    }

    private static final Callback<IIdentifiable, Object> WALK_CALLBACK = key -> new HashMap<IIdentifiable, Object>();

    @SuppressWarnings("unchecked")
    private static Map<IIdentifiable, Object> walk(final Map<IIdentifiable, Object> current, final IIdentifiable wrapper) {
        return (Map<IIdentifiable, Object>)SafeMap.safeGet(current, wrapper, WALK_CALLBACK);
    }

    private static final Comparator<Entry<IIdentifiable, Object>> COMPARATOR = (o1, o2) -> {
        final String title1 = (o1.getKey() instanceof ITitled ? ((ITitled)o1.getKey()).getTitle() : String.valueOf(o1.getKey()));
        final String title2 = (o2.getKey() instanceof ITitled ? ((ITitled)o2.getKey()).getTitle() : String.valueOf(o2.getKey()));
        return title1.compareTo(title2);
    };

    @SuppressWarnings("unchecked")
    private static long collect(final Model.Node parent, final Map<IIdentifiable, Object> root, final List<Model.Node> nodeList, final Set<Long> selectedIds, long counter) {
        final List<Entry<IIdentifiable, Object>> entryList = new ArrayList<>(root.entrySet());
        Collections.sort(entryList, COMPARATOR);

        for (final Map.Entry<IIdentifiable, Object> entry: entryList) {
            final IIdentifiable entity = entry.getKey();
            final String title = (entity instanceof ITitled ? ((ITitled)entity).getTitle() : String.valueOf(entity));
            final Node node = new Model.Node(counter++/*все id должны быть разными*/, parent, title);
            nodeList.add(node);
            if (entry.getValue() instanceof Map) {
                counter = collect(node, (Map<IIdentifiable, Object>) entry.getValue(), nodeList, selectedIds, counter);

            } else if (entry.getValue() instanceof IStudentPaymentDescription) {
                final IStudentPaymentDescription payment = (IStudentPaymentDescription)entry.getValue();
                if (selectedIds.contains(payment.getId())) { node.select(true); }
                node.collect(payment);

            } //else {
                // skip it
            //}
        }
        return counter;
    }



    private Map<IStudentPaymentDescription, StudentPaymentAppointmentExtract> getAppointmentMap() {
        // после вызова IUnispExtractDAO.instance.get() объекты все равно уже в сессии
        return SafeMap.get(payment -> get(StudentPaymentAppointmentExtract.class, payment.getId()));
    }


    private List<Filter> preparefilters(final Model model) {

        final List<Filter> filters = model.getFilters();
        if (filters.isEmpty()) {
            // first time

            filters.add(FILTER_CATEGORY);
            filters.add(FILTER_FORMATIVE_OU);
            filters.add(FILTER_TERRITORIAL_OU);
            filters.add(FILTER_DEVELOP_FORM);
            filters.add(FILTER_DEVELOP_CONDITION);
            filters.add(FILTER_DEVELOP_TECH);
            filters.add(FILTER_DEVELOP_PERIOD);
            filters.add(FILTER_EDU_HS);
            filters.add(FILTER_COMPENSATION);
            filters.add(FILTER_COURSE);
            filters.add(FILTER_GROUP);
            filters.add(FILTER_STUDENT);

            model.setSelectedFilters(Arrays.asList(
                    FILTER_COURSE, FILTER_GROUP, FILTER_STUDENT
            ));
        }
        return filters;
    }

    @Override
    public void prepareFilterModels(final Model model, final boolean clearFiltersMap) {
        refreshFilterData(model);
    }



}