/**
 *$Id$
 */
package ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.logic.allocationTab;

import org.hibernate.criterion.Restrictions;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;
import ru.tandemservice.unisp.entity.orders.gen.StudentPaymentExtractGen;

import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 14.02.13
 */
public class StudentPaymentOrderAllocationDao extends UniBaseDao implements IStudentPaymentOrderAllocationDao
{
    @Override
    @SuppressWarnings("unchecked")
    public void deleteStudentPayments(final StudentPaymentsOrder order, final Long studentId)
    {
        final List<StudentPaymentAppointmentExtract> extracts = getSession().createCriteria(StudentPaymentAppointmentExtract.class)
                .add(Restrictions.eq(StudentPaymentExtractGen.L_ENTITY + ".id", studentId))
                .createCriteria(StudentPaymentExtractGen.L_PARAGRAPH).add(Restrictions.eq(IAbstractParagraph.L_ORDER, order))
                .list();

        for (final StudentPaymentAppointmentExtract e: extracts)
            delete(e);
    }
}
