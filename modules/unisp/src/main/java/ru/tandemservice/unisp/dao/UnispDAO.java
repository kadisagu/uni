/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.unisp.dao;

import java.util.List;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IOrgstructDAO;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractBasic;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.catalog.gen.StudentPaymentGen;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToBasicsRel;
import ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToExtractTypeRel;
import ru.tandemservice.unisp.entity.orders.StudentPaymentToExtractTypeRel;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author dseleznev
 * Created on: 18.12.2009
 */
public class UnispDAO<Model> extends UniDao<Model> implements IUnispDAO<Model>
{
    @Override
    public boolean isStudentPaymentsTabVisible(final OrgUnit orgUnit)
    {
        return IOrgstructDAO.instance.get().isOrgUnitFormingOrTerritorial(orgUnit);
    }

    @Override
    public List<StudentPayment> getPaymentToExtractTypePaymentsList(final StudentPaymentExtractType extractType)
    {
        final MQBuilder builder = new MQBuilder(StudentPaymentToExtractTypeRel.ENTITY_CLASS, "rel", new String[] {StudentPaymentToExtractTypeRel.L_SECOND});
        builder.add(MQExpression.eq("rel", StudentPaymentToExtractTypeRel.L_FIRST, extractType));
        builder.addOrder("rel", StudentPaymentToExtractTypeRel.L_SECOND + "." + StudentPaymentGen.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public List<StudentPaymentExtractReason> getReasonToExtractTypePaymentsList(final StudentPaymentExtractType extractType)
    {
        final MQBuilder builder = new MQBuilder(StudentPaymentReasonToExtractTypeRel.ENTITY_CLASS, "rel", new String[] {StudentPaymentReasonToExtractTypeRel.L_SECOND});
        builder.add(MQExpression.eq("rel", StudentPaymentReasonToExtractTypeRel.L_FIRST, extractType));
        builder.addOrder("rel", StudentPaymentReasonToExtractTypeRel.L_SECOND + "." + StudentPaymentGen.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public List<StudentPaymentExtractBasic> getReasonToBasicsList(final StudentPaymentExtractReason extractReason)
    {
        final MQBuilder builder = new MQBuilder(StudentPaymentReasonToBasicsRel.ENTITY_CLASS, "rel", new String[] {StudentPaymentReasonToBasicsRel.L_SECOND});
        builder.add(MQExpression.eq("rel", StudentPaymentReasonToBasicsRel.L_FIRST, extractReason));
        builder.addOrder("rel", StudentPaymentReasonToBasicsRel.L_SECOND + "." + StudentPaymentExtractBasic.P_TITLE);
        return builder.getResultList(getSession());
    }

    @Override
    public int getExtractsCount(final StudentPaymentsOrder order)
    {
        final MQBuilder builder = new MQBuilder(StudentPaymentExtract.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", IAbstractExtract.L_PARAGRAPH + "." + IAbstractParagraph.L_ORDER, order));
        return (int)builder.getResultCount(getSession());
    }
}