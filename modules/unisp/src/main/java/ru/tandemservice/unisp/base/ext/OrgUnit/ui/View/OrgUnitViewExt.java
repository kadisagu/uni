/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.unisp.base.ext.OrgUnit.ui.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;

/**
 * @author Vasily Zhukov
 * @since 02.04.2012
 */
@Configuration
public class OrgUnitViewExt extends BusinessComponentExtensionManager
{
    @Autowired
    public OrgUnitView _orgUnitView;

}
