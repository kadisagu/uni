package ru.tandemservice.unisp.component.settings.PrintNameStudentPaymentEdit;

import org.tandemframework.core.component.Input;

import ru.tandemservice.unisp.entity.catalog.IPrintablePayment;

/**
 * 
 * @author nkokorina
 * Created on: 18.02.2010
 */

@Input(keys = { "studentPaymentId", "studentPaymentId" })
public class Model
{
    private Long _studentPaymentId;
    private IPrintablePayment _studentPayment;

    public void setStudentPaymentId(Long studentPaymentId)
    {
        this._studentPaymentId = studentPaymentId;
    }

    public Long getStudentPaymentId()
    {
        return _studentPaymentId;
    }

    public void setStudentPayment(IPrintablePayment _studentPayment)
    {
        this._studentPayment = _studentPayment;
    }

    public IPrintablePayment getStudentPayment()
    {
        return _studentPayment;
    }
}
