/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentsOrderAdd;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.unisp.IUnispComponents;

/**
 * @author dseleznev
 * Created on: 23.12.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
        if (model.getOrder().getType()!=null&&model.getOrder().getType().isHasBasic()&&model.getOrder().getOrderBasic()==null)
        {
            model.setOrderBasicVisible(true);
            model.getOrder().setOrderBasic(model.getOrder().getType().getBasic());
        }
        else if (model.getOrder().getType() != null && model.getOrder().getType().isHasBasic())
        {
            model.setOrderBasicVisible(true);
        }
        else
        {
            model.setOrderBasicVisible(false);
            model.getOrder().setOrderBasic(null);
        }
    }
    
    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        
        deactivate(component);
        
        if(getModel(component).isAddForm())
        {
            activateInRoot(component, new ComponentActivator(IUnispComponents.STUDENT_PAYMENTS_ORDER_PUB, new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getOrder().getId())));
        }
    }

    public void onChangeOrderType(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getOrder().getType()!=null&&model.getOrder().getType().isHasBasic())
        {
            model.setOrderBasicVisible(true);
            model.getOrder().setOrderBasic(model.getOrder().getType().getBasic());
        }
        else
        {
            model.setOrderBasicVisible(false);
            model.getOrder().setOrderBasic(null);
        }
    }
}