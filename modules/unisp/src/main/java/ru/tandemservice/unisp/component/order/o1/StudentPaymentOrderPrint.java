/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.o1;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.unisp.UnispUtils;
import ru.tandemservice.unisp.component.order.CommonStudentPaymentOrderPrint;
import ru.tandemservice.unisp.component.order.VirtualParagraph;
import ru.tandemservice.unisp.dao.UnispDaoFacade;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 11.01.2010
 */
public class StudentPaymentOrderPrint implements IPrintFormCreator<StudentPaymentsOrder>
{
    @Override
    @SuppressWarnings("unchecked")
    public RtfDocument createPrintForm(byte[] template, StudentPaymentsOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<VirtualParagraph> paragraphList = UnispDaoFacade.getUnispPrintDAO().prepareOrderReportsData(order);

        RtfInjectModifier injectModifier = CommonStudentPaymentOrderPrint.createListOrderInjectModifier(order);

        if (order.getType().isHasBasic())
        {
            injectModifier.put("basic", order.getOrderBasic());
        }
        else
        {
            List<String> paragraphsToRemoveList = new ArrayList<>();
            paragraphsToRemoveList.add("basic");

            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, paragraphsToRemoveList, false, true);
        }

        //Итого по fmOUTypeG:students sum sumW
        // sumTitle
        int sumCnt = 0;
        long sumPayments = 0;
        for (VirtualParagraph paragraph : paragraphList)
            for(StudentPaymentAppointmentExtract extract : (List<StudentPaymentAppointmentExtract>) paragraph.getExtractsList())
            {
                sumPayments += extract.getPaymentCost();
                sumCnt++;
            }
        String sumTitle = "Итого";
        if (order.getOrgUnit().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.FACULTY))
            sumTitle = sumTitle + " по факультету";
        if (order.getOrgUnit().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH))
            sumTitle = sumTitle + " по филилалу";
        if (order.getOrgUnit().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE))
            sumTitle = sumTitle + " по институту";
        injectModifier.put("sumTitle", sumTitle);
        injectModifier.put("students", String.valueOf(sumCnt));
        injectModifier.put("sum", new DoubleFormatter(2, true, true).format(UnispUtils.getWrappedLongValue(sumPayments)));
        injectModifier.put("sumW", NumberConvertingUtil.writeSum(sumPayments, 3));

        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonStudentPaymentOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);


        CommonStudentPaymentOrderPrint.injectReports(document, order, paragraphList);
        CommonStudentPaymentOrderPrint.injectParagraphs(document, order, UnispDaoFacade.getUnispPrintDAO().prepareOrderParagraphsData(order));

        return document;
    }
}