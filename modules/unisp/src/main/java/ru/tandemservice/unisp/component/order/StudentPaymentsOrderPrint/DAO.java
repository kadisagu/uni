/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentsOrderPrint;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unisp.dao.UnispDaoFacade;
import ru.tandemservice.unisp.entity.orders.StudentPaymentOrderRelation;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author dseleznev
 * Created on: 12.01.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrder(getNotNull(StudentPaymentsOrder.class, model.getOrder().getId()));

         String stateCode = model.getOrder().getState().getCode();

        // если приказ на согласовании, согласован или проведен, то печатная форма должна быть сохранена
         if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(stateCode)
                || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(stateCode)
        //        || UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(stateCode)
         )
        {
        //    // приказ проведен => печатная форма сохранена
            Criteria c = getSession().createCriteria(StudentPaymentOrderRelation.class);
            c.add(Restrictions.eq(StudentPaymentOrderRelation.L_ORDER, model.getOrder()));
            StudentPaymentOrderRelation rel = (StudentPaymentOrderRelation) c.uniqueResult();
            model.setData(rel.getText());
        } else
        {
            model.setData(UnispDaoFacade.getUnispExtractDAO().getTemplate(model.getOrder().getType(), 1));
        }
    }
}