package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Параграф приказа о назначении выплат студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentParagraphGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph";
    public static final String ENTITY_NAME = "studentPaymentParagraph";
    public static final int VERSION_HASH = -408131197;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_NUMBER = "number";

    private StudentPaymentsOrder _order;     // Приказ о назначении выплат студентам
    private int _number;     // Номер

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о назначении выплат студентам. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentsOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ о назначении выплат студентам. Свойство не может быть null.
     */
    public void setOrder(StudentPaymentsOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentParagraphGen)
        {
            setOrder(((StudentPaymentParagraph)another).getOrder());
            setNumber(((StudentPaymentParagraph)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentParagraphGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentParagraph.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentParagraph();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((StudentPaymentsOrder) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return StudentPaymentsOrder.class;
                case "number":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentParagraph> _dslPath = new Path<StudentPaymentParagraph>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentParagraph");
    }
            

    /**
     * @return Приказ о назначении выплат студентам. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph#getOrder()
     */
    public static StudentPaymentsOrder.Path<StudentPaymentsOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends StudentPaymentParagraph> extends EntityPath<E>
    {
        private StudentPaymentsOrder.Path<StudentPaymentsOrder> _order;
        private PropertyPath<Integer> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о назначении выплат студентам. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph#getOrder()
     */
        public StudentPaymentsOrder.Path<StudentPaymentsOrder> order()
        {
            if(_order == null )
                _order = new StudentPaymentsOrder.Path<StudentPaymentsOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(StudentPaymentParagraphGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return StudentPaymentParagraph.class;
        }

        public String getEntityName()
        {
            return "studentPaymentParagraph";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
