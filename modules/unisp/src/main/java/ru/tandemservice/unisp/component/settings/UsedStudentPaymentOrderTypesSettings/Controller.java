package ru.tandemservice.unisp.component.settings.UsedStudentPaymentOrderTypesSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisp.IUnispComponents;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 31.01.11
 * Time: 11:01
 * To change this template use File | Settings | File Templates.
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    protected void prepareListDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<StudentPaymentExtractType> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Название", StudentPaymentExtractType.title()).setClickable(false).setTreeColumn(true));
        dataSource.addColumn(new ToggleColumn("Основание", StudentPaymentExtractType.hasBasic().s()).setListener("onClickChangeReason"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem"));
        getModel(component).setDataSource(dataSource);
    }

    public void onClickChangeReason(IBusinessComponent component)
    {
        getDao().updateHasBasic((Long) component.getListenerParameter());
    }

    public void onClickEditItem(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUnispComponents.USED_STUDENT_PAYMENT_ORDER_TYPES_EDIT, new ParametersMap().add("studentPaymentExtractTypeId", (Long) component.getListenerParameter())));
    }
}
