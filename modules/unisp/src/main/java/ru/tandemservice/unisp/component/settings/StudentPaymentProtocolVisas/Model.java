// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.StudentPaymentProtocolVisas;

import java.util.List;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author oleyba
 * @since 08.02.2010
 */
public class Model
{
    private ISelectModel orgUnitModel;
    private ISelectModel employeePostModel;

    private OrgUnit orgUnit;
    private List<EmployeePost> employeePostList;

    public ISelectModel getOrgUnitModel()
    {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel)
    {
        this.orgUnitModel = orgUnitModel;
    }

    public ISelectModel getEmployeePostModel()
    {
        return employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        this.employeePostModel = employeePostModel;
    }

    public OrgUnit getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    public List<EmployeePost> getEmployeePostList()
    {
        return employeePostList;
    }

    public void setEmployeePostList(List<EmployeePost> employeePostList)
    {
        this.employeePostList = employeePostList;
    }
}
