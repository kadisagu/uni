/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.ReasonToBasicSettingsEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractBasic;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;
import ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToBasicsRel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 28.12.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setReason(get(StudentPaymentExtractReason.class, model.getStudentPaymentExtractReasonId()));

        model.setSelectedBasicsList(getSelectedBasicsList(model));
        model.setStudentPaymentBasicsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(StudentPaymentExtractBasic.ENTITY_CLASS, "b");
                builder.add(MQExpression.like("b", StudentPaymentExtractBasic.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("b", StudentPaymentExtractBasic.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
    }

    @Override
    public void update(Model model)
    {
        List<Long> reasonsToStay = new ArrayList<>();
        MQBuilder builder = new MQBuilder(StudentPaymentReasonToBasicsRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StudentPaymentReasonToBasicsRel.L_FIRST, model.getReason()));
        List<StudentPaymentReasonToBasicsRel> relsList = builder.getResultList(getSession());
        for(StudentPaymentReasonToBasicsRel rel : relsList)
            if(!model.getSelectedBasicsList().contains(rel.getSecond()))
                getSession().delete(rel);
            else
                reasonsToStay.add(rel.getSecond().getId());

        for (StudentPaymentExtractBasic basic : model.getSelectedBasicsList())
            if (!reasonsToStay.contains(basic.getId()))
            {
                StudentPaymentReasonToBasicsRel rel = new StudentPaymentReasonToBasicsRel();
                rel.setFirst(model.getReason());
                rel.setSecond(basic);
                getSession().save(rel);
            }
    }

    private List<StudentPaymentExtractBasic> getSelectedBasicsList(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentPaymentReasonToBasicsRel.ENTITY_CLASS, "rel", new String[] {StudentPaymentReasonToBasicsRel.L_SECOND});
        builder.add(MQExpression.eq("rel", StudentPaymentReasonToBasicsRel.L_FIRST, model.getReason()));
        builder.addOrder("rel", StudentPaymentReasonToBasicsRel.L_SECOND + "." + StudentPaymentExtractBasic.P_TITLE);
        return builder.getResultList(getSession());
    }
}