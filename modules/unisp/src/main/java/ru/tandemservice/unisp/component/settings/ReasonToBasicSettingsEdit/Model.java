/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.ReasonToBasicSettingsEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractBasic;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;

/**
 * @author dseleznev
 * Created on: 28.12.2009
 */
@Input(keys = {"studentPaymentExtractReasonId", "studentPaymentExtractReasonId"})
public class Model
{
    private Long _studentPaymentExtractReasonId;
    private StudentPaymentExtractReason _reason;

    private IMultiSelectModel _studentPaymentBasicsListModel;
    private List<StudentPaymentExtractBasic> _selectedBasicsList;

    public Long getStudentPaymentExtractReasonId()
    {
        return _studentPaymentExtractReasonId;
    }

    public void setStudentPaymentExtractReasonId(Long studentPaymentExtractReasonId)
    {
        this._studentPaymentExtractReasonId = studentPaymentExtractReasonId;
    }

    public StudentPaymentExtractReason getReason()
    {
        return _reason;
    }

    public void setReason(StudentPaymentExtractReason reason)
    {
        this._reason = reason;
    }

    public IMultiSelectModel getStudentPaymentBasicsListModel()
    {
        return _studentPaymentBasicsListModel;
    }

    public void setStudentPaymentBasicsListModel(IMultiSelectModel studentPaymentBasicsListModel)
    {
        this._studentPaymentBasicsListModel = studentPaymentBasicsListModel;
    }

    public List<StudentPaymentExtractBasic> getSelectedBasicsList()
    {
        return _selectedBasicsList;
    }

    public void setSelectedBasicsList(List<StudentPaymentExtractBasic> selectedBasicsList)
    {
        this._selectedBasicsList = selectedBasicsList;
    }
}