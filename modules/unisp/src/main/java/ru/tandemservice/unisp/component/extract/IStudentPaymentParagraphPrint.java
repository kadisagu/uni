/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.extract;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;

import ru.tandemservice.unisp.component.order.VirtualParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;

/**
 * @author dseleznev
 * Created on: 13.01.2010
 */
public interface IStudentPaymentParagraphPrint<T extends StudentPaymentExtract>
{
   RtfInjectModifier createParagraphInjectModifier(VirtualParagraph<T> paragraph, int paragraphNumber);

   RtfTableModifier createParagraphTableModifier(VirtualParagraph<T> paragraph);

    String createParagraphText(VirtualParagraph<T> paragraph);
}