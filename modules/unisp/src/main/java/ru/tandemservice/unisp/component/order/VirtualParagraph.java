/**
 * $Id$
 */
package ru.tandemservice.unisp.component.order;

import java.util.List;

import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;

/**
 * @author dseleznev
 * Created on: 13.01.2010
 */
public class VirtualParagraph<T extends StudentPaymentExtract>
{
    private List<T> _extractsList;
    
    public VirtualParagraph(List<T> extractsList)
    {
        this._extractsList = extractsList;
    }
    
    public T getFirstExtract()
    {
        return _extractsList.get(0);
    }

    public List<T> getExtractsList()
    {
        return _extractsList;
    }

    public void setExtractsList(List<T> extractsList)
    {
        this._extractsList = extractsList;
    }
}