package ru.tandemservice.unisp.component.settings.PrintNameStudentPaymentEdit;

import org.tandemframework.core.entity.IEntity;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisp.entity.catalog.IPrintablePayment;

/**
 * 
 * @author nkokorina
 * Created on: 18.02.2010
 */

public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        IEntity payment = getNotNull(model.getStudentPaymentId());
        model.setStudentPayment((IPrintablePayment)payment);
    }

    @Override
    public void update(Model model)
    {
        update(model.getStudentPayment());
    }
}
