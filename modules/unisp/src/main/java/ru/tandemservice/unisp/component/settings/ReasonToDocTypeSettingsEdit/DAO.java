/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.ReasonToDocTypeSettingsEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToExtractTypeRel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dseleznev
 * Created on: 28.12.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setExtractType(get(StudentPaymentExtractType.class, model.getStudentPaymentExtractTypeId()));

        model.setSelectedReasonsList(getSelectedReasonsList(model));
        model.setStudentPaymentReasonsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(StudentPaymentExtractReason.ENTITY_CLASS, "sp");
                builder.add(MQExpression.like("sp", StudentPaymentExtractReason.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("sp", StudentPaymentExtractReason.P_TITLE);
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });
    }

    @Override
    public void update(Model model)
    {
        List<Long> reasonsToStay = new ArrayList<>();
        MQBuilder builder = new MQBuilder(StudentPaymentReasonToExtractTypeRel.ENTITY_CLASS, "rel");
        builder.add(MQExpression.eq("rel", StudentPaymentReasonToExtractTypeRel.L_FIRST, model.getDocType()));
        List<StudentPaymentReasonToExtractTypeRel> relsList = builder.getResultList(getSession());
        for(StudentPaymentReasonToExtractTypeRel rel : relsList)
            if(!model.getSelectedReasonsList().contains(rel.getSecond()))
                getSession().delete(rel);
            else
                reasonsToStay.add(rel.getSecond().getId());

        for (StudentPaymentExtractReason reason : model.getSelectedReasonsList())
            if (!reasonsToStay.contains(reason.getId()))
            {
                StudentPaymentReasonToExtractTypeRel rel = new StudentPaymentReasonToExtractTypeRel();
                rel.setFirst(model.getDocType());
                rel.setSecond(reason);
                getSession().save(rel);
            }
    }

    private List<StudentPaymentExtractReason> getSelectedReasonsList(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentPaymentReasonToExtractTypeRel.ENTITY_CLASS, "rel", new String[] {StudentPaymentReasonToExtractTypeRel.L_SECOND});
        builder.add(MQExpression.eq("rel", StudentPaymentReasonToExtractTypeRel.L_FIRST, model.getDocType()));
        builder.addOrder("rel", StudentPaymentReasonToExtractTypeRel.L_SECOND + "." + StudentPaymentExtractReason.P_TITLE);
        return builder.getResultList(getSession());
    }
}