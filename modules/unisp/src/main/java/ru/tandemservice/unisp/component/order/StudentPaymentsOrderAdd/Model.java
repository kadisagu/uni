/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentsOrderAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.List;

/**
 * @author dseleznev
 * Created on: 23.12.2009
 */
@Input( { 
    @Bind(key = "orgUnitId", binding = "orgUnitId"), 
    @Bind(key = "orderId", binding = "order.id") 
    })
public class Model
{
    private Long _orgUnitId;
    private OrgUnit _orgUnit;

    private StudentPaymentsOrder _order = new StudentPaymentsOrder();

    private List<StudentPaymentExtractType> _orderTypesList;
    private ISelectModel _educationYearsList;

    private ISelectModel _yearPartModel;
    private ISelectModel _forOrderChangingListModel;
    private ISelectModel _employeePostModel;
    private ISelectModel _orgUnitListModel;

    private EmployeePost _employeePost;
    
    private boolean _addForm;
    private boolean _editForm;

    private boolean orderBasicVisible;

    public boolean isOrderBasicVisible()
    {
        return orderBasicVisible;
    }

    public void setOrderBasicVisible(boolean orderBasicVisible)
    {
        this.orderBasicVisible = orderBasicVisible;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        this._orgUnit = orgUnit;
    }

    public StudentPaymentsOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentPaymentsOrder order)
    {
        this._order = order;
    }

    public List<StudentPaymentExtractType> getOrderTypesList()
    {
        return _orderTypesList;
    }

    public void setOrderTypesList(List<StudentPaymentExtractType> orderTypesList)
    {
        this._orderTypesList = orderTypesList;
    }

    public ISelectModel getEducationYearsList()
    {
        return _educationYearsList;
    }

    public void setEducationYearsList(ISelectModel educationYearsList)
    {
        this._educationYearsList = educationYearsList;
    }

    public ISelectModel getYearPartModel()
    {
        return _yearPartModel;
    }

    public void setYearPartModel(ISelectModel yearPartModel)
    {
        _yearPartModel = yearPartModel;
    }

    public ISelectModel getForOrderChangingListModel()
    {
        return _forOrderChangingListModel;
    }

    public void setForOrderChangingListModel(ISelectModel forOrderChangingListModel)
    {
        this._forOrderChangingListModel = forOrderChangingListModel;
    }

    public ISelectModel getEmployeePostModel()
    {
        return _employeePostModel;
    }

    public void setEmployeePostModel(ISelectModel employeePostModel)
    {
        this._employeePostModel = employeePostModel;
    }

    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        this._orgUnitListModel = orgUnitListModel;
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        this._employeePost = employeePost;
    }

    public boolean isAddForm()
    {
        return _addForm;
    }

    public void setAddForm(boolean addForm)
    {
        this._addForm = addForm;
    }

    public boolean isEditForm()
    {
        return _editForm;
    }

    public void setEditForm(boolean editForm)
    {
        this._editForm = editForm;
    }
}