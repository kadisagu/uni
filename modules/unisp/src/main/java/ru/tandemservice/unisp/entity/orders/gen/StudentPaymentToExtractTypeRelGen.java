package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.StudentPaymentToExtractTypeRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выплаты с типом приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentToExtractTypeRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentToExtractTypeRel";
    public static final String ENTITY_NAME = "studentPaymentToExtractTypeRel";
    public static final int VERSION_HASH = -795915292;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIRST = "first";
    public static final String L_SECOND = "second";

    private StudentPaymentExtractType _first;     // Тип документа о назначении выплат
    private StudentPayment _second;     // Выплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип документа о назначении выплат. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentExtractType getFirst()
    {
        return _first;
    }

    /**
     * @param first Тип документа о назначении выплат. Свойство не может быть null.
     */
    public void setFirst(StudentPaymentExtractType first)
    {
        dirty(_first, first);
        _first = first;
    }

    /**
     * @return Выплаты. Свойство не может быть null.
     */
    @NotNull
    public StudentPayment getSecond()
    {
        return _second;
    }

    /**
     * @param second Выплаты. Свойство не может быть null.
     */
    public void setSecond(StudentPayment second)
    {
        dirty(_second, second);
        _second = second;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentToExtractTypeRelGen)
        {
            setFirst(((StudentPaymentToExtractTypeRel)another).getFirst());
            setSecond(((StudentPaymentToExtractTypeRel)another).getSecond());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentToExtractTypeRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentToExtractTypeRel.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentToExtractTypeRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "first":
                    return obj.getFirst();
                case "second":
                    return obj.getSecond();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "first":
                    obj.setFirst((StudentPaymentExtractType) value);
                    return;
                case "second":
                    obj.setSecond((StudentPayment) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "first":
                        return true;
                case "second":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "first":
                    return true;
                case "second":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "first":
                    return StudentPaymentExtractType.class;
                case "second":
                    return StudentPayment.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentToExtractTypeRel> _dslPath = new Path<StudentPaymentToExtractTypeRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentToExtractTypeRel");
    }
            

    /**
     * @return Тип документа о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentToExtractTypeRel#getFirst()
     */
    public static StudentPaymentExtractType.Path<StudentPaymentExtractType> first()
    {
        return _dslPath.first();
    }

    /**
     * @return Выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentToExtractTypeRel#getSecond()
     */
    public static StudentPayment.Path<StudentPayment> second()
    {
        return _dslPath.second();
    }

    public static class Path<E extends StudentPaymentToExtractTypeRel> extends EntityPath<E>
    {
        private StudentPaymentExtractType.Path<StudentPaymentExtractType> _first;
        private StudentPayment.Path<StudentPayment> _second;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип документа о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentToExtractTypeRel#getFirst()
     */
        public StudentPaymentExtractType.Path<StudentPaymentExtractType> first()
        {
            if(_first == null )
                _first = new StudentPaymentExtractType.Path<StudentPaymentExtractType>(L_FIRST, this);
            return _first;
        }

    /**
     * @return Выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentToExtractTypeRel#getSecond()
     */
        public StudentPayment.Path<StudentPayment> second()
        {
            if(_second == null )
                _second = new StudentPayment.Path<StudentPayment>(L_SECOND, this);
            return _second;
        }

        public Class getEntityClass()
        {
            return StudentPaymentToExtractTypeRel.class;
        }

        public String getEntityName()
        {
            return "studentPaymentToExtractTypeRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
