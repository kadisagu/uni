package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtractTextRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки и текста выписки (приказы по выплатам студентам)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentExtractTextRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentExtractTextRelation";
    public static final String ENTITY_NAME = "studentPaymentExtractTextRelation";
    public static final int VERSION_HASH = -42365611;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String P_CODE = "code";
    public static final String P_TEXT = "text";

    private StudentPaymentExtract _extract;     // Абстрактная выписка из списочного приказа по выплатам студентам
    private int _code;     // Код печатной формы
    private byte[] _text;     // Сохраненная печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абстрактная выписка из списочного приказа по выплатам студентам. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Абстрактная выписка из списочного приказа по выплатам студентам. Свойство не может быть null.
     */
    public void setExtract(StudentPaymentExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Код печатной формы. Свойство не может быть null.
     */
    @NotNull
    public int getCode()
    {
        return _code;
    }

    /**
     * @param code Код печатной формы. Свойство не может быть null.
     */
    public void setCode(int code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     */
    @NotNull
    public byte[] getText()
    {
        return _text;
    }

    /**
     * @param text Сохраненная печатная форма. Свойство не может быть null.
     */
    public void setText(byte[] text)
    {
        dirty(_text, text);
        _text = text;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentExtractTextRelationGen)
        {
            setExtract(((StudentPaymentExtractTextRelation)another).getExtract());
            setCode(((StudentPaymentExtractTextRelation)another).getCode());
            setText(((StudentPaymentExtractTextRelation)another).getText());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentExtractTextRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentExtractTextRelation.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentExtractTextRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "code":
                    return obj.getCode();
                case "text":
                    return obj.getText();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((StudentPaymentExtract) value);
                    return;
                case "code":
                    obj.setCode((Integer) value);
                    return;
                case "text":
                    obj.setText((byte[]) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "code":
                        return true;
                case "text":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "code":
                    return true;
                case "text":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return StudentPaymentExtract.class;
                case "code":
                    return Integer.class;
                case "text":
                    return byte[].class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentExtractTextRelation> _dslPath = new Path<StudentPaymentExtractTextRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentExtractTextRelation");
    }
            

    /**
     * @return Абстрактная выписка из списочного приказа по выплатам студентам. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtractTextRelation#getExtract()
     */
    public static StudentPaymentExtract.Path<StudentPaymentExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Код печатной формы. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtractTextRelation#getCode()
     */
    public static PropertyPath<Integer> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtractTextRelation#getText()
     */
    public static PropertyPath<byte[]> text()
    {
        return _dslPath.text();
    }

    public static class Path<E extends StudentPaymentExtractTextRelation> extends EntityPath<E>
    {
        private StudentPaymentExtract.Path<StudentPaymentExtract> _extract;
        private PropertyPath<Integer> _code;
        private PropertyPath<byte[]> _text;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абстрактная выписка из списочного приказа по выплатам студентам. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtractTextRelation#getExtract()
     */
        public StudentPaymentExtract.Path<StudentPaymentExtract> extract()
        {
            if(_extract == null )
                _extract = new StudentPaymentExtract.Path<StudentPaymentExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Код печатной формы. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtractTextRelation#getCode()
     */
        public PropertyPath<Integer> code()
        {
            if(_code == null )
                _code = new PropertyPath<Integer>(StudentPaymentExtractTextRelationGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сохраненная печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentExtractTextRelation#getText()
     */
        public PropertyPath<byte[]> text()
        {
            if(_text == null )
                _text = new PropertyPath<byte[]>(StudentPaymentExtractTextRelationGen.P_TEXT, this);
            return _text;
        }

        public Class getEntityClass()
        {
            return StudentPaymentExtractTextRelation.class;
        }

        public String getEntityName()
        {
            return "studentPaymentExtractTextRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
