package ru.tandemservice.unisp.component.settings.UsedStudentPaymentOrderTypesSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 31.01.11
 * Time: 11:02
 * To change this template use File | Settings | File Templates.
 */
public class Model
{
    private DynamicListDataSource<StudentPaymentExtractType> _dataSource;

    public DynamicListDataSource<StudentPaymentExtractType> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StudentPaymentExtractType> dataSource)
    {
        this._dataSource = dataSource;
    }
}
