package ru.tandemservice.unisp.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.common.ITitled;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author root
 */
public interface IUnispExtractDAO {
    SpringBeanCache<IUnispExtractDAO> instance = new SpringBeanCache<IUnispExtractDAO>(IUnispExtractDAO.class.getName());

    /** описание платежа (итогового) */
    public interface IStudentPaymentDescription extends ITitled, IIdentifiable {
        StudentPayment getPaymenType();
        Date getPaymentStartDate();
        Date getPaymentStopDate();
        Double getPaymentCostAsDouble();
        Double getPaymentTotalCostAsDouble();
    }

    /**
     * Возвращает список платежей, которые перекрывают платежи из списка
     * @param appointmentList - список платежей, которые надо проверить
     * @return { appointmentExtract.id -> { overlappedExrtractDescription } }
     */
    Map<Long, Collection<IStudentPaymentDescription>> findIntersections(Collection<StudentPaymentAppointmentExtract> appointmentList);

    /**
     * Возвращает список платежей для каждого студента в рамках приказа
     * @param order - приказ
     * @param studentIds - список идентификаторов студентов
     * @return { student.id -> { extract-row } }
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    Map<Long, List<IStudentPaymentDescription>> getTotalStudentExtractListMap(StudentPaymentsOrder order, Collection<Long> studentIds);

    /**
     * Добавляет в итоговый список платежей дополнительные элементы
     * @param totalStudentExtractListMap - результат вфзова функции getTotalStudentExtractListMap
     * @param appointmentExtractList - список выписок о назначении, которые необходимо добавить
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    void applyStudentPaymentAppointmentList(Map<Long, List<IStudentPaymentDescription>> totalStudentExtractListMap, Collection<StudentPaymentAppointmentExtract> appointmentExtractList);

    /**
     * Возвращает размер полного фактического фонда по списку выписок.
     * Применяется для расчета фактического фонда для приказа.
     * @param extractsMap - список платежей для каждого студента в рамках приказа
     * @return размер полного фактического фонда.
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    double getTotalOrderFactFund(Map<Long, List<IUnispExtractDAO.IStudentPaymentDescription>> extractsMap);
    
    /**
     * Возвращает размер месячного фактического фонда по списку выписок.
     * Применяется для расчета фактического фонда для приказа, без домножения сумм на количество месяцев.
     * @param extractsMap - список платежей для каждого студента в рамках приказа
     * @return размер полного фактического фонда.
     */
    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    double getTotalOrderMonthFactFund(final Map<Long, List<IUnispExtractDAO.IStudentPaymentDescription>> extractsMap);

    /**
     * создает параграф в приказе со следующим номером
     * @param order приказ
     * @return параграф
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    StudentPaymentParagraph doCreateParagraph(StudentPaymentsOrder order);


    /**
     * `Грамотно` (со всеми блокировками) создает список выписок в приказе
     * @param order приказ
     * @param appointmentList список transient выписок
     * @param errorCollectorPrefix префикс для имен полей в ErrorCollectorе (см. исходник)
     * @return true, если операция выполнена успешно
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    StudentPaymentParagraph doCreateAppointmentExtractList(StudentPaymentsOrder order, Collection<StudentPaymentAppointmentExtract> appointmentList, String errorCollectorPrefix);

    /**
     * Возвращает печатный шаблон по заданному типу выписки (приказа) и индексу
     * @param тип документа модуля "выплаты студентам"
     * @return печатный шаблон  
     */
    byte[] getTemplate(StudentPaymentExtractType extractType, int templateIndex);
    
    /**
     * Возвращает список типов приказов о назначении выплат 
     * с которыми может работать пользователь
     */
    List<StudentPaymentExtractType> getStudentPaymentOrderTypesList(final String filter, OrgUnit orgUnit);
    
    /**
     * Проверяет уникальность номера приказа в рамках календарного года
     */
    void checkOrderNumber(StudentPaymentsOrder order);
}