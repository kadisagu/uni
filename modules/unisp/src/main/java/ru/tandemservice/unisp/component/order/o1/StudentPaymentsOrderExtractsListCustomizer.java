/**
 * $Id$
 */
package ru.tandemservice.unisp.component.order.o1;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unisp.component.order.IOrderExtractsListCustomizer;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;

/**
 * @author dseleznev
 * Created on: 11.01.2010
 */
public class StudentPaymentsOrderExtractsListCustomizer implements IOrderExtractsListCustomizer
{
    @Override
    public void customizeParagraphList(DynamicListDataSource dataSource, IBusinessComponent component)
    {
        dataSource.addColumn(new SimpleColumn("Сумма", StudentPaymentAppointmentExtract.P_PAYMENT_COST, MoneyFormatter.ruMoneyFormatter(3)).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Выплата", new String[]{StudentPaymentAppointmentExtract.L_PAYMENT_TYPE, StudentPayment.P_SHORT_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Период", StudentPaymentAppointmentExtract.P_FORMATTED_PAYMENT_PERIOD).setClickable(false).setOrderable(false));
    }
}