/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.ReasonToDocTypeSettingsEdit;

import java.util.List;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;

/**
 * @author dseleznev
 * Created on: 28.12.2009
 */
@Input(keys = {"studentPaymentExtractTypeId", "studentPaymentExtractTypeId"})
public class Model
{
    private Long _studentPaymentExtractTypeId;
    private StudentPaymentExtractType _extractType;

    private IMultiSelectModel _studentPaymentReasonsListModel;
    private List<StudentPaymentExtractReason> _selectedReasonsList;

    public Long getStudentPaymentExtractTypeId()
    {
        return _studentPaymentExtractTypeId;
    }

    public void setStudentPaymentExtractTypeId(Long studentPaymentExtractTypeId)
    {
        this._studentPaymentExtractTypeId = studentPaymentExtractTypeId;
    }

    public StudentPaymentExtractType getDocType()
    {
        return _extractType;
    }

    public void setExtractType(StudentPaymentExtractType extractType)
    {
        this._extractType = extractType;
    }

    public IMultiSelectModel getStudentPaymentReasonsListModel()
    {
        return _studentPaymentReasonsListModel;
    }

    public void setStudentPaymentReasonsListModel(IMultiSelectModel studentPaymentReasonsListModel)
    {
        this._studentPaymentReasonsListModel = studentPaymentReasonsListModel;
    }

    public List<StudentPaymentExtractReason> getSelectedReasonsList()
    {
        return _selectedReasonsList;
    }

    public void setSelectedReasonsList(List<StudentPaymentExtractReason> selectedReasonsList)
    {
        this._selectedReasonsList = selectedReasonsList;
    }
}