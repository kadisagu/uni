/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentsOrderPub;

import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unisp.IUnispComponents;
import ru.tandemservice.unisp.UnispUtils;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;

/**
 * @author dseleznev
 *         Created on: 24.12.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
        prepareListDataSource(component);
    }

    public void prepareListDataSource(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        final DynamicListDataSource<StudentPaymentExtract> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("ФИО студента", StudentPaymentExtract.STUDENT_FIO_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", StudentPaymentExtract.STUDENT_PASSPORT_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние студента", StudentPaymentExtract.STUDENT_STATUS_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Курс", StudentPaymentExtract.STUDENT_COURSE_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группа", StudentPaymentExtract.STUDENT_GROUP_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", StudentPaymentExtract.FORMATIVE_ORGUNIT_SHORT_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", StudentPaymentExtract.TERRITORIAL_ORGUNIT_SHORT_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", StudentPaymentExtract.educationLevelHighSchoolStr().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", StudentPaymentExtract.DEVELOP_FORM_KEY).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид возм. затрат", StudentPaymentExtract.compensationTypeStr().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Категория обучаемого", StudentPaymentExtract.STUDENT_CATEGORY_KEY).setClickable(false).setOrderable(false));

        try
        {
            UnispUtils.getExtractsListComponentCustomizer(model.getOrder().getType()).customizeParagraphList(dataSource, component);
        }
        catch (Exception e)
        {
            CoreExceptionUtils.logException("error", e);
        }

        //dataSource.addColumn(new ToggleColumn("Отдельный параграф", StudentPaymentExtract.P_INDIVIDUAL_PARAGRAPH).setListener("onClickIndividual").setDisabledProperty(StudentPaymentExtract.P_NO_DELETE));

        if (!model.getOrder().isCantBeEdited())
        {
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteParagraph", "Удалить выписку из приказа?").setPermissionKey(model.getSecModel().getPermission("deleteExtract")));
        }

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component)
    {
        getModel(component).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component)
    {
        Model model = this.getModel(component);
        model.setStudentFirstName(null);
        model.setStudentLastName(null);
        onClickSearch(component);
    }

    public void onClickSendToCoordination(final IBusinessComponent component)
    {
        IPrincipalContext principalContext = component.getUserContext().getPrincipalContext();
        if (!(principalContext instanceof IPersistentPersonable))
            throw new ApplicationException(EntityRuntime.getMeta(principalContext.getId()).getTitle() + " не может отправлять документы на согласование.");
        getDao().doSendToCoordination(getModel(component), (IPersistentPersonable) principalContext);
    }

    public void onClickEdit(final IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IUnispComponents.STUDENT_PAYMENTS_ORDER_ADD_EDIT, new ParametersMap()
                .add("orgUnitId", null)
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }
    /*
    public void onClickSetExecutor(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator(IMoveStudentComponents.STUDENT_ORDER_SET_EXECUTOR, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }*/

    public void onClickReject(final IBusinessComponent component)
    {
        getDao().doReject(getModel(component));
    }

    public void onClickCommit(final IBusinessComponent component)
    {
        getDao().doCommit(getModel(component));
    }

    public void onClickSendToFormative(final IBusinessComponent component)
    {
        getDao().doSendToFormative(getModel(component));
    }

    public void onClickRollback(final IBusinessComponent component)
    {
        getDao().doRollback(getModel(component));
    }

    public void onClickDeleteOrder(final IBusinessComponent component)
    {
        deactivate(component);
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(getModel(component).getOrder());
    }

    public void onClickPrintOrder(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUnispComponents.STUDENT_PAYMENTS_ORDER_PRINT, new ParametersMap()
                .add("orderId", getModel(component).getOrder().getId())
        ));
    }

    public void onClickIndividual(IBusinessComponent component)
    {
        getDao().updateIndividualParagraph((Long) component.getListenerParameter());
    }

    public void onClickDeleteParagraph(final IBusinessComponent component)
    {
        //TODO: удаление пустых параграфов при удалении последней выписки из параграфа
        getDao().delete((Long) component.getListenerParameter());
    }

    public void onClickPrintProtocol(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUnispComponents.PROTOCOL_PRINT, new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getOrder().getId())
        ));
    }

    public void onClickPrintPaymentAllocResults(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUnispComponents.PAYMENTS_ALLOC_RESULTS_PRINT, new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getOrder().getId())
        ));
    }
}