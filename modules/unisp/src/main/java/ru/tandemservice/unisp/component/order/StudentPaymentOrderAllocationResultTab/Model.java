/* $Id: Model.java 11100 2010-01-11 08:14:04Z dseleznev $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderAllocationResultTab;

import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.IRowCustomizer;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.unisp.UnispUtils;
import ru.tandemservice.unisp.dao.IUnispExtractDAO.IStudentPaymentDescription;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.*;

/**
 * @author vdanilov
 */
@Input({
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "order.id")
})
public class Model
{
    private StudentPaymentsOrder order = new StudentPaymentsOrder();
    public StudentPaymentsOrder getOrder() { return order; }
    public void setOrder(final StudentPaymentsOrder order) { this.order = order; }

    private Map<Long, List<IStudentPaymentDescription>> data = Collections.emptyMap();
    public Map<Long, List<IStudentPaymentDescription>> getData() { return data; }
    public void setData(final Map<Long, List<IStudentPaymentDescription>> data) { this.data = data; }


    @SuppressWarnings("serial")
    public static class Filter extends IdentifiableWrapper<IEntity> {
        private final String path;
        private final Object defaultValue;

        public Filter(final String path, final String title, final Object defaultValue) {
            super((long) path.hashCode(), title);
            this.path = path;
            this.defaultValue = defaultValue;
        }

        public IdentifiableWrapper<IEntity> wrap(final Object object) {
            final Object value = FastBeanUtils.getValue(object, path);
            final String title = (value instanceof ITitled ? ((ITitled)value).getTitle() : value != null? String.valueOf(value) : String.valueOf(defaultValue));
            final Long id = (value instanceof IEntity ? ((IEntity)value).getId() : Long.valueOf(title.hashCode()));
            return new IdentifiableWrapper<>(id, title);
        }
    }

    private final List<Filter> filters = new ArrayList<>();
    public List<Filter> getFilters() { return filters; }

    private IMultiSelectModel filtersModel = new FullCheckSelectModel() {
        @Override public ListResult findValues(final String filter) { return new ListResult<>(getFilters()); }
    };
    public IMultiSelectModel getFiltersModel() { return filtersModel; }


    private List<Filter> selectedFilters = Collections.emptyList();
    public List<Filter> getSelectedFilters() { return selectedFilters; }
    public void setSelectedFilters(final List<Filter> selectedFilters) { this.selectedFilters = selectedFilters; }

    private final Map<Long, SelectModel<Collection<IdentifiableWrapper<IEntity>>>> filtersDataMap = new HashMap<>();
    public Map<Long, SelectModel<Collection<IdentifiableWrapper<IEntity>>>> getFiltersDataMap() { return filtersDataMap; }

    private Filter currentFilter;
    public Filter getCurrentFilter() { return currentFilter; }
    public void setCurrentFilter(final Filter currentFilter) { this.currentFilter = currentFilter; }

    public SelectModel<Collection<IdentifiableWrapper<IEntity>>> getCurrentFilterModel() { return filtersDataMap.get(getCurrentFilter().getId()); }

    private boolean onlySelectedElements = true;
    public boolean isOnlySelectedElements() { return onlySelectedElements; }
    public void setOnlySelectedElements(final boolean onlySelectedElements) { this.onlySelectedElements = onlySelectedElements; }


    @SuppressWarnings("serial")
    public static class Node extends IdentifiableWrapper<IEntity> implements IHierarchyItem
    {
        private final Node parent;
        @Override public IHierarchyItem getHierarhyParent() { return parent; }

        private long payment;
        public long getPayment() { return payment; }

        private boolean selected;
        public boolean isSelected() { return selected; }

        public Node(final Long id, final Node parent, final String title) throws ClassCastException {
            super(id, title);
            this.parent = parent;
        }

        public void collect(final IStudentPaymentDescription value) {
            if (null != parent) { parent.collect(value); }
            payment += UnispUtils.getUnwrappedDoubleValue(value.getPaymentCostAsDouble());
        }


        public void select(final boolean value) {
            if (null != parent) { parent.select(value); }
            selected |= value;
        }

    }

    private SimpleListDataSource<Node> dataSource = new SimpleListDataSource<Node>(Collections.<Node>emptyList()) {
        {
            addColumn(new SimpleColumn("Назначенная выплата", "title").setTreeColumn(true).setOrderable(false).setClickable(false));
            addColumn(new SimpleColumn("Распределенная сумма", "payment", MoneyFormatter.ruMoneyFormatter(3)).setOrderable(false).setClickable(false));
            setRowCustomizer(new IRowCustomizer<Node>() {
                @Override public boolean isClickable(final IPublisherLinkColumn column, final Node rowEntity, final IEntity itemEntity) {
                    return false;
                }

                @Override public boolean isClickable(final AbstractColumn<?> column, final Node rowEntity) {
                    return false;
                }

                @Override public String getRowStyle(final Node entity) {
                    if (isOnlySelectedElements()) { return ""; }
                    return (entity.isSelected() ? "font-weight:bold" : "color:gray");
                }

                @Override public String getHeadRowStyle() {
                    return "";
                }
            });
        }
        @Override public long getStartRow() { return 0; };
        @Override public long getCountRow() { return getTotalSize(); }
        @Override public long getTotalSize() {return super.getTotalSize(); }
    };
    public SimpleListDataSource<Node> getDataSource() { return dataSource; }


}