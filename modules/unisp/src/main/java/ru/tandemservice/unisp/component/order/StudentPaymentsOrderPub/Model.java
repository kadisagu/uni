/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentsOrderPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.component.visa.VisaList.IVisaOwnerModel;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dseleznev
 *         Created on: 24.12.2009
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "orderId"),
        @Bind(key = "selectedTab", binding = "selectedTab"),
        @Bind(key = "selectedSubTab", binding = "selectedSubTab")
})
@Output({
        @Bind(key = "selectedPage", binding = "selectedOrgUnitPage"),
        @Bind(key = "selectedOrgUnitStudentPaymentsPage", binding = "selectedOrgUnitStudentPaymentsPage")
})
public class Model implements IVisaOwnerModel
{
    public static final String FIRST_EXTRACT = "firstExtract";
    public static final String EXTRACT_COUNT = "extractCount";

    private IDataSettings _settings;
    private CommonPostfixPermissionModel _secModel;

    private Long _orderId;
    private StudentPaymentsOrder _order;
    private StudentPaymentOrderProtocol _protocol;

    private DynamicListDataSource<StudentPaymentExtract> _dataSource;

    private String _studentFirstName;
    private String _studentLastName;

    private String _selectedSubTab;
    private String _selectedTab;
    private Long _visingStatus;

    private String _selectedOrgUnitPage = "uniSPStudentPaymentsTab";
    private String _selectedOrgUnitStudentPaymentsPage = "orgUnitStudentPaymentsSelectedTab";

    private Map<String, Object> _visaListParameters = new HashMap<>();

    {
        _visaListParameters.put("visaOwnerModel", this);
    }

    public boolean isNeedVising()
    {
        return _visingStatus != null;
    }

    // IVisaOwnerModel

    @Override
    public IAbstractDocument getDocument()
    {
        return _order;
    }

    @Override
    public boolean isVisaListModificable()
    {
        return UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(_order.getState().getCode());
    }

    @Override
    public String getSecPostfix()
    {
        return "studentPaymentOrder";
    }

    // Getters & Setters

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(final IDataSettings settings)
    {
        _settings = settings;
    }

    public CommonPostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(final CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public StudentPaymentsOrder getOrder()
    {
        return _order;
    }

    public void setOrder(final StudentPaymentsOrder order)
    {
        _order = order;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(final Long orderId)
    {
        _orderId = orderId;
    }

    public DynamicListDataSource<StudentPaymentExtract> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<StudentPaymentExtract> dataSource)
    {
        this._dataSource = dataSource;
    }

    public String getStudentFirstName()
    {
        return _studentFirstName;
    }

    public void setStudentFirstName(String studentFirstName)
    {
        this._studentFirstName = studentFirstName;
    }

    public String getStudentLastName()
    {
        return _studentLastName;
    }

    public void setStudentLastName(String studentLastName)
    {
        this._studentLastName = studentLastName;
    }

    public String getSelectedSubTab()
    {
        return _selectedSubTab;
    }

    public void setSelectedSubTab(final String selectedSubTab)
    {
        this._selectedSubTab = selectedSubTab;
    }

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(final String selectedTab)
    {
        _selectedTab = selectedTab;
    }

    public Long getVisingStatus()
    {
        return _visingStatus;
    }

    public void setVisingStatus(final Long visingStatus)
    {
        _visingStatus = visingStatus;
    }

    public Map<String, Object> getVisaListParameters()
    {
        return _visaListParameters;
    }

    public void setVisaListParameters(final Map<String, Object> visaListParameters)
    {
        _visaListParameters = visaListParameters;
    }

    public String getSelectedOrgUnitPage()
    {
        return _selectedOrgUnitPage;
    }

    public void setSelectedOrgUnitPage(String selectedOrgUnitPage)
    {
        this._selectedOrgUnitPage = selectedOrgUnitPage;
    }

    public String getSelectedOrgUnitStudentPaymentsPage()
    {
        return _selectedOrgUnitStudentPaymentsPage;
    }

    public void setSelectedOrgUnitStudentPaymentsPage(String selectedOrgUnitStudentPaymentsPage)
    {
        this._selectedOrgUnitStudentPaymentsPage = selectedOrgUnitStudentPaymentsPage;
    }

    public StudentPaymentOrderProtocol getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(StudentPaymentOrderProtocol protocol)
    {
        _protocol = protocol;
    }
}