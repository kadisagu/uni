package ru.tandemservice.unisp.component.settings.ReasonsCommentSettingsEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 28.01.11
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
public class DAO extends UniDao<ru.tandemservice.unisp.component.settings.ReasonsCommentSettingsEdit.Model> implements ru.tandemservice.unisp.component.settings.ReasonsCommentSettingsEdit.IDAO
{
    @Override
    public void prepare(ru.tandemservice.unisp.component.settings.ReasonsCommentSettingsEdit.Model model)
    {
        model.setReason(get(StudentPaymentExtractReason.class, model.getStudentPaymentExtractReasonId()));
    }

    @Override
    public void update(ru.tandemservice.unisp.component.settings.ReasonsCommentSettingsEdit.Model model)
    {
        update(model.getReason());
    }

}