package ru.tandemservice.unisp.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.unisp.entity.catalog.gen.UnispDocumentTemplateGen;

/**
 * Печатные шаблоны документов модуля «Выплаты студентам»
 */
public class UnispDocumentTemplate extends UnispDocumentTemplateGen implements ITemplateDocument
{
    @Override
    public byte[] getContent()
    {
        return CommonBaseUtil.getTemplateContent(this);
    }
}