/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.ReasonsCommentSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisp.IUnispComponents;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;

/**
 * @author dseleznev
 * Created on: 29.12.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<StudentPaymentExtractReason> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", StudentPaymentExtractReason.title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Печатное название", StudentPaymentExtractReason.printTitle().s()).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Требует примечания", StudentPaymentExtractReason.commentable().s()).setListener("onClickCommentable"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem"));
        model.setDataSource(dataSource);
    }

    public void onClickCommentable(IBusinessComponent component)
    {
        getDao().updateCommentable((Long)component.getListenerParameter());
    }

    public void onClickEditItem(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IUnispComponents.DOC_TYPE_TO_REASONS_COMMENT_EDIT, new ParametersMap().add("studentPaymentExtractReasonId", (Long) component.getListenerParameter())));
    }


}