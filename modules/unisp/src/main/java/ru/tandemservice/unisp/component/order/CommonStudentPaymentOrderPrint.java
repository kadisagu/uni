/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.unisp.UnispUtils;
import ru.tandemservice.unisp.component.extract.IStudentPaymentParagraphPrint;
import ru.tandemservice.unisp.dao.UnispDaoFacade;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 *         Created on: 11.01.2010
 */
public class CommonStudentPaymentOrderPrint
{
    public static final String REPORTS = "REPORTS";
    public static final String PARAGRAPHS = "PARAGRAPHS";
    public static final String STUDENT_LIST = "STUDENT_LIST";

    /*public static final IStudentListParagraphPrintFormatter STUDENT_LIST_FORMATTER = new IStudentListParagraphPrintFormatter()
    {
        @Override
        public String formatSingleStudent(Student student, int extractNumber)
        {
            StringBuilder buffer = new StringBuilder();
            buffer.append("\\par ").append(extractNumber).append(".  ");
            buffer.append(student.getPerson().getFullFio()).append(" (");
            buffer.append(student.getCompensationType().getShortTitle()).append(")");
            return buffer.toString();
        }
    };*/

    public static RtfInjectModifier createListOrderInjectModifier(StudentPaymentsOrder order)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("commitDate", order.getCommitDate() == null ? "          " : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        injectModifier.put("orderNumber", StringUtils.isEmpty(order.getNumber()) ? "          " : order.getNumber());
        injectModifier.put("eduYear", order.getEducationYear().getTitle());
        String executor = order.getExecutor();
        injectModifier.put("executor", "");
        injectModifier.put("executorPhone", "");
        if (StringUtils.isNotEmpty(executor))
        {
            int phoneStart = executor.indexOf(" т. ");
            if (phoneStart == -1)
            {
                injectModifier.put("executor", executor);
            } else
            {
                injectModifier.put("executor", executor.substring(0, phoneStart));
                injectModifier.put("executorPhone", executor.substring(phoneStart + 1));
            }
        }

        EmployeePost post = (EmployeePost) TopOrgUnit.getInstance().getHead();
        if (null != post) injectModifier.put("rector", post.getPerson().getIdentityCard().getIof());
        else injectModifier.put("rector", ""); //TODO

        initOrgUnit(injectModifier, order.getOrgUnit(), "formativeOrgUnit", "");


        return injectModifier;
    }

    public static RtfTableModifier createListOrderTableModifier(StudentPaymentsOrder order)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        List<String[]> visaData = new ArrayList<>();
        List<String[]> primaryVisaData = new ArrayList<>();
        List<String[]> secondaryVisaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());

        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            if (visa.getGroupMemberVising().getCode().equals("2"))
                primaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
            else
                secondaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        tableModifier.put("PRIMARY_VISAS", primaryVisaData.toArray(new String[primaryVisaData.size()][]));
        tableModifier.put("SECONDARY_VISAS", secondaryVisaData.toArray(new String[secondaryVisaData.size()][]));
        return tableModifier;
    }

    public static void injectReports(final RtfDocument document, StudentPaymentsOrder order, List<VirtualParagraph> paragraphsList)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, REPORTS);

        // 2. Если нашли, то вместо него вставляем все отчеты
        if (rtfSearchResult.isFound())
        {
            int repCounter = 1;
            List<IRtfElement> parList = new ArrayList<>();

            for (VirtualParagraph paragraph : paragraphsList)
            {
                RtfDocument reportPart = getReportPart(order.getType(), paragraph, repCounter++);
                RtfUtil.modifySourceList(document.getHeader(), reportPart.getHeader(), reportPart.getElementList());
                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(reportPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    public static void injectParagraphs(final RtfDocument document, StudentPaymentsOrder order, List<VirtualParagraph> paragraphsList)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            List<IRtfElement> parList = new ArrayList<>();
            int paragraphNumber=1;
            for (VirtualParagraph paragraph : paragraphsList)
            {
                RtfDocument reportPart = getParagraphPart(paragraph, paragraphNumber);
                RtfUtil.modifySourceList(document.getHeader(), reportPart.getHeader(), reportPart.getElementList());
                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(reportPart.getElementList());

                //TODO: hack for USUE
                boolean pageBreaksNeeded = Boolean.getBoolean(ApplicationRuntime.getProperty("unisp.listorder.insertPageBreaksAfterEachParagraph"));

                if (pageBreaksNeeded&&paragraphsList.indexOf(paragraph) < paragraphsList.size() - 1)
                {
                    rtfGroup.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.SECT));
                    rtfGroup.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.SECTD));
                }

                paragraphNumber++;
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    public static RtfDocument getReportPart(StudentPaymentExtractType orderType, VirtualParagraph paragraph, int repNumber)
    {
        byte[] reportTemplate = UnispDaoFacade.getUnispExtractDAO().getTemplate(orderType, 2);

        RtfDocument reportPart = new RtfReader().read(reportTemplate);
        IOrderReportPrint reportPrint = UnispUtils.getOrderReportPrintController(orderType);
        RtfInjectModifier injectModifier = reportPrint.createReportInjectModifier(paragraph, repNumber);
        RtfTableModifier tableModifier = reportPrint.createReportTableModifier(paragraph);
        injectModifier.modify(reportPart);
        tableModifier.modify(reportPart);
        return reportPart;
    }

    @SuppressWarnings("unchecked")
    public static RtfDocument getParagraphPart(VirtualParagraph paragraph, int paragraphNumber)
    {
        byte[] paragraphTemplate = UnispDaoFacade.getUnispExtractDAO().getTemplate(paragraph.getFirstExtract().getType(), 3);

        RtfDocument paragraphPart = new RtfReader().read(paragraphTemplate);
        IStudentPaymentParagraphPrint printForm = UnispUtils.getOrderParagraphPrintController(paragraph.getFirstExtract().getType());
        RtfInjectModifier injectModifier = printForm.createParagraphInjectModifier(paragraph, paragraphNumber);
        RtfTableModifier tableModifier = printForm.createParagraphTableModifier(paragraph);
        injectModifier.modify(paragraphPart);
        tableModifier.modify(paragraphPart);
        return paragraphPart;
    }

    /*public static String createParagraphText(IAbstractParagraph paragraph)
    {
        return createParagraphText(paragraph, STUDENT_LIST_FORMATTER);
    }*/

    /*public static String createParagraphText(IAbstractParagraph paragraph, IStudentListParagraphPrintFormatter formatter)
    {
        // получаем студентов из параграфа
        StringBuilder buffer = new StringBuilder();
        int counter = 1;
        for (ListStudentExtract enrollmentExtract : (List<ListStudentExtract>) paragraph.getExtractList())
            buffer.append(formatter.formatSingleStudent(enrollmentExtract.getEntity(), counter++));
        buffer.append("\\par");
        return buffer.toString();
    }*/

    public static void initOrgUnit(RtfInjectModifier modifier, OrgUnit orgUnit, String prefix, String postfix)
    {
        String title = "";
        String nominative = null;
        String genitive = null;
        String dative = null;
        String prepositional = null;
        if (orgUnit != null)
        {
            title = orgUnit.getFullTitle();
            nominative = orgUnit.getNominativeCaseTitle();
            genitive = orgUnit.getGenitiveCaseTitle();
            dative = orgUnit.getDativeCaseTitle();
            prepositional = orgUnit.getPrepositionalCaseTitle();
        }
        modifier.put(prefix + postfix, nominative == null ? title : nominative);
        modifier.put(prefix + postfix + "_G", genitive == null ? title : genitive);
        modifier.put(prefix + postfix + "_D", dative == null ? title : dative);
        modifier.put(prefix + postfix + "_P", prepositional == null ? title : prepositional);
    }

    public static void initDevelopForm(RtfInjectModifier modifier, DevelopForm developForm, String postfix)
    {
        final String devForm = "developForm" + postfix;
        switch (developForm.getCode())
        {
            case DevelopFormCodes.FULL_TIME_FORM:
                modifier.put("DEVELOPFORM", "ОЧНОЕ ОТДЕЛЕНИЕ");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "очное");
                modifier.put(devForm + "_G", "очного");
                modifier.put(devForm + "_D", "очному");
                modifier.put(devForm + "_A", "очное");
                modifier.put(devForm + "_I", "очным");
                modifier.put(devForm + "_P", "очном");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "очная");
                modifier.put(devForm + "_GF", "очной");
                modifier.put(devForm + "_DF", "очной");
                modifier.put(devForm + "_AF", "очную");
                modifier.put(devForm + "_IF", "очной");
                modifier.put(devForm + "_PF", "очной");
                break;
            case DevelopFormCodes.CORESP_FORM:
                modifier.put("DEVELOPFORM", "ЗАОЧНОЕ ОТДЕЛЕНИЕ");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "заочное");
                modifier.put(devForm + "_G", "заочного");
                modifier.put(devForm + "_D", "заочному");
                modifier.put(devForm + "_A", "заочное");
                modifier.put(devForm + "_I", "заочным");
                modifier.put(devForm + "_P", "заочном");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "заочная");
                modifier.put(devForm + "_GF", "заочной");
                modifier.put(devForm + "_DF", "заочной");
                modifier.put(devForm + "_AF", "заочную");
                modifier.put(devForm + "_IF", "заочной");
                modifier.put(devForm + "_PF", "заочной");
                break;
            case DevelopFormCodes.PART_TIME_FORM:
                modifier.put("DEVELOPFORM", "ЗАОЧНОЕ ОТДЕЛЕНИЕ");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "очно-заочное");
                modifier.put(devForm + "_G", "очно-заочного");
                modifier.put(devForm + "_D", "очно-заочному");
                modifier.put(devForm + "_A", "очно-заочное");
                modifier.put(devForm + "_I", "очно-заочным");
                modifier.put(devForm + "_P", "очно-заочном");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "очно-заочная");
                modifier.put(devForm + "_GF", "очно-заочной");
                modifier.put(devForm + "_DF", "очно-заочной");
                modifier.put(devForm + "_AF", "очно-заочную");
                modifier.put(devForm + "_IF", "очно-заочной");
                modifier.put(devForm + "_PF", "очно-заочной");
                break;
            case DevelopFormCodes.EXTERNAL_FORM:
                modifier.put("DEVELOPFORM", "");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "экстернат");
                modifier.put(devForm + "_G", "экстерната");
                modifier.put(devForm + "_D", "экстернату");
                modifier.put(devForm + "_A", "экстернат");
                modifier.put(devForm + "_I", "экстернатом");
                modifier.put(devForm + "_P", "экстернате");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "экстернат");
                modifier.put(devForm + "_GF", "экстерната");
                modifier.put(devForm + "_DF", "экстернату");
                modifier.put(devForm + "_AF", "экстернат");
                modifier.put(devForm + "_IF", "экстернатом");
                modifier.put(devForm + "_PF", "экстернате");
                break;
            case DevelopFormCodes.APPLICANT_FORM:
                modifier.put("DEVELOPFORM", "");

                // для мужского пола объекта, например "очное отделение"
                modifier.put(devForm, "самостоятельное обучение и итоговая аттестация");
                modifier.put(devForm + "_G", "самостоятельного обучения и итоговой аттестации");
                modifier.put(devForm + "_D", "самостоятельному обучению и итоговой аттестации");
                modifier.put(devForm + "_A", "самостоятельное обучение и итоговую аттестацию");
                modifier.put(devForm + "_I", "самостоятельным обучением и итоговой аттестацией");
                modifier.put(devForm + "_P", "самостоятельном обучении и итоговой аттестации");

                // для женского пола объекта, например "очная форма обучения"
                modifier.put(devForm + "__F", "самостоятельное обучение и итоговая аттестация");
                modifier.put(devForm + "_GF", "самостоятельного обучения и итоговой аттестации");
                modifier.put(devForm + "_DF", "самостоятельному обучению и итоговой аттестации");
                modifier.put(devForm + "_AF", "самостоятельное обучение и итоговую аттестацию");
                modifier.put(devForm + "_IF", "самостоятельным обучением и итоговой аттестацией");
                modifier.put(devForm + "_PF", "самостоятельном обучении и итоговой аттестации");
                break;
            default:
                throw new IllegalStateException();
        }
    }
}