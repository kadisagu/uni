// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.StudentPaymentOrderVisaList;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisaGroup;

/**
 * @author oleyba
 * @since 24.02.2010
 */
public class Model
{
    private IDataSettings _settings;
    private ISelectModel _orgUnitModel;

    private DynamicListDataSource<VisaWrapper> _dataSource;

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<VisaWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<VisaWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    public ISelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    protected static class VisaWrapper extends IdentifiableWrapper<StudentPaymentOrderVisa>
    {
        private static final long serialVersionUID = 1L;
        private StudentPaymentOrderVisa visa;

        public VisaWrapper(StudentPaymentOrderVisaGroup group)
        {
            super(group.getId(), group.getTitle());
        }

        public VisaWrapper(StudentPaymentOrderVisa visa)
                throws ClassCastException
        {
            super(visa.getId(), visa.getGroup().getTitle());
            this.visa = visa;
        }

        private static final StudentPaymentOrderVisa.Path _visaInstance = new StudentPaymentOrderVisa.Path("visa");
        public static StudentPaymentOrderVisa.Path visa() { return _visaInstance; }

        public boolean isGroup()
        {
            return visa == null;
        }

        public StudentPaymentOrderVisa getVisa()
        {
            return visa;
        }

        public void setVisa(StudentPaymentOrderVisa visa)
        {
            this.visa = visa;
        }
    }
}
