/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.orgunit.OrgUnitOrdersList;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unisp.IUnispComponents;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author dseleznev
 * Created on: 22.12.2009
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsPrefixKey(model)));
        getDao().refreshRequiredOrderTypeFieldSettings(model);
        prepareListDataSource(component);
    }

    public void onClickAddOrder(final IBusinessComponent component)
    {
        component.createChildRegion("studentPaymentOrdersListScope", new ComponentActivator(IUnispComponents.STUDENT_PAYMENTS_ORDER_ADD_EDIT, new ParametersMap()
                .add("orgUnitId", getModel(component).getOrgUnitId())));
    }

    public void onClickEditStudentPaymentOrder(final IBusinessComponent component)
    {
        component.createChildRegion("studentPaymentOrdersListScope", new ComponentActivator(IUnispComponents.STUDENT_PAYMENTS_ORDER_ADD_EDIT, new ParametersMap()
                .add("orderId", component.getListenerParameter())));
    }

    public void onClickDeleteStudentPaymentOrder(final IBusinessComponent component)
    {
        MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(getDao().get(StudentPaymentsOrder.class, (Long)component.getListenerParameter()));
        this.getModel(component).getDataSource().refresh();
    }

      public void onClickPrintListOrder(final IBusinessComponent component)
      {
          activateInRoot(component, new ComponentActivator(IUnispComponents.STUDENT_PAYMENTS_ORDER_PRINT, new ParametersMap()
              .add("orderId", component.getListenerParameter())));
      }

    public void onClickSearch(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        DataSettingsFacade.saveSettings(model.getSettings());
        model.getDataSource().refresh();
    }

    public void onClickClear(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);
        model.getSettings().clear();
        // TODO: recover OrderType Filter Value
        DataSettingsFacade.saveSettings(model.getSettings());
        getDao().refreshRequiredOrderTypeFieldSettings(getModel(component));
        onClickSearch(component);
    }

    public void prepareListDataSource(final IBusinessComponent component)
    {
        final Model model = getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        final DynamicListDataSource<StudentPaymentsOrder> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Дата формирования", IAbstractOrder.P_CREATE_DATE).setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME));
        dataSource.addColumn(new SimpleColumn("Дата приказа", StudentPaymentsOrder.P_COMMIT_DATE).setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", IAbstractOrder.P_NUMBER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", new String[]{IAbstractOrder.L_STATE, ICatalogItem.CATALOG_ITEM_TITLE}).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во назначений", StudentPaymentsOrder.P_COUNT_EXTRACT).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Сумма по приказу", StudentPaymentsOrder.P_FACT_MONTH_FUND_MONEY_FORMATTED).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Планируемый размер фонда", StudentPaymentsOrder.P_PLANNED_FUND_MONEY_FORMATTED).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(IAbstractOrder.P_COMMIT_DATE_SYSTEM, "Дата проведения", IAbstractOrder.P_COMMIT_DATE_SYSTEM, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrintListOrder", "Печать").setPermissionKey(model.getSecModel().getPermission("print")).setDisabledProperty("disabledPrint"));

        if (!model.getOrgUnit().isArchival())
        {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditStudentPaymentOrder").setPermissionKey(model.getSecModel().getPermission("edit")).setDisabledProperty(StudentPaymentsOrder.P_CANT_BE_EDITED));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteStudentPaymentOrder", "Удалить {0} со всеми выписками?", new Object[] { StudentPaymentsOrder.P_DELETABLE_MESSAGE_TITLE }).setPermissionKey(model.getSecModel().getPermission("delete")).setDisabledProperty(StudentPaymentsOrder.P_CANT_BE_EDITED));
        }

        dataSource.setOrder(IAbstractExtract.P_CREATE_DATE, OrderDirection.desc);
        model.setDataSource(dataSource);

    }

    private String getSettingsPrefixKey(final Model model)
    {
        final StringBuilder key = new StringBuilder("OrgUnitStudentPaymentOrdersList_");
        if (model.getOrgUnitId() != null)
        {
            key.append(model.getOrgUnitId());
        }
        key.append(".filter");
        return key.toString();
    }
}