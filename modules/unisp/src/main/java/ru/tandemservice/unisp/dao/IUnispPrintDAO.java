/**
 * $Id$
 */
package ru.tandemservice.unisp.dao;

import java.util.List;

import ru.tandemservice.uni.dao.IUniDao;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unisp.component.order.VirtualParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author dseleznev
 * Created on: 12.01.2010
 */
public interface IUnispPrintDAO extends IUniDao
{
    SpringBeanCache<IUnispPrintDAO> instance = new SpringBeanCache<IUnispPrintDAO>(IUnispPrintDAO.class.getName());
    
    List<VirtualParagraph> prepareOrderReportsData(StudentPaymentsOrder order);
    
    List<VirtualParagraph> prepareOrderParagraphsData(StudentPaymentsOrder order);
}