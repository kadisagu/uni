package ru.tandemservice.unisp.component.settings.UsedStudentPaymentOrderTypesSettingsEdit;

import org.tandemframework.core.component.Input;

import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 28.01.11
 * Time: 15:44
 * To change this template use File | Settings | File Templates.
 */
@Input(keys = {"studentPaymentExtractTypeId", "studentPaymentExtractTypeId"})
public class Model
{
    private Long _studentPaymentExtractTypeId;
    private StudentPaymentExtractType _extractType;

    public Long getStudentPaymentExtractTypeId()
    {
        return _studentPaymentExtractTypeId;
    }

    public void setStudentPaymentExtractTypeId(Long studentPaymentExtractTypeId)
    {
        this._studentPaymentExtractTypeId = studentPaymentExtractTypeId;
    }

    public StudentPaymentExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(StudentPaymentExtractType extractType)
    {
        this._extractType = extractType;
    }


}
