package ru.tandemservice.unisp.dao;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.catalog.UnispDocumentTemplate;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;
import ru.tandemservice.unisp.entity.orders.gen.StudentPaymentExtractGen;
import ru.tandemservice.unisp.entity.orders.gen.StudentPaymentParagraphGen;

import java.util.*;

/**
 * @author root
 */
public class UnispExtractDAO extends UniBaseDao implements IUnispExtractDAO {


    @Override
    @SuppressWarnings("unchecked")
    public Map<Long, Collection<IUnispExtractDAO.IStudentPaymentDescription>> findIntersections(final Collection<StudentPaymentAppointmentExtract> appointmentList) {
        final Map<Long, List<IStudentPaymentDescription>> totalMap = getTotalStudentExtractListMap(null, new HashSet<>(CollectionUtils.collect(appointmentList, input -> {
            return ((StudentPaymentAppointmentExtract) input).getEntity().getId();
        })));

        final Map<Long, Collection<IStudentPaymentDescription>> result = SafeMap.get(key -> new ArrayList<>());
        for (final StudentPaymentAppointmentExtract appointment: appointmentList) {
            final Long entityId = appointment.getEntity().getId();
            final Long appointmentId = (null == appointment.getId() ? entityId : appointment.getId());
            final List<IStudentPaymentDescription> total = totalMap.get(entityId);
            result.get(appointmentId).addAll(null == total ? Collections.emptyList() : CollectionUtils.select(
                    total,
                    object -> {
                        final IStudentPaymentDescription dsc = (IStudentPaymentDescription) object;
                        if (appointmentId.equals(dsc.getId())) { return false; /* сама себя выписка не перекрывает */ }

                        if (!appointment.getPaymentType().getType().equals(dsc.getPaymenType().getType())) { return false; /* должны совпадать типы выплаты */ }

                        if (appointment.getPaymentStopDate().before(dsc.getPaymentStartDate())) { return false; /* должны быть пересечения периодов назначения */ }
                        if (dsc.getPaymentStopDate().before(appointment.getPaymentStartDate())) { return false; /* должны быть пересечения периодов назначения */ }


                        // считаем, что есть перечесение
                        return true;
                    }
            ));
        }
        return result;
    }


    /* @return { orderId -> { StudentPaymentAppointmentExtract } } */
    private Map<Long, List<StudentPaymentAppointmentExtract>> getOrderId2AppointmentExtractListMap(final StudentPaymentsOrder order, final Collection<Long> ids) {
        final MQBuilder builder = new MQBuilder(StudentPaymentAppointmentExtract.ENTITY_CLASS, "extract");
        builder.addJoinFetch("extract", StudentPaymentExtractGen.L_PARAGRAPH, "paragraph");
        if (null != order) { builder.add(MQExpression.eq("paragraph", StudentPaymentParagraphGen.L_ORDER, order)); }
        if (null != ids) { builder.add(MQExpression.in("extract", StudentPaymentExtractGen.L_ENTITY + ".id", ids)); }

        final Map<Long, List<StudentPaymentAppointmentExtract>> orderId2appointmentExtractListMap = SafeMap.get(ArrayList.class);
        final List<StudentPaymentAppointmentExtract> appointmentExtractList = builder.getResultList(getSession());
        for (final StudentPaymentAppointmentExtract e: appointmentExtractList) {
            orderId2appointmentExtractListMap.get(e.getParagraph().getOrder().getId()).add(e);
        }
        return orderId2appointmentExtractListMap;
    }


    @Override
    public Map<Long, List<IUnispExtractDAO.IStudentPaymentDescription>> getTotalStudentExtractListMap(final StudentPaymentsOrder order, final Collection<Long> studentIds) {
        // TODO: нужно уметь это делать с учетом перекрытий в других приказах (если приказ ссылается на приказ), выписок об отмене и изменяющих выписках
        // TODO: нужно сортировать

        final Map<Long, Map<Long, List<IStudentPaymentDescription>>> orderId2resultMap = SafeMap.get(HashMap.class);

        /* сначала подготавливаем выписки о назначении */ {
            final BatchUtils.Action<Long> action = ids -> {
                final Map<Long, List<StudentPaymentAppointmentExtract>> map = getOrderId2AppointmentExtractListMap(order, ids);
                for (final Map.Entry<Long, List<StudentPaymentAppointmentExtract>> e: map.entrySet()) {
                    applyStudentPaymentAppointmentList(orderId2resultMap.get(e.getKey()), e.getValue());
                }
            };

            if (null == studentIds) {
                action.execute(null);
            } else {
                BatchUtils.execute(studentIds, 100, action);
            }
        }



        if (null != order) {
            // just return single result
            return orderId2resultMap.get(order.getId());
        } else {

            // interorder merge
            // TODO: нужно уметь это делать с учетом перекрытий в других приказах (если приказ ссылается на приказ), выписок об отмене и изменяющих выписках
            // TODO: нужно сортировать
            final Map<Long, List<IStudentPaymentDescription>> result = new HashMap<>();
            for (final Map.Entry<Long, Map<Long, List<IStudentPaymentDescription>>> orderIdEntry: orderId2resultMap.entrySet()) {
                for (final Map.Entry<Long, List<IStudentPaymentDescription>> studentIdEdntry: orderIdEntry.getValue().entrySet()) {
                    SafeMap.safeGet(result, studentIdEdntry.getKey(), STUDENT_EXTRACT_MAP_CALLBACK).addAll(studentIdEdntry.getValue());
                }
            }

            return result;
        }
    }

    final SafeMap.Callback<Long, List<IStudentPaymentDescription>> STUDENT_EXTRACT_MAP_CALLBACK = key -> new ArrayList<>(2);

    @Override
    public void applyStudentPaymentAppointmentList(final Map<Long, List<IStudentPaymentDescription>> totalStudentExtractListMap, final Collection<StudentPaymentAppointmentExtract> appointmentExtractList) {
        for (final StudentPaymentAppointmentExtract e : appointmentExtractList) {
            final List<IStudentPaymentDescription> studentPaymentList = SafeMap.safeGet(totalStudentExtractListMap, e.getEntity().getId(), STUDENT_EXTRACT_MAP_CALLBACK);
            studentPaymentList.add(getStudentPaymentExtractDescription(e));
        }
    }

    protected IStudentPaymentDescription getStudentPaymentExtractDescription(final StudentPaymentAppointmentExtract e) {
        return new IStudentPaymentDescription() {
            @Override
            public Long getId() {
                return e.getId();
            }

            @Override
            public String getTitle() {
                return MoneyFormatter.ruMoneyFormatter(3).format(e.getPaymentCost()) + " (" + e.getPaymentType().getShortTitle() + " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(e.getPaymentStartDate()) + "-" + DateFormatter.DEFAULT_DATE_FORMATTER.format(e.getPaymentStopDate()) + ", " + e.getFinancingSource().getShortTitle() + ")";
            }

            @Override
            public Double getPaymentTotalCostAsDouble() {
                return e.getPaymentTotalCostAsDouble();
            }

            @Override
            public Double getPaymentCostAsDouble() {
                return e.getPaymentCostAsDouble();
            }

            @Override
            public Date getPaymentStartDate() {
                return e.getPaymentStartDate();
            }

            @Override
            public Date getPaymentStopDate() {
                return e.getPaymentStopDate();
            }

            @Override
            public StudentPayment getPaymenType() {
                return e.getPaymentType();
            }
        };
    }

    @Override
    public double getTotalOrderFactFund(final Map<Long, List<IUnispExtractDAO.IStudentPaymentDescription>> extractsMap) {
        double factOrderFund = 0d;
        for (final List<IStudentPaymentDescription> list : extractsMap.values()) {
            for (final IStudentPaymentDescription item : list) {
                factOrderFund += item.getPaymentTotalCostAsDouble();
            }
        }
        return factOrderFund;
    }

    @Override
    public double getTotalOrderMonthFactFund(final Map<Long, List<IUnispExtractDAO.IStudentPaymentDescription>> extractsMap) {
        double factOrderFund = 0d;
        for (final List<IStudentPaymentDescription> list : extractsMap.values()) {
            for (final IStudentPaymentDescription item : list) {
                factOrderFund += item.getPaymentCostAsDouble();
            }
        }
        return factOrderFund;
    }

    @Override
    public StudentPaymentParagraph doCreateParagraph(final StudentPaymentsOrder order) {
        final StudentPaymentParagraph result = new StudentPaymentParagraph();
        result.setOrder(order);

        synchronized (String.valueOf(order.getId()).intern()) {
            // глобальная блокировка по идентификатору приказа //

            final Session session = getSession();
            final Number lastNumber = (Number) session.createCriteria(StudentPaymentParagraph.class).add(Restrictions.eq(StudentPaymentParagraphGen.L_ORDER, order)).setProjection(Projections.max(StudentPaymentParagraphGen.P_NUMBER)).uniqueResult();
            result.setNumber(1 + (null == lastNumber ? 0 : lastNumber.intValue()));
            session.save(result);

            // вызывать ДО выхода из точки синхранизации
            session.flush();
        }
        return result;
    }

    @Override
    public StudentPaymentParagraph doCreateAppointmentExtractList(final StudentPaymentsOrder order, final Collection<StudentPaymentAppointmentExtract> appointmentList, final String errorCollectorPrefix) {
        final Session session = getSession();
        final ExtractStates state = get(ExtractStates.class, ExtractStates.P_CODE, "1");

        synchronized (String.valueOf(order.getId()).intern()) {
            // глобальная блокировка по идентификатору приказа //
            final Map<Long, Collection<IStudentPaymentDescription>> intersections = IUnispExtractDAO.instance.get().findIntersections(appointmentList);

            boolean hasErrors = false;
            for (final StudentPaymentAppointmentExtract appointment: appointmentList) {
                final Long appointmentId = (null == appointment.getId() ? appointment.getEntity().getId() : appointment.getId());
                final Collection<IStudentPaymentDescription> intersection = intersections.get(appointmentId);
                if (intersection.size() > 0) {
                    if (null != errorCollectorPrefix) {
                        UserContext.getInstance().getErrorCollector().add(
                                "У студента уже есть выплата с типом «"+appointment.getPaymentType().getType().getTitle()+"» на указанный период.",
                                errorCollectorPrefix+appointmentId+"paymentStartDate",
                                errorCollectorPrefix+appointmentId+"paymentStopDate"
                        );
                    }
                    hasErrors = true;
                }
            }
            if (hasErrors) { return null; }

            final StudentPaymentParagraph paragraph = IUnispExtractDAO.instance.get().doCreateParagraph(order);
            for (final StudentPaymentAppointmentExtract appointment: appointmentList) {
                appointment.setCreateDate(new Date());
                appointment.setState(state);
                appointment.setParagraph(paragraph);
                session.save(appointment);
            }

            // вызывать ДО выхода из точки синхранизации
            session.flush();

            return paragraph;
        }
    }
    
    @Override
    public byte[] getTemplate(StudentPaymentExtractType extractType, int templateIndex)
    {
        Criteria c = getSession().createCriteria(UnispDocumentTemplate.class);
        c.add(Restrictions.eq(UnispDocumentTemplate.L_TYPE, extractType));
        c.add(Restrictions.eq(UnispDocumentTemplate.P_INDEX, templateIndex));
        return ((UnispDocumentTemplate) c.uniqueResult()).getContent();
    }
    
    @Override
    public List<StudentPaymentExtractType> getStudentPaymentOrderTypesList(final String filter, OrgUnit orgUnit)
    {
        final MQBuilder builder = new MQBuilder(StudentPaymentExtractType.ENTITY_CLASS, "dt");
        builder.add(MQExpression.isNull("dt", StudentPaymentExtractType.L_PARENT));
        builder.add(MQExpression.like("dt", StudentPaymentExtractType.P_TITLE, CoreStringUtils.escapeLike(filter)));
        builder.addOrder("dt", StudentPaymentExtractType.P_CODE);
        List<StudentPaymentExtractType> list = builder.getResultList(getSession());
        
        List<StudentPaymentExtractType> resultList = new ArrayList<>();
        for(StudentPaymentExtractType type : list)
        {
            String permissionKey = "workWithOrders_" + orgUnit.getOrgUnitType().getCode() + "_" + type.getCode();
            if(CoreServices.securityService().check(orgUnit, ContextLocal.getUserContext().getPrincipalContext(), permissionKey))
                resultList.add(type);
        }

        return resultList;
    }
    
    @Override
    public void checkOrderNumber(StudentPaymentsOrder order)
    {
        String number = order.getNumber();
        if (number == null) return;
        number = number.trim();
        Date date = order.getCreateDate();
        if (date == null)
            throw new ApplicationException("Нельзя проверить номер приказа. Не указана дата формирования.");

        MQBuilder builder = new MQBuilder(StudentPaymentsOrder.ENTITY_NAME, "o");
        builder.add(UniMQExpression.eqYear("o", IAbstractOrder.P_CREATE_DATE, CoreDateUtils.getYear(date)));
        builder.add(MQExpression.eq("o", IAbstractOrder.P_NUMBER, number));
        if (order.getId() != null)
            builder.add(MQExpression.notEq("o", "id", order.getId()));

        List<StudentPaymentsOrder> list = builder.getResultList(getSession());
        if (!list.isEmpty())
            throw new ApplicationException("Номер приказа должен быть уникален в рамках календарного года среди всех приказов по выплатам студентам.");
    }
}