// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.settings.StudentPaymentProtocolVisas;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unisp.entity.settings.StudentPaymentProtocolVisa;

import java.util.HashMap;
import java.util.Map;

/**
 * @author oleyba
 * @since 08.02.2010
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setOrgUnitModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                Session session = DataAccessServices.dao().getComponentSession();
                MQBuilder builder = new MQBuilder(OrgUnitToKindRelation.ENTITY_CLASS, "rel", new String[]{OrgUnitToKindRelation.orgUnit().s()});
                builder.add(MQExpression.eq("rel", OrgUnitToKindRelation.orgUnitKind().code().s(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
                builder.add(MQExpression.eq("rel", OrgUnitToKindRelation.orgUnit().archival().s(), Boolean.FALSE));
                filter = StringUtils.trimToNull(filter);
                if (null != filter)
                    builder.add(MQExpression.like("rel", OrgUnitToKindRelation.orgUnit().title().s(), filter));
                return new ListResult<OrgUnit>(builder.<OrgUnit>getResultList(session, 0, 100), builder.getResultCount(session));                
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                OrgUnit orgUnit = UniDaoFacade.getCoreDao().get((Long) primaryKey);
                return UniDaoFacade.getOrgstructDao().isOrgUnitHasKind(orgUnit, UniDefines.CATALOG_ORGUNIT_KIND_FORMING) ? orgUnit : null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnit) value).getFullTitle();
            }
        });
        model.setEmployeePostModel(new OrderExecutorSelectModel());
    }

    @Override
    public void refreshEmployeeList(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentPaymentProtocolVisa.ENTITY_CLASS, "rel", new String[]{StudentPaymentProtocolVisa.employeePost().s()});
        builder.add(MQExpression.eq("rel", StudentPaymentProtocolVisa.orgUnit().s(), model.getOrgUnit()));
        builder.addOrder("rel", StudentPaymentProtocolVisa.employeePost().employee().person().identityCard().lastName().s());
        model.setEmployeePostList(builder.<EmployeePost>getResultList(getSession()));
    }

    @Override
    public void update(Model model)
    {
        Map<EmployeePost, StudentPaymentProtocolVisa> existing = new HashMap<EmployeePost, StudentPaymentProtocolVisa>();
        for (StudentPaymentProtocolVisa rel : getList(StudentPaymentProtocolVisa.class, StudentPaymentProtocolVisa.orgUnit().s()))
            existing.put(rel.getEmployeePost(), rel);
        for (EmployeePost employeePost : model.getEmployeePostList())
        {
            if (existing.containsKey(employeePost))
                existing.remove(employeePost);
            else
            {
                StudentPaymentProtocolVisa rel = new StudentPaymentProtocolVisa();
                rel.setOrgUnit(model.getOrgUnit());
                rel.setEmployeePost(employeePost);
                save(rel);
            }
        }
        for (StudentPaymentProtocolVisa rel : existing.values())
            delete(rel);
    }
}
