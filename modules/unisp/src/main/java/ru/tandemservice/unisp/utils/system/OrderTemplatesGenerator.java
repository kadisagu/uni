/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.utils.system;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.SystemUtil;

import ru.tandemservice.uni.util.system.UniSystemUtils;

/**
 * Генератор печатных форм для списочных приказов
 * <p/>
 *
 * @author dseleznev
 * Created on: 12.01.2010
 */
public class OrderTemplatesGenerator
{
    private final static String ORDER_HEADER = "@orderHeader.txt";
    private final static String ORDER_TOP = "@orderTop.txt";
    private final static String ORDER_BOTTOM = "@orderBottom.txt";
    
    private final static String PARAGRAPH_HEADER = "@paragraphHeader.txt";
    private final static String PARAGRAPH_TOP = "@paragraphTop.txt";
    private final static String PARAGRAPH_BOTTOM = "@paragraphBottom.txt";
    
    private final static String EXTRACT_HEADER = "@extractHeader.txt";
    private final static String EXTRACT_TOP = "@extractTop.txt";
    private final static String EXTRACT_BOTTOM = "@extractBottom.txt";
    
    private final static String ORDER_TEMPLATE_FILENAME_PREFIX =  "order_";
    private final static String ORDER_REPORT_TEMPLATE_FILENAME_PREFIX = "orderReport_";
    private final static String PARAGRAPH_MODULAR_TEMPLATE_FILENAME_PREFIX = "modularParagraph_";
    private final static String PARAGRAPH_LIST_TEMPLATE_FILENAME_PREFIX = "listParagraph_";
    private final static String EXTRACT_TEMPLATE_FILENAME_PREFIX = "extract_";

    public static void main(String[] args) throws IOException
    {
        File modules = UniSystemUtils.getModulesDir();
        String moduleOwner = System.getProperty("moduleOwner");
        if (StringUtils.isEmpty(moduleOwner))
            throw new RuntimeException("Plz specify printforms moduleOwner: for example 'unisp'");

        String printformsPath = modules.getAbsolutePath() + ("unisp".equals(moduleOwner) ? "/" : "/projects/") + moduleOwner + "/src/main/java/ru/tandemservice/" + moduleOwner + ("unisp".equals(moduleOwner) ? "/utils/system/sptemplates/printforms/" : "/utils/templates/unispmove");
        String templatesPath = modules.getAbsolutePath() + ("unisp".equals(moduleOwner) ? "/" : "/projects/") + moduleOwner + "/src/main/resources/" + moduleOwner + "/templates/unispmove/";

        byte[] orderHeaderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + ORDER_HEADER));
        byte[] orderTopUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + ORDER_TOP));
        byte[] orderBottomUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + ORDER_BOTTOM));
        byte[] paragraphHeaderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + PARAGRAPH_HEADER));
        byte[] paragraphTopUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + PARAGRAPH_TOP));
        byte[] paragraphBottomUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + PARAGRAPH_BOTTOM));
        byte[] extractHeaderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_HEADER));
        byte[] extractTopUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_TOP));
        byte[] extractBottomUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_BOTTOM));

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // create templates
        File path = new File(printformsPath);
        for (File file : path.listFiles())
        {
            if (file.getName().endsWith(".txt") && !file.getName().startsWith("@"))
            {
                out.reset();
                
                if (file.getName().startsWith(ORDER_TEMPLATE_FILENAME_PREFIX))
                {
                    // create order template
                    out.write(getRtfData(orderHeaderUTF8));
                    out.write(getRtfData(orderTopUTF8));
                    out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                    out.write(getRtfData(orderBottomUTF8));
                } else if (file.getName().startsWith(ORDER_REPORT_TEMPLATE_FILENAME_PREFIX))
                {
                    // create order report template
                    out.write(getRtfData(paragraphHeaderUTF8));
                    out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                    out.write("\\par}".getBytes());
                } else if (file.getName().startsWith(PARAGRAPH_MODULAR_TEMPLATE_FILENAME_PREFIX))
                {
                    // create modular paragraph template
                    out.write(getRtfData(paragraphHeaderUTF8));
                    out.write(getRtfData(paragraphTopUTF8));
                    out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                    out.write(getRtfData(paragraphBottomUTF8));
                    out.write("\\page}".getBytes());
                } else if (file.getName().startsWith(PARAGRAPH_LIST_TEMPLATE_FILENAME_PREFIX))
                {
                    // create list paragraph template
                    out.write(getRtfData(paragraphHeaderUTF8));
                    out.write(getRtfData(paragraphTopUTF8));
                    out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                    out.write(getRtfData(paragraphBottomUTF8));
                    out.write("\\page}".getBytes());
                } else if (file.getName().startsWith(EXTRACT_TEMPLATE_FILENAME_PREFIX))
                {
                    // create extract template
                    out.write(getRtfData(extractHeaderUTF8));
                    out.write(getRtfData(extractTopUTF8));
                    out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                    out.write(getRtfData(extractBottomUTF8));
                }
                
                File newFile = new File(templatesPath + "/" + FilenameUtils.getBaseName(file.getName()) + ".rtf");
                FileUtils.writeByteArrayToFile(newFile, out.toByteArray());
                System.out.println("[+] " + newFile.getAbsolutePath());
            }
        }
    }

    /**
     * My_Param} -> {\field{\*\fldinst{COMMENTS My_Param \\* MERGEFORMAT}}{\fldrslt{My_Param}}}
     * \n -> \par
     *
     * @param readableDataUTF - массив байт в кодировке UTF-8 шаблона выписки
     * @return массив получившихся байт после подстановки параметров
     * @throws java.io.IOException если будет ошибка записи в ByteArrayOutputStream
     */
    private static byte[] getRtfData(byte[] readableDataUTF) throws IOException
    {
        byte[] readableData = new String(readableDataUTF, "UTF-8").getBytes("CP1251");

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        for (int i = 0; i < readableData.length; i++)
        {
            byte c = readableData[i];
            if (c < 0)
            {
                // русские символы превращаются в rtf-escape-последовательность
                result.write(new byte[]{'\\', '\''});
                result.write(SystemUtil.getBytes(c));
            } else if (c == '{')
            {
                i++;
                boolean isParameter = true;
                for (; i < readableData.length && readableData[i] != '}'; i++)
                {
                    byte b = readableData[i];
                    isParameter = isParameter && ((b >= 'A' && b <= 'Z') || (b >= 'a' && b <= 'z') || b == '_');
                    buffer.write(b);
                }
                if (isParameter)
                    result.write(("{\\field{\\*\\fldinst{COMMENTS " + buffer.toString() + "}}{\\fldrslt{" + buffer.toString() + "}}}").getBytes());
                else
                {
                    result.write('{');
                    result.write(buffer.toByteArray());
                    result.write('}');
                }
                buffer.reset();
            } else
                result.write(c);
        }
        return result.toByteArray();
    }
}