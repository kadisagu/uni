/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.utils.system;

/**
 * @author dseleznev
 * Created on: 29.12.2009
 */
public class OrderAndExtractData
{
    //список всех компонентов приказов из сборного приказа
    public static final String[][] ORDER_LIST = new String[][]
    {
        /* 1*/{"studentPaymentsOrder", "О назначении выплат студентам"}
    };
    
    //список всех entity выписок из сборного приказа
    public static final String[][] EXTRACT_LIST = new String[][]
    {
        /* 1*/{"studentPaymentAppointmentExtract", "О назначении выплаты"}
    };
}