package ru.tandemservice.unisp.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.entity.visa.gen.IPossibleVisaGen;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa;
import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisaGroup;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Виза для приказов о назначении выплат
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentOrderVisaGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa";
    public static final String ENTITY_NAME = "studentPaymentOrderVisa";
    public static final int VERSION_HASH = -810680527;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String L_POSSIBLE_VISA = "possibleVisa";
    public static final String P_PRIORITY = "priority";
    public static final String L_GROUPS_MEMBER_VISING = "groupsMemberVising";

    private StudentPaymentOrderVisaGroup _group;     // Группа виз для приказов о назначении выплат
    private IPossibleVisa _possibleVisa;     // Возможная виза
    private int _priority;     // Приоритет
    private GroupsMemberVising _groupsMemberVising;     // Группа участников визирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа виз для приказов о назначении выплат. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentOrderVisaGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа виз для приказов о назначении выплат. Свойство не может быть null.
     */
    public void setGroup(StudentPaymentOrderVisaGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Возможная виза. Свойство не может быть null.
     */
    @NotNull
    public IPossibleVisa getPossibleVisa()
    {
        return _possibleVisa;
    }

    /**
     * @param possibleVisa Возможная виза. Свойство не может быть null.
     */
    public void setPossibleVisa(IPossibleVisa possibleVisa)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && possibleVisa!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IPossibleVisa.class);
            IEntityMeta actual =  possibleVisa instanceof IEntity ? EntityRuntime.getMeta((IEntity) possibleVisa) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_possibleVisa, possibleVisa);
        _possibleVisa = possibleVisa;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     */
    @NotNull
    public GroupsMemberVising getGroupsMemberVising()
    {
        return _groupsMemberVising;
    }

    /**
     * @param groupsMemberVising Группа участников визирования. Свойство не может быть null.
     */
    public void setGroupsMemberVising(GroupsMemberVising groupsMemberVising)
    {
        dirty(_groupsMemberVising, groupsMemberVising);
        _groupsMemberVising = groupsMemberVising;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentOrderVisaGen)
        {
            setGroup(((StudentPaymentOrderVisa)another).getGroup());
            setPossibleVisa(((StudentPaymentOrderVisa)another).getPossibleVisa());
            setPriority(((StudentPaymentOrderVisa)another).getPriority());
            setGroupsMemberVising(((StudentPaymentOrderVisa)another).getGroupsMemberVising());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentOrderVisaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentOrderVisa.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentOrderVisa();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "group":
                    return obj.getGroup();
                case "possibleVisa":
                    return obj.getPossibleVisa();
                case "priority":
                    return obj.getPriority();
                case "groupsMemberVising":
                    return obj.getGroupsMemberVising();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "group":
                    obj.setGroup((StudentPaymentOrderVisaGroup) value);
                    return;
                case "possibleVisa":
                    obj.setPossibleVisa((IPossibleVisa) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "groupsMemberVising":
                    obj.setGroupsMemberVising((GroupsMemberVising) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "group":
                        return true;
                case "possibleVisa":
                        return true;
                case "priority":
                        return true;
                case "groupsMemberVising":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "group":
                    return true;
                case "possibleVisa":
                    return true;
                case "priority":
                    return true;
                case "groupsMemberVising":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "group":
                    return StudentPaymentOrderVisaGroup.class;
                case "possibleVisa":
                    return IPossibleVisa.class;
                case "priority":
                    return Integer.class;
                case "groupsMemberVising":
                    return GroupsMemberVising.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentOrderVisa> _dslPath = new Path<StudentPaymentOrderVisa>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentOrderVisa");
    }
            

    /**
     * @return Группа виз для приказов о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa#getGroup()
     */
    public static StudentPaymentOrderVisaGroup.Path<StudentPaymentOrderVisaGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Возможная виза. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa#getPossibleVisa()
     */
    public static IPossibleVisaGen.Path<IPossibleVisa> possibleVisa()
    {
        return _dslPath.possibleVisa();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa#getGroupsMemberVising()
     */
    public static GroupsMemberVising.Path<GroupsMemberVising> groupsMemberVising()
    {
        return _dslPath.groupsMemberVising();
    }

    public static class Path<E extends StudentPaymentOrderVisa> extends EntityPath<E>
    {
        private StudentPaymentOrderVisaGroup.Path<StudentPaymentOrderVisaGroup> _group;
        private IPossibleVisaGen.Path<IPossibleVisa> _possibleVisa;
        private PropertyPath<Integer> _priority;
        private GroupsMemberVising.Path<GroupsMemberVising> _groupsMemberVising;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа виз для приказов о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa#getGroup()
     */
        public StudentPaymentOrderVisaGroup.Path<StudentPaymentOrderVisaGroup> group()
        {
            if(_group == null )
                _group = new StudentPaymentOrderVisaGroup.Path<StudentPaymentOrderVisaGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Возможная виза. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa#getPossibleVisa()
     */
        public IPossibleVisaGen.Path<IPossibleVisa> possibleVisa()
        {
            if(_possibleVisa == null )
                _possibleVisa = new IPossibleVisaGen.Path<IPossibleVisa>(L_POSSIBLE_VISA, this);
            return _possibleVisa;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(StudentPaymentOrderVisaGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Группа участников визирования. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisa#getGroupsMemberVising()
     */
        public GroupsMemberVising.Path<GroupsMemberVising> groupsMemberVising()
        {
            if(_groupsMemberVising == null )
                _groupsMemberVising = new GroupsMemberVising.Path<GroupsMemberVising>(L_GROUPS_MEMBER_VISING, this);
            return _groupsMemberVising;
        }

        public Class getEntityClass()
        {
            return StudentPaymentOrderVisa.class;
        }

        public String getEntityName()
        {
            return "studentPaymentOrderVisa";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
