package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.GlobalFinancingSource;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка о назначении выплаты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentAppointmentExtractGen extends StudentPaymentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract";
    public static final String ENTITY_NAME = "studentPaymentAppointmentExtract";
    public static final int VERSION_HASH = -1571161548;
    private static IEntityMeta ENTITY_META;

    public static final String L_PAYMENT_TYPE = "paymentType";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String P_PAYMENT_COST = "paymentCost";
    public static final String P_PAYMENT_START_DATE = "paymentStartDate";
    public static final String P_PAYMENT_STOP_DATE = "paymentStopDate";

    private StudentPayment _paymentType;     // Выплаты
    private GlobalFinancingSource _financingSource;     // Источник финансирования
    private long _paymentCost; 
    private Date _paymentStartDate; 
    private Date _paymentStopDate; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выплаты. Свойство не может быть null.
     */
    @NotNull
    public StudentPayment getPaymentType()
    {
        return _paymentType;
    }

    /**
     * @param paymentType Выплаты. Свойство не может быть null.
     */
    public void setPaymentType(StudentPayment paymentType)
    {
        dirty(_paymentType, paymentType);
        _paymentType = paymentType;
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     */
    @NotNull
    public GlobalFinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования. Свойство не может быть null.
     */
    public void setFinancingSource(GlobalFinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public long getPaymentCost()
    {
        return _paymentCost;
    }

    /**
     * @param paymentCost  Свойство не может быть null.
     */
    public void setPaymentCost(long paymentCost)
    {
        dirty(_paymentCost, paymentCost);
        _paymentCost = paymentCost;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public Date getPaymentStartDate()
    {
        return _paymentStartDate;
    }

    /**
     * @param paymentStartDate  Свойство не может быть null.
     */
    public void setPaymentStartDate(Date paymentStartDate)
    {
        dirty(_paymentStartDate, paymentStartDate);
        _paymentStartDate = paymentStartDate;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public Date getPaymentStopDate()
    {
        return _paymentStopDate;
    }

    /**
     * @param paymentStopDate  Свойство не может быть null.
     */
    public void setPaymentStopDate(Date paymentStopDate)
    {
        dirty(_paymentStopDate, paymentStopDate);
        _paymentStopDate = paymentStopDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof StudentPaymentAppointmentExtractGen)
        {
            setPaymentType(((StudentPaymentAppointmentExtract)another).getPaymentType());
            setFinancingSource(((StudentPaymentAppointmentExtract)another).getFinancingSource());
            setPaymentCost(((StudentPaymentAppointmentExtract)another).getPaymentCost());
            setPaymentStartDate(((StudentPaymentAppointmentExtract)another).getPaymentStartDate());
            setPaymentStopDate(((StudentPaymentAppointmentExtract)another).getPaymentStopDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentAppointmentExtractGen> extends StudentPaymentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentAppointmentExtract.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentAppointmentExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "paymentType":
                    return obj.getPaymentType();
                case "financingSource":
                    return obj.getFinancingSource();
                case "paymentCost":
                    return obj.getPaymentCost();
                case "paymentStartDate":
                    return obj.getPaymentStartDate();
                case "paymentStopDate":
                    return obj.getPaymentStopDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "paymentType":
                    obj.setPaymentType((StudentPayment) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((GlobalFinancingSource) value);
                    return;
                case "paymentCost":
                    obj.setPaymentCost((Long) value);
                    return;
                case "paymentStartDate":
                    obj.setPaymentStartDate((Date) value);
                    return;
                case "paymentStopDate":
                    obj.setPaymentStopDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "paymentType":
                        return true;
                case "financingSource":
                        return true;
                case "paymentCost":
                        return true;
                case "paymentStartDate":
                        return true;
                case "paymentStopDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "paymentType":
                    return true;
                case "financingSource":
                    return true;
                case "paymentCost":
                    return true;
                case "paymentStartDate":
                    return true;
                case "paymentStopDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "paymentType":
                    return StudentPayment.class;
                case "financingSource":
                    return GlobalFinancingSource.class;
                case "paymentCost":
                    return Long.class;
                case "paymentStartDate":
                    return Date.class;
                case "paymentStopDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentAppointmentExtract> _dslPath = new Path<StudentPaymentAppointmentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentAppointmentExtract");
    }
            

    /**
     * @return Выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getPaymentType()
     */
    public static StudentPayment.Path<StudentPayment> paymentType()
    {
        return _dslPath.paymentType();
    }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getFinancingSource()
     */
    public static GlobalFinancingSource.Path<GlobalFinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getPaymentCost()
     */
    public static PropertyPath<Long> paymentCost()
    {
        return _dslPath.paymentCost();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getPaymentStartDate()
     */
    public static PropertyPath<Date> paymentStartDate()
    {
        return _dslPath.paymentStartDate();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getPaymentStopDate()
     */
    public static PropertyPath<Date> paymentStopDate()
    {
        return _dslPath.paymentStopDate();
    }

    public static class Path<E extends StudentPaymentAppointmentExtract> extends StudentPaymentExtract.Path<E>
    {
        private StudentPayment.Path<StudentPayment> _paymentType;
        private GlobalFinancingSource.Path<GlobalFinancingSource> _financingSource;
        private PropertyPath<Long> _paymentCost;
        private PropertyPath<Date> _paymentStartDate;
        private PropertyPath<Date> _paymentStopDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getPaymentType()
     */
        public StudentPayment.Path<StudentPayment> paymentType()
        {
            if(_paymentType == null )
                _paymentType = new StudentPayment.Path<StudentPayment>(L_PAYMENT_TYPE, this);
            return _paymentType;
        }

    /**
     * @return Источник финансирования. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getFinancingSource()
     */
        public GlobalFinancingSource.Path<GlobalFinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new GlobalFinancingSource.Path<GlobalFinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getPaymentCost()
     */
        public PropertyPath<Long> paymentCost()
        {
            if(_paymentCost == null )
                _paymentCost = new PropertyPath<Long>(StudentPaymentAppointmentExtractGen.P_PAYMENT_COST, this);
            return _paymentCost;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getPaymentStartDate()
     */
        public PropertyPath<Date> paymentStartDate()
        {
            if(_paymentStartDate == null )
                _paymentStartDate = new PropertyPath<Date>(StudentPaymentAppointmentExtractGen.P_PAYMENT_START_DATE, this);
            return _paymentStartDate;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract#getPaymentStopDate()
     */
        public PropertyPath<Date> paymentStopDate()
        {
            if(_paymentStopDate == null )
                _paymentStopDate = new PropertyPath<Date>(StudentPaymentAppointmentExtractGen.P_PAYMENT_STOP_DATE, this);
            return _paymentStopDate;
        }

        public Class getEntityClass()
        {
            return StudentPaymentAppointmentExtract.class;
        }

        public String getEntityName()
        {
            return "studentPaymentAppointmentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
