// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderVisaGroupAdd;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import ru.tandemservice.unisp.entity.settings.StudentPaymentOrderVisaGroup;

/**
 * @author oleyba
 * @since 26.02.2010
 */
@Input(keys = {"documentId"}, bindings = {"documentId"})
public class Model
{
    private Long _documentId;
    private ISelectModel _groupModel;
    private StudentPaymentOrderVisaGroup _group;

    public Long getDocumentId()
    {
        return _documentId;
    }

    public void setDocumentId(Long documentId)
    {
        _documentId = documentId;
    }

    public ISelectModel getGroupModel()
    {
        return _groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        _groupModel = groupModel;
    }

    public StudentPaymentOrderVisaGroup getGroup()
    {
        return _group;
    }

    public void setGroup(StudentPaymentOrderVisaGroup group)
    {
        _group = group;
    }
}
