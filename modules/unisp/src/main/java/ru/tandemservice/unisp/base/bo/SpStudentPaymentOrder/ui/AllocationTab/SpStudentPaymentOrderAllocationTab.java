/**
 * $Id$
 */
package ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.ui.AllocationTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.SpStudentPaymentOrderManager;
import ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.logic.allocationTab.StudentPaymentOrderAllocationSearchListDSHandler;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 14.02.13
 */
@Configuration
public class SpStudentPaymentOrderAllocationTab extends AbstractUniStudentList
{
    public static final String GROUP_DS = "spPaymentAllocationGroupDS";
    public static final String WORK_PLAN_ROW_KIND_DS = "workPlanRowKindDS";
    public static final String DEBT_STATUS_SELECT_DS = "debtStatusDS";
    public static final String PERFORMANCE_STATUS_SELECT_DS = "performanceStatusDS";
    public static final String LAST_SESSION_STATUS_SELECT_DS = "lastSessionStatusDS";

    public static final String CHECKBOX_COLUMN = "checkbox";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), SpStudentPaymentOrderManager.instance().studentSearchListDSHandler()))
                        .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
                        .addDataSource(selectDS(DEBT_STATUS_SELECT_DS, SpStudentPaymentOrderManager.instance().debtStatusDSHandler()))
                        .addDataSource(selectDS(PERFORMANCE_STATUS_SELECT_DS, SpStudentPaymentOrderManager.instance().performanceStatusDSHandler()))
                        .addDataSource(selectDS(LAST_SESSION_STATUS_SELECT_DS, SpStudentPaymentOrderManager.instance().lastSessionStatusDSHandler()))
                        .addDataSource(SpStudentPaymentOrderManager.instance().regStructureDSConfig())
                        .addDataSource(selectDS(WORK_PLAN_ROW_KIND_DS, workPlanRowKindDSHandler()))
        );
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        return createStudentSearchListColumnsBuilder()
                .addColumn(textColumn(StudentPaymentOrderAllocationSearchListDSHandler.TERM_COLUMN, StudentPaymentOrderAllocationSearchListDSHandler.TERM_COLUMN).required(true).create())
                .addColumn(textColumn(StudentPaymentOrderAllocationSearchListDSHandler.PAYMENT_COLUMN, StudentPaymentOrderAllocationSearchListDSHandler.PAYMENT_COLUMN).formatter(NewLineFormatter.NOBR_IN_LINES).required(true).create())
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon("edit"), "onClickEditStudentPayments").disabled("ui:editDisable"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon("delete"), "onClickDeleteStudentPayments").alert(alert("studentSearchListDS.delete.alert", Student.person().fio())).disabled("ui:deleteDisable"))
                .create();
    }

    @Override
    protected List<ColumnBase> customizeStudentSearchListColumns(List<ColumnBase> columnList)
    {
        removeColumns(
                columnList,
                // эти колонки заменятся, что бы в них выводились сокр. названия
                FORMATIVE_ORG_UNIT_COLUMN,
                TERRITORIAL_ORG_UNIT_COLUMN,
                DEVELOP_FORM_COLUMN,
                DEVELOP_CONDITION_COLUMN,
                // эти не нужны
                PRODUCTIVE_ORG_UNIT_COLUMN,
                CITIZENSHIP_COLUMN,
                PERSONAL_NUMBER_COLUMN,
                PERSONAL_FILE_NUMBER_COLUMN,
                PRODUCTIVE_ORG_UNIT_COLUMN
        );

        insertColumns(columnList, checkboxColumn(CHECKBOX_COLUMN).before(FIO_COLUMN).create());
        insertColumns(columnList, textColumn(FORMATIVE_ORG_UNIT_COLUMN, Student.educationOrgUnit().formativeOrgUnit().shortTitle()).after(SPECIAL_PURPOSE_RECRUIT_COLUMN).order().create());
        insertColumns(columnList, textColumn(TERRITORIAL_ORG_UNIT_COLUMN, Student.educationOrgUnit().territorialOrgUnit().territorialShortTitle()).after(FORMATIVE_ORG_UNIT_COLUMN).order().create());
        insertColumns(columnList, textColumn(DEVELOP_FORM_COLUMN, Student.educationOrgUnit().developForm().shortTitle()).after(ORIENTATION_COLUMN).create());
        insertColumns(columnList, textColumn(DEVELOP_CONDITION_COLUMN, Student.educationOrgUnit().developCondition().shortTitle()).after(DEVELOP_FORM_COLUMN).create());

        return columnList;
    }

    @Override
    protected IMergeRowIdResolver newMergeRowIdResolver()
    {
        return entity -> String.valueOf(((IEntity) entity.getProperty(Student.L_PERSON)).getId());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> workPlanRowKindDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppWorkPlanRowKind.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> groupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                final StudentPaymentsOrder order = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_ORDER);
                final OrgUnit orgUnit = order.getOrgUnit();
                final List<Course> courseList = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_COURSE_LIST);

                final DQLSelectBuilder groupIdBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                        .where(eq(property(Student.archival().fromAlias("s")), value(false)))
                        .where(or(
                                eq(property(Student.educationOrgUnit().formativeOrgUnit().fromAlias("s")), value(orgUnit)),
                                eq(property(Student.educationOrgUnit().territorialOrgUnit().fromAlias("s")), value(orgUnit))))
                        .predicate(DQLPredicateType.distinct)
                        .column(property(Student.group().id().fromAlias("s")));

                dql.where(in(property(Group.id().fromAlias(alias)), groupIdBuilder.buildQuery()));

                if (null != courseList && !courseList.isEmpty())
                {
                    dql.where(or(
                            in(property(Group.course().fromAlias(alias)), courseList),
                            exists(
                                    new DQLSelectBuilder().fromEntity(Student.class, "s").column(property(Student.id().fromAlias("s")))
                                            .where(eq(property(Student.group().fromAlias("s")), property(alias)))
                                            .where(in(property(Student.course().fromAlias("s")), courseList))
                                            .buildQuery())));
                }
            }
        }
                .filter(Group.title())
                .filter(Group.shortTitle())
                .order(Group.title());
    }
}
