/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentsOrderPrint;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;

import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

/**
 * @author dseleznev
 * Created on: 12.01.2010
 */
@State({
        @Bind(key = "orderId", binding = "order.id")
})
public class Model
{
    private StudentPaymentsOrder _order = new StudentPaymentsOrder();
    private byte[] _data;

    public StudentPaymentsOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentPaymentsOrder order)
    {
        _order = order;
    }

    public byte[] getData()
    {
        return _data;
    }

    public void setData(byte[] data)
    {
        _data = data;
    }
}