package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Приказ о назначении выплат студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentsOrderGen extends EntityBase
 implements IAbstractDocument{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder";
    public static final String ENTITY_NAME = "studentPaymentsOrder";
    public static final int VERSION_HASH = -1428383498;
    private static IEntityMeta ENTITY_META;

    public static final String P_CREATE_DATE = "createDate";
    public static final String P_COMMIT_DATE = "commitDate";
    public static final String P_COMMIT_DATE_SYSTEM = "commitDateSystem";
    public static final String P_NUMBER = "number";
    public static final String P_TEXT_PARAGRAPH = "textParagraph";
    public static final String P_EXECUTOR = "executor";
    public static final String L_STATE = "state";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_TYPE = "type";
    public static final String L_FOR_ORDER_CHANGING = "forOrderChanging";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_YEAR_PART = "yearPart";
    public static final String P_PLANNED_FUND = "plannedFund";
    public static final String P_ORDER_BASIC = "orderBasic";

    private Date _createDate;     // Дата формирования
    private Date _commitDate;     // Дата вступления в силу
    private Date _commitDateSystem;     // Дата проведения
    private String _number;     // Номер
    private String _textParagraph;     // Текстовый параграф
    private String _executor;     // Исполнитель
    private OrderStates _state;     // Состояние приказа
    private OrgUnit _orgUnit;     // Подразделение
    private StudentPaymentExtractType _type;     // Тип приказа. Первый уровень иерархии справочника
    private StudentPaymentsOrder _forOrderChanging;     // Вносит изменения в приказ
    private EducationYear _educationYear;     // Учебный год
    private YearDistributionPart _yearPart;     // Часть года
    private long _plannedFund;     // Планируемый размер фонда
    private String _orderBasic;     // Основание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null и должно быть уникальным.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Дата вступления в силу.
     */
    public Date getCommitDate()
    {
        return _commitDate;
    }

    /**
     * @param commitDate Дата вступления в силу.
     */
    public void setCommitDate(Date commitDate)
    {
        dirty(_commitDate, commitDate);
        _commitDate = commitDate;
    }

    /**
     * @return Дата проведения.
     */
    public Date getCommitDateSystem()
    {
        return _commitDateSystem;
    }

    /**
     * @param commitDateSystem Дата проведения.
     */
    public void setCommitDateSystem(Date commitDateSystem)
    {
        dirty(_commitDateSystem, commitDateSystem);
        _commitDateSystem = commitDateSystem;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Текстовый параграф.
     */
    public String getTextParagraph()
    {
        return _textParagraph;
    }

    /**
     * @param textParagraph Текстовый параграф.
     */
    public void setTextParagraph(String textParagraph)
    {
        dirty(_textParagraph, textParagraph);
        _textParagraph = textParagraph;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     */
    @NotNull
    public OrderStates getState()
    {
        return _state;
    }

    /**
     * @param state Состояние приказа. Свойство не может быть null.
     */
    public void setState(OrderStates state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Тип приказа. Первый уровень иерархии справочника. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип приказа. Первый уровень иерархии справочника. Свойство не может быть null.
     */
    public void setType(StudentPaymentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Вносит изменения в приказ.
     */
    public StudentPaymentsOrder getForOrderChanging()
    {
        return _forOrderChanging;
    }

    /**
     * @param forOrderChanging Вносит изменения в приказ.
     */
    public void setForOrderChanging(StudentPaymentsOrder forOrderChanging)
    {
        dirty(_forOrderChanging, forOrderChanging);
        _forOrderChanging = forOrderChanging;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть года.
     */
    public YearDistributionPart getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть года.
     */
    public void setYearPart(YearDistributionPart yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Планируемый размер фонда. Свойство не может быть null.
     */
    @NotNull
    public long getPlannedFund()
    {
        return _plannedFund;
    }

    /**
     * @param plannedFund Планируемый размер фонда. Свойство не может быть null.
     */
    public void setPlannedFund(long plannedFund)
    {
        dirty(_plannedFund, plannedFund);
        _plannedFund = plannedFund;
    }

    /**
     * @return Основание.
     */
    public String getOrderBasic()
    {
        return _orderBasic;
    }

    /**
     * @param orderBasic Основание.
     */
    public void setOrderBasic(String orderBasic)
    {
        dirty(_orderBasic, orderBasic);
        _orderBasic = orderBasic;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentsOrderGen)
        {
            setCreateDate(((StudentPaymentsOrder)another).getCreateDate());
            setCommitDate(((StudentPaymentsOrder)another).getCommitDate());
            setCommitDateSystem(((StudentPaymentsOrder)another).getCommitDateSystem());
            setNumber(((StudentPaymentsOrder)another).getNumber());
            setTextParagraph(((StudentPaymentsOrder)another).getTextParagraph());
            setExecutor(((StudentPaymentsOrder)another).getExecutor());
            setState(((StudentPaymentsOrder)another).getState());
            setOrgUnit(((StudentPaymentsOrder)another).getOrgUnit());
            setType(((StudentPaymentsOrder)another).getType());
            setForOrderChanging(((StudentPaymentsOrder)another).getForOrderChanging());
            setEducationYear(((StudentPaymentsOrder)another).getEducationYear());
            setYearPart(((StudentPaymentsOrder)another).getYearPart());
            setPlannedFund(((StudentPaymentsOrder)another).getPlannedFund());
            setOrderBasic(((StudentPaymentsOrder)another).getOrderBasic());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentsOrderGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentsOrder.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentsOrder();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "createDate":
                    return obj.getCreateDate();
                case "commitDate":
                    return obj.getCommitDate();
                case "commitDateSystem":
                    return obj.getCommitDateSystem();
                case "number":
                    return obj.getNumber();
                case "textParagraph":
                    return obj.getTextParagraph();
                case "executor":
                    return obj.getExecutor();
                case "state":
                    return obj.getState();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "type":
                    return obj.getType();
                case "forOrderChanging":
                    return obj.getForOrderChanging();
                case "educationYear":
                    return obj.getEducationYear();
                case "yearPart":
                    return obj.getYearPart();
                case "plannedFund":
                    return obj.getPlannedFund();
                case "orderBasic":
                    return obj.getOrderBasic();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "commitDate":
                    obj.setCommitDate((Date) value);
                    return;
                case "commitDateSystem":
                    obj.setCommitDateSystem((Date) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "textParagraph":
                    obj.setTextParagraph((String) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
                case "state":
                    obj.setState((OrderStates) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "type":
                    obj.setType((StudentPaymentExtractType) value);
                    return;
                case "forOrderChanging":
                    obj.setForOrderChanging((StudentPaymentsOrder) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "yearPart":
                    obj.setYearPart((YearDistributionPart) value);
                    return;
                case "plannedFund":
                    obj.setPlannedFund((Long) value);
                    return;
                case "orderBasic":
                    obj.setOrderBasic((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "createDate":
                        return true;
                case "commitDate":
                        return true;
                case "commitDateSystem":
                        return true;
                case "number":
                        return true;
                case "textParagraph":
                        return true;
                case "executor":
                        return true;
                case "state":
                        return true;
                case "orgUnit":
                        return true;
                case "type":
                        return true;
                case "forOrderChanging":
                        return true;
                case "educationYear":
                        return true;
                case "yearPart":
                        return true;
                case "plannedFund":
                        return true;
                case "orderBasic":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "createDate":
                    return true;
                case "commitDate":
                    return true;
                case "commitDateSystem":
                    return true;
                case "number":
                    return true;
                case "textParagraph":
                    return true;
                case "executor":
                    return true;
                case "state":
                    return true;
                case "orgUnit":
                    return true;
                case "type":
                    return true;
                case "forOrderChanging":
                    return true;
                case "educationYear":
                    return true;
                case "yearPart":
                    return true;
                case "plannedFund":
                    return true;
                case "orderBasic":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "createDate":
                    return Date.class;
                case "commitDate":
                    return Date.class;
                case "commitDateSystem":
                    return Date.class;
                case "number":
                    return String.class;
                case "textParagraph":
                    return String.class;
                case "executor":
                    return String.class;
                case "state":
                    return OrderStates.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "type":
                    return StudentPaymentExtractType.class;
                case "forOrderChanging":
                    return StudentPaymentsOrder.class;
                case "educationYear":
                    return EducationYear.class;
                case "yearPart":
                    return YearDistributionPart.class;
                case "plannedFund":
                    return Long.class;
                case "orderBasic":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentsOrder> _dslPath = new Path<StudentPaymentsOrder>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentsOrder");
    }
            

    /**
     * @return Дата формирования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getCommitDate()
     */
    public static PropertyPath<Date> commitDate()
    {
        return _dslPath.commitDate();
    }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getCommitDateSystem()
     */
    public static PropertyPath<Date> commitDateSystem()
    {
        return _dslPath.commitDateSystem();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getTextParagraph()
     */
    public static PropertyPath<String> textParagraph()
    {
        return _dslPath.textParagraph();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getState()
     */
    public static OrderStates.Path<OrderStates> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Тип приказа. Первый уровень иерархии справочника. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getType()
     */
    public static StudentPaymentExtractType.Path<StudentPaymentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Вносит изменения в приказ.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getForOrderChanging()
     */
    public static StudentPaymentsOrder.Path<StudentPaymentsOrder> forOrderChanging()
    {
        return _dslPath.forOrderChanging();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть года.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getYearPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Планируемый размер фонда. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getPlannedFund()
     */
    public static PropertyPath<Long> plannedFund()
    {
        return _dslPath.plannedFund();
    }

    /**
     * @return Основание.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getOrderBasic()
     */
    public static PropertyPath<String> orderBasic()
    {
        return _dslPath.orderBasic();
    }

    public static class Path<E extends StudentPaymentsOrder> extends EntityPath<E>
    {
        private PropertyPath<Date> _createDate;
        private PropertyPath<Date> _commitDate;
        private PropertyPath<Date> _commitDateSystem;
        private PropertyPath<String> _number;
        private PropertyPath<String> _textParagraph;
        private PropertyPath<String> _executor;
        private OrderStates.Path<OrderStates> _state;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private StudentPaymentExtractType.Path<StudentPaymentExtractType> _type;
        private StudentPaymentsOrder.Path<StudentPaymentsOrder> _forOrderChanging;
        private EducationYear.Path<EducationYear> _educationYear;
        private YearDistributionPart.Path<YearDistributionPart> _yearPart;
        private PropertyPath<Long> _plannedFund;
        private PropertyPath<String> _orderBasic;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(StudentPaymentsOrderGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Дата вступления в силу.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getCommitDate()
     */
        public PropertyPath<Date> commitDate()
        {
            if(_commitDate == null )
                _commitDate = new PropertyPath<Date>(StudentPaymentsOrderGen.P_COMMIT_DATE, this);
            return _commitDate;
        }

    /**
     * @return Дата проведения.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getCommitDateSystem()
     */
        public PropertyPath<Date> commitDateSystem()
        {
            if(_commitDateSystem == null )
                _commitDateSystem = new PropertyPath<Date>(StudentPaymentsOrderGen.P_COMMIT_DATE_SYSTEM, this);
            return _commitDateSystem;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(StudentPaymentsOrderGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Текстовый параграф.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getTextParagraph()
     */
        public PropertyPath<String> textParagraph()
        {
            if(_textParagraph == null )
                _textParagraph = new PropertyPath<String>(StudentPaymentsOrderGen.P_TEXT_PARAGRAPH, this);
            return _textParagraph;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(StudentPaymentsOrderGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Состояние приказа. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getState()
     */
        public OrderStates.Path<OrderStates> state()
        {
            if(_state == null )
                _state = new OrderStates.Path<OrderStates>(L_STATE, this);
            return _state;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Тип приказа. Первый уровень иерархии справочника. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getType()
     */
        public StudentPaymentExtractType.Path<StudentPaymentExtractType> type()
        {
            if(_type == null )
                _type = new StudentPaymentExtractType.Path<StudentPaymentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Вносит изменения в приказ.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getForOrderChanging()
     */
        public StudentPaymentsOrder.Path<StudentPaymentsOrder> forOrderChanging()
        {
            if(_forOrderChanging == null )
                _forOrderChanging = new StudentPaymentsOrder.Path<StudentPaymentsOrder>(L_FOR_ORDER_CHANGING, this);
            return _forOrderChanging;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть года.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getYearPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Планируемый размер фонда. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getPlannedFund()
     */
        public PropertyPath<Long> plannedFund()
        {
            if(_plannedFund == null )
                _plannedFund = new PropertyPath<Long>(StudentPaymentsOrderGen.P_PLANNED_FUND, this);
            return _plannedFund;
        }

    /**
     * @return Основание.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder#getOrderBasic()
     */
        public PropertyPath<String> orderBasic()
        {
            if(_orderBasic == null )
                _orderBasic = new PropertyPath<String>(StudentPaymentsOrderGen.P_ORDER_BASIC, this);
            return _orderBasic;
        }

        public Class getEntityClass()
        {
            return StudentPaymentsOrder.class;
        }

        public String getEntityName()
        {
            return "studentPaymentsOrder";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
