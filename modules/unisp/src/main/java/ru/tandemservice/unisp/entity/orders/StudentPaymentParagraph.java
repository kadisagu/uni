package ru.tandemservice.unisp.entity.orders;

import java.util.List;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unisp.entity.orders.gen.StudentPaymentParagraphGen;

/**
 * Параграф приказа о назначении выплат студентам
 */
public class StudentPaymentParagraph extends StudentPaymentParagraphGen implements IAbstractParagraph<StudentPaymentsOrder>
{
    @Override
    public int getExtractCount()
    {
        return UniDaoFacade.getCoreDao().getCount(StudentPaymentExtract.class, IAbstractExtract.L_PARAGRAPH, this);
    }

    @Override
    public List<? extends IAbstractExtract> getExtractList()
    {
        return UniDaoFacade.getCoreDao().getList(StudentPaymentExtract.class, IAbstractExtract.L_PARAGRAPH, this, IAbstractExtract.P_NUMBER);
    }
}