/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.catalog.studentPayment.StudentPaymentPub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import ru.tandemservice.unibase.UniBaseUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentType;

/**
 * @author AutoGenerator
 * Created on 19.12.2009
 */
public class DAO extends DefaultCatalogPubDAO<StudentPayment, Model> implements IDAO
{
    @Override
    protected void prepareListItemDataSource(Model model)
    {
        List<StudentPaymentItemWrapper> paymentWrappersList = new ArrayList<StudentPaymentItemWrapper>();
        List<StudentPaymentType> paymentTypesList = CatalogManager.instance().dao().getCatalogItemListOrderByCode(StudentPaymentType.class);
        List<StudentPayment> paymentsList = CatalogManager.instance().dao().getCatalogItemListOrderByCode(StudentPayment.class);

        Map<Long, List<StudentPayment>> paymentsMap = new HashMap<Long, List<StudentPayment>>();
        for (StudentPayment payment : paymentsList)
        {
            List<StudentPayment> payments = paymentsMap.get(payment.getType().getId());
            if (null == payments) payments = new ArrayList<StudentPayment>();
            payments.add(payment);
            paymentsMap.put(payment.getType().getId(), payments);
        }

        for (StudentPaymentType type : paymentTypesList)
        {
            StudentPaymentItemWrapper typeWrapper = new StudentPaymentItemWrapper(type.getId(), type, null);
            paymentWrappersList.add(typeWrapper);

            if (paymentsMap.containsKey(type.getId()))
            {
                for (StudentPayment payment : paymentsMap.get(type.getId()))
                {
                    paymentWrappersList.add(new StudentPaymentItemWrapper(payment.getId(), payment, typeWrapper));
                }
            }
        }

        model.getOwnDataSource().setCountRow(paymentWrappersList.size());
        UniBaseUtils.createPage(model.getOwnDataSource(), paymentWrappersList);

        model.getDataSource().setCountRow(paymentsList.size());
        UniBaseUtils.createPage(model.getDataSource(), paymentsList);
    }
}