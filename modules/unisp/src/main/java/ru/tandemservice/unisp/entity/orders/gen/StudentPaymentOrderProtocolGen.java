package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Протокол заседания стипендиальной комиссии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentOrderProtocolGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol";
    public static final String ENTITY_NAME = "studentPaymentOrderProtocol";
    public static final int VERSION_HASH = 1124408265;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ORDER = "order";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";
    public static final String P_PROTOCOL_DATE = "protocolDate";
    public static final String P_COMM_ORDER_NUMBER = "commOrderNumber";
    public static final String P_COMM_ORDER_DATE = "commOrderDate";
    public static final String P_SESSION_START_DATE = "sessionStartDate";
    public static final String P_SESSION_END_DATE = "sessionEndDate";
    public static final String P_RETAKE_DATE = "retakeDate";
    public static final String P_CHAIR = "chair";
    public static final String P_COMMISSION = "commission";
    public static final String P_CAPTAIN = "captain";
    public static final String P_GROUPS_WITHOUT_EXTRACTS = "groupsWithoutExtracts";
    public static final String P_STUDENTS_WITHOUT_EXTRACTS = "studentsWithoutExtracts";
    public static final String P_QUALIFICATION = "qualification";
    public static final String P_COURSE = "course";
    public static final String P_GROUP = "group";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private StudentPaymentsOrder _order;     // Приказ о назначении выплат студентам
    private String _protocolNumber;     // Номер протокола
    private Date _protocolDate;     // Дата протокола
    private String _commOrderNumber;     // Номер приказа (утв-щего комиссию)
    private Date _commOrderDate;     // Дата приказа (утв-щего комиссию)
    private Date _sessionStartDate;     // Дата начала сессии
    private Date _sessionEndDate;     // Дата окончания сессии
    private Date _retakeDate;     // Дата сдачи задолженностей
    private String _chair;     // Председатель комиссии
    private String _commission;     // Члены комиссии
    private String _captain;     // Староста
    private boolean _groupsWithoutExtracts;     // Печатать группы без назначенных выплат
    private boolean _studentsWithoutExtracts;     // Печатать студентов без назначенных выплат
    private String _qualification;     // Квалификация
    private String _course;     // Курс
    private String _group;     // Группа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приказ о назначении выплат студентам. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentsOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ о назначении выплат студентам. Свойство не может быть null.
     */
    public void setOrder(StudentPaymentsOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Номер протокола.
     */
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    /**
     * @return Дата протокола.
     */
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    /**
     * @return Номер приказа (утв-щего комиссию).
     */
    @Length(max=255)
    public String getCommOrderNumber()
    {
        return _commOrderNumber;
    }

    /**
     * @param commOrderNumber Номер приказа (утв-щего комиссию).
     */
    public void setCommOrderNumber(String commOrderNumber)
    {
        dirty(_commOrderNumber, commOrderNumber);
        _commOrderNumber = commOrderNumber;
    }

    /**
     * @return Дата приказа (утв-щего комиссию).
     */
    public Date getCommOrderDate()
    {
        return _commOrderDate;
    }

    /**
     * @param commOrderDate Дата приказа (утв-щего комиссию).
     */
    public void setCommOrderDate(Date commOrderDate)
    {
        dirty(_commOrderDate, commOrderDate);
        _commOrderDate = commOrderDate;
    }

    /**
     * @return Дата начала сессии.
     */
    public Date getSessionStartDate()
    {
        return _sessionStartDate;
    }

    /**
     * @param sessionStartDate Дата начала сессии.
     */
    public void setSessionStartDate(Date sessionStartDate)
    {
        dirty(_sessionStartDate, sessionStartDate);
        _sessionStartDate = sessionStartDate;
    }

    /**
     * @return Дата окончания сессии.
     */
    public Date getSessionEndDate()
    {
        return _sessionEndDate;
    }

    /**
     * @param sessionEndDate Дата окончания сессии.
     */
    public void setSessionEndDate(Date sessionEndDate)
    {
        dirty(_sessionEndDate, sessionEndDate);
        _sessionEndDate = sessionEndDate;
    }

    /**
     * @return Дата сдачи задолженностей.
     */
    public Date getRetakeDate()
    {
        return _retakeDate;
    }

    /**
     * @param retakeDate Дата сдачи задолженностей.
     */
    public void setRetakeDate(Date retakeDate)
    {
        dirty(_retakeDate, retakeDate);
        _retakeDate = retakeDate;
    }

    /**
     * @return Председатель комиссии.
     */
    @Length(max=255)
    public String getChair()
    {
        return _chair;
    }

    /**
     * @param chair Председатель комиссии.
     */
    public void setChair(String chair)
    {
        dirty(_chair, chair);
        _chair = chair;
    }

    /**
     * @return Члены комиссии.
     */
    @Length(max=255)
    public String getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Члены комиссии.
     */
    public void setCommission(String commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Староста.
     */
    @Length(max=255)
    public String getCaptain()
    {
        return _captain;
    }

    /**
     * @param captain Староста.
     */
    public void setCaptain(String captain)
    {
        dirty(_captain, captain);
        _captain = captain;
    }

    /**
     * @return Печатать группы без назначенных выплат. Свойство не может быть null.
     */
    @NotNull
    public boolean isGroupsWithoutExtracts()
    {
        return _groupsWithoutExtracts;
    }

    /**
     * @param groupsWithoutExtracts Печатать группы без назначенных выплат. Свойство не может быть null.
     */
    public void setGroupsWithoutExtracts(boolean groupsWithoutExtracts)
    {
        dirty(_groupsWithoutExtracts, groupsWithoutExtracts);
        _groupsWithoutExtracts = groupsWithoutExtracts;
    }

    /**
     * @return Печатать студентов без назначенных выплат. Свойство не может быть null.
     */
    @NotNull
    public boolean isStudentsWithoutExtracts()
    {
        return _studentsWithoutExtracts;
    }

    /**
     * @param studentsWithoutExtracts Печатать студентов без назначенных выплат. Свойство не может быть null.
     */
    public void setStudentsWithoutExtracts(boolean studentsWithoutExtracts)
    {
        dirty(_studentsWithoutExtracts, studentsWithoutExtracts);
        _studentsWithoutExtracts = studentsWithoutExtracts;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация.
     */
    public void setQualification(String qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа.
     */
    @Length(max=1000)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentOrderProtocolGen)
        {
            setContent(((StudentPaymentOrderProtocol)another).getContent());
            setFormingDate(((StudentPaymentOrderProtocol)another).getFormingDate());
            setOrder(((StudentPaymentOrderProtocol)another).getOrder());
            setProtocolNumber(((StudentPaymentOrderProtocol)another).getProtocolNumber());
            setProtocolDate(((StudentPaymentOrderProtocol)another).getProtocolDate());
            setCommOrderNumber(((StudentPaymentOrderProtocol)another).getCommOrderNumber());
            setCommOrderDate(((StudentPaymentOrderProtocol)another).getCommOrderDate());
            setSessionStartDate(((StudentPaymentOrderProtocol)another).getSessionStartDate());
            setSessionEndDate(((StudentPaymentOrderProtocol)another).getSessionEndDate());
            setRetakeDate(((StudentPaymentOrderProtocol)another).getRetakeDate());
            setChair(((StudentPaymentOrderProtocol)another).getChair());
            setCommission(((StudentPaymentOrderProtocol)another).getCommission());
            setCaptain(((StudentPaymentOrderProtocol)another).getCaptain());
            setGroupsWithoutExtracts(((StudentPaymentOrderProtocol)another).isGroupsWithoutExtracts());
            setStudentsWithoutExtracts(((StudentPaymentOrderProtocol)another).isStudentsWithoutExtracts());
            setQualification(((StudentPaymentOrderProtocol)another).getQualification());
            setCourse(((StudentPaymentOrderProtocol)another).getCourse());
            setGroup(((StudentPaymentOrderProtocol)another).getGroup());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentOrderProtocolGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentOrderProtocol.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentOrderProtocol();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "order":
                    return obj.getOrder();
                case "protocolNumber":
                    return obj.getProtocolNumber();
                case "protocolDate":
                    return obj.getProtocolDate();
                case "commOrderNumber":
                    return obj.getCommOrderNumber();
                case "commOrderDate":
                    return obj.getCommOrderDate();
                case "sessionStartDate":
                    return obj.getSessionStartDate();
                case "sessionEndDate":
                    return obj.getSessionEndDate();
                case "retakeDate":
                    return obj.getRetakeDate();
                case "chair":
                    return obj.getChair();
                case "commission":
                    return obj.getCommission();
                case "captain":
                    return obj.getCaptain();
                case "groupsWithoutExtracts":
                    return obj.isGroupsWithoutExtracts();
                case "studentsWithoutExtracts":
                    return obj.isStudentsWithoutExtracts();
                case "qualification":
                    return obj.getQualification();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "order":
                    obj.setOrder((StudentPaymentsOrder) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
                case "commOrderNumber":
                    obj.setCommOrderNumber((String) value);
                    return;
                case "commOrderDate":
                    obj.setCommOrderDate((Date) value);
                    return;
                case "sessionStartDate":
                    obj.setSessionStartDate((Date) value);
                    return;
                case "sessionEndDate":
                    obj.setSessionEndDate((Date) value);
                    return;
                case "retakeDate":
                    obj.setRetakeDate((Date) value);
                    return;
                case "chair":
                    obj.setChair((String) value);
                    return;
                case "commission":
                    obj.setCommission((String) value);
                    return;
                case "captain":
                    obj.setCaptain((String) value);
                    return;
                case "groupsWithoutExtracts":
                    obj.setGroupsWithoutExtracts((Boolean) value);
                    return;
                case "studentsWithoutExtracts":
                    obj.setStudentsWithoutExtracts((Boolean) value);
                    return;
                case "qualification":
                    obj.setQualification((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "order":
                        return true;
                case "protocolNumber":
                        return true;
                case "protocolDate":
                        return true;
                case "commOrderNumber":
                        return true;
                case "commOrderDate":
                        return true;
                case "sessionStartDate":
                        return true;
                case "sessionEndDate":
                        return true;
                case "retakeDate":
                        return true;
                case "chair":
                        return true;
                case "commission":
                        return true;
                case "captain":
                        return true;
                case "groupsWithoutExtracts":
                        return true;
                case "studentsWithoutExtracts":
                        return true;
                case "qualification":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "order":
                    return true;
                case "protocolNumber":
                    return true;
                case "protocolDate":
                    return true;
                case "commOrderNumber":
                    return true;
                case "commOrderDate":
                    return true;
                case "sessionStartDate":
                    return true;
                case "sessionEndDate":
                    return true;
                case "retakeDate":
                    return true;
                case "chair":
                    return true;
                case "commission":
                    return true;
                case "captain":
                    return true;
                case "groupsWithoutExtracts":
                    return true;
                case "studentsWithoutExtracts":
                    return true;
                case "qualification":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "order":
                    return StudentPaymentsOrder.class;
                case "protocolNumber":
                    return String.class;
                case "protocolDate":
                    return Date.class;
                case "commOrderNumber":
                    return String.class;
                case "commOrderDate":
                    return Date.class;
                case "sessionStartDate":
                    return Date.class;
                case "sessionEndDate":
                    return Date.class;
                case "retakeDate":
                    return Date.class;
                case "chair":
                    return String.class;
                case "commission":
                    return String.class;
                case "captain":
                    return String.class;
                case "groupsWithoutExtracts":
                    return Boolean.class;
                case "studentsWithoutExtracts":
                    return Boolean.class;
                case "qualification":
                    return String.class;
                case "course":
                    return String.class;
                case "group":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentOrderProtocol> _dslPath = new Path<StudentPaymentOrderProtocol>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentOrderProtocol");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приказ о назначении выплат студентам. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getOrder()
     */
    public static StudentPaymentsOrder.Path<StudentPaymentsOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    /**
     * @return Дата протокола.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    /**
     * @return Номер приказа (утв-щего комиссию).
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCommOrderNumber()
     */
    public static PropertyPath<String> commOrderNumber()
    {
        return _dslPath.commOrderNumber();
    }

    /**
     * @return Дата приказа (утв-щего комиссию).
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCommOrderDate()
     */
    public static PropertyPath<Date> commOrderDate()
    {
        return _dslPath.commOrderDate();
    }

    /**
     * @return Дата начала сессии.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getSessionStartDate()
     */
    public static PropertyPath<Date> sessionStartDate()
    {
        return _dslPath.sessionStartDate();
    }

    /**
     * @return Дата окончания сессии.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getSessionEndDate()
     */
    public static PropertyPath<Date> sessionEndDate()
    {
        return _dslPath.sessionEndDate();
    }

    /**
     * @return Дата сдачи задолженностей.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getRetakeDate()
     */
    public static PropertyPath<Date> retakeDate()
    {
        return _dslPath.retakeDate();
    }

    /**
     * @return Председатель комиссии.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getChair()
     */
    public static PropertyPath<String> chair()
    {
        return _dslPath.chair();
    }

    /**
     * @return Члены комиссии.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCommission()
     */
    public static PropertyPath<String> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Староста.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCaptain()
     */
    public static PropertyPath<String> captain()
    {
        return _dslPath.captain();
    }

    /**
     * @return Печатать группы без назначенных выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#isGroupsWithoutExtracts()
     */
    public static PropertyPath<Boolean> groupsWithoutExtracts()
    {
        return _dslPath.groupsWithoutExtracts();
    }

    /**
     * @return Печатать студентов без назначенных выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#isStudentsWithoutExtracts()
     */
    public static PropertyPath<Boolean> studentsWithoutExtracts()
    {
        return _dslPath.studentsWithoutExtracts();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getQualification()
     */
    public static PropertyPath<String> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    public static class Path<E extends StudentPaymentOrderProtocol> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private StudentPaymentsOrder.Path<StudentPaymentsOrder> _order;
        private PropertyPath<String> _protocolNumber;
        private PropertyPath<Date> _protocolDate;
        private PropertyPath<String> _commOrderNumber;
        private PropertyPath<Date> _commOrderDate;
        private PropertyPath<Date> _sessionStartDate;
        private PropertyPath<Date> _sessionEndDate;
        private PropertyPath<Date> _retakeDate;
        private PropertyPath<String> _chair;
        private PropertyPath<String> _commission;
        private PropertyPath<String> _captain;
        private PropertyPath<Boolean> _groupsWithoutExtracts;
        private PropertyPath<Boolean> _studentsWithoutExtracts;
        private PropertyPath<String> _qualification;
        private PropertyPath<String> _course;
        private PropertyPath<String> _group;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(StudentPaymentOrderProtocolGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приказ о назначении выплат студентам. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getOrder()
     */
        public StudentPaymentsOrder.Path<StudentPaymentsOrder> order()
        {
            if(_order == null )
                _order = new StudentPaymentsOrder.Path<StudentPaymentsOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(StudentPaymentOrderProtocolGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

    /**
     * @return Дата протокола.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(StudentPaymentOrderProtocolGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

    /**
     * @return Номер приказа (утв-щего комиссию).
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCommOrderNumber()
     */
        public PropertyPath<String> commOrderNumber()
        {
            if(_commOrderNumber == null )
                _commOrderNumber = new PropertyPath<String>(StudentPaymentOrderProtocolGen.P_COMM_ORDER_NUMBER, this);
            return _commOrderNumber;
        }

    /**
     * @return Дата приказа (утв-щего комиссию).
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCommOrderDate()
     */
        public PropertyPath<Date> commOrderDate()
        {
            if(_commOrderDate == null )
                _commOrderDate = new PropertyPath<Date>(StudentPaymentOrderProtocolGen.P_COMM_ORDER_DATE, this);
            return _commOrderDate;
        }

    /**
     * @return Дата начала сессии.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getSessionStartDate()
     */
        public PropertyPath<Date> sessionStartDate()
        {
            if(_sessionStartDate == null )
                _sessionStartDate = new PropertyPath<Date>(StudentPaymentOrderProtocolGen.P_SESSION_START_DATE, this);
            return _sessionStartDate;
        }

    /**
     * @return Дата окончания сессии.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getSessionEndDate()
     */
        public PropertyPath<Date> sessionEndDate()
        {
            if(_sessionEndDate == null )
                _sessionEndDate = new PropertyPath<Date>(StudentPaymentOrderProtocolGen.P_SESSION_END_DATE, this);
            return _sessionEndDate;
        }

    /**
     * @return Дата сдачи задолженностей.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getRetakeDate()
     */
        public PropertyPath<Date> retakeDate()
        {
            if(_retakeDate == null )
                _retakeDate = new PropertyPath<Date>(StudentPaymentOrderProtocolGen.P_RETAKE_DATE, this);
            return _retakeDate;
        }

    /**
     * @return Председатель комиссии.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getChair()
     */
        public PropertyPath<String> chair()
        {
            if(_chair == null )
                _chair = new PropertyPath<String>(StudentPaymentOrderProtocolGen.P_CHAIR, this);
            return _chair;
        }

    /**
     * @return Члены комиссии.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCommission()
     */
        public PropertyPath<String> commission()
        {
            if(_commission == null )
                _commission = new PropertyPath<String>(StudentPaymentOrderProtocolGen.P_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Староста.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCaptain()
     */
        public PropertyPath<String> captain()
        {
            if(_captain == null )
                _captain = new PropertyPath<String>(StudentPaymentOrderProtocolGen.P_CAPTAIN, this);
            return _captain;
        }

    /**
     * @return Печатать группы без назначенных выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#isGroupsWithoutExtracts()
     */
        public PropertyPath<Boolean> groupsWithoutExtracts()
        {
            if(_groupsWithoutExtracts == null )
                _groupsWithoutExtracts = new PropertyPath<Boolean>(StudentPaymentOrderProtocolGen.P_GROUPS_WITHOUT_EXTRACTS, this);
            return _groupsWithoutExtracts;
        }

    /**
     * @return Печатать студентов без назначенных выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#isStudentsWithoutExtracts()
     */
        public PropertyPath<Boolean> studentsWithoutExtracts()
        {
            if(_studentsWithoutExtracts == null )
                _studentsWithoutExtracts = new PropertyPath<Boolean>(StudentPaymentOrderProtocolGen.P_STUDENTS_WITHOUT_EXTRACTS, this);
            return _studentsWithoutExtracts;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getQualification()
     */
        public PropertyPath<String> qualification()
        {
            if(_qualification == null )
                _qualification = new PropertyPath<String>(StudentPaymentOrderProtocolGen.P_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(StudentPaymentOrderProtocolGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(StudentPaymentOrderProtocolGen.P_GROUP, this);
            return _group;
        }

        public Class getEntityClass()
        {
            return StudentPaymentOrderProtocol.class;
        }

        public String getEntityName()
        {
            return "studentPaymentOrderProtocol";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
