/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentsOrderAdd;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unisp.dao.UnispDaoFacade;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.Date;

/**
 * @author dseleznev
 * Created on: 23.12.2009
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        if (null != model.getOrgUnitId()) model.setOrgUnit((OrgUnit)getNotNull(model.getOrgUnitId()));

        if (null != model.getOrder().getId())
        {
            model.setAddForm(false);
            model.setOrder(getNotNull(StudentPaymentsOrder.class, model.getOrder().getId()));
        }
        else
        {
            model.setAddForm(true);
            model.getOrder().setCreateDate(new Date());
            if (null != model.getOrgUnit() && null == model.getOrder().getOrgUnit()) model.getOrder().setOrgUnit(model.getOrgUnit());
            model.getOrder().setEducationYear(UniDaoFacade.getCoreDao().get(EducationYear.class, EducationYear.P_CURRENT, Boolean.TRUE));
        }

        model.setEditForm(!model.isAddForm());

        model.setOrderTypesList(UnispDaoFacade.getUnispExtractDAO().getStudentPaymentOrderTypesList("", model.getOrder().getOrgUnit()));
        model.setEducationYearsList(new EducationYearModel());
        model.setEmployeePostModel(new OrderExecutorSelectModel());
        model.setOrgUnitListModel(new OrgUnitAutocompleteModel());

        model.setForOrderChangingListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(StudentPaymentsOrder.ENTITY_CLASS, "o");
                builder.add(MQExpression.eq("o", IAbstractOrder.L_STATE + "." + OrderStates.P_CODE, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED));
                builder.add(MQExpression.like("o", IAbstractOrder.P_NUMBER, CoreStringUtils.escapeLike(filter)));
                return new ListResult<>(builder.getResultList(getSession()));
            }
        });

        model.setYearPartModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(YearDistributionPart.class, "ep");
                FilterUtils.applySimpleLikeFilter(builder, "ep", YearDistributionPart.title(), filter);
                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });
    }

    @Override
    public void update(Model model)
    {
        UnispDaoFacade.getUnispExtractDAO().checkOrderNumber(model.getOrder());

        String executor = OrderExecutorSelectModel.getExecutor(model.getEmployeePost());
        if (executor != null)
            model.getOrder().setExecutor(executor);
        getSession().saveOrUpdate(model.getOrder());
    }
}