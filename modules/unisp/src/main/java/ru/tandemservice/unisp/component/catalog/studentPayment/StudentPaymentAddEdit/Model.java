/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.catalog.studentPayment.StudentPaymentAddEdit;

import java.util.List;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.unisp.entity.catalog.PaymentKind;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentType;

/**
 * @author AutoGenerator
 * Created on 19.12.2009
 */
public class Model extends DefaultCatalogAddEditModel<StudentPayment>
{
    private List<StudentPaymentType> _paymentTypesList;
    private List<PaymentKind> _paymentKindsList;

    public List<StudentPaymentType> getPaymentTypesList()
    {
        return _paymentTypesList;
    }

    public void setPaymentTypesList(List<StudentPaymentType> paymentTypesList)
    {
        this._paymentTypesList = paymentTypesList;
    }

    public List<PaymentKind> getPaymentKindsList()
    {
        return _paymentKindsList;
    }

    public void setPaymentKindsList(List<PaymentKind> paymentKindsList)
    {
        this._paymentKindsList = paymentKindsList;
    }
}