package ru.tandemservice.unisp.component.settings.UsedStudentPaymentOrderTypesSettingsEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 31.01.11
 * Time: 15:06
 * To change this template use File | Settings | File Templates.
 */
public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        model.setExtractType(get(StudentPaymentExtractType.class, model.getStudentPaymentExtractTypeId()));
    }

    @Override
    public void update(Model model)
    {
        update(model.getExtractType());
    }
}
