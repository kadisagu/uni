package ru.tandemservice.unisp.entity.orders.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractBasic;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;
import ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToBasicsRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь причин с основаниями документов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentReasonToBasicsRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToBasicsRel";
    public static final String ENTITY_NAME = "studentPaymentReasonToBasicsRel";
    public static final int VERSION_HASH = -1112796301;
    private static IEntityMeta ENTITY_META;

    public static final String L_FIRST = "first";
    public static final String L_SECOND = "second";

    private StudentPaymentExtractReason _first;     // Причины документов о назначении выплат
    private StudentPaymentExtractBasic _second;     // Основания документов о назначении выплат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Причины документов о назначении выплат. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentExtractReason getFirst()
    {
        return _first;
    }

    /**
     * @param first Причины документов о назначении выплат. Свойство не может быть null.
     */
    public void setFirst(StudentPaymentExtractReason first)
    {
        dirty(_first, first);
        _first = first;
    }

    /**
     * @return Основания документов о назначении выплат. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentExtractBasic getSecond()
    {
        return _second;
    }

    /**
     * @param second Основания документов о назначении выплат. Свойство не может быть null.
     */
    public void setSecond(StudentPaymentExtractBasic second)
    {
        dirty(_second, second);
        _second = second;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentReasonToBasicsRelGen)
        {
            setFirst(((StudentPaymentReasonToBasicsRel)another).getFirst());
            setSecond(((StudentPaymentReasonToBasicsRel)another).getSecond());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentReasonToBasicsRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentReasonToBasicsRel.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentReasonToBasicsRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "first":
                    return obj.getFirst();
                case "second":
                    return obj.getSecond();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "first":
                    obj.setFirst((StudentPaymentExtractReason) value);
                    return;
                case "second":
                    obj.setSecond((StudentPaymentExtractBasic) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "first":
                        return true;
                case "second":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "first":
                    return true;
                case "second":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "first":
                    return StudentPaymentExtractReason.class;
                case "second":
                    return StudentPaymentExtractBasic.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentReasonToBasicsRel> _dslPath = new Path<StudentPaymentReasonToBasicsRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentReasonToBasicsRel");
    }
            

    /**
     * @return Причины документов о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToBasicsRel#getFirst()
     */
    public static StudentPaymentExtractReason.Path<StudentPaymentExtractReason> first()
    {
        return _dslPath.first();
    }

    /**
     * @return Основания документов о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToBasicsRel#getSecond()
     */
    public static StudentPaymentExtractBasic.Path<StudentPaymentExtractBasic> second()
    {
        return _dslPath.second();
    }

    public static class Path<E extends StudentPaymentReasonToBasicsRel> extends EntityPath<E>
    {
        private StudentPaymentExtractReason.Path<StudentPaymentExtractReason> _first;
        private StudentPaymentExtractBasic.Path<StudentPaymentExtractBasic> _second;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Причины документов о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToBasicsRel#getFirst()
     */
        public StudentPaymentExtractReason.Path<StudentPaymentExtractReason> first()
        {
            if(_first == null )
                _first = new StudentPaymentExtractReason.Path<StudentPaymentExtractReason>(L_FIRST, this);
            return _first;
        }

    /**
     * @return Основания документов о назначении выплат. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.orders.StudentPaymentReasonToBasicsRel#getSecond()
     */
        public StudentPaymentExtractBasic.Path<StudentPaymentExtractBasic> second()
        {
            if(_second == null )
                _second = new StudentPaymentExtractBasic.Path<StudentPaymentExtractBasic>(L_SECOND, this);
            return _second;
        }

        public Class getEntityClass()
        {
            return StudentPaymentReasonToBasicsRel.class;
        }

        public String getEntityName()
        {
            return "studentPaymentReasonToBasicsRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
