// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolAdd;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.unisp.entity.orders.StudentPaymentOrderProtocol;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.List;

/**
 * @author oleyba
 * @since 13.01.2010
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "order.id")
public class Model
{
    private StudentPaymentsOrder _order = new StudentPaymentsOrder();

    private StudentPaymentOrderProtocol protocol = new StudentPaymentOrderProtocol();

    private ISelectModel commissionModel;
    private ISelectModel qualificationModel;
    private ISelectModel courseModel;
    private ISelectModel groupModel;
    private ISelectModel workPlanRowKindModel;
    private ISelectModel captainModel;

    private EmployeePost chair;
    private List<EmployeePost> commission;
    private Student captain;

    private boolean captainsFromGroup = true;
    private List<Qualifications> qualificationList;
    private List<Course> courseList;
    private List<Group> groupList;
    private List<EppWorkPlanRowKind> workPlanRowKindList;
    private boolean _withNullGroup;

    private boolean initialized = false;

    public StudentPaymentsOrder getOrder()
    {
        return _order;
    }

    public void setOrder(StudentPaymentsOrder order)
    {
        _order = order;
    }

    public ISelectModel getCommissionModel()
    {
        return commissionModel;
    }

    public void setCommissionModel(ISelectModel commissionModel)
    {
        this.commissionModel = commissionModel;
    }

    public EmployeePost getChair()
    {
        return chair;
    }

    public void setChair(EmployeePost chair)
    {
        this.chair = chair;
    }

    public List<EmployeePost> getCommission()
    {
        return commission;
    }

    public void setCommission(List<EmployeePost> commission)
    {
        this.commission = commission;
    }

    public ISelectModel getQualificationModel()
    {
        return qualificationModel;
    }

    public void setQualificationModel(ISelectModel qualificationModel)
    {
        this.qualificationModel = qualificationModel;
    }

    public ISelectModel getCourseModel()
    {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel)
    {
        this.courseModel = courseModel;
    }

    public ISelectModel getGroupModel()
    {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        this.groupModel = groupModel;
    }

    public ISelectModel getWorkPlanRowKindModel()
    {
        return workPlanRowKindModel;
    }

    public void setWorkPlanRowKindModel(ISelectModel workPlanRowKindModel)
    {
        this.workPlanRowKindModel = workPlanRowKindModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        this.qualificationList = qualificationList;
    }

    public List<Course> getCourseList()
    {
        return courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        this.courseList = courseList;
    }

    public List<Group> getGroupList()
    {
        return groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        this.groupList = groupList;
    }

    public List<EppWorkPlanRowKind> getWorkPlanRowKindList()
    {
        return workPlanRowKindList;
    }

    public void setWorkPlanRowKindList(List<EppWorkPlanRowKind> workPlanRowKindList)
    {
        this.workPlanRowKindList = workPlanRowKindList;
    }

    public boolean isInitialized()
    {
        return initialized;
    }

    public void setInitialized(boolean initialized)
    {
        this.initialized = initialized;
    }

    public ISelectModel getCaptainModel()
    {
        return captainModel;
    }

    public void setCaptainModel(ISelectModel captainModel)
    {
        this.captainModel = captainModel;
    }

    public Student getCaptain()
    {
        return captain;
    }

    public void setCaptain(Student captain)
    {
        this.captain = captain;
    }

    public boolean isCaptainsFromGroup()
    {
        return captainsFromGroup;
    }

    public void setCaptainsFromGroup(boolean captainsFromGroup)
    {
        this.captainsFromGroup = captainsFromGroup;
    }

    public StudentPaymentOrderProtocol getProtocol()
    {
        return protocol;
    }

    public void setProtocol(StudentPaymentOrderProtocol protocol)
    {
        this.protocol = protocol;
    }

    public boolean isWithNullGroup()
    {
        return _withNullGroup;
    }

    public void setWithNullGroup(boolean withNullGroup)
    {
        _withNullGroup = withNullGroup;
    }
}
