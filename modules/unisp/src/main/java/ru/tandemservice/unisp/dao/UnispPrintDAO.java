/**
 * $Id$
 */
package ru.tandemservice.unisp.dao;

import org.tandemframework.shared.commonbase.base.util.key.FourKey;
import org.tandemframework.shared.commonbase.base.util.key.TripletKey;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unisp.component.order.VirtualParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 12.01.2010
 */
public class UnispPrintDAO extends UniDao implements IUnispPrintDAO
{
    public static Comparator<VirtualParagraph<StudentPaymentAppointmentExtract>> REPORT_PARAGRAPHS_COMPARATOR = new Comparator<VirtualParagraph<StudentPaymentAppointmentExtract>>()
    {
        @Override
        public int compare(VirtualParagraph<StudentPaymentAppointmentExtract> par1, VirtualParagraph<StudentPaymentAppointmentExtract> par2)
        {
            StudentPaymentAppointmentExtract extr1 = par1.getFirstExtract();
            StudentPaymentAppointmentExtract extr2 = par2.getFirstExtract();

            if(!extr1.getEntity().getCourse().equals(extr2.getEntity().getCourse())) return extr1.getEntity().getCourse().getIntValue() < extr2.getEntity().getCourse().getIntValue() ? -1 : 1;
            if(!extr1.getPaymentStartDate().equals(extr2.getPaymentStartDate())) return extr1.getPaymentStartDate().compareTo(extr2.getPaymentStartDate());
            return extr1.getPaymentStopDate().compareTo(extr2.getPaymentStopDate());
        }
    };

    public static Comparator<VirtualParagraph<StudentPaymentAppointmentExtract>> PARAGRAPHS_COMPARATOR = new Comparator<VirtualParagraph<StudentPaymentAppointmentExtract>>()
    {
        @Override
        public int compare(VirtualParagraph<StudentPaymentAppointmentExtract> par1, VirtualParagraph<StudentPaymentAppointmentExtract> par2)
        {
            StudentPaymentAppointmentExtract extr1 = par1.getFirstExtract();
            StudentPaymentAppointmentExtract extr2 = par2.getFirstExtract();

            if(null == extr1.getEntity().getGroup() && null != extr2.getEntity().getGroup()) return 1;
            if(null != extr1.getEntity().getGroup() && null == extr2.getEntity().getGroup()) return -1;
            if(null == extr1.getEntity().getGroup() && null == extr2.getEntity().getGroup()) return 0;
            if(!extr1.getEntity().getCourse().equals(extr2.getEntity().getCourse())) return Integer.valueOf(extr1.getEntity().getCourse().getIntValue()).compareTo(extr2.getEntity().getCourse().getIntValue());
            if(!extr1.getPaymentStartDate().equals(extr2.getPaymentStartDate())) return extr1.getPaymentStartDate().compareTo(extr2.getPaymentStartDate());
            if(!extr1.getEntity().getGroup().equals(extr2.getEntity().getGroup())) return extr1.getEntity().getGroup().getTitle().compareTo(extr2.getEntity().getGroup().getTitle());
            return extr1.getPaymentStopDate().compareTo(extr2.getPaymentStopDate());
        }
    };

    public static final Comparator<StudentPaymentExtract> EXTRACT_STUDENT_FIO_COMPARATOR = CommonCollator.comparing(e -> e.getEntity().getFullFio(), true);

    protected class ReportDataComparator implements Comparator<String[]>
    {
        private int _fieldToCompareNum;

        public ReportDataComparator(int fieldToCompareNum)
        {
            _fieldToCompareNum = fieldToCompareNum;
        }

        @Override
        public int compare(String[] line1, String[] line2)
        {
            return line1[_fieldToCompareNum].compareTo(line2[_fieldToCompareNum]);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VirtualParagraph> prepareOrderReportsData(StudentPaymentsOrder order)
    {
        List<VirtualParagraph> paragraphsList = new ArrayList<>();
        Map<TripletKey<Course, Date, Date>, VirtualParagraph<StudentPaymentAppointmentExtract>> paragraphsMap = new HashMap<>();


        for (IAbstractParagraph<StudentPaymentsOrder> paragraph : order.getParagraphList())
        {
            for (IAbstractExtract<Student, StudentPaymentParagraph> extract : paragraph.getExtractList())
            {
                StudentPaymentAppointmentExtract extr = (StudentPaymentAppointmentExtract)extract;
                TripletKey<Course, Date, Date> key = new TripletKey<>(extract.getEntity().getCourse(), extr.getPaymentStartDate(), extr.getPaymentStopDate());

                VirtualParagraph<StudentPaymentAppointmentExtract> virtParagraph = paragraphsMap.get(key);
                if(null == virtParagraph) virtParagraph = new VirtualParagraph<>(new ArrayList<>());
                virtParagraph.getExtractsList().add(extr);
                paragraphsMap.put(key, virtParagraph);
            }
        }

        paragraphsList.addAll(paragraphsMap.values());
        Collections.sort((List)paragraphsList, REPORT_PARAGRAPHS_COMPARATOR);

        return paragraphsList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VirtualParagraph> prepareOrderParagraphsData(StudentPaymentsOrder order)
    {
        List<VirtualParagraph> paragraphsList = new ArrayList<>();
        Map<FourKey<Course, Group, Date, Date>, VirtualParagraph<StudentPaymentAppointmentExtract>> paragraphsMap = new HashMap<>();


        for (IAbstractParagraph<StudentPaymentsOrder> paragraph : order.getParagraphList())
        {
            for (IAbstractExtract<Student, StudentPaymentParagraph> extract : paragraph.getExtractList())
            {
                StudentPaymentAppointmentExtract extr = (StudentPaymentAppointmentExtract)extract;
                FourKey<Course, Group, Date, Date> key = new FourKey<>(extract.getEntity().getCourse(), extract.getEntity().getGroup(), extr.getPaymentStartDate(), extr.getPaymentStopDate());

                VirtualParagraph<StudentPaymentAppointmentExtract> virtParagraph = paragraphsMap.get(key);
                if(null == virtParagraph) virtParagraph = new VirtualParagraph<>(new ArrayList<>());
                virtParagraph.getExtractsList().add(extr);
                paragraphsMap.put(key, virtParagraph);
            }
        }

        paragraphsList.addAll(paragraphsMap.values());
        Collections.sort((List)paragraphsList, PARAGRAPHS_COMPARATOR);

        for(VirtualParagraph<StudentPaymentAppointmentExtract> paragraph : paragraphsList)
            Collections.sort(paragraph.getExtractsList(), EXTRACT_STUDENT_FIO_COMPARATOR);

        return paragraphsList;
    }
}