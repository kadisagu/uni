package ru.tandemservice.unisp.component.settings.UsedStudentPaymentOrderTypesSettings;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;

import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 31.01.11
 * Time: 11:01
 * To change this template use File | Settings | File Templates.
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(StudentPaymentExtractType.ENTITY_CLASS, "r");
        new OrderDescriptionRegistry("r").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void updateHasBasic(long extractTypeId)
    {
    StudentPaymentExtractType hasBasic = get(StudentPaymentExtractType.class, extractTypeId);
        if (hasBasic != null)
        {
            hasBasic.setHasBasic(!hasBasic.isHasBasic());
            update(hasBasic);
        }
    }
}
