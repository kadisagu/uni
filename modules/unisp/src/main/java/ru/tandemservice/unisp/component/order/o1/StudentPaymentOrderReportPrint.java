/**
 * $Id$
 */
package ru.tandemservice.unisp.component.order.o1;

import java.util.Collections;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.unisp.UnispUtils;
import ru.tandemservice.unisp.component.order.IOrderReportPrint;
import ru.tandemservice.unisp.component.order.VirtualParagraph;
import ru.tandemservice.unisp.entity.orders.StudentPaymentAppointmentExtract;

import java.util.*;

/**
 * @author dseleznev
 * Created on: 12.01.2010
 */
public class StudentPaymentOrderReportPrint implements IOrderReportPrint
{
    private static Comparator<Group> GROUPS_COMPARATOR = new Comparator<Group>()
    {
        @Override
        public int compare(Group grp1, Group grp2)
        {
            if(null == grp1 && null != grp2) return 1;
            if(null != grp1 && null == grp2) return -1;
            if(null == grp1 && null == grp2) return 0;
            return grp1.getTitle().compareTo(grp2.getTitle());
        }
    };

    @Override
    public RtfInjectModifier createReportInjectModifier(VirtualParagraph paragraph, int repNumber)
    {
        StudentPaymentAppointmentExtract extract = (StudentPaymentAppointmentExtract) paragraph.getFirstExtract();
        RtfInjectModifier injectModifier = new RtfInjectModifier()
        .put("reportNumber", String.valueOf(repNumber))
        .put("course", extract.getEntity().getCourse().getTitle())
        .put("courseStr_G", NumberConvertingUtil.getSpelledNumeric(extract.getEntity().getCourse().getIntValue(), false, NumberConvertingUtil.GENETIVE_CASE))
        .put("beginDate", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getPaymentStartDate()))
        .put("endDate", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(extract.getPaymentStopDate()));
        return injectModifier;
    }

    @Override
    @SuppressWarnings("unchecked")
    public RtfTableModifier createReportTableModifier(VirtualParagraph paragraph)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();

        int sumCnt = 0;
        double sumPayments = 0d;
        List<Group> groupsList = new ArrayList<Group>();
        Map<Group, Double> groupPaymentsMap = new HashMap<Group, Double>();
        Map<Group, Integer> groupExtractsCntMap = new HashMap<Group, Integer>();
        for(StudentPaymentAppointmentExtract extract : (List<StudentPaymentAppointmentExtract>) paragraph.getExtractsList())
        {
            Group grp = extract.getEntity().getGroup();
            if(!groupsList.contains(grp)) groupsList.add(grp);

            Double groupPayments = groupPaymentsMap.get(grp);
            Integer groupExtractsCnt = groupExtractsCntMap.get(grp);

            if(null == groupExtractsCnt) groupExtractsCnt = 0;
            if(null == groupPayments) groupPayments = 0d;

            groupPayments += extract.getPaymentCostAsDouble();
            sumPayments += extract.getPaymentCostAsDouble();
            groupExtractsCnt++;
            sumCnt++;

            groupExtractsCntMap.put(grp, groupExtractsCnt);
            groupPaymentsMap.put(grp, groupPayments);
        }

        Collections.sort(groupsList, GROUPS_COMPARATOR);

        int cnt = 1;
        DoubleFormatter formatter = new DoubleFormatter(2, true, true);
        List<String[]> reportDataLines = new ArrayList<String[]>();
        for(Group grp : groupsList)
            reportDataLines.add(new String[] {String.valueOf(cnt++), null != grp ? grp.getTitle() : "без группы", String.valueOf(groupExtractsCntMap.get(grp)), formatter.format(groupPaymentsMap.get(grp)), NumberConvertingUtil.writeSum(UnispUtils.getUnwrappedDoubleValue(groupPaymentsMap.get(grp)), 3)});

        tableModifier.put("T", reportDataLines.toArray(new String[][]{}));
        tableModifier.put("V", new String[][] {{"", "Всего:", String.valueOf(sumCnt), formatter.format(sumPayments), NumberConvertingUtil.writeSum(UnispUtils.getUnwrappedDoubleValue(sumPayments), 3)}});
        return tableModifier;
    }
}