package ru.tandemservice.unisp.entity.catalog;

import java.util.List;

import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.unisp.dao.UnispDaoFacade;
import ru.tandemservice.unisp.entity.catalog.gen.StudentPaymentExtractTypeGen;

/**
 * Тип документа о назначении выплат
 */
public class StudentPaymentExtractType extends StudentPaymentExtractTypeGen implements IHierarchyItem
{
    public static final String P_PAYMENTS = "payments";
    public static final String P_REASONS = "reasons";

    @SuppressWarnings("unchecked")
    public List<StudentPayment> getPayments()
    {
        return UnispDaoFacade.getUnispDAO().getPaymentToExtractTypePaymentsList(this);
    }
    
    @SuppressWarnings("unchecked")
    public List<StudentPaymentExtractReason> getReasons()
    {
        return UnispDaoFacade.getUnispDAO().getReasonToExtractTypePaymentsList(this);
    }

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return (IHierarchyItem)getParent();
    }
}