package ru.tandemservice.unisp.component.settings.PrintNameStudentPayment;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * 
 * @author nkokorina
 * Created on: 18.02.2010
 */

public interface IDAO extends IUniDao<Model>
{
    @Override
    void prepare(Model model);

}
