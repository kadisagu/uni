package ru.tandemservice.unisp.entity.settings.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unisp.entity.settings.StudentPaymentProtocolVisa;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Виза для протокола заседания стипендиальной комиссии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentProtocolVisaGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.settings.StudentPaymentProtocolVisa";
    public static final String ENTITY_NAME = "studentPaymentProtocolVisa";
    public static final int VERSION_HASH = -1977298682;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_EMPLOYEE_POST = "employeePost";

    private OrgUnit _orgUnit;     // Подразделение
    private EmployeePost _employeePost;     // Сотрудник

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentProtocolVisaGen)
        {
            setOrgUnit(((StudentPaymentProtocolVisa)another).getOrgUnit());
            setEmployeePost(((StudentPaymentProtocolVisa)another).getEmployeePost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentProtocolVisaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentProtocolVisa.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentProtocolVisa();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "employeePost":
                    return obj.getEmployeePost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "employeePost":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "employeePost":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "employeePost":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentProtocolVisa> _dslPath = new Path<StudentPaymentProtocolVisa>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentProtocolVisa");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentProtocolVisa#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentProtocolVisa#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    public static class Path<E extends StudentPaymentProtocolVisa> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private EmployeePost.Path<EmployeePost> _employeePost;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentProtocolVisa#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.settings.StudentPaymentProtocolVisa#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

        public Class getEntityClass()
        {
            return StudentPaymentProtocolVisa.class;
        }

        public String getEntityName()
        {
            return "studentPaymentProtocolVisa";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
