package ru.tandemservice.unisp.entity.catalog;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;

public interface IPrintablePayment extends ITitled, IEntity
{
    public static final String P_TITLE = "title";
    public static final String P_NOMINATIVE_CASE_TITLE = "nominativeCaseTitle";
    public static final String P_GENITIVE_CASE_TITLE = "genitiveCaseTitle";
    public static final String P_DATIVE_CASE_TITLE = "dativeCaseTitle";
    public static final String P_ACCUSATIVE_CASE_TITLE = "accusativeCaseTitle";
    public static final String P_PREPOSITIONAL_CASE_TITLE = "prepositionalCaseTitle";

    /**
     * @return Именительный падеж.
     */
    public String getNominativeCaseTitle();

    /**
     * @return Родительный падеж.
     */
    public String getGenitiveCaseTitle();

    /**
     * @return Дательный падеж.
     */
    public String getDativeCaseTitle();

    /**
     * @return Винительный падеж.
     */
    public String getAccusativeCaseTitle();

    /**
     * @return Предложный падеж.
     */
    public String getPrepositionalCaseTitle();

}
