package ru.tandemservice.unisp.entity.orders;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.sec.ISecLocalEntityOwner;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.ui.formatters.MoneyFormatter;
import ru.tandemservice.uni.ui.formatters.RuMoneyFormatter;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unisp.UnispUtils;
import ru.tandemservice.unisp.dao.IUnispExtractDAO;
import ru.tandemservice.unisp.dao.UnispDaoFacade;
import ru.tandemservice.unisp.entity.orders.gen.StudentPaymentsOrderGen;

/**
 * Приказ о назначении выплат студентам
 */
public class StudentPaymentsOrder extends StudentPaymentsOrderGen implements IAbstractOrder, ISecLocalEntityOwner
{
    public static final String P_PLANNED_FUND_MONEY_FORMATTED = "plannedFundMoneyFormatted";
    public static final String P_FACT_FUND_MONEY_FORMATTED = "factFundMoneyFormatted";
    public static final String P_FACT_MONTH_FUND_MONEY_FORMATTED = "factMonthFundMoneyFormatted";
    public static final String P_VALUE_AS_DOUBLE = "valueAsDouble";
    public static final String P_CANT_BE_EDITED = "cantBeEdited";
    public static final String P_FACT_FUND = "factFund";
    public static final String P_FACT_MONTH_FUND = "factMonthFund";
    public static final String P_COUNT_EXTRACT = "countExtract";     // view-property
    public static final String P_COUNT_PARAGRAPH = "countParagraph"; // view-property
    public static final String P_DELETABLE_MESSAGE_TITLE = "deletableMessageTitle";

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return Collections.<IEntity>singleton(getOrgUnit());
    }

    public String getEditableTitle()
    {
        return (null != getType() ? "«" + getType().getTitle() + "»" : "") + (getNumber() == null ? "" : " №" + getNumber()) + (getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
    }

    @Override
    public String getTitle()
    {
        if (getType() == null) {
            return this.getClass().getSimpleName();
        }
        return "Приказ «" + getType().getTitle() + "»" + (getNumber() == null ? "" : " №" + getNumber()) + (getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
    }

    public String getDeletableMessageTitle()
    {
        return "приказ «" + getType().getTitle() + "»" + (getNumber() == null ? "" : " №" + getNumber()) + (getCommitDate() == null ? "" : " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getCommitDate()));
    }

    public double getPlannedFundAsDouble()
    {
        return UnispUtils.getWrappedLongValue(getPlannedFund());
    }

    public void setPlannedFundAsDouble(final double value)
    {
        setPlannedFund(UnispUtils.getUnwrappedDoubleValue(value));
    }

    public String getPlannedFundMoneyFormatted()
    {
        return MoneyFormatter.ruMoneyFormatter(3).format(getPlannedFund());
    }

    public boolean isCantBeEdited()
    {
        return !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getState().getCode());
    }

    public int getCountExtract()
    {
        return UnispDaoFacade.getUnispDAO().getExtractsCount(this);
    }

    public double getFactFund()
    {
        IUnispExtractDAO dao = UnispDaoFacade.getUnispExtractDAO();
        return dao.getTotalOrderFactFund(dao.getTotalStudentExtractListMap(this, null));
    }

    public double getFactMonthFund()
    {
        IUnispExtractDAO dao = UnispDaoFacade.getUnispExtractDAO();
        return dao.getTotalOrderMonthFactFund(dao.getTotalStudentExtractListMap(this, null));
    }

    public String getFactFundMoneyFormatted()
    {
        return RuMoneyFormatter.INSTANCE.format(getFactFund());
    }

    public String getFactMonthFundMoneyFormatted()
    {
        return RuMoneyFormatter.INSTANCE.format(getFactMonthFund());
    }

    @Override
    public int getParagraphCount()
    {
        return UniDaoFacade.getCoreDao().getCount(StudentPaymentParagraph.class, IAbstractParagraph.L_ORDER, this);
    }

    @Override
    public List<? extends IAbstractParagraph> getParagraphList()
    {
        return UniDaoFacade.getCoreDao().getList(StudentPaymentParagraph.class, IAbstractParagraph.L_ORDER, this, IAbstractParagraph.P_NUMBER);
    }

    @Override
    public void setState(final ICatalogItem state)
    {
        setState((OrderStates)state);
    }
}