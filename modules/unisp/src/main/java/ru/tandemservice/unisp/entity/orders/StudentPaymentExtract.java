package ru.tandemservice.unisp.entity.orders;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.catalog.entity.ICatalogItem;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import ru.tandemservice.unisp.entity.orders.gen.StudentPaymentExtractGen;

/**
 * Абстрактная выписка из списочного приказа по студенту
 */
public abstract class StudentPaymentExtract extends StudentPaymentExtractGen implements IAbstractExtract<Student, StudentPaymentParagraph>
{
    public static final Object STUDENT_FIO_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_PERSON, Person.P_FULLFIO};
    public static final Object STUDENT_PASSPORT_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_PERSON, Person.P_FULL_IDCARD_NUMBER};
    public static final Object STUDENT_STATUS_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_STATUS, StudentStatus.P_TITLE};
    public static final Object STUDENT_COURSE_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_COURSE, Course.P_TITLE};
    public static final Object STUDENT_GROUP_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_GROUP, Group.P_TITLE};
    public static final Object FORMATIVE_ORGUNIT_SHORT_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_SHORT_TITLE};
    public static final Object TERRITORIAL_ORGUNIT_SHORT_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_SHORT_TITLE};
    public static final Object EDU_LEVEL_SHORT_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.P_SHORT_TITLE};
    public static final Object DEVELOP_FORM_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_DEVELOP_FORM, DevelopForm.P_TITLE};
    public static final Object STUDENT_COMPENSATION_TYPE_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_COMPENSATION_TYPE, CompensationType.P_TITLE};
    public static final Object STUDENT_CATEGORY_KEY = new String[]{IAbstractExtract.L_ENTITY, Student.L_STUDENT_CATEGORY, StudentCategory.P_TITLE};
    
    public static final String P_NO_DELETE = "noDelete";   // нельзя удалять
    

    @Override
    public String getTitle()
    {
        return "";
    }

    @Override
    public void setState(final ICatalogItem state)
    {
        setState((ExtractStates)state);
    }

    @Override
    public void setType(final ICatalogItem type)
    {
        setType((StudentPaymentExtractType)type);
    }

    public void setup(final Student student, final GrammaCase rusCase)
    {
        this.setEntity(student);

        if (rusCase != null)
        {
            final IdentityCard identityCard = student.getPerson().getIdentityCard();
            final boolean isMaleSex = identityCard.getSex().isMale();

            final StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex));
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
            {
                str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));
            }
            setStudentTitle(str.toString());
        }
        else
        {
            setStudentTitle(student.getPerson().getFullFio());
        }

        this.setStudentTitleStr(student.getPerson().getFullFio());

        this.setStudentCategoryStr(student.getStudentCategory().getTitle());
        this.setStudentStatusStr(student.getStatus().getTitle());
        this.setPersonalNumberStr(student.getPersonalNumber());
        this.setCourseStr(String.valueOf(student.getCourse().getIntValue()));
        this.setGroupStr(null == student.getGroup() ? "" : student.getGroup().getTitle());
        this.setCompensationTypeStr(student.getCompensationType().getTitle());

        final EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        this.setFormativeOrgUnitStr(educationOrgUnit.getFormativeOrgUnit().getFullTitle());
        this.setTerritorialOrgUnitStr(educationOrgUnit.getTerritorialOrgUnit().getTerritorialFullTitle());
        this.setEducationLevelHighSchoolStr(educationOrgUnit.getEducationLevelHighSchool().getDisplayableShortTitle());
        this.setDevelopFormStr(educationOrgUnit.getDevelopForm().getTitle());
        this.setDevelopConditionStr(educationOrgUnit.getDevelopCondition().getTitle());
        this.setDevelopTechStr(educationOrgUnit.getDevelopTech().getTitle());
        this.setDevelopPeriodStr(educationOrgUnit.getDevelopPeriod().getTitle());
    }
    
    public boolean isNoDelete()
    {
        return !UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(getParagraph().getOrder().getState().getCode());
    }
}