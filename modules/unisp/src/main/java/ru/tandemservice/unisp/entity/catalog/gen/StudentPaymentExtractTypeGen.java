package ru.tandemservice.unisp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип документа о назначении выплат
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentExtractTypeGen extends EntityBase
 implements INaturalIdentifiable<StudentPaymentExtractTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType";
    public static final String ENTITY_NAME = "studentPaymentExtractType";
    public static final int VERSION_HASH = -1856381266;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_INDEX = "index";
    public static final String P_HAS_BASIC = "hasBasic";
    public static final String P_BASIC = "basic";
    public static final String L_PARENT = "parent";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private Integer _index;     // Индекс
    private boolean _hasBasic;     // Имеет основание
    private String _basic;     // Основание
    private StudentPaymentExtractType _parent;     // Тип документа о назначении выплат
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Индекс.
     */
    public Integer getIndex()
    {
        return _index;
    }

    /**
     * @param index Индекс.
     */
    public void setIndex(Integer index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Имеет основание. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasBasic()
    {
        return _hasBasic;
    }

    /**
     * @param hasBasic Имеет основание. Свойство не может быть null.
     */
    public void setHasBasic(boolean hasBasic)
    {
        dirty(_hasBasic, hasBasic);
        _hasBasic = hasBasic;
    }

    /**
     * @return Основание.
     */
    public String getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание.
     */
    public void setBasic(String basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Тип документа о назначении выплат.
     */
    public StudentPaymentExtractType getParent()
    {
        return _parent;
    }

    /**
     * @param parent Тип документа о назначении выплат.
     */
    public void setParent(StudentPaymentExtractType parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentExtractTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StudentPaymentExtractType)another).getCode());
            }
            setShortTitle(((StudentPaymentExtractType)another).getShortTitle());
            setIndex(((StudentPaymentExtractType)another).getIndex());
            setHasBasic(((StudentPaymentExtractType)another).isHasBasic());
            setBasic(((StudentPaymentExtractType)another).getBasic());
            setParent(((StudentPaymentExtractType)another).getParent());
            setTitle(((StudentPaymentExtractType)another).getTitle());
        }
    }

    public INaturalId<StudentPaymentExtractTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StudentPaymentExtractTypeGen>
    {
        private static final String PROXY_NAME = "StudentPaymentExtractTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentPaymentExtractTypeGen.NaturalId) ) return false;

            StudentPaymentExtractTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentExtractTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPaymentExtractType.class;
        }

        public T newInstance()
        {
            return (T) new StudentPaymentExtractType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "index":
                    return obj.getIndex();
                case "hasBasic":
                    return obj.isHasBasic();
                case "basic":
                    return obj.getBasic();
                case "parent":
                    return obj.getParent();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "hasBasic":
                    obj.setHasBasic((Boolean) value);
                    return;
                case "basic":
                    obj.setBasic((String) value);
                    return;
                case "parent":
                    obj.setParent((StudentPaymentExtractType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "index":
                        return true;
                case "hasBasic":
                        return true;
                case "basic":
                        return true;
                case "parent":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "index":
                    return true;
                case "hasBasic":
                    return true;
                case "basic":
                    return true;
                case "parent":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "index":
                    return Integer.class;
                case "hasBasic":
                    return Boolean.class;
                case "basic":
                    return String.class;
                case "parent":
                    return StudentPaymentExtractType.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPaymentExtractType> _dslPath = new Path<StudentPaymentExtractType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPaymentExtractType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Индекс.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Имеет основание. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#isHasBasic()
     */
    public static PropertyPath<Boolean> hasBasic()
    {
        return _dslPath.hasBasic();
    }

    /**
     * @return Основание.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getBasic()
     */
    public static PropertyPath<String> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Тип документа о назначении выплат.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getParent()
     */
    public static StudentPaymentExtractType.Path<StudentPaymentExtractType> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StudentPaymentExtractType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<Integer> _index;
        private PropertyPath<Boolean> _hasBasic;
        private PropertyPath<String> _basic;
        private StudentPaymentExtractType.Path<StudentPaymentExtractType> _parent;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StudentPaymentExtractTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(StudentPaymentExtractTypeGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Индекс.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(StudentPaymentExtractTypeGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Имеет основание. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#isHasBasic()
     */
        public PropertyPath<Boolean> hasBasic()
        {
            if(_hasBasic == null )
                _hasBasic = new PropertyPath<Boolean>(StudentPaymentExtractTypeGen.P_HAS_BASIC, this);
            return _hasBasic;
        }

    /**
     * @return Основание.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getBasic()
     */
        public PropertyPath<String> basic()
        {
            if(_basic == null )
                _basic = new PropertyPath<String>(StudentPaymentExtractTypeGen.P_BASIC, this);
            return _basic;
        }

    /**
     * @return Тип документа о назначении выплат.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getParent()
     */
        public StudentPaymentExtractType.Path<StudentPaymentExtractType> parent()
        {
            if(_parent == null )
                _parent = new StudentPaymentExtractType.Path<StudentPaymentExtractType>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StudentPaymentExtractTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StudentPaymentExtractType.class;
        }

        public String getEntityName()
        {
            return "studentPaymentExtractType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
