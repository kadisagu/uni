package ru.tandemservice.unisp.component.settings.ReasonsCommentSettingsEdit;

import org.tandemframework.core.component.Input;

import ru.tandemservice.unisp.entity.catalog.StudentPaymentExtractReason;

/**
 * Created by IntelliJ IDEA.
 * User: Tandem
 * Date: 28.01.11
 * Time: 15:44
 * To change this template use File | Settings | File Templates.
 */
@Input(keys = {"studentPaymentExtractReasonId", "studentPaymentExtractReasonId"})
public class Model
{
    private Long _studentPaymentExtractReasonId;
    private StudentPaymentExtractReason _reason;

    public Long getStudentPaymentExtractReasonId()
    {
        return _studentPaymentExtractReasonId;
    }

    public void setStudentPaymentExtractReasonId(Long studentPaymentExtractReasonId)
    {
        this._studentPaymentExtractReasonId = studentPaymentExtractReasonId;
    }

    public StudentPaymentExtractReason getReason()
    {
        return _reason;
    }

    public void setReason(StudentPaymentExtractReason reason)
    {
        this._reason = reason;
    }


}
