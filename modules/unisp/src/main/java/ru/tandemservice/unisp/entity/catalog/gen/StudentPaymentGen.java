package ru.tandemservice.unisp.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unisp.entity.catalog.PaymentKind;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;
import ru.tandemservice.unisp.entity.catalog.StudentPaymentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выплаты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentPaymentGen extends EntityBase
 implements INaturalIdentifiable<StudentPaymentGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisp.entity.catalog.StudentPayment";
    public static final String ENTITY_NAME = "studentPayment";
    public static final int VERSION_HASH = -201207804;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String L_TYPE = "type";
    public static final String L_KIND = "kind";
    public static final String P_VALUE = "value";
    public static final String P_FIXED_VALUE = "fixedValue";
    public static final String P_NOMINATIVE_CASE_TITLE = "nominativeCaseTitle";
    public static final String P_GENITIVE_CASE_TITLE = "genitiveCaseTitle";
    public static final String P_DATIVE_CASE_TITLE = "dativeCaseTitle";
    public static final String P_ACCUSATIVE_CASE_TITLE = "accusativeCaseTitle";
    public static final String P_PREPOSITIONAL_CASE_TITLE = "prepositionalCaseTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private StudentPaymentType _type;     // Тип выплаты
    private PaymentKind _kind;     // Характер выплаты
    private long _value;     // Сумма
    private boolean _fixedValue;     // Фиксированное значение
    private String _nominativeCaseTitle;     // Именительный падеж
    private String _genitiveCaseTitle;     // Родительный падеж
    private String _dativeCaseTitle;     // Дательный падеж
    private String _accusativeCaseTitle;     // Винительный падеж
    private String _prepositionalCaseTitle;     // Предложный падеж
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название.
     */
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Тип выплаты. Свойство не может быть null.
     */
    @NotNull
    public StudentPaymentType getType()
    {
        return _type;
    }

    /**
     * @param type Тип выплаты. Свойство не может быть null.
     */
    public void setType(StudentPaymentType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Характер выплаты. Свойство не может быть null.
     */
    @NotNull
    public PaymentKind getKind()
    {
        return _kind;
    }

    /**
     * @param kind Характер выплаты. Свойство не может быть null.
     */
    public void setKind(PaymentKind kind)
    {
        dirty(_kind, kind);
        _kind = kind;
    }

    /**
     * @return Сумма. Свойство не может быть null.
     */
    @NotNull
    public long getValue()
    {
        return _value;
    }

    /**
     * @param value Сумма. Свойство не может быть null.
     */
    public void setValue(long value)
    {
        dirty(_value, value);
        _value = value;
    }

    /**
     * @return Фиксированное значение. Свойство не может быть null.
     */
    @NotNull
    public boolean isFixedValue()
    {
        return _fixedValue;
    }

    /**
     * @param fixedValue Фиксированное значение. Свойство не может быть null.
     */
    public void setFixedValue(boolean fixedValue)
    {
        dirty(_fixedValue, fixedValue);
        _fixedValue = fixedValue;
    }

    /**
     * @return Именительный падеж.
     */
    @Length(max=255)
    public String getNominativeCaseTitle()
    {
        return _nominativeCaseTitle;
    }

    /**
     * @param nominativeCaseTitle Именительный падеж.
     */
    public void setNominativeCaseTitle(String nominativeCaseTitle)
    {
        dirty(_nominativeCaseTitle, nominativeCaseTitle);
        _nominativeCaseTitle = nominativeCaseTitle;
    }

    /**
     * @return Родительный падеж.
     */
    @Length(max=255)
    public String getGenitiveCaseTitle()
    {
        return _genitiveCaseTitle;
    }

    /**
     * @param genitiveCaseTitle Родительный падеж.
     */
    public void setGenitiveCaseTitle(String genitiveCaseTitle)
    {
        dirty(_genitiveCaseTitle, genitiveCaseTitle);
        _genitiveCaseTitle = genitiveCaseTitle;
    }

    /**
     * @return Дательный падеж.
     */
    @Length(max=255)
    public String getDativeCaseTitle()
    {
        return _dativeCaseTitle;
    }

    /**
     * @param dativeCaseTitle Дательный падеж.
     */
    public void setDativeCaseTitle(String dativeCaseTitle)
    {
        dirty(_dativeCaseTitle, dativeCaseTitle);
        _dativeCaseTitle = dativeCaseTitle;
    }

    /**
     * @return Винительный падеж.
     */
    @Length(max=255)
    public String getAccusativeCaseTitle()
    {
        return _accusativeCaseTitle;
    }

    /**
     * @param accusativeCaseTitle Винительный падеж.
     */
    public void setAccusativeCaseTitle(String accusativeCaseTitle)
    {
        dirty(_accusativeCaseTitle, accusativeCaseTitle);
        _accusativeCaseTitle = accusativeCaseTitle;
    }

    /**
     * @return Предложный падеж.
     */
    @Length(max=255)
    public String getPrepositionalCaseTitle()
    {
        return _prepositionalCaseTitle;
    }

    /**
     * @param prepositionalCaseTitle Предложный падеж.
     */
    public void setPrepositionalCaseTitle(String prepositionalCaseTitle)
    {
        dirty(_prepositionalCaseTitle, prepositionalCaseTitle);
        _prepositionalCaseTitle = prepositionalCaseTitle;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentPaymentGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StudentPayment)another).getCode());
            }
            setShortTitle(((StudentPayment)another).getShortTitle());
            setType(((StudentPayment)another).getType());
            setKind(((StudentPayment)another).getKind());
            setValue(((StudentPayment)another).getValue());
            setFixedValue(((StudentPayment)another).isFixedValue());
            setNominativeCaseTitle(((StudentPayment)another).getNominativeCaseTitle());
            setGenitiveCaseTitle(((StudentPayment)another).getGenitiveCaseTitle());
            setDativeCaseTitle(((StudentPayment)another).getDativeCaseTitle());
            setAccusativeCaseTitle(((StudentPayment)another).getAccusativeCaseTitle());
            setPrepositionalCaseTitle(((StudentPayment)another).getPrepositionalCaseTitle());
            setTitle(((StudentPayment)another).getTitle());
        }
    }

    public INaturalId<StudentPaymentGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StudentPaymentGen>
    {
        private static final String PROXY_NAME = "StudentPaymentNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentPaymentGen.NaturalId) ) return false;

            StudentPaymentGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentPaymentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentPayment.class;
        }

        public T newInstance()
        {
            return (T) new StudentPayment();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "type":
                    return obj.getType();
                case "kind":
                    return obj.getKind();
                case "value":
                    return obj.getValue();
                case "fixedValue":
                    return obj.isFixedValue();
                case "nominativeCaseTitle":
                    return obj.getNominativeCaseTitle();
                case "genitiveCaseTitle":
                    return obj.getGenitiveCaseTitle();
                case "dativeCaseTitle":
                    return obj.getDativeCaseTitle();
                case "accusativeCaseTitle":
                    return obj.getAccusativeCaseTitle();
                case "prepositionalCaseTitle":
                    return obj.getPrepositionalCaseTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "type":
                    obj.setType((StudentPaymentType) value);
                    return;
                case "kind":
                    obj.setKind((PaymentKind) value);
                    return;
                case "value":
                    obj.setValue((Long) value);
                    return;
                case "fixedValue":
                    obj.setFixedValue((Boolean) value);
                    return;
                case "nominativeCaseTitle":
                    obj.setNominativeCaseTitle((String) value);
                    return;
                case "genitiveCaseTitle":
                    obj.setGenitiveCaseTitle((String) value);
                    return;
                case "dativeCaseTitle":
                    obj.setDativeCaseTitle((String) value);
                    return;
                case "accusativeCaseTitle":
                    obj.setAccusativeCaseTitle((String) value);
                    return;
                case "prepositionalCaseTitle":
                    obj.setPrepositionalCaseTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "type":
                        return true;
                case "kind":
                        return true;
                case "value":
                        return true;
                case "fixedValue":
                        return true;
                case "nominativeCaseTitle":
                        return true;
                case "genitiveCaseTitle":
                        return true;
                case "dativeCaseTitle":
                        return true;
                case "accusativeCaseTitle":
                        return true;
                case "prepositionalCaseTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "type":
                    return true;
                case "kind":
                    return true;
                case "value":
                    return true;
                case "fixedValue":
                    return true;
                case "nominativeCaseTitle":
                    return true;
                case "genitiveCaseTitle":
                    return true;
                case "dativeCaseTitle":
                    return true;
                case "accusativeCaseTitle":
                    return true;
                case "prepositionalCaseTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "type":
                    return StudentPaymentType.class;
                case "kind":
                    return PaymentKind.class;
                case "value":
                    return Long.class;
                case "fixedValue":
                    return Boolean.class;
                case "nominativeCaseTitle":
                    return String.class;
                case "genitiveCaseTitle":
                    return String.class;
                case "dativeCaseTitle":
                    return String.class;
                case "accusativeCaseTitle":
                    return String.class;
                case "prepositionalCaseTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentPayment> _dslPath = new Path<StudentPayment>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentPayment");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Тип выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getType()
     */
    public static StudentPaymentType.Path<StudentPaymentType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Характер выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getKind()
     */
    public static PaymentKind.Path<PaymentKind> kind()
    {
        return _dslPath.kind();
    }

    /**
     * @return Сумма. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getValue()
     */
    public static PropertyPath<Long> value()
    {
        return _dslPath.value();
    }

    /**
     * @return Фиксированное значение. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#isFixedValue()
     */
    public static PropertyPath<Boolean> fixedValue()
    {
        return _dslPath.fixedValue();
    }

    /**
     * @return Именительный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getNominativeCaseTitle()
     */
    public static PropertyPath<String> nominativeCaseTitle()
    {
        return _dslPath.nominativeCaseTitle();
    }

    /**
     * @return Родительный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getGenitiveCaseTitle()
     */
    public static PropertyPath<String> genitiveCaseTitle()
    {
        return _dslPath.genitiveCaseTitle();
    }

    /**
     * @return Дательный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getDativeCaseTitle()
     */
    public static PropertyPath<String> dativeCaseTitle()
    {
        return _dslPath.dativeCaseTitle();
    }

    /**
     * @return Винительный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getAccusativeCaseTitle()
     */
    public static PropertyPath<String> accusativeCaseTitle()
    {
        return _dslPath.accusativeCaseTitle();
    }

    /**
     * @return Предложный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getPrepositionalCaseTitle()
     */
    public static PropertyPath<String> prepositionalCaseTitle()
    {
        return _dslPath.prepositionalCaseTitle();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StudentPayment> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private StudentPaymentType.Path<StudentPaymentType> _type;
        private PaymentKind.Path<PaymentKind> _kind;
        private PropertyPath<Long> _value;
        private PropertyPath<Boolean> _fixedValue;
        private PropertyPath<String> _nominativeCaseTitle;
        private PropertyPath<String> _genitiveCaseTitle;
        private PropertyPath<String> _dativeCaseTitle;
        private PropertyPath<String> _accusativeCaseTitle;
        private PropertyPath<String> _prepositionalCaseTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StudentPaymentGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(StudentPaymentGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Тип выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getType()
     */
        public StudentPaymentType.Path<StudentPaymentType> type()
        {
            if(_type == null )
                _type = new StudentPaymentType.Path<StudentPaymentType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Характер выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getKind()
     */
        public PaymentKind.Path<PaymentKind> kind()
        {
            if(_kind == null )
                _kind = new PaymentKind.Path<PaymentKind>(L_KIND, this);
            return _kind;
        }

    /**
     * @return Сумма. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getValue()
     */
        public PropertyPath<Long> value()
        {
            if(_value == null )
                _value = new PropertyPath<Long>(StudentPaymentGen.P_VALUE, this);
            return _value;
        }

    /**
     * @return Фиксированное значение. Свойство не может быть null.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#isFixedValue()
     */
        public PropertyPath<Boolean> fixedValue()
        {
            if(_fixedValue == null )
                _fixedValue = new PropertyPath<Boolean>(StudentPaymentGen.P_FIXED_VALUE, this);
            return _fixedValue;
        }

    /**
     * @return Именительный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getNominativeCaseTitle()
     */
        public PropertyPath<String> nominativeCaseTitle()
        {
            if(_nominativeCaseTitle == null )
                _nominativeCaseTitle = new PropertyPath<String>(StudentPaymentGen.P_NOMINATIVE_CASE_TITLE, this);
            return _nominativeCaseTitle;
        }

    /**
     * @return Родительный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getGenitiveCaseTitle()
     */
        public PropertyPath<String> genitiveCaseTitle()
        {
            if(_genitiveCaseTitle == null )
                _genitiveCaseTitle = new PropertyPath<String>(StudentPaymentGen.P_GENITIVE_CASE_TITLE, this);
            return _genitiveCaseTitle;
        }

    /**
     * @return Дательный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getDativeCaseTitle()
     */
        public PropertyPath<String> dativeCaseTitle()
        {
            if(_dativeCaseTitle == null )
                _dativeCaseTitle = new PropertyPath<String>(StudentPaymentGen.P_DATIVE_CASE_TITLE, this);
            return _dativeCaseTitle;
        }

    /**
     * @return Винительный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getAccusativeCaseTitle()
     */
        public PropertyPath<String> accusativeCaseTitle()
        {
            if(_accusativeCaseTitle == null )
                _accusativeCaseTitle = new PropertyPath<String>(StudentPaymentGen.P_ACCUSATIVE_CASE_TITLE, this);
            return _accusativeCaseTitle;
        }

    /**
     * @return Предложный падеж.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getPrepositionalCaseTitle()
     */
        public PropertyPath<String> prepositionalCaseTitle()
        {
            if(_prepositionalCaseTitle == null )
                _prepositionalCaseTitle = new PropertyPath<String>(StudentPaymentGen.P_PREPOSITIONAL_CASE_TITLE, this);
            return _prepositionalCaseTitle;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.unisp.entity.catalog.StudentPayment#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StudentPaymentGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StudentPayment.class;
        }

        public String getEntityName()
        {
            return "studentPayment";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
