/**
 * $Id$
 */
package ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.logic.allocationTab;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.abstractHandler.AbstractStudentSearchListDSHandler;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.ui.AllocationTab.SpStudentPaymentOrderAllocationTabUI;
import ru.tandemservice.unisp.dao.IUnispExtractDAO;
import ru.tandemservice.unisp.entity.orders.StudentPaymentExtract;
import ru.tandemservice.unisp.entity.orders.StudentPaymentsOrder;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.max;
import static ru.tandemservice.unisp.base.bo.SpStudentPaymentOrder.SpStudentPaymentOrderManager.*;

/**
 * @author Alexander Shaburov
 * @since 14.02.13
 */
public class StudentPaymentOrderAllocationSearchListDSHandler extends AbstractStudentSearchListDSHandler
{
    public static final String TERM_COLUMN = "termColumn";
    public static final String PAYMENT_COLUMN = "paymentColumn";
    public static final String EMPTY_COLUMN = "emptyColumn";

    public static final String STUDENT_EXTRACT_LIST_MAP = "studentExtractListMap";
    public static final String STUDENT_LAST_TERM_NUMBER_MAP = "studentLastTermNumberMap";

    /**
     * Значения фильтров.
     */
    public static abstract class FilterParameters
    {
        /**
         * Академ. задолженность на начало сессии
         */
        public abstract DataWrapper getDebtStatus();

        /**
         * Успеваемость в последнюю сессию
         */
        public abstract DataWrapper getPerformanceStatus();

        /**
         * Оценки в последнюю сессию
         */
        public abstract DataWrapper getLastSessionStatus();

        public final boolean isEmpty()
        {
            return (null == getDebtStatus()) &&
                    (null == getPerformanceStatus()) &&
                    (null == getLastSessionStatus());
        }
    }

    public StudentPaymentOrderAllocationSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, final String alias, DSInput input, ExecutionContext context)
    {
        final StudentPaymentsOrder order = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_ORDER);
        final OrgUnit orgUnit = order.getOrgUnit();
        final List<Group> groupList = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_GROUP);
        final Boolean spHasNoRelation = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_SP_HAS_NO_RELATION);

        {
            // только неархивные студенты
            builder.where(eq(property(alias, Student.P_ARCHIVAL), value(Boolean.FALSE)));

            // формирующее или территориальное
            builder.where(or(
                    eq(property(Student.educationOrgUnit().formativeOrgUnit().fromAlias(alias)), value(orgUnit)),
                    eq(property(Student.educationOrgUnit().territorialOrgUnit().fromAlias(alias)), value(orgUnit))
            ));
        }

        {
            if (null != groupList && !groupList.isEmpty())
            {
                builder.where(in(property(Student.group().fromAlias(alias)), groupList));
            }

            if (Boolean.TRUE.equals(spHasNoRelation))
            {
                builder.where(notExists(
                        new DQLSelectBuilder()
                                .fromEntity(StudentPaymentExtract.class, "spe").column(property("spe.id"))
                                .where(eq(property(StudentPaymentExtract.paragraph().order().fromAlias("spe")), value(order)))
                                .where(eq(property(alias), property(StudentPaymentExtract.entity().fromAlias("spe"))))
                                .buildQuery()
                ));
            }
        }

        final Collection<Long> studentIds = UniBaseDao.ids(builder.createStatement(context.getSession()).<Student>proxyList());
        context.put(STUDENT_EXTRACT_LIST_MAP, IUnispExtractDAO.instance.get().getTotalStudentExtractListMap(order, studentIds));

        sessionMarkFilters(builder, alias, context, studentIds);
    }

    @Override
    protected void wrap(DataWrapper wrapper, ExecutionContext context)
    {
        super.wrap(wrapper, context);

        final Map<Long, List<IUnispExtractDAO.IStudentPaymentDescription>> studentExtractListMap = context.get(STUDENT_EXTRACT_LIST_MAP);
        final Map<Long, Number> studentLastTermNumberMap = context.get(STUDENT_LAST_TERM_NUMBER_MAP);

        final Number lastTermNumber = studentLastTermNumberMap.get(wrapper.getId());
        if (null != lastTermNumber && lastTermNumber.intValue() > 0)
            wrapper.setProperty(TERM_COLUMN, String.valueOf(lastTermNumber.intValue()));
        else
            wrapper.setProperty(TERM_COLUMN, "");

        final List<IUnispExtractDAO.IStudentPaymentDescription> extractList = studentExtractListMap.get(wrapper.getId());
        if (null != extractList)
        {
            final StringBuilder sb = new StringBuilder();
            for (final IUnispExtractDAO.IStudentPaymentDescription e : extractList)
            {
                if (sb.length() > 0)
                {
                    sb.append("\n");
                }
                sb.append(e.getTitle());
            }
            wrapper.setProperty(PAYMENT_COLUMN, sb.toString());
            wrapper.setProperty(EMPTY_COLUMN, extractList.isEmpty());
        } else
        {
            wrapper.setProperty(PAYMENT_COLUMN, "");
            wrapper.setProperty(EMPTY_COLUMN, Boolean.TRUE);
        }
    }

    protected void sessionMarkFilters(DQLSelectBuilder builder, final String alias, ExecutionContext context, final Collection<Long> studentIds)
    {
        final DataWrapper debtStatus = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_DEBT_STATUS);
        final DataWrapper performanceStatus = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_PERFORMANCE_STATUS);
        final DataWrapper lastSessionStatus = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_LAST_SESSION_STATUS);

        context.put(STUDENT_LAST_TERM_NUMBER_MAP, lastTermNumberMap(studentIds, context));

        final FilterParameters filter = new FilterParameters()
        {
            @Override
            public DataWrapper getDebtStatus()
            {
                return debtStatus;
            }

            @Override
            public DataWrapper getPerformanceStatus()
            {
                return performanceStatus;
            }

            @Override
            public DataWrapper getLastSessionStatus()
            {
                return lastSessionStatus;
            }
        };

        // все фильтры простые - берем прямо из запроса
        if (filter.isEmpty())
            return;

        if (studentIds.isEmpty())
            builder.where(nothing());
        else
        {
            // сложный фильтр - предется грузить всех студентов и фильтровать руками
            final Collection<Long> filteredStudentIds = filterSessionMarkStudentIds(studentIds, filter, context);
            builder.where(in(property(alias, Student.id()), filteredStudentIds));
        }
    }

    /**
     * Для каждого студента возвращает номер последненго семестра, в котором у него есть оценки
     *
     * @param studentIds список идентифиаторов студентов
     * @return { student.id -> term.intValue }
     */
    protected Map<Long, MutableInt> lastTermNumberMap(final Collection<Long> studentIds, ExecutionContext context)
    {
        final Map<Long, MutableInt> lastTermNumberMap = SafeMap.get(MutableInt.class);
        for (List<Long> elements : Iterables.partition(studentIds, DQL.MAX_VALUES_ROW_NUMBER))
        {

            final List<Object[]> list = new DQLSelectBuilder().fromEntity(SessionMark.class, "m")
                    .column(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")))
                    .column(property(SessionMark.slot().studentWpeCAction().studentWpe().term().intValue().fromAlias("m")))
                    .where(in(property(SessionMark.slot().studentWpeCAction().studentWpe().student().id().fromAlias("m")), elements))
                    .createStatement(context.getSession()).list();

            for (final Object[] row : list)
            {
                final Long studentId = (Long) row[0];
                final Integer term = (Integer) row[1];

                final MutableInt i = lastTermNumberMap.get(studentId);
                if (i.intValue() < term)
                    i.setValue(term.intValue());
            }
        }

        return lastTermNumberMap;
    }

    /**
     * Фильтрует студентов, согласно фильтрам:
     * 1) Академ. задолженность на начало сессии
     * 2) Экзамены сданы в сессию
     * 3) Успеваемость в последнюю сессию
     * 4) Оценки в последнюю сессию
     * 5) Учитывать мероприятия
     * 6) Виды дисциплин
     *
     * @param studentIds список идентификаторов студентов
     * @param filters    условия фильтрации
     * @return отфильтрованный список идентификаторов студентов
     */
    protected Collection<Long> filterSessionMarkStudentIds(Collection<Long> studentIds, FilterParameters filters, ExecutionContext context)
    {
        if (filters.isEmpty()) return studentIds;

        StudentPaymentsOrder order = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_ORDER);
        List<EppRegistryStructure> structureList = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_REG_STRUCTURE_LIST);
        List<EppWorkPlanRowKind> workPlanRowKindList = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_WORK_PLAN_ROW_KIND_LIST);
        EducationYear year = order.getEducationYear();
        YearDistributionPart part = order.getYearPart();
        if (null == part)
            throw new ApplicationException("В приказе не указана часть учебного года.");

        Long debtStatusId = filters.getDebtStatus() == null ? null : filters.getDebtStatus().getId();
        Long pfStatusId = filters.getPerformanceStatus() == null ? null : filters.getPerformanceStatus().getId();
        Long lastSessionStatusId = filters.getLastSessionStatus() == null ? null : filters.getLastSessionStatus().getId();

        if (null != debtStatusId)
        {
            studentIds = filterDebtStatus(studentIds, debtStatusId, year, part, structureList, workPlanRowKindList, context);
        }
        if (null == pfStatusId && null == lastSessionStatusId) return studentIds;

        List<String> sessionMarkCodes = Lists.newArrayList();
        if (null != pfStatusId)
        {
            if (!pfStatusId.equals(PERFORMANCE_5ALL))
                sessionMarkCodes.add(SessionMarkGradeValueCatalogItemCodes.HOROSHO);
            if (!pfStatusId.equals(PERFORMANCE_4ALL))
                sessionMarkCodes.add(SessionMarkGradeValueCatalogItemCodes.OTLICHNO);
            sessionMarkCodes.add(SessionMarkGradeValueCatalogItemCodes.ZACHTENO);
        }

        Set<Long> resultIds = Sets.newHashSet();
        Map<Long, Map<Long, SessionMarkCatalogItem>> studentMap = getStudentMap(studentIds, year, part, context);
        for (Map.Entry<Long, Map<Long, SessionMarkCatalogItem>> entry : studentMap.entrySet())
        {
            int fourCount = 0;
            boolean isOneMarkEmpty = false;
            boolean isMarkListEmpty = true;
            Long studentId = entry.getKey();

            Map<Long, Boolean> pfMap = new HashMap<Long, Boolean>()
            {{
                put(PERFORMANCE_5ALL, true);
                put(PERFORMANCE_4ALL, true);
                put(PERFORMANCE_4SINGLE_5ALL, true);
                put(PERFORMANCE_4ALL_5ALL_INC, true);
                put(PERFORMANCE_4ALL_5ALL_EXC, true);
            }};

            for (Map.Entry<Long, SessionMarkCatalogItem> entryAction : entry.getValue().entrySet())
            {
                SessionMarkCatalogItem item = entryAction.getValue();
                if (null != item)
                {
                    isMarkListEmpty = false;
                    String markCode = item.getCode();

                    if (null != pfStatusId)
                    {
                        boolean remarkable = item instanceof SessionMarkStateCatalogItem && !((SessionMarkStateCatalogItem) item).isRemarkable();

                        if (!item.isCachedPositiveStatus())
                        {
                            isMarkListEmpty = true;
                            break;
                        } else if (pfStatusId.equals(PERFORMANCE_5ALL))
                        {
                            if (!sessionMarkCodes.contains(markCode) && !remarkable)
                            {
                                pfMap.put(PERFORMANCE_5ALL, false);
                                break;
                            }
                        } else if (pfStatusId.equals(PERFORMANCE_4ALL))
                        {
                            if (!sessionMarkCodes.contains(markCode) && !remarkable)
                            {
                                pfMap.put(PERFORMANCE_4ALL, false);
                                break;
                            }
                        } else if (pfStatusId.equals(PERFORMANCE_4SINGLE_5ALL))
                        {
                            if (!sessionMarkCodes.contains(markCode) && !remarkable)
                            {
                                pfMap.put(PERFORMANCE_4SINGLE_5ALL, false);
                                break;
                            }
                            if (markCode.equals(SessionMarkGradeValueCatalogItemCodes.HOROSHO)) fourCount++;
                        } else if (pfStatusId.equals(PERFORMANCE_4ALL_5ALL_INC))
                        {
                            if (!sessionMarkCodes.contains(markCode) && !remarkable)
                            {
                                pfMap.put(PERFORMANCE_4ALL_5ALL_INC, false);
                                break;
                            }
                        } else if (pfStatusId.equals(PERFORMANCE_4ALL_5ALL_EXC))
                        {
                            if (!sessionMarkCodes.contains(markCode) && !remarkable)
                            {
                                pfMap.put(PERFORMANCE_4ALL_5ALL_EXC, false);
                                break;
                            }
                            if (markCode.equals(SessionMarkGradeValueCatalogItemCodes.HOROSHO)) fourCount++;
                        }
                    }
                } else
                    isOneMarkEmpty = true;
            }

            if (isMarkListEmpty)
                pfMap.clear();

            boolean performanceCheck = true;
            boolean lastSessionCheck = true;

            if (null != pfStatusId)
            {
                performanceCheck = !pfMap.isEmpty() &&
                        (
                                isPerformanceEqAndContain(PERFORMANCE_5ALL, pfStatusId, pfMap) ||
                                        isPerformanceEqAndContain(PERFORMANCE_4ALL, pfStatusId, pfMap) ||
                                        isPerformanceEqAndContain(PERFORMANCE_4SINGLE_5ALL, pfStatusId, pfMap) && fourCount == 1 ||
                                        isPerformanceEqAndContain(PERFORMANCE_4ALL_5ALL_INC, pfStatusId, pfMap) ||
                                        isPerformanceEqAndContain(PERFORMANCE_4ALL_5ALL_EXC, pfStatusId, pfMap) && fourCount != 0
                        );
            }
            if (null != lastSessionStatusId)
            {
                lastSessionCheck = lastSessionStatusId.equals(DEBT_STATUS_DEBT) && !isOneMarkEmpty || lastSessionStatusId.equals(DEBT_STATUS_NO_DEBT) && isOneMarkEmpty;
            }
            if (performanceCheck && lastSessionCheck)
            {
                resultIds.add(studentId);
            }
        }

        if (null == pfStatusId && null != lastSessionStatusId && lastSessionStatusId.equals(DEBT_STATUS_NO_DEBT) && CollectionUtils.isEmpty(structureList))
        {
            studentIds.removeAll(studentMap.keySet());
            resultIds.addAll(studentIds);
        }
        return resultIds;
    }

    private boolean isPerformanceEqAndContain(Long statusId, Long checkStatusId, Map<Long, Boolean> map)
    {
        return checkStatusId.equals(statusId) && map.get(statusId);
    }

    /**
     * Фильтрует студентов по фильтру "Академ. задолженность на начало сессии"
     *
     * @param studentIds          список идентификаторов студентов
     * @param statusId            академ. задолженность на начало сессии
     * @param year                учебный год приказа
     * @param part                часть учебного года приказа
     * @param structureList       типы мероприятий, которые необходимо учитывать при запросе (если null - учитываем все)
     * @param workPlanRowKindList виды дисциплин, которые необходимо учитывать при запросе (если null - учитываем все)
     * @return отфильтрованный список идентификаторов студентов
     */
    private Collection<Long> filterDebtStatus(
            final Collection<Long> studentIds,
            final Long statusId,
            final EducationYear year,
            final YearDistributionPart part,
            final List<EppRegistryStructure> structureList,
            final List<EppWorkPlanRowKind> workPlanRowKindList,
            ExecutionContext context)
    {
        final Set<Long> debtStudentIds = Sets.newHashSet();
        List<Long> actionIds = getActionBuilder(structureList, workPlanRowKindList)
                .column(property("a.id"))
                .where(in(property("s.id"), studentIds))
                .where(or(
                        eq(property("year", EppYearEducationProcess.educationYear().intValue()), value(year.getIntValue() - 1)),
                        and(
                                eq(property("year", EppYearEducationProcess.educationYear()), value(year)),
                                lt(property("wpe", EppStudentWorkPlanElement.part().number()), value(part.getNumber()))
                        )
                ))
                .createStatement(context.getSession()).list();

        // находим старший семестр в списке МСРП для каждого студента
        DQLSelectBuilder mark1 = getMarkBuilder(actionIds, "studentId", "termNumber");
        // если в старшем семестре есть не положительная оценка, то отбираем такого студента
        DQLSelectBuilder mark2 = getMarkBuilder(actionIds, "studentIdNotPositive", "termNumberNotPositive")
                .where(eq(property("mark", SessionMark.cachedMarkPositiveStatus()), value(Boolean.FALSE)));

        debtStudentIds.addAll(
                new DQLSelectBuilder()
                        .column(property("m1.studentId"), "studentId")
                        .fromDataSource(mark1.buildQuery(), "m1")
                        .fromDataSource(mark2.buildQuery(), "m2")
                        .where(eq(property("m1.studentId"), property("m2.studentIdNotPositive")))
                        .where(eq(property("m1.termNumber"), property("m2.termNumberNotPositive")))
                        .createStatement(context.getSession()).<Long>list()
        );

        context.getSession().flush(); // WTF ???
        context.getSession().clear();

        // если выбран фильтр, что нет задолженностей на начало сессии, то ревертим список
        if (statusId.equals(LAST_SESSION_NOT_ALL_MANDATORY_PASSED))
        {
            studentIds.removeAll(debtStudentIds);
            return studentIds;
        }
        return debtStudentIds;
    }

    /**
     * Возвращает мап оценок студентов по всем МСРП
     * учебного года и части года приказа
     *
     * @param studentIds список идентификаторов студентов
     * @param year       учебный год приказа
     * @param part       часть учебного года приказа
     * @return { student.id -> { action.id -> SessionMarkCatalogItem }}
     */
    private Map<Long, Map<Long, SessionMarkCatalogItem>> getStudentMap(Collection<Long> studentIds, EducationYear year, YearDistributionPart part, ExecutionContext context)
    {
        final boolean marksInSession = context.getBoolean(SpStudentPaymentOrderAllocationTabUI.PROP_MARKS_IN_SESSION, false);
        final List<EppRegistryStructure> structureList = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_REG_STRUCTURE_LIST);
        final List<EppWorkPlanRowKind> workPlanRowKindList = context.get(SpStudentPaymentOrderAllocationTabUI.PROP_WORK_PLAN_ROW_KIND_LIST);

        final Map<Long, Map<Long, SessionMarkCatalogItem>> resultMap = SafeMap.get(HashMap.class);

        List<Long> actionIds = getActionBuilder(structureList, workPlanRowKindList)
                .column(property("a.id"))
                .where(in(property("s.id"), studentIds))
                .where(eq(property("year", EppYearEducationProcess.educationYear()), value(year)))
                .where(eq(property("wpe", EppStudentWorkPlanElement.part()), value(part)))
                .createStatement(context.getSession()).list();

        List<Long> student2ActionList = getActionBuilder(structureList, workPlanRowKindList)
                .column(property("s.id")).distinct()
                .where(in(property("s.id"), studentIds))
                .where(eq(property("year", EppYearEducationProcess.educationYear()), value(year)))
                .where(eq(property("wpe", EppStudentWorkPlanElement.part()), value(part)))
                .createStatement(context.getSession()).list();

        for (List<Long> actionsPart : Lists.partition(actionIds, DQL.MAX_VALUES_ROW_NUMBER))
        {

            DQLSelectBuilder studentBuilder = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "a")
                    .column(property("a.id"))
                    .column(property("a", EppStudentWpeCAction.studentWpe().student().id()))
                    .where(in(property("a.id"), actionsPart));

            for (Object[] item : studentBuilder.createStatement(context.getSession()).<Object[]>list())
            {
                Long actionId = (Long) item[0];
                Long studentId = (Long) item[1];
                resultMap.get(studentId).putIfAbsent(actionId, null);
            }

            DQLSelectColumnNumerator markBuilder = new DQLSelectColumnNumerator(
                    new DQLSelectBuilder()
                            .fromEntity(SessionMark.class, "mark")
                            .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                            .joinPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                            .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "action")
                            .joinEntity("mark", DQLJoinType.left, SessionSlotLinkMark.class, "linkMark", eq(property("mark.id"), property("linkMark.id")))
                            .where(instanceOf("document", SessionStudentGradeBookDocument.class))
                            .where(eq(property("slot", SessionDocumentSlot.inSession()), value(marksInSession)))
                            .where(in(property("action.id"), actionsPart))
            );

            final int row_mark = markBuilder.column(property("mark"));
            final int row_target_mark = markBuilder.column(property("linkMark", SessionSlotLinkMark.target()));
            final int row_mark_action_id = markBuilder.column(property("action.id"));
            final int row_mark_student_id = markBuilder.column(property("action", EppStudentWpeCAction.studentWpe().student().id()));

            for (Object[] item : markBuilder.getDql().createStatement(context.getSession()).<Object[]>list())
            {
                Long actionId = (Long) item[row_mark_action_id];
                Long studentId = (Long) item[row_mark_student_id];
                SessionMark mark = (SessionMark) item[row_mark];
                SessionMark targetMark = (SessionMark) item[row_target_mark];
                SessionMark regularMark = targetMark != null ? targetMark : mark;
                resultMap.get(studentId).put(actionId, regularMark == null ? null : regularMark.getValueItem());
            }

            context.getSession().flush(); // WTF ???
            context.getSession().clear();
        }
        context.getSession().flush();
        context.getSession().clear();

        // заполняем студентов, у которых нет ни одной выставленной оценки, но есть МСРП
        student2ActionList.stream()
                .filter(studentId -> !resultMap.containsKey(studentId))
                .forEach(studentId -> resultMap.get(studentId).put(null, null));
        return resultMap;
    }

    /**
     * Билдер актуальных МСРП, в которых есть ссылка на строку РУП
     *
     * @param structureList       типы мероприятий, которые необходимо учитывать при запросе (если null - учитываем все)
     * @param workPlanRowKindList виды дисциплин, которые необходимо учитывать при запросе (если null - учитываем все)
     */
    private DQLSelectBuilder getActionBuilder(List<EppRegistryStructure> structureList, List<EppWorkPlanRowKind> workPlanRowKindList)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, "a")
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("a"), "wpe")
                .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("wpe"), "s")
                .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.year().fromAlias("wpe"), "year")
                .where(isNull(property("a", EppStudentWpeCAction.removalDate())))
                .where(isNull(property("wpe", EppStudentWorkPlanElement.removalDate())))
                .where(isNotNull(property("wpe", EppStudentWorkPlanElement.sourceRow())));

        if (CollectionUtils.isNotEmpty(workPlanRowKindList))
            builder.where(in(property("wpe", EppStudentWorkPlanElement.sourceRow().kind()), workPlanRowKindList));

        if (CollectionUtils.isNotEmpty(structureList))
            builder.where(in(property("wpe", EppStudentWorkPlanElement.registryElementPart().registryElement().parent()), structureList));

        return builder;
    }

    /**
     * Билдер оценок из зачетки студента
     *
     * @param actionIds    список идентификаторов МСРП
     * @param studentAlias алиас для студента
     * @param termAlias    алиас для семестра
     */
    private DQLSelectBuilder getMarkBuilder(List<Long> actionIds, String studentAlias, String termAlias)
    {
        return new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "mark")
                .column(property("s.id"), studentAlias)
                .column(max(property("wpe", EppStudentWorkPlanElement.term().intValue())), termAlias)
                .joinPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                .joinPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "action")
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("action"), "wpe")
                .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("wpe"), "s")
                .where(instanceOf("document", SessionStudentGradeBookDocument.class))
                .where(eq(property("slot", SessionDocumentSlot.inSession()), value(Boolean.FALSE)))
                .where(in(property("action.id"), actionIds))
                .group(property("s.id"));
    }
}