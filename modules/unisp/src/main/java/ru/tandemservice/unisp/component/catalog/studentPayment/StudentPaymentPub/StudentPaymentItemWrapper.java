/**
 * $Id$
 */
package ru.tandemservice.unisp.component.catalog.studentPayment.StudentPaymentPub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.tool.tree.IHierarchyItem;

import ru.tandemservice.unisp.entity.catalog.PaymentKind;
import ru.tandemservice.unisp.entity.catalog.StudentPayment;

/**
 * @author dseleznev
 * Created on: 19.12.2009
 */
public class StudentPaymentItemWrapper extends IdentifiableWrapper<ICatalogItem> implements IHierarchyItem
{
    private static final long serialVersionUID = -3910751058942202575L;
    public static final String P_TITLE = "title";
    public static final String P_VALUE = "value";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_FIXED_VALUE = "fixedValue";
    public static final String L_KIND = "kind";

    public static final String P_ROOT_NODE = "rootNode";
    public static final String P_CHILD_NODE = "childNode";

    private ICatalogItem _catalogItem;
    private StudentPaymentItemWrapper _parent;

    public StudentPaymentItemWrapper(Long id, ICatalogItem catalogItem, StudentPaymentItemWrapper parent)
    {
        super(id, catalogItem.getTitle());
        _parent = parent;
        _catalogItem = catalogItem;
    }

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return _parent;
    }

    public String getShortTitle()
    {
        if (_catalogItem instanceof StudentPayment)
            return ((StudentPayment)_catalogItem).getShortTitle();
        return null;
    }

    public Boolean getFixedValue()
    {
        if (_catalogItem instanceof StudentPayment)
            return ((StudentPayment)_catalogItem).isFixedValue();
        return null;
    }

    public Long getValue()
    {
        if (_catalogItem instanceof StudentPayment)
            return ((StudentPayment)_catalogItem).getValue();
        return null;
    }

    public PaymentKind getKind()
    {
        if (_catalogItem instanceof StudentPayment)
            return ((StudentPayment)_catalogItem).getKind();
        return null;
    }

    public boolean isRootNode()
    {
        return !(_catalogItem instanceof StudentPayment);
    }
    
    public boolean isChildNode()
    {
        return _catalogItem instanceof StudentPayment;
    }

    public ICatalogItem getCatalogItem()
    {
        return _catalogItem;
    }
}