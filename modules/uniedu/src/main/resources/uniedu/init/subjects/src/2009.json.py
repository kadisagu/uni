#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
from collections import OrderedDict
import hashlib

okso_qlist = OrderedDict()
okso_subjs = OrderedDict()
okso_links = OrderedDict()

def parent(okso):
    while okso[-2:] == '00':
        okso = okso[:-2]
    return okso[:-2].ljust(6, '0')

def insert(dict, key, value):
    old = dict.get(key, None)
    if (old is not None) and (old != value):
        print "\n",old,"\n",value
        raise KeyError(old + "!=" + value)
    dict[key] = value

quals = OrderedDict()

quals['62'] = 'Бакалавр'
quals['68'] = 'Магистр'
quals['65'] = 'Специалист'
quals['50'] = None
quals['40'] = None
quals['00'] = 'Кандидат наук'

def group(qcode, full_code):
    # для кандидатов наук укрупнённую группу напишем хардкодом:
    if qcode == '00':
        return '000000'
    return full_code[0:2] + '0000'

for qcode, qtitle in quals.items():
    if len(qcode) != 2: raise KeyError(qcode)
    reader = csv.reader(open('2009.%s.utf8.csv' % qcode, 'rb'), delimiter=';', quotechar='"')

    for row in reader:
        full_code   = row[0].strip()
        ugs_code    = group(qcode, full_code)
        title = row[1].strip()

        # okso subjects
        subj_code = "2009.%s.%s" % (qcode, full_code)
        subj_item = '["2009.%s","%s","%s","%s","%s"]' % (qcode, subj_code, ugs_code, full_code, title)
        insert(okso_subjs, subj_code, subj_item)

        # qualifications
        if qtitle is not None:

            # okso qualifications
            qual_title_code = "2009.%s" % qcode  # по одной на каждый уровень, если есть
            qual_item = '["2009.%s", "%s", "%s", "%s"]' % (qcode, qual_title_code, qcode, qtitle)
            insert(okso_qlist, qual_title_code, qual_item)

            # okso subject-to-qualification links
            link_code = "%s.%s" % (qual_title_code, full_code)
            link_item = '["%s","%s"]' % (qual_title_code, subj_code)
            insert(okso_links, link_code, link_item)

# write result to file
def write_result(name, result):
    f = open('2009.%s.init.json' % name, 'w')
    f.write("""{\n""")

    s = open('2009.%s.init.header' % name, 'r')
    for l in s.readlines(): f.write(l)
    s.close()

    f.write(""" "data":[\n""")
    first = 1
    for k, v in result.items():
        if first: first = 0
        else: f.write(",\n")
        f.write(v)

    f.write("""\n""")
    f.write(""" ]\n""")
    f.write("""}\n""")

write_result("A.qlist", okso_qlist)
write_result("B.subjs", okso_subjs)
write_result("C.links", okso_links)
