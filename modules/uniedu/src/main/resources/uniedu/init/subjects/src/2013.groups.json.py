#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
from collections import OrderedDict
import hashlib

f_list = OrderedDict()
g_list = OrderedDict()

def insert(dict, key, value):
    old = dict.get(key, None)
    if (old is not None) and (old != value):
        print "\n",old,"\n",value
        raise KeyError(old + "!=" + value)
    dict[key] = value

reader = csv.reader(open('2013.groups.utf8.csv', 'rb'), delimiter=';', quotechar='"')

for row in reader:
    g_code   = row[0].strip()
    g_title  = row[1].strip()
    f_title  = row[2].strip()

    # field
    f_code   = "2013.%s" % hashlib.sha1(f_title.upper()).hexdigest()
    f_item = '["%s","%s"]' % (f_code, f_title)
    insert(f_list, f_code, f_item)

    # group
    g_item = '["%s","%s","%s"]' % (g_code, f_code, g_title)
    insert(g_list, g_code, g_item)

# fields
f = open('2013.fields.init.json.data', 'w')
for k, v in f_list.items():
    f.write(v + ",\n")

# groups
f = open('2013.groups.init.json.data', 'w')
for k, v in g_list.items():
    f.write(v + ",\n")
