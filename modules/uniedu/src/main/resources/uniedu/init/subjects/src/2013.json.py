#!/usr/bin/env python
# -*- coding: utf-8 -*-


import csv
from collections import OrderedDict
import hashlib

q_list = OrderedDict()
s_list = OrderedDict()
l_list = OrderedDict()

def insert(dict, key, value):
    old = dict.get(key, None)
    if (old is not None) and (old != value):
        print "\n",old,"\n",value
        raise KeyError(old + "!=" + value)
    dict[key] = value

def group(s_code, x_code):
    if s_code[3:5] != x_code: raise KeyError, "%s !~ %s" % (s_code, x_code)
    return s_code[0:2]


xcodes = ['01', '02', '03', '04', '05', '06', '07', '08', '10']
for x_code in xcodes:
    if len(x_code) != 2: raise KeyError(x_code)
    reader = csv.reader(open('2013.%s.utf8.csv' % x_code, 'rb'), delimiter=';', quotechar='"')

    for row in reader:
        if len(row) == 0: continue
        s_code = row[0].strip()
        s_group = group(s_code, x_code)
        s_title = row[1].strip()

        # subjects (для 2013 x_code зашит в s_code)
        subj_code = "2013.%s" % s_code
        subj_item = '["2013.%s","%s.00.00","%s","%s","%s"]' % (x_code, s_group, subj_code, s_code, s_title)
        insert(s_list, subj_code, subj_item)

        qlist = [ x.strip() for x in row[2].split(";") ]
        for q in qlist:
            # qualifications
            qual_title_code = "2013.%s.%s" % (x_code, hashlib.sha1(q).hexdigest())
            qual_item = '["2013.%s", "%s", "%s"]' % (x_code, qual_title_code, q)
            insert(q_list, qual_title_code, qual_item)

            # subject-to-qualification links
            link_code = "%s.%s" % (qual_title_code, subj_code)
            link_item = '["%s","%s"]' % (qual_title_code, subj_code)
            insert(l_list, link_code, link_item)

# write result to file
def write_result(name, result):
    f = open('2013.%s.init.json' % name, 'w')
    f.write("""{\n""");

    s = open('2013.%s.init.header' % name, 'r')
    for l in s.readlines(): f.write(l)
    s.close()

    f.write(""" "data":[\n""");

    first = 1
    for k, v in result.items():
        if first: first = 0
        else: f.write(",\n")
        f.write(v)

    f.write("""\n""");
    f.write(""" ]\n""");
    f.write("""}\n""");



write_result("A.subjs", s_list) # subjects
write_result("B.qlist", q_list) # qualifications
write_result("C.links", l_list) # qualification links

