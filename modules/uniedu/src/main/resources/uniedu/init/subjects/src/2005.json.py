#!/usr/bin/env python

import csv
from collections import OrderedDict
import hashlib

rows=[]
rows += [ row for row in csv.reader(open('2005.utf8.sort.csv', 'rb'), delimiter=';', quotechar='"') ]
rows += [ row for row in csv.reader(open('2005.utf8.cross.csv', 'rb'), delimiter=';', quotechar='"') ]

okso_items = OrderedDict()
okso_qlist = OrderedDict()
okso_subjs = OrderedDict()
okso_links = OrderedDict()

def parent(okso):
    okso = okso.split('/')[0]
    while okso[-2:] == '00':
        okso = okso[:-2]
    return okso[:-2].ljust(6, '0')

def insert(dict, key, value):
    old = dict.get(key, None)
    if (old is not None) and (old != value):
        print "\n",old,"\n",value
        raise KeyError(old + "!=" + value)
    dict[key] = value


for row in rows:
    if len(row) == 0: continue
    okso_code   = row[0].strip()

    okso_parent = parent(okso_code)
    if okso_parent[2:] == '0000':
        okso_parent = None
    else:
        if okso_items.get(okso_parent, None) is None: 
            print ("%s;No-item-with-okso;;" % okso_parent)

    ugs_code = okso_code[0:2] + '0000'
    title = row[1].strip()
    qcode = row[2].strip()

    # okso item
    if okso_parent is None:
        okso_item = '["%s", "",            "2005.%s", "%s", "%s"]' % (ugs_code, okso_code, okso_code, title)
    else:
        okso_item = '["%s", "2005.%s", "2005.%s", "%s", "%s"]' % (ugs_code, okso_parent, okso_code, okso_code, title)
    insert(okso_items, okso_code, okso_item)

    if len(qcode) == 0: continue
    if len(qcode) != 2: raise KeyError(okso_code)

    slcode = qcode
    if slcode == '51': slcode = '50'
    elif slcode == '52': slcode = '50'

    # okso subjects
    subj_code = "2005.%s.%s" % (okso_code, qcode)
    subj_item = '["2005.%s","%s","2005.%s","%s.%s", "%s"]' % (slcode, subj_code, okso_code, okso_code, qcode, title)
    insert(okso_subjs, subj_code, subj_item)

    qlist = [ x.strip() for x in row[3].split(";") ]
    for q in qlist:
        # okso qualifications
        qual_title_code = "2005.%s.%s" % (qcode, hashlib.sha1(q).hexdigest())
        qual_item = '["2005.%s", "%s", "%s", "%s"]' % (slcode, qual_title_code, qcode, q)
        insert(okso_qlist, qual_title_code, qual_item)

        # okso subject-to-qualification links
        link_code = "%s.%s" % (qual_title_code, okso_code)
        link_item = '["%s","%s"]' % (qual_title_code, subj_code)
        insert(okso_links, link_code, link_item)


# okso items
f_okso_items = open('2005.A.items.init.json.data', 'w')
for k, v in okso_items.items():
    f_okso_items.write(v + ",\n")

# okso qualifications
f_okso_qlist = open('2005.B.qlist.init.json.data', 'w')
for k, v in okso_qlist.items():
    f_okso_qlist.write(v + ",\n")

# okso qualifications
f_okso_subjs = open('2005.C.subjs.init.json.data', 'w')
for k, v in okso_subjs.items():
    f_okso_subjs.write(v + ",\n")

# okso links
f_okso_links = open('2005.D.links.init.json.data', 'w')
for k, v in okso_links.items():
    f_okso_links.write(v + ",\n")

