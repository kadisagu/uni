<?xml version="1.0" encoding="UTF-8"?>
<!-- $Id: contractor.entity.xml 8273 2009-06-03 07:12:55Z vdanilov $ -->

<!-- образовательные программы -->
<entity-config name="uniedu-program-entities"
               package-name="ru.tandemservice.uniedu.program.entity"
               xmlns="http://www.tandemframework.org/meta/entity"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xsi:schemaLocation="http://www.tandemframework.org/meta/entity http://www.tandemframework.org/schema/meta/meta-entity.xsd">

	<!-- образовательная программа -->
	<entity name="eduProgram" abstract="true" title="Образовательная программа" table-name="edu_program_t">
		<comment>
			Образовательная программа, ОП.
			Реализует направление подготовки в акк.ОУ в некотором году (приема).
		</comment>
		
		<property name="programUniqueKey" type="trimmedstring" required="true" title="Ключ образовательной программы в рамках года">
        	<comment>
                Генерируется и проверяется на уникальность в рамках года на основе параметров ОП (включая данные подклассов и связанных объектов)
                http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23104523
            </comment>
        </property>

        <unique-constraint name="uk_programUniqueKey" message="Значение в поле «Ключ образовательной программы» объекта «Образовательная программа» должно быть уникальным в рамках учебного года.">
          <property-link name="year"/>
          <property-link name="programUniqueKey"/>
          <handler class-name="ru.tandemservice.uniedu.program.util.ProgramKeyUniqueHandler" path="instance"/>
        </unique-constraint>

		<property name="title" type="trimmedstring" required="true" title="Название">
        	<comment>Название образовательной программы</comment>
        </property>
        <property name="shortTitle" type="trimmedstring" required="true" title="Сокращенное название"/>
		
		<!-- ключевые поля, значение которых недопустимо после создания объекта -->
		<!-- { -->
		<many-to-one name="kind" entity-ref="eduProgramKind" required="true" title="Вид образовательной программы">
        	<comment>Определяет вид ОП, через который определяется принадлежность уровню (виду, подвиду) образования</comment>
        </many-to-one>
	 	<many-to-one name="year" entity-ref="educationYear" required="true" title="Учебный год начала обучения">
        	<comment>Учебный год, в котором начинается обучение по образовательной программе</comment>
        </many-to-one>
        <many-to-one name="institutionOrgUnit" entity-ref="eduInstitutionOrgUnit" required="true" title="Аккредитованное подразделение">
        	<comment>Аккредитованное ОУ, в рамках которого происходит обучение</comment>
        </many-to-one>
		<many-to-one name="ownerOrgUnit" entity-ref="eduOwnerOrgUnit" required="true" title="Выпускающее подразделение">
        	<!-- Ответственное подразделение всегда одно! Перечень подразделений, участвующих в разработке ОП и планов будет определятся через объект-связь к персистентному интерфейсу, когда это потребуется -->
        	<comment>Подразделение, ответственное за выпуск по образовательной программе</comment>
        </many-to-one>
	 	<many-to-one name="form" entity-ref="eduProgramForm" required="true" title="Форма обучения">
        	<comment>Форма обучения образовательной программы</comment>
        </many-to-one>
	 	<many-to-one name="duration" entity-ref="eduProgramDuration" required="true" title="Продолжительность обучения">
        	<comment>Продолжительность обучения по образовательной программе</comment>
        </many-to-one>
        <many-to-one name="eduProgramTrait" entity-ref="eduProgramTrait" title="Особенность реализации ОП">
            <comment>Особенность реализации ОП</comment>
        </many-to-one>
		<!-- } -->
        
	</entity>


	<child-entity name="eduProgramPrimary" parent-ref="eduProgram" abstract="true" title="Основная образовательная программа"> <!--table-name="edu_program_primary_t"-->
		<comment>
			Основное образование (все, что не является дополнительным).
		</comment>
	</child-entity>

	<child-entity name="eduProgramBasic" parent-ref="eduProgramPrimary" title="Основная общеобразовательная программа" table-name="edu_program_basic_t">
		<comment>
			Основное общее образование: реализуется по уровням основного образования, уровень берется из вида образовательной программы.
		</comment>
		
        <check-constraint name="eduProgramBasic_kind" message="Вид ОП должен соответствовать основной общеобразовательной программе.">
            <![CDATA[ kind.programBasic = true ]]>
        </check-constraint>		
        <check-constraint name="eduProgramBasic_form" message="Форма обучения должна быть очной.">
            <![CDATA[ form.code in ('1') ]]>
        </check-constraint>		
	</child-entity>
	
	<child-entity name="eduProgramProf" parent-ref="eduProgramPrimary" abstract="true" title="Основная профессиональная образовательная программа" table-name="edu_program_prof_t">
		<comment>
			Профессиональное образование: реализуется по направлениям подготовки (специальностям, профессиям).
		</comment>
		<many-to-one name="baseLevel" entity-ref="eduLevel" required="true" title="На базе">
        	<comment>Необходимый уровень для обучения по образовательной программе</comment>
        </many-to-one>
		<many-to-one name="programSubject" entity-ref="eduProgramSubject" required="true" title="Направление подготовки"/>
        <many-to-one name="programQualification" entity-ref="eduProgramQualification" required="true" title="Квалификация"/>

        <many-to-one name="subjectQualification" entity-ref="eduProgramSubjectQualification" required="true" title="Квалификации направления подготовки (для констрейнта)">
            <comment>
                Связь направления подготовки и квалификации. Ссылка нужна для констрейнта, чтобы нельзя было эту связь удалить.
                <!-- Можно ею заменить направление и квалификацию, но это снижение производительности (всегда лишний джойн) и рефакторинг. -->
            </comment>
        </many-to-one>

		<!-- проверка на код kind осуществляется через проверку в subjectList -->
        <check-constraint name="eduProgramProf_programSubject" message="Вид ОП должен совпадать с видом ОП из перечня направления подготовки.">
            <![CDATA[ programSubject.subjectIndex.programKind = kind ]]>
        </check-constraint>
        <check-constraint name="qualification_check" message="Связь квалификации направления подготовки (для констрейнта) должна ссылаться на направление и квалификацию ОП.">
            <![CDATA[ subjectQualification.programQualification = programQualification and subjectQualification.programSubject = programSubject ]]>
        </check-constraint>
	</child-entity>
	
	<child-entity name="eduProgramHigherProf" parent-ref="eduProgramProf" title="Образовательная программа ВО" table-name="edu_program_prof_higher_t">
		<comment>
			Высшее образование: реализуется по направлениям подготовки (специальностям) с указанием направленности.
		</comment>
		<many-to-one name="programSpecialization" entity-ref="eduProgramSpecialization" required="true" title="Направленность">
        	<comment>Направленность образовательной программы</comment>
        </many-to-one>

        <many-to-one name="programOrientation" entity-ref="eduProgramOrientation" required="false" title="Ориентация ОП"/>

        <check-constraint name="eduProgramHigherProf_orientation" message="Если есть хотя бы одна ориентация для перечня квалификации, и задана ориентация ОП, то перечень этой ориентации должен совпадать с перечнем квалификации.">
            <![CDATA[ (programOrientation is null) or (programQualification.subjectIndex = programOrientation.subjectIndex) or not exists(from eduProgramOrientation o where o.subjectIndex = programQualification.subjectIndex) ]]>
        </check-constraint>
        <check-constraint name="eduProgramHigherProf_programSpecialization" message="Перечень направленности должен совпадать с перечнем направления.">
            <![CDATA[ programSpecialization.programSubject = programSubject ]]>
        </check-constraint>
        <check-constraint name="eduProgramHigherProf_kind" message="Вид ОП должен соответствовать уровню ВО.">
            <![CDATA[ kind.programHigherProf = true ]]>
        </check-constraint>
	</child-entity>
	
	<child-entity name="eduProgramSecondaryProf" parent-ref="eduProgramProf" title="Образовательная программа СПО" table-name="edu_program_prof_secondary_t">
		<comment>
			Среднее профессиональное образование: реализуется по направлениям (специальностям, профессиям) и признаку углубленного изучения.
		</comment>
		<property name="inDepthStudy" type="boolean" required="true" title="Углубленное изучение">
        	<comment>Определяет уровень изучения ОП: true = углубленный уровень, false = базовый уровень</comment>
        </property>

        <check-constraint name="eduProgramSecondaryProf_kind" message="Вид ОП должен соответствовать уровню СПО.">
            <![CDATA[ kind.programSecondaryProf = true ]]>
        </check-constraint>
	</child-entity>
    
	<child-entity name="eduProgramVocational" parent-ref="eduProgramPrimary" title="Основная программа профессионального обучения"><!-- table-name="edu_program_vocational_t"-->
		<!-- пока не делаем -->
		<comment>
			Профессиональное обучение: реализуется по профессиям рабочих и должностям служащих.
		</comment>
		<!-- TODO: ограничение на kind -->
		<!-- TODO: ссылка на реестр профессий рабочих и должностей -->
	</child-entity>
	

	<child-entity name="eduProgramAdditional" parent-ref="eduProgram" abstract="true" title="Дополнительная образовательная программа"><!-- table-name="edu_program_additional_t"-->
		<comment>
			Дополнительное образование: реализуются на усмотрение ОУ, определяется видом и названием.
		</comment>
	</child-entity>
	
	<child-entity name="eduProgramAdditionalProf" parent-ref="eduProgramAdditional" title="Образовательная программа ДПО" table-name="edu_program_additional_prof_t">
		<comment>
			ДПО: на усмотрение ОУ, определяется названием.
		</comment>
		
        <check-constraint name="eduProgramAdditionalProf_kind" message="Вид ОП должен соответствовать уровню ДПО.">
            <![CDATA[ kind.programAdditionalProf = true ]]>
        </check-constraint>
	</child-entity>

</entity-config>
