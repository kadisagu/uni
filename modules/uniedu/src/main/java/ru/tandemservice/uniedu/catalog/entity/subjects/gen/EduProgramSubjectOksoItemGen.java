package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoGroup;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление, специальность, профессия (перечень направлений подготовки ОКСО)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSubjectOksoItemGen extends EntityBase
 implements INaturalIdentifiable<EduProgramSubjectOksoItemGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem";
    public static final String ENTITY_NAME = "eduProgramSubjectOksoItem";
    public static final int VERSION_HASH = -1119448601;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_ITEM_CODE = "itemCode";
    public static final String L_PARENT = "parent";
    public static final String L_GROUP = "group";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _itemCode;     // Код элемента
    private EduProgramSubjectOksoItem _parent;     // Родительский элемент
    private EduProgramSubjectOksoGroup _group;     // Укрупненная группа
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * Код элемента (то, что написано в классификаторе) - не уникален
     *
     * @return Код элемента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getItemCode()
    {
        return _itemCode;
    }

    /**
     * @param itemCode Код элемента. Свойство не может быть null.
     */
    public void setItemCode(String itemCode)
    {
        dirty(_itemCode, itemCode);
        _itemCode = itemCode;
    }

    /**
     * @return Родительский элемент.
     */
    public EduProgramSubjectOksoItem getParent()
    {
        return _parent;
    }

    /**
     * @param parent Родительский элемент.
     */
    public void setParent(EduProgramSubjectOksoItem parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Укрупненная группа. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectOksoGroup getGroup()
    {
        return _group;
    }

    /**
     * @param group Укрупненная группа. Свойство не может быть null.
     */
    public void setGroup(EduProgramSubjectOksoGroup group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramSubjectOksoItemGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EduProgramSubjectOksoItem)another).getCode());
            }
            setItemCode(((EduProgramSubjectOksoItem)another).getItemCode());
            setParent(((EduProgramSubjectOksoItem)another).getParent());
            setGroup(((EduProgramSubjectOksoItem)another).getGroup());
            setTitle(((EduProgramSubjectOksoItem)another).getTitle());
        }
    }

    public INaturalId<EduProgramSubjectOksoItemGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramSubjectOksoItemGen>
    {
        private static final String PROXY_NAME = "EduProgramSubjectOksoItemNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramSubjectOksoItemGen.NaturalId) ) return false;

            EduProgramSubjectOksoItemGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSubjectOksoItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSubjectOksoItem.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSubjectOksoItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "itemCode":
                    return obj.getItemCode();
                case "parent":
                    return obj.getParent();
                case "group":
                    return obj.getGroup();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "itemCode":
                    obj.setItemCode((String) value);
                    return;
                case "parent":
                    obj.setParent((EduProgramSubjectOksoItem) value);
                    return;
                case "group":
                    obj.setGroup((EduProgramSubjectOksoGroup) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "itemCode":
                        return true;
                case "parent":
                        return true;
                case "group":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "itemCode":
                    return true;
                case "parent":
                    return true;
                case "group":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "itemCode":
                    return String.class;
                case "parent":
                    return EduProgramSubjectOksoItem.class;
                case "group":
                    return EduProgramSubjectOksoGroup.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSubjectOksoItem> _dslPath = new Path<EduProgramSubjectOksoItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSubjectOksoItem");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * Код элемента (то, что написано в классификаторе) - не уникален
     *
     * @return Код элемента. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getItemCode()
     */
    public static PropertyPath<String> itemCode()
    {
        return _dslPath.itemCode();
    }

    /**
     * @return Родительский элемент.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getParent()
     */
    public static EduProgramSubjectOksoItem.Path<EduProgramSubjectOksoItem> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Укрупненная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getGroup()
     */
    public static EduProgramSubjectOksoGroup.Path<EduProgramSubjectOksoGroup> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EduProgramSubjectOksoItem> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _itemCode;
        private EduProgramSubjectOksoItem.Path<EduProgramSubjectOksoItem> _parent;
        private EduProgramSubjectOksoGroup.Path<EduProgramSubjectOksoGroup> _group;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EduProgramSubjectOksoItemGen.P_CODE, this);
            return _code;
        }

    /**
     * Код элемента (то, что написано в классификаторе) - не уникален
     *
     * @return Код элемента. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getItemCode()
     */
        public PropertyPath<String> itemCode()
        {
            if(_itemCode == null )
                _itemCode = new PropertyPath<String>(EduProgramSubjectOksoItemGen.P_ITEM_CODE, this);
            return _itemCode;
        }

    /**
     * @return Родительский элемент.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getParent()
     */
        public EduProgramSubjectOksoItem.Path<EduProgramSubjectOksoItem> parent()
        {
            if(_parent == null )
                _parent = new EduProgramSubjectOksoItem.Path<EduProgramSubjectOksoItem>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Укрупненная группа. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getGroup()
     */
        public EduProgramSubjectOksoGroup.Path<EduProgramSubjectOksoGroup> group()
        {
            if(_group == null )
                _group = new EduProgramSubjectOksoGroup.Path<EduProgramSubjectOksoGroup>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoItem#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramSubjectOksoItemGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EduProgramSubjectOksoItem.class;
        }

        public String getEntityName()
        {
            return "eduProgramSubjectOksoItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
