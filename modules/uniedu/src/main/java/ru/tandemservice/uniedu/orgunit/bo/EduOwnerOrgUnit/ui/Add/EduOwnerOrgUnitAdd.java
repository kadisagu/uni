/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 04.02.2014
 */
@Configuration
public class EduOwnerOrgUnitAdd extends BusinessComponentManager
{
    public static final String ORG_UNIT_DS = "orgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler())
                        .addColumn(OrgUnit.fullTitle().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                IDQLSelectableQuery eduInstitutionOrgUnitsQuery = new DQLSelectBuilder()
                        .fromEntity(EduOwnerOrgUnit.class, "eoou")
                        .column(property("eoou", EduOwnerOrgUnit.orgUnit().id()))
                        .buildQuery();

                dql.where(notIn(property(alias, OrgUnit.id()), eduInstitutionOrgUnitsQuery));
                dql.where(eq(property(alias, OrgUnit.archival()), value(Boolean.FALSE)));

                super.applyWhereConditions(alias, dql, context);
            }
        }
                .filter(OrgUnit.fullTitle())
                .order(OrgUnit.fullTitle())
                .pageable(true);
    }
}