/* $Id$ */
package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.debug.Debug;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 14.10.2015
 */
public class MS_uniedu_2x9x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Переименовываем квалификацию "Академический бакалавр" в "Бакалавр".
        // Код тоже меняется.

        final String academicBachelorCode = "2013.03.ebbeb20a0959bb4ce68a04cbb534c5fa5a93e6d3";
        final String simpleBachelorCode = "2013.03.9ac370aa611a7839fa6d35c145f724dd88942b02";
        final String simpleBachelorTitle = "Бакалавр";
        int ret = tool.executeUpdate(
                "update edu_c_pr_qual_t set code_p=?, title_p=? where code_p=?",
                simpleBachelorCode, simpleBachelorTitle, academicBachelorCode
        );
        if (Debug.isEnabled()) {
            System.out.println("MS_uniedu_2x9x1_0to1: renamed " + ret + " qualifications");
        }
    }
}