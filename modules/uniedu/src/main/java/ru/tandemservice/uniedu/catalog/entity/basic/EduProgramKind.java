package ru.tandemservice.uniedu.catalog.entity.basic;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.bo.Declinable.ui.Edit.DeclinableEdit;
import org.tandemframework.shared.commonbase.base.bo.Declinable.ui.Edit.DeclinableEditUI;
import org.tandemframework.shared.commonbase.base.entity.IDeclinable;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IPrioritizedCatalogItem;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorLanguage;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EduProgramKindGen;

import java.util.*;

/** @see EduProgramKindGen */
public class EduProgramKind extends EduProgramKindGen implements IDynamicCatalogItem,  IPrioritizedCatalogItem
{
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name){
        return new EntityComboDataSourceHandler(name, EduProgramKind.class)
                .order(EduProgramKind.priority())
                .filter(EduProgramKind.title());
    }

    private static Set<String> HIDDEN_PROPERTIES = ImmutableSet.of(
        EduProgramKind.P_PROGRAM_ADDITIONAL_PROF,
        EduProgramKind.P_PROGRAM_BASIC,
        EduProgramKind.P_PROGRAM_HIGHER_PROF,
        EduProgramKind.P_PROGRAM_PROF,
        EduProgramKind.P_PROGRAM_SECONDARY_PROF,
        EduProgramKind.P_PROGRAM_BACHELOR_DEGREE,
        EduProgramKind.P_PROGRAM_MASTER_DEGREE,
        EduProgramKind.P_PROGRAM_SPECIALIST_DEGREE,
        EduProgramKind.P_PROGRAM_HIGHER_QUAL,
        EduProgramKind.P_PROGRAM_POSTGRADUATE,
        EduProgramKind.P_PROGRAM_TRAINEESHIP,
        EduProgramKind.P_PROGRAM_INTERNSHIP
    );

    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public Collection<String> getHiddenFields() { return HIDDEN_PROPERTIES; }
            @Override public Collection<String> getHiddenFieldsAddEditForm() {
                final List<String> list = new ArrayList<>(HIDDEN_PROPERTIES);
                list.add(EduProgramKindGen.P_EDU_PROGRAM_SUBJECT_KIND);
                return null;
            }
        };
    }

    @Override
    public Collection<String> getDeclinableProperties(){ return Arrays.asList(P_TITLE, P_EDU_PROGRAM_SUBJECT_KIND); }

    public static class EditDeclinationAction extends NamedUIAction
    {
        public EditDeclinationAction(String name){ super(name); }

        @Override
        public void execute(IUIPresenter presenter)
        {
            InflectorLanguage lang = IUniBaseDao.instance.get().get(InflectorLanguage.class, InflectorLanguage.enabled().s(), Boolean.TRUE);
            if (null == lang)
                throw new ApplicationException("Не установлен активный язык склонений.");

            presenter.getActivationBuilder().asRegion(DeclinableEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, presenter.getListenerParameterAsLong())
                .parameter(DeclinableEditUI.BIND_LANGUAGE_ID, lang.getId())
                .activate();
        }
    }

    /**
     * @param inflectorVariantCode код падежа из {@link org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes}
     * @return название типа направления (направление, профессия, специальность) в нужном падеже
     */
    public String getEduProgramSubjectKindTitle(String inflectorVariantCode) {
        return DeclinableManager.instance().dao().getPropertyValue(this, P_EDU_PROGRAM_SUBJECT_KIND, IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, inflectorVariantCode));
    }

    /**
     * @param inflectorVariantCode код падежа из {@link org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes}
     * @return название в нужном падеже, если оно отсутствует просто название
     */
    public String getDeclinableTitle(String inflectorVariantCode)
    {
        String declinableValue = DeclinableManager.instance().dao().getPropertyValue(this, P_TITLE, IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, inflectorVariantCode));
        return declinableValue != null ? declinableValue : getTitle();
    }

    public boolean isBS() {
        return EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA.equals(getCode()) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(getCode());
    }

    public boolean isHigher() {
        return EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_.equals(getCode()) || EduProgramKindCodes.PROGRAMMA_ORDINATURY.equals(getCode()) || EduProgramKindCodes.PROGRAMMA_INTERNATURY.equals(getCode());
    }
}
