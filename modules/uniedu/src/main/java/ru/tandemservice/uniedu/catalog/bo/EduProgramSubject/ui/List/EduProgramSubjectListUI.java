/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.List;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSearchListUtil;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.BaseCatalogPubUI;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Edit.EduProgramSubjectEdit;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubject.ui.Pub.EduProgramSubjectPub;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author oleyba
 * @since 11/20/13
 */
@State({
    @Bind(key = DefaultCatalogPubModel.CATALOG_CODE, binding = "catalogCode")
})
public class EduProgramSubjectListUI extends BaseCatalogPubUI
{
    private static final String QUAL_TITLE_LIST = "qualTitleList";

    @Override
    public boolean isUserCatalog()
    {
        return false;
    }

    @Override
    public String getAdditionalFiltersPageName()
    {
        return EduProgramSubjectListUI.class.getPackage()+".EduProgramSubjectListFilters";
    }

    @Override
    protected void prepareItemDS() {
        final DynamicListDataSource<ICatalogItem> dataSource = new DynamicListDataSource<>(this, component -> {
            updateItemDS();
        });

        dataSource.addColumn(new SimpleColumn("Код по перечню", EduProgramSubject.subjectCode().s()).setClickable(false));
        dataSource.addColumn(new PublisherLinkColumn("Название", ICatalogItem.CATALOG_ITEM_TITLE).setResolver(new SimplePublisherLinkResolver("id").setComponentName(EduProgramSubjectPub.class.getSimpleName())));
        dataSource.addColumn(new SimpleColumn("Сокр. название", EduProgramSubject.shortTitle()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Вид обр. программы", EduProgramSubject.subjectIndex().programKind().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Квалификации", QUAL_TITLE_LIST, NewLineFormatter.SIMPLE).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Укрупненная группа", EduProgramSubject.groupTitle().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Перечень", EduProgramSubject.subjectIndex().title()).setClickable(false).setOrderable(false));

        addActionColumns(dataSource);

        setItemDS(dataSource);
    }

    @Override
    protected void updateItemDS()
    {
        DQLOrderDescriptionRegistry orderDescriptionRegistry = new DQLOrderDescriptionRegistry(getItemClass(), "ci");
        DQLSelectBuilder dql = orderDescriptionRegistry.buildDQLSelectBuilder().column(property("ci"));

        CommonBaseFilterUtil.applySimpleLikeFilter(dql, "ci", EduProgramSubject.subjectCode().s(), getSettings().<String>get("subjectCode"));
        CommonBaseFilterUtil.applySimpleLikeFilter(dql, "ci", ICatalogItem.CATALOG_ITEM_TITLE, getSettings().<String>get("title"));
        CommonBaseFilterUtil.applySelectFilter(dql, "ci", EduProgramSubject.subjectIndex(), getSettings().<EduProgramSubjectIndex>get("subjectIndex"));
        CommonBaseFilterUtil.applySelectFilter(dql, "ci", EduProgramSubject.subjectIndex().programKind(), getSettings().<EduProgramKind>get("programKind"));

        orderDescriptionRegistry.applyOrder(dql, getItemDS().getEntityOrder());

        CommonBaseSearchListUtil.createPage(getItemDS(), dql);

        Map<EduProgramSubject, Set<String>> map = SafeMap.get(TreeSet.class);
        for (EduProgramSubjectQualification qual : IUniBaseDao.instance.get().getList(EduProgramSubjectQualification.class, EduProgramSubjectQualification.programSubject().id(), UniBaseDao.ids(getItemDS().getEntityList()))) {
            map.get(qual.getProgramSubject()).add(qual.getProgramQualification().getTitle());
        }

        for (ViewWrapper<EduProgramSubject> wrapper : ViewWrapper.<EduProgramSubject>getPatchedList(getItemDS())) {
            wrapper.setViewProperty(QUAL_TITLE_LIST, StringUtils.join(map.get(wrapper.getEntity()), "\n"));
        }

        setDeleteDisabledIds(ImmutableSet.<Long>of());
        setEditDisabledIds(ImmutableSet.<Long>of());
    }

    @Override
    public void onClickEditItem()
    {
        getActivationBuilder()
                .asRegion(EduProgramSubjectEdit.class)
                .parameter(IUIPresenter.PUBLISHER_ID, this.getListenerParameterAsLong())
                .activate();
    }
}