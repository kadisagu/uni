/* $Id:$ */
package ru.tandemservice.uniedu.catalog.entity.basic;

/**
 * @author oleyba
 * @since 8/17/12
 *
 * Для всяких настроек, у которых natural-id - это educationYear, и нужна модель выбора "учебного года"
 */
public interface ISelectableByEducationYear
{
    EducationYear getEducationYear();
}
