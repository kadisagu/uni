/* $Id$ */
package ru.tandemservice.uniedu.program.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

/**
 * @author Nikolay Fedorovskih
 * @since 11.02.2015
 */
public interface IEduProgramUniqueKeyUpdateDaemonDao
{
    public static final String GLOBAL_DAEMON_LOCK = IEduProgramUniqueKeyUpdateDaemonDao.class.getSimpleName() + ".lock";

    final SpringBeanCache<IEduProgramUniqueKeyUpdateDaemonDao> instance = new SpringBeanCache<>(IEduProgramUniqueKeyUpdateDaemonDao.class.getName());

    /**
     * Обновляет коды ОП.
     * {@see http://wiki.tandemservice.ru/pages/viewpage.action?pageId=23104523}
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void doUpdateEduProgramsUniqueKey();
}