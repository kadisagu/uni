package ru.tandemservice.uniedu.program.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditional;
import ru.tandemservice.uniedu.program.entity.EduProgramAdditionalProf;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Образовательная программа ДПО
 *
 * ДПО: на усмотрение ОУ, определяется названием.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramAdditionalProfGen extends EduProgramAdditional
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.program.entity.EduProgramAdditionalProf";
    public static final String ENTITY_NAME = "eduProgramAdditionalProf";
    public static final int VERSION_HASH = 1795987958;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramAdditionalProfGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramAdditionalProfGen> extends EduProgramAdditional.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramAdditionalProf.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramAdditionalProf();
        }
    }
    private static final Path<EduProgramAdditionalProf> _dslPath = new Path<EduProgramAdditionalProf>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramAdditionalProf");
    }
            

    public static class Path<E extends EduProgramAdditionalProf> extends EduProgramAdditional.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EduProgramAdditionalProf.class;
        }

        public String getEntityName()
        {
            return "eduProgramAdditionalProf";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
