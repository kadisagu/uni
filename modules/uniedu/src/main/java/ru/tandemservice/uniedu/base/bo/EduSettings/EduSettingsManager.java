/* $Id$ */
package ru.tandemservice.uniedu.base.bo.EduSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniedu.base.bo.EduSettings.logic.EduSettingsDao;
import ru.tandemservice.uniedu.base.bo.EduSettings.logic.IEduSettingsDao;

/**
 * @author Nikolay Fedorovskih
 * @since 30.04.2014
 */
@Configuration
public class EduSettingsManager extends BusinessObjectManager
{
    public static EduSettingsManager instance()
    {
        return instance(EduSettingsManager.class);
    }

    @Bean
    public IEduSettingsDao dao()
    {
        return new EduSettingsDao();
    }
}