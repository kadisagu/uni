package ru.tandemservice.uniedu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniedu_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность eduProgramSubjectIndex

		// созданы обязательные свойства eduLevelAndIndexNotation и eduProgramSubjectType
		{
			// создать колонку
			if(!tool.columnExists("edu_c_pr_subject_index_t", "edulevelandindexnotation_p"))
			{
				tool.createColumn("edu_c_pr_subject_index_t", new DBColumn("edulevelandindexnotation_p", DBType.createVarchar(255)));
			}

			if(!tool.columnExists("edu_c_pr_subject_index_t", "eduprogramsubjecttype_p"))
			{
				tool.createColumn("edu_c_pr_subject_index_t", new DBColumn("eduprogramsubjecttype_p", DBType.createVarchar(255)));
			}

			// задать значения
			//СПО специальности (ОКСО)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "СПО ГОС ОКСО", "специальность СПО", "2005.50");

			//ВПО бакалавриат (ОКСО)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВПО ГОС ОКСО", "направление бакалавров", "2005.62");

			//ВПО специалитет (ОКСО)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВПО ГОС ОКСО", "специальность ВПО", "2005.65");

			//ВПО магистратура (ОКСО)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВПО ГОС ОКСО", "направление магистров", "2005.68");

			//НПО профессии (2009)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "НПО ФГОС 2009", "профессия НПО", "2009.40");

			//СПО специальности (2009)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "СПО ФГОС 2009", "специальность СПО", "2009.50");

			//ВПО бакалавриат (2009)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВПО ФГОС 2009", "направление бакалавров", "2009.62");

			//ВПО специалитет (2009)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВПО ФГОС 2009", "специальность ВПО", "2009.65");

			//ВПО магистратура (2009)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВПО ФГОС 2009", "направление магистров", "2009.68");

			//СПО профессии (2013)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "СПО ФГОС 2013", "профессия СПО", "2013.01");

			//СПО специальности (2013)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "СПО ФГОС 2013", "специальность СПО", "2013.02");

			//ВО бакалавриат (2013)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВО ФГОС 2013", "направление бакалавров", "2013.03");

			//ВО магистратура (2013)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВО ФГОС 2013", "направление магистров", "2013.04");

			//ВО специалитет (2013)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВО ФГОС 2013", "специальность ВО", "2013.05");

			//ВО аспирантура (2013)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВО ФГОС 2013", "направление подготовки в аспирантуре", "2013.06");

			//ВО адъюнктура (2013)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВО ФГОС 2013", "направление подготовки в адъюнктуре", "2013.07");

			//ВО ординатура (2013)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВО ФГОС 2013", "направление подготовки в ординатуре", "2013.08");

			//ВО интернатура (2013)
			tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВО ФГОС 2013", "направление подготовки в интернатуре", "2013.10");

			// сделать колонки NOT NULL
			tool.setColumnNullable("edu_c_pr_subject_index_t", "edulevelandindexnotation_p", false);
			tool.setColumnNullable("edu_c_pr_subject_index_t", "eduprogramsubjecttype_p", false);
		}
    }
}