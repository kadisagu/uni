/**
 *$Id: EduInstitutionOrgUnitManager.java 32254 2014-02-04 09:18:39Z vdanilov $
 */
package ru.tandemservice.uniedu.base.bo.EducationYear;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.ISelectValueStyleSource;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.IViewSelectValueStyle;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.tapsupport.component.selection.ISelectValueStyle;

import ru.tandemservice.uniedu.base.bo.EducationYear.logic.EducationYearDao;
import ru.tandemservice.uniedu.base.bo.EducationYear.logic.IEducationYearDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.ISelectableByEducationYear;

/**
 */
@Configuration
public class EducationYearManager extends BusinessObjectManager
{
    public static final String DS_EDU_YEAR = "eduYearDS";
    public static final String CURRENT_YEAR_STYLE = "background-color:#fff6c2 !important;";

    public static EducationYearManager instance() {
        return instance(EducationYearManager.class);
    }

    @Bean
    public IEducationYearDao dao() { return new EducationYearDao(); }


    /**
     * @return текущий год (not-null)
     * @throws ApplicationException если текущего года нет
     */
    public EducationYear getCurrentRequired() throws ApplicationException
    {
        EducationYear year = dao().getCurrent();
        if (null != year) { return year; }
        throw new ApplicationException("В системе не настроен текущий учебный год.");
    }


    @Bean
    public UIDataSourceConfig eduYearDSConfig()
    {
        return SelectDSConfig.with(DS_EDU_YEAR, getName())
        .dataSourceClass(SelectDataSource.class)
        .handler(eduYearDSHandler())
        .valueStyleSource(getEduYearValueStyleSource())
        .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName());
    }

    public static ISelectValueStyleSource getEduYearValueStyleSource()
    {
        return new ISelectValueStyleSource()
        {
            @Override
            public IViewSelectValueStyle getSelectValueStyle(Object value)
            {
                final EducationYear eduYear = (value instanceof ISelectableByEducationYear ? ((ISelectableByEducationYear) value).getEducationYear() : null);
                return new ISelectValueStyle()
                {
                    @Override public boolean isDisabled() { return false; }
                    @Override public boolean isSelectable() { return true; }
                    @Override public boolean isRemovable() { return false; }
                    @Override public String getControlColumnStyle() { return null; }
                    @Override public String getColumnStyle(int columnIndex) { return null; }

                    @Override
                    public String getRowStyle() {
                        if (null == eduYear)
                            return null;
                        return Boolean.TRUE.equals(eduYear.getCurrent()) ? CURRENT_YEAR_STYLE : null;
                    }
                };
            }
        };
    }
}