/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.ui.List.logic.EduInstitutionOrgUnitListDSHandler;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;

/**
 * @author Alexander Zhebko
 * @since 03.02.2014
 */
@Configuration
public class EduInstitutionOrgUnitList extends BusinessComponentManager
{
    public static final String EDU_INSTITUTION_ORG_UNIT_LIST_DS = "eduInstitutionOrgUnitListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EDU_INSTITUTION_ORG_UNIT_LIST_DS, eduInstitutionOrgUnitListDSColumns(), eduInstitutionOrgUnitListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduInstitutionOrgUnitListDSColumns()
    {
        return columnListExtPointBuilder(EDU_INSTITUTION_ORG_UNIT_LIST_DS)
                .addColumn(publisherColumn("orgUnitTitle", EduInstitutionOrgUnit.orgUnit().fullTitle())
                        .businessComponent(OrgUnitView.class)
                        .primaryKeyPath(EduInstitutionOrgUnit.orgUnit().id().s())
                        .required(Boolean.TRUE)
                        .order())
		        .addColumn(textColumn("code", EduInstitutionOrgUnit.code()))
		        .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("eduInstitutionOrgUnitEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEduInstitutionOrgUnit")
                        .alert(new FormattedMessage("eduInstitutionOrgUnitListDS.eduInstitutionOrgUnitDeleteAlert", EduInstitutionOrgUnit.orgUnit().fullTitle().s())))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduInstitutionOrgUnitListDSHandler()
    {
        return new EduInstitutionOrgUnitListDSHandler(getName());
    }
}