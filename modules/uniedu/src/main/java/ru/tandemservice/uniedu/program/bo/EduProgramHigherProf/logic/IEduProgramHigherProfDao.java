/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * @author Alexander Zhebko
 * @since 07.02.2014
 */
public interface IEduProgramHigherProfDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет образовательную программу ВПО, используя указанную направленность; если указанная - null, то выбирает подходящую, соответствующую направлению подготовки.
     * @param eduProgram образовательная программа ВПО
     * @param eduProgramSpecialization направленность; null, если требуется указать соответствующую направлению
     */
    void saveOrUpdateEduProgramHigherProf(EduProgramHigherProf eduProgram, @Nullable EduProgramSpecialization eduProgramSpecialization);

    /** Создает копии образовательных программ ВО на следующий год. */
    Integer createNextYearCopy(@NotNull Collection<EduProgramHigherProf> eduPrograms);

    void updateParams(EduProgramHigherProf program, EduLevel baseLevel, EduProgramTrait eduProgramTrait, EduProgramDuration duration);
}