/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramSpecialization;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author oleyba
 * @since 11/27/13
 */
@Configuration
public class EduProgramSpecializationManager extends BusinessObjectManager
{
    public static EduProgramSpecializationManager instance()
    {
        return instance(EduProgramSpecializationManager.class);
    }
}

