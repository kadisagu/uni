/* $Id$ */
package ru.tandemservice.uniedu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Ekaterina Zvereva
 * @since 24.01.2017
 */
public class MS_uniedu_2x11x0_5to6 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.0")
                };
    }
    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("update accessmatrix_t set permissionkey_p='menuEduProgramSubjectComparisonList' where permissionkey_p='menuDipEduProgramSubjectComparisonList'");
    }
}