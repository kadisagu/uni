/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.program.bo.EduProgram.ui.ChangeParams.EduProgramChangeParams;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.AddEdit.EduProgramHigherProfAddEdit;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;

/**
 * @author Alexander Zhebko
 * @since 06.02.2014
 */
@State({
        @Bind(key = "selectedTab"),
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id", required = true)
})
public class EduProgramHigherProfPubUI extends UIPresenter
{
    private EntityHolder<EduProgramHigherProf> _holder = new EntityHolder<>();
    public EntityHolder<EduProgramHigherProf> getHolder(){ return _holder; }
    public EduProgramHigherProf getEduProgram(){ return _holder.getValue(); }
    private String _selectedTab;

    // TODO: вырезать это, когда будут сделаны нормальные параметры обучения студентов
    @Zlo("решение временное, поэтому хардкодим")
    private static final boolean eduOuExists = (null != EntityRuntime.getMeta("educationOrgUnit"));
    public boolean isShowStudentParamBlock() {
        return eduOuExists && getEduProgram().getKind().isProgramHigherProf();
    }


    @Override
    public void onComponentRefresh()
    {
        _holder.refresh();
    }

    public void onClickEditEduProgram()
    {
        _uiActivation.asRegionDialog(EduProgramHigherProfAddEdit.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getEduProgram().getId()).activate();
    }

    public void onClickEditEduProgramData()
    {
        _uiActivation.asDesktopRoot(EduProgramChangeParams.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getEduProgram().getId()).activate();
    }

    public void onClickDeleteEduProgram()
    {
        IUniBaseDao.instance.get().delete(getHolder().getValue());
        deactivate();
    }

    // getters and setters

    public String getSelectedTab()
    {
        return _selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        _selectedTab = selectedTab;
    }
}