/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.logic.EduProgramHigherProfDao;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.logic.IEduProgramHigherProfDao;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.02.2014
 */
@Configuration
public class EduProgramHigherProfManager extends BusinessObjectManager
{
    public static final String EDU_PROGRAM_KIND = "eduProgramKind";
    public static final String EDU_PROGRAM_KIND_HIGHER_PROF_DS = "eduProgramKindHigherProfDS";

    public static EduProgramHigherProfManager instance()
    {
        return instance(EduProgramHigherProfManager.class);
    }

    @Bean
    public IEduProgramHigherProfDao dao()
    {
        return new EduProgramHigherProfDao();
    }

    @Bean
    public IDefaultComboDataSourceHandler baseLevelDS()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EduLevel.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EduProgramKind kind = context.get(EDU_PROGRAM_KIND);

                if (kind == null) {
                    dql.where(nothing());
                    return;
                }

                if (kind.isBS()) {
                    // Для бак/спец - СОО + все подвиды профессионального
                    dql.where(or(
                            eq(property(alias, EduLevel.code()), value(EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE)),
                            eq(property(alias, EduLevel.parent().code()), value(EduLevelCodes.PROFESSIONALNOE_OBRAZOVANIE))
                    ));
                }
                else {
                    // Остальным предопределено
                    final String levelCode;
                    switch (kind.getCode())
                    {
                        case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY:
                            levelCode = EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT;
                            break;
                        case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_:
                        case EduProgramKindCodes.PROGRAMMA_ORDINATURY:
                        case EduProgramKindCodes.PROGRAMMA_INTERNATURY:
                            levelCode = EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA;
                            break;
                        default:
                            dql.where(nothing());
                            return;
                    }
                    dql.where(eq(property(alias, EduLevel.code()), value(levelCode)));
                }
            }
        };

        handler.order(EduLevel.code());
        handler.filter(EduLevel.title());
        return handler;
    }

    @Bean
    public UIDataSourceConfig programKindHigherProfDSConfig()
    {
        return CommonBaseStaticSelectDataSource.selectDS(EDU_PROGRAM_KIND_HIGHER_PROF_DS,getName(),
                EduProgramKind.defaultSelectDSHandler(getName())
                        .where(EduProgramKind.programHigherProf(), true))
                .create();
    }
}