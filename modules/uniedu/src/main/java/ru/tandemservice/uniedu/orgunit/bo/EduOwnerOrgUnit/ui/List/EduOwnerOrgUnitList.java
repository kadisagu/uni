/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.shared.organization.base.bo.OrgUnit.ui.View.OrgUnitView;
import ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.ui.List.logic.EduOwnerOrgUnitListDSHandler;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;

/**
 * @author Alexander Zhebko
 * @since 04.02.2014
 */
@Configuration
public class EduOwnerOrgUnitList extends BusinessComponentManager
{
    public static final String EDU_OWNER_ORG_UNIT_LIST_DS = "eduOwnerOrgUnitListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EDU_OWNER_ORG_UNIT_LIST_DS, eduOwnerOrgUnitListDSColumns(), eduOwnerOrgUnitListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduOwnerOrgUnitListDSColumns()
    {
        return columnListExtPointBuilder(EDU_OWNER_ORG_UNIT_LIST_DS)
                .addColumn(publisherColumn("orgUnitTitle", EduOwnerOrgUnit.orgUnit().fullTitle())
                        .businessComponent(OrgUnitView.class)
                        .primaryKeyPath(EduOwnerOrgUnit.orgUnit().id().s())
                        .required(Boolean.TRUE)
                        .order())
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDeleteEduOwnerOrgUnit")
                    .alert(new FormattedMessage("eduOwnerOrgUnitListDS.orgUnitDeleteAlert", EduInstitutionOrgUnit.orgUnit().fullTitle().s())))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduOwnerOrgUnitListDSHandler()
    {
        return new EduOwnerOrgUnitListDSHandler(getName());
    }
}