/* $Id:$ */
package ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniedu.entity.EduProgramSubjectComparison;
import ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison.EduProgramSubjectComparisonManager;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;

import static ru.tandemservice.uniedu.settings.bo.EduProgramSubjectComparison.ui.List.EduProgramSubjectComparisonList.SUBJECT_LIST_DS;

/**
 * @author rsizonenko
 * @since 13.03.2015
 */
public class EduProgramSubjectComparisonListUI extends UIPresenter
{
    public static final String PARAM_OLD_INDEX = "oldIndex";
    public static final String PARAM_NEW_INDEX = "newIndex";
    public static final String PARAM_EDU_DOCUMENT_BLANK = "eduDocumentBlank";
    public static final String PARAM_TITLE = "title";

    private SubjectComparisonWrapper _currentEditSubject;

    @Override
    public void onComponentRefresh()
    {
        _currentEditSubject = null;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(PARAM_OLD_INDEX, getSettings().get(PARAM_OLD_INDEX));
        dataSource.put(PARAM_NEW_INDEX, getSettings().get(PARAM_NEW_INDEX));
        dataSource.put(PARAM_EDU_DOCUMENT_BLANK, getSettings().get(PARAM_EDU_DOCUMENT_BLANK));
        dataSource.put(PARAM_TITLE, getSettings().get(PARAM_TITLE));
    }

    // util

    public boolean isShowList()
    {
        return null != getSettings().get(PARAM_OLD_INDEX) && null != getSettings().get(PARAM_NEW_INDEX);
    }

    public boolean isInEditMode()
    {
        return null != getCurrentEditSubject();
    }

    // listeners

    public void onClickEdit()
    {
        final SubjectComparisonWrapper dataSourceSelectedValue = getConfig().getDataSource(SUBJECT_LIST_DS).getRecordById(getListenerParameterAsLong());
        setCurrentEditSubject(dataSourceSelectedValue);

        if (null == _currentEditSubject.getComparison())
            _currentEditSubject.setComparison(new EduProgramSubjectComparison());
    }

    public void onClickSaveChanges()
    {
        EduProgramSubjectComparison comparison = _currentEditSubject.getComparison();

        if (null != comparison.getNewProgramSubject())
        {
            if (null == comparison.getOldProgramSubject())
                comparison.setOldProgramSubject(_currentEditSubject.getSubject());
            DataAccessServices.dao().saveOrUpdate(comparison);
        }
        else
            DataAccessServices.dao().delete(comparison);

        setCurrentEditSubject(null);
    }

    public void onClickSearch()
    {
        getSettings().save();
    }

    public void onClickClear()
    {
        getSettings().set(PARAM_TITLE, null);
        onClickSearch();
    }

    // getters setters

    public SubjectComparisonWrapper getSubject()
    {
        return getSupport().getDataSourceCurrentValue(SUBJECT_LIST_DS);
    }

    public void onClickFillByNamesOverlap()
    {
        EduProgramSubjectComparisonManager.instance().dao().fillComparisonsByNameOverlap((EduProgramSubjectIndex) getSettings().get(PARAM_OLD_INDEX), (EduProgramSubjectIndex) getSettings().get(PARAM_NEW_INDEX));
    }

    public SubjectComparisonWrapper getCurrentEditSubject()
    {
        return _currentEditSubject;
    }

    public void setCurrentEditSubject(SubjectComparisonWrapper currentEditSubject)
    {
        _currentEditSubject = currentEditSubject;
    }

    public SubjectComparisonWrapper getCurrentSubject()
    {
        final SubjectComparisonWrapper dataSourceCurrentValue = getSupport().getDataSourceCurrentValue(SUBJECT_LIST_DS);
        return dataSourceCurrentValue;
    }

    public boolean isSubjectInEditMode()
    {
        return null != getCurrentEditSubject() && getCurrentSubject().getId().equals(getCurrentEditSubject().getId());
    }
}