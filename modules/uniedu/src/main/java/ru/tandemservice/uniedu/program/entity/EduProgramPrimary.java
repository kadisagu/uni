package ru.tandemservice.uniedu.program.entity;

import ru.tandemservice.uniedu.program.entity.gen.*;

/**
 * Основная образовательная программа
 *
 * Основное образование (все, что не является дополнительным).
 */
public abstract class EduProgramPrimary extends EduProgramPrimaryGen
{
}