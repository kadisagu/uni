package ru.tandemservice.uniedu.catalog.entity.basic;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EduProgramOrientationGen;

/** @see ru.tandemservice.uniedu.catalog.entity.basic.gen.EduProgramOrientationGen */
public class EduProgramOrientation extends EduProgramOrientationGen implements IDynamicCatalogItem
{
    public static final String DS_ORIENTATION = "orientationDS";

    /**
     * Источник данных по умолчанию для селектов выбора ориентации ОП.
     * В контексте всегда должен быть перечень ОП.
     */
    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId) {
        return new EntityComboDataSourceHandler(ownerId, EduProgramOrientation.class)
                .titleProperty(EduProgramOrientation.title().s())
                .filter(EduProgramOrientation.title())
                .where(EduProgramOrientation.subjectIndex(), EduProgramSubjectIndexManager.PARAM_SUBJECT_INDEX)
                .order(EduProgramOrientation.title());
    }
}