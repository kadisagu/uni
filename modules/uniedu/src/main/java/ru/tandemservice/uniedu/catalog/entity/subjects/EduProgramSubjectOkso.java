package ru.tandemservice.uniedu.catalog.entity.subjects;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSubjectOksoGen;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;

/**
 * Направление подготовки профессионального образования (ОКСО)
 *
 * Направление классификаторов ОКСО: ОКСО-подобная структура, иерархия общая для всех типов ОП. Перечень УГС глобальный и общий для ОКСО и 2009.
 * Направление подготовки в системе ссылается на элемент общего классификатора направлений (специальностей, профессий).
 * http://wiki.tandemservice.ru/pages/viewpage.action?pageId=17465605
 */
public class EduProgramSubjectOkso extends EduProgramSubjectOksoGen
{
    @Override
    public String getGroupTitle()
    {
        return getItem().getGroup().getTitle();
    }

    @Override
    public String getTitleWithCodeOksoWithoutSpec() {
        return StringUtils.substringBefore(getSubjectCode(), ".") + " " + getTitle();
    }

    @Override
    public boolean isLabor()
    {
        return false;
    }

    @Override public boolean isOKSO() { return true; }
    @Override public boolean is2009() { return false; }
    @Override public boolean is2013() { return false; }

    @Override
    public IPersistentAccreditationOwner getSubjectGroup()
    {
        return getItem().getGroup();
    }
}