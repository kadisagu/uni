package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направленность, соответствующая направлению
 *
 * Соответствует самому направлению подготовки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSpecializationRootGen extends EduProgramSpecialization
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationRoot";
    public static final String ENTITY_NAME = "eduProgramSpecializationRoot";
    public static final int VERSION_HASH = 994526757;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramSpecializationRootGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSpecializationRootGen> extends EduProgramSpecialization.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSpecializationRoot.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSpecializationRoot();
        }
    }
    private static final Path<EduProgramSpecializationRoot> _dslPath = new Path<EduProgramSpecializationRoot>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSpecializationRoot");
    }
            

    public static class Path<E extends EduProgramSpecializationRoot> extends EduProgramSpecialization.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EduProgramSpecializationRoot.class;
        }

        public String getEntityName()
        {
            return "eduProgramSpecializationRoot";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
