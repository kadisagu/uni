package ru.tandemservice.uniedu.catalog.entity.basic.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ориентация образовательной программы
 *
 * Ориентация ОП. Например, прикладной/академический бакалавр.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramOrientationGen extends EntityBase
 implements INaturalIdentifiable<EduProgramOrientationGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation";
    public static final String ENTITY_NAME = "eduProgramOrientation";
    public static final int VERSION_HASH = -287289242;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_SHORT_TITLE = "shortTitle";
    public static final String P_ABBREVIATION = "abbreviation";
    public static final String P_PRINT_TITLE = "printTitle";
    public static final String L_SUBJECT_INDEX = "subjectIndex";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _shortTitle;     // Сокращенное название
    private String _abbreviation;     // Аббревиатура
    private String _printTitle;     // Печатное название
    private EduProgramSubjectIndex _subjectIndex;     // Перечень
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getShortTitle()
    {
        return _shortTitle;
    }

    /**
     * @param shortTitle Сокращенное название. Свойство не может быть null.
     */
    public void setShortTitle(String shortTitle)
    {
        dirty(_shortTitle, shortTitle);
        _shortTitle = shortTitle;
    }

    /**
     * @return Аббревиатура. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAbbreviation()
    {
        return _abbreviation;
    }

    /**
     * @param abbreviation Аббревиатура. Свойство не может быть null.
     */
    public void setAbbreviation(String abbreviation)
    {
        dirty(_abbreviation, abbreviation);
        _abbreviation = abbreviation;
    }

    /**
     * @return Печатное название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPrintTitle()
    {
        return _printTitle;
    }

    /**
     * @param printTitle Печатное название. Свойство не может быть null.
     */
    public void setPrintTitle(String printTitle)
    {
        dirty(_printTitle, printTitle);
        _printTitle = printTitle;
    }

    /**
     * @return Перечень. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectIndex getSubjectIndex()
    {
        return _subjectIndex;
    }

    /**
     * @param subjectIndex Перечень. Свойство не может быть null.
     */
    public void setSubjectIndex(EduProgramSubjectIndex subjectIndex)
    {
        dirty(_subjectIndex, subjectIndex);
        _subjectIndex = subjectIndex;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramOrientationGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((EduProgramOrientation)another).getCode());
            }
            setShortTitle(((EduProgramOrientation)another).getShortTitle());
            setAbbreviation(((EduProgramOrientation)another).getAbbreviation());
            setPrintTitle(((EduProgramOrientation)another).getPrintTitle());
            setSubjectIndex(((EduProgramOrientation)another).getSubjectIndex());
            setTitle(((EduProgramOrientation)another).getTitle());
        }
    }

    public INaturalId<EduProgramOrientationGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramOrientationGen>
    {
        private static final String PROXY_NAME = "EduProgramOrientationNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramOrientationGen.NaturalId) ) return false;

            EduProgramOrientationGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramOrientationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramOrientation.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramOrientation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "shortTitle":
                    return obj.getShortTitle();
                case "abbreviation":
                    return obj.getAbbreviation();
                case "printTitle":
                    return obj.getPrintTitle();
                case "subjectIndex":
                    return obj.getSubjectIndex();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "shortTitle":
                    obj.setShortTitle((String) value);
                    return;
                case "abbreviation":
                    obj.setAbbreviation((String) value);
                    return;
                case "printTitle":
                    obj.setPrintTitle((String) value);
                    return;
                case "subjectIndex":
                    obj.setSubjectIndex((EduProgramSubjectIndex) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "shortTitle":
                        return true;
                case "abbreviation":
                        return true;
                case "printTitle":
                        return true;
                case "subjectIndex":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "shortTitle":
                    return true;
                case "abbreviation":
                    return true;
                case "printTitle":
                    return true;
                case "subjectIndex":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "shortTitle":
                    return String.class;
                case "abbreviation":
                    return String.class;
                case "printTitle":
                    return String.class;
                case "subjectIndex":
                    return EduProgramSubjectIndex.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramOrientation> _dslPath = new Path<EduProgramOrientation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramOrientation");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getShortTitle()
     */
    public static PropertyPath<String> shortTitle()
    {
        return _dslPath.shortTitle();
    }

    /**
     * @return Аббревиатура. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getAbbreviation()
     */
    public static PropertyPath<String> abbreviation()
    {
        return _dslPath.abbreviation();
    }

    /**
     * @return Печатное название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getPrintTitle()
     */
    public static PropertyPath<String> printTitle()
    {
        return _dslPath.printTitle();
    }

    /**
     * @return Перечень. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getSubjectIndex()
     */
    public static EduProgramSubjectIndex.Path<EduProgramSubjectIndex> subjectIndex()
    {
        return _dslPath.subjectIndex();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EduProgramOrientation> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _shortTitle;
        private PropertyPath<String> _abbreviation;
        private PropertyPath<String> _printTitle;
        private EduProgramSubjectIndex.Path<EduProgramSubjectIndex> _subjectIndex;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(EduProgramOrientationGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Сокращенное название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getShortTitle()
     */
        public PropertyPath<String> shortTitle()
        {
            if(_shortTitle == null )
                _shortTitle = new PropertyPath<String>(EduProgramOrientationGen.P_SHORT_TITLE, this);
            return _shortTitle;
        }

    /**
     * @return Аббревиатура. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getAbbreviation()
     */
        public PropertyPath<String> abbreviation()
        {
            if(_abbreviation == null )
                _abbreviation = new PropertyPath<String>(EduProgramOrientationGen.P_ABBREVIATION, this);
            return _abbreviation;
        }

    /**
     * @return Печатное название. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getPrintTitle()
     */
        public PropertyPath<String> printTitle()
        {
            if(_printTitle == null )
                _printTitle = new PropertyPath<String>(EduProgramOrientationGen.P_PRINT_TITLE, this);
            return _printTitle;
        }

    /**
     * @return Перечень. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getSubjectIndex()
     */
        public EduProgramSubjectIndex.Path<EduProgramSubjectIndex> subjectIndex()
        {
            if(_subjectIndex == null )
                _subjectIndex = new EduProgramSubjectIndex.Path<EduProgramSubjectIndex>(L_SUBJECT_INDEX, this);
            return _subjectIndex;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EduProgramOrientationGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EduProgramOrientation.class;
        }

        public String getEntityName()
        {
            return "eduProgramOrientation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
