package ru.tandemservice.uniedu.orgunit.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выпускающее подразделение
 *
 * Подразделение, ответственное за формирование образовательных программ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduOwnerOrgUnitGen extends EntityBase
 implements INaturalIdentifiable<EduOwnerOrgUnitGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit";
    public static final String ENTITY_NAME = "eduOwnerOrgUnit";
    public static final int VERSION_HASH = -1891393474;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_TITLE = "title";

    private OrgUnit _orgUnit;     // Подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduOwnerOrgUnitGen)
        {
            if (withNaturalIdProperties)
            {
                setOrgUnit(((EduOwnerOrgUnit)another).getOrgUnit());
            }
        }
    }

    public INaturalId<EduOwnerOrgUnitGen> getNaturalId()
    {
        return new NaturalId(getOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<EduOwnerOrgUnitGen>
    {
        private static final String PROXY_NAME = "EduOwnerOrgUnitNaturalProxy";

        private Long _orgUnit;

        public NaturalId()
        {}

        public NaturalId(OrgUnit orgUnit)
        {
            _orgUnit = ((IEntity) orgUnit).getId();
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduOwnerOrgUnitGen.NaturalId) ) return false;

            EduOwnerOrgUnitGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduOwnerOrgUnitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduOwnerOrgUnit.class;
        }

        public T newInstance()
        {
            return (T) new EduOwnerOrgUnit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduOwnerOrgUnit> _dslPath = new Path<EduOwnerOrgUnit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduOwnerOrgUnit");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends EduOwnerOrgUnit> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(EduOwnerOrgUnitGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return EduOwnerOrgUnit.class;
        }

        public String getEntityName()
        {
            return "eduOwnerOrgUnit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
