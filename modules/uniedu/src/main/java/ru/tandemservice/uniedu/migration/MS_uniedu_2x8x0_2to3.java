/* $Id$ */
package ru.tandemservice.uniedu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Nikolay Fedorovskih
 * @since 01.04.2015
 */
@SuppressWarnings({"unused"})
public class MS_uniedu_2x8x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("update edu_c_pr_subject_index_t set code_p=? where code_p=?", "2013.10", "2013.09");
    }
}