package ru.tandemservice.uniedu.catalog.entity.subjects.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Квалификация направления подготовки
 *
 * Связывает квалификацию и направление подготовки.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramSubjectQualificationGen extends EntityBase
 implements INaturalIdentifiable<EduProgramSubjectQualificationGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification";
    public static final String ENTITY_NAME = "eduProgramSubjectQualification";
    public static final int VERSION_HASH = -879609741;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROGRAM_SUBJECT = "programSubject";
    public static final String L_PROGRAM_QUALIFICATION = "programQualification";

    private EduProgramSubject _programSubject;     // Направление подготовки
    private EduProgramQualification _programQualification;     // Квалификация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubject getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление подготовки. Свойство не может быть null.
     */
    public void setProgramSubject(EduProgramSubject programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     */
    @NotNull
    public EduProgramQualification getProgramQualification()
    {
        return _programQualification;
    }

    /**
     * @param programQualification Квалификация. Свойство не может быть null.
     */
    public void setProgramQualification(EduProgramQualification programQualification)
    {
        dirty(_programQualification, programQualification);
        _programQualification = programQualification;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EduProgramSubjectQualificationGen)
        {
            if (withNaturalIdProperties)
            {
                setProgramSubject(((EduProgramSubjectQualification)another).getProgramSubject());
                setProgramQualification(((EduProgramSubjectQualification)another).getProgramQualification());
            }
        }
    }

    public INaturalId<EduProgramSubjectQualificationGen> getNaturalId()
    {
        return new NaturalId(getProgramSubject(), getProgramQualification());
    }

    public static class NaturalId extends NaturalIdBase<EduProgramSubjectQualificationGen>
    {
        private static final String PROXY_NAME = "EduProgramSubjectQualificationNaturalProxy";

        private Long _programSubject;
        private Long _programQualification;

        public NaturalId()
        {}

        public NaturalId(EduProgramSubject programSubject, EduProgramQualification programQualification)
        {
            _programSubject = ((IEntity) programSubject).getId();
            _programQualification = ((IEntity) programQualification).getId();
        }

        public Long getProgramSubject()
        {
            return _programSubject;
        }

        public void setProgramSubject(Long programSubject)
        {
            _programSubject = programSubject;
        }

        public Long getProgramQualification()
        {
            return _programQualification;
        }

        public void setProgramQualification(Long programQualification)
        {
            _programQualification = programQualification;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EduProgramSubjectQualificationGen.NaturalId) ) return false;

            EduProgramSubjectQualificationGen.NaturalId that = (NaturalId) o;

            if( !equals(getProgramSubject(), that.getProgramSubject()) ) return false;
            if( !equals(getProgramQualification(), that.getProgramQualification()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProgramSubject());
            result = hashCode(result, getProgramQualification());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProgramSubject());
            sb.append("/");
            sb.append(getProgramQualification());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramSubjectQualificationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramSubjectQualification.class;
        }

        public T newInstance()
        {
            return (T) new EduProgramSubjectQualification();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "programSubject":
                    return obj.getProgramSubject();
                case "programQualification":
                    return obj.getProgramQualification();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((EduProgramSubject) value);
                    return;
                case "programQualification":
                    obj.setProgramQualification((EduProgramQualification) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "programSubject":
                        return true;
                case "programQualification":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "programSubject":
                    return true;
                case "programQualification":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "programSubject":
                    return EduProgramSubject.class;
                case "programQualification":
                    return EduProgramQualification.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EduProgramSubjectQualification> _dslPath = new Path<EduProgramSubjectQualification>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramSubjectQualification");
    }
            

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification#getProgramSubject()
     */
    public static EduProgramSubject.Path<EduProgramSubject> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification#getProgramQualification()
     */
    public static EduProgramQualification.Path<EduProgramQualification> programQualification()
    {
        return _dslPath.programQualification();
    }

    public static class Path<E extends EduProgramSubjectQualification> extends EntityPath<E>
    {
        private EduProgramSubject.Path<EduProgramSubject> _programSubject;
        private EduProgramQualification.Path<EduProgramQualification> _programQualification;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification#getProgramSubject()
     */
        public EduProgramSubject.Path<EduProgramSubject> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new EduProgramSubject.Path<EduProgramSubject>(L_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectQualification#getProgramQualification()
     */
        public EduProgramQualification.Path<EduProgramQualification> programQualification()
        {
            if(_programQualification == null )
                _programQualification = new EduProgramQualification.Path<EduProgramQualification>(L_PROGRAM_QUALIFICATION, this);
            return _programQualification;
        }

        public Class getEntityClass()
        {
            return EduProgramSubjectQualification.class;
        }

        public String getEntityName()
        {
            return "eduProgramSubjectQualification";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
