/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.EduProgramSecondaryProfManager;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.ui.Pub.EduProgramSecondaryProfPub;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 10.02.2014
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "holder.id"))
public class EduProgramSecondaryProfAddEditUI extends UIPresenter
{
    private EntityHolder<EduProgramSecondaryProf> _holder = new EntityHolder<>();
    private EduProgramSubjectIndex _eduProgramSubjectIndex;
    private List<EduProgramQualification> _eduProgramQualifications;

    @Override
    public void onComponentRefresh()
    {
        getHolder().refresh();
        if (getHolder().getValue() == null) {
            getHolder().setValue(new EduProgramSecondaryProf());
        }

        if (isEditForm()) {
            setEduProgramSubjectIndex(getEduProgram().getProgramSubject().getSubjectIndex());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EduProgramSecondaryProfAddEdit.EDU_PROGRAM_KIND, getEduProgram().getKind());
        dataSource.put(EduProgramSecondaryProfAddEdit.EDU_PROGRAM_SUBJECT_INDEX, getEduProgramSubjectIndex());
        dataSource.put(EduProgramSecondaryProfAddEdit.EDU_PROGRAM_SUBJECT, getEduProgram().getProgramSubject());
        dataSource.put(EduInstitutionOrgUnit.PARAM_EDIT_FORM, isEditForm());
    }

    public void onClickApply()
    {
        EduProgramSecondaryProfManager.instance().dao().saveOrUpdateEduProgramSecondaryProf(getEduProgram());
        deactivate();
        if (isAddForm())
            _uiActivation.asDesktopRoot(EduProgramSecondaryProfPub.class).parameter(PublisherActivator.PUBLISHER_ID_KEY, getEduProgram().getId()).activate();
    }

    public boolean isAddForm(){ return getHolder().getId() == null; }
    public boolean isEditForm(){ return !isAddForm(); }


    // getters and setters

    public EntityHolder<EduProgramSecondaryProf> getHolder(){ return _holder; }
    public EduProgramSecondaryProf getEduProgram(){ return getHolder().getValue(); }

    public EduProgramSubjectIndex getEduProgramSubjectIndex(){ return _eduProgramSubjectIndex; }
    public void setEduProgramSubjectIndex(EduProgramSubjectIndex eduProgramSubjectIndex){ _eduProgramSubjectIndex = eduProgramSubjectIndex; }

    public List<EduProgramQualification> getEduProgramQualifications(){ return _eduProgramQualifications; }
    public void setEduProgramQualifications(List<EduProgramQualification> eduProgramQualifications){ _eduProgramQualifications = eduProgramQualifications; }

	public OrgUnit getOrgUnit()
	{
		EduOwnerOrgUnit eduOwnerOrgUnit = getEduProgram().getOwnerOrgUnit();
		return eduOwnerOrgUnit == null ? null : eduOwnerOrgUnit.getOrgUnit();
	}

	public void setOrgUnit(OrgUnit orgUnit)
	{
		EduOwnerOrgUnit eduOwnerOrgUnit = (orgUnit == null) ? null : DataAccessServices.dao().getByNaturalId(new EduOwnerOrgUnit.NaturalId(orgUnit));
		getEduProgram().setOwnerOrgUnit(eduOwnerOrgUnit);
	}
}