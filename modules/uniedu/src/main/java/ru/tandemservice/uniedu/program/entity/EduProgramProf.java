package ru.tandemservice.uniedu.program.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.commonbase.base.util.Wiki;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uniedu.program.entity.gen.EduProgramProfGen;

/**
 * Основная профессиональная образовательная программа
 *
 * Профессиональное образование: реализуется по направлениям подготовки (специальностям, профессиям).
 */
public abstract class EduProgramProf extends EduProgramProfGen
{
    @Override
    protected StringBuilder uniqueKeyBuilder() {
        // При изменении ключа не забыть исправить ProgramKeyUniqueHandler
        return super.uniqueKeyBuilder()
        .append(this.getBaseLevel().getCode()).append("/")
        .append(this.getProgramSubject().getCode()).append("/");
    }

    @Override
    protected StringBuilder titleBuilder() {
        StringBuilder builder = super.titleBuilder()
        .append(this.getProgramSubject().getSubjectCode()).append(" ")
        .append(this.getProgramSubjectTitlePart()).append(", ")
        .append(this.getForm().getShortTitle());

        if (this.getEduProgramTrait() != null) {
            builder.append(", ").append(this.getEduProgramTrait().getShortTitle());
        }

        if (this.getKind().isProgramBachelorDegree() || this.getKind().isProgramSpecialistDegree()) {
            if (!EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE.equals(this.getBaseLevel().getCode())) {
                builder.append(", на базе ").append(StringUtils.uncapitalize(this.getBaseLevel().getTitle()));
            }
        }


        return builder.append(" ").append(this.getDuration().getTitle());
    }

    /** @return назване направления в названии ОП (для ВПО - это название направленности) */
    protected String getProgramSubjectTitlePart() {
        return this.getProgramSubject().getTitle();
    }


    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 1</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    public String getTitle(){ return super.getTitle(); }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 2</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    public String getShortTitle(){ return super.getShortTitle(); }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 3</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getImplConditionsWithForm(){ return getImplConditions(true, false); }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 4</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getImplConditionsShortWithForm(){ return getImplConditions(true, true); }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 5</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getImplConditions(){ return getImplConditions(false, false); }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 6</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getImplConditionsShort(){ return getImplConditions(false, true); }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 7</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getTitleWithCodeAndConditions(){ return getSubjectCode() + " " + this.getTitle() + " (" + getImplConditions() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 8</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getTitleWithCodeAndConditionsShort(){ return getSubjectCode() + " " + this.getTitle() + " (" + getImplConditionsShort() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 9</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getShortTitleWithCodeAndConditionsShort(){ return getSubjectCode() + " " + getShortTitle() + " (" + getImplConditionsShort() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 10</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getTitleAndConditions(){ return this.getTitle() + " (" + getImplConditions() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 11</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getTitleAndConditionsShort(){ return this.getTitle() + " (" + getImplConditionsShort() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 12</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getShortTitleAndConditionsShort(){ return getShortTitle() + " (" + getImplConditionsShort() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 13</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getTitleWithCodeAndConditionsWithForm(){ return getSubjectCode() + " " + this.getTitle() + " (" + getImplConditionsWithForm() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 14</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getTitleWithCodeAndConditionsShortWithForm(){ return getSubjectCode() + " " + this.getTitle() + " (" + getImplConditionsShortWithForm() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 15</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getShortTitleWithCodeAndConditionsShortWithForm(){ return getSubjectCode() + " " + getShortTitle() + " (" + getImplConditionsShortWithForm() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 16</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getTitleAndConditionsWithForm(){ return this.getTitle() + " (" + getImplConditionsWithForm() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 17</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getTitleAndConditionsShortWithForm(){ return this.getTitle() + " (" + getImplConditionsShortWithForm() + ")"; }

    /** <a href="http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862">Вариант 18</a> */
    @Override
    @Wiki(url = "http://wiki.tandemservice.ru/pages/viewpage.action?pageId=20677862")
    @EntityDSLSupport
    public String getShortTitleAndConditionsShortWithForm(){ return getShortTitle() + " (" + getImplConditionsShortWithForm() + ")"; }

    private String getImplConditions(boolean withForm, boolean shortVersion)
    {
        final StringBuilder result = new StringBuilder(24);
        final String orientationShortTitle = getOrientationShortTitle();
        if (StringUtils.isNotEmpty(orientationShortTitle)) {
            result.append(orientationShortTitle).append(", ");
        }
        if (withForm) {
            result.append(getForm().getTitle()).append(shortVersion ? ", " : " форма, ");
        }

        result.append(getSecondaryInDepthStudyString());
        result.append(getDuration().getTitle());
        result.append(", на базе ").append(getBaseLevel().getShortTitle());

        if (getEduProgramTrait() != null) {
            result.append(", ").append(shortVersion ? getEduProgramTrait().getShortTitle() : getEduProgramTrait().getTitle());
        }

        result.append(" | ");

        if (!getInstitutionOrgUnit().getOrgUnit().isTop()) {
            result.append(getInstitutionOrgUnit().getOrgUnit().getShortTitleWithTopEmphasized()).append(", ");
        }
        result.append(getOwnerOrgUnit().getOrgUnit().getShortTitle());
        return result.toString();
    }

    protected String getOrientationShortTitle(){ return null; }
    protected String getSecondaryInDepthStudyString(){ return ""; }

    private String getSubjectCode(){ return this.getProgramSubject().getSubjectCode(); }
}