package ru.tandemservice.uniedu.catalog.entity.subjects;

import org.tandemframework.core.common.IEntityDebugTitled;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSubjectQualificationGen;

/**
 * Квалификация направления подготовки
 *
 * Связывает квалификацию и направление подготовки.
 */
public class EduProgramSubjectQualification extends EduProgramSubjectQualificationGen implements IEntityDebugTitled
{
    @Override
    public String getEntityDebugTitle() {
        final EduProgramSubject subject = getProgramSubject();
        final EduProgramQualification qualification = getProgramQualification();
        if (qualification == null || subject == null) {
            return this.getClass().getSimpleName();
        }
        return qualification.getTitle() + " : " + subject.getTitleWithCode();
    }
}