/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.EduOwnerOrgUnitManager;

/**
 * @author Alexander Zhebko
 * @since 04.02.2014
 */
public class EduOwnerOrgUnitAddUI extends UIPresenter
{
    private OrgUnit _orgUnit;
    public OrgUnit getOrgUnit(){ return _orgUnit; }
    public void setOrgUnit(OrgUnit orgUnit){ _orgUnit = orgUnit; }

    public void onClickApply()
    {
        EduOwnerOrgUnitManager.instance().dao().doGetEduOwnerOrgUnit(getOrgUnit());
        deactivate();
    }
}