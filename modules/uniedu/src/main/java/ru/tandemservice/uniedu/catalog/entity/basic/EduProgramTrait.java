package ru.tandemservice.uniedu.catalog.entity.basic;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EduProgramTraitGen;

/**
 * Особенность реализации образовательной программы
 */
public class EduProgramTrait extends EduProgramTraitGen implements IDynamicCatalogItem
{
}