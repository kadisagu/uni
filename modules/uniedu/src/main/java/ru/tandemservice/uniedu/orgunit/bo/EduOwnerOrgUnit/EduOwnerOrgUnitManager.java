/**
 *$Id$
 */
package ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

import ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.logic.EduOwnerOrgUnitDao;
import ru.tandemservice.uniedu.orgunit.bo.EduOwnerOrgUnit.logic.IEduOwnerOrgUnitDao;
/**
 * @author Alexander Zhebko
 * @since 04.02.2014
 */
@Configuration
public class EduOwnerOrgUnitManager extends BusinessObjectManager
{
    public static EduOwnerOrgUnitManager instance() {
        return instance(EduOwnerOrgUnitManager.class);
    }

    @Bean
    public IEduOwnerOrgUnitDao dao() {
        return new EduOwnerOrgUnitDao();
    }
}