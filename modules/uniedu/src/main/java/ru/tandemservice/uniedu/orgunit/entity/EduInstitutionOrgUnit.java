package ru.tandemservice.uniedu.orgunit.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.gen.EduInstitutionOrgUnitGen;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Аккредитованное подразделение
 *
 * Подразделение, для которого были отдельно аккредитованы направления (профессии, специальности), т.е. сама организация (головное подразделение) и ее филиалы.
 */
public class EduInstitutionOrgUnit extends EduInstitutionOrgUnitGen implements ITitled
{
    public static final String PARAM_EDIT_FORM = "editForm";

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String ownerId) {
        final EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(ownerId, EduInstitutionOrgUnit.class) {
            @Override protected boolean isExternalSortRequired() {
                return true;
            }
            @Override protected List<IEntity> sortSelectedValues(List<IEntity> list, Set primaryKeys) {
                Collections.sort(list, EDU_INST_COMPARATOR);
                return list;
            }
            @Override protected List<IEntity> sortOptions(List<IEntity> list) {
                Collections.sort(list, EDU_INST_COMPARATOR);
                return list;
            }
        };

        handler.customize((alias, dql, context, filter) -> {
            dql.joinPath(DQLJoinType.inner, EduInstitutionOrgUnit.orgUnit().fromAlias(alias), "ou");

            if (!Boolean.TRUE.equals(context.get(PARAM_EDIT_FORM))) {
                // Если это форма редактирования обр. программы, то нужно чтобы выбранный ранее орг.юнит попал в выборку,
                // даже если он архивный (иначе поредактировать они не смогут)
                dql.where(eq(property("ou", OrgUnit.archival()), value(false)));
            }

            // Фильтр по сокр. названию тоже непростой, т.к. для головной организации там хардкод
            OrgUnit.applyShortTitleDQLFilter(dql, "ou", filter);

            return dql;
        });
        handler.titleProperty(EduInstitutionOrgUnit.orgUnit().shortTitleWithTopEmphasized().s());
        handler.pageable(true);
        return handler;
    }

    private static final Comparator<IEntity> EDU_INST_COMPARATOR = (o1, o2) -> {
        final OrgUnit i1 = ((EduInstitutionOrgUnit) o1).getOrgUnit();
        final OrgUnit i2 = ((EduInstitutionOrgUnit) o2).getOrgUnit();
        int ret;
        if (0 != (ret = Boolean.compare(i1.isTop(), i2.isTop()))) return -ret; // Головная организация идет первой
        if (0 != (ret = CommonCollator.RUSSIAN_COLLATOR.compare(i1.getShortTitle(), i2.getShortTitle()))) return ret;
        return ret;
    };

    @Override
    public String getTitle()
    {
        if (getOrgUnit() == null) {
            return this.getClass().getSimpleName();
        }
        return getOrgUnit().getTitle();
    }
}