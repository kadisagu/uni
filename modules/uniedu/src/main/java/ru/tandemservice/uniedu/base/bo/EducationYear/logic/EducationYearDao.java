package ru.tandemservice.uniedu.base.bo.EducationYear.logic;

import java.util.LinkedHashMap;
import java.util.Map;

import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.program.entity.EduProgram;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EducationYearDao extends UniBaseDao implements IEducationYearDao {

    @Override
    public EducationYear getCurrent() {
        // TODO: cache me
        return get(EducationYear.class, EducationYear.current(), Boolean.TRUE);
    }

    @Override
    public Map<String, EducationYear> getEducationYearMapByCode() {
        final Map<String, EducationYear> result = new LinkedHashMap<>();
        for (final EducationYear year: getList(EducationYear.class, EducationYear.P_INT_VALUE)) {
            if (null != result.put(year.getCode(), year)) {
                throw new IllegalStateException();
            }
        }
        return result;
    }

    @Override
    public Map<Integer, EducationYear> getEducationYearMapByNumber() {
        final Map<Integer, EducationYear> result = new LinkedHashMap<>();
        for (final EducationYear year: getList(EducationYear.class, EducationYear.P_INT_VALUE)) {
            if (null != result.put(year.getIntValue(), year)) {
                throw new IllegalStateException();
            }
        }
        return result;
    }

    @Override
    public <T extends EduProgram> T getProgramCopy(T program, EducationYear year)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgram.class, "p")
                .column("p")
                .where(eq(property("p", EduProgram.year()), value(year)))
                .where(eq(property("p", EduProgram.programUniqueKey()), value(program.getProgramUniqueKey())));

        return dql.createStatement(getSession()).uniqueResult();
    }

    @Override
    public <T extends EduProgram> T getProgramCopy(T program, int year)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgram.class, "p")
                .column("p")
                .where(eq(property("p", EduProgram.year().intValue()), value(year)))
                .where(eq(property("p", EduProgram.programUniqueKey()), value(program.getProgramUniqueKey())));

        return dql.createStatement(getSession()).uniqueResult();
    }

}
