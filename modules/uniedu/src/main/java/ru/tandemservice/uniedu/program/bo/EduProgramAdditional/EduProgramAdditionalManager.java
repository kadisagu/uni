/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramAdditional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.logic.EduProgramSecondaryProfDao;
import ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.logic.IEduProgramSecondaryProfDao;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 10.02.2014
 */
@Configuration
public class EduProgramAdditionalManager extends BusinessObjectManager
{
    public static final String EDU_PROGRAM_KIND_ADDITIONAL_DS = "eduProgramKindAdditionalDS";

    public static EduProgramAdditionalManager instance()
    {
        return instance(EduProgramAdditionalManager.class);
    }

    @Bean
    public UIDataSourceConfig programKindAdditionalDSConfig(){
        return CommonBaseStaticSelectDataSource.selectDS(EDU_PROGRAM_KIND_ADDITIONAL_DS, getName(), EduProgramKind.defaultSelectDSHandler(getName())
                .where(EduProgramKind.programAdditionalProf(), true))
                .create();
    }
}