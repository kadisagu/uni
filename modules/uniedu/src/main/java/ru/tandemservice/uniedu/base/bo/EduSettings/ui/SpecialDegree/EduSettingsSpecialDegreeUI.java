/* $Id$ */
package ru.tandemservice.uniedu.base.bo.EduSettings.ui.SpecialDegree;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniedu.base.bo.EduSettings.EduSettingsManager;

/**
 * @author Nikolay Fedorovskih
 * @since 30.04.2014
 */
public class EduSettingsSpecialDegreeUI extends UIPresenter
{
    public void onToggleSpecialDegreeOn()
    {
        EduSettingsManager.instance().dao().setAssignSpecialDegreeForEduProgram2009(getListenerParameterAsLong(), true);
    }

    public void onToggleSpecialDegreeOff()
    {
        EduSettingsManager.instance().dao().setAssignSpecialDegreeForEduProgram2009(getListenerParameterAsLong(), false);
    }
}