package ru.tandemservice.uniedu.program.entity;

import ru.tandemservice.uniedu.program.entity.gen.EduProgramBasicGen;

/**
 * Основная общеобразовательная программа
 *
 * Основное общее образование: реализуется по уровням основного образования, уровень берется из вида образовательной программы.
 */
public class EduProgramBasic extends EduProgramBasicGen
{

    @Override
    protected StringBuilder uniqueKeyBuilder() {
        throw new UnsupportedOperationException(); // пока мы не поддерживаем такие ОП
    }

    @Override
    protected StringBuilder titleBuilder() {
        throw new UnsupportedOperationException(); // пока мы не поддерживаем такие ОП
    }

}