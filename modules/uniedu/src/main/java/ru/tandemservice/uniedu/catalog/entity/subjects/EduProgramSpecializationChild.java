package ru.tandemservice.uniedu.catalog.entity.subjects;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSpecializationChildGen;

/**
 * Направленность в рамках направления
 *
 * Соответсвует профилю направления подготовки
 */
public class EduProgramSpecializationChild extends EduProgramSpecializationChildGen
{
    @Override public boolean isRootSpecialization() {
        return false;
    }

    @Override
    @EntityDSLSupport(parts = P_TITLE)
    public String getDisplayableTitle()
    {
        return getTitle();
    }
}