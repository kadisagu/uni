/**
 * $Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.bo.EduProgramQualification.EduProgramQualificationManager;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramTrait;
import ru.tandemservice.uniedu.catalog.entity.subjects.*;
import ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.EduInstitutionOrgUnitManager;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.02.2014
 */
@Configuration
public class EduProgramHigherProfAddEdit extends BusinessComponentManager
{
    public static final String EDU_BASE_LEVEL_DS = "baseLevelDS";
    public static final String EDU_PROGRAM_SUBJECT_INDEX_DS = "eduProgramSubjectIndexDS";
    public static final String EDU_PROGRAM_SUBJECT_DS = "eduProgramSubjectDS";
    public static final String EDU_PROGRAM_SPECIALIZATION_DS = "eduProgramSpecializationDS";
    public static final String EDU_PROGRAM_QUALIFICATION_DS = "qualificationDS";
    public static final String EDU_PROGRAM_FORM_DS = "eduProgramFormDS";
    public static final String EDU_PROGRAM_DURATION_DS = "eduProgramDurationDS";
	public static final String EDU_OWNER_ORG_UNIT_DS = "eduOwnerOrgUnitDS";
    public static final String EDU_PROGRAM_TRAIT_DS = "eduProgramTraitDS";
    public static final String EDU_PROGRAM_SUBJECT = "eduProgramSubject";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EduProgramHigherProfManager.instance().programKindHigherProfDSConfig())
                .addDataSource(selectDS(EDU_PROGRAM_SUBJECT_INDEX_DS, eduProgramSubjectIndexDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_SUBJECT_DS, eduProgramSubjectDSHandler())
                .addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(selectDS(EDU_BASE_LEVEL_DS, EduProgramHigherProfManager.instance().baseLevelDS()))
                .addDataSource(selectDS(EDU_PROGRAM_SPECIALIZATION_DS, eduProgramSpecializationDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_QUALIFICATION_DS, qualificationDSHandler()))
                .addDataSource(EduProgramQualificationManager.instance().orientationDSConfig())
                .addDataSource(EducationYearManager.instance().eduYearDSConfig())
                .addDataSource(selectDS(EDU_PROGRAM_FORM_DS, eduProgramFormDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_DURATION_DS, eduProgramDurationDSHandler()))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(EDU_OWNER_ORG_UNIT_DS, getName(), EduOwnerOrgUnit.defaultDSHandler(getName())))
                .addDataSource(EduInstitutionOrgUnitManager.instance().eduInstitutionOrgUnitDSConfig())
                .addDataSource(selectDS(EDU_PROGRAM_TRAIT_DS, eduProgramTraitDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSubjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
                .where(EduProgramSubjectIndex.programKind(), EduProgramHigherProfManager.EDU_PROGRAM_KIND)
                .order(EduProgramSubjectIndex.priority())
                .filter(EduProgramSubjectIndex.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
                .where(EduProgramSubject.subjectIndex(), EduProgramSubjectIndexManager.PARAM_SUBJECT_INDEX)
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramSpecializationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSpecializationChild.class)
                .where(EduProgramSpecializationChild.programSubject(), EDU_PROGRAM_SUBJECT)
                .order(EduProgramSpecializationChild.title())
                .filter(EduProgramSpecializationChild.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler qualificationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramQualification.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(new DQLSelectBuilder()
                        .fromEntity(EduProgramSubjectQualification.class, "sq")
                        .where(eq(property(EduProgramSubjectQualification.programQualification().fromAlias("sq")), property(alias)))
                        .where(eq(property(EduProgramSubjectQualification.programSubject().fromAlias("sq")), commonValue(context.get(EDU_PROGRAM_SUBJECT))))
                        .buildQuery()));
            }
        }
                .order(EduProgramQualification.title())
                .filter(EduProgramQualification.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class)
                .order(EduProgramForm.title())
                .filter(EduProgramForm.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramDurationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramDuration.class)
                .order(EduProgramDuration.title())
                .filter(EduProgramDuration.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler eduProgramTraitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramTrait.class)
                .filter(EduProgramTrait.title())
                .order(EduProgramTrait.title());
    }
}