package ru.tandemservice.uniedu.catalog.entity.basic;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.BaseDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogDesc;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.uniedu.catalog.bo.EduProgramDuration.ui.AddEdit.EduProgramDurationAddEdit;
import ru.tandemservice.uniedu.catalog.entity.basic.gen.EduProgramDurationGen;

/** @see EduProgramDurationGen */
public class EduProgramDuration extends EduProgramDurationGen implements IDynamicCatalogItem
{
    public static IDynamicCatalogDesc getUiDesc() {
        return new BaseDynamicCatalogDesc() {
            @Override public String getAddEditComponentName() { return EduProgramDurationAddEdit.class.getSimpleName(); }
        };
    }

}