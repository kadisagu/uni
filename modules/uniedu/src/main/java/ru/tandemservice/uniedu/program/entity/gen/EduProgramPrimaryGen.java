package ru.tandemservice.uniedu.program.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramPrimary;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Основная образовательная программа
 *
 * Основное образование (все, что не является дополнительным).
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EduProgramPrimaryGen extends EduProgram
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniedu.program.entity.EduProgramPrimary";
    public static final String ENTITY_NAME = "eduProgramPrimary";
    public static final int VERSION_HASH = -1399989736;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EduProgramPrimaryGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EduProgramPrimaryGen> extends EduProgram.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EduProgramPrimary.class;
        }

        public T newInstance()
        {
            throw new RuntimeException("EduProgramPrimary is abstract");
        }
    }
    private static final Path<EduProgramPrimary> _dslPath = new Path<EduProgramPrimary>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EduProgramPrimary");
    }
            

    public static class Path<E extends EduProgramPrimary> extends EduProgram.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return EduProgramPrimary.class;
        }

        public String getEntityName()
        {
            return "eduProgramPrimary";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
