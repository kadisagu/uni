/**
 *$Id$
 */
package ru.tandemservice.uniedu.program.bo.EduProgramSecondaryProf.ui.List.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniedu.program.entity.EduProgramSecondaryProf;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 10.02.2014
 */
public class EduProgramSecondaryProfListDSHandler extends DefaultSearchDataSourceHandler
{
	public static final String EDU_PROGRAM_TITLE = "eduProgramTitle";
	public static final String EDU_PROGRAM_KIND = "eduProgramKind";
	public static final String EDU_PROGRAM_FORM = "eduProgramForm";
	public static final String EDU_PROGRAM_DURATION = "eduProgramDuration";
	public static final String EDU_YEAR = "eduYear";
	public static final String EDU_PROGRAM_SUBJECT_INDEX = "eduProgramSubjectIndex";
	public static final String EDU_PROGRAM_SUBJECT = "eduProgramSubject";
	public static final String EDU_OWNER_ORG_UNIT = "eduOwnerOrgUnit";
	public static final String EDU_INSTITUTION_ORG_UNIT = "eduInstitutionOrgUnit";
	public static final String BIND_NEXT_YEAR_COPY = "nextYearCopy";

    public EduProgramSecondaryProfListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EduProgramSecondaryProf.class, "e")
                .fetchPath(DQLJoinType.inner, EduProgramSecondaryProf.programSubject().fromAlias("e"), "ps")
                .fetchPath(DQLJoinType.inner, EduProgramSecondaryProf.programSubject().subjectIndex().fromAlias("e"), "psi")
                .fetchPath(DQLJoinType.inner, EduProgramSecondaryProf.ownerOrgUnit().orgUnit().fromAlias("e"), "ouo")
                .fetchPath(DQLJoinType.inner, EduProgramSecondaryProf.institutionOrgUnit().orgUnit().fromAlias("e"), "iou")
                .fetchPath(DQLJoinType.inner, EduProgramSecondaryProf.kind().fromAlias("e"),  "k")
                .fetchPath(DQLJoinType.inner, EduProgramSecondaryProf.form().fromAlias("e"),  "f")
                .fetchPath(DQLJoinType.inner, EduProgramSecondaryProf.duration().fromAlias("e"),  "d")
                .fetchPath(DQLJoinType.inner, EduProgramSecondaryProf.year().fromAlias("e"),  "y");

        String eduProgramTitle = StringUtils.trimToNull(context.<String>get(EDU_PROGRAM_TITLE));
        EduProgramKind eduProgramKind = context.get(EDU_PROGRAM_KIND);
        EduProgramForm eduProgramForm = context.get(EDU_PROGRAM_FORM);
        EduProgramDuration eduProgramDuration = context.get(EDU_PROGRAM_DURATION);
        EducationYear eduYear = context.get(EDU_YEAR);
        EduProgramSubjectIndex eduProgramSubjectIndex = context.get(EDU_PROGRAM_SUBJECT_INDEX);
        EduProgramSubject eduProgramSubject = context.get(EDU_PROGRAM_SUBJECT);
        EduOwnerOrgUnit eduOwnerOrgUnit = context.get(EDU_OWNER_ORG_UNIT);
        EduInstitutionOrgUnit eduInstitutionOrgUnit = context.get(EDU_INSTITUTION_ORG_UNIT);
        DataWrapper nextYearCopy = context.get(BIND_NEXT_YEAR_COPY);

        if (eduProgramTitle != null)
            builder.where(likeUpper(property("e", EduProgramSecondaryProf.title()), value(CoreStringUtils.escapeLike(eduProgramTitle))));

        if (eduProgramKind != null)
            builder.where(eq(property("e", EduProgramSecondaryProf.kind()), value(eduProgramKind)));

        if (eduProgramForm != null)
            builder.where(eq(property("e", EduProgramSecondaryProf.form()), value(eduProgramForm)));

        if (eduProgramDuration != null)
            builder.where(eq(property("e", EduProgramSecondaryProf.duration()), value(eduProgramDuration)));

        if (eduYear != null)
            builder.where(eq(property("e", EduProgramSecondaryProf.year()), value(eduYear)));

        if (eduProgramSubjectIndex != null)
            builder.where(eq(property("e", EduProgramSecondaryProf.programSubject().subjectIndex()), value(eduProgramSubjectIndex)));

        if (eduProgramSubject != null)
            builder.where(eq(property("e", EduProgramSecondaryProf.programSubject()), value(eduProgramSubject)));

        if (eduOwnerOrgUnit != null)
            builder.where(eq(property("e", EduProgramSecondaryProf.ownerOrgUnit()), value(eduOwnerOrgUnit)));

        if (eduInstitutionOrgUnit != null)
            builder.where(eq(property("e", EduProgramSecondaryProf.institutionOrgUnit()), value(eduInstitutionOrgUnit)));

        if (nextYearCopy != null)
        {
            IDQLSelectableQuery nextYearCopyQuery = new DQLSelectBuilder()
                .fromEntity(EduProgramSecondaryProf.class, "p").column(property("p.id"))
                .where(eq(property("e", EduProgramSecondaryProf.programUniqueKey()), property("p", EduProgramSecondaryProf.programUniqueKey())))
                .where(eq(plus(property("e", EduProgramSecondaryProf.year().intValue()), value(1)), property("p", EduProgramSecondaryProf.year().intValue())))
                .buildQuery();

            if (TwinComboDataSourceHandler.getSelectedValueNotNull(nextYearCopy))
                builder.where(exists(nextYearCopyQuery));
            else
                builder.where(notExists(nextYearCopyQuery));
        }

        DQLOrderDescriptionRegistry orderDescription = new DQLOrderDescriptionRegistry(EduProgramSecondaryProf.class, "e");
        {
            orderDescription.setOrders(EduProgramSecondaryProf.title(), new OrderDescription("e", EduProgramSecondaryProf.title()));
            orderDescription.setOrders(EduProgramSecondaryProf.kind().shortTitle(), new OrderDescription("e", EduProgramSecondaryProf.kind().shortTitle()));
            orderDescription.setOrders(EduProgramSecondaryProf.form().title(), new OrderDescription("e", EduProgramSecondaryProf.form().title()));
            orderDescription.setOrders(EduProgramSecondaryProf.duration().title(), new OrderDescription("e", EduProgramSecondaryProf.duration().title()));
            orderDescription.setOrders(EduProgramSecondaryProf.year().title(), new OrderDescription("e", EduProgramSecondaryProf.year().intValue()));
            orderDescription.setOrders(EduProgramSecondaryProf.programSubject().subjectIndex().code(), new OrderDescription("e", EduProgramSecondaryProf.programSubject().subjectIndex().code()));
            orderDescription.setOrders(EduProgramSecondaryProf.programSubject().titleWithCode(),
                    new OrderDescription("e", EduProgramSecondaryProf.programSubject().code()),
                    new OrderDescription("e", EduProgramSecondaryProf.programSubject().title()));

            orderDescription.setOrders(EduProgramSecondaryProf.ownerOrgUnit().orgUnit().shortTitle(), new OrderDescription("e", EduProgramSecondaryProf.ownerOrgUnit().orgUnit().shortTitle()));
            orderDescription.setOrders(EduProgramSecondaryProf.institutionOrgUnit().orgUnit().shortTitle(), new OrderDescription("e", EduProgramSecondaryProf.institutionOrgUnit().orgUnit().shortTitle()));
        }

        orderDescription.applyOrder(builder, input.getEntityOrder());

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }

}