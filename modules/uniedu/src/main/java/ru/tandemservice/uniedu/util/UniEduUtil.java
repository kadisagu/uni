package ru.tandemservice.uniedu.util;

import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * @author avedernikov
 * @since 13.01.2017
 */
public class UniEduUtil
{
	private static final String PERMITTED_NON_ALPHABETIC_SYMBOLS = "()";

	private static final List<String> CONJUNCTIONS = Arrays.asList("И", "ДА", "НО", "ИЛИ", "ЛИБО");

	private static final List<String> PREPOSITIONS = Arrays.asList("В", "БЕЗ", "ДО", "ИЗ", "К", "НА", "ПО", "О", "ОТ", "ПЕРЕД", "ПРИ", "ЧЕРЕЗ", "С", "У", "ЗА", "НАД", "ОБ", "ПОД", "ПРО", "ДЛЯ");

	/** Сформировать сокрю название. */
	public static String buildAbbreviation(String source)
	{
		if (StringUtils.isEmpty(source))
			return null;

		source = source.toUpperCase();

		Set<Character> sourceChars = new HashSet<>();
		for (char c: source.toCharArray())
			sourceChars.add(c);

		for (Character c: sourceChars)
		{
			if (StringUtils.contains(PERMITTED_NON_ALPHABETIC_SYMBOLS, c))
				source = StringUtils.replace(source, c.toString(), " " + c + " ");
			else if (!Character.isLetter(c))
				source = StringUtils.replace(source, c.toString(), " ");
		}

		List<String> resultChars = new ArrayList<>();
		for (String word: StringUtils.split(source, ' '))
		{
			if (!CONJUNCTIONS.contains(word) && !PREPOSITIONS.contains(word))
				resultChars.add(word.substring(0, 1));
		}

		return StringUtils.join(resultChars, "");
	}
}
