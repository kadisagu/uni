package ru.tandemservice.uniedu.orgunit.bo.EduInstitutionOrgUnit.logic;

import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.HibSupportUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.gen.EduInstitutionOrgUnitGen;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author vdanilov
 */
public class EduInstitutionOrgUnitDao extends UniBaseDao implements IEduInstitutionOrgUnitDao {

    @Override
    public EduInstitutionOrgUnit doGetEduInstitutionOrgUnit(OrgUnit orgUnit) {
        Session session = lock("EduInstitutionOrgUnit."+orgUnit.getId());

        EduInstitutionOrgUnit ou = HibSupportUtils.getByNaturalId(session, new EduInstitutionOrgUnitGen.NaturalId(orgUnit));
        if (null != ou) { return ou; }

        ou = new EduInstitutionOrgUnit();
        ou.setOrgUnit(orgUnit);
        session.save(ou);
        session.flush();

        return ou;
    }

    @Override
    public void deleteEduInstitutionOrgUnit(Long eduInstitutionOrgUnitId)
    {
        delete(eduInstitutionOrgUnitId);
    }

	@Override
	public void updateEduInstitutionOrgUnit(EduInstitutionOrgUnit orgUnit)
	{
		DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EduInstitutionOrgUnit.class, "ou")
				.top(1).column("ou.id")
				.where(eq(property("ou", EduInstitutionOrgUnit.P_CODE), value(orgUnit.getCode())))
				.where(ne(property("ou.id"), value(orgUnit.getId())));
		Long id = builder.createStatement(getSession()).uniqueResult();
		if (id != null)
		{
			throw new ApplicationException("Введенный код уже используется в системе, введите другой код.");
		}

		update(orgUnit);
	}
}
