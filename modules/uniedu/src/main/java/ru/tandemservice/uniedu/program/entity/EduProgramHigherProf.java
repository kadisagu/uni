package ru.tandemservice.uniedu.program.entity;

import ru.tandemservice.uniedu.program.entity.gen.EduProgramHigherProfGen;

/**
 * Образовательная программа ВПО
 *
 * Высшее образование: реализуется по направлениям подготовки (специальностям) с указанием направленности.
 */
public class EduProgramHigherProf extends EduProgramHigherProfGen
{
    @Override
    protected StringBuilder uniqueKeyBuilder() {
        // При изменении ключа не забыть исправить ProgramKeyUniqueHandler
        return super.uniqueKeyBuilder()
        .append(Long.toString(this.getProgramSpecialization().getId(), 32)).append("/")
        .append(Long.toString(this.getProgramQualification().getId(), 32)).append("/") // Квалификация здесь, а не в EduProgramProf - так надо для СПО.
        .append(this.getProgramOrientation() != null ? Long.toString(this.getProgramOrientation().getId(), 32) : "null")
        ;
    }

    @Override protected String getProgramSubjectTitlePart() {
        return this.getProgramSpecialization().getTitle();
    }

    @Override
    protected String getOrientationShortTitle() {
        return this.getProgramOrientation() != null ? this.getProgramOrientation().getShortTitle() : null;
    }
}