/* $Id:$ */
package ru.tandemservice.uniedu.catalog.bo.EduProgramQualification;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;

/**
 * @author rsizonenko
 * @since 08.09.2014
 */
@Configuration
public class EduProgramQualificationManager extends BusinessObjectManager {

    public static EduProgramQualificationManager instance() {
        return instance(EduProgramQualificationManager.class);
    }

    @Bean
    public UIDataSourceConfig orientationDSConfig() {
        return CommonBaseStaticSelectDataSource.selectDS(EduProgramOrientation.DS_ORIENTATION, getName(), EduProgramOrientation.defaultSelectDSHandler(getName()))
                .create();
    }

}
